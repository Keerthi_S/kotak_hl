package com.ycs.tenjin.adapter.selenium.kotaksfdchl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.selenium.asv2.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv2.generic.WebMethodsImpl;
import com.ycs.tenjin.bridge.selenium.asv2.web.ExecutionMethodsAbstract;
import com.ycs.tenjin.util.Utilities;

public class ExecutionMethodsImpl extends ExecutionMethodsAbstract{

	protected static final Logger logger = LoggerFactory.getLogger(LearningMethodsImpl.class);

	protected GenericAdapterMethods genericAdapterMethods;

	
	
	public ExecutionMethodsImpl(DriverContext driverContext, WebMethodsImpl webGenericMethods) {
		super(driverContext, webGenericMethods);
		this.genericAdapterMethods = new GenericAdapterMethods(driverContext, webGenericMethods);
	}

	@Override
	public WebElement locateMRBElement(TestObject t, WebElement targetRowForMRB, int objectIdentificationTimeout)
			throws AutException {
		WebElement ele = null;
		
		if(t.getAction()!=null) { 
			if(t.getAction().equalsIgnoreCase("query")){
			return ele;
		}
		}
		
		if(t.getObjectClass().equalsIgnoreCase("button")) {
			
			if(t.getData().equalsIgnoreCase("edit")) {
				
				try {
					ele = this.driverContext.getDriver().findElement(By.xpath(".//a[@title='"+t.getData()+"']| .//button[@name='"+t.getData()+"']"));
				} catch (Exception e) {
					ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//button[text()='"+t.getData()+"' and @class='slds-button slds-button_brand']|.//button[text()='"+t.getData()+"' and @class='slds-button slds-button_destructive']"),5);
				}
				if(ele!=null){
					return ele;
				}
				
			}else {
				
				ele =  this.locateElementExtend(t);
				
			}
		}else if(t.getData()!=null && t.getData().equalsIgnoreCase("Approve")){
			
			return ele;
			
		}else {
			String[] clmnID = t.getUniqueId().split("_");
		   
			int n = Integer.parseInt(clmnID[1])+1;
			
			WebElement	td = null;
			try {
					try {
						try {
							td = targetRowForMRB.findElement(By.xpath(".//th[" + (n-1) + "]"));
						} catch (Exception e) {
							td = targetRowForMRB.findElement(By.xpath(".//td[" + n + "]"));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				
				
					try {
						
						ele = td.findElement(By.xpath(".//input|.//select|.//a"));
						
					}catch(NoSuchElementException e) {
						
						ele = td.findElement(By.xpath(".//button"));
					}
					
					if(ele.getAttribute("type").equalsIgnoreCase("radio")) {
						if(t.getData()!=null)
						ele=td.findElement(By.xpath(".//lightning-radio-group//span[text()='"+t.getData()+"']"));
					}
					if(ele.getAttribute("type").contentEquals("checkbox")) {
						ele = ele.findElement(By.xpath(".//following::span[1]"));
						t.setObjectClass("CHECKBOX");
					}
					
					if(ele==null) {
						ele = td.findElement(By.xpath(".//div"));
					}
					
					
			}catch (Exception e) {
				
				
			  td = targetRowForMRB.findElements(By.xpath(".//td|.//th")).get(n-1);
				
				
				ele = td.findElement(By.xpath(".//input|.//select|.//a"));
				
				if(ele==null) {
					ele = td.findElement(By.xpath(".//div"));
				}
			
				
			}
			
			
			Map<String, String> attributes = this.genericAdapterMethods.getHTMLAttributesOfElement(ele);
			
			if(attributes.containsValue("listbox")||attributes.containsValue("search")||attributes.containsValue("Select an Option")) {
				t.setObjectClass("LIST");t.setIdentifiedBy("LIST");
			}else if(attributes.containsValue("textbox")||attributes.containsValue("text")) {
				t.setObjectClass("TEXTBOX");t.setIdentifiedBy("TEXTBOX");
			}else if(ele.getTagName().equals("a")||ele.getTagName().equals("button")||ele.getTagName().equals("span")) {
				t.setObjectClass("BUTTON");t.setIdentifiedBy("BUTTON");
			}else if(attributes.containsValue("checkbox")) {
				t.setObjectClass("RADIO");t.setUniqueId(ele.getAttribute("title"));
			}
			
		}
		return ele;
		
	}

	@Override
	public void performMRBInput(WebElement element, TestObject t, String data) throws AutException {
		
		if(t.getAction().equalsIgnoreCase("input")) {
			
			if(element!=null) {
				
				this.genericAdapterMethods.moveToCenterOfScreen(element);
				
			}
			if(data.equalsIgnoreCase("Approve")) {
				String[] clmnID = t.getUniqueId().split("_");
				   
				int n = Integer.parseInt(clmnID[1])+1;
				
				List<WebElement> elements=this.driverContext.getDriver().findElements(By.xpath(".//table//tbody/tr//td[" + n + "]//input"));
				try {
					for(WebElement ele :elements) {
						this.genericAdapterMethods.moveToCenterOfScreen(ele);
						ele.click();
						this.webGenericMethods.locateVisibleElement(By.xpath(".//lightning-base-combobox-item//span[text()='"+data+"']"),5).click();
		
					}
				} catch (Exception e) {
				}
			}
			else {
			this.performInput(element, t, data);
			}
		}
		
		
	}

	@Override
	public String performMRBOutput(WebElement element, TestObject t) throws AutException {
		
		String value = null;
		
		value = element.getText();
		
		return value;
	}
	
	public WebElement checkVisibleElement(List<WebElement> eles) {
		for (WebElement webElement : eles) {
			if (webElement.isDisplayed()) {
				return webElement;
			}
		}
		return null;
	}

	@Override
	public WebElement locateElementExtend(TestObject t) throws AutException {
		WebElement ele = null;
		
		if(t.getObjectClass().equalsIgnoreCase(FieldType.BUTTON.name())) {
			
			if(this.driverContext.getDriver().getWindowHandles().size()>1){
				
				this.webGenericMethods.switchDriverToLatestWindow();
			
			}
			if(t.getData().startsWith("@@")) {
				return this.driverContext.getDriver().findElement(By.xpath(".//html"));
			}
			// Identifying the button by name
			if (t.getData().equalsIgnoreCase("close"))
				ele = this.driverContext.getDriver().findElement(By.xpath(".//button[@title='Close this window']|.//button[@title='close']"));
			else if (t.getData().startsWith("Loan Applicants"))
				ele = this.driverContext.getDriver().findElement(By.xpath(".//span[contains(text(),'Loan Applicants')]"));
			else if (t.getData().startsWith("Disbursement Request"))
				ele = this.driverContext.getDriver().findElement(By.xpath(".//span[contains(text(),'Disbursement Request')]/ancestor::a[1]"));
			else if (t.getData().equalsIgnoreCase("Generate Sanction Letter"))
				ele = this.driverContext.getDriver().findElement(By.xpath(".//button[text()='" + t.getData() + "']"));
			else if (t.getData().equalsIgnoreCase("Show more actions"))
				ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//span[text()='" + t.getData() + "']/ancestor::button[1]"),3);
			else if (t.getData().contains(";;")) {
				int indexVal = Integer.parseInt(t.getData().split(";;")[1])-1;
				String data = t.getData().split(";;")[0];
				try {
				ele = this.driverContext.getDriver().findElements(By.xpath(".//button[text()='"+data+"']|.//button[@title='"+data+"']")).get(indexVal);
				}catch (Exception e) {
					ele = this.driverContext.getDriver().findElements(By.xpath(".//span[text()='"+data+"']/ancestor::a[1]")).get(indexVal);
				}
			}
			//Generic identifying the buttons
			try {
				if (ele == null) {
					List<WebElement> eles = this.driverContext.getDriver().findElements(By.xpath(".//a[text()='" + t.getData() + "']"));
					ele = this.checkVisibleElement(eles);
				}}catch (Exception e) {}
			try {
			if (ele == null) {
				List<WebElement> elets = this.driverContext.getDriver().findElements(By.xpath(".//button[text()='"+t.getData()+"']|.//button[@title='"+t.getData()+"']"));
				ele = this.checkVisibleElement(elets);
			}}catch(Exception e1) {}
			if (ele == null) 
				ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//button/span[text()='"+t.getData()+"']"),2);
			if (ele == null)
				ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//a[@title='" + t.getData() + "']"),2);
			if (ele == null)
				ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//a[@title='" + t.getData() + "']/span | .//span[text()='"+t.getData()+"']/ancestor::a[1]"),2);
			if (ele == null) 
				ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//a/span[text()='" + t.getData() + "']|.//a[contains(text(),'"+t.getData()+"')]|.//span[contains(text(),'"+t.getData()+"')]/ancestor::a[1]"),2);
			/*if(ele==null) {
				ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//a[@title='"+t.getData()+"']/span"),5);
			}
			if(ele==null) {
				
				ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//a[@title='"+t.getData()+"']|.//a[text()='"+t.getData()+"']"),5);

			}*/
			
//			if(ele==null) {
//				
//				WebElement ele1 = this.webGenericMethods.locateVisibleElement(By.xpath(".//input[@placeholder='Search this list...']"),5);
//				if(ele1!=null) {
//					ele1.sendKeys(t.getData());
//					ele1.sendKeys(Keys.ENTER);
//					ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//a[@title='"+t.getData()+"']|.//a[text()='"+t.getData()+"']"),5);
//				}
//			}
			return ele;
			
		}else if(t.getObjectClass().equalsIgnoreCase(FieldType.LISTVIEW.name())) {
			
			ele = this.webGenericMethods.locateElement(By.xpath(".//div[text()='"+t.getUniqueId()+"']"),15);
			
			if(ele!=null) {
				
				String id=ele.getAttribute("id").split("-")[2];
				
				id="source-list-".concat(id);
				
				ele=this.webGenericMethods.locateElement(By.xpath(".//ul[@id='"+id+"']//li//span[text()='"+t.getData()+"']"),15);
				
			}
			return ele;
			
		}
		else if(t.getIdentifiedBy().equalsIgnoreCase("name")) {
			
			ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//*[@name='"+t.getUniqueId()+"']"),5);
			
			if(ele==null) {
				
				ele = this.webGenericMethods.locateElement(By.xpath(".//*[@name='"+t.getUniqueId()+"']"),5);
			}
			
			return ele;
			
		}else if(t.getIdentifiedBy().equalsIgnoreCase("label")) {
			
			WebElement ele1 = null;
			List<WebElement> eles=null;
			
			/*ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//label[text()='"+t.getLabel()+"']"),5);
			
			if(ele==null) {
				ele = this.webGenericMethods.locateElement(By.xpath(".//span[contains(text(),'"+t.getLabel()+"')]/ancestor::label"),5);
			}*/
			
			try {
				eles = this.driverContext.getDriver().findElements(By.xpath(".//label[text()='"+t.getLabel()+"' and contains(@class,'sl')]"));
				if(eles.isEmpty())
					throw new Exception();
			} catch (Exception e) {
				try {
					eles = this.driverContext.getDriver().findElements(By.xpath(".//span[contains(text(),'"+Utilities.trim(t.getLabel())+"')]/ancestor::label"));
				} catch (Exception e1) {
						eles = this.driverContext.getDriver().findElements(By.xpath(".//label[starts-with(text(),'"+t.getLabel().split("'")[0]+"') and contains(@class,'sl')]"));
				}
			}
			if(eles.isEmpty())
				eles = this.driverContext.getDriver().findElements(By.xpath(".//span[text()='"+t.getLabel()+"']"));

			if (!eles.isEmpty()) {
				for (WebElement elemets : eles) {
					if (elemets.isDisplayed()) {
						ele = elemets;
						break;
					}
				}
			}
			
			if(ele!=null) {
				
				ele1 = this.webGenericMethods.locateElement(By.xpath(".//*[@id='"+ele.getAttribute("for")+"']"),5);
				if(ele1==null) {
					
					ele1 = this.webGenericMethods.locateElement(By.xpath(".//*[@aria-describedby='"+ele.getAttribute("id")+"']"),5);
				}
				if(ele1==null) {
					
					ele1 = this.webGenericMethods.locateElement(By.xpath(".//*[@id='"+ele.getAttribute("id")+"']"),5);
				}
				
				if(ele1==null&&t.getLocation().equalsIgnoreCase("Technical Valuation")) {
					try {
						ele1 = ele.findElement(By.xpath("following::button[@aria-label='Select transaction type']"));
					} catch (Exception e) {}
				}
				if (ele1 == null) {
					try {
						String id = ele.getAttribute("for").replace("combobox", "listbox");
						ele1 = this.webGenericMethods.locateElement(By.xpath(".//*[@aria-controls='" + id + "']"), 5);
					} catch (AutException e) {
					}
				}
				
				if(ele1==null) {
					
					ele=this.webGenericMethods.locateVisibleElement(By.xpath(".//span[contains(text(),'"+t.getLabel()+"')]/ancestor::label/following::lightning-textarea/label"),5);
					
					ele1 = this.webGenericMethods.locateElement(By.xpath(".//*[@id='"+ele.getAttribute("for")+"']"),5);		
				}
			}
			
			if(ele==null && t.getObjectClass().equalsIgnoreCase("file")) {
				
				if(t.getLabel().contains("Or")) {
					
				String label=t.getLabel().split("Or")[0].trim();
				
				ele = this.webGenericMethods.locateElement(By.xpath(".//span[contains(text(),'"+label+"')]/ancestor::label"),5);
				
				ele1 = this.webGenericMethods.locateElement(By.xpath(".//*[@id='"+ele.getAttribute("for")+"']"),5);
				
				}
			}if(ele1==null) {
				
				ele1 = this.webGenericMethods.locateElement(By.xpath(".//input[@placeholder='"+t.getLabel()+"']"),5);
				
			}if(ele1==null) {
				//for a tag select
				ele1 = this.webGenericMethods.locateElement(By.xpath(".//span[text()='"+t.getLabel()+"' and contains(@id,'label')]/following::a[@class='select']"),5);
				
			}
			
			return ele1;
			
		}else if(t.getIdentifiedBy().equalsIgnoreCase("index")) {
			String compositeXpath = ".//input[@type='text' or @type='Text' or @type='TEXT']|.//textarea|.//input[@type='password' or @type='Password' or @type='PASSWORD' or @type='passWord' or @type='PassWord']|.//button[@title!='Remove selected option']|.//input[@type='button' or @type='submit' or @type='Button' or @type='Submit' or @type='BUTTON' or @type='SUBMIT']|.//select|.//input[@type='radio' or @type='Radio' or @type='RADIO']|.//input[@type='Checkbox' or @type='CHECKBOX' or @type='checkbox' or @type='checkBox']";
			compositeXpath+="|.//input[@role='textbox']|.//input[@type='file']|.//input[@type='search']|.//input[@type='phone']|.//input[@type='tel']|.//button[@aria-haspopup='listbox']";
			
			String[] ids= t.getUniqueId().split("_");
			
			String id =ids[ids.length-1];
			
			WebElement wrapper = this.webGenericMethods.locateElement(By.xpath(".//*[text()='"+t.getLocation()+"']//ancestor::div[@class='slds-accordion__summary']//following-sibling::div"),2);
			
			if(wrapper==null) {
				
				WebElement title = this.webGenericMethods.locateElement(By.xpath(".//li[@title='"+t.getLocation()+"']/a[@role='tab']"),2);
				
				if(title==null) {
					
					String location=t.getLocation().split(" ")[0];
					
					title=this.webGenericMethods.locateElement(By.xpath(".//li/a[contains(@data-label,'"+location+"')]"),2);
					
				}
				if(title!=null)
			 	wrapper = this.webGenericMethods.locateElement(By.xpath(".//lightning-tab[@id='"+title.getAttribute("aria-controls")+"']"),2);
				else
					wrapper = this.webGenericMethods.locateElement(By.xpath(".//*[text()='"+t.getLocation()+"']//ancestor::section[@role]"),2);
			}
			
			//Keerthi to identify check box
			if(wrapper==null && t.getObjectClass().equalsIgnoreCase("checkbox")) {
				wrapper = this.webGenericMethods.locateVisibleElement(By.xpath(".//span[text()='"+t.getLabel()+"']/ancestor::label[1]"),2);
				if(wrapper!=null)
				return this.driverContext.getDriver().findElement(By.xpath(".//input[@id='"+wrapper.getAttribute("for")+"']"));
			}
			
			List<WebElement> elements = wrapper.findElements(By.xpath(compositeXpath));
			
			ele = elements.get(Integer.parseInt(id));
			
			if(ele!=null) {
				
				WebElement ele1=this.webGenericMethods.locateElement(By.xpath(".//label[@for='"+ele.getAttribute("id")+"']"),2);
				
				String label;
				
				label=ele1.getText();
					
				if(label==null||label.isEmpty()) {
					
					String id1=ele.getAttribute("aria-controls").replace("listbox", "combobox");
	                
	                ele1 = this.webGenericMethods.locateElement(By.xpath(".//label[@for='"+id1+"']"),2);
	                
	                label=ele1.getText();
	                
				}
	                if(label.contains("*")) {
	                	label=label.substring(1);
	                }
				
				if(!label.equalsIgnoreCase(t.getLabel())) {
					throw new AutException("Couldnt locate the element,screen has changed");
				}
			}
		}
		
		return ele;
	}

	@Override
	public WebElement processButtonHandlersExtended(WebElement wrapper, TestObject t) throws AutException {

		return null;
	}
	
	@Override
	public boolean performInputExtend(WebElement element, TestObject t, String data) throws AutException {
		
		WebElement ele = null;
		if(t.getObjectClass().equalsIgnoreCase(FieldType.LIST.name())) {
			WebDriverWait wait = new WebDriverWait(this.driverContext.getDriver(), 4);
				try {
					if(t.getData().startsWith("@@SEARCH,")||t.getData().startsWith("@@ADD,")) {
					if (t.getData().startsWith("@@ADD,")) {
						element.click();
						Thread.sleep(800);
						element.findElement(By.xpath("following::lightning-base-combobox-item[1]")).click();
						return true;
					}else {
					String inData = t.getData().replace("@@SEARCH,", "");
					element.sendKeys(inData);
					Thread.sleep(800);
					element.findElement(By.xpath("following::lightning-base-combobox-item[1]")).click();
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//div[@class='searchScrollerWrapper']//a[@data-special-link='true']"))).click();
					//this.driverContext.getDriver().findElement(By.xpath(".//a[@title='"+inData+"']")).click();
					return true;}
					}
				} catch (Exception e) {
					logger.info("Could Not input List input :"+t.getData());
				}
			if(element.getTagName().equalsIgnoreCase("button")) {
	        	  
	        	  try {
	        		  //element.click();
	        		  ((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].click();", element);
					
						 ele=this.webGenericMethods.locateVisibleElement(By.xpath(".//lightning-base-combobox-item//span[text()='"+data+"']"),5);
						if(ele!=null) {
			        		  ((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].click();", ele);
							//ele.click();
						}
					} catch (Exception e) {}
	        	  
	        	  return true;
	        	  
	          }else if( t.getIdentifiedBy().equalsIgnoreCase("name")||element.getTagName().equalsIgnoreCase("input")) {
            	
	            try {
					element.click();
				} catch (Exception e) {
					try {
						JavascriptExecutor js = (JavascriptExecutor) this.driverContext.getDriver();
						js.executeScript("arguments[0].scrollIntoView();", element);
						element.click();
					} catch (Exception e1) {
						this.genericAdapterMethods.moveToCenterOfScreen(element);
						element.click();
					}
				}

	            if(element.getTagName().equalsIgnoreCase("input")) {
					    element.sendKeys(data);this.webGenericMethods.explicitWait(1);
	            }else if(element.getTagName().equalsIgnoreCase("select")) {
	            	return false;
	            }
				
	            try {
	            	Thread.sleep(500);
	            	ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//div[@title='"+data+"']/ancestor::li/a[1]"));
	            	if(ele==null)
	            	ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//lightning-base-combobox-item//span[text()='"+data+"']|.//span[text()='"+data+"']|.//span/span[normalize-space()='"+data+"']|.//div[@title='"+data+"']/ancestor::a[1]"),3);
	            	if(ele!=null) {
	            		if(data.equals("Life Insurance"))
	            			this.driverContext.getDriver().findElements(By.xpath(".//lightning-base-combobox-formatted-text[@title='Life Insurance']")).get(0).click();
	            		else
						ele.click();
					}else if(element.getAttribute("type").equalsIgnoreCase("search")) {
						element.sendKeys(Keys.ENTER);
						this.genericAdapterMethods.waitUntilPageLoad();
						return true;
					}else if(ele==null) {
						element.click();return false;}
	            } catch (Exception e) {
	            	
					try {
						if(ele==null) {
						ele=this.webGenericMethods.locateVisibleElement(By.xpath(".//lightning-base-combobox-item//span[text()='"+data+"']|.//span[text()='"+data+"']|.//span/span[normalize-space()='"+data+"']"),1);
						if(ele==null) 
						return false;
						((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].scrollIntoView(true);", ele);
						ele.click();
						}
					} catch (Exception e1) {}
				}
	           
	            return true;
	           
	          }else if(t.getIdentifiedBy().equalsIgnoreCase("label")&&element.getTagName().equalsIgnoreCase("a")) {
	        	  ((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].click();", element);
				try {
					Thread.sleep(500);
					ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//li/a[text()='" + data + "']"), 2);
					if (ele == null)
						return false;
					//((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].scrollIntoView(true);", ele);
					ele.click();
				} catch (InterruptedException e) {
				}
	        	  return true;
	          }
           
         }else if(t.getObjectClass().equalsIgnoreCase(FieldType.FILE.name())) {
				
			element.sendKeys(data);
				
			return true;
				
		}else if(t.getObjectClass().equalsIgnoreCase(FieldType.BUTTON.name())||t.getIdentifiedBy().equalsIgnoreCase("Button")) {
			
			if (t.getData().equalsIgnoreCase("@@click")) {
      		  ((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].click();", element);
				//element.click();
				return true;
			} else if (data.equalsIgnoreCase("Open In New Tab")) {
				this.handleNewWindow(element);
				return true;
			} else if (data.equalsIgnoreCase("Generate Agreement")) {
				this.handleAgreement(element);
				return true;
			} else if (data.equalsIgnoreCase("@@verifyotp")) {
				this.genericAdapterMethods.verifyMobileOTP();
				return true;
			} else if (data.startsWith("@@SELECT,")) {
				this.listselect(data);
				return true;
			} else if (data.startsWith("@@Fetch Aadhaar,")) {
				this.fetchAadhar(data);
				return true;
			} else if (data.startsWith("@@View All,")) {
				((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].click();",this.driverContext.getDriver().findElement(By.xpath(".//span[text()='"+t.getData().split(",")[1]+"']/ancestor::span[1]")));
				return true;
			}else if (data.equalsIgnoreCase("@@Switchtoinitialwindow")) {
				this.webGenericMethods.switchDriverToInitialWindow();
				return true;
			} else if (data.equalsIgnoreCase("@@Items to Approve")) {
				this.driverContext.getDriver().findElement(By.xpath(".//span[@title='Items to Approve']/ancestor::article//a[1]")).click();
				return true;
			} else if (data.equalsIgnoreCase("@@REFRESH")) {
				this.driverContext.getDriver().navigate().refresh();
				this.explicitWait(2);
				this.genericAdapterMethods.waitUntilPageLoad();
				return true;
			} else if (data.equalsIgnoreCase("@@BACK")) {
				this.driverContext.getDriver().navigate().back();
				this.explicitWait(2);
				this.genericAdapterMethods.waitUntilPageLoad();
				return true;
			} else if (data.startsWith("@@UPLOAD")) {
				this.uploadFile(data);
				return true;
			} else if (t.getData().equalsIgnoreCase("@@scroll")) {
				WebElement bar = null;
				List<WebElement> scrollbar = this.driverContext.getDriver().findElements(By.xpath(".//div[@class='slds-grid slds-wrap']"));
				for (WebElement scroll : scrollbar) {
					if (scroll.isDisplayed()) {
						bar = scroll;
						break;
					}
				}
				if (bar.getSize().getHeight() > 1000) {
					((JavascriptExecutor) this.driverContext.getDriver()).executeScript("window.scrollBy(0,1000)");
					return true;
				}
			} else if (data.startsWith("@@CHECK,")) {
				data = data.replace("@@CHECK,", "");
				this.driverContext.getDriver().findElement(By.xpath(".//a[@title='" + data + "']/preceding::label[1]/span[1]")).click();
				return true;
			} else if (data.equalsIgnoreCase("@@CLICK,Primary Borrower")) {
				List<WebElement> eles = this.driverContext.getDriver().findElements(By.xpath(".//p[@title='Primary Borrower']/parent::div//span"));
				for (WebElement el : eles) {
					if (el.isDisplayed()) {
						el.click();
						break;
					}
				}
				this.explicitWait(1);
				this.genericAdapterMethods.waitUntilPageLoad();
				return true;
			} else if (data.startsWith("@@RADIO,")) {
				data = data.replace("@@RADIO,", "");
				WebDriverWait wait = new WebDriverWait(this.driverContext.getDriver(), 5);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//td[text()='" + data + "']/following::input[1]")));
				return true;
			}else if (data.startsWith("@@CLICK,FIN")) {
				WebDriverWait wait = new WebDriverWait(this.driverContext.getDriver(), 5);
				WebElement finbutton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//span[text()='Applicant Financials']/following::records-hoverable-link[1]")));
				 ((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].click();", finbutton);
				return true;
			}else if (data.startsWith("@@SCREENSHOT,")) {
				return true;
			}
		} else if (t.getObjectClass().equalsIgnoreCase(FieldType.LISTVIEW.name())) {
			try {
				element.click();
			} catch (Exception e) {
				JavascriptExecutor js = (JavascriptExecutor) this.driverContext.getDriver();
				js.executeScript("arguments[0].scrollIntoView();", element);
				element.click();
			}

			this.webGenericMethods.locateVisibleElement(By.xpath(".//button[@title='Move selection to Selected']"), 5).click();

			return true;
		}
		else if (t.getObjectClass().equalsIgnoreCase(FieldType.CHECKBOX.name())&&element.getTagName().equalsIgnoreCase("input")) {
			try {
				element.click();
				return true;
			} catch (Exception e) {
			     List<WebElement> eles = this.driverContext.getDriver().findElements(By.xpath(".//*[@name='"+t.getUniqueId()+"']"));
				for (int i = 0; i < eles.size(); i++) {
					try {
						eles.get(i).click();
						return true;
					} catch (Exception e1) {
						try {
							element.findElement(By.xpath("ancestor::div[1]")).click();
							return true;
						} catch (Exception e2) {}
					}
				}	      
			}
		}
//		} else if (t.getObjectClass().equalsIgnoreCase(FieldType.TEXTBOX.name())&&element.getTagName().equalsIgnoreCase("lightning-input-field")) {
//			try {
//				element = element.findElement(By.xpath(".//input[1]"));
//			} catch (Exception e) {
//				element = element.findElement(By.xpath(".//textarea[1]"));
//			}
//			element.clear();
//			element.sendKeys(data);
//			return true;
//		}
        return false;
	}
	
	//This Method used to select the list on Landing page
	private void listselect(String data) {
		data=data.replace("@@SELECT,", "");
		this.driverContext.getDriver().findElement(By.xpath(".//button[@title='Select a List View']")).click();
		WebDriverWait wait = new WebDriverWait(this.driverContext.getDriver(), 5);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//div[@role='dialog' and contains(@class,'active')]//span[text()='"+data+"']/ancestor::a[1]"))).click();
		this.webGenericMethods.explicitWait(1);
		this.genericAdapterMethods.waitUntilPageLoad();
	}

	//This method is used to fetch the Aadhar
	private void fetchAadhar(String data) {
		data = data.replace("@@Fetch Aadhaar,", "");
		((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].click();", this.driverContext.getDriver().findElement(By.xpath(".//a[contains(text(),'Fetch Aadhaar Reference Number')]")));
		WebDriverWait wait = new WebDriverWait(this.driverContext.getDriver(), 5);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//section[@role='dialog']//input"))).sendKeys(data);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//section[@role='dialog']//button[@title='Fetch Reference Number']"))).click();
	}

	//this method is used to upload the file or Reports
	private void uploadFile(String data) {
		if (data.startsWith("@@UPLOAD,")) {
			data = data.replace("@@UPLOAD,", "");
			List<WebElement> fils = this.driverContext.getDriver().findElements(By.xpath(".//input[@type='file']"));
			WebElement fil = this.checkVisibleElement(fils);
			fil.sendKeys(data);
			this.webGenericMethods.explicitWait(2);
			this.driverContext.getDriver().findElement(By.xpath(".//span[text()='Done']/ancestor::button[1]")).click();
			this.genericAdapterMethods.waitUntilPageLoad();
		} else if (data.startsWith("@@UPLOADCAM,")) {
			data = data.replace("@@UPLOADCAM,", "");
			WebElement frm = this.driverContext.getDriver().findElement(By.xpath(".//iframe"));
			this.driverContext.getDriver().switchTo().frame(frm);
			WebElement fil = this.driverContext.getDriver().findElement(By.xpath(".//input[@type='file']"));
			fil.sendKeys(data);
			this.webGenericMethods.explicitWait(2);
			this.genericAdapterMethods.waitUntilPageLoad();
			this.driverContext.getDriver().switchTo().defaultContent();
		}
	}
	
	//This Method is used to hadle window once you click on Open in New Window link
	private void handleNewWindow(WebElement element) {
		try {
			WebDriverWait wait = new WebDriverWait(this.driverContext.getDriver(), 5);
			WebElement newFrame = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//iframe[@title='accessibility title']")));
			//WebElement newFrame = this.driverContext.getDriver().findElement(By.xpath(".//iframe[@title='accessibility title']"));
			this.driverContext.getDriver().switchTo().frame(newFrame);
			element = this.driverContext.getDriver().findElement(By.xpath(".//a[text()='Open In New Tab']"));
			((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].click();", element);
			wait.until(ExpectedConditions.numberOfWindowsToBe(2));
			String presentWin = this.driverContext.getDriver().getWindowHandle();
			Set<String> windows = this.driverContext.getDriver().getWindowHandles();
			for(String windowHandle  : windows)
			   {
			   if(!windowHandle.equals(presentWin))
			      {
			      this.driverContext.getDriver().switchTo().window(windowHandle);
				  wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//a[contains(text(),'Attach To Record and')]"))).click();
				  this.explicitWait(4);
			      this.driverContext.getDriver().close(); 
			      this.driverContext.getDriver().switchTo().window(presentWin); //cntrl to parent window
			      this.driverContext.getDriver().switchTo().defaultContent();
			      wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//span[text()='Cancel']/ancestor::footer[1]/button[1]"))).click();
			      }
			   }
		} catch (Exception e) {
			logger.info("Could not switch and handle multiple window");
		}		
	}
	
	//This Method is used for Generate Agreement
	public void handleAgreement(WebElement element) {
		WebDriverWait wait = new WebDriverWait(this.driverContext.getDriver(), 5);
		String presentWin = this.driverContext.getDriver().getWindowHandle();
		((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].click();", element);
		this.explicitWait(1);
		Set<String> windows = this.driverContext.getDriver().getWindowHandles();
		for (String windowHandle : windows) {
			if (!windowHandle.equals(presentWin)) {
				this.driverContext.getDriver().switchTo().window(windowHandle);
				this.explicitWait(8);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//button[text()='Save']"))).click();
				this.explicitWait(3);
				this.driverContext.getDriver().close();
				this.driverContext.getDriver().switchTo().window(presentWin);
				this.driverContext.getDriver().switchTo().defaultContent();
			}
		}
	}

	private void explicitWait(int i) {
		try {
			Thread.sleep(1000*i);
		} catch (Exception e) {
		}
	}

	@Override
	public void buttonInput(TestObject t, WebElement element) throws ExecutorException {
		try {
		if(t.getData().equalsIgnoreCase("Take Ownership"))
		{
			this.webGenericMethods.locateVisibleElement(By.xpath(".//button[@class='slds-button slds-button_icon-border-filled']"),1).click();
			 
			this.webGenericMethods.locateVisibleElement(By.xpath(".//a/span[text()='Take Ownership']"),1).click();
		}else {
				try {
					element.click();
				} catch (Exception e) {
					try {
						this.genericAdapterMethods.moveToCenterOfScreen(element);
						element.click();
					} catch (Exception e1) {
						try {
							JavascriptExecutor js = (JavascriptExecutor) this.driverContext.getDriver();
							js.executeScript("arguments[0].scrollIntoView();", element);
							element.click();
						} catch (Exception e2) {
							JavascriptExecutor js = (JavascriptExecutor) this.driverContext.getDriver();
							js.executeScript("arguments[0].click();", element);
						}
					}
					
				}
			
			Thread.sleep(500);
			
			this.genericAdapterMethods.waitUntilPageLoad();

		} 
		}catch (Exception e) {
			logger.error("An exception occurred while clicking the button", e);
			throw new ExecutorException("Could not click the "+t.getData()+" button.");
		}
	}
	
	@Override
	public void checkBoxInput(TestObject t, WebElement element, String data)
			throws ExecutorException {
		try {
			if (data.equalsIgnoreCase("yes")) {
				if (!element.isSelected()) {
					element.click();
				}
			} else if (data.equalsIgnoreCase("no")) {
				if (element.isSelected()) {
					element.click();
				}
			} else {
				throw new ExecutorException("Could not input " + data
						+ " in checkbox");
			}
		} catch (ElementClickInterceptedException e) {
			try {
				List<WebElement> eles = this.driverContext.getDriver().findElements(By.xpath(".//*[@name='"+t.getUniqueId()+"']"));
				for (WebElement eli : eles) {
					if(eli.isEnabled()) {
						eli.click();
						break;
					}
				}
				
				/*WebElement ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//*[@name='"+t.getUniqueId()+"']/parent::span//following-sibling::label/span[@class='slds-checkbox_faux']"),5);
				if(ele==null)
				{
					ele = this.webGenericMethods.locateElement(By.xpath(".//*[contains(text(),'"+t.getUniqueId()+"')]/parent::label/span[@class='slds-checkbox_faux']"),5);
				}
				this.checkBoxInput(t,ele,data);*/
			} catch (Exception e1) {}
			
		}catch(Exception e) {
			
			  logger.error(
			  "An exception occurred while setting value to checkbox/radio button", e);
			  throw new ExecutorException("Could not input " + data + " in checkbox");
			 
		}
	}
	
	@Override
	public void radioButtonInput(TestObject t, WebElement element, String data)
			throws ExecutorException {
		try {
			/*
			 * if(data.equalsIgnoreCase("yes")){ element.click(); }
			 */
			boolean targetRadioFound = false;
			List<WebElement> radios = this.driverContext.getDriver().findElements(By.xpath(".//input[@value='"+t.getUniqueId()+"']"));
			try {
				if (radios != null && radios.size() > 0) {
					for (WebElement radio : radios) {
						String rLabel = radio.getAttribute("value");
						if (rLabel != null && rLabel.equalsIgnoreCase(data)) {
							targetRadioFound = true;
							try {
								radio.click();
							} catch (Exception e) {
								radio=this.driverContext.getDriver().findElement(By.xpath(".//input[@value='"+t.getUniqueId()+"']/following::span[1]"));
								radio.click();
							}
							break;
						}
					}
				}else {
					try {
						WebElement radio=this.driverContext.getDriver().findElement(By.xpath(".//input[@name='"+t.getUniqueId()+"' and @value='"+data+"']/parent::span"));
						radio.click();
						targetRadioFound = true;
					} catch (ElementClickInterceptedException e) {}
					}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (!targetRadioFound) {
				throw new ExecutorException("Could not find Option " + data
						+ " in " + t.getLabel());
			}

		} catch (ExecutorException e) {
			throw new ExecutorException(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(
					"An exception occurred while setting value to checkbox/radio button",
					e);
			throw new ExecutorException("Could not input " + data
					+ " to field " + t.getLabel(), e);
		}
	}

	@Override																															 
	public void prePerformInput(WebElement element, TestObject t, String data) throws AutException {
		
		if(element!=null && !t.getObjectClass().equalsIgnoreCase("Checkbox")) {
			try {
				if(this.driverContext.getDriver().findElement(By.xpath(".//div[@role='dialog' and @aria-modal='true'] | .//section[@role='dialog' and @aria-modal='true']")).isDisplayed())
				((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].scrollIntoView();", element);
			} catch (Exception e) {
				this.genericAdapterMethods.moveToCenterOfScreen(element);
				logger.info("Could not scroll to that element-->"+data);
			}
		}else
			this.genericAdapterMethods.moveToCenterOfScreen(element);
	}

	@Override
	public void postPerformInput(WebElement element, TestObject t, String data) throws AutException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String performOutputExtend(WebElement element, TestObject t) throws AutException {
		
		String elemType = t.getObjectClass();
			if(element.getAttribute("type").equalsIgnoreCase("text")) {
				try {
					return element.getAttribute("value");
				} catch (Exception e) {
					logger.error("ERROR getting value from text box", e);
					throw new ExecutorException("Could not get value of text box");
				}
			}else if(elemType.equalsIgnoreCase("list")) {
				try {
					return element.getAttribute("data-value");
				} catch (Exception e) {
					logger.error("ERROR getting value from list box", e);
					throw new ExecutorException("Could not get value of list box");
				}
			}
			
		return null;
			
	}

	@Override
	public void prePerformOutput(WebElement element, TestObject t) throws AutException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postPerformOutput(WebElement element, TestObject t) throws AutException {
		// TODO Auto-generated method stub
		
	}

}
