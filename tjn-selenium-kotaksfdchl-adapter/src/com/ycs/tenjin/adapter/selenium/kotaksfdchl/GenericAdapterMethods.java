package com.ycs.tenjin.adapter.selenium.kotaksfdchl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.selenium.asv2.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv2.generic.WebMethodsImpl;

public class GenericAdapterMethods {

	protected DriverContext driverContext;
	protected WebMethodsImpl webGenericMethods;
	
	protected static final Logger logger = LoggerFactory.getLogger(GenericAdapterMethods.class);	
	
	public GenericAdapterMethods(DriverContext driverContext,WebMethodsImpl webGenericMethods) {
		this.driverContext = driverContext;
		this.webGenericMethods = webGenericMethods;
	}
	
	
	public WebElement getVisibleElement(By by) {
		WebElement webElement = null;
		
		List<WebElement> list = this.driverContext.getDriver().findElements(by);
		
		for (WebElement ele : list) {
			
			if(ele.isDisplayed()) {
				webElement = ele;
				break;
			}
			
		}
		
		
		return webElement;
	}
	
	public void waitUntilPageLoad() {
		
		 WebDriverWait wait = null;
		
		 ExpectedCondition<Boolean> expectation = new
	                ExpectedCondition<Boolean>() {
	                    public Boolean apply(WebDriver driver) {
	                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
	                    }
	                };
	        try {
	            Thread.sleep(200);
	            wait = new WebDriverWait(this.driverContext.getDriver(), 50);
	            wait.until(expectation);
	        } catch (Throwable error) {
	            logger.error("Timeout waiting for Page Load Request to complete.");
	        }
	        
	      
	        try {
		        WebElement spinner = null;
				
				spinner = this.webGenericMethods.locateElement(By.xpath(".//div[@class='slds-spinner_container slds-grid']|.//div[@class='slds-spinner slds-spinner_medium' and @role'status']|.//lightning-spinner//span[text()='Loading']"),0);
				
				int counter=0;
		        while(spinner!=null && counter<=100) {
		        	logger.info("Page loading..."+counter);
					counter++;	
					spinner = this.webGenericMethods.locateElement(By.xpath(".//div[@class='slds-spinner_container slds-grid']|.//div[@class='slds-spinner slds-spinner_medium' and @role'status']|.//lightning-spinner//span[text()='Loading']"));
		        }
	        
			} catch (Exception e) {
				e.printStackTrace();
			}
	        
	        logger.info("Page loaded...");
	}
	
	
	public String checkloginError() throws AutException {
		WebElement error=null;
		try {
			 error = this.driverContext.getDriver().findElement(By.xpath(".//div[@id='error']"));
			if(error!=null)
				return error.getText();
		} catch (Exception e) {
			
		}
		return null;
	}
	
	public String checkErrors() {
		String msgs = null;
		List<WebElement> errorList = this.driverContext.getDriver().findElements(By.xpath(".//ul[contains(@class,'errorsList')]|.//div[@class='slds-theme--error slds-notify--toast slds-notify slds-notify--toast forceToastMessage']//span[@class='toastMessage forceActionsText']|.//div[@class='slds-form-element__help']|.//div[@class='slds-form-element__help']/following::span[1]"));
		
		if(!errorList.isEmpty()) {
			StringBuffer str = new StringBuffer();
			for(WebElement ele : errorList) {
				str.append(ele.getText()).append(", ");
			}
			msgs = str.toString();
		}
		return msgs;
	}
	
	
	public void moveToCenterOfScreen(WebElement element) {
		try {
				String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
			        + "var elementTop = arguments[0].getBoundingClientRect().top;"
			        + "window.scrollBy(0, elementTop-(viewPortHeight/2));";

			((JavascriptExecutor) this.driverContext.getDriver()).executeScript(scrollElementIntoMiddle, element);
			
		} catch (Exception e) {}
	}
	
	
	public Map<String, String> getHTMLAttributesOfElement(WebElement element) {

		try {
			@SuppressWarnings("unchecked")
			ArrayList<String> parentAttributes = (ArrayList<String>) ((JavascriptExecutor) this.driverContext
					.getDriver()).executeScript(
							"var s = []; var attrs = arguments[0].attributes; for (var l = 0; l < attrs.length; ++l) { var a = attrs[l]; s.push(a.name + ':' + a.value); } ; return s;",
							element);

			Map<String, String> attributesMap = new HashMap<String, String>();
			String allAttrs = "";
			String aValue = null;
			for (String s : parentAttributes) {
				String[] split = s.split(":");
				if (split.length >= 2) {
					if (split.length == 2) {
						attributesMap.put(split[0], split[1]);
					} else {
						aValue = "";
						for (int i = 1; i < split.length; i++) {
							if (i < split.length - 1) {
								aValue = aValue + split[i] + ":";
							} else {
								aValue = aValue + split[i];
							}
						}

						attributesMap.put(split[0], aValue);
					}
				} else {
					attributesMap.put(split[0], null);
				}

				allAttrs = allAttrs + s + ",";
			}

			attributesMap.put("all", allAttrs);
			attributesMap.put("tag", element.getTagName());

			return attributesMap;
		} catch (Exception e) {
			logger.error("ERROR while getting HTML attributes of element", e);
			return null;
		}
	}


	public void verifyMobileOTP() {
		try {
			WebElement link = this.driverContext.getDriver().findElements(By.xpath(".//a[contains(text(),'Kotak_OTPVerificationPage')]")).get(0);
			
			this.moveToCenterOfScreen(link);
			
			if(link.isDisplayed()) {
				WebElement checkBox = null;
				((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].click();", link);
				
				this.webGenericMethods.switchDriverToLatestWindow();
				
				this.webGenericMethods.locateElement(By.xpath(".//input[@name='otpNumber']")).sendKeys("112233");
				
				checkBox = this.webGenericMethods.locateElement(By.xpath(".//input[@name='ndncConsent']/parent::span//span[@class='slds-checkbox_faux']"));
				
				if(checkBox!=null) {
					checkBox.click();
				}
				
				checkBox = this.webGenericMethods.locateElement(By.xpath(".//input[@name='whatsappConsent']/parent::span//span[@class='slds-checkbox_faux']"));
				if(checkBox!=null) {
					checkBox.click();
				}
				
				checkBox = this.webGenericMethods.locateElement(By.xpath(".//input[@name='indianCitizenConsent']/parent::span//span[@class='slds-checkbox_faux']"));
				if(checkBox!=null) {
					checkBox.click();
				}
				
				checkBox = this.webGenericMethods.locateElement(By.xpath(".//input[@name='karzaConsent']/parent::span//span[@class='slds-checkbox_faux']"));
				if(checkBox!=null) {
					checkBox.click();
				}
				
				checkBox = this.webGenericMethods.locateElement(By.xpath(".//input[@name='cibilConsent']/parent::span//span[@class='slds-checkbox_faux']"));
				if(checkBox!=null) {
					checkBox.click();
				}
				this.webGenericMethods.locateElement(By.xpath(".//button[@title='Verify']")).click();
				
				this.webGenericMethods.explicitWait(3);
				
				this.waitUntilPageLoad();
				
				String error = this.checkErrors();
				
				if(error!=null) {
					throw new ExecutorException(error);
				}
				
				this.webGenericMethods.switchDriverToInitialWindow();
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			throw new ExecutorException("Could not done Mobile OTP verification!");
		}
	}
	
	public void scroll() {
		try {
			WebElement tabDivScroll = this.driverContext.getDriver().findElement(By.xpath(".//div[@data-aura-class='uiScroller']"));
			int width= tabDivScroll.getSize().getWidth();
			int windowHeight = tabDivScroll.getSize().getHeight();
			int hg = (width/windowHeight)+1;
			int iteration = 0;
			int startHeight = 0;
			
			JavascriptExecutor exe=((JavascriptExecutor) this.driverContext.getDriver());
			exe.executeScript("arguments[0].scrollTop = arguments[1];",tabDivScroll,0);
			
			while(iteration < hg) {
				JavascriptExecutor exe1=((JavascriptExecutor) this.driverContext.getDriver());
				exe1.executeScript("arguments[0].scrollTo(arguments[1], arguments[2]);",tabDivScroll,startHeight , startHeight +windowHeight);
				Thread.sleep(500);
				startHeight = startHeight + windowHeight;
				iteration++;
			}
		} catch(Exception e){
			logger.info("No Scroll");
		}
	}
	
	
}
