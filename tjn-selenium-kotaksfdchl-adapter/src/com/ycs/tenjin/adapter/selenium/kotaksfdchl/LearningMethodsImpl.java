package com.ycs.tenjin.adapter.selenium.kotaksfdchl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.selenium.asv2.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv2.generic.WebMethodsImpl;
import com.ycs.tenjin.bridge.selenium.asv2.web.LearningMethodsAbstract;
import com.ycs.tenjin.util.Utilities;

public class LearningMethodsImpl extends LearningMethodsAbstract{

	protected static final Logger logger = LoggerFactory.getLogger(LearningMethodsImpl.class);

	protected GenericAdapterMethods genericAdapterMethods;
	
	
	public LearningMethodsImpl(DriverContext driverContext, WebMethodsImpl webGenericMethods) {
		super(driverContext, webGenericMethods);
		this.genericAdapterMethods = new GenericAdapterMethods(driverContext, webGenericMethods);
	}

	@Override
	public FieldType getTypeOfElementExtend(Map<String, String> attributesMap) {
		FieldType elementDesc = null;
		
		
		try {
			if (attributesMap.containsValue("listbox")||attributesMap.containsValue("search")) {
				elementDesc = FieldType.LIST;
			}else if(attributesMap.containsValue("textbox")||attributesMap.containsValue("phone")||attributesMap.containsValue("tel")||attributesMap.containsValue("textarea")) {
				elementDesc = FieldType.TEXTBOX;
			}else if(attributesMap.containsKey("placeholder")) {
				if(attributesMap.get("placeholder").toLowerCase().contains("search")||attributesMap.get("tag").equalsIgnoreCase("select")) {
					elementDesc = FieldType.LIST;
				}
			}else if(attributesMap.containsValue("select")) {
				if(attributesMap.get("class").toLowerCase().contains("select")||attributesMap.get("tag").equalsIgnoreCase("a")) {
					elementDesc = FieldType.LIST;
				}
			}else if(attributesMap.containsValue("group")) {
				elementDesc = FieldType.LISTVIEW;
			}
		} catch (Exception e) {
			logger.info("Could not get type of element in Extend");
		}
		
		return elementDesc;
	}

	@Override
	public Map<String, String> getUniqueIdOfElementExtend(Map<String, String> attributesMap, WebElement element)
			throws LearnerException {
		Map<String, String> uidMap = new HashMap<String, String>();
		String uid = "";
		String identifiedBy = "";
			try {
				if (attributesMap.containsKey("name")&&(!attributesMap.get("name").equalsIgnoreCase("Picklist")&&!attributesMap.get("name").equalsIgnoreCase("input1"))) {
					uid = attributesMap.get("name");
					identifiedBy = "name";
				} else {
					uid = getLabelOfElementExtend(null, element, attributesMap);
					identifiedBy = "label";
				}
				uidMap.put("uid", uid);
				uidMap.put("uidType", identifiedBy);
			} catch (Exception e) {
			}int n;
		return uidMap;
	}

	@Override
	public String getLabelOfElementExtend(FieldType type, WebElement element, Map<String, String> attributesMap) {
		
		String label = null; WebElement ele=null;
		String id = null;
		try {
			if(attributesMap.containsValue("radio")) {
				
				id=element.getAttribute("id");
				
				ele = this.webGenericMethods.locateElement(By.xpath(".//input[@id='"+id+"']/ancestor::lightning-radio-group//legend"),0);
				if(ele!=null) {
					label=ele.getText();
					if(label.startsWith("*")) {
						label = label.substring(1);
					}
					return label.replaceAll("\\n", " ");
				}
				
			}
			if(attributesMap.containsKey("id")) {
				
				id = attributesMap.get("id");
				
				if(attributesMap.containsValue("Search...")) {
                    id=element.getAttribute("aria-controls").replace("listbox", "combobox");
                    ele = this.webGenericMethods.locateElement(By.xpath(".//label[@for='"+id+"']"),0);
                }
				try {
					ele = element.findElement(By.xpath(".//ancestor::flowruntime-lwc-field//lightning-formatted-rich-text"));
				} catch (Exception e) {
					try {
						ele = element.findElement(By.xpath(".//ancestor::flowruntime-lwc-field//label"));
					} catch (Exception e1) {
						try {
							ele = this.driverContext.getDriver().findElement(By.xpath(".//label[@for='"+id+"']"));
						} catch (Exception e2) {
							ele = this.webGenericMethods.locateElement(By.xpath(".//*[@id='"+id+"']/parent::div/parent::div/label"),0);
						}
					}
				}
//				if(ele==null) {
//					ele = this.webGenericMethods.locateElement(By.xpath(".//*[@id='"+id+"']/parent::div/parent::div/label"),0);
//				}
					
				if(ele!=null) {
					
					label = ele.getText();
					if(label.isEmpty()) {
						ele=this.webGenericMethods.locateElement(By.xpath(".//label[@for='"+id+"']//ancestor::div[@class='slds-form-element slds-form-element_horizontal']/label"),0);
						if(ele!=null) {
						label = ele.getText();
						}
					}
					if(label!=null) {
					if(label.startsWith("*")) {
						label = label.substring(1);
					}if(label.endsWith("*")) {
						label = label.replace("*", "");
					}
					return label.replaceAll("\\n", " ");
					}
				}
			}
			
			if(attributesMap.containsKey("name")) {
			
				id = attributesMap.get("name");
				try {
					ele = element.findElement(By.xpath(".//ancestor::flowruntime-lwc-field//lightning-formatted-rich-text"));
				} catch (Exception e) {
					ele = this.webGenericMethods.locateElement(By.xpath(".//*[@name='"+id+"']/parent::div/parent::div/label"),0);
				}

				if(ele==null)
				{
					//ele = this.webGenericMethods.locateElement(By.xpath(".//*[@name='"+id+"']/parent::div/parent::div/label"),0);
					ele = this.webGenericMethods.locateElement(By.xpath(".//*[@name='"+id+"']/parent::label"),0);
				}
				
				if(ele!=null) {
					
					label = ele.getText();
					if(label.startsWith("*")) {
						label = label.substring(1);
					}if(label.endsWith("*")) {
						label = label.substring(0);
					}
					return label.replaceAll("\\n", " ");
				}
				
				
			}
			if(attributesMap.containsKey("placeholder")) {
				
				label=element.getAttribute("placeholder");
				
				return label.replaceAll("\\n", " ");
			}
			if(attributesMap.containsValue("group")) {
				
				id = attributesMap.get("aria-labelledby");
				
				ele=this.webGenericMethods.locateElement(By.xpath(".//div[@id='"+id+"']"),0);
				
				if(ele!=null) {
					
					label = ele.getText();
					
					if(label.startsWith("*")) {
						
						label = label.substring(1);
					}
					return label.replaceAll("\\n", " ");
				}
			}
			if(attributesMap.containsValue("select")) {
				try {
					ele= element.findElement(By.xpath(".//preceding::span[1]"));
				} catch (Exception e) {
				}
				if(ele!=null) {
					
					label = ele.getText();
					
					if(label.startsWith("*")) {
						
						label = label.substring(1);
					}
					return label.replaceAll("\\n", " ");
				}
			}
				
		} catch (AutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return label;
	}

	@Override
	public String getDropdownValuesOfElementExtend(FieldType type, WebElement element,
			Map<String, String> attributesMap) {
		String value = "";
		if (attributesMap.containsValue("listbox")&&!attributesMap.containsKey("disabled")) {
			//dropValues = getDropdownValuesOfList(element);
			try {
				this.genericAdapterMethods.moveToCenterOfScreen(element);
				element.click();
			} catch (Exception e1) {
				JavascriptExecutor js = (JavascriptExecutor) this.driverContext.getDriver();
				js.executeScript("arguments[0].scrollIntoView();", element);
				element.click();
			}try {
				
				Thread.sleep(200);
				
				List<WebElement> ele = this.driverContext.getDriver().findElements(By.xpath(".//div[@id='"+attributesMap.get("aria-owns")+"']//li//span[@class='slds-media__body']"));//By.xpath(".//div[@id='"+attributesMap.get("aria-owns")+"']/lightning-base-combobox-item/span[@class='slds-media__body']")
				
				if(ele.isEmpty()) {
					//element.click();
					Thread.sleep(200);
					ele = this.driverContext.getDriver().findElements(By.xpath(".//div[@id='"+attributesMap.get("aria-controls")+"']/lightning-base-combobox-item/span[@class='slds-media__body']/span"));
					
				}
				
				
				if(!ele.isEmpty()) {
					StringBuilder sb = new StringBuilder(value);
					
					for (WebElement webElement : ele) {
						sb.append(";");
						sb.append(webElement.getText());
					}
					
					value = sb.toString().substring(1);
				}
			} catch (Exception e) {e.printStackTrace();}
			
			element.sendKeys(Keys.TAB);
		}else if(attributesMap.containsValue("radio")) {
			String radioId = attributesMap.get("id");
			radioId=radioId.split("-")[2];
			List<WebElement> radios = this.driverContext.getDriver().findElements(
					By.xpath(".//label[contains(@for,'"+radioId+"')]/span"));
			if(!radios.isEmpty()) {
				StringBuilder sb = new StringBuilder(value);
				
				for (WebElement webElement : radios) {
					if(!webElement.getText().isEmpty()) {
					sb.append(";");
					sb.append(webElement.getText());
					}
				}
				
				value = sb.toString().substring(1);
			}
		}else if(attributesMap.containsValue("group")) {
			
			
			List<WebElement> ele=element.findElements(By.xpath(".//div[@id='"+attributesMap.get("aria-labelledby")+"']/following-sibling::div//div[@class='slds-dueling-list__options']/ul/li"));
				
				if(!ele.isEmpty()) {
				StringBuilder sb = new StringBuilder(value);
				
				for (WebElement webElement : ele) {
					sb.append(";");
					sb.append(webElement.getText());
				}
				
				value = sb.toString().substring(1);
		}
		} else if (attributesMap.containsValue("select")) {
			try {
				((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].click();", element);
				//element.click();
				Thread.sleep(500);

				List<WebElement> ele = this.driverContext.getDriver().findElements(By.xpath(".//div[@class='select-options']//a"));

				if (!ele.isEmpty()) {
					StringBuilder sb = new StringBuilder(value);

					for (WebElement webElement : ele) {
						if (webElement.isDisplayed()) {
							sb.append(";");
							sb.append(webElement.getText());
						}
					}
					value = sb.toString().substring(1);
					element.sendKeys(Keys.TAB);
				}
			} catch (Exception e) {
				logger.info("Could not find the values for select which is a tah list");
			}
		}
		return value;
	}

	@Override
	public boolean isMandatory(Map<String, String> attributesMap, WebElement element) throws LearnerException {
		
		 WebElement ele=null;
		String id = null;
		try {
			if(attributesMap.containsValue("radio")) {
				
				id=element.getAttribute("id");
				
				ele = this.webGenericMethods.locateElement(By.xpath(".//input[@id='"+id+"']/ancestor::lightning-radio-group//legend"),0);
				
				if(ele!=null&&ele.getText().startsWith("*")) {
					return true;
				
				}
				
			}
			if(attributesMap.containsKey("id")) {
				
				id = attributesMap.get("id");
				try {
					ele = element.findElement(By.xpath(".//ancestor::flowruntime-lwc-field//lightning-formatted-rich-text"));
				} catch (Exception e) {
					try {
						ele = element.findElement(By.xpath(".//ancestor::flowruntime-lwc-field//label"));
					} catch (Exception e1) {
						ele = this.webGenericMethods.locateElement(By.xpath(".//label[@for='"+id+"']"),0);
					}
				}
				
				
				
				if(ele==null) {
					ele = this.webGenericMethods.locateElement(By.xpath(".//*[@id='"+id+"']/parent::div/parent::div/label"),0);
				}
					
				if(ele!=null&&ele.getText().startsWith("*")) {
					return true;
				}
			}
			
			if(attributesMap.containsKey("name")) {
			
				id = attributesMap.get("name");
				try {
					ele = element.findElement(By.xpath(".//ancestor::flowruntime-lwc-field//lightning-formatted-rich-text"));
				} catch (Exception e) {
					try {
						ele = element.findElement(By.xpath(".//ancestor::flowruntime-lwc-field//label"));
					} catch (Exception e1) {
						ele = this.webGenericMethods.locateElement(By.xpath(".//*[@name='"+id+"']/parent::div/parent::div/label"),0);
					}
				}
				
				
				if(ele!=null&&ele.getText().startsWith("*")) {
					return true;
				}
				else {
					ele = this.webGenericMethods.locateElement(By.xpath(".//*[@name='"+id+"']//ancestor::div[@class='slds-form-element slds-form-element_horizontal']/label"),0);
					
					if(ele!=null&&ele.getText().endsWith("*")) {
						return true;
					}
				}
			}

			if(attributesMap.containsValue("group")) {
				
				id = attributesMap.get("aria-labelledby");
				
				ele=this.webGenericMethods.locateElement(By.xpath(".//div[@id='"+id+"']"),0);
							
				if(ele!=null&&ele.getText().startsWith("*")) {
					return true;
				}
				
			}
				
		} catch (AutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

}
