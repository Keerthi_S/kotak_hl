package com.ycs.tenjin.adapter.selenium.kotaksfdchl;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.enums.PageType;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.selenium.asv2.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv2.generic.WebMethodsImpl;
import com.ycs.tenjin.bridge.selenium.asv2.web.CommonMethodsAbstract;

public class CommonMethodsImpl extends CommonMethodsAbstract{

	protected static final Logger logger = LoggerFactory.getLogger(CommonMethodsImpl.class);
	
	protected GenericAdapterMethods genericAdapterMethods;
	
	public CommonMethodsImpl(DriverContext driverContext, WebMethodsImpl webGenericMethods) {
		super(driverContext, webGenericMethods);
		this.genericAdapterMethods = new GenericAdapterMethods(driverContext, webGenericMethods);
	}

	@Override
	public void navigateToPageExtend(Location location) throws BridgeException {
		
		if (location.getLocationType().equalsIgnoreCase(PageType.TAB.name())) {
			this.navigateToLink(location.getLabel());
		} else if (location.getLocationType().equalsIgnoreCase(PageType.ACCORDION.name())) {
			WebElement accordion = this.driverContext.getDriver().findElement(By.xpath(".//span[text()='" + location.getWayIn() + "']/ancestor::button[1]"));
			if (accordion.findElement(By.xpath(".//following::div[1]")).getAttribute("aria-hidden").equalsIgnoreCase("true")) {
				((JavascriptExecutor) this.driverContext.getDriver()).executeScript("arguments[0].click();", accordion);
				this.webGenericMethods.explicitWait(6);
				if(location.getScreenTitle().equalsIgnoreCase("Financials"))
					this.webGenericMethods.explicitWait(10);
				logger.info("Clicked the Accordian-->" + location.getScreenTitle());
				// accordion.click();
			}
		}
	}

	@Override
	public void navigateToButton(String buttonText) throws BridgeException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void navigateToLink(String buttonText) throws BridgeException {
		
		try {
			
			
			List<WebElement> eles=this.driverContext.getDriver().findElements(By.xpath(".//li[@title='"+buttonText+"']/a[@role='tab'] | .//a[@title='Details']"));
			
			WebElement ele=null;
			if(buttonText.equalsIgnoreCase("Co-applicant 1")) {
				
				List<WebElement> tablist =  this.driverContext.getDriver().findElements(By.xpath(".//ul[@role='tablist']//li[@class='slds-tabs_default__item']/a[@role='tab']"));
				if(tablist.isEmpty()) {
					tablist =  this.driverContext.getDriver().findElements(By.xpath(".//ul[@role='tablist']//li[@class='slds-tabs_default__item slds-is-active']/a[@role='tab']"));

				}
				ele = tablist.get(tablist.size()-1);
				
				if(!ele.isDisplayed()) {
					
					ele=this.webGenericMethods.locateElement(By.xpath(".//li[@title='"+buttonText+"']/a[@role='tab']"),2);			
					
				}
			
			}else if(buttonText.equalsIgnoreCase("SanctionDeviations")) {
				eles = this.driverContext.getDriver().findElements(By.xpath(".//a[@data-label='Sanction/Deviations']"));
			}
			if(eles!=null) {
			for (WebElement tabele : eles) {
				if (tabele.isDisplayed()) {
					this.genericAdapterMethods.moveToCenterOfScreen(tabele);
					tabele.click();break;
				}
			}
			}else if(ele!=null) {
				
				this.genericAdapterMethods.moveToCenterOfScreen(ele);
				
				ele.click();
				
			}
			
		} catch (AutException e) {
			logger.info(e.getMessage());
			throw new BridgeException("Couldn't click on tab "+buttonText);
		}
		
	}

}
