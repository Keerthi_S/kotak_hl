package com.ycs.tenjin.adapter.selenium.kotaksfdchl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.bridge.selenium.asv2.taskhandler.task.ExecutorTask;
import com.ycs.tenjin.bridge.selenium.asv2.taskhandler.task.ExecutorTaskAbstract;
import com.ycs.tenjin.util.Utilities;


public class ExecutorImpl extends ExecutorTaskAbstract implements ExecutorTask {

	protected static final Logger logger = LoggerFactory.getLogger(ExecutorImpl.class);

	
	protected GenericAdapterMethods genericAdapterMethods;
	
	
	public ExecutorImpl(int runId, ExecutionStep step, int screenshotOption, int fieldTimeout) {
		super(runId, step, screenshotOption, fieldTimeout);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initialize() throws ExecutorException {
		
		this.genericAdapterMethods = new GenericAdapterMethods(driverContext, webGenericMethods);
		this.executionMethods = new ExecutionMethodsImpl(driverContext, webGenericMethods);
		this.commonMethods = new CommonMethodsImpl(driverContext, webGenericMethods);
		
	}

	@Override
	public void preExecution() throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postExecution() throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preProcessAllPageAreas() throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postProcessAllPageAreas() throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prePageExecution(Location location) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postPageExecution(Location location) throws ExecutorException {
		String checkErrors = this.genericAdapterMethods.checkErrors();
		if (checkErrors != null&& !this.step.getType().equalsIgnoreCase("negative")) {
			if (!checkErrors.contains("No Obligations Found")&&!checkErrors.contains("search term., Approved Amount")) {
				if (!checkErrors.contains("Complete this field.")) {
					throw new ExecutorException(checkErrors);
				}
			}
		}
	}

	@Override
	public void preSingleRecordPageAreaExecution(Location location) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postSingleRecordPageAreaExecution(Location location) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preMultiRecordPageAreaExecution(Location location) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postMultiRecordPageAreaExecution(Location location) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public WebElement queryRowMRB(Location location, JSONArray drQueryFields) throws ExecutorException {
		 WebElement targetRow = null;
			boolean matchFound = false;
			boolean targetFound = false;
			
			WebElement table = null;
			
			try {
				table = this.webGenericMethods.locateElement(By.xpath(".//table[@class='"+location.getWayIn()+"']//tbody"),5);
				if(table==null)
					table = this.webGenericMethods.locateElement(By.xpath(".//table[@aria-label='"+location.getWayIn()+"']//tbody"),5);
			} catch (AutException e3) {
				table = this.driverContext.getDriver().findElement(By.xpath(".//table[@role='"+location.getWayIn()+"']/tbody"));
			}

			if(table==null) {
				List<WebElement> tables = this.driverContext.getDriver().findElements(By.xpath(".//table"));
				for (WebElement grid : tables) {
					if(grid.isDisplayed()) {
						table=grid;
						break;
					}
				}
			}
			this.genericAdapterMethods.moveToCenterOfScreen(table);
			//this scroll will do scroll and make it visible all the row
			this.genericAdapterMethods.scroll();
			
			List<WebElement> rowsList = table.findElements(By.tagName("tr"));
			
			if(!rowsList.isEmpty()){
				try {
					if(drQueryFields.getJSONObject(0).getString("DATA").toString().startsWith("@@,")) {
						int data = Integer.parseInt(drQueryFields.getJSONObject(0).getString("DATA").toString().replace("@@,", ""));
						return table.findElements(By.tagName("tr")).get(data-1);
					}else {
					List<WebElement> targetRows = table.findElements(By.xpath(".//*[text()='"+drQueryFields.getJSONObject(0).getString("DATA")+"']/ancestor::tr"));
					for (WebElement trow : targetRows) {
						if(trow.isDisplayed()) {
							targetRow=trow;
							targetFound=true;
							break;
						}
					}
					}
				} catch (JSONException e2) {
					logger.info("Couldn't locate the row from table");
				}
			}
			if (targetFound) {
				return targetRow;
			}else {
				try {
					List<WebElement> targetRows = this.driverContext.getDriver().findElements(By.xpath(".//*[text()='"+drQueryFields.getJSONObject(0).getString("DATA")+"']/ancestor::tr"));
					for (WebElement trow : targetRows) {
						if(trow.isDisplayed()) {
							targetRow=trow;
							targetFound=true;
							break;
						}
					}
				} catch (JSONException e2) {}
			}
			return targetRow;
			//Commented by Keerthi
			/*try {
				try {
					table = this.driverContext.getDriver().findElement(By.xpath(".//table[@role='"+location.getWayIn()+"']/tbody"));
				} catch (Exception e) {
					table = this.webGenericMethods.locateElement(By.xpath(".//table[@"+location.getWayIn()+"]//tbody"));
				}
				if(table==null) {
					table = this.webGenericMethods.locateElement(By.xpath(".//table[@class='"+location.getWayIn()+"']//tbody"));
				}
				if(table==null) {
					List<WebElement> tables = this.driverContext.getDriver().findElements(By.xpath(".//table"));
					for (WebElement grid : tables) {
						if(grid.isDisplayed()) {
							table=grid;
							break;
						}
					}
				}
				this.genericAdapterMethods.moveToCenterOfScreen(table);
				
			} catch (Exception e2) {
				logger.info(e2.getMessage());
				return targetRow;
			}
			this.scroll();
			
			List<WebElement> rowsList = table.findElements(By.tagName("tr"));
			
			if(!rowsList.isEmpty()){
				try {
					List<WebElement> targetRows = table.findElements(By.xpath(".//*[text()='"+drQueryFields.getJSONObject(0).getString("DATA")+"']/ancestor::tr"));
					for (WebElement trow : targetRows) {
						if(trow.isDisplayed()) {
							targetRow=trow;
							targetFound=true;
							break;
						}
					}
				} catch (JSONException e2) {
				}
				
				for(WebElement tRow : rowsList) {
					
					if (targetFound) {
						break;
					}
					
					this.genericAdapterMethods.moveToCenterOfScreen(tRow);
					
					for (int q = 0; q < drQueryFields.length() && !targetFound; q++) {
						
						JSONObject drQueryField;List<WebElement> rowFields=null;String data=null;
						try {
							
							drQueryField = drQueryFields.getJSONObject(q);
							
							 rowFields = tRow.findElements(By.tagName("td"));
							  data = drQueryField.getString("DATA");
							
						} catch (JSONException e1) {
							logger.info(e1.getMessage());
							break;
						}
				
						for (WebElement td : rowFields) {
							String value = null;
							WebElement inputElement=null;
							try {
								try {
									inputElement = td.findElement(By.xpath(".//input"));
								} catch (Exception e) {}
								
								if(inputElement==null) {
									inputElement = td.findElement(By.xpath(".//div|.//span"));
								}
								
								value = inputElement.getAttribute("value");
							} catch (Exception e) {}
							
							if(value==null && inputElement!=null) {
								try {
									value = inputElement.getText();
								} catch (Exception e) {}
							}if(value==null || value.trim().isEmpty()) {
								try {
									value = td.getText();
								} catch (Exception e) {}
							}
							
							
							if(value!=null && (!value.trim().isEmpty() && value.equalsIgnoreCase(data))) {
								matchFound = true;
								break;
							}else {
								matchFound = false;
							}
						}
						
					}
					
					if(matchFound) {
						targetFound = true;
						 targetRow = tRow;
					}
				}
			}
			*/
			
			
	}

	@Override
	public WebElement addRowMRB(Location location, WebElement targetRow) throws ExecutorException {
		try {
			
			this.webGenericMethods.locateElement(By.xpath(".//div[@"+location.getWayIn()+"]//button[contains(@title,'Add')]")).click();
				
				List<WebElement> rows = this.driverContext.getDriver().findElements(By.xpath(".//table[@"+location.getWayIn()+"]//tbody//tr"));
				
				targetRow = rows.get(rows.size()-1);
				
				this.genericAdapterMethods.moveToCenterOfScreen(targetRow);
				
		} catch (Exception e) {
			logger.error("Could not click Add button for table.");
			throw new ExecutorException(
					"Could not click Add button for table.");
		}
		return targetRow;
	}

	@Override
	public WebElement deleteRowMRB(Location location, WebElement targetRow) throws ExecutorException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void preFieldExecution(Location location, TestObject t) throws ExecutorException {
		if(this.driverContext.getDriver().getWindowHandles().size()>1)
		{
			this.webGenericMethods.switchDriverToLatestWindow();
		}else if(t.getObjectClass().equalsIgnoreCase("BUTTON") && t.getData().startsWith("@@SCREENSHOT,")) {
			this.caputureSnap(t.getData());
		}
	}
	private void caputureSnap(String data) {
		int n = Integer.parseInt(data.replaceFirst("@@SCREENSHOT,", ""));
		((JavascriptExecutor) this.driverContext.getDriver()).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
		this.capturePageScreenshot(true);

		while(n>=1) {
			((JavascriptExecutor) this.driverContext.getDriver()).executeScript("window.scrollBy(0,600)");
			this.capturePageScreenshot(true);
			n--;
		}
	}

	@Override
	public void postFieldExecution(Location location, TestObject t) throws ExecutorException {
		if(t.getObjectClass().equalsIgnoreCase("BUTTON") && this.driverContext.getDriver().getWindowHandles().size()>1)
		{
			this.webGenericMethods.switchDriverToLatestWindow();
		}
	}

	@Override
	public void preMRBFieldExecution(Location location, TestObject t) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postMRBFieldExecution(Location location, TestObject t) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processIterationStatus() throws ExecutorException {
		
		String referenceId=null,errorMsgs = null;
		List<WebElement> successList = null;
		
			WebElement ele=null;
			String endMsg = "";
			if(this.step.getOperation().equalsIgnoreCase("new")||this.step.getOperation().equalsIgnoreCase("edit")) {
						
					 errorMsgs = this.genericAdapterMethods.checkErrors();
					try {
						WebDriverWait wait = new WebDriverWait(this.driverContext.getDriver(), 3);
						WebElement msgItem = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//div[@role='alert' and @data-key='success']")));
						endMsg = msgItem.getText();
						if(endMsg.contains("Close")) 
						endMsg = endMsg.replace("Close", "");
								//this.driverContext.getDriver().findElement(By.xpath(".//div[@role='alert' and @data-key='success']//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
					} catch (Exception e1) {
						 successList = this.driverContext.getDriver().findElements(By.xpath(".//div[@class='slds-theme--success slds-notify--toast slds-notify slds-notify--toast forceToastMessage']//span[@class='toastMessage forceActionsText']"));
					}

					 
					 try {
						ele = this.driverContext.getDriver().findElement(By.xpath(".//button[text()='Close']"));
					} catch (Exception e1) {}
					
					 /*if(ele!=null && this.step.getAutLoginType().equalsIgnoreCase("sales")) {
						 
						 ele.click();
						 
						 ele = this.webGenericMethods.locateElement(By.xpath(".//slot[@slot='primaryField']"));
						 
						 
						 if(ele!=null) {
							 referenceId = ele.getText();
							 RuntimeFieldValue r = this.getRuntimeFieldValueObject("Reference id:", referenceId);
							 this.runtimeValues.add(r);
							 this.webGenericMethods.locateVisibleElement(By.xpath(".//button[@class='slds-button slds-button_icon-border-filled']"),1).click();
							 
							this.webGenericMethods.locateVisibleElement(By.xpath(".//a/span[text()='Pass to Maker']"),1).click();
							WebElement textBox=this.webGenericMethods.locateVisibleElement(By.xpath(".//div//textarea[@name='Remarks__c']"),1);
							if(textBox!=null) {
								textBox.sendKeys("Tenjin automation.");
								WebElement button=this.webGenericMethods.locateVisibleElement(By.xpath(".//button[text()='Submit-Pass To Maker']"),1);
								button.click();
							}
							
						 }
						
					 }*/
					 /*if(this.step.getAutLoginType().equalsIgnoreCase("maker")) {
						 
						 
						 
						 try {
							ele = this.driverContext.getDriver().findElement(By.xpath(".//slot[@slot='primaryField']"));
						} catch (Exception e) {}
						 
						 
						 if(ele!=null) {
							 referenceId = ele.getText();
							 RuntimeFieldValue r = this.getRuntimeFieldValueObject("Reference id:", referenceId);
							 this.runtimeValues.add(r);
							 this.webGenericMethods.locateVisibleElement(By.xpath(".//button[@class='slds-button slds-button_icon-border-filled']"),1).click();
							 
							this.webGenericMethods.locateVisibleElement(By.xpath(".//a/span[text()='Pass to Checker']"),1).click();
							WebElement textBox=this.webGenericMethods.locateVisibleElement(By.xpath(".//div//textarea[@name='Remarks__c']"),1);
							if(textBox!=null) {
								textBox.sendKeys("Tenjin automation.");
								WebElement button=this.webGenericMethods.locateVisibleElement(By.xpath(".//button[text()='Submit-Pass To Checker']"),1);
								button.click();
							}
							
						 }
						
					 }if(this.step.getAutLoginType().equalsIgnoreCase("checker")) {
						 
						 
						 
						 ele = this.webGenericMethods.locateElement(By.xpath(".//slot[@slot='primaryField']"));
						 
						 
						 if(ele!=null) {
							 referenceId = ele.getText();
							 RuntimeFieldValue r = this.getRuntimeFieldValueObject("Reference id:", referenceId);
							 this.runtimeValues.add(r);
							 this.webGenericMethods.locateVisibleElement(By.xpath(".//button[@class='slds-button slds-button_icon-border-filled']"),1).click();
							 
							this.webGenericMethods.locateVisibleElement(By.xpath(".//a/span[text()='Pass To Credit']"),1).click();
							try {
								WebElement textBox=this.webGenericMethods.locateVisibleElement(By.xpath(".//div//textarea[@name='Remarks__c']"),1);
								if(textBox!=null) {
									textBox.sendKeys("Tenjin automation.");
									WebElement button=this.webGenericMethods.locateVisibleElement(By.xpath(".//button[text()='Submit-Pass To Credit']"),1);
									button.click();
								}
							} catch (Exception e) {
								 errorMsgs = this.genericAdapterMethods.checkErrors();
							}
							
						 }
						
					 }*/
					 /*if(this.step.getAutLoginType().equalsIgnoreCase("preops")) {
						 
						 
						 
						 ele = this.webGenericMethods.locateElement(By.xpath(".//slot[@slot='primaryField']"));
						 
						 
						 if(ele!=null) {
							 referenceId = ele.getText();
							 RuntimeFieldValue r = this.getRuntimeFieldValueObject("Reference id:", referenceId);
							 this.runtimeValues.add(r);
							 this.webGenericMethods.locateVisibleElement(By.xpath(".//button[@class='slds-button slds-button_icon-border-filled']"),1).click();
							 
							this.webGenericMethods.locateVisibleElement(By.xpath(".//a/span[text()='Pass to Ops']"),1).click();
							WebElement textBox=this.webGenericMethods.locateVisibleElement(By.xpath(".//div//textarea[@name='Remarks__c']"),1);
							if(textBox!=null) {
								textBox.sendKeys("Tenjin automation.");
								WebElement button=this.webGenericMethods.locateVisibleElement(By.xpath(".//button[text()='Submit']"),1);
								button.click();
							}
							
						 }
						
					 }
					 
				} catch (Exception e) {
					logger.info(e.getMessage());
				}*/
			}else if(this.step.getOperation().equalsIgnoreCase("verify")) {
				
				
				if(this.step.getAutLoginType().equalsIgnoreCase("rm")) {
					try {
						
						this.driverContext.getDriver().navigate().refresh();
						this.genericAdapterMethods.waitUntilPageLoad();
						
					
						referenceId = this.webGenericMethods.locateElement(By.xpath(".//span[text()='Loan Applicant']/parent::div/following-sibling::div//a//span")).getText();
						
						
						RuntimeFieldValue r = this.getRuntimeFieldValueObject("Reference id:", referenceId);
						 this.runtimeValues.add(r);
						
					} catch (Exception e) {
						logger.info(e.getMessage());
						e.printStackTrace();
					} 
					
				}
				
				 errorMsgs = this.genericAdapterMethods.checkErrors();
				 successList = this.driverContext.getDriver().findElements(By.xpath(".//div[@class='slds-theme--success slds-notify--toast slds-notify slds-notify--toast forceToastMessage']//span[@class='toastMessage forceActionsText']"));
			}
			
			if(errorMsgs==null) {
				
				StringBuffer msgs = null;
				
				this.iStatus.setResult("S");
				if(!endMsg.isEmpty()) {
					this.iStatus.setMessage(endMsg);
				}else if(!successList.isEmpty()) {
					msgs = new StringBuffer();
					
					for(WebElement e : successList) {
						
						msgs.append(e.getText()).append(", ");
					}
					
					this.iStatus.setMessage(msgs.toString());
				}else {
					this.iStatus.setMessage("Execution completed successfully.");
				}
				
			 }else {
				 this.iStatus.setResult("F");
				 this.iStatus.setMessage(errorMsgs);
				 this.captureFailureScreenshot();
			 }
			
			if(this.step.getFunctionCode().contains("Lead")) {
				 try {
					WebElement Dele = this.webGenericMethods.locateElement(By.xpath(".//div[@class='slds-grid primaryFieldRow']"));
					 
					 if(Dele!=null) {
						 String key = Dele.findElement(By.xpath(".//div[@class='entityNameTitle slds-line-height--reset']")).getText();
						 String value = Dele.findElement(By.xpath(".//lightning-formatted-text[1]")).getText();
						 RuntimeFieldValue r = this.getRuntimeFieldValueObject(key, value);
						 this.runtimeValues.add(r);
					 }
				} catch (AutException e) {
					logger.info("Could not set the Lead number in the Runtime Values");
				}
			 }
		if(this.step.getType().equalsIgnoreCase("positive"))
			this.validateWithExpectedMessage(this.iStatus.getMessage());
		else if(errorMsgs.contains(",")&&this.step.getType().equalsIgnoreCase("negative")) {
				String actualmsg= "";
				String[] megs = errorMsgs.split(",");
				String expmsg = this.step.getExpectedResult().toString().replaceAll("%", "");
				for (int i = 0; i < megs.length; i++) {
					if(megs[i].contains(expmsg)) {
						actualmsg = megs[i];
						break;
					}
				}
			this.validateWithExpectedMessage(actualmsg);
			if(this.iStatus.getExpectedMessageFound()==true && !actualmsg.isEmpty() ) {
				RuntimeFieldValue r = this.getRuntimeFieldValueObject("Actual Validation", Utilities.trim(actualmsg));
				 this.runtimeValues.add(r);
//				ValidationResult val = this.getValidationResultObject("Message", "Negative Test Step", expmsg, actualmsg, tdUid);
//				List<ValidationResult> rs = new ArrayList<ValidationResult>();
//				rs.add(val);
//				iStatus.setValidationResults(rs);
			}
		}
	}

}
