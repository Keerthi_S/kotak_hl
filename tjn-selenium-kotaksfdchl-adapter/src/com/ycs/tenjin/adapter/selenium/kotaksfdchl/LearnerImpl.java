package com.ycs.tenjin.adapter.selenium.kotaksfdchl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.enums.PageType;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.selenium.asv2.SeleniumApplicationImpl;
import com.ycs.tenjin.bridge.selenium.asv2.taskhandler.task.LearnerTaskAbstract;
import com.ycs.tenjin.util.Utilities;

public class LearnerImpl extends LearnerTaskAbstract{

	protected static final Logger logger = LoggerFactory.getLogger(LearnerImpl.class);

	
	protected GenericAdapterMethods genericAdapterMethods;

	
	private List<String> learntPages=null;

	private ArrayList<String> learntFields;
	
	public LearnerImpl(SeleniumApplicationImpl oApplication, Module function) {
		super(oApplication, function);
		this.genericAdapterMethods = new GenericAdapterMethods(driverContext, webGenericMethods);
		commonMethods = new CommonMethodsImpl(driverContext, webGenericMethods);
		learningMethods = new LearningMethodsImpl(driverContext, webGenericMethods);
		executionMethods = new ExecutionMethodsImpl(driverContext, webGenericMethods);
		
	}

	@Override
	public WebElement getWrapper(Location objLocation) throws LearnerException {
		
		WebElement wrapper = null;
		
		if(objLocation.getLocationType().equalsIgnoreCase(PageType.MAIN.name())) {
			
			learntPages = new ArrayList<String>();
			try {
				wrapper = this.driverContext.getDriver().findElement(By.xpath(".//div[@class='content']/ancestor::article[@class='slds-card']"));
			} catch (Exception e) {
				wrapper = this.genericAdapterMethods.getVisibleElement(By.xpath(".//div[@class='windowViewMode-maximized oneContent active lafPageHost']/div/div/div"));
			}
			if(wrapper==null) 
			wrapper = this.driverContext.getDriver().findElement(By.xpath(".//div[@class='siteforceSldsTwoCol84Layout siteforceContentArea']"));
			
			/*wrapper = this.genericAdapterMethods.getVisibleElement(By.xpath(".//div[@class='slds-accordion__content']"));
			if(wrapper==null) {
				learntPages.add(wrapper.getAttribute("id"));
			}else {
				wrapper = this.genericAdapterMethods.getVisibleElement(By.xpath(".//div[@class='windowViewMode-maximized oneContent active lafPageHost']"));
			}if(wrapper==null) {
					//	WebElement wa = this.driverContext.getDriver().findElement(By.xpath(".//body//div[@class='desktop container forceStyle oneOne navexDesktopLayoutContainer lafAppLayoutHost forceAccess tablet']"));
				wrapper = this.driverContext.getDriver().findElement(By.xpath(".//div[@class='content']/ancestor::article[@class='slds-card']"));
				//wrapper = this.genericAdapterMethods.getVisibleElement(By.xpath(".//div[@class='siteforceSldsTwoCol84Layout siteforceContentArea']"));
			}*/
			
			return wrapper;
		}else if(objLocation.getLocationType().equalsIgnoreCase(PageType.ACCORDION.name())) {
			//wrapper = this.genericAdapterMethods.getVisibleElement(By.xpath(".//div[@id='"+objLocation.getWayIn()+"']"));
			//return wrapper;
			return this.getAccodianWrapper(objLocation);
			
		}else if(objLocation.getLocationType().equalsIgnoreCase(PageType.TABLE.name())) {
			return objLocation.getWrapper();
		}else if(objLocation.getLocationType().equalsIgnoreCase(PageType.TAB.name())){
			
			 try {
				 	String screentitle=objLocation.getScreenTitle();
					WebElement ele = null;
				 	List<WebElement> eles=null;
					try {
						eles = this.driverContext.getDriver().findElements(By.xpath(".//li[@title='"+screentitle+"']/a[@role='tab']"));
						for (WebElement elems : eles) {
							if(elems.isDisplayed()) {
								ele = elems;break;
							}
						}
					} catch (Exception e) {
						screentitle=screentitle.toLowerCase();
						ele = this.genericAdapterMethods.getVisibleElement(By.xpath(".//li[translate(@title,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz') = '"+screentitle+"']/a[@role='tab']"));
					}
					
					if(ele.getAttribute("aria-controls")!=null) {
					 	try {
							wrapper=this.driverContext.getDriver().findElement(By.xpath(".//lightning-tab[@id='"+ele.getAttribute("aria-controls")+"']"));
						} catch (Exception e) {
					 		wrapper=this.webGenericMethods.locateElement(By.xpath(".//*[@role='tabpanel' and @id='"+ele.getAttribute("aria-controls")+"']"));
						}
				 }
				//wrapper=this.driverContext.getDriver().findElement(By.xpath(".//*[@aria-labelledby='"+ele.getAttribute("id")+"']"));
				 	
//				 	if(ele==null) {
//				 		
//				 		screentitle=screentitle.toLowerCase();
//				 		
//				 		ele = this.genericAdapterMethods.getVisibleElement(By.xpath(".//li[translate(@title,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz') = '"+screentitle+"']/a[@role='tab']"));
//				 		
//				 	}
				 	
//				 	if(ele.getAttribute("aria-controls")!=null) {
//				 		
//				 	wrapper=this.webGenericMethods.locateElement(By.xpath(".//lightning-tab[@id='"+ele.getAttribute("aria-controls")+"']"));
//				 	
//				 	if(wrapper==null) {
//				 		
//				 		wrapper=this.webGenericMethods.locateElement(By.xpath(".//*[@role='tabpanel' and @id='"+ele.getAttribute("aria-controls")+"']"));
//				 		
//				 	}
//			 }
				 	
				 	return wrapper;
				 	
			} catch (AutException e) {
				logger.warn("Couldn't find wrapper! "+e.getMessage());
			}
			
		}else if(objLocation.getLocationType().equalsIgnoreCase(PageType.PAGE.name())){
			return objLocation.getWrapper();
		}
		
		return wrapper;
	}

	@Override
	public String getScreenTitle(Location objLocation) throws LearnerException {
		
		String title = "";
		
		if(objLocation.getLocationType().equalsIgnoreCase(PageType.MAIN.name())) {
			
			WebElement ele;
			try {
				ele = objLocation.getWrapper().findElement(By.xpath(".//span[1]"));
				if(ele.getText().isEmpty())
					throw new Exception();
			} catch (Exception e) {
				try {
					ele = this.driverContext.getDriver().findElement(By.xpath(".//*[@class='slds-rich-text-editor__output']"));
				} catch (Exception e1) {
					 ele = this.genericAdapterMethods.getVisibleElement(By.xpath(".//h3[@class='slds-accordion__summary-heading']//span"));
				}
			}
			//WebElement ele = this.genericAdapterMethods.getVisibleElement(By.xpath(".//*[@class='slds-rich-text-editor__output']"));
			
			if(ele!=null) {
				return ele.getText().replaceAll("\\n", " ");
			}
			
			/*if(title.isEmpty()) {
				 ele = this.genericAdapterMethods.getVisibleElement(By.xpath(".//h3[@class='slds-accordion__summary-heading']//span"));
				 
				 if(ele!=null) {
						return ele.getText().replaceAll("\\n", " ");
					}
			}*/
		}else if(objLocation.getLocationType().equalsIgnoreCase(PageType.ACCORDION.name())) {
			return objLocation.getScreenTitle();
			/*WebElement ele = null;
			try {
				ele = this.webGenericMethods.locateElement(By.xpath(".//div[@id='"+objLocation.getWayIn()+"']/parent::section//h3[@class='slds-accordion__summary-heading']//span"));
			} catch (AutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(ele!=null) {
				return ele.getText();
			}*/
		}else if(objLocation.getLocationType().equalsIgnoreCase(PageType.TABLE.name())) {
			
			WebElement ele = this.genericAdapterMethods.getVisibleElement(By.xpath(".//h3[@class='slds-accordion__summary-heading']//span"));
			 
			 if(ele!=null) {
					return "MRB_"+ele.getText().replaceAll("\\n", " ");
			}
			else{
				//Keerthi
				try {
					String gridtitle = this.driverContext.getDriver().findElement(By.xpath(".//table[@aria-label='"+objLocation.getWayIn()+"']")).getAttribute("aria-label");
					return "MRB_"+gridtitle;
				} catch (Exception e) {
					return "MRB_"+objLocation.getParent();
				}
				
				/*try {
					
					ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//table[contains(@class,'slds-table')]/parent::div/preceding-sibling::div//h2/span")); 
					
					if(ele!=null) {
					return "MRB_"+ele.getText();
					}else {
						ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//h1/div[@class='entityNameTitle slds-line-height_reset']")); 
						
						if(ele!=null) {
						return "MRB_"+ele.getText();
						}else {
							ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//div[@class='slds-media__figure']/following::h2/span")); 
							
							if(ele!=null) {
								
							return "MRB_"+ele.getText();
							
							}else {
								ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//li[@class='slds-tabs_default__item slds-is-active']/a")); 
								
								if(ele!=null) {
									
								return "MRB_"+ele.getText();
								
								}else {
									ele = this.webGenericMethods.locateVisibleElement(By.xpath(".//div[@class='slds-media__body slds-align-middle']/h1")); 
									
									if(ele!=null) {
										
									return "MRB_"+ele.getText();
									
									}else{
								
							ele=this.genericAdapterMethods.getVisibleElement(By.xpath(".//button[text()='Add Obligation']"));
							
							if(ele!=null) {
								
								return "MRB_"+ele.getText();
							}
							}
						}
					}
					}
				}
			}catch (AutException e) {}
				
			}
			
			return "MRB_"+objLocation.getParent();*/
			}
			
		}else if(objLocation.getLocationType().equalsIgnoreCase(PageType.TAB.name())) {
			
			return objLocation.getScreenTitle();
			
		}else if(objLocation.getLocationType().equalsIgnoreCase(PageType.PAGE.name())) {
			
			WebElement ele = null;
			
			if(objLocation.getWayIn()!=null) {
				
				ele = this.genericAdapterMethods.getVisibleElement(By.xpath(".//div[@data-aura-rendered-by='"+objLocation.getWayIn()+"']//h2/span |.//div[@data-aura-rendered-by='"+objLocation.getWayIn()+"']//h2"));
				try {
					if(ele==null)
					ele = this.driverContext.getDriver().findElement(By.xpath(".//h3/span"));
				} catch (Exception e) {
					try {
						ele = this.driverContext.getDriver().findElement(By.xpath(".//section[@role='dialog']//header//h2 | .//header/h2"));
					} catch (Exception e1) {
						logger.info("No Label->");//ele.getText()
					}
				}
				if(ele!=null) {
					return ele.getText();
				}
			}
			
			
			ele = this.genericAdapterMethods.getVisibleElement(By.xpath(".//h3[@class='slds-accordion__summary-heading']//span"));
			
			if(ele!=null) {
				
				return ele.getText();
				
			}else {
			
			ele = this.genericAdapterMethods.getVisibleElement(By.xpath(".//div[@class='slds-col slds-m-left_medium']/h3"));
			
			if(ele!=null) {
				
				return ele.getText();
				
			}else {
				
				ele=this.genericAdapterMethods.getVisibleElement(By.xpath(".//button[starts-with(text(), 'Add')]"));
				
				if(ele!=null) {
					
					return ele.getText();
					
				}else {
					
					ele=this.genericAdapterMethods.getVisibleElement(By.xpath("//h2/span"));
					
					if(ele!=null) {
						
						return ele.getText();
						
					}else {
					
					ele=this.genericAdapterMethods.getVisibleElement(By.xpath(".//label[@class='btn btn-outline-primary my-2 my-sm-0 btn-state-width btn-upload']"));
					
					if(ele!=null) {
						
						return ele.getText();
					}else {
						
						ele=this.genericAdapterMethods.getVisibleElement(By.xpath(".//div[@class='slds-card__body']//div//h1"));
						
						if(ele!=null) {
							
							return ele.getText();
						}else {
							
							ele=this.genericAdapterMethods.getVisibleElement(By.xpath(".//li[@class='slds-tabs_default__item slds-is-active']//a"));
							
							if(ele!=null) {
								
								return ele.getText();
							}
						}
					}
				}
				}	
		}
		}
		}
		return title;
		
	}
	private boolean isAssisted(Location currentLocation) {
        logger.info("Learning isAssisted---->" + currentLocation.getLocationName());
		Collection<TestObject> fields = this.learningData.get(currentLocation.getLocationName());
		if(fields.size()>0){
			return true;
		}
		return false;
	}
	
	private WebElement getAccodianWrapper(Location objectLoctaion) {
		WebElement Wraper=null;
		Wraper = this.driverContext.getDriver().findElements(By.xpath(".//span[text()='"+objectLoctaion.getScreenTitle()+"']//following::div[contains(@id,'accordion')]")).get(0);
		try {
			if (Wraper.findElement(By.xpath(".//iframe")) != null && Wraper.findElement(By.xpath(".//iframe")).isDisplayed()) {
				Wraper = Wraper.findElement(By.xpath(".//iframe"));
				this.driverContext.getDriver().switchTo().frame(Wraper);
				Wraper = this.driverContext.getDriver().findElement(By.xpath(".//body[contains(@class,'opportunityTab')]"));
			}
		} catch (Exception e) {
			logger.info("No frame in the Accordian");
		}
		return Wraper;
		
	}

	@Override
	public ArrayList<Location> getAllPageAreas(Location currentLocation) throws LearnerException {
		
		ArrayList<Location> listLocations = new ArrayList<Location>();
		boolean check = isAssisted(currentLocation);

		this.webGenericMethods.switchDriverToLatestWindow();
		
		if(currentLocation.getLocationType().equalsIgnoreCase(PageType.MAIN.name())||currentLocation.getLocationType().equalsIgnoreCase(PageType.ACCORDION.name())||currentLocation.getLocationType().equalsIgnoreCase(PageType.TAB.name())||currentLocation.getLocationType().equalsIgnoreCase(PageType.PAGE.name())) {
			//Setting New Location For Accordion
			boolean accordionsLearnt=false;
			List<WebElement> list=null;
			String id = null;
			if(!currentLocation.getLocationName().equalsIgnoreCase("Add Co-applicantGuarantor")){
			 list =  this.driverContext.getDriver().findElements(By.xpath(".//flexipage-accordion-section2[@slot='accordionSections']"));

			for (WebElement ele : list) {
				if(ele.isDisplayed()) {
					accordionsLearnt=true;
					id = ele.findElement(By.xpath(".//h3")).getText();
					if(!learntPages.contains(id)) {
						learntPages.add(id);
						Location location = new Location();
						location.setLocationType(PageType.ACCORDION.name());
						location.setWayIn(id);
						location.setScreenTitle(id);
						location.setWrapper(ele);
						location.setParent(currentLocation.getLocationName());
						listLocations.add(location);
					}
				}
			}
			
			//Setting New Location For Page Area
			if(!accordionsLearnt) {
				list =  this.driverContext.getDriver().findElements(By.xpath(".//div[@role='dialog']|.//section[@role='dialog']|.//div[@class='actionBody']| .//div[@id='wrapper-body']|.//div[@class='slds-box slds-m-top_medium']|.//div[@class='row content']|.//c-pllwcfifileupload/parent::div|.//article[@class='slds-card slds-card_narrow']/parent::div|.//div[@class='slds-card__body']"));
				for (WebElement ele : list) {
					if(ele.isDisplayed() && check) {
						Location location = new Location();
						location.setLocationType(PageType.PAGE.name());
						if(ele.getAttribute("data-aura-rendered-by")!=null) {
							location.setWayIn(ele.getAttribute("data-aura-rendered-by"));
						}else {
							location.setWayIn("NA");
						}
						location.setWrapper(ele);
						location.setParent(currentLocation.getLocationName());
						if(!learntPages.contains(this.getScreenTitle(location))) {
							learntPages.add(this.getScreenTitle(location));
							listLocations.add(location);
						}
					}
				}
			}
		}
		
			//Setting New Location For Table or MRB
			List<WebElement> mrbs =  this.driverContext.getDriver().findElements(By.xpath(".//table[contains(@class,'slds-table')]"));
			
			for (int i=0;i<mrbs.size();i++) {
				
				if(mrbs.get(i).isDisplayed()) {
					try {
						id = mrbs.get(i).getAttribute("aria-label");
						if(id==null)
							throw new Exception();
					} catch (Exception e) {
						id = (String) ((JavascriptExecutor) this.driverContext.getDriver()).executeScript("var attrs =arguments[0].attributes;var a=attrs[0].value;return a",mrbs.get(i));
						if(id.isEmpty()) {
							id = (String) ((JavascriptExecutor) this.driverContext.getDriver()).executeScript("var attrs =arguments[0].attributes;var a=attrs[0].name;return a",mrbs.get(i));
						}
					}
					
					if((!learntPages.contains(id)|| (id.equalsIgnoreCase("grid"))&& !currentLocation.getLocationName().equalsIgnoreCase("Underwriting"))) {//||id.equalsIgnoreCase("slds-table slds-table_bordered slds-table_col-bordered")||id.equalsIgnoreCase("slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable")||
						
						learntPages.add(id);
						Location location = new Location();
						location.setLocationType(PageType.TABLE.name());
						location.setWayIn(id);
						location.setWrapper(mrbs.get(i));
						location.setMultiRecordBlock(true);
						location.setParent(currentLocation.getLocationName());
						listLocations.add(location);
					}
				}
			}
			
			//Setting New Location For Tabs
			// && !currentLocation.getLocationType().equalsIgnoreCase(PageType.MAIN.name())		
			if (check) {
				List<WebElement> tablist = this.driverContext.getDriver().findElements(By.xpath(
						".//ul[@role='tablist']//li[@class='slds-tabs_default__item']|.//ul[@role='tablist']//li//a[@class='slds-tabs_default__link']"));//|.//li[@data-aura-class='uiTabItem']/a
				for (WebElement ele : tablist) {
					if (ele.isDisplayed()) {
						id = ele.getAttribute("title");
						if (id.isEmpty()) {
							id = ele.getText();
						}
						if (!learntPages.contains(id)&& !id.equalsIgnoreCase("Integration Status")|| (currentLocation.getLocationName().equalsIgnoreCase("Fetch Insurance Amount")&& id.equalsIgnoreCase("details"))
								|| (currentLocation.getLocationName().equalsIgnoreCase("New Integration Status")&& id.equalsIgnoreCase("details"))|| (currentLocation.getLocationName().equalsIgnoreCase("Details 04")&& id.equalsIgnoreCase("details"))
								|| (currentLocation.getLocationName().equalsIgnoreCase("Documents")&& id.equalsIgnoreCase("details"))) {
							learntPages.add(id);
							Location location = new Location();
							location.setLocationType(PageType.TAB.name());
							location.setWayIn("NA");
							location.setWrapper(ele);
							location.setScreenTitle(id);
							location.setLabel(id);
							location.setParent(currentLocation.getLocationName());
							listLocations.add(location);
						}
					}
				}
			}
		}
		return listLocations;
	}

	@Override
	public void validateLocation(Location location) throws LearnerException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getWayout(Location objLocation) throws LearnerException {
		// TODO Auto-generated method stub
		return "NA";
	}

	@Override
	public ArrayList<TestObject> getAllVisibleElementForMRB(Location currentLocation) throws LearnerException {
		ArrayList<TestObject> testObjects = new ArrayList<TestObject>();
		List<WebElement> mrbElements = null;
		
		JavascriptExecutor js = (JavascriptExecutor) this.driverContext.getDriver();
		js.executeScript("arguments[0].scrollIntoView(true);", currentLocation.getWrapper());
		
			mrbElements = currentLocation.getWrapper().findElements(By.xpath(".//thead/tr/th[not(contains(@style,'text-align: center;'))]"));
			
			String dropValues = "";
			int count = 0;
			for (int i = 0; i < mrbElements.size(); i++) {
				String label = null;
				TestObject t = new TestObject();
				WebElement mrbElement = null;
				try {
					mrbElement = mrbElements.get(i);
				//	mrbElement = mrbElement.findElement(By.xpath(".//span|.//label|.//input"));
				} catch (Exception e) {
					continue;
				}
				try {
					WebElement labelElement = mrbElement.findElement(By.xpath(".//div//span[@class='slds-truncate']"));
					if(labelElement!=null) {
						label=labelElement.getText().trim().replaceAll("\\n", " ");
					}
				} catch (Exception e) {
					logger.info("No Label Element in MRB");
				}
				try {
					if(label==null)
					label = mrbElement.getText().trim().replaceAll("\\n", " ");
				} catch (Exception e) {}
				
				if (i > 6 && label.equalsIgnoreCase("")) {
					((JavascriptExecutor) this.driverContext.getDriver())
					.executeScript("arguments[0].scrollIntoView();", mrbElement);
					label = mrbElement.getText().trim().replaceAll("\\n", " ");
				}
				
				if(label.equalsIgnoreCase("")) {
					try {
						WebElement labelElement = mrbElement.findElement(By.xpath(".//div//span[@class='slds-truncate']"));
						if(labelElement!=null) {
							if(labelElement.getAttribute("title")!=null) {
								label=labelElement.getAttribute("title").trim().replaceAll("\\n", " ");
							}
						}
					} catch (Exception e) {
						logger.info("No title in MRB");
					}
				}

				t = this.getTestObject(currentLocation,FieldType.TABLE_COLUMN, label, "ColumnIndex", "COL_" + count,false, dropValues, false, false, "");
				
				count++;
				
				testObjects.add(t);
			}
		
		return testObjects;
	}

	@Override
	public void preLearning() throws LearnerException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postLearning() throws LearnerException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prePageLearning(Location currentLocation) throws LearnerException {
		if(this.learntFields==null) {
			this.learntFields = new ArrayList<String>();
		}
	}

	@Override
	public void postPageLearning(Location currentLocation) throws LearnerException {
		this.learntFields.clear();
		
	}

	@Override
	public Map<String, Object> performAssistedLearningExtended(Location currentLocation, String stage,
			TestObject field) {
		
		if(field.getIdentifiedBy().equalsIgnoreCase("label")) {
		
			try {
				
				WebElement ele = this.webGenericMethods.locateElement(By.xpath(".//span[text()='"+field.getLabel()+"']//ancestor::span/input"));
				
				if(ele!=null) {
					
					if(ele.getAttribute("type").equalsIgnoreCase("checkbox")) {
						
						ele= this.webGenericMethods.locateElement(By.xpath(".//span[text()='"+field.getLabel()+"']//preceding-sibling::span"));
						
						field.setObjectClass("CHECKBOX");field.setIdentifiedBy("CHECKBOX");
						
						this.executionMethods.performInput(ele, field, field.getData());
						
						}
						
					}
				} catch (AutException e) {}
		}else if (field.getIdentifiedBy().equalsIgnoreCase("index")) {
			TestObject targetField = new TestObject();
			targetField.setObjectClass("index");
			targetField.setIdentifiedBy("index");
			targetField.setLocation(currentLocation.getLocationName());
			targetField.setUniqueId(field.getLabel());
			targetField.setLabel(field.getLabel().split("_")[0]);
			WebElement targetele;
			try {
				targetele = this.executionMethods.locateElement(targetField,
						currentLocation.getWrapper(), 10);
				if(targetele.getAttribute("type").equalsIgnoreCase("search")) {
					field.setObjectClass("LIST");
				}else if(targetele.getAttribute("placeholder").toLowerCase().contains("search")) {
					field.setObjectClass("LIST");
					}
				this.executionMethods.performInput(targetele, field, field.getData());
			} catch (AutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		if(field.getIdentifiedBy().equalsIgnoreCase("@@clear")) {
			try {
				WebElement ele=this.webGenericMethods.locateElement(By.xpath(".//label[text()='"+field.getData()+"']/following::button[@title='Remove selected option'][1]"));
				ele.click();
			} catch (AutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(field.getIdentifiedBy().equalsIgnoreCase("@@close")) {
			try {
				WebElement ele=this.webGenericMethods.locateElement(By.xpath(".//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']"));
				if(ele!=null) {
					 this.webGenericMethods.locateElement(By.xpath(".//button[@title='Close this window']")).click();;

				}
			} catch (AutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return null;
	}

	@Override
	public void preGetAllVisibleElements(Location currentLocation, List<WebElement> elements,
			ArrayList<TestObject> testObjects) throws LearnerException {
		
		
		
	}

	@Override
	public void postGetAllVisibleElements(Location currentLocation, List<WebElement> elements,
			ArrayList<TestObject> testObjects) throws LearnerException {
		 try {
			 if(currentLocation.getLocationType().equals(PageType.PAGE.toString())||currentLocation.getLocationType().equals(PageType.TAB.toString())||currentLocation.getLocationType().equals(PageType.ACCORDION.toString())) {
				if (testObjects != null || testObjects.size() == 0) {
					TestObject t = this.getTestObject(currentLocation, FieldType.BUTTON, "Button", "Button", "Button",
							false, "", false, false, "");
					testObjects.add(t);
				}
	            }
			 if(currentLocation.getLocationName().equalsIgnoreCase("Financials")&&currentLocation.getParent().equalsIgnoreCase("Underwriting"))
	            	this.driverContext.getDriver().switchTo().defaultContent();
		} catch (Exception e) {
		}
	}

	@Override
	public ArrayList<Location> getAllPageAreasMRBs(Location currentLocation) throws LearnerException {
		
		ArrayList<Location> listLocations = new ArrayList<Location>();
		
	//	this.getAllPageAreas(currentLocation);
		
		List<WebElement> list =  this.driverContext.getDriver().findElements(By.xpath(".//div[@class='slds-accordion__contents']"));
		String id = null;
		for (WebElement ele : list) {
			id = ele.getAttribute("id");
			if(!learntPages.contains(id) && ele.isDisplayed()) {
				learntPages.add(id);
				Location location = new Location();
				location.setLocationType(PageType.ACCORDION.name());
				location.setWayIn(id);
				location.setWrapper(ele);
				location.setParent(currentLocation.getLocationName());
				listLocations.add(location);
			}
		}
		

		list =  this.driverContext.getDriver().findElements(By.xpath(".//div[@role='dialog']|.//section[@role='dialog'] |.//div[@class='actionBody'] |.//div[@class='slds-box slds-m-top_medium']|.//div[@class='row content']|.//c-pllwcfifileupload/parent::div|.//article[@class='slds-card slds-card_narrow']/parent::div|.//c-pllwc-rcu-agency-update-status//div[@class='slds-form']|.//lightning-layout[@class='slds-m-top_medium slds-grid']"));
		for (WebElement ele : list) {
			if(ele.isDisplayed()) {
				Location location = new Location();
				location.setLocationType(PageType.PAGE.name());
				if(ele.getAttribute("data-aura-rendered-by")!=null) {
					location.setWayIn(ele.getAttribute("data-aura-rendered-by"));
				}else {
					location.setWayIn("NA");
				}
				location.setWrapper(ele);
				location.setParent(currentLocation.getLocationName());
				String screenTitle=this.getScreenTitle(location);
				if(!learntPages.contains(screenTitle) || screenTitle.equalsIgnoreCase("BCIF")) {
					learntPages.add(screenTitle);
					listLocations.add(location);
				}
				
			}
	}
		
		List<WebElement> mrbs = this.driverContext.getDriver().findElements(By.xpath(".//table[contains(@class,'slds-table')]"));
		
		for (int i=0;i<mrbs.size();i++) {
			
			if(mrbs.get(i).isDisplayed()) {
				
				id = (String) ((JavascriptExecutor) this.driverContext.getDriver()).executeScript("var attrs =arguments[0].attributes;var a=attrs[0].value;return a",mrbs.get(i));
				
				if(id.isEmpty()) {
					id = (String) ((JavascriptExecutor) this.driverContext.getDriver()).executeScript("var attrs =arguments[0].attributes;var a=attrs[0].name;return a",mrbs.get(i));
				}
				
				if(!learntPages.contains(id)) {
					
					learntPages.add(id);
					Location location = new Location();
					location.setLocationType(PageType.TABLE.name());
				//	location.setSequence(i);
					location.setWayIn(id);
					location.setWrapper(mrbs.get(i));
					location.setMultiRecordBlock(true);
					location.setParent(currentLocation.getLocationName());
					listLocations.add(location);
				}
			}
			
		}
int n;
		List<WebElement> tablist =  this.driverContext.getDriver().findElements(By.xpath(".//ul[@role='tablist']//li[@role='presentation']"));//.//ul[@role='tablist']//li[@class='slds-tabs_default__item']
		
		for (WebElement ele : tablist) {
			if(ele.isDisplayed()) {
				id = ele.getAttribute("title");
				if(id.isEmpty()) {
					id=ele.getText();
				}
				
				if((!learntPages.contains(id) && !id.equalsIgnoreCase("Integration Status"))|| (id.equalsIgnoreCase("details") && currentLocation.getParent().equalsIgnoreCase("Integration Status"))|| (id.equalsIgnoreCase("details")&& currentLocation.getParent().equalsIgnoreCase("Offering"))
						|| (id.equalsIgnoreCase("details")&& currentLocation.getLocationName().equalsIgnoreCase("MRB_Loan Offerings"))//|| (id.equalsIgnoreCase("details")&& currentLocation.getParent().equalsIgnoreCase("ltv"))
						|| (id.equalsIgnoreCase("details")&& currentLocation.getLocationName().equalsIgnoreCase("MRB_Disbursement Request"))|| (id.equalsIgnoreCase("details")&& currentLocation.getLocationName().equalsIgnoreCase("MRB_PDCMandates"))
						|| (id.equalsIgnoreCase("details")&& currentLocation.getLocationName().equalsIgnoreCase("MRB_MRB_Submit for Approval"))|| (id.equalsIgnoreCase("details")&& currentLocation.getLocationName().equalsIgnoreCase("MRB_Regulatory Norms"))) {
					learntPages.add(id);
					Location location = new Location();
					location.setLocationType(PageType.TAB.name());
					location.setWayIn("NA");
					location.setWrapper(ele);
					location.setScreenTitle(id);
					location.setLabel(id);
					location.setParent(currentLocation.getLocationName());
					listLocations.add(location);
				}
			}
		}
		return listLocations;
	}
	
	@Override
	public boolean validateElement(WebElement element) throws LearnerException{
		boolean validElement = true;
		
		this.genericAdapterMethods.moveToCenterOfScreen(element);
		if(element.getAttribute("type")!=null) {
		if(!element.getAttribute("type").equalsIgnoreCase("file")) {
			if (!element.isDisplayed()) { 
				validElement = false;
			}
		}if(element.getAttribute("type").equalsIgnoreCase("radio")) {
			if(element.getAttribute("id").split("-")[1].equalsIgnoreCase("1")){
				validElement = false;
			}
		}if(element.getAttribute("type").equalsIgnoreCase("checkbox")) {
				validElement = true;
		}
		}
		return validElement;
	}
	
	@Override
	protected ArrayList<TestObject> getAllVisibleElement(Location currentLocation, List<WebElement> elements)
			throws LearnerException {

		ArrayList<TestObject> testObjects = new ArrayList<TestObject>();

		this.preGetAllVisibleElements(currentLocation, elements, testObjects);

		if (elements != null) {

			int counter = 0;
			
			//To get list of test objects if already learnt.
			ArrayList<TestObject> testobjectsListOld = this.testObjectByLocation
					.get(currentLocation.getLocationName());
			HashMap<String, TestObject> mapTestObjectsOld = new HashMap<String, TestObject>();
			if (testobjectsListOld != null) {
				for (TestObject testobj1 : testobjectsListOld) {
					mapTestObjectsOld.put(testobj1.getUniqueId(), testobj1);
				}
			}
			
			for (int i = 0; i < elements.size(); i++) {
				counter++;
				logger.debug("Processing element {}", counter);
				WebElement element = elements.get(i);
				if(currentLocation.getLocationName().equalsIgnoreCase("Doc Manager")&& counter==20) {
					break;
				}
				//logger.warn("Element {} --------> " + element.getAttribute("title"), counter);
				if (!validateElement(element)) {
					logger.warn("Element {} is hidden. Will be skipped", counter);
					continue;
				}

				Map<String, String> attributesMap = this.getHTMLAttributesOfElement(element);
				logger.debug("All element attributes --> {}", attributesMap.get("all"));

				FieldType type = this.learningMethods.getTypeOfElement(attributesMap);
				logger.debug("Element Type --> {}", type);
				// addded by shivam to handle NullPointerException - starts
				if (type == null) {
					logger.warn("Could not get type element {}. This element will be skipped", counter);
					continue;
				}
				// added by shivam to handle NullPointerException - ends
				Map<String, String> uidMap = this.learningMethods.getUniqueIdOfElement(attributesMap, element);
				logger.debug("UID Map --> {}", uidMap);

				if (uidMap == null || Utilities.trim(uidMap.get("uid")).equalsIgnoreCase("")) {
					logger.warn("Could not get Unique ID for element {}. This element will be skipped", counter);
					continue;
				}

				String identifiedBy = uidMap.get("uidType");
				String uid = uidMap.get("uid");

				//Ignore elements which are already learnt
				if (mapTestObjectsOld.containsKey(uid)) {
					continue;
				}

				if(this.learntFields.contains(uid)) {
					uid+="_"+i;
					identifiedBy = "index";
					this.learntFields.add(uid);
				}else {
					this.learntFields.add(uid);
				}
				
				
				String label = this.learningMethods.getLabelOfElement(type, element,attributesMap);
				logger.debug("Label --> {}", label);
				if (Utilities.trim(label).equalsIgnoreCase("")) {
					logger.warn("Label empty. So using UID as label");
					label = uid;
					logger.debug("New Label --> {}", label);
				}
				if(label !=null && label.length()>99) {
					label = label.toString().substring(0, 98);
				}
				
				String dropValues = this.learningMethods.getDropdownValuesOfElement(type, element, attributesMap);
				logger.debug("Default Options --> {}", dropValues);

				boolean mandatory = this.learningMethods.isMandatory(attributesMap, element);
				logger.debug("Mandatory --> {}", mandatory);
				boolean hasLov = this.learningMethods.elementHasLOV(attributesMap, element);
				logger.debug("Has LOV --> {}", hasLov);
				boolean hasAutoLov = this.learningMethods.elementHasAutoLOV(attributesMap, element);
				logger.debug("Has AutoLOV --> {}", hasAutoLov);
				String fieldGroup = this.learningMethods.getFieldGroup(attributesMap, element);
				logger.debug("Field Group --> {}", fieldGroup);
				
				logger.warn("Adding Element--"+ counter +"--------> " + label);

				TestObject t = this.getTestObject(currentLocation, type, label, identifiedBy, uid, mandatory,
						dropValues, hasLov, hasAutoLov, fieldGroup);
				testObjects.add(t);
			}
		}

		this.postGetAllVisibleElements(currentLocation, elements, testObjects);

		return testObjects;
	}
	@Override
	protected void performAssistedLearning(Location currentLocation, String stage) throws LearnerException {

		logger.debug("Entering Assisted Learning Input for screen --> " + currentLocation);

		if (this.learningData != null) {

			Collection<TestObject> fields = this.learningData.get(stage + currentLocation.getLocationName());
 
			boolean firstField = true;
			ArrayList<TestObject> testObjects = null;
			
			logger.debug("Found {} fields to input on this screen", fields.size());

			for (TestObject field : fields) {

				try {

					if (field.getIdentifiedBy() == null || field.getIdentifiedBy().equals("")
							|| field.getIdentifiedBy().equalsIgnoreCase("id")|| field.getIdentifiedBy().equalsIgnoreCase("button")) {

						if (firstField && stage.equalsIgnoreCase("pre_")) {
							if(field.getIdentifiedBy() == null || field.getIdentifiedBy().trim().equals("")){
								testObjects = this.getAllTestObjectsForPage(currentLocation);
								firstField = false;
								
								this.learntFields.clear();
							}
						}
						
						TestObject targetField = this.getTargetField(
								currentLocation, field,testObjects);
						

						// Operate on the field
						if (targetField != null) {
							logger.debug("Found target field");

							WebElement targetele=null;
							if (!currentLocation.isMultiRecordBlock()) { 
								targetele = this.executionMethods.locateElement(targetField,
										currentLocation.getWrapper(), 10);
							}else{ 
								targetele = this.executionMethods.locateMRBElement(targetField,
										currentLocation.getWrapper(), 10);
							}

							try {
								this.executionMethods.performInput(targetele, targetField, field.getData());
							} catch (AutException e) {
								throw new LearnerException(e.getMessage());
							}
						} else {
							throw new LearnerException("Field for assisted learning not found.");
						}

					} else if (field.getIdentifiedBy().toLowerCase().startsWith("@@wait")) {

						logger.debug("Wait for element {}", field.getLabel());

						try {
							int seconds = 1;
							if (field.getData() != null && !field.getData().equals("")) {
								seconds = Integer.parseInt(field.getData());
							}
							this.webGenericMethods.explicitWait(seconds);
						} catch (Exception e) {
							throw new LearnerException(e.getMessage());
						}
					}else if (field.getIdentifiedBy().equalsIgnoreCase("@@LEARN")) {
                        
//                        TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();
                        ArrayList<TestObject> testobjectsListOld = this.testObjectByLocation
                                .get(currentLocation.getLocationName());
                        ArrayList<TestObject> testobjectsListNew = this
                                .getAllTestObjectsForPage(currentLocation);
 
                        HashMap<String, TestObject> mapTestObjectsOld = new HashMap<String, TestObject>();
                        if(testobjectsListOld!=null){
                            for (TestObject testobj1 : testobjectsListOld) {
                                mapTestObjectsOld.put(testobj1.getUniqueId(),
                                        testobj1);
                            }
                        }
 
                        for (int j = 0; j < testobjectsListNew.size(); j++) {
                            TestObject testobj2 = testobjectsListNew.get(j);
                            if (!mapTestObjectsOld.containsKey(testobj2
                                    .getUniqueId())) {
                                testobjectsListOld.add(testobj2);
                            }
                        } 
                        
                        this.testObjectByLocation.put(
                                currentLocation.getLocationName(), testobjectsListOld); 
                    }else{
    					this.performAssistedLearningExtended(currentLocation, stage, field);
                    }


				} catch (Exception ex) {
					logger.error("Could not perform assisted learning for "
							+ field.getLabel());
					throw new LearnerException(
							"Could not perform assisted learning for "
									+ field.getLabel() + " on page "
									+ currentLocation.getLocationName());
				}

			}

		}
	}
	private ArrayList<TestObject> getAllTestObjectsForPage(Location currentLocation) {
		// ArrayList<TestObject> testObjects = null;
		// added by shivam - starts
		ArrayList<TestObject> testObjects = new ArrayList<TestObject>();
		// added by shivam - ends
		try {
			if (!currentLocation.getMultiRecordBlock()) {
				List<WebElement> elements = currentLocation.getWrapper()
						.findElements(By.xpath(this.oApplication.getCompositeXpath()));
				testObjects = this.getAllVisibleElement(currentLocation, elements);
			} else {
				testObjects = this.getAllVisibleElementForMRB(currentLocation);
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Could not get all visible elements on {}", currentLocation);
		}
		return testObjects;
	}

}


