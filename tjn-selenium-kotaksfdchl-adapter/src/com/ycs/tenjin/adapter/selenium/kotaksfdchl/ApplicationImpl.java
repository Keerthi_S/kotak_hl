package com.ycs.tenjin.adapter.selenium.kotaksfdchl;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.ApplicationUnresponsiveException;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.DriverException;
import com.ycs.tenjin.bridge.oem.gui.GuiApplication;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.selenium.asv2.SeleniumApplicationImpl;


public class ApplicationImpl extends SeleniumApplicationImpl implements GuiApplication{

	protected static final Logger logger = LoggerFactory.getLogger(ApplicationImpl.class);
	protected GenericAdapterMethods genericAdapterMethods;
	
	public ApplicationImpl(Aut aut, String clientIp, int port, String browser) {
		super(aut, clientIp, port, browser);
	}
	
	@Override
	public  String getCompositeXpath() {
		
		return ".//a[@class='select'] |.//input[@type='text' or @type='Text' or @type='TEXT']|.//textarea|.//input[@type='password' or @type='Password' or @type='PASSWORD' or @type='passWord' or @type='PassWord']|.//button[@class!='section-control slds-button slds-button_reset slds-accordion__summary-action']|.//input[@type='button' or @type='submit' or @type='Button' or @type='Submit' or @type='BUTTON' or @type='SUBMIT']|.//select|.//input[@type='radio' or @type='Radio' or @type='RADIO']|.//input[@type='Checkbox' or @type='CHECKBOX' or @type='checkbox' or @type='checkBox']|.//input[@role='textbox']|.//input[@type='file']|.//input[@type='search']|.//input[@type='phone']|.//input[@type='tel']|.//input[@type='percent']|.//lightning-dual-listbox/div[@role='group'] | .//input[@type='checkbox']";
	}
	

	@Override
	public void login(String LoginName, String Password)
			throws AutException, ApplicationUnresponsiveException, DriverException {
		
		this.genericAdapterMethods = new GenericAdapterMethods(this.driverContext,this.webGenericMethods);
		
		WebElement userName=this.webGenericMethods.locateElement(By.xpath(".//input[@id='username']|.//input[@placeholder='Username']"),5);
		userName.clear();
		userName.sendKeys(LoginName);
		WebElement passWord=this.webGenericMethods.locateElement(By.xpath(".//input[@id='password']|.//input[@placeholder='Password']"));
		passWord.clear();
		passWord.sendKeys(Password);
		WebElement loginBtn = this.webGenericMethods.locateElement(By.xpath(".//input[@id='Login']|.//span[text()='Log in']"));
		loginBtn.click();
		
		this.genericAdapterMethods.waitUntilPageLoad();
		
		String msg = this.genericAdapterMethods.checkloginError();
		if(msg!=null)
			throw new AutException("Login Error ->"+msg);
		
	}

	@Override
	public void navigateToFunction(String code, String name, String menuContainer)
			throws AutException, BridgeException, ApplicationUnresponsiveException, DriverException {

		try {
			if(!menuContainer.contains(">")) {
				WebElement ele1 = this.webGenericMethods.locateElement(By.xpath(".//span[text()='"+menuContainer+"']/ancestor::one-app-nav-bar-item-root[1]"));
				ele1.click();
				this.genericAdapterMethods.waitUntilPageLoad();
				this.explicitWait(1);
			}else if (menuContainer.contains(">")) {
				
				String menus[] = menuContainer.split(">");
					
					for (String menu:menus) {
						
						WebElement ele = this.webGenericMethods.locateElement(By.xpath(".//span[text()='"+menu+"']/parent::a/parent::one-app-nav-bar-item-root|.//a[@title='"+menu+"']|.//button[@title='"+menu+"']|.//span[text()='"+menu+"']//parent::a/parent::li"));
						
						if(ele!=null) {
							ele.click();
						}else {
							throw new AutException("Could not navigate to the menu "+menu);
						}
						
						this.genericAdapterMethods.waitUntilPageLoad();
				}
					
			}else {
				if(menuContainer.toLowerCase().contains("otp")) {
					
					WebElement ele = this.webGenericMethods.locateElement(By.xpath(".//span[text()='"+menuContainer+"']/parent::a/parent::one-app-nav-bar-item-root"),1);
					
					if(ele!=null&&ele.isDisplayed()) {
						
						ele.click();
						
						this.genericAdapterMethods.waitUntilPageLoad();
					} else {
						  ele = this.webGenericMethods.locateElement(By.xpath(".//div[@one-appnavbar_appnavbar]//span[text()='More']//parent::a"));
						  if(ele!=null) {
							  ele.click();
							  this.genericAdapterMethods.waitUntilPageLoad();
							  this.webGenericMethods.locateElement(By.xpath(".//a[@one-appnavbarmenuitem_appnavbarmenuitem]//span[text()='Mobile OTP Details']/../../..")).click();	  
							  this.genericAdapterMethods.waitUntilPageLoad();
						  }
						}
						
					if(ele==null) {
						throw new AutException("Could not navigate to the menu Mobile OTP Details.");
					}
					
					WebElement rv = this.webGenericMethods.locateElement(By.xpath(".//span[text()='Recently Viewed']"),0);
					
					if(rv!=null&&rv.isDisplayed()) {
						this.webGenericMethods.locateElement(By.xpath(".//span[text()='Recently Viewed']//parent::h1/parent::div"),0).click();
						this.genericAdapterMethods.waitUntilPageLoad();
						this.webGenericMethods.locateElement(By.xpath(".//span[text()='All']//parent::a/parent::li"),0).click();
						this.genericAdapterMethods.waitUntilPageLoad();
						this.webGenericMethods.locateElement(By.xpath(".//button[@title='Pin this list view']"),0).click();	
					}
				  }  
				}
		} catch (Exception e) {
			logger.info(e.getMessage());
			throw new AutException("Could not navigate to the menu.");
		}
	}

	@Override
	public void logout() throws AutException, DriverException {
		try {
		WebDriverWait wait = new WebDriverWait(this.driverContext.getDriver(), 5);
		WebElement logout = this.driverContext.getDriver().findElement(By.xpath(".//button[@class='slds-button branding-userProfile-button slds-button slds-global-actions__avatar slds-global-actions__item-action forceHeaderButton']|.//a[@class='trigger-link  ']"));
		if(logout!=null) {
			logout.click();
			this.explicitWait(1);
			this.genericAdapterMethods.waitUntilPageLoad();
			wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Log Out"))).click();
			this.explicitWait(1);
			try {
				this.driverContext.getDriver().switchTo().alert().accept();
			} catch (Exception e) {
				logger.info("NO Alert while doing log out");
			}
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='username']|.//input[@placeholder='Username']")));
			
			/*WebElement dailog = this.webGenericMethods.locateElement(By.xpath(".//div[@class='userProfilePanel uiPanel--default uiPanel positioned south open active']|.//div[@class='menuList']"));
			
			if(dailog!=null) {
				logout=this.webGenericMethods.locateElement(By.linkText("Log Out"),0);
				
				if(logout!=null) {
					
					logout.click();
					
					}else{
						
						this.webGenericMethods.locateElement(By.linkText("Logout"),0).click();
						
					}
				
				
				 try {
				        WebElement screen = null;
						
				        screen = this.webGenericMethods.locateElement(By.xpath(".//input[@id='username']|.//input[@placeholder='Username']"));
						
						int counter=0;
				        while(screen==null && counter<=20) {
				        	logger.info("Waiting to logout..."+counter);
							counter++;	
							screen = this.webGenericMethods.locateElement(By.xpath(".//input[@id='username']|.//input[@placeholder='Username']"));
							
				        }
			        
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				*/
		}else {
			throw new AutException("Couldn't logout the application!");
		}}catch (Exception e) {
			logger.info("Could not log out from the application");
		}
		
	}

	@Override
	public void recover() throws AutException, DriverException {
		this.webGenericMethods.switchDriverToInitialWindow();
		try {
			List<WebElement> discardBtn = this.driverContext.getDriver().findElements(By.xpath(".//button[text()='Discard Changes']"));
			for (WebElement btn : discardBtn) {
				if(btn.isDisplayed()) {
					btn.click();
					break;
				}
			}
		} catch (Exception e) {
			logger.info("No Discard Changes Button in Recover");
		}
		
		try {
			WebElement ele = this.driverContext.getDriver().findElement(By.xpath(".//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']"));
			if(ele!=null)
				this.driverContext.getDriver().findElement(By.xpath(".//button[@title='Close this window']")).click();
			else
				this.driverContext.getDriver().findElement(By.xpath(".//button[@title='Close']")).click();
		} catch (Exception e) {
			logger.info("No close windoe in Recover");
		}
		
		/*WebElement ele=this.webGenericMethods.locateElement(By.xpath(".//div[@class='modal-body scrollable slds-modal__content slds-p-around--medium']"));
		if(ele!=null) {
			 this.webGenericMethods.locateElement(By.xpath(".//button[@title='Close this window']")).click();;

		}else {
			ele=this.genericAdapterMethods.getVisibleElement(By.xpath(".//button[@title='Close']"));
			if(ele!=null) {
				ele.click();
			}
		}*/
	}

}
