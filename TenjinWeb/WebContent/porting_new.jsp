<!--
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  porting_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 
*/

-->

<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="java.util.List"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Data Porting</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel='stylesheet' href='css/select2.min.css'/>
		<link rel="SHORTCUT ICON" HREF="images/yethi.png">
		<link rel="Stylesheet" href="css/summarypagetable.css" />
		
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/formvalidator.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/porting_new.js'></script>
		<script type='text/javascript' src='js/select2.min.js'></script>
	
		
	</head>
	<body>
	
		<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		
		<%
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		List<Aut> auts = (List<Aut>)request.getSession().getAttribute("AUTS");
		Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
		String message=(String)map.get("message");
		String status=(String)map.get("status");
		
		%>
     <%
      
         if(tjnSession!=null){
     %>

		
		<div class='title'>
			<p>Data Porting</p>
		</div>
	 
		 <form name='port-form' action='PortingServlet' method='POST' enctype='multipart/form-data' onsubmit="return validate()">
		
			
			<input type='hidden' id='txnType' name='TXNTYPE' value='init'/>
			<input type='hidden' id='appName' name='appName' />
			<input type='hidden' id='appNameT' name='appNameT' />
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			
			<div class='toolbar'>
				<input type='submit' id='btnGo' value='Go' class='imagebutton ok'/>
				<input type="button" class="imagebutton reset" id="btnReset" value="Clear"> 
			</div>
			
			<%
			 if(status.equalsIgnoreCase("Empty")){
				%>
				<div id='user-message' class='msg-err' style='display:block;'>
				<div class="msg-icon">&nbsp;</div>
					
					<div class="msg-text"><%=Utilities.escapeXml(message) %></div>
					
				</div>
				<%} else{%>
					<div id='user-message'></div>
				<%}
				%>
			
			<div class='form container_16'>
					<div class='fieldSection grid_8'>
				<fieldset>
					<legend>Source Information</legend>
					
						<div class='grid_2'>
							<label for='application'>Application</label>
						</div>
						
						<div class='grid_5'>
							<select name='application' id='application' class='stdTextBox' mandatory='yes' title='Source Application'>
								<option value='-1'>-- Select One --</option>
								<%
								if(auts != null){
									for(Aut aut:auts){
										%>
										<option value ='<%=aut.getId() %>' data-autname='<%=Utilities.escapeXml(aut.getName()) %>'><%=Utilities.escapeXml(aut.getName()) %></option>
										<%
									}
								}
								%>
							</select>
						</div>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='lstModules'>Function</label>
						</div>
						<div class='grid_5'>
							<select id='lstModules' name='lstModules' class='stdTextBox' mandatory='yes' title='Source Function'>
								<option value='-1'>-- Select One --</option>
								
							</select>
							&nbsp; &nbsp;
							<img src='images/inprogress.gif' id='funcLoader'/>
						</div>
						
						
						<div class='clear'></div>
						<div class='grid_2'>
						<label for='sFile'>Data Sheet</label>
						</div>
						<div class='grid_5'>
							<input type='file' id='sFile' name='sFile' class='stdTextBox' mandatory='yes' title='Source Data Sheet'/>
							</br><span id="srclblError" style="color: red;"></span>
						</div>
				</fieldset>
				</div>
				<div class='fieldSection grid_8'>
				<fieldset >
					<legend>Target Information</legend>
					
					
						<div class='grid_2'>
							<label for='application'>Application</label>
						</div>
						
						<div class='grid_5'>
							<select name='applicationT' id='applicationT' class='stdTextBox' mandatory='yes' title='Target Application'>
								<option value='-1'>-- Select One --</option>
								<%
								if(auts != null){
									for(Aut aut:auts){
										%>
										<option value='<%=aut.getId() %>' data-tautname='<%=Utilities.escapeXml(aut.getName()) %>'><%=Utilities.escapeXml(aut.getName()) %></option>
										<%
									}
								}
								%>
							</select>
						</div>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='lstModules'>Function</label>
						</div>
						<div class='grid_5'>
							<select id='lstModulesT' name='lstModulesT' class='stdTextBox' mandatory='yes' title='Target Function'>
								<option value='-1'>-- Select One --</option>
								
							</select>
							&nbsp; &nbsp;
							<img src='images/inprogress.gif' id='funcLoaderT'/>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'>
						<label for='tFile'>Data Sheet</label>
						</div>
						<div class='grid_5'>
							<input type='file' id='tFile' name='tFile' class='stdTextBox' mandatory='yes' title='Target Data Sheet'/>
							</br><span id="tarlblError" style="color: red;"></span>
						</div>
				</fieldset>
				</div>
			</div>
			
		</form>
	<%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>
		
		
	</body>
</html>