<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  error.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ page isErrorPage="true" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Error - Tenjin - Intelligent Enterprise Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/error.css' />
		<link rel="SHORTCUT ICON" HREF="images/yethi.png">
	</head>
	<body>
		<div class='header'>
			<img class='header-img' src='images/tenjin_logo_new.png' alt='Tenjin - Intelligent Enterprise Testing Engine'>
		</div>
		
		<div class='error-content'>
			<div id='title'>Tenjin encountered a fatal error from which it was not able to recover</div>
			<div id='error-desc'>
				<p>This may happen due to one or more of the following causes</p>
				<ul>
					<li>Your Tenjin Session may have expired</li>
					<li>Your Administrator might have cleared your Tenjin session</li>
					<li>A network problem might have caused this issue</li>
					<li>Your Tenjin License may have expired</li>
				</ul>
			</div>
			<div id='error-resol'>
				<p><a href='InitialLaunchServlet' target='_parent'>Click here</a> to go back to the login page. If you are still facing a problem, please contact your System Administrator for further assistance</p>
			</div>
		</div>
	</body>
</html>