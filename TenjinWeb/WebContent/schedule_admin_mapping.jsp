.<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_admin_mapping.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->

 <%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
 <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel="Stylesheet" href="css/bordered.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/formvalidator.js'></script>
<script type="text/javascript" src="jquery-ui-1.10.0/tests/jquery-1.9.0.js"></script>
<script src="jquery-ui-1.10.0/ui/jquery-ui.js"></script>
<script type='text/javascript' src='js/pages/schedule_admin_mapping.js'></script>
			
	</head>
	<body>
	<%
	Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken); 
    
	TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
	Project project = null;
	project = tjnSession.getProject();
	
	
	Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
	String schId1=(String)map.get("SCH_ID1");
	String task=(String)map.get("TASK1");

	String status = (String)map.get("STATUS");
	String message = (String)map.get("MESSAGE");
	if(message == null){
		message = "";
	}
	List<Aut> auts = (List<Aut>)map.get("AUTS");
	
	%>
	<div class='main_content'>
	
		<div class='title'>
		<p>Scheduled Task Details</p>
	</div>
	<form name='sch_new_form_map' action='SchedulerServlet' method='GET'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<div class='toolbar'>
			<input type='button' class='imagebutton back' value='Back'
				id='btnBack' /> 
				<input type='button' class='imagebutton save'
				value='Save' id='btnSave' />
		</div>

		<div id='user-message'></div>
			<div class='form container_16'>
				
				<fieldset>
					<legend>Basic Information</legend>
					<div class='fieldSection grid_7'>
					
					
						<div class='clear'></div>
						<div class='grid_2'><label for='txtSchId'>Schedule Id</label></div>
						<div class='grid_4'>
						
							<input type='text' class='stdTextBox' disabled='disabled' value='<%=schId1 %>' mandatory='yes' id='txtSchId' name='txtSchId' title='Sequence No'/>
							
						</div>
							
						<div class='clear'></div>
						
						<div class='grid_2'>
							<label for='lstApplication'>Application</label>
						</div>
						<div class='grid_4'>
							<select id='lstApplication' name='lstApplication' mandatory='yes' class='stdTextBox'>
								<option value='-1'>-- Select One --</option>
								<%
								for(Aut aut:auts){
									%>
									<option value='<%=aut.getId() %>'><%=aut.getName() %></option>
									<%
								}
								%>
								
							</select>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='lstModules'>Function</label>
						</div>
						<div class='grid_4'>
							<select id='lstModules' name='lstModules' mandatory='yes' class='stdTextBox'>
								<option value='-1'>-- Select One --</option>
								
							</select>
							
								<input type='hidden' class='stdTextBox'  value='<%=task %>' mandatory='yes' id='txtTask1' name='txtTask1' title='Task'/>
						</div>
						
						<%
						if(task.equalsIgnoreCase("Extract")){
						%>
						<div class='clear'></div>
						<div class='grid_2'><label for='txtExtFile'>Extract File Path</</label></div>
						<div class='grid_4'>
							<input type='text' class='stdTextBox'   mandatory='yes' id='txtExtFile' name='txtExtFile' title='Extract File'/>
						</div>
						<%
						}%>
				
			
			</div>
			</fieldset>
			</div>
			
		
	</form>
	</div>

<div class='sub_content' style='height:500px;'>
			<iframe class='ifr_Main' name='content_frame' id='content_frame'
				src=''></iframe>
		</div>
	
</body>
</html>