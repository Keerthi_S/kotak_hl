<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  apicode_learner_selection.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 18-11-2020				Ashiki					Newly added for TENJINCG-1213
  
*/

-->
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.ApiOperation"%>
<%@page import="com.ycs.tenjin.bridge.oem.api.generic.ApiLearnType"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Api"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>API Learner - Tenjin Intelligent Testing Engine</title> 
        <link rel='stylesheet' href='css/cssreset-min.css' /> 
        <link rel='stylesheet' href='css/ifr_main.css' /> 
        <link rel='stylesheet' href='css/style.css'/> 
        <link rel='stylesheet' href='css/tabs.css'/> 
        <link rel='stylesheet' href='css/buttons.css'/> 
        <link rel='stylesheet' href='css/960_16_col.css'/> 
        <link rel='stylesheet' href='css/jquery.dataTables.css'/> 
        <script type='text/javascript' src='js/jquery-3.6.0.min.js'></script> 
        <script type="text/javascript" src="js/jquery.dataTables.js"></script> 
        <script type='text/javascript' src='js/pages/tjnmaster.js'></script> 
        
        <script type='text/javascript'>
        	$(document).ready(function() {
        		$('#tblSelectedOperations').dataTable({
        			"searching":false
        		});
        		
        		var status = $('#scrStatus').val();
        		 if(status.toLowerCase() === 'success') {
        			 showMessage($('#scrMessage').val(), 'success');
        		 } else if(status.toLowerCase() === 'error') {
        			 showMessage($('#scrMessage').val(), 'error');
        		 }
        	});
        </script>
	</head>
	<body>
		 <% 
		    Cache<String, Boolean> csrfTokenCache = null;
		    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
		    session.setAttribute("csrfTokenCache", csrfTokenCache);
		    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
		    session.setAttribute ("csrftoken_session", csrftoken);
            TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION"); 
            User cUser = tjnSession.getUser(); 
             
            
			String lrnrInitStatus = (String) request.getSession().getAttribute("LRNR_INIT_STATUS");
			String lrnrInitMessage = (String) request.getSession().getAttribute("LRNR_INIT_MESSAGE");
			
			Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("API_LRNR_MAP");
			String appId = (String) map.get("LRNR_AUT_ID");
			String appName = (String) map.get("LRNR_AUT");
			List<Api> apis = (List<Api>) map.get("LRNR_API");
			String apiType = (String) map.get("LRNR_API_Type");
        %> 
        
        <div class='title'>
			<p>API Learner - Type Selection</p>
		</div>
		
		<form id='api_learn_selection_form' action='ApiLearnerServlet' method='POST'>
			<input type='hidden' name='requesttype' id='requesttype' value='apicode_learn'/>
			<div class='toolbar'>
				<a style="text-decoration: none;" href='APIServlet?param=show_api_list&appid=<%=appId%>'>
				<input type='button' id='btnBack' class='imagebutton back' value='Back'/>
				</a>
				<input type='submit' id='btnLearn' class='imagebutton learn' value='Learn'/>
			</div>
			
			<div id='user-message'></div>
			<input type='hidden' id='scrStatus' value='<%=lrnrInitStatus %>'/>
			<input type='hidden' id='scrMessage' value='<%=lrnrInitMessage %>'/>
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='form container_16'>
				<fieldset>
					<legend>API Information</legend>
					
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='appName'>Application</label></div>
						<div class='grid_4'><input type='text' id='appName' class='stdTextBox' readonly value='<%=appName %>' /></div>
					</div>
				</fieldset>
				
				<fieldset>
					<legend>Learning Configuration</legend>
					<div class='fieldSection grid_14'>
						<div class='grid_2'><label for='learnType'>Learning Type</label></div>
						<div class='grid_6'>
							<select id='learnType' name='learnType' class='stdTextBoxNew'>
							<%if(apiType.equalsIgnoreCase("rest.generic")){ %>
							<option value='<%=ApiLearnType.REQUEST_XML.toString() %>'>Learn from Request and Response</option>
							<%}else{ %>
								<option value='<%=ApiLearnType.URL.toString() %>'>Learn from URL</option>
								<option value='<%=ApiLearnType.REQUEST_XML.toString() %>'>Learn from Request and Response</option>
								<%} %>
							</select>
						</div>
					</div>
				</fieldset>
				
				
					<fieldset>
						<legend>Selected APIs (<%=apis.size() %>)</legend>
						
						<div class='fieldSection grid_7'>
							<div id='dataGrid'>
								<table class='display' id='tblSelectedOperations'>
									<thead>
										<tr>
											<th>Sl No.</th>
											<th>API Code</th>
											<th>Operation Name</th>
											
										</tr>
									</thead>
									<tbody>
										<%
										int slNo = 0;
										for(Api api:apis) {
											
											for(ApiOperation operation:api.getOperations()){
												slNo++;
											%>
											<tr>
												<td><%=slNo %></td>
												<td><%=api.getCode() %></td>
												<td><%=operation.getName() %></td>
												
											</tr>
											<%
										}
										}
										%>
									</tbody>
								</table>
							</div>
						</div>
					</fieldset>
					
			</div>
		</form>
		
	</body>
</html>