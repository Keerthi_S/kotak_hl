<!-- 

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: api_run_progress.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright &copy 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 
 */
 -->

<%@page import="com.ycs.tenjin.bridge.pojo.run.StepIterationResult"%>
<%@page import="com.ycs.tenjin.project.TestStep"%>
<%@page import="com.ycs.tenjin.util.HatsConstants"%>
<%@page import="com.ycs.tenjin.project.TestCase"%>
<%@page import="com.ycs.tenjin.project.TestSet"%>
<%@page import="com.ycs.tenjin.run.TestRun"%>
<%@page import="com.ycs.tenjin.run.ExecutionProgressView"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>API Execution Progress</title>
		
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		
		<link rel='stylesheet' href='css/rprogress.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel="Stylesheet" href="css/summarypagetable.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/list.css'/>
		
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/CircularLoader-v1.3.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		
		<style>
			.current
			{
			    -webkit-animation-name: example; 
			    -webkit-animation-duration: 1s; 
			    animation-name: example;
			    animation-duration: 1s;
			    animation-iteration-count: 1000;
			    
			}
			
			@keyframes example {
			    
			    
			    from {background-color: #f79232; }
    			to {background-color: #f6c342; }
			}
			
			@-webkit-keyframes example {
			    
			    from {background-color: red;}
    			to {background-color: yellow;}
			}
			
			
			.summtable tbody td
			{
				cursor:default;
			}
			
			.txn-validations-view.Pass {
				font-weight:bold;
				color:green;
			}
			
			.txn-validations-view.Fail {
				font-weight:bold;
				color:red;
			}
			
			.txn-validations-view.Error {
				font-weight:bold;
				color:yellow;
			}
			
			.pass-txn
			{
				background-color:#36B37E;
				cursor:pointer;
			}
			
			.fail-txn
			{
				background-color:#FF5630;
				cursor:pointer;
			}
			
			.error-txn
			{
				background-color:#FFAB00;
				cursor:pointer;
			}
			
			.all-txn
			{
				background-color:#00B8D9;
				cursor:pointer;
			}
			
			td.fs_group_heading, td.txn
			{
				padding:5px;
			}
			
			td.fs_group_heading
			{
				font-size:1em;
			}
			
			td.txn-status-icon
			{
				text-align:center;
			}
			td.txn-status-icon > img
			{
				width:14px;
			}
			.layout-value{
				text-overflow: ellipsis; 
				overflow: hidden; 
				white-space: nowrap; 
				max-width: 150px;
			}
		</style>
		
		<script>
		var loop;
		
		function getTotalTransactions() {
			alert($('.txn-row').length);
		}
		
		function repaintStatus(progress) {
			$('#elapsedTime').html(progress.elapsedTime);
			
			$('#diagram-id-2').circularloader({
				progressPercent: progress.percentage,
	            backgroundColor: "#ffffff",
	            fontColor: "#000000",
	            fontSize: "20px",
	            radius: 33,
	            progressBarBackground: "#fff",
	            progressBarColor: "#14892c",
	            progressBarWidth: 3,
	            speed: 10,
	            progressvalue: progress.percentage,
	            showText: true
			});
			
			if(progress.percentage >= 100) {
				clearInterval(loop);
				var runId = $('#runId').val();
				
				window.location.href='ExecutorServlet?param=api_run_progress&paramval=' + runId;
				
				
			}
			
			var transactions = progress.transactions;
			for(var i=0; i<progress.transactions.length; i++) {
				var transaction = progress.transactions[i];
				
				var tduid = transaction.tdUid;
				var $row = $('.summtable').find("tr[data-tduid=" + tduid + "]");
				var $statusCell = $row.find('.txn-status-icon');
				var $messageCell = $row.find('.txn-message');
				var $requestCell = $row.find('.txn-request');
				var $responseCell = $row.find('.txn-response');
				var $validationCell = $row.find('.txn-validations');
				var $runtimeValuesCell = $row.find('.txn-runtimevalues');
				
				
				
				var status = transaction.status;
				var resultValidationStatus = transaction.resultValidationStatus;
				var runtimevaluesPresent = transaction.runtimeValuesPresent;
				var rtvMarkup = '';
				if(runtimevaluesPresent === 'true' || runtimevaluesPresent === true ){
					rtvMarkup = "<a href='#' class='txn-runtimevalues-view'>View</a>";
				}else{
					rtvMarkup = "N/A";
				}
				
				
				var statusHtml = '';
				var messageHtml = '';
				var requestHtml = '';
				var responseHtml = '';
				var validationHtml = '';
				if(status.toLowerCase() === 's') {
					statusHtml = "<img src='images/success_20c20.png' alt='Passed'/>";
					requestHtml = "<a class='get-txn-request' href='#'>Download</a>";
					responseHtml = "<a class='get-txn-response' href='#'>Download</a>";
					if(resultValidationStatus=='N/A'){
						validationHtml = "N/A";
					}else{
						validationHtml = "<a class='txn-validations-view "+ resultValidationStatus +"' href='#'>View</a>";
					}
					messageHtml = transaction.message;
					$statusCell.addClass('complete');
				}else if(status.toLowerCase() == 'f'){
					statusHtml = "<img src='images/failure_20c20.png' alt='Failed'/>";
					requestHtml = "<a class='get-txn-request' href='#'>Download</a>";
					responseHtml = "<a class='get-txn-response' href='#'>Download</a>";
					if(resultValidationStatus=='N/A'){
						validationHtml = "N/A";
					}else{
						validationHtml = "<a class='txn-validations-view "+ resultValidationStatus +"' href='#'>View</a>";
					}
					messageHtml = transaction.message;
					$statusCell.addClass('complete');
				}else if(status.toLowerCase() == 'e'){
					statusHtml = "<img src='images/error_20c20.png' alt='Error'/>";
					requestHtml = "<a class='get-txn-request' href='#'>Download</a>";
					responseHtml = "<a class='get-txn-response' href='#'>Download</a>";
					validationHtml = "<a class='txn-validations-view "+ resultValidationStatus +"' href='#'>View</a>";
					messageHtml = transaction.message;
					$statusCell.addClass('complete');
				}else if(status.toLowerCase() == 'x') {
					statusHtml = "<img src='images/ajax-loader.gif' alt='Executing' />";
					$('.current-txn').html("<a href='#"+ tduid +"'>" + tduid + "</a>");
				}else {
					statusHtml = "<img src='images/queued_20c20.png' alt='Queued' />";
				}

				$statusCell.removeClass('complete');
				$statusCell.html(statusHtml);
				$statusCell.removeClass('S');
				$statusCell.removeClass('F');
				$statusCell.removeClass('E');
				$statusCell.removeClass('X');
				$statusCell.addClass(status);
				$messageCell.html(messageHtml);
				$requestCell.html(requestHtml);
				$responseCell.html(responseHtml);
				$validationCell.html(validationHtml);
				$runtimeValuesCell.html(rtvMarkup);
			}
			
			
			var totalTransactions = $('tr.txn-row').length;
			var passed = $('.S').length;
			var failed = $('.F').length;
			var error = $('.E').length;
			
			$(".all-txn").html(totalTransactions);
			$('.pass-txn').html(passed);
			$('.fail-txn').html(failed);
			$('.error-txn').html(error);
			
		}
		
		$(document).ready(function() {
			
			
			var runId = $('#runId').val();
			loop = window.setInterval(function(){
				$.ajax({
					url:'ExecutorServlet?param=check_api_run_progress&runId=' + runId,
					dataType:'json',
					success:function(data) {
						if(data.status.toLowerCase() === 'success') {
							clearMessages();
							repaintStatus(data.progress);
						}else if(data.status.toLowerCase() === 'wait') {
							clearMessages();
						}else{
							showMessage(data.message, 'error');
							clearInterval(loop);
						}
					},
					
					error:function(xhr, textStatus, errorThrown) {
						showMessage('Could not load progress. Retrying in a second...', 'error');
					}
				}); 
			}, 750); 
		});
		
		$(document).on('click','.txn-validations a', function() {
			var $parentTd = $(this).parent();
			var $parentTr = $parentTd.parent();
			
			var runId = $('#runId').val();
			var stepId = $parentTr.data('stepId');
			var ino = $parentTr.data('iteration');
			var tduid= $parentTr.data('tduid');
			alert(tduid);
			var valName ='Validations within Step';
			var caseId = $parentTr.data('testCaseId');
			$('#val-res-sframe').attr('src','ResultsServlet?param=rvd_result_api&stepid=' + stepId + '&ino=' + ino + '&tduid=' + tduid + '&caseid=' + caseId + '&runId=' + runId);
			$('.modalmask').show();
			$('.subframe').show();
		});
		
		
		$(document).on('click','.txn-runtimevalues a', function() {
			var $parentTd = $(this).parent();
			var $parentTr = $parentTd.parent();
			
			var runId = $('#runId').val();
			var stepId = $parentTr.data('stepId');
			var ino = $parentTr.data('iteration');
			var tduid= $parentTr.data('tduid');
			var valName ='Validations within Step';
			var caseId = $parentTr.data('testCaseId');
			$('#val-res-sframe').attr('src','ResultsServlet?param=run_time_values_api&stepid=' + stepId + '&ino=' + ino + '&tduid=' + tduid + '&caseid=' + caseId);
			$('.modalmask').show();
			$('.subframe').show();
		});
		
		function closeModal(){
			$('.subframe').hide();
			$('.modalmask').hide();
			$('#val-res-sframe').attr('src','');
		}
		</script>
		
	</head>
	<body>
		<%
		ExecutionProgressView view = (ExecutionProgressView) request.getSession().getAttribute("EXEC_PROGRESS_VIEW");
		TestRun run = view.getRun();
		TestSet testSet = run.getTestSet();
		%>
		
		<div class='title'>
			<p>API Execution Progress</p>
		</div>
		
		
		<div id='user-message'></div>
		
		<div class='form' style='width:1053px;'>
			<div class='twoplusone-col-field-section'>
				<div class='fs_title'>Run Information</div>
			</div>
			<div class='three-col-field-section'>
				<div class='fs_title'>Overall Progress</div>
			</div>
			<div class='three-col-field-section'>
				<input type='hidden' id='runId' value='<%=run.getId() %>'/>
				<table class='layout'>
					<tr>
						<td class='layout-label'>Run ID</td>
						<td class='layout-value'><%=run.getId() %> - <%=testSet.getName() %><input type='hidden' id='runId' value='<%=run.getId() %>' /></td>
					</tr>
					<tr>
						<td class='layout-label'>Started On</td>
						<%
							String df = TenjinConfiguration.getProperty("DATE_FORMAT");
							SimpleDateFormat sdf = new SimpleDateFormat(df);
							String cTime = sdf.format(run.getStartTimeStamp());
						%>
						<td class='layout-value'><%=cTime %></td>
					</tr>
					
					<tr>
						<td class='layout-label'>Domain</td>
						<td class='layout-value' title='<%=run.getDomainName() %>'><%=run.getDomainName() %></td>
					</tr>
					
					
					
				</table>
			</div>
			
			<div class='three-col-field-section' >
				<table class='layout'>
					<tr>
						<td class='layout-label'>Started By</td>
						<td class='layout-value'><%=run.getUser() %></td>
					</tr>
					<tr>
						<td class='layout-label'>Elapsed Time</td>
						<td class='layout-value'><span id='elapsedTime'>00:00:00</span></td>
					</tr>
					<tr>
						<td class='layout-label'>Project</td>
						<td class='layout-value' title='<%=run.getProjectName() %>'><%=run.getProjectName() %></td>
						
					</tr>
					
				</table>
			</div>
			<div class='three-col-field-section' style='height:85px;text-align:center;border:1px solid #fff'>
				<input type='hidden' id='percent-display' value=''/>
				<div id="diagram-id-2" style='margin-top:10px;'></div>
			</div>
			<div class='clear'></div>
			
			<div class='twoplusone-col-field-section'>
				<table class='layout'>
					<tbody>
						<tr>
							<td class='layout-label' style='text-align:left;width:98px;'>Currently Executing</td>
							<td class='layout-value current-txn' style='height:42px;'></td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class='three-col-field-section'>
				<table class='layout'>
					<tbody>
						<tr>
							<td class='layout-value' colspan='2'>
								<table class='layout'>
									<tbody>
									<span><small><b>Iteration count for run</b></small></span>
										<tr>
											<td class='layout-label all-txn'></td>
											<td class='layout-label pass-txn'></td>
											<td class='layout-label fail-txn'></td>
											<td class='layout-label error-txn'></td>
										</tr>
									</tbody>
								</table>	
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class='one-col-field-section' id='allTransactions' style='height:350px;max-height:350px;overflow-y:auto;'>
				<table class='summtable' style='width:100%;'>
					<thead>
						<tr>
							<th>Status</th>
							<th>Step ID</th>
							<th>Summary</th>
							<th>TDUID</th>
							<th>Message</th>
							<th>Request</th>
							<th>Response</th>
							<th>Validations</th>
							<th>Run-time Values</th>
						</tr>
					</thead>
					<tbody>
						<%
						for(TestCase testCase : testSet.getTests()) {
							%>
							<tr>
								<td colspan='9' class='fs_group_heading'><%=testCase.getTcId() %> - <%=testCase.getTcName() %></td>
							</tr>
							<%
							for(TestStep step : testCase.getTcSteps()) {
								int iterationNo = 0;
								for(StepIterationResult iteration : step.getDetailedResults()) {
									iterationNo++;
									%>
									<tr id='<%=iteration.getTduid() %>'class='txn-row' data-iteration='<%=iterationNo %>' data-step-id='<%=step.getId() %>' data-test-case-id='<%=testCase.getTcId() %>' data-tduid = '<%=iteration.getTduid() %>'>
										<td class='txn txn-status-icon'><img src='images/queued_20c20.png' alt='Queued' /></td>
										<td class='txn'><%=step.getId() %></td>
										<td class='txn'><%=step.getShortDescription() %></td>
										<td class='txn'><%=iteration.getTduid() %></td>
										<td class='txn txn-message'></td>
										<td class='txn txn-request'></td>
										<td class='txn txn-response'></td>
										<td class='txn txn-validations'></td>
										<td class='txn txn-runtimevalues'></td>
									</tr>
									<%
								}
							}
							%>
							<%
						}
						
						%>
					</tbody>
				</table>
			</div>
		</div>
		<div class='modalmask'></div>
		<div class='subframe' style='left:3%'>
			<iframe frameborder="2" scrolling="no"  marginwidth="5" marginheight="5" style='height:500px;width:650px;' seamless="seamless" id='val-res-sframe' src=''></iframe>
		</div>
	</body>
</html>