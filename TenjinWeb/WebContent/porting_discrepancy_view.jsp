<!--
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  porting_descrepancy_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY            		DESCRIPTION
  
*/

-->


<%@page import="com.ycs.tenjin.port.PortingSession"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="java.util.List"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Data Porting</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel='stylesheet' href='css/bordered.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel='stylesheet' href='css/select2.min.css' />
<link rel="SHORTCUT ICON" HREF="images/yethi.png">
<link rel="Stylesheet" href="css/summarypagetable.css" />
<link rel="stylesheet" href="jstree/themes/default/style.min.css" />

<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script src='jstree/jstree.min.js'></script>
<script type='text/javascript' src='js/formvalidator.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript'
	src='js/pages/porting_discrepancy_view.js'></script>
<script type='text/javascript' src='js/select2.min.js'></script>

</head>
<body>

	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	
	<%
		TenjinSession tjnSession = (TenjinSession) request.getSession()
				.getAttribute("TJN_SESSION");
		PortingSession pSession = (PortingSession) request.getSession()
				.getAttribute("PORTING_SESSION");
	%>

	<div class='title'>
		<p>Data Porting - Porting Map</p>
	</div>

	<form name='port-form' action='PortingServlet' method='POST'>

		<input type='hidden' id='txnType' name='TXNTYPE' value='disc' />
		<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />

		<div class='toolbar'>
			<input type='button' id='btnBack' value='Back'
				class='imagebutton back' /> <input type='button' id='btnGo'
				value='Go' class='imagebutton ok' /> <img
				src='images/inprogress.gif' id='portProgress' />
		</div>

		<div id='user-message'></div>

		
				
				<div class='form container_16'>
					<div class='fieldSection grid_8'>
				  <fieldset>
					<legend>Source Information</legend>
					<div class='grid_2'>
						<label for='sourceApplication'>Application</label>
					</div>
					
					<div class='grid_2'>
						<input readonly type='text' class='stdTextBox' value ='<%=pSession.getSourceAppName()%>'
							id='sourceApplication' name='sourceApplication' disabled="disabled" />
					</div>
					<br>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='sourceFunction'>Function Code</label>
					</div>
					<div class='grid_2'>
						<input readonly type='text' class='stdTextBox' value='<%=pSession.getSourceFunction() %>'
							id='sourceFunction' name='sourceFunction' disabled="disabled" />
					</div>


				</fieldset>
				 

				<fieldset>
				
					<legend>Source Workbook</legend>
					<div class='fieldSection grid_8'
				style='height: 450px; overflow: auto;'>

					<div id='dTree'>
						<ul>
							<li>Root node 1
								<ul>
									<li id="child_node_1"
										data-jstree='{"selected":true,"icon":"images/tree.png"}'><a
										href='#'>Child node 1</a></li>
									<li>Child node 2</li>
								</ul>
							</li>
							<li data-jstree='{"disabled":true}'>Root node 2</li>
						</ul>
					</div>

				</fieldset>
			</div>

			<div class='fieldSection grid_8'
				style='height: 450px; overflow: auto;'>
				<fieldset>
				<legend>Target Information</legend>
				<div class='grid_2'>
						<label for='tagetApplication'>Application</label>
					</div>
					
					<div class='grid_2'>
						<input readonly type='text' class='stdTextBox' value ='<%=pSession.getTargetAppName()%>'
							id='targetApplication' name='tagetApplication' disabled="disabled" />
					</div>
					<br>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='targetFunction'>Function Code</label>
					</div>
					<div class='grid_2'>
						<input readonly type='text' class='stdTextBox' value='<%=pSession.getTargetFunc() %>'
							id='targetFunction' name='targetFunction' disabled="disabled" />
					</div>


				</fieldset>
				<fieldset>
					<legend>Equivalent in Target Workbook</legend>
					<div style='margin-top: 30px;'>
						<div class='grid_1'>
							<label for='sheet'>Sheet</label>
						</div>
						<div class='grid_6'>
							<select id='sheet' name='sheet' class='stdTextBox'>
								<option value='-1'>-- Select One --</option>
								<%
									if (pSession != null && pSession.getTargetSheetMap() != null) {
										for (String key : pSession.getTargetSheetMap().keySet()) {
											if (key.equals("TTD_HELP")) {

											} else {
								%>
								<option value='<%=key%>'><%=key%></option>
								<%
									}
										}
									}
								%>
							</select>
						</div>
						<div class='clear'></div>

						<div class='grid_1'>
							<label for='field'>Field</label>
						</div>
						<div class='grid_6'>
							<select id='field' name='field' class='stdTextBox'>
								<option value='-1'>-- Select One --</option>
							</select> &nbsp; <input type='button' id='saveMap' value='Save'
								class='imagebutton save' /> <input type='hidden' id='srcSheet'
								value='' /> <input type='hidden' id='srcField' value='' /> <input
								type='hidden' id='sFieldNodeId' value='' />

						</div>
						<div class='clear'></div>
						<div class='grid_6'>
							<div class='clear'></div>
							<div class='grid_6'>UnMapped Buttons In Source</div>
							<div class='clear'></div>
							<div class='grid_6'>


								<select id='unmapped_field' name='unmapped_field'
									class='stdTextBox'>
									<option value='-1'>-- Select One --</option>
								</select> &nbsp; <input type='button' id='Addtotarget'
									value='Add to target' class='imagebutton Add' /> <input
									type='hidden' id='srcSheet' value='' /> <input type='hidden'
									id='srcField' value='' /> <input type='hidden'
									id='sFieldNodeId' value='' />

							</div>

							<div class='clear'></div>
						</div>
					</div>

				</fieldset>
			</div>

		</div>

	</form>
	<div class='modalmask'></div>
	<div class='subframe' style='left: 20%'>
		<iframe frameborder="2" scrolling="no" marginwidth="5"
			marginheight="5" style='height: 500px; width: 600px;'
			seamless="seamless" id='ttd-options-frame' src=''></iframe>
	</div>

</body>
</html>