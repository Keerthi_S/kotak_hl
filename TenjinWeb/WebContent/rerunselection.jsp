
<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  rerunselection.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


<%@page import="com.ycs.tenjin.project.TestCase"%>
<%@page import="com.ycs.tenjin.project.TestStep"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.project.TestSet"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="java.util.Collections" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--
Added by Roshni for TENJINCG-259( 3-Jul-2017 )
/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                  CHANGED BY              DESCRIPTION
 
*/

-->
<head>
<title>Run Test - Tenjin Intelligent Testing Engine</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel="Stylesheet" href="css/bordered.css" />
<link rel='stylesheet' href='css/accordion.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/formvalidator.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/pages/rerunselection.js'></script>
</head>
<body>


	<%
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			Project project = null;
			project = tjnSession.getProject();
			
			boolean canMakeChanges = false;
			
			
			if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){
				canMakeChanges = true;
			}
			
			
			
			Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
			String status = (String)map.get("STATUS");
			String message = (String)map.get("MESSAGE");
			TestSet testSet = (TestSet)map.get("TEST_SET");
			int parentRunId = (Integer) map.get("PARENT_RUN_ID");
			String criteria=(String)map.get("CRITERIA");
			String callBack=(String)map.get("callback");
		%>

	<div class='title'>
		<p>Test Selection</p>
		<div id='prj-info'>
			<span style='font-weight: bold'>Domain: </span> <span><%=project.getDomain() %></span>
			<span>&nbsp;&nbsp;</span> <span style='font-weight: bold'>Project:
			</span> <span><%=project.getName() %></span>
		</div>
	</div>

	<form name='main_form'>
		<div class='toolbar'>
			<input type='button' class='imagebutton back' value='Back' id='btnBack' />
			<input type='button' class='imagebutton refresh' value='Refresh' id='btnRefresh'/>
			 <input type='button' value='Run Test' class='imagebutton runtestset' id='btnRun' /> 
			 <input type='hidden' id='parentRunId' name='parentRun' value='<%=parentRunId %>' />
			 <input type='hidden' id='txtTsRecId' value='<%=testSet.getId() %>' name='txtTsRecId' />
			 <input type="hidden" id='callback' value='<%=callBack%>'/>
			 <input type="hidden" id='callbackparam' value='rerun' name='callbackparam'/>
			
		</div>
		<%
			if(status != null && status.equalsIgnoreCase("SUCCESS")){
				%>
		<div id='user-message' class='msg-success' style='display: block;'>
			<div class="msg-icon">&nbsp;</div>
			<div class="msg-text"><%=message %></div>
		</div>
		<%
			}else if(status != null && !status.equalsIgnoreCase("") && !status.equalsIgnoreCase("SUCCESS")){
				%>
		<div id='user-message' class='msg-err' style='display: block;'>
			<div class="msg-icon">&nbsp;</div>
			<div class="msg-text"><%=message %></div>
		</div>

		<%
			}else{
				%>
		<div id='user-message'></div>

		<%
			}
			%>

		<div class='form container_16'>
			<input type='hidden' id='executionMode' name='executionMode' value='<%=testSet.getMode() %>' />
			<div class='fieldSection grid_8'>
				<input type='hidden' id='txtTsRecId' value='<%=testSet.getId() %>' name='txtTsRecId' />
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='txtTestSetId'>Test Set ID</label>
				</div>
				<div class='grid_4'>
					<%
						if(testSet.getId() > 0){
							%>
					<input type='text' id='txtTestSetId' name='txtTestSetId'
						disabled='disabled' class='stdTextBox info-holder'
						value='<%=testSet.getId()%>' /> <input type='hidden'
						id='txtCreateTestSet' name='createTestSet' value='yes' />
					<%
						}else{
							%>
					<input type='text' id='txtTestSetId' name='txtTestSetId'
						disabled='disabled' class='stdTextBox info-holder' value='TBD' />
					<input type='hidden' id='txtCreateTestSet' name='createTestSet'
						value='yes' />
					<%
						}
						%>
				</div>
				</div>
				<div class='fieldSection grid_7'>
				<div class='grid_2'>
					<label for='runId'>Run ID</label>
				</div>
				<div class='grid_4'>
				<input type='text' disabled='disabled' id='runId' class='stdTextBox info-holder' value='<%=parentRunId%>'/>
				</div>
				</div>
				<div class='fieldSection grid_16'>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='txtTestSetName'>Name</label>
				</div>
				<div class='grid_13'>
					<input type='text' id='txtTestSetName' name='txtTestSetName'
						value='<%=testSet.getName() %>' disabled='disabled'
						class='stdTextBox info-holder long' mandatory='yes' title='Name' />
				</div>
				</div>
				<div class='fieldSection grid_16'>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='selcriteria'>Criteria</label>
				</div>
				<div class='grid_4'>
				<input type='hidden' id='selcr' value='<%=criteria%>'/>
					<select id='selcriteria' name='selcriteria' class='stdTextBox'>
						<option value=''>All</option>
						<option value='Fail'>Failed</option>
						<option value='Error'>Error</option>
					</select>
				</div>
				<div class='grid_2'>
					<input type='button' value='Go' class='imagebutton ok' id='btnGo'>
				</div>
			</div><div class='clear'></div>
               <fieldset>
					<legend>Tests</legend>
					<div>
						<ul id='tabs'>
							<li><a href='#test-steps-tab' class='teststeps maptcs' id='test-steps-tab-anchor'>Tests selection for this run</a></li>
						</ul>
						<div id='test-steps-tab' class='tab-section'>
							<div id='user-message-steps'></div>
							<div id='dataGrid'>
							<div id='accordion'>
							<%
							boolean flag=false;
							   if(testSet!=null){
								ArrayList<TestCase> testCases = testSet.getTests();
								if(testCases != null && testCases.size() > 0){
									for(TestCase testCase:testCases){
											%>
											 
											<h3><input type='checkbox' class='chk_one' id='<%=testCase.getTcRecId()%>' name='testcase' checked="checked"/>
											<%=testCase.getTcId() %>
											<%=testCase.getTcName() %>
											 <div style="float:right; padding-right:10px ">
												<span><%= testCase.getTcStatus()%></span>							
											</div>
											</h3>
											
											<div>
													<table class='bordered'  id='tblTestSteps' >
														<thead>
															<tr>
																<th></th>
																<th class='text-center'>Step No.</th>
																<th>ID</th>
																<th>Description</th>
																<th>Type</th>
																<th>Application</th>
																<th>Status</th>
															</tr>
														</thead>
														<tbody>
															<%
														 
															if(testCase.getTcSteps() != null && testCase.getTcSteps().size() > 0){
																int counter=0;
																boolean msgFlag=false;
																 Collections.sort(testCase.getTcSteps(), TestStep.sequenceNo);
																for(TestStep step:testCase.getTcSteps()){
															 
																			counter++;
																			flag=true;
																			%>
																			<tr>
																			 
																			<td><input type='checkbox'  checked="checked" disabled="disabled"  class='teststep <%=testCase.getTcRecId()%>' id='<%=step.getRecordId()%>' name='<%=testCase.getTcRecId()%>'  
																		 '> </td>
																			<td class='text-center'><%=counter %></td>
																			<td><%=step.getId() %></td>
																			<td><%=step.getShortDescription() %></td>
																			<td><%=step.getType() %></td>
																			<td><%=step.getAppName() %></td>
																			<td ><%=step.getStatus()%></td>
																			</tr>
																			<%
																		 
																}
																 
															}else{
																%>
																<tr>
																	<td colspan='5'>This test case has no steps</td>
																</tr>
																<%
															}
															%>
														</tbody>
													</table>
												</div>
												
											<%
										}
									}
								 else
								   {
									   %>
									   
											<h3>No Records Found</h3>
										
									   <%
								   }
									}
							  
									%>
						</div>
												<input type="hidden" id='reRunFlag' value='<%=flag%>'/>
			</div>
	</div>
</div>
</fieldset>

</div>
<input type='hidden' id='moduleList' name='moduleList' value='' /> <input
			type='hidden' id='learnType' name='learnType' value='NORMAL' />
</form>
</body>
</html>