<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  faq.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%--New faq.jsp file Created By Leelaprasad For The requirment Defect #823 on 10-11-2016 --%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--
/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 **/
 -->
<head>
<title>FAQ - Tenjin Intelligent Enterprise Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel='stylesheet' href='css/style.css'/>
		
		<link rel="SHORTCUT ICON" HREF="images/yethi.png">
		<style type="text/css">
		.ptext:before{
		content: "\261B";
		font-size:20px;
		
		}
		.ptext{
		font-family:'Verdana',sans-serif;
		}
		.font{
		font-family:Italic;
		font-size:15px;
		color: blue;
		}
		.font1{
		font-family:Italic;
		/* color: #6A5ACD; */
		color: #0000A0;
		font-size:15px;
		font-weight: bold;
		
		
		
		}
		</style>
</head>
<body>
     <%
        TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
         if(tjnSession!=null){
      %>
         
<div class='title'>
		<p>Frequently Asked Questions</p>	
		</div>
		<div class='form container_16'>
				<fieldset style='margin-top:10px'>
					<legend>FAQ's</legend>
			<table class='border' width='70%' style='margin-left: 25px;'>
			<thead>
								<tr>
								
									
								</tr>
							</thead>
			
			<tbody>
			
						<tr><td><p class='font1'><b>I. Tenjin shows "Selenium driver launching failed".</b></p>
						 <p class='ptext'> Launch the driver client through command prompt.</p></td></tr>
						<tr><td><p class='font1'><b>II. Tenjin is not launching the browser although the drivier is started.</b></p>
						<p class='ptext'> Ensure whether Port is specific to machine</p>
						 <p class='ptext'>  Ensure whether selected browser as IE from the default google chrome</p></td></tr>
						<tr><td><p class='font1'><b>III. Tenjin is not identifying the Test data path.</b></p>
						<p class='ptext'>  Verify the Test Data Path is defined correctly for the function.</p>
						 <p class='ptext'>   Ensure Test Data File name is as per Tenjin standards. E.g HTM_admin_TTD.xls.</p>
						 <p class='ptext'>   The Test Data File to be placed in the machine server where Tenjin is deployed.</p></td></tr>
						<tr><td><p class='font1'><b>IV. Test Data Path is defined and File Name is appropriate. But Tenjin is still not able to read data.</b></p>
						<p class='ptext'>  Ensure data sheet has all numeric values prefixed with a -'.</p></td></tr>
						<tr><td><p class='font1'><b>V. Tenjin launching AUT, but unable to Login.</b></p>
						<p class='ptext'>  Ensure user Id's are defined appropriately. One maker and One checker is only allowed per execution.</p>
						</td></tr>
						<tr><td><p class='font1'><b>VI. Tenjin launching and logging into AUT, but not inputting Function ID.</b></p>
						
						<p class='ptext'>  Ensure Function ID and User Id's are defined correctly.</p>
						 <p class='ptext'>   Ensure Maker and Checker Id's and passwords are correct.</p>
						   </td></tr>
						<tr><td><p class='font1'><b>VII. Tenjin showing "user Id already exists" while adding Maker/checker.</b></p>
						
						<p class='ptext'>  Ensure user Id's are defined appropriately. One Maker and One checker is only allowed per execution.</p>
						</td></tr>
						<tr><td><p class='font1'><b>VIII. How is result verifications done through Tenjn templates.</b></p>
						
						<p class='ptext'>  Colour all the required fields during Creation/Authorisation (As the case may be) Function to have result.</p>
						<p class='ptext'>  verification Test Cases. The report generated will list all the Expected and Actual results against every validations mentioned in the Data sheet.</p>
						</td></tr>
						<tr><td><p class='font1'><b>XI. How to run different scenarios in the same Function sheet Eg.Creation and verification using HTM menu.</b></p>
						
						<p class='ptext'>  Use different GID-UID set for unrelated scenarios, and define a new test case in Tenjin.</p>
						</td></tr>
						<tr><td><p class='font1'><b>X. AUT requires a Go button to be clicked to proceed with testing, which is not learnt by Tenjin and not available in the template.</b></p>
						
						<p class='ptext'>  Add GO button in Data Sheets since this needs to be handled by the user, and not through Tenjin.</p>
						</td></tr>
						<tr><td><p class='font1'><b>XI. While re-executing test cases Tenjin shows error that Tenjin is already running.</b></p>
						
						<p class='ptext'>  Before doing any learning or execution, please check task manager for IE Driver Server.</p>
						<p class='ptext'>  If any process is existing, kindly kill the same and start your execution.</p>
						</td></tr>
						<tr><td><p class='font1'><b>XII. How can multiple users work on the same data sheet for different functions.
						</b></p>
						
						<p class='ptext'>  Ensure Test Data File name is as per Tenjin standards. Eg.HTM_admin_TTD.xls. This allows multiple users to work on the same data sheet.</p>
						
						</td></tr>
						<tr><td><p class='font1'><b>XIII. Tenjin logging out without executing all rows.</b></p>
						
						<p class='ptext'>  Tenjin will not allow to have empty rows in between. Enter the no of rows under execution.</p>
						
						</td></tr>
						<tr><td><p class='font1'><b>XIV. Internal server error occurred during execution.
						</b></p>
						
						<p class='ptext'>  Stop the execution and rerun the same data set.</p> 
						<p class='ptext'> Logout and Login and attempt to rerun the test case.</p>
						</td></tr>
						<tr><td><p class='font1'><b>XV. The datasheet template does not contain one or more fields that is present in the screen.
						
						</b></p>
						
						<p class='ptext'>  Report to Development team on missing fields. If required re-learn the function id and use the latest template.</p>
						</td></tr>
						</tbody>	</table>
			<table class='border'width='70%' style='margin-left: 50px;'>
			<thead>
			
			</thead>
			
			<tbody>
			
			<tr><td><b><p class='font'>In case of any other issues, kindly log the same in Webissues and report it to Tenjin support.</p></b></tr>
			<tr><td> &nbsp;</td></tr>
			</tbody>
			</table>
					</fieldset>
					</div>
	<%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>
					


</body>
</html>