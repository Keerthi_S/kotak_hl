<!--

Yethi Consulting Private Ltd. CONFIDENTIAL

Name of this file:  apk_upload.jsp

Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE

Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
*/

-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>File Upload</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/ttree/ttree.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/ttree.js'></script>
<script src="js/formvalidator.js"></script>
<script type="text/javascript">
	function validate() {
		clearMessages();
		var allowedFiles = [ ".apk", ".ipa" ];
		var fileUpload = document.getElementById("filename");
		var filename = fileUpload.value.toLowerCase();

		var lblError = document.getElementById("lblError");
		var regex = new RegExp(allowedFiles.join('|'));

		if (!regex.test(filename)) {
			lblError.innerHTML = "Please upload files having extensions: <b>"
					+ allowedFiles.join(', ') + "</b> only.";
			return false;
		}
		lblError.innerHTML = "";
		return true;
		return (true);

	}
</script>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						clearMessages();
						if ($('#param').val() == 'autView') {
							var msg = $('#msg').val();
							window.showMessage(msg, 'success');
						}
						$('#btnCancel')
								.click(
										function() {
											window.parent.location.href = "ApplicationServlet?t=view&key="
													+ $('#appId').val();
										});
					});
</script>
</head>
<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		if (tjnSession != null) {
			String appId = request.getParameter("appId");
			String param = request.getParameter("param");
			String msg = request.getParameter("msg");
	%>
	<div class='title'>
		<p>File Upload</p>
	</div>
	<form name='new_project_form' method="POST" action='ApplicationServlet'
		enctype="multipart/form-data" onsubmit="return validate()">
		<div class='form container_16'>
			<input type="hidden" id="appId" name='appId' value='<%=appId%>' /> <input
				type="hidden" id="param" name='param' value='<%=param%>' /> <input
				type="hidden" id="msg" name='msg' value='<%=msg%>' />
				<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div id='user-message'></div>
			<fieldset style='border: 0px; margin-top: 10px;'>
				<div id='fieldSection' class='grid_8'>
					<div>
						<label for="lstDomain">Upload File</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="file" id="filename" name="filename" class="">
						<img src="images/inprogress.gif" class="loader-image"
							id="imgLoad_domain"> <br> <span id="lblError"
							style="color: red;"></span>
					</div>
				</div>
				<div class='clear'></div>
				<div id='fieldSection' class='grid_5' style='height: 15px;'></div>
				<div class='clear'></div>
				<div id='fieldSection' class='grid_5' style='text-align: center'>
					<input type='submit' value='Submit' class='imagebutton ok'
						id='btnOk' /> <input type='button' value='Cancel'
						class='imagebutton cancel' id='btnCancel'
						onclick='closeDialodBox()' />
				</div>
			</fieldset>
		</div>
	</form>

	<%
		} else {
			response.sendRedirect("noSession.jsp");
		}
	%>
</body>
</html>