<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testmanager_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.ycs.tenjin.testmanager.TestManagerInstance"%>
   <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 

*/
 
-->



<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Test Manager</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel="Stylesheet" href="css/bordered.css" />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/formvalidator.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/pages/testmanager_new.js'></script>	
<script src="js/formvalidator.js"></script>

</head>
<body>
     <%
     Cache<String, Boolean> csrfTokenCache = null;
     csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
     session.setAttribute("csrfTokenCache", csrfTokenCache);
     UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
     
        TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
         if(tjnSession!=null){
     %>

<div class='main_content'>
<div class='title'>
		<p> Test Management Instance Configuration </p>
	</div>
	<form name='new_testmanager' action='TestManagementServlet' method='get'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<div class='toolbar'>
		 
			<input type='button' value='Save' id='btnSave'
				class='imagebutton save' />
			<input type='button' value='Cancel' id='btnCancel' class='imagebutton cancel' />
        </div>
			
			
		<div id='user-message'></div>
		<div class='emptySmall'>&nbsp</div>

			<div class='form container_16' style='margin-top:-10px;'>
				<fieldset >
				<legend>Instance Details</legend>
					<div class='fieldSection grid_8'>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for=txtName>Name</label>
						</div>
						<div class='grid_4'>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						  <input type="text" class='stdTextBox mid'  mandatory='yes' maxlength='20'   name="txtName" id="txtName" title='Name' placeholder='MaxLength 20'>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						</div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='grid_2'>
							<label for='txtTool'>Tool</label>
						</div>
						<div class='grid_4'>
					     <select id='txtTool' title='Tool' mandatory='yes' name='txtTool' class='stdTextBoxNew'>
					     </select>
						</div>
					</div>
					<div class='fieldSection grid_16'>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtUrl'>URL</label>
						</div>
						<div class='grid_13'>
							<input type='text' id='txtUrl' name='txtUrl' class='stdTextBox long'   mandatory='yes' title='Url' maxlength='200' />
						</div>
					
			</div>		
		 </fieldset>
		</div>
			
</form>

	</div>
	<%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>
	
</body>
</html>