<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  newrvd.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


 
<%@page import="com.ycs.tenjin.bridge.pojo.run.ResultValidation"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============

 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Create Result Validation Function</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/newrvd.js'></script>
		<script type="text/javascript" src="js/formvalidator.js"></script>
	</head>
	<body>
		<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
 
		<%
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			Project project = null;
			project = tjnSession.getProject();
			
			boolean canMakeChanges = false;
			
			
			if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){
				canMakeChanges = true;
			}
			
			Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
			String status = "";
			String message = "";
			ResultValidation rv = null;
			if(map != null){
				status= (String)map.get("STATUS");
				message = (String)map.get("MESSAGE");
				rv = (ResultValidation)map.get("RVD");
			}
		%>
		
		<div class='title'>
			<p>New Result Validation Function</p>
		</div>
		
		<form name='main_form' action='ResultsServlet' method='POST'>
		<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		
			<div class='toolbar'>
				<input type='Submit' value='Save' id='btnSave' class='imagebutton save'/>
				<input type='button' value='Cancel' id='btnCancel' class='imagebutton cancel'/>
			</div>
			
			<%
			if(status != null && status.equalsIgnoreCase("SUCCESS") && message != null && !message.equalsIgnoreCase("")){
				%>
				<div id='user-message' class='msg-success' style='display:block;'>
					<div class="msg-icon">&nbsp;</div>
					<div class="msg-text"><%=message %></div>
				</div>
				<%
			}else if(status != null && !status.equalsIgnoreCase("") && !status.equalsIgnoreCase("SUCCESS")){
				%>
				<div id='user-message' class='msg-err' style='display:block;'>
					<div class="msg-icon">&nbsp;</div>
					<div class="msg-text"><%=message %></div>
				</div>
				
				<%
			}else{
				%>
				<div id='user-message'></div>
				
				<%
			}
			%>
			
			<div class='form container_16'>
				<fieldset>
					<legend>Application Information</legend>
					<div class='fieldSection grid_8'>
						<div class='clear'></div>
						<div class='grid_2'><label for='lstApplication'>Application</label></div>
						<div class='grid_5'>
							<select id='lstApplication' name='lstApplication' class='stdTextBox' mandatory='yes'></select>
						</div>
					</div>
				</fieldset>
				
				<fieldset>
					<legend>Basic Information</legend>
					<div class='fieldSection grid_16'>
						<div class='clear'></div>
						<div class='grid_2'><label for='txtName'>Name</label></div>
						<div class='grid_12'>
							<input type='text' id='txtName' name='txtName' class='stdTextBox long' mandatory='yes' title='Name'/>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'><label for='txtDesc'>Description</label></div>
						<div class='grid_12'>
							<textarea class='stdTextBox long' id='txtDesc' name='txtDesc' style='height:150px;'></textarea>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'><label for='lstType'>Verification Type</label></div>
						<div class='grid_12'>
							<select class='stdTextBox' id='lstType' name='lstType'>
								<option value='GUI'>GUI-Based</option>
								<option value='ASCII'>ASCII File Validation</option>
								<option value='Report'>Report Validation</option>
							</select>
						</div>
						
						
					</div>
				</fieldset>
			</div>
		</form>
	</body>
</html>