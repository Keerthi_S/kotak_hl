
<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  reruninit.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->



<%@page import="com.ycs.tenjin.client.RegisteredClient"%>
<%@page import="com.ycs.tenjin.project.TestCase"%>
<%@page import="com.ycs.tenjin.project.TestStep"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.project.TestSet"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="com.ycs.tenjin.device.RegisteredDevice"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--
Added by Roshni for TENJINCG-259( 3-Jul-2017 )
/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY               DESCRIPTION
  18-12-2020		   Pushpalatha				TENJINCG-1246
*/

-->
<head>
<title>Run Test - Tenjin Intelligent Testing Engine</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel="Stylesheet" href="css/bordered.css" />
<link rel='stylesheet' href='css/accordion.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/formvalidator.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/pages/reruninit.js'></script>
</head>
<body>


	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>

	<%
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			Project project = null;
			project = tjnSession.getProject();
			
			boolean canMakeChanges = false;
			
			
			if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){
				canMakeChanges = true;
			}
			
			
			
			Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
			String status = (String)map.get("STATUS");
			String message = (String)map.get("MESSAGE");
			TestSet testSet = (TestSet)map.get("TEST_SET");
			
			String backStatus=(String)map.get("Invoke_By");
			
			ArrayList<RegisteredClient> clientList = null;
			clientList = (ArrayList<RegisteredClient>) map.get("CLIENT_LIST");
			int parentRunId = (Integer) map.get("PARENT_RUN_ID");
			ArrayList<TestCase> tc=(ArrayList<TestCase>)map.get("TEST_CASES");
			
			String callBack=(String)map.get("callback");
			String callbackparam=(String)map.get("callbackparam");
		 
			String mobilityFlag = (String)map.get("MOBILITY_FLAG");
		%>

	<div class='title'>
		<p>Run Information</p>
		<div id='prj-info'>
			<span style='font-weight: bold'>Domain: </span> <span><%=project.getDomain() %></span>
			<span>&nbsp;&nbsp;</span> <span style='font-weight: bold'>Project:
			</span> <span><%=project.getName() %></span>
		</div>
	</div>

	<form name='main_form' action='PartialExecutorServlet' method='POST'>
		<div class='toolbar'>
			<input type='button' class='imagebutton back' value='Back'
				id='btnBack' /> 
				<input type='hidden'  value=''
				id='client' name='client'/> 
				<input type='submit' value='Run Test'
				class='imagebutton runtestset' id='btnRun' /> <input type='hidden'
				value='<%=backStatus %>' id='backStatus' /> <input type='hidden'
				id='parentRunId' name='parentRun' value='<%=parentRunId %>' />
		</div>
		 <input type="hidden" id='callback' value='<%=callBack%>' name='callback'/>
		 <input type="hidden" id='callbackparam' value=<%=callbackparam%> name='callbackparam'>
		 <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		 
		<%
			if(status != null && status.equalsIgnoreCase("SUCCESS")){
				%>
		<div id='user-message' class='msg-success' style='display: block;'>
			<div class="msg-icon">&nbsp;</div>
			<div class="msg-text"><%=message %></div>
		</div>
		<%
			}else if(status != null && !status.equalsIgnoreCase("") && !status.equalsIgnoreCase("SUCCESS")){
				%>
		<div id='user-message' class='msg-err' style='display: block;'>
			<div class="msg-icon">&nbsp;</div>
			<div class="msg-text"><%=message %></div>
		</div>

		<%
			}else{
				%>
		<div id='user-message'></div>

		<%
			}
			%>

		<div class='form container_16'>
		<fieldset>
		<legend>Test Set Details</legend>
			<input type='hidden' id='executionMode' name='executionMode'
				value='<%=testSet.getMode() %>' />
			<div class='fieldSection grid_8'>
				<input type='hidden' id='txtTsRecId' value='<%=testSet.getId() %>'
					name='txtTsRecId' />
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='txtTestSetId'>Test Set ID</label>
				</div>
				<div class='grid_4'>
					<%
						if(testSet.getId() > 0){
							%>
					<input type='text' id='txtTestSetId' name='txtTestSetId'
						disabled='disabled' class='stdTextBox info-holder'
						value='<%=testSet.getId()%>' /> <input type='hidden'
						id='txtCreateTestSet' name='createTestSet' value='yes' />
					<%
						}else{
							%>
					<input type='text' id='txtTestSetId' name='txtTestSetId'
						disabled='disabled' class='stdTextBox info-holder' value='TBD' />
					<input type='hidden' id='txtCreateTestSet' name='createTestSet'
						value='yes' />
					<%
						}
						%>
				</div>
				</div>
				<div class='fieldSection grid_7'>
				<div class='grid_2'>
					<label for='runId'>Run ID</label>
				</div>
				<div class='grid_4'>
				<input type='text' disabled='disabled' id='runId' class='stdTextBox info-holder' value='<%=parentRunId%>'/>
				</div>
				</div>
				<div class='fieldSection grid_16'>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='txtTestSetName'>Name</label>
				</div>
				<div class='grid_13'>
					<input type='text' id='txtTestSetName' name='txtTestSetName'
						value='<%=testSet.getName() %>' disabled='disabled'
						class='stdTextBox info-holder long' mandatory='yes' title='Name' />
				</div>
			</div>
			</fieldset>
			<%
				/* if(testSet.getMode().equalsIgnoreCase("API")){
					
				}else{ */
					%>
			<fieldset>
				<legend>Settings</legend>
				<div class='fieldSection grid_8'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtclientregistered'>Client</label>
					</div>
					<div class='grid_4'>
						<%
							
							String ipAddress=request.getRemoteAddr();
                        	String hostName=request.getRemoteHost();
							
                        	boolean flag=false;
                        		String clientName=null;
                        		String host=null;
                        		
							if (clientList.size() > 0) { %>
						<select id='txtclientregistered' name='txtclientregistered'
							class='stdTextBox'>
							<%for (RegisteredClient rc : clientList)  { 
								
									if(ipAddress.equals(rc.getHostName())|| hostName.equals(rc.getHostName()))
                    				{
                    					flag=true;
                    					clientName=rc.getName();
                    					host=rc.getHostName();
                    					break;
                    				}
								}
								
								if(flag){
								
								%>
							<option host='<%=host %>' value='<%=host %>'><%=clientName %></option>
							<% }else
                                   		{
                                   	%>
							<option value='-1'>---Select One---</option>
							<%  } 
									for (RegisteredClient rc : clientList)  {
										if(rc.getName().equals(clientName))
										{
											
										}
										else
										{
									%>
							<option host='<%=rc.getHostName() %>' value='<%=rc.getHostName() %>'><%=rc.getName() %></option>
							<% 	}
									}
									%>
						</select>
						<%} 
							%>
					</div>
					<%
					if(mobilityFlag!=null && mobilityFlag.equalsIgnoreCase("True")) {
					%>
					<input type="hidden" name='mobFlag' value='True'/>
					<div class='clear'></div>
						
					<div class='grid_2'>
						<label for='listDevice'>Device</label>
					</div>
					<div class='grid_4'>
						<select id='listDevice' name='listDevice' class='stdTextBox'>
						<option value='-1'>---Select One---</option>
						 
						</select>
					</div>
					<%
						}
					%>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='Screen'>Screenshot</label>
					</div>
					<div class='grid_4'>
						<%
								String scrShotOption = tjnSession.getProject().getScreenshotoption();
								%>
						<input type='hidden' id='defaultScreenshotOption'
							value='<%=scrShotOption %>' /> <select id='Screen' name='Screen'
							class='stdTextBox'>
							<option value='0'>Never Capture</option>
							<option value='1' selected>Capture Only for Failures /
								Errors</option>
							<option value='2'>Capture For All Messages
							<option value='3'>Capture At the End of Each Page</option>
						</select>
					</div>
					
			 
					<input type='hidden' id='videoCaptureFlag' name='videoCaptureFlag' value='N' />
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstBrowserType'>Browser</label>
					</div>
					<div class='grid_4'>	
						<select id='lstBrowserType' name='lstBrowserType'
							class='stdTextBox'>
							<option value='APPDEFAULT'>Use Application Default</option>
							<option value='<%=BrowserType.CHROME %>'><%=BrowserType.CHROME %></option>
							<!-- Modified by Prem for TENJINCG-1246 starts -->
							<%-- <option value='<%=BrowserType.FIREFOX %>'><%=BrowserType.FIREFOX %></option> --%>
							<option value='<%=BrowserType.IE %>'><%=BrowserType.IE %></option>
							  <option value='<%=BrowserType.CE %>'>Microsoft Edge</option>
							  <!-- Modified by Prem for TENJINCG-1246 ends -->
						</select>
					</div>
					<div class='clear'></div>
				 
				</div>
			</fieldset>
			<%
				/* } */
				%>


               <fieldset>
					<legend>Tests</legend>
					<div>
						<ul id='tabs'>
							<li><a href='#test-steps-tab' class='teststeps maptcs' id='test-steps-tab-anchor'>Tests included for this run</a>
							<li>
							 
						</ul>
						<div id='test-steps-tab' class='tab-section'>
							
							<div id='user-message-steps'></div>
							
							<div id='dataGrid'>
								<div id='accordion'>
									<%
								   if(testSet!=null){
								   
									ArrayList<TestCase> testCases = tc;
									if(testCases != null && testCases.size() > 0){
										for(TestCase testCase:testCases){
											if(testCase.getTcSteps() != null && testCase.getTcSteps().size() > 0){
											%>
											<h3><%=testCase.getTcId() %> - <%=testCase.getTcName() %></h3>
											<div>
												<div >
													<table class='bordered'>
														<thead>
															<tr>
																<th class='text-center'>Step No.</th>
																<th>ID</th>
																<th>Description</th>
																<th>Type</th>
																<th>Application</th>
															</tr>
														</thead>
														<tbody>
															<%
															if(testCase.getTcSteps() != null && testCase.getTcSteps().size() > 0){
																int counter=0;
																for(TestStep step:testCase.getTcSteps()){
																	counter++;
																	%>
																	<tr>
																		<td class='text-center'><%=counter %></td>
																		<td><%=step.getId() %></td>
																		<td><%=step.getShortDescription() %></td>
																		<td><%=step.getType() %></td>
																		<td><%=step.getAppName() %></td>
																	</tr>
																	<%
																}
															}else{
																%>
																<tr>
																	<td colspan='5'>This test case has no steps</td>
																</tr>
																<%
															}
															%>
														</tbody>
													</table>
												</div>
											</div>
											<%
											}
										}
									 }
									}
									%>
								</div>
							</div>
						</div>




</div>
</fieldset>

		</div>
		<input type='hidden' id='moduleList' name='moduleList' value='' /> <input
			type='hidden' id='learnType' name='learnType' value='NORMAL' />
	</form>
</body>
</html>