<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  uploadFromTestSet.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="java.util.Map"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 
*/

-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>File Upload</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/ttree/ttree.css' />
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/ttree.js'></script>
<%
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
			String status = (String)map.get("status");
			String message = (String)map.get("message");
			String path=(String)map.get("path");
			String param=(String)map.get("param");
		if(status.equalsIgnoreCase("SUCCESS"))
		{
		%>
			<script>
			$(document).ready(function() {
				var msg="<%=message%>";
				var path="<%=path%>";
				var param="<%=param%>";
				 
				var redirectUrl ;
				window.parent.closeModalWindow();
			 		redirectUrl = "TestSetServletNew?t=testSet_View&msg="+msg+"&downloadPath="+path+"&paramval="+param;
			 	redirectUrl = encodeURI(redirectUrl);
			 	window.parent.location.href=redirectUrl;
				});
			 </script>
		<%
		}
		else
		{
			%>
			<script>
			$(document).ready(function() {
				var mes="<%=message%>";			
				window.parent.closeModalWindow();
				window.parent.showMessage(mes,'error');
				});
			</script>
			<%
		}
	   %> 
</head>
<body>

</body>
</html>