<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  data_extraction_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Data Extraction</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel='stylesheet' href='css/select2.min.css'/>
		<link rel="SHORTCUT ICON" HREF="images/yethi.png">
		
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/data_extraction_new.js'></script>
		<script type="text/javascript" src='js/formvalidator.js'></script>
		<script type='text/javascript' src='js/select2.min.js'></script>
	</head>
	<body>
		<%
		
	    Cache<String, Boolean> csrfTokenCache = null;
	    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
	    session.setAttribute("csrfTokenCache", csrfTokenCache);
	    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
	    session.setAttribute ("csrftoken_session", csrftoken);
		
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Map<String, Object> map = (Map<String, Object>)request.getSession().getAttribute("EXTRACTOR_SCREEN_MAP");
		String status = (String)map.get("STATUS");
		String message = (String)map.get("MESSAGE");
		if(message == null){
			message = "";
		}
		List<Aut> auts = (List<Aut>)map.get("AUTS");
		%>
     <%
       
         if(tjnSession!=null){
     %>

		
		<div class='title'>
			<p>Data Extraction</p>
		</div>
		<form name='extraction-form' action='ExtractorServlet' method='POST' enctype='multipart/form-data' onsubmit="return validate()">
			<div class='toolbar'>
				<input type='submit' id='btnGo' value='Go' class='imagebutton ok'/>
				<input type='reset' id='btnReset' value='Clear' class='imagebutton refresh'/>
			</div>
			
			<input type='hidden' id='screenStatus' value='<%=status %>'/>
			<input type='hidden' id='screenMessage' value='<%=Utilities.escapeXml(message )%>'/>
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<%
			 if(status.equalsIgnoreCase("Failure")){
				%>
				<div id='user-message' class='msg-err' style='display:block;'>
				<div class="msg-icon">&nbsp;</div>
					<div class="msg-text"><%=message %></div>
				</div>
				<%} else{%>
					<div id='user-message'></div>
				<%}
				%>
			
			<div class='form container_16'>
				<fieldset >
				<legend>Basic Information</legend>
					
					<div class='fieldSection grid_10'>
						<div class='grid_2'>
							<label for='lstApplication'>Application</label>
						</div>
						<div class='grid_7'>
								<select id='lstApplication' name='lstApplication' class='stdTextBox' mandatory='yes' title="Application">
								<option value='-1'>-- Select One --</option>
								<%
								for(Aut aut:auts){
									%>
									<option value='<%=aut.getId() %>'><%=Utilities.escapeXml(aut.getName()) %></option>
									<%
								}
								%>
							</select>
						</div>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='lstModules'>Function</label>
						</div>
						<div class='grid_7'>
							<select id='lstModules' name='lstModules' class='stdTextBox' mandatory='yes' title="Function">
								<option value='-1'>-- Select One --</option>
								
							</select>
						</div>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtInputFile'>Input File</label>
							
						</div>
						
						<div class='grid_7'>
							<input type='file' id='txtInputFile' name="inputFilePath" class='stdTextBox' mandatory='yes' title="Input File" />
							</br><span id="lblError" style="color: red;"></span>
						</div>
					</div>
				</fieldset>
			</div>
		</form>
	<%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>
		
	</body>
</html>