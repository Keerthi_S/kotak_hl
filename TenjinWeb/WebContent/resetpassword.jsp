<!--
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  resetpassword.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright ? 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%@page import="java.util.UUID"%>
<%@page import="com.google.common.cache.Cache"%>
<%@page import="com.google.common.cache.CacheBuilder"%>
<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- New File Added by Prafful for Req#TJN_23_05 on 10-Aug-2016 -->

<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION

-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<title>ResetPassword</title>
<link rel='stylesheet'href='${pageContext.request.contextPath}/css/cssreset-min.css' />
<link rel='stylesheet'href='${pageContext.request.contextPath}/css/ifr_main.css' />
<link rel='stylesheet'href='${pageContext.request.contextPath}/css/style.css' />
<link rel='stylesheet'href='${pageContext.request.contextPath}/css/tabs.css' />
<link rel="Stylesheet"href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
<link rel='stylesheet'href='${pageContext.request.contextPath}/css/buttons.css' />
<link rel='stylesheet'href='${pageContext.request.contextPath}/css/960_16_col.css' />
<script type='text/javascript'src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
<script type="text/javascript"src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
<script type="text/javascript" src='js/formvalidator.js'></script>
<script type='text/javascript'src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
<script type='text/javascript'src='${pageContext.request.contextPath}/js/data-grids.js'></script>
<script type='text/javascript'src='${pageContext.request.contextPath}/js/sha.js'></script>

<script>

		$(document).ready(function(){
		 /*  mandatoryFields('form container_16'); */

		 $(document).on("click", '#submit', function() {
			var userId = $('#userId').val();
			var email = $('#email').val();
         if(userId=='' || userId ==null)
             {
        	 showMessage('User Id is mandatory','error');
				return false;
             }else if(email=='' || email ==null)
             {
            	 showMessage('Email Id is mandatory','error');
    				return false;
                 }
         else{
        	 window.location.href='TenjinPasswordResetServlet?param=resetreq&userid=' + $('#userId').val()+'&email=' + $('#email').val();
             }

		});
		}); 
		</script>
</head>
<style>
  .required:after {
    content:" *";
    color: red;
  }
  </style>
<body>
	
	<div class='title'>Reset Password</div>


	<input type='hidden' id='tjn-status' value='${screenState.status }' />
	<input type='hidden' id='tjn-message' value='${screenState.message }' />
		<div id='user-message'></div>

		<div class='form container_16'>
			<div class='fieldSection grid_8'>
				<div class='clear'></div>
				<div class='grid_3'>
					<label for='userId' class="required">User Id</label>
				</div>
				<div class='grid_4'>
					<input type='text' id='userId' name='userId' mandatory='yes'
						class='stdTextBox' />
				</div>
				<div class='clear'></div>
				<div class='grid_3'>
					<label for='email' class="required">Email</label>
				</div>
				<div class='grid_4'>
					<input type='text' id='email' name='email' mandatory='yes'
						class='stdTextBox' />
				</div>
				<div class='grid_4'>
					<input type='submit' value='Submit' class='imagebutton ok'
						id='submit' />
				</div>
				<div class='clear'></div>
			</div>


		</div>

	<!-- </form> -->
</body>
</html>