<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  tsetmap.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.project.TestCase"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
   <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************

* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
  
*/

-->
	<head>
		<title>Test case mapping - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel='stylesheet' href='css/ttree/ttree.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="js/formvalidator.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/tsetmap.js'></script>
		
	
		<style type="text/css">
		#testCaseId, #testCaseName{
		max-width:75px;
	    text-overflow: ellipsis;
	    white-space: nowrap;
	    overflow: hidden;
	    
		}
		.mapping{
   max-width:100px;
    white-space: nowrap;
    overflow: hidden !important;
    text-overflow: ellipsis;
      overflow: visible;
      
		 }
		</style>
		
	</head>
	<body style="overflow-x: hidden;">
		<%
		
		Cache<String, Boolean> csrfTokenCache = null;
	     csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
	     session.setAttribute("csrfTokenCache", csrfTokenCache);
	     UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
	     
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			Project project = null;
			project = tjnSession.getProject();
			
			boolean canMakeChanges = false;
			
			
			if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){
				canMakeChanges = true;
			}
			Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
			ArrayList<TestCase> testcases=(ArrayList<TestCase>)map.get("tcs");
			String tsRecID=(String)map.get("tsRecId");
			String tsName=(String)map.get("tsName");
			String status=(String)map.get("status");
		    String message=(String)map.get("message");
		    
		    
		    String mode=(String)map.get("mode");
		    
		    String priority=(String)map.get("priority");
		    		%>
		<div class='title'>
			<p>Map Test Cases to Test Set</p>
		 
		</div>
		
		
		<form name='main_form' action='TestSetServlet' method='POST'>
	 <input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
			<input type='button' id='btnBack' class='imagebutton back' value='Back' />
				 
				<input type='button' value='Go' id='btnOk' class='imagebutton ok'/>
				 
				<input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh' />
				 
				<div id='user-message'></div>
				 
			</div>
			
			 
		   <div class='form container_16' id='bodyContainer' style="width:1070px;">
		   <div id='user-message'></div>
		    <fieldset>
					<legend >Test Set Details</legend>
					 <div class='fieldSection grid_7' >
						<div class="clear" ></div>
						<div class="grid_2"><label for="txtDomainName">Test Set</label></div>
						<div class="grid_4">
							<input type="text" id="txtTestSet" name="txtTestSet" value='<%=tsName %>' class="stdTextBox info" disabled="disabled" title="TestSet">
						</div>
						<div class="clear"></div>
						  
					<div class="clear"></div>
						</div>
				</fieldset>
			</div>
		 	<div class='form container_16' id='bodyContainer'>
		
		
			<fieldset>
		   <legend>Map Test Cases</legend>
		   
		     
			 <div class='form container_16' style="margin-left: 95px;" >
		     <div id='dataGrid' class='grid_7' style="margin-left: -95px;width: 450px;">
		  
                <p style='margin-bottom:10px;'><b>Unmapped Test Cases</b></p> 
                <p style="text-align:right "  class="unmapsearch" > Search: <input id="usearch" type="text"> </p>   
	        	<table id='Testcase-Table' class='display' cellspacing='0'
					width='100%'>
					<thead>
					<tr>
					 
                           <th class='tiny'><input type='checkbox' id='chk_all' class='tbl-select-all-rows unmapped-testcase'	 title='Select All'></th>
	                       <th>Test Case ID</th>
						   <th>Test Case Name</th>
					</tr>
					</thead>
					<tbody id="unmapsetbody">
				 	</tbody>
					</table>
				 
				</div>
				 
				<div class='grid_7' id='tsMapedTcContainer' style='width:27px;
				margin-left:17px;
                    border-radius:0px;
                    overflow: none;
                    margin-top:160px;
                   	box-shadow: none;
  					border:none;
  					min-height:60px;
  				    max-height:100px;
				    background:none;'>
  
  
  
  
  <input type='button'   id='btnAddTC' class='imagebutton add' style="width:5px; padding-left:10px;"/><br/><br/>
  <input type='button'  id='btnRemoveTC' class='imagebutton delete' style="width:5px; padding-left:10px"/>
  
            
            
</div>
			
			
			<div id='dataGrid' class='grid_7' style="width: 450px;">
			<p style='margin-bottom:10px;'><b>Mapped Test Cases</b></p>
			<p style="text-align:right" class="mapsearch" > Search: <input id="search" type="text"> </p>
		    	<table id='MapTestcase-Table' class='display sortable-table' cellspacing='0' width='100%'>
					<thead>
						<tr>
                           <th class='tiny'><input class='tbl-select-all-rows mapped-testcase' type='checkbox' id='check_all' title='Select All'></th>						
	                       <th>Test Case ID</th>
						   <th>Test Case Name</th>
						</tr>
					</thead>
					<tbody id="tsetbody">
					</tbody>
				</table>
			</div>
			</div>
			</fieldset>
			</div>
			<input type='hidden' value='<%=tsRecID %>' id='recID'/>
			<input type='hidden' value='<%=tsName %>' id='tsName'/>
			<input type='hidden' value='<%=mode %>' id='tsMode'/>
			
			<input type='hidden' value='<%=mode %>' id='mode'/>
			
		</form>
 
	</body>
</html>