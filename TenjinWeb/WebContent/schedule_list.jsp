<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.scheduler.Scheduler"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
	 <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
  
*/

-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Scheduler Maintenance</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/bordered.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel='stylesheet' href='css/accordion.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/pages/schedule_list.js'></script>
<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
<script src="js/formvalidator.js"></script>
</head>
<body>
	<div class='main_content'>
		 
		<%
		Cache<String, Boolean> csrfTokenCache = null;
		csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
		session.setAttribute("csrfTokenCache", csrfTokenCache);
		UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE); 
		 session.setAttribute ("csrftoken_session", csrftoken);
			TenjinSession tjnSession = (TenjinSession) request.getSession()
					.getAttribute("TJN_SESSION");
			Project project = null;
			project = tjnSession.getProject();
			String scrStatus = "";
			String message = "";
			String projectId = "";
			ArrayList<Scheduler> scheduleList = null;
			Map<String, Object> map = (Map<String, Object>) request
					.getSession().getAttribute("SCR_SCH");
			if (map != null) {
				scrStatus = (String) map.get("SCH_STATUS");
				message = (String) map.get("MESSAGE");
				projectId = (String) map.get("projectId");
				scheduleList = (ArrayList<Scheduler>) map.get("SCH_LIST");
			}
			
		%>
		<%
			if (project != null) {
				String projState=project.getState();
		%>
		 
		<input type='hidden' id='projId' value='<%=projectId%>' />
	 
<input type='hidden' id='projState' value='<%=project.getState()%>' />
		<%
			}
		%>
       
       <div class='title'>
			<p>Task Status</p>
			<%
			if (project != null) {
			
			%>
		 
			<%} %>
		</div>
		<form name='sch_list_form' action='SchedulerServlet' method='post'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<ul id="tabs">
				<li><a href='#scheduler-scheduled-tab'
					id='scheduler-all-tab-anchor' class="all_jobs">All Tasks</a></li>
				<li><a href='#scheduler-scheduled-tab'
					id='scheduler-scheduled-tab-anchor' class="schedule">Scheduled Tasks</a></li>
				<li><a href='#scheduler-scheduled-tab'
					id='scheduler-progress-tab-anchor' class="progress">Running Tasks</a></li>
				<li><a href='#scheduler-scheduled-tab'
					id='scheduler-complete-tab-anchor' class="ok">Completed Tasks</a></li>
				<li><a href='#scheduler-scheduled-tab'
					id='scheduler-cancelled-tab-anchor' class="cancel">Cancelled Tasks</a></li>
				<li><a href='#scheduler-scheduled-tab'
					id='scheduler-aborted-tab-anchor' class="abort"
					style='font-weight: bold; color: #cc0000;'>Aborted Tasks</a></li>
			</ul>
			<div id='scheduler-scheduled-tab' class='tab-section'>
			 
				 <% 
				if (project != null) {
			String projectState=project.getState();
			if(projectState.equalsIgnoreCase("A")){
			%>
				<div class='toolbar'>
					<div id='AllTasks'>
					 
							
					</div>
				</div>
				<%}} %>
	 
				<div id='user-message'></div>
			<input type=hidden value='<%=message%>' id='message' />
				<div class='form container_16' style='margin: 0 auto;'>

					<div id='dataGrid'>
						<table id='userTable' class='borded' cellspacing='0' width='100%'>
							<thead>
								<tr>
									 
									<th class='nosort' type='hidden'></th>
									<th>Task Name</th>
									<th class="text-center">Task Id</th>
									<th>Scheduled Date</th>
									<th>Task</th>
									<th>Client</th>
									<th>Status</th>
									<th>Recurrence</th>
									<th>Created By</th>
									<th>Run Information</th>
								</tr>
							</thead>
							<tbody>
								<%
									if (scheduleList.size() > 0) {
										for (Scheduler scheduler : scheduleList) {
								%>
								<tr id=schList>
									<td type='hidden'></td>
									<%
									if(scheduler.getTaskName()==null) {
										scheduler.setTaskName("");
									}
									%>
									<td><%=Utilities.escapeXml(scheduler.getTaskName())%></td>
									<%
										if (scheduler.getStatus().equalsIgnoreCase("Executing")
														|| scheduler.getStatus()
																.equalsIgnoreCase("Aborted")) {
									%>
									<td class="text-center">
							 
									<%=scheduler.getSchedule_Id()%>
									</td>
									<%
										} else {
									%>
									<td class="text-center"><a
										href='SchedulerServlet?param=VIEW_SCH&paramval=<%=scheduler.getSchedule_Id()%>&tabType=<%=scheduler.getAction()%>'
										id='<%=scheduler.getSchedule_Id()%>' class='edit_record'><%=scheduler.getSchedule_Id()%></td>

									<%
										}
									%>
									<td><%=scheduler.getSch_date()%>&nbsp;&nbsp;<%=scheduler.getSch_time()%></td>
									 
									<td><%=scheduler.getAction()%></td>
									<td><%=Utilities.escapeXml(scheduler.getReg_client())%></td>
									<td><%=scheduler.getStatus()%></td>
									<td><%=scheduler.getSchRecur()%></td>
									<td><%=Utilities.escapeXml(scheduler.getCreated_by())%></td>

									<%
										if(scheduler.getStatus().equalsIgnoreCase("Scheduled")){
										%>	
										<td>N/A</td>
										<% 
										}else
									
										if (scheduler.getAction().equalsIgnoreCase("Execute")) {
													if (scheduler.getStatus().equalsIgnoreCase("Aborted")
															|| scheduler.getStatus().equalsIgnoreCase(
																	"Cancelled")) {
									%>
									<td>N/A</td>
									<%
										} else {
									%>
									<td><a
										href='ResultsServlet?param=run_result&run=<%=scheduler.getRun_id()%>&callback=scheduler'>View</a></td>
									<%
										}
												} else if (scheduler.getAction()
														.equalsIgnoreCase("Extract")) {
													if (scheduler.getStatus().equalsIgnoreCase("Aborted")
															|| scheduler.getStatus().equalsIgnoreCase(
																	"Cancelled")) {
									%>
									<td>N/A</td>
									<%
										} else {
									%>
									<td><a
										href='ExtractorServlet?param=extractor_progress&runid=<%=scheduler.getRun_id()%>'>View</a></td>
									<%
										}
												} else if (scheduler.getAction()
														.equalsIgnoreCase("LearnAPI")) {
													if (scheduler.getStatus().equalsIgnoreCase("Aborted")
															|| scheduler.getStatus().equalsIgnoreCase(
																	"Cancelled")) {
									%>
									<td>N/A</td>
									<%
										} else {
									%>
									<td>
									<a href='ApiLearnerServlet?param=learner_result&runid=<%=scheduler.getRun_id()%>&callback=scheduler'>View</a></td>
									<%
										}
												} else {
													if (scheduler.getStatus().equalsIgnoreCase("Aborted")
															|| scheduler.getStatus().equalsIgnoreCase(
																	"Cancelled")) {
									%>
									<td>N/A</td>
									<%
										}
													else {
									%>
									<td>
									<a href='LearnerServlet?param=learner_progress&runid=<%=scheduler.getRun_id()%>&callback=schedule'>View</a></td>
									<%
			
													}
												}
									%>

								</tr>
								<%
									}
									} else {
								%>
							 
								<%
									}
								%>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</form>
	</div>
	<div class='sub_content' style='height: 500px;'>
		<iframe class='ifr_Main' name='content_frame' id='content_frame'
			src=''></iframe>
	</div>


</body>
</html>