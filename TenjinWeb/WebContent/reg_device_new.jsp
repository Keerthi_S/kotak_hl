<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  reg_device_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Device</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script src="js/formvalidator.js"></script>
<script type='text/javascript' src='js/pages/reg_device_new.js'></script>
</head>
<body>

	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<%
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		if (tjnSession != null) {
	%>
	<div class='title'>
		<p>Register New Device</p>
	</div>
	<form name='new_device' action='DeviceServlet' method='get'>
		<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		
		<div class='toolbar'>
			<input type='button' value='Save' id='btnRegister'
				class='imagebutton save' /> <input type='button' value='Back'
				id='btnCancel' class='imagebutton cancel' />
		</div>

		<div id='user-message'></div>
		<div class='form container_16'>
			<fieldset>
				<legend>Basic Information</legend>

				<div class='fieldSection grid_16'>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtDeviceName'>Device Name</label>
					</div>
					<div class='grid_13'>
						<input type='text' id='txtDeviceName' name='txtDeviceName'
							class='stdTextBox mid' mandatory='yes' title='Device Name'
							maxlength="255" />
					</div>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtDeviceId'>Device Id</label>
					</div>
					<div class='grid_13'>
						<input type='text' id='txtDeviceId' name='txtDeviceId'
							class='stdTextBox mid' mandatory='yes' title='Device Id'
							maxlength="255" />

					</div>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtDeviceType'>Device Type</label>
					</div>
					<div class='grid_13'>
						<select id='txtDeviceType' name='txtDeviceType' mandatory='yes'
							class='stdTextBoxNew'>
							<option value='Device'>Device</option>
							<option value='Emulator'>Emulator</option>

						</select>
					</div>

					<div class='clear Platform'></div>
					<div class='grid_2 Platform'>
						<label for='txtPlatform'>Platform</label>
					</div>
					<div class='grid_13 Platform'>
						<select id='txtPlatform' name='txtPlatform' mandatory='yes'
							class='stdTextBoxNew'>
							<option value='Android'>Android</option>
							<option value='iOS'>iOS</option>
						</select>
					</div>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtPltVersion'>Platform Version</label>
					</div>
					<div class='grid_13'>
						<input type='text' id='txtPltVersion' name='txtPltVersion'
							class='stdTextBox mid' mandatory='yes' title='Platform Version'
							maxlength="50" />

					</div>

				</div>
			</fieldset>
		</div>

	</form>
	<%
		} else {
			response.sendRedirect("noSession.jsp");
		}
	%>
</body>
</html>