<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_recurrence.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!-- Added by sahana for scheduling recurrence  on 29/11/2016 -->
<%@page import="com.ycs.tenjin.TenjinSession"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 03-06-2020				Ruksar					Tenj210-110
*/

-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Recurrence Preferences</title>
<link rel="stylesheet" href="css/cssreset-min.css">
<link rel="stylesheet" href="css/ifr_main.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/buttons.css">
<link rel="stylesheet" href="css/960_16_col.css">
<link rel="Stylesheet" href="css/jquery.dataTables.css">
<script type="text/javascript" src="js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="js/pages/tjnmaster.js"></script>
<script type='text/javascript' src='js/pages/schedule_admin_new.js'></script>
<script type='text/javascript' src='js/pages/schedule_reccurence.js'></script>

<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script src="js/formvalidator.js"></script>
<script type='text/javascript' src='js/ttree.js'></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script type='text/javascript' src="js/jquery-1.12.4.js"></script>
<script type='text/javascript' src="js/jquery-ui.js"></script>
<style>
	#footer {
  	  position: fixed;
   	 bottom: 0;
     width: 100%;
     text-align:center
	}
</style>
</head>
<body>
	<%
	Cache<String, Boolean> csrfTokenCache = null;
	csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
	session.setAttribute("csrfTokenCache", csrfTokenCache);
	UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE); 
	
		String param=request.getParameter("paramval");
		String dateHr = request.getParameter("val");
		String[] str=dateHr.split(",");
		String dateName=str[0];
		String hour=str[1];
		String frequency="";
		String recurCycle="";
		String recurDays=""; 
		String endDate="";
		if(str.length>2){
		frequency=str[2];
		}
		if(str.length>3){
		recurCycle=str[3];
		}
		if(str.length>4){
		recurDays=str[4];
		}
		if(str.length>5){
		endDate=str[5];
		}
	%>
	
	
     <%
        TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
         if(tjnSession!=null){
     %>
	
	<div class='main_content'>
		<div class='title'>
			<p>Recurrence Preferences</p>
		</div>

		<form name='schedule_recurrence_form' action='' method=''>
		 
        <input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div id='user-message'></div>
			<div class='form container_16'>
				<fieldset>
					<legend>Recurrence Preference</legend>
					<div id='recurrenceOption' class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='recurrence'>Frequency</label>
						</div>
						<div class='grid_4'>
							<select id='recurrence' name='recurrence' mandatory='yes'
								class='stdTextBoxNew'>
								<option value='none'>None</option>
								<!-- <Modified by ruksar for Tenj210-110 starts -->
								<option value='daily'>Periodic</option>
								<!-- <Modified by ruksar for Tenj210-110 ends -->
								<option value='daily-Repetitive'>Daily-Repetitive</option>
								<option value='weekly'>Weekly</option>
							</select>
						</div>
						<div id='dailyOption'>
							<div class='clear'></div>
							<div class='clear'></div>
							<div class='grid_2' style="float: right; width: 260px;">
								<div class="input-w">
									&nbsp;Every &nbsp;<input type='text' id='numDays' maxlength='3'
										size='2' /> day(s)
								</div>
								 
							</div>

						</div>
						<div id='weeklyOption'>
							<div class='clear'></div>
							<div class='clear'></div>
							<div class='clear'></div>
							 
							<div class='grid_2' style="float: left; width: 520px;">
								<div class="input-w" id='checks'>
							 
										<label for="chk1" style="display:inline"><input type="checkbox" name="chk1"
											id="chk1" value='Monday' class='chk'>&nbsp;Monday</label>&nbsp;
									<label for="chk2" style="display:inline" ><input type="checkbox" name="chk2"
										class='chk' id="chk2" value='Tuesday'>&nbsp;Tuesday</label>&nbsp;
									<label for="chk3" style="display:inline" ><input type="checkbox" name="chk3"
										class='chk' id="chk3" value='Wednesday'>&nbsp;Wednesday</label>&nbsp;
									<label for="chk4" style="display:inline" ><input type="checkbox" name="chk4"
										class='chk' id="chk4" value='Thursday'>&nbsp;Thursday</label>&nbsp;
									<label for="chk5" style="display:inline" ><input type="checkbox" name="chk5"
										class='chk' id="chk5" value='Friday'>&nbsp;Friday</label>&nbsp;
									<label for="chk6" style="display:inline" ><input type="checkbox" name="chk6"
										class='chk' id="chk6" value='Saturday'>&nbsp;Saturday</label>&nbsp;
									<label for="chk7" style="display:inline" ><input type="checkbox" name="chk7"
										class='chk' id="chk7" value='Sunday'>&nbsp;Sunday</label>&nbsp;
								</div>
							</div>
						</div>
							<input type='hidden' id='savedFrequency' value='<%=frequency %>'/>
							<input type='hidden' id='savedRecurCycle' value='<%=recurCycle %>'/>
							<input type='hidden' id='savedRecurDays' value='<%=recurDays %>'/>
							<input type='hidden' id='savedEndDate' value='<%=endDate %>'/>
							<input type='hidden' id='param'  value='<%=param %>'/>
						
						<div id='dailyRepOption'>
							<div class='clear'></div>
							<div class='clear'></div>
							<div class='grid_2' style="float: right; width: 260px;">
								<div class="input-w">
									&nbsp;Every &nbsp;<input type='text' id='numHrs' maxlength='2'
										size='2' /> hour(s)
								</div>
							 
							</div>

						</div>
						<div class='clear'></div>
						<div class='clear'></div>
						<div id='end-date'>
							<div class='grid_2'>
								<label for='datepicker1'>End Date</label>
							</div>
							<div class='grid_4'>
								<input type='hidden' id='hourTime' name='hourTime'
									value='<%=hour%>' /> <input type='hidden'
									id='hiddendatepicker1' name='hiddendatepicker1'
									value='<%=dateName%>' />
						 
									<input type='text' id="datepicker1"
									style='border: 1px solid #ccc; border-radius: 3px; padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 177px;'
									title='End Date' mandatory='yes' />
							</div>
						</div>
					</div>
				</fieldset>
				 
				<div class='toolbar' id="footer">
					 <input type='button' value='Go' class='imagebutton ok' id='btnOk' />
					 <input type='button' value='Cancel' class='imagebutton cancel' id='btnCancel' />
				</div>
			</div>
		</form>
	</div>
	
	<%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>
	
</body>
</html>