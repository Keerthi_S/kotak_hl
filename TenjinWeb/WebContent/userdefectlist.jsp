<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  userdefectlist.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


 
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.defect.DefectManagementInstance"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
   <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION

 
-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DTT Users</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/pages/userdefectlist.js'></script>
<script src="js/tables.js"></script>
</head>
<body>
<%Cache<String, Boolean> csrfTokenCache = null;
csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
session.setAttribute("csrfTokenCache", csrfTokenCache);
UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);  %>
	<div class='main_content'>
		<div class='title'>
		 
			<p>Defect Management Users</p>
			 
		</div>

		<form name='user_aut_cred_list_form' action='AutServlet' method='POST'>
			<div class='toolbar'>
			<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
				<input type='button' value='New' id='btnNewUser'
					class='imagebutton new' /> <input type='button' value='Remove'
					id='btnDelete' class='imagebutton delete' />
					
		
			</div>
			<div id='user-message'></div>

			<%
			TenjinSession tjnSession = (TenjinSession) request.getSession()
			.getAttribute("TJN_SESSION");
	User cUser = tjnSession.getUser();
			
			Map<String, Object> map = (Map<String, Object>) request
			.getSession().getAttribute("SCR_MAP");
				List<DefectManagementInstance> defectUsers = (ArrayList<DefectManagementInstance>)map.get("USER_DEFECT_MAPPINGS");
				String message = (String)map.get("MESSAGE");
			%>

			<input type=hidden value='<%=message%>' id='message' />
			<div class='form container_16'  style='margin:0 auto;'>
			
			
				<div id='dataGrid'> 
					<table id='userDefectTable' class='display' cellspacing='0'
						width='100%'>
						<thead>
							<tr>
								<th><input type='checkbox' id='chk_all' title='Select All'  class="tbl-select-all-rows" value=''></th>
								<th>User ID</th>
								 
								<th>Instance</th>
								<th>Instance User Id</th>
								
							</tr>
						</thead>
						<tbody>
							<%
								if (defectUsers.size() > 0) {
										for (DefectManagementInstance dm : defectUsers) {
							%>
							<tr>
								<td> <input type='checkbox' class='checkbox chkBox' id='chk_user'
									value='<%=Utilities.escapeXml(dm.getAdminId())%>:<%=Utilities.escapeXml(dm.getInstance())%>' data-userid='<%=Utilities.escapeXml(dm.getAdminId())%>:<%=Utilities.escapeXml(dm.getInstance())%>'/> 
									<td><a
									href='DefectServlet?param=fetch_defect_user&paramval=<%=Utilities.escapeXml(dm.getTjnUserId())%>&instance=<%=Utilities.escapeXml(dm.getInstance())%>'
									id='<%=dm.getInstance()%>' class='edit_record'><%=Utilities.escapeXml(dm.getTjnUserId())%></a></td>
									
								<td><%=Utilities.escapeXml(dm.getInstance())%></td>
							
								<td><%=Utilities.escapeXml(dm.getAdminId())%></td>
								
							</tr>
							<%
								}
									} else {
							%>
							 
							<%
								}
							%>
						</tbody>
					</table>
				 </div> 
				 
			</div>
		</form>
	</div>
	<div class='sub_content' style='height: 500px;'>
		<iframe class='ifr_Main' name='content_frame' id='content_frame'
			src=''></iframe>
	</div>
</body>
</html>