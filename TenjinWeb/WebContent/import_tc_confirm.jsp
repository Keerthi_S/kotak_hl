<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  import_tc_confirm.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->



<%@page import="org.slf4j.LoggerFactory"%>
<%@page import="org.slf4j.Logger"%>
<%@page import="com.ycs.tenjin.project.TestCase"%>
<%@page import="java.util.List"%>
<%@page import="com.ycs.tenjin.db.TestCasePaginator"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="java.text.SimpleDateFormat"%>

<%-- 
/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION 
 
 
 
 --%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Learner - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel="Stylesheet" href="css/summarypagetable.css" />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/tclist.js'></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<style>
			#selectedFiles
			{
				margin-left:0;
			}
			
			.att_block
			{
				padding:5px;
				border:1px solid #707a86;
				background-color:#b7c0ef;
				border-radius:5px;
				margin-left:0;
			}
			
			.att_block p
			{
				overflow:hidden;
				text-overflow:ellipsis;
			}
			
			#def_review_confirmation_dialog,#def_review_progress_dialog,#def_linkage_dialog
				{
					background-color:#fff;
					
				}
				
				.subframe_content
				{
					display:block;
					width:100%;
					margin:0 auto;
					min-height:120px;
					padding-bottom:20px;
				}
				.subframe_title
				{
					width:644px;
					height:20px;
					background:url('images/bg.gif');
					color:white;
					font-family:'Open Sans';
					padding-top:6px;
					padding-bottom:6px;
					padding-left:6px;
					font-weight:bold;
					margin-bottom:10px;
				}
				
				.subframe_actions
				{
					display:inline-block;
					width:100%;
					background-color:#ccc;
					padding-top:5px;
					padding-bottom:5px;
					font-size:1.2em;
					text-align:center;
				}
				
				.highlight-icon-holder
				{
					display:block;
					max-height:50px;
					margin-bottom:10px;
					text-align:center;
				}
				
				.highlight-icon-holder > img
				{
					height:50px;
				}
				
				.subframe_single_message
				{
					width:80%;
					text-align:center;
					font-weight:bold;
					display:block;
					margin:0 auto;
					font-size:1.3em;
				}
				
				#progressBar
				{
					width:475px;
					margin-top:20px;
					margin-left:auto;
					margin-right:auto;
				}
				
				p.subtitle
				{
					display:block;
					float:right;
					font-weight:bold;
					font-size:12px;
					line-height:22px;
					margin-right:10px;
					color:red;
				}
		</style>
</head>
<body>
     <%
        TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
         if(tjnSession!=null){
     %>

<div class="title">
<p>Import Test cases</p>
</div>

<div class="form container_8">

			<div class='toolbar'>
				
				<input type='button' id='btnCCancel' value='Cancel' class='imagebutton cancel'/> 
				<input type='button' id='btnExcel' value='From Excel' class='imagebutton template'/>
				<input type='button' id='btnTM' value='From TM' class='imagebutton tmImport'/>
			</div>
			 

		<div class='subframe' id='tm_review_progress_dialog' style='left: 20%'>
			<div class='subframe_title'>
				<p>Processing...</p> 
			
			<div class='highlight-icon-holder' id='processing_icon'></div> 
			<div class='subframe_content'>
				<div class='subframe_single_message' id='tm_review_message'>
					<p></p>
				</div>
			</div>
			<div class='subframe_actions' id='tm_review_pr_actions'
				style='display: none;'>
				<input type='button' id='btnPClose' value='Close'
					class='imagebutton cancel' />
			</div>
		
	</div>
	</div>
	</div>
	<%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>
	
</body>
</html>