<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  metadata_export.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


 


<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.ModuleBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
	<head>
		<title>AUT Functions - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel="SHORTCUT ICON" HREF="images/yethi.png">
		<link rel="stylesheet" type="text/css" href="css/progressbarskins/default/progressbar.css">
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		
		<script type="text/javascript" src="js/progressbar.js"></script>
		<script type='text/javascript' src='js/formvalidator.js'></script>
		<script type='text/javascript' src='js/pages/metadata_export.js'></script>
		<script type='text/javascript' src='js/pages/metadata_table.js'></script>
		<style>
			table, th, td {
				cellpadding:1px;
				cellspacing:1px;
			}
			th{
				background:#B4F114;
			}
			.active {
				background:red;
			}
		</style>
	</head>
	<body>
		
		<%
		    Cache<String, Boolean> csrfTokenCache = null;
		    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
		    session.setAttribute("csrfTokenCache", csrfTokenCache);
		    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
		    session.setAttribute ("csrftoken_session", csrftoken);
		%>
		
		<%
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			Project project = null;
			project = tjnSession.getProject();
			
			boolean canMakeChanges = false;
			
			
			if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){
				canMakeChanges = true;
			}
			
			Map<String, Object> result = (Map<String, Object>)request.getSession().getAttribute("METADATA_MAP");
			ArrayList<Aut> auts = null;
			String status= "";
			String message = "";
			if(result != null){
				status = (String)result.get("STATUS");
				if(status != null && status.equalsIgnoreCase("SUCCESS")){
					auts = (ArrayList<Aut>) result.get("auts");
				} else {
					message = (String) result.get("message");
				}
			}
			
		%>
		
		
		<div class='title'>
			<p>Metadata Export</p>
		</div>
		
		<div class='toolbar'>
			<input type='button' value='Back' id='btnBack' class='imagebutton back'/>
			<input type="submit" value='Export Metadata' id='export' class='imagebutton download'/>
			<input type='button' value='Export History' id='audit' class='imagebutton download'/>
		</div>
		<div id='user-message'>
		
		</div>
		<form action='AutServlet' name='aut_metadata_form' id='aut_metadata_form' method='POST'>
			<input type='hidden' id='requesttype' name='requesttype' value='learning_funcs'/>
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			
			<div class='form container_16'>
				<fieldset>
					<legend>Selection</legend>
						<div class='fieldSection grid_16'>
							
							<div class='grid_1'>
							<div class="clear"></div>
								<input type="radio" id='radio_application' value='application' name='selection'/>
							</div>
							<div class='grid_2'><label for='radio_application' style='text-align:right;'>Application</label></div>
							
							<div class='grid_1'>
							<div class="clear"></div>
							<input type='radio' id='radio_function' value='function' name='selection'/>
							</div>
							<div class='grid_2'><label for='radio_function' id='grouplabel' style='text-align:right;'>Function</label></div>
							
							<div class='function_mask'>
							<div class='grid_4' >
								<div class="clear"></div>
								<select id='lstApplication' name='lstApplication' class='stdTextBox'>
									<option value='-1'>-- Select Application --</option>
									<%
									if(auts != null){
										for(Aut aut:auts){
											%>
											<option value='<%=aut.getId() %>'><%=aut.getName() %></option>
											<%
											}
										}
									 %>
								</select>
						 	</div>
						 	</div>
						 	
						 	<div class='grid_2'>
						 	<label for='txtFileName'>Enter File Name</label></div>
							<div class='grid_1'>
							<input type='text' class='stdTextBox' id='txtFileName' name='txtFileName' mandatory='yes' title='File Name'/></div>
							
						</div>
				</fieldset>
			<div class='progress_mask'>
				<fieldset>
					<legend>Progress</legend>
					<div class='grid_16'>
						<div class='grid_16' style='margin-top:15px;'>
						<div class='lrnr_progress_bar grid_8' style='margin:0 auto;'>
							<div id="progressBar" class="default"><div></div></div>
						</div>
						<div class='clear'></div>
						
						<div class='grid_3' style='margin-top:15px;margin-right:1px;width:120px;'><label><b>Exporting</b></label></div>
						<div class='grid_8' style='margin-top:15px;'>
							<b>No. </b><input type='text' class='stdTextBox' disabled id='txtcounter' style='min-width:25px;width:25px;'/><b>&nbsp&nbspOf&nbsp&nbsp </b>
							<input type='text' class='stdTextBox' disabled id='txttotal' style='min-width:25px;width:25px;'/>
						</div>
					</div>
						
					</div>
				</fieldset>
			</div>
			<fieldset>
					<legend id='dynlegend'>Functions</legend>
					<div class='fieldSection grid_14' id='modulesInfo'>
					<div class='function_mask'>
					<div class='fieldSection grid_14' id='pagination_block'>
						<div style='display:table-cell;float:left;'>Show <select id='pages_max_rows'>
							<option value='10'>10</option>
							<option value='20'>20</option>
							<option value='50'>50</option>
							<option value='100'>100</option>
						</select> entries</div>
						<div style='display:table-cell;float:right;'>
							<a href='#' class='pagination' id='first' title='First Page'><<</a>
							<a href='#' class='pagination' id='prev' title='Previous Page'><</a>
							<a href='#' class='pagination' id='next' title='Next Page'>></a>
							<a href='#' class='pagination' id='last' title='Last Page'>>></a>
							&nbsp
							Page <input type='text' id='curpage'> <a href='#' class='pagination' id='gotopage'>Go</a> out of <span id='totpages'></span>
						</div>
					</div>
						<table id='tblMetadataModules' class='bordered' cellspacing='0' width='100%'>
							<thead>
								<tr>
									<th><input type='checkbox' class='tbl-select-all-modules tiny' id='chk_all_modules'/></th>
									<th>Function Code</th>
									<th>Function Name</th>
								</tr>
							</thead>
							<tbody id='tblMetadataModules_body'>
							</tbody>
						</table>
						</div>
						<div class='application_mask'>
							<table id='tblMetadataApp' class='bordered' cellspacing='0' width='100%'>
							<thead>
								<tr>
									<th><input type='checkbox' class='tbl-select-all-apps tiny' id='chk_all_apps'/></th>
									<th>Application Name</th>
								</tr>
							</thead>
							<tbody id='tblMetadataApp_body'>
								<%
									if(auts !=null){
										for (Aut aut : auts) {
											%>
											<tr>
												<td class='tiny'><input type='checkbox' name='appName' value='<%=aut.getId() %>' id='<%=aut.getId() %>' /></td>
												<td><%=aut.getName() %></td>
											</tr>
											<%
										}
									}
								%>
							</tbody>
						</table>
						</div>
					</div>
				</fieldset>
			</div>
			</form>
		<div class='modalmask'></div>
		<div class='subframe' style='left:20%'>
			<iframe frameborder="2" scrolling="no"  marginwidth="5" marginheight="5" style='height:400px;width:600px;' seamless="seamless" id='ttd-options-frame' src=''></iframe>
		</div>
	</body>
</html>