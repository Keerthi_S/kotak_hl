<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  license_details.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India





<!--
/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
*/
-->

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.Map"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
<head>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />

<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src="js/jquery.dataTables.js"></script>
		<script  type='text/javascript' src="js/tables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#renewed_license_info').hide();
	
});
$(document).on('click',	'#renewed_license',	function() {
	$(document).scrollTop($(document).height());
	$('#renewed_license_info').show();
});
$(document).on('click',	'#hide_info',	function() {
	$('#renewed_license_info').hide();
});
	$(document).on('click',	'#btnRefresh',	function() {
			window.location.href = 'LicenseServlet?param=view';
	});
	$(document).on('click',	'#btnrenew',	function() {
		window.location.href = 'LicenseServlet?param=renew';
	});
	$(document).on('click',	'#btnmodify',	function() {
		window.location.href = 'LicenseServlet?param=modify';
	});
</script>
</head>
<body>

	 <%
	    Cache<String, Boolean> csrfTokenCache = null;
	    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
	    session.setAttribute("csrfTokenCache", csrfTokenCache);
	    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
	    session.setAttribute ("csrftoken_session", csrftoken);
	 %>
	<div class='title'>
		<p>License Details</p>
	</div>

	<form Action='LicenseServlet?param=renewal' method='POST'>
		<div class='toolbar'>
		<input type="button" id='btnmodify' value='Modify' class='imagebutton save' /> 
			<input type="button" id='btnrenew' value='Renewal' class='imagebutton save' /> 
			<input type="button" id='btnRefresh' value='Refresh' class='imagebutton reset' />
		</div>
		<input type='hidden' id='systemId' name="systemId" value='${systemId}' />
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${screenState.message }' />
		<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<div id='user-message'></div>
		<div class='form container_16'>
			<fieldset>
				<c:choose>
					<c:when test="${renewedlicense ne null}">
						<legend>License Information (<a href="#renewed_license_info" id="renewed_license">Click here </a>to see Renewed license Info )</legend>
					</c:when>
					<c:otherwise>
						<legend>License Information</legend>
					</c:otherwise>
				</c:choose>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label>Company</label>
					</div>
					<div class='grid_4'>
						<input type='text' value='${license.customer.name}'
							class='stdTextBox' disabled="disabled" title='Company' />
					</div>
					<div class='clear'></div>
					<div class='grid_2'><label>Start Date</label></div>
					<div class='grid_4'>
						<input type='text' id='txtport'value='<fmt:formatDate value="${license.startDate}" pattern="dd-MMM-yyyy HH:mm:ss"/>'class='stdTextBox' disabled="disabled" title='Start Date' />
					</div>
					<div class='clear'></div>
					<div class='grid_2'><label>Grace Days</label></div>
					<div class='grid_4'>
						<input type='text' value='${license.gracePeriod}'class='stdTextBox' disabled="disabled" title='Grace period' />
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2' style="width: 102px;"><label>Relationship Since</label></div>
					<div class='grid_4'>
						<input type='text' id='txtport'value='<fmt:formatDate value="${license.relationshipSince}" pattern="dd-MMM-yyyy HH:mm:ss"/>'class='stdTextBox' disabled="disabled" title='Relationship Since' />
					</div>
					<div class='clear'></div>
					<div class='grid_2' style="width: 102px;"><label>End Date</label></div>
					<div class='grid_4'>
						<input type='text' class='stdTextBox' disabled="disabled" value='<fmt:formatDate value="${license.endDate}" pattern="dd-MMM-yyyy HH:mm:ss"/>'title='End Date' />
					</div>
				</div>
			</fieldset>

			<fieldset>
				<legend>Core License Information</legend>
							<div class='fieldSection grid_7'>

								<div class='clear'></div>
								<div class='grid_2'><label>Type</label></div>
								<div class='grid_4'>
									<input type='text' class='stdTextBox' disabled="disabled"value='${license.type.name}' title='Type' />
								</div>
							</div>
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label>Limit</label></div>
								<div class='grid_4'>
									<input type='text' class='stdTextBox' disabled="disabled" value='${license.coreLimit}' title='Limit' />
								</div>
							</div>
					</fieldset>

			<fieldset>
				<legend>Adapter License Information</legend>
				<div class='form container_16'>
					<table  class="display tjn-default-data-table">
						<thead>
							<tr>

								<th>Name</th>
								<th class="text-center">Limit</th>
								<th class="text-center">Start Date</th>
								<th class="text-center">End Date</th>

							</tr>
						</thead>
						<tbody>
							<c:forEach items="${licComponents}"
								var="component">
										<tr>
											<td>${component.productComponent.name}</td>
											<td class="text-center">${component.limit}</td>
											<td class="text-center"><fmt:formatDate  value="${component.startDate}" pattern="dd-MMM-yyyy HH:mm:ss" /></td>
											<td class="text-center"><fmt:formatDate value="${component.endDate}" pattern="dd-MMM-yyyy HH:mm:ss" /></td>
										</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
			
		<div id="renewed_license_info">
			<fieldset>
			<legend>Renewed License Information (<a id="hide_info" href="#">Hide</a>)</legend>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label>Start Date</label></div>
					<div class='grid_4'>
						<input type='text' id='txtport'value='<fmt:formatDate value="${renewedlicense.startDate}" pattern="dd-MMM-yyyy HH:mm:ss"/>'class='stdTextBox' disabled="disabled" title='Start Date' />
					</div>
					<div class='clear'></div>
					<div class='grid_2'><label>Grace Days</label></div>
					<div class='grid_4'>
						<input type='text' value='${renewedlicense.gracePeriod}'class='stdTextBox' disabled="disabled" title='Grace period' />
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2' ><label>End Date</label></div>
					<div class='grid_4'>
						<input type='text' class='stdTextBox' disabled="disabled" value='<fmt:formatDate value="${renewedlicense.endDate}" pattern="dd-MMM-yyyy HH:mm:ss"/>'title='End Date' />
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Core License Information</legend>
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label>Type</label></div>
								<div class='grid_4'>
									<input type='text' class='stdTextBox' disabled="disabled"value='${renewedlicense.type.name}' title='Type' />
								</div>
							</div>
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label>Limit</label></div>
								<div class='grid_4'>
									<input type='text' class='stdTextBox' disabled="disabled" value='${renewedlicense.coreLimit}' title='Limit' />
								</div>
							</div>
					</fieldset>
			<fieldset>
				<legend>Adapter License Information</legend>
				<div class='form container_16'>
					<table  class="display tjn-default-data-table">
						<thead>
							<tr>
								<th>Name</th>
								<th class="text-center">Limit</th>
								<th class="text-center">Start Date</th>
								<th class="text-center">End Date</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${renewedlicComponents}"
								var="component">
										<tr>
											<td>${component.productComponent.name}</td>
											<td class="text-center">${component.limit}</td>
											<td class="text-center"><fmt:formatDate  value="${component.startDate}" pattern="dd-MMM-yyyy HH:mm:ss" /></td>
											<td class="text-center"><fmt:formatDate value="${component.endDate}" pattern="dd-MMM-yyyy HH:mm:ss" /></td>
										</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
		</div>
	 </div>
 </form>

</body>
</html>