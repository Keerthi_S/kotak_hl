<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!-- Added by sahana for Req#TJN_24_03 on 14/06/2016 -->

<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.scheduler.Scheduler"%>
<%@page import="com.ycs.tenjin.scheduler.SchMapping"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.client.RegisteredClient"%>
<%@page import="java.util.Map"%>
<%@ page
	import="java.util.Date,java.text.SimpleDateFormat,java.text.ParseException"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@ page import="java.util.Random"%>
<%@page import="com.ycs.tenjin.scheduler.SchMapping"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
   18-12-2020			Pushpalatha				TENJINCG-1246
  
*/

-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Schedule Task</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel='stylesheet' href='css/bordered.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src="js/jquery-1.12.4.js"></script>
<script type='text/javascript' src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/pages/autfunclist_table.js'></script>


<script src="js/formvalidator.js"></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script type='text/javascript' src=' js/bootstrap.min.js'></script>
<script type='text/javascript' src='js/pages/schedule_new.js'></script>



<style>
.input-w label {
	display: inline-block;
	vertical-align: middle;
}

.input-w input {
	display: inline-block;
	vertical-align: middle;
}
.modal-open {
	overflow: hidden;
}

.modal {
	display: none;
	overflow: hidden;
	position: fixed;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	z-index: 1050;
	-webkit-overflow-scrolling: touch;
	outline: 0;
}

.modal.fade .modal-dialog {
	-webkit-transform: translate(0, -25%);
	-ms-transform: translate(0, -25%);
	-o-transform: translate(0, -25%);
	transform: translate(0, -25%);
	-webkit-transition: -webkit-transform 0.3s ease-out;
	-o-transition: -o-transform 0.3s ease-out;
	transition: transform 0.3s ease-out;
}

.modal.in .modal-dialog {
	-webkit-transform: translate(0, 0);
	-ms-transform: translate(0, 0);
	-o-transform: translate(0, 0);
	transform: translate(0, 0);
}

.modal-open .modal {
	overflow-x: hidden;
	overflow-y: auto;
}

.modal-dialog {
	position: relative;
	width: auto;
	margin: 10px;
}

.modal-content {
	position: relative;
	background-color: #ffffff;
	border: 1px solid #999999;
	border: 1px solid rgba(0, 0, 0, 0.2);
	border-radius: 6px;
	-webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
	box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
	-webkit-background-clip: padding-box;
	background-clip: padding-box;
	outline: 0;
}

.modal-backdrop {
	position: fixed;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	z-index: 1040;
	background-color: #000000;
}

.modal-backdrop.fade {
	opacity: 0;
	filter: alpha(opacity = 0);
}

.modal-backdrop.in {
	opacity: 0.5;
	filter: alpha(opacity = 50);
}

.modal-header {
	padding: 15px;
	border-bottom: 1px solid #e5e5e5;
	min-height: 16.42857143px;
}

.modal-header .close {
	margin-top: -2px;
}

.modal-title {
	margin: 0;
	line-height: 1.42857143;
}

.modal-body {
	position: relative;
	padding: 15px;
}

.modal-footer {
	padding: 15px;
	text-align: right;
	border-top: 1px solid #e5e5e5;
}

.modal-footer .btn+.btn {
	margin-left: 5px;
	margin-bottom: 0;
}

.modal-footer .btn-group .btn+.btn {
	margin-left: -1px;
}

.modal-footer .btn-block+.btn-block {
	margin-left: 0;
}

.modal-scrollbar-measure {
	position: absolute;
	top: -9999px;
	width: 50px;
	height: 50px;
	overflow: scroll;
}
#btnLogs {
	background-colour: blue;
}
</style>
</head>
<body>
	<% 
	Cache<String, Boolean> csrfTokenCache = null;
	csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
	session.setAttribute("csrfTokenCache", csrfTokenCache);
	UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE); 
	 session.setAttribute ("csrftoken_session", csrftoken);
	
		TenjinSession tjnSession = (TenjinSession) request.getSession()
				.getAttribute("TJN_SESSION");
		User cUser = tjnSession.getUser();
		Project project = null;
		project = tjnSession.getProject();

		Map<String, Object> map = (Map<String, Object>) request
				.getSession().getAttribute("SCR_MAP");

		String scrStatus = "";

		String status = (String) map.get("STATUS");
		String message = (String) map.get("MESSAGE");
		Scheduler sch = (Scheduler) map.get("SCH_BEAN");
		ArrayList<SchMapping> scheduleList = (ArrayList<SchMapping>) map
				.get("SCH_Map_List");

		ArrayList<RegisteredClient> clientList = null;
		clientList = (ArrayList<RegisteredClient>) map.get("CLIENT_LIST");
	
	
	%>
	<div class='main_content'>
		<div class='title'>

			<p>Schedule Task</p>
		 
		</div>
		<form name='new_schedule' action='SchedulerServlet' method='get'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
	<% 
			String projectState=project.getState();
	
		/* Modified by Pushpa for TJN262R2-42 starts */
			if(projectState.equalsIgnoreCase("A")){
			%>
			<div class='toolbar'>
				<input type='button' value='Save' id='btnSchSave'
					class='imagebutton save' />
				<input type='button' value='Recurrence' id='btnSchRecurrence'
					class='imagebutton recurrence' />
				<input type='hidden' id='projectState' value='<%=projectState%>'>
				<input type='button' value='Cancel' id='btnCancelTask' class='imagebutton cancel' />
					<input type="button" value="Logs" id="btnLogs" class="imagebutton list" > 
				 <input type='button' value='Refresh' id='btnRefreshAut' class='imagebutton refresh' />
					
				<div id="importLogs" class="modal fade">
					<div class="modal-dialog" style='height: 400px; width:600px;'>

						<div class="modal-content">
							<div class="modal-header">
								<p>Schedule Task Logs</p>
							</div>
							
							<div class="modal-body">
							<p id='errorMessage' ></p>
							</div>
							<div class="modal-footer">
								<input type="button" value='Close' id='btnclose' class="imagebutton cancel" data-dismiss="modal"> 
							</div>
						</div>

					</div>
				</div>
				<div class='grid_2' style='float: right; font-size: 0.7em;'>
					<label id='date'></label>
				</div>

			</div>
			
			<%}%>
				<div id='user-message'></div>
			
			<div class='form container_16'>
				<fieldset>

					<legend>Schedule Details</legend>
					<div class='fieldSection grid_16'
						style='margin-top: 5px; height: 28px'>
						<div class='grid_2'>
							<label for='taskName'>Task Name</label>
						</div>
						<div class='grid_13'>
                            <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<input type='text' id="taskName" class='stdTextBox long'
								maxlength="40" mandatory='yes' title='Task Name'
								style='width: 81.7%' placeholder="MaxLength 40"/>
                             <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						</div>
					</div>
					<div class='fieldSection grid_7'>


						<%
							if (project != null) {
						%>
						<input type='hidden' value='<%=project.getId()%>' id='projId'
							name='projId' />
						<%
							}
						%>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='datepicker'>Date</label>
						</div>
						<div class='grid_4'>
							<%
								String todaydate1 = "";

								Calendar calendar = Calendar.getInstance();
								SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
								todaydate1 = dateFormat1.format(calendar.getTime());
							%>
							  
								<input type='text' id="datepicker"  class='stdTextBox mid' value=<%=todaydate1 %>
								style='border: 1px solid #ccc; border-radius: 3px; padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 177px;'
								size="25" mandatory='yes' title='Schedule Date'/>
							 

						</div>

						<div class='clear'></div>
						<div class='grid_2'>
							<label for='btntime'>Time</label>
						</div>
						<div class='grid_4'>

							<select id='btntime' name='btntime' class='stdTextBox'
								style='min-width: 60px; width:60px'>

							</select> &nbsp;&nbsp;<font for='btntime' color='#00008B'>(HH)</font>&nbsp;&nbsp;&nbsp;&nbsp;
							<%
								String myTime1 = "00:00";
								SimpleDateFormat df1 = new SimpleDateFormat("mm");
								Date d1 = df1.parse(myTime1);
								Calendar cal1 = Calendar.getInstance();
								cal1.setTime(d1);
							%>
							<select id='btntime1' name='btntime1' class='stdTextBox'
								style='min-width: 60px; width:60px' mandatory='yes'>

							</select> &nbsp;&nbsp;<font for='btntime1' color='#00008B'>(MM)</font>
						</div>

						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtTask'>Task</label>
						</div>

						<div class='grid_4'>
							<input type='text' id='txtTask' name='txtTask' value='Execute'
								class='stdTextBox mid' disabled='disabled' mandatory='yes'
								title='Task' />
						</div>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='type'>Type</label>
						</div>
                          <div class='grid_4'>
							<select id='type' name='type' class='stdTextBoxNew' mandatory='yes'>
								<option value='0' selected>--Select One--</option>
								<option value='1'>Test Case</option>
								<option value='2'>Test Set</option>
							</select>
						</div>
						
						<div id="tset">		
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtTestSet' >Test Set</label>
							
						</div>

						<div class='grid_4'>
							<select id='txtTestSet' name='txtTestSet' 
								class='stdTextBoxNew'  title=" TestSet" mandatory='yes'></select>

						</div>
						</div>
						<div id="tCase">	
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtTestCase' >Test Case</label>
							
						</div>

						<div class='grid_4'>
							<select id='txtTestCase' name='txtTestCase' 
								class='stdTextBoxNew'  title=" TestCase" mandatory='yes'></select>

						</div>	
						</div>
						<div class="clear"></div>
				 
					</div>

					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div id="txtClient">
						<div class='grid_2'>
							<label for='txtclientregistered'>Client</label>
						</div>
						<div class='grid_4'>
							 
								<select id='txtclientregistered' name='txtclientregistered'
								type='select' class='stdTextBoxNew'
								title="Select Reg Client" mandatory='yes'></select>
						</div>
                     </div>


						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtCrtBy'>Created By</label>
						</div>
						<div class='grid_4'>
							<input type='text' id='txtCrtBy' name='txtCrtBy'
								class='stdTextBox mid' value='<%=cUser.getId()%>'
								disabled='disabled' title='Created By' />
						</div>

						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtCrtOn'>Created On</label>
						</div>
						<div class='grid_4'>
							<%
								String todaydate = "";

								Calendar calendar1 = Calendar.getInstance();
								SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
								todaydate = dateFormat.format(calendar1.getTime());
							%>
							<input type='text' value='<%=todaydate%>' id='txtCrtOn'
								name='txtCrtOn' title='Created On' mandatory='no'
								disabled='disabled' class='stdTextBox mid' />
						</div>

						<div class='clear'></div>

						<input type='hidden' value='Scheduled' id='txtStatus'
							name='txtStatus' />
							<div id="txtBrowser">
						<div class='grid_2'>
							<label for='lstBrowserType'>Browser</label>
						</div>
						<div class='grid_4'>
							<select id='lstBrowserType' name='lstBrowserType'
								class='stdTextBoxNew'>

								<option value='APPDEFAULT'>Use Application Default</option>

								<option value='<%=BrowserType.CHROME%>'>Google Chrome</option>
								<!-- Added by paneendra for Firefox browser starts  -->
								<option value="<%=BrowserType.FIREFOX%>"><%=BrowserType.FIREFOX%></option>
								<!-- Added by paneendra for Firefox browser ends  -->
								<option value='<%=BrowserType.IE%>'>Microsoft Internet
									Explorer</option>
								<!-- Modified by Prem for TENJINCG-1246 starts -->
								<%-- <option value='<%=BrowserType.FIREFOX%>'>Mozilla
									Firefox</option> --%>
									 <option value='<%=BrowserType.CE %>'><%=BrowserType.CE %></option>
							<!-- Modified by Prem for TENJINCG-1246 starts -->
							</select>
						</div>
                           </div>
                    <div class="clear"></div>
						<div class='grid_2'>
							<label for='screenShot'>Screenshot</label>
						</div>
						<input type='hidden' id='projectScreenShotOption' name='projectScreenShotOption' value='<%=project.getScreenshotoption()%>'>
						<div class='grid_4'>
							<select id='screenShot' name='screenShot' class='stdTextBoxNew'>
								<option value='0'>Never Capture</option>
								<option value='1' selected>Capture Only for Failures /
									Errors</option>
								<option value='2'>Capture For All Messages
								<option value='3'>Capture At the End of Each Page</option>
							</select>
						</div>
					</div>

				</fieldset>

			</div>

		</form>
		<div class='form container_16' id='tasktype'>
			<fieldset>
				<legend>Test Cases</legend>

				<div class='clear'></div>
				<div class='clear'></div>
				<div class='fieldSection grid_15' id='modulesInfo'>
					<!-- Modified by Preeti for V2.8-113 starts -->
					<table id='tblNaviflowModules' class='display tjn-default-data-table' cellspacing='0'
						width='100%'>
						<tbody id='tblNaviflowModules_body'></tbody>
					</table>
				</div>
			</fieldset>
		</div>
	</div>
	<div class='modalmask'></div>
	<div class='subframe' style='left: 40%'> 
		<iframe frameborder="2" scrolling="no" marginwidth="5"
			marginheight="5" style='height: 350px; width: 600px;'
			seamless="seamless" id='recur-sframe' src=''></iframe>
	</div>
</body>
</html>
