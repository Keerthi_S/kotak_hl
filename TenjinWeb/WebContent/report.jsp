<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  report.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--
/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
   
*/
-->
<head>
<title>Reports - Tenjin Intelligent Testing Engine</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel='stylesheet' href='css/bordered.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel='stylesheet' href='css/jquery-ui.css' />
<link rel="SHORTCUT ICON" HREF="images/yethi.png">
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/pages/autfunclist_table.js'></script>
<script type='text/javascript' src='js/pages/report.js'></script>
<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script type='text/javascript' src="js/jquery-1.12.4.js"></script>
<script type='text/javascript' src="js/jquery-ui.js"></script>
<script type='text/javascript' src='js/pages/report_table.js'></script>
<script type="text/javascript" src="js/jquery.tabletojson.js"></script>
<style>
#tblReportSummary th img {
	width: 16px;
	height: 16px;
}

#tblReportSummary th {
	padding: 3px;
}
</style>
</head>
<body>
	<%
		TenjinSession tjnSession = (TenjinSession) request.getSession()
				.getAttribute("TJN_SESSION");
		String param = request.getParameter("param");
		Map<String, Object> reportMap = (Map<String, Object>) request
				.getSession().getAttribute("REPORTS_MAP");
		User cUser = tjnSession.getUser();
		Project project = null;
		project = tjnSession.getProject();
		ArrayList<Aut> auts = null;
		ArrayList<String> userNames = null;
		String status = "";
		String message = "";
		boolean canMakeChanges = false;

		if ((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase(
				"Site Administrator"))
				|| project.hasAdminPrivileges(cUser.getId())) {
			canMakeChanges = true;
		}

		if (reportMap != null) {
			status = (String) reportMap.get("STATUS");
			if (status.equalsIgnoreCase("SUCCESS")) {
				if (param.equalsIgnoreCase("user")) {
					userNames = (ArrayList<String>) reportMap
							.get("userNames");
				} else if (param.equalsIgnoreCase("aut")) {
					auts = (ArrayList<Aut>) reportMap.get("auts");
				}
			} else {
				message = (String) reportMap.get("MESSAGE");
			}
		}
	%>
	 
	<div class='title'>
		<%
			if (param.equalsIgnoreCase("aut")) {
		%>
		<p>AUT Wise Reports</p>
		<%
			} else {
		%>
		<p>User Wise Reports</p>
		<%
			}
		%>
	 
	</div>
	<%
		String showError = "noerror";
		if (status != null && status.equalsIgnoreCase("error")) {
			showError = message;
		}
	%>
	<input type="hidden" id="message" value="<%=showError%>" name="message">
	<input type='hidden' id='projectId' value='<%=project.getId()%>' />
	<div class='toolbar'>
		<input type='button' value='Generate Report' id='gen_report'
			class='imagebutton download' />
	 
		<input type='button' value='Reset' id='btnReset'
			class='imagebutton reset' />
		<input type='button' value='Download' id='btnDownload'
			class='imagebutton download' />
	</div>
	<div id='user-message'></div>
	<div class='form container_16'>
		<fieldset>
			<legend>Report Criteria</legend>
			<div class='fieldSection grid_16'>
				<input type='hidden' id='reportType' value='<%=param%>' />
				<%
					if (param.equalsIgnoreCase("aut")) {
				%>
				<div class='grid_2'>
					<label for='lstApplication' id='applicationlabel'>Application</label>
				</div>
				<div class='grid_5'>
					<select id='lstApplication' name='lstApplication'
						class='stdTextBoxNew' tjn_name='A'>
						<option value='-1'>-- Select One --</option>
						<option value='0'>-- ALL --</option>
						<%
							if (auts != null) {
									for (Aut aut1 : auts) {
						%>
						<option value="<%=aut1.getId()%>"><%=aut1.getName()%></option>
						<%
							}
								}
						%>
					</select> &nbsp&nbsp
				</div>
				<div class='grid_2'>
					<label for='lstFunction' id='functionLabel'>Function</label>
				</div>
				<div class='grid_5'>
					<select id='lstFunction' name='lstFunction' class='stdTextBoxNew'
						tjn_name='F'>
						<option value='-1'>-- Select One --</option>
						<option value='0'>-- ALL --</option>
					</select> &nbsp&nbsp
				</div>
				<%
					} else {
				%>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='lstUser' id='userIdlabel'>User ID</label>
				</div>
				<div class='grid_5'>
					<select id='lstUser' name='lstUser' class='stdTextBoxNew'
						tjn_name='U'>
						<option value='-1'>-- Select One --</option>
						<option value='0'>-- ALL --</option>
						<%
							if (userNames != null) {
									for (String userName : userNames) {
						%>
						<option value='<%=userName%>'><%=userName%></option>
						<%
							}
								}
						%>
					</select> &nbsp&nbsp
				</div>
				 
				<%
					}
				%>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='fromDate' id='fromDateLabel'>From Date</label>
				</div>
				<div class='grid_5'>
				 
						<input type="text" id='fromDate' name='fromDate'
						style='border: 1px solid #ccc; border-radius: 3px; padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 177px;'
						tjn_name='FD' > &nbsp&nbsp
					 
				</div>
				<div class='grid_2'>
					<label for='toDate' id='toDatelabel'>To Date</label>
				</div>
				<div class='grid_5'>
				 
						<input type="text" id='toDate' name='toDate'
						style='border: 1px solid #ccc; border-radius: 3px; padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 177px;'
						tjn_name='TD'  /> &nbsp&nbsp
					 
				</div>
				<%
					if (param.equalsIgnoreCase("aut")) {
				%>
				 
				<%
					}
				%>
			</div>
		</fieldset>
	</div>
	<div class='form container_16' id='resultReport'>
		<fieldset>
			<legend>Results</legend>
			<div class='clear'></div>
			<div class='fieldSection grid_15' id='pagination_block'>
				<div style='display: table-cell; float: left;'>
					Show <select id='pages_max_rows'>
						<option value='10'>10</option>
						<option value='20'>20</option>
						<option value='50'>50</option>
						<option value='100'>100</option>
					</select> entries
				</div>
				<div style='display: table-cell; float: right;'>
					<a href='#' class='pagination' id='first' title='First Page'><<</a>
					<a href='#' class='pagination' id='prev' title='Previous Page'><</a>
					<a href='#' class='pagination' id='next' title='Next Page'>></a> <a
						href='#' class='pagination' id='last' title='Last Page'>>></a>
					&nbsp Page <input type='text' id='curpage'> <a href='#'
						class='pagination' id='gotopage'>Go</a> out of <span id='totpages'></span>
				</div>
			</div>
			<div class='fieldSection grid_16' id='reportSummary'></div>

			<input type='hidden' id='rawReportData' value='' />
		</fieldset>
		<div class='clear'></div>
		<fieldset id='def_logged_block' style='display: none;'>
			<legend>Defects Logged</legend>
			<div id='dataGrid'>
				<table class='bordered' style='width: 100%; margin-left: 9px;'>
					<thead>
						<tr>
							<th>ID</th>
							<th>Summary</th>
							<th>Application</th>
							<th>Function</th>
							<th>Severity</th>
							<th>Status</th>
							<th>Posted</th>
						</tr>
					</thead>
					<tbody id='defBody'>
					</tbody>
				</table>
			</div>
		</fieldset>
	</div>
</body>
</html>