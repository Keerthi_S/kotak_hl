<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  metadata_import.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


 


<%@page import="org.json.JSONObject"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.ModuleBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page import="org.json.JSONArray" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
	<head>
		<title>AUT Functions - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/accordion.css' />
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel="SHORTCUT ICON" HREF="images/yethi.png">
		<link rel="stylesheet" type="text/css" href="css/progressbarskins/default/progressbar.css">
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
		<script type='text/javascript' src='js/pages/metadata_import.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src="js/progressbar.js"></script>
		<script type='text/javascript' src='js/formvalidator.js'></script>
		<script type='text/javascript' src='js/pages/metadata_table.js'></script>
	</head>
	<body>
	
		 <%
		    Cache<String, Boolean> csrfTokenCache = null;
		    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
		    session.setAttribute("csrfTokenCache", csrfTokenCache);
		    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
		    session.setAttribute ("csrftoken_session", csrftoken);
		 %>
	
		 <%
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			Project project = null;
			project = tjnSession.getProject();
			
			boolean canMakeChanges = false;
			if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){
				canMakeChanges = true;
			}
			Map<String, Object> result = (Map<String, Object>)request.getSession().getAttribute("METADATA_MAP");
			
			String status= "";
			String message = "";
			String fileName = "";
			String appNodeName = "";
			JSONObject json = null;
			if(result != null){
				status = (String)result.get("status");
				fileName = (String)result.get("filename");
				message = (String)result.get("message");
				request.getSession().setAttribute("METADATA_MAP", result);
				if(status != null && status.equalsIgnoreCase("SUCCESS")){
					json = (JSONObject) result.get("json");
					appNodeName = (String)result.get("appNodeName");
				}
			}
		%>
		
		<div class='title'>Metadata Import</div>
		<form name='main_form' id='main_form' action='MetaDataUploadServlet' method='POST' enctype='multipart/form-data'>
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
				<input type='button' value='Back' id='btnBack' class='imagebutton back'/>
				<input type='button' value='Import Metadata' class='imagebutton upload' id='import'/>
				<input type='button' value='Import History' class='imagebutton upload' id='audit'/>
			</div>
			<%
			String showError="noerror";
			String showMsg = "nomsg";
			if(message != null && message != "" && status.equalsIgnoreCase("error")){
				showError = message;
			}else if(message != null && message != "" && status.equalsIgnoreCase("success")){
				showMsg = message;
			}
		%>
			<div id='user-message'></div>
			
			<input type="hidden" id='error-msg-text' value='<%=showError%>'>
			<input type="hidden" id='success-msg-text' value='<%=showMsg%>'>
			<div class='form container_16'>

				<fieldset id='uploadBlock'>
					<legend>Import Metadata</legend>
					<div class='fieldSection grid_14'>
						<div class='grid_3'><label for='txtFile'>Metadata File Path</label></div>
						<div class='grid_6'>
							<input type='file' id='txtFile' name='txtFile' class='stdTextBox' accept='text/xml' style='display: none;'/>
							<input type="button" class='imagebutton search' id='btnBrowse' value="Browse..." onclick="document.getElementById('txtFile').click();" />
							<input type="text" class='stdTextBox' value="<%=fileName%>" id='txtFileName' disabled="disabled"/>
						</div>
						<div class='grid_4'>
							
							<input type="button" value='Upload' id='btnUpload' class='imagebutton upload'/>
							<input type="button" value='Clear' id='btnClear' class='imagebutton delete'/>
						</div>
					</div>
				</fieldset>
			
			<fieldset class='progress_mask'>
					<legend>Progress</legend>
					<div class='grid_14'>
						<div class='grid_14' style='margin-top:15px;'>
						<div class='lrnr_progress_bar grid_8' style='margin:0 auto;'>
							<div id="progressBar" class="default" style='margin:0 auto;'><div></div></div>
						</div>
						<div class='clear'></div>
						
						<div class='grid_2' style='margin-top:15px;margin-right:1px;width:120px;'><label><b>Importing...</b></label></div>
						
						<div class='grid_3' style='margin-top:15px;'>
							<input type='text' class='stdTextBox' disabled id='txtcounter' style='min-width:25px;width:25px;'/><b>&nbsp&nbspOf&nbsp&nbsp </b>
							<input type='text' class='stdTextBox' disabled id='txttotal' style='min-width:25px;width:25px;'/>
						</div>
					</div>
						<div class='emptysmall'></div> 
                    </div>
                    
				</fieldset>

			<fieldset class='loader_mask'>
				<legend>Progress</legend>
				<div class='grid_14'>
					<div class='grid_14' style='margin-top: 15px;'>
						<div id='progressbar' style='display: solid; text-align: left'>
							<img src='images/inprogress.gif'
								alt='Task in Progress. Please wait' title='Please wait'
								id='imgLoader' /> 
						</div>
					</div>
				</div>
			</fieldset>

			<fieldset class='function_mask'>
					<legend>Application Information</legend>
					<div class='fieldSection grid_14'>
						<div class='clear'></div>
						<div class='grid_2'><label for='lstApplication'>Application</label></div>
						<div class='grid_4'>
						
							<select id='lstApplication' name='lstApplication' class='stdTextBox'>
									<option value='-1'>-- Select Application --</option>
									<%
									if(json != null){
											JSONArray auts = json.getJSONArray("auts");
											for(int i = 0; i < auts.length(); i++){
											JSONObject aut = (JSONObject) auts.get(i);
											JSONObject autJson = aut.getJSONObject("aut");
											%>
											<option value='<%=autJson.getString(appNodeName) %>'><%=autJson.getString(appNodeName) %></option>
											<%
											}
									}
									 %>
								</select>
						</div>
					</div>
				</fieldset>
				<fieldset class='function_mask'>
					<legend>Function Information</legend>
					<div class='clear'></div>
					<div class='fieldSection grid_14' id='pagination_block'>
						<div style='display:table-cell;float:left;'>Show <select id='pages_max_rows'>
							<option value='10'>10</option>
							<option value='20'>20</option>
							<option value='50'>50</option>
							<option value='100'>100</option>
						</select> entries</div>
						<div style='display:table-cell;float:right;'>
							<a href='#' class='pagination' id='first' title='First Page'><<</a>
							<a href='#' class='pagination' id='prev' title='Previous Page'><</a>
							<a href='#' class='pagination' id='next' title='Next Page'>></a>
							<a href='#' class='pagination' id='last' title='Last Page'>>></a>
							&nbsp
							Page <input type='text' id='curpage'> <a href='#' class='pagination' id='gotopage'>Go</a> out of <span id='totpages'></span>
						</div>
					</div>
					<div class='fieldSection grid_14' >
						<table id='tblMetadataModules' class='bordered' cellspacing='0' width='100%'>
							<thead>
								<tr>
									<th><input type='checkbox' class='tbl-select-all-modules tiny' id='chk_all_modules'/></th>
									<th>Function Code</th>
									<th>Function Name</th>
								</tr>
							</thead>
							<tbody id='tblMetadataModules_body'>
							</tbody>
						</table>
						</div>
				</fieldset>
			</div>
		</form>
		<div class='modalmask'></div>
		<div class='subframe' style='left:20%'>
			<iframe frameborder="2" name= 'frame2' scrolling="no"  marginwidth="5" marginheight="5" style='height:400px;width:600px;' seamless="seamless" id='ttd-options-frame' src=''></iframe>
		</div>
	</body>
</html>