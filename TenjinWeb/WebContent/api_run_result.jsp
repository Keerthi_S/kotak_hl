<!-- 

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: api_run_result.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright &copy 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 
 * 
 */
 -->
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.bridge.pojo.run.StepIterationResult"%>
<%@page import="com.ycs.tenjin.project.TestStep"%>
<%@page import="com.ycs.tenjin.util.HatsConstants"%>
<%@page import="com.ycs.tenjin.project.TestCase"%>
<%@page import="com.ycs.tenjin.project.TestSet"%>
<%@page import="com.ycs.tenjin.run.TestRun"%>
<%@page import="com.ycs.tenjin.run.ExecutionProgressView"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>API Execution Progress</title>
		
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		
		<link rel='stylesheet' href='css/rprogress.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel="Stylesheet" href="css/summarypagetable.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/list.css'/>
		
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/CircularLoader-v1.3.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/jquery.jqplot.min.js'></script>
		<script type='text/javascript' src='js/jqplot.pieRenderer.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/api_run_result.js'></script>
		
		
		
		<style>
			.current
			{
			    -webkit-animation-name: example; 
			    -webkit-animation-duration: 1s; 
			    animation-name: example;
			    animation-duration: 1s;
			    animation-iteration-count: 1000;
			    
			}
			
			@keyframes example {
			    
			    
			    from {background-color: #f79232; }
    			to {background-color: #f6c342; }
			}
			
			@-webkit-keyframes example {
			    
			    from {background-color: red;}
    			to {background-color: yellow;}
			}
			
			.txn-validations-view.Pass {
				font-weight:bold;
				color:green;
			}
			
			.txn-validations-view.Fail {
				font-weight:bold;
				color:red;
			}
			
			.txn-validations-view.Error {
				font-weight:bold;
				color:yellow;
			}
			
			.summtable tbody td
			{
				cursor:default;
			}
			
			
			td.fs_group_heading, td.txn
			{
				padding:5px;
			}
			
			td.fs_group_heading
			{
				font-size:1em;
			}
			
			td.txn-status-icon
			{
				text-align:center;
			}
			td.txn-status-icon > img
			{
				width:14px;
			}
			
		.subframe_title1{
		    width: auto;
			height: 20px;
			background: url('images/bg.gif');
			color: white;
			font-family: 'Open Sans';
			padding-top: 6px;
			padding-bottom: 6px; 
			padding-left: 6px;
			font-weight: bold;
		}
	.layout-value{
		text-overflow: ellipsis; 
		overflow: hidden; 
		white-space: nowrap; 
		max-width: 150px;
	}
		</style>
		
		<script>
			function closeModal(){
				$('.subframe').hide();
				$('.modalmask').hide();
				$('#val-res-sframe').attr('src','');
			}
		</script>
		
	</head>
	<body>
		<%
		Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
		TestRun run = (TestRun) map.get("RUN");
		TestSet testSet = run.getTestSet();
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		User cUser = tjnSession.getUser();
		Project project = null;
		project = tjnSession.getProject();
		String callBack = request.getParameter("callback");
		String tcRecId = request.getParameter("tcRecId");
		String status = (String) map.get("STATUS");
		String message = (String) map.get("MESSAGE");
		%>
		
		<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		
		<div class='title'>
			<p>API Execution Summary</p>
			<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		    <div id='prj-info'>
			<span style='font-weight: bold'>Domain: </span> <span><%=project.getDomain() %></span>
			<span>&nbsp&nbsp</span> <span style='font-weight: bold'>Project:
			</span> <span><%=project.getName() %></span>
			</div>
		</div>
		
		<div class='toolbar'>
			<input type='button' id='btnClose' value='Back' class='imagebutton back'/>
			<% if(run.getStatus().equalsIgnoreCase("not started") ){
			%>
				<input type='button' id='report-gen' value='Report' class='imagebutton pdfreport' disabled="disabled"/>
			<%
				} else {
			%>
				<input type='button' id='report-gen' value='Report' class='imagebutton pdfreport'/>
			<%} %>
			<input type='button' id='downloadMessages' value='Download All API Messages' class='imagebutton download'/>
			<input type='button'  value='Defects' class='imagebutton defect' id='btnDefect'/>
			
			<%
			int temp = run.getId();
			List<Integer> list = run.getAllParentRunIds();
			if(!run.getStatus().equalsIgnoreCase("Fail") && !run.getStatus().equalsIgnoreCase("Error") && !run.getStatus().equalsIgnoreCase("Aborted")||list.contains(temp)){
			%>
				<input type='button' value='Re-Run Test' class='imagebutton runtestset' id='btnRun' disabled="disabled" />	
			<%
			} else {
			%>
				<input type='button' value='Re-Run Test' class='imagebutton runtestset' id='btnRun' />
			<%
					}
			%>
			
			 <% if(run.getStatus().equalsIgnoreCase("not started") ){
				%>
			    <input type='button' value='Send Mail' class='imagebutton mail' id='btnSendMail'  disabled="disabled" />
			     <%
					} else {
				%>
				 <input type='button' value='Send Mail' class='imagebutton mail' id='btnSendMail'/>
				 <%} %>
			    
			<input type='button' value='Mail Log' class='imagebutton list' id='btnMailLog'  />
			<div style="float: right; padding-right: 80px">
				<%
					int currentId = run.getId();
					int pureRunId = run.getPureRunId();
					if (run.getReRunIds() != null && run.getReRunIds().size() > 0) {
						%>
				<span id='reRunMsg' style='font-size: 0.7em; font-weight: bold; margin-right: 10px;'>
				This run has associated runs</span>
				<%
				if (pureRunId == currentId) {
				%>
				<span> <b><%=pureRunId%></b></span>
				<%
				}
				else {
				%>
				<input type='hidden' id='tcRecId' name='tcRecId' value='<%=run.getTcRecId()%>' />
				<span><a href="ResultsServlet?param=run_result&run=<%=pureRunId%>&callback=<%=callBack%>"><%=pureRunId%></a></span>
				<%
					}
						for (int rId : run.getReRunIds()) {
							if (rId == currentId) {
				%>
				<span>/<b><%=rId%></b>
				</span>
				<%
					} else {
				%>
				<span>/<a href="PartialExecutorServlet?param=rerun_result&run=<%=rId%>&callback=<%=callBack%>"><%=rId%></a></span>
				<%
				 		}
				 	}
				}
				else{
				%>
					<span id='reRunMsg' style='font-size: 0.7em; font-weight: bold; margin-right: 10px;'>
					This run has no associated run(s)</span>
				<%} %>
			</div>
		</div>
		
		
		<div id='user-message'></div>
		
		<div class='form' style='width:1053px;'>
			<div class='twoplusone-col-field-section'>
				<div class='fs_title'>Run Information</div>
			</div>
			<div class='three-col-field-section'>
				<div class='fs_title'>Overall Status</div>
			</div>
			<div class='three-col-field-section'>
				<input type='hidden' id='runId' value='<%=run.getId() %>'/>
				<table class='layout'>
					<tr>
						<td class='layout-label'>Run ID</td>
						<td class='layout-value'><%=run.getId() %><input type='hidden' id='runId' value='<%=run.getId() %>' /></td>
					</tr>
					<tr>
						<td class='layout-label'>Started On</td>
						<%
							String df = TenjinConfiguration.getProperty("DATE_FORMAT");
							SimpleDateFormat sdf = new SimpleDateFormat(df);
							String sTime = sdf.format(run.getStartTimeStamp());
						%>
						<td class='layout-value'><%=sTime %></td>
					</tr>
					<tr>
						<td class='layout-label'>Elapsed Time</td>
						<td class='layout-value'><%=run.getElapsedTime() %></td>
					</tr>
					<tr>
						<td class='layout-label'>Domain</td>
						<td class='layout-value' title='<%=run.getDomainName() %>'><%=run.getDomainName() %></td>
					</tr>
					<tr>
						<td class='layout-label'>Status</td>
						<td class='layout-value'><%=run.getStatus() %></td>
					</tr>
					
				</table>
			</div>
			
			<div class='three-col-field-section' >
				<table class='layout'>
					<tr>
						<td class='layout-label'>Started By</td>
						<td class='layout-value'><%=run.getUser() %></td>
					</tr>
					<tr>
						<td class='layout-label'>Ended On</td>
						<%
							if(run.getEndTimeStamp() != null){
								String eTime = sdf.format(run.getEndTimeStamp());
							
								%>
						<td class='layout-value'><%=eTime %></td>
						<%
						}else{
								%>
						<td class='layout-value'><%=run.getEndTimeStamp()%></td>
						<%
							}
							%>
					</tr>
					<tr>
						<td class='layout-label'>Test Set</td>
						<td class='layout-value' title='<%=testSet.getName() %>'><%=testSet.getName() %></td>
					</tr>
					<tr>
						<td class='layout-label'>Project</td>
						<td class='layout-value' title='<%=run.getProjectName() %>'><%=run.getProjectName() %></td>
					</tr>
					<tr>
						<td class='layout-label'>Test Type</td>
						<td class='layout-value'>API</td>
					</tr>
				</table>
			</div>
			<div class='three-col-field-section' style='height:141px;text-align:center;border:1px solid #fff'>
				<input type='hidden' id='prjId' value='<%=project.getId() %>' />
				<input type='hidden' id='tSetRecId'value='<%=run.getTestSet().getId() %>' /> <input type='hidden' id='callback' value='<%=callBack %>' />
			    <input type='hidden' id='tcRecId' value='<%=tcRecId %>' />
			     <input type='hidden' id='testsetexecutionType' value='<%=testSet.getRecordType() %>' />
			  <input type='hidden' id='tcRecId1' value='<%=run.getTcRecId() %>' />
				<input type='hidden' id='passedTests' value='<%=run.getPassedTests() %>'/>
				<input type='hidden' id='failedTests' value='<%=run.getFailedTests() %>'/>
				<input type='hidden' id='erroredTests' value='<%=run.getErroredTests() %>'/>
				<input type='hidden' id='notExecutedTests' value='<%=run.getTotalTests() - run.getExecutedTests() %>'/>
				<div id='run-summary-chart'></div>
			</div>
			<div class='clear'></div>
			<div class='one-col-field-section' id='allTransactions' style='height:400px;overflow-y:auto;max-height:400px;'>
				<table class='summtable' style='width:100%;'>
					<thead>
							<tr>
								<th>Status</th>
								<th style='width:50px;'>Step ID</th>
								<th>Summary</th>
								<th>TDUID</th>
								<th>Elapsed Time</th>
								<th>Message</th>
								<th>Request</th>
								<th>Response</th>
								<th>Validations</th>
								<th>Run-time Values</th>
							</tr>
						</thead>
						<tbody>
				<%
				boolean headerAdded = false;
				
				for(TestCase testCase : testSet.getTests()) {
					%>
					<tr><td colspan='10' class='fs_group_heading'><%=testCase.getTcId() %> - <%=testCase.getTcName() %></td></tr>
						<%
						if(!headerAdded) {
							headerAdded = true;
							%>
							<%
						}
						%>
							<%
							for(TestStep step : testCase.getTcSteps()) {
								int iterationNo = 0;
								for(StepIterationResult iteration : step.getDetailedResults()) {
									iterationNo++;
									%>
									<tr data-iteration='<%=iterationNo %>' data-step-rec-id='<%=step.getRecordId() %>' data-step-id='<%=step.getId() %>' data-test-case-id='<%=testCase.getTcId() %>' data-tduid = '<%=iteration.getTduid() %>'>
										<td class='txn-status-icon'>
											<%
											String cellColor = "";
											String imgUrl = "";
											if(iteration.getResult().equalsIgnoreCase("S")){
												imgUrl = "success_20c20.png";
												cellColor = "green";
											}else if(iteration.getResult().equalsIgnoreCase("F")){
												imgUrl = "failure_20c20.png";
												cellColor = "red";
											}else if(iteration.getResult().equalsIgnoreCase("E")){
												imgUrl = "error_20c20.png";
												cellColor = "red";
											}
											%>
											<img src='images/<%=imgUrl %>' />
										</td>
										<td><%=step.getId() %></td>
										<td><%=step.getShortDescription() %></td>
										<td><%=iteration.getTduid() %></td>
										<td><%=iteration.getElapsedTime() %></td>
										<td class='txn-message' style='color:<%=cellColor %>;'><%=iteration.getMessage() %></td>
										<td class='txn-request'><a href='#'>Download</a></td>
										<td class='txn-response'><a href='#'>Download</a></td>
										<td class='txn-validations'>
										<%
										if(iteration.getValidationResults() != null && iteration.getValidationResults().size() > 0) {
											%>
											<a href='#' class='txn-validations-view <%=iteration.getValidationStatus() %>'>View</a>
											<%
										}else{
											%>
											N/A
											<%
										}
										%>
										</td>
										<td class='txn-runtimevalues'>
											<%
											if(iteration.getRuntimeFieldValues() != null && iteration.getRuntimeFieldValues().size() > 0) {
												%>
												<a href='#' class='txn-runtimevalues-view'>View</a>
												<%
											}else{
												%>
												N/A
												<%
											}
											
											%>
										</td>
									</tr>
									<%
								}
							}
							%>
					<%
				}
				%>
				</tbody>
					</table>
		           <input type='hidden' id='prjNotification' value='<%=project.getProjectMail().getPrjMailNotification() %>' />
		           <input type='hidden' id='runid' name='runid'
					value='<%=run.getId() %>' />
			</div>
		</div>
		<div class='modalmask'></div>
		<div class='subframe' style='left:3%'>
			<iframe frameborder="2" scrolling="no"  marginwidth="5" marginheight="5" style='height:500px;width:650px;' seamless="seamless" id='val-res-sframe' src=''></iframe>
		</div>
			
				<div class='subframe' id='sendMailManual' style='position:absolute;top:70px;left:150px; max-width:650px ;max-height:450px ;min-height:450px;overflow-y:auto; background:white'>
				<div class='subframe_title1' ><p>Please select project users...</p></div>
				<div class='toolbar'>
					<input type='button' value='Send' id='btnSend' class='imagebutton mail' />
					 <input	type='button' value='Close' id='btnCloseIt' class='imagebutton cancel' />
					 <img src='images/inprogress.gif' id='img_mail_loader' style='display: none;'/>
			</div>
			<div id='user-message-users'></div>
				<div class='subframe_content'> 
				<div id='dataGrid' style='width:95%;padding:3px;'>
					<table id='unmapped-users-table' class='display' cellspacing='0' width='95%'>
							<thead>
								<tr>
									<th><input type='checkbox' class='tbl-select-all-rows' id='chk_all_users' title='Select All'/></th>
									<th>User ID</th>
									<th>Full Name</th>
									<th>Email</th>
								</tr>
								
							</thead>
				
							<tbody>
							<% for (User user : project.getUsers()) {%>
							<tr>
							<td class='tiny'><input type='checkbox' class= 'checkbox' id='chk_user' value='<%=user.getId()%>' /></td>
							<td><%=user.getId()%></td>
							<td><%=user.getFullName() %></td>
							<td><%=user.getEmail() %></td>
							</tr><%}%>
							</tbody>
						</table>
				</div>
				</div>
			</div>
	</body>
</html>