<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  aut_api_information.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Api"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
  */

-->
	<head>
		<meta charset="ISO-8859-1">
		<title>AUT Details</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel="Stylesheet" href="css/bordered.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<style>
			#name{
   			 width:200px;
    
    		height: 2em; 
   			line-height: 1em; 
    		cursor: pointer;
   			 word-break: break-all;
    		height:auto;
    		white-space: normal;
			}
			#name{
   			 overflow: visible; 
    		}
		</style>
		<script>
		$(document).ready(function (){
				var type=$("#getType").val();
			var count=$("#count").val();
			if(type=="null")
				$('#swgrsCount').html(count);
			if(type=='swagger'){
				$('#tblWsdls').hide();
				$('#tblWadls').hide();
				$('#wsdl').hide();
				$('#wadl').hide();
				$('#swgrsCount').html(count);
			}else{
			refreshWSDLs();
			refreshXSDs();
			refreshWADLs();
			}
			
			$("#btnSave").click(function(){
				$('#main_form').submit();
			});
			$("#btnCancel").click(function(){
				var appId=$('#appId').val();
				window.location.href='ApplicationServlet?t=view&key='+appId;
			});
			$('#btnUploadWSDLs').click(function() {
				
				window.location.href='ApiUploadServlet?rtype=WSDL&app='+ $('#appId').val();
			});
			
			$('#btnUploadWADLs').click(function() {
				window.location.href='ApiUploadServlet?rtype=WADL&app='+ $('#appId').val();
			});
			
			$('#btnUploadXSDs').click(function() {
				$('.modalmask').show();
				$('#uploadApiResources').show();
				$('#uploadFrame').attr('src','ApiUploadServlet?rtype=XSD&app=' + $('#appId').val());
			});
			
			
			$('#btnUploadSwagger').click(function() {
				
				window.location.href='ApiUploadServlet?rtype=Swagger&app='+ $('#appId').val();
				
			});
			
			$(document).ready(function() {
				var selAuthType = $("#authType").data('selType');
				$("#authType").children('option').each(function() {
					if(selAuthType === $(this).attr('value')) {
						$(this).prop('selected',true);
					}
				});
			var val = $('#authType').val();
			if(val == 'oAuth2'){
				$('#btnOauth').show();
			}else{
				$('#btnOauth').hide();
			}
			
			$('#btnOauth').click(function() {
				window.location.href='ApplicationServlet?t=oauth_view&app='+ $('#appId').val();
			});
		});
		
		
		function refreshWSDLs (appId) {
			$('#tblWsdls tbody').html("<tr><td colspan='3'><img src='images/inprogress.gif' /> &nbsp; Loading. Please Wait... </td></tr>");
			var showFile=$('#showFile').val();
			$.ajax({
				url:'APIServlet?param=fetch_wsdls_for_aut&appId=' + $('#appId').val()+'&showFile='+showFile,
				dataType:'json',
				success : function(data) {
					if(data.status.toLowerCase() === 'success') {
						var html = '';
						var wsdlsAvailable = false;
					$('#wsdlCount').html(data.wsdls.length);
						for(var i=0; i<data.wsdls.length; i++) {
							wsdlsAvailable = true;
							var wsdl = data.wsdls[i];
							html += "<tr>";
							html += "<td><div id='name'>" + wsdl.name + "</div></td>";
							html += "<td>" + wsdl.lastModified + "</td>";
							html += "</tr>";
						}
						if(wsdlsAvailable === true) {
							$('#tblWsdls tbody').html(html);
						}else{
							$("#tblWsdls tbody").html("<tr><td colspan='3'>No WSDLs are available.</td></tr>");
						}
					}else {
						showMessage(data.message,'error');
						alert(errorThrown);
						$("#tblWsdls tbody").html("<tr><td colspan='3'>No WSDLs are available.</td></tr>");
					}
				},
				
				error : function(xhr, textStatus, errorThrown) {
					alert(errorThrown);
					showMessage('An internal error occurred. Please contact Tenjin Support.', 'error');
				}
			});
		}
		
		function closeModal(reloadInformation) {
			$('#uploadFrame').attr('src','');
			$('.modalmask').hide();
			$('#uploadApiResources').hide();
			
			
			if(reloadInformation === true) {
				refreshWSDLs();
				refreshXSDs();
				refreshWADLs();
			}
		}
		function refreshWADLs (appId) {
			$('#tblWadls tbody').html("<tr><td colspan='3'><img src='images/inprogress.gif' /> &nbsp; Loading. Please Wait... </td></tr>");
			var showFile=$('#showFile').val();
			$.ajax({
				url:'APIServlet?param=fetch_wadls_for_aut&appId=' + $('#appId').val()+'&showFile='+showFile,
				dataType:'json',
				success : function(data) {
					if(data.status.toLowerCase() === 'success') {
						var html = '';
						var wadlsAvailable = false;
						$('#wadlCount').html(data.wadls.length);
						for(var i=0; i<data.wadls.length; i++) {
							wadlsAvailable = true;
							var wadl = data.wadls[i];
							html += "<tr>";
							html += "<td><div id='name'>" + wadl.name + "</div></td>";
							html += "<td>" + wadl.lastModified + "</td>";
							html += "</tr>";
						}
						if(wadlsAvailable === true) {
							$('#tblWadls tbody').html(html);
						}else{
							$("#tblWadls tbody").html("<tr><td colspan='3'>No WADLs are available.</td></tr>");
						}
					}else {
						showMessage(data.message,'error');
						alert(errorThrown);
						$("#tblWadls tbody").html("<tr><td colspan='3'>No WADLs are available.</td></tr>");
					}
				},
				
				error : function(xhr, textStatus, errorThrown) {
					alert(errorThrown);
					showMessage('An internal error occurred. Please contact Tenjin Support.', 'error');
				}
			});
		}
		
		function refreshXSDs (appId)  {
			$('#tblXsds tbody').html("<tr><td colspan='3'><img src='images/inprogress.gif' /> &nbsp; Loading. Please Wait... </td></tr>");
			$.ajax({
				url:'APIServlet?param=fetch_xsds_for_aut&appId=' + $('#appId').val(),
				dataType:'json',
				success : function(data) {
					if(data.status.toLowerCase() === 'success') {
						var html = '';
						var xsdsAvailable = false;
						for(var i=0; i<data.wsdls.length; i++) {
							xsdsAvailable = true;
							var xsd = data.wsdls[i];
							html += "<tr>";
							html += "<td>" + xsd.name + "</td>";
							html += "<td>" + xsd.lastModified + "</td>";
							html += "</tr>";
						}
						if(xsdsAvailable === true) {
							$('#tblXsds tbody').html(html);
						}else{
							$("#tblXsds tbody").html("<tr><td colspan='3'>No XSDs are available.</td></tr>");
						}
					}else {
						showMessage(data.message,'error');
						$("#tblXsds tbody").html("<tr><td colspan='3'>No XSDs are available.</td></tr>");
					}
				},
				
				error : function(xhr, textStatus, errorThrown) {
					alert(errorThrown);
					showMessage('An internal error occurred. Please contact Tenjin Support.', 'error');
				}
			});
		}
		});
	
		</script>
	</head>
	<body>
		<%
	    Cache<String, Boolean> csrfTokenCache = null;
	    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
	    session.setAttribute("csrfTokenCache", csrfTokenCache);
	    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
	    session.setAttribute ("csrftoken_session", csrftoken);
		Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("AUT_API_INFO_MAP");
		List<Api> apis=(List<Api>)request.getAttribute("API_LIST");
		String getType=(String)request.getAttribute("Type");
		int count=0;
		
		Aut aut = (Aut) map.get("AUT");
		String appId = "0";
		if(aut != null) {
			appId = Integer.toString(aut.getId());
		}
		
		String autBase = (String) map.get("AUT_BASE");
		
		String status = (String) map.get("STATUS");
		String message = (String) map.get("MESSAGE");
		String showFile = (String) map.get("showFile");
		%>
	
		<div class='title'>
			API Information
		</div>
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${screenState.message }' />
		<input type='hidden' id='showFile' value='<%=showFile %>' />
		<div class='toolbar'>
		    <input type='button' value='Back' class='imagebutton back' id='btnCancel'/>
			<input type='button' id='btnSave' value='Save' class='imagebutton save'/>
			<input type='button' id='btnUploadWSDLs' value='Upload WSDLs' class='imagebutton upload'/>
			<input type='button' id='btnUploadWADLs' value='Upload WADL' class='imagebutton upload'/>
		<input type='button' id='btnUploadSwagger' value='Import From Swagger' class='imagebutton upload'/>
		<input type='button' id='btnOauth' value='OAuth 2.0 Details' class='imagebutton new'/>
		</div>
			<%
			String s = "";
			if(status != null && status.equalsIgnoreCase("SUCCESS")){
				%>
				<%if(message!=null && !message.equalsIgnoreCase("")){ %>
				<div id='user-message' class='msg-success' style='display:block;'>
					<div class="msg-icon">&nbsp;</div>
					<div class="msg-text"><%=message %></div>
					
				</div>
				<%
				}
				s = message;
			}else if(status != null && !status.equalsIgnoreCase("SUCCESS")){
				if(message!=null && !message.equalsIgnoreCase("")){
				%>
				
				<div id='user-message' class='msg-err' style='display:block;'>
					<div class="msg-icon">&nbsp;</div>
					<div class="msg-text"><%=message %></div>
				</div>
				
				<%
				}
				s = message;
			}else{
				%>
				<div id='user-message'></div>
				
				<%
				s = "";
			}
			%>
		<form action='APIServlet' id='main_form' method='POST'>
			<input type='hidden' id='requesttype' name='requesttype' value='update_aut_api_info'/>
			<input type='hidden' id='getType' name='getType' value='<%=Utilities.escapeHtml(getType)%>'/>
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='form container_17'>
				<input type='hidden' id='appId' name='app' value='<%=Utilities.escapeHtml(appId) %>'/>
				<fieldset>
					<legend>API Resources Base</legend>
					<div class='fieldSection grid_15'>
						<div class='grid_4'><input style='text-align:right;' type='text' id='autBase' name='autBase' class='stdTextBox' readonly value='<%=Utilities.escapeHtml(autBase) %>'/></div>
						<div class='grid_4'><input type='text' id='apiBase' name='apiBasePath' value='<%=Utilities.escapeHtml(aut.getApiBaseUrl()) %>' class='stdTextBox' placeholder='API Base'/></div>
					</div>
				</fieldset>
				
			
			
				<fieldset>
					<legend>Authorization</legend>
					<div class='fieldSection grid_15'>
						<div class='grid_4'><label for='authType'>Authentication Type</label></div>
						<div class='grid_10'>
							<select id='authType' name='authType' class='stdTextBox' data-sel-type='<%= map.get("API_AUTH_TYPE")%>'>
								<option value='NOAUTH'>No Authentication</option>
								<option value='Basic' Selected>Basic Authentication</option>
								<option value='oAuth2'>OAuth 2.0</option>
							</select>
						</div>
					</div>
				</fieldset>
				
				<div class='grid_7' id='wsdl'>
					<fieldset>
						<legend>WSDLs (<span id='wsdlCount'></span>)</legend>
						
						<table class='bordered' id='tblWsdls'>
							<thead>
								<tr>
									<th>WSDL Name</th>
									<th>Last Modified</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						
					</fieldset>
				</div>
				
				<div class='grid_7' id='wadl'>
					<fieldset>
						<legend>WADLs (<span id='wadlCount'></span>)</legend>
						<table class='bordered' id='tblWadls'>
							<thead>
								<tr>
									<th>WADLs Name</th>
									<th>Last Modified</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</fieldset>
				</div>
			<div class='grid_7'>
					<fieldset>
						<legend>SWAGGER APIs(<span id='swgrsCount'></span>)</legend>
						<table class='bordered' id='tblSwgrs'>
							<thead>
								<tr>
									<th>API Code</th>
									<th>API Name</th>
								</tr>
								
								<%
								if(apis!=null){
								for(Api api:apis){ %>
								<tr><td><%=Utilities.escapeHtml(api.getCode()) %></td>
								<td><%=Utilities.escapeHtml(api.getName()) %></td>
								</tr>
								<%count++;}
								}else{%>
								<tr><td colspan='3'>No Swagger APIs are available</td></tr>
								<%} %>
							</thead>
							<tbody>
							</tbody>
						</table>
					</fieldset>
				</div>
			
			
			</div>
			<input type='hidden' id='count' name='count' value='<%=count%>'/>
		</form>
		
		<div class='subframe' id='uploadApiResources' style='position:absolute;top:100px;left:250px;'>
			 <iframe src='' scrolling="no" seamless="seamless" id='uploadFrame' style='width:600px;height:350px;'></iframe> 
		</div>
		
	</body>
</html>