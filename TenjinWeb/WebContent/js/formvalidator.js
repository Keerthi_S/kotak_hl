/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  formvalidator.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 3-Nov-2016				Sriram					Defect#673
* 08-Dec-2017               Padmavathi              TCGST-18
*/

function validateForm(formName)
{
	//alert('entered validateForm');
	var showError = false;
	var fieldTitle = '';
	for(var i=0;i<document[formName].elements.length;i++){
		//Check for Textbox
		if(document[formName].elements[i].type=='text'){
			//If no value, then show error
			/*Modified by padmavathi for TCGST-18 starts*/
		/*	if(document[formName].elements[i].getAttribute("mandatory") == 'yes' && document[formName].elements[i].value)==''){*/
			if(document[formName].elements[i].getAttribute("mandatory") == 'yes' && (document[formName].elements[i].value).trim()==''){
				fieldTitle = document[formName].elements[i].getAttribute("title");
				showError = true;
				break;
			}
		}
		//Fix for Defect#673 - Sriram 
		if(document[formName].elements[i].type=='file'){
			if(document[formName].elements[i].getAttribute("mandatory") == 'yes' && document[formName].elements[i].value==''){
				fieldTitle = document[formName].elements[i].getAttribute("title");
				showError = true;
				break;
			}
		}//Fix for Defect#673 - Sriram  ends
		
		if(document[formName].elements[i].type=='password'){
			//If no value, then show error
			if(document[formName].elements[i].getAttribute("mandatory") == 'yes' && document[formName].elements[i].value==''){
				fieldTitle = document[formName].elements[i].getAttribute("title");
				showError = true;
				break;
			}
		}
		
		/***************
		 * Fix for Defect ID 30 in Tenjinv2.2
		 * To include Select option also under form validation
		 * Fix by Mahesh
		 */
		
		if(document[formName].elements[i].tagName=='SELECT'){
			//If no value, then show error
			if(document[formName].elements[i].getAttribute("mandatory") == 'yes' && document[formName].elements[i].value=='-1'){
				fieldTitle = document[formName].elements[i].getAttribute("title");
				showError = true;
				break;
			}
		}
		
		/***************
		 * Fix for Defect ID 30 in Tenjinv2.2
		 * To include Select option also under form validation
		 * Fix by Mahesh
		 */
		
		if(document[formName].elements[i].type=='textarea'){
			//If no value, then show error
			/*if(document[formName].elements[i].getAttribute("mandatory") == 'yes' && document[formName].elements[i].value)==''){*/
			if(document[formName].elements[i].getAttribute("mandatory") == 'yes' &&( document[formName].elements[i].value).trim()==''){
				fieldTitle = document[formName].elements[i].getAttribute("title");
				showError = true;
				break;
			}
		}
	}
	/*Modified by padmavathi for TCGST-18 ends*/
	if(showError == true){
		//alert(fieldTitle + ' is required');
		return fieldTitle + ' is required';
		
	}else{
		return 'SUCCESS';
	}
}