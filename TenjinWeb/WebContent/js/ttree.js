/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ttree.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 15-10-2018		   Pushpalatha			   TENJINCG-845
* 17-10-2018           Leelaprasad P           TENJINCG-827
*/

$.tree = (function(type){
	var method = {};
	var treeData = "";
	var $tree = $("<ul class='ttree'/>");
	var treeEntity = type;
	
	_getData = function(type){
		$.ajax({
			url:'TreeServlet',
			headers:{"type":treeEntity},
			data:'param='+treeEntity,
			async:false,
			dataType:'json',
			success:function(data){
				treeData = data;
			},
			error:function(xhr, errorThrown, textStatus){
				alert(errorThrown);
			}
		});
		
		/*$.ajax({
			url : "TreeServlet",
			data : "type=static_tree",
			async: false,
			success : function (data) {
				treeData = data;
		  },
		  error: function(xhr, textStatus,errorThrown){
			  alert(errorThrown);
		  },
		  dataType : "json"
		});*/
	};
	
	
	_prepareTree = function(node,$holder){
		var flSize = node.length;
		$(node).each(function(ix, vl) {
			var $nd = $("<li />");
			//$nd.attr({"id": vl.id, "entity": vl.entity});
			if(vl.child != null){
				var cls = 'ttree-branch ttree-br-closed';
				if (ix+1 == flSize) {
					cls = cls + ' ttree-end';
				}
				$nd.addClass(cls);
				$nd.append("<i/>");
				var $ct = $("<a/>").append($("<i/>"), vl.label);
				$ct.attr({"id": vl.id, "entity": vl.entity});
				$ct.addClass('ttree-' + vl.entity);
				$nd.append($ct);
				$holder.append($nd);
				var $subTree = $("<ul/>");
				$nd.append($subTree);
				_prepareTree(vl.child,$subTree);
			}else{
				var cls = 'ttree-node';
				if (ix+1 == flSize) {
					cls = cls + ' ttree-end';
				}
				$nd.addClass(cls);
				$nd.append("<i/>");
				var $ct = $("<a/>").append($("<i/>"), vl.label);
				$ct.attr({"id": vl.id, "entity": vl.entity});
				$ct.addClass('ttree-' + vl.entity);
				$nd.append($ct);
				$holder.append($nd);
			}
		});
	};
	
	_findAndOpenParents = function($nd){
		var $parentLi = $nd.parents("li.ttree-branch");

		if($parentLi != undefined && $parentLi != null && $parentLi.hasClass('ttree-br-opened')){
			$parentLi.children("i").trigger('click');
		}
		
		if($parentLi != undefined && $parentLi != null && $parentLi.length > 0){
			_findAndOpenParents($parentLi);
		}
		
		
	};
	
	
	method.getTree = function(){
		_getData(treeEntity);
		_prepareTree(treeData.tree,$tree);
		return $tree;
	};
	
	method.selectNode = function(treeLocator,id){
		var $targetLink = $(treeLocator + ' #' + id);
		
		//Find all its parents and open them one by one
		
		//Click the target
		var $targetLiItem = $targetLink.parents("li");
		_findAndOpenParents($targetLiItem);
		/*03-Nov-2014 Tree Refresh Changes: Starts*/
		$('.ttree-selected').removeClass('ttree-selected');
		$targetLink.addClass('ttree-selected');
		$targetLink.focus();
		if(treeLocator=='#domains_tree')
		    $targetLink.trigger('click');
		/*03-Nov-2014 Tree Refresh Changes: Ends*/
		
	};
	
	method.getRootNode = function(treeLocator){
		var $rootNode = $(treeLocator + ' .ttree-branch');
		var $link = $rootNode.children('a');
		return $link;
	}
	
	method.getSelectedNode = function(treeLocator){
		var $targetLink = $(treeLocator + ' .ttree-selected');
		/*var selNodeId = $targetLink.attr('id');
		alert(selNodeId);*/
		
		return $targetLink;
	};
	
	
	
	
	method.attachEvents = function(){
		/*03-Nov-2014 Tree Refresh Changes: Starts*/
		//$(".ttree-branch > i").click(function() {
		$(document).on("click", ".ttree-branch > i", function() {
		/*03-Nov-2014 Tree Refresh Changes: Ends*/
			//alert('entered onclick');
			$(this).parent().toggleClass("ttree-br-closed").toggleClass("ttree-br-opened");
			$(this).siblings("ul").toggle();
		});
		
		/*03-Nov-2014 Tree Refresh Changes: Starts*/
		//$('.ttree li a').click(function(){
		$(document).on("click", ".ttree li a", function() {
		/*03-Nov-2014 Tree Refresh Changes: Ends*/
			var id = $(this).attr('id');
			$('.ttree-selected').removeClass('ttree-selected');
			$(this).addClass('ttree-selected');
			var entity = $(this).attr('entity');
//			alert('You have selected a ' + entity + ' with ID ' + id);
			
			if(entity == 'domain'){
				/*Added by Pushpalatah for TENJINCG-845 starts*/
				$('.ifr_Main').attr('src','DomainServletNew?param=domain_view&paramval=' + id);
				/*Added by Pushpalatha for TENJINCG-845 ends*/
				$('#t_addProject').prop('disabled',false);
			}
			/*else if(entity == 'project'){*/
			/*changed by anudeep on 04092015 for to show active deactivate projects*/
			
			else if(entity == 'project' || entity == 'inactiveproject'){
				/*changed by Leelaprasad for TENJINCG-827 starts*/
				/*$('.ifr_Main').attr('src','ProjectServlet?param=project_view&paramval=' + id + '&v=ADMIN');*/
				$('.ifr_Main').attr('src','ProjectServletNew?t=load_project&paramval=' + id + '&view_type=ADMIN');
				/*changed by Leelaprasad for TENJINCG-827 starts*/
				//$('.ifr_Main')[0].contentWindow.setViewSource('admin');
			}
			/*changed by anudeep on 04092015 for to show active deactivate projects*/
			
			else if(entity == 'project_folder'){
				var arr = id.split('_');
				var folderType = arr[arr.length-1];
				
				var $parentBranch = $(this).parents(".ttree-branch");
				var $anchorInParent = $parentBranch.children("a");
				var domainId = $anchorInParent.attr('id');
				/*Added by Pushpalatha for TENJINCG-845 starts*/
				$('.ifr_Main').attr('src','DomainServletNew?param=domain_view&paramval=' + domainId + '&type=' + folderType);
				/*Added by Pushpalatha for TENJINCG-845 ends*/
			}
			else if(entity == 'testset'){
				//alert('clicked testset');
				var arr = id.split('_');
				var tsId = arr[1];
				$('.ifr_Main').attr('src','TestSetServlet?param=ts_view&paramval=' + tsId);
			}
			else if(entity == 'testcase'){
				//alert('clicked testset');
				var arr = id.split('_');
				var tcId = arr[1];
				$('.ifr_Main').attr('src','TestCaseServlet?param=tc_view&paramval=' + tcId);
			}
		});
		
		
	};
	
	/*03-Nov-2014 Tree Refresh Changes: Starts*/
	_detachEvents = function(){
		$(document).off("click", ".ttree-branch > i");
		$(document).off("click", ".ttree li a");		
	};
	
	method.destroy = function(){
		_detachEvents();
		$tree.remove();
	};
	/*03-Nov-2014 Tree Refresh Changes: Ends*/
	
	return method;
});