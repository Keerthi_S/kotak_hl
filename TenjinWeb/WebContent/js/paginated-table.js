/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  paginated_table.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 19-Oct-2017		Sriram Sridharan		Newly added for TENJINCG-396
 * 
 */
var tableId = '';
(function ($) {
	var opts;
	
	$.fn.paginatedTable = function() {
		if(!$(this).hasClass('paginated-table')) {
			$(this).addClass('paginated-table');
		}
		tableId = $(this).attr('id');
		//var $parentDiv = $(this).parent();
		var $showDiv = $("<div style='display:table-cell;float:left;margin-bottom:10px;'>Show </div>");
		$showSelect = $(" <select id='pages_max_rows' />");
		$showSelect.append("<option value='10'>10</option>");
		$showSelect.append("<option value='25'>25</option>");
		$showSelect.append("<option value='50'>50</option>");
		$showSelect.append("<option value='100'>100</option>");
		/*$parentDiv.append($showSelect);
		$parentDiv.append(" entries");*/
		$showDiv.append($showSelect);
		$showDiv.append(" entries");
		var $searchText = $("<span style='margin-left:20px;'>Search</span>");
		/*$searchText.insertBefore($(this));*/
		$showDiv.append($searchText);
		$showDiv.append($("<input type='text' class='stdTextBox paginated-table-search' id='"+ tableId + "_search' style='margin-left:20px;'/>"));
		$showDiv.insertBefore($(this));
		
		/*var $searchDiv = $("<div style='display:table-cell;float-left;'>Search</div>");
		$searchDiv.append($("<input type='text' class='stdTextBox paginated-table-search' id='"+ tableId + "_search'/>"));
		$searchDiv.insertBefore($(this));*/
		
		
		
		var $paginateDiv = $("<div style='display:table-cell;float:right;margin-bottom:10px;' />");
		$paginateDiv.append($("<a href='#' class='pagination' id='first' title='First Page'><<</a>"));
		$paginateDiv.append($("<a href='#' class='pagination' id='prev' title='Previous Page'><</a>"));
		$paginateDiv.append($("<a href='#' class='pagination' id='next' title='Next Page'>></a>"));
		$paginateDiv.append($("<a href='#' class='pagination' id='last' title='Last Page'>>></a>"));
		$paginateDiv.append("&nbsp;");
		$paginateDiv.append($("<span>Page </span>"));
		$paginateDiv.append($("<input type='text' id='curpage' style='text-align:center;'>"));
		$paginateDiv.append($("<a href='#' class='pagination' id='gotopage' style='margin-right:3px;'>Go</a>"));
		$paginateDiv.append($("<span> out of </span>"));
		$paginateDiv.append($("<span id='totpages'></span>"));
		
		$paginateDiv.insertBefore($(this));
		
		$('#curpage').val('1');
		
		
		paginate();
		
		$('.pagination').click(function() {
			var ms = $('#totpages').text();
			var maxPages = parseInt(ms, 10);
			var curPage = $('#curpage').val();
			curPage = parseInt(curPage,0);
			
			var id=$(this).attr('id');
			
			if(id == 'next'){
				if(curPage < maxPages){
				curPage = curPage + 1;
				}else{
					curPage = maxPages;
				}
			}else if(id == 'prev'){
				if(curPage > 1){
					curPage = curPage -1;
				}else{
					curPage=1;
				}
			}else if(id == 'first'){
				curPage = 1;
			}else if(id == 'last'){
				var rowsToShow = $('#pages_max_rows').val();
				var rowsShown = parseInt(rowsToShow, 10);
				var rowsTotal = $('#'+ tableId +' tbody tr').length;
				var numPages = rowsTotal/rowsShown;
				var numPages2 = numPages.toFixed();
				if(numPages2 < numPages){
					numPages = parseInt(numPages2,10)+1;
				}else{
					numPages = numPages2;
				}
				curPage = numPages;
			}
			
			
			gotoPage(curPage);	
		});
		
		$('#pages_max_rows').change(function(){
			paginate();
		});
		
		$('#' + tableId + '_search').keyup(function() {
			var searchinput=$(this).val();
			searchTable(searchinput);
		});
		
	};
	
	$.fn.greenify = function() {
	    this.css( "color", "green" );
	};
}( jQuery ));

function paginate(){
	var rowsToShow = $('#pages_max_rows').val();
	var rowsShown = parseInt(rowsToShow, 10);
	var rowsTotal = $('#'+ tableId +' tbody tr').length;
	var numPages = rowsTotal/rowsShown;
	var numPages2 = numPages.toFixed();
	if(numPages2 < numPages){
		numPages = parseInt(numPages2,10)+1;
	}else{
		numPages = numPages2;
	}
	$('#totpages').text(numPages);
	$('#'+ tableId +' tbody tr').hide();
	$('#'+ tableId +' tbody tr').slice(0, rowsShown).show();
	
	//Fix by Sriram for Defect#589
	gotoPage(1);
	//Fix by Sriram for Defect#589 ends
	
}

function gotoPage(pageNum){
	var rowsToShow = $('#pages_max_rows').val();
	var rowsShown = parseInt(rowsToShow, 10);
	var startItem = (pageNum-1) * rowsShown;
	var endItem = startItem + rowsShown;
	$('#'+ tableId +' tbody tr').css('opacity','0.0').hide().slice(startItem, endItem).css('display','table-row').animate({opacity:1}, 300);
	/*$('#curpage').text(pageNum);*/
	$('#curpage').val(pageNum);
}

function searchTable(inputVal)
{
	var dataSize=inputVal.length;
	if(dataSize>1){
		var table = $('#' + tableId);	
		table.find('tr').each(function(index, row)
				{
			var allCells = $(row).find('td');
			if(allCells.length > 0)
			{
				var found = false;
				allCells.each(function(index, td)
						{
					var regExp = new RegExp(inputVal, 'i');
					if(regExp.test($(td).text()))
					{
						found = true;
						return false;
					}
						});
				if(found == true)$(row).show().animate({opacity:1},300);else $(row).hide();
			}
				});}
	if(dataSize===0){


		var ms = $('#totpages').text();
		var maxPages = parseInt(ms, 10);
		var curPage = $('#curpage').val();
		curPage = parseInt(curPage,0);

		var id=$(this).attr('id');

		if(id == 'next'){
			if(curPage < maxPages){
				curPage = curPage + 1;
			}else{
				curPage = maxPages;
			}
		}else if(id == 'prev'){
			if(curPage > 1){
				curPage = curPage -1;
			}else{
				curPage=1;
			}
		}else if(id == 'first'){
			curPage = 1;
		}else if(id == 'last'){
			var rowsToShow = $('#pages_max_rows').val();
			var rowsShown = parseInt(rowsToShow, 10);
			var rowsTotal = $('#' + tableId + ' tbody tr').length;
			var numPages = rowsTotal/rowsShown;
			var numPages2 = numPages.toFixed();
			if(numPages2 < numPages){
				numPages = parseInt(numPages2,10)+1;
			}else{
				numPages = numPages2;
			}
			curPage = numPages;
		}


		gotoPage(curPage);		

	}

}