/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  tables.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 */	

var alwaysVisibleColumnClasses = ['always-visible', 'nosort'];
var excludeColvisClasses = ['always-visible', 'nosort', 'rowaction'];

 (function($) {

    $.fn.convertToDataTable = function (opts) {

        var defaultOptions = {
            searching: true,
            lengthChange: true,
            orderable: true,
            serverSide: false,
            exportExcel: false,
            exportPdf: false,
            exportCsv: false,
            changeColumnVisibility: false,
            scrollX: false,
            scrollY: false,
            rowSelect: false,
            pageLength: 10
        };

        var options = $.extend(defaultOptions, opts);


        var $rawTable = this;

        var columnDefArray = [];
        
        var colIndex = 0;
        $rawTable.find("th").each(function() {
            var $th = $(this);

            var columnDef = {};
            columnDef.targets = colIndex;
            if($th.hasClass('nosort')) {
                columnDef.sortable = false;
            }

            if($th.hasClass("nosearch")) {
                columnDef.searchable = false;
            }

            columnDefArray.push(columnDef);

            colIndex++;
        });

        var pageLength = $rawTable.data('defaultPageLength');
        if(!pageLength) {
            //check if it is there in the options
            if(options.pageLength) {
                pageLength = options.pageLength;
            }else{
                pageLength = 10;
            }
        }

        if(isNaN(pageLength)) {
            pageLength = 10;
        }

        var finalDataTableOptions = {};
        finalDataTableOptions.searching = options.searching;
        finalDataTableOptions.orderable = options.orderable;
        finalDataTableOptions.pageLength = pageLength;
        finalDataTableOptions.lengthChange = options.lengthChange;
        finalDataTableOptions.columnDefs = columnDefArray;

        $rawTable.dataTable(finalDataTableOptions);
        /*Added by Roshni for TENJINCG-1094 starts
        var theads=$rawTable.get(0).getElementsByTagName('thead');
        var tr=theads[0].getElementsByTagName('tr');
        var ths=tr[0].cells;
        for(var i in ths){
        	if(ths[i].classList!=undefined && ths[i].classList.contains('nosort'))
        		ths[i].classList.remove('sorting_asc','sorting_desc');
        }
        Added by Roshni for TENJINCG-1094 ends*/

    };

 } (jQuery));



 $(document).ready(function() {
    $('table').each(function() {
        if($(this).hasClass('tjn-default-data-table')) {
            $(this).convertToDataTable();
        }
    });
 });
 	/*Added by Pushpa for TENJINCG-1088 starts*/
 	var selectedItems=[];
	$(document).on('change','.chkBox',function(){
		if($(this). prop("checked") == true){
			var arr1 = Object.keys($(this).data());
			var temp=$(this).data(arr1[0]);
			selectedItems.push(temp);
		}else{
			var arr1 = Object.keys($(this).data());
			var itemtoRemove=$(this).data(arr1[0]);
			selectedItems.splice($.inArray(itemtoRemove, selectedItems),1);
		}
	});
	/*Added by Pushpa for TENJINCG-1088 ends*/
 