/**
 * Selectable and Searchable entity list using jQuery, jQuery UI and CSS3
 * Sriram Sridharan
 * 5th April, 2017
 */
$(document).ready(function () {
	
	/*$('.entity-list').selectable({
		cancel:'a'
	});
	
	$('#search').keyup(function() {
		
	});*/
});

(function ($) {
	var opts;
	var $root;
	var rootSearchableId;
	
	var methods = {
			init : function(options) {
				//Extend the default options
				opts = $.extend({}, $.fn.searchableList.defaults, options);
				
				$root = $(this);
				rootSearchableId = $root.attr('id') + '_search_input';
				var $searchableDiv = $("<div />");
				$searchableDiv.addClass('entity-search');
				
				var $searchBox = $("<input type='text' placeholder='" + opts.searchPlaceholder + "' class='entity-search-input stdTextBox' data-list-id='" + $root.attr('id') + "' id='" + rootSearchableId + "'/>");
				$searchableDiv.append($searchBox);
				
				$searchableDiv.insertBefore($root);
				
				/*if(opts.selectable === 'true' || opts.selectable === true || opts.selectable == true || opts.selectable == 'true') {
					$root.selectable({
						cancel:opts.selectExclusions
					});
				}*/
			}
	}
	
	$.fn.searchableList = function (methodOrOptions) {
		
		if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' is undefined for jQuery.searchableList' );
        }    
		
	};
	
	$.fn.searchableList.defaults = {
			selectable: 'true',
			selectExclusions:'a',
			searchPlaceholder:'Type to search...'
	}
} (jQuery));

$(document).on('keyup', '.entity-search-input', function(){
	var val = $(this).val();
	
	var $connectedList = $('#' + $(this).data('listId'));
	
	if(val === '') {
		$connectedList.find('li').each(function() {
			$(this).removeClass('hidden');
		});
	} else{
		$connectedList.find('li').each(function() {
			var html = $(this).html();
			if(!html.toLowerCase().includes(val.toLowerCase())){
				$(this).addClass('hidden');
			}else{
				$(this).removeClass('hidden');
			}
		});
	}
});