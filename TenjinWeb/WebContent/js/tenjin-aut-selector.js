/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  tenjin-aut-selector.js

Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 		CHANGED BY              DESCRIPTION
* 23-Oct-2017			    Sriram					Newly Added For TENJINCG-399
* 14-03-2018                Padmavathi            	TENJINCG-612
* * 14-11-2019              Ramya                   TENJINCG-1166
* 15-11-2019				Ashiki					TENJINCG-1166
* 15-06-2021				Ashiki					TENJINCG-1275
* 
*/

$(document).on('change', '.applicationSelector', function() {
	/*Modified by Padmavathi for TENJINCG-612 starts*/
	var loadApis=$(this).data('loadApis');
	var loadFunctions = $(this).data('loadFunctions');
	var loadFunctionGroups = $(this).data('loadFunctionGroups');
	var loadOperations = $(this).data('loadOperations');
	var loadAutLoginTypes = $(this).data('loadAutLoginTypes');
	var functionSelector = $(this).data('functionSelector');
	var apiSelector = $(this).data('apiSelector');
	var groupSelector = $(this).data('functionGroupSelector');
	var operationSelector = $(this).data('operationSelector');
	var autLoginTypeSelector = $(this).data('autLoginTypeSelector');
	/*Added by Ashiki for TENJINCG-1275 starts*/
	var loadMsg = $(this).data('loadMsg');
	/*Added by Ashiki for TENJINCG-1275 ends*/
	var appId = $(this).val();
	/*Modified by Padmavathi for TENJINCG-612 ends*/
	if(loadFunctionGroups === true || loadFunctionGroups === 'true') {
		$.ajax({
			url:'AutFunctionAjaxServlet?entity=function_group&a=' + appId,
			async:true,
			dataType:'json',
			success:function(result) {
				if(result.status.toLowerCase() === 'success') {
					var $functionGroupSelector = $('#' + groupSelector);
					$functionGroupSelector.html("<option value='ALL'>-- Select One --</option>");
					for(var i=0; i<result.data.length; i++) {
						var group = result.data[i];
						/*Added by Pushpalatha for TENJINCG-568 starts*/
						if(group!='Ungrouped')
							/*Added by Pushpalatha for TENJINCG-568 starts*/
							/* Modified by Ramya for TENJINCG-1166  starts*/
							/*$functionGroupSelector.append($("<option value='" + group + "'>" + group + "</option>"));*/
						    $functionGroupSelector.append($("<option value='" + escapeHtmlEntities(group) + "'>" + escapeHtmlEntities(group) + "</option>"));
						    /* Modified by Ramya for TENJINCG-1166  ends*/
					}
				}else{
					alert('Error: ' + result.message);
				}
			},
			error:function(xhr, textStatus, errorThrown) {
				Console.log(textStatus + ' - ' + errorThrown);
				showMessage('An internal error occurred. Please contact Tenjin Support.');
			}
		});
	}/*Added by Padmavathi for TENJINCG-612 starts*/
	if(loadFunctions === true||loadFunctions==='true'){
		$.ajax({
			url:'AutFunctionAjaxServlet?entity=function&a=' + appId,
			async:true,
			dataType:'json',
			success:function(result) {
				if(result.status.toLowerCase() === 'success') {
					var $functionSelector = $('#' + functionSelector);
					$functionSelector.html("<option value='-1'>-- Select One --</option>");
					for(var i=0; i<result.data.length; i++) {
						var func = result.data[i];
						/*Modified by Ashiki for TENJINCG-1166 starts*/
						/*$functionSelector.append($("<option value='" + func.code + "'>" +func.code + "</option>"));*/
						$functionSelector.append($("<option value='" + escapeHtmlEntities(func.code) + "'>" +escapeHtmlEntities( func.code) + "</option>"));
						/*Modified by Ashiki for TENJINCG-1166 ends*/
					}
				}else{
					alert('Error: ' + result.message);
				}
			},
			error:function(xhr, textStatus, errorThrown) {
				showMessage('An internal error occurred. Please contact Tenjin Support.');
			}
		});
		
	}if(loadApis === true||loadApis==='true'){
		$.ajax({
			url:'AutFunctionAjaxServlet?entity=api&a=' + appId,
			async:true,
			dataType:'json',
			success:function(result) {
				if(result.status.toLowerCase() === 'success') {
					var $apiSelector = $('#' + apiSelector);
					$apiSelector.html("<option value='-1'>-- Select One --</option>");
					for(var i=0; i<result.data.length; i++) {
						var api = result.data[i];
						$apiSelector.append($("<option value='" + escapeHtmlEntities(api.code) + "'>" + escapeHtmlEntities(api.code) + "</option>"));
					}
				}else{
					alert('Error: ' + result.message);
				}
			},
			error:function(xhr, textStatus, errorThrown) {
				showMessage('An internal error occurred. Please contact Tenjin Support.');
			}
		});
		
	}
	if(loadOperations === true||loadOperations==='true'){
		$.ajax({
			url:'AutFunctionAjaxServlet?entity=operation&a=' + appId,
			async:true,
			dataType:'json',
			success:function(result) {
				if(result.status.toLowerCase() === 'success') {
					var $operation = $('#' + operationSelector);
					$operation.html("<option value='-1'>-- Select One --</option>");
					for(var i=0; i<result.data.length; i++) {
						var operations = result.data[i];
						/*Modified by Ashiki for TENJINCG-1166 starts*/
						/*$operation.append($("<option value='" +operations + "'>" +operations + "</option>"));*/
						$operation.append($("<option value='" +escapeHtmlEntities(operations) + "'>" +escapeHtmlEntities(operations) + "</option>"));
						/*Modified by Ashiki for TENJINCG-1166 ends*/
					}
				}else{
					alert('Error: ' + result.message);
				}
			},
			error:function(xhr, textStatus, errorThrown) {
				showMessage('An internal error occurred. Please contact Tenjin Support.');
			}
		});
		
	}if(loadAutLoginTypes === true||loadAutLoginTypes==='true'){
		$.ajax({
			url:'AutFunctionAjaxServlet?entity=autLoginTypes&a=' + appId,
			async:true,
			dataType:'json',
			success:function(result) {
				if(result.status.toLowerCase() === 'success') {
					var $autLoginTypeSelector = $('#' + autLoginTypeSelector);
					$autLoginTypeSelector.html("<option value='-1'>-- Select One --</option>");
					for(var i=0; i<result.data.length; i++) {
						var autLoginType= result.data[i];
						/* Modified by Ramya for TENJINCG-1166  starts*/
						/*$autLoginTypeSelector.append($("<option value='" +autLoginType + "'>" +autLoginType + "</option>"));*/
						$autLoginTypeSelector.append($("<option value='" +escapeHtmlEntities(autLoginType) + "'>" +escapeHtmlEntities(autLoginType) + "</option>"));
						/* Modified by Ramya for TENJINCG-1166  ends*/
					}
				}else{
					alert('Error: ' + result.message);
				}
			},
			error:function(xhr, textStatus, errorThrown) {
				showMessage('An internal error occurred. Please contact Tenjin Support.');
			}
		});
		
	}
	/*Added by Ashiki for TENJINCG-1275 starts*/
	if(loadMsg === true){

		$.ajax({
			url:'AutFunctionAjaxServlet?entity=messageFormat&a=' + appId,
			async:true,
			dataType:'json',
			success:function(result) {
				
				if(result.status.toLowerCase() === 'success') {
					var $msgFormatSelector = $('#msgFormat');
					$msgFormatSelector.html("<option value='ALL'>-- Select One --</option>");
					for(var i=0; i<result.data.length; i++) {
						var messageCode = result.data[i];
						$msgFormatSelector.append($("<option value='" +messageCode + "'>" + messageCode + "</option>"));
					}
				}else{
					alert('Error: ' + result.message);
				}
			
			},
			error:function(xhr, textStatus, errorThrown) {
				showMessage('An internal error occurred. Please contact Tenjin Support.');
			}
		});
		
	
	}
	/*Added by Ashiki for TENJINCG-1275 ends*/
});
$(document).on('change','.apiSelector',function(){
	var loadApiOperations=$(this).data('loadApiOperations');
	var apiOperationSelector = $(this).data('apiOperationSelector');
	var appId = $('#appId').val();
	var apiCode = $(this).val();
	
	if(loadApiOperations === true || loadApiOperations === 'true') {
		$.ajax({
			url:'AutFunctionAjaxServlet?entity=apiOperation&a=' + appId+"&apiCode="+apiCode,
			async:true,
			dataType:'json',
			success:function(result) {
				if(result.status.toLowerCase() === 'success') {
					var $apiOperationSelector = $('#' + apiOperationSelector);
					$apiOperationSelector.html("<option value='-1'>-- Select One --</option>");
					for(var i=0; i<result.data.length; i++) {
						var apiOperations = result.data[i];
						$apiOperationSelector.append($("<option value='" + escapeHtmlEntities(apiOperations.name) + "'>" + escapeHtmlEntities(apiOperations.name) + "</option>"));
					}
				}else{
					alert('Error: ' + result.message);
				}
			},
			error:function(xhr, textStatus, errorThrown) {
				Console.log(textStatus + ' - ' + errorThrown);
				showMessage('An internal error occurred. Please contact Tenjin Support.');
			}
		});
		}
});
$(document).on('change','#apiOperation',function(){
	var loadResponseType=$(this).data('loadResponseType');
	var responseTypeSelector = $(this).data('responseTypeSelector');
	var apiCode=$('#apiCode').val();
	var appId = $('#appId').val();
	var operation = $(this).val();
	
	if(loadResponseType === true || loadResponseType === 'true') {
		$.ajax({
			url:'AutFunctionAjaxServlet?entity=apiResponsertype&a=' + appId+"&apiCode="+apiCode+"&operation="+operation,
			async:true,
			dataType:'json',
			success:function(result) {
				if(result.status.toLowerCase() === 'success') {
					var $responseTypeSelector = $('#' + responseTypeSelector);
					$responseTypeSelector.html("");
					for(var i=0; i<result.data.length; i++) {
						var responseType = result.data[i];
						$responseTypeSelector.append($("<option value='" +responseType + "'>" + responseType + "</option>"));
					}
				}else{
					alert('Error: ' + result.message);
				}
			},
			error:function(xhr, textStatus, errorThrown) {
				showMessage('An internal error occurred. Please contact Tenjin Support.');
			}
		});
		}
});
/*Modified by Padmavathi for TENJINCG-612 ends*/
/*Added by Ashiki for TENJINCG-1275 starts*/
$(document).on('change','#msgFormat',function(){
	var msgFormat=$('#msgFormat').val();
	var appId = $('#appId').val();
	if(msgFormat!=null && appId!=null) {
		$.ajax({
			url:'AutFunctionAjaxServlet?entity=fetch_Message_Name&a=' + appId+"&msgFormat="+msgFormat,
			async:true,
			dataType:'json',
			success:function(result) {
				if(result.status.toLowerCase() === 'success') {
					var $msgSelector = $('#Msg');
					$msgSelector.html("");
					for(var i=0; i<result.data.length; i++) {
						var messageCode = result.data[i];
						$msgSelector.append($("<option value='" +messageCode + "'>" + messageCode + "</option>"));
					}
				}else{
					alert('Error: ' + result.message);
				}
			},
			error:function(xhr, textStatus, errorThrown) {
				showMessage('An internal error occurred. Please contact Tenjin Support.');
			}
		});
		}
		/*Added by Ashiki for TENJINCG-1275 ends*/
});