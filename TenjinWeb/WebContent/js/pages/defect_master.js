/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  defect_master.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 * DATE CHANGED BY DESCRIPTION
 * 16-Dec-2016 		Manish			TEN-86
 * 19-Dec-2016 		Manish			TEN-119
 * 19-Dec-2016 		Manish			TEN-118
 * 15-06-2018       Padmavathi      T251IT-27 
 * 18-06-2018       Padmavathi      T251IT-48
 * 29-05-2019		Ashiki			V2.8-77
 * 18-11-2019		Roshni			TENJINCG-1166
 * 11-12-2020		Pushpalatha		TENJINCG-1221
 */
function handleFileSelect(e) {
	var files = e.target.files;
	var filesArr = Array.prototype.slice.call(files);
	
	filesArr.forEach(function(f){
		storedFiles.push(f);
		
		var reader = new FileReader();
		reader.onload = function(e){
			//var html = "<div class='clear'></div><div class='grid_15 att_block'><span>" + f.name + "</span><a href='#' data-file='" + f.name + "' data-res='" + e.target.result + "' class='selFile' title='Click to Remove'>Remove</a></div>";
			
			var html = "<div class='att_block grid_6'><p class='grid_4' title='" + f.name + "'>" + f.name + "</p><a href='#' recid='0' data-file='" + f.name + "' class='selFile' title='Click to Remove'>Remove</a></div>";
			selDiv.append(html);
		}
		
		reader.readAsDataURL(f);
	});
	
	//alert(storedFiles.length);
}

function removeFile(e){
	var file = $(this).data("file");
	for(var i=0; i<storedFiles.length; i++){
		if(storedFiles[i].name === file){
			storedFiles.splice(i,1);
			break;
		}
	}

	removedFiles.push($(this).attr('recid'));
	$(this).parent().remove();
	//alert(storedFiles.length);
}

function handleForm(e){
	e.preventDefault();
	$('.modalmask').show();
	$('#processing_icon').html("<img src='images/loading.gif' alt='In Progress..' />");
	$('#def_review_message').html('<p>Tenjin is processing your actions on the defects. Please Wait...</p>')
	$('#def_review_progress_dialog').show();
	var data = new FormData();
	for(var i=0, len=storedFiles.length; i<len; i++){
		data.append('files', storedFiles[i]);
	}
	
	
	data.append('form_data', $('#form_data').val());
	data.append('req_type','new_defect');
	data.append('post_defect', $('#post_defect').val());
	data.append('csrftoken_form', $('#csrftoken_form').val());
	$.ajax({
		type:'post',
		url:'DefectServlet',
		data:data,
		processData:false,
		contentType:false,
		dataType:'json',
		success:function(r){
			console.log(r);
			//alert(r.status);
			if(r.status == 'SUCCESS'){
				$('#processing_icon').html("<img src='images/success_large.png' alt='Completed' />");
				$('#def_review_message').html('<p>' + r.message + '</p>');
				var callback = 'DefectServlet?param=fetch_defect_details&did=' + r.did;
				$('#btnPClose').attr('callback',callback);
				$('#def_review_pr_actions').show();
			}else if(r.status=='ERROR'){
				$('#processing_icon').html("<img src='images/error_large.png' alt='Completed' />");
				$('#def_review_message').html('<p>' + r.message + '</p>');
				$('#def_review_pr_actions').show();
			}else if(r.status == 'WARN'){
				$('#processing_icon').html("<img src='images/warning_large.png' alt='Completed' />");
				$('#def_review_message').html('<p>' + r.message + '</p>');
				var callback = 'DefectServlet?param=fetch_defect_details&did=' + r.did;
				$('#btnPClose').attr('callback',callback);
				$('#def_review_pr_actions').show();
				$('#btnPClose').attr('callback',callback);
			}
		},
		error:function(r){
			$('#processing_icon').html("<img src='images/error_large.png' alt='Completed' />");
			$('#def_review_message').html('<p>An internal error occurred. Please contact Tenjin Support.</p>')
			/*Added by Ashiki for V2.8-77 starts*/
			$('#def_review_pr_actions').show();
			$('#btnPClose').show();
			/*Added by Ashiki for V2.8-77 ends*/
		}
	});
}

function handleUpdateForm(e){
	e.preventDefault();
	$('.modalmask').show();
	$('#processing_icon').html("<img src='images/loading.gif' alt='In Progress..' />");
	$('#def_review_message').html('<p>Processing. Please Wait...</p>')
	$('#def_review_progress_dialog').show();
	var data = new FormData();
	for(var i=0, len=storedFiles.length; i<len; i++){
		data.append('files', storedFiles[i]);
	}
	var remFiles = '';
	for(var i=0, len=removedFiles.length; i<len; i++){
		remFiles = remFiles + removedFiles[i] + ',';
	}
	
	data.append('remove_files', remFiles);
	
	data.append('form_data', $('#form_data').val());
	data.append('req_type','update_defect');
	data.append('post_defect', $('#post_defect').val());
	data.append('csrftoken_form', $('#csrftoken_form').val());
	$.ajax({
		type:'post',
		url:'DefectServlet',
		data:data,
		processData:false,
		contentType:false,
		dataType:'json',
		success:function(r){
			console.log(r);
			//alert(r.status);
			if(r.status == 'SUCCESS'){
				$('#processing_icon').html("<img src='images/success_large.png' alt='Completed' />");
				$('#def_review_message').html('<p>' + r.message + '</p>');
				var callback = 'DefectServlet?param=fetch_defect_details&did=' + $('#recordId').val() + '&callback=' + $('#callback').val();
				$('#btnPClose').attr('callback',callback);
				$('#def_review_pr_actions').show();
			}else if(r.status=='ERROR'){
				$('#processing_icon').html("<img src='images/error_large.png' alt='Completed' />");
				$('#def_review_message').html('<p>' + r.message + '</p>');
				$('#def_review_pr_actions').show();
			}else if(r.status == 'WARN'){
				$('#processing_icon').html("<img src='images/warning_large.png' alt='Completed' />");
				$('#def_review_message').html('<p>' + r.message + '</p>');
				var callback = 'DefectServlet?param=fetch_defect_details&did=' +  $('#recordId').val() + '&callback=' + $('#callback').val();
				$('#btnPClose').attr('callback',callback);
				$('#def_review_pr_actions').show();
				$('#btnPClose').attr('callback',callback);
			}
		},
		error:function(r){
			$('#processing_icon').html("<img src='images/error_large.png' alt='Completed' />");
			$('#def_review_message').html('<p>An internal error occurred. Please contact Tenjin Support.</p>')
		}
	});
}

function buildJson(){
	var json = new Object();
	/*var txn = $('#txn').val();*/
	json.summary = $('#txtSummary').val();
	json.tc = $('#testcase').val();
	json.tstep = $('#teststep').val();
	json.runId = $('#runid').val();
	json.app = $('#appid').val();
	json.func = $('#functionId').val();
	json.instance = $('#dttname').val();
	json.dttproject = $('#dttproject').val();
	json.severity = $('#severity').val();
	json.priority = $('#priority').val();
	json.desc = $('#description').val();
	/*Added by Pushpa for TENJINCG-1221 starts*/
	json.owner=$('#repoOwner').val();
	/*Added by Pushpa for TENJINCG-1221 ends*/
	json.post_defect = $('#post_defect').val();
	/* json.createdOn = $('#createdOn').val(); */

	var jstring = JSON.stringify(json);
	$('#form_data').val(jstring);
}

function buildJsonForUpdate(){
	var json = new Object();
	/*var txn = $('#txn').val();*/
	json.recid = $('#recordId').val();
	json.summary = $('#txtSummary').val();
	json.severity = $('#severity').val();
	json.priority = $('#priority').val();
	json.desc = $('#description').val();
	json.status = $('#lstStatus').val();
	json.action = $('#action').val();
	json.instance = $('#dttname').val();
	json.dttproject = $('#dttproject').val();
	json.createdby = $('#createdby').val();
	json.createdon = $('#createdon').val();
	/*added by shruthi for TENJINCG-1224 starts*/
	json.runId= $('#runid').val();
	/*added by shruthi for TENJINCG-1224 ends*/
	/*Added by Pushpa for TENJINCG-1221 starts*/
	json.owner=$("#owner").val();
	/*Added by Pushpa for TENJINCG-1221 ends*/
	/*changed by manish for defect TEN-86 on 16-Dec-2016 starts*/
	
	var action = $('#action').val();
	if(action==='POST'){
		$('#post_defect').attr('value','y');
	}
	/*changed by manish for defect TEN-86 on 16-Dec-2016 ends*/
	/* json.createdOn = $('#createdOn').val(); */

	var jstring = JSON.stringify(json);
	$('#form_data').val(jstring);
}

function clearDTTInformation(){
	$('#severity').html("<option value='-1'>-- Select One --</option>");
	$('#severity').val('-1').change();
	$('#priority').html("<option value='-1'>-- Select One --</option>");
	$('#priority').val('-1').change();
	
	$('#dttname').val('');
	
	
	$('#dttproject').val('');
	
}


function populateDTTInformation(appId){
	$('#severity').val("-1").change();
	$('#priority').val('-1').change();
	/*changed by manish for defect TEN-118 on 19-Dec-2016 starts*/
	$('#dttname').val("");
	$('#dttproject').val("");
	clearMessages();
	/*changed by manish for defect TEN-118 on 19-Dec-2016 ends*/
	$.ajax({
		url:'DefectServlet',
		data:'param=fetch_defect_Instance_prj&appId=' + appId,
		async:true,
		dataType:'json',
		success:function(data){
			var status = data.status;
			//alert(status);
			//alert(data.instance);
			if(status == 'success'){
				//alert(data.instance);
				/* modified by Roshni for TENJINCG-1166 starts */
				$('#dttname').val(escapeHtmlEntities(data.instance));
				
				//alert(data.project);
				$('#dttproject').val(escapeHtmlEntities(data.project));
				/*Added by Pushpa for TENJINCG-1221 starts*/
				if(data.dttInstance=='github'){
					$('#subsetBlock').show();
					$('#ownerBlock').show();
					$('#dttSubset').val(escapeHtmlEntities(data.subset));
					$('#repoOwner').val(escapeHtmlEntities(data.owner));
				}else{
					$('#subsetBlock').hide();
					$('#ownerBlock').hide();
				}
				/*Added by Pushpa for TENJINCG-1221 ends*/
				/* modified by Roshni for TENJINCG-1166 ends */
				var sArray = data.severities;
				var sHtml = "<option value='-1'>-- Select One --</option>";
				for(i=0; i<sArray.length;i++){
					var json = sArray[i];
					/* modified by Roshni for TENJINCG-1166 starts */
					sHtml = sHtml + "<option value='" + json.code + "'>" + escapeHtmlEntities(json.code) + "</option>";
					/* modified by Roshni for TENJINCG-1166 ends */
				}
				
				$('#severity').html(sHtml);
				
				var pArray = data.priorities;
				var pHtml = "<option value='-1'>-- Select One --</option>";
				for(i=0; i<pArray.length;i++){
					var json = pArray[i];
					/* modified by Roshni for TENJINCG-1166 starts */
					pHtml = pHtml + "<option value='" + json.code + "'>" + escapeHtmlEntities(json.code) + "</option>";
					/* modified by Roshni for TENJINCG-1166 ends */
				}
				
				$('#priority').html(pHtml);
				
			}
		},
		
		error:function(xhr, textStatus, errorThrown){
			
		}
	});
}

function populateDTTInformationWithDefaults(appId, defaultSeverity, defaultPriority){
	$('#severity').val("-1").change();
	$('#priority').val("-1").change();
	
	$.ajax({
		url:'DefectServlet',
		data:'param=fetch_defect_Instance_prj&appId=' + appId,
		async:true,
		dataType:'json',
		success:function(data){
			var status = data.status;
			//alert(status);
			//alert(data.instance);
			if(status == 'success'){
				//alert(data.instance);
				$('#dttname').val(data.instance);
				
				//alert(data.project);
				$('#dttproject').val(data.project);
				
				var sArray = data.severities;
				var sHtml = "<option value='-1'>-- Select One --</option>";
				for(i=0; i<sArray.length;i++){
					var json = sArray[i];
					sHtml = sHtml + "<option value='" + json.code + "'>" + json.code + "</option>";
				}
				
				$('#severity').html(sHtml);
				$('#severity').val(defaultSeverity).change();
				
				var pArray = data.priorities;
				var pHtml = "<option value='-1'>-- Select One --</option>";
				for(i=0; i<pArray.length;i++){
					var json = pArray[i];
					pHtml = pHtml + "<option value='" + json.code + "'>" + json.code + "</option>";
				}
				
				$('#priority').html(pHtml);
				/*Added by Padmavathi for T251IT-27 starts*/
				$('#priority').val(defaultPriority).change();
				/*Added by Padmavathi for T251IT-27 ends*/
				
			}
		},
		
		error:function(xhr, textStatus, errorThrown){
			
		}
	});
}

function populateTestCases(runId) {
	$('#testcase').html("<option value='-1'>Loading. Please Wait...</option>");
	$.ajax({
		url:'DefectServlet',
		data:'param=fetch_tcs_for_run&run=' + runId,
		async:true,
		dataType:'json',
		success:function(data){
			var status = data.status;
			if(status == 'success'){
				var html = "<option value='-1'>-- Select One --</option>";
				var jarray = data.data;
				for(i=0;i<jarray.length;i++){
					var json = jarray[i];
					/* modified by Roshni for TENJINCG-1166 starts */
					/*html = html + "<option value='" + json.code + "'>" + json.name + "</option>";*/
					html = html + "<option value='" + json.tcRecId + "'>" + escapeHtmlEntities(json.tcId)  + " - " + escapeHtmlEntities(json.tcName) + "</option>";
					/* modified by Roshni for TENJINCG-1166 starts */
				}
				
				$('#testcase').html(html);
			}else{
				$('#testcase').html("<option value='-1'>-- Select One --</option>");
				showMessage(data.message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			$('#testcase').html("<option value='-1'>-- Select One --</option>");
			showMessage(errorThrown,'error');
		}
	});
}

function populateTestSteps(tcRecId,runId) {
	$('#teststep').html("<option value='-1'>Loading. Please Wait...</option>");
	$.ajax({
		url:'DefectServlet',
		/*Modified by Padmavathi for T251IT-48 starts*/
		/*data:'param=fetch_steps_for_tc&tcrecid=' + tcRecId,*/
		data:'param=fetch_steps_for_tc&tcrecid=' + tcRecId+'&runId='+runId,
		/*Modified by Padmavathi for T251IT-48 ends*/
		async:true,
		dataType:'json',
		success:function(data){
			var status = data.status;
			if(status == 'success'){
				var html = "<option value='-1'>-- Select One --</option>";
				var jarray = data.data;
				for(i=0;i<jarray.length;i++){
					var json = jarray[i];
					/* modified by Roshni for TENJINCG-1166 starts */
					/*html = html + "<option value='" + json.code + "'>" + json.name + "</option>";*/
					html = html + "<option value='" + json.recordId+ "' appid='" + json.appId +"' func='" + json.moduleCode +"' funcname='" + json.moduleName + "'>" + escapeHtmlEntities(json.id) + "</option>";
					/* modified by Roshni for TENJINCG-1166 ends */
				}
				
				$('#teststep').html(html);
				/*changed by manish for defect TEN-119 on 19-Dec-2016 starts*/
				$('#teststep').val('-1').change();
				/*changed by manish for defect TEN-119 on 169-Dec-2016 ends*/
			}else{
				$('#teststep').html("<option value='-1'>-- Select One --</option>");
				/*changed by manish for defect TEN-119 on 19-Dec-2016 starts*/
				$('#teststep').val('-1').change();
				/*changed by manish for defect TEN-119 on 19-Dec-2016 ends*/
				showMessage(data.message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			$('#testcase').html("<option value='-1'>-- Select One --</option>");
			/*changed by manish for defect TEN-119 on 19-Dec-2016 starts*/
			$('#teststep').val('-1').change();
			/*changed by manish for defect TEN-119 on 19-Dec-2016 ends*/
			showMessage(errorThrown,'error');
		}
	});
}


function populateModuleInfo(appId){
	$('#functionId').html("<option value='-1'>Loading. Please Wait...</option>");
	$.ajax({
		url:'AutServlet',
		data:'param=fetch_modules_for_app&app=' + appId,
		async:true,
		dataType:'json',
		success:function(data){
			jsonObj = data;
			var status = jsonObj.status;
			if(status== 'SUCCESS'){
				var html = "<option value='-1'>-- Select One --</option>";
				var jarray = jsonObj.modules;
				for(i=0;i<jarray.length;i++){
					var json = jarray[i];
					/* modified by Roshni for TENJINCG-1166 starts */
					/*html = html + "<option value='" + json.code + "'>" + json.name + "</option>";*/
					html = html + "<option value='" + json.code + "'>" + escapeHtmlEntities(json.code)  + " - " + escapeHtmlEntities(json.name) + "</option>";
					/* modified by Roshni for TENJINCG-1166 ends */
				}
				
				$('#functionId').html(html);
				/***************************************
				 * Added by Sriram to support Multipe login types (Tenjin v2.1)
				 */
				//populateAutCreds(appId);
				/***************************************
				 * Added by Sriram to support Multipe login types (Tenjin v2.1) ends
				 */
			}else{
				showMessage(jsonObj.message,'error');
			}
			
		},
		error:function(xhr, textStatus, errorThrown){
			showMessage('Could not fetch Functions due to an internal error. Please contact Tenjin Support','error');
		}
	});
}