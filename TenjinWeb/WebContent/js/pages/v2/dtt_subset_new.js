/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  dtt_subset_new.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 25-09-2019		  	Ashiki				Newly added for TENJINCG-1101
* 11-12-2020			Pushpalatha				TENJINCG-1221
*/

$(document).ready(function(){
	$('#projects_table').hide();
	$('#dmLoader').hide();
	mandatoryFields('main_form');
$('#btnCancel').click(function(){	
	var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
	if(confirmResult){
		window.location.href='DTTSubsetServlet?param=FETCH_ALL_SUBSET';
	}else{
		return false;
	}
	
});
	
$('#lstInstance').change(function(){
	$('#dmLoader').show();
	var instance=$(this).val();
	var urlLink='param=Fetch_Projects&instance='+instance;
	 $.ajax({
			url:'DTTSubsetServlet',
			data :urlLink,
			async:false,
			dataType:'json',
			success: function(data){
			var status = data.status;
			var projectLists = data.projectLists;
			if(status == 'SUCCESS')
			  {			

				var html = "";
				for(i=0;i<projectLists.length;i++){
					var json = projectLists[i];
					/*Modified by Pushpalatha for TENJINCG-1221 starts*/
					var id=json.pname+":"+json.pid+":"+json.owner;
					/*Modified by Pushpalatha for TENJINCG-1221 ends*/
					
						html = html + "<tr><td><input class='table-row-selector' type='checkbox' name='"+id+"' value='"+json+"'  id='"+id+"'></input></td><td>"+json.pname+"</td></tr>"
				}
				$('#dmLoader').hide();
				$('#projects_table').show();
				$('#projectList').html(html);
			
			  }
			  else
			  {
				showMessage(data.message, 'error');	
				
			  }
		    },
		
		   error:function(xhr, textStatus, errorThrown){
			   showMessage(data.message, 'error');			
		  }
		    
	     });
	 /*added by shruthi for TCGST-61 starts*/
	 $('#dmLoader').hide();
	 /*added by shruthi for TCGST-61 ends*/
});



$('#btnSaveProjects').click(function(){
	var count=0;
	var selectedProjects='';
	var subSetName=$('#subSetName').val();
	var instance = $('#txtTool').val();
	var instanceName = $('#lstInstance').val();
	
	if($('#subSetName').val()==null || $('#subSetName').val()==""){
		showMessage("SubSet Name is required"); 
		return false;
	}
	
	$('#defectManagement-table').find('input[type="checkbox"]:checked').each(function (){
		if(this.id != 'chk_all'){
			selectedProjects = selectedProjects + this.id + ',';
			count++;
		}
		
	});
	
	if( selectedProjects.charAt(selectedProjects.length-1) == ','){
		selectedProjects = selectedProjects.slice(0,-1);
	}
	if(count===0)
	{
		showMessage("Project is Required ",'error'); 

	}else
	{
if(count===1 || count>1){
		
	//window.location.href='DTTSubsetServlet?param=Persist_Projects&projects='+selectedProjects+'&subSetName='+subSetName+'&instanceName='+instanceName;
	
	var csrf_token=$('#csrftoken_form').val();
	$.ajax({
		type:'post',
		url:'DTTSubsetServlet',
		data:{param:"Persist_Projects",projects:selectedProjects,subSetName:subSetName,instanceName:instanceName,csrftoken_form:csrf_token},
		dataType:'json',
		success:function(data){
			if(data.status == 'success'){
				var subsetId=data.subsetId;
				window.location.href='DTTSubsetServlet?param=view&recId='+subsetId;
				showMessage(data.message,'success');
			}else{
				window.location.href='DTTSubsetServlet?param=new';
				showMessage(data.message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown,'error');
		}
	});
	
		}
	}
}); 
});

