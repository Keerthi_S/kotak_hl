/***
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  group_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
*//******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 19-02-2018		   Pushpalatha 			   Newly added for TENJINCG-603
* 19-12-2019		   Prem					   Cross scripting fixes added fn:escapeXml to all input fields.
*/	 



$(document).ready(function(){
	$('#btnBack').click(function(){
		window.location.href="GroupServlet"
	});
	
	$('#btnSave').click(function(){
		var grpName=$('#txtGroupName').val();
		var appName=$("#lstApplication option:selected").text();
		var appId=$("#selAut").val();
		var grouDesc=$('#txtGroupDesc').val();
		var type=$('#type').val();
		if(type=="function"){
			var val = [];
			var i=0;
		
			$('#t_draggable1 tr').each(function() {
				val[i]= $(this).find(".func").html();  
				i++;
			});
		
			var record1 = JSON.stringify(val);
			var funcidsUnMapped = JSON.parse(record1);
			var val1=[];
			var j=0;
			$('#t_draggable2 tr').each(function() {
				val1[j]= $(this).find(".func").html();  
				j++;
			});
			var record = JSON.stringify(val1);
			var funcidsMapped = JSON.parse(record);
        
			$('#main_form').append($("<input type='hidden' id='del' name='del' value='functionMap' />"));
			$('#main_form').append($("<input type='hidden' id='paramval' name='paramval' value='"+ funcidsMapped +"' />"));
			$('#main_form').append($("<input type='hidden' id='paramval' name='paramval1' value='"+ funcidsUnMapped +"' />"));
			$('#main_form').append($("<input type='hidden' id='appid' name='appid' value='"+appId+"' />"));
			$('#main_form').append($("<input type='hidden' id='group' name='group' value='"+grpName+"' />"));
			$('#main_form').append($("<input type='hidden' id='groupDesc' name='groupDesc' value='"+escapeHtmlEntities(grouDesc)+"' />"));
			$('#main_form').submit();
			
		}else{
			
			var val = [];
			var i=0;
		
			$('#t_draggable1 tr').each(function() {
				val[i]= $(this).find(".func").html();  
				i++;
			});
		
			var record1 = JSON.stringify(val);
			var funcidsUnMapped = JSON.parse(record1);
			var val1=[];
			var j=0;
			$('#t_draggable2 tr').each(function() {
				val1[j]= $(this).find(".func").html();  
				j++;
			});
			var record = JSON.stringify(val1);
			var funcids = JSON.parse(record);
			
			$('#main_form').append($("<input type='hidden' id='del' name='del' value='apiMap' />"));
			$('#main_form').append($("<input type='hidden' id='paramval' name='paramval' value='"+ funcids +"' />"));
			$('#main_form').append($("<input type='hidden' id='paramval' name='paramval1' value='"+ funcidsUnMapped +"' />"));
			$('#main_form').append($("<input type='hidden' id='appid' name='appid' value='"+appId+"' />"));
			$('#main_form').append($("<input type='hidden' id='group' name='group' value='"+grpName+"' />"));
			$('#main_form').append($("<input type='hidden' id='groupDesc' name='groupDesc' value='"+escapeHtmlEntities(grouDesc)+"' />"));
			$('#main_form').submit();
			
		
			
		}
	});
});


$(document).on('change','#type',function(){
	
	var type=$('select[name=type]').val();
	
	var autId=$('#selAut').val();
	var grpName=$('#txtGroupName').val();
	
		window.location.href='GroupServlet?t=view&paramval='+grpName+'&a='+autId+'&type='+type;
	
});


