/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testcase_list.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 12-03-2018		   Pushpalatha G		   Newly Added for TENJINCG-611
* 18-09-2018			Sriram					TENJINCG-736
* 28-09-2018			Leelaprasad			    TENJINCG-738
* 23-10-2018			Preeti					TENJINCG-850
* 07-11-2018            Padmavathi              TENJINCG-893
* 04-12-2018            Padmavathi              TJNUN262-3
* 06-12-2018            Padmavathi              TJNUN262-3
* 07-12-2018            Padmavathi              TJNUN262-83
* 26-02-2018            Padmavathi              TENJINCG-977
* 13-03-2019			Preeti					TENJINCG-987
* 12-06-2019			Preeti					V2.8-56
* 19-11-2020            Priyank                 TENJINCG-1231
*/

//TENJINCG-736 - Sriram
var testCaseTable;
//TENJINCG-736 - Sriram ends	
$(document).ready(function() {
	  /*Added by Priyanka for TENJINCG-1231 starts*/
	const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
	var current_datetime = new Date();
	var formatted = current_datetime.getDate() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getFullYear();
	   var projectEndDate=$('#endDate').val();   
		  var tdyDate = Date.parse(formatted);
		  var prjEndDate  = Date.parse(projectEndDate);
             if(prjEndDate>=tdyDate){
			  
			  $('#btnNew').show();
			  $('#btnDelete').show();
			  $('#import').show();
			  $('#btnAddToTestSet').show();
			  $('#btnSync').show();
			  $('#Download').show();
			  $('#btnRefresh').show();
		  }
		  else{
			  $('#btnNew').hide();
			  $('#btnDelete').hide();
			  $('#import').hide();
			  $('#btnAddToTestSet').hide();
			  $('#btnSync').hide();
			  $('#Download').show();
			  $('#btnRefresh').hide();
			  
			  
		  }
             /*Added by Priyanka for TENJINCG-1231 ends*/
	
	/*Commented by Padmavathi for TENJINCG-977 starts*/
	/*$('#clearTestCases').hide();*/
	/*Commented by Padmavathi for TENJINCG-977 ends*/
	
	//TENJINCG-736 - Sriram
	/*$('#testcase-table').paginatedTable();*/
	initTable();
	//TENJINCG-736 - Sriram ends
	
	 /* Added by Padmavathi for TENJINCG-893 starts */
	var path=$('#downloadPath').val();
	if(path!=''){
		/*var $c = $("<a id='downloadFile' href="+path+" target='_blank' download />");*/
		var $c = $("<a id='downloadFile' href='DownloadServlet?param="+path+"' target='_blank' download />");
		$("body").append($c);
			$c.get(0).click();
	}
	 /* Added by Padmavathi for TENJINCG-893 ends */
	
	$('#btnNew').click(function(){
		var domain=$('#domain').val();
		var project=$('#projectName').val();
		var projectId=$('#pid').val();
		var user=$('#user').val();
		window.location.href='TestCaseServletNew?t=init_new_testcase&user='+user+'&project='+project+'&domain='+domain+'&pid='+projectId;
	});
	
	
	/*$('#lstMode').children('option').each(function(){
        var tcMode=$('#selTcModeFilter').val();
        var val = $(this).val();
        if(val == tcMode){
            $(this).attr({'selected':true});
        }
    });*/
	
	$('.tc_anchor').click(function() {
		var tcrecid = $(this).attr('recid');
		var pid=$('#pid').val();
		var domain=$('#domain').val();
		var project=$('#projectName').val();
		var user=$('#user').val();
		window.location.href = 'TestCaseServletNew?t=testCase_View&paramval=' + tcrecid+'&project='+project+'&domain='+domain+'&pid='+pid;
	});
	
	$('#btnDelete').click(function(){
		var domain=$('#domain').val();
		var projectName=$('#projectName').val();
		var projectId=$('#pid').val();
		var user=$('#user').val();
		
		clearMessagesOnElement($('#user-message'));
		/*Modified by Padmavathi for TJNUN262-3 ends*/	
		/*if($('#testcase-table').find('input[type="checkbox"]:checked').length < 1) {
			showLocalizedMessage('no.records.selected', 'error');
			return false;
		}else if($('#testcase-table').find('input[type="checkbox"]:checked').length < 2 && $('#testcase-table .tbl-select-all-rows').is(':checked')) {
			showLocalizedMessage('no.records.selected', 'error');
			return false;
		}*/
		// selectedCases variable contain all the selected test cases 
		if(selectedCases == ''){
			showLocalizedMessage('no.records.selected', 'error');
			return false;
		}
		if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
			var $testcaseForm = $("<form action='TestCaseServletNew' method='POST' />");
			$testcaseForm.append($("<input type='hidden' id='del' name='del' value='true' />"))
			/*$('#testcase-table').find('input[type="checkbox"]:checked').each(function() {
				if(!$(this).hasClass('tbl-select-all-rows')) {*/
					/*$testcaseForm.append($("<input type='hidden' name='selectedTestCase' value='"+ $(this).data('testcaseRecId') +"' />"));*/
				$testcaseForm.append($("<input type='hidden' name='domain' value='"+ domain +"' />"));
					$testcaseForm.append($("<input type='hidden' name='projectName' value='"+ projectName +"' />"));
					$testcaseForm.append($("<input type='hidden' name='pid' value='"+ projectId +"' />"));
					$testcaseForm.append($("<input type='hidden' name='user' value='"+ user +"' />"));
					$testcaseForm.append($("<input type='hidden' id='csrftoken_form' name='csrftoken_form' value='" + $('#csrftoken_form').val() + "' />"))
					
			/*	}
			});*/
					// selectedCases variable contain all the selected test cases 
			$testcaseForm.append($("<input type='hidden' name='selectedTestCase' value='"+ selectedCases+"' />"));
			/*Modified by Padmavathi for TJNUN262-3 ends*/	
			$('body').append($testcaseForm);
			$testcaseForm.submit();
		}
	});
	
	$('#btnReset').click(function(){
		/*Modified by Padmavathi for TJNUN262-83 starts*/
		if(flag){
			window.location.href='TestCaseServletNew?t=list';
		}else{
		$('#searchTcName').val('');
		$('#searchTcId').val('');
		$('#lstType').val('');
		//$('#lstMode').val('');
		flag=false;
		clearMessagesOnElement($('#user-message'));
		/*Modified by Padmavathi for TJNUN262-83 ends*/
		}
		
	});
	
	$('#lstType').children('option').each(function(){
        var tcType=$('#selTcTypeFilter').val();
        var val = $(this).val();
        if(val == tcType){
            $(this).attr({'selected':true});
        }
    });
	/*Added by Padmavathi for TJNUN262-83 starts*/
	var flag=false;
	/*Added by Padmavathi for TJNUN262-83 ends*/
	$('#btnSearch').click(function(){
		/*Added by Padmavathi for TJNUN262-83 starts*/
		flag=true;
		$('#Selected_count').html('');
		/*Commented by Padmavathi for TENJINCG-977 starts*/
		/*$('#clearTestCases').hide();*/
		/*Commented by Padmavathi for TENJINCG-977 ends*/
		selectedCases=[];
		/*Added by Padmavathi for TJNUN262-83 ends*/
		
		//TENJINCG-736 - Sriram (Search handling logic commented)
    	/*var search = false;
    	var domain=$('#domain').val();
		var project=$('#projectName').val();
		var projectId=$('#pid').val();
		var user=$('#user').val();
    	var tcIdFilter = $('#searchTcId').val();
    	var tcNameFilter = $('#searchTcName').val();
    	var tcTypeFilter = $('#lstType').val();
    	var tcModeFilter = $('#lstMode').val();
    	var prjId = $('#prjId').val();
    	
    	if(tcIdFilter != undefined && tcIdFilter.length > 0){
    		tcIdFilter = tcIdFilter.replace(/%/g,'%25');
    		search = true;
    	}else if(tcNameFilter != undefined && tcNameFilter.length > 0){
    		tcNameFilter = tcNameFilter.replace(/%/g,'%25');
    		tcIdFilter = '';
    		search = true;
    	}else if(tcTypeFilter != undefined && tcTypeFilter.length > 0){
    		tcNameFilter = '';
    		search = true;
    	}
    	
    	else if(tcModeFilter != undefined && tcModeFilter.length > 0){
    		tcNameFilter = '';
    		search=true;
    	}
    	else{
    		tcTypeFilter = '';
    		tcModeFilter = '';
    	}
    	
    	if(search){
    		window.location.href='TestCaseServletNew?t=tc_search&tcid=' + tcIdFilter + '&tcname=' + tcNameFilter + '&type=' + tcTypeFilter+ '&mode='+tcModeFilter+ '&project='+project+'&domain='+domain+'&user='+user+'&pid='+projectId;
    	}else{
    		alert('Please provide search criteria');
    	}*/
		redrawTableForSearch();
		/*Added by Padmavathi for TJNUN262-83 starts*/
		$('#tbl-select-all-rows-tc').prop('checked',false);
		/*Added by Padmavathi for TJNUN262-83 ends*/
		
		//TENJINCG-736 - Sriram ends
    });
	
	/*Changed by Leelaprasad for the Requirement TENJINCG-738 starts*/
	$("#searchTcId").autocomplete({
	    delay: 100,
	    minLength: 3,
	    source: function (request, response) {  
	    	$('#searchTcName').val('');
	        $.ajax({
	            method: 'GET',
	            dataType: 'json',
	            url: 'TestCaseServletNew',
	            data: {term:request.term,t:'autocomplete_tcid',prj_id:$('#pid').val()},
	            success: function (data) {
	            	
	                 if (typeof Array.prototype.forEach != 'function') {
	    		         Array.prototype.forEach = function(callback){
	    		           for (var i = 0; i < this.length; i++){
	    		             callback.apply(this, [this[i], i, this]);
	    		           }
	    		         };
	    		     }
	                 if(data.status=='SUCCESS')
	    		    var values = data.tclistIds;
	                 var newArray = new Array(values.length);
	    		   for(var i=0;i<values.length;i++){
	    			   newArray[i] = values[i];
		
	                 }
	    		   
	    	    response(newArray);
	         }
	     })   
	  }
  });
	/*Changed by Leelaprasad for the Requirement TENJINCG-738 ends*/
	
	/*Added by Ashiki for TENJINCG-1215 starts*/
    $("#searchLabel").autocomplete({
	    delay: 100,
	    minLength: 3,
	    source: function (request, response) {  
	    	$('#searchTcName').val('');
	        $.ajax({
	            method: 'GET',
	            dataType: 'json',
	            url: 'TestCaseServletNew',
	            data: {term:request.term,t:'autocomplete_label',prj_id:$('#pid').val()},
	            success: function (data) {
	            	
	                 if (typeof Array.prototype.forEach != 'function') {
	    		         Array.prototype.forEach = function(callback){
	    		           for (var i = 0; i < this.length; i++){
	    		             callback.apply(this, [this[i], i, this]);
	    		           }
	    		         };
	    		     }
	                 if(data.status=='SUCCESS')
	    		    var values = data.tclistIds;
	                 var newArray = new Array(values.length);
	    		   for(var i=0;i<values.length;i++){
	    			   newArray[i] = values[i];
		
	                 }
	    		   
	    	    response(newArray);
	         }
	     })   
	  }
  });
    /*Added by Ashiki for TENJINCG-1215 ends*/
	
	$('#import').click(function(){
		clearMessagesOnElement($('#user-message'));
		$('.modalmask').show();
		$('#uploadTestCases').show();
		
		
	});
	
	$('#btnCCancel').click(function(){
		$('.modalmask').hide();
		$('#uploadTestCases').hide();
	});
	
	$('#btnTCancel').click(function(){
		$('.modalmask').hide();
		$('#syncTestCases').hide();
	});
	
	$('#btnExcel').click(function(){
		$('.modalmask').hide();
		$('#uploadTestCases').hide();
		
		$('#uploadExcel > iframe').attr('src','fileupload.jsp');
		
		$('#uploadExcel').width(350).height(300); 
		$('#uploadExcel').width(400).height(180);
		
		$('#uploadExcel > iframe').width(350).height(300); 
		$('#uploadExcel > iframe').width(400).height(180);
		
		$('.modalmask').show();
		$('#uploadExcel').show();
	});
	
	$('#btnTM').click(function(){
		$('.modalmask').hide();
		$('#uploadTestCases').hide();
		$('#img_download_loader').show();
		window.location.href="TestCaseServletNew?t=fetch_tm_testcases";
	});
	/*Added by Preeti for TENJINCG-850 starts*/
	$('#btnPrj').click(function(){
		$('.modalmask').hide();
		$('#uploadTestCases').hide();
		window.location.href = "TestCaseServletNew?t=fetch_prj_testcases";
	});
	/*Added by Preeti for TENJINCG-850 ends*/
	$('#Download').click(function(){
		
			clearMessagesOnElement($('#user-message'));
			var selectedTests = '';
			/*Modified by Padmavathi for TJNUN262-3 starts*/
			/*$('#testcase-table').find('input[type="checkbox"]:checked').each(function () {
				if(this.id != 'tbl-select-all-rows-tc'){
					selectedTests = selectedTests + this.id +',';
				}
			});
			if( selectedTests.charAt(selectedTests.length-1) == ','){
				selectedTests = selectedTests.slice(0,-1);
			}*/
			
			// selectedCases variable contain all the selected test cases 
			selectedTests=selectedCases;
			/*Modified by Padmavathi for TJNUN262-3 ends*/
			var data = '';
			if(selectedTests.length < 1){
				
				if (window.confirm('Are you sure you want to download all Test Cases?')){
				/*data='param=downloadtestcases&paramval=';*/
					data='param=downloadtestcases';
				}else{
					return false;
				}
			}else{
				data='param=downloadtestcases_selected&tclist=' + selectedTests;
			}
			if(data!=''){
			$('#img_download_loader').show();
			$('#Download').prop('disabled',true);
				  $.ajax({
				     url:'TestCaseServlet',
				     data:data,
				     async:true,
				     dataType:'json',
				      success:function(data){
					         var status = data.status;
					            if(status == 'SUCCESS')
					            {
									showMessage(data.message,'success');
									 /* var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");*/
						            var $c = $("<a id='downloadFile' href='DownloadServlet?param="+data.path+"' target='_blank' download />");
					                $("body").append($c);
					                /*Modified by Priyank for TJN27-120 starts and during 2.12 testing*/
						               // $("#downloadFile").get(0).click();
						                $c.get(0).click();
						                /*Modified by Priyanka for TJN27-120 ends* and during 2.12 testing ends*/
					                $c.remove();
					             }
					            else
					             {
						            showMessage(data.message,'error');
					             }
					            $('#img_download_loader').hide();
					            $('#Download').prop('disabled',false);
				            },
				       error:function(xhr, textStatus, errorThrown){
					           showMessage(errorThrown,'error');
					           $('#img_download_loader').hide();
					           $('#Download').prop('disabled',false);
				          },
				      });
			}
		     });
	
	$('#btnAddToTestSet').click(function() {
		clearMessagesOnElement($('#user-message'));
		/*Modified by Padmavathi for TJNUN262-3 starts*/
		var selectedTcs = '';
		// selectedCases variable contain all the selected test cases 
		selectedTcs=selectedCases;
		/*$('#testcase-table').find('input[type="checkbox"]:checked').each(function () {
		       if(this.id != 'tbl-select-all-rows-tc'){
		    	   selectedTcs = selectedTcs + this.id + ',';
		       }
		});*/
		/*Modified by Padmavathi for TJNUN262-3 ends*/
		if(selectedTcs == ''){
			showMessage('Please choose atleast one test case.', 'error');
			return false;
		}
		
		$('#addToTestSet > iframe').attr('src', 'TestCaseServlet?param=test_set_selection&tests=' + selectedTcs);
		$('.modalmask').show();
		$('#addToTestSet').show();
	});
	
	$('#btnSync').click(function() {
		clearMessagesOnElement($('#user-message'));
		/*Modified by Padmavathi for TJNUN262-3 starts*/
		var selectedTcs = '';
		/*$('#testcase-table').find('input[type="checkbox"]:checked').each(function () {
		      
		       if(this.id != 'tbl-select-all-rows-tc'){
		    	   selectedTcs = selectedTcs + this.id + ',';
		       }
		       
		});*/
		// selectedCases variable contain all the selected test cases 
		selectedTcs=selectedCases;
		/*Modified by Padmavathi for TJNUN262-3 starts*/
		var prjId = $('#prjId').val();
		
		if(selectedTcs == ''){
			showMessage('Please select atleast one test case to synchronize', 'error');
			
			return false;
		}
		
		$('.modalmask').show();
		$('#syncTestCases').show();
		
		
	});
	/* Added by Priyanka starts*/
	$(document).on('click','#btnRefresh', function() {
		window.location.href ='TestCaseServletNew?t=list';
	});
	/* Added by Priyanka ends*/
	$('#btnCOk').click(function(){
		var val='';
		var recIds='';
		var tcs =[];
		var count=0;
		$(function(){
			$('.synch:checked').each(function(){
				var syn=$(this).attr('sync');
				var id=$(this).attr('id');
				if(syn=='Remote')
				{
					var obj = new Object();
					obj.tcrecid = id;
					obj.tcid = $(this).attr('tcId');
					val = val+syn+',';
					recIds = recIds+id+',';
					tcs.push(obj);
					count++;
				}
				
			});
		});
		
		if(count>0){
			window.location.href = 'TestCaseServlet?param=sync_test_cases&paramval=' + JSON.stringify(tcs)+'&source=list';
		}else{
			$('.modalmask').hide();
			$('#syncTestCases').hide();
			showMessage('Please select atleast one remote test case to synchronize');
		}
		
	});
	 /*Commented by Padmavathi for TENJINCG-977 starts*/
	/*Added by Padmavathi for TJNUN262-3 starts*/
	/*$('#clearTestCases').click(function(){
		// selectedCases variable contain all the selected test cases 
		unCheckSelectedTc=selectedCases;
		selectedCases=[];
		unCheckCheckboxes();
	});*/
	/*Added by Padmavathi for TJNUN262-3 ends*/
	/*Commented by Padmavathi for TENJINCG-977 ends*/
	$('#btnImportLogs').click(function(){
		clearMessages();
		$('#importLogs').modal('show');
	});
	
	var theads=$('#testcase-table').getElementsByTagName('thead');
    var tr=theads[0].getElementsByTagName('tr');
    var ths=tr[0].cells;
    for(var i in ths){
    	if(ths[i].classList.contains('nosort'))
    		ths[i].classList.remove('sorting_asc');
    }
});



/*Modified by Ashiki for TENJINCG-1214 ends*/
window.closeModalWindow = function () {
	closeModal();
};



function closeTestSetMapModal() {
	$('#addToTestSet > iframe').attr('src','');
	$('#addToTestSet').hide();
	$('.modalmask').hide();
}

function closeModal(){
	$('.subframe > iframe').attr('src','');
	$('.subframe').hide();
	$('.modalmask').hide();
	$('#content_frame').attr('src','tclist.jsp');
}



//TENJINCG-736 - Sriram
function redrawTableForSearch() {
	var url = "TestCaseAjaxServlet?t=search";
	var tcIdFilter = $('#searchTcId').val();
	var tcNameFilter = $('#searchTcName').val();
	var tcTypeFilter = $('#lstType').val();
	/*Modified by Ashiki for TENJINCG-1215 ends*/
	var labelFilter=$('#searchLabel').val();
	/*Modified by Ashiki for TENJINCG-1215 ends*/
	/*code changed by shruthi for TENJINCG-1177*/
	var tcModeFilter = "";
	/*code changed by shruthi for TENJINCG-1177 ends*/
	



	/*Modified by Ashiki for TENJINCG-1214 starts*/
	url += "&tcid=" + tcIdFilter + "&tcname=" + tcNameFilter + "&type=" + tcTypeFilter + "&mode=" + tcModeFilter+ "&label=" + labelFilter;
	/*Modified by Ashiki for TENJINCG-1214 ends*/
	url = encodeURI(url); //added to encode special characters in the search request
	testCaseTable.ajax.url(url);
	testCaseTable.draw();
}

function initTable() {
	
	var url = "TestCaseAjaxServlet?t=search";
	
	testCaseTable = $('#testcase-table').DataTable({
		"processing":true,
		"serverSide":true,
		"searching":false,
		"ajax":{"url":url, "dataSrc":"records"},
		"columns":[
			{"data":"tcRecId"},
			{"data":"tcId"},
			{"data":"tcName"},
			{"data":"tcType"},
			{"data":"tcCreatedOn"},
			/*commented by shruthi for TENJINCG-1177 starts*/
			//{"data":"mode"},
			/*commented by shruthi for TENJINCG-1177 ends*/
			{"data":"storage"}
		],
		
		"columnDefs":[{
			"targets":0,
			"searchable":false,
			"orderable":false,
			"className":"select-row",
			"render":function(data, type, full, meta) {
				return "<input type='checkbox' data-testcase-rec-id='" + data + "' id='" + data + "' class='synch' sync='" + full.storage + "' tcId='" + escapeHtmlEntities(full.tcId) + "'/>";
			}
		}, {
			"targets":[1,2],
			"render":function(data, type, full, meta) {
				return "<a href='TestCaseServletNew?t=testCase_View&paramval=" + full.tcRecId + "'>" + escapeHtmlEntities(data) + "</a>";
			}
		}
		/*Commented by Preeti for V2.8-56 starts*/
		/*,
		Added by Preeti for TENJINCG-987 starts
		{
			"targets":4,
			"className":"text-center"
		}*/
		/*Commented by Preeti for V2.8-56 ends*/
		/*Added by Preeti for TENJINCG-987 ends*/
		]
		
	});
}
//TENJINCG-736 - Sriram ends

