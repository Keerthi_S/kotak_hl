/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testset_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
 10-04-2018		   		Pushpalatha			   	Newly Added 
 17-04-2018				Preeti					TENJINCG-638
 20-04-2018             Padmavathi              TENJINCG-636
 15-06-2018				Preeti					T251IT-58
 22-06-2018				Padmavathi				T251IT-59
 21-09-2018             Padmavathi              TENJINCG-748
 23-10-2018				Preeti					TENJINCG-848
 19-12-2018            Leelaprasad             TJNUN262-119
 25-03-2019				Pushpalatha				TENJINCG-968
 28-05-2019				Ashiki					V2.8-57
 19-09-2019				Preeti					TENJINCG-1068,1069
 19-10-2020             Priyanka                TENJINCG-1231
*/

$(document).ready(function(){
	/*   Added by Priyanka for TENJINCG-1231 starts */
	const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
	var current_datetime = new Date();
	var formatted = current_datetime.getDate() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getFullYear();
	   var projectEndDate=$('#endDate').val();   
		  var tdyDate = Date.parse(formatted);
		  var prjEndDate  = Date.parse(projectEndDate);
		  if (prjEndDate >=tdyDate) {
			    $('#btnBack').show();
				$('#btnSave').show();
				$('#btnCopy').show();
				$('#btnExecuteHistory').show();
				$('#btnRun').show();
				$('#btnUpload').show();
				$('#history').show();
				$('#btnDownload').show();
				
				
		  }
		  else{
			    $('#btnBack').show();
				$('#btnSave').hide();
				$('#btnCopy').hide();
				$('#btnExecuteHistory').hide();
				$('#btnRun').hide();
				$('#btnUpload').hide();
				$('#history').hide();
				$('#btnDownload').hide();
				
				
		  }
		  /*   Added by Priyanka for TENJINCG-1231 ends */
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('main_form');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	/*Added By Ashiki for V2.8-57 starts*/
	var path=$('#downloadPath').val();
	if(path!=''){
		/*var $c = $("<a id='downloadFile' href="+path+" target='_blank' download />");*/
		var $c = $("<a id='downloadFile' href='DownloadServlet?param="+path+"' target='_blank' download />");
		$("body").append($c);
			$c.get(0).click();
	}
	/*Added By Ashiki for V2.8-57 ends*/
	var testSetId=$('#txtTestSetId').val();
	$('#accordion').accordion({heightStyle:'content'});
	$('.tab-section').hide();
    $('#tabs a').bind('click', function(e){
        $('#tabs a.current').removeClass('current');
        $('.tab-section:visible').hide();
        $(this.hash).show();
        $(this).addClass('current');
        e.preventDefault();
    }).filter(':first').click();
    
    $('#test-steps-table').DataTable();
    
  /* Changed by Padmavathi for TENJINCG-748 (removing drag and drop for testcases mapping) starts*/
   /* $('#btnMapCases').click(function(){
    	var testSetId=$('#txtTestSetId').val();
    	var testSetname=$('#txtTestSetName').val();
    	var mode = $('#txtTestMode').val();
		 window.location.href='TestSetServletNew?t=fetch_unmapped_tcs&testset=' + testSetId + '&testsetname=' + testSetname + '&mode=' + mode;
		 
	 });*/
    $('#btnMapCases').click(function(){
    	var testSetId=$('#txtTestSetId').val();
    	var testSetname=$('#txtTestSetName').val();
    	var mode = $('#txtTestMode').val();
		 window.location.href='TestSetServlet?param=fetch_unmapped_tcs&testset=' + testSetId + '&testsetname=' + 
		 testSetname + '&mode=' + mode;
	 });
    
    /* Changed by Padmavathi for TENJINCG-748 removing drag and drop for testcases mapping ends*/
    var selectedMode = $('#selectedMode').val();
    $('#txtTestMode').children('option').each(function(){
    	var val = $(this).val();
    	if(val == selectedMode){
    		$(this).attr({'selected':true});
    		$('#selectedMode').val($(this).text());
    	}
    });
    
    $('#btnRun').click(function(){
   
    	window.location.href='TestSetServlet?entity_name=testset&param=initiate_run&ts=' + testSetId+'&callback=testset';
    	
    });
    
    /*Modified by Preeti for TENJINCG-638 starts*/
    $('#btnExecuteHistory').click(function(){
    	var testSetRecId = $('#txtTsRecId').val();
    	window.location.href='TestSetServletNew?t=testset_execution_history&recid=' + testSetRecId;
    });
	/*Modified by Preeti for TENJINCG-638 ends*/
    
    $('#btnBack').click(function() {
    	window.location.href = 'TestSetServletNew?t=list';
    });
  
    $('#lstPriority').children('option').each(function(){
        var tcPriority=$('#selectedPriority').val();
        var val = $(this).val();
        if(val == tcPriority){
            $(this).attr({'selected':true});
        }
    });
    
    $('#lstType').children('option').each(function(){
        var tcType=$('#selectedType').val();
        var val = $(this).val();
        if(val == tcType){
            $(this).attr({'selected':true});
        }
    });
    
    
	   $('#btnSave').click(function(){
		   var recId=$("#txtTestSetId").val();
		   var name=$('#txtTestSetName').val();
		   var desc=$('#txtTestSetDesc').val();
		   var type=$('#lstType').val();
		   var priority=$("#lstPriority").val();
		  /*Commented By shruthi for Tenj210-40 starts*/
		  /* var mode=$("#tsMode").val();*/
		  /*Commented By shruthi for Tenj210-40 ends*/
		   
		   $('#main_form').append($("<input type='hidden' id='del' name='del' value='updateTestSet' />"));
		   $('#main_form').append($("<input type='hidden' id='txtTestSetId' name='id' value='"+recId+"' />"));
		   $('#main_form').append($("<input type='hidden' id='txtTestSetName' name='name' value='"+name+"' />"));
		   $('#main_form').append($("<input type='hidden' id='txtTestSetDesc' name='description' value='"+desc+"' />"));
		   $('#main_form').append($("<input type='hidden' id='lstType' name='type' value='"+type+"' />"));
		   $('#main_form').append($("<input type='hidden' id='lstPriority' name='priority' value='"+priority+"' />"));
		   /*Commented By shruthi for Tenj210-40 starts*/
		   /*$('#main_form').append($("<input type='hidden' id='lstmode' name='mode' value='"+mode+"' />"));*/
		   /*Commented By shruthi for Tenj210-40 ends*/
		   $('#main_form').submit();
	   });
	  
	   /*Changed by Leelaprasad for the requirement TJNUN262-119 starts*/
	   /*$('#btnRefreshSteps').click(function() {
		    location.reload();
		});*/
	   /*Changed by Leelaprasad for the requirement TJNUN262-119 ends*/
	  
	   $('#btnUpload').click(function(){
		   var mode=$("#txtTestMode").val();
		 /*  Added by Padmavathi for T251IT-59 Starts*/
		   var recId=$("#txtTestSetId").val();
		   /*  Added by Padmavathi for T251IT-59 ends*/
		     var setName=$("#tc_original").val();
		   /* Modified by Padmavathi for T251IT-59 Starts*/
			/*$('#uploadExcel > iframe').attr('src','uploadTestCase.jsp?mode='+mode+'&setName='+setName);*/
		   $('#uploadExcel > iframe').attr('src','uploadTestCase.jsp?mode='+mode+'&setName='+setName+'&setId='+recId);
		   /* Modified by Padmavathi for T251IT-59 ends*/
			
			 $('#uploadExcel').width(350).height(300); 
			$('#uploadExcel').width(400).height(180);
			
			
			 $('#uploadExcel > iframe').width(350).height(300); 
			$('#uploadExcel > iframe').width(400).height(180);
			
			$('.modalmask').show();
			$('#uploadExcel').show();
			 $('#historyCases').hide();
			});
	   
	   /*Modified by Preeti for TENJINCG-638 starts*/
	   $('#btnDownload').click(function(){
		   var txtTestSetId=$("#txtTestSetId").val();
		   	/*Modified by Preeti for T251IT-58 starts*/
		   	/*if (window.confirm('Are you sure you want to download the Test Set?')){*/
		   	if (window.confirm('Are you sure you want to download the Test Case(s) from the Test Set?')){
			/*Modified by Preeti for T251IT-58 ends*/
			data='t=download_tc_for_ts&txtTestSetId='+txtTestSetId;
			}else{
				return false;
			}
		   
			if(data!=''){
				
				$('#img_download_loader').show();
				$('#btnDownload').prop('disabled',true);
					  $.ajax({
					     url:'TestSetServletNew',
					     data:data,
					     async:true,
					     dataType:'json',
					      success:function(data){
						         var status = data.status;
						            if(status == 'SUCCESS')
						            {
										showMessage(data.message,'success');
										  /* var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");*/
							            var $c = $("<a id='downloadFile' href='DownloadServlet?param="+data.path+"' target='_blank' download />");
						                $("body").append($c);
						                $("#downloadFile").get(0).click();
						                $c.remove();
						             }
						            else
						             {
							            showMessage(data.message,'error');
						             }
						            $('#img_download_loader').hide();
						            $('#btnDownload').prop('disabled',false);
					            },
					       error:function(xhr, textStatus, errorThrown){
						           showMessage(errorThrown,'error');
						           $('#img_download_loader').hide();
						           $('#btnDownload').prop('disabled',false);
					          },
					      });
				}
		   
	   });
	  /*Modified by Preeti for TENJINCG-638 ends*/
	/*Added by Preeti for TENJINCG-848 starts*/
	$('#btnCopy').click(function(){
		var txtTestSetId=$("#txtTestSetId").val();
		var domain=$('#domain').val();
		var project=$('#projectName').val();
		var projectId=$('#pid').val();
		var user=$('#user').val();
		window.location.href='TestSetServletNew?t=testset_copy&user='+user+'&project='+project+'&domain='+domain+'&pid='+projectId+'&recId='+txtTestSetId;
	}); 
	/*Added by Preeti for TENJINCG-848 ends*/ 
	/*Added by Preeti for TENJINCG-1068,1069 starts*/
	$('#history').click(function(){
		clearMessagesOnElement($('#user-message'));
		var activityArea = 'Project';
		var recId = $('#txtTsRecId').val();
		var id = $('#tc_original').val();
		var entityType ='testset';
		$('#history-sframe').attr('src','TestReportServlet?param=CHANGE_HISTORY&activityArea='+activityArea+'&recId='+recId+'&id='+id+'&entityType='+entityType);
		$('.modalmask').show();
		$('#historyCases').show();
		$('#uploadExcel').hide();
	});
	/*Added by Preeti for TENJINCG-1068,1069 ends*/
});

function getTsRecId(){
	var recid = $('#txtTsRecId').val();
	return recid;
}

/*Added By Ashiki for V2.8-57 starts*/
window.closeModalWindow = function () {
	closeModal();
};
/*Added By Ashiki for V2.8-57 ends*/
/*Modified by Preeti for TENJINCG-1068,1069 starts*/
/*function closeModal(){
	
	var tsId = getTsRecId();
	$('#prj-sframe').attr('src','');
	$('.subframe').hide();
	$('.modalmask').hide();
	Commented By Ashiki for V2.8-57 starts
	window.location.href='TestSetServletNew?t=testset_view&paramval=' + tsId;
	Commented By Ashiki for V2.8-57 starts
}*/
function closeModal(){
		$('.subframe > iframe').attr('src','');
		$('.subframe').hide();
		$('.modalmask').hide();
		
	}
/*Modified by Preeti for TENJINCG-1068,1069 ends*/