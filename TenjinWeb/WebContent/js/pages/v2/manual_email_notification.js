/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  manual_email_notification.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 16-10-2018			Sriram					File optimized for TENJINCG-876
 30-10-2018			Sriram Sridharan		TENJINCG-886
*/
$(document).ready(function() {
	$('#img_loader').hide();
	initTable();
});

$(document).on('click', '#btnExit', function() {
	window.parent.closeModal();
});

$(document).on('click', '#sendMail', function() {
	clearMessages();
	sendMail();
});

function sendMail() {
	var projectId = $('#h-projectId').val();
	var runId = $('#h-runId').val();
	
	var selectedUsers = '';
	$('#prj-users-table').find('input[type="checkbox"]:checked').each(function() {
		if(this.id === 'chk_all_users'){
			
		}else{
			selectedUsers += this.id + ";";
		}
	});
	
	console.log("Selected users: " + selectedUsers);
	
	if(selectedUsers === ''){
		showMessage('Please choose atleast one user to continue', 'error');
		return false;
	}
	
	$('#img_loader').show();
	var csrftoken_form = $("#csrftoken_form").val();
	$.ajax({
		url:'ProjectMailServlet',
		data:'param=send_mail_manual&run=' + runId + '&project=' + projectId + '&users=' + selectedUsers+'&csrftoken_form='+csrftoken_form,
		dataType:'json',
		async:true,
		success:function(data) {
			if(data && data.status && data.status.toLowerCase() === 'success') {
				showMessage(data.message, 'success');
			}else if(data.message && data.message !== '') {
				showMessage(data.message, 'error');
			}else{
				showMessage('An internal error occurred.', 'error');
			}
			$('#img_loader').hide();
		}, error:function(xhr, textStatus, errorThrown) {
			showMessage('A network error occurred. Please try again', 'error');
			$('#img_loader').hide();
		}
	});
	
}


function initTable(){
	$('#prj-users-table').DataTable({
		ajax:{
			url:'ProjectAjaxServlet?t=search_prj_users&projectId=' + $('#h-projectId').val(), //TENJINCG-886 (Sriram)
			dataSrc:''
		},
		"columns":[
			{"data":"id"},
			{"data":"id"},
			{"data":"fullName"},
			{"data":"email"}
		],
		"columnDefs":[{
			"targets":0,
			"searchable":false,
			"orderable":false,
			"className":"select-row",
			"render":function(data, type, full, meta) {
				return "<input type='checkbox' data-user-id='" + data + "' id='" + data + "'/>";
			}
		}]
	});
}