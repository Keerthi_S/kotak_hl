
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  metadata.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 31-10-2018			Sriram					Newly added for TENJINCG-894
* 15-11-2018            Padmavathi              to hide identification properties and attributes
* 06-12-2018            Padmavathi               TENJINCG-709
* 31-12-2018            Padmavathi               To refresh user remarks field 
*/

var appId;
var functionCode;

$(document).ready(function() {
	/*Added by Padmavathi to hide identification properties and attributes */
	$('.fieldDetailsBlockHide').hide();
	/*Added by Padmavathi to hide identification properties and attributes ends */
	appId = $('#h-appId').val();
	functionCode = $('#h-functionCode').val();
	hideLoadingBlock();
	hideFieldDetails();
	hideSaveProgress();
	hideSaveSuccess();
	initTree();
	/* Added by Padmavathi for TENJINCG-907 starts */
	$('#btnExpandAll').click(function(){
		$("#m_tree").jstree("open_all"); 
	});
	
	$('#btnCollapseAll').click(function(){
		$("#m_tree").jstree("close_all"); 
	});
	/* Added by Padmavathi for TENJINCG-907 ends */
	$('#m_tree').on('changed.jstree', function(e,data) {
		/*Added by Padmavathi to hide save success image on click on another field */
		hideSaveSuccess();
		/*Added by Padmavathi to hide save success image on click on another field ends */
		var nodeType = data.node.li_attr.node_type;
		if(nodeType && nodeType.toLowerCase() === 'field') {
			var pageName = data.node.li_attr.text;
			var uname = data.node.li_attr.uname;
			hideFieldDetails();
			showLoadingBlock();
			resetFieldDetails();
			var mUrl = 'MetaDataServlet?param=testObject&app=' + appId + '&func=' + functionCode + '&page=' + encodeURIComponent(pageName) + '&funame=' + encodeURIComponent(uname);
//			mUrl = encodeURIComponent(mUrl);
			$.ajax({
				url:mUrl,
				dataType:'json',
				async:true,
				success:function(result){
					if(result && result.status && result.status.toLowerCase() === 'success') {
						console.log(result.content);
						populateFieldDetails(result.content, pageName);
						hideLoadingBlock();
						showFieldDetails();
					}else if(result && result.message) {
						console.error(result.message);
						showMessage(result.message, 'error');
						hideLoadingBlock();
						//showFieldDetails();
					}else{
						console.error("An unknown error occurred");
						showMessage("An internal error occurred", 'error');
						hideLoadingBlock();
						//showFieldDetails();
					}
				},
				error:function(xhr, textStatus, errorThrown) {
					console.error("An unknown error occurred");
					console.error(xhr);
					showMessage("An internal error occurred", 'error');
					hideLoadingBlock();
					//showFieldDetails();
				}
				
			});
		}else{
			hideLoadingBlock();
			hideFieldDetails();
			hideSaveProgress();
			hideSaveSuccess();
			resetFieldDetails();
		}
		
	});
});

$(document).on('click', '#btnBack', function() {
	appId = $('#h-appId').val();
	functionCode = $('#h-functionCode').val();
	window.location.href='FunctionServlet?t=view&key=' + functionCode + '&appId=' + appId;
});

function resetFieldDetails() {
	$('#pageArea').val('');
	$('#fieldUniqueName').val('');
	$('#fieldTypeIndicator').text('');
	$('#mandatoryIndicator').hide();
	$('#readOnlyIndicator').hide();
	
	$('#fieldUserRemarks').text('');
	
	$('#sequence').val('');
	$('#identifiedBy').val('');
	$('#uniqueId').val('');
	$('#label').val('');
	$('#defaultOptions').val('');
	$('#lov').val('');
	$('#autoLov').val('');
	$('#multiRecord').val('');
	$('#group').val('');
}

function populateFieldDetails(field, pageAreaName) {
	$('#pageArea').val(pageAreaName);
	$('#fieldUniqueName').val(field.name);
	$('#fieldTypeIndicator').text(field.objectClass);
	/*Added by Prem to disable button fields while Tenjin 2.10 testing start*/
	$('#fieldUserRemarks').attr("disabled", false);
	$('#btnSave').attr("disabled", false);
	$('#fieldDetailsButton').hide();
	/*Added by Prem to disable button fields while Tenjin 2.10 testing End*/
	if(field.mandatory && field.mandatory.toLowerCase() === 'yes') {
		$('#mandatoryIndicator').show();
	}
	clearMessages();
	/*Added by Prem to disable button fields while Tenjin 2.10 testing start*/
	if(field.objectClass === 'BUTTON') {
		$('#fieldUserRemarks').attr("disabled", true);
		$('#btnSave').attr("disabled", true);
		$('#fieldDetailsButton').show();
	}
	/* Added by Prem to disable button fields while Tenjin 2.10 testing End*/
	/*if(field.viewmode && field.viewmode.toLowerCase() === 'y') {*/
	if(field.objectClass && field.objectClass.toLowerCase() === 'static text') {
		$('#readOnlyIndicator').show();
	}
	
	if(field.userRemarks) {
		/*Modified by Padmavathi to refresh user remarks field starts */
		/*$('#fieldUserRemarks').text(field.userRemarks);*/
		$('#fieldUserRemarks').val(field.userRemarks);
	}else{
		/*$('#fieldUserRemarks').text('');*/
		$('#fieldUserRemarks').val('');
		/*Modified by Padmavathi to refresh user remarks field ends */
	}
	
	$('#sequence').val(field.sequence);
	$('#identifiedBy').val(field.identifiedBy);
	$('#uniqueId').val(field.uniqueId);
	$('#label').val(field.label);
	$('#defaultOptions').val(field.defaultOptions);
	$('#lov').val(field.lovAvailable);
	$('#autoLov').val(field.autoLovAvailable);
	$('#multiRecord').val(field.isMultiRecord);
	$('#group').val(field.group);
	
	
}

$(document).on('click', '#btnSave', function() {
	/* Added by Prem to Tenj210-37 start*/
	clearMessages();
	/* Added by Prem to Tenj210-37 end*/
	updateUserInfo();
});

function updateUserInfo() {
	var t = {};
	t.location = $('#pageArea').val();
	t.name = $('#fieldUniqueName').val();
	 /* Added by Prem to Tenj210-37 start*/
	var Remarks = $('#fieldUserRemarks').val();
	/*if(Remarks!=""){
        if(/[^a-zA-Z0-9 ]/.test(Remarks)) {
             showMessage("Only characters,numbers and spaces are allowed for Remarks","error");
             return false;
        }
    }
*/	/* Added by Prem to Tenj210-37 end*/
	t.userRemarks = Remarks;
	
	var tJson = JSON.stringify(t);
	
	var url = 'MetaDataServlet?param=update_field_user_info&app=' + appId + '&func=' + functionCode ;
	url = encodeURI(url);
	hideSaveSuccess();
	showSaveProgress();
	$.ajax({
		url:url,
		method:'POST',
		contentType:'application/json',
		dataType:'json',
		data: tJson,
		async:true,
		success:function(result) {
			if(result.status && result.status.toLowerCase() === 'success') {
				showSaveSuccess();
			}else if(result.message) {
				showMessage(result.message, 'error');
				hideSaveSuccess();
			}else{
				showMessage("An internal error occurred. Please contact Tenjin Support.", 'error');
				hideSaveSuccess();
			}
			hideSaveProgress();
		}, error: function(xhr, textStatus, errorThrown) {
			showMessage("An internal error occurred. Please contact Tenjin Support.", 'error');
			hideSaveSuccess();
			hideSaveProgress();
		}
	})
	
}


function initTree() {
	$('#m_tree').jstree({
		"core":{
			"data":{
				"url":"MetaDataServlet",
				"data": function(node) {
					return {'param':'tree','id':node.id, 'a':appId, 'f':functionCode, 'txnMode':'G', 'o':''};
				},
				"dataType":"json"
			},
			"themes":{
				"variant":"medium"
			}
		},
		"types":{},
		"plugins":["wholerow", "types"]
	});
}

function showLoadingBlock(){
	$('#loadingBlock').show();
}

function hideLoadingBlock() {
	$('#loadingBlock').hide();
}

function showFieldDetails() {
	$('#fieldDetails').show();
}

function hideFieldDetails() {
	$('#fieldDetails').hide();
}

function showSaveProgress() {
	$('#save_progress').show();
}

function hideSaveProgress() {
	$('#save_progress').hide();
}

function showSaveSuccess() {
	$('#save_success').show();
}

function hideSaveSuccess() {
	$('#save_success').hide();
}