//Created for Changes for TENJINCG-1118 (Sriram)


var allApis = [];
var newApis = [];
var apisByMethod = {};
$(document).ready(function() {
	console.log("Swagger import page loaded");
	
	initState();
	/*Added By Priyanka for checking the file starts During DB Migration starts*/
	$('#btnUploadSwaggerJSON').click(function(e) {
		e.preventDefault();
		initState();
		var json = $('#swaggerJSON').val();
		if(!json || json.length < 1) {
			showLocalizedMessage('Please choose atleast one file to upload', '', 'error');
			return false;
		}
	});
	/*Added By Priyanka for checking the file starts During DB Migration ends*/
	
	$('#btnImportFromUrl').click(function(e) {
		e.preventDefault();
		initState();
		var url = $('#swaggerURL').val();
		if(!url) {
			showLocalizedMessage('Please enter a valid URL', '', 'error');
			return false;
		}
		
		$("#swaggerURLLoader").show();
		$.ajax({
			url : 'RestProcessServlet?param=importFromUrl&appid=' + $("#appid").val() + '&url=' + url,
			dataType:'json',
			success : function(result) {
				if(result && result.status && result.status.toLowerCase() == 'success') {
					var $table = $("#api-table");
					$table.find("tbody").detach();
					
					var $tbody = $("<tbody />").appendTo($table);
					if(result.apis && result.apis.length > 0) {
						var index=0;
						result.apis.forEach(function(api){
							var $tr = $("<tr />").appendTo($tbody);
							var operation = api.operations[0];
							var $selectorTd = $("<td class='table-row-selector' />").appendTo($tr);
							var $checkbox = $("<input type='checkbox' name='chk_api' id='chk_api_'" + index + "/>").appendTo($selectorTd);
							$checkbox.data("api", api);
							$("<td>" + api.code + "</td>").appendTo($tr);
							$("<td>" + operation.method + "</td>").appendTo($tr);
							$("<td>" + api.url + "</td>").appendTo($tr);
							$("<td>" + api.name + "</td>").appendTo($tr);
							var existStatus = api.nonExistent ? "New" : "Existing";
							$("<td>" + existStatus + "</td>").appendTo($tr);
							index++;
							
							allApis.push(api);
							if(!apisByMethod[operation.method]) {
								apisByMethod[operation.method] = [];
							}
							
							if(api.nonExistent) newApis.push(api);
							
							apisByMethod[operation.method].push(api);
						});
					}
					
					$("#allApisCount").text(allApis.length);
					$("#newApisCount").text(newApis.length);
					
					var $methodsBlock = $("#quickImportMethods");
					$methodsBlock.html("");
					Object.keys(apisByMethod).forEach(function(key) {
						var $div = $("<div style='display:inline-block;position:relative;float:left;margin-right:20px;'/>").appendTo($methodsBlock);
						var $checkbox = $("<input type='checkbox' disabled id='select_method_" + key + "' class='inline-checkbox' name='api-method-selector' />").appendTo($div);
						$checkbox.data("method", key);
						$("<label for='select_method_" + key + "' class='inline'>" + key + " (" + apisByMethod[key].length + ")</label>").appendTo($div);
					});
					
					$table.convertToDataTable({
						pageLength:100
					});
					$(".post-import").show();
				}else{
					showLocalizedMessage(result.message, '', 'error');
				}
				$("#swaggerURLLoader").hide();
			},
			error : function(xhr, textStatus, errorThrown) {
				console.error("Could not fetch APIs from specified URL");
				console.log(xhr);
				$("#swaggerURLLoader").hide();
			}
		});
	});
	
	$("input[name=quickImportSelection]").click(function(e) {
		if($(this).val() === 'METHOD') {
			$("#quickImportMethods").find("input[type=checkbox]").prop("disabled", false);
		}else{
			$("#quickImportMethods").find("input[type=checkbox]").prop("disabled", true);
		}
	});
	
	$("#btnSave").click(function(e) {
		e.preventDefault();
		var apisToImport = processApiSelection();
		
		if(!apisToImport || apisToImport.length < 1) {
			showLocalizedMessage("Please select at least one API to import", '', 'error');
			return false;
		}
		
		var appId = $("#appid").val();
		$("#btnSaveLoader").show();
		$.ajax({
			url : 'RestProcessServlet?app=' + appId,
			method : 'post',
			dataType: 'json',
			contentType : 'application/json',
			data : JSON.stringify(apisToImport),
			success : function(result) {
				if(result.status === 'SUCCESS') {
					showLocalizedMessage(apisToImport.length + " APIs imported successfully.", '', 'success');
					initState();
				}else{
					showLocalizedMessage(result.message, '', 'error');
					$("#btnSaveLoader").hide();
				}
			},
			error : function(xhr, textStatus, errorThrown) {
				showLocalizedMessage("An internal error occurred. Please contact Tenjin Support", '', 'error');
				$("#btnSaveLoader").hide();
			}
		});
	});
});

function processApiSelection() {
	var manuallySelectedApis = getApisFromManualSelection();
	if(manuallySelectedApis.length > 0) {
		//Manual selection takes precedence
		return manuallySelectedApis;
	}else{
		var quickImportSelection = $("input[name=quickImportSelection]:checked").val();
		var selectedApis = [];
		if(quickImportSelection && quickImportSelection == 'METHOD') {
			$("input[name=api-method-selector]:checked").each(function() {
				var method = $(this).data("method");
				if(apisByMethod[method] && apisByMethod[method].length > 0) {
					apisByMethod[method].forEach(function(api){
						selectedApis.push(api);
					});
				}
			});
		}else if(quickImportSelection && quickImportSelection == 'ALL') {
			selectedApis = allApis;
		}else if(quickImportSelection && quickImportSelection == 'NEW') {
			selectedApis = newApis;
		}else{
			return [];
		}
		
		return selectedApis;
	}
}

function getApisFromManualSelection() {
	var selectedApis = [];
	
	$("input[name=chk_api]:checked").each(function() {
		selectedApis.push($(this).data("api"));
	});
	
	return selectedApis;
}

function initState() {
	allApis = [];
	newApis = [];
	apisByMethod = {};
	$(".ajx-loader").hide();
	allApis = [];
	$(".post-import").hide();
	if($.fn.DataTable.isDataTable("#api-table")) {
		$("#api-table").DataTable().destroy();
	}
}