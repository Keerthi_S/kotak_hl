
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  dtt_subset_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 25-09-2019		  	Ashiki				Newly added for TENJINCG-1101
* 04-06-2020			Ruksar				for Tenj210-129
*/


$(document).ready(function(){
	$('#dmLoader').hide();
	if($('#status').val()=="Y"){
		$('#btnAddProjects').show();
		$('#btnRemoveProjects').hide();
		/* Added by Ruksar for Tenj210-129 starts*/
		$('#fetchProjects').hide();
		/* Added by Ruksar for Tenj210-129 ends*/
	}else{
		$('#btnAddProjects').hide();
		$('#btnRemoveProjects').show();
	}
	
	mandatoryFields('main_form');
	$('#btnBack').click(function(){	
		/* Added by Priyanka for Tenj210-130 starts*/
		if($('#btnAddProjects').is(":hidden")){
			window.location.href='DTTSubsetServlet?param=FETCH_ALL_SUBSET';
		}else{
			$('#btnAddProjects').hide();
			$('#btnRemoveProjects').show()
			$('#fetchProjects').show()
		}
		/* Added by Priyanka for Tenj210-130 ends*/
	});
	
	$('#btnAddProjects').click(function(){
		var count=0;
		var selectedProjects='';
		var subSetName=$('#subSetName').val();
		var instanceName = $('#lstInstance').val();
		var recId=$('#recId').val();
		if(subSetName==null){
			var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
			if(!confirmResult) {
				return false;
			}
		}
		if($('#subSetName').val()==null || $('#subSetName').val()==""){
			showMessage("SubSet Name is required"); 
			return false;
		}
		$('#projects-table').find('input[type="checkbox"]:checked').each(function (){
			if(this.id != 'chk_all'){
				selectedProjects = selectedProjects + this.id + ',';
				count++;
			}
			
		});
		
		if( selectedProjects.charAt(selectedProjects.length-1) == ','){
			selectedProjects = selectedProjects.slice(0,-1);
		}
		if(count===0)
		{
			showMessage("Please select atleast one Project ",'error'); 

		}else
		{
	if(count===1 || count>1){
			
		//window.location.href='DTTSubsetServlet?param=Add_Projects&projects='+selectedProjects+'&subSetName='+subSetName+'&instanceName='+instanceName+'&recId='+recId;
		
		
		var csrf_token=$('#csrftoken_form').val();
		$.ajax({
			type:'post',
			url:'DTTSubsetServlet',
			data:{param:"Add_Projects",projects:selectedProjects,subSetName:subSetName,instanceName:instanceName,recId:recId,csrftoken_form:csrf_token},
			dataType:'json',
			success:function(data){
				if(data.status == 'success'){
					var subsetId=data.subsetId;
					window.location.href='DTTSubsetServlet?param=view&recId='+subsetId;
					showMessage(data.message,'success');
				}else{
					showMessage(data.message,'error');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
			}
		});
		
		
			}
		}
	});
	
	$('#btnRemoveProjects').click(function(){
		var count=0;
		var selectedProjects='';
		var subSetName=$('#subSetName').val();
		var instanceName = $('#lstInstance').val();
		var recId=$('#recId').val();
		
		
		
		$('#projects-table').find('input[type="checkbox"]:checked').each(function (){
			if(this.id != 'chk_all'){
				selectedProjects = selectedProjects + this.id + ',';
				count++;
			}
			
		});
		
		if( selectedProjects.charAt(selectedProjects.length-1) == ','){
			selectedProjects = selectedProjects.slice(0,-1);
		}
		if(count===0)
		{
			showMessage("Please select atleast one Project ",'error'); 

		}else
		{
	if(count===1 || count>1){
			
		if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')){
			window.location.href='DTTSubsetServlet?param=Remove_Projects&projects='+selectedProjects+'&subSetName='+subSetName+'&instanceName='+instanceName+'&recId='+recId;
		}
		
			}
		}
	});
	
	$('#fetchProjects').click(function(){	
		$('#dmLoader').show();
		$('#fetchProjects').hide();
		var instanceName = $('#lstInstance').val();
		var recId=$('#recId').val();
		window.location.href='DTTSubsetServlet?param=Fetch_Projects_View&instanceName='+instanceName+'&recId='+recId;
	});
});
	