/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  upload_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
*
*/

$(document).ready(function(){
	
	mandatoryFields('upload-form');
	$('#apitype').hide();
	$('#functiontype').hide();
	$('#imgLoad_domain').hide();
	/*Added by Ashiki for TENJINCG-1275 starts*/
	$('#MsgFormat').hide();
	$('#MsgType').hide();
	/*Added by Ashiki for TENJINCG-1275 ends*/
	$('#btnNewFile').click(function(){

		$('#tst-sframe').attr('src','TestDataUploadServlet?param=new');
	});
	/*$('#btnNewFile').click(function(){
			$('#tst-sframe').attr('src','TestDataUploadServlet?param=new');
			$('#tst-sframe').show();
			$('.modalmask').show();
			$('.subframe').show();
			
	});*/
	
	
//$('#lstModules').select2();
	
	var scrStatus = $('#screenStatus').val();
	var scrMessage = $('#screenMessage').val();
	if(scrMessage == ''){
		clearMessages();
	}else{
		if(scrStatus == 'SUCCESS'){
			showMessage(scrMessage, 'success');
		}else{
			showMessage(scrMessage,'error');
		}
	}
	
	$('#btnCancel').click(function(){
		if(window.confirm('Are you sure you want to cancel?')){
			window.location.href="TestDataUploadServlet?param=load_testdatalist";
		}
	});
	
	$('#btnRefresh').click(function(){
		if(window.confirm('Are you sure you want to reset?')){
			window.location.href="TestDataUploadServlet?param=new";
		}
	});
	
	
	$('#validationblock').change(function(){
		var valtype = $(validationType).val();
		
		clearMessages();
		if(valtype == 'Gui'){
			$('#functiontype').show();
			$('#apitype').hide();
			$('#MsgFormat').hide();
		}else if(valtype=='Api'){
			$('#apitype').show();
			$('#functiontype').hide();
			/*Added by Ashiki for TENJINCG-1275 starts*/
			$('#MsgFormat').hide();
		}else if(valtype=='MSG'){
			$('#functiontype').hide();
			$('#apitype').hide();
			$('#MsgFormat').show();
			/*Added by Ashiki for TENJINCG-1275 ends*/
		}
		else{
			$('#apitype').hide();
			$('#functiontype').hide();
			$('#MsgFormat').hide();
		}
	});
	
	
	
	
	
	
	
	/*$('#lstApplication').change(function(){
		var app = $(this).val();
		
		clearMessages();
		if(app == '-1'){
			//$('#moduleInfo').hide();
			$('#lstModules').html("<option value='-1' selected>-- Select One --</option>");
			$('#lstModules').select2('val','-1');
			$('#Apitype').html("<option value='-1' selected>-- Select One --</option>");
			$('#Apitype').select2('val','-1');
			
			
		}else{
				populateModuleInfo(app);
						   
		}
	});
	*/
	
	$('#validationType').change(function(){
		var valtype = $(this).val();
		var app=$('#lstApplication').val();
		clearMessages();
		if(valtype == 'Gui'){
			populateModuleInfo(app);
		/*modified by shruthi api execution testing starts*/
		}else if(valtype == 'Api'){
		/*modified by shruthi api execution testing starts*/
			getallAPIs(app);
		}
		else{/*Added by Ashiki for TENJINCG-1275 starts*/
			populateMessageFormatInfo(app);
			/*Added by Ashiki for TENJINCG-1275 ends*/
		}
	});
	
	 
	
	
	$('#uploadFile').click(function(){
		var appId = $('#lstApplication').val();
		var valType=$('#validationType').val();
		var funcCode = $('#lstModules').val();
		var apiCode = $('#Apitype').val();
		/*Added by Ashiki for TENJINCG-1275 starts*/
		var msgType = $('#msgFormat').val();
	/*Added by Ashiki for TENJINCG-1275 ends*/
		if(appId==''|| appId==-1)
		{
		showMessage("Please select Application", 'error');
		return false;
		}
		else if(valType==''|| valType==-1)
			{
			showMessage("Please select function or API", 'error');
			return false;
			}
		else if(valType=='Gui')
			{
			if(funcCode==''|| funcCode==-1){
				showMessage("Please select function", 'error');
				return false;
			}}
		else if	(valType=='Api')
			{
			if(apiCode==''|| apiCode==-1){
				showMessage("Please select API", 'error');
				return false;
			}/*Added by Ashiki for TENJINCG-1275 starts*/
			}else if	(valType=='MSG')
			{
				if(msgType==''|| msgType==-1){
					showMessage("Please select API", 'error');
					return false;
				}
				}	
		 /*Added by Ashiki for TENJINCG-1275 ends*/
	
		});

	
});

function getallAPIs(appId){
	var app = $('#lstApplication').val();
	var options='';
	$.ajax({
		url:'TestDataUploadServlet',
		data:'param=FETCH_ALL_APIS&app='+app,
		dataType:'json',
		async:false,
		success:function(data){
			var status = data.status;
			if(status == 'SUCCESS'){
				var html = "<option value='-1'>-- Select One --</option>";
				var jarray = data.apis;
				for(i=0;i<jarray.length;i++){
					var json = jarray[i];
					html = html + "<option value='" + json.code + "'>" + escapeHtmlEntities(json.code)  + " - " + escapeHtmlEntities(json.name) + "</option>";
					/*options += '<option value="' +  json.code + '">' + json.name + '</option>';*/
				
				}
				/*options += '<option value="">--Select One--</option>';
				$('#Apitype').append(options);*/
			
				$('#Apitype').html(html);
				
				
			}else{
				showMessage(jsonObj.message,'error');
				
				var html = "<option value='-1'>-- Select One --</option>";
				$('#Apitype').html(html);
				$('#Apitype').val('-1').change();
			}
		}
				
				
	});
	
}



	function populateModuleInfo(appId){
		var jsonObj = fetchModulesForApp(appId);
		
		
		$('#lstModules').val('-1').change();
		$('#lstModules').html('');
		
		var status = jsonObj.status;
		if(status== 'SUCCESS'){
			var html = "<option value='-1'>-- Select One --</option>";
			var jarray = jsonObj.modules;
			for(i=0;i<jarray.length;i++){
				var json = jarray[i];
				html = html + "<option value='" + json.code + "'>" + escapeHtmlEntities(json.code)  + " - " + escapeHtmlEntities(json.name) + "</option>";
			}
			
			$('#lstModules').html(html);
			
			
		}else{
			showMessage(jsonObj.message,'error');
			
			var html = "<option value='-1'>-- Select One --</option>";
			$('#lstModules').html(html);
			$('#lstModules').val('-1').change();
		}
	}
	/*Added by Ashiki for TENJINCG-1275 starts*/
	function populateMessageFormatInfo (appId){

		var jsonObj = fetchMessageFormatForApp(appId);
		
		
		$('#msgFormat').val('-1').change();
		$('#msgFormat').html('');
		
		var status = jsonObj.status;
		if(status== 'SUCCESS'){
			var html = "<option value='-1'>-- Select One --</option>";
			var jarray = jsonObj.messageType;
			for(i=0;i<jarray.length;i++){
				var json = jarray[i];
				html = html + "<option value='" + json.message + "'>" + escapeHtmlEntities(json.message) + "</option>";
			}
			
			$('#msgFormat').html(html);
			
			
		}else{
			showMessage(jsonObj.message,'error');
			
			var html = "<option value='-1'>-- Select One --</option>";
			$('#msgFormat').html(html);
			$('#msgFormat').val('-1').change();
		}
		
	}
	
	function populateMessageTypeInfo (appId){ 


		var jsonObj = fetchMessageFormatForApp(appId);
		
		
		$('#msgType').val('-1').change();
		$('#msgType').html('');
		$('#msgType').show();
		var status = jsonObj.status;
		if(status== 'SUCCESS'){
			var html = "<option value='-1'>-- Select One --</option>";
			var jarray = jsonObj.messageType;
			for(i=0;i<jarray.length;i++){
				var json = jarray[i];
				html = html + "<option value='" + json.message + "'>" + escapeHtmlEntities(json.message) + "</option>";
			}
			
			$('#msgType').html(html);
			
			
		}else{
			showMessage(jsonObj.message,'error');
			
			var html = "<option value='-1'>-- Select One --</option>";
			$('#msgType').html(html);
			$('#msgType').val('-1').change();
		}
		
	
	}
	/*Added by Ashiki for TENJINCG-1275 ends*/
	function validate()
	{

	   var allowedFiles = [".xls", ".xlsx"];
	   var fileUpload = document.getElementById("txtInputFile");
	   var filename=fileUpload.value.toLowerCase();
	   
	   var lblError = document.getElementById("lblError");
	   var regex = new RegExp(allowedFiles.join('|'));
	  
	   
	   if (!regex.test(filename)) {
	       lblError.innerHTML = "Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.";
	      
	       return false;
	   }
	   lblError.innerHTML = "";
	   $('#imgLoad_domain').show();
	   return true;

	   return( true );
	}
	

