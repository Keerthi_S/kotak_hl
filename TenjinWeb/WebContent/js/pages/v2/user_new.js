/*Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  user_new.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

*//*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 03-11-2017				Roshni Das		Newly added for TENJINCG-398
 08-01-2017             Padmavathi              for TENJINCG-578
 14-06-2018             Padmavathi              T251IT-15
 25-03-2019				Pushpalatha				TENJINCG-968

*/
		
		$(document).on('click', '#btnBack', function() {
			var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
			if(confirmResult) {
				window.location.href='TenjinUserServlet';
			}else{
				return false;
			}
		});
		$(document).ready(function(){
			/*Added by Pushpalatha for TENJINCG-968 starts*/
			mandatoryFields('userForm');
			/*Added by Pushpalatha for TENJINCG-968 ends*/
			
			$('#btnSave').click(function(){
			/*Added by Padmavathi for T251IT-15 starts*/
			var userId=	$('#id').val().trim();
			if(userId!=""){
				if(/[^a-zA-Z0-9_]/.test(userId)) {
					 showMessage("Only characters,numbers and _ symbol are allowed for User Id","error");
					 return false;
				}
			}	
			/*Added by Padmavathi for T251IT-15 ends*/
				if($('#email').val()!=''){ 
					if(!validateEmail()){
						showMessage("Please enter valid email address","error");
						 return false;
						}
				}
			
			var primaryPhone=$('#primaryPhone').val();
			if(primaryPhone!=""){
				if (/[a-zA-Z!@#$%&*?<^|(){}>=_`~,]/.test(primaryPhone)){
					 showMessage("Please enter valid contact number.","error");
					 return false;
				}
				if(primaryPhone.length>6){
					var phoneno = new RegExp(/^((\+)?[0-9]{1,3})?([-\s\.])?(\(\d{1,11}\))$/g);
					if((primaryPhone.match(phoneno)))
			        {  
						showMessage("Please enter valid contact number.");
						 return false;
			        }
					
				}
				else{
					showMessage("Contact number should be atleast 6 digits.");
					 return false;
				}

				}
				/*added by Prem for VAPT issue fix starts*/
			var pwdObj = document.getElementById('password');
			if(!CheckPassword(pwdObj)){
				/*var hashObj = new jsSHA("SHA-256", "TEXT", {numRounds: 1});
				hashObj.update(pwdObj.value);
				var hash = hashObj.getHash("HEX");
				pwdObj.value = hash;
				 $('#password').val(hash);*/
				showMessage("Please check the password rules","error");
				return false;
			}
			/*else{
				showMessage("Please check the password rules","error");
				return false;
			}*/
			/*added by Prem for VAPT issue fix ends*/
			
			});
			
			});
		
		function validateEmail() {
		    var email = document.forms["userForm"]["email"].value;
		    var atstr = email.indexOf("@");
		    var dotstr = email.lastIndexOf(".");
		    if (atstr<1 || dotstr<atstr+2 || dotstr+2>=email.length) {
		      
		        return false;
		    }
		    else{
		    	clearMessages();
		    	 return true;
		    }
		}
		/*added by Prem for VAPT issue fix starts*/
		function CheckPassword(inputtxt) 
		{ 
		var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
		if(inputtxt.value.match(decimal)) 
		{ 
		return true;
		}
		else
		{ 
			 return false;
		}
		} 
		
		function hasWhiteSpace(s) 
		{
		    var reWhiteSpace = new RegExp("/\s/");

		    // Check for white space
		    if (reWhiteSpace.test(s)) {
		        //alert("Please Check Your Fields For Spaces");
		        return false;
		    }

		    return true;
		}
		
		/*added by Prem for VAPT issue fix starts*/
		