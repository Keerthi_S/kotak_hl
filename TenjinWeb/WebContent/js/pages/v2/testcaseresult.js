/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testcaseresult2.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright Â© 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 16-10-2018			Sriram					File optimized for TENJINCG-876
 30-10-2018			Sriram					TENJINCG-886
 03-07-2019         Leelaprasad             TJN27-27
 18-11-2020			Pushpa						TENJINCG-1228
*/

var testCase = {};

function initialize() {
	var json = $('#json-data').val();
	testCase = JSON.parse(json);
	paintStepSummary();
	adjustHeights();
	$("#steps_table").dataTable({
		dom : "<'container-fluid'"
            + "<'row'"
            + "<'col-md-12 y-dt-buttons'B>>"
            + "<'row'<'col-md-12 y-dt-search'"
            + ""
            + ">"
            + ">"
            + "<'row dt-table'" + "<'col-md-12 y-dt-table'tr>" + ">"
            + "<'row'"
            + "<'col-md-12 mt-1 y-dt-table-info-block'"
            + "<'y-dt-table-length-change'l>"
            + "<'y-dt-table-info'i>"
            + "<'y-dt-table-pagination'p>"
            + ">"
            + ">"
            + ">"
	});
}

function paintStepResultDetails(step) {
	var $container = $("#test_step_result");
	$container.html("");
	
	paintStepDetailInformation(step, $container);
	var iterationResultContainer = $("<div id='iteration_results' />").appendTo($container);
	if(step.detailedResults && step.detailedResults.length === 1) {
				/*Modified by Pushpa for Tenj210-120 starts*/
		
		paintIterationResult(step.detailedResults[0],step.recordId,step.id,iterationResultContainer,"",step.txnMode);
			/*Modified by Pushpa for Tenj210-120 ends*/
	
	}else if(step.detailedResults && step.detailedResults.length>1){

		console.log('more than one iteration');
		/*Modified by Pushpa for Tenj210-120 starts*/
		paintIterationTable(step.detailedResults,step.recordId,step.id,iterationResultContainer,"",step.txnMode);
		/*Modified by Pushpa for Tenj210-120 ends*/
	}
}

function paintStepDetailInformation(step, $container) {
	var $row = $("<div class='row' />").appendTo($container);
	var $leftCol = $("<div class='col-md-6' />").appendTo($row);
	var $rightCol = $("<div class='col-md-6' />").appendTo($row);
	
	var $leftTable = $("<table class='layout' />").appendTo($leftCol);
	var $rightTable = $("<table class='layout' />").appendTo($rightCol);
	
	var $appRow = $("<tr />").appendTo($leftTable);
	$appRow.append($("<td class='layout-label' width='110px'>Application</td>"));
	$appRow.append($("<td class='layout-value'>" + (step.appName ? step.appName : "Not Available") + "</td>"));
	
	var $funcRow = $("<tr />").appendTo($leftTable);
	$funcRow.append($("<td class='layout-label' width='110px'>" + (step.txnMode === 'GUI' ? "Function" : "API") + "</td>"));
	$funcRow.append($("<td class='layout-value'>" + (step.moduleCode ? step.moduleCode : "Not Available") + "</td>"));
	
	var $validationTypeRow = $("<tr />").appendTo($rightTable);
	$validationTypeRow.append($("<td class='layout-label' width='110px'>Validation Type</td>"));
	$validationTypeRow.append($("<td class='layout-value'>" + step.validationType + "</td>"));
	
	var $operationRow = $("<tr />").appendTo($rightTable);
	$operationRow.append($("<td class='layout-label' width='100px'>Operation</td>"));
	$operationRow.append($("<td class='layout-value'>" + step.operation + "</td>"));
	
	var $loginType = $("<tr />").appendTo($rightTable);
	$loginType.append($("<td class='layout-label' width='100px'>Login Type</td>"));
	$loginType.append($("<td class='layout-value'>" + (step.autLoginType ? step.autLoginType : "Not Available") + "</td>"));
	/*Modified by Prem for Tenj210-75 starts*/
	/*var $browserRow = $("<tr />").appendTo($rightTable);
	$browserRow.append($("<td class='layout-label' width='100px'>Browser</td>"));
	$browserRow.append($("<td class='layout-value'>" + (step.browserType ? step.browserType : "Not Available") + "</td>"));*/
	
	/*var $expResultCol = $("<div class='col-md-12' />").appendTo($("<div class='row' />").appendTo($container));
	var $expResultRow = $("<tr />").appendTo($("<table class='layout' />").appendTo($expResultCol));
	$expResultRow.append($("<td class='layout-label' width='110px'>Expected Result</td>"));
	$expResultRow.append($("<td class='layout-value'>" + (step.expectedResult ? step.expectedResult : "Not Available") + "</td>"));*/
	
	var expResultRow = $("<tr />").appendTo($leftTable );
	expResultRow.append($("<td class='layout-label' width='100px'>Expected Result</td>"));
	expResultRow.append($("<td class='layout-value'>" + (step.expectedResult ? step.expectedResult : "Not Available") + "</td>"));
	/*Modified by Prem for Tenj210-75 Ends*/
}
/*Modified by Pushpa for Tenj210-120 starts*/
function paintIterationResult(detailedResult,stepRecordId,stepId, $container,detailedResults,txnMode) {
	/*Modified by Pushpa for Tenj210-120 ends*/
	if($("#iteration_results").length>0 && $("#iteration_results").length>0){
		$("#iteration_results").empty();
	}
	$container.append("<hr />");
	if(detailedResults){
		$container.append
		($("<div class='test_case_summary_title'>Results for Iteration " + detailedResult.iterationNo + "</div>").append
		($("<input type='button' value='Back' class='imagebutton back back-iteration-table' style='margin-left:250px;background-color:#ecf0f1;border:none;'>")))
	}else{
		$container.append($("<div class='test_case_summary_title'>Results for Iteration " + detailedResult.iterationNo + "</div>"));
	}
	
	var $iterTable = $("<table class='layout' />").appendTo($container);
	var $firstRow = $("<tr />").appendTo($iterTable);
	
	var statusObj= chooseStatusIcon(detailedResult);
	var statusText =statusObj.statusText; 
	var statusIcon =statusObj.statusIcon;
	$firstRow.append($("<td class='layout-label' width='60px'>Status</td>"));
	$firstRow.append($("<td class='layout-value' width='50px' title='" + statusText + "' style='text-align:center;'><img src='" + statusIcon + "' /></td>"));
	$firstRow.append($("<td class='layout-label' width='60px'>TDUID</td>"));
	/*Modified by pushpa for TENJINCG-1106 starts*/
	/*$firstRow.append($("<td class='layout-value'>" + detailedResult.dataId + "</td>"));*/
	$firstRow.append($("<td class='layout-value'>" + detailedResult.tduid + "</td>"));
	/*Modified by pushpa for TENJINCG-1106 ends*/
	$firstRow.append($("<td class='layout-label' width='110px'>Elapsed Time</td>"));
	$firstRow.append($("<td class='layout-value'>" + detailedResult.elapsedTime + "</td>"));
	var $secondRow = $("<tr />").appendTo($iterTable);
	$secondRow.append($("<td class='layout-label' width='60px'>Message</td>"));
	$secondRow.append($("<td class='layout-value' colspan='5'>" + detailedResult.message + "</td>"));
	/*Modified by Pushpa for Tenj210-120 starts*/
	if(txnMode=='API'){
	var $thirdRow = $("<tr />").appendTo($iterTable);
	$thirdRow.append($("<td class='layout-label' width='60px'>Request</td>"));
	$thirdRow.append($("<td class='layout-value' width='60px'  style='text-align:center;'>" +
			"<a href=ResultsServlet?param=api_request&run="+detailedResult.runId+"&tsrecid="+stepRecordId+"&tduid="+detailedResult.tduid+">Download</a></td>"));
	$thirdRow.append($("<td class='layout-label' width='60px'>Response</td>"));
	/*<a href='ResultsServlet?param=api_response&run=${run.id }&tsrecid=${step.recordId}&tduid=${iteration.dataId}'>Download</a>*/

	$thirdRow.append($("<td class='layout-value'>" +
			"<a href=ResultsServlet?param=api_response&run="+detailedResult.runId+"&tsrecid="+stepRecordId+"&tduid="+detailedResult.tduid+">Download</a></td>"));
	
	}
	
	
	
	/*Modified by Pushpa for Tenj210-120 ends*/
	/*Added by khalid for TENJINCG-1194 starts*/
	/*Added by Pushpa for TENJINCG-1228 starts*/
	if(txnMode=='API'){
	paintApiRequestLog(detailedResult,$container);
	/*Added by Pushpa for TENJINCG-1228 ends*/
	}
	paintRuntimeValues(detailedResult,$container);
	paintValidationResult(detailedResult,$container);	
		
	var $screenshotTable = $("<table class='layout'/>");
	$screenshotTable.append($("<td class='layout-label' width='60px'>Screenshots</td>"));
	/*Modified by pushpa for TENJINCG-1106 starts*/
	/*$screenshotTable.append($("<td class='layout-value sreenshotdata'></td>").append([$("<a href='#' class='iteration_screenshot' data-run-id="+detailedResult.runId+" data-step-rec-id="+stepRecordId+" data-tduid="+detailedResult.dataId+" data-step-id="+stepId+" data-iteration="+detailedResult.iterationNo+">View</a>")]));*/
	$screenshotTable.append($("<td class='layout-value sreenshotdata'></td>").append([$("<a href='#' class='iteration_screenshot' data-run-id="+detailedResult.runId+" data-step-rec-id="+stepRecordId+" data-tduid="+detailedResult.tduid+" data-step-id="+stepId+" data-iteration="+detailedResult.iterationNo+">View</a>")]));
	/*Modified by pushpa for TENJINCG-1106 ends*/
	$container.append($screenshotTable);
	
	$(".iteration_screenshot").click(function(){
		$(".sreenshotdata").append($("<img src='images/inprogress.gif' id='img_screenshoot_loader' style='margin-left:5px;'/>"));
		 var tsId = $(this).data('step-id');
		    var ino = $(this).data('iteration');
		    var runId = $(this).data('run-id');
		    var tduid = $(this).data('tduid');
		    var tsRecId = $(this).data('step-rec-id');
		    window.location.href='ResultsServlet?param=view_screenshots&stepid=' + tsId + '&txn=' + ino + '&run=' +runId + '&tsrecid='+tsRecId + '&tduid=' + tduid;
	})
	
	$(".back-iteration-table").click(function(e){
		e.preventDefault();
		console.log('clicked');
		/*Modified by Pushpa for Tenj210-120 starts*/
		paintIterationTable(detailedResults,stepRecordId,stepId,$container);
		/*Modified by Pushpa for Tenj210-120 ends*/
	})
}
		/*Modified by Pushpa for Tenj210-120 starts*/

function paintIterationTable(detailedResults,stepRecordId,stepId,$container,txnMode){
		/*Modified by Pushpa for Tenj210-120 starts*/

	if($("#iteration_results").length>0 && $("#iteration_results").length>0){
		$("#iteration_results").empty();
	}
	
	$container.append($("<hr/>"));
	$container.append($("<div class='test_case_summary_title'>Iteration Results</div>"));
	var $table = $("<table class='summtable' style='width:80%;'/>");
	var $thead = $("<thead/>");
	$thead.append($("<th>txn no</th>"));
	$thead.append($("<th>status</th>"));
	$thead.append($("<th>tduid</th>"));
	var $tbody = $("<tbody/>");
	/*Modified by Ashiki for Tenj210-155 starts*/
	for(detailedResult of detailedResults){
	/*Modified by Ashiki for Tenj210-155 ends*/
		var statusObj = chooseStatusIcon(detailedResult);
		var statusText = statusObj.statusText;
		var statusIcon = statusObj.statusIcon;
		var $tr = $("<tr class='iteration-table-row' data-iteration-no="+detailedResult.iterationNo+"/>");
		$tr.append($("<td/>").append(detailedResult.iterationNo));
		$tr.append($("<td class='layout-value' title='" + statusText + "'><img src='" + statusIcon + "' /></td>"));
		/*Modified by pushpa for TENJINCG-1106 starts*/
		/*$tr.append($("<td/>").append(detailedResult.dataId));*/
		$tr.append($("<td/>").append(detailedResult.tduid));
		/*Modified by pushpa for TENJINCG-1106 ends*/
		$tbody.append($tr);
	}
	$table.append($thead);
	$table.append($tbody);
	$container.append($table);
	$(".iteration-table-row").click(function(){
		var iterationNo = $(this).data('iteration-no');
		/*Modified by Ashiki for Tenj210-155 starts*/
		detailedResult = detailedResults.filter(function (dr) { return dr.iterationNo == iterationNo});
		/*Modified by Ashiki for Tenj210-155 ends*/
		/*Modified by Pushpa for Tenj210-120 starts*/
		paintIterationResult(detailedResult[0],stepRecordId,stepId,$container,detailedResults,txnMode);
		/*Modified by Pushpa for Tenj210-120 starts*/
	})
}

function chooseStatusIcon(detailedResult){
	let statusIcon = "";
	let statusText = "";
	switch(detailedResult.result) {
	case "S":
		statusIcon = "images/success_20c20.png";
		statusText = "Pass";
		break;
	case "F":
		statusIcon = "images/failure_20c20.png";
		statusText = "Fail";
		break;
	case "E":
		statusIcon = "images/error_20c20.png";
		statusText = "Error";
		break;
	case "X":
		statusIcon = "images/ajax-loader.gif";
		statusText = "Executing";
		break;
	default:
		statusIcon = "images/queued_20c20.png";
		statusText = "Queued";
	}
	return {statusText:statusText,statusIcon:statusIcon};
}
/*Added by Pushpa for TENJINCG-1228 starts*/
function paintApiRequestLog(detailedResult,$container){
	
			$container.append("<hr />");
			$container.append($("<div class='test_case_summary_title' style='font-size:12px;'>Request Logs</div>"));
			var runtimeValuesArr = JSON.parse(detailedResult.apiRequestLog);
			
			var headerJSON = runtimeValuesArr.headers;
			
			var $table = $("<table class='summtable' id='' style='width:80%;'/>");
				var $tbody = $("<tbody/>");
				 var $tr = $("<tr/>");
				 for (var key in headerJSON) {
					 var $tr = $("<tr/>");
					 $tr.append($("<td/>").append(key));
						$tr.append($("<td/>").append( headerJSON[key]));
						$tbody.append($tr);
					
					}
				 $tr = $("<tr/>");
				 $tr.append($("<td/>").append("URL"));
					$tr.append($("<td/>").append(runtimeValuesArr.requestUrl));
					$tbody.append($tr);
				$tr = $("<tr/>");
				  $tr.append($("<td/>").append("Media type"));
					$tr.append($("<td/>").append(runtimeValuesArr.requestMediaType));
					$tbody.append($tr);
				$tr = $("<tr/>");
					$tr.append($("<td/>").append("Method"));
						$tr.append($("<td/>").append(runtimeValuesArr.method));
						$tbody.append($tr);
			
				 
				$table.append($tbody);
				$container.append($table);
			
		
}
/*Added by Pushpa for TENJINCG-1228 ends*/

function paintRuntimeValues(detailedResult,$container){
	$.ajax({
		url:'RunAjaxServlet',
		/*Modified by pushpa for TENJINCG-1106 starts*/
		/*data :{param:"run_time_values",tsrecid:detailedResult.tsRecId,run:detailedResult.runId,tduid:detailedResult.dataId},*/
		data :{param:"run_time_values",tsrecid:detailedResult.tsRecId,run:detailedResult.runId,tduid:detailedResult.tduid},
		/*Modified by pushpa for TENJINCG-1106 ends*/
		async:false,
		dataType:'json',
		success: function(data){
			$container.append("<hr />");
			$container.append($("<div class='test_case_summary_title' style='font-size:12px;'>Run time values</div>"));
			var runtimeValuesArr = JSON.parse(data.runtimevalues);
			if(runtimeValuesArr.length>0){
				console.log(data.runtimevalues.length)
			var $table = $("<table class='summtable' id='run_time_values_table' style='width:80%;'/>");
			var $thead = $("<thead/>");
			$thead.append($("<th>Field</th>"));
			$thead.append($("<th>Value</th>"));
			$table.append($thead);
			var $tbody = $("<tbody/>");
			
				console.log('inside runtime values if block');
				console.log(JSON.parse(data.runtimevalues));
				
				//FIx for Tenj210-136
				// for(runtime in runtimeValuesArr){
				// 	var $tr = $("<tr/>");
				// 	$tr.append($("<td/>").append(runtime.field));
				// 	$tr.append($("<td/>").append(runtime.value));
				// 	$tbody.append($tr);
				// }


				runtimeValuesArr.forEach(function(runtime) {
					var $tr = $("<tr/>");
					$tr.append($("<td/>").append(runtime.field));
					$tr.append($("<td/>").append(runtime.value));
					$tbody.append($tr);
				});
				//FIx for Tenj210-136 ends

				//console.log(data);
				$table.append($tbody);
				$container.append($table);
			}else{
				var $p = $("<p style='margin-left:1rem;'>No data is available</p>");
				$container.append($p);
			}
			
		},
		error:function(xhr, textStatus, errorThrown){
			console.log(errorThrown);
			//showMessage(errorThrown, 'error');			
		}
	});
}

function paintValidationResult(detailedResult,$container){
	$.ajax({
		url:'RunAjaxServlet',
		data :{param:"validation_results",tsrecid:detailedResult.tsRecId,run:detailedResult.runId,txn:detailedResult.iterationNo},
		async:false,
		dataType:'json',
		success: function(data){
			$container.append("<hr />");
			$container.append($("<div class='test_case_summary_title' style='font-size:12px;'>Validation results</div>"));
			var validationresultJson = JSON.parse(data.validationresult);
			var validationResults = validationresultJson.validationResults;
			if(validationResults.length>0){
			var $table = $("<table class='summtable' id='validation_results_table' style='width:100%;'/>");
			var $thead = $("<thead/>");
			$thead.append($("<th>Page</th>"));
			$thead.append($("<th>Field</th>"));
			$thead.append($("<th>Expected Value</th>"));
			$thead.append($("<th>Actual Value</th>"));
			$thead.append($("<th>Status</th>"));
			$table.append($thead);
			var $tbody = $("<tbody/>");

			//Fix for Tenj210-137
				// for(result in validationResults){
				// 	var $tr = $("<tr/>");
				// 	$tr.append($("<td/>").append(result.page));
				// 	$tr.append($("<td/>").append(result.field));
				// 	$tr.append($("<td/>").append(result.expectedValue));
				// 	$tr.append($("<td/>").append(result.actualValue));
				// 	$tr.append($("<td/>").append(result.status));
				// 	$tbody.append($tr);
				// }

				validationResults.forEach(function(result) {
					var $tr = $("<tr/>");
					$tr.append($("<td/>").append(result.page));
					$tr.append($("<td/>").append(result.field));
					$tr.append($("<td/>").append(result.expectedValue));
					$tr.append($("<td/>").append(result.actualValue));
					$tr.append($("<td/>").append(result.status));
					$tbody.append($tr);	
				});
				$table.append($tbody);
				$container.append($table);
				//Fix for Tenj210-137 ends
			}else{
				var $p = $("<p style='margin-left:1rem;'>No data is available</p>");
				$container.append($p);
			}
			
			
		},
		error:function(xhr, textStatus, errorThrown){
			console.log(errorThrown);
			//showMessage(errorThrown, 'error');			
		}
	});
}

/*Added by khalid for TENJINCG-1194 ends*/
function adjustHeights() {
	var titleHeight = $(".title").get(0).offsetHeight;
	var toolbarHeight = $(".toolbar").get(0).offsetHeight;
	var headerRowHeight = $("#report-header-row").get(0).offsetHeight;
	var frameHeight = window.parent.document.getElementById("content_frame").offsetHeight;
	var footerHeight = window.parent.document.querySelector(".footer").offsetHeight;
	var targetHeight = frameHeight - (headerRowHeight + toolbarHeight + titleHeight) - 4 ;
	$("#steps-list, #step-details").css("height", targetHeight + "px");
	$("#steps-list, #step-details").css("max-height", targetHeight + "px");
	$("#steps-list, #step-details").css("overflow-y", "auto");
}

function paintStepSummary() {
	if(!testCase) return false;
	
	var statusMap = {
			Pass:{
				count: 0,
				color: 'success'
			},
			Fail: {
				count: 0,
				color: 'danger'
			},
			Error: {
				count: 0,
				color: 'warning'
			},
			"Not Executed": {
				count: 0,
				color: 'default'
			}
	};
	
	if(testCase.tcSteps) {
		testCase.tcSteps.forEach(function(step) {
			statusMap[step.status].count = statusMap[step.status].count+1;
		});
	}
	
	
	Object.keys(statusMap).forEach(function (key)  {
		if(statusMap[key].count > 0) {
			$("<span title='" + key + "' style='margin-left:2px; margin-right: 2px;' class='label label-" + statusMap[key].color + "'>" + statusMap[key].count + "</span>").appendTo($("#stepSummaryLabels"));
		}
	});
}

window.addEventListener("resize", adjustHeights);

$(document).ready(function() {
	initialize();
	//Event Listeners
	$('#btnBack').click(function() {
		var runId = $('#h-runId').val();
		var callback = $('#h-callback').val();
		
		window.location.href='ResultsServlet?param=run_result&run=' + runId + '&callback=' + callback;
	});
	
	$(".test-step-detail-trigger").click(function(e) {
		e.preventDefault();
		$("#steps_table tr.selected").removeClass("selected");
		
		$(this).closest("tr").addClass("selected");
		
		var stepRecordId = $(this).data("stepId");
		var stepMode = $(this).data("stepMode");
		if(testCase && testCase.tcSteps) {
			testCase.tcSteps.forEach(function(step) {
				if(step.recordId === stepRecordId) {
					paintStepResultDetails(step);
				}
			});
		}
	});
});

$(document).on('click', '.modalmask', function() {
	closeModal();
});

$(document).on('click','tr.uival-page', function() {
	$('.modalmask').show();
	$('#ui-field-results').show();
	
	var valType = $(this).data('valType');
	var pageArea = $(this).data('pageName');
	var status = $(this).data('status');
	
	$('#valName').text(valType);
	$('#valName').removeClass('Pass');
	$('#valName').removeClass('Fail');
	$('#valName').addClass(status);
	$('#pageName').text(pageArea);
	
	
	var valResultsString = $(this).data('fieldResults');
	var results = JSON.parse(valResultsString);
	
	/*$('#valResultsTable').text(valResultsString);*/
	
	var $tbody = $("<tbody />");
	for(var i=0; i<results.length; i++) {
		var result = results[i];
		var $tr = $("<tr />");
		$tr.append($("<td>" + result.field + "</td>"));
		$tr.append($("<td>" + result.expectedValue + "</td>"));
		$tr.append($("<td>" + result.actualValue + "</td>"));
		$tr.append($("<td>" + getImageForStatus(result.status) + "</td>"));
		$tbody.append($tr);
	}
	$('#valResults').find('tbody').remove();
	
	$('#valResults').append($tbody);
	
});

$(document).on('click', '#btnUIClose', function() {
	$('.modalmask').hide();
	$('#ui-field-results').hide();
});

function showUIResults(stepRecordId, runId) {
	var $loaderDiv = $("<div class='ajx-loader-block' />");
	$loaderDiv.text('Loading. Please Wait...');
	$('#uiValDetail').html('');
	$('#uiValDetail').append($loaderDiv);
	$('#uiValDetail').show();
	$('#tranDetail').hide();
	
	$.ajax({
		url: 'ResultsServlet?param=ui_val_result&run='+ runId + '&srecid=' + stepRecordId,
		dataType:'json',
		async:true,
		success:function(response) {
			if(response.status === 'SUCCESS' || response.status === 'success') {
				//$('#uiValDetail').html(JSON.stringify(response.uiresults));
				paintUIResults(response.uiresults);
				$loaderDiv.remove();
			}else{
				showMessage(response.message,'error');
				$loaderDiv.remove();
			}
		},
		error:function(xhr, textStatus, errorThrown) {
			showMessage('An internal error occurred. Please contact Tenjin Support', 'error');
			$loaderDiv.remove();
		}
	});
	
}

function paintUIResults(uiresults) {
	var $accordionBlock = $("<div id='uiValAccordion' />");
	$.each(uiresults, function(key, value) {
		var $accordionHeader = $("<div class='acc-head'> /");
		var $title = $("<span class='acc-head-title' />");
		$title.text(key);
		
		var $summary=$("<span class='acc-head-summary' />");
		
		
		$accordionHeader.append($title);
		$accordionHeader.append($summary);
		
		$accordionBlock.append($accordionHeader);
		
		var $accordionContent = $("<div />");
		var $accordionTableContainer = $("<div class='ui-val-page-area' />");
		var $table = $("<table class='summtable' />");
		var $thead = $("<thead><tr><th>Page Area Name</th><th>Total Fields</th><th>Passed</th><th>Failed</th><th>Status</th></tr></thead>");
		$table.append($thead);
		
		var summaryTotal=0;
		var summaryPass=0;
		var summaryFail=0;
		var summaryStatus = 'Pass';
		
		var $tbody = $("<tbody />");
		$.each(value, function(pageName, validationsOnPage){
			var $tr = $("<tr />");
			var $pageNameTd = $("<td>" + pageName + "</td>");
			var $totalTd = $("<td />");
			var $passTd = $("<td />");
			var $failTd = $("<td />");
			var $statusTd = $("<td />");
			
			var total=0;
			var pass=0;
			var fail=0;
			var status = "<img src='images/success_20c20.png' />";
			
			var valResults = [];
			for(var i=0; i<validationsOnPage.length; i++) {
				var validation = validationsOnPage[i];
				var recId = validation.recordId;
				for(var j=0; j<validation.results.length; j++) {
					var result = validation.results[j];
					result.recordId = recId;
					if(result.status==='Pass') {
						pass++;
						summaryPass++;
					}else if(result.status === 'Fail') {
						fail++;
						summaryFail++;
						status = "<img src='images/failure_20c20.png' />";
						summaryStatus = 'Fail';
					}
					
					total++;
					summaryTotal++;
					valResults.push(result);
				}
			}
			
			$totalTd.text(total);
			$passTd.text(pass);
			$failTd.text(fail);
			$statusTd.html(status);
			$tr.append($pageNameTd);
			$tr.append($totalTd);
			$tr.append($passTd);
			$tr.append($failTd);
			$tr.append($statusTd);
			if(fail > 0) {
				$tr.data('status', 'Fail');
			}else{
				$tr.data('status','Pass');
			}
			$tr.data('valType', key);
			$tr.data('pageName', pageName);
			$tr.data('fieldResults', JSON.stringify(valResults));
			$tr.addClass('uival-page')
			$tbody.append($tr);
		});
		var summary='';
		if(summaryPass > 0) {
			summary += summaryPass + ' Passed';
			if(summaryFail < 1){
				summary+= ' ';
			}
		}
		
		if(summaryFail > 0) {
			if(summaryPass > 0) {
				summary+= ', ';
			}
			summary += summaryFail + ' Failed ';
		}
		
		summary+= ' out of ' + summaryTotal + ' ';
		if(summaryTotal > 0 && summaryTotal < 2) {
			summary+= 'Field';
		}else{
			summary += 'Fields';
		}
		$summary.text(summary);
		if(summaryStatus === 'Fail') {
			$summary.css('color', 'red');
		}else{
			$summary.css('color', 'green');
		}
		$table.append($tbody);
		$accordionTableContainer.append($table);
		$accordionContent.append($accordionTableContainer);
		$accordionBlock.append($accordionContent);
		
	});
	
	$('#uiValDetail').append($accordionBlock);
	$accordionBlock.accordion({heightStyle:'content',header: ".acc-head", collapsible: true, active: false});
}

//TENJINCG-886 (Sriram)
$(document).on('click', '#report-gen', function() {
	$('.modalmask').show();
	$('#screenShotOption').show();
});


$(document).on('click', '#btnYes', function() {
	$('.modalmask').hide();
	$('#screenShotOption').hide();
	reportGeneration('true');
});
$(document).on('click', '#btnNo', function() {
	$('.modalmask').hide();
	$('#screenShotOption').hide();
	reportGeneration('false');
});

function reportGeneration(screenShotOption){
	var runId = $('#h-runId').val();
	window.location.href="ResultsServlet?param=TEST_REPORT_GEN&runid=" + runId+"&screenShotOption="+screenShotOption;
	/*$('#report-gen').prop('disabled',true);
	$('#img_report_loader').show();
	$.ajax({
		url:'ResultsServlet',
		data:'param=TEST_REPORT_GEN&runid=' + runId+'&screenShotOption='+screenShotOption,
		async:true,
		dataType:'json',
		success:function(data){
			var status = data.status;
			if(status == 'SUCCESS'){
				var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
				$("body").append($c);
                $("#downloadFile").get(0).click();
                $c.remove();
                $('#report-gen').prop('disabled',false);
        		$('#img_report_loader').hide();
			}else{
				var message = data.message;
				showMessage(message,'error');
				$('#report-gen').prop('disabled',false);
				$('#img_report_loader').hide();
			}
		},
		error:function(xhr, textStatus, errorThrown){
			$('#report-gen').prop('disabled',false);
			$('#img_report_loader').hide();
			showMessage(errorThrown, 'error');
		}
	});*/
}//TENJINCG-886 (Sriram) ends