 /***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:   project_dashboard.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 19-02-2019			Padmavathi		        Newly added for TENJINCG-924
 * 21-02-2019           Padmavathi              TENJINCG-924
 * 03-05-2019			Preeti					TENJINCG-1019
 * 12-06-2019			Preeti					V2.8-103
 
 */
$(document).ready(function(){
	$('#defect_table').hide();
	$('#line_chart_week').hide();
	drawPieChart();
	generateLineChartForDayWise();
	var fetchData=true;
	$('#view').on('change', function() {
		if($(this).val()=='1'){
			$('#line_chart_week').hide();
			$('#line_chart_day').show();
		}
		else{
			if(fetchData==true){
				generateReportForWeeklyWise();
			}
			fetchData=false;
			$('#line_chart_day').hide();
			$('#line_chart_week').show();
		}
	});
	$(document).on('click', '#btnDefects', function() {
		$('#runs_table').hide();
		$('#defect_table').show();
	});
	$(document).on('click', '#btnRuns', function() {
		$('#defect_table').hide();
		$('#runs_table').show();
		
	});
	/*Added by Preeti for TENJINCG-1019 starts*/
	var selexStatusTS = $('#exStatusTS').val();
	if(selexStatusTS){
		selexStatusTS = selexStatusTS.replace('[', '');
		selexStatusTS = selexStatusTS.replace(']', '');
		var values = selexStatusTS.split(",");
		var exStatusId = '#exStatusRun';
		getRunsForTestSet(values,exStatusId);
	}
	var selDefTS = $('#defSevTS').val();
	if(selDefTS){
		selDefTS = selDefTS.replace('[', '');
		selDefTS = selDefTS.replace(']', '');
		var valuesDef = selDefTS.split(",");
		var defSevId = '#defSevRun';
		getRunsForTestSet(valuesDef,defSevId);
		var defStatusId = '#defStatusRun';
		getRunsForTestSet(valuesDef,defStatusId);
	}
	$('#exStatusTS').on('change', function() {
		var selexStatusTS = $('#exStatusTS').val();
		selexStatusTS = selexStatusTS.replace('[', '');
		selexStatusTS = selexStatusTS.replace(']', '');
		var values = selexStatusTS.split(",");
		var exStatusId = '#exStatusRun';
		getRunsForTestSet(values,exStatusId);
		generatePieChartForRun();
	});
	
	$('#exStatusRun').on('change',function(){
		generatePieChartForRun();
	});
	$('#exTrendTS').on('change',function(){
		generateLineChartForDayWise();
		$('#view').val(1);
		$('#line_chart_week').hide();
		$('#line_chart_day').show();
		fetchData=true;
	});
	$('#defSevRun').on('change',function(){
		var selRunId = $('#defSevRun').val();
		var chartId = 'barchart1';
		generateBarChartForDefect(selRunId,chartId,'severity','#B22222','Severity');
	});
	
	$('#defStatusRun').on('change',function(){
		var selRunId = $('#defStatusRun').val();
		var chartId = 'barchart2';
		generateBarChartForDefect(selRunId,chartId,'status','#00CED1','Status');
	});
	$('#defSevTS').on('change', function() {
		var selDefTS = $('#defSevTS').val();
		selDefTS = selDefTS.replace('[', '');
		selDefTS = selDefTS.replace(']', '');
		var values = selDefTS.split(",");
		var defSevId = '#defSevRun';
		getRunsForTestSet(values,defSevId);
	});
	
	var defectsSev = $('#defectsSeverity').val();
	var chartIdForDefectSeverity = 'barchart1';
	if(defectsSev)
		getMapForDefect(defectsSev,chartIdForDefectSeverity,'#B22222','Severity');
	
	var defectsStatus = $('#defectsStatus').val();
	var chartIdForDefectStatus = 'barchart2';
	if(defectsStatus)
		getMapForDefect(defectsStatus,chartIdForDefectStatus,'#00CED1','Status');
});

function getMapForDefect(defectId,chartId,color,xLabel){
	defectId = defectId.replace('{', '');
	defectId = defectId.replace('}', '');
	var values = defectId.split(",");
	var map = {};
	var array =[];
	for(var i=0; i<values.length;i++){
		array.push(values[i]);
	}
	var arrayVal =[];
	for(var i=0; i<array.length;i++){
		arrayVal=array[i].trim().split("=");
		map[arrayVal[0]] = arrayVal[1];			
	}
	var ticks=[];
	var count =[];
	for(var key in map){
		ticks.push(key);
		count.push(map[key]);
	}
	drawBarChartForDefect(count,ticks,chartId,color,xLabel);
}
function generatePieChartForRun(){
	var myNode = document.getElementById("pie_chart");
	myNode.innerHTML = '';
	var selRunId = $('#exStatusRun').val();
	$.ajax({
		url		: 'ProjectDashBoardServlet',
		type	: 'GET',
		data	: {'t' :'run_details',param:selRunId},
		dataType: 'json',
		async:true,
		success	: function(data){
			var status = data.STATUS;
			if(status == 'SUCCESS'){
				var runJson = data.RUN_JSON;
				var passedTests = runJson['totalPassedTcs'];
				var failedTests = runJson['totalFailedTcs'];
				var erroredTests = runJson['totalErrorTcs'];
				var totalTcs = runJson['totalTcs'];
				$('#passedTests').val(passedTests);
				$('#failedTests').val(failedTests);
				$('#erroredTests').val(erroredTests);
				$('#totalCases').val(totalTcs);
				drawPieChart();
			}else{
				showMessage(data.MESSAGE, "Error");
			}				
		}
	});
}
function generateBarChartForDefect(runId,chartId,defectKey,color,xLabel){
	
	$.ajax({
		url		: 'ProjectDashBoardServlet',
		type	: 'GET',
		data	: {'t' :'defect_details',param:runId},
		dataType: 'json',
		async:true,
		success	: function(data){
			var status = data.STATUS;
			if(status == 'SUCCESS'){
				var defectJson = data.DEFECT_JSON;
				var ticks=[];
				var count =[];
				for(var key in defectJson[defectKey]){
					ticks.push(key);
					count.push(defectJson[defectKey][key]);
				}
				drawBarChartForDefect(count,ticks,chartId,color,xLabel);
			}else{
				showMessage(data.MESSAGE, "Error");
			}				
		}
	});
}
function drawBarChartForDefect(count,ticks,chartId,color,xLabel){
	var myNode = document.getElementById(chartId);
	myNode.innerHTML = '';
	$.jqplot.config.enablePlugins = true;
	var minval = 0,maxval=0;

	var passTemp = Math.max.apply(Math,count);
	if(passTemp>maxval){
		maxval = passTemp;
	}
	var mod = maxval /10;
	var temp = 0;
	if(mod < 5){
		temp = 5 - mod;
	}else{
		temp = 10 - mod;
	}
	maxval = Number(maxval)+Number(temp);
	
    plot1 = $.jqplot(chartId, [count], {
        // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
        /*animate: !$.jqplot.use_excanvas,*/
        /*Modified by Preeti for V2.8-103 starts*/
    	/*seriesColors:[color],*/
        seriesDefaults:{
            renderer:$.jqplot.BarRenderer,
            rendererOptions: {
                varyBarColor: true
            },
        /*Modified by Preeti for V2.8-103 ends*/
            pointLabels: { show: true }
        },
        
        axes: {
            xaxis: {
            	label: xLabel,
            	labelOptions: {
    	            fontSize: '12pt',
    	            textColor :'#3b73af'
    	          },
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ticks,
                tickOptions:{
		        	 formatString: '%d',
					 showGridline: false,
					},
            },
            yaxis: {
		          label: "Count",
		          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
		          max: maxval,
		          min:minval,
		          labelOptions: {
	    	            fontSize: '12pt',
	    	            textColor :'#3b73af'
	    	          },
	    	          tickOptions:{
	 		        	 formatString: '%d',
	 					 showGridline: false,
	 					},
		        }
        },
        highlighter: { show: false }
    });
 
}

function generateLineChartForDayWise(){
	var tsRecId = $('#exTrendTS').val();
	if(!tsRecId)
		tsRecId=0;
	$.ajax({
		url		: 'ProjectDashBoardServlet',
		type	: 'GET',
		data	: {'t' :'generate_Report','tsRecId':tsRecId},
		dataType: 'json',
		success	: function(data){
			var status = data.STATUS;
			var chart='line_chart_day';
			var tickInterval='1 day';
			if(status == 'SUCCESS'){
				var  legendValues=[],xAxisSeriesValue=[];
				var passDataArray=[],errorDataArray=[],failDataArray=[];
				
				var day_report_json = data.DAY_REPORT_JSON;
				var reportArray = day_report_json.reportArray;
				
				for(var i = 0; i<reportArray.length; i++){
					var dayReport = reportArray[i];
					legendValues.push(dayReport.STATUS);
					for (var j = 1; j <=7; j++) {
						var data = dayReport['Day'+j];
						if(i==0){
							xAxisSeriesValue.push(data.split('|')[0]);
						}
							passDataArray.push(data.split('|')[1]);
					}
				}
				if(passDataArray.length==0){
					passDataArray=[0,0,0,0,0,0,0];
				}
				if(reportArray.length > 0){
					drawLineChart(passDataArray,xAxisSeriesValue,chart,tickInterval);
				}else{
					var options = 'No Records to display.';
					$('#line_chart_week').html(options);
				}
			}else{
				showMessage(data.MESSAGE, "Error");
			}
		}
	});
}
function generateReportForWeeklyWise(){
var tsRecId = $('#exTrendTS').val();
if(!tsRecId)
	tsRecId=0;
$.ajax({
	url		: 'ProjectDashBoardServlet',
	type	: 'GET',
	data	: {'t' :'generate_Report',param:'weekly','tsRecId':tsRecId},
	dataType: 'json',
	async:true,
	success	: function(data){
		var status = data.STATUS;
		if(status == 'SUCCESS'){
			var  legendValues=[],xAxisSeriesValue=[];
			var passDataArray=[];
		
			var weekly_report_json = data.WEEKLY_REPORT_JSON;
			var reportArray = weekly_report_json.reportArray;
			
			for(var i = 0; i<reportArray.length; i++){
				var weeklyReport = reportArray[i];
				legendValues.push(weeklyReport.STATUS);
				for (var j = 1; j <=5; j++) {
					var data = weeklyReport['week'+j];
					if(i==0){
						xAxisSeriesValue.push((data.split('|')[0]).substring(0,10));
					}
						passDataArray.push(data.split('|')[1]);
				}
			}
			if(passDataArray.length==0){
				passDataArray=[0,0,0,0,0];
			}
			if(reportArray.length > 0){
				drawLineChart(passDataArray,xAxisSeriesValue,'line_chart_week','1 week');
			}else{
				var options = 'No Records to display.';
				$('#line_chart_week').html(options);
			}
		}else{
			showMessage(data.MESSAGE, "Error");
		}
		
	}
});

}
function drawLineChart(passArray,xAxisSeriesValue,chart,tickInterval){
	var minval = 0,maxVal=0;
	var passTemp = Math.max.apply(Math,passArray);

	if(passTemp>maxVal){
		maxVal = passTemp;
	}
	var mod = maxVal /10;
	var temp = 0;
	if(mod < 5){
		temp = 5 - mod;
	}else{
		temp = 10 - mod;
	}
	maxVal = Number(maxVal)+Number(temp);

	var plot2 = $.jqplot(chart, [passArray], {
		axesDefaults: {
			tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
			tickOptions: {
				fontSize: '8pt'
			}
		},
		seriesColors:['#FFD700'],
		axes:{
			xaxis:{
				renderer:$.jqplot.CategoryAxisRenderer, 
				label: "Date",
				ticks: xAxisSeriesValue,
				labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
				labelOptions: {
					fontSize: '12pt',
					textColor :'#3b73af'
				},
				tickOptions:{showGridline: false,},
				tickInterval:tickInterval
			},
			yaxis: {
				label: "TCs",
				labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
				labelOptions: {
					fontSize: '12pt',
					textColor :'#3b73af'
				},
				min: minval,
				max: maxVal,
				tickOptions:{
					formatString: '%d',
					showGridline: false,
				},
			}
		},
	});
	plot2.replot();
}

function getRunsForTestSet(selTS,id){
	var dropdown=$(id);
	dropdown.empty();
	for(var i=0; i < selTS.length; i++) {
       $(id).append('<option value="' + selTS[i].trim() + '">' + selTS[i].trim() + '</option>');
    }
}
/*Added by Preeti for TENJINCG-1019 ends*/

function drawPieChart() {
	var passedTests = $('#passedTests').val();
	var failedTests = $('#failedTests').val();
	var erroredTests = $('#erroredTests').val();
	
	if($('#totalCases').val()=='1'){
		var positionFactor=0;
	}else{
		 var positionFactor=0.5;
	}
	var data = [['Passed', Number(passedTests)], ['Failed',Number(failedTests)],['Error',Number(erroredTests)]];
	var plot1 = jQuery.jqplot ('pie_chart', [data],{
		
		legend: { show: true,
			placement: 'outsideGrid',
            location: 'e',
			},
		seriesColors: [ "#27ae60", "#E65454",  "#f39c12"],
		highlighter: {
			  show: true,
			  useAxesFormatters: false,
			  tooltipFormatString: '%s',
			  varyBarColor: true
			},
			seriesDefaults: {
				renderer: jQuery.jqplot.PieRenderer,
				trendline:{ show:false },
				rendererOptions:{
					padding: 5, 
					showDataLabels: true,
					dataLabelPositionFactor: positionFactor,
				},
			},
	});
}

/*Commented by Preeti for TENJINCG-1019 starts*/
/*function generateLineChartForDayWise(){

$.ajax({
	url		: 'ProjectDashBoardServlet',
	type	: 'GET',
	data	: {'t' :'generate_Report'},
	dataType: 'json',
	success	: function(data){
		var status = data.STATUS;
		var chart='line_chart_day';
		var tickInterval='1 day';
		if(status == 'SUCCESS'){
			var  legendValues=[],xAxisSeriesValue=[];
			var passDataArray=[],errorDataArray=[],failDataArray=[];
			
			var day_report_json = data.DAY_REPORT_JSON;
			var reportArray = day_report_json.reportArray;
			
			for(var i = 0; i<reportArray.length; i++){
				var dayReport = reportArray[i];
				legendValues.push(dayReport.STATUS);
				for (var j = 1; j <=7; j++) {
					var data = dayReport['Day'+j];
					if(i==0){
						xAxisSeriesValue.push(data.split('|')[0]);
					}
					if(dayReport.STATUS=='Pass' ){
						passDataArray.push(data.split('|')[1]);
					}else if(dayReport.STATUS=='Error' ){
						errorDataArray.push(data.split('|')[1]);
					}else if(dayReport.STATUS=='Fail'){
						failDataArray.push(data.split('|')[1]);
					}
				}
			}
			if(passDataArray.length==0){
				passDataArray=[0,0,0,0,0,0,0];
			}
			if(errorDataArray.length==0){
				errorDataArray=[0,0,0,0,0,0,0];
			}
			if(failDataArray.length==0){
				failDataArray=[0,0,0,0,0,0,0];
			}
			
			if(reportArray.length > 0){
				drawLineChart(passDataArray, failDataArray,errorDataArray,xAxisSeriesValue,chart,tickInterval);
			}else{
				var options = 'No Records to display.';
				$('#line_chart_week').html(options);
			}
		}else{
			showMessage(data.MESSAGE, "Error");
		}
	}
});
}
function generateReportForWeeklyWise(){


$.ajax({
url		: 'ProjectDashBoardServlet',
type	: 'GET',
data	: {'t' :'generate_Report',param:'weekly'},
dataType: 'json',
async:true,
success	: function(data){
	var status = data.STATUS;
	if(status == 'SUCCESS'){
		var  legendValues=[],xAxisSeriesValue=[];
		var passDataArray=[],errorDataArray=[],failDataArray=[];
	
		var weekly_report_json = data.WEEKLY_REPORT_JSON;
		var reportArray = weekly_report_json.reportArray;
		
		for(var i = 0; i<reportArray.length; i++){
			var weeklyReport = reportArray[i];
			legendValues.push(weeklyReport.STATUS);
			for (var j = 1; j <=5; j++) {
				var data = weeklyReport['week'+j];
				if(i==0){
					xAxisSeriesValue.push((data.split('|')[0]).substring(0,10));
				}
				if(weeklyReport.STATUS=='Pass'){
					passDataArray.push(data.split('|')[1]);
				}else if(weeklyReport.STATUS=='Error'){
					errorDataArray.push(data.split('|')[1]);
				}else if(weeklyReport.STATUS=='Fail'){
					failDataArray.push(data.split('|')[1]);
				}
			}
		}
		if(passDataArray.length==0){
			passDataArray=[0,0,0,0,0];
		}
		if(errorDataArray.length==0){
			errorDataArray=[0,0,0,0,0];
		}
		if(failDataArray.length==0){
			failDataArray=[0,0,0,0,0];
		}
		
		if(reportArray.length > 0){
			drawLineChart(passDataArray, failDataArray,errorDataArray,xAxisSeriesValue,'line_chart_week','1 week');
		}else{
			var options = 'No Records to display.';
			$('#line_chart_week').html(options);
		}
	}else{
		showMessage(data.MESSAGE, "Error");
	}
	
}
});

}
function drawLineChart(passArray,failArray,errorArray ,xAxisSeriesValue,chart,tickInterval){

var minval = 0,maxVal=0;
var passTemp = Math.max.apply(Math,passArray);
var failTemp = Math.max.apply(Math,failArray);
var errorTemp = Math.max.apply(Math,errorArray);

if(passTemp>maxVal){
maxVal = passTemp;
}
if(failTemp>maxVal){
maxVal = failTemp;
}
if(errorTemp>maxVal){
maxVal = errorTemp;
}
var mod = maxVal /10;
var temp = 0;
if(mod < 5){
temp = 5 - mod;
}else{
temp = 10 - mod;
}
maxVal = Number(maxVal)+Number(temp);

var plot2 = $.jqplot(chart, [passArray,failArray,errorArray], {
axesDefaults: {
    tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
    tickOptions: {
      fontSize: '8pt'
    }
},
  axes:{
    xaxis:{
      renderer:$.jqplot.CategoryAxisRenderer, 
      label: "Date",
      ticks: xAxisSeriesValue,
      labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
      labelOptions: {
        fontSize: '12pt',
        textColor :'#3b73af'
      },
      tickOptions:{showGridline: false,},
      tickInterval:tickInterval
    },
    yaxis: {
          label: "Runs Count",
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
          labelOptions: {
            fontSize: '12pt',
            textColor :'#3b73af'
          },
          min: minval,
          max: maxVal,
          tickOptions:{
        	 formatString: '%d',
			 showGridline: false,
			},
        }
  },
  series:[
           {label:'Passed',},
           {label:'Failed'},
           {label:'Error'},
        ],
  legend: { show: true,
		placement: 'outsideGrid',
        location: 'e',
        fontSize: '9px',
        rowSpacing: '2px',
        textColor: '#222222',
        fontFamily: 'Lucida Grande, Lucida Sans, Arial, sans-serif',
        marginTop: '2px',
	  },
    seriesColors: [ "#27ae60", "#E65454",  "#f39c12"],
    seriesDefaults: { 
        showMarker:false,
        pointLabels: { show:true } 
      },
      highlighter: {
			show: true,
			showMarker : false,
			tooltipFormatString: '%s',
			tooltipContentEditor: function(str, seriesIndex, pointIndex, plot){
				return plot.series[seriesIndex]["label"] + ': ' + str;
			},
			tooltipAxes: 'y',
			location: 'n'
		},
});
plot2.replot();
}*/
/*Commented by Preeti for TENJINCG-1019 ends*/