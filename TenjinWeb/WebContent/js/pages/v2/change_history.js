/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  change_history.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 19-09-2019			Preeti					TENJINCG-1068,1069
*/

$(document).ready(function(){
	mandatoryFields('history_form');
	$('#historyDateFrom').datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		dateFormat:'dd-M-yy' ,
		maxDate : new Date()
	});
	$('#historyDateTo').datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		dateFormat:'dd-M-yy' ,
		maxDate : new Date()
	});
	$('#btnOk').click(function(){
		clearMessages();
		var reportType=$('#reportType').val();
		var activityArea = $('#activityArea').val();
		var recId = $('#entityRecId').val();
		var id = $('#entityId').val();
		var entityType =$('#entityType').val();
		var dateFrom=$('#historyDateFrom').val();
		var dateTo=$('#historyDateTo').val();
		var selFromDate=convertDate(dateFrom);
		var selToDate=convertDate(dateTo);
		var todaysDate=new Date();
		todaysDate.setHours(0,0,0,0);
		
		if(dateFrom != ''&& !validateDate(dateFrom)){
		    showMessage('Please enter Date From value in this format DD-MMM-YYYY.','error');
			return false;
		}else if(dateTo != ''&& !validateDate(dateTo)){
			showMessage('Please enter Date To value in this format DD-MMM-YYYY.','error');
			return false;
		}else if(selFromDate>todaysDate){
			showMessage("Date From should be less than or equal to Today's Date","error");
			return false;
		}else if(selToDate>todaysDate){
			showMessage("Date To should be less than or equal to Today's Date","error");
			return false;
		}
		else if(selFromDate>selToDate){
			showMessage("Date To should be greater than or equal to Date From","error");
			return false;
		}
		$('#img_report_loader').show();
		$.ajax({
			url:'TestReportServlet',
			data:'param=CHANGE_HISTORY_REPORT&activityArea='+activityArea+'&recId='+recId+'&id='+id+'&entityType='+entityType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&reportType='+reportType,
			async:true,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					/*modified by paneendra for VAPT fix starts*/
					/*var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");*/
					var $c = $("<a id='downloadFile' href='DownloadServlet?param="+data.path+"' target='_blank' download />");
					/*modified by paneendra for VAPT fix ends*/
					$("body").append($c);
	                $("#downloadFile").get(0).click();
	                $c.remove();
	                window.parent.closeModal();
				}else{
					var message = data.message;
					showMessage(message,'error');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown, 'error');
			}
		});
	});
	
	$('#btnCancel').click(function(){
		window.parent.closeModal();
	});
});

function convertDate(stringdate)
{
    var DateRegex = /([^-]*)-([^-]*)-([^-]*)/;
    var DateRegexResult = stringdate.match(DateRegex);
    var DateResult;
    var StringDateResult = "";
    try
    {
        DateResult = new Date(DateRegexResult[2]+"/"+DateRegexResult[3]+"/"+DateRegexResult[1]);
    } 
    catch(err) 
    { 
        DateResult = new Date(stringdate); 
    }
    // format the date properly for viewing
    /*StringDateResult = (DateResult.getMonth()+1)+"/"+(DateResult.getDate()+1)+"/"+(DateResult.getFullYear());*/

    return DateResult;
}