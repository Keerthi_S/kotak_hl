/***
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testcase_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 


*//******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 08-03-2018		   	Pushpalatha			   	Newly Added 
* 20-03-2018			Preeti					TENJINCG-611(Test case execution history)
* 22-03-2018            Padmavathi              for navigation of back button in run information page 
* 09-05-2018            Padmavathi              TENJINCG-646 
* 14-05-2018            Padmavathi              TENJINCG-646 
* 24-10-2018            Padmavathi              TENJINCG-849
* 28-02-2019            Padmavathi              TENJINCG-942 & TENJINCG-991 
* 11-03-2019            Padmavathi              TENJINCG-997
* 20-03-2019			Preeti					TENJINCG-1020
* 25-03-2019			Pushpalatha             TENJINCG-968
* 19-09-2019			Preeti					TENJINCG-1068,1069
* 19-11-202             Priyanka                TENJINCG-1231
*//* 
**/
 $(document).on('click','#btnBack',function(){
        var user=$('#user').val();
        var projectName=$('#projectName').val();
        var domain=$('#domain').val();
        var pid=$('#projectId').val();
        window.location.href='TestCaseServletNew?t=list&user='+user+'&project='+projectName+'&domain='+domain+'&pid='+pid;
    });
    	/*Added By Padmavathi for TENJINCG-646 starts*/
 var selectedStep;
 var selectedStepRules;
 	/*Added By Padmavathi for TENJINCG-646 ends*/
 $(document).ready(function() {
	 /*Added by Priyanka for TENJINCG-1231 starts*/
	 const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
		var current_datetime = new Date();
		var formatted = current_datetime.getDate() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getFullYear();
		   var projectEndDate=$('#endDate').val();   
			  var tdyDate = Date.parse(formatted);
			  var prjEndDate  = Date.parse(projectEndDate);
			  if (prjEndDate >=tdyDate) {
				  $('#history').show();
					$('#btnSave').show();
					$('#btnSaveAs').show();
					$('#btnTcExecuteHistory').show();
					$('#btnRun').show();
					$('#history').show();
					$('#btnAddStep').show();
					$('#btnRefreshSteps').show();
					$('#btnBack').show();
					$('#btnRemoveStep').show();
					
					
			  }
			  else{
				  $('#history').hide();
					$('#btnSave').hide();
					$('#btnSaveAs').hide();
					$('#btnTcExecuteHistory').hide();
					$('#btnRun').hide();
					$('#history').hide();
					$('#btnAddStep').hide();
					$('#btnRefreshSteps').show();
					$('#btnBack').show();
					$('#btnRemoveStep').hide();
			  }
			  /*Added by Priyanka for TENJINCG-1231 ends*/
	 
	 
	 /*Added by Pushpalatha for TENJINCG-968 starts*/
		mandatoryFields('main_form');
		/*Added by Pushpalatha for TENJINCG-968 ends*/
	 
	/* $('#test-steps-table').paginatedTable();*/
     $('.tab-section').hide();
 	/*Added By Padmavathi for TENJINCG-646 starts*/
     $('#presetRulesList').hide();
 	/*Added By Padmavathi for TENJINCG-646 ends*/
     $('#tabs a').bind('click', function(e){
         $('#tabs a.current').removeClass('current');
         $('.tab-section:visible').hide();
         $(this.hash).show();
         $(this).addClass('current');
         e.preventDefault();
     }).filter(':first').click();
     
     /*Commented by Padmavathi for TENJINCG-997 starts*/
 	/*Added By Padmavathi for TENJINCG-646 starts*/
/*     if($('#execuStatus').val()!=''){
  		$('#presetRules').attr('checked',true)
  		$('#presetRulesList').show();
  		
  		if($('#execuStatus').val()=='Pass'){
  			$('#presetRules').attr('checked',true)
  			$('#presetRulePass').attr('checked',true)
  		}else if($('#execuStatus').val()=='Fail'){
  			$('#presetRuleFail').attr('checked',true)
  		}else{
  			$('#presetRuleError').attr('checked',true)
  		}
  	 }else{
  		 $('#presetRules').attr('checked',false)
  		 $('#presetRulesList').hide();
  	 }
     
     $('#presetRules').click(function(){
    	 if( $('#presetRules').is(':checked')){
    	  	   $('#presetRulesList').show();
    	     } else{
    	    	 $('#presetRulesList').hide();
    	     }
     });*/
     /*Commented by Padmavathi for TENJINCG-997 ends*/
     /*Added by Padmavathi for TENJINCG-997 starts*/
     $('#dependencyRules').children('option').each(function(){
         var dependncyRules=$('#selectedRules').val();
         var val = $(this).val();
         if(val == dependncyRules){
             $(this).attr({'selected':true});
         }
     });
     /*Added by Padmavathi for TENJINCG-997 ends*/
     $('#test-steps-table tr').mouseup(function (event) {
    	 selectedStep=$(this).attr('id');
    	 selectedStepRules= $(this).data('rules');
		
     });
 	/*Added By Padmavathi for TENJINCG-646 ends*/
     
     $('#lstPriority').children('option').each(function(){
         var tcPriority=$('#selectedPriority').val();
         var val = $(this).val();
         if(val == tcPriority){
             $(this).attr({'selected':true});
         }
     });
   
 	$('.sortable-table tbody').sortable({
			stop: function( event, ui ) {
				/*Added By Padmavathi for TENJINCG-646 starts*/
				clearMessages();
				 var recid=$('#txtTcRecId').val();
				 var ruleType='';
				$.ajax({
					url:'TestStepServlet?t=dependencyRules&testCaseRecId=' + recid,
					dataType:'json',
					success:function(data) {
						if(data.status.toLowerCase() === 'success') {
							ruleType=data.ruleType;
							reOrderSteps(ruleType);
						}
					},
				});
				/*Added By Padmavathi for TENJINCG-646 ends*/
			}
		});
	
		
		 $('#lstTestCaseType').children('option').each(function(){
	            var tcType=$('#selectedCaseType').val();
	            var val = $(this).val();
	            if(val == tcType){
	                $(this).attr({'selected':true});
	            }
	     });
		 
	    $('#btnRun').click(function(){
	    	/*Added by Padmavathi for TENJINCG-991 starts*/
	    	 alert("Tenjin will execute all steps under a test case irrespective of test steps selection.");
	    	 /*Added by Padmavathi for TENJINCG-991 ends*/
	    	 window.location.href='TestSetServlet?entity_name=testcase&param=initiate_adhoc_run_tc&tc=' + $('#txtTcRecId').val()+'&callback=testcase';
	    
	    });
	    
	    $('#btnSave').click(function(){
	    	/*Added By Padmavathi for TENJINCG-646 starts*/
	    	 var recid=$('#txtTcRecId').val();
	    	 /*Commented by Padmavathi for TENJINCG-997 starts*/
	    	/*if($('#totalSteps').val() >1){ 
	    		var ruleType;
				$.ajax({
					url:'TestStepServlet?t=dependencyRules&testCaseRecId=' + recid,
					dataType:'json',
					async:false,
					success:function(data) {
						if(data.status.toLowerCase() === 'success') {
							ruleType=data.ruleType;
						}
					},
				});
				
				if(ruleType=='TestCase'|| $('#presetRules').is(':checked'))
					if(confirm('Preset rules changes will apply for all Test steps under Test case.This will override defined custom rules(if any),do you want to proceed ?')) {
	    		 }else{
	    			 return ;
	    		 }
	    	}*/
	    	 /*Added By Padmavathi for TENJINCG-646 ends*/
	    	 /*Commented by Padmavathi for TENJINCG-997 ends*/
	    	 
	    	 /*Added by Padmavathi for TENJINCG-997 starts*/
	    	 if($('#totalSteps').val() >1){ 
					if($('#selectedRules').val()!='-1' || $('#dependencyRules').val()!='-1')
						if(confirm('Preset rules changes will apply for all Test steps under Test case.This will override defined custom rules(if any),do you want to proceed ?')) {
		    		 }else{
		    			 return ;
		    		 }
		    	}
	    	 /*Added by Padmavathi for TENJINCG-997 starts*/
	    	
	    	
            var name = $('#txtTestCaseName').val();
            var priority = $('#lstPriority').val();
            var desc = $('#txtTestCaseDesc').val();
            var type = $('#lstTestCaseType').val(); 
            var id = $('#txtTestCaseId').val();
            var mode = $('#txtTestMode').val();
            /*Commented by Padmavathi for TENJINCG-997 Starts*/
            	/*Added By Padmavathi for TENJINCG-646 starts*/
           /* var presetRules=$('#txtTestMode').val();
            if( $('#presetRules').is(':checked')){
            	presetRules='Y';
     	     } else{
     	    	presetRules='N';
     	     }*/
            	/*Added By Padmavathi for TENJINCG-646 ends*/
            /*Commented by Padmavathi for TENJINCG-997 ends*/
            $('#main_form').append($("<input type='hidden' id='del' name='del' value='updateTestCase' />"));
            $('#main_form').append($("<input type='hidden' id='txtTcRecId' name='txtTcRecId' value='"+recid+"' />"));
            $('#main_form').append($("<input type='hidden' id='txtTestCaseName' name='txtTestCaseName' value='"+name+"' />"));
            $('#main_form').append($("<input type='hidden' id='lstPriority' name='lstPriority' value='"+priority+"' />"));
            $('#main_form').append($("<input type='hidden' id='lstTestCaseType' name='lstTestCaseType' value='"+type+"' />"));
           
            $('#main_form').append($("<input type='hidden' id='txtTestCaseDesc' name='txtTestCaseDesc' value='"+desc+"' />"));
            /*Commented by Padmavathi for TENJINCG-997 Starts*/
            /*	Added By Padmavathi for TENJINCG-646 starts
            $('#main_form').append($("<input type='hidden' id='preSetRule' name='presetRule' value='"+presetRules+"' />"));
           	Added By Padmavathi for TENJINCG-646 ends*/
            /*Commented by Padmavathi for TENJINCG-997 ends*/
            $('#main_form').submit();
        });
	    
	    $('#btnTcExecuteHistory').click(function(){
            var tcrecid = $('#txtTcRecId').val();
           /* Modified by Preeti for TENJINCG-611(Test case execution history) starts*/
            /*window.location.href='ResultsServlet?param=test_case_execution_history&recid=' + tcrecid;*/
            window.location.href='TestCaseServletNew?t=testcase_execution_history&recid=' + tcrecid;
       		/*Modified by Preeti for TENJINCG-611(Test case execution history) ends*/
        });
	    
	    $('#btnAddStep').click(function(){
	    	var selectedMode = $('#txtTestMode').val();
	    	var tcRecId=$('#txtTcRecId').val();
	    	/*Modified By Padmavathi for TENJINCG-646 Starts*/
	    	/*Modified by Padmavathi for TENJINCG-997 Starts*/
	    	/*var presetRule=$('#execuStatus').val();*/
	    	var presetRule=$('#selectedRules').val();
	    	/*Modified by Padmavathi for TENJINCG-997 ends*/
	    
	    	var totalSteps=$('#totalSteps').val();
	    	var tcId = $('#tcid').val();
	    	/*Modified By Padmavathi for TENJINCG-942 starts*/
	    	/*window.location.href='TestStepServlet?t=new&mode='+selectedMode+'&tcRecId='+tcRecId+'&presetRule='+presetRule+'&totalSteps='+totalSteps;*/
	    	/*Modified by Preeti for TENJINCG-1020 starts*/
	    	/*window.location.href='TestStepServlet?t=new&mode='+selectedMode+'&tcRecId='+tcRecId+'&presetRule='+presetRule+'&totalSteps='+totalSteps+'&aut='+$('#aut').val();*/
	    	window.location.href='TestStepServlet?t=new&mode='+selectedMode+'&tcRecId='+tcRecId+'&presetRule='+presetRule+'&totalSteps='+totalSteps+'&aut='+$('#aut').val()+'&tcId='+tcId;
	    	/*Modified by Preeti for TENJINCG-1020 ends*/
	    	/*Modified By Padmavathi for TENJINCG-942 ends*/
	    	
	    	/*Modified By Padmavathi for TENJINCG-646 ends*/
	    });
	    
	    $('#btnRefreshSteps').click(function(){
			var tcRecId = $('#txtTcRecId').val();
			window.location.href = 'TestCaseServletNew?t=testCase_View&paramval=' + tcRecId+'&project='+$('#projectName').val()+'&domain='+$('#domain').val()+'&pid='+$('#projectId').val();
		});
	    
       $('#btnSync').click(function(){
            
            $('.modalmask').show();
            $('#syncTestCases').show();
            $('#historyCases').hide();
        });
       $('#btnCOk').click(function(){
   		var val='';
   		var recIds='';
   		var tcs =[];
   		//var count=0;
   		/*$(function(){*/
   			/*$('.synch:checked').each(function(){*/
   				var syn=$(this).attr('sync');
   				var id=$(this).attr('id');
   				if(syn=='Remote')
   				{
   					var obj = new Object();
   					obj.tcrecid = id;
   					obj.tcid = $(this).attr('tcId');
   					val = val+syn+',';
   					recIds = recIds+id+',';
   					tcs.push(obj);
   					//count++;
   				}
   				
   			/*});*/
   		/*});*/
   		
   		/*if(count>0){*/
   			window.location.href = 'TestCaseServlet?param=sync_test_cases&paramval=' + JSON.stringify(tcs)+'&source=list';
   		/*}else{
   			$('.modalmask').hide();
   			$('#syncTestCases').hide();
   			showMessage('Please select atleast one remote test case to synchronize');
   		}*/
   		
   	});
       /* Added By Ashiki for TJN252-60 starts*/
       $('#btnCOk').click(function(){
    	   var tcid1 =$('#txtTestCaseId').val();
    	   var tcrecid1 =$('#txtTcRecId').val();
    	   var tcs =[];
    	   var obj = new Object();
			obj.tcrecid = tcrecid1;
			obj.tcid = tcid1;
			tcs.push(obj);
    	   window.location.href = 'TestCaseServlet?param=sync_test_cases&paramval=' + JSON.stringify(tcs)+'&source=detail';
       });
       $('#btnTCancel').click(function(){
   		$('.modalmask').hide();
   		$('#syncTestCases').hide();
   	});
       /* Added By Ashiki for TJN252-60 ends*/
       $('#btnLogs').click(function(){
   			clearMessages();
   			$('#importLogs').modal('show');
   		});
       /*Added by Padmavathi for TENJINCG-849 starts*/
   	$('#btnSaveAs').click(function(){
   		clearMessages();
   		window.location.href='TestCaseServletNew?t=testCaseCopy&tcRecId='+$('#txtTcRecId').val();
   	})
   	/*Added by Padmavathi for TENJINCG-849 ends*/
   	/*Added by Preeti for TENJINCG-1068,1069 starts*/
   	$('#history').click(function(){
		clearMessagesOnElement($('#user-message'));
		var activityArea = 'Project';
		var recId = $('#txtTcRecId').val();
		var id = $('#tcid').val();
		var entityType ='testcase';
		$('#history-sframe').attr('src','TestReportServlet?param=CHANGE_HISTORY&activityArea='+activityArea+'&recId='+recId+'&id='+id+'&entityType='+entityType);
		$('.modalmask').show();
		$('#historyCases').show();
		$('#syncTestCases').hide();
	});
	/*Added by Preeti for TENJINCG-1068,1069 ends*/
	});
 
	
 $(document).on("click", "#btnRemoveStep", function() {
		
		if($('#test-steps-table').find('input[type="checkbox"]:checked').length < 1) {
			showLocalizedMessage('no.records.selected', 'error');
			return false;
		}else if($('#test-steps-table').find('input[type="checkbox"]:checked').length < 2 && $('#test-steps-table .tbl-select-all-rows').is(':checked')) {
			showLocalizedMessage('no.records.selected', 'error');
			return false;
		}
		/*Added By Padmavathi for TENJINCG-646 starts*/
		var message;
		if($('#totalSteps').val() >1){
			message='Are you sure you want to delete the selected record(s),dependency rules also will be deleted(if any) ? Please note that this action cannot be undone.'
		}else{
		message='Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.'
		}
		/*Added By Padmavathi for TENJINCG-646 ends*/	
	if(confirm(message)) {
		var $testStepForm = $("<form action='TestStepServlet' method='POST' />");
		$testStepForm.append($("<input type='hidden' id='transactionType' name='transactionType' value='true' />"))
		$testStepForm.append($("<input type='hidden' id='txtTcRecId' name='txtTcRecId' value='"+$('#txtTcRecId').val()+"' />"))
			$testStepForm.append($("<input type='hidden' id='projectId' name='projectId' value='"+$('#projectId').val()+"' />"))
			/* commented by shruthi for TCGST-52 starts*/
			/*uncommented by Priyanka for TestStep Deletion during DB Migration starts*/
			$testStepForm.append($("<input type='hidden' id='csrftoken_form' name='csrftoken_form' value='" + $('#csrftoken_form').val() + "' />"))
			/*uncommented by Priyanka for TestStep Deletion during DB Migration ends*/
			/*commented by shruthi for TCGST-52 ends*/
		$('#test-steps-table').find('input[type="checkbox"]:checked').each(function() {
			if(!$(this).hasClass('tbl-select-all-rows')) {
				$testStepForm.append($("<input type='hidden' name='TestStepRecId' value='"+ $(this).data('recordId') +"' />"));
			}
		});
		$('body').append($testStepForm);
		$testStepForm.submit();
	}
});
 
 /*Modified By Padmavathi for TENJINCG-646 starts*/	
 	function reOrderSteps(ruleType) {
 		presetRules='N';
 	if(ruleType=='TestCase'){
 		/*Commented by Padmavathi for TENJINCG-997 Starts*/
 			/*if( $('#presetRules').is(':checked')){
 				presetRules='Y';
 				var	execStatus;
 				if($('#presetRulePass').is(':checked')){
 					execStatus='Pass';
 				}else if($('#presetRuleFail').is(':checked')){
 					execStatus='Fail';
 				}else{
 					execStatus='Error';
 				}
 			}*/
 		/*Commented by Padmavathi for TENJINCG-997 ends*/
 		/*Added by Padmavathi for TENJINCG-997 Starts*/
 		if( $('#selectedRules').val()!='-1'){
				presetRules='Y';
				var	execStatus=$('#selectedRules').val();
			}
 		}
 	/*Added by Padmavathi for TENJINCG-997 ends*/
	var jsonArray = [];
	var oldJsonArray = [];
	var dependencyJson=new Object();
	var seq = 0;
	var alertShow='Yes';
	var preserveRules='Yes';
	/*Added by Padmavathi for TENJINCG-997 Starts*/
	var dependentStepRecId=0;
	/*Added by Padmavathi for TENJINCG-997 ends*/
	$('#test-steps-table tbody').find('tr').each(function (){
		seq++;
		var stepRecordId = $(this).data('recordId');
		var oldSeq = $(this).data('sequence');
		var json = new Object();
		
		json.stepRecId = stepRecordId;
		json.sequence = seq;
		/*Added by Padmavathi for TENJINCG-997 Starts*/
		json.dependentStepRecId=dependentStepRecId;
		/*Added by Padmavathi for TENJINCG-997 ends*/
		jsonArray.push(json);
		
		if(ruleType=='TestCase'||ruleType=='TestStep'){
		var jsonOld=new Object();
		
		if(seq==1&&oldSeq!=1&&seq!=oldSeq){
			alertShow='No';
			alert("Rules for 1st step will not be preserved.");
				
			}
		
		jsonOld.stepRecId = stepRecordId;
		jsonOld.oldSeq = oldSeq;
		oldJsonArray.push(jsonOld);
		}
		/*Added by Padmavathi for TENJINCG-997 Starts*/
		dependentStepRecId=stepRecordId;
		/*Added by Padmavathi for TENJINCG-997 ends*/
		$(this).find('.sequence').html(seq);
	});
	if(ruleType=='TestStep'&& selectedStepRules!=''){
		if(alertShow=='Yes'){
			if(confirm("You want to preserve the rules for this step.")){
				preserveRules='Yes';
				}else{
					preserveRules='No';
					}
				} 
		}
	
	var jsonString = JSON.stringify(jsonArray);
	var testCaseRecordId = $('#txtTcRecId').val();
	
	if(ruleType=='TestCase'||ruleType=='TestStep'){
		dependencyJson.selectedStep=selectedStep;
		dependencyJson.preserveRules=preserveRules;
		dependencyJson.presetRules=presetRules;
		dependencyJson.execStatus=execStatus;
		/*Added by Padmavathi for TENJINCG-997 Starts*/
		dependencyJson.ruleType=ruleType;
		/*Added by Padmavathi for TENJINCG-997 ends*/
		var oldJsonString= JSON.stringify(oldJsonArray);
		var dependencyJsonString=JSON.stringify(dependencyJson);
		/*modified by paneendra for Tenj210-169 starts*/
		/*var data='t=dependecny_reorder_steps&tcrecid=' + testCaseRecordId + '&order=' + jsonString+'&dependencyJson='+dependencyJsonString+'&oldOrder='+oldJsonString;*/
		var data={t:"dependecny_reorder_steps",tcrecid:testCaseRecordId,order:jsonString,dependencyJson:dependencyJsonString,oldOrder:oldJsonString};
		/*modified by paneendra for Tenj210-169 ends*/
	}else{
		var data='t=reorder_steps&tcrecid=' + testCaseRecordId + '&order=' + jsonString;
	}
	$.ajax({
		
		url:'TestStepServlet',
		data:data,
		dataType:'json',
		success:function(data) {
			if(data.status.toLowerCase() === 'success') {
				$('#stepMessage').html('Steps re-ordered successfully.');
				$('#stepMessage').addClass('success');
				document.location.reload();
				
			}else{
				$('#stepMessage').html(data.message);
				$('#stepMessage').addClass('error');
			}
		},
		error:function(data) {
			$('#stepMessage').html('An internal error occcurred. Please contact Tenjin Support.');
			$('#stepMessage').addClass('error');
		}
		
	});
	/*Modified By Padmavathi for TENJINCG-646 ends*/	
}
	/*Added by Preeti for TENJINCG-1068,1069 starts*/
 	function closeModal(){
 		$('.subframe > iframe').attr('src','');
 		$('.subframe').hide();
 		$('.modalmask').hide();
 		
 	}
 	/*Added by Preeti for TENJINCG-1068,1069 ends*/