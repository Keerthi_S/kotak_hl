/*Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  import_testdata.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

*//*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 03-11-2017				Roshni Das		Newly added for TENJINCG-398
 08-01-2017             Padmavathi              for TENJINCG-578
 14-06-2018             Padmavathi              T251IT-15
 25-03-2019				Pushpalatha				TENJINCG-968

*/

$(document).ready(function() {
	$('#showtestdata').click(function() {
		var userId = $('#lstProjectUsers').val();
		var appName = $('#lstProjectApps').val();
		var appId = $('#prjAppId').val();
		if (userId == -1) {
			showLocalizedMessage('Please select User.', 'error');
			return false;
		}
		if (appName == -1) {
			showLocalizedMessage('Please select Application.', 'error');
			return false;
		}
		window.location.href = "TestDataUploadServlet?param=show_testdata&userId=" + userId + "&appId=" + appId + "&appName=" + appName;
	});

	$('#lstProjectUsers').children('option').each(function() {
		var user = $('#SelectedUser').val();
		var val = $(this).val();
		if (val == user) {
			$(this).attr({ 'selected': true });
		}

	});

	$('#lstProjectApps').children('option').each(function() {
		var app = $('#SelectedApp').val();
		var val = $(this).val();
		if (val == app) {
			$(this).attr({ 'selected': true });
		}

	});


});


$(document).on("click", "#btnImportFile", function() {

		if ($('#testdatatable').find('input[type="checkbox"]:checked').length === 0 || ($('#testdatatable').find('input[type="checkbox"]:checked').length == 1 && $('#testdatatable').find('input[type="checkbox"]:checked').hasClass('tbl-select-all-rows'))) {
			showLocalizedMessage('no.records.selected', '', 'error');
			return false;
		}
		if (confirm('Selection of the existed Test Data to copy will override the existing one, Are you sure you want to copy the selected record(s)? Please note that this action cannot be undone.')) {

		var $testdataForm = $("<form action='TestDataUploadServlet' method='POST' />");
		$testdataForm.append($("<input type='hidden' id='importing' name='importing' value='true' />"))
		$testdataForm.append($("<input type='hidden' id='csrftoken_form' name='csrftoken_form' value='" + $('#csrftoken_form').val() + "' />"))

		$testdataForm.append($("<input type='hidden' name='selectedAppName' value='" + $('#SelectedApp').val() + "' />"));
		$testdataForm.append($("<input type='hidden' name='selectedUserName' value='" + $('#SelectedUser').val() + "' />"));
		$testdataForm.append($("<input type='hidden' name='selectedAppId' value='" + $('#prjAppId').val() + "' />"));
		$('#testdatatable').find('input[type="checkbox"]:checked').each(function() {
			if (!$(this).hasClass('tbl-select-all-rows')) {
				$testdataForm.append($("<input type='hidden' name='selectedFunctions' value='" + $(this).data('functionId') + "' />"));
			}
		});
		$('body').append($testdataForm);
		$testdataForm.submit();
	}
});

