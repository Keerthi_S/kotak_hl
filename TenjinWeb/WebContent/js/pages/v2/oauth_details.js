/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  oauth_details.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 ADDED BY              DESCRIPTION
* 22-03-2019			Sunitha				Newly added for OAuth 2.0 requirement TENJINCG-1018
* 31-05-2019			Prem				V2.8-93
*/
$(document).ready(function() {
	/* Modified by Prem for V2.8-93 start*/
	mandatoryFields('oauthform');
	/* Modified by Prem for V2.8-93 ends*/
	$('#btnSave').click(function(){
		($('#clientId').is(":visible")) ? '' : $('#clientId').val(''); 
		($('#clientSecret').is(":visible")) ? '' : $('#clientSecret').val(''); 
		($('#callBackUrl').is(":visible")) ? '' : $('#callBackUrl').val(''); 
		($('#authUrl').is(":visible")) ? '' : $('#authUrl').val(''); 
		($('#accessToken').is(":visible")) ? '' : $('#accessToken').val(''); 
		($('#authData').is(":visible")) ? '' : $('#authData').val(''); 
	});
	$('#btnBack').click(function(){
		window.location.href = 'APIServlet?param=show_aut_api_info&app=' + $('#appId').val() +'&showFile=no';
	});
	
	$('#authData').children('option').each(function(){
        var authDataTo=$('#selectedAuthData').val();
        var val = $(this).val();
        if(val == authDataTo){
            $(this).attr({'selected':true});
        }
    });
	
	$('#grantType').children('option').each(function(){
        var grantType=$('#selectedGrantType').val();
        var val = $(this).val();
        if(val == grantType){
            $(this).attr({'selected':true});
            checkGrantType(val);
        }
    });
});


function checkGrantType(val){
	$('#clientId').closest('.fieldSection').show();
	$('#clientSecret').closest('.fieldSection').show();
	$('#callBackUrl').closest('.fieldSection').show();
	$('#authUrl').closest('.fieldSection').show();
	$('#accessToken').closest('.fieldSection').show();
	$('#authData').closest('.fieldSection').show();
	switch(val){
		case "implicit":
			$('#clientSecret').closest('.fieldSection').hide();
			$('#accessToken').closest('.fieldSection').hide();
		break;
		case "password" :
		case "client_credentials":
			$('#callBackUrl').closest('.fieldSection').hide();
			$('#authUrl').closest('.fieldSection').hide();
		break;
	}
}