/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  teststep_new.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 05-03-2018		   Padmavathi 		       Newly added for TENJINCG-612
* 22-03-2018		   Padmavathi 		       TENJINCG-612
* 09-05-2018           Padmavathi              TENJINCG-645
* 12-10-2018           Padmavathi              TENJINCG-847
* 23-10-2018           Padmavathi              TENJINCG-847
* 20-12-2018           Padmavathi              TENJINCG-911
* 11-03-2019           Padmavathi              TENJINCG-997
* 25-03-2019		   Pushpalatha			   TENJINCG-968
* 04-04-2019           Padmavathi              TNJN27-45
* 05-06-2020           Priyanka                Tenj210-70 
* 16-06-2021		   Ashiki				   TENIINCG-1275
*/
$(document).ready(function(){
	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('teststepform');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	var mode=$('#txnMode').val();
	/*Added by Padmavathi for TENIINCG-847 starts*/
	if($('#stepCopy').val()=='true'){
		$('#tcId').show();
	}else{
		$('#tcId').hide();
	}
	/*Added by Padmavathi for TENIINCG-847 ends*/
	
	/*Commented by Padmavathi for TENJINCG-997 starts*/
	/*Added by Padmavathi for TENJINCG-645 starts*/
	/*if($('#totalSteps').val() >1 ){
		$('#customDependencyRule').show();
	}else{
		$('#customDependencyRule').hide();
	}*/
	
 /*   if($('#presetRule').val()!=''){
    	
    	$('#customRule').val('Y');
  		$('#customRules').attr('checked',true)
  		$('#customRulesList').show();
  		if($('#presetRule').val()=='Pass'){
  			
  			$('#customRules').attr('checked',true)
  			$('#customRulePass').attr('checked',true);
  		}else if($('#presetRule').val()=='Fail'){
  			
  			$('#customRuleFail').attr('checked',true)
  		}else{
  			$('#customRuleError').attr('checked',true)
  		}
  	 }else{
  		 $('#customRule').val('N');
  		$('#customDependencyRule').hide();
  	 }*/
    
    /*Added by Padmavathi for TENJINCG-645 ends*/
	/*Commented by Padmavathi for TENJINCG-997 ends*/
	
	/*Added by Padmavathi for TENJINCG-997 starts*/
	$('#dependencyRules').children('option').each(function(){
        var dependncyRules=$('#selectedRules').val();
        var val = $(this).val();
        if(val == dependncyRules){
            $(this).attr({'selected':true});
        }
    });
		$('#dependencyRules').prop('disabled',true)
		/*Added by Padmavathi for TENJINCG-997 ends*/
    /*Added by Padmavathi for TENJINCG-911 starts*/
    if($('#rerunStep').val()=='Y'){
		$('#showForRerun').prop('checked',true);
	}else{
		$('#showForRerun').prop('checked',false);
	}
    /*Added by Padmavathi for TENJINCG-911 ends*/
	if (mode == 'API') {
		$('#learn-mode').val(mode);
		$('#module_label').text(mode);
		$('#lstModules').attr("Title",mode);
		$('#lstValType').prop('disabled', true);
		$('#lstResponseType').show();
		}else if(mode == 'GUI'){
			$('#lstResponseType').hide();
			$('#ResponseType').hide();
			
		}else{
			$('#module_label').text("Format");
			$('#operationId').text("Message");
			$('.autLogintypes').hide();
			$('.txtTsType').hide();
			
		}
	/*Added by Padmavathi for TENIINCG-847 starts*/
	/*Added by shruthi for TENJINCG-1177 starts*/
	$('.lstTestMode').change(function(){
		var selectedMode=$('.lstTestMode').val();
		var tcRecId=$('#tcRecId').val();
		var presetRule=$('#presetRule').val();
		var totalSteps=$('#totalSteps').val();
		var tcId=$('#tcId1').val();
		var stepId=$('#txtStepId').val();
		var summary=$('#txtStepdesc').val();
		
    	window.location.href='TestStepServlet?t=new&mode='+selectedMode+'&tcRecId='+tcRecId+'&presetRule='+presetRule+'&totalSteps='+totalSteps+'&aut='+$('#aut1').val()+'&tcId='+tcId+'&stepId='+stepId+'&summary='+summary;

	});
	/*Added by shruthi for TENJINCG-1177 ends*/
	$('#lstValType').children('option').each(function(){
        var validationType=$('#selectedValType').val();
        var val = $(this).val();
        if(val == validationType){
            $(this).attr({'selected':true});
        }
    });
	 $('#lstTestcase').children('option').each(function(){
        var tcRecTd=$('#txnTcRecTd').val();
        var val = $(this).val();
        if(val == tcRecTd){
            $(this).attr({'selected':true});
        }
    });
	 $('#lstTestcase').change(function(){
		 $('#txnTcRecTd').val($(this).val()); 
	 });
	 /*Added by Padmavathi for TENIINCG-847 ends*/
	$('#lstValType').change(function() {
		if($(this).val() === 'UI') {
			$('#txtNoOfRows').val('1');
			$('#txtNoOfRows').prop('disabled',true);
			$("#txtNoOfRows").removeAttr('mandatory');
		}else{
			$('#txtNoOfRows').val('');
			$('#txtNoOfRows').prop('disabled',false);
			$("#txtNoOfRows").attr('mandatory','yes');
			
		}
	});
	$('#txtStepId').change(function(){
		$('#txtDataId').val($(this).val());
	});
	/*Added by Padmavathi for TENIINCG-847 starts*/
	 var showAlert=false;
	 var applicationId=$('#appId').val();
	 /*Added by Padmavathi for TENIINCG-847 ends*/
	$('#aut').change(function(){
		clearMessages();
		var app = $(this).val();
		/*Added by Padmavathi for TENIINCG-847 starts*/
		if($('#stepCopy').val()=='true' && $('#txnMode').val()=='GUI'){
			if(applicationId!= app){
				$('#childEntityCopy').val(false);
				showAlert=true;
				if($('#lstValType').val()=='UI')
					alert('Application is changed,it will not copy UIValidation steps and Manual field map for this step.');
				else
					alert('Application is changed,it will not copy Manual field map for this step.');
			}else{
				$('#childEntityCopy').val(true);
			}
		}
		/*Added by Padmavathi for TENIINCG-847 ends*/
		$('#appId').val(app);
		
		var val = $(this).val();
		if(val=='-1'){
			$("#api option").remove();
			$("#function option").remove();
			$("#operation option").remove();
			$("#autLogintypes option").remove();
			$("#apiOperation option").remove();
			$("#lstResponseType option").remove();
			$("#moduleCode option").remove();
			return false;
		}
		$("#apiOperation option").remove();
		$("#lstResponseType option").remove();
	
	});
	$('#function').change(function(){
		var functionCode=$(this).val();
		if(functionCode=='-1'){
			return false;
		}
		var appId=$('#aut').val();
		var mode=$('#txnMode').val();
		$.ajax({
			url:'TestStepServlet?t=learnCheck&functionCode=' + functionCode + '&appId=' + appId+'&mode='+mode,
			dataType:'json',
			success:function(data) {
				if(data.status === 'WARN') {
					/*Added by Padmavathi for TENIINCG-847 starts*/
					$('#childEntityCopy').val(false);
					/*Added by Padmavathi for TENIINCG-847 ends*/
					alert(data.message);
				}
				/*Added by Padmavathi for TENIINCG-847 starts*/
				else if(showAlert == false){		
					if($('#stepCopy').val()=='true' && $('#txnMode').val()=='GUI' ){
						if($('#funCode').val()!= functionCode){
							$('#childEntityCopy').val(false);
							if( $('#lstValType').val()=='UI')
								alert('Function is changed,it will not copy UIValidation steps and Manual field map for this step.');
							else
								alert('Function is changed,it will not copy Manual field map for this step.');
						}else{
							$('#childEntityCopy').val(true);
						}
					}
				}
				/*Added by Padmavathi for TENIINCG-847 ends*/
			},
		});
	});
	$('#api').change(function(){
		var apiCode = $(this).val();
		$('#apiCode').val(apiCode);
		if(apiCode=='-1'){
			$("#apiOperation option").remove();
			$("#lstResponseType option").remove();
			return false;
		}else{
			$("#apiOperation option").remove();
			$("#lstResponseType option").remove();
			var appId=$('#aut').val();
			var mode=$('#txnMode').val();
			$.ajax({
				url:'TestStepServlet?t=learnCheck&functionCode=' + apiCode + '&appId=' + appId+'&mode='+mode,
				dataType:'json',
				success:function(data) {
					if(data.status.toLowerCase() === 'warn') {
						alert(data.message);
					}
				},
			});
		}
		
	});
	$('#apiOperation').change(function(){
		var apiOperation = $(this).val();
		if(apiOperation=='-1'){
			$("#lstResponseType option").remove();
			return false;
		}
		
	});
 
	$('#btnSave').click(function(){
		
		/*Added by Ruksar for Tenj-210-14 starts*/
		var tsId=$('#txtStepId').val();
		if(tsId!=""){
            if(/[^a-zA-Z0-9_]/.test(tsId)) {
                 showMessage("Only characters,numbers and '_' are allowed for Teststep Id","error");
                 return false;
            }
        }
		/* Added by Ruksar for Tenj210-14 ends*/
		
		/*Added by Padmavathi for TENJINCG-911 starts*/
			    if ( $('#showForRerun').is(':checked') ) {
			    	 $("#showForRerun").val('Y');
			    } else {
			    	$("#showForRerun").val('N');
			    }
			    /*Added by Padmavathi for TENJINCG-911 ends*/	
		
		$('#txtNoOfRows').keyup(function(){
			var inputVal = $('#txtNoOfRows').val();
			var regex = new RegExp(/[^0-9]/g);
			var containsNonNumeric = inputVal.match(regex);
			if(containsNonNumeric){
				showMessage('Please enter valid numer of rows to execute.');
				$(this).val('');
				return false;
			}
		 /*commented by Padmavathi for TNJN27-45 starts */
		 /*else{
				if(inputVal==0){
					showMessage('Please enter valid numer of rows to execute.');
					return false;
				}
			}*/
			/*commented by Padmavathi for TNJN27-45 ends */
		});
		/*Added by Preeti for TENJINCG-1020 starts*/
		var id = $('#txtStepId').val();
		if(!id){
			var stepid = $('#teststepid').val();
			if(!stepid){
				$('#txtStepId').val($('#defaultstepid').val());
			}
			else{
				$('#txtStepId').val(stepid);
			}
		}
		/*Added by Preeti for TENJINCG-1020 ends*/
	});
	
	$(document).on('click', '#btnCancel', function() {
		var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
		/*Modified by Padmavathi for TENIINCG-847 starts*/
		if(confirmResult) {
			if($('#stepCopy').val()=='true'){
				window.location.href='TestStepServlet?t=view&key='+$('#txtRecordId').val()+'&callback=testcase'
			}else{
				window.location.href = 'TestCaseServletNew?t=testCase_View&paramval=' + $('#txnTcRecTd').val();
			}
			/*Modified by Padmavathi for TENIINCG-847 ends*/
		}else{
			return false;
		}
	});
	/*Added by Priyanka for Tenj210-70 starts*/
	$('#lstTestType').children('option').each(function(){
        var tsType=$('#txtTsType').val();
        var val = $(this).val();
        if(val == tsType){
            $(this).attr({'selected':true});
        }
 });
	/*Added by Priyanka for Tenj210-70 ends*/
});

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

   return true;
}
