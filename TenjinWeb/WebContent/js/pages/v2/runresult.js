
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  runresult.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 11-10-2018			Sriram					File optimized for TENJINCG-876
* 24-10-2018			Sriram					TENJINCG-866
* 30-10-2018			Sriram					TENJINCG-886
* 18-02-2019            Padmavathi              TENJINCG-973
* 19-02-2019            Padmavathi              TENJINCG-924 
* 25-01-2019       		Sahana 			    	pCloudy 
* 18-20-2019			Roshni					TJN252-8
* 10-03-2020			Sriram					Changed in multiple places for TENJINCG-1175 (TENJINCG-1194)
* 12-03-2020			Khalid					Added datatable filter property for TENJINCG-1194      
*/

var plot1;

$(document).ready(function() {
	//Set the page to its initial state
	initialState();
	
	//Draw Chart
	drawChart();
	
	$('#btnYes').click(function(){
		$('.modalmask').hide();
		$('#screenShotOption').hide();
		reportGeneration('true');
	});
	$('#btnNo').click(function(){
		$('.modalmask').hide();
		$('#screenShotOption').hide();
		reportGeneration('false');
	});
	
	$('#btnDefect').click(function(){
		var runId = $('#runid').val();
		var callback = $('#callback').val();
		var tcRecId = $('#tcRecId').val();
		window.location.href = 'ResultsServlet?param=defects&runid=' + runId + '&callback=' + callback;
	});
	
//	$('.tcstats').click(function(){
//		var filter = $(this).attr('filter');
//		if(filter == 'All' || filter=='Executed'){
//			$('#tc-table .tcrow').show();
//		}else{
//			var selector = '#tc-table .' + filter + '-row';
//			$('#tc-table .tcrow').hide();
//			$(selector).show();
//		}
//	});
//	
//	
//	$('.tcrow').click(function(){
//		var tcRecId = $(this).attr('_tcrecid');
//		var runId = $('#runId').text();
//		var callback = $('#callback').val();
//		window.location.href='ResultsServlet?param=tc_result&tc=' + tcRecId + '&run=' + runId + '&callback=' + callback;
//	});
	/*Added by Sahana for pCloudy: Starts*/
	$('#btnDeviceFarmReport').click(function(){
		/* 1.Get report link from pCloudy API
		   2.In new tab, display the report link */
		var runId = $('#runid').val();
		var execIp= $('#clientIp').val();
		var pCloudySessionName="Execution(RunId:"+runId+")";
		var reportLink="";
		$.ajax({
			url:'ResultsServlet',
			data :{param:"DEVICE_FARM_REPORT",sessionName:pCloudySessionName,machineIp:execIp},
			async:false,
			dataType:'json',
			success: function(data){
				var status = data.status;
				if(data.status == 'SUCCESS'){
					var jsonReport=data.pCloudyReport;
					reportLink=jsonReport.report_path;
					var win = window.open(reportLink, '_blank');
					win.focus();
				}else{
					showMessage(data.message,'error');
				}	
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown, 'error');			
			}
		});
	});
	/*Added by Sahana for pCloudy: Ends*/
	
	$('#btnRunTimeValues').click(function(){
		var runId = $('#runid').val();
		window.location.href = 'ResultsServlet?param=RUNTIME_VALUE&runid=' + runId;
	});
	
	
});

//TENJINCG-886 (Sriram)
$(document).on('click', '#btnMailLog', function() {
	clearMessagesOnElement($('#user-message'));
	var runId = $('#runid').val();
	var callback = $('#callback').val();
	window.location.href='ResultsServlet?param=mailLog&runId=' + runId+'&callback='+callback;
});

$(document).on('click', '#btnRun', function() {
	var runId = $('#runid').val();
	var testSetRecordId= $('#tSetRecId').val();
	var callback = $('#callback').val();
	
	window.location.href='PartialExecutorServlet?param=Re-RunTest&run=' + runId + '&ts=' + testSetRecordId + '&callback='+callback;
});

//TENJINCG-886 (Sriram) ends

//Back button click
$(document).on('click', '#btnBack', function() {
	var callback = $('#callback').val();
	if(callback) {
		callback = callback.toLowerCase();
	}else{
		callback = 'tsehistory'; //Redirect to Test Set Execution History by default
	}
	if(callback === 'tsehistory' || callback === 'tcehistory') {
		//Redirect to Test Set History page by default
		window.location.href='TestSetServletNew?t=testset_execution_history&recid=' + $('#tSetRecId').val();
	} else if(callback === 'runlist') {
		//Fix for TENJINCG-866 (Sriram)
		window.location.href='TestRunServlet';
		//Fix for TENJINCG-866 (Sriram) ends
	} else if(callback === 'scheduler') {
		window.location.href="SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id="+$('#prjId').val();
	}	/* Added by Padmavathi for TENJINCG-924 starts */
	else if(callback === 'project_dashboard') {
		window.location.href='ProjectDashBoardServlet?t=project_dashboard&paramval=' + $('#prjId').val();
	}
	/* Added by Padmavathi for TENJINCG-924 ends */
	/*Added by Padmavathi for TENJINCG-973 starts*/
	else if(callback === 'testsetlist') {
		window.location.href="TestSetServletNew?t=list"
	}
	/*Added by Padmavathi for TENJINCG-973 ends*/
});

//Click Report button
$(document).on('click', '#report-gen', function() {
	$('.modalmask').show();
	$('#screenShotOption').show();
});


$(document).on('click', '#btnSendMail', function() {
	var runId = $('#runId').text();
	$('#val-res-frame').attr('src','ResultsServlet?param=send_mail&run=' + runId);
	$('.modalmask').show();
	$('#val_frame').show();
});

function initialState() {
	$('#img_report_loader').hide();
	
	var status = $('#status').val();
	
	//TENJINCG-886 (Sriram)
	var hasChildRuns = $('#hasChildRuns').val();
	if(hasChildRuns && hasChildRuns.toLowerCase() === 'y') {
		$('#btnRun').prop('disabled',true);
	}
	
	//Keerthi
	//if(!status || status.toLowerCase() === 'not started') {
	if(!status || status.toLowerCase() === '') {
		$('#btnRun').prop('disabled',true);
		$('#report-gen').prop('disabled',true);
		$('#btnDefect').prop('disabled',true);
		$('#btnSendMail').prop('disabled',true);
		$('#btnMailLog').prop('disabled',true);
	}else if(status && status.toLowerCase() === 'pass') {
		$('#btnRun').prop('disabled',true);
	}
	
	$("#tc-table").dataTable({
		dom : "<'container-fluid'"
            + "<'row'"
            + "<'col-md-12 y-dt-buttons'B>>"
            + "<'row'<'col-md-12 y-dt-search'"
            + ""
            + ">"
            + ">"
            + "<'row dt-table'" + "<'col-md-12 y-dt-table'tr>" + ">"
            + "<'row'"
            + "<'col-md-12 mt-1 y-dt-table-info-block'"
            + "<'y-dt-table-length-change'l>"
            + "<'y-dt-table-info'i>"
            + "<'y-dt-table-pagination'p>"
            + ">"
            + ">"
            + ">"
	});
	
	var $dtWrapper = $("#tc-table").closest('.dataTables_wrapper');
	var $filterBlock = $dtWrapper.find('.y-dt-search');

	$filterBlock.html('');
	var $row = $("<div class='row' />").appendTo($filterBlock);
	const $filterByStatusBlock = $("<div class='col-md-3' />").appendTo($row);
	$("<label class='filter-label' style='margin-right: 4px;'>Show Status: </label>").appendTo($filterByStatusBlock);
	const $statusDropdown = $("<select class='stdTextBoxNew' id='status_filter'  style='min-width:150px' />").appendTo($filterByStatusBlock);
	$statusDropdown.append($("<option value='ALL'>All</option>"));
	$statusDropdown.append($("<option value='Pass'>Pass</option>"));
	$statusDropdown.append($("<option value='Fail'>Fail</option>"));
	$statusDropdown.append($("<option value='Error'>Error</option>"));
	$statusDropdown.append($("<option value='Not Executed'>Not Executed</option>"));
	
	var $textSearchBlock = $("<div class='col-md-3' />").appendTo($row);
	$("<label class='filter-label' style='margin-right:4px;'>Search: </label>").appendTo($textSearchBlock);
	const textSearchString = "";
	var $textSearch = $("<input type='text' class='ml-2 ngfui-table-column-text-filter stdTextBox' placeholder='Type to search...' value='" + textSearchString + "'/>").appendTo($textSearchBlock);
	
	/*Addded by khalid for TENJINCG-1194 starts*/
	$textSearch.on('keyup',function(){
		console.log('key up');
		$("#tc-table").DataTable().search(this.value).draw();	
	})
	
	$statusDropdown.on('change',function(){
		console.log(this.value);
		if(this.value==='ALL'){
			$("#tc-table").DataTable().columns(0).search('').draw();
		}
		else if(this.value==='Pass'){
			$("#tc-table").DataTable().columns(0).search('success').draw();
		}
		else if(this.value==='Not Executed'){
			$("#tc-table").DataTable().columns(0).search('queued').draw();
		}
		else{			
			$("#tc-table").DataTable().columns(0).search(this.value).draw();
		}
	})
	
	/*Addded by khalid for TENJINCG-1194 ends*/
}

 

function drawChart() {
	var passedTests = $('#passedTests').val();
	var failedTests = $('#failedTests').val();
	var erroredTests = $('#erroredTests').val();
	var notExecutedTests = $('#notExecutedTests').val();
	var data = [['Passed', Number(passedTests)], ['Failed',Number(failedTests)],['Error',Number(erroredTests)],['Not Executed', Number(notExecutedTests)]];
	plot1 = jQuery.jqplot ('run-summary-chart', [data],{
		seriesDefaults: {renderer: jQuery.jqplot.PieRenderer,
			rendererOptions:{padding: 2, sliceMargin: 2, showDataLabels: false }
		},
		legend: { show:true, location: 'ne',placement:'insideGrid',fontSize: '6px' },
		seriesColors: [ "#27ae60", "#c0392b",  "#f39c12", "#bdc3c7" ],
		height:150,
		width:150,
	});
}

function reportGeneration(screenShotOption){
	var runId = $('#runid').val();
	window.location.href="ResultsServlet?param=TEST_REPORT_GEN&runid=" + runId+"&screenShotOption="+screenShotOption;
}

function closeModal(){
	$('.subframe').hide();
	$('.modalmask').hide();
	$('#val_frame').attr('src','');
}

