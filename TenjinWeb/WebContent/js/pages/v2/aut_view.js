 /***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  aut_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 27-Oct-2017		   Padmavathi 		       Newly added for TENJINCG-400
 * 24-Nov-2017			Sahana					Updated appType to integer,UI changes
 * 16-03-2018			Preeti					TENJINCG-73
 * 22-03-2018			Preeti					Modified for TENJINCG-73
 * 18-06-2018			Preeti					T251IT-95
 * 28-06-2018			Preeti					T251IT-133
 * 26-Nov 2018			Shivam	Sharma			TENJINCG-904
 * 30-11-2018           Padmavathi              TJNUN262-33
 * 21-03-2019           Padmavathi              TENJINCG-1015
 * 25-03-2019			Pushpalatha				TENJINCG-968
 * 27-03-2019			Ashiki					TENJINCG-1004
 * 10-04-2019			Preeti					TENJINCG-1026
 * 12-08-2019			Prem					TENJINCG-1085

 */

$(document).ready(function() {
	$('#dmLoader').hide();
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('autForm');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	/* Added by paneendra for VAPT fix starts*/
	var templatePwd = $('#txtTemplatePwd').val();
	 /*Added by paneendra for VAPT fix ends */
	
	$(document).on('click', '#btnBack', function() {
		window.location.href='ApplicationServlet';
	});
	/*Added by Prem for TENJINCG-1085 starts*/
	$(document).on('click','#btnRefreshAut', function() {
		if ( confirm("Unsaved changes will be cleared this action cannot be undone")){
		window.location.href ='ApplicationServlet?t=view&key= '+$('#txtAutId').val();
		}
	});
	/*Added by Prem for TENJINCG-1085 Ends*/
	/*Added by Preeti for TENJINCG-73 starts*/
	var pauseLocation = $('#txtPauselocation').val();
	$('#txtLocation').val(pauseLocation);
	/*Added by Preeti for TENJINCG-73 ends*/
	$('#btnApiInformation').hide();
	$('#btnApiInformation').click(function() {
		/*Modified by Preeti for T251IT-133 starts*/
		window.location.href = 'APIServlet?param=show_aut_api_info&app=' + $('#txtAutId').val()+'&showFile=no';
		/*Modified by Preeti for T251IT-133 ends*/
	});

	var appType= $('#appTypeNum').val();

	if(appType==1){
		$('#btnApiInformation').show();
	}

	if(appType!=1 && appType!=2)
	{
		$("#txtPlatform").attr('mandatory','yes');
		$("#txtPackage").attr('mandatory','yes');
		//$("#txtPlatformVersion").attr('mandatory','yes');
		$("#txtAutURL").removeAttr('mandatory');
		$("#txtAutBrowser").removeAttr('mandatory');

		$('.show-for-all').hide();
		$('.mobileTxt').show();

		if(appType==3)
		{
			$('#txtPackageAnd').show();
			$('#txtPackageIos').hide();
			$("#txtActivity").attr('mandatory','yes');

		}
		else if(appType==4)
		{
			$("#txtActivity").removeAttr('mandatory');
			$('#txtPackageAnd').hide();
			$('#txtPackageIos').show();
			$('.android').hide();
		}
	}
	else
	{
		$('#txtPlatform').val("Web");
		$("#txtAutURL").attr('mandatory','yes');
		$("#txtAutBrowser").attr('mandatory','yes');
		$("#txtPlatform").attr('mandatory','yes');
		$("#txtPackage").removeAttr('mandatory');
		//$("#txtPlatformVersion").removeAttr('mandatory');
		$("#txtActivity").removeAttr('mandatory');
		$('.show-for-all').show();
		$('.mobileTxt').hide();

	}
	$('#txtDateTable').hide();
	$('#dateFormat').keyup(function() {
	}).focus(function() {
		$('#txtDateTable').show();
		/*Added by Padmavathi for  TENJINCG-1015 starts*/
		$(document).scrollTop($(document).height());
		/*Added by Padmavathi for  TENJINCG-1015 ends*/
	}).blur(function() {
		$('#txtDateTable').hide();
	});
	$('#BtnSave').click(function() {
		
		/* Added by paneendra for VAPT fix starts*/
		var newPwd = $('#txtTemplatePwd').val();
		
		if(newPwd!=templatePwd && newPwd.length>0){
		if(newPwd.length < 8){
			clearMessages();
			showMessage('Password should be minimum 8 characters', 'error');
			return false;
		}
		
		var tmpPassword=document.getElementById('txtTemplatePwd');
		if(!CheckPassword(tmpPassword)){
			showMessage("Please check the password rules","error");
			return false;
			 
		}
		}/* Added by paneendra for VAPT fix ends*/
		
		/*Added by Padmavathi for TJNUN262-33 starts*/
		var dateFormat=$('#dateFormat').val()
		var regex = new RegExp(/[0-9]/g);
		var containsNonNumeric = dateFormat.match(regex);
		if(containsNonNumeric){
			showMessage("Please enter valid date format", 'error');
			return false;
		}
		/*Added by Padmavathi for TJNUN262-33 ends*/
		var dateValue=isValidDate($('#dateFormat').val());
		if(!dateValue){
			showMessage("Please enter valid date format", 'error');
			return false;
		}
		/*Added by Preeti for TENJINCG-73 starts*/
		var pauseTime= $('#txtPauseTime').val();
		var pauseLocation= $('#txtLocation').val();
		if(isNaN(pauseTime) && !((pauseTime%1)===0)) {
			showMessage("Please enter valid value for pause time", 'error');
			return false;
		}
		/*Modified by Preeti for TENJINCG-1026 starts*/
		/*if(pauseTime.includes("."))*/
		if(pauseTime.indexOf(".") != -1)
		/*Modified by Preeti for TENJINCG-1026 ends*/
		{
			showMessage("Please enter valid value for pause time", 'error');
			return false;
		}
		if(pauseTime > 2147483647){
			showMessage("Pause time exeeds the limit. Please enter less value", 'error');
			return false;
		}
		/*Added by Preeti for TENJINCG-73 ends*/
		/*Added by Preeti for T251IT-95 starts*/
		if(pauseLocation =='' &&  pauseTime !=''){
			showMessage("Please enter pause location", 'error');
			return false;
		}
		if(pauseLocation !='' &&  pauseTime ==''){
			showMessage("Please enter pause time", 'error');
			return false;
		}
		/*Added by Preeti for T251IT-95 ends*/
	});
	
	/*<!-- added by shivam sharma for  TENJINCG-904 starts -->*/
	$('#btnUpload').click(function(){

		$('#uploadApk > iframe').attr('src','apk_upload.jsp?appId='+$('#txtAutId').val());

		$('#uploadApk').width(350).height(300); 
		$('#uploadApk').width(400).height(180);


		$('#uploadApk > iframe').width(350).height(300); 
		$('#uploadApk > iframe').width(400).height(180);

		$('.modalmask').show();
		$('#uploadApk').show();
	});
	/*<!-- added by shivam sharma for  TENJINCG-904 ends -->*/
	/*Added by Ashiki for TENJINCG-1004 starts*/
	$('#btnDownload').click(function(){
		$('#dmLoader').show();
		var appId = $('#txtAutId').val();
		$('#dmLoader').show();
		$.ajax({
			url:'ApplicationServlet',
			data:'t=download_APK_file&appId='+appId,   
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status =='SUCCESS'){
							$('#dmLoader').hide();
				            var $c = $("<a id='downloadFile' href="+data.path+"  download />");
			                $("body").append($c);
			                $("#downloadFile").get(0).click();
			                $c.remove();
			                showMessage(data.message,'success');
		            }else{
		            	$('#dmLoader').hide();
		            	showMessage(data.message, 'error');				
				}
				 $('#dmLoader').hide();
		            $('#btnDownload').prop('disabled',false);
			},
		});
	});
	/*Added by Ashiki for TENJINCG-1004 ends*/
	
	function isValidDate(date)
	{
		if(date.trim()!=""){
			var matches =/([0-9])|([a-z])/.exec(date);
			if (matches == null) return false;
			else return true;
		}
		else{
			return true;
		}

	}

	/*$('#txtPlatformVersion').keyup(function (){
					var inputVal = $(this).val();
					var regex = new RegExp(/[^0-9\.]/g);
					var containsNonNumeric = inputVal.match(regex);
					if(containsNonNumeric){
						alert('Please enter valid platform version');
						$(this).val('');
					}
				});*/
});