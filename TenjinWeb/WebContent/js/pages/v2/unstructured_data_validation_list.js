$(document).ready(function(){
	
	$('#btnDelete').click(function(){

		if($('#unstructured-data-table').find('input[type="checkbox"]:checked').length === 0 || ($('#unstructured-data-table').find('input[type="checkbox"]:checked').length == 1 && $('#unstructured-data-table').find('input[type="checkbox"]:checked').hasClass('tbl-select-all-rows'))) {
			showLocalizedMessage('no.records.selected', '', 'error');
			return false;
		}
		
		clearMessages();
		
		if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
			var $unstructuredDataForm = $("<form action='UnstructuredDataServlet' method='POST' />");
			$unstructuredDataForm.append($("<input type='hidden' id='del' name='del' value='true' />"))
			
			$('#unstructured-data-table').find('input[type="checkbox"]:checked').each(function() {
				if(!$(this).hasClass('tbl-select-all-rows')) {
					$unstructuredDataForm.append($("<input type='hidden' name='stepRecId' value='"+ $(this).data('testStepId') +"' />"));
					$unstructuredDataForm.append($("<input type='hidden' name='recId' value='"+ $(this).data('recordid') +"' />"));
				}
			});
			$('body').append($unstructuredDataForm);
			$unstructuredDataForm.submit();
}
	
	});
	
});