/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  accesstoken_details.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 ADDED BY              DESCRIPTION
* 22-03-2019			Sunitha				Newly added for OAuth 2.0 requirement TENJINCG-1018
*/
$(document).ready(function() {
	if(typeof $('#closeFlag').val() !== "undefined" && $('#closeFlag').val() != ''){
		window.close();
		var uacId = $('#uacId').val();
		var url ='ApplicationUserServlet?t=accessToken&uacId='+uacId;
		var $c = $("<a id='targetUrl' href='"+url+"' target='content_frame' />");
		$("body").append($c);
		$("#targetUrl").get(0).click();
		$c.remove();
	}
	
	if($('#caller').val() == 'refresh'){
		requestToken();
	}
	
	$('#btnAccessToken').click(function(){
		requestToken();
	});
	
	function requestToken(uacId, appId, userType){
		var uacId = $('#uacId').val();
		var appId = $('#appId').val();
		var userType = $('#userType').val();
		var caller = $('#caller').val();
		$.ajax({
			url:'ApplicationUserServlet?t=getauthcode&&appId='+appId+'&userType='+userType+'&uacId='+uacId+'&caller='+caller,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					if(data.type == 'window'){
						window.open(data.url, 'authwindow', '_blank');
					}else
						window.location.href=data.url;
				}else{
					showMessage(data.message,'ERROR');
				}
			},error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'ERROR');
			}
		});
	}
	
	/*$('#getRefreshToken').click(function(){
		var uacId = $('#uacId').val();
		var appId = $('#appId').val();
		var userType = $('#userType').val();
		window.location.href = 'ApplicationUserServlet?t=refreshtoken&&appId='+appId+'&userType='+userType+'&uacId='+uacId;
	});*/
	
	$('#btnSave').hide();
	
	$('#btnSave').click(function(){
		var tkngenTime = $('#tokenGenTime').val();
		
		if(dateRegEx(tkngenTime)){
			$('#oauthform').submit();
		}else{
			showMessage("Invalid Date", "error");
			return false;
		}
	});
	
	function dateRegEx(date){
		var pattern = new RegExp("^(3[01]|[12][0-9]|0[1-9])-(1[0-2]|0[1-9])-[0-9]{4} (2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])$");
		if (date.search(pattern)===0){
			return true;
		}else {
			return false; 
		} 
	}
	$('#btnBack').click(function(){
		var uacId = $('#uacId').val();
		window.location.href = 'ApplicationUserServlet?t=view&key='+uacId;
	});
	$('#btnEdit').click(function(){
		$('#accessToken').removeAttr('readonly');
		$('#tokenGenTime').removeAttr('readonly');
		$('#tokenValidity').removeAttr('readonly');
		$('#refreshToken').removeAttr('readonly');
		$('#btnEdit').hide();
		$('#btnSave').show();
	});
	var type = $('#grantTypeHide').attr("value")
	if(type == 'implicit' || type== 'client_credentials'){
		$('#refreshToken').closest('.fieldSection').hide();
	}
});