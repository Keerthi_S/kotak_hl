/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testset_list.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 10-04-2018		   	Pushpalatha			   	Newly Added 
* 27-09-2018		   	Pushpalatha			   	TENJINCG-740
* 28-09-2018			Pushpa				   	TENJINCG-819
* 07-11-2018            Padmavathi              TENJINCG-893
* 19-12-2018            Leelaprasad             TJNUN262-118
* 15-07-2019			Padmavathi 				TJN27-31
* 10-10-2019            padmavathi              TJN27-47
* 19-11-2020            Priyanka                TENJINCG-1231 
*/

var testSetTable;
$(document).ready(function() {
	/* Added by Priyanka for TENJINCG-1231 starts*/ 
	const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
	var current_datetime = new Date();
	var formatted = current_datetime.getDate() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getFullYear();
	   var projectEndDate=$('#endDate').val();   
		  var tdyDate = Date.parse(formatted);
		  var prjEndDate  = Date.parse(projectEndDate);
		 
           if(prjEndDate>=tdyDate){
			  
			  $('#btnNew').show();
			  $('#btnDelete').show();
			  $('#btnUpload').show();
			  $('#btnRefreshSets').show();
			  $('#btnDownload').show();
			  
			  
		  }
		  else{
			  $('#btnNew').hide();
			  $('#btnDelete').hide();
			  $('#btnUpload').hide();
			  $('#btnRefreshSets').hide();
			  $('#btnDownload').show();
			  
		  }
           /* Added by Priyanka for TENJINCG-1231 ends*/
	
	/*$('#testset-table').paginatedTable();*/
	
	/*Added by Padmavathi  for TJN27-31 starts
	$('#testset-table').dataTable( {
		'columnDefs': [
		{
			'searchable' : false,
			'targets'  : [0]
		},
		]
	} );
	Added by Padmavathi for TJN27-31 ends*/
	initTable();
	 /* Changed by Padmavathi for TENJINCG-893 starts */
	var path=$('#downloadPath').val();
	if(path!=''){
		/*modified by paneendra for VAPT fix starts*/
		/*var $c = $("<a id='downloadFile' href="+path+" target='_blank' download />");*/
		var $c = $("<a id='downloadFile' href='DownloadServlet?param="+path+"' target='_blank' download />");
		/*modified by paneendra for VAPT fix ends*/
		$("body").append($c);
			$c.get(0).click();
	}
	 /* Changed by Padmavathi for TENJINCG-893 ends */
	
	$('#btnNew').click(function(){
		var domain=$('#domain').val();
		var project=$('#projectName').val();
		var projectId=$('#pid').val();
		var user=$('#user').val();
		window.location.href='TestSetServletNew?t=init_new_testset&user='+user+'&project='+project+'&domain='+domain+'&pid='+projectId;
	});	
	
	$('#btnDelete').click(function(){
		
		if($('#testset-table').find('input[type="checkbox"]:checked').length < 1) {
			showLocalizedMessage('no.records.selected', 'error');
			return false;
		}else if($('#testset-table').find('input[type="checkbox"]:checked').length < 2 && $('#testset-table .tbl-select-all-rows').is(':checked')) {
			showLocalizedMessage('no.records.selected', 'error');
			return false;
		}
		
		if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
			var $testsetForm = $("<form action='TestSetServletNew' method='POST' />");
			$testsetForm.append($("<input type='hidden' id='del' name='del' value='true' />"))
			$testsetForm.append($("<input type='hidden' id='csrftoken_form' name='csrftoken_form' value='" + $('#csrftoken_form').val() + "' />"))
			$('#testset-table').find('input[type="checkbox"]:checked').each(function() {
				if(!$(this).hasClass('tbl-select-all-rows')) {
					$testsetForm.append($("<input type='hidden' name='selectedTestSet' value='"+ $(this).data('testsetRecId') +"' />"));
				}
			});
			$('body').append($testsetForm);
			$testsetForm.submit();
		}
	});
	
	/*Changed by Leelaprasad for the requirement TJNUN262-118 starts*/
	$('#btnRefreshSets').click(function() {
		window.location.href='TestSetServletNew?t=list';
	});
	/*Changed by Leelaprasad for the requirement TJNUN262-118 ends*/
	/*Added by Pushpa for TENJINCG-740 Starts*/
	$('#btnDownload').click(function(){
		clearMessagesOnElement($('#user-message'));
		   var txtTestSetId="";
		   var i=0;
		  /* Modified by Pushpa for TENJINCG-816 starts*/
		   if($('#testset-table').find('input[type="checkbox"]:checked').length ==1&& $('#testset-table .tbl-select-all-rows').is(':checked')){
			   showLocalizedMessage('no.records.selected', 'error');
  				return false;
		   }
		   
		   if($('#testset-table').find('input[type="checkbox"]:checked').length <1) {
			   $('#testset-table').find('input[type="checkbox"]').each(function() {
		   			
					if(!$(this).hasClass('tbl-select-all-rows')) {
						if(i==0)
							txtTestSetId=$(this).data('testsetRecId');
							else	
							txtTestSetId=txtTestSetId+","+$(this).data('testsetRecId');
							i=i+1;
						
		   			}
	   			});
			   if(txtTestSetId.length<1){
	   				showLocalizedMessage('no.records.selected', 'error');
	   				return false;
	   			}else{
	   				if (!window.confirm('Are you sure you want to download  all Test Set(s) and mapped TestCase(s)?')){
	   					return false;
	   				}
				}
		   }
		   else{
				if (window.confirm('Are you sure you want to download Selected Test Set(s) and mapped Test Case(s)?')){
					   
			   		$('#testset-table').find('input[type="checkbox"]:checked').each(function() {
			   			
						if(!$(this).hasClass('tbl-select-all-rows')) {
							if(i==0)
								txtTestSetId=$(this).data('testsetRecId');
								else	
								txtTestSetId=txtTestSetId+","+$(this).data('testsetRecId');
								i=i+1;
			   			}
			   		
					});
				}else{
					return false;
				}
			}
		   /*Modified by Pushpa for TENJINCG-816 ends	*/
		   		
			data='t=download_ts&txtTestSetId='+txtTestSetId;
			
		   
			if(data!=''){
				
				$('#img_download_loader').show();
				$('#btnDownload').prop('disabled',true);
					  $.ajax({
					     url:'TestSetServletNew',
					     data:data,
					     async:true,
					     dataType:'json',
					      success:function(data){
						         var status = data.status;
						            if(status == 'SUCCESS')
						            {
										showMessage(data.message,'success');
										/*modified by paneendra for VAPT fix starts*/
							            /*var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");*/
							            var $c = $("<a id='downloadFile' href='DownloadServlet?param="+data.path+"' target='_blank' download />");
							            /*modified by paneendra for VAPT fix ends*/
						                $("body").append($c);
						                $("#downloadFile").get(0).click();
						                $c.remove();
						             }
						            else
						             {
							            showMessage(data.message,'error');
						             }
						            $('#img_download_loader').hide();
						            $('#btnDownload').prop('disabled',false);
					            },
					       error:function(xhr, textStatus, errorThrown){
						           showMessage(errorThrown,'error');
						           $('#img_download_loader').hide();
						           $('#btnDownload').prop('disabled',false);
					          },
					      });
				}
		   
	   });
	
	 $('#btnUpload').click(function(){
		 clearMessagesOnElement($('#user-message'));
		 
		   $('#uploadExcel > iframe').attr('src','uploadTestCase.jsp');
			
			$('#uploadExcel').width(350).height(300); 
			$('#uploadExcel').width(400).height(180);
			
			$('#uploadExcel > iframe').width(350).height(300); 
			$('#uploadExcel > iframe').width(400).height(180);
			
			$('.modalmask').show();
			$('#uploadExcel').show();
			
			
	});
	 
	 /*Added by Pushpa for TENJINCG-740 Ends*/
	 
});
/*Added by Pushpa for TENJINCG-740 Starts*/
window.closeModalWindow = function () {
	closeModal();
};
function closeModal(){
	$('.subframe > iframe').attr('src','');
	$('.subframe').hide();
	$('.modalmask').hide();
	
}
/*Added by Pushpa for TENJINCG-740 Ends*/

/*Added by Padmavathi  for TJN27-47 starts*/
function initTable() {
	
	var url = "TestSetAjaxServlet?t=search";
	
	testSetTable = $('#testset-table').DataTable({
		"processing":true,
		"serverSide":true,
		"searching":true,
		"ajax":{"url":url, "dataSrc":"records"},
		"columns":[
			{"data":"id"},  
			{"data":"name"},
			{"data":"type"},
			{"data":"createdOn"},
			{"data":"createdBy"},
			/*{"data":"mode"},*/
			{"data":"lastRunId"},
			{"data":"lastRunStatus"}	
		],
		
		"columnDefs":[  {
			"targets":0,
			"searchable":false,
			"orderable":false,
			"className":"select-row",
			"render":function(data, type, full, meta) {
				return "<input type='checkbox' data-testset-rec-id='" + data + "' id='" + data + "'/>";
			}
		}, {
			"targets":1,
			"render":function(data, type, full, meta) {
				return "<a href='TestSetServletNew?t=testset_view&paramval="+ full.id +"' title='"+ data +"'>"+ data+"</a>";
			},
			
		},{
			"targets":3,
			"searchable":false,
			"render":function(data, type, full, meta) {
				return data;
			},
			class:"text-center"
			
		},
		{
			"targets":5,
			"searchable":false,
			"render":function(data, type, full, meta) {
				if(full.lastRunId==0){
					return 'N-A'
				}else{
					return "<a href='ResultsServlet?param=run_result&callback=testsetlist&run="+ data +"'>"+data+"</a>";
				}
			},
			class:"text-center"
		},
		{
			"targets":6,
			"searchable":false,
			"render":function(data, type, full, meta) {
				if(full.lastRunId==0){
					return 'N-A'
				}else{
					return data;
				}
			},
			class:"text-center"
		}
		]
		
	});

}
/*Added by Padmavathi  for TJN27-47 ends*/
	


