/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  project_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 23-10-2018		   Pushpalatha G		   Newly Added 
* 26-10-2017			Sriram						TENJINCG-875
* 08-11-2018           Leelaprasad              TENJINCG-874
* 08-11-2018           Leelaprasad              TENJINCG-888
* 07-12-2018           Padmavathi               TJNUN262-82
* 07-12-2018           Pushpa		            TJNUN262-58
* 11-12-2018           Leelaprasad               TJNUN262-100
* 13-12-2018           Padmavathi                TJNUN262-91
* 27-12-2018			Preeti					TJN262R2-73
* 08-02-2019		   Pushpalatha				TENJINCG-925
* 25-02-2019		   Pushpalatha				TENJINCG-976
* 28-02-2019		   Pushpalatha				TENJINCG-980,981
* 13-03-2019		   Preeti					TENJINCG-987
* 14-03-2019           Padmavathi               TENJINCG-995
* 15-03-2019		   Ashiki				  	TENJINCG-986 
* 20-03-2019           Padmavathi               TENJINCG-1014
* 21-03-2019			Preeti					TENJINCG-1021
* 25-03-2019		   Pushpalatha				TENJINCG-968
* 10-04-2019           Padmavathi               TNJN27-70
* 11-04-2019           Padmavathi               TNJN27-84 
* 18-04-2019           Padmavathi               TNJNR2-3
* 24-04-2019			Roshni					TENJINCG-1038
* 11-06-2019		   Pushpa					TJN27-5
* 13-06-2019			Preeti					V2.8-41
* 24-06-2019			Preeti					V2.8-59
* 19-09-2019			Preeti					TENJINCG-1068,1069
* 14-10-2019			Ashiki					Sprint27-9
* 15-11-2019		   Roshni					TENJINCG-1166
* 03-06-2020		   Ruksar					Tenj210-122
* 05-02-2021           Shruthi A N             TENJINCG-1255 
*/
var projectTDPTable;
var projectUserTable;
var projectAutTable;
var projectDefectTable;
/*Added by Preeti for TJN262R2-73 starts*/
/*Commented by Preeti for TENJINCG-1021 starts*/
/*var editTDP;*/
/*Commented by Preeti for TENJINCG-1021 ends*/
/*Added by Preeti for TJN262R2-73 ends*/
$(document).ready(function(){
	/*Added by Priyanka for TENJINCG-1231 starts*/
	const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
	var current_datetime = new Date();
	var formatted = current_datetime.getDate() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getFullYear();
	   var projectEndDate=$('#endDate').val();   
		  var tdyDate = Date.parse(formatted);
		  var prjEndDate  = Date.parse(projectEndDate);
             if(prjEndDate>=tdyDate){
			  
			  $('#btnAddNewProjectUser').show();
			  $('#btnRefreshProjectUser').show();
			  $('#btnAddNewAut').show();
			  $('#btnSaveURL').show();
			  $('#btnRefreshAut').show();
			  $('#btnSaveTDP').show();
			  $('#btnRemoveProjectUser').show();
			  $('#btnRemoveAut').show();
			 
			  
		  }
		  else{
			  $('#btnAddNewProjectUser').hide();
			  $('#btnRefreshProjectUser').show();
			  $('#btnAddNewAut').hide();
			  $('#btnSaveURL').hide();
			  $('#btnRefreshAut').show();
			  $('#btnSaveTDP').hide();
			  $('#btnRemoveProjectUser').hide();
			  $('#btnRemoveAut').hide();
				 
			 
			  
		  } 
         /*Added by Priyanka for TENJINCG-1231 ends*/
	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('main_form');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	/*Added by Pushpa for TENJINCG-980 starts*/
	/*added by shruthi for TENJINCG-1255 starts*/
	var repoType= $('#repotype').val();
	if(repoType=='AWS S3')
		{
		$('.show-for-all').show();
		$('.show-for-native').hide();
		}
	else if(repoType=='Native')
		{
		$('.show-for-all').hide();
		$('.show-for-native').show();
		}
	else
		{
		$('.show-for-all').hide();
		$('.show-for-native').hide();
		}
	/*added by shruthi for TENJINCG-1255 ends*/
	var subsetlists;
	$('#endDate').change(function(){
		var endDate=$('#endDate').val();
		$('#edate').val(endDate);
	});
	/*Added by Padmavathi for TENJINCG-1014 Starts*/
	$('#endDate').prop('disabled',true);
	/*Added by Padmavathi for TENJINCG-1014 ends*/
	/*Added by Pushpa for TENJINCG-980 ends*/
	
	/*Added by Leelaprasad for TENJINCG-888 starts*/
	/*Added by Pushpalatha for TENJINCG-925 starts*/
	var viewType=$('#view_type').val();
	if(viewType=="project"){
		$('#btnDeactive').hide();
		$('#btnActive').hide();
		/*Added by Pushpalatha for TENJINCG-976 starts*/
		$('#btnAmmend').hide();
		/*Added by Pushpalatha for TENJINCG-976 ends*/
	}
	/*Added by Pushpalatha for TENJINCG-925 ends*/
	$('.tab-section').hide();
	$('#dmLoader').hide();
	$('#btnSaveDef').hide();
	/*Added by Padmavathi for TENJINCG-995 starts*/
	$('#btnCancelTDP').hide();
	/*Added by Padmavathi for TENJINCG-995 ends*/
    $('#tabs a').bind('click', function(e){
        $('#tabs a.current').removeClass('current');
        $('.tab-section:visible').hide();
        $(this.hash).show();
        $(this).addClass('current');
        e.preventDefault();
    }).filter(':first').click();
    /*Added by Leelaprasad for TENJINCG-888 ends*/
	//TENJINCG-875 (Sriram)
	checkForTreeChange();
	//TENJINCG-875 (Sriram) ends
	/*$("#project-defect-tab").removeClass("active");
	$("project-users-tab").addClass("active");*/
	var projectId=$('#projectId').val();
	/*Added and modified by Pushpalatha for TJNUN262-58 starts*/
	var currentUserRole=$('#userRole').val();
	var currentUserID=$('#currentUser').val();
	var projectOwner=$('#projectOwner').val();
	initUserTable(projectId,currentUserRole,projectOwner,currentUserID);
	/*Added and modified by Pushpalatha for TJNUN262-58 ends*/
	$('#project-aut-tab').hide();
	$('#project-tdp-tab').hide();
	$('#project-defect-tab').hide();
	$('#project-details-tab').hide();
	
	$('#project-users-tab-anchor').click(function(e){
		e.preventDefault();
		/*Modified by Pushpalatha for TJNUN262-58 starts*/
		initUserTable(projectId,currentUserRole,projectOwner,currentUserID);
		/*Modified by Pushpalatha for TJNUN262-58 ends*/
		$('#project-aut-tab').hide();
		$('#project-tdp-tab').hide();
		$('#project-users-tab').show();
		$('#project-defect-tab').hide();
		$('#project-details-tab').hide();
		
	});
		
		var type=$("#prjType").val();
	var screenshot=$("#scrnshot").val();
	
	$("#lstType").val(type);
	
	$("#txtScreenShot").val(screenshot);
	
	$('#btnDeactive').click(function(){
		window .location.href="ProjectServletNew?t=state_change&currentState=A&project="+projectId;
	});

	$('#btnActive').click(function(){
		window .location.href="ProjectServletNew?t=state_change&currentState=I&project="+projectId;
	});
	
	$('#btnAmmend').click(function(){
		 
		$(function() {
			
			$("#endDate").datepicker({
				
				showOn: 'button',
				buttonText: 'Show Date',
				buttonImageOnly: true,
				buttonImage: './images/button/calendar.png',
				dateFormat: 'dd-M-yy',
				minDate:0,
				constrainInput: true
			}); 

		});
	var projectOwner=$('#prjOwner').val();
	var userRole=$('#userRole').val();
	var currentUser=$('#currentUser').val();
	
	$(this).hide();
	
	/*Added by Ruksar for Tenj210-122 starts*/
	$('#history').hide();
	/*Added by Ruksar for Tenj210-122 ends*/
	
	$('#btnMail').hide();
	$('#btnextTmInt').hide();
	/*Added by Padmavathi for TENJINCG-995 ends*/
	$('#btnSave').show();
	$('#btnCancel').show();
	$('#btnDeactive').hide();
	var prjId = $('#txtId').val();
	 
	$('#txtScreenShot').prop('disabled',false); 
	/*commented by shruthi for TCGST-34 starts*/
	/*$('#txtName').prop('disabled',false);*/
	/*commented by shruthi for TCGST-34 ends*/
	$('#txtDescription').prop('disabled',false);
	$('#lstType').prop('disabled',false);
	/*Added by Pushpa for TENJINCG-981 starts*/
	$('#txtEmail').prop('disabled',false);
	/*Added by Pushpa for TENJINCG-981 ends*/
	if(userRole=='Tester' && !currentUser==projectOwner ){
		$('#lstType').prop('disabled',true);
	}
	/*Added by Padmavathi for TENJINCG-1014 Starts*/
	$('#endDate').prop('disabled',false);
	/*Added by Padmavathi for TENJINCG-1014 Ends*/
	});
	$('#btnCancel').click(function(){
		$(this).hide();
		$('#btnSave').hide();
		$('#btnAmmend').show();
		/*Added by Padmavathi for TENJINCG-995 starts*/
		$('#btnMail').show();
		$('#btnextTmInt').show();
		/*Added by Ruksar for Tenj210-122 starts*/
		$('#history').show();
		/*Added by Ruksar for Tenj210-122 ends*/
		/*Added by Padmavathi for TENJINCG-995 ends*/
		$('#btnDeactive').show();
		$('#txtScreenShot').prop('disabled',true);
		$('#txtName').prop('disabled',true);
		$('#txtDescription').prop('disabled',true);
		$('#lstType').prop('disabled',true); 
		/*Added by Padmavathi for TENJINCG-1014 Starts*/
		$('#endDate').prop('disabled',true);
		$( "#endDate" ).datepicker( "destroy" );
		/*Added by Padmavathi for TENJINCG-1014 ends*/
		
	});
	
	$('#btnMail').click(function(){
		window.location.href = 'ProjectMailServlet?param=view&projectid='+projectId;
	});
	$('#project-aut-tab-anchor').click(function(e){
		e.preventDefault();
		clearMessagesOnElement($('#user-message-auts'));
		var projectId=$('#projectId').val();
		initAutTable(projectId);
		$('#project-users-tab').hide();
		$('#project-aut-tab').show();
		$('#project-tdp-tab').hide();
		$('#project-defect-tab').hide();
		$('#project-details-tab').hide();

		
	});
	
	
	$('#project-tdp-tab-anchor').click(function(){
		
		clearMessagesOnElement($('#user-message-tdps'));
		/*$('.tdpval').prop('disabled',true);*/
		/*Commented by Preeti for TENJINCG-1021 starts*/
		/*$('#functionGroup').prop('disabled',true);
		$('#map_url_path').prop('disabled',true);
		$('#addpath').prop('disabled',true);
		$('#application').prop('disabled',true);
		$('#btnEditTDP').show();
		$('#btnSaveTDP').hide();*/
		/*Commented by Preeti for TENJINCG-1021 ends*/
		
		
		var projectId=$('#projectId').val();
		var applicationId=$("#application").attr("defaultValue");
		var groupName=$('#functionGroup').val();
		initTDPTable(projectId,applicationId,groupName);
		
		
		$('#project-users-tab').hide();
		$('#project-aut-tab').hide();
		$('#project-tdp-tab').show();
		$('#project-defect-tab').hide();
		$('#project-details-tab').hide();
	});
	
	$('#project-defect-tab-anchor').click(function(){
		initProjectDefectTable(projectId);
		$('#project-users-tab').hide();
		$('#project-aut-tab').hide();
		$('#project-tdp-tab').hide();
		$('#project-defect-tab').show();
		$('#project-details-tab').hide();

	});
	
	$('#project-details-tab-anchor').click(function(){
		
		$('#project-users-tab').hide();
		$('#project-aut-tab').hide();
		$('#project-tdp-tab').hide();
		$('#project-defect-tab').hide();
		$('#project-details-tab').show();
		
	});
	
	$('#btnAddNewProjectUser').click(function(){
		var projectId=$('#projectId').val();
			clearMessagesOnElement($('#user-message-users'));
			clearMessagesOnElement($('#user-message'));
			
			$('#prj-sframe').attr('src','ProjectServletNew?t=UnmappedProjectUsers&projectId='+projectId);
			$('#prj-sframe').show();
			$('.modalmask').show();
			$('.subframe').show();
			
	});
	
	$('#btnAddNewAut').click(function(){
		var projectId=$('#projectId').val();
			clearMessagesOnElement($('#user-message-auts'));
			clearMessagesOnElement($('#user-message'));
			var csrftoken_form=$("#csrftoken_form").val();
			$('#prj-sframe').attr('src','ProjectServletNew?t=UnMappedProjectAuts&projectId='+projectId+'&csrftoken_form='+csrftoken_form);
			$('#prj-sframe').show();
			$('.modalmask').show();
			$('.subframe').show();
	});
	
	$('#btnSaveURL').click(function(){
		clearMessagesOnElement($('#user-message-auts'));
		clearMessagesOnElement($('#user-message'));
		var values = new Array();
		var count=1;
	
		$('#project-aut-table').find('input[type="checkbox"]').each(function () {
			var projectId=$('#projectId').val();
			var id=this.id;
		
			if(this.id != 'chk_all_pr_aut'){
				var obj=new Object();
				obj.appId=this.id;
				obj.url=$('#urlText'+count).val();
				obj.prjId=projectId;
					/*commented by shruthi for Tenj212-4 starts*/
				/*obj.appPath=$('#tdp'+count).val();*/
					/*commented by shruthi for Tenj212-4 ends*/
				count++;
				values.push(obj);
			}
		});
		var jstr = JSON.stringify(values);
		var csrftoken_form=$('#csrftoken_form').val();
		if(count==0)
		{
			showMessageOnElement('Please select atleast one application to update','error',$('#user-message-auts'));
		}
		else{
			$.ajax({
				url:'ProjectServletNew',
				data:{t:"updateProjectAuts",paramval:jstr,csrftoken_form:csrftoken_form},
				async: false,
				dataType:'json',
				type:'POST',
				success:function(data){
					if(data.status == 'success'){
						if(data.autCallBack=='autReload'){
							showMessageOnElement(data.message,'success',$('#user-message-auts'));
						}
					}else{
						showMessageOnElement(data.message,'error',$('#user-message-auts'));
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(errorThrown,'error');
				}
			});
		}
		
	});
	
	$('#btnSaveTDP').click(function(){
		/*Modified by Pushpa for TJN27-5 starts*/
		/*var count=1;
		clearMessagesOnElement($('#user-message'));
		var array = [];
		var projectId = $('#projectId').val();
		
		$('.tdpval').each(function() {
			 array.push({
				 "appid":$(this).data('appId'),
				"func":$(this).data('funcCode'),
				"tdp":$(this).val()
				
			 });
		})
		
		var jstr = JSON.stringify(array);
		var obj = new Object();
		obj.param = "UPDATE_TD_PATHS";
		obj.projid = projectId;
		obj.paramval = jstr;
		var json = JSON.stringify(obj);
		
		
    	$.ajax({
    		url:'ProjectServletNew',
    		data:'t=UPDATE_TD_PATHS&projid=' + projectId + '&paramval=' + jstr,
    		dataType:'json',
			async:false,
			type:'POST',
    		success:function(data){
    			var status = data.status;
				if(status == 'SUCCESS'){
					showMessageOnElement(data.message,'success',$('#user-message-tdps'));
					
				}else{
					showMessageOnElement(data.message,'error',$('#user-message-tdps'));
				}
    		},
    		error:function(xhr, textStatus, errorThrown){
    			showMessageOnElement(errorThrown,'error',$('#user-message-tdps'));
    		}
    	});*/
	
		
		clearMessagesOnElement($('#user-message'));

		var projectId = $('#projectId').val();
		var application=$('#application').val();
		var group=$('#functionGroup').val();
		var path=$('#map_url_path').val();
		var csrftoken_form=$('#csrftoken_form').val();
		if(application!=-1 && path!=''){
			
			if(application && path){
				$.ajax({
		    		url:'ProjectServletNew',
		    		data:'t=UPDATE_TDP_FOR_APP&projid=' + projectId + '&path=' + path+'&application='+application+'&group='+group+'&csrftoken_form='+csrftoken_form,
		    		dataType:'json',
					async:false,
					type:'POST',
		    		success:function(data){
		    			var status = data.status;
						if(status == 'SUCCESS'){
							showMessageOnElement(data.message,'success',$('#user-message-tdps'));
							
						}else{
							showMessageOnElement(data.message,'error',$('#user-message-tdps'));
						}
		    		},
		    		error:function(xhr, textStatus, errorThrown){
		    			showMessageOnElement(errorThrown,'error',$('#user-message-tdps'));
		    		}
		    	});
			}
		}
		else{
			var count=1;
			var array = [];
		
		$('.tdpval').each(function() {
			 array.push({
				 "appid":$(this).data('appId'),
				"func":$(this).data('funcCode'),
				"tdp":$(this).val()
				
			 });
		})
		
		var jstr = JSON.stringify(array);
		var obj = new Object();
		obj.param = "UPDATE_TD_PATHS";
		obj.projid = projectId;
		obj.paramval = jstr;
		var json = JSON.stringify(obj);
		var csrftoken_form=$('#csrftoken_form').val();
		
    	$.ajax({
    		url:'ProjectServletNew',
    		data:'t=UPDATE_TD_PATHS&projid=' + projectId + '&paramval=' + jstr+'&csrftoken_form='+csrftoken_form,
    		dataType:'json',
			async:false,
			type:'POST',
    		success:function(data){
    			var status = data.status;
				if(status == 'SUCCESS'){
					showMessageOnElement(data.message,'success',$('#user-message-tdps'));
					
				}else{
					showMessageOnElement(data.message,'error',$('#user-message-tdps'));
				}
    		},
    		error:function(xhr, textStatus, errorThrown){
    			showMessageOnElement(errorThrown,'error',$('#user-message-tdps'));
    		}
    	});
    	/*Modified by Pushpa for TJN27-5 ends*/
	}
	});
	/*Added by Padmavathi for TENJINCG-1014 Starts*/
	$('#btnSave').click(function(){
		var projectEndDate=$('#endDate').val();
		if(projectEndDate=='' || projectEndDate==undefined){
			showMessage('Project end date is mandatory','error');
			return false;
		}else if(!validateDate(projectEndDate)){
				showMessage('Please enter valid End Date in this format "DD-MMM-YYYY".','error');
				return false;
		}else if($('#startDate').val()!='NA' && new Date(projectEndDate) < new Date($('#startDate').val())){
			showMessage('End Date should be greater than or equal to Start Date','error');
			return false;
		}
	});
	/*Added by Padmavathi for TENJINCG-1014 ends*/
	
	$('#btnRemoveProjectUser').click(function(){
	
		var projectId=$('#projectId').val();
		clearMessagesOnElement($('#user-message-users'));
		clearMessagesOnElement($('#user-message'));
		var selectedUsers = '';
		var currentUser = $('#currentUser').val();
		/*Added by Padmavathi for TNJN27-84 starts*/
		var prjAdminCount=0;
		/*Added by Padmavathi for TNJN27-84 ends*/
		if($('#project-users-table').find('input[type="checkbox"]:checked').length ==1&& $('#project-users-table .tbl-select-all-rows').is(':checked')){
			   showLocalizedMessage('no.records.selected', 'error');
				return false;
		 }
		
		if($('#project-users-table').find('input[type="checkbox"]:checked').length < 1) {
			showLocalizedMessage('no.records.selected', 'error');
			return false;
		}else if($('#project-users-table').find('input[type="checkbox"]:checked').length < 2 && $('#project-users-table .tbl-select-all-rows').is(':checked')) {
			showLocalizedMessage('no.records.selected', 'error');
			return false;
		}else{
			
			$('#project-users-table').find('input[type="checkbox"]:checked').each(function() {
				if(!$(this).hasClass('tbl-select-all-rows')) {
					/*Modified by Preeti for V2.8-41 starts*/
					if(this.id!=currentUser){
					/*if(this.id==currentUser){
						return false;
					}*/
					/*Added by Padmavathi for TNJN27-84 starts*/
					
					/*Added by Padmavathi for TNJN27-84 ends*/
					selectedUsers=selectedUsers+this.id+",";
					}
					if($(this).data('currentrole')=='Project Administrator'){
						prjAdminCount++;
					}
					/*Modified by Preeti for V2.8-41 ends*/
				}
			});
			var csrftoken_form=$("#csrftoken_form").val();
			$.ajax({
				url:'ProjectServletNew',
				/*Modified by Padmavathi for TNJN27-84 starts*/
				/*data:'t=unMapProjectUsers&projectId=' +projectId+ '&selectedUsers=' + selectedUsers,*/
				data:'t=unMapProjectUsers&projectId=' +projectId+ '&selectedUsers=' + selectedUsers+'&prjAdminCount='+prjAdminCount+'&prjType='+$('#prjType').val()+'&csrftoken_form='+csrftoken_form,
				/*Modified by Padmavathi for TNJN27-84 ends*/
				dataType:'json',
				async:false,
				type:'POST',
				success:function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						showMessageOnElement(data.message,'success',$('#user-message-users'));
						$('#chk_all_asc_users').prop('checked', false);
					}else{
						showMessageOnElement(data.message,'error',$('#user-message-users'));
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessageOnElement(errorThrown,'error',$('#user-message-users'));
				}
			});
			
			projectUserTable.ajax.reload();
		}
	});
	
	
	$('#btnRemoveAut').click(function(){
		clearMessagesOnElement($('#user-message-auts'));
		clearMessagesOnElement($('#user-message'));
		var projectId=$('#projectId').val();
		var csrftoken_form=$("#csrftoken_form").val();
		var selectedAuts = '';
		var tjnusr = $('#currentUser').val();
		$('#project-aut-table').find('input[type="checkbox"]:checked').each(function () {
			if(this.id != 'chk_all_pr_aut' && this.id != tjnusr){
				selectedAuts = selectedAuts + this.id + ';';
			}
		});

		if(selectedAuts == '' || selectedAuts=='undefined;'){
			showMessageOnElement('Please choose an Application to continue','error',$('#user-message-auts'));
		}else{
			if (window.confirm('Are you sure you want to delete?'))
			{ 
				clearMessagesOnElement($('#user-message-auts'));
				$.ajax({
					url:'ProjectServletNew',
					data:'t=unMapProjectAut&projectId=' + projectId + '&autlist=' + selectedAuts+'&csrftoken_form='+csrftoken_form,
					dataType:'json',
					type:'POST',
					async:false,
					success:function(data){
						var status = data.status;
						if(status == 'SUCCESS'){
							showMessageOnElement(data.message,'success',$('#user-message-auts'));
						}else{
							showMessageOnElement(data.message,'error',$('#user-message-auts'));
						}
					},
					error:function(xhr, textStatus, errorThrown){
						showMessageOnElement(errorThrown,'error',$('#user-message-users'));
					}
				});
			}

		}
		projectAutTable.ajax.reload();
	});
	
	$('#btnRefreshProjectUser').click(function(){
		
		projectUserTable.ajax.reload();
		/*Added by Padmavathi for TJNUN262-82 starts*/
		$('#chk_all_asc_users').prop('checked',false);
		clearMessagesOnElement($("#user-message-users"));
		clearMessagesOnElement($("#user-message"));
		/*Added by Padmavathi for TJNUN262-82 ends*/
	});
	
	
	
	
	$('#btnEditTDP').click(function(){
		/*Modified by Preeti for TJN262R2-73 starts*/
		clearMessagesOnElement($('#user-message-tdps'));
		/*$('.tdpval').prop('disabled',false);*/
		editTDP = true;
		var projectId=$('#projectId').val();
		var applicationId=$("#application").attr("defaultValue");
		var groupName=$('#functionGroup').val();
		$('#project-tdp-table').DataTable().destroy();
		initTDPTable(projectId,applicationId,groupName);
		/*Modified by Preeti for TJN262R2-73 ends*/
		$('#functionGroup').prop('disabled',false);
		$('#map_url_path').prop('disabled',false);
		$('#addpath').prop('disabled',false);
		$('#application').prop('disabled',false);
		$('#btnEditTDP').hide();
		$('#btnSaveTDP').show();
		/*Added by Padmavathi for TENJINCG-995 starts*/
		$('#btnCancelTDP').show();
		/*Added by Padmavathi for TENJINCG-995 ends*/
	});
	
	$('#btnCancelTDP').click(function(){
		/*Modified by Preeti for TJN262R2-73 starts*/
		clearMessagesOnElement($('#user-message-tdps'));
		/*$('.tdpval').prop('disabled',true);*/
		editTDP = false;
		/*Added by Padmavathi for TENJINCG-995 starts*/
		$(this).hide();
		/*Added by Padmavathi for TENJINCG-995 ends*/
		var projectId=$('#projectId').val();
		var applicationId=$("#application").attr("defaultValue");
		var groupName=$('#functionGroup').val();
		$('#project-tdp-table').DataTable().destroy();
		initTDPTable(projectId,applicationId,groupName);
		/*Modified by Preeti for TJN262R2-73 ends*/
		$('#functionGroup').prop('disabled',true);
		$('#map_url_path').prop('disabled',true);
		$('#addpath').prop('disabled',true);
		$('#application').prop('disabled',true);
		$('#btnEditTDP').show();
		$('#btnSaveTDP').hide();
	});
	
	$('#btnRefreshAut').click(function(){
		projectAutTable.ajax.reload();
		/*Added by Padmavathi for TJNUN262-82 starts*/
		$('#chk_all_pr_aut').prop('checked',false);
		clearMessagesOnElement($("#user-message-auts"));
		clearMessagesOnElement($("#user-message"));
		/*Added by Padmavathi for TJNUN262-82 ends*/
	});
	
	$('#application').change(function(){
		var applicationId=$("#application").val();
		var groupName=$('#functionGroup').val();
		var url='ProjectAjaxServlet?t=search_prj_TDP&projectId='+projectId+'&applicationId='+applicationId+'&groupName='+groupName;
		url = encodeURI(url);
		
		projectTDPTable.ajax.url(url).load();
		projectTDPTable.draw();
	});
	
	$('#functionGroup').change(function(){
		var applicationId=$("#application").val();
		var groupName=$('#functionGroup').val();
		var url='ProjectAjaxServlet?t=search_prj_TDP&projectId='+projectId+'&applicationId='+applicationId+'&groupName='+groupName;
		url = encodeURI(url);
		
		projectTDPTable.ajax.url(url).load();
		projectTDPTable.draw();
	})
		/*Changed by Leelaprasa dfor TENJINCG-874 starts*/
	$('#btnextTmInt').click(function(){	
		var etmprjtool = $('#etmprjtool').val();
		window.location.href='TestManagerServlet?param=TmInstances&etmprjtool='+etmprjtool;
		/*Changed by Leelaprasa dfor TENJINCG-874 ends*/
	});	
	
	$('#btnSaveDef').click(function(){
		var count = $('.defectManagerSelect').length;
		var count=0;

		var jsonArray = [];
		var pId=null;

		var	dm=null;
		var dp=null;
		var ds=null;
		var proceedToSave = true;
		$('.defectManagerSelect').each(function(){
			var $parentRow = $(this).parent().parent();
			var instance = $(this).val();
			var tool = $('option:selected', this).attr('tool');
			var instance = $('option:selected', this).val();
			var app = $('option:selected', this).attr('app');
			var status = 'N';
			if(instance=='-1'){
				proceedToSave=false;
			
			}
			if($('#enableStatus'+app).is(":checked")){
				status='Y';
			}
			pId = $('#projectId').val();
			var json = new Object();
			var	dm=json.defectManager = $(this).val();
			
			var dttkey = "";

			if(instance=='HP ALM' || tool =='alm' || tool=='qc'){
				/*dttkey=  $parentRow.find('.defectManagerProject').val();*/
				/* Changed By Ashiki TENJINCG-986 starts */
				dttkey=  $parentRow.find('.dttproject').val();
				/* Changed By Ashiki TENJINCG-986 ends */
				if(dttkey===""){
					
					proceedToSave = false;
				}
				dp=json.dttproject = dttkey;
				if(!dm || !dp){
					proceedToSave = false;
				}
			}else{
				dttkey=  $parentRow.find('.defectManagerProject').val(); 
				/*Modified by Padmavathi for TNJNR2-3 starts*/
				/*if(dttkey == '-1' || dttkey == 'null' || dttkey == ''){*/
				if(dttkey == '-1' || dttkey == 'null' || dttkey == ''|| !dttkey){
				/*Modified by Padmavathi for TNJNR2-3 ends*/
					proceedToSave = false;
				}
				/* Added By Ashiki TENJINCG-986 starts */
				ds=json.dttsubset =$('option:selected',$parentRow.find('.defectManagerSubset')).attr('name');
				/* Added By Ashiki TENJINCG-986 ends */
				dp=json.dttproject =$('option:selected',$parentRow.find('.defectManagerProject')).attr('name'); 
				/*Added by Ashiki for Sprint27-9 starts*/
				$parentRow.find('.dttDelete').replaceWith($('<td><p> Mapped </p> </td>'));
				/*Added by Ashiki for Sprint27-9 ends*/
				if(!dm || !dp || !ds){
					proceedToSave = false;
				}
			}
			json.dttkey=dttkey;
			json.app =app;
			json.status=status;
			/* Modified By Ashiki TENJINCG-986 starts */
			json.tool=tool;
			/*if(dm!=null && dp!="" && dp!= -1){*/
			/*if(dm!=null && dp!="" && dp!= -1 && ds!="" && ds!= -1){*/
			/* Modified By Ashiki TENJINCG-986 starts */
				jsonArray.push(json);
			/*}*/

		});

		var jString = JSON.stringify(jsonArray); 
		var csrftoken_form=$("#csrftoken_form").val();
		if(jString!=null && jsonArray.length>0 && proceedToSave === true){

			$.ajax({
				url:'ProjectServlet',
				data:'param=DTT_MULTIPLE_ENABLE&p=' + pId + '&j=' + jString + '&count='+count+'&csrftoken_form='+csrftoken_form,
				async:false,
				dataType:'json',
				success:function(data){
					var status = data.status;
					if(status == 'SUCCESS'){			
						/*Added by Ashiki for Sprint27-9 starts*/
						projectTDPTable.ajax.reload();
						showMessageOnElement(data.message,'success',$('#user-message-def'));
						$('#btnSaveDef').hide();
						/*Added by Ashiki for Sprint27-9 ends*/
						$('#dmLoader').hide();
					}else{
						/*Modified by Ashiki for Sprint27-9 starts*/
						showMessageOnElement(data.message,'error',$('#user-message-def'));
						/*Modified by Ashiki for Sprint27-9 ends*/
						$('#dmLoader').hide();
						
					}
				},
			});
		}
		
		else{
			/*Modified by Ashiki for Sprint27-9 starts*/
			showMessageOnElement('Defect Management Instance and Defect Management Project is mandatory. Please review your selections and try again.','error',$('#user-message-def'));
			/*Modified by Ashiki for Sprint27-9 starts*/
		}

	});
	/*Added by Preeti for TENJINCG-1068,1069 starts*/
	$('#history').click(function(){
		clearMessagesOnElement($('#user-message'));
		var activityArea = 'Project';
		var recId = $('#projectId').val();
		var id = $('#prjName').val();
		var entityType ='project';
		$('#history-sframe').attr('src','TestReportServlet?param=CHANGE_HISTORY&activityArea='+activityArea+'&recId='+recId+'&id='+id+'&entityType='+entityType);
		$('.modalmask').show();
		$('.subframe').show();
		$('#prj-sframe').hide();
	});
	/*Added by Preeti for TENJINCG-1068,1069 ends*/
});

function closeModal(){
	$('.subframe > iframe').attr('src','');
	$('.subframe').hide();
	$('.modalmask').hide();
	
}

function reloadProjectUserTable() {
	if(projectUserTable) {
		projectUserTable.ajax.reload();
	}
}

function reloadProjectAutTable() {
	if(projectAutTable) {
		projectAutTable.ajax.reload();
	}
}
/*Modified by Pushpa for TJNUN262-58 starts*/
function initUserTable(projectId,currentUserRole,projectOwner,currentUserID) {
/*Modified by Pushpa for TJNUN262-58 ends*/
	projectUserTable=$('#project-users-table').DataTable({
		ajax:{
			url:'ProjectAjaxServlet?t=search_prj_users&projectId='+projectId,
			dataSrc:''
		},
		"columns":[
			{"data":"id"},
			{"data":"id"},
			{"data":"fullName"},
			{"data":"email"},
			{"data":"roleInCurrentProject"}
		],
		"columnDefs":[{
			"targets":0,
			"searchable":false,
			"orderable":false,
			"className":"select-row",
			"render":function(data, type, full, meta) {
				/*Modified by Padmavathi for TNJN27-84 starts*/
				if(currentUserRole=='Tester' && projectOwner!=currentUserID){
					/*return "<input type='checkbox' data-user-id='" + data + "' id='" + data + "' disabled/>";*/
					return "<input type='checkbox' data-user-id='" + data + "' data-currentRole='" + full.roleInCurrentProject + "' id='" + data + "' disabled/>";
				}else{
					/*return "<input type='checkbox' data-user-id='" + data + "' id='" + data + "' />";*/
					return "<input type='checkbox' data-user-id='" + data + "' data-currentRole='" + full.roleInCurrentProject + "' id='" + data + "' />";
				}
				/*Modified by Padmavathi for TNJN27-84 ends*/
			}
		}]
	});
}


function initAutTable(projectId) {
	var count=0;
	/*Added by Roshni for TENJINCG-1038 starts*/
	var currentUserRole=$('#currentprjrole').val();
	/*Added by Roshni for TENJINCG-1038 ends*/
	projectAutTable=$('#project-aut-table').DataTable({
		ajax:{
			url:'ProjectAjaxServlet?t=search_prj_auts&projectId='+projectId,
			dataSrc:''
		},
		"columns":[
			{"data":"id"},
			{"data":"id"},
			{"data":"name"},
			/*Added by Roshni for TENJINCG-1038 starts*/
			{"data":"templatePwd"},
			/*Added by Roshni for TENJINCG-1038 ends*/
			{"data":"URL"},
				/*commented by shruthi for Tenj212-4 starts*/
			/*{"data":"testDataPath"}*/
				/*commented by shruthi for Tenj212-4 ends*/
		],
		"columnDefs":[{
			"targets":0,
			"searchable":false,
			"orderable":false,
			"className":"select-row",
			"render":function(data, type, full, meta) {
				return "<input type='checkbox' data-aut-id='" + data + "' id='" + data + "'/>";
				
				
			}
		},{
			/*"targets":3,*/
			/*Modified by Roshni for TENJINCG-1038 starts*/
			"targets":4,
			/*Modified by Roshni for TENJINCG-1038 ends*/
			"searchable":false,
			"orderable":false,
			"className":"select-row",
			"render":function(data, type, full, meta) {
				count=count+1;
				return "<input type='text' class='stdTextBox long ' name='urlText' value='"+data+"' data-aut-id='" + data + "' id='urlText"+count+"' />";
				
				
			}
		},
			/*commented by shruthi for Tenj212-4 starts*/
		/*{
			"targets":4,
			Modified by Roshni for TENJINCG-1038 starts
			"targets":5,
			Modified by Roshni for TENJINCG-1038 ends
			"searchable":false,
			"orderable":false,
			"className":"select-row",
			"render":function(data, type, full, meta) {
				return "<input type='text' class='stdTextBox long ' name='appTDP' value='"+data+"' data-aut-id='" + data + "' id='tdp"+count+"' />";
				
				
			}
		},*/
			/*commented by shruthi for Tenj212-4 ends*/
		/*Added by Preeti for TENJINCG-987 starts*/
		{
			"targets":1,
			"className":"text-center"
		},
		/*Added by Preeti for TENJINCG-987 ends*/
		
		/*Added by Roshni for TENJINCG-1038 starts*/
		{
			"targets":3,
			"render":function(data, type, full, meta) {
				if(currentUserRole!='Project Administrator'){
					projectAutTable.columns([3]).visible(false);
				}
				return data;
			}
		}
		/*Added by Roshni for TENJINCG-1038 ends*/
		]
	});
}


function initTDPTable(projectId,applicationId,groupName) {
	var counttd=0;
	projectTDPTable = $('#project-tdp-table').DataTable({
		ajax:{
			url:'ProjectAjaxServlet?t=search_prj_TDP&projectId='+projectId+'&applicationId='+applicationId+'&groupName='+groupName,
			dataSrc:''
		},
		"columns":[
		    {"data":"appId"},
			{"data":"appName"},
			{"data":"funcCode"},
			{"data":"groupName"},
			/*commented by shruthi for Tenj212-4 starts*/
			/*{"data":"testDataPath"},*/
		  /* commented by shruthi for Tenj212-4 ends*/
		],
		"columnDefs":[{
			"targets":4,
			"searchable":false,
			"orderable":false,
			"className":"select-row",
			"render":function(data, type, full, meta) {
				counttd=counttd+1;
				/*Modified by Preeti for TJN262R2-73 starts*/
				/*Modified by Preeti for TENJINCG-1021 starts*/
				/*if(editTDP==true)*/
					return "<input type='textbox' data-app-id='" + full.appId + "' data-func-code='" + full.funcCode + "' data-testdata-path='" + full.testDataPath + "' class='stdTextBox tdpval' value='"+data+"'/>";
				/*else
					return "<input type='textbox' data-app-id='" + full.appId + "' data-func-code='" + full.funcCode + "' data-testdata-path='" + full.testDataPath + "' class='stdTextBox tdpval' value='"+data+"' disabled/>";*/
				/*Modified by Preeti for TENJINCG-1021 ends*/
				/*Modified by Preeti for TJN262R2-73 ends*/
				
			}
			
		},
		/*Added by Preeti for TENJINCG-987 starts*/
		{
			"targets":0,
			"className":"text-center"
		}
		/*Added by Preeti for TENJINCG-987 ends*/
		]
	});
}

function addPath()
{
	var map_url_path = $('#map_url_path').val();
	var application=$('#application').val();
	var groupName=$('#functionGroup').val();
	if(application=="-1"){
		showMessageOnElement('Please choose an application.','error',$('#user-message-tdps'));
		return false;
	}
	if ("" === map_url_path) {
		showMessageOnElement('Please enter url.','error',$('#user-message-tdps'));
		return false;
	}
	
	$('.tdpval').each(function() {
		
			$(this).val(map_url_path);
			
		 
	})

}

//TENJINCG-875 (Sriram)
function checkForTreeChange() {
	var refreshTree = $('#h-refreshTree').val();
	if(refreshTree || refreshTree === 'true') {
		window.parent.refreshDomainsTree();
	}
	
	var selectProject = $('#h-selectProject').val();
	if(selectProject || selectProject === 'true') {
		window.setTimeout(function() {
			window.parent.selectProjectInTree($('#projectId').val());
		}, 1000);
	}
}//TENJINCG-875 (Sriram) ends

function  refreshProjectUser(){
	projectUserTable.ajax.reload();
}

function initProjectDefectTable(projectId) {
	
	projectTDPTable = $('#project-def-table').DataTable({
		ajax:{
			url:'ProjectAjaxServlet?t=search_prj_Defect&projectId='+projectId,
			dataSrc:''
		},
		"columns":[
		    {"data":"name"},
			{"data":"instance"},
			/* Added By Ashiki TENJINCG-986 starts */
			{"data":"subSet"},
			/* Added By Ashiki TENJINCG-986 end */
			{"data":"dttProject"},
			{"data":"dttEnableStatus"},
			{"data":"id"},
			/*Added by Ashiki for Sprint27-9 starts*/
			{"data":"instance"},
			/*Added by Ashiki for Sprint27-9 ends*/
			
		],
		"columnDefs":[{
			
			"targets":1,
			/*Commented by Padmavathi for TJNUN262-91 starts*/
			/*"searchable":false,*/
			/*Commented by Padmavathi for TJNUN262-91 ends*/
			"orderable":false,
			"className":"select-row instance ",
			"render":function(data, type, full, meta) {
				
				$(this).attr('id', 'instance'+full.id)

				if(data==null){
					return "<p>Not Mapped</p>"
				}else{
					return data
				}
				
			}
			
		},/* Added By Ashiki TENJINCG-986 starts */
		{
			"targets":2,
			"orderable":false,
			"className":"select-row dttsubset",
			"render":function(data, type, full, meta) {
				if(data==null){
					return "<p>Not Mapped</p>"
				}
				else{
					return  data
				}
				
			}
			
		
			
		},/* Added By Ashiki TENJINCG-986 end */
		{
			"targets":3,
			/*Commented by Padmavathi for TJNUN262-91 starts*/
			/*"searchable":false,*/
			/*Commented by Padmavathi for TJNUN262-91 ends*/
			"orderable":false,
			"className":"select-row dttproject",
			"render":function(data, type, full, meta) {
				if(data==null){
					return "<p>Not Mapped</p>"
				}else{
					/*Modified by Padmavathi for TNJN27-70 starst*/
					return  data
					/*return  data.split(':')[0]*/
					/*Modified by Padmavathi for TNJN27-70 ends*/
				}
				
			}
			
		},{
			"targets":4,
			/*Commented by Padmavathi for TJNUN262-91 starts*/
			/*"searchable":false,*/
			/*Commented by Padmavathi for TJNUN262-91 ends*/
			"orderable":false,
			"className":"select-row enableStatus",
			"render":function(data, type, full, meta) {
				return data;
			}
			
		},{
			"targets":5,
			"searchable":false,
			"orderable":false,
			"className":"select-row",
			"render":function(data, type, full, meta) {
				return "<input type='button' id='dttEdit' value='Edit' app='"+full.id+"' project='"+full.dttProject+"' " +
						"tool='"+full.tool+"' defectInstance='"+full.instance+"' subset='"+full.subSet+"' enableStatus='"+full.dttEnableStatus+"'" +
						"class='dttEnableStatus imagebutton edit'>";
			}
			
		},{ /*Added by Ashiki for Sprint27-9 starts*/
			"targets":6,
			"orderable":false,
			"className":"select-row dttDelete",
			"render":function(data, type, full, meta) {
				if(data==null){
					return "<p>Not Mapped</p>"
				}
				else{
					return "<input type='button' id='dttDelete' value='Un-Map' app='"+full.id +"' enableStatus='"+full.dttEnableStatus+"'" +
					"class='dttDelete imagebutton unmapsingle'>";
				}
				
			}
			/*Added by Ashiki for Sprint27-9 ends*/
		}]
	});

}
/*Modified by Preeti for V2.8-59 starts*/
$(document).on('click', '.dttEnableStatus', function() {
	$(this).hide();
	/*Added by Ashiki for Sprint27-9 starts*/
	clearMessagesOnElement($('#user-message-def'));
	clearMessagesOnElement($('#user-message'));
	/*Added by Ashiki for Sprint27-9 ends*/
	$('#btnSaveDef').show();
	var $parentRow = $(this).parent().parent();
	var app=$(this).attr("app");
	var instance=$(this).attr("defectInstance");
	var project = $(this).attr("project");
	var tool=$(this).attr('tool');
	var subSet = $(this).attr('subset');
	var enableStatus = $(this).attr('enableStatus');
	var htmlS = "<input type='checkbox' id='enableStatus"+app+"' class='enableStatus'/>";
	$parentRow.find('.enableStatus').html(htmlS);
	if(enableStatus=='Y'){
		$('#enableStatus'+app).prop('checked', true);
	}
	if(instance!='undefined'){
		var a=0;
		if(instance=='HP ALM' || tool =='alm' || tool=='qc'){
			$parentRow.find('.dttproject').replaceWith($('<td><input type="text" class="stdTextBox dttproject" value="'+project+'" id="dttproject'+app+'">'));
			$('#dmLoader').hide();
			$('.dttInstance').prop('disabled',false);
			getData(app,tool,instance,$parentRow);
		}else{ 
			getData(app,tool,instance,$parentRow);
			getsubSet(instance, subSet, $parentRow);
			getsubsetProjects(subSet, project, $parentRow);
				
		}
		
	}else{
		$('#dmLoader').hide();
		getData(app,tool,instance,$parentRow);
	}
});
function getData(app,tool,instance,$parentRow){
	$.ajax({
		url:'ProjectServletNew',
		data :{t:'edit_dtt_mapping_settings'},
		async:true,
		dataType:'json',
		success: function(data){
			var status = data.status;
			if(status == 'success')
			{			
				var html = "<select app='" + app + " tool='"+tool+"' class='stdTextBox defectManagerSelect'>" +
				"<option value='-1'>-- Select One --</option>";
				var dArray = data.instances;


				for(var i=0;i<dArray.length;i++){
					var module = dArray[i];
					if(module.instance===instance){
						/* modified by Roshni for TENJINCG-1166 starts */
						/*html = html + "<option app='" + app + "' tool='"+module.tool+"' value='" + module.instance+ "'selected>" + module.instance + "</option>";*/
						html = html + "<option app='" + app + "' tool='"+module.tool+"' value='" + module.instance+ "' >" + escapeHtmlEntities(module.instance) + "</option>";
					}else{
						/*html = html + "<option app='" + app + "' tool='"+module.tool+"' value='" + module.instance+ "' >" + module.instance + "</option>";*/
						html = html + "<option app='" + app + "' tool='"+module.tool+"' value='" + module.instance+ "' >" + escapeHtmlEntities(module.instance) + "</option>";
						/* modified by Roshni for TENJINCG-1166 ends */
					}				
				} 
				$parentRow.find('.instance').html(html);
			}
			else
			{
				showMessage(data.message, 'error');	
				$('.defectManagerSelect').prop('disabled', false);
				$('#dmLoader').hide();
			}
			$('.defectManagerSelect').prop('disabled', false);
			$('.defectManagerProject').prop('disabled', false);
		},

		error:function(xhr, textStatus, errorThrown){
			/*Modified by Ashiki for Sprint27-9 starts*/
			showMessageOnElement('Could not load Projects from Defect Management Tool. Please contact Tenjin Support','error',$('#user-message-def'));
			/*Modified by Ashiki for Sprint27-9 starts*/
		}
	});
}
/*$(document).on('click', '.dttEnableStatus', function() {
	$(this).hide();
	$('#dmLoader').show();
	$('#btnSaveDef').show();
	//Find the parent row for this edit button
	var $parentRow = $(this).parent().parent();
	var app=$(this).attr("app");
	var instance=$(this).attr("defectInstance");
	 Added By Ashiki TENJINCG-986 starts 
	var project = $(this).attr("project");
	 Added By Ashiki TENJINCG-986 ends 
	var tool=$(this).attr('tool');
	var htmlS = "<input type='checkbox' id='enableStatus"+app+"' class='enableStatus'/>";
	$parentRow.find('.enableStatus').html(htmlS);
	if(instance!='undefined'){
		var a=0;
		if(instance=='HP ALM' || tool =='alm' || tool=='qc'){
		//	var project=$parentRow.find('.dttproject').val();
			$parentRow.find('.dttproject').replaceWith($('<td><input type="text" class="stdTextBox dttproject" value="'+project+'" id="dttproject'+app+'">'));
			$('#dmLoader').hide();
			$('.dttInstance').prop('disabled',false);
			$('.dttEnableStatus').show();
			$('#btnSaveDef').show();
		}else{ 
		 Commented By Ashiki TENJINCG-986 starts 
		
			$.ajax({
				url:'ProjectServletNew',
				data :{t:'Fetch_Manager_Projects',instance:instance},
				async:true,
				dataType:'json',
				success: function(data){
					var status = data.status;
					if(status == 'SUCCESS')
					{	
						var html= "<select  class='stdTextBox defectManagerProject'>" +
						"<option value='-1'>-- Select One --</option>";
						var dArray = data.subSet;
						for(var i=0;i<dArray.length;i++){
							var module = dArray[i];
							html=html+'<option name="'+module.pname+'" value="'+module.pid + '">'+module.pname+'</option>';
							html=html+'<option name="'+module.subSet+'</option>';
						} 
						$parentRow.find('.dttproject').html(html);
						$('#dmLoader').hide();
					}
					else
					{
						showMessage(data.message, 'error');	
						$('.defectManagerSelect').prop('disabled', false);
						$('#btnSaveDef').show();
					}
				
					$('.defectManagerSelect').prop('disabled', false);
					$('#btnSaveDef').show();
				},

				error:function(xhr, textStatus, errorThrown){
					showMessage('Could not load Projects from Defect Management Tool. Please contact Tenjin Support', 'error');	
					$('.defectManagerSelect').prop('disabled', false);
					$('#dmLoader').hide();
					$('#btnSaveDef').show();
				}
			});
		
		 Commented By Ashiki TENJINCG-986 ends 
		}
	
	}else{
		$('#dmLoader').hide();
	}
	
	
	$.ajax({
		url:'ProjectServletNew',
		data :{t:'edit_dtt_mapping_settings'},
		async:true,
		dataType:'json',
		success: function(data){
			var status = data.status;
			if(status == 'success')
			{			
				var html = "<select app='" + app + " tool='"+tool+"' class='stdTextBox defectManagerSelect'>" +
				"<option value='-1'>-- Select One --</option>";
				var dArray = data.instances;


				for(var i=0;i<dArray.length;i++){
					var module = dArray[i];
					if(module.instance===instance){
						html = html + "<option app='" + app + "' tool='"+module.tool+"' value='" + module.instance+ "'selected>" + module.instance + "</option>";
					}else{
						html = html + "<option app='" + app + "' tool='"+module.tool+"' value='" + module.instance+ "' >" + module.instance + "</option>";
					}				
				} 
				$parentRow.find('.instance').html(html);
			}
			else
			{
				showMessage(data.message, 'error');	
				$('.defectManagerSelect').prop('disabled', false);
				$('#dmLoader').hide();
			}
			$('.defectManagerSelect').prop('disabled', false);
			 Added By Ashiki TENJINCG-986 starts 
			$('.defectManagerProject').prop('disabled', false);
			 Added By Ashiki TENJINCG-986 ends 
		},

		error:function(xhr, textStatus, errorThrown){
			showMessage('Could not load Projects from Defect Management Tool. Please contact Tenjin Support', 'error');	
		}
	});
});*/
function getsubSet(instance,subSet,$parentRow){
	$.ajax({
		url:'ProjectServlet',
		data :{param:'Fetch_Manager_Projects',instance:instance},
		async:true,
		dataType:'json',
		success: function(data){
			var status = data.status;
			
			if(status == 'SUCCESS')
			{	
				var html= "<select  class='stdTextBox defectManagerSubset'>" +
				"<option value='-1'>-- Select One --</option>";
				var dArray = data.subSet;
				for(var i=0;i<dArray.length;i++){
					var module = dArray[i];
					if(module.subSet===subSet){
						html=html+'<option name="'+module.subSet+'" value="'+module.subSet + '" selected>'+module.subSet+'</option>';
					}else{
						html=html+'<option name="'+module.subSet+'" value="'+module.subSet + '">'+module.subSet+'</option>';;
					}	
						
					
				} 
				$parentRow.find('.dttsubset').html(html);
				$('#dmLoader').hide();
			}
			$('#dmLoader').hide();
			$('.defectManagerSelect').prop('disabled', false);
			$('#btnSaveDef').show();
		},

		error:function(xhr, textStatus, errorThrown){
			/*Modified by Ashiki for Sprint27-9 starts*/
			showMessageOnElement('Could not load Projects from Defect Management Tool. Please contact Tenjin Support','error',$('#user-message-def'));
			/*Modified by Ashiki for Sprint27-9 ends*/
			$('.defectManagerSelect').prop('disabled', false);
			$('#dmLoader').hide();
			$('#btnSaveDef').show();
		}
	})
}
function getsubsetProjects(subSet,projectSel,$parentRow){
	$.ajax({
		url:'ProjectServlet',
		data :{param:'Fetch_Subset_Projects',subSet:subSet},
		async:true,
		dataType:'json',
		success: function(data){
			var status = data.status;
			
			if(status == 'SUCCESS')
			{	
				var html= "<select  class='stdTextBox defectManagerProject'>" +
				"<option value='-1'>-- Select One --</option>";
				var dArray = data.projects;
				
				var projectArray=dArray.project.split(',');
				for(var i=0;i<projectArray.length;i++){
					var project=projectArray[i].split(':');
					if(projectSel===project[0]){
						html=html+'<option name="'+project[0]+'" value="'+project[1] + '" selected>'+project[0]+'</option>';
					}else{
						html=html+'<option name="'+project[0]+'" value="'+project[1] + '">'+project[0]+'</option>';	
					}
				} 
				
				$parentRow.find('.dttproject').html(html);
				$('#dmLoader').hide();
			}
			$('#dmLoader').hide();
			$('.defectManagerSubset').prop('disabled', false);
			$('#btnSaveDef').show();
		},

		error:function(xhr, textStatus, errorThrown){
			/*Modified by Ashiki for Sprint27-9 starts*/
			showMessageOnElement('Could not load Projects from Defect Management Tool. Please contact Tenjin Support','error',$('#user-message-def'));
			/*Modified by Ashiki for Sprint27-9 ends*/
			$('.defectManagerSubset').prop('disabled', false);
			$('#dmLoader').hide();
			$('#btnSaveDef').show();
		}
	});
}
/*Modified by Preeti for V2.8-59 ends*/
$(document).on('change', '.defectManagerSelect', function() {
	clearMessages();
	clearMessagesOnElement($('#msg-text'));
	$('#dmLoader').show();
	/*var $parentRow = $(this).parent().parent();
	var instance;
	var tool;
	var app;*/
	/* Added By Ashiki TENJINCG-986 starts */
	/*var app=$(this).attr("app");
	var tool=$(this).attr('tool');*/
	/* Added By Ashiki TENJINCG-986 end */
	
	$('.defectManagerSelect').each(function(){
		var $parentRow = $(this).parent().parent();
		var instance = $(this).val();
		var tool = $('option:selected', this).attr('tool');
		 var instance = $('option:selected', this).val();
		var app = $('option:selected', this).attr('app');
		 if(instance=='-1'){
				/*Modified by Preeti for V2.8-59 starts*/
				var dttsubsetHtml= "<select  class='stdTextBox defectManagerSubset'>" +
				"<option value='-1'>-- Select One --</option>";
				$parentRow.find('.defectManagerSubset').html(dttsubsetHtml);
				var dttprojectHtml= "<select  class='stdTextBox defectManagerProject'>" +
				"<option value='-1'>-- Select One --</option>";
				$parentRow.find('.defectManagerProject').html(dttprojectHtml);
				/*Modified by Preeti for V2.8-59 ends*/
				$('#dmLoader').hide();
				return false;
			}/* Added By Ashiki TENJINCG-986 starts */
			if(instance=='HP ALM' || tool =='alm' || tool=='qc'){
				$parentRow.find('.dttsubset').replaceWith($('<td><input type="text" class="stdTextBox" value="N/A" disabled>'));
				$parentRow.find('.dttproject').replaceWith($('<td><input type="text" class="stdTextBox dttproject" placeholder="Domain:Project" id="dttproject'+app+'">'));
				$('#dmLoader').hide();
				$('.dttInstance').prop('disabled',false);
				$('.dttEnableStatus').show();
				$('#btnSaveDef').show();
				/* Added By Ashiki TENJINCG-986 end */
			}else{
			/*Changed by Leelaprasad for the Defect TJNUN262-100 ends*/
			$.ajax({
				url:'ProjectServlet',
				data :{param:'Fetch_Manager_Projects',instance:instance},
				async:true,
				dataType:'json',
				success: function(data){
					var status = data.status;
					
					if(status == 'SUCCESS')
					{	
						var html= "<select  class='stdTextBox defectManagerSubset'>" +
						"<option value='-1'>-- Select One --</option>";
						var dArray = data.subSet;
						for(var i=0;i<dArray.length;i++){
							var module = dArray[i];
							/* Modified By Ashiki TENJINCG-986 starts */
							/*html=html+'<option name="'+module.pname+'" value="'+module.pid + '">'+module.pname+'</option>';*/
							/* modified by Roshni for TENJINCG-1166 starts */
							/*html=html+'<option name="'+module.subSet+'" value="'+module.subSet + '">'+module.subSet+'</option>';*/
							html=html+'<option name="'+module.subSet+'" value="'+module.subSet + '">'+escapeHtmlEntities(module.subSet)+'</option>';;
							/* modified by Roshni for TENJINCG-1166 ends */
							
						} 
						$parentRow.find('.dttsubset').html(html);
						/*Added by Padmavathi for TNJNR2-3 starts*/
						var dttProjectHtml= "<select  class='stdTextBox defectManagerSubset'>" +
						"<option value='-1'>-- Select One --</option>";
						$parentRow.find('.dttproject').html(dttProjectHtml);
						/*Added by Padmavathi for TNJNR2-3 ends*/
						/* Modified By Ashiki TENJINCG-986 starts */
						$('#dmLoader').hide();
					}
					else
					{
						/*Modified by Ashiki for Sprint27-9 starts*/
						showMessageOnElement(data.message,'error',$('#user-message-def'));
						/*Modified by Ashiki for Sprint27-9 starts*/
						$('.defectManagerSelect').prop('disabled', false);
						$('#btnSaveDef').show();

					}
					$('#dmLoader').hide();
					$('.defectManagerSelect').prop('disabled', false);
					$('#btnSaveDef').show();
				},

				error:function(xhr, textStatus, errorThrown){
					/*Modified by Ashiki for Sprint27-9 starts*/
					showMessageOnElement('Could not load Projects from Defect Management Tool. Please contact Tenjin Support','error',$('#user-message-def'));
					/*Modified by Ashiki for Sprint27-9 starts*/
					$('.defectManagerSelect').prop('disabled', false);
					$('#dmLoader').hide();
					$('#btnSaveDef').show();
				}
			})

			};
	});
	
	
	
	
	
	
});


/* Added By Ashiki TENJINCG-986 starts */
$(document).on('change', '.defectManagerSubset', function() {

	clearMessages();
	clearMessagesOnElement($('#msg-text'));
	$('#dmLoader').show();
	var $parentRow = $(this).parent().parent();
	var subSet = $(this).val();
	var project = $parentRow.find('.dttproject').val();
	/*if(instance=='-1'){
		$('#dmLoader').hide();
		return false;
	}*/
	/*Modified by Preeti for V2.8-59 starts*/
	if(subSet=='-1'){
		$('#dmLoader').hide();
		var html= "<select  class='stdTextBox defectManagerProject'>" +
		"<option value='-1'>-- Select One --</option>";
		$parentRow.find('.defectManagerProject').html(html);
		return false;
	}
	/*Modified by Preeti for V2.8-59 ends*/
	$.ajax({
		url:'ProjectServlet',
		data :{param:'Fetch_Subset_Projects',subSet:subSet},
		async:true,
		dataType:'json',
		success: function(data){
			var status = data.status;
			
			if(status == 'SUCCESS')
			{	
				var html= "<select  class='stdTextBox defectManagerProject'>" +
				"<option value='-1'>-- Select One --</option>";
				var dArray = data.projects;
				
				var projectArray=dArray.project.split(',');
				for(var i=0;i<projectArray.length;i++){
				/*Modified by Padmavathi for TNJN27-70 starts*/
					var project=projectArray[i].split(':');
					/*html=html+'<option name="'+projectArray[i]+'" value="'+projectArray[i] + '">'+projectArray[i]+'</option>';;*/
					/* modified by Roshni for TENJINCG-1166 starts */
					/*html=html+'<option name="'+project[0]+'" value="'+project[1] + '">'+project[0]+'</option>';*/
					html=html+'<option name="'+project[0]+'" value="'+project[1] + '">'+escapeHtmlEntities(project[0])+'</option>';
					/* modified by Roshni for TENJINCG-1166 starts */
				/*Modified by Padmavathi for TNJN27-70 ends*/
				} 
				
				$parentRow.find('.dttproject').html(html);
				$('#dmLoader').hide();
			}
			else
			{
				$('#dmLoader').hide();
				showMessage(data.message, 'error');	
				$('.defectManagerSubset').prop('disabled', false);
				$('#btnSaveDef').show();

			}
			$('#dmLoader').hide();
			$('.defectManagerSubset').prop('disabled', false);
			$('#btnSaveDef').show();
		},

		error:function(xhr, textStatus, errorThrown){
			/*Modified by Ashiki for Sprint27-9 starts*/
			showMessageOnElement('Could not load Projects from Defect Management Tool. Please contact Tenjin Support','error',$('#user-message-def'));
			/*Modified by Ashiki for Sprint27-9 ends*/
			$('.defectManagerSubset').prop('disabled', false);
			$('#dmLoader').hide();
			$('#btnSaveDef').show();
		}
		/* Added By Ashiki TENJINCG-986 ends */
	});

});

/*Added by Ashiki for Sprint27-9 starts*/
$(document).on('click', '.dttDelete', function() {
	var app=$(this).attr("app");
	var proceedToSave = true;
	if(app==undefined){
		proceedToSave=false;
	}
	pId = $('#projectId').val();
	var $parentRow = $(this).parent().parent();
	if(proceedToSave === true){

		$.ajax({
			url:'ProjectServletNew',
			data:'t=DTT_UNMAP&p=' + pId + '&j=' + app,
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				var msg=data.message;
				if(status == 'SUCCESS'){	
					$('#dmLoader').hide();
					projectTDPTable.ajax.reload();
					showMessageOnElement(data.message,'success',$('#user-message-def'));
					$('#btnSaveDef').hide();
				}else{
					showMessageOnElement(data.message,'error',$('#user-message-def'));
					$('#dmLoader').hide();
				}
			},
		});
	}
});
/*Added by Ashiki for Sprint27-9 ends*/
