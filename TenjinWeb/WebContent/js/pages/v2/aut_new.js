/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  aut_new.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 27-Oct-2017		   Padmavathi 		       Newly added for TENJINCG-400
* 24-Nov-2017			Sahana					Updated appType to integer,UI changes
* 16-03-2018			Preeti					TENJINCG-73
* 22-03-2018			Preeti					Modified for TENJINCG-73
* 28-05-2018            Leelaprasad             TENJINCG-674
* 15-06-2018			Preeti					T251IT-24
* 18-06-2018			Preeti					T251IT-95
* 20-06-2018			Preeti					T251IT-94
* 30-11-2018            Padmavathi              TJNUN262-33
* 21-03-2019            Padmavathi              TENJINCG-1015
* 25-03-2019			Pushpalatha				TENJINCG-968
* 10-04-2019			Preeti					TENJINCG-1026
* 09-04-2020            Shruthi                 TENJINCG-1142

*/

$(document).ready(function() {
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	 mandatoryFields('autform');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	/*Added by Preeti for T251IT-24 starts*/
	$('#txtAutBrowser').children('option').each(function(){
        var browser = $('#lstBrowser').val();
        var val = $(this).val();
        if(val == browser){
            $(this).attr({'selected':true});
        }
    });
	$('#txtAppType').children('option').each(function(){
        var appType = $('#lstAppType').val();
        var val = $(this).val();
        if(val == appType){
            $(this).attr({'selected':true});
        }
    });
	$('#adapterPackage').children('option').each(function(){
        var adapter = $('#lstAdapter').val();
        var val = $(this).val();
        if(val == adapter){
            $(this).attr({'selected':true});
        }
    });
	/*Added by Preeti for T251IT-24 ends*/
	
	$(document).on('click', '#btnCancel', function() {
		var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
		if(confirmResult) {
			window.location.href='ApplicationServlet';
		}else{
			return false;
		}
	});
	/*Added by Preeti for TENJINCG-73 starts*/
	var pauseLocation = $('#txtPauselocation').val();
	$('#txtLocation').val(pauseLocation);
	/*Added by Preeti for TENJINCG-73 ends*/
	
	/*Added by Preeti for T251IT-94 starts*/
	var appType= $('#txtAppType').val();
	if(appType!=1 && appType!=2)
	{
		$("#txtPlatform").attr('mandatory','yes');
		//$("#txtPackage").attr('mandatory','yes');
		$("#txtAutURL").removeAttr('mandatory');
		$("#txtAutBrowser").removeAttr('mandatory');
		$('.show-for-all').hide();
		$('.mobileTxt').show();
		if(appType==3)
		{
			$('#txtPackageAnd').show();
			$("#txtActivity").attr('mandatory','yes');
		//	$("#txtPackage").attr('mandatory','yes');
			$('#txtPackageIos').hide();
		}
		else if(appType==4)
		{
			$("#txtActivity").removeAttr('mandatory');
			$('#txtPackageAnd').hide();
			$('#txtPackageIos').show();
			$('.android').hide();
		}
	}
	else
	{
		$('#txtPlatform').val("Web");
		$("#txtAutURL").attr('mandatory','yes');
		$("#txtAutBrowser").attr('mandatory','yes');
		$("#txtPlatform").attr('mandatory','yes');
		$("#txtPackage").removeAttr('mandatory');
		$("#txtActivity").removeAttr('mandatory');
		$('.show-for-all').show();
		$('.mobileTxt').hide();

	}
	/*Added by Preeti for T251IT-94 ends*/
	
	$("#txtAutURL").attr('mandatory','yes');
	$("#txtAutBrowser").attr('mandatory','yes');
	
	$('#txtAppType').on('change', function() {
		var appType= $('#txtAppType').val();

		if(appType!=1 && appType!=2)
		{
			$("#txtPlatform").attr('mandatory','yes');
			$("#txtPackage").attr('mandatory','yes');
			//$("#txtPlatformVersion").attr('mandatory','yes');
			$("#txtAutURL").removeAttr('mandatory');
			$("#txtAutBrowser").removeAttr('mandatory');

			$('.show-for-all').hide();
			$('.mobileTxt').show();

			if(appType==3)
			{
				$("#txtActivity").attr('mandatory','yes');
			//	$("#txtPackageAnd").attr('mandatory','yes');
				//$("#txtPackage").attr('mandatory','yes');
				$('#txtPackageAnd').show();
				$('#txtPackageIos').hide();

			}
			else if(appType==4)
			{
				$("#txtActivity").removeAttr('mandatory');
				$('#txtPackageAnd').hide();
				$('#txtPackageIos').show();
				$('.android').hide();
			}
		}
		else
		{
			$('#txtPlatform').val("Web");
			$("#txtAutURL").attr('mandatory','yes');
			$("#txtAutBrowser").attr('mandatory','yes');
			$("#txtPlatform").attr('mandatory','yes');
			$("#txtPackage").removeAttr('mandatory');
			//$("#txtPlatformVersion").removeAttr('mandatory');
			$("#txtActivity").removeAttr('mandatory');
			$('.show-for-all').show();
			$('.mobileTxt').hide();
			/*Changed by leelaprasad for the requirement defect TENJINCG-674 starts*/
			/*if(appType==2)
			$('.browser').hide();*/
			/*Changed by leelaprasad for the requirement defect TENJINCG-674 ends*/
			

		}
	});
	
	/*$('#txtPlatformVersion').keyup(function (){
		var inputVal = $(this).val();
		var regex = new RegExp(/[^0-9\.]/g);
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			alert('Please enter valid platform version');
			$(this).val('');
		}
	});*/
	$('#txtDate').hide();
	$('#dateFormat').keyup(function() {
	}).focus(function() {
		$('#txtDate').show();
		/*Added by Padmavathi for  TENJINCG-1015 starts*/
		$(document).scrollTop($(document).height());
		/*Added by Padmavathi for  TENJINCG-1015 ends*/
	}).blur(function() {
		$('#txtDate').hide();
	});
	$('#btnSave').click(function() {
		/*Added by Padmavathi for TJNUN262-33 starts*/
		var dateFormat=$('#dateFormat').val()
		var regex = new RegExp(/[0-9]/g);
		var containsNonNumeric = dateFormat.match(regex);
		if(containsNonNumeric){
			showMessage("Please enter valid date format", 'error');
			return false;
		}
		/*Added by Padmavathi for TJNUN262-33 ends*/
		var dateValue=isValidDate($('#dateFormat').val());
		if(!dateValue){
			showMessage("Please enter valid date format", 'error');
			return false;
		}
		/*Added by Preeti for TENJINCG-73 starts*/
		var pauseTime= $('#txtPauseTime').val();
		var pauseLocation= $('#txtLocation').val();
		if(isNaN(pauseTime) && !((pauseTime%1)===0)) {
			showMessage("Please enter valid value for pause time", 'error');
			return false;
		}
		/*Modified by Preeti for TENJINCG-1026 starts*/
		/*if(pauseTime.includes("."))*/
		if(pauseTime.indexOf(".") != -1)
		/*Modified by Preeti for TENJINCG-1026 ends*/
		{
			showMessage("Please enter valid value for pause time", 'error');
			return false;
		}
		if(pauseTime> 2147483647){
			showMessage("Pause Time exeeds the limit. Please enter less value", 'error');
			return false;
		}
		/*Added by Preeti for TENJINCG-73 ends*/
		/*Added by Preeti for T251IT-95 starts*/
		if(pauseLocation =='' &&  pauseTime !=''){
			showMessage("Please enter Pause location", 'error');
			return false;
		}
		if(pauseLocation !='' &&  pauseTime ==''){
			showMessage("Please enter Pause time", 'error');
			return false;
		}
		/*Added by shruthi for TENJINCG-1142 starts*/
		var appType= $('#txtAppType').val();
		if(appType!=3 && appType!=4){
		var uri = $("#txtAutURL").val()
        var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        if (!pattern.test(uri)) {
            showMessage("Please enter valid URI", 'error');
            return false;
        }}
        /*Added by shruthi for TENJINCG-1142 ends*/
        
		/*Added by Preeti for T251IT-95 ends*/
		/* Added by paneendra for VAPT fix starts*/
		var templatePwd = $('#txtTemplatePwd').val();
		if(templatePwd.length>0){
		if(templatePwd.length < 8){
			clearMessages();
			showMessage('Password should be minimum 8 characters', 'error');
			return false;
		}
		
		var tmpPassword=document.getElementById('txtTemplatePwd');
		if(!CheckPassword(tmpPassword)){
			showMessage("Please check the password rules","error");
			return false;
			 
		}/* Added by paneendra for VAPT fix ends*/
		
		}
		
		
		
		
	});
		function isValidDate(date)
		{
			if(date.trim()!=""){
				var matches =/([0-9])|([a-z])/.exec(date);
				if (matches == null) return false;
				else return true;
			}
			else{
				return true;
			}

		}
});