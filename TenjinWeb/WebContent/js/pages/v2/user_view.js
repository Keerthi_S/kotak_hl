
/*Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  user_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

*//*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 03-11-2017				Roshni Das		Newly added for TENJINCG-398
 08-01-2017            Padmavathi              for TENJINCG-578
 14-06-2018            Padmavathi              for T251IT-11
 02-07-2018            Padmavathi              for T251IT-166
 28-11-2018			   Ashiki				   for TJNUN262-9
 25-03-2019			   Pushpalatha			   TENJINCG-968
 07-06-2019			   Pushpalatha             TENJINCG-1067,1070
 28-06-2019			   Pushpalatha			   V2.8-177,178
 03-12-2019			   Pushpalatha			   TENJINCG-1170
*//*
*/


$(document).ready(function() {
	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('userForm');
	mandatoryFields('userForm1');
	
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	/*Added by Pushpa forV2.8-177,178 starts */
	var loogedinUserRole=$('#userRole').val();
	if(loogedinUserRole=='Tenjin User'){
		$("#btnBack").hide(); 
	}
	/*Added by Pushpa forV2.8-177,178 ends */
	/*Added by Pushpa for TENJINCG-1070,1067 starts */
	
	
	$('#appReportDateFrom').datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		dateFormat:'dd-M-yy' ,
		maxDate : new Date()
	});
	$('#appReportDateTo').datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		dateFormat:'dd-M-yy' ,
		maxDate : new Date()
	});
	
	$("#fromDate").datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		disabled: false,
		dateFormat: 'dd-mm-y',
		maxDate : new Date(),
		onSelect: function (selected) {
			clearMessages();
			var dt = new Date(selected);
			dt.setDate(dt.getDate());
			$("#toDate").datepicker("option", "minDate", dt);
		}
	});
	
	
	$("#toDate").datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		disabled: false,
		dateFormat: 'dd-mm-y',
		maxDate : new Date(),
		onSelect: function (selected) {
			clearMessages();
			var dt = new Date(selected);
			dt.setDate(dt.getDate());
			/*dt.setDate(dt.getDate()-1);*/
			$("#fromDate").datepicker("option", "maxDate", dt);
		}
	});
	/*Added by Pushpa for TENJINCG-1070,1067 ends */
	
					/*Added by Padmavathi for T251IT-166 starts*/
					var userRole=$('#userRole').val()
					if(userRole=='Tester'){
						$('#btnBack').hide();
					}
					/*Added by Padmavathi for T251IT-166 ends*/
					var userId = $('#id').val();
					var cUserId = $('#cUserId').val();
					/*Modified by Pushpa for TENJINCG-1170 starts*/
					if (userId == cUserId || userRole == 'Tenjin user') {
					/*Modified by Pushpa for TENJINCG-1170 ends*/
						$('#btnEnable').hide();
						/*Added by Ashiki for TJNUN262-9 starts*/
						$('#roles').prop('disabled',true);
						/*Added by Ashiki for TJNUN262-9 starts*/
					}
					
					/*Added by Pushpa for TENJINCG-1170 starts*/
					if (!userId == cUserId && userRole == 'Tenjin user') {
						$('#btnChangePwd').hide();
					}
					
					if (!userId == cUserId && userRole == 'Tenjin user') {
						$('#btnSave').hide();
					}
					/*Added by Pushpa for TENJINCG-1170 ends*/

					$.fn.toggleAttr = function(attr, attr1, attr2) {
						return this.each(function() {
							var self = $(this);
							if (self.attr(attr) == attr1)
								self.attr(attr, attr2);
							else
								self.attr(attr, attr1);
						});
					};

					$('#btnEnable').click(function() {

										var status = $('#btnEnable').val();
										var confirmResult = '';
										if (status == 'Enable') {
											confirmResult = confirm(getLocalizedMessage(
													'confirm.user.enable', ''));
										} else {
											confirmResult = confirm(getLocalizedMessage(
													'confirm.user.disable', ''));
										}
										if (confirmResult) {
											$('#btnEnable').toggleAttr('value',
													'Enable', 'Disable');
											$('#btnEnable').toggleAttr('class',
													'imagebutton enable',
													'imagebutton diasble');
											/*Modified by Padmavathi for T251IT-11 starts*/
											/*$
												
											.ajax({
														type : "POST",
														url : 'TenjinUserServlet?operation=updateStatus&id='
																+ userId
																+ '&status='
																+ status

													})*/
											$('#userForm').append($("<input type='hidden' id='updateStatus' name='updateStatus' value='true' />"));
											$('#userForm').append($("<input type='hidden' id='id' name='id' value='"+userId+"' />"));
											$('#userForm').append($("<input type='hidden' id='status' name='status' value='"+status+"' />"));
											$('#userForm').submit();
											/*Modified by Padmavathi for T251IT-11 ends*/
										} else {
											return false;
										}
									})
					$('#btnChangePwd')
							.click(
									function() {
										window.location.href = "UserPasswordServlet?operation=changepassword&userid="
												+ userId;
									})
					$('#btnBack').click(function() {
						window.location.href = "TenjinUserServlet";
					});
					
				/*	commented by shruthi for TCGST-67 starts	*/
				/*	Added by Leelaprasad  for TENJINCG-1042 starts*/
			/*		$('#btnPwdReport').click(function() {
						var user = $('#id').val();
						$('#img_report_loader').show();
						$.ajax({
							url:'TenjinUserServlet',
							data:'t=password_report &user='+user,
							async:true,
							dataType:'json',
							success:function(data){
								var status = data.status;
								if(status == 'SUCCESS'){
									var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
									$("body").append($c);
					                $("#downloadFile").get(0).click();
					                $c.remove();
					                $('#img_report_loader').hide();
								}else{
									var message = data.message;
									showMessage(message,'error');
									$('#report-gen').prop('disabled',false);
									$('#img_report_loader').hide();
								}
							},
							error:function(xhr, textStatus, errorThrown){
								$('#img_report_loader').hide();
								showMessage(errorThrown, 'error');
							}
					});
					});*/
					
					/*Added by Leelaprasad  for TENJINCG-1042 ends*/
					/*commented by shruthi for TCGST-67 ends*/
					
					/*Added by Pushpa for TENJINCG-1070,1067 starts */
					$('#userActivityReport').click(function(){
						$('#appReportDateFrom').val('');
						$('#appReportDateTo').val('');
						$('.modalmask').show();
						$('#uploadTestCases').show();
					});
					
					$('#btnOk').click(function(){
						var user = $('#id').val();
						var reportType=$('#reportType').val();
						
						var startDate=$('#appReportDateFrom').val();
						var endDate=$('#appReportDateTo').val();
						$('#img_report_loader').show();
						$.ajax({
							url:'TenjinUserServlet',
							data:'t=user_activity_report&user='+user+'&startDate='+startDate+'&endDate='+endDate+'&reportType='+reportType,
							async:true,
							dataType:'json',
							success:function(data){
								var status = data.status;
								if(status == 'SUCCESS'){
									var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
									$("body").append($c);
					                $("#downloadFile").get(0).click();
					                $c.remove();
					                $('#img_report_loader').hide();
					                $('.modalmask').hide();
									$('#uploadTestCases').hide();
								}else{
									var message = data.message;
									showMessage(message,'error');
									$('#report-gen').prop('disabled',false);
									$('#img_report_loader').hide();
									$('.modalmask').hide();
									$('#uploadTestCases').hide();
								}
							},
							error:function(xhr, textStatus, errorThrown){
								$('#img_report_loader').hide();
								showMessage(errorThrown, 'error');
								$('.modalmask').hide();
								$('#uploadTestCases').hide();
							}
						});
					});
					
					$('#btnCCancel').click(function(){
						$('.modalmask').hide();
						$('#uploadTestCases').hide();
					});
					
					/*Added by Pushpa for TENJINCG-1070,1067 ends */
					$('#btnSave')
							.click(
									function() {
										if ($('#email').val() != '') {
											if (!validateEmail()) {
												showMessage(
														"Please enter valid email address",
														"error");
												return false;
											}
										}

										var primaryPhone = $('#primaryPhone')
												.val();
										if (primaryPhone != "") {
											if (/[a-zA-Z!@#$%&*?<^|(){}>=_`~,]/
													.test(primaryPhone)) {
												showMessage(
														"Please enter valid contact number.",
														"error");
												return false;
											}
											if (primaryPhone.length > 6) {
												var phoneno = new RegExp(
														/^((\+)?[0-9]{1,3})?([-\s\.])?(\(\d{1,11}\))$/g);
												if ((primaryPhone
														.match(phoneno))) {
													showMessage("Please enter valid contact number.");
													return false;
												}

											} else {
												showMessage("Contact number should be atleast 6 digits.");
												return false;
											}

										}
									});
					
				});
function validateEmail() {
	var email = document.forms["userForm"]["email"].value;
	var atstr = email.indexOf("@");
	var dotstr = email.lastIndexOf(".");
	if (atstr < 1 || dotstr < atstr + 2 || dotstr + 2 >= email.length) {

		return false;
	} else {
		clearMessages();
		return true;
	}
}
