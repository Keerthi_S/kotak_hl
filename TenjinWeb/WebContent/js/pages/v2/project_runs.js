
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  project_runs.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 24-10-2018			Sriram					File optimized for TENJINCG-866
* 30-10-2018			Sriram					TENJINCG-886
* 05-12-2018	     	Padmavathi				for TJNUN262-54 
* 13-12-2018	     	Padmavathi				for TJNUN262-97
* 13-03-2019			Preeti					TENJINCG-987
* 20-03-2019            Padmavathi              TENJINCG-1010
* 20-03-2019            Padmavathi              TENJINCG-1014
* 11-06-2019			Roshni					TJN27-7
* 12-06-2019			Preeti					V2.8-56
* 25-05-2020			Pushpalatha				TENJINCG-1210
* 28-09-2020			Ashiki					TENJINCG-1211
*/

var projectRunsTable;
var searchUrl = "TestRunServlet?param=search";
var runStatusLabel = {
		"Pass":"label-pass",
		"Fail":"label-fail",
		"Error":"label-error",
		"Aborted":"label-aborted",
		"Not Started":"label-not-started",
		"Executing":"label-executing"
};

$(document).ready(function() {
   /*Added by Priyanka for TENJINCG-1211 starts*/
	$('#inProgress').hide();
	/*Added by Priyanka for TENJINCG-1211 ends*/
	initTable();
	populateProjectUsers();
	//populateProjectApps();
	$('#startedOn').datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		dateFormat:'dd-M-yy' ,
		/*Added by Padmavathi for TJNUN262-54 starts*/
		maxDate : new Date()
	/*Added by Padmavathi for TJNUN262-54 ends*/
	});
});

/*function showSearchModal() {
	$('.modalmask').show();
	$('.subframe').show();
}

function closeModal() {
	$('.subframe').hide();
	$('.modalmask').hide();
}

$(document).on('click', '#btnfilter', function() {
	showSearchModal();
});

$(document).on('click', '#btnCancel', function() {
	closeModal();
});

$(document).on('click', '.modalmask', function() {
	closeModal();
});*/
/*Added by Pushpa for TENJINCG-1210 starts*/
$(document).on('click','#download',function(){
	var selectedRuns="";
	$('#prj_runs').find('input[type="checkbox"]:checked').each(function () {
		if(this.id != 'tbl-select-all-rows-tc'){
			selectedRuns = selectedRuns + this.id +',';
		}
	});
	
	if(selectedRuns.length < 1){
		 showMessage("Please select at least one Run to download", 'error');
	}
	
		$('#img_download_loader').show();
		$('#download').prop('disabled',true);
			  $.ajax({
			     url:'ResultsServlet',
			     data:'param=download_runs&runlist=' + selectedRuns,
			     async:true,
			     dataType:'json',
			      success:function(data){
				         var status = data.status;
				            if(status == 'SUCCESS')
				            {
								showMessage(data.message,'success');
					            var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
				                $("body").append($c);
				                $("#downloadFile").get(0).click();
				                $c.remove();
				             }
				            else
				             {
					            showMessage(data.message,'error');
				             }
				            $('#img_download_loader').hide();
				            $('#download').prop('disabled',false);
			            },
			       error:function(xhr, textStatus, errorThrown){
				           showMessage(errorThrown,'error');
				           $('#img_download_loader').hide();
				           $('#download').prop('disabled',false);
			          },
			      });
		
	
	
});
/*Added by Pushpa for TENJINCG-1210 ends*/

$(document).on('click', '#btnSearch', function() {
	/* Added by Padmavathi for TENJINCG-1014 starts */
	var startedOn=$('#startedOn').val();
	if(startedOn!=''){
		var todaysDate=new Date();
		todaysDate.setHours(0,0,0,0);
		if(!validateDate(startedOn)){
			showMessage('Please enter valid Started On date in this format DD-MMM-YYYY.','error');
			return false;
		}else if(new Date(startedOn)>todaysDate){
			showMessage("Please enter Started On date should be less than or equal to Today's Date","error");
			return false;
		}
		
	}
	/* Added by Padmavathi for TENJINCG-1014 ends */
	searchRuns();
});

$(document).on('click', '#showAllRuns', function() {
	window.location.href='TestRunServlet';
});

/*Added by Pushpa for TENJINCG-1223 starts*/
$(document).on('click', '#btnOk', function() {
	$('#deletemodal').modal('hide');
	var selectedRuns="";
	$('#prj_runs').find('input[type="checkbox"]:checked').each(function () {
		if(this.id != 'tbl-select-all-rows-tc'){
			selectedRuns = selectedRuns + this.id +',';
		}
	});
	
	if(selectedRuns.length < 1){
		showMessage("Please choose atleast one record to delete.","error");
	}else{
	/*modified by shruthi for TENJINCG-1224 starts*/
	    var preserveDefect= $('#preserveDefect').prop('checked');
		data='param=delete_runs&runlist=' + selectedRuns;
		window.location.href="ResultsServlet?param=delete_runs&runlist=" + selectedRuns+"&preserveDefect="+preserveDefect;
	/*modifield by shruthi for TENJINCG-1224 ends*/
	}
});
/*Added by Pushpa for TENJINCG-1223 ends*/
function searchRuns() {
	var searchRequest = {};
	searchRequest.param = "search";
	searchRequest.runid = $('#txtRunId').val();
	searchRequest.startedBy = $('#startedBy').val();
	searchRequest.startedOn = $('#startedOn').val();
	searchRequest.result = $('#result').val();
	searchRequest.testCase = $('#testCaseId').val();
	searchRequest.testStep = $('#testStepId').val();
	searchRequest.app = $('#application').val();
	searchRequest.function = $('#function').val();
	
	redrawTableForSearch(searchRequest);
	$('#filtermodal').modal('hide');
	
}
//TENJINCG-886 (Sriram)
$(document).on('click', '#btnMyRuns', function() {
	var currentUser = $('#h-currentUserId').val();
	$('#startedBy').val(currentUser);
	
	searchRuns();
});
//TENJINCG-886 (Sriram) ends

function redrawTableForSearch(searchRequest) {
	var url = 'TestRunServlet?';
	var params = '';
	if(searchRequest) {
		for(var key in searchRequest) {
			if(searchRequest.hasOwnProperty(key)) {
					if(searchRequest[key] && searchRequest[key] !== '' && searchRequest[key] !== '-1') {
					if(params !== '') {
						params += '&';
					}
					params += key + '=' + searchRequest[key];
				}
			}
		}
	}
	
	if(params !== '') {
		url += params;
	}else{
		url = 'TestRunServlet?param=search';
	}
	
	
	url = encodeURI(url); //added to encode special characters in the search request
	projectRunsTable.ajax.url(url);
	projectRunsTable.draw();
}
/*Modified by Pushpa for TENJINCG-1210 starts*/
function initTable() {
	projectRunsTable = $('#prj_runs').DataTable({
		"processing":true,
		"serverSide":true,
		"searching":false,
		"ajax":{"url":searchUrl,"dataSrc":"records"},
		"columns": [
			/*{"data":"id"},*/
			{data:"id"},
			{"data":"id"},
			{"data":"testSetName"},
			{"data":"user"},
			{"data":"startTimeStamp"},
			{"data":"endTimeStamp"},
			{"data":"elapsedTime"},
			{"data":"status"},
			/* Added by Padmavathi for TENJINCG-1010 starts */
			{"data":"executedLevel"}
			/* Added by Padmavathi for TENJINCG-1010 ends */
		],
		"columnDefs":[{
			"targets":0,
			"searchable":false,
			"orderable":false,
			"className":"select-row",
			"render":function(data, type, full, meta) {
				return "<input type='checkbox' data-run-id='" + data + "' id='" + data + "' class='synch' />";
			}
		}, {
			"targets":1,
			"render":function(data, type, full, meta) {
//				console.log(data);
//				console.log(full);
				return "<a href='ResultsServlet?param=run_result&callback=runlist&run=" + data + "'>" + data + "</a>";
			}
		}/*,{"targets":0,
		"render":function(data, type, full, meta) {
//			console.log(data);
//			console.log(full);
			return "";
		}
	}*/,{
			"targets":7,
			"render":function(data, type, full, meta) {
				var labelClass = runStatusLabel[data];
				//console.log("Label class for this run is " + labelClass);
				return "<span class='label " + labelClass + "'>" + data + "</span>";
			}
		}, //TENJINCG-886 (Sriram)
		{
			"targets":4,
			"render":function(data, type, full, meta) {
				return "<span class='ts-small'>" + data + "</span>";
			}
		},{
			"targets":5,
			"render":function(data, type, full, meta) {
				return "<span class='ts-small'>" + data + "</span>";
			}
		},//TENJINCG-886 (Sriram) ends
		/*Added by Padmavathi for TJNUN262-97 starts*/
		{
			"targets":6,
			"orderable":false,
		}
		/*Added by Padmavathi for TJNUN262-97 ends*/
		/*Added by Preeti for TENJINCG-987 starts*/
		,{
			/*Modified by Preeti for V2.8-56 starts*/
			/*"targets":[0,3,4,5],*/
			"targets":[1,6],
			/*Modified by Preeti for V2.8-56 ends*/
			"className":"text-center"
		},
		/*Added by Preeti for TENJINCG-987 ends*/
		/*Added by Padmavathi for TENJINCG-1010 starts*/
		{
			"targets":8,
		/*	"orderable":false,*/
			"render":function(data,type,full,meta){
				if(data=='TS'){
					return 'Test Set'
				}else{
					return 'Test Case'
				}
				
			}
		}
		/*Added by Padmavathi for TENJINCG-1010 ends*/
		
		],
		/*Added by Roshni for TJN27-7 starts */
		"order": [[0, 'desc' ]]
		/*Added by Roshni for TJN27-7 ends */
	});
}
//Modified by Pushpa for TENJINCG-1210 ends
function populateProjectUsers() {
	var projectId = $('#h-projectId').val();
	
	$.ajax({
		url:'ProjectAjaxServlet?t=search_prj_users&projectId=' + projectId,
		dataType:'json',
		async:true,
		success:function(result) {
			//Added by prem to restrict unauthorized users starts
			var status = result.status;
			if(status=='error'){
				showMessage(result.message,'error');
				window.location.href="ProjectServletNew?t=";
			}
			
			else{ 
				//Added by prem to restrict unauthorized users Ends
				if(result) {
				result.forEach(function(user){
					var fullName = user.firstName;
					if(user.lastName) fullName += " " + user.lastName;
					$('#startedBy').append($("<option value='" + user.id + "'>" + fullName + "</option>"));
				});
			}else{
				console.error("An error occurred. Could not process project users");
				showMessage(result.message,'error');
			}
			}
		}, error:function(xhr, textStatus, errorThrown) {
			console.error("An error occurred. Could not process project users");
			showMessage(result.message,'error');
		}
	})
}
/*Added by Ashiki for TENJINCG-1211 starts*/
$(document).on('click','#consolated',function(){
	var prjId = $('#h-projectId').val()
	$('#inProgress').show();
	  $.ajax({
		     url:'ResultsServlet?param=consoliated&projectId='+prjId,
		     async:true,
		     dataType:'json',
		      success:function(data){
			         var status = data.status;
			            if(status == 'SUCCESS')
			            {
			            	$('#inProgress').hide();
							showMessage(data.message,'success');
			             }
			            else
			             {
			            	$('#inProgress').hide();
				            showMessage(data.message,'error');
			             }
			            $('#img_download_loader').hide();
			            $('#download').prop('disabled',false);
		            },
		       error:function(xhr, textStatus, errorThrown){
		    	   $('#inProgress').hide();
			           showMessage(errorThrown,'error');
			           $('#img_download_loader').hide();
			           $('#download').prop('disabled',false);
		          },
		      });
	
});
/*Added by Ashiki for TENJINCG-1211 ends*/
