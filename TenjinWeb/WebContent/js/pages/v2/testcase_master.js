
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testcase_master.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 06-12-2018            Padmavathi              newly added for TJNUN262-3 & TJNUN262-69 
 * 26-02-2018            Padmavathi              TENJINCG-977
 */




var unCheckSelectedTc=[];
var selectedCases=[];
$(document).on('change','.synch',function(){
	 //adding selected testcase recids to selectedCases array
	if($(this).is(':checked')){
		selectedCases.push($(this).data('testcaseRecId'));
		if(selectedCases.length>0){
			 //displaying selected testcase recids count
			$('#Selected_count').html('('+selectedCases.length+' Selected)');
			/*Commented by Padmavathi for TENJINCG-977 starts*/
			/*$('#clearTestCases').show();*/
			/*Commented by Padmavathi for TENJINCG-977 ends*/
		}
	}else{
		 //removing un selected testcase recids from selectedCases array
		var itemtoRemove = $(this).data('testcaseRecId');
		selectedCases.splice($.inArray(itemtoRemove, selectedCases),1);
		if(selectedCases.length>0){
			 //displaying selected testcase recids count
			$('#Selected_count').html('('+selectedCases.length+' Selected)');
			/*Commented by Padmavathi for TENJINCG-977 starts*/
			/*$('#clearTestCases').show();*/
			/*Commented by Padmavathi for TENJINCG-977 ends*/
		}else{
			 //displaying selected testcase recids count
			$('#Selected_count').html('');
			/*Commented by Padmavathi for TENJINCG-977 starts*/
			/*$('#clearTestCases').hide();*/
			/*Commented by Padmavathi for TENJINCG-977 ends*/
		}
	}
	
});
$(document).on('change','.tbl-select-all-rows',function(){
   //adding selected testcase recids to selectedCases array
	$('#testcase-table').find('input[type="checkbox"]:checked').each(function () {
	       if(this.id != 'tbl-select-all-rows-tc'){
	    	   var itemtoRemove = $(this).data('testcaseRecId');
	    	 //checking if selected testcases or not there adding testcase recids to selectedCases array
	    	   if($.inArray(itemtoRemove, selectedCases)=='-1'){
	    		   selectedCases.push($(this).data('testcaseRecId'));
	    	   }
	    	  
	       }
	       
	});
	$('#testcase-table').find('input[type="checkbox"]:not(:checked)').each(function () {
		 //removing un selected testcase recids from selectedCases array
		var itemtoRemove = $(this).data('testcaseRecId');
		if(itemtoRemove!=undefined && ($.inArray(itemtoRemove, selectedCases))!='-1'){
			selectedCases.splice($.inArray(itemtoRemove, selectedCases),1);
		}
		 //displaying selected testcase recids count
		if(selectedCases.length>0){
			$('#Selected_count').html('('+selectedCases.length+' Selected)');
			/*Commented by Padmavathi for TENJINCG-977 starts*/
			/*$('#clearTestCases').show();*/
			/*Commented by Padmavathi for TENJINCG-977 ends*/
		}else{
			$('#Selected_count').html('');
			/*Commented by Padmavathi for TENJINCG-977 starts*/
			/*$('#clearTestCases').hide();*/
			/*Commented by Padmavathi for TENJINCG-977 ends*/
		}
	       
	});
	 //displaying selected testcase recids count
	if(selectedCases.length>0){
		$('#Selected_count').html('('+selectedCases.length+' Selected)');
		/*Commented by Padmavathi for TENJINCG-977 starts*/
		/*$('#clearTestCases').show();*/
		/*Commented by Padmavathi for TENJINCG-977 ends*/
	}else{
		//hiding selected testcase recids count
		$('#Selected_count').html('');
		/*Commented by Padmavathi for TENJINCG-977 starts*/
		/*$('#clearTestCases').hide();*/
		/*Commented by Padmavathi for TENJINCG-977 ends*/
	}
});
//To make select checkboxes  based on selectedCases array values
function checkCheckboxes(){
		if(selectedCases.length>0){
			$('#testcase-table').find('input[type="checkbox"]').each(function() {
				for (var index = 0; index < selectedCases.length; index++) {
					if(selectedCases[index]==$(this).data('testcaseRecId'))
						$(this).prop("checked", true);
					continue;
				}
			});
		}
		if(selectedCases.length>0){
			$('#Selected_count').html('('+selectedCases.length+' Selected)');
			/*Commented by Padmavathi for TENJINCG-977 starts*/
			/*$('#clearTestCases').show();*/
			/*Commented by Padmavathi for TENJINCG-977 ends*/
		}
}
//To remove all selected testcases
function unCheckCheckboxes(){
	if(unCheckSelectedTc.length>0){
		$('#testcase-table').find('input[type="checkbox"]').each(function() {
			for (var index = 0; index < unCheckSelectedTc.length; index++) {
				if(unCheckSelectedTc[index]==$(this).data('testcaseRecId'))
					$(this).prop("checked", false);
				continue;
			}
		});
		$('#Selected_count').html('');
		/*Commented by Padmavathi for TENJINCG-977 starts*/
		/*$('#clearTestCases').hide();*/
		/*Commented by Padmavathi for TENJINCG-977 ends*/
		$('.tbl-select-all-rows').prop('checked',false)
		unCheckSelectedTc=[];
	}
}
/*Added by Padmavathi for TJNUN262-3 ends*/
