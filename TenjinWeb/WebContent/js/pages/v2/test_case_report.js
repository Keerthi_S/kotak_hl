/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  test_case_report.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 *
 * DATE              	CHANGED BY              DESCRIPTION
 * 23-05-2019			Preeti					TENJINCG-1061,1062
 * 17-06-2019			Preeti					V2.8-149
 * 20-06-2019			Preeti					V2.8-162
 * 03-02-2021			Paneendra	            Tenj210-158 
 */

$(document).ready(function() {
	mandatoryFields('main_form');
	$('#caseExecDetail').hide();
	$('#caseExecHistory').hide();
	$('#img_report_loader').hide();
	$('#tcReport').change(function(){
		var reportType = $('#tcReport').val();
		if(reportType==1){
			$('#caseExecDetail').show();
			$('#caseExecHistory').hide();
		}
		else if(reportType==2){
			$('#caseExecHistory').show();
			$('#caseExecDetail').hide();
		}
		else{
			$('#caseExecDetail').hide();
			$('#caseExecHistory').hide();
		}
	});
	$('#execDetailExOn').datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		dateFormat:'dd-M-yy' ,
		maxDate : new Date()
	});
	$('#execHistDateFrom').datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		dateFormat:'dd-M-yy' ,
		maxDate : new Date()
	});
	$('#execHistDateto').datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		dateFormat:'dd-M-yy' ,
		maxDate : new Date()
	});
	
	/* Added by Paneendra for  Tenj210-158 starts*/
	
	$('#execDetailTC').change(function(){
		clearMessages();
		var tc = $('#execDetailTC').val();
		var options='';
		$('#execDetailExOn').empty();
		if(tc){
			$.ajax({
				url:'TestReportServlet',
				data :'param=get_testcase_dates&tc=' + tc,
				async:false,
				dataType:'json',
				success: function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						var dArray = data.runDates;
						for(var i=0;i<dArray.length;i++){
							var runDate = dArray[i];
							options += '<option value="' + runDate.runDate + '">' + runDate.runDate + '</option>';
						} 
						$('#execDetailExOn').append(options);
					}else{
						
						showMessage(data.message, 'error');
					}
					
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(textStatus, 'error');			
				}
			});
		}else{
			options += '<option value="">--Select One--</option>';
			$('#execDetailExOn').append(options);
		}
	});
	
	/*get executed dates  for test case execution detail */
	
	/* fetching the dates for test case execution history*/
	$('#execDetailTC1').change(function(){
		clearMessages();
		var tc = $('#execDetailTC1').val();
		var options='';
		$('#execHistDateFrom').empty();
		$('#execHistDateto').empty();
		if(tc){
			$.ajax({
				url:'TestReportServlet',
				data :'param=get_testcase_dates&tc=' + tc,
				async:false,
				dataType:'json',
				success: function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						var dArray = data.runDates;
						for(var i=0;i<dArray.length;i++){
							var runDate = dArray[i];
							options += '<option value="' + runDate.runDate + '">' + runDate.runDate + '</option>';
						} 
						$('#execHistDateFrom').append(options);
						$('#execHistDateto').append(options);
					}/*
							options += '<option value="">--Select One--</option>';
							$('#execHistDateFrom').append(options);
							$('#execHistDateto').append(options);
						
					*/
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(textStatus, 'error');			
				}
			});
		}else{
			options += '<option value="">--Select One--</option>';
			$('#execHistDateFrom').append(options);
			$('#execHistDateto').append(options);
		}
	});
	
	/* Added by Paneendra for  Tenj210-158 ends*/
	
	
	 /* fetching the dates for test case execution history*/
	$('#btnGenerate').click(function(){
		clearMessages();
		var reportFor = $('#tcReport').val();
		var param = '';
		/*Added by Ruksar for TENJINCG-1146 starts*/
		if(reportFor==''|| reportFor==-1)
			{
			showMessage("Please select atleast one report to continue", 'error');
			return false;
			}
		/*Added by Ruksar for TENJINCG-1146 ends*/
		if(reportFor){
			$('#img_report_loader').show();
			if(reportFor==1){
				param = generateTCExecDetailReport();
			}
			else if(reportFor==2){
				param = generateTCExecHistoryReport();
			}
			if(param){
			$.ajax({
				url:'TestReportServlet',
				data:param,
				async:true,
				dataType:'json',
				success:function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						/*var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");*/
						var $c = $("<a id='downloadFile'  href='DownloadServlet?param="+data.path+"' target='_blank' download />");
						$("body").append($c);
						$("#downloadFile").get(0).click();
						$c.remove();
						$('#report-gen').prop('disabled',false);
						$('#img_report_loader').hide();
					}else{
						var message = data.message;
						showMessage(message,'error');
						$('#report-gen').prop('disabled',false);
						$('#img_report_loader').hide();
					}
				},
				error:function(xhr, textStatus, errorThrown){
					$('#report-gen').prop('disabled',false);
					$('#img_report_loader').hide();
					showMessage(errorThrown, 'error');
				}
			});
		}else{
			$('#img_report_loader').hide();
		}
		}
	});
});
function generateTCExecDetailReport(){
	var reportType = $('#reportType').val();
	var tc = $('#execDetailTC').val();
	var execDate = $('#execDetailExOn').val();
	var userId = $('#execDetailExBy').val();
	/*Modified by Preeti for V2.8-162 starts*/
	/*var selExecDate=new Date(execDate);*/
	//var selExecDate=convertDate(execDate);
	/*Modified by Preeti for V2.8-162 ends*/
	var todaysDate=new Date();
	todaysDate.setHours(0,0,0,0);
	/* Added by Paneendra for  Tenj210-158 starts*/
	/*if(execDate != ''&& !validateDate(execDate)){
	    showMessage('Please enter Executed Date value in this format DD-MMM-YYYY.','error');
		return false;
	}else if(selExecDate>todaysDate){
		showMessage("Executed On should be less than or equal to Today's Date","error");
		return false;
	}*/
	/* Added by Paneendra for  Tenj210-158 ends*/
	return 'param=TC_EXECUTION_DETAIL_REPORT&tc=' +tc+'&execDate='+execDate+'&userId='+userId+'&reportType='+reportType
}
function generateTCExecHistoryReport(){
	var reportType = $('#reportType').val();
	var tc = $('#execDetailTC1').val();
	var userId = $('#execHistExecBy').val();
	var fromDate = $('#execHistDateFrom').val();
	var toDate = $('#execHistDateto').val();
	/*Modified by Preeti for V2.8-162 starts*/
	/*var selFromDate=new Date(fromDate);
	var selToDate=new Date(toDate);*/
	//var selFromDate=convertDate(fromDate);
	//var selToDate=convertDate(toDate);
	/*Modified by Preeti for V2.8-162 ends*/
	var todaysDate=new Date();
	todaysDate.setHours(0,0,0,0);
	/* Added by Paneendra for  Tenj210-158 starts*/
	/*if(fromDate != ''&& !validateDate(fromDate)){
	    showMessage('Please enter Date From value in this format DD-MMM-YYYY.','error');
		return false;
	}else if(toDate != ''&& !validateDate(toDate)){
		showMessage('Please enter Date To value in this format DD-MMM-YYYY.','error');
		return false;
	}else if(selFromDate>todaysDate){
		showMessage("Date From should be less than or equal to Today's Date","error");
		return false;
	}else if(selToDate>todaysDate){
		showMessage("Date To should be less than or equal to Today's Date","error");
		return false;
	}
	else if(selFromDate>selToDate){
		showMessage("Date To should be greater than or equal to Date From","error");
		return false;
	}*/
	/* Added by Paneendra for  Tenj210-158 ends*/
	return 'param=TC_EXECUTION_HISTORY_REPORT&tc='+tc+'&userId=' + userId+'&dateFrom='+fromDate+'&dateTo='+toDate+'&reportType='+reportType
}
/*Added by Preeti for V2.8-162 starts*/
function convertDate(stringdate)
{
    var DateRegex = /([^-]*)-([^-]*)-([^-]*)/;
    var DateRegexResult = stringdate.match(DateRegex);
    var DateResult;
    var StringDateResult = "";
    try
    {
        DateResult = new Date(DateRegexResult[2]+"/"+DateRegexResult[3]+"/"+DateRegexResult[1]);
    } 
    catch(err) 
    { 
        DateResult = new Date(stringdate); 
    }
    // format the date properly for viewing
    /*StringDateResult = (DateResult.getMonth()+1)+"/"+(DateResult.getDate()+1)+"/"+(DateResult.getFullYear());*/

    return DateResult;
}
/*Added by Preeti for V2.8-162 ends*/