/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  teststep_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 05-03-2018		   Padmavathi 		       Newly added for TENJINCG-612
* 22-03-2018           Padmavathi              for navigation of back button in run information page 
* 09-04-2018		   Preeti					TENJINCG-619
* 09-05-2018           Padmavathi              TENJINCG-645
* 04-05-2018		   Pushpalatha			   TENJINCG-621
* 05-06-2018           Padmavathi              removed unused variable 
* 21-09-2018           Padmavathi              TENJINCG-737
* 12-10-2018           Padmavathi              TENJINCG-847
* 24-10-2018           Padmavathi              TENJINCG-847
* 20-12-2018           Padmavathi              TENJINCG-911
* 11-03-2019           Padmavathi              TENJINCG-997
* 25-03-2019		   Pushpalatha			   TENJINCG-968
* 04-04-2019           Padmavathi              TNJN27-45
* 19-09-2019			Preeti				   TENJINCG-1068,1069
* 22-10-2019			Preeti				   TENJINCG-1114
* 05-06-2020           Priyanka                Tenj210-70 
* 19-11-2020           Priyanka                TENJINCG-1231
*/


$(document).ready(function(){
	/* Added by Priyanka for TENJINCG-1231 starts*/
	const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
	var current_datetime = new Date();
	var formatted = current_datetime.getDate() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getFullYear();
	   var projectEndDate=$('#endDate').val();   
		  var tdyDate = Date.parse(formatted);
		  var prjEndDate  = Date.parse(projectEndDate);
		  if(prjEndDate>=tdyDate){
			  
			  $('#btnSave').show();
			  $('#btnSaveAs').show();
			  $('#btnManual').show();
			  $('#btnRun').show();
			  $('#history').show();
			  $('#validation').show();
			  $('#btnBack').show();
		  }
		  else{
			  
			  $('#btnSave').hide();
			  $('#btnSaveAs').hide();
			  $('#btnManual').hide();
			  $('#btnRun').hide();
			  $('#history').hide(); 
			  $('#validation').hide();
			  $('#btnBack').show();
			  
		  }
		  /* Added by Priyanka for TENJINCG-1231 ends*/
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('teststep_form');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	/*Added by Padmavathi for TENJINCG-847 starts*/
	if($('#callback').val()=='testset'){
		$('#btnSaveAs').hide();
	}
	/*Added by Padmavathi for TENJINCG-847 ends*/
	 $('#btnUiValidations').hide();
	var mode=$('#txnMode').val();
	/*Added by Padmavathi for TENJINCG-997 starts*/
	$('#dependencyRules').children('option').each(function(){
        var dependncyRules=$('#selectedRules').val();
        var val = $(this).val();
        if(val == dependncyRules){
            $(this).attr({'selected':true});
        }
    });
	
	if($('#seq').val()=='1'){
		$('#dependencyRules').prop('disabled',true)
	}
	/*Added by Padmavathi for TENJINCG-997 ends*/
	/*Added by Pushpalatha for TENJINCG-621 starts*/
	var functionCode=$('#function').val();
	if(functionCode=='-1'){
		return false;
	}
	var appId=$('#aut').val();
	var mode=$('#txnMode').val();
	$.ajax({
		url:'TestStepServlet?t=learnCheck&functionCode=' + functionCode + '&appId=' + appId+'&mode='+mode,
		dataType:'json',
		success:function(data) {
			if(data.status.toLowerCase() === 'warn') {
				$('#btnManual').hide();
			}else{
				$('#btnManual').show();
			}
		},
	});
	/*Added by Pushpalatha for TENJINCG-621 ends*/
	 /*Added by Padmavathi for TENJINCG-911 starts*/ 
	if($('#rerunStep').val()=='Y'){
		$('#showForRerun').prop('checked',true);
	}else{
		$('#showForRerun').prop('checked',false);
	}
	 /*Added by Padmavathi for TENJINCG-911 ends*/ 
	
	/*Commented by Padmavathi for TENJINCG-997 starts*/
	/*Added by Padmavathi for TENJINCG-645 starts*/
/*	if($('#seq').val()!='1'){
		if($('#customRule').val()!=''){
   
			$('#customRules').attr('checked',true)
			$('#customRules').val('Y');
			$('#customRulesList').show();
			if($('#customRule').val()=='Pass'){
  			
				$('#customRules').attr('checked',true)
				$('#customRulePass').attr('checked',true);
			}else if($('#customRule').val()=='Fail'){
  			
				$('#customRuleFail').attr('checked',true)
			}else{
				$('#customRuleError').attr('checked',true)
			}
		}else{
			$('#customRules').val('N');
			$('#customDependencyRule').show();
			$('#customRulesList').hide();
		}
	}else{
		$('#customRules').val('N');
		$('#customDependencyRule').hide();
	}
	
    $('#customRules').click(function(){
   	 if( $('#customRules').is(':checked')){
   		$('#customRules').val('Y');
   	  	   $('#customRulesList').show();
   	     } else{
   	    	$('#customRules').val('N');
   	    	 $('#customRulesList').hide();
   	     }
    });*/
    /*Added by Padmavathi for TENJINCG-645 ends*/
	/*Commented by Padmavathi for TENJINCG-997 ends*/
	if (mode == 'API') {
		$('#learn-mode').val(mode);
		$('#module_label').text(mode);
		$('#lstModules').attr("Title",mode);
		$('#lstValType').prop('disabled', true);
		$('#btnManual').hide();
		$('#lstResponseType').show();
		}/*Added by Ashiki for TENIINCG-1275 starts*/
		else if(mode == 'GUI'){
			$('#lstResponseType').hide();
			$('#ResponseType').hide();
			/*Added by Ashiki for TENIINCG-1275 end*/
		}else{
		/*Added by Ashiki for TENIINCG-1275 starts*/
			$('#module_label').text("Format");
			$('#operationId').text("Message");
			$('.autLogintypes').hide();
			$('.txtTsType').hide();
			/*Added by Ashiki for TENIINCG-1275 ends*/
			$('#lstResponseType').hide();
			$('#ResponseType').hide();
		}
	var selApp = $('#appId').val();
	var selType = $('#type').val();
	var	selValType=	$('#valType').val();
	$('#lstValType').children('option').each(function () {
		var val = $(this).val();
		if(val == selValType) {
			$(this).attr({'selected':true});
		}
	});
	if(selValType==='UI'){
    $('#btnUiValidations').show();
	}
	if($('#lstValType').val() === 'UI') {
		$('#txtNoOfRows').val('1');
		$('#txtNoOfRows').prop('disabled',true);
	}
	$('#lstValType').change(function() {
		if($(this).val() === 'UI') {
			$('#txtNoOfRows').val('1');
			$('#txtNoOfRows').prop('disabled',true);
			/*Commented by Preeti for TENJINCG-619 starts*/
			/*$('#btnUiValidations').show();*/
			/*Commented by Preeti for TENJINCG-619 ends*/
		}else{
			$('#txtNoOfRows').val($('#selRowsToExecute').val());
			$('#txtNoOfRows').prop('disabled',false);
			/*Commented by Preeti for TENJINCG-619 starts*/
			/* $('#btnUiValidations').hide();*/
			/*Commented by Preeti for TENJINCG-619 ends*/
		}
	});
	
	$('#aut').change(function(){
		clearMessages();
		var val = $(this).val();
		
		if(val=='-1'){
			$("#api option").remove();
			$("#function option").remove();
			$("#operation option").remove();
			$("#autLogintypes option").remove();
			$("#apiOperation option").remove();
			$("#lstResponseType option").remove();
			return false;
		}
		$("#operation option").remove();
		$("#apiOperation option").remove();
		$("#lstResponseType option").remove();
		
	});
	$('#function').change(function(){
		var functionCode=$(this).val();
		if(functionCode=='-1'){
			return false;
		}
		var appId=$('#aut').val();
		var mode=$('#txnMode').val();
		$.ajax({
			url:'TestStepServlet?t=learnCheck&functionCode=' + functionCode + '&appId=' + appId+'&mode='+mode,
			dataType:'json',
			success:function(data) {
				if(data.status.toLowerCase() === 'warn') {
					alert(data.message);
				}
			},
		});
	});
	$('#api').change(function(){
		var apiCode = $(this).val();
		$('#apiCode').val(apiCode);
		if(apiCode=='-1'){
			$("#apiOperation option").remove();
			$("#lstResponseType option").remove();
			return false;
		}else{
			$("#apiOperation option").remove();
			$("#lstResponseType option").remove();
			var appId=$('#aut').val();
			var mode=$('#txnMode').val();
			$.ajax({
				url:'TestStepServlet?t=learnCheck&functionCode=' + apiCode + '&appId=' + appId+'&mode='+mode,
				dataType:'json',
				success:function(data) {
					if(data.status.toLowerCase() === 'warn') {
						alert(data.message);
					}
				},
			});
		}
		
	});
	$('#apiOperation').change(function(){
		var apiOperation = $(this).val();
		if(apiOperation=='-1'){
			$("#lstResponseType option").remove();
			return false;
		}
		
	});
	$('#btnSave').click(function(){
		var formVal = validateForm('teststep_form');
		if(formVal != 'SUCCESS'){
			showMessage(formVal,'error');
			return false;
		}
		 /*Added by Padmavathi for TENJINCG-911 starts*/ 
		if ( $('#showForRerun').is(':checked') ) {
	    	 $("#showForRerun").val('Y');
	    } else {
	    	$("#showForRerun").val('N');
	    }
		 /*Added by Padmavathi for TENJINCG-911 ends*/ 
		var inputVal = $('#txtNoOfRows').val();
		var regex = new RegExp(/[^0-9]/g);
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			showMessage('Please enter valid numer of rows to execute.');
			$(this).val('');
			return false;
		}/*commented by Padmavathi for TNJN27-45 starts */
		/*else{
			if(inputVal==0){
				showMessage('Please enter valid numer of rows to execute.');
				return false;
			}
		}*/
		/*commented by Padmavathi for TNJN27-45 ends */
	});
	
	$('#lstTestType').children('option').each(function(){
		var val = $(this).val();
		if(val == selType){
			$(this).attr({'selected':true});
		}
	});
	/*Commented by Preeti for TENJINCG-1114 starts*/
	/*$('#txtStepId').change(function(){
		$('#txtDataId').val($(this).val());
	});*/
	/*Commented by Preeti for TENJINCG-1114 ends*/
	$('#btnManual').click(function(){
		window.location.href='TestStepServlet?t=manualMapping&tcRecId=' + $('#txtRecordId').val();
	});
	$('#btnBack').click(function(){
		var tcRecId = $('#txnTcRecTd').val();
		/*Modified by Padmavathi for TENJINCG-737 starts*/
		if($('#callback').val()=='testset'){
			window.location.href='TestSetServletNew?t=testset_view&paramval='+$("#setId").val();
		}else {
			window.location.href = 'TestCaseServletNew?t=testCase_View&paramval=' + $('#txnTcRecTd').val();
		}
		/*Modified by Padmavathi for TENJINCG-737 ends*/
	});
	$('#btnRun').click(function(){
		clearMessages();
			window.location.href='TestSetServlet?entity_name=teststep&param=initiate_adhoc_run_ts&ts=' + $('#txtRecordId').val()+'&callback=teststep';
		
	});
	/*Added by Padmavathi for TENJINCG-847 starts*/
	$('#btnSaveAs').click(function(){
		clearMessages();
		window.location.href='TestStepServlet?t=copyTestStep&stepRecId='+$('#txtRecordId').val()+'&prjId='+$('#prjId').val();
	})
	/*Added by Padmavathi for TENJINCG-847 ends*/
	/*Added by Preeti for TENJINCG-1068,1069 starts*/
	$('#history').click(function(){
		clearMessagesOnElement($('#user-message'));
		var activityArea = 'Project';
		var recId = $('#txtRecordId').val();
		var id = $('#txtId').val();
		var entityType ='teststep';
		$('#history-sframe').attr('src','TestReportServlet?param=CHANGE_HISTORY&activityArea='+activityArea+'&recId='+recId+'&id='+id+'&entityType='+entityType);
		$('.modalmask').show();
		$('.subframe').show();
	});
	/*Added by Preeti for TENJINCG-1068,1069 ends*/
	
	$('#validation').click(function() {
		var callback = $('#h-callback').val();
		var recId = $('#txtRecordId').val()
		window.location.href='UnstructuredDataServlet?t=Unstructured_Data_List&stepRecId='+recId;
	});
	/*Added by Priyanka for Tenj210-70 starts*/
	$('#lstTestType').children('option').each(function(){
        var tsType=$('#txtTsType').val();
        var val = $(this).val();
        if(val == tsType){
            $(this).attr({'selected':true});
        }
 });
	/*Added by Priyanka for Tenj210-70 ends*/
});
$(document).on('click', '#btnUiValidations', function() {
	var tsRecId = $('#txtRecordId').val();
	window.location.href='UIValidationServlet?param=tstep_ui_vals_list&srecid=' + tsRecId;
});
function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

   return true;
}
/*Added by Preeti for TENJINCG-1068,1069 starts*/
function closeModal(){
	$('.subframe > iframe').attr('src','');
	$('.subframe').hide();
	$('.modalmask').hide();
	
}
/*Added by Preeti for TENJINCG-1068,1069 ends*/