/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  assistedlearning_new.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 26-09-2018			Preeti					TENJINCG-742
* 27-09-2018			Preeti					TENJINCG-816	
* 04-10-2018			Preeti					TENJINCG-823	
* 07-11-2018            Leelaprasad             TENJINCG-900
* 19-12-2018            Leelaprasad             TJNUN262-31
* 08-02-2018            Leelaprasad             TENJINCG-940
* 01-03-2019			Preeti					TENJINCG-998
* 05-06-2020			Ruksar					Tenj210-17
*/


$(document).ready(function(){
	/*Added by Preeti for TENJINCG-998 starts*/
	$('#assistedLearningTable').dataTable({
		paging: false,
		sort : false
	});
	$('.dataTables_empty').remove();
	/*Added by Preeti for TENJINCG-998 ends*/
	$('#imgProgress').hide();
	/*Added by Preeti for TENJINCG-823 starts*/
	$('#imgProgress1').hide();
	$('#uploadBlock').hide();
	$('#btnSave').show();
	/*Added by Preeti for TENJINCG-823 ends*/
	var seq = $('#sequenceAdd').val();
	var flag = $('#flag').val();
	if(seq==0){
		/*Modified by Preeti for TENJINCG-998 starts*/
		 /*var tbody = $("#tblNaviflowModules_body");*/
		 var table = $('#assistedLearningTable').DataTable();
		 for(var i=seq;i<5;i++){
			 seq=++seq;
			 $('#sequenceAdd').val(seq);
			 table.row.add(["<input type='checkbox'/>",
							"<span class='sequence' id='txtSeq_"+seq+"'>"+seq+"</span>",
							"<input type='text' class='page' id='txtPage_"+seq+"'/>",
							"<input type='text' class='field' id='txtField_"+seq+"'/>",
						 	"<input type='text' class='identified' id='txtIdentifiedBy_"+seq+"'/>",
							"<input type='text' class='data' id='txtData_"+seq+"'/>"]).draw();
			
			/* $("\
						<tr><td class='table-row-selector'><input type='checkbox'/></td>\
						<td class='sequence' id='txtSeq_"+seq+"'>"+seq+"</td>\
						<td><input type='text' class='page' id='txtPage_"+seq+"'/></td>\
						<td><input type='text' class='field' id='txtField_"+seq+"'/></td>\
					 	<td><input type='text' class='identified' id='txtIdentifiedBy_"+seq+"'/></td>\
						<td><input type='text' class='data' id='txtData_"+seq+"'/></td></tr>").appendTo(tbody);*/
						/*<td align='center'><input type='button' class='imagebutton delete' id='btnDelete'/></td></tr>").appendTo(tbody);*/	
		 /*Modified by Preeti for TENJINCG-998 ends*/
		 }
	}
	$('#btnBack').click(function(){
		var appId = $('#appId').val();
		var funcCode = $('#func').val();
		window.location.href='FunctionServlet?t=view&key='+ funcCode +'&appId=' + appId ;
	});
	$('#btnRefresh').click(function(){
		var appId = $('#appId').val();
		var funcCode = $('#func').val();
		window.location.href='AssistedLearningDataServlet?param=init&app=' + appId + '&func=' + funcCode;
	});
	/*Commented by Preeti for TENJINCG-998 starts*/
	/*Changed by leelaprasad for TENJINCG-940 starts*/
	/*var i=0;
	Changed by leelaprasad for TENJINCG-940 ends
	$('table').on('click', 'input[type="button"]', function(e){
		Changed by leelaprasad for TENJINCG-940 starts
		if(i==0){
			i++;
	 alert( "This operation will just delete the record from UI,you should click on save to complete the operation" ); 
		}
		Changed by leelaprasad for TENJINCG-940 ends
		   $(this).closest('tr').remove();
		   reOrderSteps();
		});*/
	/*Commented by Preeti for TENJINCG-998 ends*/
	$('#btnSave').click(function(){
		$('#imgProgress').show();
		var values = new Array();
		var funcCode = $('#func').val();
		var appId = $('#appId').val();
		var count=0; count1=0;
		var csrftoken_form=$("#csrftoken_form").val();
		var table = document.getElementById("assistedLearningTable");
		for (var i = 1, row; row = table.rows[i]; i++) {
			var json = new Object();
			json.appId = $('#appId').val();
			json.functionCode = $('#txtFunc').val();
			json.sequence = $('#txtSeq_'+i).text();
			json.pageAreaName = $('#txtPage_'+i).val().trim();
			json.fieldName = $('#txtField_'+i).val().trim();
			json.identifiedBy = $('#txtIdentifiedBy_'+i).val().trim();
			json.data = $('#txtData_'+i).val().trim();
			if(!(json.pageAreaName == '' && json.fieldName == '' && json.identifiedBy == '' && json.data == '')){	
				values.push(json);
				if(count>0){
					count1++;
				}
			}
			else{
				count++;
			}
			if(count1>0){
				showMessage("You cannot have blank row(s) in between. Please remove the blank row(s) and try again.", 'error');
				$('#imgProgress').hide();
				return false;
			}
		}
		var jsonString = JSON.stringify(values);
		/*Added by Preeti for TENJINCG-816 starts*/
		if(values.length==0 && flag==0){
			showMessage("No assisted learning data available to save.", 'error');
			$('#imgProgress').hide();
			return false;
			
		}
		/*Added by Preeti for TENJINCG-816 ends*/
		if(values.length==0 && flag>0){
			if(!confirm('Are you sure you want to continue, it will delete all record(s)? Please note that this action cannot be undone.')) {
				$('#imgProgress').hide();
				var appId = $('#appId').val();
				var funcCode = $('#func').val();
				window.location.href='AssistedLearningDataServlet?param=init&app=' + appId + '&func=' + funcCode;
				return false;
			}
		}
		jsonString = encodeURIComponent(jsonString);
		$.ajax({
			url:'AssistedLearningAjaxServlet',
			data:'param=PERSIST_DATA&json=' + jsonString+'&funcCode='+funcCode+'&appId='+appId+'&csrftoken_form='+csrftoken_form,
			async:false,
			type: 'POST',
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					flag=values.length;
					$('#imgProgress').hide();
					showMessage(data.message,'success');
				}else{
					$('#imgProgress').hide();
					showMessage(data.message,'error');
				}
			},error:function(xhr, textStatus, errorThrown){
				$('#imgProgress').hide();
				showMessage(errorThrown,'error');
			}
		});
		
	});
	 $('#btnNew').click(function(){
		 var tbody = $("#tblNaviflowModules_body");
		 var seq = $('#sequenceAdd').val();
		 seq=++seq;
		 $('#sequenceAdd').val(seq);
		 /*Modified by Preeti for TENJINCG-998 starts*/
		/* var table = $('#assistedLearningTable').DataTable();
			 table.row.add(["<input type='checkbox'/>",
							"<span class='sequence' id='txtSeq_"+seq+"'>"+seq+"</span>",
							"<input type='text' class='page' id='txtPage_"+seq+"'/>",
							"<input type='text' class='field' id='txtField_"+seq+"'/>",
						 	"<input type='text' class='identified' id='txtIdentifiedBy_"+seq+"'/>",
							"<input type='text' class='data' id='txtData_"+seq+"'/>"]).draw();*/
		 $("\
					<tr><td><input type='checkbox'/></td>\
					<td><span class='sequence' id='txtSeq_"+seq+"'>"+seq+"</span></td>\
					<td><input type='text' class='page' id='txtPage_"+seq+"'/></td>\
					<td><input type='text' class='field' id='txtField_"+seq+"'/></td>\
				 	<td><input type='text' class='identified' id='txtIdentifiedBy_"+seq+"'/></td>\
					<td><input type='text' class='data' id='txtData_"+seq+"'/></td></tr>").appendTo(tbody);
					/*<td align='center'><input type='button' class='imagebutton delete' id='btnDelete'/></td></tr>").appendTo(tbody);*/	 
					/*Modified by Preeti for TENJINCG-998 ends*/
				reOrderSteps();
	 });
	 
	 $('.sortable-table tbody').sortable({
			stop: function( event, ui ) {
				reOrderSteps();
			}
	 });
	 /*Added by Preeti for TENJINCG-998 starts*/
	 $(document).on("click", "#btnDelete", function() {
			var count =0;
			clearMessages();
			if($('#assistedLearningTable').find('input[type="checkbox"]:checked').length < 1) {
				showLocalizedMessage('no.records.selected', 'error');
				return false;
			} 
			$('#assistedLearningTable').find('input[type="checkbox"]:checked').each(function() {
				if(!$(this).hasClass('tbl-select-all-rows')) {
					$(this).closest('tr').find("input[type='text']").each(function() {
						if($(this).attr("class") == "page")
				        if($(this).val().trim()== "") {
							showMessage('No data in selected row(s) to remove.','error');
							$('#imgProgress').hide();
							count++;
							return false;
						}
						/*Added by Ruksar for Tenj210-17 starts*/
						if($(this).find('input[type="checkbox"]:checked') ) {
							showMessage('Data in selected row(s) has been removed.','success');
							$('#imgProgress').hide();
							return false;
						}
						/*Added by Ruksar for Tenj210-17 ends*/
				    });
				}
			});
			if(count==0)
				if(confirm('This operation will delete selected row(s) from UI, you should click on save to complete the operation.')) {
					$('#assistedLearningTable').find('input[type="checkbox"]:checked').each(function() {
						if(!$(this).hasClass('tbl-select-all-rows')) {
							$(this).closest('tr').remove();	
						}
					});
			reOrderSteps();
		}
	});
	/*Added by Preeti for TENJINCG-998 ends*/ 
	/*Changed by Leelaprasad for TJNUN262-31 starts*/ 
	 /*$('#btnDeleteAll').click(function(){
		 
		$('#assistedLearningTable tbody tr').each(function(){
			$(this).remove();
		});
	    alert("This operation will delete all record(s) from UI, you should click on save to complete the operation");
		if(!confirmResult) {
			var appId = $('#appId').val();
			var funcCode = $('#func').val();
			window.location.href='AssistedLearningDataServlet?param=init&app=' + appId + '&func=' + funcCode;
		}
		 var funcCode = $('#func').val();
			var appId = $('#appId').val();
			Changed by leelaprasad for TENJINCG-940 starts
			var confirmResult = confirm("Are you sure you want to continue, it will delete all record(s) from UI? you should click on save to complete the operation");
			if(confirmResult) {
				Changed by leelaprasad for TENJINCG-940 ends
			window.location.href='AssistedLearningDataServlet?param=delete_all&funcCode='+funcCode+'&appId='+appId;
			Changed by leelaprasad for TENJINCG-940 starts
			}else{
				return false;
			}
			Changed by leelaprasad for TENJINCG-940 ends
		});*/
	 /*Changed by Leelaprasad for TJNUN262-31 ends*/
	/*Added by Preeti for TENJINCG-823 starts*/	 
	 $('#btnUpload').click(function(){
				clearMessagesOnElement($('#user-message'));
				$('#uploadBlock').show();
			});
	/*Added by Preeti for TENJINCG-823 ends*/
	 /*Changed by Leelaprasad for TENJINCG-900 starts*/
	 $('#btnDownload').click(function(){
			/*Added By padmavathi for T25IT-326 starts*/
			clearMessagesOnElement($('#user-message'));
			/*Added By padmavathi for T25IT-326 ends*/
			var appId = $('#appId').val();
			var funcCode = $('#txtFunc').val();
			
			$.ajax({
				url:'AssistedLearningDataServlet',
				data:'param=export&app=' + appId + '&func=' + funcCode,
				dataType:'json',
				async:true,
				success:function(data){
					
					var status =data.status;
					if(status =='SUCCESS'){
						/*modified by paneendra for VAPT fix starts*/
						/*var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");*/
						var $c = $("<a id='downloadFile' href='DownloadServlet?param="+data.path+"'target='_blank' download />");
						/*modified by paneendra for VAPT fix ends*/
						$("body").append($c);
		                $("#downloadFile").get(0).click();
		                $c.remove();
					}else{
						var message = data.message;
						showMessage(message,'error');
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage('A network error occurred. Please try again','error');
				}
			});
		});
	 /*Changed by Leelaprasad for TENJINCG-900 ends*/
});
function reOrderSteps() {
	var jsonArray = [];
	var seq1 = 0;
	
	$('#assistedLearningTable tbody').find('tr').each(function (){
		seq1++;
		var json = new Object();
		json.sequence = seq1;
		
		jsonArray.push(json);
		
		jsonArray.push(json);
		/*Added by Preeti for TENJINCG-998 starts*/
		if(seq1%2==0){
			$(this).removeClass();
			$(this).addClass('even');
		}
		else{
			$(this).removeClass();
			$(this).addClass('odd');
		}
		/*Added by Preeti for TENJINCG-998 ends*/
		$(this).find('.sequence').html(seq1);
		$(this).find('.sequence').attr('id','txtSeq_'+seq1);
		$(this).find('.page').attr('id','txtPage_'+seq1);
		$(this).find('.field').attr('id','txtField_'+seq1);
		$(this).find('.identified').attr('id','txtIdentifiedBy_'+seq1);
		$(this).find('.data').attr('id','txtData_'+seq1);
	});
}
/*Added by Preeti for TENJINCG-823 starts*/
function validate()
{

   var allowedFiles = [".xls", ".xlsx"];
   var fileUpload = document.getElementById("txtFile");
   var filename=fileUpload.value.toLowerCase();
   
   var lblError = document.getElementById("lblError");
   var regex = new RegExp(allowedFiles.join('|'));
   if (!regex.test(filename)) {
       lblError.innerHTML = "Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.";
       return false;
   }
   var flag = $('#flag').val();
   if(flag>0){
   if(!confirm('Are you sure you want to continue, it will override existing record(s)? Please note that this action cannot be undone.')) {
		var appId = $('#appId').val();
		var funcCode = $('#func').val();
		window.location.href='AssistedLearningDataServlet?param=init&app=' + appId + '&func=' + funcCode;
		return false;
	}
   }
   $('#imgProgress1').show();
   $('#btnSave').hide();
   lblError.innerHTML = "";
   return true;
   
   return( true );
}
/*Added by Preeti for TENJINCG-823 ends*/