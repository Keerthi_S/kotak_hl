/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  project_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 24-05-2018		   Pushpalatha G		   Newly Added 
* 18-06-2019			Preeti					V2.8-150
* 20-06-2019			Preeti					V2.8-162
* 19-08-2019			Preeti					TENJINCG-1065,1072
* 21-08-2019			Preeti					TENJINCG-1066,1071
* 07-06-2020			Ashiki					Tenj210-68
*/

$(document).ready(function() {
	mandatoryFields('main_form');
	$('#application_wise_report').hide();
	/*Added by Preeti for TENJINCG-1065,1072 starts*/
	$('#defect_wise_report').hide();
	/*Added by Preeti for TENJINCG-1065,1072 ends*/
	$('#scheduler_report').hide();
	$('#img_report_loader').hide();
	
	$('#runDateFrom').datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		dateFormat:'dd-M-yy' ,
		maxDate : new Date()
	});
	$('#runDateTo').datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		dateFormat:'dd-M-yy' ,
		maxDate : new Date()
	});
	/*Added by Preeti for TENJINCG-1066,1071 starts*/
	$('#schDateFrom').datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		dateFormat:'dd-M-yy' ,
		maxDate : new Date()
	});
	$('#schDateTo').datepicker({
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		dateFormat:'dd-M-yy' ,
		/*Commeneted by Ashiki for Tenj210-68 starts*/
		/*maxDate : new Date()*/
		/*Commeneted by Ashiki for Tenj210-68 end*/
	});
	/*Added by Preeti for TENJINCG-1066,1071 ends*/
	$('#appRunReport').change(function(){
		var reportFor = $('#appRunReport').val();
		if(reportFor=='-1'){
			$('#application_wise_report').hide();
			$('#defect_wise_report').hide();
			$('#scheduler_report').hide();
		}else
		if(reportFor=='1'){
			$('#application_wise_report').show();
			/*Added by Preeti for TENJINCG-1065,1072 starts*/
			$('#defect_wise_report').hide();
			$('#scheduler_report').hide();
		}else
			if(reportFor=='2'){
				$('#defect_wise_report').show();
				$('#application_wise_report').hide();
				$('#scheduler_report').hide();
				getTestSet();
			}
			/*Added by Preeti for TENJINCG-1065,1072 ends*/
			/*Added by Preeti for TENJINCG-1066,1071 starts*/
			else if(reportFor=='3'){
				$('#scheduler_report').show();
				$('#defect_wise_report').hide();
				$('#application_wise_report').hide();
				getUsers();
			}
			/*Added by Preeti for TENJINCG-1066,1071 ends*/
	})
	$('#btnGenerate').click(function(){
		clearMessages();
		var reportFor = $('#appRunReport').val();
		var param = '';
		/*Added by Ruksar for TENJINCG-1146 starts*/
		if(reportFor==''|| reportFor==-1){
            showMessage("Please select atleast one report to continue", 'error');
            return false;
        }
		/*Added by Ruksar for TENJINCG-1146 ends*/
		if(reportFor){
			$('#img_report_loader').show();
			if(reportFor=='1'){
				
				param = generateAppRunReport();
			}
			/*Added by Preeti for TENJINCG-1065,1072 starts*/
			else if(reportFor=='2'){
				param = generateDefectReport();
			/*Added by Preeti for TENJINCG-1065,1072 ends*/
			}
			/*Added by Preeti for TENJINCG-1066,1071 starts*/
			else if(reportFor == '3'){
				param = generateScheduerReport();
			}
			/*Added by Preeti for TENJINCG-1066,1071 ends*/
			if(param){
			$.ajax({
				url:'TestReportServlet',
				data:param,
				async:true,
				dataType:'json',
				success:function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						/*var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");*/
						var $c = $("<a id='downloadFile'  href='DownloadServlet?param="+data.path+"' target='_blank' download />");
						$("body").append($c);
						$("#downloadFile").get(0).click();
						$c.remove();
						$('#report-gen').prop('disabled',false);
						$('#img_report_loader').hide();
					}else{
						var message = data.message;
						showMessage(message,'error');
						$('#report-gen').prop('disabled',false);
						$('#img_report_loader').hide();
					}
				},
				error:function(xhr, textStatus, errorThrown){
					$('#report-gen').prop('disabled',false);
					showMessage(errorThrown, 'error');
					$('#img_report_loader').hide();
				}
			});
		}
			else{
				$('#img_report_loader').hide();
			}
		}
	});
	/*Added by Preeti for TENJINCG-1065,1072 starts*/
	$('#defectTS').change(function(){
		clearMessages();
		var ts = $('#defectTS').val();
		var options='';
		$('#defRunFrom').empty();
		$('#defRunTo').empty();
		if(ts){
			$.ajax({
				url:'TestReportServlet',
				data :'param=get_runs&ts=' + ts,
				async:false,
				dataType:'json',
				success: function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						var dArray = data.runIds;
						for(var i=0;i<dArray.length;i++){
							var runId = dArray[i];
							options += '<option value="' + runId.runId + '">' + runId.runId + '</option>';
						} 
						$('#defRunFrom').append(options);
						$('#defRunTo').append(options);
					}else{
							showMessage(data.message, 'error');
							options += '<option value="">--Select One--</option>';
							$('#defRunFrom').append(options);
							$('#defRunTo').append(options);
						
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(textStatus, 'error');			
				}
			});
		}else{
			options += '<option value="">--Select One--</option>';
			$('#runHistRunFrom').append(options);
			$('#runHistRunTo').append(options);
		}
	});
});

function getTestSet(){
	clearMessages();
	var options='<option value="">--Select One--</option>';
	$('#defectTS').empty();
		$.ajax({
			url:'TestReportServlet',
			data :'param=get_test_sets',
			async:false,
			dataType:'json',
			success: function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					var dArray = data.testsets;
					for(var i=0;i<dArray.length;i++){
						var ts = dArray[i];
						options += '<option value="' + ts.id + ':' + ts.name + '">' + ts.name + '</option>';
					} 
					$('#defectTS').append(options);
				}else{
						showMessage(data.message, 'error');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(textStatus, 'error');			
			}
		});
}
/*Added by Preeti for TENJINCG-1065,1072 ends*/
/*Added by Preeti for TENJINCG-1066,1071 starts*/
function getUsers(){
	clearMessages();
	var options='<option value="All">All</option>';
	$('#schUser').empty();
		$.ajax({
			url:'TestReportServlet',
			data :'param=get_users',
			async:false,
			dataType:'json',
			success: function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					var dArray = data.users;
					for(var i=0;i<dArray.length;i++){
						var user = dArray[i];
						options += '<option value="' + user.id + '">' + user.id + '</option>';
					} 
					$('#schUser').append(options);
				}else{
						showMessage(data.message, 'error');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(textStatus, 'error');			
			}
		});
}
/*Added by Preeti for TENJINCG-1066,1071 ends*/
function generateAppRunReport(){
	var reportType = $('#reportType').val();
	var fromDate = $('#runDateFrom').val();
	var toDate = $('#runDateTo').val();
	var appId=$('#aut').val();
	/*Modified by Preeti for V2.8-162 starts*/
	/*var selFromDate=new Date(fromDate);
	var selToDate=new Date(toDate);*/
	var selFromDate=convertDate(fromDate);
	var selToDate=convertDate(toDate);
	/*Modified by Preeti for V2.8-162 ends*/
	var todaysDate=new Date();
	todaysDate.setHours(0,0,0,0);
	if(appId=='-1'){
		 showMessage('Please select application for report.','error');
		return false;
	}
	if(fromDate != ''&& !validateDate(fromDate)){
	    showMessage('Please enter Date From value in this format DD-MMM-YYYY.','error');
		return false;
	}else if(toDate != ''&& !validateDate(toDate)){
		showMessage('Please enter Date To value in this format DD-MMM-YYYY.','error');
		return false;
	}else if(selFromDate>todaysDate){
		showMessage("Date From should be less than or equal to Today's Date","error");
		return false;
	}else if(selToDate>todaysDate){
		showMessage("Date To should be less than or equal to Today's Date","error");
		return false;
	}
	else if(selFromDate>selToDate){
		showMessage("Date To should be greater than or equal to Date From","error");
		return false;
	}
	return 'param=APPLICATIONWISE_EXECUTION_REPORT&dateFrom='+fromDate+'&dateTo='+toDate+'&reportType='+reportType+'&appId='+appId
}
/*Added by Preeti for TENJINCG-1065,1072 starts*/
function generateDefectReport(){
	var reportType = $('#reportType').val();
	var ts = $('#defectTS').val();
	var runFrom = $('#defRunFrom').val();
	var runTo = $('#defRunTo').val();
	if(ts==''){
		showMessage('Test Set is mandatory.','error');
		return false;
	}
	return 'param=SEVERITY_WISE_DEFECT_REPORT&ts='+ts+'&runFrom='+runFrom+'&runTo='+runTo+'&reportType='+reportType;
} 
/*Added by Preeti for TENJINCG-1065,1072 ends*/
/*Added by Preeti for TENJINCG-1066,1071 starts*/
function generateScheduerReport(){
	var reportType = $('#reportType').val();
	var taskType = $('#taskType').val();
	var taskStatus = $('#taskStatus').val();
	var fromDate = $('#schDateFrom').val();
	var toDate = $('#schDateTo').val();
	var userId = $('#schUser').val();
	var schType = $('#schType').val();
	
	var selFromDate=convertDate(fromDate);
	var selToDate=convertDate(toDate);
	var todaysDate=new Date();
	todaysDate.setHours(0,0,0,0);
	
	if(fromDate != ''&& !validateDate(fromDate)){
	    showMessage('Please enter Date From value in this format DD-MMM-YYYY.','error');
		return false;
	}else if(toDate != ''&& !validateDate(toDate)){
		showMessage('Please enter Date To value in this format DD-MMM-YYYY.','error');
		return false;
	}/*Commeneted by Ashiki for Tenj210-68 starts*/
	/*else if(selFromDate>todaysDate){
		showMessage("Date From should be less than or equal to Today's Date","error");
		return false;
	}else if(selToDate>todaysDate){
		showMessage("Date To should be less than or equal to Today's Date","error");
		return false;
	}
	else if(selFromDate>selToDate){
		showMessage("Date To should be greater than or equal to Date From","error");
		return false;
	}*/
	/*Commeneted by Ashiki for Tenj210-68 end*/
	return 'param=SCHEDULER_REPORT&taskType='+taskType+'&taskStatus='+taskStatus+'&dateFrom='+fromDate+'&dateTo='+toDate+'&userId='+userId+'&schType='+schType+'&reportType='+reportType;
}
/*Added by Preeti for TENJINCG-1066,1071 ends*/
/*Added by Preeti for V2.8-162 starts*/
function convertDate(stringdate)
{
    var DateRegex = /([^-]*)-([^-]*)-([^-]*)/;
    var DateRegexResult = stringdate.match(DateRegex);
    var DateResult;
    var StringDateResult = "";
    try
    {
        DateResult = new Date(DateRegexResult[2]+"/"+DateRegexResult[3]+"/"+DateRegexResult[1]);
    } 
    catch(err) 
    { 
        DateResult = new Date(stringdate); 
    }
    // format the date properly for viewing
    /*StringDateResult = (DateResult.getMonth()+1)+"/"+(DateResult.getDate()+1)+"/"+(DateResult.getFullYear());*/

    return DateResult;
}
/*Added by Preeti for V2.8-162 ends*/