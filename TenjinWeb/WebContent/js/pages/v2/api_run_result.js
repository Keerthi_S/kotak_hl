/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  api_run_result.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 16-10-2018			Sriram					File optimized for TENJINCG-876
 30-10-2018			Sriram					TENJINCG-886
 18-02-2019         Padmavathi              TENJINCG-973  
 19-02-2019         Padmavathi              TENJINCG-924
*/



function initialize() {
	$('#img_report_loader').hide();
}

function paintChart() {
	var passedTests = $('#passedTests').val();
	var failedTests = $('#failedTests').val();
	var erroredTests = $('#erroredTests').val();
	var notExecutedTests = $('#notExecutedTests').val();
	var data = [['Passed', Number(passedTests)], ['Failed',Number(failedTests)],['Error',Number(erroredTests)],['Not Executed', Number(notExecutedTests)]];
	var plot1 = jQuery.jqplot ('run-summary-chart', [data],{
		seriesDefaults: {renderer: jQuery.jqplot.PieRenderer,
			rendererOptions:{padding: 5, sliceMargin: 2, showDataLabels: false }
		},
		seriesColors: [ "#27ae60", "#c0392b",  "#f39c12", "#bdc3c7" ],
		height:160,
		width:100
	});
}

function closeModal(){
	$('.subframe').hide();
	$('.modalmask').hide();
	$('#val-res-sframe').attr('src','');
}



$(document).ready(function() {
	initialize();
	paintChart();
});

$(document).on('click', '#btnClose', function() {
	var callback = $('#h-callback').val();
	if(!callback || callback === 'tcehistory') {
		callback = "tsehistory";
	}
	if(callback === 'tsehistory') {
		var testSetRecId = $('#h-testSetRecordId').val();
		window.location.href = 'TestSetServletNew?t=testset_execution_history&recid=' + testSetRecId;
	}
	else if(callback === 'runlist') {
		window.location.href='TestRunServlet'; //TENJINCG-886 (Sriram)
	}
	else if(callback=='scheduler'){
		window.location.href="SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id="+$('#h-projectId').val();
	}/* Added by Padmavathi for TENJINCG-924 starts */
	else if(callback === 'project_dashboard') {
		window.location.href='ProjectDashBoardServlet?t=project_dashboard&paramval=' + $('#h-projectId').val();
	}
	/* Added by Padmavathi for TENJINCG-924 ends */
	
	/*Added by Padmavathi for TENJINCG-973 starts*/
	else if(callback === 'testsetlist') {
		window.location.href="TestSetServletNew?t=list"
	}
	/*Added by Padmavathi for TENJINCG-973 ends*/
});

$(document).on('click', '#btnMailLog', function() {
	clearMessagesOnElement($('#user-message'));
	var runId = $('#h-runId').val();
	var callback = $('#h-callback').val();
	window.location.href='ResultsServlet?param=mailLog&runId=' + runId+'&callback='+callback;
});

$(document).on('click', '.vValidation', function() {
	var runId = $('#h-runId').val();
	var stepRecordId = $(this).data('stepRecId');
	var iteration = $(this).data('iteration');
	var tduid = $(this).data('tduid');
	var stepId = $(this).data('stepId');
	var testCaseId = $(this).data('testCaseId');
	$('#val-res-frame').attr('src','ResultsServlet?param=validation_results&run=' + runId + '&tsrecid=' + stepRecordId + '&txn=' + iteration + '&tcid=' + testCaseId + '&tduid=' + tduid + '&stepid=' + stepId);
	$('.modalmask').show();
	$('#val_frame').show();
});

$(document).on('click', '.vRuntimeValues', function() {
	var runId = $('#h-runId').val();
	var stepRecordId = $(this).data('stepRecId');
	var iteration = $(this).data('iteration');
	var tduid = $(this).data('tduid');
	var stepId = $(this).data('stepId');
	var testCaseId = $(this).data('testCaseId');
	
	$('#val-res-frame').attr('src','ResultsServlet?param=run_time_values&run=' + runId + '&tsrecid=' + stepRecordId + '&tduid=' + tduid + '&tcid=' + testCaseId + '&stepid=' + stepId);
	$('.modalmask').show();
	$('#val_frame').show();
});

$(document).on('click', '.modalmask', function() {
	closeModal();
});

$(document).on('click', '#btnDefect', function(){
	var runId = $('#h-runId').val();
	var callback = $('#h-callback').val();
	window.location.href = 'ResultsServlet?param=defects&runid=' + runId + '&callback=' + callback;
});


$(document).on('click', '#report-gen', function() {
	var runId = $('#h-runId').val();
	
	$.ajax({
		url:'ResultsServlet',
		data:'param=TEST_REPORT_GEN&runid=' + runId,
		async:false,
		dataType:'json',
		success:function(data){
			var status = data.status;
			if(status == 'SUCCESS'){
				var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
				$("body").append($c);
                $("#downloadFile").get(0).click();
                $c.remove();
			}else{
				var message = data.message;
				showMessage(message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown, 'error');
		}
	});
});

$(document).on('click', '#btnRun', function() {
	var runId = $('#h-runId').val();
	var testSetRecordId = $('#h-testSetRecordId').val();
	/*Modified by Padmavathi for TENJINCG-653 starts*/
	var callback = $('#callback').val();
	window.location.href='PartialExecutorServlet?param=Re-RunTest&run=' + runId + '&ts=' + testSetRecordId + '&callback='+callback;
	/*Modified by Padmavathi for TENJINCG-653 ends*/
	
});

$(document).on('click', '#downloadMessages', function() {
	var runId = $('#h-runId').val();
	$('#img_report_loader').show();
	$.ajax({
		url:'ResultsServlet?param=api_messages&run=' + runId,
		dataType:'json',
		success:function(result) {
			if(result && result.status && result.status === 'success') {
				var $c = $("<a id='downloadFile' href='" + result.path + "' target='_blank' download />");
				$("body").append($c);
				$('#downloadFile').get(0).click();
				$c.remove();
			}else if(result && result.message) {
				showMessage(result.message, 'error');
			}else{
				showMessage("An internal error occurred. Please contact Tenjin Support.", 'error');
			}
			$('#img_report_loader').hide();
		}, error:function(xhr, textStatus, errorThrown) {
			showMessage("An internal error oc	curred. Please contact Tenjin Support.", 'error');
			$('#img_report_loader').hide();
		}
	});
});

$(document).on('click', '#btnSendMail', function() {
	/*clearMessagesOnElement($('#user-message-users'))
	UncheckAll();
	if($('#prjNotification').val()=='N'){
		showMessage('E-mail notification for this project is disabled, so mail cannot be sent at the moment. Please change the settings and try again.','error');
	}else{
		$('.modalmask').show();
		$('#screenShotOption').hide();
		$('#sendMailManual').show();
		$('#unmapped-users-table').dataTable();
	}*/
	var runId = $('#h-runId').val();
	$('#val-res-frame').attr('src','ResultsServlet?param=send_mail&run=' + runId);
	$('.modalmask').show();
	$('#val_frame').show();
	
});

function UncheckAll(){ 
    var w = document.getElementsByTagName('input'); 
    for(var i = 0; i < w.length; i++){ 
      if(w[i].type=='checkbox'){ 
        w[i].checked = false; 
      }
    }
} 