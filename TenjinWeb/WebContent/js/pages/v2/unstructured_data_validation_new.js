/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  unstructured_data_validation.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 08-04-2020		   Ashiki 				   TENJINCG-1204
*/



$(document).ready(function() {
	mandatoryFields('validate_form');
	$('#excelVldtn').hide();
	$('#pdfVldtn').hide();
	$('#sheetNme').hide();
	$('#txtToSearch').hide();
	$('#cellRefrnce').hide();
	$('#cellRnge').hide();
	$('#expctdOccrnce').hide();
	$('#pageNo').hide();
	$('#passwd').hide();
	$('#chckPassword').hide();
	$('#validation1').hide();
	$('#validation2').hide();
	$('#validation3').hide();
	$('#validation4').hide();
	$('#validation5').hide();
	
	
	
	$('#fileType').change(function(){
		clearMessages();
		var fileType = $('#fileType').val();
		if(fileType=="pdf"){
			$('#pdfVldtn').show();
			$('#excelVldtn').hide();
			$('#sheetNme').hide();
			$('#txtToSearch').show();
			$('#cellRefrnce').hide();
			$('#cellRnge').hide();
			$('#expctdOccrnce').show();
			$('#pageNo').hide();
			$('#passwd').hide();
			$('#chckPassword').show();
			$('#validation4').show();
			$('#validation2').hide();
			$('#validation3').hide();
			$('#validation5').hide();
			$('#validation1').hide();
			
			
		}
		else if(fileType=="excel"){
			$('#validation1').show();
			$('#excelVldtn').show();
			$('#pdfVldtn').hide();
			$('#sheetNme').show();
			$('#txtToSearch').show();
			$('#cellRefrnce').show();
			$('#cellRnge').hide();
			$('#expctdOccrnce').hide();
			$('#pageNo').hide();
			$('#passwd').hide();
			$('#chckPassword').show();
			$('#validation2').hide();
			$('#validation3').hide();
			$('#validation4').hide();
			$('#validation5').hide();
		}
	});
	
	$('#excelValidation').change(function(){
		var excelValidation = $('#excelValidation').val();
		if(excelValidation=="Text occurrence")
		{
			
			$('#txtToSearch').show();
			$('#sheetNme').show();
			$('#cellRefrnce').hide();
			$('#cellRnge').hide();
			$('#expctdOccrnce').hide();
			$('#chckPassword').show();
			$('#pageNo').hide();
		}
		else if(excelValidation=='Text validation')
		{
			$('#validation1').show();
			$('#txtToSearch').show();
			$('#sheetNme').show();
			$('#cellRefrnce').show();
			$('#cellRnge').hide();
			$('#expctdOccrnce').hide();
			$('#chckPassword').show();
			$('#pageNo').hide();
			$('#validation1').show();
			$('#validation4').hide();
			$('#validation2').hide();
			$('#validation3').hide();
			$('#validation5').hide();
		}
		else if(excelValidation=="Text occurrence in cell range")
		{
			$('#txtToSearch').show();
			$('#sheetNme').show();
			$('#cellRefrnce').hide();
			$('#cellRnge').show();
			$('#expctdOccrnce').hide();
			$('#chckPassword').show();
			$('#pageNo').hide();
		}
		else if(excelValidation=="Text occurrence validation")
		{
			$('#txtToSearch').show();
			$('#sheetNme').show();
			$('#expctdOccrnce').show();
			$('#cellRefrnce').hide();
			$('#cellRnge').hide();
			$('#chckPassword').show();
			$('#pageNo').hide();
			$('#validation2').show();
			$('#validation1').hide();
			$('#validation4').hide();
			$('#validation3').hide();
			$('#validation5').hide();
		}
		else if(excelValidation=="Text occurrence validation in cell range")
		{
			$('#txtToSearch').show();
			$('#sheetNme').show();
			$('#cellRefrnce').hide();
			$('#cellRnge').show();
			$('#expctdOccrnce').show();
			$('#chckPassword').show();
			$('#pageNo').hide();
			$('#validation3').show();
			$('#validation1').hide();
			$('#validation2').hide();
			$('#validation4').hide();
			$('#validation5').hide();
		}
		
	});
	
	$('#pdfvalidation').change(function(){

		var pdfvalidation = $('#pdfvalidation').val();
		if(pdfvalidation=='Text occurrence')
		{
			$('#txtToSearch').show();
			$('#sheetNme').hide();
			$('#cellRefrnce').hide();
			$('#cellRnge').hide();
			$('#expctdOccrnce').hide();
			$('#chckPassword').show();
			$('#pageNo').hide();
		}
		else if(pdfvalidation=="Text occurrence validation")
		{
			$('#txtToSearch').show();
			$('#sheetNme').hide();
			$('#cellRefrnce').hide();
			$('#cellRnge').hide();
			$('#expctdOccrnce').show();
			$('#chckPassword').show();
			$('#pageNo').hide();
			$('#validation4').show();
			$('#validation1').hide();
			$('#validation2').hide();
			$('#validation3').hide();
			$('#validation5').hide();
		}
		else if(pdfvalidation=="Text occurrence in page")
		{
			$('#txtToSearch').show();
			$('#sheetNme').hide();
			$('#expctdOccrnce').hide();
			$('#cellRefrnce').hide();
			$('#cellRnge').hide();
			$('#pageNo').show();
			$('#chckPassword').show();
		}
		else if(pdfvalidation=="Text occurrence validation in page")
		{
			$('#txtToSearch').show();
			$('#sheetNme').hide();
			$('#cellRefrnce').hide();
			$('#cellRnge').hide();
			$('#expctdOccrnce').show();
			$('#pageNo').show();
			$('#chckPassword').show();
			$('#validation5').show();
			$('#validation1').hide();
			$('#validation2').hide();
			$('#validation3').hide();
			$('#validation4').hide();
		}
		
	
	});
	
	
	$('#password').change(function(){
		if(this.checked)
			$('#passwd').show();
		else
		$('#passwd').hide();
		});
	
	 	  
	  $('#btnCancel').click(function(){	
			var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
			if(confirmResult){
				var stepRecId = $('#stepRecId').val();
				  window.location.href='UnstructuredDataServlet?t=Unstructured_Data_List&stepRecId=' + stepRecId;
			}else{
				return false;
			}
			
		});
	  
	  var defFileType = $('#fType').val();
		var validateName=$('#validateName').val();
		if(defFileType=='pdf'){
			
			$('#pdfvalidation').val(validateName)
			$('#pdfVldtn').show();
			if(validateName=='Text occurrence')
			{
				$('#txtToSearch').show();
				$('#sheetNme').hide();
				$('#cellRefrnce').hide();
				$('#cellRnge').hide();
				$('#expctdOccrnce').hide();
				$('#chckPassword').show();
				$('#pageNo').hide();
			}
			else if(validateName=="Text occurrence validation")
			{
				$('#txtToSearch').show();
				$('#sheetNme').hide();
				$('#cellRefrnce').hide();
				$('#cellRnge').hide();
				$('#expctdOccrnce').show();
				$('#chckPassword').show();
				$('#pageNo').hide();
				$('#validation1').hide();
				$('#validation2').hide();
				$('#validation3').hide();
				$('#validation4').show();
				$('#validation5').hide();
			}
			else if(validateName=="Text occurrence in page")
			{
				$('#txtToSearch').show();
				$('#sheetNme').hide();
				$('#expctdOccrnce').hide();
				$('#cellRefrnce').hide();
				$('#cellRnge').hide();
				$('#pageNo').show();
				$('#chckPassword').show();
			}
			else if(validateName=="Text occurrence validation in page")
			{
				$('#txtToSearch').show();
				$('#sheetNme').hide();
				$('#cellRefrnce').hide();
				$('#cellRnge').hide();
				$('#expctdOccrnce').show();
				$('#pageNo').show();
				$('#chckPassword').show();
				$('#validation1').hide();
				$('#validation2').hide();
				$('#validation3').hide();
				$('#validation4').hide();
				$('#validation5').show();
			}
			
		
			
		}else if(defFileType=='excel'){
			$('#excelValidation').val(validateName);
			$('#excelVldtn').show();
			if(validateName=="Text occurrence")
			{
				$('#txtToSearch').show();
				$('#sheetNme').show();
				$('#cellRefrnce').hide();
				$('#cellRnge').hide();
				$('#expctdOccrnce').hide();
				$('#chckPassword').show();
				$('#pageNo').hide();
			}
			else if(validateName=='Text validation')
			{
				$('#txtToSearch').show();
				$('#sheetNme').show();
				$('#cellRefrnce').show();
				$('#cellRnge').hide();
				$('#expctdOccrnce').hide();
				$('#chckPassword').show();
				$('#pageNo').hide();
				$('#validation1').show();
				$('#validation2').hide();
				$('#validation3').hide();
				$('#validation4').hide();
				$('#validation5').hide();
			}
			else if(validateName=="Text occurrence in cell range")
			{
				$('#txtToSearch').show();
				$('#sheetNme').show();
				$('#cellRefrnce').hide();
				$('#cellRnge').show();
				$('#expctdOccrnce').hide();
				$('#chckPassword').show();
				$('#pageNo').hide();
			}
			else if(validateName=="Text occurrence validation")
			{
				$('#txtToSearch').show();
				$('#sheetNme').show();
				$('#expctdOccrnce').show();
				$('#cellRefrnce').hide();
				$('#cellRnge').hide();
				$('#chckPassword').show();
				$('#pageNo').hide();
				$('#validation1').hide();
				$('#validation2').show();
				$('#validation3').hide();
				$('#validation4').hide();
				$('#validation5').hide();
			}
			else if(validateName=="Text occurrence validation in cell range")
			{
				$('#txtToSearch').show();
				$('#sheetNme').show();
				$('#cellRefrnce').hide();
				$('#cellRnge').show();
				$('#expctdOccrnce').show();
				$('#chckPassword').show();
				$('#pageNo').hide();
				$('#validation1').hide();
				$('#validation2').hide();
				$('#validation3').show();
				$('#validation4').hide();
				$('#validation5').hide();
			}
			
		}
	  
});

function isNumberKey(evt){
	   var charCode = (evt.which) ? evt.which : event.keyCode
	   if (charCode > 31 && (charCode < 48 || charCode > 57))
	      return false;

	   return true;
		  }




