/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  project_mail_setting.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 30-04-2019           Padmavathi              TENJINCG-1049
 * 06-05-2019			Ashiki					TENJINCG-1051
 * 08-05-2019			Preeti					TENJINCG-1053
   12-03-2020			Lokanath                TENJINCG-1185
*/ 
var projectUserTable;
$(document).ready(function(){
	
	$('#admin_config_actions').dataTable({
		  "bSort": false
	});
	$('#user_config_actions').dataTable({
		  "bSort": false
	});
	$('.tab-section').hide();
	$('#tabs a').bind('click', function(e){
        $('#tabs a.current').removeClass('current');
        $('.tab-section:visible').hide();
        $(this.hash).show();
        $(this).addClass('current');
        e.preventDefault();
    }).filter(':first').click();
	
	
	$("#txtEsc").hide();
	$("#txtSub").hide();
	
	
	var selEscRuleId=$('#escRuleId').val();
	var count=0;
	$('input:radio').each(function() {
		
		if(selEscRuleId==0){
			if(count==0){
			$("#"+$(this).attr('id')).prop("checked", true);
			count=count+1;
			}
		}else{
		if($("#"+$(this).attr('id')).val()==selEscRuleId){
			$("#"+$(this).attr('id')).prop("checked", true);
		}else{
			$("#"+$(this).attr('id')).prop("checked",false);
		}
		}
	});
	
	var escRulePercentage=$('#escRulePercentage').val();
	
	$('input:radio').each(function() {
		
		if($(this).is(':checked')) {
		
		}else{
			var idlength=$(this).attr('id').length;
			var id=$(this).attr('id').substr(idlength-1,idlength);
		$("#"+this.id+id).val('');
		}
	});
	
	
	$('input:radio').each(function() {
		  if($(this).is(':checked')) {
		    
			  $(this).attr('id');
				var idlength=$(this).attr('id').length;
				var id=$(this).attr('id').substr(idlength-1,idlength);
			$("#"+this.id+id).show();
			$("."+this.id+id).show();
			
		  } 
		  else {
			  $(this).attr('id');
				var idlength=$(this).attr('id').length;
				var id=$(this).attr('id').substr(idlength-1,idlength);
			$("#"+this.id+id).hide();
			$("."+this.id+id).hide();
		  }
		});
	
	if($('#txtEscRule').val()=='Y'){
		$('#txtEscRule').attr('checked',true)
		$("#txtEsc").show();
	 }else{
		 $('#txtEscRule').attr('checked',false)
		 /*Modified By Ashiki for TENJINCG-1051 starts*/
		 /*$('#txtEsc').hide();*/
		 $('#txtEsc').show();
		 /*Modified By Ashiki for TENJINCG-1051 ends*/
	 }
	
	
	$('#btnBack').click(function(){
		var projectId = $('#projectid').val();
		window.location.href = 'ProjectServletNew?t=load_project&paramval=' + projectId + '&v=PROJECT';
	});
		
	if($('#txtemail').val()=='Y'){
		$('#txtemail').attr('checked',true)
		$("#txtSub").show();
	 }else{
		 $('#txtemail').attr('checked',false)
		 /*Modified By Ashiki for TENJINCG-1051 starts*/
		/* $('#txtSub').hide();*/
		 $('#txtSub').show();
		 /*Modified By Ashiki for TENJINCG-1051 ends*/
	 }
	 /*Added by Preeti for TENJINCG-1053 starts*/
	 if($('#txtEscRule').is(':checked')){
		var map = new Map();
		var j=1;
		for(var i =101;i<=104;i++){
			map[i]= $('.rulePercentage'+j).val();
			j=j+1;
		}
		map[105] = $('#seltestset').val();
		map[106] = $('#selaut').val();
		var a=101;
		var b=1;
		$('#emailEscalation-table').find('input[type="checkbox"]').each(function() {
			if(!$(this).hasClass('tbl-select-all-rows')) {
				if(map[a])
					$(this).prop('checked', true);
				a=a+1;
				b=b+1;
			}
		});
	}
	/*Added by Preeti for TENJINCG-1053 ends*/
	$('#btnSave').click(function(){
	if ( $('#txtemail').is(':checked') ) {
		$('#txtemail').val('Y');
	} else {
		$('#txtemail').val('N');
	}
	/*Added by lokanath for TENJINCG-1185 starts*/
	var additionalMails;
	var i;
	if($('#addAddditionalMailIds').val()){
		additionalMails=$('#addAddditionalMailIds').val().split(',');
		for(i=0;i<additionalMails.length;i++){
			var atposition=additionalMails[i].indexOf("@");  
			var dotposition=additionalMails[i].lastIndexOf(".");  
			if (atposition<1 || dotposition<atposition+2 || dotposition+2>=additionalMails[i].length){  
			 // showMessageOnElement('Please enter a valid e-mail address','error',$('#user-message-action'));
			  showMessage('Please enter a valid e-mail address','error');
			  return false;  
			  }  
		}
	}
	
	/*Added by lokanath for TENJINCG-1185 ends*/
	
	
	/*Modified By Ashiki for TENJINCG-1051 starts*/
	if($('#txtEscRule').is(':checked')){
			
			var $escalationRuleForm = $("<form action='ProjectMailServlet' method='POST' />");
			var count =1;
			var json = {};
			/*Modified by Preeti for TENJINCG-1053 starts*/
			var jsonMailIds = {};
			$('#emailEscalation-table').find('input[type="checkbox"]:checked').each(function() {
				var cnt =1;
				if(!$(this).hasClass('tbl-select-all-rows')) {
					for(var i=0 ;i<6;i++){
						if($(this).hasClass('escalationRules'+cnt)) {
							json[$('.escalationRules'+cnt).val()] = $('.rulePercentage'+cnt).val().trim();
							jsonMailIds[$('.escalationRules'+cnt).val()] = $('.escalationMailId'+cnt).val().trim();
						}
						cnt=cnt+1;
					}
				}
			});
			$('#main_form').append($("<input type='hidden' name='escalationRulesDesc' value='"+ JSON.stringify(json) +"' />"));
			$('#main_form').append($("<input type='hidden' name='escalationMailIds' value='"+ JSON.stringify(jsonMailIds) +"' />"));
			/*Modified by Preeti for TENJINCG-1053 ends*/
			$('body').append($escalationRuleForm);
			$escalationRuleForm.submit();
			
			
			 /*$(this).attr('id');
				var idlength=$(this).attr('id').length;
				var id=$(this).attr('id').substr(idlength-1,idlength);
				$("#"+this.id+id).prop('required',true);
			var rulePercentage=$("#"+this.id+id).val();
			if(rulePercentage==''|| rulePercentage=='0'){
				showLocalizedMessage('invalid.input', 'error');
				return false;
			}
			var ruleDesc=$("#"+this.id+id+id).val();
			
			
			 $('#main_form').append($("<input type='hidden' id='ruleCriteria' name='ruleCriteria' value='"+rulePercentage+"' />"));
			 $('#main_form').append($("<input type='hidden' id='ruleDesc' name='ruleDesc' value='"+ruleDesc+"' />"));*/
			 
		/*Modified By Ashiki for TENJINCG-1051 ends*/
	}
	//Added by Lokanath for TENJINCG-1184 starts
	// showMessageOnElement("Saved sucessfully",'success',$('#user-message-user-action'));
	 showMessage('Saved sucessfully','success');
	//Added by Lokanath for TENJINCG-1184 ends
	});
	
	
	
	$('.rulePercentage').keyup(function (){
		var inputVal = $(this).val();
	
		var regex = new RegExp(/[^0-9]/g);
		
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			alert('Enter only Numbers ');
			$(this).val('');
		}
		if(!inputVal=='' && inputVal==0){
			alert('Entered percentage should be greater than zero.');
			$(this).val('');
		}
		if(inputVal>100){
			alert('Entered percentage should be less than or equal to hundred.');
			$(this).val('');
		}
	});
	
	var action;
	$('#admin_config_actions').on('click', 'tbody tr', function(e) {
        $(this).addClass('highlight').siblings().removeClass('highlight');
        action = $(this).closest('tr').attr('id');
		enableCheckBoxesForSelectedUsers($('#projectid').val(),action);
      });
	
	
	$('#btnSaveAdminAction').click(function() {
		clearMessagesOnElement($('#user-message-action'));
		var prjId= $('#projectid').val();
		var selectedUsers = '';
		if(!action){
			showMessageOnElement('Please select atleast one Action to continue','error',$('#user-message-action'));
			return false;
		}
		$('#prj_users-Table').find('input[type="checkbox"]:checked').each(function () {
		       //this is the current checkbox
		       if(this.id != 'check_all'){
		    	   selectedUsers = selectedUsers + this.id + ',';
		       }
		});
		
		if(selectedUsers==''){
			showMessageOnElement('Please select atleast one user to continue','error',$('#user-message-action'));
			return false;
		}
		//Removing extra comma from selectedUsers
		selectedUsers=selectedUsers.slice(0,-1);
		  $.ajax({
			  url:'ProjectMailServlet',
			  data :{param:'admin_config_action',prjId:prjId,action:action,selectedUsers:selectedUsers},
			  async:true,
			  dataType:'json',
			  success:function(data){
				  var status = data.status;
				  if(status == 'SUCCESS')
				  {
					  showMessageOnElement(data.message,'success',$('#user-message-action'));
				  }
				  else{
					  showMessageOnElement(data.message,'error',$('#user-message-action'));
				  }
			   },
			   error:function(xhr, textStatus, errorThrown){
				   showMessage(errorThrown,'error');
			   },
			   });
		});
	$('#btnSaveUserAction').click(function() {
		clearMessagesOnElement($('#user-message-user-action'));
		var prjId= $('#projectid').val();
		var selectedActions = '';
		$('#user_config_actions').find('input[type="checkbox"]:checked').each(function () {
		       //this is the current checkbox
		       if(this.id != 'check_all'){
		    	   selectedActions = selectedActions + this.id + ',';
		       }
		});
		//Removing extra comma from selectedUsers
		selectedActions=selectedActions.slice(0,-1);
		  $.ajax({
			  url:'ProjectMailServlet',
			  data :{param:'user_config_action',prjId:prjId,selectedActions:selectedActions},
			  async:true,
			  dataType:'json',
			  success:function(data){
				  var status = data.status;
				  if(status == 'SUCCESS')
				  {
					  showMessageOnElement(data.message,'success',$('#user-message-user-action'));
				  }
				  else{
					  showMessageOnElement(data.message,'error',$('#user-message-user-action'));
				  }
			   },
			   error:function(xhr, textStatus, errorThrown){
				   showMessage(errorThrown,'error');
			   },
			   });
		});
	$('#project-user-config-tab-anchor').click(function(e){
		clearMessagesOnElement($('#user-message-user-action'));
		clearMessages();
		var prjId= $('#projectid').val();
		e.preventDefault();
		enableCheckBoxesForSelectedActions(prjId);
		
	});
	$('#project-admin-config-tab-anchor').click(function(e){
		clearMessagesOnElement($('#user-message-action'));
		clearMessages();
		var prjId= $('#projectid').val();
		e.preventDefault();
		initUserTable(prjId);
		
	});
	/*Added by Preeti for TENJINCG-1053 starts*/
	$('.rulePercentage5').children('option').each(function(){
		var ts=$('#seltestset').val();
        var val = $(this).val();
        if(val == ts){
            $(this).attr({'selected':true});
        }
    });
	$('.rulePercentage6').children('option').each(function(){
		var aut=$('#selaut').val();
        var val = $(this).val();
        if(val == aut){
            $(this).attr({'selected':true});
        }
    });
   	/*Added by Preeti for TENJINCG-1053 ends*/
	
	/*Added by Lokanath for TENJINCG-1184 start*/
	$('#selectOperationType').children('option').each(function(){
        var mailType=$('#selectedPriority').val();
        var val = $(this).val();
        if(val == mailType){
            $(this).attr({'selected':true});
        }
	});
	/*Added by Lokanath for TENJINCG-1184 ends*/
});

$(document).on('change','.escalationRules',function(){
	
		$('input:radio').each(function() {
			  if($(this).is(':checked')) {
			    
				  $(this).attr('id');
					var idlength=$(this).attr('id').length;
					var id=$(this).attr('id').substr(idlength-1,idlength);
				$("#"+this.id+id).show();
				
			  } 
			  else {
				  $(this).attr('id');
					var idlength=$(this).attr('id').length;
					var id=$(this).attr('id').substr(idlength-1,idlength);
				$("#"+this.id+id).hide();
			  }
			});

	});


		
$(document).on('change','#txtEscRule',function(){
	if($("#txtEscRule").is(":checked")){
		
		$("#txtEsc").show();
	}else{
		/*Modified By Ashiki for TENJINCG-1051 starts*/
		/*$("#txtEsc").hide();*/
		$("#txtEsc").show();
		/*Modified By Ashiki for TENJINCG-1051 ends*/
	}
});

$(document).on('change','#txtemail',function(){
	if($("#txtemail").is(":checked")){
		
		$("#txtSub").show();
	}else{
		 /*Modified By Ashiki for TENJINCG-1051 starts*/
		/*$("#txtSub").hide();*/
		$("#txtSub").show();
		 /*Modified By Ashiki for TENJINCG-1051 ends*/
	}
});


function initUserTable(projectId) {

		projectUserTable=$('#prj_users-Table').DataTable({
			ajax:{
				url:'ProjectAjaxServlet?t=search_prj_users&projectId='+projectId,
				dataSrc:''
			},
			"columns":[
				{"data":"id"},
				{"data":"id"},
				
			],
			"columnDefs":[{
				"targets":0,
				"searchable":false,
				"orderable":false,
				"className":"select-row",
				"render":function(data, type, full, meta) {
					return "<input type='checkbox' data-user-id='" + data + "'  id='" + data + "' />";
				}
			}]
		});
	}

function enableCheckBoxesForSelectedUsers(projectId,action) {
	  $.ajax({
		  url:'ProjectMailServlet',
		  data :{param:'getPrjMailPreference',prjId:projectId,action:action},
		  async:true,
		  dataType:'json',
		  success:function(data){
			  var status = data.status;
			  if(status == 'SUCCESS')
			  {
				  var users= users=data.users.split(',');
				$('#prj_users-Table').find('input[type="checkbox"]').each(function() {
					$(this).prop("checked", false);
					for (var index = 0; index < users.length; index++) {
						if(users[index]==$(this).attr('id'))
							$(this).prop("checked", true);
						continue;
					}
				});
			  }
			  else{
				  showMessageOnElement(data.message,'error',$('#user-message-action'));
			  }
		   },
		   error:function(xhr, textStatus, errorThrown){
			   showMessage(errorThrown,'error');
		   },
		   });
}

function enableCheckBoxesForSelectedActions(projectId) {
	  $.ajax({
		  url:'ProjectMailServlet',
		  data :{param:'get_user_mail_preferences',prjId:projectId},
		  async:true,
		  dataType:'json',
		  success:function(data){
			  var status = data.status;
			  if(status == 'SUCCESS')
			  {
				  
				$('#user_config_actions').find('input[type="checkbox"]').each(function() {
					for (var index = 0; index < data.actions.length; index++) {
						if(data.actions[index]==$(this).attr('id')){
							$(this).prop("checked", true);
						}
						continue;
					}
				});
				}else{
				  showMessageOnElement(data.message,'error',$('#user-message-user-action'));
			  }
		   },
		   error:function(xhr, textStatus, errorThrown){
			   showMessage(errorThrown,'error');
		   },
		   });
}
