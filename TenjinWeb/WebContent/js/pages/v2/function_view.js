/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  function_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                	CHANGED BY              DESCRIPTION
* 24-Oct-2017			Sriram Sridharan		Newly added for TENJINCG-399
* 14-11-2017			Preeti					TENJINCG-413
* 31-10-2018			Sriram Sridharan		TENJINCG-894
* 30-11-2018            Padmavathi              TJNUN262-28
* 25-03-2019			Pushpalatha				TENJINCG-968
* 12-04-2019			Pushpalatha				TENJINCG-1031
*/
/*Added by Preeti for TENJINCG-413 starts*/
$(document).ready(function() {
	/*Added by Pushpalatha for TENJINCG-1031 starts*/
	$('#download').click(function(){
		var appId=$('#aut').val();
		var functionCode=$('#functionCode').val();
		window.location.href="FunctionsDownloadServlet?aut="+appId+"&functionCode="+functionCode;
	});
	/*Added by Pushpalatha for TENJINCG-1031 ends*/
	
		/*Added by Pushpalatha for TENJINCG-968 starts*/
		mandatoryFields('functionForm');
		/*Added by Pushpalatha for TENJINCG-968 ends*/
		
		$('#txtDate').hide();
		$('#dateFormat').keyup(function() {
		}).focus(function() {
			$('#txtDate').show();
		}).blur(function() {
			$('#txtDate').hide();
		});
		$('#btnSave').click(function() {
			/*Added by Padmavathi for TJNUN262-33 starts*/
			var dateFormat=$('#dateFormat').val()
			var regex = new RegExp(/[0-9]/g);
			var containsNonNumeric = dateFormat.match(regex);
			if(containsNonNumeric){
				showMessage("Please enter valid date format", 'error');
				return false;
			}
			/*Added by Padmavathi for TJNUN262-33 ends*/
			var dateValue=isValidDate($('#dateFormat').val());
			if(!dateValue){
				showMessage("Please enter valid date format", 'error');
				return false;
			}
		});
		});
/*Added by Preeti for TENJINCG-413 ends*/
var storedFiles = [];
		var removedFiles = [];
		$(document).on('click', '#btnBack', function() {
	var appId = $('#aut').val();
	var groupName = $('#originalGroup').val();
	window.location.href='FunctionServlet?appId=' + appId + '&group=' + groupName;
});

$(document).on('click', '#btnLearningHistory', function() {
	var appId = $('#aut').val();
	var functionCode = $('#functionCode').val();
	window.location.href='FunctionServlet?t=learningHistory&appId=' + appId + '&function=' + functionCode;
});

$(document).on('click', '#btnExtractionHistory', function() {
	var appId = $('#aut').val();
	var functionCode = $('#functionCode').val();
	window.location.href='FunctionServlet?t=extractHistory&appId=' + appId + '&function=' + functionCode;
});


$(document).on('click', '#btnWaitTime', function() {
	var funcCode = $('#functionCode').val();
	window.location.href = 'ModuleServlet?param=field_wait_time&appID=' + $('#aut').val() + '&funcCode=' + funcCode;
});


$(document).on('click', '#btnAssistedLearning', function() {
	var funcCode = $('#functionCode').val();
	window.location.href='AssistedLearningDataServlet?param=init&app=' + $('#aut').val() + '&func=' + funcCode;
});

$(document).on('click', '#template', function() {
	$('#ttd-options-frame').attr('src','ttd_download_options.jsp?a=' + $('#aut').val() + '&m=' +$('#functionCode').val() + '&t=G');
	$('.modalmask').show();
	$('#templateDownloadModal').show();
});
/*Modified by Preeti for TENJINCG-413 starts*/
$(document).on('click', '#exportMetadata', function() {
	var $img = $("<img src='images/inprogress.gif' alt='In progress. Please wait...' />");
	$('.progressbar').show();
	$('.progressbar').append($img);
	var appId = $('#aut').val();
	var funcCode = $('#functionCode').val();
	/* Modified By Prem for Tenj212-31 Start*/
	$.ajax({
		type:'GET',
		url:'SpreadsheetDownloadServlet',
	data:'requesttype=metadata&aut-selection='+appId+'&func-selection='+funcCode,
		processData:false,
		contentType:false,
		dataType:'json',
		success:function(data){
			console.log(data);
			if(data.status == 'success'){
				var path = data.path;
				var $c = $("<a id='downloadFile' href="+path+" target='_blank' download />");
				$("body").append($c);
				$("#downloadFile").get(0).click();
				$c.remove();
				$img.remove();
				$('.progressbar').hide();
			}else if(data.status=='error'){
				showMessage(data.message, 'error', $('#export-message'));
				$img.remove();
				$('.progressbar').hide();
			}else {
				showMessage('An internal error occurred. Please contact Tenjin Support.', 'error', $('#export-message'));
				$img.remove();
				$('.progressbar').hide();
			}
		},
		error:function(data){
			showMessage('An internal error occurred. Please contact Tenjin Support.', 'error', $('#export-message'));
			$img.remove();
			$('.progressbar').hide();
		}
	});
	/* Modified By Prem for Tenj212-31 End*/
});
/*Modified by Preeti for TENJINCG-413 ends*/
$(document).on('click', '#importMetadata', function() {
	$('#modal_import').prop('disabled',false);
	$('#modal_import').show();
	$('#modal_cancel').prop('disabled',false);
	$('#modal_cancel').attr('value','Cancel');
	
	clearMessagesOnElement($('#user-message-import'));
	
	var $el = $('#uploadedFile');
	$el.wrap('<form>').closest('form').get(0).reset();
	$el.unwrap();
	
	$('.modalmask').show();
	$('#importMetadataModal').show();
});

$(document).on('click', '#modal_cancel', function() {
	closeModal();
});

$(document).on('click', '#modal_import', function(e) {
	var appId = $('#aut').val();
	var funcCode = $('#functionCode').val(); 
	
	handleFileUploadForm(e, appId, funcCode);
});

/*$('#txtFile').on("change", handleFileSelect);*/
$(document).on('change', '#uploadedFile', handleFileSelect);

function handleFileSelect(e) {
	var files = e.target.files;
	var filesArr = Array.prototype.slice.call(files);
	
	filesArr.forEach(function(f){
		storedFiles.push(f);
		
		var reader = new FileReader();
		reader.onload = function(e){
			//var html = "<div class='clear'></div><div class='grid_15 att_block'><span>" + f.name + "</span><a href='#' data-file='" + f.name + "' data-res='" + e.target.result + "' class='selFile' title='Click to Remove'>Remove</a></div>";
			
			/*var html = "<div class='att_block grid_6'><p class='grid_4' title='" + f.name + "'>" + f.name + "</p><a href='#' recid='0' data-file='" + f.name + "' class='selFile' title='Click to Remove'>Remove</a></div>";
			selDiv.append(html);*/
		}
		
		reader.readAsDataURL(f);
	});
	
	//alert(storedFiles.length);
}

function handleFileUploadForm(e, appId, functionCode){
	e.preventDefault();
	//$('.modalmask').show();
	/*$('#processing_icon').html("<img src='images/loading.gif' alt='In Progress..' />");
	$('#import_message').html('<p>Tenjin is processing your actions on the defects. Please Wait...</p>')
	$('#import_progress_dialog').show();*/
	$('#import_progress_dialog').show();
	var data = new FormData();
	for(var i=0, len=storedFiles.length; i<len; i++){
		data.append('files', storedFiles[i]);
	}
	
	$('#modal_import').prop('disabled',true);
	$('#modal_cancel').prop('disabled',true);
	
	/*data.append('form_data', $('#form_data').val());
	data.append('req_type','new_defect');
	data.append('post_defect', $('#post_defect').val());*/
	data.append('requesttype', 'metadata');
	data.append('aut-selection', appId);
	data.append('func-selection', functionCode);
	$.ajax({
		type:'post',
		url:'SpreadsheetUploadServlet',
		data:data,
		processData:false,
		contentType:false,
		dataType:'json',
		success:function(r){
			console.log(r);
			//alert(r.status);
			if(r.status == 'success'){
				showMessageOnElement(r.message,'success',$('#user-message-import'));
				$('#import_progress_dialog').hide();
				$('#modal_import').prop('disabled',false);
				$('#modal_cancel').prop('disabled',false);
				$('#modal_cancel').attr('value','Close');
				$('#modal_import').hide();
			}else if(r.status=='error'){
				showMessageOnElement(r.message,'error',$('#user-message-import'));
				$('#import_progress_dialog').hide();
				$('#modal_import').prop('disabled',false);
				$('#modal_cancel').prop('disabled',false);
			}else {
				showMessageOnElement('An internal error occurred. Please contact Tenjin Support.','error',$('#user-message-import'));
				$('#import_progress_dialog').hide();
				$('#modal_import').prop('disabled',false);
				$('#modal_cancel').prop('disabled',false);
			}
		},
		error:function(r){
			showMessageOnElement('An internal error occurred. Please contact Tenjin Support.','error',$('#user-message-import'));
			$('#import_progress_dialog').hide();
			$('#modal_import').prop('disabled',false);
			$('#modal_cancel').prop('disabled',false);
		}
	});
}

function closeModal() {
	$('.modalmask').hide();
	$('.subframe').hide();
	$('#ttd-options-frame').attr('src', '');
}


//TENJINCG-894 (Sriram)
$(document).on('click', '#btnMetadata', function() {
	var appId = $('#aut').val();
	var funcCode = $('#functionCode').val(); 
	window.location.href='FunctionServlet?t=metadata&app=' + appId + '&func=' + funcCode;
});