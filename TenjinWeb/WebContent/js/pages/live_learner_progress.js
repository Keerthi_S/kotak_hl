
/*
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  live_learner_progress.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India



*//*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
  11-02-2019            Leelaprasad P           TENJINCG-936
  19-03-2019			Preeti					TNJN27-12
  08-03-2019			Sahana 					pCloudy 
  11-04-2019			Ashiki					TENJINCG-1032
  16-05-2019			Ashiki					V2.8-12
  08-04-2020			Lokanath				TENJINCG-1193
*/

$(document).ready(function(){
	var msg = $('#user-msg').val();
	/*Added by Leelaprasad for TENJINCG-936 starts*/
	$('#btnBack').hide();
	/*Added by Leelaprasad for TENJINCG-936 ends*/
	/*Added by Ashiki for TENJINCG-1032 starts*/
	$('#btnRelearn').hide();
	/*Added by Ashiki for TENJINCG-1032 ends*/
	/*Added by Lokanath for TENJINCG-1193 starts*/
	$('#btnAbort').show();
	
	/*Added by Lokanath for TENJINCG-1193 ends*/
	if(msg == 'noerror'){
		clearMessages();
	}else{
		showMessage(msg, 'error');
	}
	
	var progressPercentage = $('#progpcnt').val();
	progressBar(progressPercentage, $('#progressBar'));
	
	/*Added by Roshni for T25IT-75 starts */
	if(progressPercentage=='100')
	{
	$('#refreshbar').hide();
	$('#refTimeout').hide();
	$('#btnRefresh').hide();
	/*Added by Lokanath for TENJINCG-1193 starts*/
	$('#btnAbort').hide();
	/*Added by Lokanath for TENJINCG-1193 starts*/
	/*Added by Leelaprasad for TENJINCG-936 starts*/
	$('#btnBack').show();
	/*Added By Ashiki for V2.8-12 starts*/
	var learnStatus=$('#lrnStatus').val();
	if(learnStatus == 'complete'){
		$('#btnRelearn').hide();
	}else{
		/*Added by Ashiki for TENJINCG-1032 starts*/
		$('#btnRelearn').show();
		/*Added by Ashiki for TENJINCG-1032 ends*/
	}
	
	/*Added By Ashiki for V2.8-12 ends*/
	
	/*Added by Leelaprasad for TENJINCG-936 ends*/
	/*Added by Sahana for pCloudy: Starts*/
	$('#btnLiveView').hide();
	/*Added by Sahana for pCloudy: Ends*/
	}
	/*Added by Leelaprasad for TENJINCG-936 starts*/
	$(document).on('click', '#btnBack', function() {
		var appId = $('#aut').val();
		/*Modified by Preeti for TNJN27-12 starts*/
		if(appId != 0)
			window.location.href='FunctionServlet?appId=' + appId ;
		else
			window.location.href='SchedulerServlet?param=FETCH_ALL_ADMIN_SCH&paramval=All';
		/*Modified by Preeti for TNJN27-12 ends*/
	});
	/*Added by Leelaprasad for TENJINCG-936 ends*/
	
	/*Added by Roshni for T25IT-75 ends */
	$('#btnRefresh').click(function(){
		window.location.href='LearnerServlet?param=learner_progress&runid=' + $('#runId').val();
	});
	
	$(document).on('click','.template-gen',function(e){
		var mod = $(this).attr('mod');
		var app = $(this).attr('app');
		var txnMode=$(this).attr('txnMode');
		$('#ttd-options-frame').attr('src','ttd_download_options.jsp?a=' + app + '&m=' +mod + '&t=' + txnMode);
    	$('.modalmask').show();
    	$('.subframe').show();
	});
	
	
	
	//For Automatic Refresh every 5 seconds
	var loop; 
	var autoRefreshFlag = $('#autoRefreshFlag').val();
	if(autoRefreshFlag == 'yes'){
		loop = window.setInterval(function(){window.location.href='LearnerServlet?param=learner_progress&runid=' + $('#runId').val()}, 5000);
	}else{
		$('#toggleRefresh').hide();
	}
	
	//To stop automatic refresh
	$('#toggleRefresh').click(function(){
		var action = $(this).attr('ac');
		if(action =='stop'){
			clearInterval(loop);
			$(this).attr('ac','start');
			$(this).attr('value','Start Automatic Refresh');
			$(this).removeClass('cancel');
			$(this).addClass('ok');
		}else if(action =='start'){
			loop = window.setInterval(function(){window.location.href='LearnerServlet?param=learner_progress&runid=' + $('#runId').val()}, 5000);

			$(this).attr('ac','stop');
			$(this).attr('value','Stop Automatic Refresh');
			$(this).removeClass('ok');
			$(this).addClass('cancel');
		}
	});
	
$('.modalmask').click(function(){
		
    	closeModal();
	});
/*Added by Sahana for pCloudy: Starts*/
$('#btnLiveView').click(function(){
	var win = window.open('https://device.pcloudy.com/storage', '_blank');
	win.focus();
});
/*Added by Sahana for pCloudy: Ends*/
/*Added by Ashiki for TENJINCG-1032 starts*/
$('#btnRelearn').click(function(){
	var clientName=$('#clientName').val();
	
	$('#main_form').submit();
});
/*Added by Ashiki for TENJINCG-1032 ends*/
/*  Added by Lokanath for TENJINCG-1193 starts*/
$(document).on('click','#btnAbort', function() {
	var runId = $('#runId').val();
	$('#btnAbort').hide();
	$.ajax({
		url:'TestRunServlet?param=abort&run=' + runId,
		dataType:'json',
		async:true,
		success:function(data) {
			if(data.status.toLowerCase() ==='success'){
				window.location.href='ExecutorServlet?param=learn_abort_progress&paramval=' + runId;
			}else{
				showMessage(data.message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown) {
			showMessage('Could not abort run due to an internal error. Please contact Tenjin Support.', 'error');
		}
	});
});
/* Added by Lokanath for TENJINCG-1193 ends */
});

function closeModal()
{
	$('.modalmask').hide();
	$('.subframe').hide();
	$('#ttd-options-frame').attr('src','');
}