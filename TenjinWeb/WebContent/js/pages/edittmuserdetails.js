/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  edituserautdetails.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 24-01-2017			Manish					TENJINCG-54
* 15-06-2018            Padmavathi              T251IT-107
* 25-06-2018			Preeti					T251IT-140
* 04-01-2019			Ashiki					for TM user credentials
* 25-03-2019			Pushpalatha				TENJINCG-968
*/	



$(document).ready(function(){
	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('defect_user_mapping_form');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	/*Modified by Padmavathi for T251IT-107 starts*/
	$('#lstInstance').change(function(){
		var instance=$('#lstInstance').val();
		/*Added by Preeti for T251IT-140 starts*/
		var oldinstance=$('#tmInstance').val();
		/*Added by Preeti for T251IT-140 ends*/
		var userId = $('#txtUserId').val();
		$.ajax({
			url:'TestCaseServlet',
			/*Modified by Preeti for T251IT-140 starts*/
			/*data:'param=check_tm_user_name&&instance='+instance+'&userId='+userId,*/
			data:'param=check_tm_user_name&instance='+instance+'&userId='+userId+'&oldinstance='+oldinstance,
			/*Modified by Preeti for T251IT-140 ends*/
			async:false,
			dataType:'json',
			success: function(data){
			var status = data.status;
			if(status == 'ERROR'){		
				showMessage(data.message, 'error');
				$("#TMUserName").focus();
			}else{clearMessages()}
		},
		
		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown, 'error');			
		}
	});
	}); 
	/*Modified by Padmavathi for T251IT-107 ends*/
	$('#btnBack').click(function(){
		window.location.href = 'TestCaseServlet?param=FETCH_TM_USER_CREDENTIALS';
	});
	
	
	$('#btnSave').click(function(){
		//Added By Priyanka for Mandatory filed validation starts
		var validated = validateForm('defect_user_mapping_form');
		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
		}
		//Added By Priyanka for Mandatory filed validation ends
		var json = new Object();
		json.instance = $('#lstInstance').val();
		json.tmUser = $('#TMUserName').val();
		/*Added By Ashiki for TM user credentials starts*/
		/*json.tmPwd =$('#TMPassword').val();*/
		var tmPwd =encodeURIComponent($('#TMPassword').val());
		/*Added By Ashiki for TM user credentials ends*/
		/* Added by Padmavathi for T251IT-107 starts*/
		json.oldInstance=$('#tmInstance').val();
		/* Added by Padmavathi for T251IT-107 ends*/
		var jstring = JSON.stringify(json);
		$.ajax({
			url:'TestCaseServlet',
			/*Added By Ashiki for TM user credentials starts*/
			/*data:'param=update_tm_user_credentials&json='+jstring,*/
			data:'param=update_tm_user_credentials&json='+jstring+'&tmPwd='+tmPwd,
			/*Added By Ashiki for TM user credentials ends*/
			async : false,
			dataType : 'json',
			success : function(data) {
				var status = data.status;
				if (status == 'SUCCESS') {
					
					
					showMessage(data.message,
							'success');
				} else {
					showMessage(data.message,
							'error');
				}
			}
		});
		
		
	});
});