/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  userlist.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 14-12-2016            Parveen                 Defect #TEN-58
* 20-Jan-2017			Manish					TENJINCG-8
* 21-06-2018			Preeti					T251IT-74
*/
$(document).ready(function(){
	
	$('#progressBar').hide();
	var loop;
	
	$('#tbl-defects').DataTable({
		columnDefs:[
/*changed by  Parveen for  DEfect TEN-58   starts*/
		   /* {width:500, targets:2},
		    {"render":function(data,type,row){
				return data.substr(0,75) + "...";}, targets:2}*/
		    {width:500, targets:3},
		    {"render":function(data,type,row){
				return data.substr(0,75) + "...";}, targets:3}
		    /*changed by  Parveen for  DEfect TEN-58  ends*/
		]
	
	});
	
	var defReviewJson = '';
	
	$('#btnBack').click(function(){
		var callback = $('#callback').val();
		var runId = $('#runid').val();
		/* changed by manish for req TENJINCG-8 on 20-Jan-2017 starts */
		var tcRecId = $('#tcRecId').val();
		/*window.location.href='ResultsServlet?param=run_result&run=' + runId + '&callback=' + callback;*/
		window.location.href='ResultsServlet?param=run_result&run=' + runId + '&callback=' + callback+'&tcRecId='+tcRecId;
		/* changed by manish for req TENJINCG-8 on 20-Jan-2017 ends */
	});
	
	$('#btnSave').click(function(){
		var jsonArray=[];
		var confirmationRequired = false;
		var count =0;
		$('.defect-row').each(function(){
			var drecid = $(this).attr('drecid');
			var posted = $('#def_posted_' + drecid).val();
			/*Added by Preeti for T251IT-74 starts*/
			var action =$('#action_' + drecid).val();
			/*Added by Preeti for T251IT-74 ends*/
			if(posted !='YES'){
				var sevSelection = $('#severity_' + drecid).val();
				/*Added by Preeti for T251IT-74 starts*/
				if(action=="POST"){
				if(sevSelection==-1)
					count++;
				}
				/*Added by Preeti for T251IT-74 ends*/
				var actionSelection = $('#action_' + drecid).val();
				var dttInstance = $('#dtt_instance_' + drecid).val();
				var item={};
				item["drecid"] = drecid;
				item["action"] = actionSelection;
				item["severity"] = sevSelection;
				item["dtt_instance"] = dttInstance;
				jsonArray.push(item);
				
				if(actionSelection == '-1' || actionSelection == 'NOREVIEW'){
					confirmationRequired = true;
				}
			}
		});
		/*Added by Preeti for T251IT-74 starts*/
		if(count>0){
			showMessage("Please select severity for post action.");
			return false;
		}
		else{
		/*Added by Preeti for T251IT-74 ends*/
		defReviewJson = JSON.stringify(jsonArray);
		//alert(jsonString);
		
		if(confirmationRequired){
			$('.modalmask').show();
			$('#def_review_confirmation_dialog').show();
		}else{
			$('.modalmask').show();
			$('#def_review_progress_dialog').show();
			$('#def_review_pr_actions').hide();
			processDefects(defReviewJson);
		}
		}
	});
	
	$('#btnCCancel').click(function(){
		$('.modalmask').hide();
		$('#def_review_confirmation_dialog').hide();
	});
	
	$('#btnPClose').click(function(){
		$('.modalmask').hide();
		$('#def_review_progress_dialog').hide();
		var callback = $('#callback').val();
		var runId = $('#runid').val();
		window.location.href = 'TestRunServlet?param=hydrate_run_defects&runId=' + runId + '&callback=' + callback;
	});
	
	$('#btnCOk').click(function(){
		$('#def_review_confirmation_dialog').hide();
		$('#def_review_progress_dialog').show();
		$('#def_review_pr_actions').hide();
		processDefects(defReviewJson);
	});
});


function processDefects(json){
	$('#processing_icon').html("<img src='images/loading.gif' alt='In Progress..' />");
	$('#progressBar').show();
	$('#def_review_message').html('<p>Tenjin is processing your actions on the defects. Please Wait...</p>')
	
	loop = window.setInterval(function(){
		$.ajax({
			url:'ResultsServlet?param=def_review_progress',
			dataType:'json',
			async:true,
			success:function(data){
				progressBar(data.progress, $('#progressBar'));
			},
			error:function(xhr, textStatus, errorThrown){
				/*Changed by leelaprasad for TJN210-87 starts*/
				/*alert(errorThrown);*/
				console.error(errorThrown);
				console.error(xhr);
				/*Changed by leelaprasad for TJN210-87 ends*/
			}
		});
	}, 500);
	
	$.ajax({
		url:'ResultsServlet?param=review_run_defects&runid=' + $('#runid').val() + '&json=' + json,
		dataType:'json',
		async:true,
		success:function(data){
			if(data.status == 'success'){
				$('#processing_icon').html("<img src='images/success_large.png' alt='Completed' />");
				$('#def_review_message').html('<p>' + data.message + '</p>');
				$('#def_review_pr_actions').show();
				progressBar(100, $('#progressBar'));
				clearInterval(loop);
			}else if(data.status == 'warning'){
				$('#processing_icon').html("<img src='images/warning_large.png' alt='Completed' />");
				$('#def_review_message').html('<p>' + data.message + '</p>');
				$('#def_review_pr_actions').show();
				progressBar(100, $('#progressBar'));
				clearInterval(loop);
			}else if(data.status == 'error'){
				$('#processing_icon').html("<img src='images/error_large.png' alt='Completed' />");
				$('#def_review_message').html('<p>' + data.message + '</p>');
				$('#def_review_pr_actions').show();
				progressBar(100, $('#progressBar'));
				clearInterval(loop);
			}
		}
	});
}

