/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  assistedlearning.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 25-10-2016           Sriram                 Fix for Defect#551
* 05-11-2016             Leelaprasad          Fix  For Defect num#663
* 06-02-2017           Leelaprasad              To fix Excel sheet name should allow numbers
* 28-08-2017           Padmavathi               defect T25IT-326
* 24-10-2017			Sriram					TENJINCG-399
*/
$(document).ready(function(){
	
	//Fix for Defect#551 (Sriram)
	$('#btnBack').click(function(){
		var appId = $('#lstApp').val();
		var funcCode = $('#txtFunc').val();
		/*Changed by Padmavathi starts*/
		var learnStatus =$('#learnstatus').val();
		/*window.location.href='ModuleServlet?param=VIEW_FUNC&paramval=' + funcCode + '&a=' + appId;*/
		//TENJINCG-399
		/*window.location.href='ModuleServlet?param=VIEW_FUNC&paramval=' + funcCode + '&a=' + appId+'&learnStatus='+learnStatus;*/
		window.location.href='FunctionServlet?t=view&key='+ funcCode +'&appId=' + appId ;
		//TENJINCG-399
		/*Changed by Padmavathi ends*/
	});
	//Fix for Defect#551 (Sriram) ends
	$('#txtFunc').attr('disabled',true);
	//selectIdTypes();
	
	$('#btnDownload').click(function(){
		/*Added By padmavathi for T25IT-326 starts*/
		clearMessagesOnElement($('#user-message'));
		/*Added By padmavathi for T25IT-326 ends*/
		var appId = $('#lstApp').val();
		var funcCode = $('#txtFunc').val();
		
		$.ajax({
			url:'AssistedLearningDataServlet',
			data:'param=export&app=' + appId + '&func=' + funcCode,
			dataType:'json',
			async:true,
			success:function(data){
				
				var status =data.status;
				if(status =='SUCCESS'){
					var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
					$("body").append($c);
	                $("#downloadFile").get(0).click();
	                $c.remove();
				}else{
					var message = data.message;
					showMessage(message,'error');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage('A network error occurred. Please try again','error');
			}
		});
	});
	
	
	$('#btnUpload').click(function(){
	/*Added By padmavathi for T25IT-326 starts*/
		clearMessagesOnElement($('#user-message'));
		/*Added By padmavathi for T25IT-326 ends*/
		$('#uploadBlock').show();
	});
});


/* Added by leelaprsad for requirement bug number#663 on 05-11-2016 starts*/ 

function validate()
  {
  
     var allowedFiles = [".xls", ".xlsx"];
     var fileUpload = document.getElementById("txtFile");
     var filename=fileUpload.value.toLowerCase();
     
     var lblError = document.getElementById("lblError");
     /*changed by Leelaprasad to allow numbers in Excel file names starts*/
     /*var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");*/
     var regex = new RegExp(allowedFiles.join('|'));
     /*changed by Leelaprasad to allow numbers in Excel file names ends*/
    
     
     if (!regex.test(filename)) {
         lblError.innerHTML = "Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.";
         return false;
     }
     lblError.innerHTML = "";
     return true;

     return( true );
  }
	


/* Added by leelaprsad for requirement bug number#663 on 05-11-2016 ends*/ 

function selectIdTypes(){
	
	$('.aldIdType').each(function(){
		var seq = $(this).attr('seq');
		var selType = $('#sel_idtype_' + seq).val();
		$(this).children('option').each(function(){
			var val = $(this).val();
			if(val == selType){
				$(this).attr({'selected':true});
			}
		});
	});
	
}