
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: selectiveFieldsInput.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 19-Sep-2016 			prafful					 Newly Added For # TJN_24_09
*/

var appid;
var moduleCode;

$(document).ready(function(){
	
	populateAppId();
	$('#lstApplication').on('change', function () {
		var select = document.getElementById("lstApplication");
		var answer = select.options[select.selectedIndex].value;
		populateModuleCode(answer);
	});
		
	$('#btnSearch').click(function(){
		 appid = $("#lstApplication").val();
		 moduleCode = $("#lstModules").val();
		
		if(appid!=-1&&moduleCode!=-1)
		openModal();

	});
	

	$('.modalmask').click(function(){
		
    	closeModal();
	});
	
});



function populateAppId(){
	
	var jsonObj = fetchAllAuts();
	
	var status = jsonObj.status;
	 clearMessages();
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jArray = jsonObj.auts;
		
		for(i=0;i<jArray.length;i++){
			var aut = jArray[i];
			html = html + "<option value='" + aut.id + "'>" + aut.name + "</option>";
		}
		
		$('#lstApplication').html(html);
		
	}else{
		showMessage(jsonObj.message, 'error');
	}
}


	function populateModuleCode(appId){
		var jsonObj = fetchModulesForApp(appId);
		
		$('#lstModules').html('');
		var status = jsonObj.status;
		if(status== 'SUCCESS'){
			var html = "<option value='-1'>-- Select One --</option>";
			var jarray = jsonObj.modules;
			for(i=0;i<jarray.length;i++){
				var json = jarray[i];
				html = html + "<option value='" + json.code + "'>" + json.code  + " - " + json.name + "</option>";
			}
			
			$('#lstModules').html(html);
			
		}else{
			showMessage(jsonObj.message,'error');
		}
	}

	
	function openModal(){
		$('.subframe > iframe').attr('src',"MetaDataServlet?param=s&param1="+appid+"_"+moduleCode);				
		$('.subframe').width(800).height(500);				
		$('.subframe > iframe').width(800).height(500);				
		$('.modalmask').show();
		$('.subframe').show();
	}
	

	function closeModal()
	{
		$('.modalmask').hide();
		$('.subframe').hide();
		$('#ttd-options-frame').attr('src','');
	}
	
	