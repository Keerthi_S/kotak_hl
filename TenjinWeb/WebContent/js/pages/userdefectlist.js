/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  userautdetails.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              	DESCRIPTION
* 16-11-2016           Manish                 	DTT User Credentials
* 15-12-2016           Leelaprasad            	DEFECT FIX#TEN-62
* 04-Aug-2017		   Roshni					TENJINCG-342
* 28-Aug-2017		   Padmavathi				defect T25IT-326
* 30-08-2017           Manish	                T25IT-284
* 29-05-2019		   Ashiki					V2.8-63
* 07-06-2019		   Ashiki					V2.8-118
* 
*/

$(document).ready(function(){
	
	/*Added by Ashiki for V2.8-118 starts*/
	var message=$('#message').val();
	if(message!=""){
		showMessage(message,'success');
	}
	/*Added by Ashiki for V2.8-118 starts*/
	$('.main_content').show();
	$('.sub_content').hide();
	
	var dataTable = $('#userDefectTable').dataTable({
		   
		   aoColumnDefs : [{
		      orderable : false, aTargets : [0] 
		   }],
		   order : []  
		});

	$('#btnNewUser').click(function(event) {

		/*changed by manish for bug T25IT-284 on 30-Aug-2017 starts*/
		/*$('.main_content').hide();
		$('.ifr_Main').attr('src', 'userdefectdetails.jsp');
		$('.sub_content').show();*/
		/*changed by manish for bug T25IT-284 on 30-Aug-2017 ends*/
		 $.ajax({
				url : 'DefectServlet',
				data : 'param=new_dtt_user',
				async : false,
				dataType : 'json',
				success : function(data) {
					var status = data.status;
				},
			});
		 /*changed by manish for bug T25IT-284 on 30-Aug-2017 starts*/
			window.location.href='userdefectdetails.jsp';
		/*changed by manish for bug T25IT-284 on 30-Aug-2017 ends*/
	});

	$('#chk_all').click(function(e){	
		   if ($("#chk_all").is(':checked')) 
		   {	
		     $(this).closest('table').find('input[type=checkbox]').prop('checked', this.checked);	
		     e.stopPropagation();
		   }
		   else
		    {
			 $(this).closest('table').find('input[type=checkbox]').prop('checked',false);	 
			 e.stopPropagation();
		    }
		}); 
	$('#btnDelete').click(function(){
		/*	 Added by padmavathi for T25IT-326 starts */
		clearMessagesOnElement($('#user-message'));
		/*	 Added by padmavathi for T25IT-326 ends */
		 var count=0;
		  var urllink;
//		 $("#chk_user:checked").each(function () {
//			 count++;
//	      });
		  
		  for(var i=0;i<selectedItems.length;i++ ){
				 count++;
			 }
		  
		 if (count==0) { 
		 /* showMessage("Please select at least one Id to Delete", 'error');	*/
		 
		 /* changed by Roshni for TENJINCG-342 starts */
			 showMessage("Please select at least one user to delete", 'error');
		/* changed by Roshni for TENJINCG-342 ends */	
		 } 
		 else if ($("#chk_all").is(':checked') || count>0) {
			
			 var val =selectedItems;
			 var instance = [];
			 var i=0;
			 $("#chk_user:checked").each(function () {
				 var tmp = $(this).val().split(":");
				 val[i] = tmp[0];
				 instance[i] = tmp[1];			 
				 i++;
			    });
			 var uid = JSON.stringify(val);
			 var instances = JSON.stringify(instance)
	         var userids = JSON.parse(uid);
			 var dttInstances = JSON.parse(instances);
	         urllink='param=delete_DTT_user&userids='+ userids+'&dttInstances='+dttInstances;
	         /*Changed by leeaprasad for the requirement defect fix TEN-62 on 15-12-2016 starts*/
	      //   window.confirm('Are you sure you want to delete?')
	         /*Changed by leeaprasad for the requirement defect fix TEN-62 on 15-12-2016 starts*/
	         
		   }
		  else if ($("#chk_all").is(':checked') || count==1) {
			var checkedvalue=$('input:checkbox:checked').val();
			urllink='param=delete_DTT_user&paramval='+ checkedvalue;
			/*Changed by leeaprasad for the requirement defect fix TEN-62 on 15-12-2016 starts*/
			//window.confirm('Are you sure you want to delete?')
			/*Changed by leeaprasad for the requirement defect fix TEN-62 on 15-12-2016 ends*/
			}
		
		 if(count==1 || count>1){
			 /*Changed by leeaprasad for the requirement defect fix TEN-62 on 15-12-2016 starts*/
			 if(window.confirm('Are you sure you want to delete?')){
				 /*Changed by leeaprasad for the requirement defect fix TEN-62 on 15-12-2016 ends*/
		  $.ajax({
			url:'DefectServlet',
			data :urllink,
			async:false,
			dataType:'json',
			success: function(data){
			var status = data.status;
			if(status == 'SUCCESS')
			  {			
				
				/*commented by Ashiki for V2.8-63 starts*/
				 /*window.setTimeout(function(){window.location.href='DefectServlet?param=fetch_def_user_mapping_data';},500);*/
				/*commented by Ashiki for V2.8-63 ends*/
				/*Added by Ashiki for V2.8-118 starts*/
				/*showMessage(data.message, 'success');*/	
				var message=data.message;
				window.location.href='DefectServlet?param=fetch_def_user_mapping_data&message='+message;
				/*Added by Ashiki for V2.8-118 ends*/
			  }
			  else
			  {
				showMessage(data.message, 'error');				
			  }
		    },
		
		   error:function(xhr, textStatus, errorThrown){
			   showMessage(data.message, 'error');			
		  }
	     });
		  /*Changed by leeaprasad for the requirement defect fix TEN-62 on 15-12-2016 starts*/
		 }
			 /*Changed by leeaprasad for the requirement defect fix TEN-62 on 15-12-2016 ends*/ 
		 }
	    }
	
	);	
});



/*function updateUserMessage(msg, type){
	var $element = $('#user-message-user');
	if(msg == ''){
		clearMessagesOnElement($element);
	}else{
		showMessageOnElement(msg, type, $element);
	}
}*/

