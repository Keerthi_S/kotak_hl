/*Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:uploadtestdata_list.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 


*//******************************************
* CHANGE HISTORY
* ==============*/

$(document).ready(function(){
	$(document).on('click','#download', function(){
		var app=$(this).data("app");
		var func=$(this).data("func");
		var fname=$(this).data("fname");
		url="TestDataUploadServlet";
		reqData = "param=DOWNLOAD&app="+app+"&func="+func+"&fname="+fname;
		$.ajax({
			url:url,
			data:reqData,
			async:true,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
					$("body").append($c);
					$("#downloadFile").get(0).click();
					$c.remove();
				}else{
					var message = data.message;
					showMessage(data.message, 'error');	
				}
			},
			error:function(xhr, textStatus, errorThrown){
				//showMessage(errorThrown, 'error');
			}
		});
	});
});