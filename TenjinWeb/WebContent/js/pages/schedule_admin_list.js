/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_admin_list.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
   31-08-2017           Manish		         back button functionality
* 
*/

/*
 * Added file by sahana for Req#TJN_24_03 on 14/09/2016
 */

$(document).ready(function(){
	
	$('.main_content').show();
	$('.sub_content').hide();
	$('#schTable').dataTable();
	
	$('#btnNewSch').click(function(event) {
		/*changed by manish for back button functionality on 31-Aug-2017 starts*/
		/*$('.main_content').hide();
		$('.ifr_Main').attr('src','schedule_admin_new.jsp');
		$('.sub_content').show();*/
		window.location.href='schedule_admin_new.jsp';
		/*changed by manish for back button functionality on 31-Aug-2017 ends*/
	});
		
	$('#btnDelete').click(function(){
		if (window.confirm('Are you sure you want to delete?'))
		{ 
		 var count=0;
		  var url;
		
		 $("#chk_sch:checked").each(function () {
			 count++;
	      });
		
			
		 if ($("#chk_all").is(':checked') || count>1) {
			 
			 var val = [];
			 var i=0;
			 $("#chk_sch:checked").each(function () {
			 val[i] = $(this).val();
				 i++;
			    });
			 var record = JSON.stringify(val);
	         var schids = JSON.parse(record);
	         url='param=deleteall&paramval='+schids;
		   }
		
		 else if ($("#chk_all").is(':checked') || count==1) {
			
			var checkedvalue=$('input:checkbox:checked').val();
			url='param=delete&paramval='+checkedvalue;
			}
		 else if (count==0) {
			 showMessage("Please select the Schedule Id to Delete", 'error');	
		 }
		
		 if(count==1 || count>1){
					
		  $.ajax({
			url:'SchedulerServlet',
			data :url,
			async:false,
			dataType:'json',
			success: function(data){
			var status = data.status;
			if(status == 'SUCCESS')
			  {			
				showMessage(data.message,'success');	
				 window.setTimeout(function(){window.location='SchedulerServlet?param=FETCH_ALL_ADMIN_SCH';},500);
			  }
			  else
			  {
				showMessage(data.message, 'error');				
			  }
		    },
		
		   error:function(xhr, textStatus, errorThrown){
			   showMessage(data.message, 'error');			
		  }
	     });
		  
		 }
		
	   }
	 });	
	
});

