/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  data_extraction_new.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 09-12-2016           Parveen                 Defect #TEN-6
* 09-12-2016           Parveen                 Defect #TEN-8
* 12-Dec-2016          Leelaprasad             Defect #TEN-11
* 06-02-2017           Leelaprasad             To fix Excel sheet name should allow numbers
* 22-06-2018           Padmavathi              T251IT-125
* 27-5-2019		       Prem					   mandatory field
* 20-11-2019		   Ashiki				   TENJINCG-1166
* 
*/

$(document).ready(function(){
	/*Added by Prem  starts*/
	mandatoryFields('extraction-form');
	/*Added by Prem  ends*/
	//*changes done by parveen for the defect #TEN-6 starts
	$('#btnReset').click(function(){
	window.location.href = 'ExtractorServlet?param=new'
	});
	//*changes done by parveen for the defect #TEN-6 ends
	$('#lstModules').select2();
	
	var scrStatus = $('#screenStatus').val();
	var scrMessage = $('#screenMessage').val();
	if(scrMessage == ''){
		clearMessages();
	}else{
		if(scrStatus == 'SUCCESS'){
			showMessage(scrMessage, 'success');
		}else{
			showMessage(scrMessage,'error');
		}
	}
	
	$('#lstApplication').change(function(){
		var app = $(this).val();
		
		clearMessages();
		if(app == '-1'){
			//$('#moduleInfo').hide();
			$('#lstModules').html("<option value='-1' selected>-- Select One --</option>");
			$('#lstModules').select2('val','-1');
			
			
		}else{
			populateModuleInfo(app);
			//$('#moduleInfo').show();
		}
	});
	
	$('#btnGo').click(function(){
		var appId = $('#lstApplication').val();
		var funcCode = $('#lstModule').val();
		var filePath = $('#txtInputFile').val();
		/*Modified by Padmavathi for T251IT-125 starts*/
		/*if(appId == '-1' || funcCode == '-1' || filePath == ''){
			showMessage('All fields are mandatory','error');
			return false;
		}
		//return false;
*/		
	var formVal = validateForm('extraction-form');
	if(formVal != 'SUCCESS'){
	showMessage(formVal,'error');
	return false;
	}
		});
	/*Modified by Padmavathi for T251IT-125 ends*/
	
});

function populateModuleInfo(appId){
	var jsonObj = fetchModulesForApp(appId);
	
	//*changes done by parveen for the defect #TEN-8 starts
	$('#lstModules').val('-1').change();
	//*changes done by parveen for the defect #TEN-8 ends
	$('#lstModules').html('');
	
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html = "<option value='-1'>-- Select One --</option>";
		var jarray = jsonObj.modules;
		for(i=0;i<jarray.length;i++){
			var json = jarray[i];
			/*Modified by Ashiki for TENJINCG-1166 starts*/
			/*html = html + "<option value='" + json.code + "'>" + json.name + "</option>";*/
			html = html + "<option value='" + json.code + "'>" + escapeHtmlEntities(json.code)  + " - " + escapeHtmlEntities(json.name) + "</option>";
			/*Modified by Ashiki for TENJINCG-1166 ends*/
		}
		
		$('#lstModules').html(html);
		/***************************************
		 * Added by Sriram to support Multipe login types (Tenjin v2.1)
		 */
		//populateAutCreds(appId);
		/***************************************
		 * Added by Sriram to support Multipe login types (Tenjin v2.1) ends
		 */
	}else{
		showMessage(jsonObj.message,'error');
		
		//*changes done by parveen for the defect #TEN-8 starts
		var html = "<option value='-1'>-- Select One --</option>";
		$('#lstModules').html(html);
		$('#lstModules').val('-1').change();
		//*changes done by parveen for the defect #TEN-8 ends
	}
}


/*<!-- Changed by Leelaprasad for the Requirement Defect fix number TEN-11 ON 12-Dec_2016 starts -->*/
function validate()
{

   var allowedFiles = [".xls", ".xlsx"];
   var fileUpload = document.getElementById("txtInputFile");
   var filename=fileUpload.value.toLowerCase();
   
   var lblError = document.getElementById("lblError");
   /*changed by Leelaprasad to allow numbers in Excel file names starts*/
   /*var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");*/
   var regex = new RegExp(allowedFiles.join('|'));
   /*changed by Leelaprasad to allow numbers in Excel file names ends*/
  
   
   if (!regex.test(filename)) {
       lblError.innerHTML = "Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.";
       return false;
   }
   lblError.innerHTML = "";
   return true;

   return( true );
}
/*<!-- Changed by Leelaprasad for the Requirement Defect fix number TEN-11 ON 12-Dec_2016 starts -->*/
