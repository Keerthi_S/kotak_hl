/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: testmanager_list.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 04-Aug-2017			Roshni					TENJINCG-342
* 28-Aug-2017			Padmavathi				defect T25IT-326
* 30-08-2017            Manish	                T25IT-284
* 29-05-2019			Ashiki					V2.8-63
* 07-06-2019			Ashiki					V2.8-118
* 
*/	

/*
 * Added File by Parveen for Requirement TENJINCG_53
 */

$(document).ready(function(){
	/*Added by Ashiki for V2.8-118 starts*/
	var message=$('#message').val();
	if(message!=""){
		showMessage(message,'success');
	}
	/*Added by Ashiki for V2.8-118 ends*/
	
	
	$('.main_content').show();
	$('.sub_content').hide();
	$('#testManagerTable').dataTable();
	
	$('#btnNew').click(function(event) {
		/*changed by manish for bug T25IT-284 on 30-Aug-2017 starts*/
       /* $('.main_content').hide();
		$('.ifr_Main').attr('src','testmanager_new.jsp');
		$('.sub_content').show();*/
		window.location.href='testmanager_new.jsp';
		/*changed by manish for bug T25IT-284 on 30-Aug-2017 ends*/
       });
	
	/*added by shruthi for Tenj210-111 starts*/
	$("#chk_all").click(function(){
		$("input[type=checkbox]").prop('checked', $(this).prop('checked'));   
		});
	/*added by shruthi for Tenj210-111 ends*/
	
	
		
	$('#btnDelete').click(function(){
		/*	 Added by padmavathi for T25IT-326 starts */
		clearMessagesOnElement($('#user-message'));
		/*	 Added by padmavathi for T25IT-326 ends */

          var count=0;
		  var url;
		
		/* $("#chk_def:checked").each(function () {
			 count++;
	      });*/
		  
		  /*for(var i=0;i<selectedItems.length;i++ ){
				 count++;
			 }*/
		  count=selectedItems.length;
		  
		  
     if ($("#chk_all").is(':checked') || count>1) {
			 
			/* var val = selectedItems;
			 var i=0;
			 $("#chk_def:checked").each(function () {
			 val[i] = $(this).val();
				 i++;
			    });*/
			 var record = JSON.stringify(selectedItems);
	         var name = JSON.parse(record);
	         url='param=deleteall&paramval='+name;
		   }
		
		 else if ($("#chk_all").is(':checked') || count==1) {
            var checkedvalue=$('input:checkbox:checked').val();
			url='param=delete&paramval='+checkedvalue;
			}
		 if (count==0) {
		 /* showMessage("Please select the instance to Delete", 'error'); */
		  /* changed by Roshni for TENJINCG-342 starts */
			 showMessage("Please select at least one External TM instance to delete", 'error');	
		 /* changed by Roshni for TENJINCG-342 ends */
		 }
		
		 if(count==1 || count>1){
			  if (window.confirm('Are you sure you want to delete?')){
				  /* Added by Padmavathi for TENJINCG-1101 starts*/
					 var mappedStatus=false;
					  $("#chk_def:checked").each(function () {
						  if($(this).data('status')=='Mapped'){	
							  mappedStatus=true;
							  return false;
						  }
					   });
					  if(mappedStatus){
						  showMessage("Could not delete selected External TM instance(s), some of them are mapped to project.", 'error');
						  return false;
					  }
					  /* Added by Padmavathi for TENJINCG-1101 ends*/
		  
		  $.ajax({
			url:'TestManagerServlet',
			data :url,
			async:false,
			dataType:'json',
			success: function(data){
			var status = data.status;
			if(status == 'SUCCESS')
			  {			
				/*commented by Ashiki for V2.8-63 starts*/
				 /*window.setTimeout(function(){window.location='TestManagerServlet?param=FETCH_ALL';},500);*/
				/*commented by Ashiki for V2.8-63 end*/
				/*Added by Ashiki for V2.8-118 starts*/
				/*showMessage(data.message,'success');*/
				var message=data.message;
				window.location='TestManagerServlet?param=FETCH_ALL&message='+message;
				/*Added by Ashiki for V2.8-118 ends*/
			  }
			  else
			  {
				showMessage(data.message, 'error');				
			  }
		    },
		
		   error:function(xhr, textStatus, errorThrown){
			   showMessage(data.message, 'error');			
		  }
	     });
		 
		 }
	 }
});	
	
});