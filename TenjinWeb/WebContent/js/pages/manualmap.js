/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  manualmap.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 18-Nov-16			nagareddy				   manualmap.js for manual field mapping
* 13-12-2016        Parveen                    defect #TEN-44
* 19-12-2016        nagareddy                  defect #TEN-42
* 14-03-2018        Padmavathi                 TENJINCG-612
* 25-06-2018        Padmavathi                 T251IT-142
* 01-03-2019        Padmavathi                 TENJINCG-996 
* 27-03-2018        Padmavathi                 TENJINCG-1002
* 
*/

$(document).ready(function(){
	/*populateAuts();
	populateRVDs();*/
	/*Added by Padmavathi for TENJINCG-1002 starts*/
	$('#manualmapfield').hide();
	$('#addMap').hide();
	/*Added by Padmavathi for TENJINCG-1002 ends*/
	var selApp = $('#selApp').val();

	var selMod = $('#selModule').val();
	var selType = $('#selType').val();
	var selTxnMode = $('#selTxnMode').val();
	
	$('.rem-res-val').click(function(){
		var stepRecId = $('#txtRecordId').val();
		var rid = $(this).attr('id');
		$.ajax({
			url:'TestCaseServlet',
			data:'param=unmap_res_val&s=' + stepRecId + '&r=' + rid,
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					window.location.href='TestCaseServlet?param=test_step_view&tstep=' + stepRecId;
				}else{
					showMessage(data.message,'error');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
			}
		});
	});

	$('#btnAddVal').click(function(){
		var val = $('#lstAllResVals').val();
		
		if(val !=='-1'){
			addRVD(val);
		}
	});
	
	$('#lstModules').children('option').each(function(){
		var val = $(this).val();
		if(val == selMod){
			$(this).attr({'selected':true});
		}
	});
	
	$('#lstTestType').children('option').each(function(){
		var val = $(this).val();
		if(val == selType){
			$(this).attr({'selected':true});
		}
	});
	
	$('#lstTxnType').children('option').each(function(){
		var val = $(this).val();
		if(val == selTxnMode){
			$(this).attr({'selected':true});
		}
	});
	
	
	/**********************************
	 * Changes for TENJIN V2.3 BEGINS
	 */
	$('#lstModules').change(function(){
		var func = $(this).val();
		var app = $('#lstApplication').val();
		clearMessages();
		
		/*if(func == '-1'){
			$('#lstOperation').html('');
		}else if(func == selMod){
			populateOperations(selApp, selMod, selOperation);
		}else{
			populateOperations(app,func,'');
		}*/
	});
	/**********************************
	 * Changes for TENJIN V2.3 Ends
	 */
	
	/*******************************
	 * Change application name
	 *******************************/
	/*$('#lstApplication').change(function(){
		var app = $(this).val();
		clearMessages();
		if(app == '-1'){
			$('#moduleInfo').hide();
			$('#lstModules').html('');
		}else{
			populateModuleInfo(app);
			$('#moduleInfo').show();
		}
	});*/
	$('#txtStepId').change(function(){
		$('#txtDataId').val($(this).val());
	});
	
	$("#sheet").val("");
	$("#field").val("");
	
	/*Changed by Naga Reddy to Defect fix TEN-42 on 19-12-2016 starts*/
//////unmap the manual field 
	$('#btnUnMapAll').click(function(){
    	///////////////////////////
		 var r = confirm("Do you want to clear all mapping?");
		    if (r == true) {
		
		var json = new Object();
		json.stepid = $('#txtStepId').val();
		json.recid = $('#txtTcRecId').val();		
		json.app = $('#lstApplication').val();
		json.module = $('#lstModules').val();
		json.tcid = $('#txtTcId').val();	
		
		
		/****************************
		 * Fix by Sriram to add AUT Login Type for Test Step (26-02-2015) ends
		 */		
		var jsonString = JSON.stringify(json);
		
		$.ajax({
			url:'TestCaseServlet',
			data:'param=TEST_STEP_UNMAP_MANUAL&json=' + jsonString,
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){					
					showMessage(data.message,'success');
						}else{
					showMessage(data.message,'error');				}
			},error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
			}
		});
		//window.location.reload();
		$("#dTree").jstree("refresh");
		    }
		    else {
		    	
		    }
	});
	/*Changed by Naga Reddy to Defect fix TEN-42 on 19-12-2016 ends*/
	
	$('#btnUnMap').click(function(){
    	///////////////////////////
		
		var unmapsheet=$('#sheetunmap').val();
		var unmapfield=$('#fieldunmap').val();		
		var mapstatus=$('#mappedstatus').val();
		
//		if((mapstatus==='undefined')
//				||(mapstatus===false)
//				||(mapstatus.length==0)){
		if(mapstatus.length==5){
			
			$('#user-message').html('Please select the mapped field').addClass('msg-err').show();	
		}
		else{
			 var r = confirm("Do you want to clear field '"+unmapfield+"' on sheet '"+unmapsheet+"'");
			    if (r == true) {
		var json = new Object();
		json.stepid = $('#txtStepId').val();
		json.recid = $('#txtTcRecId').val();		
		json.app = $('#lstApplication').val();
		json.module = $('#lstModules').val();
		json.tcid = $('#txtTcId').val();	
		
		
		/****************************
		 * Fix by Sriram to add AUT Login Type for Test Step (26-02-2015) ends
		 */		
		var jsonString = JSON.stringify(json);
		
		$.ajax({
			url:'TestCaseServlet',
			data:'param=TEST_STEP_UNMAPSINGLE_MANUAL&json=' + jsonString+'&sheet='+unmapsheet+'&field='+unmapfield,
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){					
					showMessage(data.message,'success');
						}else{
					showMessage(data.message,'error');				}
			},error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
			}
		});
		//window.location.reload();
		$("#dTree").jstree("refresh");
		}else{}
		}
		
		    
	});
	
	$('#btnSave').click(function(){	
		var formVal = validateForm('main_form_manual');
		if(formVal != 'SUCCESS'){
			showMessage(formVal,'error');
			return false;
		}
		//////////////////manual///////////////////////
	
		
		var arrParentIds=[];
		var array = [];
    	
    	$('.manualmapval').each(function(){
    		array.push({
    			"srcSheet":$(this).attr('sourcesheet'),
    			"srcField":$(this).attr('sourcefield'), 	
    			"sourceNodeId":$(this).attr('sourcenodeid')    			
    		});
    		
    		//srcSheet  srcField  targetSheet targetField
    	});
    	if(array.length==0){
    		showMessage('Please map manual input required','error');
    		return false;
    	}
    	var manualStr = JSON.stringify(array);    	
    	//alert(manualStr);
    	///////////////////////////
		var json = new Object();
		json.stepid = $('#txtStepId').val();
		json.recid = $('#txtTcRecId').val();
		/*Modified by Padmavathi for T251IT-142 starts*/
		/*json.app = $('#lstApplication').val();*/
		json.app = $('#selApp').val();
		/*Modified by Padmavathi for T251IT-142 ends*/
		json.module = $('#lstModules').val();
		json.tcid = $('#txtTcId').val();	
		
		
		/****************************
		 * Fix by Sriram to add AUT Login Type for Test Step (26-02-2015) ends
		 */		
		var jsonString = JSON.stringify(json);
	 /*changes done by parveen for the defect #TEN-44 starts
	        	alert(jsonString);
	 changes done by parveen for the defect #TEN-44 ends*/
		$.ajax({
			url:'TestCaseServlet',
			data:'param=TEST_STEP_MANUAL&json=' + jsonString+'&manual='+manualStr,
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					
					showMessage(data.message,'success');
					////////////manual refresh/////
				
					for(var ia=0;ia<array.length;ia++){
						
						var json = array[ia];				
					
					$('#dTree').jstree(true).get_node(json.sourceNodeId).icon='jstree/mapped.png';					
					 // this will reload the children of the node
					$("#dTree").jstree(true).refresh_node(json.sourceNodeId);
					var parent=$('#dTree').jstree(true).get_node(json.sourceNodeId).parent;
					arrParentIds.push(parent);
					}	
		try{
		$("#dataMapped tbody tr").remove();}catch(err){}
		var noDupes = ArrNoDupe(arrParentIds);
		for(var l=0;l<noDupes.length;l++){
		$("#"+noDupes[l]).jstree("refresh");
		}
		/*Added by Padmavathi for TENJINCG-1002 starts*/
		$('#manualmapfield').hide();
		/*Added by Padmavathi for TENJINCG-1002 ends*/
				}else{
					showMessage(data.message,'error');
					
				}
			},error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
				
			}
		});
		
	});
	
	$('#btnBack').click(function(){
		/*Modified by Padmavathi for TENJINCG-612 starts*/
	   /*var tcRecId = $('#txtTcRecId').val();
		window.location.replace("stepdetails_new.jsp");*/
		var tstepRecId = $('#txtRecordId').val();
		window.location.href='TestStepServlet?t=view&key=' + tstepRecId;
		/*Modified by Padmavathi for TENJINCG-612 ends*/
	});
	
	
	////////////////////////// Manual
	
		/**********************
	 * Load Source Tree
	 */
	
	
	displaySheetTree();
	/**********************
	 * JSTree events
	 */
	 /*Added by Padmavathi for TENJINCG-1002 starts*/
	$('#addMap').show();
	/*Added by Padmavathi for TENJINCG-1002 ends*/
	$('#dTree').on('changed.jstree', function(e, data){
		$('#addMap').show();		
		try{
		var nodeType = data.node.li_attr.nodetype;
		var sourceSheet = data.node.li_attr.sourcesheet;
		var sourceField = data.node.li_attr.sourcefield;
		var alreadymapped = data.node.li_attr.mapped;
		try{setTargetSheetAndFieldForUnMap(sourceSheet, sourceField,alreadymapped);}catch(errr){}
		if(alreadymapped===true){
			/*$('#user-message').html('Already mapped');*/
			$('#sheet').val('');
			$('#field').val('');
			$('#addMap').hide();
		}
		
		else {			
				
		setTargetSheetAndField(sourceSheet, sourceField);		
		$('#srcSheet').val(data.node.li_attr.sourcesheet);		
		$('#srcField').val(data.node.li_attr.sourcefield);		
		$('#sFieldNodeId').val(data.node.li_attr.nodeid);	
			
		}
		}catch(err){}
	});
	

	/****************************
	 * Even Handling - Click Add to Map
	 */
	$('#addMap').click(function() {
		
		
		$('#user-message').html('').removeClass('msg-success');
		addSheetField();
		
	})

});

function populateAuts(){
	var jsonObj = fetchAllAuts();
	
	var status = jsonObj.status;
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jArray = jsonObj.auts;
		
		for(i=0;i<jArray.length;i++){
			var aut = jArray[i];
			html = html + "<option value='" + aut.id + "'>" + aut.name + "</option>";
		}
		
		$('#lstApplication').html(html);
		
	}else{
		showMessage(jsonObj.message, 'error');
	}
}

function populateModuleInfo(appId){
	var jsonObj = fetchModulesForApp(appId);
	
	$('#lstModules').html('');
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html = "<option value='-1'>-- Select One --</option>";
		var jarray = jsonObj.modules;
		for(i=0;i<jarray.length;i++){
			var json = jarray[i];
			html = html + "<option value='" + json.code + "'>" + json.name + "</option>";
		}
		
		$('#lstModules').html(html);
	}else{
		showMessage(jsonObj.message,'error');
	}
}

function populateRVDs(){
	var jsonObj;
	
	$.ajax({
		url:'ResultsServlet',
		data:'param=fetch_all_rvds',
		async:false,
		dataType:'json',
		success:function(data){
			var status = data.status;
			if(status == 'SUCCESS'){
				var jArray = data.rvdlist;
				var html = '';
				for(i=0;i<jArray.length;i++){
					var rvd = jArray[i];
					html = html + "<option value='" + rvd.id + "'>" + rvd.name + "</option>";
				}
				
				$('#lstAllResVals').html(html);
			}else{
				showMessage(data.message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown,'error');
		}
	});
}

function addRVD(rid, rname){
	var stepRecId = $('#txtRecordId').val();
	
	$.ajax({
		url:'TestCaseServlet',
		data:'param=map_res_val&s=' + stepRecId + '&r=' + rid,
		async:false,
		dataType:'json',
		success:function(data){
			var status = data.status;
			if(status == 'SUCCESS'){
				var $span = $('<span/>');
				$span.append(data.name);
				
				var $a = $('<a/>');
				$a.attr({'id':rid});
				$a.append('Remove');
				
				var $div = $('<div/>');
				$div.attr({'class':'res-val-detail'});
				$div.append($span);
				$div.append($a);
				
				$('#res-vals').append($div);
			}else{
				showMessage(data.message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown,'error');
		}
	});
}

function populateOperations(appId, funcCode, selOperation){
	var jsonObj;
	$.ajax({
		url:'AutServlet',
		data:'param=fetch_ops_for_module&app=' + appId + '&module=' + funcCode,
		async:true,
		dataType:'json',
		success:function(data){
			jsonObj = data;
			var status = jsonObj.status;
			if(status == 'success'){
				var html = "<option value='-1'>-- Select One --</option>";
				var jarray = jsonObj.operations;
				for(i=0;i<jarray.length;i++){
					var json = jarray[i];
					/*html = html + "<option value='" + json.code + "'>" + json.name + "</option>";*/
					if(json.op == selOperation){
						html = html + "<option value='" + json.op + "' selected>" + json.op  + "</option>";
					}else{
						html = html + "<option value='" + json.op + "'>" + json.op  + "</option>";
					}
					
				}
				$('#lstOperation').html(html);
			}else{
				showMessage(jsonObj.message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown,'error');
		}
	});
}

function removeRVD(rid){
	
}
function displaySheetTree(){
	
	$(".search-input").keyup(function() {

        var searchString = $(this).val();
       // console.log(searchString);
        if(searchString.length>2){
        $('#dTree').jstree('search', searchString);      
        
        }
        if(searchString.length==0){
        	$("#dTree").jstree("close_all");
        	
        }
    });
	 

	
	$('#dTree').jstree({
		'core':{
			'data':{
				'url':'TestCaseServlet',
				'data':function (node) {
					return {'param':'tree','id':node.id};
				},
				'dataType':'json'
				
			},
	
			
		},
		 "plugins" : [ "search" , "dnd"]
	   });

}
	

function setTargetSheetAndField(sheetName, fieldName){
	
			$('#sheet').val(sheetName);
		
			$('#field').val(fieldName);
	
}
function setTargetSheetAndFieldForUnMap(sheetName, fieldName,status){
	
	$('#sheetunmap').val(sheetName);

	$('#fieldunmap').val(fieldName);
	$('#mappedstatus').val(status);

}

function addSheetField() {
	
	
	$('#user-message').html('').removeClass('msg-err');
	var sourceSheet = $('#sheet').val();
	var sourceField = $('#field').val();	
	var sourceNodeId = $('#sFieldNodeId').val();
	
	
	if(sourceSheet==='undefined' && sourceField==='undefined' && sourceNodeId==='undefined'||sourceSheet.length===0||sourceField.length===0 || sourceNodeId.length===0){
		
		
		$('#user-message').html('Please select source field').addClass('msg-err').show();
	}
	else{
	var idrow = sourceSheet + "_" + sourceField;
	var count = $('#dataMapped tr').length;
	
	var flag;
	
	if(count>0){
		
	 flag=getRowFlag(idrow,sourceNodeId);
	
	}
	
	/**/
	if(flag=='SUCCESS'){
		
		$('#user-message').html('Already added to  Map or Duplicate ').addClass('msg-err').show().focus();
		}else{
		/*Added by Padmavathi for TENJINCG-1002 starts*/
			$('#dataMapped').dataTable({
				paging: false,
				sort : false,
				searching:false,
				bInfo : false
			});
			$('.dataTables_empty').remove();
			$('#manualmapfield').show();
			/*Added by Padmavathi for TENJINCG-1002 ends*/
	$("#dataMapped tbody")
			.append(
					"<tr id='"
					+ idrow
					+ "' class='' title='source -> "+sourceSheet+" : "+sourceField+"'>"
					+ "<td class='rowtd'>"
					/*Modified by Padmavathi for TENJINCG-1002 starts*/
					+sourceSheet
					/*Modified by Padmavathi for TENJINCG-996 starts*/
					+"</td>" 
					+ "<td class='rowtd'>"
					+sourceField
					+"</td>" 
					/*Modified by Padmavathi for TENJINCG-1002 ends*/
					+"<td style='text-align:center;'><input type='hidden'  sourcesheet='"+sourceSheet+"' sourcefield='"+sourceField+"' sourcenodeid='"+sourceNodeId+"'  value='"+idrow+"' class='manualmapval'/><input type='button' class='delete btnDelete'  style='width:20px;'/></td>"
					/*Modified by Padmavathi for TENJINCG-996 ends*/
					+ "</tr>");
	$(".btnDelete").bind("click", Delete);
	
	
	}
	
	}
};

function Delete() {
	var par = $(this).parent().parent();
	par.remove();
	$('#user-message').html('').removeClass('msg-err');
	/*Modified by Padmavathi for TENJINCG-1002 starts*/
	var rowCount =$('#dataMapped tbody').find("tr").length;
	if(rowCount==1){
		$('#manualmapfield').hide();
	}
	/*Modified by Padmavathi for TENJINCG-1002 ends*/
}


function getRowFlag(data,nodeId) {
	var showError = false;
	//var showCofirm=false;
	$('#dataMapped tbody').find("tr").each(function(index,r){
		var keyValue = $(this).find("[sourcenodeid]").eq(0).attr("sourcenodeid");
		   if(r.id===data){				   
			   showError=true;	
			   
			  }  		  
		   else  if(keyValue==nodeId){
			   
			   showError=true;
			   
			   }
		  });
	/*if(showError==false && showCofirm==true){
		showError=getConfirmation();
		alert(showError);
	}*/
	 if(showError==true){
		return 'SUCCESS';
	}
	else{
		return 'FAIL';
		}
	}

function ArrNoDupe(a) {
    var temp = {};
    for (var i = 0; i < a.length; i++)
        temp[a[i]] = true;
    var r = [];
    for (var k in temp)
        r.push(k);
    return r;
}



