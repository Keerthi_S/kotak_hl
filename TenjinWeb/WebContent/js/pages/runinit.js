/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  runinit.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 03-Oct-2016		Sriram Sridharan		Rollback TJN_24_11
 * 03-11-2016      Parveen                  Defect #652
 * 10-11-2016      Parveen                  Defect #791
 * 22-Mar-2018     Padmavathi              for navigation of back button
 * 16-Apr-2018	  Pushpalatha				TENJINCG-636
 * 28-May-2018	  Pushpa					TENJINCG-386
 * 08-10-2018      Padmavathi                TENJINCG-840
 * 26-Nov 2018		Shivam	Sharma			TENJINCG-904
 * 25-01-2019       Sahana 			    	pCloudy
 * 09-04-2019		Pushpa					TENJINCG-1027
 * 10-04-2019		Preeti					TENJINCG-1028
 */

$(document).ready(function(){
	/*Added by Padmavathi for TENJINCG-840 starts*/
	$('#lstDevice').hide();
	/*Added by Padmavathi for TENJINCG-840 ends*/

	/*Added by pushpalatha for TENJINCG-386 starts */
	var execMode=$('#executionMode').val();
	var testSetId=$('#txtTsRecId').val();
	var testSetName=$('#txtTestSetName').val();
	var val_mobility=true;
	if(execMode=='GUI'){

		

	}
	
	$.ajax({

		url:'TestSetServletNew',
		data :{t:"validate_mobility",tSetId:testSetId,tSetName:testSetName},
		async:true,
		dataType:'json',
		success: function(data){
			var status = data.status;
			if(status == 'SUCCESS'){		
				val_mobility=data.mobFlag;
				if(val_mobility==false){
					$('#lstDevice').hide();
				}
				/*Added by Padmavathi for TENJINCG-840 starts*/
				else{
					$('#lstDevice').show();
				}
				/*Added by Padmavathi for TENJINCG-840 ends*/
			}
			else{

			}
		},

		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown, 'error');			
		}

	});
	/*Modified by Sahana for pCloudy: Starts*/
	$("#deviceFilter").hide();
	//added by shivam sharma for  TENJINCG-904 starts
	var clientName=$( "#txtclientregistered option:selected" ).text();
	/*Modified by Preeti for TENJINCG-1028 starts*/
	/*if(clientName.includes("Device Farm"))*/
	if(clientName.indexOf("Device Farm") != -1)
	/*Modified by Preeti for TENJINCG-1028 ends*/
	{
		clientName=clientText.replace("[Device Farm]", "").trim();
	}
	var deviceFarmCheck=$("#txtclientregistered").find(':selected').attr('data_deviceFarm');
	var clientIp=$("#txtclientregistered option:selected" ).val();
	$('#client').val(clientName);
	populateDevices(clientIp,deviceFarmCheck,0);

	$('#txtclientregistered').change(function(){
		clientIp=$("#txtclientregistered option:selected" ).val();
		deviceFarmCheck=$("#txtclientregistered").find(':selected').attr('data_deviceFarm');
		if(deviceFarmCheck=='Y'){
			callFilters(clientIp,0);
			$("#deviceFilter").show();
			var clientText=$( "#txtclientregistered option:selected" ).text();
			if(clientText.includes("Device Farm"))
			{
				clientText=clientText.replace("[Device Farm]", "").trim();
			}	
			$('#client').val(clientText);
		}
		else{
			var text=$( "#txtclientregistered option:selected" ).text();
			//clientName=text;
			$('#client').val(text);
			$("#deviceFilter").hide();
			//Changed by prem for issue TJN27-167 start
	           // populateDevices(clientIp,deviceFarmCheck,0);
	            //Changed by akshay for issue# 146 & 149 ends
	           
	        }
	 		populateDevices(clientIp,deviceFarmCheck,0);
	 		//Changed by prem for issue TJN27-167 end
	});
	//added by shivam sharma for  TENJINCG-904 ends

	/*Added by pushpalatha for TENJINCG-386 ends */
	/*Added by Sahana for pCloudy: Starts*/
	$("#btnSearchFilter").click(function() {
		var obj = new Object();
		obj.os=$('#lstOs').val();
		obj.oem=$('#lstOEM').val();
		obj.screenSize=$('#lstScrnSize').val();
		obj.network=$('#lstNetwork').val();
		obj.clientIp=$("#txtclientregistered option:selected" ).val();
		//obj.deviceLocation=$('#lstDevLoc').val();
		var jsonString = JSON.stringify(obj);
		getFilteredDevices(jsonString);
	});
	$("#btnReset").click(function() { 
		var clientIp=$("#txtclientregistered option:selected" ).val();
		var deviceFarmCheck=$("#txtclientregistered").find(':selected').attr('data_deviceFarm');
		callFilters(clientIp,0);
		populateDevices(clientIp,deviceFarmCheck,0);
	});
	/*Added by Sahana for pCloudy: Ends*/

	/*Changed by parveen for Defect #791 starts */
	var dOption = $('#defaultScreenshotOption').val();
	$('#Screen').children('option').each(function(){
		var val = $(this).val();
		if(val == dOption){
			$(this).attr({'selected':true});
		}
	});
	/*Changed by parveen for Defect #791 ends */

	/******************************
	 * Abort - Click
	 ******************************/

	/*	 Changed by parveen for Defect #652 starts */
	$('#btnBack').click(function(){	
		/*Modified by Padmavathi for navigation of back button starts*/
		/*history.back(1);*/
		var callback=$('#callback').val();
		if(callback=='teststep'){
			window.location.href='TestStepServlet?t=view&key='+$('#stepRecId').val();
		}else if(callback=='testcase'){
			window.location.href = 'TestCaseServletNew?t=testCase_View&paramval=' + $('#tcRecId').val();
		}else if((callback=='testset')){
			window.location.href='TestSetServletNew?t=testset_view&paramval='+ $('#txtTsRecId').val();
		}
		/*Modified by Padmavathi for navigation of back button ends*/

	});

	/*	 Changed by parveen for Defect #652 ends */


	/* var bstatus=$('#backStatus').val();
	 var tcid=$('#Id').val();
	 if(bstatus=="TC")
	 window.location.href = 'TestCaseServlet?param=tc_view&paramval= '+tcid;
	 else
	 window.location.href = 'TestSetServlet?param=ts_view&paramval= '+tcid;*/
	$('#btnCancel').click(function(){
		window.location.href='TestSetServlet?param=ts_view&paramval='+ $('#txtTsRecId').val();
	});


	/*<%-- Changes done By Parveen For the Requirement TJN_24_11 starts on 14/09/2016--%>	*/
	//03-Oct-2016		Sriram Sridharan		Rollback TJN_24_11
	/*$("#videoCaptureBlock").hide();
	checkHost();
    $('#txtclientregistered').change(function (){

    	checkHost();

	});*/
	//03-Oct-2016		Sriram Sridharan		Rollback TJN_24_11 ends

	/* <%-- Changes done By Parveen For the Requirement TJN_24_11 ends on 19/09/2016--%>*/
});

function checkHost(){



	var cName = $('#txtclientregistered').val();
	var hostName = '';
	$('#txtclientregistered').children('option').each(function(){
		var val = $(this).val();
		if(cName==val){
			hostName = $(this).attr('host');
			//alert(hostName);
		}
	});

	$.ajax({

		url:'ClientServlet',
		data :{param:"SELECT_CLIENT_HOST",paramval:hostName},
		async:true,
		dataType:'json',
		success: function(data){
			var status = data.status;
			if(status == 'success'){		
				$("#videoCaptureBlock").show();
			}
			else{
				$("#videoCaptureBlock").hide();
			}
		},

		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown, 'error');			
		}
	});
}
/*Modified by Pushpa for TENJINCG-1027 starts*/
$(document).on('change','#txtclientregistered',function(){
	clientIp=$("#txtclientregistered option:selected" ).val();
	deviceFarmCheck=$("#txtclientregistered").find(':selected').attr('data_deviceFarm');
	if(deviceFarmCheck=='Y'){
		callFilters(clientIp,0);
		$("#deviceFilter").show();
		var clientText=$( "#txtclientregistered option:selected" ).text();
		if(clientText.includes("Device Farm"))
		{
			clientText=clientText.replace("[Device Farm]", "").trim();
		}	
		$('#client').val(clientText);
	}
	else{
		var text=$( "#txtclientregistered option:selected" ).text();
		//clientName=text;
		$('#client').val(text);
		$("#deviceFilter").hide();
	}
	populateDevices(clientIp,deviceFarmCheck,0);
	/*Modified by Sahana for pCloudy: ends*/
});
/*Modified by Pushpa for TENJINCG-1027 ends*/
