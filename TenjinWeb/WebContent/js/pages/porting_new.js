/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  porting_new.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              	DESCRIPTION
* 31-Aug-2016			Sriram						Req#TJN_23_28
* 13-Dec-2016           Leelaprasad             	Defect fix#TEN-31
* 14-Dec-2016           Leelaprasad             	Defect fix#TEN-63
* 15-Dec-2016           Leelaprasad             	Defect fix#TEN-80
* 15-Mar-2017           Leelaprasad              	Requirement TENJINCG-142
* 04-jul-2017			Pushpalatha					TENJINCG-266
* 19-02-2019          	Padmavathi                  TENJINCG-931
* 27-05-2019           	Prem						V2.8-73
* 20-11-2019			Pushpalatha					TENJINCG-1166
* 20-05-2020			Priyanka                    TENJINCG-1208
*/
$(document).ready(function(){
	/*Added by Prem for  V2.8-73 starts*/
	mandatoryFields('port-form');
	/*Added by Prem for V2.8-73 starts*/
	$('#lstModulesT').select2();
	$('#lstModules').select2();
	$('#funcLoader').hide();
	$('#funcLoaderT').hide();
	$('#application').change(function(){
		var app = $(this).val();
		clearMessages();
		if(app == '-1'){
			//$('#moduleInfo').hide();
			$('#lstModules').html("<option value='-1' selected>-- Select One --</option>");
			$('#lstModules').select2('val','-1');
		}else{

			/*Added by Priyanka for TENJINCG-1208 starts*/
		    var appName= $(this).find(':selected').attr('data-autname');
		    /*Added by Priyanka for TENJINCG-1208 ends*/
			populateModuleInfo(app, 'lstModules');
			
			/*Added by Priyanka for TENJINCG-1208 starts*/
			$('#appName').val(appName);
			/*Added by Priyanka for TENJINCG-1208 ends*/
			
			
			/*Added by Padmavathi for  TENJINCG-931 starts*/
			$('#applicationT').val(app);
			/*Added by Padmavathi for  TENJINCG-931 ends*/
			//$('#moduleInfo').show();
			/*Added by Priyanka for TENJINCG-1208 starts*/
			var tappname= $('#applicationT').find(':selected').attr('data-tautname');
			$('#appNameT').val(tappname);
			/*Added by Priyanka for TENJINCG-1208 ends*/
		}
	});
	/*Added by Padmavathi for  TENJINCG-931 starts*/
	$('#lstModules').change(function(){
		$('#lstModulesT').select2('val',$(this).val());
	});
	/*Added by Padmavathi for  TENJINCG-931 ends*/
	$('#applicationT').change(function(){
	
		var app = $(this).val();
		clearMessages();
		if(app == '-1'){
			//$('#moduleInfo').hide();
			$('#lstModulesT').html("<option value='-1' selected>-- Select One --</option>");
			$('#lstModulesT').select2('val','-1');
		}else{
			var appNameT= $(this).find(':selected').attr('data-tautname');
		
			populateModuleInfo(app, 'lstModulesT');
			//$('#tautname').val(appNameT);
			$('#appNameT').val(appNameT);
			
			//$('#moduleInfo').show();
		}
	});
	/*Changed by Leelaprasad for the Requirement Defect Fix TEN-31 on 13-12-2016 starts*/
	$('#btnReset').click(function(){
		window.location.href="porting_new.jsp";
	});
	/*Changed by Leelaprasad for the Requirement Defect Fix TEN-31 on 13-12-2016 ends*/
		
	$('#btnGo').click(function(){
		
		
		var validationStatus = validateForm('port-form');
		
		if(validationStatus == 'SUCCESS'){
			return true;
		}else{
			showMessage(validationStatus,'error');
			return false;
		}
		//Added by Priyanka for Tenj211-12 starts
		return validate();
		//Added by Priyanka for Tenj211-12 starts
		
	});
});

/*Changed by Leelaprasad for the requirement TENJINCG_142 on 15-Mar-2017  starts*/
function validate()
{ //validateFiles();
	/*<!--  Changes Done by Pushpalatha For #TENJINCG-266 Starts-->
    validateFiles();
    <!--  Changes Done by Pushpalatha For #TENJINCG-266 ends-->*/
   var allowedFiles = [".xls", ".xlsx"];
   var srcfileUpload = document.getElementById("sFile");
   var tarfileUploadtar = document.getElementById("tFile");
   
   var srcfilename=srcfileUpload.value.toLowerCase();
   var tarfilename=tarfileUploadtar.value.toLowerCase();
   
   var srclblError = document.getElementById("srclblError");
   var tarlblError = document.getElementById("tarlblError");
   
   
   var regex = new RegExp(allowedFiles.join('|'));
    
   if (!regex.test(srcfilename)) {
	   srclblError.innerHTML = "Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.";
	   tarlblError.innerHTML='';
       return false;
   }else{
	   if (!regex.test(tarfilename)) {
		   tarlblError.innerHTML = "Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.";
		   srclblError.innerHTML = ''
	       return false;
	   }else{
		   return true;
	   }
   }
   lblError.innerHTML = "";
   
   return true;

   return( true );
}
/*Changed by Leelaprasad for the requirement TENJINCG-142 on 15-Mar-2017  ends*/
function populateModuleInfo(appId, target){
	var jsonObj;
	//*changes done by Leelaprasad for the defect #TEN-63 on 14-12-2016 starts
	
	/*changes done by Leelaprasad for the defect #TEN-80 on 14-12-2016 starts*/
	/*$('#lstModulesT').val('-1').change();
	$('#lstModules').val('-1').change();*/
	if(target=='lstModulesT'){
		$('#lstModulesT').val('-1').change();
	}
	else{
	$('#lstModules').val('-1').change();
	/*Added by Padmavathi for  TENJINCG-931 starts*/
	$('#lstModulesT').val('-1').change();
	/*Added by Padmavathi for  TENJINCG-931 ends*/
	}
	/*changes done by Leelaprasad for the defect #TEN-80 on 14-12-2016 ends*/


	//*changes done by Leelaprasad for the defect #TEN-63  on 14-12-2016 ends
	$('#funcLoaderT').show();
	$.ajax({
		url:'AutServlet',
		data:'param=fetch_modules_for_app&app=' + appId,
		async:true,
		dataType:'json',
		success:function(data){
			var jsonObj = data;
			var status = jsonObj.status;
			if(status== 'SUCCESS'){
				var html = "<option value='-1'>-- Select One --</option>";
				var jarray = jsonObj.modules;
				for(i=0;i<jarray.length;i++){
					var json = jarray[i];
					/*html = html + "<option value='" + json.code + "'>" + json.name + "</option>";*/
					/*Modified by Pushpa for TENJINCG-1166 starts*/
					html = html + "<option value='" + json.code + "'>" + escapeHtmlEntities(json.code  + " - " + json.name) + "</option>";
					/*Modified by Pushpa for TENJINCG-1166 ends*/
				}
				//alert(html);
				//alert('Target --> ' + target);
				$('#' + target).html(html);
				/*Added by Padmavathi for  TENJINCG-931 starts*/
				if(target!='lstModulesT')
				$('#lstModulesT').html(html);
				/*Added by Padmavathi for  TENJINCG-931 ends*/
				$('#funcLoaderT').hide();
				/***************************************
				 * Added by Sriram to support Multipe login types (Tenjin v2.1)
				 */
				//populateAutCreds(appId);
				/***************************************
				 * Added by Sriram to support Multipe login types (Tenjin v2.1) ends
				 */
			}else{
				$('#funcLoaderT').hide();
				showMessage(jsonObj.message,'error');
				/*changes done by Leelaprasad for the defect #TEN-63 on 14-12-2016 starts*/
				var html = "<option value='-1'>-- Select One --</option>";
				/*changes done by Leelaprasad for the defect #TEN-80 on 14-12-2016 starts*/
				/*$('#lstModulesT').html(html);
				$('#lstModulesT').val('-1').change();
				$('#lstModules').html(html);
				$('#lstModules').val('-1').change();*/
				if(target=='lstModulesT'){
					$('#lstModulesT').html(html);
					$('#lstModulesT').val('-1').change();
				}
				else{
					$('#lstModules').html(html);
					$('#lstModules').val('-1').change();
					/*Added by Padmavathi for  TENJINCG-931 starts*/
					$('#lstModulesT').html(html);
					$('#lstModulesT').val('-1').change();
					/*Added by Padmavathi for  TENJINCG-931 ends*/
				}
				
				/*changes done by Leelaprasad for the defect #TEN-80 on 14-12-2016 ends*/
				
				/*changes done by Leelaprasad for the defect #TEN-63 on 14-12-2016 ends*/
			
			}
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact your System Administrator.";
			$('#funcLoaderT').hide();
		}
	});
	
	/*Changes Done by Pushpalatha For #TENJINCG-266 Starts*/
	function validateFiles(){
		return validate();
		var sfile = document.getElementById("sFile").value;
		var tfile=document.getElementById("tFile").value;
		 var sourceFile =sfile.split('\\');
		 var sCompare=sourceFile[sourceFile.length - 1]
		  var targetFile =tfile.split('\\');
		   var tCompare=targetFile[targetFile.length - 1]
		 /*  alert(sCompare +" <> "+tCompare); */
		if(sCompare===tCompare){
		showMessage('Target file name should not be same as Source','error');
		return false;
		}
		else{
		return true;
		}
		}
	  /*Changes Done by Pushpalatha For #TENJINCG-266 ends*/
}