/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  projectdefects.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */


/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 29-Nov-16				Manish				project defects
* 14-Dec-16				Manish				defect(TEN-59)
* 10-Jan				Manish				TENJINCG-5
* 01-Aug-2017		    Manish				TENJINCG-74,75
* 19-Aug-2017		 	Manish				T25IT-245
* 28-08-2017          	Padmavathi          defect T25IT-326
* 29-08-2017           	Padmavathi          defect T25IT-326
* 30-08-2017			Padmavathi		    T25IT-246,385
* 31-Aug-2017			Roshni				T25IT-416
* 09-Oct-2017		  	Manish				displaying loader
* 19-06-2018			Preeti				T251IT-86
* 25-06-2018            Padmavathi          T251IT-69
* 09-04-2019			Prem				 TNJN27-74
* 23-5-2019				Ashiki				V2.8-47,V2.8-50,V2.8-51,V2.8-61 
* 07-06-2019			Prem				V2.8-81
* 19-11-2020            Priyanka            TENJINCG-1231
*/

var selectedDefects='';
$(document).ready(function(){
	
	/*added by Prem for V2.8-81 starts*/
	$('#project_defects').dataTable();
	/*added by Prem for V2.8-81 Ends*/
	/*changed by manish for req TENJINCG-5 on 10-Jan-2016 starts*/
	var defectsCallback = $('#defectsCallback').val();
	if(defectsCallback=='deletion'){
		$('#user-message').append(showMessage($('#userMessage').val(),'success'));
	}else if(defectsCallback=='fetch_single_defect'){
		$('#user-message').append(showMessage($('#userMessage').val(),'error'));
	}else{
		clearMessages()
	}
		/*clearMessages();*/
	/*changed by manish for req TENJINCG-5 on 10-Jan-2016 ends*/
	$('#project_defects').dataTable({
		/* Modified by Padmavathi for T251IT-69 starts*/
		columnDefs:[{width:150, targets:2},{width:300, targets:3},{width:300, targets:5},{width:300, targets:6},{width:300, targets:8},
			/*Modified by Prem for TNJN27-74 starts */
			{"render":function(data,type,row){
				    	if(data.length < 59){
				    		return data;
				    	}else{
				    		return data.substr(0,58) + "...";
				    	}
				    	/* Modified by Prem for TNJN27-74 ends */
				    }, targets:2},
				    {"render":function(data,type,row){
				    	if(data.length < 11){
				    		return data;
				    	}else{
				    		return data.substr(0,10) + "...";
				    	}
				    
				    }, targets:3},
				    {"render":function(data,type,row){
				    	if(data.length < 11){
				    		return data;
				    	}else{
				    		return data.substr(0,10) + "...";
				    	}
				    
				    }, targets:5},
				    {"render":function(data,type,row){
				    	if(data.length < 11){
				    		return data;
				    	}else{
				    		return data.substr(0,10) + "...";
				    	}
				    
				    }, targets:6},
				   /* {"render":function(data,type,row){
				    	if(data.length < 11){
				    		return data;
				    	}else{
				    		return data.substr(0,10) + "...";
				    	}
				    
				    }, targets:8},*/
				    {
				    	targets:0,
				    	sortable:false
				    }],
				    /* Modified by Padmavathi for T251IT-69 ends*/
				    order:[]
	});
	/*$('#tbl-defects').DataTable({
		columnDefs:[
		    {width:500, targets:2},
		    {"render":function(data,type,row){
				return data.substr(0,75) + "...";}, targets:2}
		]
	
	});*/
	/*Added by Padmavathi for T25IT-326 starts*/
	$("#btnCloseModal").click(function(){
		/*var prjId=$("#prjId").val();
		window.location.href='DefectServlet?param=fetch_project_defects&pid='+prjId+'&callback=fetch';*/
		clearMessages();
	});
	/*Added by Padmavathi for T25IT-326 ends*/
	/*Added by Padmavathi for T25IT-246 starts */
	$('#recordId').keyup(function (){
		var inputVal = $(this).val();
		var regex = new RegExp(/[^0-9]/g);
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			showMessage('Please enter only positive number','error');
			$(this).val('');
		}
	});
	/*Added by Padmavathi for T25IT-246  ends*/
	/* Commented by Ashiki for V2.8-50 starts*/
	/*$('#btnSearch').click(function(){
		Added by Padmavathi for T25IT-246  starts
		clearMessages();
		Added by Padmavathi for T25IT-246 ends
		Modified by Preeti for T251IT-86 starts
		 var recordId = $('#recordId').val();
		 var reporter = $.trim($('#startedby').val());
		 var status = $('#lstStatus').val();
		 var posted = $('#lstPosted').val();
		var functionCode = $.trim($('#txtfunction ').val());
		var tcase = $.trim($('#txttcid').val());
		var tstep = $.trim($('#txttstepid').val());
		Modified by Preeti for T251IT-86 ends
		var appId = $('#txtappliation').val();
		var projectId = $('#prjId').val();
		
		changed by manish for defect TEN-59 on 14-Dec-2016 starts
	
		if(recordId == '' && functionCode  == '' && reporter == '' && status == '-1' && tcase == '' && tstep == ''  && appId =='' && posted=='-1')
		{
			
			showMessage('Please enter filter criteria','error');
			return false;
		}
		
		if(recordId == '' && functionCode  == '' && reporter == '' && status == '' && tcase == '' && tstep == ''  && appId =='' && posted=='')
		{
			
			showMessage('Please enter at least one field to filter your search','error');
			return false;
		}
		Added by Padmavathi for T25IT-246,385 starts 
		var str='-';
		
		if(recordId.indexOf(str)!='-1'){
			showMessage('Please enter positive number only','error');
			return false;
		}
		Added by Roshni for T25IT-416 starts 
		if(recordId == '' ){
			recordId=-1;
		}
		Added by Roshni for T25IT-416 ends 
		if(recordId==0){
		showMessage('Please enter a number which is greater than 0','error');
		return false;
		}
		Added by Padmavathi for T25IT-246,385 ends
		
		changed by manish for defect TEN-59 on 14-Dec-2016 ends
		
		clearMessages();
		
		if(recordId == '' ){
			recordId=0;
		}
		
		
			 
		
		$('#img_report_loader').show();
		
		window.location.href='DefectServlet?param=filter_project_defects&recordId=' + recordId + '&txtfunc=' + txtfunction + '&txtstdby=' + startedby + '&txtresult=' + result + '&txtcase=' + tcase + '&txtstep=' + tstep + '&txtapp=' + appliation + '&txtstdon=' + startedon  + '&txtpid=' + projectid;
		$.ajax({
			url:'DefectServlet',
			data:'param=filter_project_defects&recordId=' + recordId + '&functionCode=' + functionCode + '&reporter=' + reporter + '&status=' + status + '&txtcase=' + tcase + '&txtstep=' + tstep + '&appId=' + appId +'&posted=' +posted +'&projectId='+projectId,
			
			async:false,
			
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					
					window.location.href='projectdefects.jsp';
					
					
					
				}else{
					showMessage(data.message,'error');
				}
				
			},
		});
				 
		
		 
	});*/ 
	/* Commented by Ashiki for V2.8-50 ends*/
	
	$("#recordId").on('change keypress  click keyup paste input',function(e){

		var recordId=	$("#recordId").val().trim();
		if(recordId!="undefined" && recordId!=0 || recordId!='')
		{
			var t= recordId.length;
		}
		else
		{
			t=0;
		}


		if(t >0)
		{
			$(".stdTextBox").prop('disabled', true);
			$(".stdTextBoxNew").prop('disabled', true);
		}
		else if(t==0)
		{
			$(".stdTextBox").prop('disabled', false);
			$(".stdTextBoxNew").prop('disabled', false);
		}
		if (e.which==8){

			if(t >0)
			{
				$(".stdTextBox").prop('disabled', true);
				$(".stdTextBoxNew").prop('disabled', true);
			}
			else if(t==0)
			{
				$(".stdTextBox").prop('disabled', false);
				$(".stdTextBoxNew").prop('disabled', false);
			}

		}

	});
	/*Commented by Padmavathi for removing duplicate code on click on remove button*/
	/*changed by manish for req TENJINCG-5 on 10-Jan-2016 starts*/
	/*$('#btnDelete').click(function(){
			Added By padmavathi for T25IT-326 starts
		clearMessagesOnElement($('#user-message'));
		Added By padmavathi for T25IT-326 ends

		selectedDefects ='';
		$('#project_defects').find('input[type="checkbox"]:checked').each(function () {
			var did = $(this).attr('id');
			
			if(did != 'all_defects'){
				selectedDefects = selectedDefects + did + ",";
			}
			
			
		});
		for(var i=0;i<selectedItems.length;i++ ){
			selectedDefects = selectedDefects + selectedItems[i] + ",";
		}
		
		if(selectedDefects.length <1){
			alert('Please choose at least one defect to Delete');
			return false;
		}
		if (window.confirm('Are you sure you want to delete?')){
			deleteDefects();
		}
		
	});*/
	/*Commented by Padmavathi for removing duplicate code on click on remove button ends*/
	/*changed by manish for req TENJINCG-5 on 10-Jan-2016 ends*/
	
	/*Commented By Ashiki for V2.8-47 starts*/
	/*$('#btnSync').click(function(){
			Added By padmavathi for T25IT-326 starts
		clearMessagesOnElement($('#user-message'));
		Added By padmavathi for T25IT-326 ends

		selectedDefects ='';
		
		$('#project_defects').find('input[type="checkbox"]:checked').each(function () {
			var did = $(this).attr('id');
			
			if(did != 'all_defects'){
				selectedDefects = selectedDefects + did + ",";
			}
			
			
		});
		
		//alert(selectedDefects);
		if(selectedDefects.length <1){
			alert('Please choose at least one defect to Synchronize');
			return false;
		}
		
		$('.modalmask').show();
		$('#def_review_confirmation_dialog').show();
	});*/
	/*Commented By Ashiki for V2.8-47 ends*/
	/* commented by Ashiki for V2.8-61  starts*/
	/*$('#btnCCancel').click(function(){
		$('.modalmask').hide();
		$('#def_review_confirmation_dialog').hide();
	});*/
	
	/*$('#btnCOk').click(function(){
		$('#def_review_confirmation_dialog').hide();
		$('#def_review_progress_dialog').show();
		$('#def_review_pr_actions').hide();
		doSync();
	});*/
	
	/*$('body').on('click','#btnPClose', function(){
		var callback = $(this).attr('callback');
		if(callback === 'undefined'){
			$('.modalmask').hide();
			$('#processing_icon').html("");
			$('#def_review_message').html('')
			$('#def_review_progress_dialog').hide();
		}else{
			window.location.href=callback;
		}
	});*/
	/* commented by Ashiki for V2.8-61  ends*/
	/*added by manish for bug T25IT-245 on 10-Aug-2017 starts*/
	$('#btnCloseModal').click(function(){
		$(".stdTextBox").prop('disabled', false);
		$(".stdTextBoxNew").prop('disabled', false);
	});
	/*added by manish for bug T25IT-245 on 10-Aug-2017 ends*/
	/*added by manish for displaying loader on 09-Oct-17 starts*/
	$('.defectDetails').click(function(){
		$('#dmLoader').show();
	});
	/*added by manish for displaying loader on 09-Oct-17 ends*/
	
});


/*Added By Ashiki for V2.8-47 starts*/
$(document).on("click","#btnSync",function() {
clearMessagesOnElement($('#user-message'));
selectedDefects ='';
$('#project_defects').find('input[type="checkbox"]:checked').each(function () {
	var did = $(this).attr('id');
	
	if(did != 'all_defects'){
		selectedDefects = selectedDefects + did + ",";
	}
});

if(selectedDefects.length <1){
	alert('Please choose at least one defect to Synchronize');
	return false;
}

$('.modalmask').show();
$('#def_review_confirmation_dialog').show();

});
/*Added By Ashiki for V2.8-47 ends*/

/*Added By Ruksar for Tenj210-52 starts*/
$(document).ready(function() {
	  $('#all_defects').click(function() {
	    var checked = this.checked;
	    $('input[type="checkbox"]').each(function() {
	      this.checked = checked;
	    });
	  });
	});
/*Added By Ruksar for Tenj210-52 ends*/

/* Added by Ashiki for V2.8-51 starts*/
$(document).on("click","#btnDelete",function() {
clearMessagesOnElement($('#user-message'));
selectedDefects ='';
$('#project_defects').find('input[type="checkbox"]:checked').each(function () {
var did = $(this).attr('id');

if(did != 'all_defects'){
	selectedDefects = selectedDefects + did + ",";
}
});

if(selectedDefects.length <1){
alert('Please choose at least one defect to Delete');
return false;
}
if (window.confirm('Are you sure you want to delete?')){
deleteDefects();
}

});
/* Added by Ashiki for V2.8-51 ends*/

/* Added by Ashiki for V2.8-50 starts*/
$(document).on("click","#btnSearch",function() {

	clearMessages();
	 var recordId = $('#recordId').val();
	 var reporter = $.trim($('#startedby').val());
	 var status = $('#lstStatus').val();
	 var posted = $('#lstPosted').val();
	var functionCode = $.trim($('#txtfunction ').val());
	var tcase = $.trim($('#txttcid').val());
	var tstep = $.trim($('#txttstepid').val());
	var appId = $('#txtappliation').val();
	var projectId = $('#prjId').val();
	
	if(recordId == '' && functionCode  == '' && reporter == '' && status == '' && tcase == '' && tstep == ''  && appId =='' && posted=='')
	{
		
		showMessage('Please enter at least one field to filter your search','error');
		return false;
	}
	var str='-';
	
	if(recordId.indexOf(str)!='-1'){
		showMessage('Please enter positive number only','error');
		return false;
	}
	if(recordId == '' ){
		recordId=-1;
	}
	if(recordId==0){
	showMessage('Please enter a number which is greater than 0','error');
	return false;
	}
	clearMessages();
	$('#img_report_loader').show();
	
	$.ajax({
		url:'DefectServlet',
		data:'param=filter_project_defects&recordId=' + recordId + '&functionCode=' + functionCode + '&reporter=' + reporter + '&status=' + status + '&txtcase=' + tcase + '&txtstep=' + tstep + '&appId=' + appId +'&posted=' +posted +'&projectId='+projectId,
		
		async:false,
		
		dataType:'json',
		success:function(data){
			var status = data.status;
			if(status == 'SUCCESS'){
				
				window.location.href='projectdefects.jsp';
			}else{
				showMessage(data.message,'error');
			}
			
		},
	});
			 
});
/* Added by Ashiki for V2.8-50 ends*/

	/* Added by Ashiki for V2.8-61  starts*/
	$(document).on("click","#btnCOk",function(){
	$('#def_review_confirmation_dialog').hide();
	$('#def_review_progress_dialog').show();
	$('#def_review_pr_actions').hide();
	doSync();
	});
	
	$(document).on("click","#btnCCancel",function(){
		$('.modalmask').hide();
		$('#def_review_confirmation_dialog').hide();
	});

	
	$(document).on('click','#btnPClose', function(){
		var callback = $(this).attr('callback');
		if(callback === 'undefined'){
			$('.modalmask').hide();
			$('#processing_icon').html("");
			$('#def_review_message').html('')
			$('#def_review_progress_dialog').hide();
		}else{
			window.location.href=callback;
		}
	});
	/* Added by Ashiki for V2.8-61  ends*/

function doSync(){
	
	$('#processing_icon').html("<img src='images/loading.gif' alt='In Progress..' />");
	$('#def_review_message').html('<p>Tenjin is synchronizing defects from External Defect Tracking tools. Please Wait...</p>');
	
	$.ajax({
		url:'DefectServlet?param=sync_multiple_defects&dids=' + selectedDefects,
		async:true,
		dataType:'json',
		success:function(r){
			console.log(r);
			//alert(r.status);
			if(r.status == 'success'){
				$('#processing_icon').html("<img src='images/success_large.png' alt='Completed' />");
				$('#def_review_message').html('<p>Synchronization successful!</p>');
				var callback = 'DefectServlet?param=fetch_project_defects&pid=' + $('#prjId').val();
				$('#btnPClose').attr('callback',callback);
				$('#def_review_pr_actions').show();
			}else if(r.status=='ERROR'){
				$('#processing_icon').html("<img src='images/error_large.png' alt='Completed' />");
				$('#def_review_message').html('<p>' + r.message + '</p>');
				$('#def_review_pr_actions').show();
			}
			/*changed by manish for req TENJINCG-5 on 10-Jan-2016 starts*/
			/*else if(r.status == 'WARN')*/
			 else if(r.status == 'WARN' || r.status == 'warn'){
			/*changed by manish for req TENJINCG-5 on 10-Jan-2016 ends*/ 
				$('#processing_icon').html("<img src='images/warning_large.png' alt='Completed' />");
				/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
				/*$('#def_review_message').html('<p>There were some problems synchronizing all defects. Some defects may not have been synchronized properly. Please check logs and contact Tenjin Support if required.</p>');*/
				var callback = 'DefectServlet?param=fetch_project_defects&pid=' + $('#prjId').val();
				$('#btnPClose').attr('callback',callback);
				$('#def_review_pr_actions').show();
				$('#def_review_message').html("");				
				$('#def_review_message').append('<p>Synchronization successful. But following defects may not have been synchronized properly</p>');
				for(var i=0; i<r.listExceptions.length; i++){
					$('#def_review_message').append('<p>'+r.listExceptions[i]+'</p>');
				}
				/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 ends*/
			}
			$('.subframe_title').html('<p>Synchronization Completed</p>');
		},
		error:function(r){
			$('#processing_icon').html("<img src='images/error_large.png' alt='Completed' />");
			$('#def_review_message').html('<p>An internal error occurred. Please contact Tenjin Support.</p>')
		}
	});
}

/*changed by manish for req TENJINCG-5 on 10-Jan-2016 starts*/
function deleteDefects(){
	var prjId = $('#prjId').val();
	var callback="deletion";
	$.ajax({
		url:'DefectServlet?param=inactive_multiple_defects&dids=' + selectedDefects+'&prjId='+prjId,
		async:true,
		dataType:'json',
		success:function(r){
			console.log(r);
			if(r.status == 'success'){
				window.location.href='DefectServlet?param=fetch_project_defects&pid='+prjId+'&callback='+callback;
				//showMessage(r.message,'success');
				/*location.reload();*/
			}else{
				window.location.href='DefectServlet?param=fetch_project_defects&pid='+prjId+'&callback='+callback;
				//showMessage(r.message,'error');
				/*location.reload();*/
			}
		},
		error:function(r){
			showMessage(r.message,'error');
		}
		
	});
}
/* Added by Priyanka for TENJINCG-1231 and TCGST-44 starts*/
const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
var current_datetime = new Date();
var formatted = current_datetime.getDate() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getFullYear();
   var projectEndDate=$('#endDate').val();   
	  var tdyDate = Date.parse(formatted);
	  var prjEndDate  = Date.parse(projectEndDate);
	  if(prjEndDate>=tdyDate){
		  
		  $('#btnfilter').show();
		  $('#btnSync').show();
		  $('#allDefects').show(); 
		  $('#btnDelete').show();
		  $('#dmLoader').hide();
		 
	  }
	  else{
		  $('#btnfilter').hide();
		  $('#btnSync').hide();
		  $('#allDefects').hide();
		  $('#btnDelete').show();
		  $('#dmLoader').hide();
		  
		  
		  
	  }
	  /* Added by Priyanka for TENJINCG-1231 and TCGST-44 ends*/

/*changed by manish for req TENJINCG-5 on 10-Jan-2016 ends*/ 
