/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  defect_new.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              	DESCRIPTION
* 01-Sept-2017         Gangadhar Badagi        	T25IT-432
* 04-Sept-2017         Gangadhar Badagi        	T25IT-432
* 25-Mar-2019		   Pushpalatha			   	TENJINCG-968
* 28-05-2019		   Ashiki				   	V2.8-63
* 30-09-2019			Preeti					For defect posting in JIRA
* 09-04-2020           shruthi                	TENJINCG-1142
* 08-12-2020		   Pushpa					TENJINCG-1220
*/	
/*
 * Added File by sahana for Requirement TJN_24_06
 */

$(document).ready(function(){
	
	/*added by shruthi for TENJINCG-1264 starts*/
	$('.passwordBlock').hide();
	$('.apikeyBlock').hide();
	/*added by shruthi for TENJINCG-1264 ends*/

	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('new_defect');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	/*Added by Pushpalatha for TENJINCG-1220 starts*/
	$('#gitOrganizationBlock').hide();
	/*Added by Pushpalatha for TENJINCG-1220 ends*/
	/*Changed by Gangadhar Badagi for T25IT-432 starts*/
	/*Added by Gangadhar Badagi for T25IT-432 starts*/
	/*$('#txtAdmin').val("");
	$('#txtPwd').val("");*/
	 /*document.getElementById("txtAdmin").value = "";*/
	 /*document.getElementById("txtPwd").value = "";*/
	$('#txtAdmin').val('  ');
	/*Added by Gangadhar Badagi for T25IT-432 ends*/
	 /*Changed by Gangadhar Badagi for T25IT-432 ends*/
	$('#btnCancel').click(function(){	
		window.location.href='DefectServlet?param=FETCH_ALL';	
	});
	populateDttTool();
	$('#txtName').change(function (){
		   var name=$('#txtName').val();
				$.ajax({
					url:'DefectServlet',
					data :{param:"CHECK_NAME",paramval:name},
					async:false,
					dataType:'json',
					success: function(data){
					var status = data.status;
					if(status == 'ERROR'){		
						showMessage(data.message, 'error');
						$("#txtName").focus();
					}
				},
				
				error:function(xhr, textStatus, errorThrown){
					showMessage(errorThrown, 'error');			
				}
			});
			}); 
	

	

	 	
	$('#btnSave').click(function(){
		
		
		/*added by shruthi for TENJINCG-1264 starts*/
		var tool =$('#txtTool').val();
		if(tool=='jira' || tool=='mantis' || tool=='github' || tool=='rally')
		{
		$("#txtapikey").attr('mandatory','yes');
		$("#txtPwd").removeAttr('mandatory');
	    var password = encodeURIComponent($('#txtapikey').val());
		}
	else
		{
		$("#txtPwd").attr('mandatory','yes');
		$("#txtapikey").removeAttr('mandatory');
		var password = encodeURIComponent($('#txtPwd').val());
		}
		
		/*added by shruthi for TENJINCG-1264 ends*/
		var validated = validateForm('new_defect');
		/*Added by shruthi for TENJINCG-1142 starts*/	
		var url = $("#txtUrl").val()
        var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        if (!pattern.test(url)) {
            showMessage("Please enter valid URI", 'error');
            return false;
        }
        /*Added by shruthi for TENJINCG-1142 ends*/
		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
		}
		
		var obj = new Object();
		
		obj.name = $('#txtName').val();
		obj.tool = $('#txtTool').val();
		obj.url = $('#txtUrl').val();
		obj.admin = $('#txtAdmin').val();
		/*Modified by Preeti for defect posting in JIRA starts*/
		/*obj.password = $('#txtPwd').val();*/
		/*var password = encodeURIComponent($('#txtPwd').val());*/
		/*Modified by Preeti for defect posting in JIRA ends*/
		obj.Severity= $('#svrty').val();
		obj.Priority= $('#prty').val();
		/*Added by Pushpalatha for TENJINCG-1220 starts*/
		obj.Organization=$('#txtOrg').val();
		/*Added by Pushpalatha for TENJINCG-1220 ends*/
		
		var jsonString = JSON.stringify(obj);
		var csrf_token=$('#csrftoken_form').val();
		$.ajax({
			type:'post',
			url:'DefectServlet',
			/*Modified by Preeti for defect posting in JIRA starts*/
			/*data:{param:"NEW_DEF",json:jsonString},*/
			/*modified by paneendra for Tenj210-171 starts*/
			/*data:'param=NEW_DEF&json='+jsonString+'&password='+password,*/
			data:{param:"NEW_DEF",json:jsonString,password:password,csrftoken_form:csrf_token},
			/*modified by paneendra for Tenj210-171 ends*/
			/*Modified by Preeti for defect posting in JIRA ends*/
			dataType:'json',
			success:function(data){
				if(data.status == 'success'){
					showMessage(data.message,'success');
					/*Commented by Ashiki for V2.8-63 starts*/
					/*window.setTimeout(function(){window.location='DefectServlet?param=FETCH_ALL';},500)*/
					/*Commented by Ashiki for V2.8-63 ends*/
					
				}else{
					showMessage(data.message,'error');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
			}
		});
	});
	/*Added by Pushpalatha for TENJINCG-1220 starts*/
	$('#txtTool').change(function(){
		var tool=$('#txtTool').val();
		/*added by shruthi for TENJINCG-1264 starts*/
		if(tool=='jira' || tool=='mantis' || tool=='github' || tool=='rally')
			{
			$('.passwordBlock').hide();
			$('.apikeyBlock').show();
			}
		else
			{
			$('.passwordBlock').show();
			$('.apikeyBlock').hide();
			}
		/*added by shruthi for TENJINCG-1264 ends*/
		
		
		if(tool=='github'){
			$('#gitOrganizationBlock').show();
		}else{
			$('#gitOrganizationBlock').hide();
		}
		/*Added by Prem for TENJINCG-1234 Starts*/
		if(tool=='mantis'){
			document.getElementsByName('svrty')[0].placeholder='';
			document.getElementsByName('svrty')[0].placeholder='block,crash,major,minor';
			document.getElementsByName('prty')[0].placeholder='';
			document.getElementsByName('prty')[0].placeholder='high,normal,low';
		}
		/*else{
			document.getElementsByName('svrty')[0].placeholder="Blocker,Critical,Major,Minor"
			document.getElementsByName('prty')[0].placeholder='High,Medium,Low';
		}*/
		/*Added by Paneendra for TENJINCG-1260 Starts*/
		else if(tool=='rally'){
			document.getElementsByName('svrty')[0].placeholder='';
			document.getElementsByName('svrty')[0].placeholder='None,Crash/Data Loss,Major Problem,Minor Problem,Cosmetic';
			document.getElementsByName('prty')[0].placeholder='';
			document.getElementsByName('prty')[0].placeholder='None,Resolve Immediately,High Attention,Low';
			}
			else{
			document.getElementsByName('svrty')[0].placeholder="Blocker,Critical,Major,Minor"
			document.getElementsByName('prty')[0].placeholder='High,Medium,Low';
			}
		/*Added by Paneendra for TENJINCG-1260 Ends*/
		/*Added by Prem for TENJINCG-1234 Ends*/
	});
	/*Added by Pushpalatha for TENJINCG-1220 ends*/
	
	
});
function populateDttTool(){
	var jsonObj=dttTool();
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html = "<option value='-1'>-- Select One --</option>";
		var jarray = jsonObj.tools;
		for(i=0;i<jarray.length;i++){
			var json = jarray[i];
			
			html = html + "<option value='" + json.dttname + "'>" + json.dttname + "</option>";
		}
		
		$('#txtTool').html(html);
		
	}else{
		showMessage(jsonObj.message,'error');
	}
}

function dttTool(){
	var jsonObj;
	$.ajax({
		url:'DefectServlet',
		data:'param=DTTtool',
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});
	
	return jsonObj;
}

	
	