/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  report_aut_defect.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 * DATE             CHANGED BY                 DESCRIPTION
 * 16-12-2016       Leelaprasad                Defect TEN-17
 * 
 */
$(document).ready(function() {
	$('#lstApplication').change(function(){
		clearMessages();
	});
	
	$('#gen_report').click(function(){
		var appId = $('#lstApplication').val();
		if(appId == '-1'){
			showMessage("Please select Application", "Error");
		}else{
		$.ajax({
			url:'ReportServlet',
			data :'param=gen_aut_defect_report'+ '&app_id=' + appId,
			async:false,
			dataType:'json',
			success: function(data){
				var status = data.STATUS;
			if(status == 'SUCCESS'){
				var body='';
				var headerFlag = true;
				var json = data.finaljson;
				var reportArray = json.report;
				var severityJson = json.Severity;
				var tempArray = [];
				for(var i in severityJson){
					tempArray.push(i);
				}
				
				for(var i = 0; i<reportArray.length; i++){ //Get the status
					var statusJson = reportArray[i];	
					var severityArray = statusJson.ServerityArray;
					var isRowCreated = true;
					var rowEndTag = false;
					for(var k = 1; k <= tempArray.length; k++){ //Get the severity under the status 
						if(headerFlag){
							body += '<tr><th>Application</th><th>Status</th>';
							for(var l = 1; l <=tempArray.length ; l++){
								body += '<th>'+severityJson['S'+l]+'('+'S'+l+')</th>';
							}
							body += '</tr>';
							headerFlag = false;
						}
						if(isRowCreated){
							body += '<tr><td><b>'+statusJson.AppName+'</b></td><td><b>'+statusJson.StatusName+'</b></td>';
							isRowCreated = false;
						}
						
						var severityValue = severityJson['S'+k];
						var reportArray1 = severityArray[severityValue];
						var StatusName = statusJson.StatusName;
						//
						/*if(reportArray1.length == 0){
						body += '<td>'+reportArray1.length+'</td>';
						}else{
							body += '<td><a href=>'+reportArray1.length+'</a></td>';
						}*/
						
						if(reportArray1.length == 0){
							body += '<td><b>'+reportArray1.length+'</b></td>'
						}else{
							var reportId = '';
						for(var j = 0; j<reportArray1.length; j++){
							var report1 = reportArray1[j];
							reportId += report1.Defect_Id+',';
							
						}
						body += '<td><b><a href=DefectServlet?param=fetch_list_of_defect&ids='+reportId.substring(0,reportId.length-1)+'>'+reportArray1.length+'</a></b></td>';
					}
						if(k==tempArray.length){
							body += '</tr>';
						}
					}
				}
				var summaryTable = '';
				if(reportArray.length > 0){
					summaryTable = '<table id="tblReportSummary" class="bordered" cellspacing="0" width="100%">'+body+'</table>';	
				}else{
					summaryTable = 'No Records to display.';
				}
				$('#reportSummary').html(summaryTable);
			  }else{
				showMessage(data.MESSAGE, "Error");
			  }
		    },
		   error:function(xhr, textStatus, errorThrown){
			   showMessage(textStatus, 'error');			
		  }
	     });
		}
	});
	
	$('#btnReset').click(function(){
		$('#lstApplication').val(-1);
		$('#reportSummary').html('');
		/*Changed by Leelaprasad for the requirement defect fix#TEN-17 on 16-12-2016 starts*/
		$('.msg-err').hide();
		/*Changed by Leelaprasad for the requirement defect fix#TEN-17 on 16-12-2016 ends*/
		
	});
	
	$('#btnDownload').click(function(){
		var table = document.getElementById('tblReportSummary');
		
		//var tableRows = table.getElementsByTagName('tr');
		if(table != null){
			var headers ='';
			var tableRows = table.getElementsByTagName('tr');
			for(var i = 0; i < tableRows.length; i++){
				var rowData = tableRows[i].getElementsByTagName('th');
				if(rowData.length > 0){
					for(j = 0; j < rowData.length; j++){
						headers += rowData[j].innerText+',';
					}
				}
			}
			
			var json = $("#tblReportSummary").tableToJSON();
			var jstring = JSON.stringify(json);
			alert('a');
			$.ajax({
				url : 'ReportServlet',
				data : {'param' : 'dwld_report', 'header' :  headers.substring(0,headers.length-1), 'tableJson' : jstring},
				dataType : 'json',
				success : function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
						$("body").append($c);
						$("#downloadFile").get(0).click();
						$c.remove();
					}else{
						showMessage(data.message, "error");
					}
				}
			});
			
		}else{
			showMessage("Generate the report to download", "Error");
		}
	});
});