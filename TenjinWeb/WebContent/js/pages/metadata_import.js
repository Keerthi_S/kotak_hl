/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  metadata_import.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY     			DESCRIPTION
* 30/08/2016			Avinash					Changes 30-08-2016 - Tenjin v.2.3 Enhancement on Metadata
*/
$(document).ready(function(){
	 $('.application_mask').hide();
	 $('.function_mask').show();
	 $('.progress_mask').hide();
	 $('.loader_mask').hide();
	 var len = $('#lstApplication').find('option').not(':first').length;
	 if(len == 0){
		 $('#lstApplication').prop('disabled', true);
	 }else{
		 $('#lstApplication option:eq(1)').prop('selected', 'selected');
	 }
//	 $('#messageText').css('background-color','blue');
	/* var messagelen = $('#messageText').val().length;
	 var messsageText = $('#messageText').val();
	 if(messagelen != 0 && $('#messageText').val() != "null" && $('#messageText').val() != "undefined" && $('#messageText').val() != undefined) {
		 if( messsageText.indexOf('Error') >= 0 ){
			 showMessage($('#messageText').val(),'error');
		 }else if(messsageText.indexOf('WARNING') >= 0){
			 showMessage($('#messageText').val(),'error');
		 }else{
		 showMessage($('#messageText').val(), "success");
		 }
	 }*/
	
	 $('#btnBack').click(function(){
			window.location.href='autfunclist_2.jsp'; 
		 });
	 
	 var errorMessage = $('#error-msg-text').val();
		if(errorMessage == 'noerror'){
		}else{
			showMessage(errorMessage,'error');
		}
	var successMessage = $('#success-msg-text').val();
		if(successMessage == 'nomsg'){
		}else{
			if(successMessage.indexOf('WARNING') >= 0){
				showMessage(successMessage,'error');
			}else{
				showMessage(successMessage,'success');
			}
		}
	 
	 var fileNameLen = $('#txtFileName').val().length;
	 if(fileNameLen == 0) {
		 $('#btnUpload').prop('disabled', true);
	 }
	 
	 $('#txtFile').change(function(){
		 $('#main_form').submit();
	 });
	 
	 $('#btnUpload').click(function(){
		window.location.href = 'MetaDataUploadServlet?param=upload'; 
	 });
	 
	 $('#btnClear').click(function() {
		window.location.href = 'MetaDataUploadServlet?param=clear';
	});
	 
	 $('#radio_application').change(function(){
		  if($('#radio_application').is(':checked')){
			  $('#user-message').hide();
			  $('.application_mask').show();
			  $('.function_mask').hide();
			  $('#tblMetadataModules').find('tr').not(':first').remove();
			  //clearModuleCheckbox();
		  }
	  });
	  
	  $('#radio_function').change(function(){
		  if($('#radio_function').is(':checked')){
			  $('#user-message').hide();
			  $('#tblMetadataModule').find('tr').not(':first').remove();
			  $('.function_mask').show();
			  $('.application_mask').hide();
			  clearAppCheckbox();
		  }
	  });
	  
	  $('#audit').click(function(){
			 window.location.href = "MetaDataServlet?param=audit&type=IMPORT";
		  });
	  
	  $('#import').click(function(){

			$('#txtcounter').val(0);
			$('#txttotal').val(0);
			
			progressBar(0, $('#progressBar'));
		  var retValue = dataToBeExport();
		  toImportData(retValue, 'check', true);
		});
	  
	  function toImportData(retValue, importOrCheck, flag){
		  if(retValue != undefined && flag == false){
			  $('.progress_mask').show();
		  }else if(retValue != undefined && flag == true){
			  $('.loader_mask').show();
		  }
		  window.setTimeout(function(){
		  if(retValue != undefined){
		  var appName = $('#lstApplication').val();
		  $.ajax({
				url:'MetaDataUploadServlet',
				data :'param=import&data='+retValue+'&appName='+appName+'&importorcheck='+importOrCheck,
				async:true,
				dataType:'json',
				success:function(data){
					var status = data.status;
					var url = data.url;
					if(url != ""){
					window.location.href = url;
					}
					else if(status == 'SUCCESS'){
						$('.progress_mask').hide();
						window.clearInterval(interval);
						if(flag){
						var confirmVal = confirm("Are you sure you want to proceed with the Import? This action cannot be undone.");
						//var confirmVal = appConfirmation();
						if(confirmVal){
							$('.loader_mask').hide();
						toImportData(retValue,'import', false)
						}else{
							$('.loader_mask').hide();
						}
						}else{
							showMessage(data.message, 'success');	
						}
					}else{
						var message = data.message;
						
						showMessage(message,'error');
						$('.progress_mask').hide();
						$('.loader_mask').hide();
					}
				},
				error:function(xhr, textStatus, errorThrown){
					$('.progress_mask').hide();
					showMessage(errorThrown, 'error');
				}
		     });
		  
		  var interval = window.setInterval(function(){
				$.ajax({
					url : "MetaDataUploadServlet",
					type:"GET",
					async : true,
					data : {param : "progress_bar"},
					dataType:'json',
					success : function(data) {
						if(data.status == 'success'){
							var total = data.total;
							var counter = data.counter;
							var percentage = 0;
							if(counter == 0){
								percentage = 1;
							}else{
								percentage = ((counter/total)*100).toFixed(0);
							}
							
							$('#txtcounter').val(counter+1);
							$('#txttotal').val(total);
							
							progressBar(percentage, $('#progressBar'));
						}				
					},

					error : function(xhr, textStatus, errorThrown) {
						$('.progress_mask').hide();
						showMessage(errorThrown, 'error');
					}
				});	
			}, 1000);
		  }
		  else if($('#btnUpload').is(':disabled')){
			  showMessage('Select the file to Import');
		  }else if($('#lstApplication').is(':disabled')){
			  showMessage('Upload the selected file');
		  }else{
			  showMessage('Select any one Function');
		  }
	  },1000);

	  }
	  
	  function dataToBeExport(){
		  var retValue;
		  if($('.application_mask').is(":visible")){
			  var temp;
			  var selectedApps = '';
			  retValue = 'app;'
			  if($('#chk_all_apps').is(':checked')){
				  selectedApps += 'allAut;';
			  }else{
				  selectedApps += 'selected_auts;';
				  temp = selectedApps;
				  $("input:checkbox[name=appName]:checked").each(function () {
					selectedApps += $(this).val() + ';';
				});
			  }
			  if(temp == selectedApps){
				  retValue = undefined;
			  }else{
			  retValue += selectedApps;
			  }
		  }else if($('.function_mask').is(':visible')){
			  var temp;
			  var selectedModules = '';
			  retValue = 'func;';
			  if($('#chk_all_modules').is(':checked')){
				var totcount = 0;
				var checked = 0;
					  $('input:checkbox[name="fcode"]').each(function(){
							totcount++;
							var checkBoxVal = (this.checked ? $(this).val() : "");
							if(checkBoxVal!=""){
								checked++;
							}
						});
					  
					  if(checked != totcount){
						  selectedModules += 'gfunc;';
						  temp = selectedModules;
						  $("input:checkbox[name=fcode]:checked").each(function () {
								 selectedModules += $(this).val() + ';';
							    });
					  }else if(checked == totcount){
						  retValue = 'app;';
						  selectedModules = 'selected_auts;'+$('#lstApplication').val()+';';
					  }
			  }else{
				  selectedModules += 'gfunc;';
				  temp = selectedModules;
				  $("input:checkbox[name=fcode]:checked").each(function () {
						 selectedModules += $(this).val() + ';';
					    });
			  }
			  if(temp == selectedModules){
				  retValue = undefined;
			  }else{
			  retValue += selectedModules;
			  }
		  }
		  return retValue;
	  }
	  
	  function appConfirmation(){
		  var confirmValue = false;
				//While creating new Project, auto generation of FolderType should be generated and disabled. By Nagababu,Badal.
				$('.subframe > iframe').attr('src','metadata_confirmation.jsp');
				/*********************************************
				Fix for Bug 121 in Tenjin Demo Pack by Sriram Sridharan
				****************/
				/* $('.subframe').width(350).height(300); */
				$('.subframe').width(400).height(180);
				/*********************************************
				Fix for Bug 121 in Tenjin Demo Pack by Sriram Sridharan ends
				****************/
				
				/*********************************************
				Fix for Bug 121 in Tenjin Demo Pack by Sriram Sridharan
				****************/
				/* $('.subframe > iframe').width(350).height(300); */
				$('.subframe > iframe').width(400).height(180);
				/*********************************************
				Fix for Bug 121 in Tenjin Demo Pack by Sriram Sridharan ends
				****************/
				$('.modalmask').show();
				$('.subframe').show();
				
				var win = document.getElementsByName('frame2').contentWindow;
				var value1 = win.confirmVal++
				alert(value1);
				
				
	  }
	  
	  $('input:checkbox[name="appName"]').click(function(){
			 var totcount = 0;
			 var checked = 0;
			  $('input:checkbox[name="appName"]').each(function(){
					totcount++;
					var checkBoxVal = (this.checked ? $(this).val() : "");
					if(checkBoxVal!=""){
						checked++;
					}
				});
			  
			  if(checked != totcount){
				  if($('input:checkbox[id="chk_all_apps"]').is(':checked')){
					  $('input:checkbox[id="chk_all_apps"]').prop('checked', false);
				  }
			  }else if(checked == totcount){
					  $('input:checkbox[id="chk_all_apps"]').prop('checked', true);
			  }
		  });
	  
	  $('input:checkbox[id="chk_all_apps"]').change(function(){
		  if($('input:checkbox[id="chk_all_apps"]').is(':checked')){
			  $('input:checkbox[name="appName"]').each(function(){
				  if($(this).is(':checked')){
					  
				  }else{
					  $(this).prop('checked', true);  
				  }
			  });
		  }
		  else{
			  $('input:checkbox[name="appName"]').each(function(){
				  if($(this).is(':checked')){
					  $(this).attr('checked', false);
				  }else{
					    
				  }
			  });
		  }
	  });
	  
	  $('input:checkbox[name="fcode"]').change(function(){
		  	$('.function_mask').show();
			 var totcount = 0;
			 var checked = 0;
			  $('input:checkbox[name="fcode"]').each(function(){
					totcount++;
					var checkBoxVal = (this.checked ? $(this).val() : "");
					if(checkBoxVal!=""){
						checked++;
					}
				});
			  if(checked == 0){
				  checked = 1;
			  }
			  if(checked != totcount){
				  if($('input:checkbox[id="chk_all_modules"]').is(':checked')){
					  $('input:checkbox[id="chk_all_modules"]').prop('checked', false);
				  }
			  }else if(checked == totcount){
					  $('input:checkbox[id="chk_all_modules"]').prop('checked', true);
			  }
		  });
	  
	  $('input:checkbox[id="chk_all_modules"]').change(function(){
		  if($('input:checkbox[id="chk_all_modules"]').is(':checked')){
			  $('input:checkbox[name="fcode"]').each(function(){
				  if($(this).is(':checked')){
					  
				  }else{
					  $(this).prop('checked', true);  
				  }
			  });
		  }
		  else{
			  $('input:checkbox[name="fcode"]').each(function(){
				  if($(this).is(':checked')){
					  $(this).attr('checked', false);
				  }else{
					    
				  }
			  });
		  }
	  });
	  
		$('#lstApplication').change(function(){
			var selApp = $(this).val();
			forApplicationChange(selApp, false);
		});
	  
		if($('#lstApplication option:eq(1)').is(':selected')){
			var selApp = $('#lstApplication option:eq(1)').val();
			forApplicationChange(selApp, true);
		}
		
	  function forApplicationChange(selApp, isOnLoad){
		  	if($('input:checkbox[id=chk_all_modules]').is(':checked')){
			$('input:checkbox[id=chk_all_modules]').prop('checked', false);
			}
	$('#user-message').hide();
	if(selApp == '-1'){
		
	}else{
		$('#tblMetadataModules').find('tr').not(':first').remove();
		var options='';
		  $.ajax({
			url:'MetaDataUploadServlet',
			data :'param=get_func'+ '&appName=' + selApp,
			async:false,
			dataType:'json',
			success: function(data){
			/*var status = data.status;
			if(status == 'SUCCESS'){*/
				var autfunctions = data.functions;
				for(var i=0;i<autfunctions.length;i++){
					var fun = autfunctions[i];
					var functionNodeName = data.functionNodeName;
					
					var functionNameNodeName = data.functionNameNodeName;
					var func = fun.tjn_function;
					if(func[functionNodeName] == undefined){
						showMessage("Please Check the XML nodes", "error");
					}else{
					options += '<tr>'+
					'<td class="tiny"><input type="checkbox" name="fcode" value="'+func[functionNodeName]+'" id="'+func[functionNodeName]+'"/></td>'+
					'<td>'+ func[functionNodeName] +'</td>'+
					'<td>'+ func[functionNameNodeName] +'</td>'+
					'</tr>';
					}
				}
				if(isOnLoad){
					/* var messagelen = $('#messageText').val().length;
					 if(messagelen != 0 && $('#messageText').val() != "null") {
						 if(messsageText.indexOf('WARNING') >= 0){
							 showMessage($('#messageText').val(),'error');
						//	 $('.msg-text').css({'background-color':'yellow'});
						 }else{
						showMessage($('#messageText').val(), "success");
						 }
					 }*/
					 var errorMessage = $('#error-msg-text').val();
						if(errorMessage == 'noerror'){
						}else{
							showMessage(errorMessage,'error');
						}
						var successMessage = $('#success-msg-text').val();
						if(successMessage == 'nomsg'){
						}else{
							if(successMessage.indexOf('WARNING') >= 0){
								showMessage(successMessage,'error');
							}else{
								showMessage(successMessage,'success');
							}
						}
				}
				$('#tblMetadataModules_body').html(options);
			  /*}
			  else{
				showMessage(data.message, 'error');				
			  }*/
		    },
		   error:function(xhr, textStatus, errorThrown){
			   showMessage(textStatus, 'error');			
		  }
	     });
	}
	  }
	  function clearAppCheckbox(){
		  $('input:checkbox[name="appName"]').each(function(){
			  var checkBoxVal = (this.checked ? $(this).val() : "");
				if(checkBoxVal!=""){
					$(this).attr('checked', false);
				} 
			});
	  }
	  
	  function clearModuleCheckbox(){
		  $('input:checkbox[name="fcode"]').each(function(){
			  var checkBoxVal = (this.checked ? $(this).val() : "");
				if(checkBoxVal!=""){
					$(this).attr('checked', false);
				} 
			});
	  }
	  
	  function getAllCheckedApps(){
			var checkedModules = '';
			$('input:checkbox[name="appName"]').each(function(){
				var checkBoxVal = (this.checked ? $(this).val() : "");
				if(checkBoxVal!=""){
				checkedModules += checkBoxVal+';';
				} 
			});
			return checkedModules;
		}
	  
});



/*$(document).ready(function(){
	$('#funcMask').hide();
	$('#appMask').hide();

	$('#lstApp').change(function(){
		var selApp = $(this).val();
		if(selApp == '-1'){
			
		}else{
			$('#tblNaviflowModules').find('tr').not(':first').remove();
			var options='';
			  $.ajax({
				url:'MetaDataUploadServlet',
				data :'param=get_func'+ '&app_id=' + selApp,
				async:false,
				dataType:'json',
				success: function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					//showMessage(data.message, 'success');	
					var dArray = data.functions;
					for(var i=0;i<dArray.length;i++){
						var fun = dArray[i];
						options += '<tr>'+
						'<td class="tiny"><input type="checkbox" value="'+fun.func+'" id="'+fun.func+'" name="func"/></td>'+
						'<td>'+ fun.func + '</td>'+
						'</tr>';
					}
					$('#tblNaviflowModules_body').html(options);
				  }
				  else{
					showMessage(data.message, 'error');				
				  }
			    },
			   error:function(xhr, textStatus, errorThrown){
				   showMessage(textStatus, 'error');			
			  }
		     });
		}
	});
		  
		  $('#appSel').change(function(){
			  if($('#appSel').is(':checked')){
				  $('#appMask').show();
				  $('#funcMask').hide();
				  $('#tblNaviflowModules').find('tr').not(':first').remove();
			  }
		  });
		  
		  $('#funSel').change(function(){
			  if($('#funSel').is(':checked')){
				  $('#funcMask').show();
				  $('#appMask').hide();
				  clearAppCheckbox();
			  }
		  });
		  
		  $('input:checkbox[name="app"]').change(function(){
			 var totcount = 0;
			 var checked = 0;
			 alert(getAllCheckedApps());
			  $('input:checkbox[name="app"]').each(function(){
					totcount++;
				  var checkBoxVal = (this.checked ? $(this).val() : "");
					if(checkBoxVal!=""){
						checked++;
					} 
				});
			  if(checked != totcount){
				  alert("notequal");
				  var isCheckedAll = $('input:checkbox[id="chk_all_modules"]:checked') ? $('input:checkbox[id="chk_all_modules"]:checked').val() : '';
				  alert(isCheckedAll);
				  if(isCheckedAll!=''){
					  alert(isCheckedAll);
					  $('input:checkbox[id="chk_all_modules"]').attr('checked', false);
				  }
			  }else if(checked == totcount){
				  alert("equal");
				  var isCheckedAll = $('input:checkbox[id="chk_all_modules"]:checked') ? $('input:checkbox[id="chk_all_modules"]:checked').val() : '';
				  alert(isCheckedAll);
				  if(isCheckedAll==''){
					  alert(isCheckedAll);
					  $('input:checkbox[id="chk_all_modules"]').attr('checked', true);
				  }
			  }
		  });
		  
		  $('input:checkbox[name="func"]').change(function(){
			  alert("avi");
				 var totcount = 0;
				 var checked = 0;
				 alert(getAllCheckedModules());
				 console.log(getAllCheckedModules());
				  $('input:checkbox[name="func"]').each(function(){
						totcount++;
					  var checkBoxVal = (this.checked ? $(this).val() : "");
						if(checkBoxVal!=""){
							checked++;
						} 
					});
				  if(checked != totcount){
					  var isCheckedAll = $('input:checkbox[id="chk_all_modules"]:checked') ? $('input:checkbox[id="chk_all_modules"]:checked').val() : '';
					  alert(isCheckedAll)
					  if(isCheckedAll!=''){
		  $('input:checkbox[id="chk_all_modules"]').attr('checked', true);
					  }
				  }
			  });
		  $('#import').click(function(){
			  
			  if($('#appMask').is(":visible")){
				  alert('appmask');
				 var allcheck = $('input:checkbox[id="chk_all_modules"]:checked') ? $('input:checkbox[id="chk_all_modules"]:checked').val() : "";
				 alert(allcheck);
				 if(allcheck!=""){
					 alert(getAllCheckedApps());
				 }else{
					 alert("all");
				 }
			  } else if($('#funcMask').is(":visible")){
				  alert("funcmask");
			  }

		  });
		  function getAllCheckedModules(){
				var checkedModules = '';
				$('input:checkbox[name="func"]').each(function(){
					var checkBoxVal = (this.checked ? $(this).val() : "");
					if(checkBoxVal!=""){
					checkedModules += checkBoxVal+';';
					} 
				});
				return checkedModules;
			}
		  
		  function getAllCheckedApps(){
				var checkedModules = '';
				$('input:checkbox[name="app"]').each(function(){
					var checkBoxVal = (this.checked ? $(this).val() : "");
					if(checkBoxVal!=""){
					checkedModules += checkBoxVal+';';
					} 
				});
				return checkedModules;
			}
		  
		  function clearAppCheckbox(){
			  $('input:checkbox[name="app"]').each(function(){
				  var checkBoxVal = (this.checked ? $(this).val() : "");
					if(checkBoxVal!=""){
						$(this).attr('checked', false);
					} 
				});
		  }
});*/