/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ui_val_new.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 24-Jul-2017			Sriram					Newly added for TENJINCG-309
* 26-Jul-2017			Sriram					TENJINCG-310
* 23-Mar-2018           Padmavathi              TENJINCG-613
* 26-06-2018            Padmavathi              T251IT-73 
* 26-06-2019			Pushpalatha				TENJINCG-968
*/


var tsRecId;
var formula;
var defExpValues;
var valDescription;
var valId;
var pagesLoaded= false;

$(document).ready(function() {
	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('main_form');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	tsRecId = $('#tstepRecId').val();
	$('#img_loadValTypes').hide();
	$('#img_loadPages').hide();
	var status = $("#status").val();
	if(status === 'error') {
		showMessage($('#message').val(), 'error');
	}else{
		populateValidationTypes();
		/*added by Padmavathi for TENJINCG-613 starts*/
		$('#img_loadValTypes').hide();
		checkFunctionLearn();
		/*added by Padmavathi for TENJINCG-613 ends*/
	}
	
	
	
});

/*$(document).on('click','#btnBack', function() {
	window.location.href='TestCaseServlet?param=test_step_view&tstep=' + tsRecId;
});*/

 /*Modified by Padmavathi for TENJINCG-613 starts*/
$(document).on('click', '#btnCancel', function() {
	var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
	if(confirmResult) {
		window.location.href='UIValidationServlet?param=tstep_ui_vals_list&srecid=' + tsRecId;
	}else{
		return false;
	}
});
/*Modified by Padmavathi for TENJINCG-613 ends*/

$(document).on('change', '#lstValidationType', function() {
	valId = $(this).val();
	var $selectedOption = $(this).find(':selected');
	valDescription = $selectedOption.data('description');
	formula = $selectedOption.data('formula');
	defExpValues = $selectedOption.data('defExp');
	
	if(valId === '-1') {
		$('#validationTypeHelp').text('Select a validation type to view its description.');
		/*added by Padmavathi for TENJINCG-613 starts*/
		$('#lstPageArea').val('-1');
		$('#fieldSelectionBlock').hide();
		/*added by Padmavathi for TENJINCG-613 ends*/
	}else{
		$('#validationTypeHelp').text(valDescription);
		
		if(!pagesLoaded) {
			loadPageAreas();
		}
	}
});

$(document).on('change','#lstPageArea',function() {
	var pageAreaName = $(this).val();
	if(pageAreaName === '-1') {
		$('#tbl_fieldsToValidate tbody').html('');
		$('#fieldSelectionBlock').hide();
	}else{
		loadFields(pageAreaName, false);
		$('#fieldSelectionBlock').show();
	}
});


function handleFormSubmit() {
	var valsteps = [];
	
	$('#tbl_fieldsToValidate').find('input[type="checkbox"]:checked').each(function() {
		if(!$(this).hasClass('tbl-select-all-rows')) {
			var label = $(this).data('uname');
			var $parentTr = $(this).parent().parent();
			var $valueElement = $parentTr.find('.exp-outcome');
			var value = $valueElement.val();
			
			var val = new Object();
			val.FLD_LABEL = label;
			val.DATA = "@@" + formula + "(" + value + ")";
			val.ACTION = "FUNCTION";
			
			valsteps.push(val);
		}
	});
	
	$('#valStepsJson').val(JSON.stringify(valsteps));
	
	/* Added for TENJINCG-310 */
	var preSteps = [];
	$('#tbl_preSteps').find('input[type="checkbox"]:checked').each(function() {
		if(!$(this).hasClass('tbl-select-all-rows')) {
			var label = $(this).data('uname');
			var oClass = $(this).data('objectClass');
			var $parentTr = $(this).parent().parent();
			var $valueElement = $parentTr.find('.exp-outcome');
			var value = $valueElement.val();
			
			var val = new Object();
			if(oClass.toLowerCase() === 'button') {
				val.FLD_LABEL = 'BUTTON';
				val.DATA = label;
			}else{
				val.FLD_LABEL = label;
				val.DATA = value;
			}
			/*val.DATA = value;*/
			val.ACTION = "INPUT";
			
			preSteps.push(val);
		}
	});
	
	$('#preStepsJson').val(JSON.stringify(preSteps));
	/* Added for TENJINCG-310 ends*/
}

$(document).on('submit','#main_form', function(e) {
	
	var valType=$('#lstValidationType').val();
	if(valType=="-1"){
		showMessage('Please select Validation Type.', 'error');
		return false;
	}
	
	var lstPageArea=$('#lstPageArea').val();
	if(lstPageArea=="-1"){
		showMessage('Please select Page Area.', 'error');
		return false;
	}
	/*Modified by Padmavathi for TENJINCG-613 starts*/
	
	/*Modified by Padmavathi for T251IT-73 starts*/
	/*if($('#tbl_fieldsToValidate').find('input[type="checkbox"]:checked').length < 1) {
		showMessage('Please select at least one field to validate','error');
		return false;
	}else if($('#tbl_fieldsToValidate').find('input[type="checkbox"]:checked').length < 2 && $('#tbl_fieldsToValidate.tbl-select-all-rows').is(':checked')) {
		showLocalizedMessage('Please select at least one field to validate','error');
		return false;
	}*/
	if($('#tbl_fieldsToValidate').find('input[type="checkbox"]:checked').length < 1 && $('#tbl_preSteps').find('input[type="checkbox"]:checked').length < 1) {
		showMessage('Please select at least one field to validate','error');
		return false;
	}else if(($('#tbl_fieldsToValidate').find('input[type="checkbox"]:checked').length < 2 && $('#tbl_fieldsToValidate .tbl-select-all-rows').is(':checked'))  &&($('#tbl_preSteps').find('input[type="checkbox"]:checked').length < 2 && $('#tbl_fieldsToValidate .tbl-select-all-rows').is(':checked'))) {
		showLocalizedMessage('Please select at least one field to validate','error');
		return false;
	}
	/*Modified by Padmavathi for T251IT-73 ends*/
	/*Added by pushpalatha for field validation starts*/
	/*var selectedTests = '';
	$('#tbl_fieldsToValidate').find('input[type="checkbox"]:checked').each(function () {
	       
		if(this.id != 'chk_all'){
			selectedTests = selectedTests + this.id +',';
		}
	});

	if( selectedTests.charAt(selectedTests.length-1) == ','){
		selectedTests = selectedTests.slice(0,-1);
	}
	
	if(selectedTests.length < 1){
		showMessage("Please select at least one field to validate", "error");
		return false;
	
	}*/
	/*Added by Pushpalatha for field validation ends*/
	/*Modified by Padmavathi for TENJINCG-613 ends*/
	e.preventDefault();
	handleFormSubmit();
	//return false;`
	var data = $(this).serialize();
	 var csrftoken_form = $('#csrftoken_form').val();
	$.ajax({
		url:'UIValidationServlet',
		data:data,
		dataType:'json',
		type:'post',
		method:'post',
		success:function(data) {
			if(data.status === 'success') {
				window.location.href = 'UIValidationServlet?param=view_ui_val_step&valrecid=' + data.recid + '&message='+ data.message+'&csrftoken_form='+csrftoken_form;
			}else{
				showMessage(data.message, 'error');
			}
		},error:function(xhr, textStatus, errorThrown) {
			showMessage('An internal error occurred.  Please contact Tenjin Support.', 'error');
		}
	});
});
	/*added by Padmavathi to checking function learning starts*/
function checkFunctionLearn() {

	$.ajax({
		url:'UIValidationServlet?param=ajax_fetch_locations&app=' + $('#appId').val() + '&func=' + $('#functionCode').val(),
		dataType:'json',
		success:function(data) {
			if(data.status === 'success') {
				if(data.locations.length==0){
					$('#img_loadValTypes').hide();
					$('#img_loadPages').hide();
					alert("Function selected for the test step was not learnt,to create validation step function should be learnt.")
				}
			}else{
				$('#img_loadPages').hide();
			}
		},
		error:function(xhr, textStatus, errorThrown) {
			$('#img_loadPages').hide();
		}
	});
	/*added by Padmavathi to checking function learning ends*/
}



