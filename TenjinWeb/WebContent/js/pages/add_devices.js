/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  add_devices.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 	05-01-2018			Sahana					Mobility
 * 25-06-2018           Padmavathi              for T251IT-145, T251IT-146
 */	

$(document).ready(function(){
	populateClients();
	$('#inProgress').hide();
	$('#devices-table').dataTable();
	var allDevices;
	$('#btnBack').click(function(){
		window.location.href='DeviceServlet?param=FETCH_ALL_DEVICES';
	});
	$('#chk_all').click(function(e){	
		if ($("#chk_all").is(':checked')) 
		{	
			$(this).closest('table').find('input[type=checkbox]').prop('checked', this.checked);	
			e.stopPropagation();
		}
		else
		{
			$(this).closest('table').find('input[type=checkbox]').prop('checked',false);	 
			e.stopPropagation();
		}
	}); 

	$('#btnScan').click(function(){
		clearMessages();
		var client=$('#txtclientregistered').val();
		if(client=='-1')
		{
			showMessage("Please select a client", 'error');	
		}
		else
		{
			$('#inProgress').show();
			var urlLink='param=SCAN_DEVICES&client='+client;
			$.ajax({
				url:'DeviceServlet',
				data :urlLink,
				dataType:'json',
				success: function(data){
					$('#inProgress').hide();
					var status = data.status;
					allDevices = data.devices;
					var existDevices=data.registeredDevices;
					if(status == 'SUCCESS')
					{
						var html = "";
						/*Added by Padmavathi for T251IT-145 starts*/
						var checkDevice="";
						/*Added by Padmavathi for T251IT-145 ends*/
						for(i=0;i<allDevices.length;i++){
							var json = allDevices[i];
							var flag=deviceExists(existDevices,json.deviceId);
							/*Added by Padmavathi for T251IT-145 starts*/
							checkDevice="true"
							/*Added by Padmavathi for T251IT-145 ends*/
							if(flag==true){
								/*Added by Padmavathi for T251IT-146 starts*/
								$('#chk_all').prop('disabled',true);
								/*Added by Padmavathi for T251IT-146 ends*/
								html = html + "<tr><td><input class='tiny' type='checkbox' disabled name='"+json.deviceId+"' value='"+json.deviceId+"'  id='"+json.deviceId+"'></input></td><td>"+json.deviceId+"</td><td>"+json.deviceName+"</td><td>"+json.deviceType+"</td><td>"+json.platform+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+json.pltVersion+"</td><td>Registered</td></tr>"
							}
							else
							{
								html = html + "<tr><td><input class='tiny' type='checkbox' name='"+json.deviceId+"' value='"+json.deviceId+"' id='"+json.deviceId+"'></input></td><td>"+json.deviceId+"</td><td>"+json.deviceName+"</td><td>"+json.deviceType+"</td><td>"+json.platform+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+json.pltVersion+"</td><td>Not-Registered</td></tr>";
							}
						}
						/*Added by Padmavathi for T251IT-145 starts*/
						if(checkDevice==""){
							html= "<tr><td colspan='7'align='center'>No devices are connected to client</td></tr>";
						}
						/*Added by Padmavathi for T251IT-145 ends*/
						$('#devicesList').html(html);

					}
					else
					{
						showMessage(data.message, 'error');				
					}
				},

			});
		}

	});

	$('#btnRegister').click(function(){
		clearMessages();
		var client=$('#txtclientregistered').val();
		var selectedDevices='';
		var count=0;
		var urlLink='';
		$('#devices-table').find('input[type="checkbox"]:checked').each(function (){
			if(this.id != 'chk_all'){
				selectedDevices = selectedDevices + this.id + ',';
				count++;
			}
		});
		if(count===0)
		{
			showMessage("Please select atleast one device ",'error'); 

		}
		else
		{
			var jsonString = JSON.stringify(allDevices);
			if(count===1)
			{
				urlLink='param=ADD_DEVICE&paramval='+ selectedDevices.substring(0,selectedDevices.length-1)+'&devices='+jsonString;

			}else if(count>1){
				urlLink='param=ADD_DEVICES&paramval='+ selectedDevices+'&devices='+jsonString;
			}
			if(count===1 || count>1)
			{
				$.ajax({
					url:'DeviceServlet',
					data :urlLink,
					async:false,
					dataType:'json',
					success: function(data){
						var status = data.status;
						if(status == 'SUCCESS')
						{			
							showMessage(data.message, 'success');	

							var client=$('#txtclientregistered').val();
							var urlLink='param=SCAN_DEVICES&client='+client;
							$.ajax({
								url:'DeviceServlet',
								data :urlLink,
								async:false,
								dataType:'json',
								success: function(data){
									var status = data.status;
									allDevices = data.devices;
									var existDevices=data.registeredDevices;
									if(status == 'SUCCESS')
									{
										var html = "";
										for(i=0;i<allDevices.length;i++){
											var json = allDevices[i];
											var flag=deviceExists(existDevices,json.deviceId);
											if(flag==true){
												html = html + "<tr><td><input class='tiny' type='checkbox' disabled name='"+json.deviceId+"' value='"+json.deviceId+"'  id='"+json.deviceId+"'></input></td><td>"+json.deviceId+"</td><td>"+json.deviceName+"</td><td>"+json.deviceType+"</td><td>"+json.platform+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+json.pltVersion+"</td><td>Registered</td></tr>"
											}
											else
											{
												html = html + "<tr><td><input class='tiny' type='checkbox' name='"+json.deviceId+"' value='"+json.deviceId+"' id='"+json.deviceId+"'></input></td><td>"+json.deviceId+"</td><td>"+json.deviceName+"</td><td>"+json.deviceType+"</td><td>"+json.platform+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+json.pltVersion+"</td><td>Not-Registered</td></tr>";
											}
										}
										$('#devicesList').html(html);
									}
									else
									{
										showMessage(data.message, 'error');				
									}
								},
							});
						}
						else
						{
							showMessage(data.message, 'error');				
						}
					},

				});
			}

		}

	});

});

function deviceExists(existDevices,deviceId){
	var flag=false;
	for(var i=0;i<existDevices.length;i++){
		if(existDevices[i].deviceId==deviceId){
			flag=true;
			break;
		}
	}
	return flag;
}
function populateClients(){

	var jsonObj = fetchAllClients();

	var status = jsonObj.status;
	clearMessages();
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jArray = jsonObj.clients;

		for(i=0;i<jArray.length;i++){
			var client = jArray[i];
			html = html + "<option value='" + client.name + "'>" + client.name + "</option>";
		}

		$('#txtclientregistered').html(html);

	}else{
		showMessage(jsonObj.message, 'error');
	}
}






