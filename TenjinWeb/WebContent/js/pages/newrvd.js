/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  newrvd.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

$(document).ready(function(){
	populateAuts();
	
	/*Change by Badal for Cancel button not working on 17th-Nov-14*/
	
	/*******************************
	 * Cancel Button Functionality
	 *******************************/
	$('#btnCancel').click(function(){
		window.location.href="rvdlist.jsp";
	});
	
	/*Change by Badal for Cancel button not working on 17th-Nov-14 Ends*/
	
	/*******************************
	 * Change application name
	 *******************************/
	$('#lstApplication').change(function(){
		var app = $(this).val();
		clearMessages();
		if(app == '-1'){
			//$('#moduleInfo').hide();
			$('#lstModules').html('');
		}else{
			populateModuleInfo(app);
			//$('#moduleInfo').show();
		}
	});
	
	/***************************
	 * Sav button - CLick
	 */
	
	$('#btnSave').click(function(){
		var formvalidation = validateForm("main_form");
		if(formvalidation != 'SUCCESS'){
			showMessage(formvalidation,'error');
			return false;
		}
		
		var selApp = $('#lstApplication').val();
		if(selApp == '-1'){
			showMessage('Application is mandatory');
			return false;
		}
		
		return true;
	});
	
	
});

function populateAuts(){
	var jsonObj = fetchAllAuts();
	
	var status = jsonObj.status;
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jArray = jsonObj.auts;
		
		for(i=0;i<jArray.length;i++){
			var aut = jArray[i];
			html = html + "<option value='" + aut.id + "'>" + aut.name + "</option>";
		}
		
		$('#lstApplication').html(html);
		
	}else{
		showMessage(jsonObj.message, 'error');
	}
}


function populateModuleInfo(appId){
	var jsonObj = fetchModulesForApp(appId);
	
	$('#lstModules').html('');
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html = "<option value='-1'>-- Select One --</option>";
		var jarray = jsonObj.modules;
		for(i=0;i<jarray.length;i++){
			var json = jarray[i];
			html = html + "<option value='" + json.code + "'>" + json.name + "</option>";
		}
		
		$('#lstModules').html(html);
	}else{
		showMessage(jsonObj.message,'error');
	}
}