/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  field_Wait_Time.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/
/*new file Added by Leelaprasad for the requirement TENJINCG-265 starts*/
/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              	DESCRIPTION
24-10-2017			Sriram Sridharan			TENJINCG-399
25-06-2018			Preeti						T251IT-144
01-03-2019          Padmavathi                  TENJINCG-996
10-04-2019			Preeti						TENJINCG-1026
*/

$(document).ready(function(){
	
	
	
	/*Changed by Naga Reddy to Defect fix TEN-42 on 19-12-2016 starts*/
//////unmap the manual field 
	$('#btnWaitTimeUnMapAll').click(function(){
    	///////////////////////////
		 var r = confirm("Do you want to clear all mapping?");
		    if (r == true) {
		
		var json = new Object();
		
		json.app = $('#txtAPPID').val();
		json.module = $('#txtFunctionCode').val();
		
			
		var jsonString = JSON.stringify(json);
		
		$.ajax({
			url:'ModuleServlet',
			data:'param=field_wait_time_unmap_all&json=' + jsonString,
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){					
					showMessage(data.message,'success');
						}else{
					showMessage(data.message,'error');				}
			},error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
			}
		});
		//window.location.reload();
		$("#dTree").jstree("refresh");
		    }
		    else {
		    	
		    }
	});
	/*Changed by Naga Reddy to Defect fix TEN-42 on 19-12-2016 ends*/
	
	$('#btnWaitTimeUnMap').click(function(){
    	///////////////////////////
		
		var unmapsheet=$('#sheetunmap').val();
		var unmapfield=$('#fieldunmap').val();			
		var mapstatus=$('#mappedstatus').val();
		
//		if((mapstatus==='undefined')
//				||(mapstatus===false)
//				||(mapstatus.length==0)){
		if(mapstatus=="false" || mapstatus==""){
			
			$('#user-message').html('Please select the mapped field to unmap').addClass('msg-err').show();	
		}
		else{
			 var r = confirm("Do you want to clear field '"+unmapfield+"' on sheet '"+unmapsheet+"'");
			    if (r == true) {
		var json = new Object();
			
		json.app = $('#txtAPPID').val();
		json.module = $('#txtFunctionCode').val();
				
		var jsonString = JSON.stringify(json);
		
		$.ajax({
			url:'ModuleServlet',
			data:'param=field_wait_time_unmap&json=' + jsonString+'&sheet='+unmapsheet+'&field='+unmapfield,
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){					
					showMessage(data.message,'success');
						}else{
					showMessage(data.message,'error');				}
			},error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
			}
		});
		//window.location.reload();
		$("#dTree").jstree("refresh");
		}else{}
		}
		
		    
	});
	
	$('#btnSave').click(function(){	
		/*Added by Preeti for T251IT-144 starts*/
		var waitTime = $('#waittime').val();
		if(isNaN(waitTime) && !((waitTime%1)===0)){
			showMessage("Please enter valid wait time.");
			return false;
		}
		/*Modified by Preeti for TENJINCG-1026 starts*/
		/*if(waitTime.includes(".")){*/
		if(waitTime.indexOf(".") != -1){
		/*Modified by Preeti for TENJINCG-1026 ends*/
			showMessage("Please enter valid wait time.");
			return false;
		}
		/*Added by Preeti for T251IT-144 ends*/
		var formVal = validateForm('main_form_manual');
		if(formVal != 'SUCCESS'){
			showMessage(formVal,'error');
			return false;
		}
		//////////////////manual///////////////////////
	
		
		var arrParentIds=[];
		var array = [];
    	
    	$('.manualmapval').each(function(){
    		array.push({
    			"srcSheet":$(this).attr('sourcesheet'),
    			"srcField":$(this).attr('sourcefield'), 
    			"srcWaitTime":$(this).attr('waittime'), 
    			"sourceNodeId":$(this).attr('sourcenodeid')    			
    		});
    		
    		//srcSheet  srcField  targetSheet targetField
    	});
    	if(array.length==0){
    		showMessage('Please add Wait Time for map field ','error');
    		return false;
    	}
    	var manualStr = JSON.stringify(array);    	
    	//alert(manualStr);
    	///////////////////////////
		var json = new Object();
				
		json.app = $('#txtAPPID').val();
		json.module = $('#txtFunctionCode').val();
			
		
		
			
		var jsonString = JSON.stringify(json);
		$.ajax({
			url:'ModuleServlet',
			data:'param=field_wait_time_save&json=' + jsonString+'&waittimefield='+manualStr,
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					
					showMessage(data.message,'success');
					////////////manual refresh/////
				
					for(var ia=0;ia<array.length;ia++){
						
						var json = array[ia];				
					
					$('#dTree').jstree(true).get_node(json.sourceNodeId).icon='jstree/mapped.png';					
					 // this will reload the children of the node
					$("#dTree").jstree(true).refresh_node(json.sourceNodeId);
					var parent=$('#dTree').jstree(true).get_node(json.sourceNodeId).parent;
					arrParentIds.push(parent);
					}	
		try{
		$("#dataMapped tbody tr").remove();}catch(err){}
		var noDupes = ArrNoDupe(arrParentIds);
		for(var l=0;l<noDupes.length;l++){
		$("#"+noDupes[l]).jstree("refresh");
		}

		
				}else{
					showMessage(data.message,'error');
					
				}
			},error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
				
			}
		});
		
	});
	
	$('#btnBack').click(function(){
		var appId=$('#txtAPPID').val();
	    var FunctionCode=$('#txtFunctionCode').val();
	    var LearnStatus="COMPLETE";
	    /* TENJINCG-399 */
		//window.location.href="ModuleServlet?param=VIEW_FUNC&paramval="+FunctionCode+"&a="+appId+"&learnStatus="+LearnStatus;
		window.location.href='FunctionServlet?t=view&key='+ FunctionCode +'&appId=' + appId ;
		/* TENJINCG-399 ENDS */
	});
	
	
	////////////////////////// Manual
	
		/**********************
	 * Load Source Tree
	 */
	
	
	displaySheetTree();
	/**********************
	 * JSTree events
	 */
	$('#dTree').on('changed.jstree', function(e, data){
		//$('#addMap').show();		
		try{
		var nodeType = data.node.li_attr.nodetype;
		var sourceSheet = data.node.li_attr.sourcesheet;
		var sourceField = data.node.li_attr.sourcefield;
		var sourceWaitTime=data.node.li_attr.waittime;
		var alreadymapped = data.node.li_attr.mapped;
		try{setTargetSheetAndFieldForUnMap(sourceSheet, sourceField,sourceWaitTime,alreadymapped);}catch(errr){}

		setTargetSheetAndField(sourceSheet, sourceField,sourceWaitTime);		
		$('#srcSheet').val(data.node.li_attr.sourcesheet);		
		$('#srcField').val(data.node.li_attr.sourcefield);
		$('#srcWaitTime').val(data.node.li_attr.waittime);
		$('#sFieldNodeId').val(data.node.li_attr.nodeid);	
			
		/*}*/
		}catch(err){}
	});
	

	/****************************
	 * Even Handling - Click Add to Map
	 */
	$('#addMap').click(function() {
		
		
		$('#user-message').html('').removeClass('msg-success');
		addSheetField();
		
	})

});





function displaySheetTree(){
	
	$(".search-input").keyup(function() {

        var searchString = $(this).val();
       // console.log(searchString);
        if(searchString.length>2){
        $('#dTree').jstree('search', searchString);      
        
        }
        if(searchString.length==0){
        	$("#dTree").jstree("close_all");
        	
        }
    });
	 

	
	$('#dTree').jstree({
		'core':{
			'data':{
				'url':'ModuleServlet',
				'data':function (node) {
					return {'param':'wait_time_tree','id':node.id};
				},
				'dataType':'json'
				
			},
			 "multiple":false
			
		},
		 "plugins" : [ "wholerow","search" , "dnd"]
	   });

}
	

function setTargetSheetAndField(sheetName, fieldName,sourceWaitTime){	
			$('#sheet').val(sheetName);		
			$('#field').val(fieldName);	
			$('#waittime').val(sourceWaitTime);	
}
function setTargetSheetAndFieldForUnMap(sheetName, fieldName,sourceWaitTime,status){	
	$('#sheetunmap').val(sheetName);
	$('#fieldunmap').val(fieldName);
	$('#waittimeunmap').val(sourceWaitTime);
	$('#mappedstatus').val(status);
}

function addSheetField() {
	
	$('#user-message').html('').removeClass('msg-err');
	var sourceSheet = $('#sheet').val();
	var sourceField = $('#field').val();
	var sourceWaitTime = $('#waittime').val();	
	var sourceNodeId = $('#sFieldNodeId').val();
	if(sourceWaitTime==='undefined'||sourceWaitTime.length===0){
		sourceWaitTime=0;
	}
	
	if(sourceSheet==='undefined' && sourceField==='undefined' && sourceNodeId==='undefined'||sourceSheet.length===0||sourceField.length===0 || sourceNodeId.length===0){
		
		
		$('#user-message').html('Please select source field').addClass('msg-err').show();
	}
	else{
	var idrow = sourceSheet + "_" + sourceField;
	var count = $('#dataMapped tr').length;
	
	var flag;
	
	if(count>0){
		
	 flag=getRowFlag(idrow,sourceNodeId);
	
	}
	
	/**/
	if(flag=='SUCCESS'){
		
		$('#user-message').html('Already added to  Map or Duplicate ').addClass('msg-err').show().focus();
		}else{
			
	$("#dataMapped tbody")
			.append(
					"<tr id='"
					+ idrow
					+ "' class='' title='source -> "+sourceSheet+" : "+sourceField+"->"+sourceWaitTime+"'>"
					+ "<td class='rowtd'><label class='small source'>"
					+sourceSheet
					+ ':'
					+sourceField
					+'['
					+sourceWaitTime
					+']'
					/*Modified by Padmavathi for TENJINCG-996 starts*/
					+"</label></td><td><input type='hidden' waittime='"+sourceWaitTime+"' sourcesheet='"+sourceSheet+"' sourcefield='"+sourceField+"' sourcenodeid='"+sourceNodeId+"'  value='"+idrow+"' class='manualmapval'/><input type='button' class='imagebutton delete btnDelete' value='Remove' style='margin-left:10px'/><td>"
					/*Modified by Padmavathi for TENJINCG-996 ends*/		
					+ "</tr>");
	$(".btnDelete").bind("click", Delete);
	
	
	}
	
	}
};

function Delete() {
	var par = $(this).parent().parent();
	par.remove();
	$('#user-message').html('').removeClass('msg-err');
}


function getRowFlag(data,nodeId) {
	var showError = false;
	//var showCofirm=false;
	$('#dataMapped tbody').find("tr").each(function(index,r){
		var keyValue = $(this).find("[sourcenodeid]").eq(0).attr("sourcenodeid");
		   if(r.id===data){				   
			   showError=true;	
			   
			  }  		  
		   else  if(keyValue==nodeId){
			   
			   showError=true;
			   
			   }
		  });
	
	 if(showError==true){
		return 'SUCCESS';
	}
	else{
		return 'FAIL';
		}
	}

function ArrNoDupe(a) {
    var temp = {};
    for (var i = 0; i < a.length; i++)
        temp[a[i]] = true;
    var r = [];
    for (var k in temp)
        r.push(k);
    return r;
}



