/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ui_val_edit.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 24-Jul-2017			Sriram					Newly added for TENJINCG-309
* 26-Jul-2017			Sriram					TENJINCG-310
* 28-Jul-2017			Pushpalatha				Req#TENJINCG-316
* 28-08-2017            Padmavathi               defect T25IT-326
* 23-03-2018            Padmavathi              TENJINCG-613
* 26-06-2018            Padmavathi              T251IT-73 
*/

var tsRecId;
var formula;
var defExpValues;
var valDescription;
var valId;

$(document).ready(function() {
	tsRecId = $('#tstepRecId').val();
	$('#img_loadPages').hide();
	var status = $("#status").val();
	if(status === 'success' && $('#message').val() !== undefined && $('#message').val().length > 0 && $('#message').val() !== 'null') {
		showMessage($('#message').val(), 'success');
	}
	
	formula = $('#val_formula').val();
	defExpValues = $('#val_def_exp_values').val();
	
	loadFields($('#pageArea').val(), true);
	/*commented by Padmavathi for TENJINCG-613 starts*/
/*	
	Added by Pushpalatha for requirement TENJINCG-316 starts
	$('#btnDelete').click(function(){
		Added By padmavathi for T25IT-326 starts
		clearMessagesOnElement($('#user-message'));
		Added By padmavathi for T25IT-326 ends
		var selectedTests = $('#recordId').val();
		var tsRecId=$('#tstepRecId').val();
			if (window.confirm('Are you sure you want to delete?')){
				data='param=delete_selectedValidationSteps&valSteplist=' + selectedTests
				
			}else{
				return false;
			}

		 $.ajax({
		     url:'UIValidationServlet',
		     data:data,
		     async:true,
		     dataType:'json',
		      success:function(data){
			         var status = data.status;
			            if(status == 'SUCCESS')
			            {
			            	
							window.location.href='UIValidationServlet?param=tstep_ui_vals_list&srecid=' + tsRecId;
							
							setTimeout(function(){
							    location = ''
							  },1200);
							showMessage("Deleted successfully",'success');
							
			             }
			            else
			             {
				            showMessage(data.message,'error');
			             }
			           
		            },
		       error:function(xhr, textStatus, errorThrown){
			           showMessage(errorThrown,'error');
			          
			          
		          },
		      });
    });*/

	/*Added by Pushpalatha for requirement TENJINCG-316 ends*/

	
});
/*commented by Padmavathi for TENJINCG-613 ends*/

/*Added by Padmavathi for TENJINCG-613 starts*/
$(document).on("click", "#btnDelete", function() {
	clearMessagesOnElement($('#user-message'));
	if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
		var $uiValForm = $("<form action='UIValidationServlet' method='POST' />");
		var csrftoken_form = $("#csrftoken_form").val();
		$uiValForm.append($("<input type='hidden' id='del' name='del' value='true' />"))
		$uiValForm.append($("<input type='hidden' id='requestPage' name='requestPage' value='viewPage' />"))
		$uiValForm.append($("<input type='hidden' id='stepRecId' name='stepRecId' value='"+$('#tstepRecId').val()+"'/>"))
		$uiValForm.append($("<input type='hidden' id='selectedUiValCodes' name='selectedUiValCodes' value='"+$('#recordId').val()+"'/>"))
		$uiValForm.append($("<input type='hidden' id='csrftoken_form' name='csrftoken_form' value='"+csrftoken_form+"' />"))
		$('body').append($uiValForm);
		$uiValForm.submit();
	}
});
/*Added by Padmavathi for TENJINCG-613 ends*/

$(document).on('click', '#btnBack', function() {
	window.location.href='UIValidationServlet?param=tstep_ui_vals_list&srecid=' + tsRecId;
});

function handleFormSubmit() {
	var valsteps = [];
	
	$('#tbl_fieldsToValidate').find('input[type="checkbox"]:checked').each(function() {
		if(!$(this).hasClass('tbl-select-all-rows')) {
			var label = $(this).data('uname');
			var $parentTr = $(this).parent().parent();
			var $valueElement = $parentTr.find('.exp-outcome');
			var value = $valueElement.val();
			
			var val = new Object();
			val.FLD_LABEL = label;
			val.DATA = "@@" + formula + "(" + value + ")";
			val.ACTION = "FUNCTION";
			
			valsteps.push(val);
		}
	});
	
	$('#valStepsJson').val(JSON.stringify(valsteps));
	
	/* Added for TENJINCG-310 */
	var preSteps = [];
	$('#tbl_preSteps').find('input[type="checkbox"]:checked').each(function() {
		if(!$(this).hasClass('tbl-select-all-rows')) {
			var label = $(this).data('uname');
			var oClass = $(this).data('objectClass');
			var $parentTr = $(this).parent().parent();
			var $valueElement = $parentTr.find('.exp-outcome');
			var value = $valueElement.val();
			
			var val = new Object();
			if(oClass.toLowerCase() === 'button') {
				val.FLD_LABEL = 'BUTTON';
				val.DATA = label;
			}else{
				val.FLD_LABEL = label;
				val.DATA = value;
			}
			/*val.DATA = value;*/
			val.ACTION = "INPUT";
			
			preSteps.push(val);
		}
	});
	
	$('#preStepsJson').val(JSON.stringify(preSteps));
	/* Added for TENJINCG-310 ends*/
}

$(document).on('submit','#main_form', function(e) {
	e.preventDefault();
	handleFormSubmit();
	//return false;
	var data = $(this).serialize();
	/*Modified by Padmavathi for T251IT-73 ends*/
	/*added by Padmavathi for TENJINCG-613 starts*/
	/*if($('#tbl_fieldsToValidate').find('input[type="checkbox"]:checked').length < 1) {
		showMessage('Please select at least one field to validate','error');
		return false;
	}else if($('#tbl_fieldsToValidate').find('input[type="checkbox"]:checked').length < 2 && $('#tbl_fieldsToValidate.tbl-select-all-rows').is(':checked')) {
		showLocalizedMessage('Please select at least one field to validate','error');
		return false;
	}*/
	if($('#tbl_fieldsToValidate').find('input[type="checkbox"]:checked').length < 1 && $('#tbl_preSteps').find('input[type="checkbox"]:checked').length < 1) {
		showMessage('Please select at least one field to validate','error');
		return false;
	}else if(($('#tbl_fieldsToValidate').find('input[type="checkbox"]:checked').length < 2 && $('#tbl_fieldsToValidate .tbl-select-all-rows').is(':checked'))  &&($('#tbl_preSteps').find('input[type="checkbox"]:checked').length < 2 && $('#tbl_fieldsToValidate .tbl-select-all-rows').is(':checked'))) {
		showLocalizedMessage('Please select at least one field to validate','error');
		return false;
	}
	/*Modified by Padmavathi for T251IT-73 ends*/
	/*added by Padmavathi for TENJINCG-613 ends*/
	var csrftoken_form = $('#csrftoken_form').val();
	$.ajax({
		url:'UIValidationServlet',
		data:data,
		dataType:'json',
		type:'post',
		method:'post',
		success:function(data) {
			if(data.status === 'success') {
				/*showMessage('Validation step created successfully!','success');*/
				window.location.href = 'UIValidationServlet?param=view_ui_val_step&valrecid=' + $('#recordId').val() + '&message=Validation step updated successfully.'+'&csrftoken_form='+csrftoken_form;
			}else{
				showMessage(data.message, 'error');
			}
		},error:function(xhr, textStatus, errorThrown) {
			showMessage('An internal error occurred.  Please contact Tenjin Support.', 'error');
		}
	});
});