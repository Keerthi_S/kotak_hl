/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_new.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 01-Dec-2016			Sahana					Req#TJN_243_03 
 * 16-DEC-2016			 Sahana					Req#TEN-72
 * 19-DEC-2016			Nagababu				Req#TEN-117
 * 21-Dec-2016         Sahana                  defect #TEN-139(The Minute Field is blank in Schedule Task screen.)
 *  10-JAN-2016          Sahana                  TENJINCG-7(Calendar Icon is to be added for all date fields)
 *  27-Jan-2017			Sahana					TENJINCG-61(A Tenjin user should be able to specify Screenshot Option while Scheduling an Execution task)
 * 20-July-2017			Gangadhar Badagi		TENJINCG-292
 * 21-July-2017			Gangadhar Badagi		Added to check mandate field type
 * 25-July-2017         Gangadhar Badagi        Added to hide the testCase,testSet when type is selected as select one
 * 01-Aug-2017        Gangadhar Badagi         to validate mandate field TestCase and TestSet
 * 02-08-2017          Padmavathi               Tenjincg-336
 * 17-08-2017          Leelaprasad              T25IT-60
 * 18-08-2017        Leelaprasad                  T25IT-132
 * 29-08-2017        Leelaprasad                 T25IT-92
 * 27-09-2017         Gangadhar Badagi       To schedule the task between 55-60 minutes.
 * 22-06-2018			Preeti					T251IT-123
 * 12-02-2019			Pushpalatha				TENJINCG-927
 * 19-02-2019			Preeti					TENJINCG-928
 * 20-03-2019			Padmavathi			    TENJINCG-1014
 * 25-03-2019			Pushpalatha				TENJINCG-968
 * 22-04-2019           Padmavathi              To display proper label alignments in Recurrence Preferences page
 * 30-05-2019			Ashiki					V2.8-63
 * 13-06-2019			Roshni					V2.8-24 & V2.8-94
 * 14-06-2019			Roshni					V2.8-96
 * 03-07-2019			Preeti					TV2.8R2-11
 * 05-07-2019			Preeti					TV2.8R2-21
 * 08-07-2019           Padmavathi              TV2.8R2-20 & 22
 * 25-09-2019			Prem					TJN2.9R2-4
 */

/*
 * Added file by sahana for Req#TJN_24_03 on 14/09/2016
 */

$(document).ready(function(){
	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('new_schedule');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	/*Added by Padmavathi for TV2.8R2-22 for starts*/
	$('#btnLogs').hide();
	/*Added by Padmavathi for TV2.8R2-22 for ends*/
	/*Added by Gangadhar Badagi for TENJINCG-292 starts*/	
$('#tset').hide();
$('#tCase').hide();
/*Added by Pushpa for TENJINCG-927 starts*/
var projectScreenShotOption=$('#projectScreenShotOption').val();
$('#screenShot').val(projectScreenShotOption);
/*Added by Pushpa for TENJINCG-927 ends*/


/*Added by Gangadhar Badagi for TENJINCG-292 ends*/
/*Added by Padmavathi for TENJINCG-336 starts*/
var name=$('#datepicker').val();
/*Added by Roshni for V2.8-24 & V2.8-94 starts */
name=name.replace("-"," ");
/*Added by Roshni for V2.8-24 & V2.8-94 ends */
populateHours(Date.parse(name));
populateMinutes(Date.parse(name));
/*Added by Padmavathi for TENJINCG-336 ends*/
	var hoursVal= $("#btntime").val();
	$(window).load(function(){
		function setSelectWidth(selector) {
			var sel = $(selector);
			var tempSel = $("<select style='display:none'>").append( $("<option>").text( sel.find('option:selected').text() ) );
			tempSel.appendTo( $("body") );

			sel.width( tempSel.width() );

			tempSel.remove();
		}

		$(function() {
			setSelectWidth("#txtTestSet");

			$("#txtTestSet").on("change", function() {
				setSelectWidth($(this));
			});
		});

	});
	$(window).load(function(){
		function setSelectWidth(selector) {
			var sel = $(selector);
			var tempSel = $("<select style='display:none'>").append( $("<option>").text( sel.find('option:selected').text() ) );
			tempSel.appendTo( $("body") );

			sel.width( tempSel.width() );

			tempSel.remove();
		}

		$(function() {
			setSelectWidth("#screenShot");

			$("#screenShot").on("change", function() {
				setSelectWidth($(this));
			});
		});

	});
	/*Added by Padmavathi for TV2.8R2-20 && 22 for starts*/
	$('#btnLogs').click(function(){
		clearMessages();
		$('#importLogs').modal('show');
		//$('#importLogs').show();
	});
	$('#btnclose').click(function(){
		clearMessages();
		$('#btnLogs').hide();
	});
	/*Added by Padmavathi for TV2.8R2-20 && 22 for ends*/
	
	/*Removed by Gangadhar Badagi for TENJINCG-292 starts*/
	//populateAllTestSets();
	/*Removed by Gangadhar Badagi for TENJINCG-292 ends*/
	$('#tasktype').hide();
	$('#pagination_block').hide();

	$(function() {
		$( "#datepicker" ).datepicker({ 
			/*   Changed by sahana for the improvement of TENJINCG-7: starts  */
			showOn: 'button',
			buttonText: 'Show Date',
			buttonImageOnly: true,
			buttonImage: './images/button/calendar.png',
			constrainInput: true,
			/*   Changed by sahana for the improvement of TENJINCG-7: ends  */
			dateFormat: 'dd-M-yy',
			minDate:0,

		});

	});
	/*changed by sahana for Req#TJN_243_03:starts*/
	var m;
	$(function() {
		m = $('#hiddendatepicker1').val();
		$( "#datepicker1" ).datepicker({
			/*   Changed by sahana for the improvement of TENJINCG-7: starts  */
			showOn: 'button',
			buttonText: 'Show Date',
			buttonImageOnly: true,
			buttonImage: './images/button/calendar.png',
			constrainInput: true,
			/*   Changed by sahana for the improvement of TENJINCG-7: starts  */
			dateFormat: 'dd-M-yy',
			minDate:0,

		});
	});
	$('#datepicker').on('change', function() {
		m = $('#datepicker').val();	
	});
	/*changed by sahana for Req#TJN_243_03:ends*/
	populateClients();

	$(document).on("click", "#btnEdit", function(){

		var client = $("#txtclientregistered").val();
		window.location="ClientServlet?param=FETCH_ALL_REG_CLIENTS&paramval="+client;			
	});
	
	/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
	$('#type').on('change', function() {
		var val=$('#type').val();
		/* Changed by Leelaprasad for the defect T25IT-132 starts*/
		$('#txtClient').show();
		$('#txtBrowser').show();
		/* Changed by Leelaprasad for the defect T25IT-132 ends*/
		if(val==1){
			
			$('#tCase').show();
			$('#tset').hide();
			$('#tasktype').hide();
			populateAllTestCases();
		}
		if(val==2)
			{
			$('#tset').show();
			$('#tCase').hide();
			populateAllTestSets();
			}
		/*Added by Gangadhar Badagi to hide the testCase,testSet when type is selected as select one starts*/
		if(val==0){
			$('#tset').hide();
			$('#tCase').hide();
			$('#tasktype').hide();
		}
		/*Added by Gangadhar Badagi to hide the testCase,testSet when type is selected as select one ends*/
	});
	/*Added by Gangadhar Badagi for TENJINCG-292 ends*/
	
	$('#txtTestSet').on('change', function() {
		/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
		if($('#type').val()==2){
			/*Added by Gangadhar Badagi for TENJINCG-292 ends*/
		var tsId=$('#txtTestSet').val();
		/* Changed by Leelaprasad for the defect T25IT-132 starts*/
		var mode=$('#txtTestSet option:selected').data("mode");
		if(mode=="API"){
			$('#txtClient').hide();
			$('#txtBrowser').hide();
		}else{
			$('#txtClient').show();
			$('#txtBrowser').show();
		}
		/* Changed by Leelaprasad for the defect T25IT-132 ends*/
		$.ajax({
			url:'TestSetServlet',
			/*Modified by Roshni for  V2.8-96  starts*/
			/*data:{param:"mapped_tcs1",t1:tsId},*/
			data:{param:"populate_tcs",t1:tsId},
			/*Modified by Roshni for  V2.8-96  ends*/
			dataType:'json',
			success:function(data){
				if(data.status == 'SUCCESS'){
					/*Modified by Preeti for V2.8-113 starts*/
					$('#tblNaviflowModules').html('');
					$('#tblNaviflowModules_body').html('');
					var testCases=data.tests;
					var html='<thead><tr><th>Test Case Id</th><th>Test Case Name</th></tr></thead>';
					html=html+"<tbody id='tblNaviflowModules_body'>";
					for(i=0;i<testCases.length;i++){
						var addClass='';
						if(i%2==0)
							addClass = 'odd';
						else
							addClass = 'even';
						var tc = testCases[i];
						html = html + "<tr class='"+addClass+"'><td>"+tc.id+"</td><td>"+tc.TC_NAME +"</td></tr>";
					}
					html=html+"</tbody>";
					/*$('#pagination_block').show();*/
					/*$('#tblNaviflowModules').html('');*/
					$('#tblNaviflowModules').html(html);
					$('#tasktype').show();
					$('#tblNaviflowModules').dataTable();
					$('#tblNaviflowModules').DataTable().destroy();
					$('#tblNaviflowModules').dataTable();
					/*paginate();*/
					/*Modified by Preeti for V2.8-113 ends*/
				}else{
					showMessage(data.message,'error');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
			}
		});
		}
	});
	/* Changed by Leelaprasad for the defect T25IT-132 starts*/
	$('#txtTestCase').on('change', function() {
		var mode=$('#txtTestCase option:selected').data("mode");
		if(mode=="API"){
			$('#txtClient').hide();
			$('#txtBrowser').hide();
		}else{
			$('#txtClient').show();
			$('#txtBrowser').show();
		}
	});
	/* Changed by Leelaprasad for the defect T25IT-132 starts*/

	/*changed by sahana for Req#TJN_243_03:starts*/
	/* Changed by Leelaprasad for the requirement defect T25IT-91 starts */
	var i=0;
	/* Changed by Leelaprasad for the requirement defect T25IT-91 starts */
	$('#btnSchRecurrence').click(function(){
		var val= $('#datepicker').val();
		var hour=$('#btntime').val();
		/* Changed by Leelaprasad for the requirement defect T25IT-91 starts */
		var frequency="";
		var recurCycle="";
		var recurDays=""; 
		var endDate="";
	
		if(i!=0){
		 frequency=$('#frequency').val();
		 recurCycle=$('#recurCycle').val();
		 recurDays= $('#recurDays').val();
		 recurDays= $('#recurDays').val();
		 if(frequency=="weekly"){
		 var str=recurDays.split(",");
		 recurDays="";
		 for(var i=0;i<str.length;i++){
			 recurDays=recurDays+str[i]+":";
		 }
		}
		 endDate=$('#endDate').val();
		}
		$('.recurDetails').remove();
		/* Changed by Leelaprasad for the requirement defect T25IT-91 starts */
		if($('#datepicker').val()!=""){
			/* Changed by Leelaprasad for the requirement defect T25IT-91 starts */
			/*$('#recur-sframe').attr('src','schedule_recurrence.jsp?val='+val+','+hour);*/
			/*Modified by padmavathi to display proper label alignments in Recurrence Preferences page starts*/
			/*$('#recur-sframe').attr('src','schedule_recurrence.jsp?val='+val+','+hour+','+frequency+','+recurCycle+','+recurDays+','+endDate);*/
			$('#recur-sframe').attr('src','schedule_recurrence.jsp?val='+val+','+hour+','+frequency+','+recurCycle+','+recurDays+','+endDate+ '&paramval=recurrence');
			/*Modified by padmavathi to display proper label alignments in Recurrence Preferences page ends*/
			/* Changed by Leelaprasad for the requirement defect T25IT-91 starts */
			$('.modalmask').show();
			$('.subframe').show();
			i++;
		}
		else{
			showMessage("Please enter the Date and Time of the Scheduler Task",'error'); 
			e.preventDefault();
		}
	});
	/*changed by sahana for Req#TJN_243_03:ends*/
	/*Modified by Preeti for TENJINCG-928 starts*/
	window.setInterval(function(){
		var today = new Date();
		/*$('#date').text(new Date().toString("dd mmm, yyyy, hh:MM:ss"));*/
		$('#date').text(today.toShortFormat());
	}, 1);
	
	Date.prototype.toShortFormat = function() {

	    var month_names =["01","02","03",
	                      "04","05","06",
	                      "07","08","09",
	                      "10","11","12"];
	    
	    var day = this.getDate();
	    var month_index = this.getMonth();
	    var year = this.getFullYear();
	    var hours = this.getHours();
	    var min = this.getMinutes();
	    var sec = this.getSeconds();
	    
	    return "" + year + "-" + month_names[month_index] + "-" + day +" " + hours +":" +min +":"+sec +" (GMT+0530 IST)";
	}
	/*Modified by Preeti for TENJINCG-928 ends*/
	$('#datepicker').on('change', function() {
		var name=$('#datepicker').val(); 
		hoursVal=$("#btntime").val();
		hoursVal=populateHours(Date.parse(name));
		populateMinutes(Date.parse(name));
	});
	
	$('#btntime').on('change', function(){	
		var html="<option value='00'>00</option>";
		var timeval=$(this).val();
		if(hoursVal!=timeval){
			if(timeval!=null){
				for(var i=1;i<12;i++){
					if(i==1)
						html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";
					else
						html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
				}
				$('#btntime1').html(html);
			}
		}
		else{
			var name=$('#datepicker').val(); 
			hoursVal=$("#btntime").val();
			hoursVal=populateHours(Date.parse(name));
			populateMinutes(Date.parse(name));
		}
	});
	/*changed by sahana for Req#TEN-117:starts*/
	/*	$('#taskName').keyup(function (){
		var inputVal = $(this).val();
		var regex = new RegExp(/[^a-zA-Z0-9]/);
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			alert('Please enter only alphanumeric characters');
			$(this).val('');
		}
	});*/
	/*changed by sahana for Req#TEN-117:starts*/
	/*changed by sahana for Req#TJN_243_03:starts*/
	$('#btnSchSave').click(function(){
		/*changed by sahana for Req#TJN_243_03:ends*/
		clearMessages();
		/*Modified by Preeti for TV2.8R2-11 starts*/
		/*var validated = validateForm('new_schedule');
		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
		}*/
		var task = $('#taskName').val();
		if(!task){
			showMessage('Task Name is required.','error');
			return false;
		}
		/*Modified by Preeti for TV2.8R2-11 ends*/
		/* Changed by Leelaprasad for the defect T25IT-132 starts*/
		var mode='';
		if($('#type').val()==2){
			mode=$('#txtTestSet option:selected').data("mode");
			
		}else{
			 mode=$('#txtTestCase option:selected').data("mode");
		}
		if(mode!="API"){
			var client=$('#txtclientregistered').val();
			if(client === '-1' || client ==='' || client == undefined) {
				showMessage('Client is required.','error');
				return false;
			}
		}
		/* Changed by Leelaprasad for the defect T25IT-132 starts*/
		
		/*Added by Preeti for T251IT-123 starts*/
		var selDate=$('#datepicker').val();
		if(!selDate){
			showMessage('Date is required.','error');
			return false;
		}
		/*Added by Padmavathi for TENJINCG-1014 starts*/
		if(!validateDate(selDate)){
			showMessage('Please enter valid Date in this format DD-MMM-YYYY.','error');
			return false;
		}
		/*Added by Padmavathi for TENJINCG-1014 ends*/
		var selhour=$('#btntime').val();
		var selMinute=$('#btntime1').val();
		var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
		var today=new Date();
		var curDate=today.getDate()+'-'+months[today.getMonth()]+'-'+today.getFullYear();
		/*Added by Padmavathi for TENJINCG-1014 starts*/
		if((new Date(curDate).getTime()) >= (new Date(selDate).getTime())){
			/*Added by Padmavathi for TENJINCG-1014 ends*/
		if((new Date(curDate).getTime()) == (new Date(selDate).getTime())){
			var curHour=today.getHours();
			var curMinute=today.getMinutes();
			if(selhour<curHour){
				alert("Selected Time should be greater than or equal to Current Time");
				return false;
			}
			if(selhour==curHour){
				if(selMinute<curMinute){
						alert("Selected Time should be greater than or equal to Current Time");
						return false;
				}
			}
		}/*Added by Preeti for T251IT-123 ends*/
		/*Added by Padmavathi for TENJINCG-1014 starts*/
		else{
			alert("Selected Date should be greater than or equal to Today's Date");
			return false;
		}
		}
		/*Added by Padmavathi for TENJINCG-1014 ends*/
		
		
		var time=$('#btntime').val()+":"+$('#btntime1').val();
		
		var obj = new Object();

		obj.date = $('#datepicker').val();
		/*changed by sahana for Req#TJN_243_03:starts*/
		var hour=$('#btntime1').val();
		if(hour!=null)
			obj.time = time;
		/*changed by sahana for Req#TJN_243_03:ends*/
		obj.task = $('#txtTask').val();
		obj.client = $('#txtclientregistered').val();
		obj.status = $('#txtStatus').val();
		obj.crtBy = $('#txtCrtBy').val();
		obj.crtOn = $('#txtCrtOn').val();
		obj.browser = $('#lstBrowserType').val();
		obj.autLogin = $('#lstAppUserType').val();
		/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
		var type=$('#type').val();
		if(type==1){
		var tcId=$('#txtTestCase').val();
		/*Added by Gangadhar Badagi to validate mandate field TestCase starts*/
		if(tcId=='-1'){
			showMessage("Test Case is required",'error');
			return false;
		}
		else{
			/*Added by Gangadhar Badagi to validate mandate field TestCase ends*/
			var jsonObj=createTs(tcId);
			var testSet=jsonObj.TEST_SET;
			var testSetId=jsonObj.testSetId;
			obj.testSetId=testSetId;
			/*Changed by Leelaprasad for the requirement defect fix T25IT-60 starts*/
			obj.type=type;
			/*Changed by Leelaprasad for the requirement defect fix T25IT-60 ends*/
		}
		}
		if(type==2){
			/*Added by Gangadhar Badagi to validate mandate field TestSet starts*/
			if($('#txtTestSet').val()=='-1'){
				showMessage("Test Set is required",'error');
				return false;
			}
			else{
				/*Added by Gangadhar Badagi to validate mandate field TestSet ends*/
				obj.testSetId=$('#txtTestSet').val();
				/*Changed by Leelaprasad for the requirement defect fix T25IT-60 starts*/
				obj.type=type;
				/*Changed by Leelaprasad for the requirement defect fix T25IT-60 ends*/
			}
			
		}
		/*Added by Gangadhar Badagi to check for mandate field type starts*/
		if(type==0){
			showMessage("Type is required",'error');
			return false;
		}
		/*Added by Gangadhar Badagi to check for mandate field type ends*/
		/*Added by Gangadhar Badagi for TENJINCG-292 ends*/
		/*changed by sahana for req#TJN_72 starts*/
		obj.taskName=$('#taskName').val();
		/*changed by sahana for req#TJN_72 Ends*/
		/*changed by sahana for improvement #TENJINCG-61: starts*/
		obj.screenShot=$('#screenShot').val();
		/*changed by sahana for improvement #TENJINCG-61: ends*/
		if(obj.task!='Execute'){
			obj.projectId=0;
		}
		else{
			obj.projectId=$('#projId').val();
		}
		/*changed by sahana for Req#TJN_243_03:starts*/
		var flag=false;
		var frequency=$('#frequency').val();
		if(typeof frequency!="undefined"){
			obj.frequency=$('#frequency').val();
			if(obj.frequency=='daily'){
				obj.recurCycles=$('#recurCycle').val();	
				obj.recurDays=$('#recurDays').val();
			}	
			else if(obj.frequency=='weekly'){
				obj.recurCycles=0;
				obj.recurDays=$('#recurDays').val();
			}
			else if(obj.frequency=='daily-Repetitive'){
				obj.recurCycles=$('#recurCycle').val();	
				obj.recurDays='N/A';
			}
			if(obj.frequency!='daily-Repetitive')
				obj.endDate=$('#endDate').val();
			flag=true;
		}
		if(flag==true){
			obj.schRecur='Y';
		}
		else{
			obj.schRecur='N';
		}
		/*changed by sahana for Req#TJN_243_03:ends*/
		var jsonString = JSON.stringify(obj);
		var csrftoken_form=$('#csrftoken_form').val();
		/*changed by sahana for Req#TJN_243_03:starts*/
		if(hour==null)
			showMessage('Invalid minute(s) entry','error');
		else{
			/*changed by sahana for Req#TJN_243_03:ends*/
			$.ajax({
				url:'SchedulerServlet',
				data:{param:"NEW_SCH",json:jsonString,functions:"TestCase",csrftoken_form:csrftoken_form},
				type: 'POST',
				async: false,
				dataType:'json',
				success:function(data){
					if(data.status == 'success'){
						showMessage(data.message,'success');
						/*Commented by Ashiki for V2.8-63 starts*/
						/*window.setTimeout(function(){window.location='SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=Scheduled&id='+obj.projectId},500)*/
						/*Commented by Ashiki for V2.8-63 ends*/
						//window.setTimeout(function(){window.location='SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id='+obj.projectId},500);
					}else{
					/*Modified by Padmavathi for TV2.8R2-20 & 22 for starts*/
						var array = data.message.replace( /\n/g, ";" ).split( ";" );
						if(array.length)
						var newHTML = [];
						$.each(array, function(index, value) {
						    newHTML.push('<span>' + value + '</span><br>');
						});
						$('#errorMessage').html(newHTML.join(""));
						showMessage('Could not create schedule, Please click Logs button for more information.','error');
						$('#btnLogs').show();
						/*Modified by Padmavathi for TV2.8R2-20 & 22 for ends*/
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(errorThrown,'error');
				}
			});
			/*changed by sahana for Req#TJN_243_03:starts*/
		}
		/*changed by sahana for Req#TJN_243_03:ends*/
	});
	/*Added by Preeti for TV2.8R2-21 starts*/
	$('#btnCancelTask').click(function(){
		var projectId = $('#projId').val();
		var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
		if(confirmResult) {
			window.location.href='SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id='+projectId;
		}else{
			return false;
		}
	});
	/*Added by Preeti for TV2.8R2-21 ends*/
});
/*Added by Prem for TJN2.9R2-4 starts*/
$(document).on('click','#btnRefreshAut', function() {
	if ( confirm("Unsaved changes will be cleared, this action cannot be undone")){
	window.location.href ='schedule_new.jsp ';
	}
});
/*Added by Prem for TJN2.9R2-4 starts*/
/*changed by sahana for Req#TJN_243_03:starts*/
function closeModal(){
	$('#recur-sframe').attr('src','');
	$('.subframe').hide();
	$('.modalmask').hide();	

}


/*changed by sahana for Req#TJN_243_03:ends*/
function populateAllTestSets(){

	var jsonObj = fetchAllTestSets();

	var status = jsonObj.status;
	clearMessages();
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jArray = jsonObj.tSets;

		for(i=0;i<jArray.length;i++){
			var set = jArray[i];
			/* Changed by Leelaprasad for the defect T25IT-132 starts*/
			/*html = html + "<option value='" + set.id +"'>" + set.name + "</option>";*/
			html = html + "<option value='" + set.id +"' data-mode='"+set.mode+"'>" + set.name + "</option>";
		    /* Changed by Leelaprasad for the defect T25IT-132 ends*/
		}

		$('#txtTestSet').html(html);

	}else{
		showMessage(jsonObj.message, 'error');
	}
}
/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
function populateAllTestCases(){

	var jsonObj = fetchAllProjectTestCases();

	var status = jsonObj.status;
	clearMessages();
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jArray = jsonObj.tcs;

		for(i=0;i<jArray.length;i++){
			var testCase = jArray[i];
		/* Changed by Leelaprasad for the defect T25IT-132 starts*/
		/*html = html + "<option value='" + testCase.recid +"'>" + testCase.name + "</option>";*/
			html = html + "<option value='" + testCase.recid +"' data-mode='"+testCase.mode+"'>" + testCase.name + "</option>";
		/* Changed by Leelaprasad for the defect T25IT-132 ends*/
		}

		$('#txtTestCase').html(html);

	}else{
		showMessage(jsonObj.message, 'error');
	}
}
/*Added by Gangadhar Badagi for TENJINCG-292 ends*/
function populateClients(){

	var jsonObj = fetchAllClients();

	var status = jsonObj.status;
	clearMessages();
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jArray = jsonObj.clients;

		for(i=0;i<jArray.length;i++){
			var client = jArray[i];
			html = html + "<option value='" + client.name + "'>" + client.name + "</option>";
		}

		$('#txtclientregistered').html(html);

	}else{
		showMessage(jsonObj.message, 'error');
	}
}
/*changed by sahana for defect #TEN-139  :starts*/
function populateHours(date1){
	/*Changed by Leelaprasad for the defect T25IT-92 starts*/
	var hoursVal= "";
	/*Changed by Leelaprasad for the defect T25IT-92 ends*/
	var dateval=new Date(date1);
	var month=dateval.getMonth()+1;
	var finaldate=dateval.getDate()+"-"+month+"-"+dateval.getFullYear();
	var curmonth=new Date().getMonth()+1;
	var currDate=new Date().getDate()+"-"+curmonth+"-"+new Date().getFullYear();
	var html ="";
	var getMin=new Date().getMinutes();
	var counter=0;
	var currentHour=new Date().getHours();
	var newHour;
	if(getMin>55 && getMin<=60)
	{
		for(var i=0;i<24;i++){
			if(currentHour==parseInt(i))
			{	
				if(parseInt(i)<10){
					newHour="0"+parseInt(i+1);
				}
				else{
					newHour=i+1;
				}
				if(i==currentHour){
					html = html + "<option value='" +newHour + "' selected='selected'>" + newHour + "</option>";
				}
				else{
					html = html + "<option value='" + newHour+ "'>" + newHour + "</option>";
				}
				i=i+1;
			}
			else if(i>currentHour+1){
				if(parseInt(i)<10){
					newHour="0"+parseInt(i);
				}
				else{
					newHour=i;
				}
				if(i==currentHour){
					html = html + "<option value='" +newHour + "' selected='selected'>" + newHour + "</option>";
				}
				else{
					html = html + "<option value='" + newHour+ "'>" + newHour + "</option>";
				}


			}
			/*Added by  Gangadhar Badagi to schedule the task between 55-60 minutes starts*/
			else{
				var newTime="";
				if(i<=9)
					newTime ="0"+i;
				else
					newTime=i;
				if(finaldate==currDate){
					if(newTime>=new Date().getHours()){

						if(counter==0){
							hoursVal=newTime;
							counter++;
						}
						html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
					}
				}
				else{	
					if(counter==0){
						hoursVal=newTime;
						counter++;
					}
					html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
				}
				
			}
			
			/*Added by  Gangadhar Badagi to schedule the task between 55-60 minutes ends*/

		}
	}
	else
	{
		for(var i=0;i<24;i++){
			var newTime="";
			if(i<=9)
				newTime ="0"+i;
			else
				newTime=i;
			if(finaldate==currDate){
				if(newTime>=new Date().getHours()){

					if(counter==0){
						hoursVal=newTime;
						counter++;
					}
					html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
				}
			}
			else{	
				if(counter==0){
					hoursVal=newTime;
					counter++;
				}
				html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
			}

		}
	}
	$('#btntime').html(html);
	return hoursVal;
}
function populateMinutes(date1){
	var html="";
	var dateval=new Date(date1);
	var month=dateval.getMonth()+1;
	var finaldate=dateval.getDate()+"-"+month+"-"+dateval.getFullYear();
	var curmonth=new Date().getMonth()+1;
	var currDate=new Date().getDate()+"-"+curmonth+"-"+new Date().getFullYear();
	var counter=0;
	var getHour=$('#btntime').val();
	var getMin=new Date().getMinutes();
	if(parseInt(getMin)>55 && parseInt(getMin)<=60)
	{
		for(var i=0;i<12;i++){
			if(i==0)
				html=html + "<option value='00'>00</option>";
			else if(i==1)
				html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";			
			else
				html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
		}
	}
	else{
		for(var i=0;i<12;i++){
			if(finaldate==currDate){
				if((i*5)>=parseInt(getMin)){
					if((i*5)<10)
						html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";			
					else
						html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
				}
			}
			else{
				if(i==0)
					html=html + "<option value='00'>00</option>";
				else if(i==1)
					html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";			
				else
					html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
			}
		}
	}
	$('#btntime1').html(html);
}
/*changed by sahana for defect #TEN-139  :ends*/

/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
function createTs(tcId)
{
	var jsonObj=null;
	$.ajax({
		url:'SchedulerServlet',
		data:'param=create_testset&tc=' + tcId,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			obj.status = "ERROR";
			obj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});
	
	return jsonObj;
	}
/*Added by Gangadhar Badagi for TENJINCG-292 ends*/

