/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  metadata_override.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY     			DESCRIPTION
* 30/08/2016			Avinash					Changes 30-08-2016 - Tenjin v.2.3 Enhancement on Metadata
* 09-11-2016            Parveen                 Defect #796
*/

$(document).ready(function(){
	$('#accordion').accordion({heightStyle:'content'});
	
    $('.progress_mask').hide();
	 
	  /*$('#export').click(function(){
		  var validationStatus = validateForm('aut_metadata_form');
			// SUCCESS
			if (validationStatus != 'SUCCESS') {
				showMessage(validationStatus, 'error');
				return false;
			}

		  var retValue = dataToBeExport();
		  if(retValue != undefined){
			  $('.progress_mask').show();
		  }
		  window.setTimeout(function(){
			  if(retValue != undefined){
				  var flag = true;
				  var app_id = $('#lstApplication').val();
				  var fileName = $('#txtFileName').val();
				  $.ajax({
						url:'MetaDataServlet',
						data :'param=export&data='+retValue+'&app_id='+app_id+'&fileName='+fileName,
						type : 'post',
						async:true,
						dataType:'json',
						success:function(data){
							var status = data.status;
							if(status == 'SUCCESS'){
								var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
								$("body").append($c);
			                    $("#downloadFile").get(0).click();
			                    $c.remove();
			                    $('.progress_mask').hide();
			                    flag = false;
			                    //$('#report-gen').prop('disabled',false);
			            		//$('#img_report_loader').hide();
							}else{
								var message = data.message;
								showMessage(message,'error');
								$('#report-gen').prop('disabled',false);
								$('#img_report_loader').hide();
							}
						},
						error:function(xhr, textStatus, errorThrown){
							$('#report-gen').prop('disabled',false);
							$('#img_report_loader').hide();
							showMessage(errorThrown, 'error');
						}
				     });  
				 if(flag){
				  window.setInterval(function(){
						$.ajax({
							url : "MetaDataServlet",
							type:"GET",
							async : true,
							data : {param : "progress_bar"},
							dataType:'json',
							success : function(data) {
								if(data.status == 'success'){
									var total = data.total;
									var counter = data.counter;
									var percentage = 0;
									if(counter == 0){
										percentage = 1;
									}else{
										percentage = ((counter/total)*100).toFixed(0);
									}
									var cfunc = data.cfunc;
									$('#txtCurrentFunc').val(cfunc);
									
									
									$('#txtcounter').val(counter+1);
									
									
									$('#txttotal').val(total);
									
									progressBar(percentage, $('#progressBar'));
								}				
							},

							error : function(xhr, textStatus, errorThrown) {
								showMessage(errorThrown, 'error');
							}

						});	
					}, 1000);
				 }
				  
			  }else if($('.application_mask').is(':visible')){
				  showMessage('Please select any one Application');
			  }else if($('.function_mask').is(':visible')){
				  showMessage('Please select any one Function');
			  }else{
				  showMessage('Please select any one Option');
			  }  
		  }, 1000);
	  });
*/	  
	 
	 $('#override').click(function(){
		 var appName = $('#appName').val();
		 var retValue = dataToBeExport();
		 if(retValue != undefined){
		 var confirmValue = confirm("Are you sure you want to proceed with the Import? This action cannot be undone.");
		if(confirmValue){
		 if(retValue != undefined){
			  $('.progress_mask').show();
		  }
		 	  window.setTimeout(function(){
				  $.ajax({
						url:'MetaDataUploadServlet',
						data :'param=override&data='+retValue+'&app='+appName,
						type : 'get',
						async:true,
						dataType:'json',
						success:function(data){
							var status = data.status;
							var message = data.message;
							if(status == 'SUCCESS'){
								$('.progress_mask').hide();
								window.clearInterval(interval);
			                    window.location.href = "metadata_import.jsp";
			                    showMessage(message,'success');
							}else{
								showMessage(message,'error');
								$('.progress_mask').hide();
							}
						},
						error:function(xhr, textStatus, errorThrown){
							showMessage(errorThrown, 'error');
						}
				     });  
				  
				  var interval = window.setInterval(function(){
						$.ajax({
							url : "MetaDataUploadServlet",
							type:"GET",
							async : true,
							data : {param : "progress_bar"},
							dataType:'json',
							success : function(data) {
								if(data.status == 'success'){
									var total = data.total;
									var counter = data.counter;
									var percentage = 0;
									if(counter == 0){
										percentage = 1;
									}else{
										percentage = ((counter/total)*100).toFixed(0);
									}
									
									$('#txtcounter').val(counter+1);
									
									$('#txttotal').val(total);
									
									progressBar(percentage, $('#progressBar'));
								}				
							},

							error : function(xhr, textStatus, errorThrown) {
								showMessage(errorThrown, 'error');
							}

						});	
					}, 1000);
			  } ,1000);
		 }
		}else{
			  showMessage('Select any one Function');
		  }
		  });
	 
	  function dataToBeExport(){
		  /*Code For Import Multiple AUTs*/
		 /* var retValue='';
		  $('input:checkbox[id^=chk_all_modules]').each(function(){
			  var id = $(this).prop('id');
			  var suffix = id.substring(id.length-1);
			 if($(this).is(':checked')){
				 var appName = id.substring(15, id.length-1);
				 retValue += appName+';';
			 }else{
				 var funcids = getAllCheckedApps(suffix);
		         retValue += funcids;
			 } 
		  });
		  return retValue;*/
		  /*Code For Import Multiple AUTs*/
		  var retValue;
		  
		  /*if($('.application_mask').is(":visible")){
			  var temp;
			  var selectedApps = '';
			  retValue = 'app;'
			  if($('#chk_all_apps').is(':checked')){
				  selectedApps += 'allAut;';
				  alert(selectedApps);
			  }else{
				  selectedApps += 'selected_auts;';
				  temp = selectedApps;
				  $("input:checkbox[name=appName]:checked").each(function () {
					selectedApps += $(this).val() + ';';
				});
			  }
			  if(temp == selectedApps){
				  retValue = undefined;
			  }else{
			  retValue += selectedApps;
			  }
		  }else if($('.function_mask').is(':visible')){*/
			  var temp;
			  var selectedModules = '';
			  if($('#chk_all_modules').is(':checked')){
				var totcount = 0;
				var checked = 0;
					  $('input:checkbox[name="fcode"]').each(function(){
							totcount++;
							var checkBoxVal = (this.checked ? $(this).val() : "");
							if(checkBoxVal!=""){
								checked++;
							}
						});
					  
					  if(checked != totcount){
						  temp = selectedModules;
						  $("input:checkbox[name=fcode]:checked").each(function () {
								 selectedModules += $(this).val() + ';';
							    });
					  }else if(checked == totcount){
						  retValue = 'all;';
					  }
			  }else{
				  temp = selectedModules;
				  $("input:checkbox[name=fcode]:checked").each(function () {
						 selectedModules += $(this).val() + ';';
					});
			  }	
			  if(temp == selectedModules){
				  retValue = undefined;
			  }else{
			  retValue += selectedModules;
			  }
		  /*}*/
		  return retValue;
	  }
	  
	  $('#btnBack').click(function(){
		 window.location.href = "metadata_import.jsp"; 
	  });
	  
	  $('input:checkbox[name^=func]').click(function(){
		  var name = $(this).prop('name');
		  var suffix = name.substring(name.length-1);
			 var totcount = 0;
			 var checked = 0;
			  $('input:checkbox[name^=func'+suffix+']').each(function(){
					totcount++;
					var checkBoxVal = (this.checked ? $(this).val() : "");
					if(checkBoxVal!=""){
						checked++;
					}
				});
			  
			  if(checked != totcount){
				  if($('input:checkbox[id^=chk_all_modules][id$='+suffix+']').is(':checked')){
					  $('input:checkbox[id^=chk_all_modules][id$='+suffix+']').prop('checked', false);
				  }
			  }else if(checked == totcount){
				  $('input:checkbox[id^=chk_all_modules][id$='+suffix+']').prop('checked', true);
			  }
		  });
	  
	  $('input:checkbox[id^=chk_all_modules]').change(function(){
		  var id = $(this).prop('id');
		  
		  var suffix = id.substring(id.length-1);
		  if($('input:checkbox[id^=chk_all_modules]').is(':checked')){
			  $('input:checkbox[name=func'+suffix+']').each(function(){
				  if($(this).is(':checked')){
					  
				  }else{
					  $(this).prop('checked', true);  
				  }
			  });
		  }
		  else{
			  $('input:checkbox[name=func'+suffix+']').each(function(){
				  if($(this).is(':checked')){
					  $(this).prop('checked', false);
				  }else{
					    
				  }
			  });
		  }
	  });
	  
	  $('input:checkbox[name="fcode"]').change(function(){
		  	$('.function_mask').show();
			 var totcount = 0;
			 var checked = 0;
			  $('input:checkbox[name="fcode"]').each(function(){
					totcount++;
					var checkBoxVal = (this.checked ? $(this).val() : "");
					if(checkBoxVal!=""){
						checked++;
					}
				});
			  if(checked == 0){
				  checked = 1;
			  }
			  
			  /*Changed by parveen for Defect #796 starts */
			 // alert(totcount + ','+checked);
			  //alert('Total Enteries are '+totcount + ', And You Have Selected '+checked);
			  /*Changed by parveen for Defect #796 ends */
			  if(checked != totcount){
				  if($('input:checkbox[id="chk_all_modules"]').is(':checked')){
					  $('input:checkbox[id="chk_all_modules"]').prop('checked', false);
				  }
			  }else if(checked == totcount){
					  $('input:checkbox[id="chk_all_modules"]').prop('checked', true);
			  }
		  });
	  
	  $('input:checkbox[id="chk_all_modules"]').change(function(){
		  if($('input:checkbox[id="chk_all_modules"]').is(':checked')){
			  $('input:checkbox[name="fcode"]').each(function(){
				  if($(this).is(':checked')){
					  
				  }else{
					  $(this).prop('checked', true);  
				  }
			  });
		  }
		  else{
			  $('input:checkbox[name="fcode"]').each(function(){
				  if($(this).is(':checked')){
					  $(this).prop('checked', false);
				  }else{
					    
				  }
			  });
		  }
	  });

		$('#lstApplication').change(function(){
			var selApp = $(this).val();
			if(selApp == '-1'){
				
			}else{
				$('#tblMetadataModules').find('tr').not(':first').remove();
				var options='';
				  $.ajax({
					url:'MetaDataServlet',
					data :'param=get_func'+ '&app_id=' + selApp,
					async:false,
					dataType:'json',
					success: function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						//showMessage(data.message, 'success');	
						var dArray = data.modules;
						for(var i=0;i<dArray.length;i++){
							var fun = dArray[i];
							options += '<tr>'+
							'<td class="tiny"><input type="checkbox" name="fcode" value="'+fun.code+'" id="'+fun.code+'"/></td>'+
							'<td>'+ fun.code +'</td>'+
							'</tr>';
						}
						$('#tblMetadataModules_body').html(options);
						$('#tblMetadataModules').dataTable();
					  }
					  else{
						showMessage(data.message, 'error');				
					  }
				    },
				   error:function(xhr, textStatus, errorThrown){
					   showMessage(textStatus, 'error');			
				  }
			     });
			}
		});

	  
	  function clearAppCheckbox(){
		  $('input:checkbox[name="appName"]').each(function(){
			  var checkBoxVal = (this.checked ? $(this).val() : "");
				if(checkBoxVal!=""){
					$(this).prop('checked', false);
				} 
			});
		  $('input:checkbox[id="chk_all_apps"]').prop('checked',false);
	  }
	  
	  function clearModuleCheckbox(){
		  $('input:checkbox[name="fcode"]').each(function(){
			  var checkBoxVal = (this.checked ? $(this).val() : "");
				if(checkBoxVal!=""){
					$(this).prop('checked', false);
				} 
			});
	  }
	  
	  function getAllCheckedApps(suffix){
			var checkedModules = '';
			$('input:checkbox[name=func'+suffix+']').each(function(){
				var checkBoxVal = (this.checked ? $(this).val() : "");
				if(checkBoxVal!=""){
				checkedModules += checkBoxVal+';';
				} 
			});
			return checkedModules;
		}
});
