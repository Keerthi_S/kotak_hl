/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  notification.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 18-Nov-16			nagareddy					notification for manual input field
*/
function notifyMe(loc,fld,contextpath) {
	
	
	  if (Notification.permission !== "granted")
		    Notification.requestPermission();
		  else {
			  var rul;
			  try{
			   rul=contextpath+'/images/manualnotify.png';
			  }catch(err){}
			
			var options={tag:'custom',
				     // icon: 'http://localhost:8080/TenjinWeb/images/manualnotify.png',
				    //  iconUrl:'http://localhost:8080/TenjinWeb/images/manualnotify.png', //firefox 'http://localhost:8080/TenjinWeb/images/Manual_Entry.png'
				    icon:rul,iconUrl:rul,  body: "Please input data for '"+fld+"' and  click continue button in Tenjin Run Screen!",
				    }
		    //var notification = new Notification('Manual Input Mode Activated on' +loc, options);
			 var notification = new Notification(loc, options);

		    notification.onclick = function () {
		      //window.open("http://stackoverflow.com/a/13328397/1269037"); 
		    	window.open("http://localhost:8080/TenjinWeb/runprogress.jsp");
		    };
		    
		  }
	  
}