
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testmanager_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 25-03-2019		   Pushpalatha			   TENJINCG-968
* 28-05-2019		   Ashiki				   V2.8-63
*/	

/*
 * Added File by Parveen for Requirement TENJINCG_53
 */

$(document).ready(function(){
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('tm_edit_form');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	$('#txtName').change(function (){
		   var name=$('#txtName').val();
				$.ajax({
					url:'TestManagerServlet',
					data :{param:"CHECK_NAME",paramval:name},
					async:false,
					dataType:'json',
					success: function(data){
					var status = data.status;
					if(status == 'ERROR'){		
						showMessage(data.message, 'error');
						$("#txtName").focus();
					}
				},
				
				error:function(xhr, textStatus, errorThrown){
					showMessage(errorThrown, 'error');			
				}
			});
			}); 
			
		var json1 = new Object();
		
		json1.name1 = $('#txtName').val();
		json1.tool1 = $('#txtTool').val();
		json1.url1 = $('#txtUrl').val();
		
		var jsonString1 = JSON.stringify(json1);
	
	
	
	$('#btnSave').click(function(){
		
		var validated = validateForm('tm_edit_form');
		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
		}
		
		var json = new Object();
		
		json.name = $('#txtName').val();
		json.tool = $('#txtTool').val();
		json.url = $('#txtUrl').val();
	    var jsonString = JSON.stringify(json);
		$.ajax({
			url:'TestManagerServlet',
			data:{param:"EDIT_TM",json:jsonString,json1:jsonString1},
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status =='success'){			
					showMessage(data.message,'success');	
					/*Commented by Ashiki for V2.8-63 starts*/
					/*window.setTimeout(function(){window.location='TestManagerServlet?param=FETCH_ALL';},500)*/
					/*Commented by Ashiki for V2.8-63 ends*/
				}else{
					showMessage(data.message, 'error');				
				}
			},
			
			error:function(xhr, textStatus, errorThrown){
				showMessage(data.message, 'error');			
			}
		});
	});
	
	
	
	
	$('#btnBack').click(function(){	
	window.location.href='TestManagerServlet?param=FETCH_ALL';
	});
	
});