/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  defect.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 * DATE CHANGED BY DESCRIPTION
 * 15-06-2018             Padmavathi              T251IT-41
 * 18-06-2018             Padmavathi              T251IT-48
 * 10-12-2018			  Pushpa				  TJNUN262-103,TJNUN262-104
 * 26-03-2019			  Pushpalatha			  TENJINCG-968
 * 11-12-2020			  Pushpalatha			  TENJINCG-1221
 */
var storedFiles = [];
var removedFiles = [];
$(document).ready(function(){
	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('new_defect_form');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	/*Added by Pushpa for TENJINCG-1221 starts*/
	$('#subsetBlock').hide();
	$('#ownerBlock').hide();
	/*Added by Pushpa for TENJINCG-1221 ends*/
	
	$('#txtFile').on("change", handleFileSelect);
	selDiv = $('#selectedFiles');
	$('body').on('click','.selFile', removeFile);
	
	$('body').on('click','#btnPClose', function(){
		var callback = $(this).attr('callback');
		if(callback === 'undefined'){
			$('.modalmask').hide();
			$('#processing_icon').html("");
			$('#def_review_message').html('')
			$('#def_review_progress_dialog').hide();
		}else{
			window.location.href=callback;
		}
	});
	$('#new_defect_form').on('submit', handleForm);
	
	
	$('select.stdTextBoxNew').select2();
	
	$('#runid').change(function(){
		var runid = $(this).val();
		if($(this).prop('disabled') === true){
			return false;
		}
		if(runid == '0' || runid=='-1'){
			$('.app_info').prop('disabled',false);
			$('#testcase').html("<option value='-1' selected>-- Select One --</option>");
			$('#teststep').html("<option value='-1' selected>-- Select One --</option>");
			
			$('#testcase').val('-1').change();
			$('#teststep').val('-1').change();
		}else{
			/*$('#fs_app_info').children().removeAttr('disabled');*/
			$('.app_info').prop('disabled',true);
			//Added by Priyanka for TCGST-46 starts
			$(".asterisk").hide(); 
			//Added by Priyanka for TCGST-46 ends 
			populateTestCases(runid);
		}
	});
	
	$('#appid').change(function(){
		var appid = $(this).val();
		if($(this).prop('disabled') === true){
			return false;
		}
		
		if(appid == '0' || appid=='-1'){
			$('.run_info').prop('disabled',false);
			$('#functionId').html("<option value='-1' selected>-- Select One --</option>");
			$('#functionId').val('-1').change();
			clearDTTInformation();
		}else{
			/*$('#fs_app_info').children().removeAttr('disabled');*/
			$('.run_info').prop('disabled',true);
			$('#runid').val('0').change();
			// Added by Priyanka for TCGST-46 starts 
			$(".asteriskV").hide(); 
			//Added by Priyanka for TCGST-46 ends 
			populateModuleInfo(appid);
			populateDTTInformation(appid);
		}
		
		
	});
	
	$('#testcase').change(function(){
	/*Added by Padmavathi for T251IT-48 starts*/
		var runid = $('#runid').val();
		/*Added by Padmavathi for T251IT-48 ends*/
		var tcRecId = $(this).val();
		/*Modified by Padmavathi for T251IT-48 starts*/
		/*populateTestSteps(tcRecId);*/
		populateTestSteps(tcRecId,runid);
		/*Modified by Padmavathi for T251IT-48 ends*/
		
	});
	
	$('#teststep').change(function(){
		var val = $(this).val();
		if(val != '0' && val != '-1'){
			var appid = $('option:selected', this).attr('appid');
			var func = $('option:selected', this).attr('func');
			/*commented by Padmavathi for removing function name starts*/
			/*var funcName = $('option:selected', this).attr('funcname');*/
			/*commented by Padmavathi for removing function name ends*/
			
			/*$('#appid').children('option').each(function(){
				var v = $(this).val();
				if(v == val){
					$(this).attr({'selected':true});
				}
			});*/
			//alert(appid);
			$('#appid').val(appid).change();
			/*Modified by Padmavathi for T251IT-48 starts*/
			/*$('#functionId').html("<option value='" + func + "'>" + func + " - " + funcName + "</option>");*/
			$('#functionId').html("<option value='" + func + "'>" + func + "</option>");
			/*Modified by Padmavathi for T251IT-48 ends*/
			$('#functionId').val(func).change();
			populateDTTInformation(appid)
		}else{
			$('#appid').val('-1').change();
			$('#functionId').html("<option value='-1'>-- Select One --</option>");
			$('#functionId').val('-1').change();
			clearDTTInformation();
		}
	});
	/*Added by Padmavathi for T251IT-41 starts*/
	$('#btnCancel').click(function(){
		/*Modified by Pushpa for TJNUN262-103 starts*/
		if(window.confirm('Are you sure you want to cancel?')){
			window.location.href="DefectServlet?param=fetch_project_defects&pid="+$("#prjId").val()+"&callback=fetch";
		}
		/*Modified by Pushpa for TJNUN262-103 ends*/
	});
	/*Added by Padmavathi for T251IT-41 ends*/
	
	/*$('#txtFile').change(function(){
		 // $('#attachments').html('');
		  var attachments = document.getElementById('txtFile');
		  var item = '';
		  for(var i=0; i<attachments.files.length; i++) {
		    item += '<li>' + attachments.files.item(i).name +
		      '&nbsp;&nbsp;<a href="#" id="'+ i +'" class="dlt-attch">Remove</a>' +
		      '</li>';
		    var path= $('#txtFile').val();
		    $('#txtFile').text(this.value.replace(path," "));
		    
		    
		    console.log(attachments.files.item(i).name);
		    storedFiles.push(attachments)
		  }
		  
		 
		  $('#attachments').append(item);
		});*/
	
	
	$('#btnSave').click(function(){
		var formOk = validateForm('new_defect_form');
		if (formOk != 'SUCCESS') {
			 showMessage(formOk,'error');
			 return false;
		}else{
			$('#post_defect').val('N');
			buildJson();
		}
	});
	
	$('#btnSavePost').click(function(){
		var formOk = validateForm('new_defect_form');
		if (formOk != 'SUCCESS') {
			 showMessage(formOk,'error');
			 return false;
		}else{
			$('#post_defect').val('Y');
			buildJson();
		}
	});
	/*Modified by Padmavathi, changed btnCancel to btnRefresh starts*/
	$('#btnRefresh').click(function(){
		/*Modified by Pushpa for TJNUN262-104 starts*/
		if(window.confirm('Are you sure you want to reset?')){
			window.location.href='DefectServlet?param=init_new_defect';
		}
		/*Modified by Pushpa for TJNUN262-104 ends*/
	});
	/*Modified by Padmavathi, changed btnCancel to btnRefresh ends*/
});

