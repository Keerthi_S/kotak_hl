/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  learnerprogress.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 22-Sep-16			sriram sridharan		CHangegd for Req#TJN_24_05
 * 26-Nov 2018			 Shivam	Sharma			TENJINCG-904
 * 25-01-2019           Sahana 			        pCloudy
 * 10-04-2019			Preeti					TENJINCG-1026
 * 27-08-2019			Ashiki					TENJINCG-1104
 */

$(document).ready(function() {
	$('#modules_table').dataTable();
	/*********************
	 * Changed by Sriram for Req#TJN_24_05
	 */

	var initMessage = $('#lrnrInitMessage').val();
	if(initMessage == 'noerror'){

	}else{
		showMessage(initMessage,'error');
	}

	/*******************
	 * Changed by Sriram for Req#TJN_24_05 ends
	 */

});
$(document).ready(function() {
	/*Added by Sahana for pCloudy: Starts*/
	$("#deviceFilter").hide();
	var applicationType=$('#appType').val();
	/*Added by Sahana for pCloudy: Ends*/
	//added by shivam sharma for  TENJINCG-904 starts
	/*Modified by Sahana for pCloudy: Starts*/
	var clientName=$("#client option:selected" ).text();
	/*Modified by Preeti for TENJINCG-1026 starts*/
	/*if(clientName.includes("Device Farm"))*/
	if(clientName.indexOf("Device Farm") != -1)
	/*Modified by Preeti for TENJINCG-1026 ends*/
	{
		clientName=clientText.replace("[Device Farm]", "").trim();
	}
	var clientIp=$("#client option:selected" ).val()
	$('#clientName').val(clientName);
	if(applicationType==3 || applicationType==4)
		populateDevices(clientIp,null,applicationType);
	$('#client').change(function(){
		//var client=$(this).val();
		var deviceFarmCheck=$("#client").find(':selected').attr('data_deviceFarm');
		clientIp=$( "#client option:selected" ).val();
		/*Added by Sahana for pCloudy: Starts*/
		if(deviceFarmCheck=='Y'){
			var clientText=$( "#client option:selected" ).text();
			if(clientText.includes("Device Farm"))
			{
				clientText=clientText.replace("[Device Farm]", "").trim();
			}	
			$('#clientName').val(clientText);
			if(applicationType==3 || applicationType==4)
			{
				callFilters(clientIp,applicationType);
				$("#deviceFilter").show();
			}
			/*Added by Sahana for pCloudy: Ends*/
		}
		else{
			var text=$( "#client option:selected" ).text();
			clientName=text;
			$('#clientName').val(text);
			$("#deviceFilter").hide();
		}	
		if(applicationType==3 || applicationType==4)
			populateDevices(clientIp,deviceFarmCheck,applicationType);
	});
	/*Modified by Sahana for pCloudy: Ends*/
//	added by shivam sharma for  TENJINCG-904 ends
	/*Added by Sahana for pCloudy: Starts*/
	$("#btnSearchFilter").click(function() { 
		var obj = new Object();
		obj.os=$('#lstOs').val();
		obj.oem=$('#lstOEM').val();
		obj.screenSize=$('#lstScrnSize').val();
		obj.network=$('#lstNetwork').val();
		obj.clientIp=$( "#client option:selected" ).val();
		//obj.deviceLocation=$('#lstDevLoc').val();
		var jsonString = JSON.stringify(obj);
		getFilteredDevices(jsonString);
	});

	$("#btnReset").click(function() { 
		var clientIp=$( "#client option:selected" ).val();
		var deviceFarmCheck=$("#client").find(':selected').attr('data_deviceFarm');
		callFilters(clientIp,applicationType);
		populateDevices(clientIp,deviceFarmCheck,applicationType);
	});
	/*Added by Sahana for pCloudy: Ends*/

	$("#btnLearn").click(function(){
		/*********************************
		 * Changed by Sriram for Req#TJN_24_05
		 */
		/*var autLoginId = $('#lstAutCred').val();
	var targetMachine = $('#txtTarget').val();
	var browser = $('#lstBrowserType').val();
	var port = $('#txtTargetPort').val();
	var txnMode = $('#txnMode').val();
	if(txnMode == "W"){
		$.ajax({
			url : "NaviflowServlet",
			type:"GET",
			data : {param : "learn",browser: browser,target:targetMachine, port:port, autloginid:autLoginId},
			success : function(data) {
				window.location.href = 'learnerresult.jsp';
			},

			error : function(xhr, textStatus, errorThrown) {
				showMessage(errorThrown, 'error');
			}

		});
	}
	else{
		window.location.href='NaviflowServlet?param=INITIATE_LRNR&autloginid=' + autLoginId + '&target=' + targetMachine + '&browser=' + browser + '&port=' + port;	
	}*/

//		alert('I wont learn');
		/*

	var selCred = $('#lstAutCred').val();
	//alert(selCred);
	if(selCred=='-1'){
		showMessage('Please choose AUT Login ID','error');
		return false;
	}

	$('#progressbar').show();
	var x = document.getElementById("lstBrowserType").value;
	var y = document.getElementById("txtTarget").value;
		 *//********************
		 * Fix by Sriram to add target machine port (2-Dec-2014)
		 *//*
	var z = document.getElementById('txtTargetPort').value;
		  *//********************
		  * Fix by Sriram to add target machine port (2-Dec-2014) ends
		  *//*
	$.ajax({
		url : "NaviflowServlet",
		type:"GET",
		   *//********************
		   * Fix by Sriram to add target machine port (2-Dec-2014)
		   *//*
		//data : {param : "learn",lstBrowserType: x,txtTarget:y},
		data : {param : "learn",lstBrowserType: x,txtTarget:y, targetPort:z, autCred:selCred},
		    *//********************
		    * Fix by Sriram to add target machine port (2-Dec-2014) ends
		    *//*
		success : function(data) {
			// var result = data.split("|");
			// alert(result[1]);

		     * if (response.redirect) { window.location.href =
		     * response.redirect; } else{ alert('No redirect'); }

			window.location.href = 'learnerresult.jsp';
		},

		error : function(xhr, textStatus, errorThrown) {
			showMessage(errorThrown, 'error');
		}

	});	
		     */
		var regClient = $('#client').val();
		/*if(regClient == '-1'){
    	   showMessage('Please choose a client.', 'error');
       }else{
    	   clearMessages();
    	   $('#main_form').submit();
       }*/

		/*changed by Sahana for Mobility: Starts*/
		//var applicationType=$('#appType').val();
		
		/*Modified by Ashiki for TENJINCG-1104 starts*/
		var jsonArray = [];
		var jsonString = "";
		var appId=$('#appId').val();
		$('#modules_table tbody').find('tr').each(function (){
			var moduleCode = $(this).data('module-code');
			var json = new Object();
			json.funCode=moduleCode;
			jsonArray.push(json);
			jsonString=JSON.stringify(jsonArray);
		});
		/*Modified by Ashiki for TENJINCG-1104 ends*/
		
		var regDevice=$('#listDevice').val();
		if(regClient == '-1'){
			showMessage('Please choose a client.', 'error');
		}else if(applicationType!=1 && applicationType!=2 && regDevice=='-1' )
		{
			showMessage('Please choose a device.', 'error');
		}
		else{
			clearMessages();
			/*Modified by Ashiki for TENJINCG-1104 starts*/
			$('#main_form').append($("<input type='hidden' id='moduleCode' name='moduleCode' value='"+jsonString+"' />"))
			$('#main_form').append($("<input type='hidden' id='appId' name='appId' value='"+appId+"' />"))
			/*Modified by Ashiki for TENJINCG-1104 starts*/
			$('#main_form').submit();
		}
		/*changed by Sahana for Mobility: ends*/
	});
	/*Modified by Ashiki for TENJINCG-1104 starts*/
	 $('#modules_table tr').mouseup(function (event) {
		 selectedFunction=$(this).attr('id');
		
     });
	
	$('.sortable-table tbody').sortable({
		stop: function( event, ui ) {
		}
	});
	
	/*Modified by Ashiki for TENJINCG-1104 ends*/
});