/**
 * 
 */
function generateOutput(runId, functionCode, tdUids){
	var $loader;
	if(tdUids == 'all'){
		$loader = $('#loader_' + functionCode);
	}else{
		$loader = $('#loader');
	}
	
	$loader.show();
	

	$.ajax({
		url:'ExtractorServlet',
		data:'param=output&runid=' + runId + '&func=' + functionCode + '&tduids=' + tdUids,
		async:true,
		dataType:'json',
		success:function(data){
			var status = data.status;
			if(status == 'success'){
				var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
				$("body").append($c);
				$("#downloadFile").get(0).click();
				$c.remove();
				$loader.hide();
			}else{
				var message = data.message;
				if(message.length < 1){
					message = 'Internal Error. Please contact Tenjin Support.';
				}
				showMessage(message,'error');
				$loader.hide();
			}
		},
		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown, 'error');
			$loader.hide();
		}
	});
}