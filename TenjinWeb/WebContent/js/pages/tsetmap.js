/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  tsetmap.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 	CHANGED BY              	DESCRIPTION
 */
var table;
var mapTable;
var dataTable;
var testCaseTable;
var mappedtc=[];
$(document).ready(function(){
    
	var testSetId=$('#recID').val();
	var testSetName=$('#tsName').val();
	var testsetMode=$('#tsMode').val();
	initTestSetTable(testSetId,testSetName,testsetMode);
	populateMappedTestCases();
	//Added by sumit for TJN27-165 start
	  $('#Testcase-Table_filter').hide(); 
	  $('#MapTestcase-Table_filter').hide(); 
	//Added by sumit for TJN27-165 end
	
	$('#MapTestcase-Table').on('mousedown.rowReorder touchstart.rowReorder', 'tbody tr td', function(){
		   tr = $(this).closest('tr');  
		   if(mappedtc==0){
			 $('#MapTestcase-Table').DataTable().page.len(-1).draw();
			 mappedtc++;
			 tr.find('input[type=checkbox]').attr('checked', true);
		   }
			 
	  $('[name="MapTestcase-Table_length"]').prop('disabled', true); 
	  $('#MapTestcase-Table_search').prop('disabled',true);
			   
		});		
	
	table= $('#Testcase-Table').dataTable({
		"pageLength": 8,
	});
	mapTable= $('#MapTestcase-Table').dataTable({
		"pageLength": 8,
	});
	$('#btnBack').click(function(){
		var tsRecId=$('#recID').val();
		
		window.location.href="TestSetServletNew?t=testset_view&paramval="+tsRecId;
	});

	$('#chk_all').click(function(e){	
		if ($("#chk_all").is(':checked')) 
		{	
			$(this).closest('table').find('input[type=checkbox]').prop('checked', this.checked);	
			e.stopPropagation();
		}
		else
		{
			$(this).closest('table').find('input[type=checkbox]').prop('checked',false);	 
			e.stopPropagation();
		}
	}); 
	$('#check_all').click(function(e){	
		if ($("#check_all").is(':checked')) 
		{	
			 $("#tsMappedTcContainer_body").find('input[type=checkbox]').each(function () {
	             
	             this.checked = true;
	        });
		}
		else
		{
             $("#tsMappedTcContainer_body").find('input[type=checkbox]').each(function () {
	             
	             this.checked = false;
	        });
			
		}
	}); 
	
	////Added by sumit for TJN27-165 start
	
    $("#search").on("keyup", function() { 
    	/*Added by Prem for  TJN27-177 start*/
    	$('#MapTestcase-Table').DataTable().page.len(-1).draw()
    	/*Added by Prem for  TJN27-177 End*/
        var value = $(this).val().toLowerCase(); 
        $("#tsetbody tr").filter(function() { 
            $(this).toggle($(this).text() 
            .toLowerCase().indexOf(value) > -1) 
        }); 
    }); 
   
   $("#usearch").on("keyup", function() { 
	   /*Added by Prem for  TJN27-177 start*/
	   $('#Testcase-Table').DataTable().page.len(-1).draw()
	   /*Added by Prem for  TJN27-177 start*/
        var value = $(this).val().toLowerCase(); 
        $("#unmapsetbody tr").filter(function() { 
            $(this).toggle($(this).text() 
            .toLowerCase().indexOf(value) > -1) 
        }); 
    }); 
     ////Added by sumit for TJN27-165 end
   
	$('#Testcase-Table tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
		}
		else {
			table.$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
		}

	});
	
	
	  
	  
	$("#btnRefresh").click(function(e) {
		var testSetId= $('#recID').val();
		var testSetname= $('#txtTestSet').val();
		
		var mode= $('#mode').val();
		window.location.href='TestSetServlet?param=fetch_unmapped_tcs&testset=' + testSetId + '&testsetname=' + testSetname+ '&mode=' + mode;
		
	});
	
	var mapLength=0;
	$("#btnAddTC").click(function(e) {
		//Added by sumit for TJN27-137 
		//$('#MapTestcase-Table_search').prop('disabled',true);
		 $('[name="MapTestcase-Table_length"]').prop('disabled', true); 
		  $('[name="Testcase-Table_length"]').prop('disabled', true); 
		 // $('#Testcase-Table_search').prop('disabled',true);
		//  $('#Testcase-Table_filter').hide(); 
		  //$('#MapTestcase-Table_filter').hide(); 
		 // $('#Testcase-Table_search').val('');
		//Added by sumit for TJN27-137 end
		//Added by sumit for TJN27-165 start
		  $('#usearch').val('');
		  var value = '';
		    $("#unmapsetbody tr").filter(function() {
		      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		    });
		  //Added by sumit for TJN27-165 end
	
		
		/* Added by Ashiki for TJN27-121 starts*/
		var testcaseTable = $('#Testcase-Table').DataTable();
		/* Added by Ashiki for TJN27-121 end*/
		clearMessagesOnElement($('#user-message'));
		var html="";
		
         if($('#Testcase-Table tbody tr td #chk_one:checked').length==0)
        	 {
        	 showMessage("Please select one testcase to map",'error');
        	 return false;
        	 }
         
         /*Added by Ashiki for TJN27-84 starts*/
         var lenght=$('#MapTestcase-Table').dataTable().fnSettings().fnRecordsTotal();
         /*Added by Ashiki for TJN27-84 ends*/
         /*Added by Ashiki for TJN27-95 starts*/
         if(mappedtc==0){
        /*Added by Ashiki for TJN27-95 ends*/
        	 if( $('#MapTestcase-Table').DataTable().page.len()>'0'){
            	 $('#MapTestcase-Table').DataTable().page.len(-1).draw();
            	 $('#Testcase-Table').DataTable().page.len(-1).draw();
        	  } 
             /*Added by Ashiki for TJN27-95 starts*/
        	 mappedtc++;
        	 /*Added by Ashiki for TJN27-95 end*/
         }
        	 
        
         $('.dataTables_empty').remove();
		$('#Testcase-Table tbody tr td #chk_one:checked').each(function(){  
			mapLength++;

			var tcId=$(this).attr("caseid");
            
			var tcName=$(this).attr("casename");
			var tcrecid=$(this).attr("recid");
			var priority= $(this).attr("casepriority");
			 var tcName1=tcName;
			 var tcId1=tcId;
			 $("#MapTestcase-Table > tbody").append( "<tr role='row'><td class='sorting_1' ><input type='checkbox' id='chk_one' recid='"+tcrecid+"' caseid='"+escapeHtmlEntities (tcId)+"' casename='"+escapeHtmlEntities (tcName)+"' casepriority='"+priority+"'/></td><td class=mapping'>"+escapeHtmlEntities (tcId)+"</td><td class='mapping'>"+escapeHtmlEntities (tcName)+"</td></tr>"
				);
			$('#Testcase-Table tbody tr td #chk_one').attr('checked', false);
			 $(this).closest('tr').remove();
			 
			/* Added by Ashiki for TJN27-121 starts*/
			 testcaseTable
		        .row( $(this).parents('tr') )
		        .remove()
		    /* Added by Ashiki for TJN27-121 end*/
		});
		

		$("#chk_all").attr('checked', false);
		

	});
	$('.sortable-table tbody').sortable({
	/*Added by Ashiki for TJN27-84 starts*/
		update: function () { 
	   /*     if (mappedtc == 0) {
	        Added by Ashiki for TJN27-138 start
	        	 var lenght=$('#MapTestcase-Table').dataTable().fnSettings().fnRecordsTotal();
	        	if( $('#MapTestcase-Table').DataTable().page.len()!='100'){
	            	 $('#MapTestcase-Table').DataTable().page.len(-1).draw();
	             }
	        	  $('[name="MapTestcase-Table_length"]').prop('disabled', true); 
				  $('#MapTestcase-Table_search').prop('disabled',true);
	         Added by Ashiki for TJN27-138 end
	        	mappedtc++;
	        }*/
	    }
	/*Added by Ashiki for TJN27-84 end*/
	});

	/*$(window).load(function(){*/
	$(window).on('load', function(){
		$("#tsMappedTcContainer_body").sortable().disableSelection();
	}); 
	
	$('#btnRemoveTC').click(function() {
		//Added by sumit for TJN27-137 start
		//$('#MapTestcase-Table_search').prop('disabled',true);
		//$('#Testcase-Table').DataTable().page.len(-1).draw();
		 $('[name="MapTestcase-Table_length"]').prop('disabled', true); 
		  $('[name="Testcase-Table_length"]').prop('disabled', true); 
		 // $('#Testcase-Table_search').prop('disabled',true);
		 // $('#Testcase-Table_filter').hide(); 
		 // $('#MapTestcase-Table_filter').hide(); 
		  //Added by sumit for TJN27-137 end 
		  //Added by sumit for TJN27-165 start
		  $('#search').val('');
		  var value = '';
		    $("#tsetbody tr").filter(function() {
		      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		    });
		  //Added by sumit for TJN27-165 end 
		 
		clearMessagesOnElement($('#user-message'));
		var html="";
		 if($('#MapTestcase-Table tbody tr td #chk_one:checked').length==0)
        	 {
        	 showMessage("Please select one testcase to unmap",'error');
        	 return false;
        	 }
		 $('.dataTables_empty').remove();
		 
		 /*Modified by Ashiki for TJN27-127 ends*/
		 var lenght=$('#MapTestcase-Table').dataTable().fnSettings().fnRecordsTotal();
         
         if(mappedtc==0){
        	 if( $('#MapTestcase-Table').DataTable().page.len()>'0'){
            	 $('#MapTestcase-Table').DataTable().page.len(-1).draw();
            	 $('#Testcase-Table').DataTable().page.len(-1).draw();
        	  } 
        	 mappedtc++;
         }
         /*Modified by Ashiki for TJN27-127 end*/
		 
		 var dataTable = $('#MapTestcase-Table').DataTable();
		 $('#MapTestcase-Table tbody tr td #chk_one:checked').each(function(){  
			var tcId=$(this).attr("caseid");
			var tcName=$(this).attr("casename");
			var tcrecid=$(this).attr("recid");
			var priority= $(this).attr("casepriority");
			var tcName1=tcName;
			var tcId1=tcId;
			$("#Testcase-Table > tbody").append( "<tr role='row'><td class='sorting_1' ><input type='checkbox' id='chk_one' recid='"+tcrecid+"' caseid='"+escapeHtmlEntities (tcId)+"' casename='"+escapeHtmlEntities (tcName)+"' casepriority='"+priority+"'/></td><td class='mapping'>"+escapeHtmlEntities (tcId)+"</td><td class='mapping'>"+escapeHtmlEntities (tcName)+"</td></tr>");
			$('#MapTestcase-Table tbody tr td #chk_one').attr('checked', false);
			
			/*Modified by Pushpa for TJN27-122 starts*/
			$(this).closest('tr').remove();
			//dataTable.row($(this).closest('tr')).remove().draw();
			/*Modified by Pushpa for TJN27-122 starts*/
		});
		 $("#check_all").attr('checked', false);
		 /*Commented by Pushpa for TJN27-122 starts*/
		// $('#MapTestcase-Table').DataTable().search('').draw();
		 /*Commented by Pushpa for TJN27-122 ends*/
	});

	$('#btnOk').click(function(){
	/*Added by Ashiki for TJN27-123 starts*/
		var lenght=$('#MapTestcase-Table').dataTable().fnSettings().fnRecordsTotal();
		if($('#MapTestcase-Table_search').val()!=('')){
		 $('#MapTestcase-Table').DataTable().search('').draw();
		}
        if(mappedtc==0){
       	 if( $('#MapTestcase-Table').DataTable().page.len() >'0'){
           	 $('#MapTestcase-Table').DataTable().page.len(lenght).draw();
            } 
       	 mappedtc++;
        }
        /*Added by Ashiki for TJN27-123 end*/
       
		var selTests = '';
			 $('#MapTestcase-Table tbody tr td input').each(function(){  
			var recid = $(this).attr('recid');

			selTests = selTests + recid + ',';
	});

		if( selTests.charAt(selTests.length-1) == ','){
			selTests = selTests.slice(0,-1);
		}

		var tsId =  $('#recID').val();

		var testSetname =  $('#txtTestSet').val();
		
		$.ajax({
			url:'TestSetServlet',

			data:'param=unmap_tcs&ts=' + tsId + '&tcs=' + selTests,

			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					showMessage(data.message,'success');
					var mode= $('#mode').val();
					window.setTimeout(function(){window.location='TestSetServlet?param=fetch_unmapped_tcs&testset=' + tsId + '&testsetname=' + testSetname+'&mode=' + mode;},3000)
				}else{
					showMessage(data.message,'error');
					
				}
			},
			error:function(xhr,textStatus,errorThrown){
				showMessage(errorThrown,'error');
			}
		});
		


	/*	$.ajax({
			url:'TestSetServlet',
			data:'param=map_tcs1&ts1=' + tsId + '&tcs1=' + selTests,
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					showMessage(data.message,'success');
					
					
					var mode= $('#mode').val();
					window.setTimeout(function(){window.location='TestSetServlet?param=fetch_unmapped_tcs&testset=' + tsId + '&testsetname=' + testSetname+'&mode=' + mode;},3000)
					

				}else{
					showMessage(data.message,'error');
				}
			},
			error:function(xhr,textStatus,errorThrown){
				showMessage(errorThrown,'error');
			}
		});*/
	});
	
	

});
function initTestSetTable(testSetId,testSetName,testsetMode) {
	table=$('#Testcase-Table').DataTable({
		sort : false,
		"lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100, "All"]],
			ajax:{
				url:'TestSetServlet?param=fetch_unmapped_tcsList&testset='+testSetId+'&testsetname='+testSetName+'&mode='+testsetMode,
				dataSrc:''
			},
			"columns":[
				{"data":"tcId"},
				{"data":"tcId"},
				{"data":"tcName"},
				
			],
			"columnDefs":[{
				"targets":0,
				"searchable":false,
				"orderable":false,
				"className":"select-row",
				"render":function(data, type, full, meta) {
					return "<input type='checkbox' id='chk_one' recid='"+full.tcRecId+"' caseid='"+full.tcId+"' casename='"+full.tcName+"' casepriority='"+full.tcPriority+"'>"
				}
			},{
				"targets":1,
				"searchable":true,
				"orderable":false,
				"className":"select-row mapping",
				"render":function(data, type, full, meta) {
					return escapeHtmlEntities(data);
				}
			},
			{
				"targets":2,
				"searchable":true,
				"orderable":false,
				"className":"select-row mapping",
				"render":function(data, type, full, meta) {
					return escapeHtmlEntities(data);
				}
			}]
		});
	}

function populateMappedTestCases() {
	var tsRecId = $('#recID').val();
	mapTable=$('#MapTestcase-Table').DataTable({
		"lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100, "All"]],
		/*"aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
        "iDisplayLength": 25,*/
		sort : false,
			ajax:{
				url:'TestSetServlet?param=mapped_tcs1&t1=' + tsRecId,
				dataSrc:''
			},
			"columns":[
				{"data":"tcId"},
				{"data":"tcId"},
				{"data":"tcName"},
				
			],
			"columnDefs":[{
				"targets":0,
				"searchable":false,
				"orderable":false,
				"className":"select-row",
				"render":function(data, type, full, meta) {
					return "<input type='checkbox' id='chk_one' recid='"+full.tcRecId+"' caseid='"+full.tcId+"' casename='"+full.tcName+"' casepriority='"+full.tcPriority+"'>"
				}
			},{
				"targets":1,
				"className":"mapping",
				"render":function(data, type, full, meta) {
					return escapeHtmlEntities(data);
				}
			},
			{
				"targets":2,
				"className":"mapping",
				"render":function(data, type, full, meta) {
					return escapeHtmlEntities(data);
				}
			}]
		});
	}
function populateMappedTests(){
	var tsRecId = $('#recID').val();

	$.ajax({
		url:'TestSetServlet',

		data:'param=mapped_tcs1&t1=' + tsRecId,

		async:false,
		dataType:'json',
		success:function(data){
			var status = data.status;
			if(status == 'SUCCESS'){
				var jArray = data.tests;
				var html = '<div><br>&nbsp&nbsp&nbsp<input type=checkbox class=check_all id=check_all><b>&nbsp&nbspSelect all testcases to unmap</b><br></div>';
				for(i=0;i<jArray.length;i++){
					var j = jArray[i];

					var ids=j.id;
					var names=j.TC_NAME;
					var priority=j.TC_Priority;
					var tcID=ids;
					var tcName=names;
					html = html + "<div id='user"+i+"' userid="+i+"  recid='"+j.recid+"' class='tomap' style='max-width:449px; ' title='"+ids+"---"+names+"---"+priority+"'>" + "<input type='checkbox'  id='user"+i+"' value="+i+" class='selectit' caseid='"+ids+"' recid='"+j.recid+"' casename='" + names + "' casepriority='"+priority+"'style='margin-right: 7px;float:left;'    >" +
					"<div class='mapping'>"+ids+"</div>" +
				       "<div class='mapping'>"+names+"</div>" +
				       "</div>";

				}
				$('#tsMappedTcContainer_body').html(html);
			}else{
				showMessage(j.message,'error');
			}
		},
		error:function(xhr, textStatus,errorThrown){
			showMessage('An internal error occurred. Please contact your System Administrator','error');
		}
	});
}
function checkBoxSelectionForTsetMap(tableId){
	var selectedRows=$('#'+tableId+' tbody').find('input[type="checkbox"]:checked').length;
	var rowsCount =$('#'+tableId+' tbody').find("tr").length;
	if(tableId=='MapTestcase-Table'){
		if(rowsCount!=selectedRows){
			$('.mapped-testcase').prop('checked',false);
		}else {
			$('.mapped-testcase').prop('checked',true);
		}
	}else{
		if(rowsCount!=selectedRows){
			$('.unmapped-testcase').prop('checked',false);
		}else {
			$('.unmapped-testcase').prop('checked',true);
		}
	}

}
