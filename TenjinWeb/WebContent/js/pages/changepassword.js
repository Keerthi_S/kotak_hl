/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  changepassword.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 06-05-2019           Added by Roshni          TENJINCG-1036
* 
*/

$(document).ready(function(){
			
	var status = $('#tjn-status').val();
	var message = $('#tjn-message').val();

	$('#btnChange').click(function(){
		var newPwd = $('#newPassword').val();
		var confPwd = $('#confirmPassword').val();
			
		if(newPwd.length < 8){
			clearMessages();
			showMessage('Password should be minimum 8 characters', 'error');
			return false;
		}
		/*Added By prem for encrypt the password Start*/
		/*var newPwd=$('#newPassword').val();
		var b641 = btoa(newPwd);
		$('#newPassword').val(b641);	*/ 
		var newPassword=document.getElementById('newPassword');
		if(!CheckPassword(newPassword)){
			/*var hashObj1 = new jsSHA("SHA-256", "TEXT", {numRounds: 1});
			hashObj1.update(newPassword.value);
			var hash1 = hashObj1.getHash("HEX");
			 $('#newPassword').val(hash1);*/
			showMessage("Please check the password rules","error");
			return false;
			 
		}
		/*else{
			showMessage("Please check the password rules","error");
			return false;
		}*/
		
		
		/*var confPwd=$('#confirmPassword').val();
		var b642 = btoa(confPwd);
		$('#confirmPassword').val(b642);	*/ 
		
		var confPwd=document.getElementById('confirmPassword');
		 
		 if(!CheckPassword(confPwd)){
			 /*var hashObj2 = new jsSHA("SHA-256", "TEXT", {numRounds: 1});
				hashObj2.update(confPwd.value);
				var hash2 = hashObj2.getHash("HEX");
				 $('#confirmPassword').val(hash2);*/
			 showMessage("Please check the password rules for confirmPassword ","error");
			 return false;
				 
			}
			/*else{
				showMessage("Please check the password rules for confirmPassword ","error");
				return false;
			}*/
		/*Added By prem for encrypt the password Ends*/
	});
		
	if(status === 'success') {
		setTimeout(function(){
            window.location.href = 'TenjinPasswordResetServlet?param=redirectLoginPage';
         }, 5000);
	}
});