
 /***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  message_file_upload.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright ï¿½ 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 15-06-2021			Ashiki					Newly added for TENJINCG-1275
 */
var storedFiles = [];
var removedFiles = [];
var selDiv;




$(document).ready(function() {
	 $('#infoFiles').hide();
	$('#uploadProgress').hide();
	
	selDiv = $('#selectedFiles');
	$('#txtFile').on("change", handleFileSelect);
	$('body').on('click','.selFile', removeFile);
	
	$('#btnCancel').click(function() {
		var value=$(this).attr('value');
		if(value.toLowerCase() === 'Cancel') {
			window.parent.closeModal(false);
		}else {
			window.parent.closeModal(true);
		}
	});
	
	/*Added by Preeti for T251IT-133 starts*/
	$('#btnBack').click(function(e){
		window.location.href='MessageServlet?appId=' + appId + '&group=' + groupName;
	});
	/*Added by Preeti for T251IT-133 ends*/
	
	$('#btnUpload').click(function(e) {
		/*Added by Gangadhar Badagi for T25IT-307 starts*/
		$("#infoFiles").empty();
		$("#infoFilesBody").empty();
		/*Added by Gangadhar Badagi for T25IT-307 ends*/
		$('#btnBack').hide();
		handleForm(e);
	});
});

function handleFileSelect(e) {
	/*Added by Gangadhar Badagi to hide uploaded files starts*/
	$('#uploadedFiles').hide();
	$('#selectedFiles').show();
	/*Added by Gangadhar Badagi to hide uploaded files ends*/
	var files = e.target.files;
	var filesArr = Array.prototype.slice.call(files);
	
	filesArr.forEach(function(f){
		storedFiles.push(f);
		
		var reader = new FileReader();
		reader.onload = function(e){
			//var html = "<div class='clear'></div><div class='grid_15 att_block'><span>" + f.name + "</span><a href='#' data-file='" + f.name + "' data-res='" + e.target.result + "' class='selFile' title='Click to Remove'>Remove</a></div>";
			
			/*	Changed by Gangadhar Badagi for T25IT-46 starts*/
			/*var html = "<div class='att_block grid_6'><p class='grid_4' title='" + f.name + "'>" + f.name + "</p><a  href='#' recid='0' data-file='" + f.name + "' class='selFile' title='Click to Remove'>Remove</a></div>";*/
			/*var html = "<div class='att_block grid_6'><p class='grid_4' title='" + f.name + "'>" + f.name + "</p><a id='remove' href='#' recid='0' data-file='" + f.name + "' class='selFile' title='Click to Remove'>Remove</a></div>";*/
			/*	Changed by Gangadhar Badagi for T25IT-82 starts*/
			var html = "<div class='att_block grid_6'><p class='grid_4' title='" + f.name + "'>" + f.name + "</p><a  href='#' recid='0' data-file='" + f.name + "' class='selFile' title='Click to Remove'>Remove</a></div>";
			/*var html1 = "<div class='att_block grid_6'><p class='grid_4' title='" + f.name + "'>" + f.name + "</p><a  href='#' recid='0' data-file='" + f.name + "' class='selFile' id='remove' title='Click to Remove'>Remove</a></div>";*/
			/*	Changed by Gangadhar Badagi for T25IT-82 ends*/
			/*	Changed by Gangadhar Badagi for T25IT-46 ends*/
			selDiv.append(html); 
		}
		
		reader.readAsDataURL(f);
	});
	
	//alert(storedFiles.length);
}

function removeFile(e){
	/*Added by Preeti for T251IT-119 starts*/
	$('#txtFile').val('');
	/*Added by Preeti for T251IT-119 ends*/
	var file = $(this).data("file");
	for(var i=0; i<storedFiles.length; i++){
		if(storedFiles[i].name === file){
			storedFiles.splice(i,1);
			break;
		}
	}

	removedFiles.push($(this).attr('recid'));
	$(this).parent().remove();
	//alert(storedFiles.length);
}

function handleForm(e){
	e.preventDefault();
	
	$('#filesToUpload').hide();
	$('#uploadProgress').show();
	$('#btnUpload').hide();
	$('#btnCancel').hide();
	
	var data = new FormData();
	for(var i=0, len=storedFiles.length; i<len; i++){
		data.append('files', storedFiles[i]);
	}
	
	for(var i=0; i<storedFiles.length;i++){
		for(var j=i+1;j<storedFiles.length; j++){
			if(storedFiles[i].name==storedFiles[j].name){
				showMessage("Please remove duplicate files.",'error');
				$('#btnBack').show();
				$('#uploadProgress').hide();
				$('#filesToUpload').show();
				$('#btnUpload').attr('value', 'Upload');
				$('#btnUpload').addClass('ok');
				$('#btnUpload').show();
				$('#btnCancel').attr('value', 'Close');
				$('#btnCancel').addClass('cancel');
				$('#btnCancel').show();
				return false;
			}
		}
	}
	if(storedFiles.length<=0){
		showMessage("Please choose atleast one file", 'error');
		$('#btnBack').show();
		$('#uploadProgress').hide();
		$('#filesToUpload').show();
		$('#btnUpload').attr('value', 'Upload');
		$('#btnUpload').addClass('ok');
		$('#btnUpload').show();
		$('#btnCancel').attr('value', 'Close');
		$('#btnCancel').addClass('cancel');
		$('#btnCancel').show();
		return false;
	}
	data.append('message',$('#message').val());
	data.append('type', $('#type').val());
	var jsonArray =[];
	
	$.ajax({
		type:'post',
		url:'MessageServlet',
		data:data,
		processData:false,
		contentType:false,
		dataType:'json',
		success:function(r){
			console.log(r);
			var failed=r.failed;
			var passedFiles=r.passed;
			$('#infoFiles').show();
			$('#selectedFiles').hide();
			jsonArray=r.jsonArray;
			if(passedFiles.length>0){
				
				$('#uploadedFiles').show();
				getUploadedFiles(jsonArray);
				storedFiles.splice(0, storedFiles.length);
				$('#selectedFiles').empty();
				clearMessages();
				$('#uploadProgress').hide();
				$('#filesToUpload').show();
				$('#txtFile').show();
				$('#btnBack').show();
				$('#btnUpload').show();
				$('#btnCancel').attr('value', 'Close');
				$('#btnCancel').removeClass('cancel');
				$('#btnCancel').addClass('ok');
				$('#btnCancel').show();
				
			}else if(r.status.toUpperCase()=='ERROR'){
				$('#infoFiles').show();
				$('#selectedFiles').hide();
				$('#uploadedFiles').show();
				getUploadedFiles(jsonArray);
				$('#selectedFiles').empty();
				storedFiles.splice(0, storedFiles.length);
				clearMessages();
				$('#btnBack').show();
				$('#uploadProgress').hide();
				$('#filesToUpload').show();
				$('#btnUpload').show();
				$('#btnCancel').show();
			}else if(r.status.toUpperCase() == 'WARN'){
				$('#infoFiles').show();
				$('#selectedFiles').hide();
				$('#uploadedFiles').show();
				getUploadedFiles(jsonArray);
				$('#selectedFiles').empty();
				storedFiles.splice(0, storedFiles.length);
				clearMessages();
				$('#btnBack').show();
				$('#uploadProgress').hide();
				$('#filesToUpload').show();
				$('#btnUpload').show();
				$('#btnCancel').show();
			}
		},
		error:function(r){
			showMessage('An internal error occurred. Please contact Tenjin Support.', 'error');
			$('#uploadProgress').hide();
			$('#filesToUpload').show();
			$('#btnUpload').show();
			$('#btnCancel').show();
		}
	});
}

	function getUploadedFiles(j) {
		 var html1 ="<thead><tr><th>Status</th><th>File Name</th><th>Message</th></tr></thead>";
		 for (var p = 0; p < j.length; p++) {
				  html1+="<tbody id='infoFilesBody'>";
				  if(j[p].status=='error'){
				  html1+="<tr><td><img src='images/failure_20c20.png'></td><td style='overflow-y:auto;width:600px;height:auto;word-break: break-all;white-space: normal;'>"+j[p].name+"</td>><td>"+j[p].message+"</td></tr>";
				  }else{
					  html1+="<tr><td><img src='images/success_20c20.png'></td><td style='overflow-y:auto;width:600px;height:auto;word-break: break-all;white-space: normal;'>"+j[p].name+"</td><td>"+j[p].message+"</td></tr>";
				  }
				  html1+="</tbody";
		 }
		  $('#infoFiles').append(html1);
}
