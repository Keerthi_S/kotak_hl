/*

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  live_extr_progress.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India



/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
  
   09-02-2017             Leelaprasad             Improvements of sprint 2
   24-10-2017			Sriram Sridharan		TENJINCG-399
*/
$(document).ready(function(){
	var msg = $('#message').val();
	if(msg == 'noerror'){
		clearMessages();
	}else{
		showMessage(msg, 'error');
		//$('#autoRefreshFlag').val('no');
	}
	
	var progressPercentage = $('#progpcnt').val();
	progressBar(progressPercentage, $('#progressBar'));
	
	
	$('#btnRefresh').click(function(){
		window.location.href='ExtractorServlet?param=extractor_progress&runid=' + $('#runId').val();
	});
	/*Changed by Leelaprasad for the requirement Improvements of sprint 2 starts*/
	$('#btnBack').click(function(){
		var funcCode = $('#txtModuleCode').val();
		var funcName = $('#txtModuleName').val();
		var selApp=$('#selApp').val();
		var parent="history";
		
		//TENJINCG-399
		/*window.location.href = 'ModuleServlet?param=extraction_history&appID=' + selApp + '&funcCode=' + funcCode+'&funcName=' + funcName+'&parent='+parent;*/
		window.location.href='FunctionServlet?t=extractHistory&appId=' + selApp + '&function=' + funcCode;
		//TENJINCG-399 ends
	});
	/*Changed by Leelaprasad for the requirement Improvements of sprint 2 ends*/
	$(document).on('click','.template-gen',function(e){
		var runId = $('#runId').val();
		var functionCode = $(this).attr('function');
		if(window.confirm('Downloading all records may take some time. Do you wish to proceed?')){
			generateOutput($('#runId').val(), functionCode, 'all');
		}else{
			return false;
		}
	});
	
	//For Automatic Refresh every 5 seconds
	var loop; 
	var autoRefreshFlag = $('#autoRefreshFlag').val();
	if(autoRefreshFlag == 'yes'){
		loop = window.setInterval(function(){window.location.href='ExtractorServlet?param=extractor_progress&runid=' + $('#runId').val()}, 5000);
	}else{
		$('#toggleRefresh').hide();
	}
	
	//To stop automatic refresh
	$('#toggleRefresh').click(function(){
		var action = $(this).attr('ac');
		if(action =='stop'){
			clearInterval(loop);
			$(this).attr('ac','start');
			$(this).attr('value','Start Automatic Refresh');
			$(this).removeClass('cancel');
			$(this).addClass('ok');
		}else if(action =='start'){
			loop = window.setInterval(function(){window.location.href='ExtractorServlet?param=extractor_progress&runid=' + $('#runId').val()}, 5000);

			$(this).attr('ac','stop');
			$(this).attr('value','Stop Automatic Refresh');
			$(this).removeClass('ok');
			$(this).addClass('cancel');
		}
	});
	
$('.modalmask').click(function(){
		
    	closeModal();
	});

});

function closeModal()
{
	$('.modalmask').hide();
	$('.subframe').hide();
	$('#ttd-options-frame').attr('src','');
}