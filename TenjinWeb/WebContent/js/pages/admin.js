/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  admin.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 05-Nov-2016       Leelaprasad                    Req#tjn_243_12
 * 11-01-2017            Leelaprasad         TENJINCG-25
 * 12-10-2018             Leelaprasad P           TENJINCG-872
 * 25-10-2018            Leelaprasad             TENJINCG-826,TENJINCG-828
 * 26-10-2017			Sriram						TENJINCG-875
 * 28-10-2018			Pushpa					TJNUN262-8
 * 01-12-2018           Padmavathi               for TJNUN262-38
 * 17-03-2020			Prem					Jquery Upgradation. 
 */


$(document).ready(function(){
	/*Added by Padmavathi for TJNUN262-38 starts */
	if($('#uesr_view').val()=='true'){
		/*window.location.href='TenjinUserServlet?t=view&key='+$('#currentUserId').val();*/
		$('#content_frame').attr('src','TenjinUserServlet?t=view&key='+$('#currentUserId').val());

		}
	/*Added by Padmavathi for TJNUN262-38 ends */
	$('#accordion').accordion({heightStyle:'content'});
	//$('#content_frame').attr('src','projectdetailview.jsp');

	populateDomainsTree();
	/*Changed by Leelaprasad for requirement TENJINCG-25 on 11-01-2017 starts*/
	if($('#domains_tree ul li').length==0){

		$('#domains_trigger').hide();
		$('#ui-accordion-accordion-panel-0').hide();
	}else{
		$('#domains_trigger').show();
		$('#ui-accordion-accordion-panel-0').show();
	}
	
	
	/*Changed by Leelaprasad for requirement TENJINCG-25 on 11-01-2017 ends*/
	$('#domains_trigger').click(function(){
		populateDomainsTree();
	});
	/*changed by leelaprasad for requirement tjn_243_12 on 05-12-2016 starts */
	$("a").click(function() {
		/* Modified by Pushpa for TJNUN262-8 starts*/
		var headerClass=$(this).attr('class');
		
		   var str=headerClass.split(" ");
		   if(str[1]!='headerLink'&& str[0]!='headerLink'){
				$("a").removeClass("active");
				$(this).addClass("active");
		   }
		/* Modified by Pushpa for TJNUN262-8 ends*/
	});
	/*changed by leelaprasad for requirement tjn_243_12 on 05-12-2016 ends */

	/***************
	 * changed by sahana for TJN_24_03--->starts
	 * 
	 */
	$('#sch-trigger').click(function(){

		$('#content_frame').attr('src','SchedulerServlet?param=FETCH_ALL_ADMIN_SCH');

	});

	
	$('#switchProject').hover(function () {
        $(this).css({"text-decoration":"underline"});
   });
  $('#switchProject').mouseout(function() {
   $(this).css({"text-decoration":"none"});
  });

$('#logged-in-user').hover(function () {
 $(this).css({"text-decoration":"underline"});
});
$('#logged-in-user').mouseout(function() {
$(this).css({"text-decoration":"none"});
});
	/***************
	 * changed by sahana for TJN_24_03--->ENDS
	 * 
	 */
	/* Changed by Leelaprasad for TENJINCG-826,TENJINCG-828 starts */
	$('#switchProject').click(function(){
		window.location.href='UserLandingServlet?key=gotoProject';
	});
	/* Changed by Leelaprasad for TENJINCG-826,TENJINCG-828 ends */
	$('#logout').click(function(){
        /*Changed by Leelaprasad for TENJINCG-872 starts*/	
	    /*window.location.href='AuthenticationServlet?param=exit_admin_view';*/
		window.location.href='TenjinSessionServlet?t=logout';
		/*Changed by Leelaprasad for TENJINCG-872 starts*/	
	});

});
/*Refresh DomainSTree Currently Added*/
function populateDomainsTree(){
//Changed for TENJINCG_875 (Sriram)
//	tree = new $.tree('domain');
//	$('#domains_tree').empty();
//	$("#domains_tree").append(tree.getTree());
//	tree.attachEvents();
//	$('.ttree-br-closed > i').trigger('click');
//	$('#t_addProject').prop('disabled',true);
	
	$('#domains_tree').on('changed.jstree', function(e, data) {
		//console.log(data.selected.length);
//		console.log(e);
//		console.log("Data object is: ");
//		console.log(data);
//		console.log(data.node.a_attr.href);
		
		console.log("Triggered event: ");
		console.log(data.event);
		if(data.event && data.node) {
			$('.ifr_Main').attr('src', data.node.a_attr.href);
		}else{
			//$('.ifr_Main').attr('src', data.node.a_attr.href);
		}
	}).jstree({
		"core" : {
				'data':{
					'url':'ProjectAjaxServlet',
					'data':function (node) {
						/*return {'param':'tree','id':node.id, 'a':app, 'f':func};*/
						return {'t':'domain_project_tree'};
					},
					'dataType':'json'
					
				},
			    "themes" : {
			      "variant" : "medium"
			    }, 
			    "multiple":false
				
			  
		  },	
		  
		  "types" : {
		      //  "default" : {  "icon" : "/images/success_20c20.png"  }
		      },
		  
			  "plugins" : [ "wholerow", "types","dnd"]
	});
	//TENJINCG-875 (Sriram) Ends
}
//TENJINCG-875 (Sriram)
/*Refresh DomainSTree Currently Added
window.refreshTree = function () {
	populateDomainsTree ();
};
Refresh the Selected Tree with the new Project Currently Added
function populateSelectedTree(treeEntity,type,nodeId){
	var selector = '';
	selector = '#domains_tree';
	tree.selectNode (selector,nodeId);
}
Refresh the Selected Tree with the new Project Currently Added
window.selectTree = function(treeEntity,type,nodeId) {
	populateSelectedTree(treeEntity,type,nodeId);
};*/

function refreshDomainsTree() {
	$('#domains_tree').jstree(true).refresh();
}

function selectProjectInTree(projectId) {
	$('#domains_tree').jstree('select_node', 'project_' + projectId);
}
//TENJINCG-875 (Sriram) Ends

/*Added by Padmavathi for V2.8-37 starts*/
function selectDomainInTree(domainName) {
	$('#domains_tree').jstree("deselect_all");
	$('#domains_tree').jstree('select_node', 'domain_' + domainName);
}
/*Added by Padmavathi for V2.8-37 ends*/

