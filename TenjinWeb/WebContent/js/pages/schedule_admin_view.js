/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_admin_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 *  28-11-2016            Parveen                Enhancement#665
 *  28-11-2016            Sahana                 For function code counter
 *   16-DEC-2016			 Sahana					Req#TEN-72
 *   16-DEC-2016			 Sahana					defect #TEN-79(scheduler accepts past time)
 *   21-Dec-2016			 Nagababu               defect #TEN-137(Time cannot be edited to prior time once the task is scheduled)
 *   21-Dec-2016         Sahana                  defect #TEN-139(The Minute Field is blank in Schedule Task screen.)
 *   21-Dec-2016         Sahana                  defect #TEN-145(In Scheduler,Getting issue while rescheduling.)
 *   10-JAN-2016          Sahana                  TENJINCG-7(Calendar Icon is to be added for all date fields)
 	23-Jan-2016          Sahana                  to check hour & min is >= current time
    27-09-2017          Gangadhar Badagi        To schedule the task between 55-60 minutes.
    21-06-2018          Padmavathi               T251IT-104   
    21-06-2018			Preeti					T251IT-108
    22-06-2018			Preeti					T251IT-116
    05-07-2018			Preeti					T251IT-181
    20-03-2019          Padmavathi              TENJINCG-1014
    25-03-2019			Pushpalatha				TENJINCG-968
    10-06-2019			Ashiki					V2.8-63
    14-06-2019			Preeti					V2.8-113
 */

/*
 * Added file by sahana for Req#TJN_24_03 on 14/09/2016
 */
$(document).ready(function(){
	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('edit_schedule_admin');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	var hoursVal= $("#btntime").val();
	var creationDate=$( "#datepicker" ).val();
	$('#lstApplication').show();
	$('#lsttask').show();
	var client=$('#txtclientregistered').val();
	var appId=$('#lstApplication').val();
	var optBrowser = $('#currentBrowser').val();
	/*Added by Padmavathi for T251IT-104 starts*/
	populateUserAutTypes(appId);
	/*Added by Padmavathi for T251IT-104 ends*/
	$('#lstBrowserType').children('option').each(function(){
		var val = $(this).val();
		if(val == optBrowser){
			$(this).attr({'selected':true});
		}
	});

	var optAut = $('#currentAUT').val();
	$('#lstAppUserType').children('option').each(function(){
		var val = $(this).val();
		if(val == optAut){
			$(this).attr({'selected':true});
		}
	});
	
	var selApiLearnType = $('#selApiLearnType').val();
	$('#lstApiLearnType').children('option').each(function(){
		var val = $(this).val();
		if(val == selApiLearnType){
			$(this).attr({'selected':true});
		}
	});
	var appFunctions=[];
	var countval=0;
	var funcFilePaths=[];
	$('.funccheckbox').each(function () {
		appFunctions[countval]=$(this).val();
		countval++;
	});
	$('.filepathcheckbox').each(function () {
		var m=new Object();
		m.funcCode=$(this).attr('funcCode');
		m.path=$(this).val();
		if(m.path!="")
			funcFilePaths.push(m);
	});

	var taskVal=$('#txtTask').val();
	populateAppInfo(appId);
	
	if(taskVal === 'LearnAPI'){
		$('.gui-only').hide();
		$('.apiBlock').show();
		populateApiCodes(appId);
		
		var selApiCode = $('#selApiCode').val(); 
		$('#lstAPICode').children('option').each(function(){
			if(selApiCode === $(this).val()) {
				$(this).attr({'selected':true});
			}
		});
		
		populalteAPIOperations(appId, selApiCode);
		
		$('#tblNaviflowModules').find("input[type='checkbox']").each(function() {
			var flag = functionExists(appFunctions, $(this).attr('id'));
			if(flag === true) {
				$(this).attr({'checked':true});
			}
		});
		
	}else{
		$('.gui-only').show();
		$('.apiBlock').hide();
		populateMappingModuleInfo(appId,appFunctions,funcFilePaths);
	}
	populateHoursInfo($('#btntime').val(),creationDate);
	populateMinutesInfo($('#btntime1').val(),creationDate);
	$('#tasktype').show();
	$('#pagination_block').show();
	$(function() {
		$( "#datepicker" ).datepicker({
			/*   Changed by sahana for the improvement of TENJINCG-7: starts  */
			showOn: 'button',
			buttonText: 'Show Date',
			buttonImageOnly: true,
			buttonImage: './images/button/calendar.png',
			/*   Changed by sahana for the improvement of TENJINCG-7: ends  */
			dateFormat: 'dd-M-yy',
			minDate:0,
			constrainInput: true
		});

	});
	/*changed by sahana for req#TEN-72 starts*/
	/*$('#taskName').keyup(function (){
		var inputVal = $(this).val();
		var regex = new RegExp(/[^a-zA-Z0-9]/);
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			alert('Please enter only alphanumeric characters');
			$(this).val('');
		}
	});*/
	/*changed by sahana for req#TEN-72 Ends*/
	$('#tblNaviflowModules').on('click','#chk_all', function(e){

		if ($("#chk_all").is(':checked')) 
		{	
			$(this).closest('table').find('input[type=checkbox]').prop('checked', this.checked);	
			e.stopPropagation();
		}
		else
		{
			$(this).closest('table').find('input[type=checkbox]').prop('checked',false);	 
			e.stopPropagation();
		}
	}); 
	
	$("#lstAPICode").change(function() {
		var apiCode = $(this).val();
		var appId = $('#lstApplication').val();
		
		if(apiCode === '-1') {
			
		}else{
			populalteAPIOperations(appId, apiCode);
			$('#tasktype').show();
			$('#pagination_block').show();
			/*paginate();*/
			
			$('#tblNaviflowModules').find("input[type='checkbox']").each(function() {
				var flag = functionExists(appFunctions, $(this).attr('id'));
				if(flag === true) {
					$(this).attr({'checked':true});
				}
			});
		}
	});
	
	$('#lstApplication').change(function(){
		var app = $(this).val();
		var task=$('#txtTask').val();
		clearMessages();
		if(app == '-1'){

			/*added by  Parveen for  Enhancement#665   on 28-11-2016 starts*/ 
			$('#lstAppUserType').html('');
			/*added by  Parveen for  Enhancement#665   on 28-11-2016 ends*/ 
			$('#lstModules').html("<option value='-1' selected>-- Select One --</option>");
			$('#lstModules').select2('val','-1');
		}else{
			/*added by  Parveen for  Enhancement#665   on 28-11-2016 starts 
			populateUserAutTypes(app);
			added by  Parveen for  Enhancement#665   on 28-11-2016 ends 
			$('#tasktype').show();
			$('#pagination_block').show();
			var task=$('#txtTask').val();
			if(app==appId){
				populateMappingModuleInfo(app,appFunctions,funcFilePaths);
			}
			else{
				//populateMappingModuleInfo(app);
				populateMappingModuleInfo(app,appFunctions,funcFilePaths);

			}
			paginate();*/
			
			var task= $('#txtTask').val();
			if(task === 'LearnAPI') {
				populateApiCodes(app);
			}else{
				/*added by  Parveen for  Enhancement#665   on 28-11-2016 starts*/ 
				populateUserAutTypes(app);
				/*added by  Parveen for  Enhancement#665   on 28-11-2016 ends*/ 
				$('#tasktype').show();
				$('#pagination_block').show();
				var task=$('#txtTask').val();
				if(app==appId){
					populateMappingModuleInfo(app,appFunctions,funcFilePaths);
				}
				else{
					//populateMappingModuleInfo(app);
					populateMappingModuleInfo(app,appFunctions,funcFilePaths);

				}
				/*paginate();*/
			}
		}
	});

	populateClients();

	$('#btnSave').click(function(){

		var validated = validateForm('edit_schedule_admin');
		var csrftoken_form=$("#csrftoken_form").val();
		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
		}
		/*Added by Preeti for T251IT-116 starts*/
		var task = $('#txtTask').val();
		if(task === 'LearnAPI') {
			
		}
		else {
			var autCred = $('#lstAppUserType').val();
			
			if(autCred === '-1' || autCred === '' || autCred == undefined) {
				showMessage('AUT User Type is required.', 'error');
				return false;
			}
		}
		/*Added by Preeti for T251IT-116 ends*/
		/*changed by sahana for Defect#TEN-79 starts*/
		var selDate=$('#datepicker').val();
		/* Added by Padmavathi for TENJINCG-1014 starts */
		if(!validateDate(selDate)){
			showMessage('Please enter valid date in this format "DD-MMM-YYYY".','error');
			return false;
		}
		/* Added by Padmavathi for TENJINCG-1014 ends */
		var selhour=$('#btntime').val();
		var selMinute=$('#btntime1').val();
		var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];

		var today=new Date();
		var curDate=today.getDate()+'-'+months[today.getMonth()]+'-'+today.getFullYear();

		if((new Date(curDate).getTime()) >= (new Date(selDate).getTime())){
			if((new Date(curDate).getTime()) == (new Date(selDate).getTime())){
				var curHour=today.getHours();
				var curMinute=today.getMinutes();
				/*Modified by Preeti for T251IT-108 starts*/
				/*changed by sahana to check hour & min is >= current time:Starts*/
				/*if(selhour<=curHour && selMinute<curMinute){
					alert("Selected Time should be greater than or equal to Current Time");
					return false;
				}*/
				if(selhour<curHour){
					alert("Selected Time should be greater than or equal to Current Time");
					return false;
				}
				if(selhour==curHour){
					if(selMinute<curMinute){
							alert("Selected Time should be greater than or equal to Current Time");
							return false;
					}
				}
				/*Modified by Preeti for T251IT-108 ends*/
				/*changed by sahana to check hour & min is >= current time:ends*/
				/*if(selhour>=curHour && selMinute>=curMinute){

				}*/			
				/*else{
					return false;
				}*/

			}
			else{
				alert("Start Date should be greater than or equal to Today's Date");
				return false;
			}
		}
		/*changed by sahana for Defect#TEN-79 ENDS*/
		var selectedTab=$('#tabselectedType').val();
		var obj = new Object();
		if(!(selectedTab=='Completed Tasks')){
			obj.schId = $('#btnSchId').val();
		}
		obj.apicode = $('#lstAPICode').val();
		obj.apilearntype = $('#lstApiLearnType').val();
		obj.date = $('#datepicker').val();
		obj.time = $('#btntime').val()+":"+$('#btntime1').val();
		obj.task = $('#txtTask').val();
		obj.client = $('#txtclientregistered').val();
		obj.status = $('#txtStatus').val();
		obj.crtBy = $('#txtCrtBy').val();
		obj.crtOn = $('#txtCrtOn').val();
		obj.browser=$('#lstBrowserType').val();
		obj.autLogin=$('#lstAppUserType').val();
		obj.appId=$('#lstApplication').val();
		/*changed by sahana for req#TEN-72 starts*/
		obj.taskName=$('#taskName').val();
		/*changed by sahana for req#TEN-72 Ends*/
		/*changed by sahana for req#TEN-145 starts*/
		obj.schRecur='N';
		/*changed by sahana for req#TEN-145 ends*/
		var jsonString = JSON.stringify(obj);
		var val = [];
		/*added by  sahana for the count of selected function code   on 28-11-2016 starts*/ 
		var count=0;
		/*added by  sahana for the count of selected function code   on 28-11-2016 ends*/ 
		if(obj.task=='Extract'){
			var i=0;
			$('.tdpval').each(function(){
				var m=new Object();
				m.funcCode=$(this).attr('func');
				m.path=$(this).val();
				if(m.path!="")
					val.push(m);
				i++;
			});
			/*added by  sahana for the count of selected function code   on 28-11-2016 starts*/ 
			count=i;
			/*added by  sahana for the count of selected function code   on 28-11-2016 ends*/ 
		}
		else{
			$(function(){
				var i=0;
				$('.tiny:checked').each(function(){
					val[i] = $(this).val();
					i++;
				});
				/*added by  sahana for the count of selected function code   on 28-11-2016 starts*/ 
				count=i;
				/*added by  sahana for the count of selected function code   on 28-11-2016 ends*/ 
			});

		}
		/*added by  sahana for the count of selected function code   on 28-11-2016 starts*/ 
		if(count==0)
		{
			showMessage('Please select atleast one function code','error');
		}
		else{
			/*added by  sahana for the count of selected function code   on 28-11-2016 ends*/ 
			if(selectedTab=='Completed Tasks'){
				$.ajax({
					url:'SchedulerServlet',
					data:{param:"NEW_SCH",json:jsonString,functions:JSON.stringify(val),csrftoken_form:csrftoken_form},
					type: 'POST',
					dataType:'json',
					success:function(data){
						if(data.status == 'success'){
							showMessage(data.message,'success');
							/*Commented by Ashiki for V2.8-63 starts*/
							/*window.setTimeout(function(){window.location='SchedulerServlet?param=FETCH_ALL_ADMIN_SCH&paramval=Scheduled';},500)*/
							/*Commented by Ashiki for V2.8-63 ends*/
						}else{
							showMessage(data.message,'error');
						}
					},
					error:function(xhr, textStatus, errorThrown){
						showMessage(errorThrown,'error');
					}
				});
			}else{
				$.ajax({
					url:'SchedulerServlet',
					data:{param:"EDIT_SCH",json:jsonString,functions:JSON.stringify(val),csrftoken_form:csrftoken_form},
					type: 'POST',
					async:false,
					dataType:'json',
					success:function(data){
						var status = data.status;
						if(status =='success'){			
							showMessage(data.message,'success');	
							window.setTimeout(function(){window.location='SchedulerServlet?param=FETCH_ALL_ADMIN_SCH&paramval=All';},500)
						}else{
							showMessage(data.message, 'error');				
						}
					},

					error:function(xhr, textStatus, errorThrown){
						showMessage(data.message, 'error');			
					}
				});
			}
			/*added by  sahana for the count of selected function code   on 28-11-2016 starts*/ 
		}
		/*added by  sahana for the count of selected function code   on 28-11-2016 ends*/ 
	});


	$('#btnBack').click(function(){	
		window.location.href='SchedulerServlet?param=FETCH_ALL_ADMIN_SCH&paramval=All';
	});
	/*Added by Priyanka for Tenj210-112 starts*/
	$(document).ready(function() {
		  $('#chk_all').click(function() {
		    var checked = this.checked;
		    $('input[type="checkbox"]').each(function() {
		      this.checked = checked;
		    });
		  });
		});
	/*Added by Priyanka for Tenj210-112 ends*/

	$('#datepicker').on('change', function() {
		var name=$('#datepicker').val(); 
		hoursVal=$("#btntime").val();
		hoursVal=populateHours(Date.parse(name));
		populateMinutes(Date.parse(name));
	});

	$('#btntime').on('change', function(){	
		var html="<option value='00'>00</option>";
		var timeval=$(this).val();
		if(hoursVal!=timeval){
			if(timeval!=null){
				for(var i=1;i<12;i++){
					if(i==1)
						html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";
					else
						html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
				}
				$('#btntime1').html(html);
			}
		}
		else{
			var name=$('#datepicker').val(); 
			hoursVal=$("#btntime").val();
			hoursVal=populateHours(Date.parse(name));
			populateMinutes(Date.parse(name));
		}
	});

});

/*added by  Parveen for  Enhancement#665   on 28-11-2016 starts*/ 
function populateUserAutTypes(appId){
	var jsonObj = fetchUserTypeForApp(appId);

	$('#lstAppUserType').html('');
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html = "<option value='-1'>-- Select One --</option>";
		var jarray = jsonObj.appUserTypes;
		if(jarray == undefined){

		}else{
			for(i=0;i<jarray.length;i++){
				var json = jarray[i];
				/*if(optAut==json.code)
				html = html + "<option value='" + json.code + "' selected='selected'>" + json.code  + "</option>";
				else*/
				html = html + "<option value='" + escapeHtmlEntities(json.code) + "'>" + escapeHtmlEntities(json.code)  + "</option>";	

			}
		}

		$('#lstAppUserType').html(html);
	}else{
		showMessage(jsonObj.message,'error');
	}
}
/*added by  Parveen for  Enhancement#665   on 28-11-2016 ends*/ 
function populateClients(){

	var jsonObj = fetchAllClients();
	var status = jsonObj.status;
	clearMessages();
	if(status == 'SUCCESS'){
		var html ="";
		var jArray = jsonObj.clients;

		for(i=0;i<jArray.length;i++){
			var client = jArray[i];
			if(client.name==client)
				html = html + "<option  value='" + escapeHtmlEntities(client.name) + "' selected='selected'>" + escapeHtmlEntities(client.name) + "</option>";
			else
				html = html + "<option  value='" + escapeHtmlEntities(client.name) + "'>" + escapeHtmlEntities(client.name) + "</option>";
		}

		$('#txtclientregistered').html(html);

	}else{
		showMessage(jsonObj.message, 'error');
	}
}
function populateModuleInfo(appId){
	var jsonObj = fetchModulesForApp(appId);
	$('#lstModules').html('');
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html = "<option value='-1'>-- Select One --</option>";
		var jarray = jsonObj.modules;
		for(i=0;i<jarray.length;i++){
			var json = jarray[i];

			html = html + "<option value='" + escapeHtmlEntities(json.code) + "'>" + escapeHtmlEntities(json.code)  + " - " + escapeHtmlEntities(json.name) + "</option>";
		}

		$('#lstModules').html(html);

	}else{
		showMessage(jsonObj.message,'error');
	}
}
function functionExists(functions,funcCode){
	var flag=false;
	for(var i=0;i<functions.length;i++){
		if(functions[i]==funcCode){
			flag=true;
			break;
		}
	}
	return flag;
}
function getfilePath(funcCode,funcFilePaths){
	var path=null;
	for(var i=0;i<funcFilePaths.length;i++){
		var m=funcFilePaths[i];
		if(m.funcCode==funcCode){
			path=m.path;
			break;
		}
	}
	return path;
}
function populateMappingModuleInfo(appId,functions,funcFilePaths){

	var jsonObj = fetchModulesForApp(appId);

	$('#tblNaviflowModules').html('');
	$('#tblNaviflowModules_body').html('');

	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html1="";
		var html = "";
		var jarray = jsonObj.modules;
		var task=$('#txtTask').val();
		if(task=='Learn'){
			html1=html1+"<thead><tr><th><input type='checkbox' class='tbl-select-all-rows' id='chk_all'/></th><th>Function Code</th><th>Function Name</th></tr></thead>";
		}else{
			html1=html1+"<thead><tr><th><input type='checkbox' class='tbl-select-all-rows' id='chk_all'/></th><th>Function Code</th><th>Input file Path</th></tr></thead>";
		}
		for(i=0;i<jarray.length;i++){
			/*Modified by Preeti for V2.8-113 starts*/
			var addClass='';
			if(i%2==0)
				addClass = 'odd';
			else
				addClass = 'even';
			var json = jarray[i];
			if(typeof functions=="undefined"){
				if(task=='Learn'){
					html = html + "<tr class='"+addClass+"'><td><input class='tiny' type='checkbox' name='"+escapeHtmlEntities(json.code)+"' value='"+escapeHtmlEntities(json.code)+"' id='"+escapeHtmlEntities(json.code)+"'></input></td><td>"+escapeHtmlEntities(json.code)+"</td><td>"+escapeHtmlEntities(json.name)+"</td></tr>";
				}
				else{
					html = html + "<tr class='"+addClass+"'><td><input class='tiny' type='checkbox' name='"+escapeHtmlEntities(json.code)+"' value='"+escapeHtmlEntities(json.code)+"' id='"+escapeHtmlEntities(json.code)+"'></input></td><td>"+escapeHtmlEntities(json.code)+"</td><td><input type='text' class='stdTextBox tdpval' id='url_path' name='urlpath' func='"+escapeHtmlEntities(json.code)+"'/></td></tr>";
				}
			}
			else{

				var flag=functionExists(functions,json.code);
				if(task=='Learn'){
					if(flag==false){
						html = html + "<tr class='"+addClass+"'><td><input class='tiny' type='checkbox' name='"+escapeHtmlEntities(json.code)+"' value='"+escapeHtmlEntities(json.code)+"' id='"+escapeHtmlEntities(json.code)+"'></input></td><td>"+escapeHtmlEntities(json.code)+"</td><td>"+escapeHtmlEntities(json.name)+"</td></tr>";
					}
					else{
						html = html + "<tr class='"+addClass+"'><td><input class='tiny' type='checkbox' name='"+escapeHtmlEntities(json.code)+"' value='"+escapeHtmlEntities(json.code)+"' id='"+escapeHtmlEntities(json.code)+"' checked='checked'></input></td><td>"+escapeHtmlEntities(json.code)+"</td><td>"+escapeHtmlEntities(json.name)+"</td></tr>";
					}
				}
				else{
					if(flag==false){
						html = html + "<tr class='"+addClass+"'><td><input class='tiny' type='checkbox' name='"+escapeHtmlEntities(json.code)+"' value='"+escapeHtmlEntities(json.code)+"' id='"+escapeHtmlEntities(json.code)+"'></input></td><td>"+escapeHtmlEntities(json.code)+"</td><td><input type='text' class='stdTextBox tdpval' id='url_path' name='urlpath' func='"+escapeHtmlEntities(json.code)+"'/></td></tr>";
					}
					else{
						var filepath=getfilePath(json.code,funcFilePaths);
						html = html + "<tr class='"+addClass+"'><td><input class='tiny' type='checkbox' name='"+escapeHtmlEntities(json.code)+"' value='"+escapeHtmlEntities(json.code)+"' id='"+escapeHtmlEntities(json.code)+"' checked='checked'></input></td><td>"+escapeHtmlEntities(json.code)+"</td><td><input type='text' class='stdTextBox tdpval' id='url_path' name='urlpath' func='"+escapeHtmlEntities(json.code)+"' value='"+filepath+"'/></td></tr>";
					}
				}

			}
		}
		
		html=html1+html;
		$('#tblNaviflowModules').html(html);
		$('#tblNaviflowModules').dataTable();
		$('#tblNaviflowModules').DataTable().destroy();
		$('#tblNaviflowModules').dataTable();
		/*Modified by Preeti for V2.8-113 ends*/
	}else{
		showMessage(jsonObj.message,'error');
	}
}

function populateAppInfo(appId){
	var jsonObj = fetchAllAuts();

	$('#lstApplication').html('');
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html = "<option value='-1'>-- Select One --</option>";
		var jarray = jsonObj.auts;
		for(i=0;i<jarray.length;i++){
			var json = jarray[i];
			if(json.id==appId)
				html = html + "<option value='" + json.id + "' selected='selected'>" + escapeHtmlEntities(json.name)+"</option>";
			else
				html = html + "<option value='" + json.id + "'>" + escapeHtmlEntities(json.name)+"</option>";
		}

		$('#lstApplication').html(html);

	}else{
		showMessage(jsonObj.message,'error');
	}
}
/*changed by nagababu for Defect #TEN-137:  Starts*/
function populateHoursInfo(hours,creationDate){
	var newHour="00";
	var html='';
	var curHour=new Date().getHours();
	var startHour;
	var today=new Date();
	var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
	var curDate=today.getDate()+'-'+months[today.getMonth()]+'-'+today.getFullYear();

	if((new Date(curDate).getTime())< (new Date(creationDate).getTime())){
		startHour="00";
	}
	else{
		if(parseInt(curHour)<=parseInt(hours)){
			startHour=curHour;
		}
		else{
			startHour=hours;
		}
	}
	$('#btntime').val('');
	for(var i=parseInt(startHour);i<24;i++){
		if(parseInt(i)<10){
			newHour="0"+parseInt(i);
		}
		else{
			newHour=i;
		}
		if(i==hours){
			html = html + "<option value='" +newHour + "' selected='selected'>" + newHour + "</option>";
		}
		else{
			html = html + "<option value='" + newHour+ "'>" + newHour + "</option>";
		}
	}
	$('#btntime').html(html);
}
function populateMinutesInfo(mins,creationDate){
	var newMin="00";
	var html='';
	var curMinute=new Date().getMinutes();
	var startMinute;
	var today=new Date();
	var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];

	var curDate=today.getDate()+'-'+months[today.getMonth()]+'-'+today.getFullYear();

	if((new Date(curDate).getTime()) < (new Date(creationDate).getTime())){
		startMinute="00";
	}
	else{
		if(parseInt(curMinute)<=parseInt(mins)){
			var n=curMinute;
			var rem=curMinute%5;
			if(rem!=0){
				if(curMinute<10){
					startMinute=curMinute+(5-rem);
					startMinute="0"+startMinute;
				}
				else{
					startMinute=curMinute+(5-rem);
				}
			}
			else{
				startMinute=curMinute;
			}

		}
		else{
			startMinute=mins;
		}
	}

	for(var i=parseInt(startMinute);i<60;i+=5){
		if(parseInt(i)<10){
			newMin="0"+parseInt(i);
		}
		else{
			newMin=i;
		}
		if(i==mins){
			html = html + "<option value='" +newMin + "' selected='selected'>" + newMin + "</option>";
		}
		else{
			html = html + "<option value='" + newMin+ "'>" + newMin + "</option>";
		}
	}
	$('#btntime1').html(html);
}
/*changed by nagababu for Defect #TEN-137:  ends*/
/*changed by sahana for defect #TEN-139  :starts*/
function populateHours(date1){
	var hoursVal= "";
	var dateval=new Date(date1);
	var month=dateval.getMonth()+1;
	var finaldate=dateval.getDate()+"-"+month+"-"+dateval.getFullYear();
	var curmonth=new Date().getMonth()+1;
	var currDate=new Date().getDate()+"-"+curmonth+"-"+new Date().getFullYear();
	var html ="";
	var getMin=new Date().getMinutes();
	var counter=0;
	var currentHour=new Date().getHours();
	var newHour;
	if(getMin>55 && getMin<=60)
	{
		for(var i=0;i<24;i++){
			if(currentHour==parseInt(i))
			{	
				if(parseInt(i)<10){
					newHour="0"+parseInt(i+1);
				}
				else{
					newHour=i+1;
				}
				if(i==currentHour){
					html = html + "<option value='" +newHour + "' selected='selected'>" + newHour + "</option>";
				}
				else{
					html = html + "<option value='" + newHour+ "'>" + newHour + "</option>";
				}
				i=i+1;
			}
			else if(i>currentHour+1){
				if(parseInt(i)<10){
					newHour="0"+parseInt(i);
				}
				else{
					newHour=i;
				}
				if(i==currentHour){
					html = html + "<option value='" +newHour + "' selected='selected'>" + newHour + "</option>";
				}
				else{
					html = html + "<option value='" + newHour+ "'>" + newHour + "</option>";
				}


			}
			/*Added by  Gangadhar Badagi to schedule the task between 55-60 minutes starts*/
			else{
				
				var newTime="";
				if(i<=9)
					newTime ="0"+i;
				else
					newTime=i;
				if(finaldate==currDate){
					if(newTime>=new Date().getHours()){

						if(counter==0){
							hoursVal=newTime;
							counter++;
						}
						html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
					}
				}
				else{	
					if(counter==0){
						hoursVal=newTime;
						counter++;
					}
					html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
				}
			}
			/*Added by  Gangadhar Badagi to schedule the task between 55-60 minutes ends*/

		}
	}
	else
	{
		for(var i=0;i<24;i++){
			var newTime="";
			if(i<=9)
				newTime ="0"+i;
			else
				newTime=i;
			if(finaldate==currDate){
				if(newTime>=new Date().getHours()){

					if(counter==0){
						hoursVal=newTime;
						counter++;
					}
					html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
				}
			}
			else{	
				if(counter==0){
					hoursVal=newTime;
					counter++;
				}
				html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
			}

		}
	}
	$('#btntime').html(html);
	return hoursVal;
}
function populateMinutes(date1){
	var html="";
	var dateval=new Date(date1);
	var month=dateval.getMonth()+1;
	var finaldate=dateval.getDate()+"-"+month+"-"+dateval.getFullYear();
	var curmonth=new Date().getMonth()+1;
	var currDate=new Date().getDate()+"-"+curmonth+"-"+new Date().getFullYear();
	var counter=0;
	var getHour=$('#btntime').val();
	var getMin=new Date().getMinutes();
	if(parseInt(getMin)>55 && parseInt(getMin)<=60)
	{
		for(var i=0;i<12;i++){
			if(i==0)
				html=html + "<option value='00'>00</option>";
			else if(i==1)
				html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";			
			else
				html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
		}
	}
	else{
		for(var i=0;i<12;i++){
			if(finaldate==currDate){
				if((i*5)>=parseInt(getMin)){
					if((i*5)<10)
						html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";			
					else
						html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
				}
			}
			else{
				if(i==0)
					html=html + "<option value='00'>00</option>";
				else if(i==1)
					html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";			
				else
					html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
			}
		}
	}
	$('#btntime1').html(html);
}
/*changed by sahana for defect #TEN-139  :ends*/
/*Added by Preeti for T251IT-181 starts*/
function fetchApiModulesTypeForApp(appId,apiCode){
	var jsonObj;
	$.ajax({
		url:'APIServlet',
		data:'param=fetch_apis_type&redirect=false&lstAppId=' + appId+'&appCode='+apiCode,
		async:false,
		dataType:'json',
		success:function(data){
			console.log('API data ' + data);
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});
	
	return jsonObj;
	
	
}

function populalteAPIOperationsType(appId,apiCode) {
	var jsonObj = fetchApiModulesTypeForApp(appId,apiCode);
	
	var jsonType="";
	var status = jsonObj.STATUS;
	if(status== 'SUCCESS'){
		
		var jarray = jQuery.parseJSON(jsonObj.API);
		 jsonType=jsonObj.API;
		jsonType=jsonType.substring(1,(jsonType.length-1));
		
		
	}else{
		showMessage(jsonObj.MESSAGE,'error');
		
	}
	
	return jsonType;
}
/*Added by Preeti for T251IT-181 ends*/
/****************************
 * Added by Sriram for TENJINCG-169 (Schedule API Learning)
 */
function populateApiCodes(appId) {
	var jsonObj = fetchApiModulesForApp(appId);
	$('#lstAPICode').html('');
	var html = "<option value='-1'>-- Select One --</option>";
	var status = jsonObj.STATUS;
	if(status== 'SUCCESS'){
		
		var jarray = jQuery.parseJSON(jsonObj.API);
		for(i=0;i<jarray.length;i++){
			var json = jarray[i];
			/*html = html + "<option value='" + json.code + "'>" + json.name + "</option>";*/
			html = html + "<option value='" + escapeHtmlEntities(json.code) + "'>" + escapeHtmlEntities(json.code)  + "</option>";
		}
		
		$('#lstAPICode').html(html);
	}else{
		/*Modified  by Preeti for T251IT-181 starts*/
		showMessage(jsonObj.MESSAGE,'error');
		/*Modified by Preeti for T251IT-181 ends*/
	}
}

function populalteAPIOperations(appId, apiCode){

	var jsonObj = fetchApiOperationsForApp(appId, apiCode);

	var val;

	$('#tblNaviflowModules').html('');
	$('#tblNaviflowModules_body').html('');
	var status = jsonObj.STATUS;
	if(status== 'SUCCESS'){
		var html1="";
		var html = "";
		var jarray = jsonObj.OPERATIONS;
		jarray = jQuery.parseJSON(jarray);
		var task=$('#txtTask').val();
		html1=html1+"<thead><tr><th><input type='checkbox' class='tbl-select-all-rows' id='chk_all'/></th><th>Operation Name</th><th>API Code</th></thead>";
		for(i=0;i<jarray.length;i++){
			/*Modified by Preeti for V2.8-113 starts*/
			var addClass='';
			if(i%2==0)
				addClass = 'odd';
			else
				addClass = 'even';
			var json = jarray[i];
			html = html + "<tr class='"+addClass+"'><td><input class='tiny' type='checkbox' name='"+escapeHtmlEntities(json.name)+"' value='"+escapeHtmlEntities(json.name)+"' id='"+escapeHtmlEntities(json.name)+"'></input></td><td>"+escapeHtmlEntities(json.name)+"</td><td>"+escapeHtmlEntities(apiCode)+"</td></tr>";
		}
		val=$("#url_path").val();
		html=html1+html;
		$('#tblNaviflowModules').html(html);
		$('#tblNaviflowModules').dataTable();
		$('#tblNaviflowModules').DataTable().destroy();
		$('#tblNaviflowModules').dataTable();
		/*Modified by Preeti for V2.8-113 ends*/
	}else{
		/*Modified by Preeti for T251IT-181 starts*/
		showMessage(jsonObj.MESSAGE,'error');
		/*Modified by Preeti for T251IT-181 ends*/
	}
}