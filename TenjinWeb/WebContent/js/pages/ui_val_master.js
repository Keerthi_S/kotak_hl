/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ui_val_master.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 24-Jul-2017			Sriram					Newly added for TENJINCG-283 (Epic)
* 24-Jul-2017			Sriram					TENJINCG-309
* 26-Jul-2017			Sriram					TENJINCG-310
* 18-Aug-2017			Sriram					T25IT-207
* 26-06-2018            Padmavathi              T251IT-73 
*/
/* For TENJINCG-309 */
$(document).on('click', '.uiselector', function() {
	var $parentTr = $(this).parent().parent();
	var $valueElement = $parentTr.find('.exp-outcome');
	if($(this).prop('checked')) {
		/*$valueElement.prop('disabled',false);*/
		$valueElement.show();
	}else{
		/*$valueElement.prop('disabled',true);*/
		$valueElement.hide();
	}
});

/*Added by Padmavathi for T251IT-73 starts*/
$(document).on("click", ".tbl-select-all-rows", function() {
	var chkId = $(this).attr('id');
	var $table;
	if(chkId === 'chk_all') {
		$table = $('#tbl_fieldsToValidate');
	}else if(chkId === 'pre_chk_all') {
		$table = $('#tbl_preSteps');
	}
	
	if($(this).prop('checked')) {
		$table.find('.exp-outcome').show();
	}else{
		$table.find('.exp-outcome').hide();
	}
});
/*Added by Padmavathi for T251IT-73 ends*/

function populateValidationTypes() {
	$('#img_loadValTypes').show();
	$.ajax({
		url:'UIValidationServlet?param=ajax_fetch_all_val_types',
		dataType:'json',
		success:function(data) {
			if(data.status === 'success') {
				//showMessage('validation types fetched successfully!', 'success');
				paintUIValTypeOptions(data.valtypes);
				$('#img_loadValTypes').hide();
			}else{
				showMessage(data.message, 'error');
				$('#img_loadValTypes').show();
			}
		},
		error:function(xhr, textStatus, errorThrown) {
			showMessage('Could not fetch Validation Types due to an internal error. Please contact Tenjin Support', 'error');
		}
	});
}

function paintUIValTypeOptions(options) {
	$('#lstValidationType').html('');
	var $defOption = $("<option value='-1'>-- Select One --</option>");
	$('#lstValidationType').append($defOption);
	for(var i=0; i<options.length; i++) {
		var option = options[i];
		var $option = $("<option />");
		$option.attr('value', option.id);
		$option.attr('data-description', option.description);
		$option.text(option.name);
		$option.attr('data-formula', option.formula);
		$option.attr('data-target', option.target);
		$option.attr('data-def-exp', option.defaultExpectedValues);
		$('#lstValidationType').append($option);
		
	}
	$('#validationTypeHelp').text('Select a validation type to view its description.');
}

function loadPageAreas() {
	$('#img_loadPages').show();
	$('#lstPageArea').html("<option value='-1'>-- Select One --</option>");
	$.ajax({
		url:'UIValidationServlet?param=ajax_fetch_locations&app=' + $('#appId').val() + '&func=' + $('#functionCode').val(),
		dataType:'json',
		success:function(data) {
			if(data.status === 'success') {
				for(var i=0; i<data.locations.length; i++) {
					var loc = data.locations[i];
					var $option = $("<option />");
					$option.val(loc.locationName);
					$option.text(loc.locationName);
					$('#lstPageArea').append($option);
				}
				
				$('#img_loadPages').hide();
			}else{
				showMessage(data.message,'error');
				$('#img_loadPages').hide();
			}
		},
		error:function(xhr, textStatus, errorThrown) {
			showMessage('Could not load page areas due to an internal error. Please contact Tenjin Support.', 'error');
			$('#img_loadPages').hide();
		}
	});
	
	pagesLoaded = true;
}



function loadFields(pageAreaName, loadSelectedFields) {
	$('#img_loadPages').show();
	$('#tbl_fieldsToValidate tbody').html('');
	$.ajax({
		url:'UIValidationServlet?param=ajax_fetch_fields&app=' + $('#appId').val() +'&func=' + $('#functionCode').val() + '&loc=' +pageAreaName,
		dataType:'json',
		success:function(data) {
			if(data.status === 'success') {
				for(var i=0; i<data.locations.length; i++) {
					var t = data.locations[i];
					var $tr = $("<tr/>");
					var $selectTd = $("<td><input class='uiselector' type='checkbox' id='" + t.sequence + "' data-uname='" + t.name + "' /></td>");
					var $labelTd = $("<td>"+ t.name +"</td>");
					var $expTd = $("<td />");
					if(defExpValues !== undefined && defExpValues.length > 0 && defExpValues !== 'null') {
						var defArray = defExpValues.split(",");
						/*var $select = $("<select class='stdTextBoxNew exp-outcome' disabled />");*/
						var $select = $("<select class='stdTextBoxNew exp-outcome' style='display:none;' />");
						$.each(defArray, function(i) {
							$select.append($("<option value='" + defArray[i] + "'>" + defArray[i] +"</option>"));
						})
						$expTd.append($select);
					}else{
						/*$expTd.append($("<input type='text' class='stdTextBox exp-outcome' disabled/>"));*/
						$expTd.append($("<input type='text' class='stdTextBox exp-outcome' style='display:none;'/>"));
					}
					
					$tr.append($selectTd);
					$tr.append($labelTd);
					$tr.append($expTd);
					
					$('#tbl_fieldsToValidate tbody').append($tr);
				}
				//selectValidatedFields();
				
				if(loadSelectedFields) {
					selectValidatedFields();
				}
				
				loadPreFields(pageAreaName, loadSelectedFields, data.locations);
				
				$('#img_loadPages').hide();
			}else{
				showMessage(data.message,'error');
				$('#img_loadPages').hide();
			}
		},
		error:function(xhr, textStatus, errorThrown) {
			showMessage('Could not load fields due to an internal error. Please contact Tenjin Support.', 'error');
			$('#img_loadPages').hide();
		}
	});
}

function selectValidatedFields() {
	var jsonStr = $('#valStepsJson').val();
	var selectedVals = JSON.parse(jsonStr);
	
	for(var i=0; i<selectedVals.length; i++) {
		var val = selectedVals[i];
		var label = val.FLD_LABEL;
		
		var $checkbox = $('#tbl_fieldsToValidate tbody').find("input[data-uname='" + label +"']");
		$checkbox.prop('checked',true);
		
		var $parentTr = $checkbox.parent().parent();
		/*$parentTr.find('.exp-outcome').prop('disabled',false);*/
		$parentTr.find('.exp-outcome').show();
		var expResult = val.DATA;
		var expResArray = expResult.split('(');
		var expVal = expResArray[1];
		if(expVal.endsWith(')')) {
			expVal = expVal.slice(0,-1);
		}
		$parentTr.find('.exp-outcome').val(expVal);
	}
}
/* For TENJINCG-309 ends*/

/* For TENJINCG-310 */
function selectPreFields() {
	var jsonStr = $('#preStepsJson').val();
	var selectedVals = JSON.parse(jsonStr);
	
	for(var i=0; i<selectedVals.length; i++) {
		var val = selectedVals[i];
		var label = val.FLD_LABEL;
		
		/*if(label.toLowerCase() !== 'button') {
			var $checkbox = $('#tbl_preSteps tbody').find("input[data-uname='" + label +"']");
			$checkbox.prop('checked',true);
			
			var $parentTr = $checkbox.parent().parent();
			$parentTr.find('.exp-outcome').prop('disabled',false);
			$parentTr.find('.exp-outcome').show();
			var expResult = val.DATA;
			$parentTr.find('.exp-outcome').val(expVal);
		}else{
			var expResult = val.DATA;
			var $checkbox = $('#tbl_preSteps tbody').find("input[data-uname='" + expResult +"']");
		}*/
		
		var $checkbox;
		var expResult = val.DATA;
		var valueToSet = expResult;
		
		if(label.toLowerCase() === 'button') {
			$checkbox = $('#tbl_preSteps tbody').find("input[data-uname='" + expResult +"']");
			valueToSet = "Click";
		}else{
			$checkbox = $('#tbl_preSteps tbody').find("input[data-uname='" + label +"']");
		}
		$checkbox.prop('checked',true);
		var $parentTr = $checkbox.parent().parent();
		$parentTr.find('.exp-outcome').show();
		$parentTr.find('.exp-outcome').val(valueToSet);
		
	}
}

function loadPreFields(pageAreaName, loadSelectedFields, fields) {
	$('#img_loadPages').show();
	$('#tbl_preSteps tbody').html('');
	for(var i=0; i<fields.length; i++) {
		var t = fields[i];
		var $tr = $("<tr/>");
		var $selectTd = $("<td><input class='uiselector' type='checkbox' id='" + t.sequence + "' data-uname='" + t.name + "' data-object-class='"+ t.objectClass +"'/></td>");
		var $labelTd = $("<td>"+ t.name +"</td>");
		var $expTd = $("<td />");
		
		var oClass = t.objectClass;
		if(oClass.toLowerCase() === 'list' || oClass.toLowerCase() === 'checkbox' || oClass.toLowerCase() === 'radio' || oClass.toLowerCase() === 'radio button') {
			var defaultValues = t.defaultOptions;
			//Fix for T25IT-207
			if(defaultValues === undefined) {
				defaultValues = '';
			}
			//Fix for T25IT-207 ends
			var defArray = defaultValues.split(';');
			var $select = $("<select class='stdTextBoxNew exp-outcome' style='display:none;' />");
			for(var j=0; j<defArray.length; j++) {
				var $option = $("<option value='" + defArray[j] + "'>" + defArray[j] + "</option>");
				$select.append($option);
			}
			$expTd.append($select);
		}else if(oClass.toLowerCase() ==='button') {
			var $inputElement = $("<input readonly value='Click' type='text' class='stdTextBox exp-outcome' style='display:none;'/>");
			$expTd.append($inputElement);
		}else{
			var $inputElement = $("<input placeholder='Enter input data here' type='text' class='stdTextBox exp-outcome' style='display:none;'/>");
			$expTd.append($inputElement);
		}
		
		$tr.append($selectTd);
		$tr.append($labelTd);
		$tr.append($expTd);
		
		$('#tbl_preSteps tbody').append($tr);
	}
	//selectValidatedFields();
	
	if(loadSelectedFields) {
		selectPreFields();
	}
}
/* For TENJINCG-310 ends*/
