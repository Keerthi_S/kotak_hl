/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  edituserautdetails.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 17-Nov-2016			Manish					DTT User Credentials
* 15-06-2018            Padmavathi              T251IT-18
* 03-01-2019			Ashiki					TJN252-63 
* 25-03-2019			Pushpalatha				TENJINCG-968
*/	



$(document).ready(function(){
	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('defect_user_mapping_form');
	
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	$('#btnBack').click(function(){
		window.location.href = 'DefectServlet?param=fetch_def_user_mapping_data';
	});
	/* Added by Padmavathi for T251IT-18 starts*/
	$('#lstInstance').change(function(){
		var name=$('#DTTUserName').val();
		var instance=$('#lstInstance').val();
		var userId = $('#txtUserId').val();
		$.ajax({
			url:'DefectServlet',
			data:'param=check_dtt_user_name&paramval='+name+'&instance='+instance+'&userId='+userId,
			async:false,
			dataType:'json',
			success: function(data){
			var status = data.status;
			if(status == 'ERROR'){		
				showMessage(data.message, 'error');
				$("#DTTUserName").focus();
			}
		},
		
		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown, 'error');			
		}
	});
	}); 
	/* Added by Padmavathi for T251IT-18 ends*/
	$('#btnSave').click(function(){
		//Added by Priyanka for TCGST-38 starts 
		var validated = validateForm('defect_user_mapping_form');
		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
			//Added by Priyanka for TCGST-38 ends 
		}
		var json = new Object();
		json.instance = $('#lstInstance').val();
		json.dttUser = $('#DTTUserName').val();
		/*Changed by Ashiki for TJN252-63 starts*/
		/*json.dttUserPwd =$('#DTTPassword').val();*/
		var dttUserPwd =encodeURIComponent($('#DTTPassword').val());
		/*Changed by Ashiki for TJN252-63 ends*/
		/* Added by Padmavathi for T251IT-18 starts*/
		json.oldInstance=$('#dmInstance').val();
		/* Added by Padmavathi for T251IT-18 ends*/
		var jstring = JSON.stringify(json);
		$.ajax({
			url:'DefectServlet',
			/*Changed by Ashiki for TJN252-63 starts*/
			/*data:'param=update_user_defect_mapping&json='+jstring,*/
			data:'param=update_user_defect_mapping&json='+jstring+'&dttUserPwd='+dttUserPwd,
			/*Changed by Ashiki for TJN252-63 ends*/
			async : false,
			dataType : 'json',
			success : function(data) {
				var status = data.status;
				if (status == 'SUCCESS') {
					
					
					showMessage(data.message,
							'success');
				} else {
					showMessage(data.message,
							'error');
				}
			}
		});
		
		
	});
});