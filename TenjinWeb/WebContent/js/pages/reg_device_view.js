/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  reg_device_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 09-01-2018			Sahana 					#TENJINCG-593
 * 25-03-2019			Pushpalatha				TENJINCG-968
 */	



$(document).ready(function() {

	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('device_details');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	var deviceRecId=$('#txtDevRecId').val();
	var optstatus = $('#deviceStatus').val();
	$('#txtDeviceStatus').children('option').each(function(){
		var val = $(this).val();
		if(val == optstatus){
			$(this).attr({'selected':true});
		}
	});

	$('#btnSave').click(function(event) {

		var validated = validateForm('device_details');
		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
		}

		var obj = new Object();

		obj.deviceName = $('#txtDeviceName').val();
		obj.platformVersion=$('#txtPlatformVersion').val();
		obj.deviceRecId=$('#txtDevRecId').val();
		obj.deviceStatus=$('#txtDeviceStatus').val();
		obj.desc=$('#txtRemarks').val();
		var jsonString = JSON.stringify(obj);

		$.ajax({
			url:'DeviceServlet',
			data:{param:"UPDATE_DEVICE",json:jsonString},
			dataType:'json',
			success:function(data){
				if(data.status == 'SUCCESS'){
					showMessage(data.message,'success');
					window.setTimeout(function(){window.location='DeviceServlet?param=FETCH_ALL_DEVICES';},500)
					
				}else{
					showMessage(data.message,'error');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
			}
		});

	});
	
	$('#btnBack').click(function(e){
		window.location.href ="DeviceServlet?param=FETCH_ALL_DEVICES";
	});
	
	
});
function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode != 46 && charCode > 31 
     && (charCode < 48 || charCode > 57))
      return false;

   return true;
}

