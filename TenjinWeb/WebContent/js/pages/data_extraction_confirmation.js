/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  data_extraction_confirmation.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 8-Nov-2016			Sriram				Screen changed for Tenjin v2.4.2
 * 03-02-2017            Leelaprasad         TENJINCG-92
 */
$(document).ready(function(){
	$('#lstModules').select2();

	var scrStatus = $('#screenStatus').val();
	var scrMessage = $('#screenMessage').val();
	if(scrMessage == ''){
		clearMessages();
	}else{
		if(scrStatus == 'SUCCESS'){
			showMessage(scrMessage, 'success');
		}else{
			showMessage(scrMessage,'error');
		}
	}

	$('#btnBack').click(function(){
		window.location.href='ExtractorServlet?param=new';
	});

	$('#btnExtract').click(function(){
		/*Changed by Leelaprasad for the Requirement TENJINCG-92 starts*/
		if($('#QueryField').val()=="Not Specified."){
			showMessage('Please Enter Query field in uploaded Template','error');
			return false;
		}
		/*Changed by Leelaprasad for the Requirement TENJINCG-92 ends*/
		/*var selection = '';
		$('#tbl-extraction-data').find('input[type="checkbox"]:checked').each(function () {
			if(this.id != 'chk_all_records'){
				selection = selection + this.id + ';';
			}
		});

		if( selection.charAt(selection.length-1) == ';'){
			selection = selection.slice(0,-1);
		}

		if(selection.length > 0){

			$('#selection').val(selection);
		}else{
			showMessage('Please choose atleast one record to extract data','error');
			return false;
		}*/

		var formOk = validateForm('extraction-form');
		if(formOk != 'SUCCESS'){
			showMessage(formOk,'error');
			return false;
		}
	});
});

