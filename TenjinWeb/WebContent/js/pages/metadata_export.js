/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  metadata_export.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY     			DESCRIPTION
* 30/08/2016			Avinash					Changes 30-08-2016 - Tenjin v.2.3 Enhancement on Metadata
*/
$(document).ready(function(){
	 $('.application_mask').hide();
	 $('.function_mask').hide();
	 $('.progress_mask').hide();
	 
	 $('#btnBack').click(function(){
		window.location.href='autfunclist_2.jsp'; 
	 });
	 
	 $('#radio_application').change(function(){
		  if($('#radio_application').is(':checked')){
			  $('#user-message').hide();
			  $('.application_mask').show();
			  $('.function_mask').hide();
			  $('#tblMetadataModules').find('tr').not(':first').remove();
			  $('#lstApplication').prop('selectedIndex', 0);
			  $('input:checkbox[id="chk_all_modules"]').prop('checked',false);
			  $('#dynlegend').text('Applications');
			  //clearModuleCheckbox();
		  }
	  });
	  
	  $('#radio_function').change(function(){
		  if($('#radio_function').is(':checked')){
			  $('#user-message').hide();
			  $('.function_mask').show();
			  $('.application_mask').hide();
			  $('#dynlegend').text('Functions');
			  clearAppCheckbox();
		  }
	  });
	  
	  $('#export').click(function(){
		  var validationStatus = validateForm('aut_metadata_form');
			// SUCCESS

			$('#txtcounter').val(0);
			$('#txttotal').val(0);
			progressBar(0, $('#progressBar'));
			if (validationStatus != 'SUCCESS') {
				showMessage(validationStatus, 'error');
				return false;
			}

		  var retValue = dataToBeExport();
		  if(retValue != undefined){
			  $('.progress_mask').show();
		  }
		  window.setTimeout(function(){
			  if(retValue != undefined){
				  var flag = true;
				  var app_id = $('#lstApplication').val();
				  var fileName = $('#txtFileName').val();
				  var csrftoken_form = $('#csrftoken_form').val();
				  $.ajax({
						url:'MetaDataServlet',
						data :'param=export&data='+retValue+'&app_id='+app_id+'&fileName='+fileName+'&csrftoken_form='+csrftoken_form,
						type : 'post',
						async:true,
						dataType:'json',
						success:function(data){
							var status = data.status;
							if(status == 'SUCCESS'){
								var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
								$("body").append($c);
			                    $("#downloadFile").get(0).click();
			                    $c.remove();
			                    $('.progress_mask').hide();
			                    window.clearInterval(interval);
							}else{
								var message = data.message;
								$('.progress_mask').hide();
								showMessage(message,'error');
							}
						},
						error:function(xhr, textStatus, errorThrown){
							showMessage(errorThrown, 'error');
						}
				     });  
				  var interval = window.setInterval(function(){
						$.ajax({
							url : "MetaDataServlet",
							type:"GET",
							async : true,
							data : {param : "progress_bar"},
							dataType:'json',
							success : function(data) {
								if(data.status == 'success'){
									var total = data.total;
									var counter = data.counter;
									var percentage = 0;
									if(counter == 0){
										percentage = 1;
									}else{
										percentage = ((counter/total)*100).toFixed(0);
									}
									
									$('#txtcounter').val(counter+1);
									
									$('#txttotal').val(total);
									
									progressBar(percentage, $('#progressBar'));
								}				
							},
							error : function(xhr, textStatus, errorThrown) {
								showMessage(errorThrown, 'error');
							}
						});	
					}, 1000);
				 }
			  else if($('.application_mask').is(':visible')){
				  showMessage('Select any one Application');
			  }else if($('.function_mask').is(':visible')){
				  showMessage('Select any one Function');
			  }else{
				  showMessage('Select any one Option');
			  }  
		  }, 1000);
	  });
	  	  $('#audit').click(function(){
		 window.location.href = "MetaDataServlet?param=audit&type=EXPORT";
	  });
	  
	  function dataToBeExport(){
		  var retValue;
		  
		  if($('.application_mask').is(":visible")){
			  var temp;
			  var selectedApps = '';
			  retValue = 'app;'
			  if($('#chk_all_apps').is(':checked')){
				  selectedApps += 'allAut;';
			  }else{
				  selectedApps += 'selected_auts;';
				  temp = selectedApps;
				  $("input:checkbox[name=appName]:checked").each(function () {
					selectedApps += $(this).val() + ';';
				});
			  }
			  if(temp == selectedApps){
				  retValue = undefined;
			  }else{
			  retValue += selectedApps;
			  }
		  }else if($('.function_mask').is(':visible')){
			  var temp;
			  var selectedModules = '';
			  retValue = 'func;';
			  if($('#chk_all_modules').is(':checked')){
				  
				var totcount = 0;
				var checked = 0;
					  $('input:checkbox[name="fcode"]').each(function(){
							totcount++;
							var checkBoxVal = (this.checked ? $(this).val() : "");
							if(checkBoxVal!=""){
								checked++;
							}
						});
					  
					  if(checked != totcount){
						  selectedModules += 'gfunc;';
						  temp = selectedModules;
						  $("input:checkbox[name=fcode]:checked").each(function () {
								 selectedModules += $(this).val() + ';';
							    });
					  }else if(checked == totcount){
						  retValue = 'app;';
						  selectedModules = 'selected_auts;'+$('#lstApplication').val()+';';
					  }
			  }else{
				  selectedModules += 'gfunc;';
				  temp = selectedModules;
				  $("input:checkbox[name=fcode]:checked").each(function () {
						 selectedModules += $(this).val() + ';';
					    });
			  }
			  if(temp == selectedModules){
				  retValue = undefined;
			  }else{
			  retValue += selectedModules;
			  }
		  }
		  return retValue;
	  }
	  
	  $('input:checkbox[name="appName"]').click(function(){
			 var totcount = 0;
			 var checked = 0;
			  $('input:checkbox[name="appName"]').each(function(){
					totcount++;
					var checkBoxVal = (this.checked ? $(this).val() : "");
					if(checkBoxVal!=""){
						checked++;
					}
				});
			  
			  if(checked != totcount){
				  if($('input:checkbox[id="chk_all_apps"]').is(':checked')){
					  $('input:checkbox[id="chk_all_apps"]').prop('checked', false);
				  }
			  }else if(checked == totcount){
					  $('input:checkbox[id="chk_all_apps"]').prop('checked', true);
			  }
		  });
	  
	  $('input:checkbox[id="chk_all_apps"]').change(function(){
		  if($('input:checkbox[id="chk_all_apps"]').is(':checked')){
			  $('input:checkbox[name="appName"]').each(function(){
				  if($(this).is(':checked')){
					  
				  }else{
					  $(this).prop('checked', true);  
				  }
			  });
		  }
		  else{
			  $('input:checkbox[name="appName"]').each(function(){
				  if($(this).is(':checked')){
					  $(this).prop('checked', false);
				  }else{
					    
				  }
			  });
		  }
	  });
	  
	  $('input:checkbox[name="fcode"]').change(function(){
		  	$('.function_mask').show();
			 var totcount = 0;
			 var checked = 0;
			  $('input:checkbox[name="fcode"]').each(function(){
					totcount++;
					var checkBoxVal = (this.checked ? $(this).val() : "");
					if(checkBoxVal!=""){
						checked++;
					}
				});
			  if(checked == 0){
				  checked = 1;
			  }
			  if(checked != totcount){
				  if($('input:checkbox[id="chk_all_modules"]').is(':checked')){
					  $('input:checkbox[id="chk_all_modules"]').prop('checked', false);
				  }
			  }else if(checked == totcount){
					  $('input:checkbox[id="chk_all_modules"]').prop('checked', true);
			  }
		  });
	  
	  $('input:checkbox[id="chk_all_modules"]').change(function(){
		  if($('input:checkbox[id="chk_all_modules"]').is(':checked')){
			  $('input:checkbox[name="fcode"]').each(function(){
				  if($(this).is(':checked')){
					  
				  }else{
					  $(this).prop('checked', true);  
				  }
			  });
		  }
		  else{
			  $('input:checkbox[name="fcode"]').each(function(){
				  if($(this).is(':checked')){
					  $(this).prop('checked', false);
				  }else{
					    
				  }
			  });
		  }
	  });
	  

		$('#lstApplication').change(function(){
			var selApp = $(this).val();
			clearMessages();
			if(selApp == '-1'){
				
			}else{
				$('#tblMetadataModules').find('tr').not(':first').remove();
				var options='';
				  $.ajax({
					url:'MetaDataServlet',
					data :'param=get_func'+ '&app_id=' + selApp,
					async:false,
					dataType:'json',
					success: function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						var dArray = data.modules;
						for(var i=0;i<dArray.length;i++){
							var fun = dArray[i];
							options += '<tr>'+
							'<td class="tiny"><input type="checkbox" name="fcode" value="'+fun.code+'" id="'+fun.code+'"/></td>'+
							'<td>'+ fun.code +'</td>'+
							'<td>'+ fun.name +'</td>'+
							'</tr>';
						}
						$('#tblMetadataModules_body').html(options);
						$('#tblMetadataModules').dataTable();
					  }
					  else{
						showMessage(data.message, 'error');				
					  }
				    },
				   error:function(xhr, textStatus, errorThrown){
					   showMessage(textStatus, 'error');			
				  }
			     });
			}
		});

	  
	  function clearAppCheckbox(){
		  $('input:checkbox[name="appName"]').each(function(){
			  var checkBoxVal = (this.checked ? $(this).val() : "");
				if(checkBoxVal!=""){
					$(this).prop('checked', false);
				} 
			});
		  $('input:checkbox[id="chk_all_apps"]').prop('checked',false);
	  }
	  
	  function clearModuleCheckbox(){
		  $('input:checkbox[name="fcode"]').each(function(){
			  var checkBoxVal = (this.checked ? $(this).val() : "");
				if(checkBoxVal!=""){
					$(this).prop('checked', false);
				} 
			});
	  }
	  
	  function getAllCheckedApps(){
			var checkedModules = '';
			$('input:checkbox[name="appName"]').each(function(){
				var checkBoxVal = (this.checked ? $(this).val() : "");
				if(checkBoxVal!=""){
				checkedModules += checkBoxVal+';';
				} 
			});
			return checkedModules;
		}
});
