/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  prj_mail.setting.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 02-05-2018			Preeti					TENJINCG-656
 * 10-05-2018			Pushpalatha				TENJINCG-629
 * 06-12-2018			Pushpalatha				TJNUN62-70,TJNUN62-61
*/ 
$(document).ready(function(){
	/*Added by Pushpalatha for TENJINCG-629 starts*/
	$("#txtEsc").hide();
	$("#txtSub").hide();
	
	
	var selEscRuleId=$('#escRuleId').val();
	var count=0;
	$('input:radio').each(function() {
		
		if(selEscRuleId==0){
			if(count==0){
			$("#"+$(this).attr('id')).prop("checked", true);
			count=count+1;
			}
		}else{
		if($("#"+$(this).attr('id')).val()==selEscRuleId){
			$("#"+$(this).attr('id')).prop("checked", true);
		}else{
			$("#"+$(this).attr('id')).prop("checked",false);
		}
		}
	});
	
	var escRulePercentage=$('#escRulePercentage').val();
	
	$('input:radio').each(function() {
		
		if($(this).is(':checked')) {
		
		}else{
			var idlength=$(this).attr('id').length;
			var id=$(this).attr('id').substr(idlength-1,idlength);
		$("#"+this.id+id).val('');
		}
	});
	
	
	$('input:radio').each(function() {
		  if($(this).is(':checked')) {
		    
			  $(this).attr('id');
				var idlength=$(this).attr('id').length;
				var id=$(this).attr('id').substr(idlength-1,idlength);
			$("#"+this.id+id).show();
			$("."+this.id+id).show();
			
		  } 
		  else {
			  $(this).attr('id');
				var idlength=$(this).attr('id').length;
				var id=$(this).attr('id').substr(idlength-1,idlength);
			$("#"+this.id+id).hide();
			$("."+this.id+id).hide();
		  }
		});
	
	if($('#txtEscRule').val()=='Y'){
		$('#txtEscRule').attr('checked',true)
		$("#txtEsc").show();
	 }else{
		 $('#txtEscRule').attr('checked',false)
		 $('#txtEsc').hide();
	 }
	
	/*Added by Pushpalatha for TENJINCG-629 ends*/
	
	$('#btnBack').click(function(){
		var projectId = $('#projectid').val();
		/*window.location.href = 'ProjectServlet?param=PROJECT_VIEW&paramval=' + projectId + '&v=PROJECT';*/
		window.location.href = 'ProjectServletNew?t=load_project&paramval=' + projectId + '&v=PROJECT';
	});
		
	if($('#txtemail').val()=='Y'){
		$('#txtemail').attr('checked',true)
		$("#txtSub").show();
	 }else{
		 $('#txtemail').attr('checked',false)
		 $('#txtSub').hide();
	 }
	
	$('#btnSave').click(function(){
	if ( $('#txtemail').is(':checked') ) {
		$('#txtemail').val('Y');
	} else {
		$('#txtemail').val('N');
	}
	/*Added by Pushpalatha for TENJINCG-629 starts*/
	if($('#txtEscRule').is(':checked')){
	$('input:radio').each(function() {
		if($(this).is(':checked')) {
			 $(this).attr('id');
				var idlength=$(this).attr('id').length;
				var id=$(this).attr('id').substr(idlength-1,idlength);
				$("#"+this.id+id).prop('required',true);
			var rulePercentage=$("#"+this.id+id).val();
			if(rulePercentage==''|| rulePercentage=='0'){
				showLocalizedMessage('invalid.input', 'error');
				return false;
			}
			var ruleDesc=$("#"+this.id+id+id).val();
			
			
			 $('#main_form').append($("<input type='hidden' id='ruleCriteria' name='ruleCriteria' value='"+rulePercentage+"' />"));
			 $('#main_form').append($("<input type='hidden' id='ruleDesc' name='ruleDesc' value='"+ruleDesc+"' />"));
			 
		}
			
	});
	}
	/*Added by Pushpalatha for TENJINCG-629 ends*/
	});
	
	
	
	$('.rulePercentage').keyup(function (){
		var inputVal = $(this).val();
	
		var regex = new RegExp(/[^0-9]/g);
		
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			alert('Enter only Numbers ');
			$(this).val('');
		}
		/*Added by Pushpalatha for TJNUN62-70 starts*/
		if(!inputVal=='' && inputVal==0){
			alert('Entered percentage should be greater than zero.');
			$(this).val('');
		}
		/*Added by Pushpalatha for TJNUN62-70 ends*/
		/*Added by Pushpalatha for TJNUN262-61 starts*/
		if(inputVal>100){
			alert('Entered percentage should be less than or equal to hundred.');
			$(this).val('');
		}
		/*Added by Pushpalatha for TJNUN262-61 ends*/
	});
	
	
});
/*Added by Pushpalatha for TENJINCG-629 starts*/
$(document).on('change','.escalationRules',function(){
	
		$('input:radio').each(function() {
			  if($(this).is(':checked')) {
			    
				  $(this).attr('id');
					var idlength=$(this).attr('id').length;
					var id=$(this).attr('id').substr(idlength-1,idlength);
				$("#"+this.id+id).show();
				
			  } 
			  else {
				  $(this).attr('id');
					var idlength=$(this).attr('id').length;
					var id=$(this).attr('id').substr(idlength-1,idlength);
				$("#"+this.id+id).hide();
			  }
			});

	});


		
$(document).on('change','#txtEscRule',function(){
	if($("#txtEscRule").is(":checked")){
		
		$("#txtEsc").show();
	}else{
		$("#txtEsc").hide();
	}
});

$(document).on('change','#txtemail',function(){
	if($("#txtemail").is(":checked")){
		
		$("#txtSub").show();
	}else{
		$("#txtSub").hide();
	}
});

/*Added by Pushpalatha for TENJINCG-629 ends*/

