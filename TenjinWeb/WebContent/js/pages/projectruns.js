/**
 *  

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  projectruns.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India



 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 						latief					 for requirement# TJN_24_17 
 * 10-JAN-2016           Sahana                 TENJINCG-7(Calendar Icon is to be added for all date fields)
 * 28-July-2017			Gangadhar Badagi		TENJINCG-315
 * 30-08-2017			Padmavathi		        T25IT-246,385
 * 25-Oct-2017			Preeti					TENJINCG-372
 * 20-02-2018			Preeti					TENJINCG-558
 * 15-06-2018			Preeti					T251IT-45
 *  */





$(function() {
	$('#datepicker').datepicker({ 
		  /* Changed by sahana for the improvement of TENJINCG-7: starts */ 
		showOn: 'button',
		buttonText: 'Show Date',
		buttonImageOnly: true,
		buttonImage: './images/button/calendar.png',
		constrainInput: true,
		  /* Changed by sahana for the improvement of TENJINCG-7: ends  */
		dateFormat:'dd-M-yy' 
	});

}); 
/*Added by Preeti for TENJINCG-372 starts*/
function populateFunctionInfo(appId){
	var jsonObj=null;
	jsonObj=fetchModulesForApp(appId);
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html = "<option value=''>-- Select One --</option>";
		var jarray = jsonObj.modules;
		for(i=0;i<jarray.length;i++){
			var json = jarray[i];
			html = html + "<option value='" + json.code + "'>" + json.code  + "</option>";
		}
		
		$('#txtfunction').html(html);
	}else{
		var html = "<option value=''>-- Select One --</option>";
		$('#txtfunction').html(html);
		/*showMessage(jsonObj.message,'error');*/
	}
}
/*Added by Preeti for TENJINCG-372 ends*/

$(document).ready(function(){
	 /* Added by Preeti for TENJINCG-372 starts*/
	$('#btnClose').click(function(){
		clearMessages();
	});
	/* Added by Preeti for TENJINCG-372 starts*/
	/*Changed by Gangadhar Badagi for TENJINCG-315 starts*/
	$('#showAllRuns').click(function() {
		window.location.href='ProjectRunServlet?param=PROJECT_RUNS&pid=' + $('#projectId').val();
	});
	/*Changed by Gangadhar Badagi for TENJINCG-315 ends*//*Added By padmavathi for T25IT-326 starts*/
	/*Added by Padmavathi for T25IT-246  starts*/
    $('#btnCloseModal').click(function(){
    	clearMessages();
    });
    /*Added by Padmavathi for T25IT-246 ends */
	$('#btnSearch').click(function(){
		
		clearMessages();
		var runId = $('#txtRunId').val();
		var txtfunction = $('#txtfunction ').val();
		var startedby = $('#startedby').val();
		var result = $('#lstStatus').val();
		/*Modified by Preeti for T251IT-45 starts*/
		/*var tcase = $('#txttcid').val();
		var tstep = $('#txttstepid').val();*/
		var tcase = $.trim($('#txttcid').val());
		var tstep = $.trim($('#txttstepid').val());
		/*Modified by Preeti for T251IT-45 ends*/
		var application = $('#txtapplication').val();
		var startedon = $('#datepicker').val();
		var projectid = $('#txtprojectid').val();


		if(runId == '' && txtfunction  == '' && startedby == '' && result == '' && tcase == '' && tstep == ''  && application ==''&& startedon =='')
		{

			showMessage('Please enter filter criteria','error');
			return false;
		}

		clearMessages();
		/*Added by Padmavathi for T25IT-246,385  starts*/
		var str='-';

        if(runId!=''){
		if(runId.indexOf(str)!='-1'){
			showMessage('Please enter only positive number','error');
			return false;
		}
		if(runId==0){
			showMessage('Please enter a number which is greater than 0','error');
			return false;
		}
        }
        /*Added by Padmavathi for T25IT-246,385 ends */
		/*Changed by Gangadhar Badagi for TENJINCG-315 starts*/

		/*$('#img_report_loader').show();


		$.ajax({
			url:'TestRunServlet',
			data:'param=tc_pagination_search&txtrunid=' + runId + '&txtfunc=' + txtfunction + '&txtstdby=' + startedby + '&txtresult=' + result + '&txtcase=' + tcase + '&txtstep=' + tstep + '&txtapp=' + application + '&txtstdon=' + startedon  + '&txtpid=' + projectid,

			async:false,

			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					window.parent.closeModal();

				}else{
					showMessage(data.message,'error');
				}
			},
		});*/
		
		window.location.href='TestRunServlet?param=tc_pagination_search&txtrunid=' + runId + '&txtfunc=' + txtfunction + '&txtstdby=' + startedby + '&txtresult=' + result + '&txtcase=' + tcase + '&txtstep=' + tstep + '&txtapp=' + application + '&txtstdon=' + startedon  + '&txtpid=' + projectid;

		/*Changed by Gangadhar Badagi for TENJINCG-315 ends*/



	}); 







	/* $('#btnReset').click(function(){
	 var nul="";
	   $('.stdTextBox').val(nul);
	    }); */
    /*Added by Padmavathi for T25IT-246 starts*/
	$('#txtRunId').keyup(function (){
		var inputVal = $(this).val();
		var regex = new RegExp(/[^0-9]/g);
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			showMessage('Please enter only positive number','error');
			$(this).val('');
		}
	});
	/*Added by Padmavathi for T25IT-246 ends */
	$("#txtRunId").on('change keypress  click keyup paste input',function(e){

		var run=	$("#txtRunId").val().trim();
		if(run!="undefined" && run!=0 || run!='')
		{
			var t= run.length;
		}
		else
		{
			t=0;
			
		}


		if(t >0)
		{
			$(".stdTextBox").prop('disabled', true);
		}
		else if(t==0)
		{
			$(".stdTextBox").prop('disabled', false);
			/*Added by Preeti for TENJINCG-558 starts*/
			$("#txtprojectid").prop('disabled', true);
			/*Added by Preeti for TENJINCG-558 ends*/
		}
		if (e.which==8){

			if(t >0)
			{
				$(".stdTextBox").prop('disabled', true);
			}
			else if(t==0)
			{
				$(".stdTextBox").prop('disabled', false);
				/*Added by Preeti for TENJINCG-558 starts*/
				$("#txtprojectid").prop('disabled', true);
				/*Added by Preeti for TENJINCG-558 ends*/
			}

		}

	});

	/*Changed by Gangadhar Badagi for TENJINCG-315 starts*/
	$('#btnMyRuns').click(function(){
		
		var projectId=$('#projectId').val();
		var userId=$('#userId').val();
		$.ajax({
			url:'ProjectRunServlet',
			data:'param=PROJECT_RUNS&pid='+projectId+ '&uid='+userId,
			/*success:function(data){
				var status = data.STATUS;
				if(status == 'SUCCESS'){
					window.location.reload();

				}else{
					showMessage(data.message,'error');
				}
			},*/
		});
		
		/*window.location.reload();*/
		window.location.href='ProjectRunServlet?param=PROJECT_RUNS&pid='+projectId+'&uid='+userId
		
	});
$('#executingRuns').click(function(){
		
		var projectId=$('#projectId').val();
		/*var userId=$('#userId').val();
		$.ajax({
			url:'ProjectRunServlet',
			data:'param=PROJECT_RUNS&pid='+projectId,
		});*/
		
		/*window.location.reload();*/
		window.location.href='ProjectRunServlet?param=PROJECT_RUNS&pid='+projectId+'&status=Executing'
		
		
	});

/*Changed by Gangadhar Badagi for TENJINCG-315 ends*/

/*Added by Preeti for TENJINCG-372 starts*/
$('#txtapplication').change(function(){
	var app=$(this).val();
	if(app!='')
		{
			populateFunctionInfo(app);
		
		}
	else
		$('#txtfunction').html('');
	
});

/*Added by Preeti for TENJINCG-372 ends*/


});  




/*   changed by latief for requirement# TJN_17  ENDS*/