/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  maptmattributes.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
*
* 25-Jan-2017			Manish					TENJINCG-57,TENJINCG-58
*/



$(document).ready(function(){
	var message = $('#message').val();
	if(message!='null' && message!=""){
		showMessage(message,'error');
	}
	$('#btnBack').click(function(){
		var etmprjtool = $('#etmprjtool').val();
		window.location.href='TestManagerServlet?param=TmInstances&etmprjtool='+etmprjtool;
	});
	
	$('#btnSave').click(function(){
		var jArray = [];
		var projectId = $('#projectId').val();
		var pass = false;
		$('.tjn_field').each(function(){
			var json = new Object();
			json.tmField=$(this).val();
			json.tjnField=$(this).attr('tjnField');
			json.rec_type=$(this).attr('rec_type');
			jArray.push(json);
			
			
		});
		if($('#tcIdField').val()!='-1' && $('#tcTypeField').val()!='-1' && $('#tsAppIdField').val()!='-1' && $('#tsFuncCodeField').val()!='-1' && $('#tsOperationField').val()!='-1' && $('#tsAutLoginTypeField').val()!='-1' && $('#tsTypeField').val()!='-1'){
			pass=true;
		}
		var jstring = JSON.stringify(jArray);
		if(pass){
			$.ajax({
				url:'TestCaseServlet',
				data:'param=map_tm_attributes&jstring='+jstring+'&projectId='+projectId,
				async:true,
				dataType:'json',
				success:function(data){
					var status = data.status;
					if(status == 'SUCCESS')
					{
						showMessage(data.message,'success');
					}
					else
					{
						showMessage(data.message,'error');
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(errorThrown,'error');
				},
			});
		}else{
			showMessage('All fields are mandatory, Please select values.');
		}
		

	});
});