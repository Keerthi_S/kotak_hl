/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ui_val_list.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 24-Jul-2017			Sriram					Newly added for TENJINCG-285
* 28-Jul-2017			Pushpalatha				Req#TENJINCG-316
* 28-08-2017           Padmavathi               defect T25IT-326
*/

var tsRecId;

$(document).ready(function() {
	
	/*Added by Padmavathi for TENJINCG-613 starts*/
	tsRecId = $('#tstepRecId').val();
	/*$('#ui_val_Table').paginatedTable();*/
	/*Added by Padmavathi for TENJINCG-613 ends*/
	/*commented by Padmavathi for TENJINCG-613 starts*/
	/*Added by Pushpalatha for requirement TENJINCG-316 starts
	$('#btnDelete').click(function(){
		Added By padmavathi for T25IT-326 starts
		clearMessagesOnElement($('#user-message'));
		Added By padmavathi for T25IT-326 ends
		var selectedTests = '';
		var flag=true;
		var count=0;
		
		$('#tblValSteps').find('input[type="checkbox"]').each(function () {
			if(this.id != 'chk_all'){
				count=count+1;
			}
		});
		
		$('#tblValSteps').find('input[type="checkbox"]:checked').each(function () {
		       
			if(this.id=='chk_all'){
				flag=false;
			}
			if(this.id != 'chk_all'){
				selectedTests = selectedTests + this.id +',';
			}
		});
		
		if( selectedTests.charAt(selectedTests.length-1) == ','){
			selectedTests = selectedTests.slice(0,-1);
		}
		
		var data = '';
		if(!flag && count<1){
			showMessage("No validation steps available to  delete", "error");
			return false;
		}
		if(flag && count<1){
			showMessage("No validation steps available to  delete", "error");
			return false;
		}
		
		if(selectedTests.length < 1){
			showMessage("select atleast one step to delete", "error");
			return false;
		
		}else{
			if (window.confirm('Are you sure you want to delete selected validation step(s)?')){
				data='param=delete_selectedValidationSteps&valSteplist=' + selectedTests
				
			}else{
				return false;
			}

		}
		
		 $.ajax({
		     url:'UIValidationServlet',
		     data:data,
		     async:true,
		     dataType:'json',
		      success:function(data){
			         var status = data.status;
			            if(status == 'SUCCESS')
			            {
			            	setTimeout(function(){
							    location = ''
							  },800)
			            
							showMessage("Deleted successfully",'success');
							
			             }
			            else
			             {
				            showMessage(data.message,'error');
			             }
			           
		            },
		       error:function(xhr, textStatus, errorThrown){
			           showMessage(errorThrown,'error');
			          
			          
		          },
		      });
    });
	*/
	/*Added by Pushpalatha for requirement TENJINCG-316 ends*/
	/*commented by Padmavathi for TENJINCG-613 ends*/
	
	
	/*Added by Padmavathi for TENJINCG-613 starts*/
	$(document).on("click", "#btnDelete", function() {
		clearMessagesOnElement($('#user-message'));
		if($('#ui_val_Table').find('input[type="checkbox"]:checked').length < 1) {
			showLocalizedMessage('no.records.selected', 'error');
			return false;
		}else if($('#ui_val_Table').find('input[type="checkbox"]:checked').length < 2 && $('#ui_val_Table .tbl-select-all-rows').is(':checked')) {
			showLocalizedMessage('no.records.selected', 'error');
			return false;
		}
	if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
		var $uiValForm = $("<form action='UIValidationServlet' method='POST' />");
		var csrftoken_form = $("#csrftoken_form").val();
		$uiValForm.append($("<input type='hidden' id='del' name='del' value='true' />"))
		$uiValForm.append($("<input type='hidden' id='stepRecId' name='stepRecId' value='"+$('#tstepRecId').val()+"'/>"))
		$uiValForm.append($("<input type='hidden' id='csrftoken_form' name='csrftoken_form' value='"+csrftoken_form+"' />"))
		$('#ui_val_Table').find('input[type="checkbox"]:checked').each(function() {
			if(!$(this).hasClass('tbl-select-all-rows')) {
				$uiValForm.append($("<input type='hidden' name='selectedUiValCodes' value='"+ $(this).data('uiValRec') +"' />"));
			}
		});
		$('body').append($uiValForm);
		$uiValForm.submit();
	}
});
	/*Added by Padmavathi for TENJINCG-613 ends*/
	
});

$(document).on('click','#btnBack', function() {
	window.location.href='TestStepServlet?t=view&key=' + tsRecId;
});

$(document).on('click','#btnNew', function() {
	window.location.href='UIValidationServlet?param=new_ui_val&srecid=' + tsRecId;
});