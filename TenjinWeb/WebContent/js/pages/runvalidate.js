/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  runvalidate.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 11-Aug-2017			Roshni					T25IT-120
* 30-Aug-2017			Roshni					T25IT-345
* 09-04-2018			Preeti					TENJINCG-618
* 23-04-2018			Pushpalatha				TENJINCG-637
* 24-04-2018			Padmavathi				TENJINCG-653
* 19-11-2018			Prem					TENJINCG-843
* 24-06-2019            Padmavathi              for license 
*/
$(document).ready(function(){
	var loadingHtml = "<img src='images/ajax-loader.gif' alt='In Progress'/>";
	var successHtml = "<img src='images/success_20c20.png' alt='Validation Ok'/>";
	var failHtml = "<img src='images/failure_20c20.png' alt='Validation Failed'/>";
	var errorHtml = "<img src='images/error_20c20.png' alt='Validation Error'/>";
	/*Added by Roshni for T25IT-120 starts*/
	$('#btnBack').hide();
	$('#failedTest').hide();
	/*Added by Roshni for T25IT-120 ends*/
	$('#tset_val').html(loadingHtml);
	/*Added by Roshni for T25IT-345 starts*/
	var set_mode=$('#set_mode').val();
	/*Added by Roshni for T25IT-345 ends*/
	$.ajax({
		url:'ExecutorServlet',
		data:'param=test_set_val',
		dataType:'json',
		async:true,
		success:function(data){
			var status = data.status;
			if(status == 'success'){
				//showMessage('Execution Completed','success');
				$('#tset_val').html(successHtml);
				$('#test_data_load').html(loadingHtml);
				$.ajax({
					url:'ExecutorServlet',
					data:'param=load_test_data',
					dataType:'json',
					async:true,
					success:function(data){
						var status = data.status;
						if(status == 'success'){
							//showMessage('Execution Completed','success');
							$('#test_data_load').html(successHtml);
							$('#test_data_val').html(loadingHtml);
							$.ajax({
								url:'ExecutorServlet',
								data:'param=test_data_val',
								dataType:'json',
								async:true,
								success:function(data){
									var status = data.status;
									if(status == 'success'){
										//showMessage('Execution Completed','success');
										$('#test_data_val').html(successHtml);
										$('#aut_cred_val').html(loadingHtml);
										$.ajax({
											url:'ExecutorServlet',
											data:'param=user_aut_val',
											dataType:'json',
											async:true,
											success:function(data){
												var status = data.status;
												if(status == 'success'){
													$('#aut_cred_val').html(successHtml);
													$('#oauth_val').html(loadingHtml);
													$.ajax({
														url:'ExecutorServlet',
														data:'param=oauth_val',
														dataType:'json',
														async:true,
														success:function(data){
															var status = data.status;
															if(status == 'success' || status == 'continue'){
																if(status == 'success')
																	$('#oauth_val').html(successHtml);
																/* Uncommented by Padmavathi for license starts */
																$('#adapter_license_val').html(loadingHtml);
																/*Added by Prem for TENJINCG-843 */
																$.ajax({
																	url:'ExecutorServlet',
																	data:'param=adapter_license',
																	dataType:'json',
																	async:true,
																	success:function(data){
																		var status = data.status;
																		if(status == 'success'){
																			//showMessage('Execution Completed','success');
																			$('#adapter_license_val').html(successHtml);
																			$('#create_run_val').html(loadingHtml);
																			$.ajax({
																				url:'ExecutorServlet',
																				data:'param=create_run',
																				dataType:'json',
																				async:true,
																				success:function(data){
																					var status = data.status;
																					if(status == 'success'){
																						//showMessage('Execution Completed','success');
																						$('#create_run_val').html(successHtml);
																						window.location.href='ExecutorServlet?param=start_run';
																					}else{
																					/*Added by Roshni for T25IT-120 starts*/
																						$('#intializing').hide();
																						$('#btnBack').show();
																						$('#failedTest').show();
																					
																						/*Added by Roshni for T25IT-120 ends*/
																						/*Added by Roshni for T25IT-345 starts*/
																							$('#validator_title').hide();
																						/*Added by Roshni for T25IT-345 ends*/
																						showMessage(data.message,'error');
																						/*var html = "";
																						var failures = data.failures;
																						if(failures.length > 0){
																							html = "<table class='bordered'>";
																							html = html + "<thead><tr><th>Application</th><th>Problem Description</th></tr></thead><tbody>";
																							$.each(failures, function(i, item) {
																							    html = html + "<tr>";
																							    html = html + "<td>" + item.aut + "</td>";
																							    html = html + "<td>" + item.message + "</td>";
																							    html = html + "</tr>";
																							});
																							html = html + "</tbody>";
																							html = html + "</table>";
																							$('#validator_errors').html(html);
																							$('#validator_result').show();
																							$.each(failures, function(i, item) {
																							    alert(item);
																							});
																						}else{
																							showMessage(data.message,'error');
																						}*/
																						$('#create_run_val').html(failHtml);
																					/* Uncommented by Padmavathi for license ends */
																					}
																				},
																				error:function(xhr, textStatus, errorThrown){
																					showMessage('A network error occurred','error');
																					/*Added by Roshni for T25IT-120 starts*/
																					$('#intializing').hide();
																					$('#btnBack').show();
																					$('#failedTest').show();
																					/*Added by Roshni for T25IT-120 ends*/
																					/*Added by Roshni for T25IT-345 starts*/
																					$('#validator_title').hide();
																					/*Added by Roshni for T25IT-345 ends*/
																					$('#create_run_val').html(errorHtml);
																				}
																			});
																		
																		}else{
																		/*Added by Roshni for T25IT-120 starts*/
																			$('#intializing').hide();
																			$('#btnBack').show();
																			$('#failedTest').show();
																			/*Added by Roshni for T25IT-120 ends*/
																			/*Added by Roshni for T25IT-345 starts*/
																			$('#validator_title').hide();
																			/*Added by Roshni for T25IT-345 ends*/
																			var html = "";
																			var failures = data.failures;
																			if(failures.length > 0){
																				html = "<table class='bordered'>";
																				html = html + "<thead><tr><th>Application</th><th>Problem Description</th></tr></thead><tbody>";
																				$.each(failures, function(i, item) {
																				    html = html + "<tr>";
																				    html = html + "<td>" + item.aut + "</td>";
																				    html = html + "<td>" + item.message + "</td>";
																				    html = html + "</tr>";
																				});
																				html = html + "</tbody>";
																				html = html + "</table>";
																				$('#validator_errors').html(html);
																				$('#validator_result').show();
																				/*$.each(failures, function(i, item) {
																				    alert(item);
																				});*/
																			}else{
																			/*Added by Roshni for T25IT-120 starts*/
																				$('#intializing').hide();
																				$('#btnBack').show();
																				$('#failedTest').show();
																				/*Added by Roshni for T25IT-120 ends*/
																				/*Added by Roshni for T25IT-345 starts*/
																				/* Uncommented by Padmavathi for license starts */
																				$('#validator_title').hide();
																				/* Uncommented by Padmavathi for license ends */
																				/*Added by Roshni for T25IT-345 ends*/
																				
																				showMessage(data.message,'error');
																			}
																			$('#adapter_license_val').html(failHtml);
																		}
																	},
																	error:function(xhr, textStatus, errorThrown){
																	/*Added by Roshni for T25IT-120 starts*/
																		$('#intializing').hide();
																		$('#btnBack').show();
																		$('#failedTest').show();
																		/*Added by Roshni for T25IT-120 ends*/
																		/*Added by Roshni for T25IT-345 starts*/
																		$('#validator_title').hide();
																		/*Added by Roshni for T25IT-345 ends*/
																		showMessage('A network error occurred','error');
																		$('#adapter_license_val').html(errorHtml);
																	}
																});
															}else{
																$('#intializing').hide();
																$('#btnBack').show();
																$('#failedTest').show();
																$('#validator_title').hide();
																var html = "";
																var failures = data.failures;
																if(failures.length > 0){
																	html = "<table class='bordered'>";
																	html = html + "<thead><tr><th>Application</th><th>User Type</th><th>Problem Description</th></tr></thead><tbody>";
																	$.each(failures, function(i, item) {
																	    html = html + "<tr>";
																	    html = html + "<td>" + item.appName + "</td>";
																	    html = html + "<td>" + item.userType + "</td>";
																	    html = html + "<td>" + item.desc + "</td>";
																	    html = html + "</tr>";
																	});
																	html = html + "</tbody>";
																	html = html + "</table>";
																	$('#validator_errors').html(html);
																	$('#validator_result').show();
																}
																showMessage(data.message,'error');
																$('#oauth_val').html(failHtml);
															}
														},
													error:function(xhr, textStatus, errorThrown){
															$('#intializing').hide();
															$('#btnBack').show();
															$('#failedTest').show();
															$('#validator_title').hide();
															showMessage('A network error occurred','error');
															$('#oauth_val').html(errorHtml);
															/*Changes by Sunitha for TENJINCG-1018 ends*/
														}
													});
												}else{
												/*Added by Roshni for T25IT-120 starts*/
													$('#intializing').hide();
													$('#btnBack').show();
													$('#failedTest').show();
													/*Added by Roshni for T25IT-120 ends*/
													/*Added by Roshni for T25IT-345 starts*/
													$('#validator_title').hide();
													/*Added by Roshni for T25IT-345 ends*/
													var html = "";
													var failures = data.failures;
													if(failures.length > 0){
														html = "<table class='bordered'>";
														html = html + "<thead><tr><th>Application</th><th>Problem Description</th></tr></thead><tbody>";
														$.each(failures, function(i, item) {
														    html = html + "<tr>";
														    html = html + "<td>" + item.aut + "</td>";
														    html = html + "<td>" + item.message + "</td>";
														    html = html + "</tr>";
														});
														html = html + "</tbody>";
														html = html + "</table>";
														$('#validator_errors').html(html);
														$('#validator_result').show();
														/*$.each(failures, function(i, item) {
														    alert(item);
														});*/
													}else{
													/*Added by Roshni for T25IT-120 starts*/
														$('#intializing').hide();
														$('#btnBack').show();
														$('#failedTest').show();
														/*Added by Roshni for T25IT-120 ends*/
														/*Added by Roshni for T25IT-345 starts*/
														$('#validator_title').hide();
														/*Added by Roshni for T25IT-345 ends*/
														
														showMessage(data.message,'error');
													}
													$('#aut_cred_val').html(failHtml);
												}
											},
											error:function(xhr, textStatus, errorThrown){
											/*Added by Roshni for T25IT-120 starts*/
												$('#intializing').hide();
												$('#btnBack').show();
												$('#failedTest').show();
												/*Added by Roshni for T25IT-120 ends*/
												/*Added by Roshni for T25IT-345 starts*/
												$('#validator_title').hide();
												/*Added by Roshni for T25IT-345 ends*/
												showMessage('A network error occurred','error');
												$('#aut_cred_val').html(errorHtml);
											}
										});
									}else{
										//showMessage(data.message,'error');
										/*Added by Roshni for T25IT-120 starts*/
										$('#intializing').hide();
										$('#btnBack').show();
										$('#failedTest').show();
										/*Added by Roshni for T25IT-120 ends*/
										/*Added by Roshni for T25IT-345 starts*/
										$('#validator_title').hide();
										/*Added by Roshni for T25IT-345 ends*/
										var html = "";
										var failures = data.failures;
										if(failures.length > 0){
											html = "<table class='bordered'>";
											html = html + "<thead><tr><th>Test Case ID</th><th>Step ID</th><th>Problem Description</th></tr></thead><tbody>";
											$.each(failures, function(i, item) {
											    html = html + "<tr>";
											    html = html + "<td>" + item.tc + "</td>";
											    html = html + "<td>" + item.step + "</td>";
											    html = html + "<td>" + item.message + "</td>";
											    html = html + "</tr>";
											});
											html = html + "</tbody>";
											html = html + "</table>";
											$('#validator_errors').html(html);
											$('#validator_result').show();
											/*$.each(failures, function(i, item) {
											    alert(item);
											});*/
										}else{
										/*Added by Roshni for T25IT-120 starts*/
											$('#intializing').hide();
											$('#btnBack').show();
											$('#failedTest').show();
											/*Added by Roshni for T25IT-120 ends*/
											/*Added by Roshni for T25IT-345 starts*/
											$('#validator_title').hide();
											/*Added by Roshni for T25IT-345 ends*/
											showMessage(data.message,'error');
										}
										$('#test_data_val').html(failHtml);
									}
								},
								error:function(xhr, textStatus, errorThrown){
								/*Added by Roshni for T25IT-120 starts*/
									$('#intializing').hide();
									$('#btnBack').show();
									$('#failedTest').show();
								/*Added by Roshni for T25IT-120 ends*/
									/*Added by Roshni for T25IT-345 starts*/
									$('#validator_title').hide();
									/*Added by Roshni for T25IT-345 ends*/
									showMessage('A network error occurred','error');
									$('#test_data_val').html(errorHtml);
								}
							});
						}else{
						/*Added by Roshni for T25IT-120 starts*/
							$('#intializing').hide();
							$('#btnBack').show();
							$('#failedTest').show();
						/*Added by Roshni for T25IT-120 ends*/
							/*Added by Roshni for T25IT-345 starts*/
							$('#validator_title').hide();
							/*Added by Roshni for T25IT-345 ends*/
							var html = "";
							var failures = data.failures;
							if(failures.length > 0){
								html = "<table class='bordered'>";
								if(set_mode=='API')
									html = html + "<thead><tr><th>Application</th><th>API Code</th><th>Problem Description</th></tr></thead><tbody>";
								else
									html = html + "<thead><tr><th>Application</th><th>Function Code</th><th>Problem Description</th></tr></thead><tbody>";
								$.each(failures, function(i, item) {
								    html = html + "<tr>";
								    html = html + "<td>" + item.app + "</td>";
								    html = html + "<td>" + item.func + "</td>";
								    html = html + "<td>" + item.message + "</td>";
								    html = html + "</tr>";
								});
								html = html + "</tbody>";
								html = html + "</table>";
								$('#validator_errors').html(html);
								$('#validator_result').show();
							}else{
							/*Added by Roshni for T25IT-120 starts*/
								$('#intializing').hide();
								$('#btnBack').show();
								$('#failedTest').show();
							/*Added by Roshni for T25IT-120 ends*/
								/*Added by Roshni for T25IT-345 starts*/
								$('#validator_title').hide();
								/*Added by Roshni for T25IT-345 ends*/
								showMessage(data.message,'error');
							}
							
							$('#test_data_load').html(failHtml);
						}
					},
					error:function(xhr, textStatus, errorThrown){
					/*Added by Roshni for T25IT-120 starts*/
						$('#intializing').hide();
						$('#btnBack').show();
						$('#failedTest').show();
					/*Added by Roshni for T25IT-120 ends*/
						/*Added by Roshni for T25IT-345 starts*/
						$('#validator_title').hide();
						/*Added by Roshni for T25IT-345 ends*/
						showMessage('A network error occurred','error');
						$('#test_data_load').html(errorHtml);
					}
				});
			}else{
			/*Added by Roshni for T25IT-120 starts*/
				$('#intializing').hide();
				$('#btnBack').show();
				$('#failedTest').show();
			/*Added by Roshni for T25IT-120 ends*/
				/*Added by Roshni for T25IT-345 starts*/
				$('#validator_title').hide();
				/*Added by Roshni for T25IT-345 ends*/
				showMessage(data.message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
		/*Added by Roshni for T25IT-120 starts*/
			$('#intializing').hide();
			$('#btnBack').show();
			$('#failedTest').show();
		/*Added by Roshni for T25IT-120 ends*/
			/*Added by Roshni for T25IT-345 starts*/
			$('#validator_title').hide();
			/*Added by Roshni for T25IT-345 ends*/
			showMessage('A network error occurred','error');
			$('#tset_val').html(errorHtml);
		}
	});
/*Added by Roshni for T25IT-120 starts*/
	$('#btnBack').click(function(){
	/*Added by Padmavathi for TENJINCG-653 starts*/
		var callback=$('#callback').val();
		var callbackParam=$('#callbackparam').val();
		if(callbackParam=='rerun'){
			window.location.href="ResultsServlet?param=run_result&run="+$('#parentRunId').val()+'&callback='+callback;
		}else{
		/*Added by Padmavathi for TENJINCG-653 ends*/
		var entity_id=$('#entity_id').val();
			var entity_name=$('#entity_name').val();
			if(entity_name=='testcase')
				{
				/*Modified by Preeti for TENJINCG-618 starts*/
				/*window.location.href = 'TestCaseServlet?param=tc_view&paramval=' + entity_id;*/
				window.location.href = 'TestCaseServletNew?t=testCase_View&paramval=' + entity_id;
				/*Modified by Preeti for TENJINCG-618 ends*/
				}
			if(entity_name=='teststep')
				{
				/*Modified by Preeti for TENJINCG-618 starts*/
				/*window.location.href = 'TestCaseServlet?param=test_step_view&tstep=' + entity_id;*/
				window.location.href = 'TestStepServlet?t=view&key=' + entity_id;
				/*Modified by Preeti for TENJINCG-618 ends*/
				}
			if(entity_name=='testset')
				{
				/*Changed by Pushpalatha for TENJINCG-637 starts*/
				window.location.href='TestSetServletNew?t=testset_view&paramval='+ entity_id;
				/*Changed by Pushpalatha for TENJINCG-637 ends*/
				}
			}
	});
	/*Added by Roshni for T25IT-120 ends*/
	/*Added by Sunitha for TENJINCG-1018 starts*/
	if($('#oauthVal').val() == 'Y'){
		$('#validator_body').css('height', '300px');
	}
	/*Added by Sunitha for TENJINCG-1018 ends*/
});

