/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  reruninit.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */
/*Added by Roshni for TENJINCG-259( 3-Jul-2017 )*/
/*****************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              	DESCRIPTION
 * 16-01-2018			Preeti					TENJINCG-574
 * 24-04-2018          	Padmavathi              TENJINCG-653
 * 26-Nov 2018			Shivam	Sharma			TENJINCG-904
 */


$(document).ready(function() {
	/*Added by Preeti for TENJINCG-574 starts*/
	var runId = $('#parentRunId').val();
	var testSetRecordId = $('#txtTsRecId').val();
	/*Added by Preeti for TENJINCG-574 ends*/

	//added by shivam sharma for  TENJINCG-904 starts
	var clientName=$( "#txtclientregistered option:selected" ).text();
	$('#client').val(clientName);
	populateDevices(clientName);

	$('#txtclientregistered').change(function(){
		var client=$(this).val();
		var cilentName='';
		if(client=='999.999.999.999'){
			var text=$( "#txtclientregistered option:selected" ).text();
			$('#client').val(text);
			cilentName='Pcloudy';
		}
		else{
			var text=$( "#txtclientregistered option:selected" ).text();
			cilentName=text;
			$('#client').val(text);
		}
		populateDevices(cilentName);
	})
	//added by shivam sharma for  TENJINCG-904 ends

	$('#accordion').accordion({heightStyle:'content'});
	$('.tab-section').hide();
	$('#tabs a').bind('click', function(e){
		$('#tabs a.current').removeClass('current');
		$('.tab-section:visible').hide();
		$(this.hash).show();
		$(this).addClass('current');
		e.preventDefault();
	}).filter(':first').click();

	$('#test-steps-table').DataTable();

	var dOption = $('#defaultScreenshotOption').val();
	$('#Screen').children('option').each(function() {
		var val = $(this).val();
		if (val == dOption) {
			$(this).attr({
				'selected' : true
			});
		}
	});

	/***********************************************************
	 * Abort - Click
	 **********************************************************/
	$('#btnBack').click(function() {
		/*Modified by Preeti for TENJINCG-574 starts*/
		/*history.back(1);*/
		/*	Modified by Padmavathi for TENJINCG-653 starts*/
		var callback = $('#callback').val();
		/*window.location.href='PartialExecutorServlet?param=Re-RunTest&run=' + runId + '&ts=' + testSetRecordId;*/
		window.location.href='PartialExecutorServlet?param=Re-RunTest&run=' + runId + '&ts=' + testSetRecordId+'&callback='+callback;

		/*	Modified by Padmavathi for TENJINCG-653 ends*/
		/*Modified by Preeti for TENJINCG-574 ends*/
	});

	$('#btnCancel')
	.click(
			function() {
				window.location.href = 'TestSetServlet?param=ts_view&paramval='
					+ $('#txtTsRecId').val();
			});

});

function checkHost() {

	var cName = $('#txtclientregistered').val();
	var hostName = '';
	$('#txtclientregistered').children('option').each(function() {
		var val = $(this).val();
		if (cName == val) {
			hostName = $(this).attr('host');

		}
	});

	$.ajax({

		url : 'ClientServlet',
		data : {
			param : "SELECT_CLIENT_HOST",
			paramval : hostName
		},
		async : true,
		dataType : 'json',
		success : function(data) {
			var status = data.status;
			if (status == 'success') {
				$("#videoCaptureBlock").show();
			} else {
				$("#videoCaptureBlock").hide();
			}
		},

		error : function(xhr, textStatus, errorThrown) {
			showMessage(errorThrown, 'error');
		}
	});
}