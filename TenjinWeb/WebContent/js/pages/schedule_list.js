/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_list.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 01-12-2016		  	sahana				To view run information on all tab's screen
 * 13-Dec-2016			sahana				defect #TEN-52 (to change alert message)
 * 13-Dec-2016			sahana				defect #TEN-51 (Check box is not required when no operations are performed) 
 * 15-Dec-2016			sahana				defect #TEN-77 (The task created is for execution but when clicked on view moves to learning progress screen)
 * 16-Dec-2016			sahana				defect#TEN-74(A refresh button required in View Task status screen)
 * 16-Dec-2016			sahana				defect #TEN-67(Task gets aborted but shows Loading when clicked on View)
 * 17-Dec-2016        	sahana				defect #TEN-104(Tenjin should not allow scheduling if Aborted tasks in Bulk)
 * 10-Jan-2017		  	sahana				TENJINCG-4(User should be able to specify Date and Time when rescheduling Aborted Tasks)
 * 19-Jan-2017			Manish				defect TENJINCG-46
 * 01-Feb-2017			Sahana              TENJINCG-19(User should be able to abort / cancel a Recurrent Scheduled Task)
 * 18-08-2017           Leelprasad          T25IT-173
 * 31-08-2017           Manish		        back button functionality
 * 1-Sept-2017         	Roshni         		T25IT-415
 * 21-09-2017           Gangadhar Badagi	TENJINCG-348
 * 22-06-2018           Padmavathi          T251IT-112
 * 22-06-2018			Preeti				T251IT-113
 * 28-12-2018			Preeti				TJN262R2-83
 * 11-02-2018			Pushpa				TENJINCG-929
 * 10-06-2019			Ashiki				V2.8-111
 * 10-06-2019			Prem				v2.8.112
 * 12-08-2019			Prem				Tenjin-1089
 * 11-06-2020           Priyanka            v210Reg-26

 */
/*
 * Added file by sahana for Req#TJN_24_03 on 14/09/2016
 */


$(document).ready(function(){
	/*Added bt Prem for  V2.8-112 starts*/
	if (!String.prototype.startsWith) {
	    Object.defineProperty(String.prototype, 'startsWith', {
	        value: function(search, pos) {
	            pos = !pos || pos < 0 ? 0 : +pos;
	            return this.substring(pos, pos + search.length) === search;
	        }
	    });
	}
	/*Added bt Prem for  V2.8-112 Ends*/
	/*Added by Ashiki for V2.8-111 starts*/
	var message=$('#message').val();
	if(message!=""){
		showMessage(message,'success');
	}
	/*Added by Ashiki for V2.8-111 ends*/
	
	var selectedTabName=null;
	/*Added by Roshni for T25IT-415 starts*/
	var projState=$('#projState').val();
	/*Added by Roshni for T25IT-415 ends*/
	
	$('.main_content').show(); 
	$('.sub_content').hide();
	$('#userTable').dataTable();

	$('.tab-section').hide();
	$('#tabs a').bind('click', function(e){
		$('#tabs a.current').removeClass('current');
		$('.tab-section:visible').hide();
		$(this.hash).show();
		$(this).addClass('current');
		e.preventDefault();
	}).filter(':first').click();

	$("#tabs li a").click(function() {
		var scheduleType=$(this).text();
		var projectId=$('#projId').val();
		selectedTabName=scheduleType;
		var url='';
		var value='';
		/*Modified by Prem for v210Reg-29 starts*/
		/* if(projectId !="undefined"){
				url='FETCH_ALL_ADMIN_SCH';
			}*/
		 /*Modified by Prem for v210Reg-29 end*/
		/*Modified by Preeti for TJN262R2-83 starts*/
		/*if(typeof projectId!="undefined"){*/
		/* Modified By Priyanka for Tenj212-12 and Tenj212-11 starts*/
		if( projectId!=undefined && projectId !="null"){
			
			url='FETCH_ALL_SCHEDULES';
		}
		/*Modified by Preeti for TJN262R2-83 ends*/
			
		else{
			url='FETCH_ALL_ADMIN_SCH';
		}
		$.ajax({
			url:'SchedulerServlet',
			data :{param:url,paramval:scheduleType,id:projectId},
			async:false,
			dataType:'json',
			success: function(data){
				var status = data.STATUS;
				if(status == 'SUCCESS')
				{			
					var arrlist=data.SCHEDULES;
					var html="";
					html=html+"<div class='form container_16' style='margin:0 auto;'><div id='dataGrid'>";
					if(scheduleType=='All Tasks'){
						/*changes done by sahana for the defect #TEN-51  starts*/
						/*html=html+" <table id='userTable'  cellspacing='0' width='100%'><thead><tr><th class='tiny'><input type='checkbox'  id='chk_all' title='Select All'></th><th>Task Id</th><th>Scheduled Date</th><!-- <th>Time</th> --><th>Task</th><th>Client</th><th>Status</th><th>Created By</th><th>Run Information</th></tr></thead><tbody>";*/
						html=html+" <table id='userTable' cellspacing='0' width='100%'><thead><tr><th type='hidden' class='table-row-selector nosort sorting_disabled '></th><th>Task Name</th><th>Task Id</th><th>Scheduled Date</th><!-- <th>Time</th> --><th>Task</th><th>Client</th><th>Status</th><th>Recurrence</th><th>Created By</th><th>Run Information</th></tr></thead><tbody>";
						/*changes done by sahana for the defect #TEN-51  ends*/
					}
					/*changes done by sahana for the defect #TEN-67  starts*/
					else if(scheduleType=='Cancelled Tasks'){
						html=html+" <table id='userTable' cellspacing='0' width='100%'><thead><tr><th class='table-row-selector nosort' class='tiny'><input type='checkbox' id='chk_all' title='Select All'></th><th>Task Name</th><th>Task Id</th><th>Scheduled Date</th><!-- <th>Time</th> --><th>Task</th><th>Client</th><th>Recurrence</th><th>Created By</th></tr></thead><tbody>";
					}
					else if(scheduleType=='Aborted Tasks'){
						/*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
					/*html=html+" <table id='userTable' cellspacing='0' width='100%'><thead><tr><th class='tiny'><input type='checkbox' id='chk_all' title='Select All'></th><th>Task Name</th><th>Task Id</th><th>Scheduled Date</th><!-- <th>Time</th> --><th>Task</th><th>Client</th><th>Created By</th></tr></thead><tbody>";*/
						html=html+" <table id='userTable' cellspacing='0' width='100%'><thead><tr><th class='table-row-selector nosort' class='tiny'><input type='checkbox' id='chk_all' title='Select All'></th><th>Task Name</th><th>Task Id</th><th>Scheduled Date</th><!-- <th>Time</th> --><th>Task</th><th>Client</th><th>Recurrence</th><th>Created By</th><th>Reason</th></tr></thead><tbody>";
					/*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
					}
					else if(scheduleType=='Running Tasks'){
						html=html+" <table id='userTable' cellspacing='0' width='100%'><thead><tr><th type='hidden' class='table-row-selector nosort sorting_disabled '></th><th>Task Name</th><th>Task Id</th><th>Scheduled Date</th><!-- <th>Time</th> --><th>Task</th><th>Client</th><th>Recurrence</th><th>Created By</th><th>Run Information</th></tr></thead><tbody>";
					}
					/*changes done by sahana for the defect #TEN-67  ends*/
					else{
						html=html+" <table id='userTable' cellspacing='0' width='100%'><thead><tr><th class='table-row-selector nosort' class='tiny'><input type='checkbox' id='chk_all' title='Select All'></th><th>Task Name</th><th>Task Id</th><th>Scheduled Date</th><!-- <th>Time</th> --><th>Task</th><th>Client</th><th>Recurrence</th><th>Created By</th><th>Run Information</th></tr></thead><tbody>";
					}
					for(var i=0;i<arrlist.length;i++){
						var scheduler = arrlist[i];
						/*changed by sahana to view run information on all tab's screen:starts*/
						var viewLink='';
						if(scheduler.schAction=='Execute'){
							viewLink='ResultsServlet?param=run_result&run='+scheduler.schrunId+'&callback=scheduler';
						}
						else if(scheduler.schAction=='Extract'){
							viewLink='ExtractorServlet?param=extractor_progress&runid='+scheduler.schrunId;
						}
						
						/***************
						 * Added by sriram for TENJINCG-169 (Schedule API Learning)
						 */
						else if(scheduler.schAction =='LearnAPI'){
							viewLink='ApiLearnerServlet?param=learner_result&runid='+scheduler.schrunId;
						}
						/***************
						 * Added by sriram for TENJINCG-169 (Schedule API Learning) ends
						 */
						else{
							viewLink='LearnerServlet?param=learner_progress&runid='+scheduler.schrunId+'&callback=schedule';
						}
						

						if(scheduleType=='All Tasks'){
							/*changes done by sahana for the defect #TEN-67  starts*/
							if(scheduler.schStatus=='Aborted'){
							/*Changed by Leelaprasad for defect T25IT-173 starts*/
							    /*html=html+"<tr><td type='hidden'></td><td>"+scheduler.taskName+"</td><td>"+parseInt(parseInt(scheduler.schId))+"</td><td>&nbsp;&nbsp;"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+scheduler.schClient+"</td><td>"+scheduler.schStatus+"</td><td>"+scheduler.schCreatedBy+"</td><td>N/A</td></tr>";*/
								html=html+"<tr><td type='hidden'></td><td>"+escapeHtmlEntities(scheduler.taskName)+"</td><td>"+parseInt(parseInt(scheduler.schId))+"</td><td>"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+escapeHtmlEntities(scheduler.schClient)+"</td><td>"+scheduler.schStatus+"</td><td>"+scheduler.schRecur+"</td><td>"+escapeHtmlEntities(scheduler.schCreatedBy)+"</td><td>N/A</td></tr>";
							 /*Changed by Leelaprasad for defect T25IT-173 ends*/
							}
							else if(scheduler.schStatus=='Cancelled'){
							     /*Changed by Leelaprasad for defect T25IT-173 starts*/
							    /*html=html+"<tr><td type='hidden'></td><td>"+scheduler.taskName+"</td><td><a href='SchedulerServlet?param=VIEW_SCH&paramval="+parseInt(scheduler.schId)+'&tabType='+scheduler.schAction +"' id="+parseInt(scheduler.schId)+" class='edit_record'>"+parseInt(scheduler.schId)+"</a></td><td>&nbsp;&nbsp;"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+scheduler.schClient+"</td><td>"+scheduler.schStatus+"</td><td>"+scheduler.schCreatedBy+"</td><td>N/A</td></tr>";*/
								html=html+"<tr><td type='hidden'></td><td>"+escapeHtmlEntities(scheduler.taskName)+"</td><td><a href='SchedulerServlet?param=VIEW_SCH&paramval="+parseInt(scheduler.schId)+'&tabType='+scheduler.schAction +"' id="+parseInt(scheduler.schId)+" class='edit_record'>"+parseInt(scheduler.schId)+"</a></td><td>"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+escapeHtmlEntities(scheduler.schClient)+"</td><td>"+scheduler.schStatus+"</td><td>"+scheduler.schRecur+"</td><td>"+escapeHtmlEntities(scheduler.schCreatedBy)+"</td><td>N/A</td></tr>";
							    /*Changed by Leelaprasad for defect T25IT-173 ends*/
							}
							//	if(scheduler.schStatus=='Executing' || scheduler.schStatus=='Aborted'){
							else if(scheduler.schStatus=='Executing'){
								/*changes done by sahana for the defect #TEN-67  ends*/
								/*changes done by sahana for the defect #TEN-51  starts*/
								/*html=html+"<tr><td class='tiny'><input type='checkbox' class= 'checkbox' id='chk_sch' value='"+parseInt(scheduler.schId)+"'/></td><td>"+parseInt(scheduler.schId)+"</td><td>&nbsp;&nbsp;"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+scheduler.schClient+"</td><td>"+scheduler.schStatus+"</td><td>"+scheduler.schCreatedBy+"</td><td><a href='"+viewLink+"'>View</a></td></tr>";*/
								/*Changed by Leelaprasad for defect T25IT-173 starts*/
								/*html=html+"<tr><td type='hidden'></td><td>"+scheduler.taskName+"</td><td>"+parseInt(scheduler.schId)+"</td><td>&nbsp;&nbsp;"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+scheduler.schClient+"</td><td>"+scheduler.schStatus+"</td><td>"+scheduler.schCreatedBy+"</td><td><a href='"+viewLink+"'>View</a></td></tr>";*/
								html=html+"<tr><td type='hidden'></td><td>"+escapeHtmlEntities(scheduler.taskName)+"</td><td>"+parseInt(scheduler.schId)+"</td><td>"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+escapeHtmlEntities(scheduler.schClient)+"</td><td>"+scheduler.schStatus+"</td><td>"+escapeHtmlEntities(scheduler.schCreatedBy)+"</td><td><a href='"+viewLink+"'>View</a></td></tr>";
								/*Changed by Leelaprasad for defect T25IT-173 ends*/
								/*changes done by sahana for the defect #TEN-51  ends*/
							}
							/* Added By Priyanka for Tenj212-12 and Tenj212-11  ends*/
							else if(scheduler.schStatus=='Completed'){
								html=html+"<tr><td type='hidden'></td><td>"+escapeHtmlEntities(scheduler.taskName)+"</td><td><a href='SchedulerServlet?param=VIEW_SCH&paramval="+parseInt(scheduler.schId)+'&tabType='+scheduler.schAction +"' id="+parseInt(scheduler.schId)+" class='edit_record'>"+parseInt(scheduler.schId)+"</a></td><td>"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+escapeHtmlEntities(scheduler.schClient)+"</td><td>"+scheduler.schStatus+"</td><td>"+scheduler.schRecur+"</td><td>"+escapeHtmlEntities(scheduler.schCreatedBy)+"</td><td><a href='"+viewLink+"'>View</a></td></tr>";
							}
							/* Added By Priyanka for Tenj212-12 and Tenj212-11  ends*/
							
							/*Added by Pushpalatha for TV2.8R2-40 starts*/
							else if(scheduler.schStatus=='Scheduled'){
								html=html+"<tr><td type='hidden'></td><td>"+escapeHtmlEntities(scheduler.taskName)+"</td><td><a href='SchedulerServlet?param=VIEW_SCH&paramval="+parseInt(scheduler.schId)+'&tabType='+scheduler.schAction +"' id="+parseInt(scheduler.schId)+" class='edit_record'>"+parseInt(scheduler.schId)+"</a></td><td>"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+escapeHtmlEntities(scheduler.schClient)+"</td><td>"+scheduler.schStatus+"</td><td>"+scheduler.schRecur+"</td><td>"+escapeHtmlEntities(scheduler.schCreatedBy)+"</td><td>N/A</td></tr>";
							}
							/*Added by Pushpalatha for TV2.8R2-40 ends*/
							else
								/*changes done by sahana for the defect #TEN-51  starts*/
								/*Changed by Leelaprasad for defect T25IT-173 starts*/
								/*html=html+"<tr><td class='tiny'><input type='checkbox' class= 'checkbox' id='chk_sch' value='"+parseInt(scheduler.schId)+"'/></td><td><a href='SchedulerServlet?param=VIEW_SCH&paramval="+parseInt(scheduler.schId)+'&tabType='+scheduler.schAction +"' id="+parseInt(scheduler.schId)+" class='edit_record'>"+parseInt(scheduler.schId)+"</a></td><td>&nbsp;&nbsp;"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+scheduler.schClient+"</td><td>"+scheduler.schStatus+"</td><td>"+scheduler.schCreatedBy+"</td><td><a href='"+viewLink+"'>View</a></td></tr>";*/
								html=html+"<tr><td type='hidden'></td><td>"+escapeHtmlEntities(scheduler.taskName)+"</td><td><a href='SchedulerServlet?param=VIEW_SCH&paramval="+parseInt(scheduler.schId)+'&tabType='+scheduler.schAction +"' id="+parseInt(scheduler.schId)+" class='edit_record'>"+parseInt(scheduler.schId)+"</a></td><td>"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+escapeHtmlEntities(scheduler.schClient)+"</td><td>"+scheduler.schStatus+"</td><td>"+scheduler.schRecur+"</td><td>"+escapeHtmlEntities(scheduler.schCreatedBy)+"</td><td><a href='"+viewLink+"'>View</a></td></tr>";
							   /*Changed by Leelaprasad for defect T25IT-173 ends*/
							/*changes done by sahana for the defect #TEN-51  ends*/
						}
						else{
							/*changes done by sahana for the defect #TEN-67  starts*/
							if(scheduleType=='Aborted Tasks'){
							/*Changed by Gangadhar for the requirement story TENJINCG-348 starts*/
								var message=null;
								if(scheduler.message==null){
									message="Aborted";
								}
								else{
									message=scheduler.message;
								}
								/*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
							/*Changed by Leelaprasad for defect T25IT-173 starts*/
							/*html=html+"<tr><td class='tiny'><input type='checkbox' class= 'checkbox' id='chk_sch' value='"+parseInt(scheduler.schId)+"' action='"+scheduler.schAction+"' cycles='"+scheduler.schRecur+"''/></td><td>"+scheduler.taskName+"</td><td>"+parseInt(scheduler.schId)+"</td><td>&nbsp;&nbsp;"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+scheduler.schClient+"</td><td>"+scheduler.schCreatedBy+"</td></tr>"; */
								/*Changed by Gangadhar for the requirement story TENJINCG-348 starts*/
								/*html=html+"<tr><td class='tiny'><input type='checkbox' class= 'checkbox' id='chk_sch' value='"+parseInt(scheduler.schId)+"' action='"+scheduler.schAction+"' cycles='"+scheduler.schRecur+"''/></td><td>"+scheduler.taskName+"</td><td>"+parseInt(scheduler.schId)+"</td><td>"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+scheduler.schClient+"</td><td>"+scheduler.schCreatedBy+"</td></tr>"; */
								html=html+"<tr><td class='tiny'><input type='checkbox' class= 'checkbox' id='chk_sch' value='"+parseInt(scheduler.schId)+"' action='"+scheduler.schAction+"' cycles='"+scheduler.schRecur+"''/></td><td>"+escapeHtmlEntities(scheduler.taskName)+"</td><td>"+parseInt(scheduler.schId)+"</td><td>"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+escapeHtmlEntities(scheduler.schClient)+"</td><td>"+scheduler.schRecur+"</td><td>"+escapeHtmlEntities(scheduler.schCreatedBy)+"</td><td>"+message+"</td></tr>"; 
								/*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
							/*Changed by Leelaprasad for defect T25IT-173 ends*/
							}
							else if(scheduleType=='Cancelled Tasks'){
							/*Changed by Leelaprasad for defect T25IT-173 starts*/
							   /*html=html+"<tr><td class='tiny'><input type='checkbox' class= 'checkbox' id='chk_sch' value='"+parseInt(scheduler.schId)+"' action='"+scheduler.schAction+"' cycles='"+scheduler.schRecur+"''/></td><td>"+scheduler.taskName+"</td><td><a href='SchedulerServlet?param=VIEW_SCH&paramval="+parseInt(scheduler.schId)+'&tabType='+scheduler.schAction +"' id="+parseInt(scheduler.schId)+" class='edit_record'>"+parseInt(scheduler.schId)+"</a></td><td>&nbsp;&nbsp;"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+scheduler.schClient+"</td><td>"+scheduler.schCreatedBy+"</td></tr>"; */
								html=html+"<tr><td class='tiny'><input type='checkbox' class= 'checkbox' id='chk_sch' value='"+parseInt(scheduler.schId)+"' action='"+scheduler.schAction+"' cycles='"+scheduler.schRecur+"''/></td><td>"+escapeHtmlEntities(scheduler.taskName)+"</td><td><a href='SchedulerServlet?param=VIEW_SCH&paramval="+parseInt(scheduler.schId)+'&tabType='+scheduler.schAction +"' id="+parseInt(scheduler.schId)+" class='edit_record'>"+parseInt(scheduler.schId)+"</a></td><td>"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+escapeHtmlEntities(scheduler.schClient)+"</td><td>"+scheduler.schRecur+"</td><td>"+escapeHtmlEntities(scheduler.schCreatedBy)+"</td></tr>"; 
								/*Changed by Leelaprasad for defect T25IT-173 ends*/
								//html=html+"<tr><td type='hidden'></td><td><a href='SchedulerServlet?param=VIEW_SCH&paramval="+parseInt(scheduler.schId)+'&tabType='+scheduler.schAction +"' id="+parseInt(scheduler.schId)+" class='edit_record'>"+parseInt(scheduler.schId)+"</a></td><td>&nbsp;&nbsp;"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+scheduler.schClient+"</td><td>"+scheduler.schCreatedBy+"</td></tr>";
							}
							// if(scheduleType=='Aborted Tasks' || scheduleType=='Running Tasks'){
							else if(scheduleType=='Running Tasks'){
								/*changes done by sahana for the defect #TEN-67  ends*/
								/*Changed by Leelaprasad for defect T25IT-173 starts*/
                                /*html=html+"<tr><td type='hidden'></td><td>"+scheduler.taskName+"</td><td>"+parseInt(scheduler.schId)+"</td><td>&nbsp;&nbsp;"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+scheduler.schClient+"</td><td>"+scheduler.schCreatedBy+"</td><td><a href='"+viewLink+"'>View</a></td></tr>"; */
								html=html+"<tr><td type='hidden'></td><td>"+escapeHtmlEntities(scheduler.taskName)+"</td><td>"+parseInt(scheduler.schId)+"</td><td>"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+escapeHtmlEntities(scheduler.schClient)+"</td><td>"+scheduler.schRecurrence+"</td><td>"+escapeHtmlEntities(scheduler.schCreatedBy)+"</td><td><a href='"+viewLink+"'>View</a></td></tr>"; 
							/*Changed by Leelaprasad for defect T25IT-173 ends*/
							}
							/*Added by Pushpalatha for TV2.8R2-40 starts*/
							else if(scheduleType=='Scheduled Tasks'){
								html=html+"<tr><td class='tiny'><input type='checkbox' class= 'checkbox' id='chk_sch'  action='"+scheduler.schAction+"' cycles='"+scheduler.schRecur+"'  value='"+parseInt(scheduler.schId)+"'/></td><td>"+scheduler.taskName+"</td><td><a href='SchedulerServlet?param=VIEW_SCH&paramval="+parseInt(scheduler.schId)+'&tabType='+scheduler.schAction +"' id="+parseInt(scheduler.schId)+" class='edit_record'>"+parseInt(scheduler.schId)+"</a></td><td>"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+scheduler.schClient+"</td><td>"+scheduler.schRecur+"</td><td>"+scheduler.schCreatedBy+"</td><td>N/A</td></tr>";
							}
							/*Added by Pushpalatha for TV2.8R2-40 ends*/
							else{
								/*changed by sahana for improvement TENJINCG-19 : starts*/
								//html=html+"<tr><td class='tiny'><input type='checkbox' class= 'checkbox' id='chk_sch'    value='"+parseInt(scheduler.schId)+"'/></td><td>"+scheduler.taskName+"</td><td><a href='SchedulerServlet?param=VIEW_SCH&paramval="+parseInt(scheduler.schId)+'&tabType='+scheduler.schAction +"' id="+parseInt(scheduler.schId)+" class='edit_record'>"+parseInt(scheduler.schId)+"</a></td><td>&nbsp;&nbsp;"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+scheduler.schClient+"</td><td>"+scheduler.schCreatedBy+"</td><td><a href='"+viewLink+"'>View</a></td></tr>";
								/*Changed by Leelaprasad for defect T25IT-173 starts*/
								/*html=html+"<tr><td class='tiny'><input type='checkbox' class= 'checkbox' id='chk_sch'  action='"+scheduler.schAction+"' cycles='"+scheduler.schRecur+"'  value='"+parseInt(scheduler.schId)+"'/></td><td>"+scheduler.taskName+"</td><td><a href='SchedulerServlet?param=VIEW_SCH&paramval="+parseInt(scheduler.schId)+'&tabType='+scheduler.schAction +"' id="+parseInt(scheduler.schId)+" class='edit_record'>"+parseInt(scheduler.schId)+"</a></td><td>&nbsp;&nbsp;"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+scheduler.schClient+"</td><td>"+scheduler.schCreatedBy+"</td><td><a href='"+viewLink+"'>View</a></td></tr>";*/
								html=html+"<tr><td class='tiny'><input type='checkbox' class= 'checkbox' id='chk_sch'  action='"+scheduler.schAction+"' cycles='"+scheduler.schRecur+"'  value='"+parseInt(scheduler.schId)+"'/></td><td>"+escapeHtmlEntities(scheduler.taskName)+"</td><td><a href='SchedulerServlet?param=VIEW_SCH&paramval="+parseInt(scheduler.schId)+'&tabType='+scheduler.schAction +"' id="+parseInt(scheduler.schId)+" class='edit_record'>"+parseInt(scheduler.schId)+"</a></td><td>"+scheduler.schDate+"</td><td>"+scheduler.schAction+"</td><td>"+escapeHtmlEntities(scheduler.schClient)+"</td><td>"+scheduler.schRecur+"</td><td>"+escapeHtmlEntities(scheduler.schCreatedBy)+"</td><td><a href='"+viewLink+"'>View</a></td></tr>";
								/*Changed by Leelaprasad for defect T25IT-173 ends*/
								/*changed by sahana for improvement TENJINCG-19 : ends*/
							}
						}
						/*changed by sahana to view run information on all tab's screen:ends*/
					}
					if(arrlist.length==0){
						/*changed by manish for defect TENJINCG-46 on 19-Jan-2017 starts*/
						/*html=html+"<tr><td colspan='3'>No Records Found</td></tr>";*/
						/*changed by manish for defect TENJINCG-46 on 19-Jan-2017 ends*/
					}
					html=html+"</tbody></table>";
					var html1="<div class='toolbar'>";
					$('#scheduler-scheduled-tab').html('');

					if(scheduleType=='Scheduled Tasks'){
						/*Added by Roshni for T25IT-415 starts*/
						if(projState=='I'){
							html1=html1+"<div id='scheduledtasks'><input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh'/></div>";
						}
						/*Added by Roshni for T25IT-415 ends*/
						else{
						/*changed by sahana for improvement TENJINCG-19 : starts*/
						//html1=html1+"<div id='scheduledtasks'><input type='button' value='Reschedule' id='btnReschedule' class='imagebutton reSchedule'/><input type='button' value='Cancel Task' id='btnCancelSch' class='imagebutton cancel'/><input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh'/></div>";
						html1=html1+"<div id='scheduledtasks'><input type='button' value='Reschedule' id='btnReschedule' class='imagebutton reSchedule'/><input type='button' value='Cancel Task' id='btnCancelSch' class='imagebutton cancel'/><input type='button' value='Cancel Recurrence' id='btnCancelRec' class='imagebutton CancelRecurrence'/><input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh'/></div>";
						/*changed by sahana for improvement TENJINCG-19 : ends*/
						}
					}
					else if(scheduleType=='Completed Tasks'){
						/*Added by Roshni for T25IT-415 starts*/
						if(projState=='I'){
							html1=html1+"<div id='Completedtasks'><input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh'/></div>";
						}
						/*Added by Roshni for T25IT-415 ends*/
						else{
							html1=html1+"<div id='Completedtasks'><input type='button' value='Reschedule' id='btnRerun' class='imagebutton reSchedule'/><input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh'/></div>";
					
							}
						}
					else if(scheduleType=='Cancelled Tasks'){
					/*Added by Roshni for T25IT-415 starts*/
						if(projState=='I'){
							html1=html1+"<div id='Cancelledtasks'><input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh'/></div>";					
						}
						/*Added by Roshni for T25IT-415 ends*/
						else{
						/*Changed by pushpalatha for TENJINCG-929 starts*/
/*						html1=html1+"<div id='Cancelledtasks'><input type='button' value='Reschedule' id='btnSchedule' class='imagebutton schedule'/><input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh'/></div>";					
*/						html1=html1+"<div id='Cancelledtasks'><input type='button' value='Reschedule' id='btnSchedule' class='imagebutton schedule'/><input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh'/></div>";					

							/*Changed by pushpalatha for TENJINCG-929 ends*/
						}
					}
					else if(scheduleType=='Aborted Tasks'){
					/*Added by Roshni for T25IT-415 starts*/
						if(projState=='I'){
							html1=html1+" <div id='Abortedtasks'><input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh'/></div>";
						}
						/*Added by Roshni for T25IT-415 ends*/
						else{
						html1=html1+" <div id='Abortedtasks'><input type='button' value='Schedule Now' id='btnRetry' class='imagebutton schedule'/><input type='button' value='Schedule Later' id='btnRetry1' class='imagebutton schedule'/><input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh'/></div>";
						}
					}
					else{
						/*changes done by sahana for the defect #TEN-74:starts*/
						/*Changed by Prem for Tenjin-1089 start
						html1=html1+"<div id='AllTasks'><input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh'/></div>";
						/*Changed by Prem for Tenjin-1089 end*/
						/*changes done by sahana for the defect #TEN-74:ends*/
					}

					html1=html1+"</div></div></div>";
					$('#scheduler-scheduled-tab').append(html1);
					$('#scheduler-scheduled-tab').append(html);
					$('#userTable').dataTable();
				}
				else
				{
					showMessage(data.message, 'error');				
				}
			},

			error:function(xhr, textStatus, errorThrown){
				showMessage(data.message, 'error');			
			}
		});

		/* location.reload();*/


	});
	/*changes done by sahana for the defect #TEN-74:starts*/
	$('#scheduler-scheduled-tab').on('click','#btnRefresh', function(){
		var activeTab=$('.current').text();
		var index=0;
		if(activeTab.startsWith('All Tasks')){
			index=0;
		}
		else if(activeTab.startsWith('Aborted Tasks')){
			index=5;
		}
		else if(activeTab.startsWith('Running Tasks')){
			index=2;
		}
		else if(activeTab.startsWith('Completed Tasks')){
			index=3;
		}
		else if(activeTab.startsWith('Cancelled Tasks')){
			index=4;
		}
		else{
			index=1;
		}

		$("#tabs li a")[index].click();
	});
	/*changes done by sahana for the defect #TEN-74:ends*/
	$('#scheduler-scheduled-tab').on('click','#chk_all', function(e){

		if ($("#chk_all").is(':checked')) 
		{	
			$(this).closest('table').find('input[type=checkbox]').prop('checked', this.checked);	
			e.stopPropagation();
		}
		else
		{
			$(this).closest('table').find('input[type=checkbox]').prop('checked',false);	 
			e.stopPropagation();
		}
	}); 
	/*changed by sahana for improvement TENJINCG-19 : starts*/
	$('#scheduler-scheduled-tab').on('click','#btnCancelRec', function(){
		var projectId=$('#projId').val();
		var status="Cancelled";
		var val = [];
		var i=0;
		var type=null;
		var action=null;
		/*$(function(){*/
			$('#chk_sch:checked').each(function(){
				val[i] = $(this).val();
				type=$(this).attr('cycles');
				action=$(this).attr('action');
				i++;
			/*});*/
		});
		if(i==0 || type!='Y')
			alert("Please select a recurrence task to cancel ");
		else if(i>1)
			alert("Please select only one recurrence task to cancel ");
		else{
			$.ajax({
				url:'SchedulerServlet',
				data:{param:"UPDATE_TASK_STATUS",paramval:status,schIds:JSON.stringify(val),taskType:"rec"},
				async:false,
				dataType:'json',
				success:function(data){
					var status = data.status;
					if(status =='SUCCESS'){		
					/*Modified by Ashiki for V2.8-111 starts*/	
						/*showMessage(data.message,'success');*/
						var message=data.message;
						if(action=='Execute')
							/*window.setTimeout(function(){window.location='SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id='+projectId;},500);*/
							window.location='SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id='+projectId+'&message='+message;
						else
							/*window.setTimeout(function(){window.location='SchedulerServlet?param=FETCH_ALL_ADMIN_SCH&paramval=All';},500);*/
							window.location='SchedulerServlet?param=FETCH_ALL_ADMIN_SCH&paramval=All&message='+message;
					/*Modified by Ashiki for V2.8-111 ends*/
					}else{
						showMessage(data.message, 'error');				
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(data.message, 'error');			
				}
			});
		}
		$('input[type=checkbox]').attr('checked',false);
	});
	/*changed by sahana for improvement TENJINCG-19 : ends*/
	$('#scheduler-scheduled-tab').on('click','#btnCancelSch', function(){
		var projectId=$('#projId').val();
		var status="Cancelled";
		var val = [];
		var i=0;
		var action=null;
		/*Modified for Tenji210-73*/
		/*$(function(){*/
		/*Modified for Tenji210-73*/
			$('#chk_sch:checked').each(function(){
				val[i] = $(this).val();
				action=$(this).attr('action');
				i++;
				/*Modified for Tenji210-73*/
		/*	});*/
				/*Modified for Tenji210-73*/
		});
		if(i==0)
			/*changes done by sahana for the defect #TEN-52  starts*/
			/*alert("Please select a schedule"); */
			alert("Please select atleast one task");
		/*changes done by sahana for the defect #TEN-52  starts*/
		else{
			$.ajax({
				url:'SchedulerServlet',
				/*changed by sahana for improvement TENJINCG-19 : starts*/
				//data:{param:"UPDATE_TASK_STATUS",paramval:status,schIds:JSON.stringify(val)},
				data:{param:"UPDATE_TASK_STATUS",paramval:status,schIds:JSON.stringify(val),taskType:"notRec"},
				/*changed by sahana for improvement TENJINCG-19 : ends*/
				async:false,
				dataType:'json',
				success:function(data){
					var status = data.status;
					if(status =='SUCCESS'){		
						/*Modified by Ashiki for V2.8-111 starts*/
						/*showMessage(data.message,'success');*/	
						var message=data.message;
						if(action=='Execute')
							/*window.setTimeout(function(){window.location='SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id='+projectId;},500);*/
							window.location='SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id='+projectId+'&message='+message;
						else
							/*window.setTimeout(function(){window.location='SchedulerServlet?param=FETCH_ALL_ADMIN_SCH&paramval=All';},500);*/
							window.location='SchedulerServlet?param=FETCH_ALL_ADMIN_SCH&paramval=All&message='+message;
							/*Modified by Ashiki for V2.8-111 ends*/
					}else{
						showMessage(data.message, 'error');				
					}
				},

				error:function(xhr, textStatus, errorThrown){
					showMessage(data.message, 'error');			
				}
			});
		}
		$('input[type=checkbox]').attr('checked',false);
	});
	$('#scheduler-scheduled-tab').on('click', '#btnReschedule,#btnRerun,#btnSchedule,#btnRetry,#btnRetry1', function(){
		var val = [];
		var counter=0;
		var selectedTab=null;
		selectedTab=selectedTabName;
		var projectId=$('#projId').val();
		var i=0;
		var flag=false;
		var csrftoken_form=$("#csrftoken_form").val();
		$('#chk_sch:checked').each(function(){
			val[i] = $(this).val();
			i++;
		});
		if(selectedTab=='Aborted Tasks'){
			/*Modified by Preeti for T251IT-113 starts*/
			/*if(i==0 || i>1){*/
			if(i==0){
				alert("Please select atleast one task");
				flag=true;
			}
			if(i>1){
			/*Modified by Preeti for T251IT-113 ends*/
				/*changes done by sahana for the defect #TEN-52  starts*/
				/*alert("Please select a schedule"); */
				alert("Please select only one task");
				/*changes done by sahana for the defect #TEN-52  starts*/
				flag=true;
			} 
		}
		else{
			if(i==0){
				/*changes done by sahana for the defect #TEN-52  starts*/
				/* alert("Please select a schedule");*/
				alert("Please select atleast one task");
				/*changes done by sahana for the defect #TEN-52  starts*/
				flag=true;
			}
			if(i>1){
				/*changes done by sahana for the defect #TEN-52  starts*/
				/* alert("Please select only one schedule");*/
				alert("Please select only one task");
				/*changes done by sahana for the defect #TEN-52  starts*/
				flag=true;
			}
		}
		if(flag==false){
			if(selectedTab=='Aborted Tasks'){
				//$('.main_content').hide();
				var btnValue=$(this).attr("value");

				/* $( "#tabs li a" ).trigger( "click" );*/
				/*changes done by sahana for the defect #TEN-104  starts*/
				/*changes done by sahana for the TENJINCG-4  starts*/

				if(btnValue=='Schedule Now'){
					/*changed by manish for back button functionality on 31-Aug-2017 starts*/
					/*$('.ifr_Main').attr('src','SchedulerServlet?param=UPDATE_TASK_STATUS&paramval=Scheduled&schIds='+JSON.stringify(val));*/
					/*window.location.href='SchedulerServlet?param=UPDATE_TASK_STATUS&paramval=Scheduled&schIds='+JSON.stringify(val);*/
					/*Modified by Padmavathi for T251IT-112 starts*/
					/*$('.ifr_Main').attr('src','SchedulerServlet?param=UPDATE_TASK_STATUS&paramval=Scheduled&schIds='+JSON.stringify(val));*/
					$('.ifr_Main').attr('src','SchedulerServlet?param=UPDATE_TASK_STATUS&paramval=Scheduled&paramval1=ScheduleNow&schIds='+JSON.stringify(val)+'&csrftoken_form='+csrftoken_form);
					/*Modified by Padmavathi for T251IT-112 ends*/
					/*changed by manish for back button functionality on 31-Aug-2017 ends*/
					if(typeof projectId!="undefined")
						window.location.href='SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id='+projectId+'&csrftoken_form='+csrftoken_form;
					else
						window.location.href='SchedulerServlet?param=FETCH_ALL_ADMIN_SCH&paramval=All'+'&csrftoken_form='+csrftoken_form
				}
				else if(btnValue=='Schedule Later'){
					/*changed by manish for back button functionality on 31-Aug-2017 starts*/
					/*$('.main_content').hide();*/
					window.location.href='SchedulerServlet?param=VIEW_SCH&paramval='+val[0]+'&tabType='+selectedTab+'&csrftoken_form='+csrftoken_form;
					/*$('.ifr_Main').attr('src','SchedulerServlet?param=VIEW_SCH&paramval='+val[0]+'&tabType='+selectedTab);
					$('.sub_content').show();*/
					/*changed by manish for back button functionality on 31-Aug-2017 ends*/
				}
				/*changes done by sahana for the TENJINCG-4  ends*/
				/*changes done by sahana for the defect #TEN-104  ends*/
			}			
			else{
				/*changed by manish for back button functionality on 31-Aug-2017 starts*/
				/*$('.main_content').hide();
				$('.ifr_Main').attr('src','SchedulerServlet?param=VIEW_SCH&paramval='+val[0]+'&tabType='+selectedTab);
				$('.sub_content').show();*/
				window.location.href='SchedulerServlet?param=VIEW_SCH&paramval='+val[0]+'&tabType='+selectedTab+'&csrftoken_form='+csrftoken_form;
				/*changed by manish for back button functionality on 31-Aug-2017 ends*/
			}
			//}
		}
	});

	$('#btnDelete').click(function(){
		if (window.confirm('Are you sure you want to delete?'))
		{ 
			var count=0;
			var url;

			$("#chk_sch:checked").each(function () {
				count++;
			});


			if ($("#chk_all").is(':checked') || count>1) {

				var val = [];
				var i=0;
				$("#chk_sch:checked").each(function () {
					val[i] = $(this).val();
					i++;
				});
				var record = JSON.stringify(val);
				var schids = JSON.parse(record);
				url='param=deleteall&paramval='+schids;
			}

			else if ($("#chk_all").is(':checked') || count==1) {

				var checkedvalue=$('input:checkbox:checked').val();
				url='param=delete&paramval='+checkedvalue;
			}
			if (count==0) {
				/*showMessage("Please select the Schedule Id to Delete", 'error');*/	
				showMessage("Please select the Schedule Id to Task", 'error');
			}

			if(count==1 || count>1){

				$.ajax({
					url:'SchedulerServlet',
					data :url,
					async:false,
					dataType:'json',
					success: function(data){
						var status = data.status;
						if(status == 'SUCCESS')
						{			
							showMessage(data.message,'success');	
							window.setTimeout(function(){window.location='SchedulerServlet?param=FETCH_ALL_SCHEDULES';},500);
						}
						else
						{
							showMessage(data.message, 'error');				
						}
					},

					error:function(xhr, textStatus, errorThrown){
						showMessage(data.message, 'error');			
					}
				});

			}

		}
	});	

});





