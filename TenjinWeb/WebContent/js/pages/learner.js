/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  learner.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

$(document).ready(function(){
	
	window.setTimeout(function(){clearMessages();},5000);
	populateAuts();
	$('#tblNaviflowModules').dataTable();
	/***********************************
	 * Click download link for test data tempate
	 **********************************/
	$('#chk_all_modules').click(function(e){	
		   if ($("#chk_all_modules").is(':checked')) 
		   {	
		     $(this).closest('table').find('input[type=checkbox]').prop('checked', this.checked);	
		     e.stopPropagation();
		   }
		   else
		    {
			 $(this).closest('table').find('input[type=checkbox]').prop('checked',false);	 
			 e.stopPropagation();
		    }
		});  
	$('#btnDelete').click(function(){
		 var appid=$('#lstApplication').val();
		 var count=0;
		 /*16-Mar-2015 R2.1: Defect #1142: adding urllink variable: Starts*/
		  var urllink;
		  /*16-Mar-2015 R2.1: Defect #1142: adding urllink variable: Ends*/
		 $("input:checkbox[name=fcode]:checked").each(function () {
			count++;
		    });
		 /*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if no checkbox is selected: Starts*/
		 if (count==0) {
			 showMessage("Please select the Function Id to Delete", 'error');	
		 }
		//if ($("#chk_all_modules").is(':checked') || count>1) {
		 else if ($("#chk_all_modules").is(':checked') || count>1) {		
		 /*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if no checkbox is selected: Ends*/
			
			 var val = [];
			 var i=0;
			 $("input:checkbox[name=fcode]:checked").each(function () {
				 val[i] = $(this).val();
				 i++;
			    });
			 var record = JSON.stringify(val);
	         var funcids = JSON.parse(record);
	         urllink='param=deleteallfuncode&paramval='+ funcids+'&appid='+appid;
		   }
		 /*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if one checkbox is selected: Starts*/
			
		/*else
			{*/
		 else if ($("#chk_all_modules").is(':checked') || count==1) {
				/*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if one checkbox is selected: Ends*/
			var checkedvalue=$('input:checkbox:checked').val();
			urllink='param=deleterecord&paramval='+ checkedvalue+'&appid='+appid;
			}
		 /*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if one checkbox is selected: Starts*/
		 if(count==1 || count>1){
	    /*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if one checkbox is selected: Ends*/
				
		  $.ajax({
			url:'AutServlet',
			data :urllink,
			async:false,
			dataType:'json',
			success: function(data){
			var status = data.status;
			if(status == 'SUCCESS')
			  {			
				showMessage(data.message, 'success');	
		        window.setTimeout(function(){populateModules(appid);
				$('#modulesInfo').show();},500); 
			  }
			  else
			  {
				showMessage(data.message, 'error');				
			  }
		    },
		
		   error:function(xhr, textStatus, errorThrown){
			   showMessage(data.message, 'error');			
		  }
	     });
		 /*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if one checkbox is selected: Starts*/
			}
		    /*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if one checkbox is selected: Ends*/
	
	  });
	$(document).on('click','.template-gen',function(e){
		/*//alert($('#chk_all:checked').length > 0);
		//alert($('#chk_all').prop('checked'));
		var mod = $(this).attr('mod');
		var app = $(this).attr('app');
		var txnMode = $(this).attr('txnmode');	20-Oct-2014 WS
		var mode = $('#lstDownloadType').val();
		var $td = $(this).parent();
		$(this).hide();
		var $img = $("<img src='images/inprogress.gif' alt='Please Wait'/>");
		$td.append($img);
		
		$.ajax({
			url:'NaviflowServlet',
			20-Oct-2014 WS: Starts
			data:'param=DOWNLOAD_DATA_TEMPLATE&m=' + mod + '&a=' + app,
	data:'param=DOWNLOAD_DATA_TEMPLATE&m=' + mod + '&a=' + app + '&type=' + mode+ '&txnMode=' + txnMode,
			20-Oct-2014 WS: Ends
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
					$("body").append($c);
                    $("#downloadFile").get(0).click();
                    $c.remove();
				}else{
					var message = data.message;
					showMessage(message,'error');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown, 'error');
			}
		});
		
		$img.remove();
		$(this).show();*/
		
		var mod = $(this).attr('mod');
		var app = $(this).attr('app');
		var txnMode=$(this).attr('txnmode');
		$('#ttd-options-frame').attr('src','ttd_download_options.jsp?a=' + app + '&m=' +mod + '&t=' + txnMode);
    	$('.modalmask').show();
    	$('.subframe').show();
	});
	
	/*********************************
	 * New Button - Click
	 */
	
	$('#btnNew').click(function(){
		  /*
		 *date:10-11-14
		 * Code Change Starts here: Badal
		 *change code id: #762
		 * in Aut functions when click on new and without selecting any function name gives success message
		 */	
		var appidtext = $("#lstApplication option:selected").text();
		var appidval=$("#lstApplication option:selected").val();
		window.location.href='ModuleServlet?param=NEW_FUNC'+ '&appidtext=' + appidtext+'&appidval='+appidval;
		//window.location.href='ModuleServlet?param=NEW_FUNC';
	});
	
	/***********************************
	 * Change AUT in the drop down
	 **********************************/
	$('#lstApplications').change(function(){
		var selApp = $(this).val();
		if(selApp == '-1'){
			$('#tblNaviflowModules_body').html('');
			$('#modulesInfo').hide();
			$('#additionalInfo').hide();
			clearMessages();
		}else{
			populateModules(selApp);
			$('#modulesInfo').show();
			$('#additionalInfo').show();
			/* change code id: #745 Aut Functions on clicking on Delete button success message comes directly. By: Badal */
			$('#btnDelete').show();
			/* change code id: #762 Aut functions when click on new and without selecting any function name gives success message. By: Badal */
			$('#btnNew').show();
		}
	});
	
	/***********************************
	 * Learn Button - Click
	 ***********************************/
	$('#btnLearn').click(function(){
		$('#learnType').val('NORMAL');
		var selectedModules = '';
		$('#tblNaviflowModules').find('input[type="checkbox"]:checked').each(function () {
		       //this is the current checkbox
		       if(this.id != 'chk_all_modules'){
		    	   /*20-Oct-2014 WS: Starts*/
					//selectedModules = selectedModules + this.id + ';';
					var txnMode = 'G';
					//if ($(this).parents('tr').find('.learn-mode'))
						txnMode = $(this).parents('tr').find('.learn-mode:checked').val();
					var wsurl = $(this).parents('tr').find('.wsurl').val();
					var wsop = $(this).parents('tr').find('.wsop').val();

					if (this.id != "")
						selectedModules = selectedModules + this.id + '|' +txnMode  + '|' +wsurl + '|' +wsop +';';
/*20-Oct-2014 WS: Ends*/
		       }
		});
		//alert(selectedScripts.charAt(selectedScripts.length-1) == ';');
		if( selectedModules.charAt(selectedModules.length-1) == ';'){
			selectedModules = selectedModules.slice(0,-1);
		}
		if(selectedModules.length > 0){
			//alert(selectedScripts);
			$('#moduleList').val(selectedModules);
			//alert('#moduleList').val();
			
		}
		else{
			showMessage('Please choose atleast one script to continue','error');
			return false;
		}
		
		if($('#txtTarget').val() == ''){
			showMessage('Please specify Target Machine','error');
			return false;
		}
		
		if($('#lstBrowserType').val() == '-1'){
			showMessage('Please specify Browser Type','error');
			return false;
		}
	});
	
	/*20-Oct-2014 WS: Starts*/
	$(document).on('click','.learn-mode',function(){
		var $t = $(this);
		var app = $("#lstApplication").val();
		var mod = $(this).parent('td').next().html();
		var txnMode = $(this).val();
		$.ajax({
			url:'AutServlet',
			data:'param=ISLEARNINGCOMPLETE&mod=' + mod + '&app=' + app + '&txnMode=' + txnMode,
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.learnt;
				if(status == 'YES'){
					if ($t.parents('tr').children('td:eq(4)').children('a').length) {
						$t.parents('tr').children('td:eq(4)').children('a').attr({"href": "NaviflowServlet?param=xmlgen&func=" + mod + "&app=" + app + "&txnMode=" +$t.val()});
						$t.parents('tr').children('td:eq(5)').children('a').attr({txnMode: $t.val()});
					} else {
						$t.parents('tr').children('td:eq(4)').append($("<a />").attr({href: "NaviflowServlet?param=xmlgen&func=" + mod + "&app=" + app + "&txnMode=" +$t.val()}));
						$t.parents('tr').children('td:eq(5)').append($("<a />").attr({app: app, mod: mod, txnMode: $t.val()}).text("Download"));
					}
				}else{
					$t.parents('tr').children('td:eq(4)').html('Not Learnt');
					$t.parents('tr').children('td:eq(5)').html('Not Available');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown, 'error');
			}
		});
	});
	/*20-Oct-2014 WS: Ends*/
});


function populateAuts(){
	
	var jsonObj = fetchAllAuts();
	
	var status = jsonObj.status;
	 clearMessages();
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jArray = jsonObj.auts;
		
		for(i=0;i<jArray.length;i++){
			var aut = jArray[i];
			html = html + "<option value='" + aut.id + "'>" + aut.name + "</option>";
		}
		
		$('#lstApplication').html(html);
		
	}else{
		showMessage(jsonObj.message, 'error');
	}
}

function populateModules(app){
	//Not refreshes the pagination in learner Page . By:Nagababu,Badal.
	var $table = $('#tblNaviflowModules').DataTable();
	$table.destroy();
	var jsonObj = fetchModulesForLearning(app);
	var status = jsonObj.status;
	 clearMessages();
	if(status == 'SUCCESS'){
		
		/*var jArray = jsonObj.modules;
		var html = '';
		for(i=0;i<jArray.length;i++){
			var json = jArray[i];
			html = html + "<tr>";
			    
				 *date:06-11-14
				 * Code Change Starts here: Nagababu
				 *change code id: #166
				 * Given name to the function code to fetch the corresponding checkbox values
				 	
			html = html + "<td class='tiny'><input type='checkbox' name='fcode' value='"+json.code+"' id='" + json.code + "|" + json.menu + "|" + json.name + "'/></td>";
			20-Oct-2014 WS: Starts
			html = html + "<td><input type='radio' name='learn-mode" +i +"' value='G' checked class='tiny-radio learn-mode' />GUI <input type='radio' name='learn-mode" +i +"' value='W' class='tiny-radio learn-mode' />WS </td>";
			20-Oct-2014 WS: Ends
			html = html + "<td>" + json.code + "</td>";
			html = html + "<td><a href='ModuleServlet?param=VIEW_FUNC&paramval=" + json.code + "&a=" + app + "'>" + json.name + "</a></td>";
			var learnt = json.learnt;
			if(learnt == 'YES'){
				20-Oct-2014 WS: Starts
				//html = html + "<td><a href='NaviflowServlet?param=xmlgen&func=" + json.code + "&app=" + app + "' target='_blank'>Download</a>";
				//html = html + "<td><a href='NaviflowServlet?param=xmlgen&func=" + json.code + "&app=" + app + "&txnMode=G' target='_blank'>Download</a>";
				29-Oct-2014 adding Endtime: Starts
				//html = html + "<td><a href='NaviflowServlet?param=xmlgen&func=" + json.code + "&app=" + app + "&txnMode=G' target='_blank'>Download</a>";
				html = html + "<td><a href='NaviflowServlet?param=xmlgen&func=" + json.code + "&app=" + app + "&txnMode=G' target='_blank'>"+json.lastlearnt+"</a>";
				29-Oct-2014 adding Endtime: Ends
				20-Oct-2014 WS: Ends
				html = html + "<td><a href='#' class='template-gen' app='" + app + "' mod='" + json.code + "' txnMode=G '" +"'>Download</a>";
			}else{
				html = html + "<td>Not Learnt</td>";
				html = html + "<td>Not Available</td>";
			}
			20-Oct-2014 WS: Starts
			html = html + "<td class='hidden-column'><input type='text' class='wsurl' value='" + json.wsurl + "'/></td>";
			html = html + "<td class='hidden-column'><input type='text' class='wsop' value='" + json.wsop + "'/></td>";
			20-Oct-2014 WS: Ends
			html = html + "</tr>";
		}*/
		var html = jsonObj.modules;
		$('#tblNaviflowModules_body').html(html);
		$('#tblNaviflowModules').dataTable();
		
		populateAutCreds(app);
	}else{
		showMessage(jsonObj.message, 'error');
		$('#tblNaviflowModules_body').html('');
		$('#tblNaviflowModules').dataTable();
	}
	
	/********************************************
	 * Function added by Sriram to support Multiple logins for Learning (Tenjin v2.1)
	 */
	function populateAutCreds(app){
		var jsonObj = fetchAppCredentials(app);
		var status = jsonObj.status;
		clearMessages();
		if(status == 'SUCCESS'){
			var html ='';
			var uCreds = jsonObj.ucreds;
			if(uCreds.length > 0){
				html = html + "<optgroup label = 'Your Credentials'>";
				for(var i=0;i<uCreds.length;i++){
					var uCred = uCreds[i];
					html = html + "<option value='" + uCred.loginname + "'>" + uCred.loginname + " (" + uCred.loginType + ")" + "</option>";
				}
				html = html + "</optgroup>";
			}
			
			var oCreds = jsonObj.ocreds;
			if(oCreds.length > 0){
				html = html + "<optgroup label = 'All Credentials'>";
				for(var i=0;i<oCreds.length;i++){
					var oCred = oCreds[i];
					html = html + "<option value='" + oCred.loginname + "'>" + oCred.loginname + " (" + oCred.loginType + ")" + "</option>";
				}
				html = html + "</optgroup>";
			}
			
			$('#lstAutCred').html('');
			$('#lstAutCred').html(html);
		}else{
			showMessage(jsonObj.message,'error');
		}
		
	}
}