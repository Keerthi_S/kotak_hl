/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  messages_new.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 15-06-2021			Ashiki					TENJINCG-1275
*/

$(document).ready(function() {
			mandatoryFields('messageForm');
		$('#txtDate').hide();
		$('#dateFormat').keyup(function() {
		}).focus(function() {
			$('#txtDate').show();
		}).blur(function() {
			$('#txtDate').hide();
		});
		$('#messageTypeDivSwift').hide();
		$('#messageTypeDivIso20022').hide();
		$('#messageTypeDivIso8583').hide();
		});
		
		
		$(document).on('change', '#msgFormat', function() {
			var selectedOperation=$('#msgFormat').val();
			
			if ('ISO20022' == selectedOperation) {
				$('#messageTypeDivIso20022').show();
				$('#messageTypeDivIso8583').hide();
				$('#messageTypeDivSwift').hide();
				$('#messageTypeDivIso20022').val('-- Select One --');
			} else if ('ISO8583' == selectedOperation) {
				$('#messageTypeDivIso8583').show();
				$('#messageTypeDivIso20022').hide();
				$('#messageTypeDivSwift').hide();
				$('#messageTypeDivIso8583').val('-- Select One --');
			}else if ('SWIFT' == selectedOperation) {
				$('#messageTypeDivSwift').show();
				$('#messageTypeDivIso20022').hide();
				$('#messageTypeDivIso8583').hide();
				$('#messageTypeDivSwift').val('-- Select One --');
			}else if ('Other' == selectedOperation) {
				$('#messageTypeDivSwift').hide();
				$('#messageTypeDivIso20022').hide();
				$('#messageTypeDivIso8583').hide();
				$('#messageTypeDivSwift').val('-- Select One --');
			} else {
				$('#msgvalType').val().hidden = true;
			}
		});	
		
		/*$(document).on('click', '#btnBack', function() {
			var appId=0;
			var groupName="Ungrouped";
			window.location.href='MessageServlet?appId=' + appId + '&group=' + groupName;
		});*/
		
		
		/*$(document).on('change', '#existingGroup', function() {
			if($(this).val() === 'ALL' || $(this).val() === '-1' || $(this).val() === 'Ungrouped') {
				$('#newGroup').prop('disabled',false);
			}else{
				$('#newGroup').prop('disabled',true);
				$('#group').val($(this).val());
			}
		});	*/
		
		/*$(document).on('change', '#newGroup', function() {
			if($(this).val() === '') {
				$('#existingGroup').prop('disabled',false);
			}else{
				$('#existingGroup').prop('disabled',true);
				$('#group').val($(this).val());
			}
		});*/
	