/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_admin_new.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â© 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 28-11-2016            Parveen                Enhancement#665
 * 28-11-2016            Sahana                 For function code counter
 * 29-11-2016			Sahana           		To hide & show aut login type field
   01-Dec-2016			Sahana					Req#TJN_243_03:
   16-Dec-2016			Sahana					Req#TEN-72
   19-DEC-2016			Nagababu				Req#TEN-117
    19-12-2016			Nagababu				Req#TEN-125
    21-Dec-2016         Sahana                  defect #TEN-139(The Minute Field is blank in Schedule Task screen.)
    22-dec-2016        Sahana                   defect#Ten-149(In schedule,Issue in check box of  weekly recurrence)
    10-JAN-2016          Sahana                  TENJINCG-7(Calendar Icon is to be added for all date fields)
    02-08-2017          Padmavathi               Tenjincg-336
    18-08-2017         Leelaprasad               T25IT-225
    24-08-2017         Padmavathi                T25IT-287
    26-08-2017		   Pushpalatha				 T25IT-290
    29-08-2017        Leelaprasad                 T25IT-92
    29-08-2017		   Pushpalatha				 T25IT-288
    31-08-2017        Leelaprasad                T25IT-393\
    31-08-2017        Gangadhar Badagi           T25IT-394
    01-09-2017        Gangadhar Badagi           Checking count of hours
    27-09-2017        Gangadhar Badagi           To schedule the task between 55-60 minutes.
    22-06-2018			Preeti					T251IT-102
    22-06-2018			Preeti					T251IT-123
    05-07-2018			Preeti					T251IT-181
    19-02-2019			Preeti					TENJINCG-928
    20-03-2019          Padmavathi              TENJINCG-1014
    25-03-2019			Pushpalatha				TENJINCG-968
    22-04-2019          Padmavathi              To display proper label alignments in Recurrence Preferences page
    02-05-2019			Roshni					TENJINCG-1046
    06-06-2019			Ashiki					V2.8-109
    13-06-2019			Roshni					V2.8-24 & V2.8-94
    14-06-2019			Preeti					V2.8-113
    05-07-2019			Preeti					TV2.8R2-21
    12-08-2019			Prem					TENJINCG-1086
    14-11-2019			Prem					TENJINCG-1166
    11-06-2020          Priyanka                v210Reg-26
    12-06-2020          Priyanka                v210Reg-23 
 */

/*
 * Added file by sahana for Req#TJN_24_03 on 14/09/2016
 */
$(document).ready(function(){
	
	/*Modified by padmavathi to display proper label alignments in Recurrence Preferences page starts*/
	if($('#param').val()=='recurrence'){
		mandatoryFields('schedule_recurrence_form');
	}else{
		/*Added by Pushpalatha for TENJINCG-968 starts*/
		mandatoryFields('new_schedule_admin');
		/*Added by Pushpalatha for TENJINCG-968 ends*/
	}
	/*Modified by padmavathi to display proper label alignments in Recurrence Preferences page ends*/
	
	/*$('#lstApplication').hide();
	changed by sahana to hide aut login type field on 29/11/2016:starts
	$('#lastAutType').hide();
	$('#lstAppUserType').hide();
	changed by sahana to hide aut login type field on 29/11/2016:ends
	$('#lsttask').hide();
	$('#tasktype').hide();
	$('#pagination_block').hide();
	changed by sahana for Req#TJN_243_03:starts
	$('#dailyOption').hide();
	$('#weeklyOption').hide();
	$('#end-date').hide();
	$('#dailyRepOption').hide();*/
	
	/*Added by Padmavathi for TENJINCG-336 starts*/
	var name=$('#datepicker').val();
	/*Added by Roshni for V2.8-24 & V2.8-94 starts */
	if(!(name===undefined))
		name=name.replace("-"," ");
	/*Added by Roshni for V2.8-24 & V2.8-94 ends */
    populateHours(Date.parse(name));
	populateMinutes(Date.parse(name));
	/*Added by Padmavathi for TENJINCG-336 starts*/
	/*Changed by Leelaprasad for the defect T25IT-92 starts*/
	var hoursVal= $("#btntime").val();
	/*Changed by Leelaprasad for the defect T25IT-92 ends*/
	$('.gui-only').hide();
	$('.show-for-all').hide();
	$('.apiBlock').hide();
	/*$('#pagination_block').hide();*/
	$('#tasktype').hide();
	
	$('.chk').prop('disabled', true);
	var obj;
	var m;
	/*changed by sahana for Req#TJN_243_03:ends*/
	$(function() {
		/*$( "#datepicker" ).datepicker({ dateFormat:'dd-M-yy',minDate:0,
				});*/
		$("#datepicker").datepicker({
			/*   Changed by sahana for the improvement of TENJINCG-7: starts  */
			showOn: 'button',
			buttonText: 'Show Date',
			buttonImageOnly: true,
			buttonImage: './images/button/calendar.png',
			/*   Changed by sahana for the improvement of TENJINCG-7: ends  */
			dateFormat: 'dd-M-yy',
			minDate:0,
			constrainInput: true
		}); 

	});


	/*changed by sahana for Req#TJN_243_03:starts*/
	$(function() {
		m = $('#hiddendatepicker1').val();
		$( "#datepicker1" ).datepicker({ 
			/*   Changed by sahana for the improvement of TENJINCG-7: starts  */
			showOn: 'button',
			buttonText: 'Show Date',
			buttonImageOnly: true,
			buttonImage: './images/button/calendar.png',
			/*   Changed by sahana for the improvement of TENJINCG-7: ends  */
			dateFormat: 'dd-M-yy',
			minDate:m,
			constrainInput: true
		});
	});
	$('#datepicker').on('change', function() {
		m = $('#datepicker').val();	
	});
	/*changed by sahana for Req#TJN_243_03:ends*/

	$('#txtTask').on('change', function(){
		
		
		var task=$('#txtTask').val();
		/*$('#lstApplication').show();
		changed by sahana to hide aut login type field on 29/11/2016:starts
		$('#lastAutType').show()
		$('#lstAppUserType').show();
		changed by sahana to hide aut login type field on 29/11/2016:ends
		$('#lsttask').show();
		$('#tasktype').hide();
		$('#pagination_block').hide();*/
		
		if(task === '-1'){
			$('.show-for-all').hide();
			$('.gui-only').hide();
			$('.apiBlock').hide();
		}else{
		
			if(task === 'LearnAPI') {
				$('.gui-only').hide();
				$('.apiBlock').show();
				$('#learnerTypeBlock').hide();
				
				$('#tasktype').hide();
				/*Changed by Padmavathi for T25IT-287 starts*/
				$('#lstAppUserType').html('');
				$('#lstApplication').val('-1')
				/*Changed by Padmavathi for T25IT-287 ends*/
			} else{
				$('.gui-only').show();
				$('.apiBlock').hide();
				$('#tasktype').hide();
				/*Changed by Padmavathi for T25IT-287 starts*/
				$('#lstAPICode').html('');
				$('#lstApplication').val('-1')
				/*Changed by Padmavathi for T25IT-287 ends*/
			}
			
			$('#lstAppUserType').html('');
			$('.show-for-all').show();
		}
		

	});
	/*Modified by Preeti for TENJINCG-928 starts*/
	window.setInterval(function(){
		var today = new Date();
		/*$('#date').text(new Date().toString("dd mmm, yyyy, hh:MM:ss"));*/
		$('#date').text(today.toShortFormat());
	}, 1);
	Date.prototype.toShortFormat = function() {

	    var month_names =["01","02","03",
	                      "04","05","06",
	                      "07","08","09",
	                      "10","11","12"];
	    
	    var day = this.getDate();
	    var month_index = this.getMonth();
	    var year = this.getFullYear();
	    var hours = this.getHours();
	    var min = this.getMinutes();
	    var sec = this.getSeconds();
	    
	    return "" + year + "-" + month_names[month_index] + "-" + day +" " + hours +":" +min +":"+sec +" (GMT+0530 IST)";
	}
	/*Modified by Preeti for TENJINCG-928 ends*/
	$('#tblNaviflowModules').on('click','#chk_all', function(e){

		if ($("#chk_all").is(':checked')) 
		{	
			$(this).closest('table').find('input[type=checkbox]').prop('checked', this.checked);	
			e.stopPropagation();
		}
		else
		{
			$(this).closest('table').find('input[type=checkbox]').prop('checked',false);	 
			e.stopPropagation();
		}
	});
	/*changed by sahana for Req#TJN_243_03:starts*/
	/* Changed by Leelaprasad for the requirement defect T25IT-91 starts */
	var i=0;
	/* Changed by Leelaprasad for the requirement defect T25IT-91 ends */
	$('#btnRecurrence').click(function(){
	
		var val= $('#datepicker').val();
		var hour=$('#btntime').val();
		/* Changed by Leelaprasad for the requirement defect T25IT-91 starts */
		var frequency="";
		var recurCycle="";
		var recurDays=""; 
		var endDate="";
	
		if(i!=0){
		 frequency=$('#frequency').val();
		 recurCycle=$('#recurCycle').val();
		 recurDays= $('#recurDays').val();
		 if(frequency=="weekly"){
		 var str=recurDays.split(",");
		 recurDays="";
		 for(var i=0;i<str.length;i++){
			 recurDays=recurDays+str[i]+":";
		 }
		}
		 endDate=$('#endDate').val();
		}
		
		$('.recurDetails').remove();
		/* Changed by Leelaprasad for the requirement defect T25IT-91 ends */
		var val= $('#datepicker').val();
		var hour=$('#btntime').val();
		if($('#datepicker').val()!=""){
			/* Changed by Leelaprasad for the requirement defect T25IT-91 starts */
			/*$('#recur-sframe').attr('src','schedule_recurrence.jsp?val='+val+','+hour);*/
			/*$('#recur-sframe').attr('src','schedule_recurrence.jsp?val='+val+','+hour+','+frequency+','+recurCycle+','+recurDays+','+endDate);*/
			/*Modified by padmavathi to display proper label alignments in Recurrence Preferences page starts*/
			$('#recur-sframe').attr('src','schedule_recurrence.jsp?val='+val+','+hour+','+frequency+','+recurCycle+','+recurDays+','+endDate+'&paramval=recurrence');
			/*Modified by padmavathi to display proper label alignments in Recurrence Preferences page ends*/
			/* Changed by Leelaprasad for the requirement defect T25IT-91 ends */
			$('.modalmask').show();
			$('.subframe').show();
			i++;
		}
		else{
			showMessage("Please enter the Date and Time of the Scheduler Task",'error'); 
			e.preventDefault();
		}
	});
	/*changed by sahana for Req#TJN_243_03:ends*/

	$("#lstAPICode").change(function() {
		var apiCode = $(this).val();
		/*Added by Pushpalatha for defect T25IT-290 starts*/
		clearMessages();
		/*Added by Pushpalatha for defect T25IT-290 ends*/
		var appId = $('#lstApplication').val();
		/*Added by pushpalatha for defect T25IT-288 starts*/
		/*Commented by Preeti for T251IT-181 starts*/
		/*var type=populalteAPIOperationsType(appId, apiCode);
		
		if(type==='rest.generic'){
			var opt= document.getElementById('lstApiLearnType').options[0];
		   
		     opt.text = 'Learn from URL';
		     opt.value='URL'
		    	 
		    var opt= document.getElementById('lstApiLearnType').options[1];
			   
		     opt.text = 'Learn from Request and Response JSON';
		     opt.value='REQUEST_JSON'
		}
		else{
			var opt= document.getElementById('lstApiLearnType').options[0];
		    
		     opt.text = 'Learn from URL';
		     opt.value='URL';
		     var opt1= document.getElementById('lstApiLearnType').options[1];
		     
		     opt1.text = 'Learn from Request And Response XML';
		     opt1.value='REQUEST_XML';
		}*/
		/*Commented by Preeti for T251IT-181 ends*/
		$('#learnerTypeBlock').show();
		/*Added by Pushpalatha for defect T25IT-288 ends*/
		
		
		if(apiCode === '-1') {
			
		}else{
			populalteAPIOperations(appId, apiCode);
			
			/*paginate();*/
		}
	});
	
	$('#lstApplication').change(function(){
		$('#learnerTypeBlock').hide();
		var app = $(this).val();
		clearMessages();
		if(app == '-1'){
		
			/*added by  Parveen for  Enhancement#665   on 28-11-2016 starts*/ 
			$('#lstAppUserType').html('');
			/*added by  Parveen for  Enhancement#665   on 28-11-2016 ends*/ 
			$('#lstModules').html("<option value='-1' selected>-- Select One --</option>");
		/*	$('#lstModules').select2('val','-1');*/
		}else{
			
			/****************
			 * Changed by sriram for TENJINCG-169 (Schedule API Learning)
			 */
			/*added by  Parveen for  Enhancement#665   on 28-11-2016 starts 
			populateUserAutTypes(app);
			added by  Parveen for  Enhancement#665   on 28-11-2016 ends 
			$('#tasktype').show();
			$('#pagination_block').show();
			var task=$('#txtTask').val();
			populateMappingModuleInfo(app);
			paginate();*/
			
			var taskType = $('#txtTask').val();
			if(taskType === 'LearnAPI'){
				populateApiCodes(app);
				/*Added by Preeti for T251IT-102 starts*/
				$('#func').hide();
				$('#api').show();
				$('#tblNaviflowModules').html('');
				/*Added by Preeti for T251IT-102 ends*/
			}else{
				$('.apiBlock').hide();
				/*added by  Parveen for  Enhancement#665   on 28-11-2016 starts*/ 
				populateUserAutTypes(app);
				/*added by  Parveen for  Enhancement#665   on 28-11-2016 ends*/ 
				$('#tasktype').show();
				$('#pagination_block').show();
				var task=$('#txtTask').val();
				populateMappingModuleInfo(app);
				/*Added by Preeti for T251IT-102 starts*/
				$('#func').show();
				$('#api').hide();
				/*Added by Preeti for T251IT-102 ends*/
				/*paginate();*/
			}
			/****************
			 * Changed by sriram for TENJINCG-169 (Schedule API Learning) ends
			 */
		}
	});
	populateAppInfo();
	populateClients();
	/*changed by sahana for Req#TJN_243_03:starts*/
	$('#btnOk').click(function(){
		var validated = validateForm('schedule_recurrence_form');
		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
		}
		
		var errorMsg='';
		var flag=false;
		object = new Object();
		object.frequency = $('#recurrence').val();
		var recurCount=0;
		if(object.frequency=='none'){
			errorMsg='Please select the frequency';
		}
		else if(object.frequency=='daily')
		{
			
			/*Changed by Leelprasad for the requirement defect T25IT-225 starts*/
			if($('#numDays').val()==0){
				/*Modified by Ashiki for V2.8-109 starts*/
				showMessage("Number of days for daily recurrence cannot be 0",'error');
				/*Modified by Ashiki for V2.8-109 ends*/
				return false;
			}
			/*Changed by Leelprasad for the requirement defect T25IT-225 ends*/
			object.recurDays='Every day';
			recurCount=$('#numDays').val();
			if(object.recurDays=='Every day'){
				var num=$('#numDays').val();
				if(num!="")
					object.recurCycles=$('#numDays').val();
				else
					errorMsg='Please enter the day(s) to recur the task';
			}
			flag=true;
		}
		else if(object.frequency=='daily-Repetitive')
		{
			object.recurDays='N/A';
			object.recurCycles=$('#numHrs').val();
			object.endDate='N/A';
			/*Added by Gangadhar Badagi for checking count of hours starts*/
			if($('#numHrs').val()==0){
				showMessage("number of hours for daily-Repetitive recurrence cannot be 0",'error');
				return false;
			}
			/*Added by Gangadhar Badagi for checking count of hours ends*/
		}
		else if(object.frequency=='weekly')
		{
			var recurdays = [];
			var i=0;
			$('#checks input:checked').each(function() {
				recurdays[i]=$(this).val();
				i++;
			});
			var days='';
			for(var i=0;i<recurdays.length;i++){
				if(i==recurdays.length-1){
					days=days+recurdays[i];
				}
				else
					days=days+recurdays[i]+",";
			}
			/*changed by Leelaprasad for T25IT-393 starts*/
			if(days==""){
				showMessage("Please select reccurence days for weekly reccrence",'error');
				return false;
			}
			/*changed by Leelaprasad for T25IT-393 ends*/
			object.recurDays=days;
			object.recurCycles=$('#recurWeeks').val();
			flag=true;
		}
		if(flag==true){
			var strt =$('#hiddendatepicker1').val();
			var end=$('#datepicker1').val();
			var strtDate=new Date(strt);
			var endDt=new Date(end);
			var timeDiff = Math.abs(endDt.getTime() - strtDate.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
		/* Added by Padmavathi for TENJINCG-1014 Starts */
			var todaysDate=new Date();
			todaysDate.setHours(0,0,0,0);
			/* Added by Padmavathi for TENJINCG-1014 ends */
			if(end=="")
			{
				showMessage('Please select the End Date','error');
				return false;
			}
			/* Added by Padmavathi for TENJINCG-1014 Starts */
			if(!validateDate(end)){
				showMessage('Please enter valid End Date in this format DD-MMM-YYYY.','error');
				return false;
			}else if(new Date(end)<todaysDate){
				showMessage("End Date should be greater than or equal to Today's Date");
				return false;
			}
			/* Added by Padmavathi for TENJINCG-1014 ends */
			if(object.frequency=='daily'){
				if(diffDays<recurCount)
					errorMsg='Recur days exceeds between start date and end date';
			}
			else{
				var arr=[];
				for(var i=0;i<diffDays;i++){
					arr[i]=new Date(strtDate+(i*1000*60*60*24));
				}
			}
			object.endDate=$('#datepicker1').val();
		}

		if(errorMsg==''){
			window.parent.closeModal();
			/* Changed by Leelaprasad for the requirement defect T25IT-91 starts */
			/*window.parent.$('body').append("<div><input type='hidden' id='frequency' value='"+object.frequency+"'/><input type='hidden' id='recurCycle' value='"+object.recurCycles+"'/><input type='hidden' id='recurDays' value='"+object.recurDays+"'/><input type='hidden' id='endDate' value='"+object.endDate+"'/></div>");*/
			window.parent.$('body').append("<div class='recurDetails'><input type='hidden' id='frequency' value='"+object.frequency+"'/><input type='hidden' id='recurCycle' value='"+object.recurCycles+"'/><input type='hidden' id='recurDays' value='"+object.recurDays+"'/><input type='hidden' id='endDate' value='"+object.endDate+"'/></div>");
			/* Changed by Leelaprasad for the requirement defect T25IT-91 ends */	
		}
		else{
			showMessage(errorMsg,'error');
		}
	});	
	$('#recurrenceOption').change(function(){
		var opt= $('#recurrence').val();
		if(opt=='daily')
		{
			$('#dailyOption').show();
			$('#weeklyOption').hide();
			$('#dailyRepOption').hide();
			$('#end-date').show();

		}
		else if(opt=='weekly'){
			$('#dailyOption').hide();
			$('#weeklyOption').show();
			$('#dailyRepOption').hide();
			$('#end-date').show();
		}
		else if(opt=='daily-Repetitive'){
			$('#dailyOption').hide();
			$('#weeklyOption').hide();
			$('#dailyRepOption').show();
			$('#end-date').hide();
		}
		else{
			$('#dailyOption').hide();
			$('#weeklyOption').hide();
			$('#dailyRepOption').hide();
			$('#end-date').hide();
		}
	});
	/*changed by sahana for Req#TEN-72:starts*/
	/*changed by sahana for Req#TEN-117:starts*/
	/*$('#taskName').keyup(function (){
		var inputVal = $(this).val();
		var regex = new RegExp(/[^a-zA-Z0-9]/);
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			alert('Please enter only alphanumeric characters');
			$(this).val('');
		}
	});*/
	/*changed by sahana for Req#TEN-72:ends*/
	/*changed by sahana for Req#TEN-117:Ends*/
	$('#numDays').keyup(function (){
		var inputVal = $(this).val();
	/*Changed by Leelprasad for the requirement defect T25IT-225 starts*/
		/*var regex = new RegExp(/[^1-9]/g);*/
		var regex = new RegExp(/[^0-9]/g);
		/*Changed by Leelprasad for the requirement defect T25IT-225 ends*/
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			alert('Please enter only numbers into the textbox and number must start with 1 ');
			$(this).val('');
		}
	});
	$('#numHrs').keyup(function (){
		var inputVal = $(this).val();
		/*Added by Gangadhar Badagi for T25IT-394 starts*/
		var number = parseInt(inputVal);
		/*var regex = new RegExp(/[^1-9]/g);*/
		var regex = new RegExp(/[^0-9]/g);
		/*Added by Gangadhar Badagi for T25IT-394 ends*/
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			alert('Please enter only numbers into the textbox and number must start with 1 ');
			$(this).val('');
		}
		var hour=$('#hourTime').val();
		if(inputVal>23){
			alert('Specify hour(s)between 1 to 23');
			$('#numHrs').val("");
		}
		else if(inputVal>(24-hour)){
			alert('Invalid hour(s) entry');	
			$('#numHrs').val("");
		}
		/*Added by Gangadhar Badagi for T25IT-394 starts*/
		else if(number==0){
			alert('Invalid hour');
			$('#numHrs').val("");
		}
		/*Added by Gangadhar Badagi for T25IT-394 ends*/
	});
	/*changed by sahana for Req#TJN_243_03:ends*/
	
	/*added by Priyanka for v210Reg-23 starts*/ 
		$(document).click(function() {
			  $('#chk_all').click(function() {
			    var checked = this.checked;
			    $('input[type="checkbox"]').each(function() {
			      this.checked = checked;
			    });
			  })
			});
	/*added by Priyanka for v210Reg-23 ends*/ 

	$('#btnSave').click(function(){
	/* Added by Padmavathi for TENJINCG-1014 Starts */
		clearMessages();
		/* Added by Padmavathi for TENJINCG-1014 ends */
		/*commented by shruthi for v210Reg-19 starts*/ 
		/*var validated = validateForm('new_schedule_admin');
		  if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
			}*/
		/*commented by shruthi for v210Reg-19 ends*/ 
		
		/*added by shruthi for v210Reg-19 starts*/ 
		var taskname = $('#taskName').val();
		var task = $('#txtTask').val();
		var application = $('#lstApplication').val();
		if(taskname==='')
			{
			showMessage('Task name is required.', 'error');
			return false;
			}
		if (task==='-1')
			{
			showMessage('Task is required.', 'error');
			return false;
			}
		if (application==='-1')
			{
			showMessage('Appication is required.', 'error');
			return false;
			}
		/*added by shruthi for v210Reg-19 ends*/ 
		
        
		if(task === 'LearnAPI') {
			//Validate API Code
			var apiCode = $('#lstAPICode').val();
			if(apiCode === '' || apiCode === '-1' || apiCode == undefined) {
				showMessage('API Code is required.', 'error');
				return false;
			}
		} else {
			//Validate AUT Credentials and client.
			var autCred = $('#lstAppUserType').val();
			var client = $('#txtclientregistered').val();
			
			if(autCred === '-1' || autCred === '' || autCred == undefined) {
				showMessage('AUT User Type is required.', 'error');
				return false;
			}
			
			if(client === '-1' || client ==='' || client == undefined) {
				showMessage('Client is required.','error');
				return false;
			}
		}
		
		/*Added by Preeti for T251IT-123 starts*/
		var selDate=$('#datepicker').val();
		/* Added by Padmavathi for TENJINCG-1014 Starts */
			if(!validateDate(selDate)){
				showMessage('Please enter valid Date in this format DD-MMM-YYYY.','error');
				return false;
			}
		/* Added by Padmavathi for TENJINCG-1014 ends */
		var today=new Date();
		var selhour=$('#btntime').val();
		var selMinute=$('#btntime1').val();
		var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
		var curDate=today.getDate()+'-'+months[today.getMonth()]+'-'+today.getFullYear();
		/* Added by Padmavathi for TENJINCG-1014 Starts */
		if((new Date(curDate).getTime()) >= (new Date(selDate).getTime())){
		/* Added by Padmavathi for TENJINCG-1014 ends */
			if((new Date(curDate).getTime()) == (new Date(selDate).getTime())){
				var curHour=today.getHours();
				var curMinute=today.getMinutes();
				if(selhour<curHour){
					alert("Selected Time should be greater than or equal to Current Time");
					return false;
				}
				if(selhour==curHour){
					if(selMinute<curMinute){
						alert("Selected Time should be greater than or equal to Current Time");
						return false;
					}
				}
			}
			/*Added by Preeti for T251IT-123 ends*/
			/* Added by Padmavathi for TENJINCG-1014 Starts */
			else{
				alert("Selected Date should be greater than or equal to Today's Date");
				return false;
			}
			/* Added by Padmavathi for TENJINCG-1014 ends */
		}

		var obj = new Object();

		obj.schId = $('#btnSchId').val();
		obj.date = $('#datepicker').val();
		/*changed by sahana for Req#TJN_243_03:starts*/
		var min=$('#btntime1').val();
		if(min!=null)
			obj.time = $('#btntime').val()+":"+$('#btntime1').val();

		/*changed by sahana for Req#TJN_243_03:ends*/
		
		if(task === 'LearnAPI') {
			obj.apicode = $('#lstAPICode').val();
			obj.apilearntype = $('#lstApiLearnType').val();
		}
		
		obj.task = $('#txtTask').val();
		obj.client = $('#txtclientregistered').val();
		obj.status = $('#txtStatus').val();
		obj.crtBy = $('#txtCrtBy').val();
		obj.crtOn = $('#txtCrtOn').val();
		obj.browser=$('#lstBrowserType').val();
		obj.autLogin=$('#lstAppUserType').val();
		obj.appId=$('#lstApplication').val();
		/*changed by sahana for req#TEN-72 starts*/
		obj.taskName=$('#taskName').val();
		/*changed by sahana for req#TEN-72 Ends*/
		/*changed by sahana for Req#TJN_243_03:starts*/
		var flag=false;
		var frequency=$('#frequency').val();
		if(typeof frequency!="undefined"){
			obj.frequency=$('#frequency').val();
			if(obj.frequency=='daily'){
				obj.recurCycles=$('#recurCycle').val();	
				obj.recurDays=$('#recurDays').val();
			}	
			else if(obj.frequency=='weekly'){
				obj.recurCycles=0;
				obj.recurDays=$('#recurDays').val();
			}
			else if(obj.frequency=='daily-Repetitive'){
				obj.recurCycles=$('#recurCycle').val();	
				obj.recurDays='N/A';
			}
			if(obj.frequency!='daily-Repetitive')
				obj.endDate=$('#endDate').val();
			flag=true;
		}
		if(flag==true){
			obj.schRecur='Y';
		}
		else{
			obj.schRecur='N';
		}
		/*changed by sahana for Req#TJN_243_03:ends*/
		var jsonString = JSON.stringify(obj);
		var csrftoken_form=$("#csrftoken_form").val();
		// var csrftoken_form = $("<input type = 'hidden' name ='csrftoken_form' id ='csrftoken_form' value = '" + csrftoken+"' />").val();
		var val = new Array();	
		/*added by  sahana for the count of selected function code   on 28-11-2016 starts*/ 
		var count=0;
		/*added by  sahana for the count of selected function code   on 28-11-2016 ends*/ 
		if(obj.task=='Extract'){
			var i=0;
			$('.tdpval').each(function(){
				var m=new Object();
				m.funcCode=$(this).attr('func');
				m.path=$(this).val();
				/*changed by sahana for req#TJN-125 Starts*/
				if(m.path!=""){
					val.push(m);
					i++;
				}
				//i++;
				/*changed by sahana for req#TJN-125 Ends*/
			});
			/*added by  sahana for the count of selected function code   on 28-11-2016 starts*/ 
			count=i;  
			/*added by  sahana for the count of selected function code   on 28-11-2016 ends*/ 
		}
		else{
			$(function(){
				var i=0;
				$('.tiny:checked').each(function(){
					val[i] = $(this).val();
					i++;
				});
				/*added by  sahana for the count of selected function code   on 28-11-2016 starts*/ 
				count=i;
				/*added by  sahana for the count of selected function code   on 28-11-2016 ends*/ 
			});
		}
		/*added by  sahana for the count of selected function code   on 28-11-2016 starts*/ 
		if(count==0)
		{
			/*changed by sahana for req#TJN-125 Starts*/
			if(obj.task=='Extract'){
				showMessage('Please specify the test data path','error');
			}else if(obj.task === 'LearnAPI') {
				showMessage('Please select atleast one API Operation', 'error');
			}
			else{
				showMessage('Please select atleast one function code','error');
			}
			/*changed by sahana for req#TJN-125 Ends*/
		}
		/*changed by sahana for Req#TJN_243_03:starts*/
		else if(min==null)
			showMessage('Invalid minute(s) entry','error');
		/*changed by sahana for Req#TJN_243_03:ends*/
		else{
			/*added by  sahana for the count of selected function code   on 28-11-2016 ends*/ 
			$.ajax({
				url:'SchedulerServlet',
				/* Modified by Roshni for TENJINCG-1046 starts */
			/*	data:{param:"NEW_SCH",json:jsonString,functions:JSON.stringify(val)}, */
				data:{param:"NEW_SCH",json:jsonString,functions:JSON.stringify(val),source:"adminpage",csrftoken_form:csrftoken_form},
				/* Modified by Roshni for TENJINCG-1046 ends */
				type: 'POST',
				async: false,
				dataType:'json',
				success:function(data){
					if(data.status == 'success'){
						showMessage(data.message,'success');
						/*Modified by Ashiki for V2.8-63 starts*/ 
						/*window.setTimeout(function(){window.location='SchedulerServlet?param=FETCH_ALL_ADMIN_SCH&paramval=Scheduled';},500)*/
						/*Modified by Ashiki for V2.8-63 ends*/ 
					}else{
						showMessage(data.message,'error');
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(errorThrown,'error');
				}
			});
			/*added by  sahana for the count of selected function code   on 28-11-2016 starts*/ 
		}
		/*added by  sahana for the count of selected function code   on 28-11-2016 ends*/ 
	});
	/*changed by sahana for Req#TJN_243_03:starts*/
	$('#btnCancel').click(function(){
		window.parent.closeModal();
	});
	$('#datepicker').on('change', function() {
		var name=$('#datepicker').val(); 
		hoursVal=$("#btntime").val();
		hoursVal=populateHours(Date.parse(name));
		populateMinutes(Date.parse(name));
	});
	$('#btntime').on('change', function(){	
		var html="<option value='00'>00</option>";
		var timeval=$(this).val();
		if(hoursVal!=timeval){
			if(timeval!=null){
				for(var i=1;i<12;i++){
					if(i==1)
						html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";
					else
						html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
				}
				$('#btntime1').html(html);
			}
		}
		else{
			var name=$('#datepicker').val(); 
			hoursVal=$("#btntime").val();
			hoursVal=populateHours(Date.parse(name));
			populateMinutes(Date.parse(name));
		}
	});

	$('#datepicker1').on('change', function() {
		/*changed by sahana for defect #TEN-149  :Starts*/
		$('#checks').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#checks').find('input').attr('disabled','disabled');
		/*changed by sahana for defect #TEN-149  :ends*/
		var strt =$('#hiddendatepicker1').val();
		var end=$('#datepicker1').val();	
		var strtDate=new Date(strt);
		var endDate=new Date(end);
		var timeDiff = Math.abs(endDate.getTime() - strtDate.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
		var arr=[];
		for(var i=0;i<=diffDays;i++){
			arr[i]=new Date(strtDate.getTime()+(i*1000*60*60*24));
		}
		var gsDayNames = new Array(
				'Sunday',
				'Monday',
				'Tuesday',
				'Wednesday',
				'Thursday',
				'Friday',
				'Saturday'
		);
		for(var i=0;i<arr.length;i++){
			var d = new Date(arr[i]);
			var dayName = gsDayNames[d.getDay()];
			var j=0;
			var flag=false;
			$('.chk').each(function() {
				var day=$(this).val();
				if(flag==false){
					if(day==dayName){
						flag=true;
						$('#chk'+(j+1)).prop('disabled', false);

					}
				}
				j++;
			});
		}
	});
	/*Added by Preeti for TV2.8R2-21 starts*/
	$('#btnCancelTask').click(function(){
		var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
		if(confirmResult) {
			window.location.href='SchedulerServlet?param=FETCH_ALL_ADMIN_SCH&paramval=All';
		}else{
			return false;
		}
	});
	/*Added by Preeti for TV2.8R2-21 ends*/
	
	/*Added by Prem for TENJINCG-1086 starts*/
	$(document).on('click','#btnRefreshAut', function() {
		if ( confirm("Unsaved changes will be cleared, this action cannot be undone")){
		window.location.href ='schedule_admin_new.jsp ';
		}
	});
	/*Added by Prem for TENJINCG-1086 Ends*/
});

function closeModal(){
	$('#recur-sframe').attr('src','');
	$('.subframe').hide();
	$('.modalmask').hide();	
}
/*changed by sahana for Req#TJN_243_03:ends*/
function populateClients(){

	var jsonObj = fetchAllClients();
	var status = jsonObj.status;
	clearMessages();
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jArray = jsonObj.clients;

		for(i=0;i<jArray.length;i++){
			var client = jArray[i];
			html = html + "<option value='" + client.name + "'>" + client.name + "</option>";
		}
		$('#txtclientregistered').html(html);
	}else{
		showMessage(jsonObj.message, 'error');
	}
}
function populateModuleInfo(appId){
	var jsonObj = fetchModulesForApp(appId);

	$('#lstModules').html('');
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html = "<option value='-1'>-- Select One --</option>";
		var jarray = jsonObj.modules;
		for(i=0;i<jarray.length;i++){
			var json = jarray[i];
			/*Modified by Prem for TENJINCG-1166 start*/
			html = html + "<option value='" + json.code + "'>" +  escapeHtmlEntities(json.code)  + " - " +  escapeHtmlEntities(json.name) + "</option>";
			/*Modified by Prem for TENJINCG-1166 ends*/
		}

		$('#lstModules').html(html);

	}else{
		showMessage(jsonObj.message,'error');
	}
}

/*added by  Parveen for  Enhancement#665   on 28-11-2016 starts*/ 
function populateUserAutTypes(appId){
	var jsonObj = fetchUserTypeForApp(appId);

	$('#lstAppUserType').html('');
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html = "<option value='-1'>-- Select One --</option>";
		var jarray = jsonObj.appUserTypes;
		if(jarray == undefined){

		}else{
			for(i=0;i<jarray.length;i++){
				var json = jarray[i];
				/*Modified by Prem for TENJINCG-1166 start*/
				html = html + "<option value='" +  escapeHtmlEntities(json.code) + "'>" +  escapeHtmlEntities(json.code)  + "</option>";
				/*Modified by Prem for TENJINCG-1166 ends*/
			}
		}

		$('#lstAppUserType').html(html);
	}else{
		showMessage(jsonObj.message,'error');
	}
}
/*added by  Parveen for  Enhancement#665   on 28-11-2016 ends*/ 
function populateMappingModuleInfo(appId){

	var jsonObj = fetchModulesForApp(appId);

	var val;

	$('#tblNaviflowModules').html('');
	$('#tblNaviflowModules_body').html('');
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html1="";
		var html = "";
		var jarray = jsonObj.modules;
		var task=$('#txtTask').val();
		if(task=='Learn'){
			html1=html1+"<thead><tr><th class='table-row-selector nosort sorting_disabled'><input type='checkbox' class='tbl-select-all-rows' id='chk_all'/></th><th>Function Code</th><th>Function Name</th></tr></thead>";
		}else{
			html1=html1+"<thead><tr><th><input type='checkbox' class='tbl-select-all-rows' id='chk_all'/></th><th>Function Code</th><th>Input file Path</th></tr></thead>";
		}
		for(i=0;i<jarray.length;i++){
		/*Modified by Preeti for V2.8-113 starts*/
			var addClass='';
			if(i%2==0)
				addClass = 'odd';
			else
				addClass = 'even';
			var json = jarray[i];
			if(task=='Learn')
				/*Modified by Prem for TENJINCG-1166 start*/
				html = html + "<tr class='"+addClass+"'><td><input class='tiny' type='checkbox' name='"+ escapeHtmlEntities(json.code)+"' value='"+ escapeHtmlEntities(json.code)+"' id='"+ escapeHtmlEntities(json.code)+"'></input></td><td>"+ escapeHtmlEntities(json.code)+"</td><td>"+ escapeHtmlEntities(json.name)+"</td></tr>";
			else	
				html = html + "<tr class='"+addClass+"'><td><input class='tiny' type='checkbox' name='"+ escapeHtmlEntities(json.code)+"' value='"+ escapeHtmlEntities(json.code)+"' id='"+ escapeHtmlEntities(json.code)+"'></input></td><td>"+ escapeHtmlEntities(json.code)+"</td><td><input type='text' class='stdTextBox tdpval' id='url_path' name='urlpath' func='"+ escapeHtmlEntities(json.code)+"'/></td></tr>";
			/*Modified by Prem for TENJINCG-1166 ends*/
		}
		val=$("#url_path").val();
		html=html1+html;
		$('#tblNaviflowModules').html('');
		$('#tblNaviflowModules').html(html);
		$('#tblNaviflowModules').DataTable().destroy();
		$('#tblNaviflowModules').html(html);
		$('#tblNaviflowModules').dataTable();
	/*	commented by shruthi for v210Reg-32 starts*/
		/*$('#tblNaviflowModules').DataTable().destroy();
		$('#tblNaviflowModules').dataTable();*/
		/*	commented by shruthi for v210Reg-32 ends*/
		/*Modified by Preeti for V2.8-113 ends*/
	}else{
		showMessage(jsonObj.message,'error');
		$('#tasktype').hide();
		/*html=html1+html;
		$('#tblNaviflowModules').html(html);
		$('#tblNaviflowModules').DataTable().destroy();
		$('#tblNaviflowModules').html(html);
		$('#tblNaviflowModules').dataTable();*/
	}
}


/****************************
 * Added by Sriram for TENJINCG-169 (Schedule API Learning)
 */
function populateApiCodes(appId) {
	var jsonObj = fetchApiModulesForApp(appId);
	$('#lstAPICode').html('');
	var html = "<option value='-1'>-- Select One --</option>";
	var status = jsonObj.STATUS;
	if(status== 'SUCCESS'){
		
		var jarray = jQuery.parseJSON(jsonObj.API);
		for(i=0;i<jarray.length;i++){
			var json = jarray[i];
			/*Modified by Prem for TENJINCG-1166 start*/
			/*html = html + "<option value='" + json.code + "'>" + json.name + "</option>";*/
			html = html + "<option value='" +  escapeHtmlEntities(json.code) + "'>" +  escapeHtmlEntities(json.code)  + "</option>";
			/*Modified by Prem for TENJINCG-1166 ends*/
		}
		
		$('#lstAPICode').html(html);
	}else{
		showMessage(jsonObj.MESSAGE,'error');
		$('#tasktype').hide();
		
	}
}

/*Added by Pushpalatha for defect-T25IT-288 starts*/
function fetchApiModulesTypeForApp(appId,apiCode){
	var jsonObj;
	$.ajax({
		url:'APIServlet',
		data:'param=fetch_apis_type&redirect=false&lstAppId=' + appId+'&appCode='+apiCode,
		async:false,
		dataType:'json',
		success:function(data){
			console.log('API data ' + data);
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});
	
	return jsonObj;
	
	
}

function populalteAPIOperationsType(appId,apiCode) {
	var jsonObj = fetchApiModulesTypeForApp(appId,apiCode);
	
	var jsonType="";
	var status = jsonObj.STATUS;
	if(status== 'SUCCESS'){
		
		var jarray = jQuery.parseJSON(jsonObj.API);
		 jsonType=jsonObj.API;
		jsonType=jsonType.substring(1,(jsonType.length-1));
		
		
	}else{
		showMessage(jsonObj.MESSAGE,'error');
		
		
	}
	
	return jsonType;
}
/*Added by Pushpalatha for defect-T25IT-288 ends*/
function populalteAPIOperations(appId, apiCode){

	var jsonObj = fetchApiOperationsForApp(appId, apiCode);

	var val;
	

	$('#tblNaviflowModules').html('');
	$('#tblNaviflowModules_body').html('');
	var status = jsonObj.STATUS;
	if(status== 'SUCCESS'){
		var html1="";
		var html = "";
		var jarray = jsonObj.OPERATIONS;
		jarray = jQuery.parseJSON(jarray);
		var task=$('#txtTask').val();
		html1=html1+"<thead><tr><th class='table-row-selector nosort sorting_disabled '><input type='checkbox' class='tbl-select-all-rows' id='chk_all'/></th><th>Operation Name</th><th>API Code</th></thead>";
		for(i=0;i<jarray.length;i++){
		/*Modified by Preeti for V2.8-113 starts*/
			var addClass='';
			if(i%2==0)
				addClass = 'odd';
			else
				addClass = 'even';
			var json = jarray[i];
			/*Modified by Prem for TENJINCG-1166 start*/
			html = html + "<tr class='"+addClass+"'><td><input class='tiny' type='checkbox' name='"+ escapeHtmlEntities(json.name)+"' value='"+ escapeHtmlEntities(json.name)+"' id='"+ escapeHtmlEntities(json.name)+"'></input></td><td>"+ escapeHtmlEntities(json.name)+"</td><td>"+ escapeHtmlEntities(apiCode)+"</td></tr>";
			/*Modified by Prem for TENJINCG-1166 ends*/
		}
		val=$("#url_path").val();
		html=html1+html;
		$('#tblNaviflowModules').html('');
		$('#tblNaviflowModules').html(html);
		$('#tblNaviflowModules').DataTable().destroy();
		$('#tblNaviflowModules').html(html);
		$('#tblNaviflowModules').dataTable();
		$('#tasktype').show();
			$('#pagination_block').show();
		/*	commented by shruthi for v210Reg-32 starts*/
		/*$('#tblNaviflowModules').DataTable().destroy();
		$('#tblNaviflowModules').dataTable();*/
		/*	commented by shruthi for v210Reg-32 ends*/
		/*Modified by Preeti for V2.8-113 ends*/
	}else{
		showMessage(jsonObj.MESSAGE,'error');
		/* added by prem for v210Reg-31 starts*/
		$('#tasktype').hide();
		/* added by shruthi for v210Reg-32 starts
		html=html1+html;
		$('#tblNaviflowModules').html(html);
		$('#tblNaviflowModules').DataTable().destroy();
		$('#tblNaviflowModules').html(html);
		$('#tblNaviflowModules').dataTable();
		 added by shruthi for v210Reg-32 ends*/
		/* added by prem for v210Reg-31 End*/
		
	}
}


function populateAppInfo(appId){
	var jsonObj = fetchAllAuts();

	$('#lstApplication').html('');
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html = "<option value='-1'>-- Select One --</option>";
		var jarray = jsonObj.auts;
		for(i=0;i<jarray.length;i++){
			var json = jarray[i];
			/*Modified by Prem for TENJINCG-1166 start*/
			html = html + "<option value='" +  json.id + "'>" +  escapeHtmlEntities(json.name)+"</option>";
			/*Modified by Prem for TENJINCG-1166 ends*/
		}

		$('#lstApplication').html(html);

	}else{
		showMessage(jsonObj.message,'error');
	}
}
/*changed by sahana for Req#TJN_243_03:starts*/
function populateClients(){

	var jsonObj = fetchAllClients();

	var status = jsonObj.status;
	clearMessages();
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jArray = jsonObj.clients;

		for(i=0;i<jArray.length;i++){
			var client = jArray[i];
			html = html + "<option value='" +  escapeHtmlEntities(client.name) + "'>" +  escapeHtmlEntities(client.name) + "</option>";
		}

		$('#txtclientregistered').html(html);

	}else{
		showMessage(jsonObj.message, 'error');
	}
}
/*changed by sahana for defect #TEN-139  :starts*/
function populateHours(date1){
	/*Changed by Leelaprasad for the defect T25IT-92 starts*/
	var hoursVal= "";
	/*Changed by Leelaprasad for the defect T25IT-92 ends*/
	var dateval=new Date(date1);
	var month=dateval.getMonth()+1;
	var finaldate=dateval.getDate()+"-"+month+"-"+dateval.getFullYear();
	var curmonth=new Date().getMonth()+1;
	var currDate=new Date().getDate()+"-"+curmonth+"-"+new Date().getFullYear();
	var html ="";
	var getMin=new Date().getMinutes();
	var counter=0;
	var currentHour=new Date().getHours();
	var newHour;
	if(getMin>55 && getMin<=60)
	{
		for(var i=0;i<24;i++){
			if(currentHour==parseInt(i))
			{	
				if(parseInt(i)<10){
					newHour="0"+parseInt(i+1);
				}
				else{
					newHour=i+1;
				}
				if(i==currentHour){
					html = html + "<option value='" +newHour + "' selected='selected'>" + newHour + "</option>";
				}
				else{
					html = html + "<option value='" + newHour+ "'>" + newHour + "</option>";
				}
				i=i+1;
			}
			else if(i>currentHour+1){
				if(parseInt(i)<10){
					newHour="0"+parseInt(i);
				}
				else{
					newHour=i;
				}
				if(i==currentHour){
					html = html + "<option value='" +newHour + "' selected='selected'>" + newHour + "</option>";
				}
				else{
					html = html + "<option value='" + newHour+ "'>" + newHour + "</option>";
				}


			}
			/*Added by  Gangadhar Badagi to schedule the task between 55-60 minutes starts*/
			else{
				var newTime="";
				if(i<=9)
					newTime ="0"+i;
				else
					newTime=i;
				if(finaldate==currDate){
					if(newTime>=new Date().getHours()){
					if(counter==0){
							hoursVal=newTime;
							counter++;
						}
						html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
					}
				}
				else{	
					if(counter==0){
						hoursVal=newTime;
						counter++;
					}
					html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
				}
			}
			/*Added by  Gangadhar Badagi to schedule the task between 55-60 minutes ends*/

		}
	}
	else
	{
		for(var i=0;i<24;i++){
			var newTime="";
			if(i<=9)
				newTime ="0"+i;
			else
				newTime=i;
			if(finaldate==currDate){
				if(newTime>=new Date().getHours()){

					if(counter==0){
						hoursVal=newTime;
						counter++;
					}
					html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
				}
			}
			else{	
				if(counter==0){
					hoursVal=newTime;
					counter++;
				}
				html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
			}

		}
	}
	$('#btntime').html(html);
	return hoursVal;
}
function populateMinutes(date1){
	var html="";
	var dateval=new Date(date1);
	var month=dateval.getMonth()+1;
	var finaldate=dateval.getDate()+"-"+month+"-"+dateval.getFullYear();
	var curmonth=new Date().getMonth()+1;
	var currDate=new Date().getDate()+"-"+curmonth+"-"+new Date().getFullYear();
	var counter=0;
	var getHour=$('#btntime').val();
	var getMin=new Date().getMinutes();
	if(parseInt(getMin)>55 && parseInt(getMin)<=60)
	{
		for(var i=0;i<12;i++){
			if(i==0)
				html=html + "<option value='00'>00</option>";
			else if(i==1)
				html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";			
			else
				html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
		}
	}
	else{
		for(var i=0;i<12;i++){
			if(finaldate==currDate){
				if((i*5)>=parseInt(getMin)){
					if((i*5)<10)
						html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";			
					else
						html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
				}
			}
			else{
				if(i==0)
					html=html + "<option value='00'>00</option>";
				else if(i==1)
					html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";			
				else
					html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
			}
		}
	}
	$('#btntime1').html(html);
}
/*changed by sahana for defect #TEN-139  :ends*/
/*changed by sahana for Req#TJN_243_03:ends*/