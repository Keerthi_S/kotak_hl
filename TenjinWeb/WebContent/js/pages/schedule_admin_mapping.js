/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_admin_mapping.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

/*
 * Added file by sahana for Req#TJN_24_03 on 14/09/2016
 */

$(document).ready(function(){	
	
	var schId1=$('#txtSchId').val();	
	
	$('#lstApplication').change(function(){
		var app = $(this).val();
		clearMessages();
		if(app == '-1'){
			
			$('#lstModules').html("<option value='-1' selected>-- Select One --</option>");
			$('#lstModules').select2('val','-1');
		}else{
			populateModuleInfo(app);
			
		}
	});
	
	$('#btnBack').click(function(){
		
		window.location.href='SchedulerServlet?param=VIEW_SCH_ADMIN&paramval='+schId1;
		
	});
	
	$('#btnSave').click(function(){
		var formVal = validateForm('sch_new_form_map');
		if(formVal != 'SUCCESS'){
			showMessage(formVal,'error');
			return false;
		}
		
		var task=$('#txtTask1').val();
		var json = new Object();
		json.schId1=$('#txtSchId').val();
		json.app = $('#lstApplication').val();
		json.func = $('#lstModules').val();
		if(task=='Extract'){
			json.ext=$('#txtExtFile').val();
			
		}
		else
		{
			json.ext="null";
		}
		
		
		var jsonString = JSON.stringify(json);
		
		$.ajax({
			url:'SchedulerServlet',
			data:{param:"NEW_AD_SCH",json:jsonString},
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'success'){
					showMessage(data.message,'success');
					window.setTimeout(function(){window.location='SchedulerServlet?param=VIEW_SCH_ADMIN&paramval='+schId1;},500)
					
				}else{
					showMessage(data.message,'error');
				}
			},error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
			}
		});
	});
	
});

function populateModuleInfo(appId){
	var jsonObj = fetchModulesForApp(appId);
	
	$('#lstModules').html('');
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html = "<option value='-1'>-- Select One --</option>";
		var jarray = jsonObj.modules;
		for(i=0;i<jarray.length;i++){
			var json = jarray[i];
			
			html = html + "<option value='" + json.code + "'>" + json.code  + " - " + json.name + "</option>";
		}
		
		$('#lstModules').html(html);
		
	}else{
		showMessage(jsonObj.message,'error');
	}
}



