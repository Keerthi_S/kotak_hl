
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  defect_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 15-03-2019			Ashiki				   TENJINCG-986
* 25-03-2019			Pushpalatha		   	   TENJINCG-968
* 10-04-2019            Padmavathi              TNJN27-70
* 28-05-2019		    Ashiki				   V2.8-63
* 30-09-2019			Preeti					For defect posting in JIRA
* 08-12-2020			Pushpalatha				TENJINCG-1220
*/	

/*
 * Added File by sahana for Requirement TJN_24_06
 */

$(document).ready(function(){
	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('def_edit_form');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	/* Added By Ashiki TENJINCG-986 starts */
	$('#dmLoader').hide();
	$('#projects_table').hide();
	$('#accordion').accordion({heightStyle:'content'});
	var projectLists;
	/* Added By Ashiki TENJINCG-986 ends */
	
	$('#txtName').change(function (){
		   var name=$('#txtName').val();
				$.ajax({
					url:'DefectServlet',
					data :{param:"CHECK_NAME",paramval:name},
					async:false,
					dataType:'json',
					success: function(data){
					var status = data.status;
					if(status == 'ERROR'){		
						showMessage(data.message, 'error');
						$("#txtName").focus();
					}
				},
				
				error:function(xhr, textStatus, errorThrown){
					showMessage(errorThrown, 'error');			
				}
			});
			}); 
			
		var json1 = new Object();
		
		json1.name1 = $('#txtName').val();
		json1.tool1 = $('#txtTool').val();
		json1.url1 = $('#txtUrl').val();
		json1.admin1 = $('#txtAdmin').val();
		/*Modified by Preeti for defect posting in JIRA starts*/
		/*json1.password1 = $('#txtPwd').val();*/
		var password1 =  encodeURIComponent($('#txtPwd').val());
		/*Modified by Preeti for defect posting in JIRA ends*/
		var jsonString1 = JSON.stringify(json1);
	
		
	
	$('#btnSave').click(function(){
		
		
		var validated = validateForm('def_edit_form');
		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
		}
		
		var json = new Object();
		
		json.name = $('#txtName').val();
		json.tool = $('#txtTool').val();
		json.url = $('#txtUrl').val();
		json.admin = $('#txtAdmin').val();
		/*Modified by Preeti for defect posting in JIRA starts*/
		/*json.password = $('#txtPwd').val();*/
		var password =  encodeURIComponent($('#txtPwd').val());
		/*Modified by Preeti for defect posting in JIRA ends*/
		json.Severity= $('#svrty').val();
		json.Priority=$('#prty').val();
		/*Added by Pushpalatha for TENJINCG-1220 starts*/
		json.Organization=$('#txtOrg').val();
		/*Added by Pushpalatha for TENJINCG-1220 ends*/
		
		
		var jsonString = JSON.stringify(json);
		var csrf_token=$('#csrftoken_form').val();
		$.ajax({
			type:'post',
			url:'DefectServlet',
			/*Modified by Preeti for defect posting in JIRA starts*/
			/*data:{param:"edit_def",json:jsonString,json1:jsonString1},*/
			data:'param=edit_def&json='+jsonString+'&password='+password+'&json1='+jsonString1+'&password1='+password1+'&csrftoken_form='+csrf_token,
			/*Modified by Preeti for defect posting in JIRA ends*/
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status =='success'){			
					showMessage(data.message,'success');	
					/*Commented by Ashiki for V2.8-63 starts*/
					/*window.setTimeout(function(){window.location='DefectServlet?param=FETCH_ALL';},500)*/
					/*Commented by Ashiki for V2.8-63 end*/
				}else{
					showMessage(data.message, 'error');				
				}
			},
			
			error:function(xhr, textStatus, errorThrown){
				showMessage(data.message, 'error');			
			}
		});
	});
	
	/* Added By Ashiki TENJINCG-986 starts */
	$('#fetchDefectProject').click(function(){	
		$('#dmLoader').show();
		var json = new Object();
		var instance = $('#txtTool').val();
		var name = $('#txtName').val();
		
		$.ajax({

			url:'DefectServlet',
			data :{param:'Fetch_Manager_Projects',instance:name},
			async:true,
			dataType:'json',
			success: function(data){
				var status = data.status;
				projectLists = data.projectLists;
				
				if(status == 'SUCCESS')
				{
					var html = "";
					for(i=0;i<projectLists.length;i++){
						var json = projectLists[i];
						/*Modified by Padmavathi for TNJN27-70 starts*/
						var id=json.pname+":"+json.pid;
						/*html = html + "<tr><td><input class='table-row-selector' type='checkbox' name='"+json+"' value='"+json+"'  id='"+json+"'></input></td><td>"+json+"</td></tr>"*/
							html = html + "<tr><td><input class='table-row-selector' type='checkbox' name='"+json+"' value='"+json+"'  id='"+id+"'></input></td><td>"+json.pname+"</td></tr>"
						/*Modified by Padmavathi for TNJN27-70 ends*/
					}
					$('#dmLoader').hide();
					$('#projects_table').show();
					$('#projectList').html(html);
				}
				
			},

			
		
		});
	});
	
	$('#btnSaveProjects').click(function(){
		
		var count=0;
		var selectedProjects='';
		var subSetName=$('#subSetName').val();
		var instance = $('#txtTool').val();
		var name = $('#txtName').val();
		
		if(subSetName==null){
			var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
			if(!confirmResult) {
				return false;
			}
		}
		if($('#subSetName').val()==null || $('#subSetName').val()==""){
			showMessage("Please enter the Sub Set Name"); 
			return false;
		}
		
		$('#defectManagement-table').find('input[type="checkbox"]:checked').each(function (){
			if(this.id != 'chk_all'){
				selectedProjects = selectedProjects + this.id + ',';
				count++;
			}
			
		});
		if( selectedProjects.charAt(selectedProjects.length-1) == ','){
			selectedProjects = selectedProjects.slice(0,-1);
		}
		if(count===0)
		{
			showMessage("Please select atleast one device ",'error'); 

		}else
		{
			var jsonString = JSON.stringify(projectLists);
			urlLink='param=Persist_Projects&paramval='+ selectedProjects+'&projects='+jsonString+'&subSetName='+subSetName+'&instance='+instance+'&name='+name;
		
		if(count===1 || count>1){
		
		var json = new Object();
		var instance = $('#txtTool').val();
		$.ajax({

			url:'DefectServlet',
			data :urlLink,
			async:true,
			dataType:'json',
			success: function(data){
				var status = data.status;
				var projectLists = data.projectLists;
				var projects="Projects";
				var subSetName=data.subsetName;
				if(status == 'SUCCESS')
				{
					
					window.location.href='DefectServlet?param=VIEW_DEF&paramval='+name;
					
				}
			},
		});
		}
		}
	});
	/* Added By Ashiki TENJINCG-986 ends */
	$('#btnBack').click(function(){	
		
		window.location.href='DefectServlet?param=FETCH_ALL';
	});
	
	
	

	
});