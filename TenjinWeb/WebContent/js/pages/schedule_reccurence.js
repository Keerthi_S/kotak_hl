/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_reccurence.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 *
 */

/*
 * Added file by Leelaprasad for Req#T25IT-91 on 30-08-2017
 */
$(document).ready(function(){
	
	var savedFrequency=$('#savedFrequency').val();
	var savedRecurCycle=$('#savedRecurCycle').val();
	var savedRecurDays=$('#savedRecurDays').val();
	var savedEndDate=$('#savedEndDate').val();
	
	if(savedFrequency=='daily')
	{
		$('#dailyOption').show();
		$('#weeklyOption').hide();
		$('#dailyRepOption').hide();
		$('#end-date').show();
		$('#recurrence').val(savedFrequency);
		$('#numDays').val(savedRecurCycle);
		$('#datepicker1').val(savedEndDate);

	}
	else if(savedFrequency=='weekly'){
		$('#dailyOption').hide();
		$('#weeklyOption').show();
		$('#dailyRepOption').hide();
		$('#end-date').show();
		$('#recurrence').val(savedFrequency);
		$('#datepicker1').val(savedEndDate);
		$('#checks .chk').each(function(){  
			
				if(savedRecurDays.indexOf($(this).val())!='-1'){
					$(this).prop('checked', true);
				}
			
		});
	}
	else if(savedFrequency=='daily-Repetitive'){
		$('#dailyOption').hide();
		$('#weeklyOption').hide();
		$('#dailyRepOption').show();
		$('#end-date').hide();
		$('#recurrence').val(savedFrequency);
		$('#numHrs').val(savedRecurCycle);
	}
	else{
		$('#dailyOption').hide();
		$('#weeklyOption').hide();
		$('#dailyRepOption').hide();
		$('#end-date').hide();
	}
});