/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  metadata_export_table.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/
$(document).ready(function(){
	$('#curpage').val('1');
	paginate();
	
	$('#pages_max_rows').change(function(){
		paginate();
	});
	
	$('#lstApplication').change(function(){
		$('#pages_max_rows').val(10).change();
	});
	
	$('.pagination').click(function(){
		var ms = $('#totpages').text();
		var maxPages = parseInt(ms, 10);
		var curPage = $('#curpage').val();
		curPage = parseInt(curPage,0);
		
		var id=$(this).attr('id');
		
		if(id == 'next'){
			if(curPage < maxPages){
			curPage = curPage + 1;
			}else{
				curPage = maxPages;
			}
		}else if(id == 'prev'){
			if(curPage > 1){
				curPage = curPage -1;
			}else{
				curPage=1;
			}
		}else if(id == 'first'){
			curPage = 1;
		}else if(id == 'last'){
			var rowsToShow = $('#pages_max_rows').val();
			var rowsShown = parseInt(rowsToShow, 10);
			var rowsTotal = $('#tblMetadataModules tbody tr').length;
			var numPages = rowsTotal/rowsShown;
			var numPages2 = numPages.toFixed();
			if(numPages2 < numPages){
				numPages = parseInt(numPages2,10)+1;
			}else{
				numPages = numPages2;
			}
			curPage = numPages;
		}
		
		
		gotoPage(curPage);		
	});
});

function paginate(){
	var rowsToShow = $('#pages_max_rows').val();
	var rowsShown = parseInt(rowsToShow, 10);
	var rowsTotal = $('#tblMetadataModules tbody tr').length;
	var numPages = rowsTotal/rowsShown;
	var numPages2 = numPages.toFixed();
	if(numPages2 < numPages){
		numPages = parseInt(numPages2,10)+1;
	}else{
		numPages = numPages2;
	}
	
	$('#totpages').text(numPages);
	$('#tblMetadataModules tbody tr').hide();
	$('#tblMetadataModules tbody tr').slice(0, rowsShown).show();
	
}

function gotoPage(pageNum){
	var rowsToShow = $('#pages_max_rows').val();
	var rowsShown = parseInt(rowsToShow, 10);
	var startItem = (pageNum-1) * rowsShown;
	var endItem = startItem + rowsShown;
	$('#tblMetadataModules tbody tr').css('opacity','0.0').hide().slice(startItem, endItem).css('display','table-row').animate({opacity:1}, 300);
	/*$('#curpage').text(pageNum);*/
	$('#curpage').val(pageNum);
}
