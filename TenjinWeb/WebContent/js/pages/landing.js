/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  landing.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 12-10-2018             Leelaprasad P           TENJINCG-872-re written
* 17-10-2018             Leelaprasad P           TENJINCG-827
* 19-10-2018           Leelaprasad             TENJINCG-829
* 25-10-2018            Leelaprasad             TENJINCG-826,TENJINCG-828
* 01-12-2018            Padmavathi               for TJNUN262-38 
* 06-12-2018            Leelaprasad             TJNUN262-62
* 12-04-2019           	Padmavathi              TNJN27-21
* 
*/

$(document).ready(function(){
	
	
	var scrStatus = $('#screenstatus').val();
	if(scrStatus == 'ERROR'){
		var msg = $('#scrmessage').val();
		showMessage(msg,'error');
	}else if(scrStatus == 'SUCCESS'){
		var msg = $('#scrmessage').val();
		showMessage(msg,'success');
	}
	
	$('#lstDomain').select2();
	$('#lstProject').select2();
	
	/* Changed by Leelaprasad for TENJINCG-826,TENJINCG-828 ends */
	/*$('#lnkadmin').click(function(){*/
	$('#adminPanel').click(function(){
		/* Changed by Leelaprasad for TENJINCG-826,TENJINCG-828 ends */
		$('#admin-form').submit();
	});
	/*Added by Padmavathi for TJNUN262-38 starts */
	$('#logged-in-user').click(function(){
		$('#param').val('user_view');
		$('#admin-form').submit();
		
	})
	/*Added by Padmavathi for TJNUN262-38 ends */
	
	$('#adminPanel').hover(function () {
                $(this).css({"text-decoration":"underline"});
        });
	 $('#adminPanel').mouseout(function() {
		 $(this).css({"text-decoration":"none"});
		 });
	 
	 $('#logged-in-user').hover(function () {
         $(this).css({"text-decoration":"underline"});
     });
     $('#logged-in-user').mouseout(function() {
	 $(this).css({"text-decoration":"none"});
	 });
	$('#lstDomain').change(function(){
		var selDomain = $(this).val();
		/*Changed by Leelaprasad for the requirement of defect TJNUN262-62 starts*/
		var prj_prefrence='N';
		if($('#rememberPrj').is(':checked') ){
			prj_prefrence='Y';
		}
		/*Changed by leelaprasad for TENJINCG-829 starts*/
		//window.location.href = 'ProjectServlet?param=load_projects&paramval=' + selDomain;
		window.location.href = 'ProjectServletNew?t=load_domain_projects&paramval=' + selDomain+'&prefrence='+prj_prefrence;
		/*Changed by Leelaprasad for the requirement of defect TJNUN262-62 ends*/
		/*Changed by leelaprasad for TENJINCG-829 starts*/
	});
	
	if($('#prjprefer').val()=='Y'){
		$("#rememberPrj").prop('checked', true);
	}
	
	$('#btnReset').click(function(){
		/*Changed by leelaprasad for TENJINCG-829 starts*/
	//window.location.href = 'ProjectServlet?param=load_projects&paramval='
		/*Modified by Padmavathi for TNJN27-21 starts*/
		/*window.location.href = 'ProjectServletNew?t=load_domain_projects';*/
		window.location.href='UserLandingServlet?key=landing';
		/*Modified by Padmavathi for TNJN27-21 ends*/
		/*Changed by leelaprasad for TENJINCG-829 ends*/
		});
	
	
	$('#logout').click(function(){
		window.location.href='TenjinSessionServlet?t=logout';
	});
	/*changed by Leelaprasad for TENJINCG-827 starts*/
	$('#btnLoadProject').click(function(){
		var projectId=$('#lstProject').val();
		var domainName=$('#lstDomain').val();
		
		if(domainName=='-1'|| domainName==null){
			alert("Please select a Domain"); 
			return false;
		}
		if(projectId=='-1'|| projectId==null){
			alert("Please select a Project"); 
			return false;
		}
		/*Changed by leelaprasad for TENJINCG-829 starts*/
		var prj_prefrence='N';
		if($('#rememberPrj').is(':checked') ){
			prj_prefrence='Y';
		}
		/*Changed by leelaprasad for TENJINCG-829 ends*/
		window.location.href='ProjectServletNew?t=project_launch&projectId='+projectId+'&prefrence='+prj_prefrence;
	});
	/*changed by Leelaprasad for TENJINCG-827 ends*/
	
});
