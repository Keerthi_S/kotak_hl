/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  tmuserdetails.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 * DATE			 CHANGED BY 			DESCRIPTION
 * 
 * 24-01-2017	 Manish					TENJINCG-54
 * 10-02-2017	 Manish					TENJINCG-54
 * 04-01-2019	 Ashiki					for TM user credentials
 * 25-03-2019	 Pushpalatha			TENJINCG-968
 */

$(document).ready(function() {
					
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('tm_user_mapping_form');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
					
					$('#lstInstance').change(function(){
						var name=$('#TMUserName').val();
						var instance=$('#lstInstance').val();
						var userId = $('#txtUserId').val();
						$.ajax({
							url:'TestCaseServlet',
							
							data:'param=check_tm_user_name&&instance='+instance+'&userId='+userId,
							async:false,
							dataType:'json',
							success: function(data){
							var status = data.status;
							if(status == 'ERROR'){		
								showMessage(data.message, 'error');
								$("#TMUserName").focus();
							}
							/*changed by manish for defect TENJINCG-109 on 10-02-2017 starts*/
							else{
								clearMessages();
							}
							/*changed by manish for defect TENJINCG-109 on 10-02-2017 ends*/
						},
						
						error:function(xhr, textStatus, errorThrown){
							showMessage(errorThrown, 'error');			
						}
					});
					}); 
					
					

					$('#btnBack')
							.click(
									function() {
										var userId = $('#txtUserId').val();
										window.location.href = 'TestCaseServlet?param=FETCH_TM_USER_CREDENTIALS&userId='
												+ userId;
									});
					
					
					$('#btnSave')
					.click(
							function() {
								
									var formOk = validateForm('tm_user_mapping_form');
								


								 if (formOk != 'SUCCESS') {
									 showMessage(formOk,'error');
								
								}else{
									var json = new Object();
									json.userId = $('#txtUserId').val();
									json.instance = $('#lstInstance').val();
									json.loginId = $('#TMUserName').val();
									/*Added By Ashiki for TM user credentials starts*/
									/*json.loginPwd = $('#TMPassword').val();*/
									var loginPwd = encodeURIComponent($('#TMPassword').val());
									/*Added By Ashiki for TM user credentials ends*/
									var jstring = JSON.stringify(json);
									
									$.ajax({
						    			url:'TestCaseServlet',
						    			/*Added By Ashiki for TM user credentials starts*/
						    			/*data:'param=PERSIST_TM_USER_CREDENTIALS&paramval='+jstring,*/
						    			data:'param=PERSIST_TM_USER_CREDENTIALS&paramval='+jstring+'&loginPwd='+loginPwd,
						    			/*Added By Ashiki for TM user credentials ends*/
						    			async:false,
						    			dataType:'json',
						    			success:function(data){
						    				var status = data.status;
						    				if(status == 'SUCCESS'){			
						    					showMessage(data.message, 'success');
						    					$('#dmLoader').hide();
						    				}else{
						    					showMessage(data.message, 'error');	
						    					$('#dmLoader').hide();
						    				}
						    			},
						    		});
								}
								
								
								
								
							});
					
				});