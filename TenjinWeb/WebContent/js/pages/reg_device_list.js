/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  reg_device_list.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 22-Dec-2017			Sahana					Mobility
 * 25-06-2018			Preeti					T251IT-147
 */	



$(document).ready(function() {
	/*Added by Preeti for T251IT-147 starts*/
	$('#devices-table').dataTable({
		columnDefs:[{width:300, targets:2},
				    {"render":function(data,type,row){
				    	if(data.length < 40){
				    		return data;
				    	}else{
				    		return data.substr(0,40) + "...";
				    	}
				    
				    }, targets:2},
				    {
				    	targets:0,
				    	sortable:false
				    }
				    ],
				    
				    order:[]
	});
	/*Added by Preeti for T251IT-147 ends*/
	$('.main_content').show();
	$('.sub_content').hide();
	/*$('#devices-table').dataTable();*/

	$('#chk_all').click(function(e){	
		if ($("#chk_all").is(':checked')) 
		{	
			$(this).closest('table').find('input[type=checkbox]').prop('checked', this.checked);	
			e.stopPropagation();
		}
		else
		{
			$(this).closest('table').find('input[type=checkbox]').prop('checked',false);	 
			e.stopPropagation();
		}
	}); 

	$('#btnAddDevices').click(function(event) {
		window.location.href='add_devices.jsp';
	});

	$('#btnDelete').click(function(e){
		var selectedDevices='';
		var count=0;
		var urlLink='';
		$('#devices-table').find('input[type="checkbox"]:checked').each(function (){
			if(this.id != 'chk_all'){
				selectedDevices = selectedDevices + this.id + ',';
				count++;
			}
		});
		if(count===0)
		{
			showMessage("Please select atleast one device to delete ",'error'); 

		}
		else
		{
			if(count===1)
			{
				urlLink='param=Delete_Device&paramval='+ selectedDevices.substring(0,selectedDevices.length-1);

			}else if(count>1){
				urlLink='param=Delete_Devices&paramval='+ selectedDevices;
			}
			if(count===1 || count>1)
			{
				if (window.confirm('Are you sure you want to delete?'))
				{
					$.ajax({
						url:'DeviceServlet',
						data :urlLink,
						async:false,
						dataType:'json',
						success: function(data){
							var status = data.status;
							if(status == 'SUCCESS')
							{			
								showMessage(data.message, 'success');	
								window.setTimeout(function(){window.location='DeviceServlet?param=FETCH_ALL_DEVICES';},500)
							}
							else
							{
								showMessage(data.message, 'error');				
							}
						},

					});
				}
			}

		}

	});

});



