/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  api_run_result.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY                	DESCRIPTION
*03-05-2018             new file added from jsp   	for TENJINCG-639
*10-05-2018				Preeti						TENJINCG-666
*17-05-2018				Preeti						TENJINCG-667
*/

$(document).ready(function(){
	var passedTests = $('#passedTests').val();
	var failedTests = $('#failedTests').val();
	var erroredTests = $('#erroredTests').val();
	var notExecutedTests = $('#notExecutedTests').val();
	   /* Changed by Padmavathi for T25IT-128  starts */
	$('#btnClose').click(function(){
        var callback = $('#callback').val();
        /* Changed by Padmavathi for T25IT-199  starts */
        var tsetid=$("#testsetexecutionType").val();
        if(callback == ''){
        if(tsetid =="TSA"){
        	callback = 'tcehistory';
        }
        }
        /* Changed by Padmavathi for T25IT-199  ends */
		if(callback == '')
			{
			callback ='tsehistory';
			}
		if(callback == 'tsehistory'){
			var testSetRecId = $('#tSetRecId').val();
			/*Modified by Preeti for TENJINCG-638 starts*/
			/* window.location.href='ResultsServlet?param=execution_history&recid=' + testSetRecId; */
			window.location.href = 'TestSetServletNew?t=testset_execution_history&recid=' + testSetRecId;
			/*Modified by Preeti for TENJINCG-638 ends*/
		}else if(callback == 'runlist'){
			window.location.href='projectruns.jsp';
		}
		else if(callback=='scheduler'){
			window.location.href="SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id="+$('#prjId').val();
		}
		else if(callback=='tcehistory'){
			var tcrecid = $('#tcRecId1').val();
			/* Modified by Preeti for TENJINCG-611(Test case execution history) starts */
			/* window.location.href='ResultsServlet?param=test_case_execution_history&recid=' + tcrecid; */
			window.location.href='TestCaseServletNew?t=testcase_execution_history&recid=' + tcrecid;
			/* Modified by Preeti for TENJINCG-611(Test case execution history) ends */
		}
});
	/* Changed by Padmavathi for T25IT-128  ends */
	
	//alert(passedTests + "," + failedTests + "," + erroredTests + "," + notExecutedTests);
	var data = [['Passed', Number(passedTests)], ['Failed',Number(failedTests)],['Error',Number(erroredTests)],['Not Executed', Number(notExecutedTests)]];
	var plot1 = jQuery.jqplot ('run-summary-chart', [data],{
		seriesDefaults: {renderer: jQuery.jqplot.PieRenderer,
			rendererOptions:{padding: 5, sliceMargin: 2, showDataLabels: false }
		},
		/* legend: { show:true, location: 'e' }, */
		seriesColors: [ "#27ae60", "#c0392b",  "#f39c12", "#bdc3c7" ],
		height:160,
		width:100,
		/*title:'Test Cases Summary',*/
	});
	
	
	$('.modalmask').click(function(){
		/*$('.modalmask').hide();
		$('.subframe').hide();*/
		closeModal();
	});
	
	/* Added by Preeti for TENJINCG-566 starts */
	$('#btnRun').click(function(){
		var runId = $('#runId').val();
		var testSetRecordId = $('#tSetRecId').val();
		/*Modified by Padmavathi for TENJINCG-653 starts*/
		var callback = $('#callback').val();
		window.location.href='PartialExecutorServlet?param=Re-RunTest&run=' + runId + '&ts=' + testSetRecordId + '&callback='+callback;
		/*Modified by Padmavathi for TENJINCG-653 ends*/
		
	});
	/* Added by Preeti for TENJINCG-566 ends */
	/*Added by Padmavathi for TENJINCG-630 starts*/
	$('#btnMailLog').click(function() {
		clearMessagesOnElement($('#user-message'));
		var runId = $('#runId').val();
		/*Modified by Padmavathi for TENJINCG-653 starts*/
		var callback = $('#callback').val();
		/* window.location.href='ResultsServlet?param=mailLog&runId=' + runId; */
		window.location.href='ResultsServlet?param=mailLog&runId=' + runId+'&callback='+callback;
		/*Modified by Padmavathi for TENJINCG-653 ends*/
	});
	/*Added by Padmavathi for TENJINCG-630 ends*/
	
	
	/*Added by Leelaprasad for TENJINCG-639 starts*/
	
	$('#btnSendMail').click(function(){
		clearMessagesOnElement($('#user-message-users'))
		UncheckAll();
		if($('#prjNotification').val()=='N'){
			showMessage('E-mail notification for this project is disabled, so mail cannot be sent at the moment. Please change the settings and try again.','error');
		}else{
		$('.modalmask').show();
		$('#screenShotOption').hide();
		$('#sendMailManual').show();
		$('#unmapped-users-table').dataTable();
		}
	});
	$('#btnCloseIt').click(function(){
		$('.modalmask').hide();
		$('#sendMailManual').hide();
	});
	
	/*Modified by Preeti for TENJINCG-666 starts*/
	$('#btnSend').click(function(){
		$('#img_mail_loader').show();
		clearMessages();
	
	var runId = $('#runid').val();
	var prjId = $('#prjId').val();
	var selectedUsers = '';
	$('#unmapped-users-table').find('input[type="checkbox"]:checked').each(function () {
	      
	       if(this.id != 'chk_all_users'){
	    	  
	    	  
	    	   selectedUsers = selectedUsers + this.value +';';
	    	   
	       }
	       
	});
	$('#emailLoader').show();
	if(selectedUsers == ''){
		$('#img_mail_loader').hide();
		showMessageOnElement('Please choose at least one user to continue','error',$('#user-message-users'));
		return false;
	}

	$.ajax({
		url:'ProjectMailServlet',
		data:'param=send_mail_manual&run=' + runId + '&project=' + prjId + '&users=' + selectedUsers,
		dataType:'json',
		async:true,
		success:function(data){
			var status = data.status;
			if(status == 'SUCCESS'){
				$('#img_mail_loader').hide();
				clearMessages();
				$('.modalmask').hide();
				$('#sendMailManual').hide();
				showMessage(data.message,'success');
				
			}else{
				clearMessages();
				$('#emailLoader').hide();
				$('.modalmask').hide();
				$('#sendMailManual').hide();
				showMessage(data.message);
			}
		},
		error:function(xhr,textStatus,errorThrown){
			clearMessages();
			showMessage('An unexpected system error occurred. Please contact your System Administrator');
		}
	});
	});
	/*Modified by Preeti for TENJINCG-666 ends*/
	/*Added by Leelaprasad for TENJINCG-639 ends*/
});

$(document).on('click', '.txn-request a', function() {
	var $parentTd = $(this).parent();
	var $parentTr = $parentTd.parent();
	
	var runId = $('#runId').val();
	var tsRecId = $parentTr.data('stepRecId');
	var tduid = $parentTr.data('tduid');
	
	$.ajax({
		url:'ResultsServlet?param=download_api_request&run=' + runId + '&step=' + tsRecId + '&tduid=' + tduid,
		dataType:'json',
		success:function(data) {
			if(data.status.toLowerCase() === 'success') {
				clearMessages();
				var path = data.path;
				var $download = $("<a id='downloadFile' href='"+ path +"' target='_blank' download/>");
				$('body').append($download);
				$("#downloadFile").get(0).click();
				$('#downloadFile').remove();
			}else{
				showMessage(data.message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown) {
			showMessage("An internal error occurred. Please contact Tenjin Support.", 'error');
		}
	});
});

$(document).on('click', '.txn-response a', function() {
	var $parentTd = $(this).parent();
	var $parentTr = $parentTd.parent();
	
	var runId = $('#runId').val();
	var tsRecId = $parentTr.data('stepRecId');
	var tduid = $parentTr.data('tduid');
	
	$.ajax({
		url:'ResultsServlet?param=download_api_response&run=' + runId + '&step=' + tsRecId + '&tduid=' + tduid,
		dataType:'json',
		success:function(data) {
			if(data.status.toLowerCase() === 'success') {
				clearMessages();
				var path = data.path;
				var $download = $("<a id='downloadFile' href='"+ path +"' target='_blank' download/>");
				$('body').append($download);
				$("#downloadFile").get(0).click();
				$('#downloadFile').remove();
			}else{
				showMessage(data.message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown) {
			showMessage("An internal error occurred. Please contact Tenjin Support.", 'error');
		}
	});
});

$(document).on('click','.txn-validations a', function() {
	var $parentTd = $(this).parent();
	var $parentTr = $parentTd.parent();
	
	var runId = $('#runId').val();
	var stepId = $parentTr.data('stepId');
	var ino = $parentTr.data('iteration');
	var tduid= $parentTr.data('tduid');
	var valName ='Validations within Step';
	var caseId = $parentTr.data('testCaseId');
	$('#val-res-sframe').attr('src','ResultsServlet?param=rvd_result_api&stepid=' + stepId + '&ino=' + ino + '&tduid=' + tduid + '&caseid=' + caseId);
	$('.modalmask').show();
	$('.subframe').show();
	/*Added by Preeti for TENJINCG-667 starts*/
	$('#sendMailManual').hide();
	/*Added by Preeti for TENJINCG-667 ends*/
});

//txn-runtimevalues

$(document).on('click','.txn-runtimevalues a', function() {
	var $parentTd = $(this).parent();
	var $parentTr = $parentTd.parent();
	
	var runId = $('#runId').val();
	var stepId = $parentTr.data('stepId');
	var ino = $parentTr.data('iteration');
	var tduid= $parentTr.data('tduid');
	var valName ='Validations within Step';
	var caseId = $parentTr.data('testCaseId');
	$('#val-res-sframe').attr('src','ResultsServlet?param=run_time_values_api&stepid=' + stepId + '&ino=' + ino + '&tduid=' + tduid + '&caseid=' + caseId);
	$('.modalmask').show();
	$('.subframe').show();
	/*Added by Preeti for TENJINCG-667 starts*/
	$('#sendMailManual').hide();
	/*Added by Preeti for TENJINCG-667 ends*/
});

/* $('#report-gen').click(function(){ */
$(document).on('click','#report-gen', function(){
	var runId = $('#runId').val();
	
	$.ajax({
		url:'ResultsServlet',
		data:'param=TEST_REPORT_GEN&runid=' + runId,
		async:false,
		dataType:'json',
		success:function(data){
			var status = data.status;
			if(status == 'SUCCESS'){
				var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
				$("body").append($c);
                $("#downloadFile").get(0).click();
                $c.remove();
			}else{
				var message = data.message;
				showMessage(message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown, 'error');
		}
	});
});


/* $('#report-gen').click(function(){ */
$(document).on('click','#downloadMessages', function(){
	var runId = $('#runId').val();
	
	$.ajax({
		url:'ResultsServlet',
		data:'param=download_all_api_messages&runid=' + runId,
		async:false,
		dataType:'json',
		success:function(data){
			var status = data.status;
			if(status.toLowerCase() === 'success'){
				var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
				$("body").append($c);
                $("#downloadFile").get(0).click();
                //T25IT-238
                $c.remove();
                //T25IT-238 ends
			}else{
				var message = data.message;
				showMessage(message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown, 'error');
		}
	});
});

$(document).on('click','#btnDefect', function(){
	var runId = $('#runId').val();
	var callback = $('#callback').val();
	var tcRecId = $('#tcRecId').val();
	window.location.href = 'TestRunServlet?param=hydrate_run_defects&runId=' + runId + '&callback=' + callback+'&tcRecId='+tcRecId;

});
/*Added by Leelaprasad for TENJINCG-639 starts*/
function UncheckAll(){ 
    var w = document.getElementsByTagName('input'); 
    for(var i = 0; i < w.length; i++){ 
      if(w[i].type=='checkbox'){ 
        w[i].checked = false; 
      }
    }
} 
/*Added by Leelaprasad for TENJINCG-639 starts*/