/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  port_download.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 04-jul-2017		   Pushpalatha G		   Newly  Added for requirement TENJINCG-266
*/

$(document).ready(function(){
	$('#btnCancel').click(function(){
		
	closeModal(window.parent);
	});
	
	$('#btnOk').click(function(){
	
	/*	var checkCount= $("input:checkbox[name=mandatorySelector]:checked").length;
		if(checkCount > 0){
			mode ='M';
		}else{
			mode='A';
		}*/
		
		var templateType = $("input[type=radio]:checked").val();
		
		if(templateType==null){
			showMessage('Please select any one', 'error');
			return false;
		}
		else{
			clearMessages();
		}
		disableFields();
			$('#progress').html("<b>Tenjin is generating your template. Please wait</b>&nbsp <img src='images/inprogress.gif'/>");
	/*		$.ajax({
				url:'LearnerServlet',
				data:'param=DOWNLOAD_DATA_TEMPLATE&m=' + mod + '&a=' + app + '&templatetype='+ templateType + '&type=' + mode+ '&txnMode=' + txnMode,
				async:true,
				dataType:'json',
				success:function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
						$("body").append($c);
						$("#downloadFile").get(0).click();
						$c.remove();
						enableFields();
						$('#progress').html();
						window.parent.closeModal();
					}else{
						var message = data.message;
						showMessage(message,'error');
						enableFields();
						$('#progress').html();
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(errorThrown, 'error');
					enableFields();
					$('#progress').html();
				}
			});*/
			
			$.ajax({
				url:'PortingServlet',
				data:{'param':'port','portData':templateType},
				dataType:'json',
				async:true,
				success:function(data){
					var status = data.status;
					if(status == 'success'){
						//alert(data.filepath);
						var $c = $("<a id='downloadFile' href="+data.filepath+" target='_blank' download />");
						$("body").append($c);
	                    $("#downloadFile").get(0).click();
	                    $c.remove();
	                    enableFields();
	                    $('#progress').html();
	                    closeModal(window.parent);
	                    
					}
					//Changed by Leelaprasad for the requirement Defect fix TEN-35 on 13-12-2016 starts
					else if(status == 'Error'){
						showMessage('Atleast Map one field to do porting', 'error');
						enableFields();
						$('#progress').html();
						
					}
					else if(status == 'serverError'){
						showMessage(data.message,'error');
						enableFields();
						$('#progress').html();
					}
					//Changed by Leelaprasad for the requirement Defect fix TEN-35 on 13-12-2016 ends
					else{
						showMessage(data.message,'error');
						$('#progress').html();
					}
				},
				error:function(xhr, textStatus, errorThrown){	
				
				
					showMessage('An internal error occurred. Please contact your Tenjin Administrator', 'error');
					
					enableFields();
					$('#progress').html();
				}
			});
			
	
	});
	
	$('input[name=templateType]').click(function(){
		if($(this).val() == 'CUSTOM'){
			$('#mandatorySelector').attr('checked',false);
			$('#mandatorySelector').attr('disabled',true);
		}else{
			$('#mandatorySelector').attr('disabled',false);
		}
	});
	
});

function disableFields(){
	$('#mandatorySelector').prop('disabled',true);
	$('#templateTypeInput').prop('disabled',true);
	$('#templateTypeMaster').prop('disabled',true);
	$('#btnOk').prop('disabled',true);
	$('#btnCancel').prop('disabled',true);
}

function enableFields(){
	$('#mandatorySelector').prop('disabled',false);
	$('#templateTypeInput').prop('disabled',false);
	$('#templateTypeMaster').prop('disabled',false);
	$('#btnOk').prop('disabled',false);
	$('#btnCancel').prop('disabled',false);
}


function setParameters(appId, funcCode){
	$('#app').val(appId);
	$('#func').val(funcCode);
}

function closeModal(paraa)
{	
	paraa.$('#ttd-options-frame').attr('src','');
	paraa.$('.modalmask').hide();
	paraa.$('.subframe').hide();
	
	
}