/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  apilist.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY               DESCRIPTION
* 28-03-2017            Bablu kumar             API list screen maintenance
* 01-08-2017            Padmavathi              API story review fixed 
* 01-08-2017            Gangadhar Badagi      	To hide Go button when Application is Select One
* 03-08-2017            Padmavathi            	Tenjincg-327
 *19-08-2017			Manish					T25IT-68
* 23-08-2017			Padmavathi				T25IT-265
* 28-08-2017            Padmavathi              defect T25IT-326
* 13-10-2017			Preeti					TENJINCG-366 
* 16-02-2018			Preeti					TENJINCG-600
* 21-02-2018			Preeti					For disable clear button
*/

$(document).ready(function(){
	/*Added by Preeti for TENJINCG-600 starts*/
	var appId=$('#lstAppId').val();
	populateAutGroups(appId);
	/*Added by Preeti for TENJINCG-600 ends*/
	$('#btnNewApi').click(function(event) {

		/*$('.main_content').hide();
		$('.ifr_Main').attr('src', 'apidetails.jsp');
		$('.sub_content').show();*/
		
		window.location.href = 'APIServlet?param=new_api_aut';
	});
	
	$('#chk_all').click(function(e){	
		   if ($("#chk_all").is(':checked')) 
		   {	
		     $(this).closest('table').find('input[type=checkbox]').prop('checked', this.checked);	
		     e.stopPropagation();
		   }
		   else
		    {
			 $(this).closest('table').find('input[type=checkbox]').prop('checked',false);	 
			 e.stopPropagation();
		    }
		}); 
	$('#btnDelete').click(function(){
	/*Added By padmavathi for T25IT-326 starts*/
		clearMessagesOnElement($('#user-message'));
		/*Added By padmavathi for T25IT-326 ends*/
		 var count=0;
		 /*Added by padmavathi for Api story review fixes on 01-08-2017 starts*/
		 var appId=$("#lstAppId").val();
		 /*added by Preeti for TENJINCG-366 starts*/
		 var apiType= $('#apiType').val();
		 /*added by Preeti for TENJINCG-366 ends*/
		 /*Added by Preeti for TENJINCG-600 starts*/
		 var apiGroup = $('#apiGroup').val();
		 /*Added by Preeti for TENJINCG-600 ends*/
		 /*Added by padmavathi for Api story review fixes on 01-08-2017 ends*/
		 /*16-Mar-2015 R2.1: Defect #1132: adding urllink variable: Starts*/
		  var urllink;
		  /*16-Mar-2015 R2.1: Defect #1132: adding urllink variable: Ends*/
		/*  modified by shruthi for sanity test satrts*/
		 /*$("#chk_user:checked").each(function () {
			 count++;
	      });*/
		 jQuery("input[name='chk_user']"+":"+"checked").each(function() {
			  count++;
			});
			/*  modified by shruthi for sanity test ends*/
		 /*16-Mar-2015 R2.1: Defect #1132: Delete click show below message if no checkbox is selected: Starts*/
		 if (count==0) {
			 /*Changed by Padmavathi for TENJINCG-327 starts*/
			 /*showMessage("Please select the Function Id to Delete", 'error');*/	
			 showMessage("Please select at least one API to delete", 'error');
			 /*Changed by Padmavathi for TENJINCG-327 ends*/
		 } 
		//if ($("#chk_all").is(':checked') || count>0) {
		 else if ($("#chk_all").is(':checked') || count>0) {
			/*16-Mar-2015 R2.1: Defect #1132: Delete click show below message if no checkbox is selected: Ends*/
			/* var i=0;
			 var val = [];
			 $("#chk_user:checked").each(function () {
				 var tmp = $(this).val();
				 val[i] = tmp;
				 i++;
			    });*/
			 
			
			  var apiCode = JSON.stringify(selectedItems);
	          var apiCodes = JSON.parse(apiCode);
	          /*changed by manish for bug T25IT-68 on 19-Aug-2017 starts*/
	          /*urllink='param=delete_api&apiCodes='+ apiCodes;*/
	         urllink='param=delete_api&apiCodes='+ apiCodes+'&appId='+appId;
	         /*changed by manish for bug T25IT-68 on 19-Aug-2017 ends*/
	       /*  changed by parveen for defect  #580 starts*/
	         //window.confirm('Are you sure you want to delete?')
	         /*  changed by parveen for defect  #580 ends*/
	         
		   }
		/*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if one checkbox is selected: Starts*/
		  else if ($("#chk_all").is(':checked') || count==1) {
					/*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if one checkbox is selected: Ends*/
			var checkedvalue=$('input:checkbox:checked').val();
			/*changed by manish for bug T25IT-68 on 19-Aug-2017 starts*/
			/*urllink='param=delete_api&paramval='+ checkedvalue;*/
			urllink='param=delete_api&paramval='+ checkedvalue+'&appId='+appId;
			/*changed by manish for bug T25IT-68 on 19-Aug-2017 ends*/
			
			 /*  changed by parveen for defect  #580 starts*/
			//window.confirm('Are you sure you want to delete?')
			 /*  changed by parveen for defect  #580 ends*/
			}
		 /*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if one checkbox is selected: Starts*/
		 if(count==1 || count>1){
			 /*Changed by padmavathi for T25IT-265 starts*/
			 if(window.confirm('Are you sure you want to delete?')){
				 /*Changed by padmavathi for T25IT-265 starts*/
		/*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if one checkbox is selected: Ends*/
		  $.ajax({
			url:'APIServlet',
			data :urllink,
			async:false,
			dataType:'json',
			success: function(data){
			var status = data.status;
			if(status == 'SUCCESS')
			  {			
				showMessage(data.message, 'success');	
				 /*window.setTimeout(function(){window.location='APIServlet?param=fetch_all_apis';},500);*/
				/*Modified by Preeti for TENJINCG-600 starts*/
				/*Changed by padmavathi for Api story review fixes on 01-08-2017 starts*/
				/*modified by Preeti for TENJINCG-366 starts*/
				/*window.setTimeout(function(){window.location=' APIServlet?param=show_api_list&app='+appId;},500);*/
				/*if(apiType!= '-1')
				{
					window.setTimeout(function(){window.location=' APIServlet?param=show_apiType_list&app='+appId+'&apiType=' +apiType;},500);
				}
				else
				{
					window.setTimeout(function(){window.location=' APIServlet?param=show_api_list&app='+appId;},500);
				}*/
				
				/*modified by Preeti for TENJINCG-366 ends*/
				window.setTimeout(function(){window.location='APIServlet?param=show_filter_api_list&app=' +appId + '&apiGroup=' +apiGroup;},3000);
				/*Changed by padmavathi for Api story review fixes on 01-08-2017 ends*/
			  	/*Modified by Preeti for TENJINCG-600 ends*/
			  }
			  else
			  {
				showMessage(data.message, 'error');				
			  }
		    },
		
		   error:function(xhr, textStatus, errorThrown){
			   showMessage(data.message, 'error');			
		  }
	     });
		/*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if one checkbox is selected: Starts*/
		 }
		 }
		/*16-Mar-2015 R2.1: Defect #1142: Delete click show below message if one checkbox is selected: Ends*/
	    }
	
	 /*  changed by parveen for defect  #580 starts*/
	  /*}*/
	 /*  changed by parveen for defect  #580 ends*/

	);	
	/*Commented by Preeti for TENJINCG-600 starts*/
	/*modified by Preeti for TENJINCG-366 starts*/
	/*$('#btnGo').click(function() {
		var appId = $('#lstAppId').val();
		window.location.href='APIServlet?param=show_api_list&app=' + appId;
		added by Preeti for TENJINCG-366 starts
		var apiType= $('#apiType').val();
		window.location.href='APIServlet?param=show_api_list&app=' +appId + '&apiType=' +apiType;
		added by Preeti for TENJINCG-366 ends
	});*/
	/*modified by Preeti for TENJINCG-366 ends*/
	/*
	$('#lstAppId').change(function(){
		var selapp = $(this).val();
		if(selapp!= '-1'){
			$('#lstSelAut').val(selapp);
			$('#btnGo').show();
		}*/
		
		/*added by Preeti for TENJINCG-366 starts
		$('#btnClear').click(function() {
			var appId = $('#lstAppId').val();
			window.location.href='APIServlet?param=show_api_list&app=' + appId;
		});
	
		$('#lstAppId').change(function(){
			var appId = $('#lstAppId').val();
			window.location.href='APIServlet?param=show_api_list&app=' + appId;
			$('#apiType').val(-1);
		
		$('#btnGo').hide();
	});
	$('#btnClear').hide();
	var apiType= $('#apiType').val();
	if(apiType!='-1'){
		$('#btnClear').show();
	}
	$('#apiType').change(function(){
		var appId = $('#lstAppId').val();
		var apiType= $('#apiType').val();
		if(appId!='-1')
			{
			if(apiType!= '-1')
				{
					window.location.href='APIServlet?param=show_apiType_list&app=' +appId + '&apiType=' +apiType;
				}
			else
				{
					window.location.href='APIServlet?param=show_api_list&app=' + appId;
					$('#btnClear').hide();
				}
			}
		var seltype1= $('#lstAppId').val();
		if(seltype!= '-1' && seltype1!= '-1'){
			$('#btnGo').show();
		}		
		Added by Gangadhar Badagi to hide Go button when Application is Select One starts
		else{
			$('#btnGo').hide();
		}
		Added by Gangadhar Badagi to hide Go button when Application is Select One ends
	});
	added by Preeti for TENJINCG-366 ends*/
	/*Commented by Preeti for TENJINCG-600 ends*/
	
	/*Added By Preeti for TENJINCG-600 starts*/	
	$('#btnGo').click(function() {
		var appId = $('#lstAppId').val();
		var apiGroup = $('#apiGroup').val();
		/*Added By shruthi for Tnj210-43 starts*/	
		if(appId==-1)
		{
		showLocalizedMessage('Please select application.', 'error');
		return false;
		}
		/*Added By shruthi for Tnj210-43 ends*/	
		window.location.href='APIServlet?param=show_filter_api_list&app=' +appId + '&apiGroup=' +apiGroup+ '&apiType=ALL';
	});
	
	$('#lstAppId').change(function(){
		var selapp = $(this).val();
		$('#selApiGroup').val("ALL");
		$('#apiType').val("ALL");
		$('#apiType').prop('disabled',true);
		/*Added by Preeti to disable clear button starts*/
		$('#btnClear').prop('disabled',true);
		/*Added by Preeti to disable clear button ends*/
		populateAutGroups(selapp);
		if(selapp!= '-1'){
			$('#lstSelAut').val(selapp);
		}
	});
	$('#apiGroup').change(function(){
		var selgrp = $('#apiGroup').val();
		$('#apiType').val("ALL");
		$('#apiType').prop('disabled',true);
		/*Added by Preeti to disable clear button starts*/
		$('#btnClear').prop('disabled',true);
		/*Added by Preeti to disable clear button ends*/
		if(selgrp != 'ALL'){
			$('#selApiGroup').val(selgrp);
		}
	});
	
	$('#apiType').change(function(){
		var appId = $('#lstAppId').val();
		var apiType= $('#apiType').val();
		var selgrp = $('#apiGroup').val();
		if(appId!='-1')
			{
				window.location.href='APIServlet?param=show_filter_api_list&app=' +appId + '&apiType=' +apiType + '&apiGroup=' +selgrp;
			}
	});
	$('#btnClear').click(function() {
		var appId = $('#lstAppId').val();
		var selgrp = $('#apiGroup').val();
		window.location.href='APIServlet?param=show_filter_api_list&app=' +appId + '&apiType=ALL' + '&apiGroup=' +selgrp;
	});
	/*Added By Preeti for TENJINCG-600 ends*/	
	
	/*Added by Ashiki for TENJINCG-1213 starts*/
	$(document).on('click', '#btnLearn', function() {
		var appId = $('#lstAppId').val();
		var apiGroup = $('#apiGroup').val();
		 
		 var selectedApis = '';
		 var selectedApiType='';
		 $('#tblNaviflowModules').find('input[type="checkbox"]:checked').each(function () {
			 if(!$(this).hasClass('tbl-select-all-rows')) {
				 selectedApis += $(this).attr('id') + ',';
				 selectedApiType += $(this).attr('apitype') + ',';
			 }
		 });
		 
		 if( selectedApis.charAt(selectedApis.length-1) == ','){
			 selectedApis = selectedApis.slice(0,-1);	
		 }
		 
		 if(selectedApis.length < 1) {
			 showMessage('No Operations selected. Please select operations before you learn', 'error');
			 return false;
		 }
		 
		 var $hidden = $("<input type='hidden' name='apiList' value='" + selectedApis + "' />");
		 var $hidden1 = $("<input type='hidden' name='appId' value='" + appId + "' />");
		 var $hiddenReqType = $('#requesttype');
		 $hiddenReqType.val('init_Api_learn');
		 var $form = $('#main_form');
		 
		 $form.attr('action','ApiLearnerServlet');
		 $form.attr('method','POST');
		 
		 $form.append($hidden);
		 $form.append($hidden1);
		 $form.append($hiddenReqType);
		 $form.append($("<input type = 'hidden' id='csrftoken_form' name ='csrftoken_form' value='"+$('#csrftoken_form').val()+"'/>"));
		 
		 $form.submit();
		 
			
	 });
	 /*Added by Ashiki for TENJINCG-1213 ends*/
});

/*$(document).ready(function(){
	
	getallAPIs();		
	$('#project-api-table').dataTable();
	
	$('#lstAppId').change(function(){
        var selapp = $(this).val();
        if(selapp!= '-1'){
              $('#lstSelAut').val(selapp);
              $('#btnGo').show();
        }
  });
	
});*/

function getallAPIs(){
	
	$.ajax({
		url:'APIServlet',
		data:'param=APIServlet?param=FETCH_ALL_APIS',
		dataType:'json',
		async:false,
		success:function(data){
			var status = data.status;
			if(status == 'SUCCESS'){
				var html = '';
				var dArray = data.auts;
				for(var i=0;i<dArray.length;i++){
					var api = dArray[i];
					html = html + '<tr>';
					html = html + "<td><input type='checkbox' id='" +api.appid + "'></td>";
					html = html + "<td>" +Utilities.escapeXml(api.apicode) + "</td>";
					html = html + "<td>" + Utilities.escapeXml(api.apiname) + "</td>";
					html = html + "<td>" + Utilities.escapeXml(api.apitype) + "</td>";
					html = html + "<td>" + Utilities.escapeXml(api.apiurl) + "</td>";	
					html = html + "</tr>";
				}
				
				var $table = $('#project-api-table').DataTable();
				$table.destroy();
				$('#project-api-table tbody').html(html);
				$('#project-api-table').dataTable();
				
				
			}else{
				showMessage(data.message, 'error');				
			}
		},
		error:function(xhr, textStatus, errorThrown){
			//showMessageOnElement(data.message, 'error');			
		}
	});
	
}

$('#chk_all').click(function(e){	
	   if ($("#chk_all").is(':checked')) 
	   {	
	     $(this).closest('table').find('input[type=checkbox]').prop('checked', this.checked);	
	     e.stopPropagation();
	   }
	   else
	    {
		 $(this).closest('table').find('input[type=checkbox]').prop('checked',false);	 
		 e.stopPropagation();
	    }
	}); 
	
/*Added By Preeti for TENJINCG-600 starts*/
function populateAutGroups(appId){
	var jsonObj = fetchAutGroups(appId);
		var currentgroup=$('#selApiGroup').val();
		$('#apiGroup').html('');
		var status = jsonObj.status;
		if(status== 'SUCCESS'){
			var html = "<option value='ALL'>-- Select One --</option>";
			var jarray = jsonObj.GROUPS;
			if(jarray == undefined){
				
			}else{
			for(i=0;i<jarray.length;i++){
				var json = jarray[i];
					 if(json != currentgroup)
						 html = html + "<option value='" + escapeHtmlEntities(json) + "'>" + escapeHtmlEntities(json)  + "</option>";
					 else
						 html = html + "<option value='"+escapeHtmlEntities(currentgroup)+"' selected>"+escapeHtmlEntities(currentgroup)+"</option>";
				}
			}
			$('#apiGroup').html(html);
		}else{
			showMessage(jsonObj.message,'error');
		}
	}
/*Added By Preeti for TENJINCG-600 ends*/	