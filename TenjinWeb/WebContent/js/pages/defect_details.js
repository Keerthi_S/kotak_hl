/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Defect.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION

* 10-Jan-2017			Manish					TENJINCG-5
* 19-02-2019           Padmavathi              TENJINCG-924
* 25-09-2020            shruthi                  TENJINCG-1224
* 19-11-2020           Priyanka                  TENJINCG-1231
*/

var defectPosted;
var storedFiles = [];
var removedFiles = [];
$(document).ready(function(){
	 /*  Added by Priyanka for TENJINCG-1231 starts */
	$('#new_defect_form').on('submit', handleUpdateForm);
	$('select.stdTextBoxNew').select2();	const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
	var current_datetime = new Date();
	var formatted = current_datetime.getDate() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getFullYear();
	   var projectEndDate=$('#endDate').val();   
		  var tdyDate = Date.parse(formatted);
		  var prjEndDate  = Date.parse(projectEndDate);
		  if(prjEndDate>=tdyDate){
			  
			  $('#btnBack').show();
			  $('#btnSave').show();
			  $('#btnDelete').show(); 
		  }
		  else{
			  $('#btnBack').show();
			  $('#btnSave').hide();
			  $('#btnDelete').hide();
			  
		  }
		  /*  Added by Priyanka for TENJINCG-1231 ends */
	$('#new_defect_form').on('submit', handleUpdateForm);
	$('select.stdTextBoxNew').select2();
	$('#txtFile').on("change", handleFileSelect);
	selDiv = $('#selectedFiles');
	$('body').on('click','.selFile', removeFile);
	var status = $('#status').val();
	var message = $('#message').val();
	/*added by shruthi for TENJINCG-1224 starts*/
	var runstatus = $('#runstatus').val();
	/*added by shruthi for TENJINCG-1224 ends*/
	
	if(status == 'SUCCESS'){
		if(message != ''){
			showMessage(message, 'success');
		}
	}else if(status =='ERROR' || status == 'error' || status == 'Error'){
		showMessage(message,'error');
	}
	
	defectPosted = $('#defPosted').val();
	//alert('Defect Posted --> ' + defectPosted);
	if(defectPosted == 'n' || defectPosted == 'no' || defectPosted == 'NO' || defectPosted=='No' || defectPosted == 'N'){
		var appId = $('#appid').val();
		//alert(appId);
		clearDTTInformation();
		populateDTTInformationWithDefaults(appId, $('#selSeverity').val(), $('#selPriority').val());
	}else if(defectPosted == 'y' || defectPosted == 'yes' || defectPosted == 'YES' || defectPosted=='Yes' || defectPosted == 'Y'){
		//clearDTTInformation();
		$('#dttname').val($('#selDttInstance').val());
		$('#dttproject').val($('#selDttProject').val());
	}
	
	if(defectPosted == 'y' || defectPosted == 'yes' || defectPosted == 'YES' || defectPosted=='Yes' || defectPosted == 'Y'){
		$('#txtSummary').prop('disabled',true);
		$('#description').prop('disabled',true);
		$('#action').prop('disabled',true);
	}
	/*added by shruthi for TENJINCG-1224 starts*/
	if(runstatus=="PURGED"){
		$('.show-for-all').show();
		$('#runstatus').show();
		}
	else{
		$('.show-for-all').hide();
		$('#runstatus').hide();
		}
	/*added by shruthi for TENJINCG-1224 ends*/
	
	var selStatus = $('#selStatus').val();
	$('#lstStatus').val(selStatus).change();
	/*alert($('#lstStatus').children('option').length);
	$('#lstStatus').children('option').each(function(){
		var val = $(this).val();
		alert(val);
		if(val == selStatus){
			alert('setting selected true');
			$(this).attr({'selected':true});
		}
	});*/
	
	$('.att_download').click(function(){
		var recid = $(this).attr('recid');
		
		$.ajax({
			url:'DefectServlet?param=download_attachment&id=' + recid,
			dataType:'json',
			async:true,
			success:function(data){
				if(data.status == 'success'){
					var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
					$("body").append($c);
					$("#downloadFile").get(0).click();
					$c.remove();
				}else{
					showMessage(data.message,'error');
				}
			},
			error:function(xhr){
				showMessage("An Internal Error occurred. Please contact Tenjin Support.", 'error');
			}
		});
	});
	
	$('#btnSave').click(function(){
		var formOk = validateForm('new_defect_form');
		if (formOk != 'SUCCESS') {
			 showMessage(formOk,'error');
			 return false;
		}else{
			//$('#post_defect').val('N');
			buildJsonForUpdate();
		}
	});
	
	/*$('#btnSavePost').click(function(){
		var formOk = validateForm('new_defect_form');
		if (formOk != 'SUCCESS') {
			 showMessage(formOk,'error');
			 return false;
		}else{
			$('#post_defect').val('Y');
			buildJsonForUpdate();
		}
	});*/
	
	$('#action').change(function(){
		if($(this).val() == 'POST'){
			$('#post_defect').val('Y');
		}else{
			$('#post_defect').val('N');
		}
	});
	
	$('body').on('click','#btnPClose', function(){
		var callback = $(this).attr('callback');
		if(callback === 'undefined'){
			$('.modalmask').hide();
			$('#processing_icon').html("");
			$('#def_review_message').html('')
			$('#def_review_progress_dialog').hide();
		}else{
			window.location.href=callback;
		}
	});
	
	$('#btnBack').click(function(){
		var callback = $('#callback').val();
		if(callback === 'none'){
			callback = 'projectdefectlist';
		}
		if(callback === 'rundefectlist'){
			window.location.href='TestRunServlet?param=hydrate_run_defects&runId=' + $("#runid").val();
		}else if(callback ==='projectdefectlist'){
			window.parent.triggerDefectSearch();
			window.location.href='DefectServlet?param=fetch_project_defects&pid=' + $('#pid').val();
		}
		/* Added by Padmavathi for TENJINCG-924 starts */
		else if(callback ==='project_dashboard'){
			window.location.href='ProjectDashBoardServlet?t=project_dashboard&paramval=' + $('#pid').val();
		}
		/* Added by Padmavathi for TENJINCG-924 ends */
	});
	/*changed by manish for req TENJINCG-5 on 11-Jan-2017 starts*/
	
	$('#btnDelete').click(function(){
		
		deleteDefect();
	});
	
	/*changed by manish for req TENJINCG-5 on 11-Jan-2017 ends*/
	
	$('#btnList').click(function(){
		showLinkageDialog();
	});
	
	$('#btnLClose').click(function(){
		hideLinkageDialog();
	});
});

function showLinkageDialog(){
	$('.modalmask').show();
	$('#def_linkage_dialog').show();
}

function hideLinkageDialog(){
	$('#def_linkage_dialog').hide();
	$('.modalmask').hide();
}
/*changed by manish for req TENJINCG-5 on 11-Jan-2017 starts*/

 function deleteDefect(){
	var recordId = $('#recordId').val();
	var prjId = $('#pid').val();
	var callback="deletion";
	$.ajax({
		url:'DefectServlet?param=inactive_multiple_defects&dids=' + recordId+'&prjId='+prjId,
		async:true,
		dataType:'json',
		success:function(r){
			console.log(r);
			if(r.status == 'success'){
				window.location.href='DefectServlet?param=fetch_project_defects&pid='+prjId+'&callback='+callback;
				//showMessage(r.message,'success');
				/*location.reload();*/
			}else{
				window.location.href='DefectServlet?param=fetch_project_defects&pid='+prjId+'&callback='+callback;
				//showMessage(r.message,'error');
				/*location.reload();*/
			}
		},
		error:function(r){
			showMessage(r.message,'error');
		}
		
	});
 }


/*changed by manish for req TENJINCG-5 on 11-Jan-2017 ends*/