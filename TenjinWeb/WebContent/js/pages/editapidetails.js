

/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  editapidetails.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
*  DATE                  CHANGED BY               DESCRIPTION
*  31-07-2017            Gangadhar Badagi         TENJINCG-320
*  08-Aug-2017			 Roshni					  T25IT-44
*  10-Aug-2017			 Manish					  T25IT-39
*  10-Aug-2017			 Sriram					  T25IT-76
*  28-08-2017            Padmavathi               defect T25IT-326
*  13-10-2017			 Preeti					  TENJINCG-366 
*  02-02-2018			 Pushpalatha			  TENJINCG-568
*  16-02-2018			 Preeti					  TENJINCG-600
*  25-06-2018            Padmavathi               T251IT-127
*  25-03-2019			 Pushpalatha	          TENJINCG-968
*  10-04-2019			 Preeti					  TENJINCG-1026
*/	


$(document).ready(function(){
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('api_details_edit_form');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	/*Added by Pushpalatha for TENJINCG-568 starts*/
	var appId=$('#lstAppId1').val();
	var grpName=$('#group').val();
	populateAutGrooups(appId,grpName);
	/*Added by Pushpalatha for TENJINCG-568 ends*/
	
	/* $('#apiOperationsTable').dataTable();*/
	
	 
	 var status = $('#scrStatus').val();
	 if(status.toLowerCase() === 'success') {
		 showMessage($('#scrMessage').val(), 'success');
	 } else if(status.toLowerCase() === 'error') {
		 showMessage($('#scrMessage').val(), 'error');
	 }
	 
	 var selType = $('#selType').val();
	 $('#txtApiType').children('option').each(function(){
		var val = $(this).val();
		if(val == selType){
			$(this).attr({'selected':true});
		}
	});
	 
	 //Fix for T25IT-76
	 /*Modified by Preeti for TENJINCG-1026 starts*/
	 /*if(selType.toLowerCase().startsWith('rest')) {*/
	 if(!selType.indexOf('rest')) {
	 /*Modified by Preeti for TENJINCG-1026 ends*/
		 $('#btnRefreshOperations').hide();
	 }else{
		 $('#btnRefreshOperations').show();
	 }
	//Fix for T25IT-76 ends
	 
	 var json1 = new Object();
	 
	 $('#btnRefreshOperations').click(function() {
	 /*Added By padmavathi for T25IT-326 starts*/
		 clearMessagesOnElement($('#user-message'));
		 /*Added By padmavathi for T25IT-326 ends*/
		 var urlValid = $('#isUrlValid').val();
		 if(urlValid === false || urlValid === 'false') {
			 showMessage('Cannot refresh operations. URL is invalid. Please correct the URL and try again.', 'error');
			 return false;
		 }
		 
	 	$('.modalmask').show();
		$('#confirmation_dialog').show();
	 });
	 
	 $('#btnCOk').click(function() {
		$('#confirmation_dialog').hide();
		$('#progress_dialog').show();
		$('#processing_icon').html("<img src='images/loading.gif' alt='In Progress..' />");
		$('#progressBar').show();
		$('#message').html('<p>Tenjin is refreshing the API Operations. Please Wait...</p>');
		refreshOperations();
	 });
	 
	 $('#btnCCancel').click(function(){
			$('.modalmask').hide();
			$('#confirmation_dialog').hide();
		});
		
		$('#btnPClose').click(function(){
			$('.modalmask').hide();
			$('#progress_dialog').hide();
			var apiCode = $('#txtApiCode').val();
			var appId = $('#lstLrnrApplication').val();;
			window.location.href = 'APIServlet?param=fetch_api&paramval=' + apiCode + '&appid=' + appId;
		});
		/*Added by Gangadhar Badagi for TENJINCG-320 starts*/
		var selectedApp = $('#lstLrnrApplication').val();
		$('#btnApiLearningHistory').click(function(){
			var apiCode = $('#txtApiCode').val();
			var apiName = $('#txtApiName').val();
			window.location.href = 'APIServlet?param=api_learning_history&appId=' + selectedApp + '&apiCode=' + apiCode+'&apiName=' + apiName;
		});
		/*Added by Gangadhar Badagi for TENJINCG-320 ends*/
	 $('#btnSave').click(function(){
		 /* Modified by Padmavathi for T251IT-127 starts*/
		 var name=$('#txtApiName').val();
			var url=$('#txtApiUrl').val();
			if(name==null || name==""){
				showMessage("API Name is mandatory", 'error');
				return false;
			}
			if(url==null || url==""){
				showMessage("URL is mandatory", 'error');
				return false;
			}
			var json = new Object();
			json.apiname = $('#txtApiName').val();
			json.apiurl = $('#txtApiUrl').val();
			json.apicode = $('#txtApiCode').val();
			json.appId=$('#lstAppId1').val();
			json.gropuName=$('#group').val();
			json.encryptionKey=$('#encryptionKey').val();
			json.csrftoken_form=$('#csrftoken_form').val();
		//json.appid=api.getApplicationId();
			
			var jstring = JSON.stringify(json);
			$.ajax({
				url:'APIServlet',
				data:{param: "edit_api",json:jstring},
				async:false,
				dataType:'json',
				success:function(data){
					var status = data.status;
					if(status == 'SUCCESS'){			
						showMessage(data.message, 'success');				
					}else{
						showMessage(data.message, 'error');				
					}
				},
				
				error:function(xhr, textStatus, errorThrown){
					showMessage('An internal error occurred. Please contact Tenjin Support.','error');		
				}
			});
			/* Modified by Padmavathi for T251IT-127 ends*/
		});
	/* Commented by Padmavathi for duplicate validation starts*/
		/*Added by Roshni for T25IT-44 starts*/
		/*$('#btnSave').click(function(){
			var name=$('#txtApiName').val();
			var url=$('#txtApiUrl').val();
			if(name==null || name==""){
				showMessage("API Name is mandatory", 'error');
				return false;
			}
			if(url==null || url==""){
				showMessage("URL is mandatory", 'error');
				return false;
			}
		});*/
		/*Added by Roshni for T25IT-44 ends*/
	 /* Commented by Padmavathi for duplicate validation ends*/
	 $('#btnBack').click(function(){	
		 	var appId = $('#lstLrnrApplication').val();
		 	/*modified by Preeti for TENJINCG-366 starts*/ 
		 	var apiType= $('#apiFilterType').val();
		 	/*Modified by Preeti for TENJINCG-600 starts*/
		 	var apiGroup = $('#apiGroup').val();
		 	/*if(apiType!= 'null')
			{
		 		window.location.href='APIServlet?param=show_apiType_list&app='+appId+'&apiType=' +apiType;
			}
			else
			{
				window.location.href='APIServlet?param=show_api_list&app='+appId;
			}*/
		 	window.location.href='APIServlet?param=show_filter_api_list&app=' +appId + '&apiGroup=' +apiGroup;	
		 	/*Modified by Preeti for TENJINCG-600 ends*/
		 	/*modified by Preeti for TENJINCG-366 ends*/ 
		});
	 
	 
	 $('#btnLearn').click(function () {
		 
		 /*var url = $('#txtApiUrl').val();
		 if(url === '' || url == '' || url === undefined) {
			 showMessage('Please specify API URL')
		 }*/
		
		 /*var urlValid = $('#isUrlValid').val();
		 if(urlValid === false || urlValid === 'false') {
			 showMessage('Cannot learn this API. URL is invalid. Please correct the URL and try again.', 'error');
			 return false;
		 }*/
		 
		 var selectedOps = '';
		 $('#tblApiOperations').find('input[type="checkbox"]:checked').each(function () {
			 if(!$(this).hasClass('tbl-select-all-rows')) {
				 selectedOps += $(this).attr('id') + ',';
			 }
		 });
		 
		 if( selectedOps.charAt(selectedOps.length-1) == ','){
			 selectedOps = selectedOps.slice(0,-1);
		 }
		 
		 if(selectedOps.length < 1) {
			 showMessage('No Operations selected. Please select operations before you learn', 'error');
			 return false;
		 }
		 
		 var $hidden = $("<input type='hidden' name='operationsList' value='" + selectedOps + "' />");
		 var csrftoken = $('#csrftoken_form');
		 var $hidden1 = $("<input type = 'hidden' name ='csrftoken_form' id ='csrftoken_form' value = '" + csrftoken+"' />");
		 var $hiddenReqType = /*$("<input type='hidden' name='requesttype' value='initlearner'/>");*/  $('#requesttype');
		 $hiddenReqType.val('initlearner');
		 var $form = $('#api_details_edit_form');
		 
		 $form.attr('action','ApiLearnerServlet');
		 $form.attr('method','POST');
		 
		 $form.append($hidden);
		 $form.append($hiddenReqType);
		 $form.append($hidden1);
		 
		 $form.submit();
		
	 });
	 
	 $(document).on('click','.template-gen',function(e){
		var mod = $(this).attr('mod');
		var app = $(this).attr('app');
		var txnMode=$(this).attr('txnMode');
		var operation = $(this).attr('operation');
		$('#ttd-options-frame').attr('src','ttd_download_options.jsp?a=' + app + '&m=' +mod + '&t=' + txnMode + '&operation=' + operation);
    	$('.modalmask').show();
    	$('#ttd_download_options').show();
	});
	 
	 $(document).on('click','.edit-descriptors',function(e){
		var app = $(this).data('appId');
		var api = $(this).data('apiCode');
		var op = $(this).data('operationName');
		var type = $(this).data('descType');
		
		$('#ttd-options-frame').attr('src','edit_api_descriptor.jsp?app=' + app + '&api=' +api + '&op=' + op + '&type=' + type);
    	$('.modalmask').show();
    	$('#ttd_download_options').show();
	});
	 $(document).on('click','.edit-operation',function(e){
			var appid = $(this).data('appId');
			var code = $(this).data('apiCode');
			var optName = $(this).data('operationName');
			var apiType=$("#selType").val();
			window.location.href='APIServlet?param=new_rest_operation&appid=' + appid +'&code='+ code +'&optName='+ optName+'&apiType='+apiType;
		});
	 
	 
	 /*Change done by Shruthi for TENJINCG-1206 starts*/
	 $(document).on("click", "#btnDelete", function(e) {
			
		 if($('#tblApiOperations').find('input[type="checkbox"]:checked').length === 0 || ($('#tblApiOperationse').find('input[type="checkbox"]:checked').length == 1 && $('#tblApiOperations').find('input[type="checkbox"]:checked').hasClass('tbl-select-all-rows'))) {
             showLocalizedMessage('no.records.selected', '', 'error');
             return false;
         }
        
         clearMessages();
        
         if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
             var $apiForm  = $("<form action='APIServlet' method='POST' />");
             var csrftoken_form = $("#csrftoken_form").val();
             $apiForm.append($("<input type='hidden' id='requesttype' name='requesttype' value='deleteApiOperations' />"))
             $apiForm.append($("<input type='hidden' id='csrftoken_form' name='csrftoken_form' value='"+csrftoken_form+"' />"))
            
             $('#tblApiOperations').find('input[type="checkbox"]:checked').each(function() {
                 if(!$(this).hasClass('tbl-select-all-rows')) {
                     
                     $apiForm .append($("<input type='hidden' name='appid' value='"+ $(this).data('appId') +"' />"));
                     $apiForm .append($("<input type='hidden' name='apicode' value='"+ $(this).data ('apiCode')+"' />"));
                     $apiForm .append($("<input type='hidden' name='operationname' value='"+ $(this).data('operationName') +"' />"));
                     
                 }
             });
             $('body').append($apiForm);
             $apiForm.submit();
         }
		 
	});
	 /*Change done by Shruthi for TENJINCG-1206 ends*/
	 
	 $('#btnNew').click(function(){
		  /*
		 *date:10-11-14
		 * Code Change Starts here: Badal
		 *change code id: #762
		 * in Aut functions when click on new and without selecting any function name gives success message
		 */	
		var code = $("#txtApiCode").val();
		var appid=$("#lstLrnrApplication").val();
		//changes done by Parveen for parameter List starts
		var apiURL=$("#txtApiUrl").val();
		//changes done by Parveen for parameter List ends
		window.location.href='APIServlet?param=new_rest_operation&appid=' + appid +'&code='+ code+'&apiURL='+ apiURL;
		//window.location.href='ModuleServlet?param=NEW_FUNC';
	});
		
});
/*Added by Pushpalatha for TENJINCG-568 starts*/
function populateAutGrooups(appId,grpName){
	var jsonObj = fetchAutGroups(appId);
		
		$('#existingGroup').html('');
		var status = jsonObj.status;
		if(status== 'SUCCESS'){
			var html = "";
			var jarray = jsonObj.GROUPS;
			if(jarray == undefined){
				
			}else{
			for(i=0;i<jarray.length;i++){
				var json = jarray[i];
				if(json=== grpName){
					html = html + "<option value='" + json + "' selected>" + json  + "</option>";
				}else{
				html = html + "<option value='" + json + "'>" + json  + "</option>";
				}
			}
			}
			
			$('#existingGroup').html(html);
		}else{
			showMessage(jsonObj.message,'error');
		}
	}
/*Added by Pushpalatha for TENJINCG-568 ends*/
function refreshOperations() {
	var appId = $('#lstLrnrApplication').val();
	var apiCode = $('#txtApiCode').val();
	
	$.ajax({
		url:'APIServlet?param=refresh_operations&appId=' + appId + '&apiCode=' + apiCode,
		dataType:'json',
		success :function(data) {
			 /*changed by manish for bug T25IT-39 on 10-09-2017 starts*/
			$('#refreshTitle').html('<p>Refresh Completed</p>');
			 /*changed by manish for bug T25IT-39 on 10-09-2017 ends*/
			if(data.status =='success') {
				$('#processing_icon').html("<img src='images/success_large.png' alt='Completed' />");
				$('#message').html('<p>Operations refreshed successfully!</p>');
				$('#pr_actions').show();
			}else {
				$('#processing_icon').html("<img src='images/error_large.png' alt='Completed' />");
				 /*changed by manish for bug T25IT-39 on 10-09-2017 starts*/
				/*$('#def_review_message').html('<p>' + data.message + '</p>');*/
				$('#message').html('<p>' + data.message + '</p>');
				 /*changed by manish for bug T25IT-39 on 10-09-2017 ends*/
				$('#pr_actions').show();
			}
		},
		error:function(xhr, textStatus, errorThrown) {
			 /*changed by manish for bug T25IT-39 on 10-09-2017 starts*/
			$('#refreshTitle').html('<p>Refresh Completed</p>');
			 /*changed by manish for bug T25IT-39 on 10-09-2017 ends*/
			$('#processing_icon').html("<img src='images/error_large.png' alt='Completed' />");
			$('#message').html('<p>An internal error occurred. Please contact Tenjin Support.</p>');
			$('#pr_actions').show();
		}
		
	});
}

function closeModal()
{
	$('.modalmask').hide();
	$('#ttd_download_options').hide();
	$('#ttd-options-frame').attr('src','');
}