/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  runprogress.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 18-11-2016			nagareddy			 for manual input field
* 16-06-2017			Sriram					TENJINCG-187
* 10-08-2017			Sriram					T25IT-62
* 19-08-2017			Sriram					T25IT-67
* 25-02-2019        	Sahana 			   		pCloudy
  13-03-2020			Sriram					Entire file changed for TENJINCG-1196  
*/

let refreshTimer = null;
let elapsedTimer = null;

let currentTestCaseId = $("#currentTestCaseId").text();
let currentStep = $("#currentTestStep").text();
let currentIteration = $("#currentIteration").text();

$(document).ready(function(){
	
	//$("#tc-table").dataTable();
	
	$("#btnRefresh").click(function(e) {
		e.preventDefault();
		window.location.href='TestRunServlet?param=run_progress&run=' + $("#runId").val();
	});
	
	renderCircularProgress(0, $("#circular_progress_bar"));
	
	refreshTimer = window.setInterval(function() {
		refreshStatus($("#runId").val())
	}, 2000);
	
	var runStartTime = new Date($("#startTimeStamp").text());
	/*added by shruthi for Tenj212.1 starts*/
	var parsedtime = new Date(Date.now(runStartTime));
    var starttime=new Date(parsedtime.toUTCString());
	   /* added by shruthi for Tenj212.1 ends*/
	elapsedTimer = window.setInterval(function() {
		var currentTime = new Date();
		var elapsedSeconds = Math.round((currentTime-starttime)/1000);
		$("#elapsedTime").text(secondsToHMS(elapsedSeconds));
	});
});

var refreshStatus = function (runId)  {
	var task = $.ajax({
		url:'RunAjaxServlet?param=tc_summary&run=' + runId,
		dataType: 'json',
		error: function(xhr, textStatus, errorThrown) {
			console.error("Error fetching run status", errorThrown);
		}
	});
	var completedStatuses = ['Complete', 'Aborted'];
	task.done(function(result) {
		if(!result.error) {
			if(completedStatuses.indexOf(result.status)> -1) {
				window.location.href = 'ResultsServlet?param=run_result&run=' + runId;
			}
			drawChart(result.testCaseExecutionProgress.testCases);
			updateTestCaseStepSummary(result.testCaseExecutionProgress.testCases);
			updateCurrentExecutionBlock(result.testCaseExecutionProgress.currentTestCase, result.testCaseExecutionProgress.currentTestStep);
		}
	});
}

var statusImageMap = {
		"Pass":"success_20c20.png",
		"Fail":"failure_20c20.png",
		"Error":"error_20c20.png",
		"Not Executed":"queued_20c20.png",
		"Executing":"ajax-loader.gif",
		"Not Started": "queued_20c20.png"
}

var updateTestCaseStepSummary = function(testCases) {
	
	testCases.forEach(function(testCase) {
		var tcRecId, tcId, tcName, tcStatus, totalPassedSteps, totalFailedSteps, totalExecutedSteps, totalErrorredSteps  = testCase;
		
		// Update status icon
		var statusIcon = "images/" + statusImageMap[tcStatus];
		var statusImageElement = $("#" + tcRecId + "_status_img");
		statusImageElement.attr("src", statusIcon);
		statusImageElement.closest("td").attr("title", tcStatus);
		
		//Update step execution stats
		$("#" + tcRecId + "_executedSteps").text(totalExecutedSteps);
		$("#" + tcRecId + "_passedSteps").text(totalPassedSteps);
		$("#" + tcRecId + "_failedSteps").text(totalFailedSteps);
		$("#" + tcRecId + "_errorSteps").text(totalErrorredSteps);
		
		// Change color classes for failed and error steps if necessary
		if(totalFailedSteps > 0) {
			$("#" + tcRecId + "_failedSteps").addClass("red");
		}else{
			$("#" + tcRecId + "_failedSteps").removeClass("red");
		}
		
		if(totalErrorredSteps > 0) {
			$("#" + tcRecId + "_errorSteps").addClass("red");
		}else{
			$("#" + tcRecId + "_errorSteps").removeClass("red");
		}
	});
}

var updateCurrentExecutionBlock = function(testCase, testStep)  {
	
	if(testCase) {
		if(currentTestCaseId !== testCase.tcId) {
			$("#currentTestCaseId").text(testCase.tcId);
			currentTestCaseId = testCase.tcId;
			$("#currentTestCaseName").text(testCase.tcName);
		}
	}
	
	if(testStep) {
		var newCurrentStepText = testStep.id + " - " + testStep.shortDescription;
		if(currentStep !== newCurrentStepText) {
			currentStep = newCurrentStepText;
			$("#currentTestStep").text(newCurrentStepText);
		}
		
		if(currentIteration !== testStep.dataId) {
			currentIteration = testStep.dataId;
			$("#currentIteration").text(testStep.dataId);
		}
	}
}

var drawChart = function(summary)  {
	var totalTestCases = summary.length;
	let completed = 0;
	var completedStatuses = ["Pass", "Fail", "Error"];
	summary.forEach(function(tc)  {
		if(completedStatuses.indexOf(tc.tcStatus)> -1) {
			completed++;
		}
	});
	
	var percentage = Math.round((completed/totalTestCases)*100);
	
	renderCircularProgress(percentage, $("#circular_progress_bar"));
}

renderCircularProgress = function(percent, chartContainer)  {
	chartContainer.html("");
    var $pCircle = $("<div class='progress-circle' />").appendTo(
      chartContainer
    );
    $pCircle.addClass("p" + percent);
    if (percent > 50) {
      $pCircle.addClass("over50");
    }
    $pCircle.append($("<span>" + percent + "%</span>"));
    var $clipper = $("<div class='left-half-clipper'/>").appendTo(
      $pCircle
    );

    $("<div class='first50-bar' />").appendTo($clipper);
    $("<div class='value-bar' />").appendTo($clipper);
}


/// manual input add by nagareddy 18-11-2016
//request permission on page load
document.addEventListener('DOMContentLoaded', function () {
	
	  if (!Notification) {
		    alert('Desktop notifications not available in your browser. Try Chromium.'); 
		    return;
		  }
  if (Notification.permission !== "granted")
    Notification.requestPermission();
});


/* Added by Sriram for TENJINCG-187 */
$(document).on('click','#btnAbortRun', function() {
	var runId = $('#runId').val();
	$.ajax({
		url:'TestRunServlet?param=abort&run=' + runId,
		dataType:'json',
		async:true,
		success:function(data) {
			if(data.status.toLowerCase() ==='success'){
				window.location.href='ResultsServlet?param=run_result&run=' + runId;
			}else{
				showMessage(data.message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown) {
			showMessage('Could not abort run due to an internal error. Please contact Tenjin Support.', 'error');
		}
	});
});
/* Added by Sriram for TENJINCG-187 ends*/


function secondsToHMS(secs) {
  function z(n){return (n<10?'0':'') + n;}
  var sign = secs < 0? '-':'';
  secs = Math.abs(secs);
  return sign + z(secs/3600 |0) + ':' + z((secs%3600) / 60 |0) + ':' + z(secs%60);
}