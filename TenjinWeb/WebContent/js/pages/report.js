/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  report.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 * 
 * DATE              	CHANGED BY              DESCRIPTION
 * 21-12-2016			SRIRAM					Defect#TEN-24
 * 22-12-2016			SRIRAM					Defect#TEN-158
 * 10-JAN-2016          Sahana                  TENJINCG-7(Calendar Icon is to be added for all date fields)
 * 17-02-2017			Manish					TENJINCG-128
 * 25-07-2017			Padmavathi				TENJINCG-71
 * 28-08-2017           Padmavathi              defect T25IT-326
 * 19-06-2018			Preeti					T251IT-82
 * 19-06-2018			Preeti					T251IT-84
 * 28-06-2018           Padmavathi              T251IT-85 
 * 13-03-2019			Preeti					TENJINCG-987
 * 20-03-2019           Padmavathi              TENJINCG-1014
 * 22-05-2019			Ashiki					TJN27-2
 */
$(document).ready(function() {
	/*Added by Preeti for T251IT-82 starts*/
	$('#resultReport').hide();
	/*Added by Preeti for T251IT-82 ends*/
	initializeReport();

	/*(function ($) {
		  $.each(['show'], function (i, ev) {
		    var el = $.fn[ev];
		    $.fn[ev] = function () {
		      this.trigger(ev);
		      return el.apply(this, arguments);
		    };
		  });
		})(jQuery);*/
	/*
	$('#user-message').on('show', function(){
		$(function() {
		    setTimeout(function() {
		        $("#user-message").hide('blind', {}, 250)
		    }, 5000);
		});

	});*/
	/* Added by Padmavathi for the improvement of TENJINCG-71: starts*/ 
	var now = new Date();
    var day = now.getDate();
    var monthNames=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var  month= monthNames[now.getMonth()];
    if(day < 10) 
        day = "0" + day;
    var today= day + '-' + month + '-' +now.getFullYear().toString().substr(-2);
    /* Added by Padmavathi for the improvement of TENJINCG-71: ends*/ 
	function disableDatepicker(){
		$('#fromDate').val('');
		$('#toDate').val('');
		$('#fromDate').prop('disabled', true);
		$('#toDate').prop('disabled', true);
	}

	function enableDatePicker(){
		$('#fromDate').prop('disabled', false);
		$('#toDate').prop('disabled', false);
	}

	$('#lstApplication').change(function(){
		var selApp = $(this).val();
		clearMessages();
		/*   Changed by sahana for the improvement of TENJINCG-7: starts  */
		$('#fromDate').datepicker('enable');
		$('#toDate').datepicker('enable');
		/* Added  by Padmavathi for the improvement of TENJINCG-71: starts*/ 
	    $('#toDate').val(today);
	    /* Added  by Padmavathi for the improvement of TENJINCG-71: ends*/ 
		/*   Changed by sahana for the improvement of TENJINCG-7: ends  */
		if(selApp == '-1'){
			disableDatepicker();
			$('#lstFunction').prop('disabled', true);
			$('#lstFunction option[value=-1]').prop('selected','selected');
			$('#tillDate').prop('disabled', true);
		}else if(selApp == '0'){
			enableDatePicker();
			$('#lstFunction').prop('disabled', true);
			$('#lstFunction option[value=-1]').prop('selected','selected');
			$('#tillDate').prop('disabled', false);
		}else{
			enableDatePicker();
			$('#lstFunction').prop('disabled', false);
			$('#tillDate').prop('disabled', false);
			$('#lstFunction').find('option').not(':first').remove();
			var options='';

			$.ajax({
				url:'ReportServlet',
				data :'param=get_func'+ '&app_id=' + selApp,
				async:false,
				dataType:'json',
				success: function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						//showMessage(data.message, 'success');	
						var dArray = data.modules;
						options += '<option value="0">--ALL--</option>';
						for(var i=0;i<dArray.length;i++){
							var module = dArray[i];
							options += '<option value="' + module.code + '">' + module.code + '</option>';
						} 
						$('#lstFunction').append(options);
					}else{
						//showMessage(data.message, 'error');
						$('#lstFunction').append(options);
						//$('#lstFunction').prop('disabled', true);
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(textStatus, 'error');			
				}
			});
		}
	});

	$('#lstUser').change(function(){
		$('#fromDate').datepicker('enable');
		$('#toDate').datepicker('enable');
		/* Added  by Padmavathi for the improvement of TENJINCG-71: starts*/ 
		$('#toDate').val(today);
		/* Added  by Padmavathi for the improvement of TENJINCG-71: ends*/ 
		if($(this).val()!= '-1'){
			enableDatePicker();
			$('#tillDate').prop('disabled', false);
		}else{
			disableDatepicker();
			$('#tillDate').prop('disabled', true);
		}
	});

	/*$('select[id^=lst]').change(function(){

		enableDatePicker();
		if($(this).is('#lstApplication')){}
		var flag = '';
		var array = [];
		$('select[id^=lst]').each(function(){
			var appValue = $(this).val();
			if($(this).is('#lstFunction')){
				return;
			}
			flag += appValue;
		});
		if($('input:checkbox[id=tillDate]').is(':checked')){
			disableDatepicker();
		}else{
			enableDatePicker();
		}
		if(flag == '-1-1' || $(this).val() == '0'){
			if($(this).is('#lstFunction')){
				$('#lstFunction').prop('disabled', false);
			}else{
				$('#lstFunction').prop('disabled', true);
				disableDatepicker();

			}
		}else{
			$('#lstFunction').prop('disabled', false);
		}

	});*/

	$('input:checkbox[id=tillDate]').change(function(){
		if($(this).is(':checked')){
			disableDatepicker();
		}else{
			enableDatePicker();
		}
	});

	function initializeReport(){
		var appId = $('#lstApplication').val();
		var module = $('#lstFunction').val();
		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();
		var user = $('#lstUser').val();

		var message = $('#message').val();
		if (message == 'noerror') {
		} else {
			showMessage(message, 'error');
		}

		$('#lstFunction').prop('disabled', true);
		$('#fromDate').prop('disabled', true);
		$('#toDate').prop('disabled', true);
		$('#tillDate').prop('disabled', true);

		if(appId == -1 || module == -1 || user == -1 ){
			disableDatepicker();
		}else{
			enableDatePicker();
		}

		//For Datepicker
		//$("#fromDate").datepicker({minDate : new Date(2016,00,01),  dateFormat: 'dd-M-y'});
		/*$("#fromDate").datepicker();
		$("#toDate").datepicker();*/
		/*$("#toDate").datepicker({
				minDate : new Date(2016,00,01),
				dateFormat: 'dd-M-y',
				onSelect: function () {
					var fromDate = $("#fromDate").datepicker("getDate");
					var toDate = $("#toDate").datepicker("getDate")
					if(fromDate == null){
						$("#fromDate").datepicker("option", "minDate", toDate.getDate()-365);
						$("#fromDate").datepicker("option", "maxDate", toDate);
					}
		        }
		});*/

		$('#lstFunction').change(function(){
			clearMessages();
		});

		$('#lstUser').change(function(){
			clearMessages();
		});

		$("#fromDate").datepicker({
			/*   Changed by sahana for the improvement of TENJINCG-7: starts  */
			showOn: 'button',
			buttonText: 'Show Date',
			buttonImageOnly: true,
			buttonImage: './images/button/calendar.png',
			constrainInput: true,
			disabled: true,
			/*   Changed by sahana for the improvement of TENJINCG-7: ends  */
			dateFormat: 'dd-M-y',
			maxDate : new Date(),
			onSelect: function (selected) {
				clearMessages();
				var dt = new Date(selected);
				dt.setDate(dt.getDate());
				/*dt.setDate(dt.getDate() + 1);*/
				$("#toDate").datepicker("option", "minDate", dt);
			}
		});
		$("#toDate").datepicker({
			/*   Changed by sahana for the improvement of TENJINCG-7: starts  */
			showOn: 'button',
			buttonText: 'Show Date',
			buttonImageOnly: true,
			buttonImage: './images/button/calendar.png',
			constrainInput: true,
			disabled: true,
			/*   Changed by sahana for the improvement of TENJINCG-7: ends  */
			dateFormat: 'dd-M-y',
			maxDate : new Date(),
			onSelect: function (selected) {
				clearMessages();
				var dt = new Date(selected);
				dt.setDate(dt.getDate());
				/*dt.setDate(dt.getDate()-1);*/
				$("#fromDate").datepicker("option", "maxDate", dt);
			}
		});


		/*$("#toDate").focusin(function() {
			var fromDate = $("#fromDate").datepicker("getDate");
			if (fromDate == null) {
				fromDate = new Date(2016,00,01);
			}
			$("#toDate").datepicker("option", "minDate", fromDate);
			console.log(fromDate.getDate()+365);
			$("#toDate").datepicker("option", "maxDate", fromDate.getDate()+365);
		});

		$("#fromDate").focusin(function() {
			var toDate = $("#toDate").datepicker("getDate");
			if (toDate != null) {
				$("#fromDate").datepicker("option", "minDate", toDate.getDate()-365);
				$("#fromDate").datepicker("option", "maxDate", toDate);
			}
		});*/

		/*var fullSDate;
		$("#fromDate").datepicker({minDate : new Date(2016,00,01)});
		$("#toDate").datepicker({minDate : new Date(2016,00,01)});

		$("#toDate").focusin(function() {
			fullSDate = $("#fromDate").datepicker("getDate");
			if (fullSDate == null) {
				fullSDate = new Date(2016,00,01);
			}
			$("#toDate").datepicker("option", "minDate", fullSDate);
		});
		$("#fromDate").focusin(function() {
			fullSDate = $("#toDate").datepicker("getDate");
			if (fullSDate == null) {
				fullSDate = new Date(2017,00,01);
			}
			$("#fromDate").datepicker("option", "maxDate", fullSDate);
		});*/

		/*$("#fromDate").datepicker({
	        dateFormat: "dd-M-yy",
	        minDate: new Date(2016,00,01),
	        onSelect: function () {
	            var dt2 = $('#toDate');
	            var maxDate = $(this).datepicker('getDate');
	            maxDate.setDate(maxDate.getDate() + 365);
	            var minDate = $(this).datepicker('getDate');
	            //dt2.datepicker('setDate', minDate);
	            dt2.datepicker('option', 'maxDate', maxDate);
	            dt2.datepicker('option', 'minDate', minDate);
	            $(this).datepicker('option', 'minDate', minDate);
	        }
	    });
		$("#toDate").datepicker({
	        dateFormat: "dd-M-yy",
	        minDate: new Date(2016,00,01),
	        onSelect: function () {
	            var dt2 = $('#fromDate');
	            var maxDate = $(this).datepicker('getDate');
	            maxDate.setDate(maxDate.getDate() + 365);
	            var minDate = $(this).datepicker('getDate');
	            //dt2.datepicker('setDate', minDate);
	            dt2.datepicker('option', 'maxDate', minDate);
	            dt2.datepicker('option', 'minDate', maxDate);
	            $(this).datepicker('option', 'minDate', maxDate);
	        }
	    });
	    $('#toDate').datepicker({
	        dateFormat: "dd-M-yy"
	    });*/
	}

	$('#btnReset').click(function(){
		/*Added by Preeti for T251IT-82 starts*/
		$('#resultReport').hide();
		/*Added by Preeti for T251IT-82 ends*/
		$('#lstApplication').val(-1);
		$('#lstFunction').val(-1);
		$('#lstFunction').prop('disabled',true);
		$('#lstUser').val(-1);
		disableDatepicker();
		if($('input:checkbox[id=tillDate]').is(':checked')){
			$('input:checkbox[id=tillDate]').prop('checked', false);
		}
		$('#user-message').hide();
		$('#tblReportSummary').remove();
		$('#totpages').text(0);
		$.datepicker._clearDate('#fromDate');
		$.datepicker._clearDate('#toDate');

		$("#fromDate").datepicker("option", "maxDate", new Date());
		$("#toDate").datepicker("option", "maxDate", new Date());
		/*Added by Padmavathi for T251IT-85 starts*/
		$('.ui-datepicker-trigger').css('opacity','0.5');
		$('.ui-datepicker-trigger').css('cursor','default');
		/*Added by Padmavathi for T251IT-85 ends*/
		$('#def_logged_block').hide();
	});

	$('#gen_report').click(function(){
		/*Added By padmavathi for T25IT-326 starts*/
		clearMessagesOnElement($('#user-message'));
		/*Added By padmavathi for T25IT-326 ends*/
		var criteria = getCriteria();
		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();
		/* Added by Padmavathi for TENJINCG-1014 starts */
		var selFromDate=new Date(fromDate);
		var selToDate=new Date(toDate);
		var todaysDate=new Date();
		todaysDate.setHours(0,0,0,0);
		/* Added by Padmavathi for TENJINCG-1014 ends */
		if(criteria == ''){
			showMessage("Please select atleast one Criteria", "error");
		}else if(fromDate != '' && toDate == ''){
			showMessage("Please select To Date", "Error");
		}else if(toDate != '' && fromDate == ''){
			showMessage("Please select From Date", "Error");
		}/* Added by Padmavathi for TENJINCG-1014 starts */
		else if(fromDate != ''&& !validateDate(fromDate)){
		    showMessage('Please enter From Date value in this format DD-MMM-YY.','error');
			return false;
		}else if(toDate != ''&& !validateDate(toDate)){
			showMessage('Please enter To Date value in this format DD-MMM-YY.','error');
			return false;
		}else if(selFromDate>todaysDate){
			showMessage("From Date should be less than or equal to Today's Date","error");
			return false;
		}else if(selToDate>todaysDate){
			showMessage("To Date should be less than or equal to Today's Date","error");
			return false;
		}
		else if(selFromDate>selToDate){
			showMessage("To Date should be greater than or equal to From Date","error");
			return false;
			/* Added by Padmavathi for TENJINCG-1014 ends */
		}else{
			$.ajax({
				url		: 'ReportServlet',
				type	: 'GET',
				data	: {'param' :'criteria','criteria' : criteria },
				dataType: 'json',
				success	: function(data){
					var options = '';
					var status = data.status;
					if(status == 'SUCCESS'){
						/*Added by Preeti for T251IT-82 starts*/
						$('#resultReport').show();
						/*Added by Preeti for T251IT-82 ends*/
						var reportArray = data.report;
						$('#rawReportData').val(JSON.stringify(reportArray));
						var summaryTable = '<table id="tblReportSummary" class="bordered" cellspacing="0" width="100%"> <thead>';
						var tableHeader = '';
						var hUser = false;
						var hApp = false;
						var hFunc = false;
						var hDefects = false;
						for(var i = 0; i<reportArray.length; i++){

							var report = reportArray[i];
							options += '<tr>';
							if(report.userid != null|| report.userid != undefined){
								options += '<td><b>'+ report.userid +'</b></td>';
								/*if(i==0){
							summaryTable += '<th>User</th>';
							}*/
								hUser = true;
							}
							if(report.application != null || report.application != undefined){
								options += '<td><b>'+ report.application +'</b></td>';
								/*if(i==0){
							summaryTable += '<th>Application</th>';
							}*/
								hApp = true;
							}
							if(report.functioncode != null|| report.functioncode != undefined){
								if(report.functioncode.length > 0){
									options += '<td><b>'+ report.functioncode +'</b></td>';
								}else{
									options+='<td><b>All</b></td>';
								}
								/*if(i==0){
							summaryTable += '<th>Function</th>';
							}*/
								hFunc = true;
							}
							if(report.totaltc != 0|| report.totaltc != undefined){
								/*Modified by Preeti for TENJINCG-987 starts*/
								options += '<td class="text-center"><b>'+ report.totaltc +'</b></td>';
								/*if(i==0){
							summaryTable += '<th>Total TC/TS</th>';
							}*/
							}
							if(report.executed != null|| report.executed != undefined){
								options += '<td class="text-center"><b>'+ report.executed +'</b></td>';
								/*if(i==0){
							summaryTable += '<th>Executed</th>';
							}*/
							}
							if(report.passedtc != 0|| report.passedtc != undefined){
								options += '<td class="text-center"><b>'+ report.passedtc +'</b></td>';
								/*if(i==0){
							summaryTable += '<th>Passed</th>';
							}*/
							}
							if(report.failedtc != 0|| report.failedtc != undefined){
								options += '<td class="text-center"><b>'+ report.failedtc +'</b></td>';
								/*if(i==0){
							summaryTable += '<th>Failed(Txn.)</th>';
							}*/
							}
							/*if(report.errortc != 0|| report.errortc != undefined){
							options += '<td><b>'+ report.errortc +'</b></td>';
							if(i==0){
							summaryTable += '<th>Error</th>';
							}
						}*/
							if(report.percentage != null|| report.percentage != undefined){
								options += '<td class="text-center"><b>'+ report.percentage +'%</b></td>';
								/*if(i==0){
							summaryTable += '<th>Complete %</th>';
							}*/
							}

							if(report.totalvals != null|| report.totalvals != undefined){
								options += '<td class="text-center"><b>'+ report.totalvals +'</b></td>';
								/*if(i==0){
							summaryTable += '<th>Complete %</th>';
							}*/
							}
							if(report.passvals != null|| report.passvals != undefined){
								options += '<td class="text-center"><b>'+ report.passvals +'</b></td>';
								/*if(i==0){
							summaryTable += '<th>Complete %</th>';
							}*/
							}
							if(report.failedvals != null|| report.failedvals != undefined){
								options += '<td class="text-center"><b>'+ report.failedvals +'</b></td>';
								/*if(i==0){
							summaryTable += '<th>Complete %</th>';
							}*/
							}
							if(report.valpercent != null|| report.valpercent != undefined){
								options += '<td class="text-center"><b>'+ report.valpercent +'%</b></td>';
								/*if(i==0){
							summaryTable += '<th>Complete %</th>';
							}*/
							}


							//Changed by Sriram to fix Defect#TEN-22
							/*if(report.defectslogged !=null && report.defectslogged.length > 0 && (report.functioncode != null || report.userid != null)){
							for(var i=0;i<report.defectslogged.length;i++){
								var defect = report.defectslogged[i];
								alert(defect['createdBy']);
							}

							alert(report.defectslogged);
							var defectId = '';
							$.each(report.defectslogged,function(key,value){
								defectId += value+',';
							});
							if(defectId.length > 1){
								options += '<td><b><a href=DefectServlet?param=fetch_list_of_defect&ids='+defectId.substring(0,defectId.length-1)+'>'+report.defectslogged.length+'</a></b></td>';
							}else{
								options += '<td><b>'+report.defectslogged.length+'</b></td>';
							}
							if(i==0){
							summaryTable += '<th>Defects Logged</th>';
							}
							var hDefects = true;
						}*/
							///Changed by Sriram for TEN-158
							/*if(report.defectslogged != null && report.defectslogged != undefined && $('#reportType').val() !='aut'){
							options += "<td><b><a href='DefectServlet?param=fetch_project_defects_report&pid=" + $('#projectId').val() + "&appId=" + report.appid + "&fromDate=" + $('#fromDate').val() + "&toDate=" + $('#toDate').val() + "&userId=" + report.userid + "' >" + report.defectslogged + "</a></b></td>";
							hDefects = true;
						}*/
							if(report.defectslogged != null && report.defectslogged != undefined && $('#reportType').val() !='aut'){
								if(report.defectslogged == 0){
									options += "<td class='text-center'><b>0</b></td>";
								}else{
									defectsNotZero = true;
									options += "<td class='text-center'><b><a href='#' class='show_defects' data-details='detail_" + i + "' pid='" + $('#projectId').val() + "' appId='" + report.appid + "' fromDate='" + $('#fromDate').val() + "' toDate='" + $('#toDate').val() + "' userId='" + report.userid + "' >" + report.defectslogged + "</a></b></td>";
								}
								hDefects = true;
								/*Modified by Preeti for TENJINCG-987 ends*/
							}///Changed by Sriram for TEN-158 ends
							if(report.fromdate != null|| report.fromdate != undefined){
								options += '<td><b>'+ report.fromdate +'</b></td>';
								/*if(i==0){
							summaryTable += '<th>Start Date</th>';
							}*/
							}
							options += '</tr>';
						}

						if(reportArray.length == 0){
							options += 'No Records to display.';
							$('#reportSummary').html(options);
						}else{

							tableHeader+= '<tr>';
							if(hUser===true){
								tableHeader+='<th rowspan=2>User</th>';
							}

							if(hApp === true){
								tableHeader+='<th rowspan=2>Application</th>';
							}

							if(hFunc === true){
								tableHeader+='<th rowspan=2>Function</th>';
							}
							/*Modified by Preeti for TENJINCG-987 starts*/
							tableHeader+="<th colspan='5' class='text-center'>Transaction Status</th>";
							tableHeader+="<th colspan='4' class='text-center'>Validation Status</th>";
							//Added by Sriram to fix defect TEN-22
							if(hDefects === true){
								tableHeader+='<th rowspan="2" class="text-center">Defects Logged</th>';
							}
							//Added by Sriram to fix defect TEN-22 ends
							tableHeader+= '</tr>';
							tableHeader+= '<tr>';
							tableHeader+='<th title="Total Transactions" class="text-center"><img src="images/sum_20c20.png" style="width:13px;height:13px;"/></th>';
							tableHeader+='<th title="Executed" class="text-center"><img src="images/executed_20c20.png"/></th>';
							tableHeader+='<th title="Passed" class="text-center"><img src="images/success_20c20.png"/></th>';
							tableHeader+='<th title="Failed" class="text-center"><img src="images/failure_20c20.png"/></th>';
							tableHeader+='<th style="font-size:15px;cursor:default;" class="text-center">%</th>';
							tableHeader+='<th title="Total Validations" class="text-center"><img src="images/sum_20c20.png" style="width:13px;height:13px;"/></th>';
							tableHeader+='<th title="Passed" class="text-center"><img src="images/success_20c20.png"/></th>';
							tableHeader+='<th title="Failed" class="text-center"><img src="images/failure_20c20.png"/></th>';
							tableHeader+='<th style="font-size:15px;cursor:default;" class="text-center">%</th>';
							/*Modified by Preeti for TENJINCG-987 ends*/


							summaryTable += tableHeader+ '</tr></thead><tbody id=tblReportSummary_body>'+options+'</tbody></table>';
							//alert(summaryTable);
							$('#reportSummary').html(summaryTable);
						}
						paginate();
					}else{
						showMessage(data.message, "Error");
					}
				}
			});
		}
	});

	function getCriteria(){
		var appId = $('#lstApplication').val();
		var module = $('#lstFunction').val();
		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();
		var user = $('#lstUser').val();
		var tillDate = $('#tillDate').val();
		var criteria = '';
		if($('#lstApplication').is(':visible')){
			if(appId != -1){
				criteria += $('#lstApplication').attr('tjn_name')+'|'+appId+',';
			}
			if(module != -1){
				criteria += $('#lstFunction').attr('tjn_name')+'|'+module+',';
			}
		}

		if($('#lstUser').is(':visible')){
			if(user != -1){
				criteria += $('#lstUser').attr('tjn_name')+'|'+user+',';
			}
		}

		if(fromDate != '' && toDate!=''){
			criteria += $('#fromDate').attr('tjn_name')+'|'+fromDate+','+$('#toDate').attr('tjn_name')+'|'+toDate+',';
		}
		if($('input:checkbox[id=tillDate]').is(':checked')){
			criteria += $('#tillDate').attr('tjn_name')+'|'+'true,';
		}
		criteria = criteria.substring(0,criteria.length-1);
		return criteria;
	}

	$("select").on('click', 'option', function() {
		if ($("select option:selected").length > 3) {
			$(this).removeAttr("selected");
			alert('You can select upto 3 options only');
		}
	});

	//Added for TEN-158 - SRIRAM
	$(document).on('click','.show_defects',function(e){
		//$('#defBody').html("<tr><td colspan='7'>Loading. Please Wait... <img src='images/inprogress.gif'/></td></tr>");

		$.ajax({
			url:'DefectServlet?param=fetch_project_defects_report&pid=' + $(this).attr('pid') + '&appId=' + $(this).attr('appid') + '&userId=' + $(this).attr('userid') + '&fromDate=' + $(this).attr('fromdate') + '&toDate=' + $(this).attr('todate'),
			async:true,
			dataType:'json',
			success:function(data){
				var status =data.status;
				if(status =='success'){
					var defects = data.defects;
					var html = '';
					for(var i=0; i<defects.length; i++){
						var defect = defects[i];
						html += '<tr>';
						html += '<td>' + defect.id + '</td>';
						/*Modified by Preeti for T251IT-84 starts*/
						if(defect.summary.length<25)
							html += '<td title="'+ defect.summary +'">' + defect.summary + '</td>';
						else
							html += '<td title="'+ defect.summary +'">' + defect.summary.substring(0,25)+'...' + '</td>';
						/*Modified by Preeti for T251IT-84 ends*/
						html += '<td>' + defect.application + '</td>';
						/***************
						 * added by manish for defect TENJINCG-128 on 17-02-2017 starts
						 */
						if(defect.func=='-1' || defect.func=='null'){
							defect.func = 'All';
						}
						/***************
						 * added by manish for defect TENJINCG-128 on 17-02-2017 ends
						 */
						html += '<td>' + defect.func + '</td>';
						html += '<td>' + defect.severity + '</td>';
						html += '<td>' + defect.status + '</td>';
						html += '<td>' + defect.posted + '</td>';
						html += '</tr>';
					}

					$('#defBody').html(html);
					$('#def_logged_block').show();
					location.hash = '#def_logged_block';
				}else{
					var html = "<tr><td colspan='7'><img src='images/failure_20c20.png'/> " + data.message + " </td></tr>";
					$('#defBody').html(html);
					$('#def_logged_block').show();
					location.hash = '#def_logged_block';
				}
			},
			error:function(xhr, textStatus, errorThrown){
				var html = "<tr><td colspan='7'><img src='images/failure_20c20.png'/> Could not load defect details. Please contact Tenjin Support.</td></tr>";
				$('#defBody').html(html);
				$('#def_logged_block').show();
				location.hash = '#def_logged_block';
			}
		});
	});
	//Added for TEN-158 - SRIRAM ends

	$('#btnDownload').click(function(){
		/*function sheet_from_array_of_arrays(data, opts) {
			var ws = {};
			var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
			for(var R = 0; R != data.length; ++R) {
				for(var C = 0; C != data[R].length; ++C) {

				 	if(range.s.r > R) range.s.r = R;
					if(range.s.c > C) range.s.c = C;
					if(range.e.r < R) range.e.r = R;
					if(range.e.c < C) range.e.c = C; 
					//alert('sr'+range.s.r +'sc'+range.s.c+'er'+range.e.r+'ec'+range.e.c+'data'+data[R][C]+'r'+R+'C'+C);
					var cell = {v: data[R][C]};
					if(cell.v == null) continue;
					var cell_ref = XLSX.utils.encode_cell({c:C,r:R});

					if(typeof cell.v === 'number') cell.t = 'n';
					else if(typeof cell.v === 'boolean') cell.t = 'b';
					else if(cell.v instanceof Date) {
						cell.t = 'n'; cell.z = XLSX.SSF._table[14];
					}
					else cell.t = 's';
					cell.v = 5;
					cell.s = { numFmt : '0.00%'}; 
					cell.t = 'n';

					//cell.s = {};

					//cell.push('<fonts count="2"><font><sz val="12"/><color theme="1"/><name val="Calibri"/><family val="2"/><scheme val="minor"/></font><font><b/><sz val="12"/><color theme="1"/><name val="Calibri"/><family val="2"/><scheme val="minor"/></font></fonts>');
					//var os = get_cell_style(opts.cellXfs, cell, opts);
					//cell = {v: 42145.822, t : 'n', s : {numFmt : '0.00%'}};
					cell.s = {
						    bold: true,
						    font: 'Arial',
						    size: 16,

						    ,
						};

					ws[cell_ref] = cell;
				}
			}
			if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
			return ws;
		}

		// original data 
		var table = document.getElementById('tblReportSummary');
		var dataArray = [];
		var tableRows = table.getElementsByTagName('tr');
		for(var i = 0; i < tableRows.length; i++){
			var rowData = tableRows[i].getElementsByTagName('th');
			if(rowData.length > 0){
				var array = [];
				for(j = 0; j < rowData.length; j++){
					array.push(rowData[j].innerText);
					}
			}else{
			rowData = tableRows[i].getElementsByTagName('td');
			var array = [];
			for(j = 0; j < rowData.length; j++){
				array.push(rowData[j].innerText);
				}
			}
			dataArray.push(array);
		}
		var data = []
		var ws_name = "Report";
		function Workbook() {
			if(!(this instanceof Workbook)) return new Workbook();
			this.SheetNames = [];
			this.Sheets = {};
		}

		var wb = new Workbook(), ws = sheet_from_array_of_arrays(dataArray);

		// add worksheet to workbook 
		wb.SheetNames.push(ws_name);
		wb.Sheets[ws_name] = ws;
		var wbout = XLSX.write(wb, {bookType:'xlsx',cellStyles: true, bookSST:true,type: 'binary', cellNF : true});

		function s2ab(s) {
			var buf = new ArrayBuffer(s.length);
			var view = new Uint8Array(buf);
			for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
			return buf;
		}
		saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), "Report.xlsx")
		$("#tblReportSummary").btechco_excelexport({
            containerid: "tblReportSummary"
           , datatype: $datatype.Table,
        });*/

		var table = document.getElementById('tblReportSummary');
		//var tableRows = table.getElementsByTagName('tr');
		if(table != null){
			var headers ='';
			var tableRows = table.getElementsByTagName('tr');
			for(var i = 0; i < tableRows.length; i++){
				var rowData = tableRows[i].getElementsByTagName('th');
				if(rowData.length > 0){
					for(j = 0; j < rowData.length; j++){
						headers += rowData[j].innerText+',';
					}
				}
			}

			var json = $("#tblReportSummary").tableToJSON();
			var jstring = JSON.stringify(json);
			$.ajax({
				url : 'ReportServlet',
				//data : {'param' : 'dwld_report', 'header' :  headers.substring(0,headers.length-1), 'tableJson' : jstring},
				data:{'param':'dwld_report','json':$('#rawReportData').val(),'reportType':$('#reportType').val()},
				dataType : 'json',
				success : function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
						$("body").append($c);
						$("#downloadFile").get(0).click();
						$c.remove();
					}else{
						showMessage(data.message, "error");
					}
				}
			});

		}else{
			showMessage("Generate the report to download", "Error");
		}
	});
});
/* Added by Padmavathi for TENJINCG-1014 starts */
function validateDate(dateValue) {
		/*Changed by Ashiki for TJN27-2 starts*/
	  	/*var dtRegex = new RegExp("^([0]?[1-9]|[1-2]\\d|3[0-1])-(JAN|FEB|MAR|APR|MAY|JUN|JULY|AUG|SEP|OCT|NOV|DEC)-[1-2]\\d{1}$", 'i');*/
		var dtRegex = new RegExp("^([0]?[1-9]|[1-2]\\d|3[0-1])-(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)-[1-2]\\d{1}$", 'i');
		/*Changed by Ashiki for TJN27-2 ends*/
	  if( dtRegex.test(dateValue)){
		  var dateArray= dateValue.split('-');
		  var dtDay= dateArray[0];
		  var dtMonth = dateArray[1].toUpperCase();
		  var dtYear = dateArray[2];
		  if ((dtMonth=='APR' || dtMonth=='JUN' || dtMonth=='SEP' || dtMonth=='NOV') && dtDay ==31){
			  showMessage('Please enter valid date','error');
			  return false;
		  }else if (dtMonth == 'FEB')
		     {
		        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
		        if (dtDay> 29 || (dtDay ==29 && !isleap)){
		        	 showMessage('Please enter valid date','error');
		             return false;
		        }
		     }  
		  return true;

	  }else{
		  showMessage('Please enter date value in this format "DD-MMM-YY".','error');
		  return false;
	  }
	 }
	 /* Added by Padmavathi for TENJINCG-1014 ends */