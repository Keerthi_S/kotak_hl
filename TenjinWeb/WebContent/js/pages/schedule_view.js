/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 *16-DEC-2016		    Sahana					Req#TEN-72
 *16-DEC-2016			Sahana					defect #TEN-79(scheduler accepts past time)
 *21-Dec-2016			Nagababu              	defect #TEN-137(Time cannot be edited to prior time once the task is scheduled)
 *21-Dec-2016       	Sahana                  defect #TEN-139(The Minute Field is blank in Schedule Task screen.)
 *21-Dec-2016         	Sahana                  defect #TEN-145(In Scheduler,Getting issue while rescheduling.)
 *10-JAN-2016          	Sahana                  TENJINCG-7(Calendar Icon is to be added for all date fields)
 *27-Jan-2017			Sahana					TENJINCG-61(A Tenjin user should be able to specify Screenshot Option while Scheduling an Execution task)
 *20-July-2017			Gangadhar Badagi		TENJINCG-292
 *25-July-2017          Gangadhar Badagi        Added to hide testCase,testSet when type is selected as select one and send error message while trying to update scheduler without type
 *18-08-2017        	Leelaprasad             T25IT-132
 *31-Aug-2017          	Gangadhar Badagi        T25IT-391
 *1-Sept-2017         	Roshni         			T25IT-415
 *27-09-2017          	Gangadhar Badagi        To schedule the task between 55-60 minutes.
 *04-06-2018            Padmavathi              for TENJINCG-651
 *19-02-2019			Preeti					TENJINCG-928
 *20-03-2019            Padmavathi              TENJINCG-1014
 *25-03-2019			Pushpalatha				TENJINCG-968
 *30-05-2019			Ashiki					V2.8-63
 *24-06-2019		    Padmavathi 				for V2.8-157 
 *03-07-2019			Preeti					TV2.8R2-11
 */

/*
 * Added file by sahana for Req#TJN_24_03 on 14/09/2016
 */
$(document).ready(function(){
	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('edit_schedule');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	/*Added by Roshni for T25IT-415 starts*/
	var projState=$('#projState').val();
	if(projState=='I')
		$("#btnSave").hide();
	/*Added by Roshni for T25IT-415 ends*/
	
	/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
	if($('#recTypeVal').val()=='TSA')
	{
		$("#tCase").show();
		$("#tset").hide();
	}
   else{
	$("#tset").show();
	$("#tCase").hide();
   }
	if($('#type').val()==1){
	$('#tasktype').hide();
	}
	/*if($('#type').val()==2){
		populateTestCaseMapping($('#testSet').val());
	}*/
	/*Added by Gangadhar Badagi for TENJINCG-292 ends*/
	var hoursVal= $("#btntime").val();
	var creationDate=$( "#datepicker" ).val();
	var regclient=$('#currentClient').val();

	var optBrowser = $('#currentBrowser').val();
	$('#lstBrowserType').children('option').each(function(){
		var val = $(this).val();
		if(val == optBrowser){
			$(this).attr({'selected':true});
		}
	});
	/*changed by sahana for improvement #TENJINCG-61: starts*/
	var optScr = $('#currentScreenShot').val();
	$('#screenShot').children('option').each(function(){
		var val = $(this).val();
		if(val == optScr){
			$(this).attr({'selected':true});
		}
	});

	/*changed by sahana for improvement #TENJINCG-61: ends*/
	$(function() {
		$( "#datepicker" ).datepicker({ 
			/*   Changed by sahana for the improvement of TENJINCG-7: starts  */
			showOn: 'button',
			buttonText: 'Show Date',
			buttonImageOnly: true,
			buttonImage: './images/button/calendar.png',
			constrainInput: true,
			/*   Changed by sahana for the improvement of TENJINCG-7: ends  */
			dateFormat: 'dd-M-yy',
			minDate:0,

		});

	});

	populateClients(regclient);

	$(window).load(function(){
		function setSelectWidth(selector) {
			var sel = $(selector);
			var tempSel = $("<select style='display:none'>").append( $("<option>").text( sel.find('option:selected').text() ) );
			tempSel.appendTo( $("body") );

			sel.width( tempSel.width() );

			tempSel.remove();
		}

		$(function() {
			setSelectWidth("#txtTestSet");

			$("#txtTestSet").on("change", function() {
				setSelectWidth($(this));
			});
		});

	});
	$(window).load(function(){
		function setSelectWidth(selector) {
			var sel = $(selector);
			var tempSel = $("<select style='display:none'>").append( $("<option>").text( sel.find('option:selected').text() ) );
			tempSel.appendTo( $("body") );

			sel.width( tempSel.width() );

			tempSel.remove();
		}

		$(function() {
			setSelectWidth("#screenShot");

			$("#screenShot").on("change", function() {
				setSelectWidth($(this));
			});
		});

	});
	/* Changed by Leelaprasad for the defect T25IT-132 starts*/
	$('#txtTestCase').on('change', function() {
		var mode=$('#txtTestCase option:selected').data("mode");
		if(mode=="API"){
			$('#txtClient').hide();
			$('#txtBrowser').hide();
		}else{
			$('#txtClient').show();
			$('#txtBrowser').show();
		}
	});
	$('#txtTestSet').on('change', function() {
		var mode=$('#txtTestSet option:selected').data("mode");
		if(mode=="API"){
			$('#txtClient').hide();
			$('#txtBrowser').hide();
		}else{
			$('#txtClient').show();
			$('#txtBrowser').show();
		}
		/*Added by Padmavathi for TENJINCG-651 starts*/
		populateTestCaseMapping($('#txtTestSet').val());
		/*Added by Padmavathi for TENJINCG-651 ends*/
	});
	/* Changed by Leelaprasad for the defect T25IT-132 ends*/
	/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
	$('#type').on('change', function() {
		var val=$('#type').val();
		/* Changed by Leelaprasad for the defect T25IT-132 starts*/
			$('#txtClient').show();
			$('#txtBrowser').show();
	/* Changed by Leelaprasad for the defect T25IT-132 ends*/
	/*	Added by Gangadhar Badagi to hide testCase,testSet when type is selected as select one starts*/
		if(val==0){
			$('#tCase').hide();
			$('#tset').hide();
			$('#tasktype').hide();
		}
		/*	Added by Gangadhar Badagi to hide testCase,testSet when type is selected as select one ends*/
		if(val==1){
			$('#tCase').show();
			$('#tset').hide();
			var tcId=$('#tcId').val();
			populateAllTestCases(tcId);
			$('#tasktype').hide();
			/*$('#tHead').hide();
			$('#tblNaviflowModules_body').hide();*/
		}
		if(val==2)
			{
			$('#tset').show();
			$('#tCase').hide();
			populateAllTestSets();
			/*Commented by Padmavathi for TENJINCG-651 starts*/
			/*var testSet=$('#txtTestSet').val();
			populateTestCaseMapping(testSet);
			populateTestSetInfo(testSet);
			populateTestCaseMapping(testSet);
			$('#pagination_block').show();
			$('#txtTestSet').on('change', function(e) {
				e.preventDefault();
				var tsId=$('#txtTestSet').val();
				populateTestCaseMapping(tsId);
				paginate();
			});*/
			/*Commented by Padmavathi for TENJINCG-651 ends*/
			}
	});
	/*Added by Gangadhar Badagi for TENJINCG-292 ends*/
	
	
	populateHoursInfo($('#btntime').val(),creationDate);
	populateMinutesInfo($('#btntime1').val(),creationDate);
	var schId1= $('#btnSchId').val();
	var prjId=$('#projId').val();
	
	/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
	if($('#type').val()==1){
		var tcId=$('#tcId').val();
		populateAllTestCases(tcId);
		
	}
	if($('#type').val()==2){
		/*Added by Gangadhar Badagi for TENJINCG-292 ends*/
		var testSet=$('#txtTestSet').val();
		populateTestSetInfo(testSet);
		populateTestCaseMapping(testSet);
		$('#pagination_block').show();
		/*$('#txtTestSet').on('change', function(e) {
			e.preventDefault();
			var tsId=$('#txtTestSet').val();
			populateTestCaseMapping(tsId);
			paginate();
		});*/
	}
	
	/*changed by sahana for req#TEN-72 starts*/
	/*$('#taskName').keyup(function (){
		var inputVal = $(this).val();
		var regex = new RegExp(/[^a-zA-Z0-9]/);
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			alert('Please enter only alphanumeric characters');
			$(this).val('');
		}
	});*/
	/*changed by sahana for req#TEN-72 Ends*/

	$('#btnSave').click(function(){
		/*Modified by Preeti for TV2.8R2-11 starts*/
		/*var validated = validateForm('edit_schedule');

		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
		}*/
		var task = $('#taskName').val();
		if(!task){
			showMessage('Task Name is required.','error');
			return false;
		}
		/*Modified by Preeti for TV2.8R2-11 ends*/
		/*changed by sahana for DEFECT#TEN-79 starts*/
		/* Changed by Leelaprasad for the defect T25IT-132 starts*/
		var mode='';
		if($('#type').val()==2){
			mode=$('#txtTestSet option:selected').data("mode");
			
		}else{
			 mode=$('#txtTestCase option:selected').data("mode");
		}
		if(mode!="API"){
			var client=$('#txtclientregistered').val();
			if(client === '-1' || client ==='' || client == undefined) {
				showMessage('Client is required.','error');
				return false;
			}
		}
		/* Changed by Leelaprasad for the defect T25IT-132 ends*/
		var selDate=$('#datepicker').val();
		if(!selDate){
			showMessage('Date is required.','error');
			return false;
		}
		/* Added by Padmavathi for TENJINCG-1014 starts */
		if(!validateDate(selDate)){
			showMessage('Please enter valid Date in this format "DD-MMM-YYYY".','error');
			return false;
		}
		/* Added by Padmavathi for TENJINCG-1014 ends */
		var selhour=$('#btntime').val();
		var selMinute=$('#btntime1').val();
		var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];

		var today=new Date();
		var curDate=today.getDate()+'-'+months[today.getMonth()]+'-'+today.getFullYear();
		if((new Date(curDate).getTime()) >= (new Date(selDate).getTime())){
			if((new Date(curDate).getTime()) == (new Date(selDate).getTime())){
				var curHour=today.getHours();
				var curMinute=today.getMinutes();
				/*changed by sahana to check hour & min is >= current time:Starts*/
				if(selhour<=curHour && selMinute<curMinute){
					alert("Selected Time should be greater than or equal to Current Time");
					return false;
				}
				/*changed by sahana to check hour & min is >= current time:ends*/
				/*if(selhour>=curHour && selMinute>=curMinute){

				}*/
				/*else{
					return false;
				}*/

			}
			else{
				alert("Start Date should be greater than or equal to Today's Date");
				return false;
			}
		}
		/*changed by sahana for defect #TEN-79 ends*/
		var selectedTab=$('#tabselectedType').val();
		var obj = new Object();
		if(!(selectedTab=='Completed Tasks')){
			obj.schId = $('#btnSchId').val();
		}

		obj.date = $('#datepicker').val();
		obj.time = $('#btntime').val()+":"+$('#btntime1').val();
		obj.task = $('#txtTask').val();
		/* Changed by Leelaprasad for the defect T25IT-132 starts*/
		/*obj.client = $('#txtclientregistered').val();*/
		if(mode!="API"){
		obj.client = $('#txtclientregistered').val();
		}else{
			obj.client="-1";
		}
		/* Changed by Leelaprasad for the defect T25IT-132 ends*/
		obj.status = $('#txtStatus').val();
		obj.crtBy = $('#txtCrtBy').val();
		obj.crtOn = $('#txtCrtOn').val();
		obj.browser=$('#lstBrowserType').val();
		
		/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
		var type=$('#type').val();
		/*Added by Gangadhar Badagi to send error message while trying to update scheduler without type starts*/
		if(type==0){
			/*showMessage("Can't update without type", 'error');*/
			showMessage("Type is required", 'error');
			return false;
		}
		/*Added by Gangadhar Badagi to send error message while trying to update scheduler without type ends*/
		if(type==1){
			var tcIdSelected=$('#tcId').val();
			var tcId=$('#txtTestCase').val();
			if(tcId=='-1'){
				showMessage("Test Case is required",'error');
				return false;
			}else{
				if(tcIdSelected!=tcId){
					var jsonObj=createTs(tcId);
					var testSetId=jsonObj.testSetId;
					obj.testSetId=testSetId;
				}
			else{
				obj.testSetId=$('#testSet').val();;
				}
			}
		}
		if(type==2){
			if($('#txtTestSet').val()=='-1'){
				showMessage("Test Set is required",'error');
				return false;
			}
			else{
				obj.testSetId=$('#txtTestSet').val();
			}
		}
		/*Changed by Leelaprasad for the requirement defect fix T25IT-132 starts*/
		obj.type=type;
		/*Changed by Leelaprasad for the requirement defect fix T25IT-132 ends*/
		/*Added by Gangadhar Badagi for TENJINCG-292 ends*/
		
		/*changed by sahana for req#TEN-72 starts*/
		obj.taskName=$('#taskName').val();
		/*changed by sahana for req#TEN-72 ENDS*/
		/*changed by sahana for improvement #TENJINCG-61: starts*/
		obj.screenShot=$('#screenShot').val();
		/*changed by sahana for improvement #TENJINCG-61: ends*/
		/*changed by sahana for req#TEN-145 starts*/
		obj.schRecur='N';
		/*changed by sahana for req#TEN-145 ends*/
		if(obj.task!='Execute'){
			obj.projectId=0;
		}
		else{
			obj.projectId=$('#projId').val();
		}
		var jsonString = JSON.stringify(obj);
		var csrftoken_form=$('#csrftoken_form').val();
		if(selectedTab=='Completed Tasks'){
			$.ajax({
				url:'SchedulerServlet',
				/*Added by Gangadhar Badagi for T25IT-391 starts*/
				type:'POST',
				/*Added by Gangadhar Badagi for T25IT-391 ends*/
				/*Modified by Padmavathi for V2.8-157 starts*/
				/*data:{param:"NEW_SCH",json:jsonString,functions:"TestCase"},*/
				data:{param:"NEW_SCH",json:jsonString,functions:"TestCase",emailParam:"reschedule",csrftoken_form:csrftoken_form},
				/*Modified by Padmavathi for V2.8-157 ends*/
				dataType:'json',
				success:function(data){
					if(data.status == 'success'){
						showMessage(data.message,'success');
						/*Added by Ashiki for V2.8-63 starts*/
						/*window.setTimeout(function(){window.location='SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id='+obj.projectId;},500)*/
						/*Added by Ashiki for V2.8-63 starts*/

					}else{
						showMessage(data.message,'error');
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(errorThrown,'error');
				}
			});
		}else{
			$.ajax({
				url:'SchedulerServlet',
				data:{param:"EDIT_SCH",json:jsonString,functions:"TestCase",csrftoken_form:csrftoken_form},
				type: 'POST',
				async:false,
				dataType:'json',
				success:function(data){
					var status = data.status;
					if(status =='success'){			
						showMessage(data.message,'success');
						/*Added by Ashiki for V2.8-63 starts*/
						/*window.setTimeout(function(){window.location='SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id='+obj.projectId;},500)*/
						/*Added by Ashiki for V2.8-63 ends*/
					}else{
						showMessage(data.message, 'error');				
					}
				},

				error:function(xhr, textStatus, errorThrown){
					showMessage(data.message, 'error');			
				}
			});
		}
	});

	$('#datepicker').on('change', function() {
		var name=$('#datepicker').val(); 
		hoursVal=$("#btntime").val();
		hoursVal=populateHours(Date.parse(name));
		populateMinutes(Date.parse(name));
	});

	$('#btntime').on('change', function(){	
		var html="<option value='00'>00</option>";
		var timeval=$(this).val();
		if(hoursVal!=timeval){
			if(timeval!=null){
				for(var i=1;i<12;i++){
					if(i==1)
						html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";
					else
						html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
				}
				$('#btntime1').html(html);
			}
		}
		else{
			var name=$('#datepicker').val(); 
			hoursVal=$("#btntime").val();
			hoursVal=populateHours(Date.parse(name));
			populateMinutes(Date.parse(name));
		}
	});



	$('#btnBack').click(function(){	

		window.location.href='SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id='+prjId;
	});
		/* Changed by Leelaprasad for the defect T25IT-132 starts*/
	var mode='';
	if($('#type').val()==2){
		mode=$('#txtTestSet option:selected').data("mode");
		
	}else{
		 mode=$('#txtTestCase option:selected').data("mode");
	} 
	if(mode=="API"){
		$('#txtClient').hide();
		$('#txtBrowser').hide();
	}else{
		$('#txtClient').show();
		$('#txtBrowser').show();
	}
			/* Changed by Leelaprasad for the defect T25IT-132 ends*/
	/*Added by Preeti for TENJINCG-928 starts*/
	window.setInterval(function(){
		var today = new Date();
		$('#date').text(today.toShortFormat());
	}, 1);
	Date.prototype.toShortFormat = function() {

	    var month_names =["01","02","03",
	                      "04","05","06",
	                      "07","08","09",
	                      "10","11","12"];
	    
	    var day = this.getDate();
	    var month_index = this.getMonth();
	    var year = this.getFullYear();
	    var hours = this.getHours();
	    var min = this.getMinutes();
	    var sec = this.getSeconds();
	    
	    return "" + year + "-" + month_names[month_index] + "-" + day +" " + hours +":" +min +":"+sec +" GMT+0530 (India Standard Time)";
	}
	/*Added by Preeti for TENJINCG-928 ends*/
});


/*changed by nagababu for Defect #TEN-137:  Starts*/
function populateHoursInfo(hours,creationDate){
	var newHour="00";
	var html='';
	var curHour=new Date().getHours();
	var startHour;
	var today=new Date();
	var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
	var curDate=today.getDate()+'-'+months[today.getMonth()]+'-'+today.getFullYear();

	if((new Date(curDate).getTime())< (new Date(creationDate).getTime())){
		startHour="00";
	}
	else{
		if(parseInt(curHour)<=parseInt(hours)){
			startHour=curHour;
		}
		else{
			startHour=hours;
		}
	}
	$('#btntime').val('');
	for(var i=parseInt(startHour);i<24;i++){
		if(parseInt(i)<10){
			newHour="0"+parseInt(i);
		}
		else{
			newHour=i;
		}
		if(i==hours){
			html = html + "<option value='" +newHour + "' selected='selected'>" + newHour + "</option>";
		}
		else{
			html = html + "<option value='" + newHour+ "'>" + newHour + "</option>";
		}
	}
	$('#btntime').html(html);
}
function populateMinutesInfo(mins,creationDate){
	var newMin="00";
	var html='';
	var curMinute=new Date().getMinutes();
	var startMinute;
	var today=new Date();
	var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];

	var curDate=today.getDate()+'-'+months[today.getMonth()]+'-'+today.getFullYear();

	if((new Date(curDate).getTime()) < (new Date(creationDate).getTime())){
		startMinute="00";
	}
	else{
		if(parseInt(curMinute)<=parseInt(mins)){
			var n=curMinute;
			var rem=curMinute%5;
			if(rem!=0){
				if(curMinute<10){
					startMinute=curMinute+(5-rem);
					startMinute="0"+startMinute;
				}
				else{
					startMinute=curMinute+(5-rem);
				}
			}
			else{
				startMinute=curMinute;
			}

		}
		else{
			startMinute=mins;
		}
	}

	for(var i=parseInt(startMinute);i<60;i+=5){
		if(parseInt(i)<10){
			newMin="0"+parseInt(i);
		}
		else{
			newMin=i;
		}
		if(i==mins){
			html = html + "<option value='" +newMin + "' selected='selected'>" + newMin + "</option>";
		}
		else{
			html = html + "<option value='" + newMin+ "'>" + newMin + "</option>";
		}
	}
	$('#btntime1').html(html);
}
/*changed by nagababu for Defect #TEN-137:  ends*/
function populateClients(regclient){

	var jsonObj = fetchAllClients();
	var status = jsonObj.status;
	clearMessages();
	if(status == 'SUCCESS'){
		var html ="";
		var jArray = jsonObj.clients;

		for(i=0;i<jArray.length;i++){
			var client = jArray[i];
			if(client.name==regclient)
				html = html + "<option  value='" + client.name + "' selected='selected'>" + client.name + "</option>";
			else
				html = html + "<option  value='" + client.name + "'>" + client.name + "</option>";
		}

		$('#txtclientregistered').html(html);

	}else{
		showMessage(jsonObj.message, 'error');
	}
}
/*changed by sahana for defect #TEN-139  :starts*/
function populateHours(date1){
    var hoursVal= "";
	var dateval=new Date(date1);
	var month=dateval.getMonth()+1;
	var finaldate=dateval.getDate()+"-"+month+"-"+dateval.getFullYear();
	var curmonth=new Date().getMonth()+1;
	var currDate=new Date().getDate()+"-"+curmonth+"-"+new Date().getFullYear();
	var html ="";
	var getMin=new Date().getMinutes();
	var counter=0;
	var currentHour=new Date().getHours();
	var newHour;
	if(getMin>55 && getMin<=60)
	{
		for(var i=0;i<24;i++){
			if(currentHour==parseInt(i))
			{	
				if(parseInt(i)<10){
					newHour="0"+parseInt(i+1);
				}
				else{
					newHour=i+1;
				}
				if(i==currentHour){
					html = html + "<option value='" +newHour + "' selected='selected'>" + newHour + "</option>";
				}
				else{
					html = html + "<option value='" + newHour+ "'>" + newHour + "</option>";
				}
				i=i+1;
			}
			else if(i>currentHour+1){
				if(parseInt(i)<10){
					newHour="0"+parseInt(i);
				}
				else{
					newHour=i;
				}
				if(i==currentHour){
					html = html + "<option value='" +newHour + "' selected='selected'>" + newHour + "</option>";
				}
				else{
					html = html + "<option value='" + newHour+ "'>" + newHour + "</option>";
				}


			}
			/*Added by  Gangadhar Badagi to schedule the task between 55-60 minutes starts*/
			else{
				
				var newTime="";
				if(i<=9)
					newTime ="0"+i;
				else
					newTime=i;
				if(finaldate==currDate){
					if(newTime>=new Date().getHours()){

						if(counter==0){
							hoursVal=newTime;
							counter++;
						}
						html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
					}
				}
				else{	
					if(counter==0){
						hoursVal=newTime;
						counter++;
					}
					html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
				}
			}
			/*Added by  Gangadhar Badagi to schedule the task between 55-60 minutes ends*/

		}
	}
	else
	{
		for(var i=0;i<24;i++){
			var newTime="";
			if(i<=9)
				newTime ="0"+i;
			else
				newTime=i;
			if(finaldate==currDate){
				if(newTime>=new Date().getHours()){

					if(counter==0){
						hoursVal=newTime;
						counter++;
					}
					html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
				}
			}
			else{	
				if(counter==0){
					hoursVal=newTime;
					counter++;
				}
				html = html + "<option value='" + newTime+ "'>" + newTime + "</option>";
			}

		}
	}
	$('#btntime').html(html);
	return hoursVal;
}
function populateTestSetInfo(testSet){
	var jsonObj = fetchAllTestSets();

	$('#txtTestSet').html('');
	var status = jsonObj.status;
	if(status== 'SUCCESS'){
		var html = "";
		var jarray = jsonObj.tSets;
		for(i=0;i<jarray.length;i++){
			var set = jarray[i];
					/* Changed by Leelaprasad for the defect T25IT-132 starts*/
		/*if(set.id==testSet)
				html = html + "<option value='" + set.id + "' selected='selected'>" + set.name+"</option>";
			else
				html = html + "<option value='" + set.id +"'>" + set.name + "</option>";
		}*/
			if(set.id==testSet)
				html = html + "<option value='" + set.id + "' data-mode='"+set.mode+"' selected='selected'>" + set.name+"</option>";
			else		
				html = html + "<option value='" + set.id +"' data-mode='"+set.mode+"'>" + set.name + "</option>";
					}
				/* Changed by Leelaprasad for the defect T25IT-132 ends*/

		$('#txtTestSet').html(html);
	}else{
		showMessage(jsonObj.message,'error');
	}
}
function populateMinutes(date1){
	var html="";
	var dateval=new Date(date1);
	var month=dateval.getMonth()+1;
	var finaldate=dateval.getDate()+"-"+month+"-"+dateval.getFullYear();
	var curmonth=new Date().getMonth()+1;
	var currDate=new Date().getDate()+"-"+curmonth+"-"+new Date().getFullYear();
	var counter=0;
	var getHour=$('#btntime').val();
	var getMin=new Date().getMinutes();
	if(parseInt(getMin)>55 && parseInt(getMin)<=60)
	{
		for(var i=0;i<12;i++){
			if(i==0)
				html=html + "<option value='00'>00</option>";
			else if(i==1)
				html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";			
			else
				html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
		}
	}
	else{
		for(var i=0;i<12;i++){
			if(finaldate==currDate){
				if((i*5)>=parseInt(getMin)){
					if((i*5)<10)
						html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";			
					else
						html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
				}
			}
			else{
				if(i==0)
					html=html + "<option value='00'>00</option>";
				else if(i==1)
					html=html + "<option value='"+"0"+(i*5)+ "'>"+"0"+(i*5) + "</option>";			
				else
					html=html + "<option value='" + (i*5)+ "'>" + (i*5) + "</option>";
			}
		}
	}
	$('#btntime1').html(html);
}

/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
function populateAllTestCases(testCaseId){

	var jsonObj = fetchAllProjectTestCases();

	var status = jsonObj.status;
	clearMessages();
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jarray = jsonObj.tcs;

		for(i=0;i<jarray.length;i++){
			var testCase = jarray[i];
			/* Changed by Leelaprasad for the defect T25IT-132 starts*/
			/*if(testCase.recid==testCaseId)
				html = html + "<option value='" + testCase.recid + "' selected='selected'>" + testCase.name+"</option>";
			else
				html = html + "<option value='" + testCase.recid +"'>" + testCase.name + "</option>";
		}*/
			if(testCase.recid==testCaseId)
				html = html + "<option value='" + testCase.recid + "' data-mode='"+testCase.mode+"' selected='selected'>" + testCase.name+"</option>";
			else
				html = html + "<option value='" + testCase.recid +"' data-mode='"+testCase.mode+"'>" + testCase.name + "</option>";
			
		}
		/* Changed by Leelaprasad for the defect T25IT-132 starts*/
		$('#txtTestCase').html(html);

	}else{
		showMessage(jsonObj.message, 'error');
	}
}

function populateAllTestSets(){

	var jsonObj = fetchAllTestSets();

	var status = jsonObj.status;
	clearMessages();
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jArray = jsonObj.tSets;

		for(i=0;i<jArray.length;i++){
			var set = jArray[i];
			html = html + "<option value='" + set.id +"' data-mode='"+set.mode+"'>" + set.name + "</option>";
		}

		$('#txtTestSet').html(html);

	}else{
		showMessage(jsonObj.message, 'error');
	}
}
/*Added by Gangadhar Badagi for TENJINCG-292 ends*/

/*changed by sahana for defect #TEN-139  :ends*/
function populateTestCaseMapping(tsId){

	$.ajax({
		url:'TestSetServlet',
		/*data:{param:"mapped_tcs1",t1:tsId},*/
		data:{param:"populate_tcs",t1:tsId},
		dataType:'json',
		success:function(data){
			if(data.status == 'SUCCESS'){
				var testCases=data.tests;
				var html='<thead><tr><th>Test Case Id</th><th>Test Case Name</th></tr></thead>';
				html=html+"<tbody id='tblNaviflowModules_body'>";
				for(i=0;i<testCases.length;i++){
					var addClass='';
					if(i%2==0)
						addClass = 'odd';
					else
						addClass = 'even';
					var tc = testCases[i];
					html = html + "<tr class='"+addClass+"'><td>"+tc.id+"</td><td>"+tc.TC_NAME +"</td></tr>";
				}
				html=html+"</tbody>";
				$('#pagination_block').show();
				$('#tblNaviflowModules').html('');
				$('#tblNaviflowModules').html(html);
				$('#tasktype').show();
				$('#tblNaviflowModules').dataTable();
				$('#tblNaviflowModules').DataTable().destroy();
				$('#tblNaviflowModules').dataTable();
				/*paginate();*/
			}else{
				showMessage(data.message,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown,'error');
		}
	});
	/*paginate();*/
	}

/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
function createTs(tcId)
{
	var jsonObj=null;
	$.ajax({
		url:'SchedulerServlet',
		data:'param=create_testset&tc=' + tcId,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			obj.status = "ERROR";
			obj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});
	
	return jsonObj;
	
	}
/*Added by Gangadhar Badagi for TENJINCG-292 ends*/
