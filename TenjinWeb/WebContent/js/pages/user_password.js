/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  user_password.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 07-04-2021          Added by Prem          
* 
*/

$(document).on('click', '#btnBack', function() {
			var userid=$('#userid').val();
				window.location.href='TenjinUserServlet?t=view&key='+userid;
		});
		$(document).ready(function(){
			mandatoryFields('change-password-form');
			
			$("#btnChangeFirst").hide();
			var entryPoint=$("#entryPoint").val();
			if(entryPoint=='first'){
				
				$("#btnBack").hide();
				$("#btnChangeFirst").show();
				$("#btnChange").hide();
				$('.toolbar').hide();
				
				
				
			}
			$('#btnChangeFirst').click(function(){
				var changeReason=$('#changeReason').val();
				
				var newPassword=document.getElementById('newPassword');
				
				if(!CheckPassword(newPassword)){
					/*var hashObj1 = new jsSHA("SHA-256", "TEXT", {numRounds: 1});
					hashObj1.update(newPassword.value);
					var hash1 = hashObj1.getHash("HEX");
					 $('#newPassword').val(hash1);*/
					showMessage("Please check the password rules","error");
					return false;
					 
				}
				/*else{
					showMessage("Please check the password rules","error");
					return false;
				}*/
				
				 var confPwd=document.getElementById('confirmPassword');
				 
				 if(!CheckPassword(confPwd)){
					 /*var hashObj2 = new jsSHA("SHA-256", "TEXT", {numRounds: 1});
						hashObj2.update(confPwd.value);
						var hash2 = hashObj2.getHash("HEX");
						 $('#confirmPassword').val(hash2);*/
					 showMessage("Please check the password rules for confirmPassword ","error");
					 return false;
						 
					}
					/*else{
						showMessage("Please check the password rules for confirmPassword ","error");
						return false;
					}*/
				 
					 /*var curntPwd=document.getElementById('currentPassword');
						var hashObj3 = new jsSHA("SHA-256", "TEXT", {numRounds: 1});
						hashObj3.update(curntPwd.value);
						var hash3 = hashObj3.getHash("HEX");
						 $('#currentPassword').val(hash3);*/
				
				var question1=$('#question1').val();
				var question2=$('#question2').val();
				var changeReason=$('#changeReason').val();
				if(!changeReason){
					showLocalizedMessage('Please provide a reason to change password', 'error');
					return false;
				}
				if(question1==-1){
					showLocalizedMessage('Please choose a security question1', 'error');
					return false;
				}
				if(question2==-1){
					showLocalizedMessage('Please choose a security question2', 'error');
					return false;
				}
				if(question1==question2){
					showLocalizedMessage('Please choose a two different security question', 'error');
					return false;
				}
				
			});
			$('#btnChange').click(function(){
				/*Modified by Prem for VAPT issue fix starts*/
var newPassword=document.getElementById('newPassword');
				
				if(!CheckPassword(newPassword)){
					/*var hashObj1 = new jsSHA("SHA-256", "TEXT", {numRounds: 1});
					hashObj1.update(newPassword.value);
					var hash1 = hashObj1.getHash("HEX");
					 $('#newPassword').val(hash1);*/
					showMessage("Please check the password rules ","error");
					return false;
					 
				}
				/*else{
					showMessage("Please check the password rules ","error");
					return false;
				}*/
				
				 var confPwd=document.getElementById('confirmPassword');
				 
				 if(!CheckPassword(confPwd)){
					 /*var hashObj2 = new jsSHA("SHA-256", "TEXT", {numRounds: 1});
						hashObj2.update(confPwd.value);
						var hash2 = hashObj2.getHash("HEX");
						 $('#confirmPassword').val(hash2);*/
					 showMessage("Please check the password rules for confirmPassword ","error");
					 return false;
						 
					}
					/*else{
						showMessage("Please check the password rules for confirmPassword ","error");
						return false;
					}*/
				 
					 /*var curntPwd=document.getElementById('currentPassword');
						var hashObj3 = new jsSHA("SHA-256", "TEXT", {numRounds: 1});
						hashObj3.update(curntPwd.value);
						var hash3 = hashObj3.getHash("HEX");
						 $('#currentPassword').val(hash3);*/
						 /*Modified by Prem for VAPT issue fix Ends*/
				if(!changeReason){
					showLocalizedMessage('Please provide a reason to change password', 'error');
					return false;
				}
			});
			
		});