/**
 * <!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  live_extr_detail_view.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India



/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 * 
 * 01-09-2017           Leelaprasad             Defect T25IT-370
 * 03-10-2017			Pushpalatha				TENJINCG-358
*/

$(document).ready(function(){
	$('.display').dataTable();
	/*Added by Pushpalatha for TENJINCG-358 starts*/
	var progressPercentage = $('#progpcnt').val();
	progressBar(progressPercentage, $('#progressBar'));
	
	var loop; 
	var autoRefreshFlag = $('#autoRefreshFlag').val();
	if(autoRefreshFlag == 'yes'){
		loop = window.setInterval(function(){window.location.href='ExtractorServlet?param=extractor_progress&runid=' + $('#runId').val()+'&func='+$('#function').val()}, 5000);
	}else{
		$('#toggleRefresh').hide();
	}
	/*Added by Pushpalatha for TENJINCG-358 ends*/
	
	$('#btnBack').click(function(){
		var runId = $('#runId').val();
		window.location.href='ExtractorServlet?param=extractor_progress&runid=' + $('#runId').val();
	});
	/*<!-- Changed by Leelaprasad for the requirement defect fix T25IT-370 starts -->*/
	$('#download').hide();
	var caption=$('#btnField').val();
	if(caption=='Completed, Success'){
	$('#download').show();
	}
	/*<!-- Changed by Leelaprasad for the requirement defect fix T25IT-370 ends -->*/
	$('#download').click(function(){
		clearMessages();
		var selectedRecords='';
		$('#extr_records_table').find('input[type="checkbox"]:checked').each(function () {
			if(this.id != 'chk_all'){
				selectedRecords = selectedRecords + $(this).attr('tduid') + ',';
			}
		});
		
		if( selectedRecords.charAt(selectedRecords.length-1) == ','){
			selectedRecords = selectedRecords.slice(0,-1);
		}
		
		//alert(selectedRecords);
		if(selectedRecords.length < 1){
			showMessage('Please select at least one record');
			return false;
		}
		
		generateOutput($('#runId').val(), $('#function').val(), selectedRecords);
	});
});