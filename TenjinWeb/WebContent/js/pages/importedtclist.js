/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  importedtclist.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
*
* 10-Jan-2017			Manish					TENJINCG-59
*  08-02-2017		   	  Manish				TENJINCG-100
*  29-08-2017		   	  Manish				to show loader
*  20-03-2017			Pushpalatha				TENJINCG-611
*  
*/


$(document).ready(function(){
	
	
	
	$('#tblTestSteps td').each(function(){
		$(this).find("input.stepCheckbox:checkbox:checked").val()
	});
	
	
	
	$('#tsListDiv').hide();
	$('#dmLoader').hide();
	
	var selectedItems = [];
	$('#btnImport').click(function(){
		selectedTcs='';
		var finalSteps = '';
		/*added by manish to show loader*/
		$('#dmLoader').show();
	$('#tblTestCases').find('input[type="checkbox"]:checked').each(function () {
		       //this is the current checkbox
		       if(this.id != 'tbl-select-all-rows-tc'){
		    	   selectedTcs = selectedTcs + this.id + ',';
		       }
		       
		});
	
	
	$('#tblTestSteps').find('input[type="checkbox"]:checked').each(function () {
		       //this is the current checkbox
			
		       if(this.id != 'tbl-select-all-rows-ts'){
		    	   finalSteps = finalSteps + this.id + ',';
		       }
		       
		});
		/*if(selectedItems!=null&&selectedItems.length>0){*/
			for(var i=0; i<selectedItems.length;i++){
				var obj1 = {}; 
				finalSteps = finalSteps+ selectedItems[i].stepId + ',';
			}
		/*}*/
		/*changed by manish for defect TENJINCG-100 on 08-02-2017 starts*/
		/*window.location.href='TestCaseServlet?param=persist_imported_testcases&tcids='+selectedTcs+'&tsteps='+finalSteps;*/
		if((selectedTcs!=null&&selectedTcs.length>0) ||(finalSteps!=null &&finalSteps.length>0)){
			window.location.href='TestCaseServletNew?t=persist_imported_testcases&tcids='+selectedTcs+'&tsteps='+finalSteps;
		}else{
			showMessage('There are no test cases or test steps to import.','error');
		}
		/*changed by manish for defect TENJINCG-100 on 08-02-2017 ends*/
		/*added by shruthi for TCGST-50 starts*/
		$('#dmLoader').hide();
		/*added by shruthi for TCGST-50 ends*/
	});
	
	
	$('.tc_anchor').click(function() {
		var tcId = $(this).attr('recid');
		
		var selectedTSteps = '';
		var flag=false;
		
		
		$('#tblTestSteps').find('input[type="checkbox"]:checked').each(function () {
		       //this is the current checkbox
		       if(this.id != 'tbl-select-all-rows-ts'){
		    	   selectedTSteps = selectedTSteps + this.id + ',';
		       }
		       var obj = new Object();
		      
		       obj.id=tcId;
		       obj.stepId=this.id;
		       var found=false;
		    	   for(var i=0; i<selectedItems.length;i++){
			    	   if(selectedItems[i].stepId == obj.stepId && selectedItems[i].id == obj.id){
			    		   found=true;
			    	   }
			    	  
			       }
		    	   if(!found){
		    		   selectedItems.push(obj);
		    	   }
		     
		      
		});
		var selectedStepsArray = selectedTSteps.split(',');
		for(var i=0; i<selectedItems.length;i++){
			var found=false;
			for(var j=0; j<selectedStepsArray.length;j++){
				if(selectedStepsArray[j]==selectedItems[i].stepId){
					found=true;
				}
			}
			if(!found){
				if(tcId==selectedItems[i].id){
					delete(selectedItems[i].stepId);
					delete(selectedItems[i].id);
				}
				
			}
			
		}
		
		for(var s=0; s<selectedStepsArray.length; s++){
			var k = selectedStepsArray[s];
		}
		
		$('#dmLoader').show();
		
		var jsonArray = [];
		var html = "<thead>"+
		"<tr>"+
		"<th class='tiny nosort'><input type='checkbox'"+
		"class='tbl-select-all-rows' id='tbl-select-all-rows-ts'"+
		"title=Select All Steps /></th>"+
		"<th>Test Step ID</th>"+
		"<th>Description</th>"+
		"<th>Application</th>"+
		"<th>Function</th>"+
		"<th>Mode</th>"+
		"</tr>"+
		"</thead>";
		
		/*jsonArray = $(this).attr('tStepArray');*/
		var jsonString= $('#'+tcId).html();
		/*Changed by Pushpalatha for TENJINCG-611 starts*/
		if(jsonString!=null && jsonString!=""){
			jsonArray=JSON.parse(jsonString);
			/*Changed by Pushpalatha for TENJINCG-611 ends*/
			for(var i=0;i<jsonArray.length;i++){
				var module = jsonArray[i];
				
				
				for(var j=0; j<selectedItems.length; j++){
					
					
					if(module.dataId==selectedItems[j].stepId){
						flag=true;
					}
				}	
				if(flag){
					html = html+ "<tr>" +
					"<td class='selector'><input type='checkbox' id='"+module.dataId+"' class='stepCheckbox' checked='true'/></td>" +
					"<td>"+module.dataId+"</td>" +
					"<td>"+module.description+"</td>" +
					"<td>"+module.appName+"</td>" +
					"<td>"+module.moduleCode+"</td>" +
					"<td>"+module.type+"</td></tr>";
				}else{
					html = html+ "<tr>" +
					"<td class='selector'><input type='checkbox' id='"+module.dataId+"' class='stepCheckbox'/></td>" +
					"<td>"+module.dataId+"</td>" +
					"<td>"+module.description+"</td>" +
					"<td>"+module.appName+"</td>" +
					"<td>"+module.moduleCode+"</td>" +
					"<td>"+module.type+"</td></tr>";
				}
					
				flag=false;
				
				
			}
			
		}else{
			html = html+"<tr>"+
			"<td colspan='6'>No steps available for this test case.</td>"+
			"</tr>";
			$('#dmLoader').hide();
		}
		$('#tblTestSteps').html(html);
		$('#dmLoader').hide();
		$('#tsListDiv').show();

	});
	
	

	$('#btnBack').click(function(){
		var projectId = $('#prjId').val();
		/*window.location.href='TestCaseServlet?param=INIT_TC_PAGINATION&pid='+projectId;*/
		window.location.href='TestCaseServletNew?t=list';
	});
});
		
	

	
