/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  tjnmaster.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 3-Nov-2016			Sriram					Fix for Defect#701
 * 16-Nov-16			Sriram					Enhancement#665 (Fetch Operations for App)
 * 24-11-2016           Parveen                	Enhancement#665 
 * 19-Oct-2017			Sriram					TENJINCG-396, TENJINCG-403
 * 06-Dec-2017			Gangadhar Badagi	    TCGST-8
 * 02-02-2018			Pushpalatha 			TENJINCG-568
 * 26-Nov 2018		 	Shivam	Sharma			TENJINCG-904
 * 20-03-2019           Padmavathi              TENJINCG-1014
 * 25-03-2019			Pushpalatha				TENJINCG-968
 * 25-01-2019        	Sahana 			   		pCloudy 
 * 28-05-2019			Prem					V2.8-73 , V2.8-75
 * 22-05-2019			Ashiki					TJN27-2
 * 12-11-2019			Preeti					TENJINCG-1166
 * 20-11-2019			Pushpalatha				TENJINCG-1166
*/

$(document).ready(function(){
	/************************
	 * Select All Checkbox
	 */

	$('.tbl-select-all-rows').change(function(){
		$(this).closest('table').find('input[type=checkbox]').prop('checked', this.checked);
	});
	/*added by manish for req TENJINCG-59 on 03-02-2017 starts*/
	$(document).on('change','.tbl-select-all-rows',function(e){
		$(this).closest('table').find('input[type=checkbox]').prop('checked', this.checked);
	});
	/*added by manish for req TENJINCG-59 on 03-02-2017 ends*/

	/* TENJINCG-396 */
	var tjnStatus = $('#tjn-status').val();
	var tjnMessage = $('#tjn-message').val();
	if(tjnMessage !== undefined && tjnMessage.length > 0) {
		if(tjnStatus.toLowerCase() === 'error') {
			showMessage(tjnMessage,'error');
		}else{
			showMessage(tjnMessage, 'success');
		}
	} 

	$('select').each(function() {
		var defaultValue = $(this).data('defaultValue');
		if(defaultValue !== undefined && defaultValue !== 'undefined' && defaultValue !== '') {
			$(this).children('option').each(function() {
				var val = $(this).val();
				if(val == defaultValue){
					$(this).attr({'selected':true});
				}
			});
		}
	});
	/* TENJINCG-396 */
});

$(document).on('submit', 'form.tovalidate', function(e) {
	var validationStatus = validateForm($(this).attr('name'));
	if(validationStatus !== 'SUCCESS') {
		showMessage(validationStatus, 'error');
		return false;
	}
});

function getAllPages(app,mod){
	var jsonObj;

	$.ajax({
		url:'NaviflowServlet',
		data:'param=fetch_pages&app=' + app + '&mod=' + mod,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			obj.status = "ERROR";
			obj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}

function getAllFields(app,mod,page){
	var jsonObj;

	$.ajax({
		url:'NaviflowServlet',
		data:'param=fetch_fields&app=' + app + '&mod=' + mod + '&page=' + page,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			obj.status = "ERROR";
			obj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}

function fetchAllDomains(userId){
	var jsonObj;

	$.ajax({
		url:'ProjectServlet',
		data:'param=FETCH_DOMAINS&paramval=' + userId,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			obj.status = "ERROR";
			obj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}

function fetchAllProjectTestCases(){

	var jsonObj;
	$.ajax({
		url:'TestCaseServlet',
		data:'param=FETCH_TCS',
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			obj.status = "ERROR";
			obj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;

}

/*Added by  Parveen for  Enhancement#665   on 24-11-2016 starts*/ 
function fetchUserTypeForApp(appId){
	var jsonObj;
	$.ajax({
		url:'AutServlet',
		data:'param=fetch_aut_user_type_for_app&app=' + appId,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact Tenjin Support.";
		}
	});

	return jsonObj;
}
/*Added by Pushpalatha for Req#TENJINCG-568 starts*/
function fetchAutGroups(appId){
	var jsonObj;
	$.ajax({
		url:'AutServlet',
		data:'param=fetch_aut_groups&app=' + appId,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact Tenjin Support.";
		}
	});

	return jsonObj;
}
/*Added by Pushpalatha for Req#TENJINCG-568 ends*/
/*Added by  Parveen for  Enhancement#665   on 24-11-2016 ends*/ 

function fetchProjectsForDomain(domainName, userId){
	var jsonObj;

	$.ajax({
		url:'ProjectServlet',
		data:'param=FETCH_DOMAIN_USER_PROJECTS&d=' + domainName + '&u=' + userId,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			obj.status = "ERROR";
			obj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}

function fetchUnmappedUsers(projectId){
	var jsonObj;
	$.ajax({
		url:'ProjectServlet',
		data:'param=FETCH_UNMAPPED_PROJECT_USERS&p=' + projectId,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			obj.status = "ERROR";
			obj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}

function fetchUnmappedAuts(projectId){
	var jsonObj;
	$.ajax({
		url:'ProjectServlet',
		data:'param=FETCH_UNMAPPED_AUT&p=' + projectId,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			obj.status = "ERROR";
			obj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}

function fetchAllAuts(){
	var jsonObj;
	$.ajax({
		url:'AutServlet',
		data:'param=fetch_all_auts',
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}
//Fix for Defect#701 - Sriram
function fetchAllProjectAuts(){
	var jsonObj;
	$.ajax({
		url:'AutServlet',
		data:'param=fetch_all_prj_auts',
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}//Fix for Defect#701 - Sriram ends

function fetchModulesForApp(appId){
	var jsonObj;
	$.ajax({
		url:'AutServlet',
		data:'param=fetch_modules_for_app&app=' + appId,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}
/* Added by Ashiki for TENJINCG-1275 starts */
function fetchMessageFormatForApp(appId){
	var jsonObj;
	$.ajax({
		url:'MessageServlet',
		data:'param=fetch_messageType_for_app&app=' + appId,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}
/* Added by Ashiki for TENJINCG-1275 ends */
//Added by suhasini on 11-04-2017 starts
function fetchApiModulesForApp(appId){
	var jsonObj;
	$.ajax({
		url:'APIServlet',
		data:'param=fetch_all_apis&redirect=false&lstAppId=' + appId,
		async:false,
		dataType:'json',
		success:function(data){
			console.log('API data ' + data);
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}

//parveen #405 strats
function fetchApiResponseTypeForApp(appId,apiSel,OperationSel){


	var jsonObj;
	$.ajax({
		url:'APIServlet',
		data:'param=FETCH_RESPONSE_TYPES&lstAppId=' + appId+'&apiSel='+apiSel+'&OperationSel='+OperationSel,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;

}
//parveen #405 ends

/******************************
 * Added by Sriram for Enhancement#665 (Fetch Operations for App)
 */
function fetchApiOperationsForApp(appId, code){
	var jsonObj;
	$.ajax({
		url:'APIServlet',
		data:'param=fetch_operations&app=' + appId+ '&code='+ code,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact Tenjin Support.";
		}
	});

	return jsonObj;
}
//Added by suhasini on 11-04-2017 ends


/******************************
 * Added by Sriram for Enhancement#665 (Fetch Operations for App)
 */
function fetchOperationsForApp(appId){
	var jsonObj;
	$.ajax({
		url:'AutServlet',
		data:'param=fetch_operations_for_app&app=' + appId,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact Tenjin Support.";
		}
	});

	return jsonObj;
}
/******************************
 * Added by Sriram for Enhancement#665 (Fetch Operations for App) ends
 */

function fetchModulesForLearning(appId){
	var jsonObj;
	$.ajax({
		url:'AutServlet',
		data:'param=fetch_modules_for_app_learning&app=' + appId,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}

/********************************************
 * Function added by Sriram to support Multiple logins for Learning (Tenjin v2.1)
 */

function fetchAppCredentials(appId){
	var jsonObj;
	$.ajax({
		url:'AutServlet',
		data:'param=fetch_user_aut_for_learning&app=' + appId,
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}

/********************************************
 * Function added by Sriram to support Multiple logins for Learning (Tenjin v2.1) ends
 */
function showMessage(message, type){
	var $msg = $('#user-message');
	$msg.removeClass();
	$msg.html('');
	$msg.append("<div class='msg-icon'>&nbsp</div>");
	/*Modified for TENJINCG-1166 starts*/
	$msg.append("<div class='msg-text'>" + escapeHtmlEntities(message) + "</div>");
	/*Modified for TENJINCG-1166 ends*/
	if(type == 'success'){
		$msg.addClass('msg-success');
	}else if(type == 'progress'){
		$msg.addClass('msg-progress');
	}
	else{
		$msg.addClass('msg-err');
	}

	$msg.show();
}

/* TENJINCG-403 */
function showLocalizedMessage(messageKey, argument, type) {
	$.ajax({
		url:'MessageAjaxServlet?key=' + messageKey + '&arg=' + argument,
		async:true,
		success:function(message) {
			showMessage(message, type);
		},
		error:function(xhr, textStatus, errorThrown) {
			console.log('Could not get localized message: ', errorThrown);
			showMessage(messageKey, type);
		}
	});
}

function getLocalizedMessage(messageKey, argument) {
	var localizedMessage = '';
	$.ajax({
		url:'MessageAjaxServlet?key=' + messageKey + '&arg=' + argument,
		async:false,
		success:function(message) {
			localizedMessage = message;
		},
		error:function(xhr, textStatus, errorThrown) {
			console.log('Could not get localized message: ', errorThrown);
			localizedMessage = messageKey;
		}
	});

	return localizedMessage;
}

function showLocalizedMessageOnElement(messageKey, argument, type, $element) {
	$.ajax({
		url:'MessageAjaxServlet?key=' + messageKey + '&arg=' + argument,
		async:true,
		success:function(message) {
			showMessageOnElement(message, type, $element);
		},
		error:function(xhr, textStatus, errorThrown) {
			console.log('Could not get localized message: ', errorThrown);
			showMessage(messageKey, type, $element);
		}
	});
}
/* TENJINCG-403 ends */
function showMessageOnElement(message, type, $element){
	var $msg = $element;
	$msg.removeClass();
	$msg.html('');
	$msg.append("<div class='msg-icon' style='float:left;'>&nbsp</div>");
	$msg.append("<div class='msg-text'>" + message + "</div>");

	if(type == 'success'){
		$msg.addClass('msg-success');
	}else if(type == 'progress'){
		$msg.addClass('msg-progress');
	}
	else{
		$msg.addClass('msg-err');
	}

	$msg.show();
}

function clearMessages(){
	var $msg = $('#user-message');
	$msg.removeClass();
	$msg.html('');
	$msg.hide();
}

function clearMessagesOnElement($element){
	var $msg = $element;
	$msg.removeClass();
	$msg.html('');
	$msg.hide();
}

/* changed by sahana for Req#TJN_24_03   on 14/09/2016   Starts  */
function fetchAllClients(){
	var jsonObj;
	$.ajax({
		/*Changed by Gangadhar Badagi for TCGST-8 starts */
		/*url:'ClientServlet',*/
		url:'SchedulerServlet',
		/*Changed by Gangadhar Badagi for TCGST-8 ends */
		data:'param=FETCH_ALL_REG_CLIENTS',
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			jsonObj.status = "ERROR";
			jsonObj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}

/* changed by sahana for Req#TJN_24_03   on 14/09/2016   Ends  */

/* changed by sahana for Req#TJN_24_03  on 14/09/2016  starts */
function fetchAllTestSets()
{
	var jsonObj;
	$.ajax({
		url:'TestSetServlet',
		data:'param=fetch_sch_map',
		async:false,
		dataType:'json',
		success:function(data){
			jsonObj = data;
		},
		error:function(xhr, textStatus, errorThrown){
			jsonObj = new Object();
			obj.status = "ERROR";
			obj.message = "An internal error occurred. Please contact your System Administrator.";
		}
	});

	return jsonObj;
}
/* changed by sahana for Req#TJN_24_03  on 14/09/2016  Ends */

//added by shivam sharma for  TENJINCG-904 starts
/*Changed by Sahana for pCloudy */
function populateDevices(clientIp,deviceFarmCheck,applicationType) {
	$.ajax({
		url:'DeviceServlet',
		data:'param=populateDevices&deviceFarmCheck='+deviceFarmCheck+'&clientIP='+clientIp,
		dataType:'json',
		async:false,
		success:function(data){
			var status = data.status;
			$('#listDevice').html('');
			var html = "<option value='-1'>-- Select One --</option>";
			var status = data.status;
			if(status== 'SUCCESS'){
				var jArray = data.devices;
				/*Added by Sahana for pCloudy: Starts*/
				jArray.sort( function( a, b ) {
					return a.deviceName < b.deviceName ? -1 : a.deviceName > b.deviceName ? 1 : 0;
				});
				/*Added by Sahana for pCloudy: Ends*/
				for(i=0;i<jArray.length;i++){
					var json = jArray[i];
					/*Added by Sahana for pCloudy: Starts*/
					/*Modified by Pushpa for TENJINCG-1166 starts*/
					if(applicationType==0)
						html = html + "<option value='" + json.recordId + "'>"+ escapeHtmlEntities(json.deviceName)+"</option>";
					else if(applicationType==3 && (deviceFarmCheck!='Y' || json.deviceName.includes('Android')))
						html = html + "<option value='" + json.recordId + "'>"+ escapeHtmlEntities(json.deviceName)+"</option>";
					else if(applicationType==4 && (deviceFarmCheck!='Y' ||json.deviceName.includes('Ios')))
						html = html + "<option value='" + json.recordId + "'>"+ escapeHtmlEntities(json.deviceName)+"</option>";
					/*Modified by Pushpa for TENJINCG-1166 ends*/
					/*Added by Sahana for pCloudy: Ends*/
				}
				$('#listDevice').html(html);
			}else{
				showMessage(data.MESSAGE,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			showMessage(jsonObj.message, 'error');
		}

	});
}
//added by shivam sharma for  TENJINCG-904 ends

/* Added by Padmavathi for TENJINCG-1014 starts */

//this function Validate date value  this format DD-MMM-YYYY
function validateDate(dateValue) {
	  /*Changed by Ashiki for TJN27-2 starts*/
	  /*var dtRegex = new RegExp("^([0]?[1-9]|[1-2]\\d|3[0-1])-(JAN|FEB|MAR|APR|MAY|JUN|JULY|AUG|SEP|OCT|NOV|DEC)-[1-2]\\d{3}$", 'i');*/
	var dtRegex = new RegExp("^([0]?[1-9]|[1-2]\\d|3[0-1])-(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)-[1-2]\\d{3}$", 'i');
	  /*Changed by Ashiki for TJN27-2 ends*/
	  if( dtRegex.test(dateValue)){
		  var dateArray= dateValue.split('-');
		  var dtDay= dateArray[0];
		  var dtMonth = dateArray[1].toUpperCase();
		  var dtYear = dateArray[2];
		  if ((dtMonth=='APR' || dtMonth=='JUN' || dtMonth=='SEP' || dtMonth=='NOV') && dtDay ==31){
			  showMessage('Please enter valid date','error');
			  return false;
		  }else if (dtMonth == 'FEB'){
		        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
		        if (dtDay> 29 || (dtDay ==29 && !isleap)){
		        	 showMessage('Please enter valid date','error');
		             return false;
		        }
		     }  
		  return true;

	  }else{
		 // showMessage('Please enter date value in this format "DD-MMM-YYYY".','error');
		  return false;
	  }
	 
	  
	}
/* Added by Padmavathi for TENJINCG-1014 ends */
/*Added by Pushpalatha for TENJINCG-968 starts*/

function mandatoryFields(formName){
	for(var i=0;i<document[formName].elements.length;i++){
		if(document[formName].elements[i].type=='text' || document[formName].elements[i].type=='password' 
			|| document[formName].elements[i].tagName=='SELECT' || document[formName].elements[i].type=='textarea' 
				/*Added by Prem for  V2.8-73 , V2.8-75 start*/
				|| document[formName].elements[i].type=='file'
					/*Added by Prem for  V2.8-73 , V2.8-75 end */
					
		){
			if(document[formName].elements[i].getAttribute("mandatory") == 'yes'){
				var $label=$('label[for="'+ document[formName].elements[i].getAttribute('id') +'"]');
				$( "<span style='color:red'>&nbsp*</span>" ).appendTo( $label );
			}
		}
	}
}
/*Added by Pushpalatha for TENJINCG-968 ends*/

//added by Sahana for pCloudy: starts
function callFilters(clientIp,applicationType) {
	$.ajax({
		url:'DeviceServlet',
		data:{param:"GET_FILTERS",clientIP:clientIp},
		dataType:'json',
		async:false,
		success:function(data){
			var status = data.status;
			if(status== 'SUCCESS'){
				var jArray = data.Filters;
				var html = "<option value='Any'>Any</option>";
				for (i in jArray.osArr) {
					if(applicationType==0)
						html = html + "<option value='" + jArray.osArr[i]+ "'>" + jArray.osArr[i] + "</option>";
					else if(applicationType==3 && jArray.osArr[i].includes('Android'))
						html = html + "<option value='" + jArray.osArr[i]+ "'>" + jArray.osArr[i] + "</option>";
					else if(applicationType==4 && jArray.osArr[i].includes('iOS'))
						html = html + "<option value='" + jArray.osArr[i]+ "'>" + jArray.osArr[i] + "</option>";

				}
				$('#lstOs').html(html);

				var html1 = "<option value='Any'>Any</option>";
				for (i in jArray.manufacturerArr) {
					if(applicationType==0)
						html1 = html1+ "<option value='" + jArray.manufacturerArr[i]+ "'>" + jArray.manufacturerArr[i] + "</option>";
					else if(applicationType==3 && jArray.manufacturerArr[i]!='APPLE')
						html1 = html1+ "<option value='" + jArray.manufacturerArr[i]+ "'>" + jArray.manufacturerArr[i] + "</option>";
					else if(applicationType==4 && jArray.manufacturerArr[i]=='APPLE')
					{
						html1 = html1+ "<option value='" + jArray.manufacturerArr[i]+ "'>" + jArray.manufacturerArr[i] + "</option>";
						break;
					}
				}
				$('#lstOEM').html(html1);

				var html2 = "<option value='Any'>Any</option>";
				for (i in jArray.displayArr) {
					html2 = html2 + "<option value='" + jArray.displayArr[i]+ "'>" + jArray.displayArr[i] + "</option>";
				}
				$('#lstScrnSize').html(html2);

				var html3 = "<option value='Any'>Any</option>";
				for (i in jArray.networkArr) {
					html3 = html3 + "<option value='" + jArray.networkArr[i]+ "'>" + jArray.networkArr[i] + "</option>";
				}
				$('#lstNetwork').html(html3);

			}else{
				showMessage(data.MESSAGE,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			showMessage(jsonObj.message, 'error');
		}
	});
}
function getFilteredDevices(jsonString)
{
	$.ajax({
		url:'DeviceServlet',
		data:{param:"GET_DEVICES_BY_FILTERS",json:jsonString},
		dataType:'json',
		async:false,
		success:function(data){
			var status = data.status;
			if(status== 'SUCCESS'){
				var jArray = data.Devices;
				jArray.models.sort( function( a, b ) {
					return a.full_name < b.full_name ? -1 : a.full_name > b.full_name ? 1 : 0;
				});
				$("#listDevice").html("");
				var html ="<option value='-1'>-- Select One --</option>";
				for (i in jArray.models) {
					var device=jArray.models[i];
					html = html + "<option value='" + device.id + "'>"+device.full_name+"</option>";
				}
				$('#listDevice').html(html);
			}else{
				showMessage(data.MESSAGE,'error');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			showMessage(jsonObj.message, 'error');
		}
	});
}
//added by Sahana for pCloudy: ends

/*Added for TENJINCG-1166 starts*/
if(typeof escapeHtmlEntities == 'undefined') {
    escapeHtmlEntities = function (text) {
        return text.replace(/[\u00A0-\u2666<>\&]/g, function(c) {
            return '&' + 
            (escapeHtmlEntities.entityTable[c.charCodeAt(0)] || '#'+c.charCodeAt(0)) + ';';
        });
    };

    // all HTML4 entities as defined here: http://www.w3.org/TR/html4/sgml/entities.html
    // added: amp, lt, gt, quot and apos
    escapeHtmlEntities.entityTable = {
        34 : 'quot', 
        38 : 'amp', 
        39 : 'apos', 
        60 : 'lt', 
        62 : 'gt', 
        160 : 'nbsp', 
        161 : 'iexcl', 
        162 : 'cent', 
        163 : 'pound', 
        164 : 'curren', 
        165 : 'yen', 
        166 : 'brvbar', 
        167 : 'sect', 
        168 : 'uml', 
        169 : 'copy', 
        170 : 'ordf', 
        171 : 'laquo', 
        172 : 'not', 
        173 : 'shy', 
        174 : 'reg', 
        175 : 'macr', 
        176 : 'deg', 
        177 : 'plusmn', 
        178 : 'sup2', 
        179 : 'sup3', 
        180 : 'acute', 
        181 : 'micro', 
        182 : 'para', 
        183 : 'middot', 
        184 : 'cedil', 
        185 : 'sup1', 
        186 : 'ordm', 
        187 : 'raquo', 
        188 : 'frac14', 
        189 : 'frac12', 
        190 : 'frac34', 
        191 : 'iquest', 
        192 : 'Agrave', 
        193 : 'Aacute', 
        194 : 'Acirc', 
        195 : 'Atilde', 
        196 : 'Auml', 
        197 : 'Aring', 
        198 : 'AElig', 
        199 : 'Ccedil', 
        200 : 'Egrave', 
        201 : 'Eacute', 
        202 : 'Ecirc', 
        203 : 'Euml', 
        204 : 'Igrave', 
        205 : 'Iacute', 
        206 : 'Icirc', 
        207 : 'Iuml', 
        208 : 'ETH', 
        209 : 'Ntilde', 
        210 : 'Ograve', 
        211 : 'Oacute', 
        212 : 'Ocirc', 
        213 : 'Otilde', 
        214 : 'Ouml', 
        215 : 'times', 
        216 : 'Oslash', 
        217 : 'Ugrave', 
        218 : 'Uacute', 
        219 : 'Ucirc', 
        220 : 'Uuml', 
        221 : 'Yacute', 
        222 : 'THORN', 
        223 : 'szlig', 
        224 : 'agrave', 
        225 : 'aacute', 
        226 : 'acirc', 
        227 : 'atilde', 
        228 : 'auml', 
        229 : 'aring', 
        230 : 'aelig', 
        231 : 'ccedil', 
        232 : 'egrave', 
        233 : 'eacute', 
        234 : 'ecirc', 
        235 : 'euml', 
        236 : 'igrave', 
        237 : 'iacute', 
        238 : 'icirc', 
        239 : 'iuml', 
        240 : 'eth', 
        241 : 'ntilde', 
        242 : 'ograve', 
        243 : 'oacute', 
        244 : 'ocirc', 
        245 : 'otilde', 
        246 : 'ouml', 
        247 : 'divide', 
        248 : 'oslash', 
        249 : 'ugrave', 
        250 : 'uacute', 
        251 : 'ucirc', 
        252 : 'uuml', 
        253 : 'yacute', 
        254 : 'thorn', 
        255 : 'yuml', 
        402 : 'fnof', 
        913 : 'Alpha', 
        914 : 'Beta', 
        915 : 'Gamma', 
        916 : 'Delta', 
        917 : 'Epsilon', 
        918 : 'Zeta', 
        919 : 'Eta', 
        920 : 'Theta', 
        921 : 'Iota', 
        922 : 'Kappa', 
        923 : 'Lambda', 
        924 : 'Mu', 
        925 : 'Nu', 
        926 : 'Xi', 
        927 : 'Omicron', 
        928 : 'Pi', 
        929 : 'Rho', 
        931 : 'Sigma', 
        932 : 'Tau', 
        933 : 'Upsilon', 
        934 : 'Phi', 
        935 : 'Chi', 
        936 : 'Psi', 
        937 : 'Omega', 
        945 : 'alpha', 
        946 : 'beta', 
        947 : 'gamma', 
        948 : 'delta', 
        949 : 'epsilon', 
        950 : 'zeta', 
        951 : 'eta', 
        952 : 'theta', 
        953 : 'iota', 
        954 : 'kappa', 
        955 : 'lambda', 
        956 : 'mu', 
        957 : 'nu', 
        958 : 'xi', 
        959 : 'omicron', 
        960 : 'pi', 
        961 : 'rho', 
        962 : 'sigmaf', 
        963 : 'sigma', 
        964 : 'tau', 
        965 : 'upsilon', 
        966 : 'phi', 
        967 : 'chi', 
        968 : 'psi', 
        969 : 'omega', 
        977 : 'thetasym', 
        978 : 'upsih', 
        982 : 'piv', 
        8226 : 'bull', 
        8230 : 'hellip', 
        8242 : 'prime', 
        8243 : 'Prime', 
        8254 : 'oline', 
        8260 : 'frasl', 
        8472 : 'weierp', 
        8465 : 'image', 
        8476 : 'real', 
        8482 : 'trade', 
        8501 : 'alefsym', 
        8592 : 'larr', 
        8593 : 'uarr', 
        8594 : 'rarr', 
        8595 : 'darr', 
        8596 : 'harr', 
        8629 : 'crarr', 
        8656 : 'lArr', 
        8657 : 'uArr', 
        8658 : 'rArr', 
        8659 : 'dArr', 
        8660 : 'hArr', 
        8704 : 'forall', 
        8706 : 'part', 
        8707 : 'exist', 
        8709 : 'empty', 
        8711 : 'nabla', 
        8712 : 'isin', 
        8713 : 'notin', 
        8715 : 'ni', 
        8719 : 'prod', 
        8721 : 'sum', 
        8722 : 'minus', 
        8727 : 'lowast', 
        8730 : 'radic', 
        8733 : 'prop', 
        8734 : 'infin', 
        8736 : 'ang', 
        8743 : 'and', 
        8744 : 'or', 
        8745 : 'cap', 
        8746 : 'cup', 
        8747 : 'int', 
        8756 : 'there4', 
        8764 : 'sim', 
        8773 : 'cong', 
        8776 : 'asymp', 
        8800 : 'ne', 
        8801 : 'equiv', 
        8804 : 'le', 
        8805 : 'ge', 
        8834 : 'sub', 
        8835 : 'sup', 
        8836 : 'nsub', 
        8838 : 'sube', 
        8839 : 'supe', 
        8853 : 'oplus', 
        8855 : 'otimes', 
        8869 : 'perp', 
        8901 : 'sdot', 
        8968 : 'lceil', 
        8969 : 'rceil', 
        8970 : 'lfloor', 
        8971 : 'rfloor', 
        9001 : 'lang', 
        9002 : 'rang', 
        9674 : 'loz', 
        9824 : 'spades', 
        9827 : 'clubs', 
        9829 : 'hearts', 
        9830 : 'diams', 
        338 : 'OElig', 
        339 : 'oelig', 
        352 : 'Scaron', 
        353 : 'scaron', 
        376 : 'Yuml', 
        710 : 'circ', 
        732 : 'tilde', 
        8194 : 'ensp', 
        8195 : 'emsp', 
        8201 : 'thinsp', 
        8204 : 'zwnj', 
        8205 : 'zwj', 
        8206 : 'lrm', 
        8207 : 'rlm', 
        8211 : 'ndash', 
        8212 : 'mdash', 
        8216 : 'lsquo', 
        8217 : 'rsquo', 
        8218 : 'sbquo', 
        8220 : 'ldquo', 
        8221 : 'rdquo', 
        8222 : 'bdquo', 
        8224 : 'dagger', 
        8225 : 'Dagger', 
        8240 : 'permil', 
        8249 : 'lsaquo', 
        8250 : 'rsaquo', 
        8364 : 'euro'
    };
}

//for TENJINCG-1166

var sanitizeHTML = function (str) {
	var temp = document.createElement('div');
	temp.textContent = str;
	return temp.innerHTML;
};

var isUrlJavascript = function(url) {
	var isJs = false;
	if(typeof URL === 'function' ) {
		url = new URL(url);
		if(url.protocol === 'javascript:') {
			return true;
		}
	}else{
		//Handle IE Specific
		 var urlSplit = url.split(":");
		 if(urlSplit[0] && urlSplit[0].toLowerCase() ==='javascript') {
			 return true;
		 }
	}
	
	return isJs;
}
/*Added for TENJINCG-1166 ends*/