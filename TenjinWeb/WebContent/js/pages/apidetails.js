/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  apidetails.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
*28-03-2017            Bablu kumar             API Detail screen maintenance
*01-08-2017            Padmavathi               API story review fixed  
    19-08-2017         Sriram           	   T25IT-244
    02-02-2018		   Pushpalatha				TENJINCG-568
*
*/

$(document).ready(function(){
	/*Added by Pushpalatha for TENJINCG-568 starts*/
	var appId=$('#lstAppId').val();
	populateAutGrooups(appId);
	/*Added by Pushpalatha for TENJINCG-568 ends*/
	
	
	
/*$('#api_details_new_form').on('submit',function(e){
	$.ajax({
		url:"APIServlet",
		data:{param:"new_api_aut",json:objstr},
		dataType:'json',
		async:false,
		success: function(data){
			var status = data.status;
			if(status == 'SUCCESS'){			
				showMessage(data.message, 'success');
				$('#txtUserId').attr('disabled', true);					
				$('#btnEdit').show();	
				$('#btnSave').hide();
			}else{
				showMessage(data.message, 'error');				
			}
		},
		
		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown, 'error');			
		}
	
});*/

/*	$('#btnSave').click(function(){

	var validated = validateForm('api_details_edit_form');
		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
		}
		var json = new Object();
		json.applicationid = $('#lstAppId').val();
		json.apicode = $('#txtApiCode').val();
		json.apiname = $('#txtApiName').val();
		json.apitype = $('#txtApiType').val();
		json.apiurl = $('#txtApiUrl').val();
		var objstr = JSON.stringify(json);
		
		$.ajax({
			url:"APIServlet",
			data:{param:"new_api_aut",json:objstr},
			dataType:'json',
			async:false,
			success: function(data){
				var status = data.status;
				if(status == 'SUCCESS'){			
					showMessage(data.message, 'success');
					$('#txtUserId').attr('disabled', true);					
					$('#btnEdit').show();	
					$('#btnSave').hide();
				}else{
					showMessage(data.message, 'error');				
				}
			},
			
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown, 'error');			
			}
				
		});
	});*/
	/*Added by padmavathi for Api story review fixes on 01-08-2017 starts*/	
	$(document).on("click", "#btnSave", function(){

		var validated = validateForm('api_details_new_form');
		if (validated != 'SUCCESS') {
			showMessage(validated, 'error');
			return false;
		}
		
		//T25It-244
		var apiCode = $('#txtApiCode').val();
		if(/\s/g.test(apiCode)) {
			showMessage('API Code cannot contain spaces', 'error');
			return false;
		}
		//T25It-244 ends
		window.location="APIServlet?param=New_api";	
	});
	/*Added by padmavathi for Api story review fixes on 01-08-2017 ends*/	
	$('#btnBack').click(function(){	
		/*Changed by padmavathi for Api story review fixes on 01-08-2017 starts*/
		/*window.location.href='APIServlet?param=FETCH_ALL_APIS';*/		
		window.location.href = 'APIServlet?param=show_api_list';	
		/*Changed by padmavathi for Api story review fixes on 01-08-2017 ends*/
	});
	
	
	});
/*Added by Pushpalatha for TENJINCG-568 starts*/
$(document).on('change',"#lstAppId",function () {

var appId=this.value;
populateAutGrooups(appId);
}); 

function populateUserAutTypes(appId){
	var jsonObj = fetchUserTypeForApp(appId);
		
		$('#lstAppUserType').html('');
		var status = jsonObj.status;
		if(status== 'SUCCESS'){
			var html = "<option value='-1'>-- Select One --</option>";
			var jarray = jsonObj.appUserTypes;
			if(jarray == undefined){
				
			}else{
			for(i=0;i<jarray.length;i++){
				var json = jarray[i];
				
				html = html + "<option value='" + json.code + "'>" + json.code  + "</option>";
			}
			}
			
			$('#lstAppUserType').html(html);
		}else{
			showMessage(jsonObj.message,'error');
		}
	}


function populateAutGrooups(appId){
	var jsonObj = fetchAutGroups(appId);
		
		$('#existingGroup').html('');
		var status = jsonObj.status;
		if(status== 'SUCCESS'){
			var html = "<option value='-1'>-- Select One --</option>";
			var jarray = jsonObj.GROUPS;
			if(jarray == undefined){
				
			}else{
			for(i=0;i<jarray.length;i++){
				var json = jarray[i];
				if(json!='Ungrouped')
				html = html + "<option value='" + json + "'>" + json  + "</option>";
			}
			}
			
			$('#existingGroup').html(html);
		}else{
			showMessage(jsonObj.message,'error');
		}
	}
/*Added by Pushpalatha for TENJINCG-568 ends*/

/*added by  Parveen for  Enhancement#665   on 24-11-2016 ends*/ 
function updateUserMessage(msg, type){
	var $element = $('#user-message-user');
	if(msg == ''){
		clearMessagesOnElement($element);
	}else{
		showMessageOnElement(msg, type, $element);
	}
}

