/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  reg_device_list.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 22-Dec-2017			Sahana					Mobility
 */	



$(document).ready(function() {

	/*$('.Platform').hide();

	$('#txtDeviceType').change(function(event) {
		var deviceType=$('#txtDeviceType').val();
		$('#txtPlatform').val(deviceType);
		$('.Platform').show();
	});*/
	
	$('#txtPltVersion').keyup(function (){
		var inputVal = $(this).val();
		var regex = new RegExp('^\d{0,2}(\.\d{1,2})?$');
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			alert('Please enter only numbers into the textbox ');
			$(this).val('');
		}
	});
	
	$('#btnCancel').click(function(event) {
		window.location.href="DeviceServlet?param=FETCH_ALL_DEVICES";
	});
	$('#btnRegister').click(function(event) {

		var validated = validateForm('new_device');
		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
		}

		var obj = new Object();

		obj.deviceName = $('#txtDeviceName').val();
		obj.deviceId = $('#txtDeviceId').val();
		obj.deviceType=$('#txtDeviceType').val();
		obj.platform = $('#txtPlatform').val();
		obj.platformVersion=$('#txtPltVersion').val();

		var jsonString = JSON.stringify(obj);

		$.ajax({
			url:'DeviceServlet',
			data:{param:"NEW_DEVICE_REG",json:jsonString},
			dataType:'json',
			success:function(data){
				if(data.status == 'SUCCESS'){
					showMessage(data.message,'success');
					window.setTimeout(function(){window.location='DeviceServlet?param=FETCH_ALL_DEVICES';},500)
					//window.parent.closeModal();
				}else{
					showMessage(data.message,'error');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
			}
		});


	});


});


