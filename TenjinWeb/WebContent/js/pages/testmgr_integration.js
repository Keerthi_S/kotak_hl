/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testmgr_integration.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*New File added  by Parveen for Requirement TENJINCG_55 and TENJINCG_56
* DATE                 CHANGED BY              DESCRIPTION
* 19-Aug-2017			Manish					for displaying loader
* 08-11-2018           Leelaprasad              TENJINCG-874
* 26-03-2019			Roshni					TJN252-54
* 03-06-2019			Prem					V2.8-100
* 
*/	
/*
 * 
 */

$(document).ready(function(){
	/* Added by prem for V2.8-100 starts*/
	mandatoryFields('external_testmgt_int');
	/* Added by prem for V2.8-100 Ends*/
	/*added by manish for showing loader starts*/
	$('#dmLoader').hide();
	/*added by manish for showing loader ends*/
	/*added by manish for req tenjincg-57,58,Defect-96 on 03-02-2017 starts*/
	$('#mapAttributes').hide();
	$('#mapAttributes').click(function(){
		var projectId = $('#txttjnPrjId').val();
		var validated = validateForm('external_testmgt_int');
		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			return false;
		}
	var instance=$('#txtInstTool').val();
	var tjnUserId = $('#txttjnUserId').val();
		$.ajax({
			url:'TestManagerServlet',
			data:'param=check_credentials&user=' + tjnUserId + '&instance=' + instance,
			async:false,
			dataType:'json',
			success:function(data){
				if(data.status == 'success'){
				
					/*Changed by Leelaprasa dfor TENJINCG-874 starts*/
					/*window.location.href='TestCaseServlet?param=fetch_tc_and_ts_from_tm&projectId='+projectId;*/
					window.location.href='ProjectServletNew?t=fetch_tc_and_ts_from_tm&projectId='+projectId;
					/*Changed by Leelaprasa dfor TENJINCG-874 ends*/
			}else{
					showMessage(data.message,'error');
				}
			},
		
		error:function(xhr, textStatus, errorThrown){
			showMessage(errorThrown, 'error');			
		}
	});
		
		
		
	});
	/*added by manish for req tenjincg-57,58,Defect-96 on 03-02-2017 ends*/
	
	$("select[name='txtTcFilter1']").hide();
	$("select[name='txtTsFilter1']").hide();
	var tjnprjId = $('#txttjnPrjId').val();
	var tjnUserId = $('#txttjnUserId').val();
	 var changeVal='DomainChange'
	$('#btnCancel').click(function(){	
		var tjnprjId = $('#txttjnPrjId').val();
		/*Changed by Leelaprasa dfor TENJINCG-874 starts*/
		/*window.location.href='ProjectServlet?param=PROJECT_VIEW&paramval='+tjnprjId+'&v=PROJECT';*/
		window.location.href = 'ProjectServletNew?t=load_project&paramval=' + tjnprjId + '&v=PROJECT';
		/*Changed by Leelaprasa dfor TENJINCG-874 ends*/
	});
	

	

	
	$('#txtTcFilter').hide();
	$('#txtTcValue').hide();
	$('#txtTsFilter').hide();
	$('#txtTsValue').hide();
	$('#TcFilterlabel').hide();
	$('#TcValuelable').hide();
	$('#TsFilterlable').hide();
	$('#TsValuelable').hide();
	

	var etmprj = $('#etmprjprj').val();
	if(etmprj!='null'){
		$('#extPrjName').val(etmprj);
	}
	
	var etmprjenable=$('#etmprjenable').val();
	if(etmprjenable==='N'){
		$('#txtCheck').attr('checked',false);
	}
	

	//for showing the TC Filter and TS Filter as text Box with its value
	if( $('#etmtcfilter').val()!='null'){
		$('#TcFilterlabel').show();
		$('#extTcFilter').show();
		$('#txtTcValue').show().prop('disabled',true);
		$('#TcValuelable').show();
		$('#txtTcValue').val($('#etmtcvalue').val());
	}
   
	if( $('#etmtsfilter').val()!='null'){
		$('#extTsFilter').show();
		$('#txtTsValue').show().prop('disabled',true);
		$('#TsFilterlable').show();
		$('#TsValuelable').show();
		$('#txtTsValue').val($('#etmtsvalue').val());
	}
	/*added by manish for req tenjincg-57,58,Defect-96 on 03-02-2017 starts*/
	if($('#etmtcfilter').val()!='null' && $('#etmtsfilter').val()!='null'){
		$('#mapAttributes').show();
	}
	/*added by manish for req tenjincg-57,58,Defect-96 on 03-02-2017 ends*/
   $('#extPrjName').change(function (){
	   var almPrj= $('#extPrjName').val();
	    if(!almPrj.includes(":")){
		 {
		 alert("Project Name Should Separated by Project:Domain");
		 }
         }
	   changeVal='DomainChangetrue'
	   
	  }); 
	
		 	
	$('#btnSave').click(function(){
		/*added by manish for showing loader starts*/
		$('#dmLoader').show();
		clearMessages();
		/*added by manish for showing loader ends*/
		var validated = validateForm('external_testmgt_int');
		if(validated != 'SUCCESS'){
			showMessage(validated,'error');
			/*added by manish for showing loader starts*/
			$('#dmLoader').hide();
			/*added by manish for showing loader ends*/
			return false;
		}
	    
		var obj = new Object();
	    obj.tjnprjId = $('#txttjnPrjId').val();
		obj.instance = $('#txtInstTool').val();
		obj.prjName = $('#extPrjName').val();
		obj.tcValue='';
		obj.tsValue='';
		obj.tcFilter='';
		obj.tsFilter='';
		obj.tsDisplayValue='';
		obj.tcDisplayValue='';
		
		
		if($('#txtTcValue').val()!='-1' && $('#txtTcValue').val()!=''){
			obj.tcValue = $('#txtTcValue').val();
			
		}
		
		
		if($("#txtTcFilter").is(':hidden')){
			if($("select[name='txtTcFilter']").val()=='-1' || $("select[name='txtTcFilter']").val()==undefined ){
				if($("#extTcFilter").is(":visible"))
				{obj.tcFilter =$('#extTcFilter').val();}
				else{
					obj.tcFilter = '';}
			}
			else
				{
				obj.tcFilter = $('#extTcFilter').val();
				}
		}else{
			if($('#txtTcFilter').val()!='-1' && $('#txtTcFilter').val()!=''){
			obj.tcFilter = $('#txtTcFilter').val();	
			var element=document.getElementById("txtTcFilter");
			obj.tcDisplayValue=element.options[element.selectedIndex].text;
			}
		}
		
		
		if($("#txtTsFilter").is(':hidden')){
			if($("select[name='txtTsFilter']").val()=='-1' || $("select[name='txtTcFilter']").val()==undefined){
				if($("#extTsFilter").is(":visible"))
				{obj.tsFilter =$('#extTsFilter').val();}
				else{
					obj.tsFilter = '';}
			}
			else
				{
				obj.tsFilter = $('#extTsFilter').val();
				
				}
		}else{
			if($('#txtTsFilter').val()!='-1' && $('#txtTsFilter').val()!=''){
			obj.tsFilter = $('#txtTsFilter').val();	}
			var element=document.getElementById("txtTsFilter");
			obj.tsDisplayValue=element.options[element.selectedIndex].text;
		}
	
		
		if($('#txtTsValue').val()!='-1' && $('#txtTsValue').val()!=''){
			obj.tsValue = $('#txtTsValue').val();
		}
		
		
		if ($("#txtCheck").is(':checked'))
			{obj.isEnable = 'Y';
			}
		else
			{obj.isEnable='N'
			}
		
		var jsonString = JSON.stringify(obj);
		
		$.ajax({
			/*Changed by Leelaprasa dfor TENJINCG-874 starts*/
			/*url:'ProjectServlet',
			/*data:{param:"UPDATE_EXT_PRJ_DETAIL",json:jsonString,tjnprjId:tjnprjId,tjnUserId:tjnUserId},*/
			url:'ProjectServletNew',
			data:{t:"UPDATE_EXT_PRJ_DETAIL",json:jsonString,tjnprjId:tjnprjId,tjnUserId:tjnUserId},
			/*Changed by Leelaprasa dfor TENJINCG-874 ends*/
			dataType:'json',
			success:function(data){
				if(data.status == 'success'){
					showMessage(data.message,'success');
					
					var html1 = "<option value='-1'>-- Select One --</option>";
					var jarrayTcFields = data.tcfields;
					for(i=0;i<jarrayTcFields.length;i++){
						var json = jarrayTcFields[i];
						var values=json.tcField.split("+");
						var value=values[1];
						var displayValue=values[0];
						html1 = html1 + "<option value='" + value + "'>" + displayValue + "</option>";
					}
				
					$('#txtTcFilter').html(html1).show();
					$('#txtTcValue').show().val(' ');
					$('#TcFilterlabel').show();
					$('#TcValuelable').show();
					$('#extTcFilter').hide();
					
					var html2 = "<option value='-1'>-- Select One --</option>";
					var jarrayTsFields = data.tsfields;
					for(i=0;i<jarrayTsFields.length;i++){
						var json = jarrayTsFields[i];
						var values=json.tsField.split("+");
						var value=values[1];
						var displayValue=values[0];
						html2 = html2 + "<option value='" + value + "'>" + displayValue + "</option>";
					}
				
					$('#txtTsFilter').html(html2).show();
					$('#txtTsValue').show().val(' ');
					$('#TsFilterlable').show();
					$('#TsValuelable').show();
					$('#extTsFilter').hide();
					
					if(data.messTMval=='TM')
						{
						showMessage(data.message,'success');
						}
					else{
						showMessage(data.message,'success');
						$('#txtTcValue').prop('disabled',false);
						$('#txtTsValue').prop('disabled',false);
						if(changeVal!='DomainChangetrue'){
							/*Changed by Leelaprasa dfor TENJINCG-874 starts*/
							/*window.setTimeout(function(){window.location.href='ProjectServlet?param=PROJECT_VIEW&paramval='+tjnprjId+'&v=PROJECT';},500);*/	
							window.setTimeout(function(){window.location.href='ProjectServletNew?t=load_project&paramval=' + tjnprjId + '&v=PROJECT';},500);
							/*Changed by Leelaprasa dfor TENJINCG-874 ends*/
						}
						
					}
					/*added by manish for showing loader starts*/
					$('#dmLoader').hide();
					/*added by manish for showing loader starts*/
		   }else{
					showMessage(data.message,'error');
				}
				/*added by manish for showing loader starts*/
				$('#dmLoader').hide();
				/*added by manish for showing loader starts*/
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown,'error');
				/*added by manish for showing loader starts*/
				$('#dmLoader').hide();
				/*added by manish for showing loader starts*/
			}
		});
	});
	
	
	
});

function populateTcField(){
	$("select[name='txtTcFilter1']").show();
	var tjnprjId = $('#txttjnPrjId').val();
	var tjnUserId = $('#txttjnUserId').val();
	$.ajax({
		/*Changed by Leelaprasa dfor TENJINCG-874 starts*/
		/*url:'ProjectServlet',
		data :{param:"UPDATE_EXT_PRJ_DETAIL",change1:"edit_tc",tjnprjId:tjnprjId,tjnUserId:tjnUserId},*/
		url:'ProjectServletNew',
		data :{t:"UPDATE_EXT_PRJ_DETAIL",change1:"edit_tc",tjnprjId:tjnprjId,tjnUserId:tjnUserId},
		/*Changed by Leelaprasa dfor TENJINCG-874 ends*/
		async:false,
		dataType:'json',
		success:function(data){
			if(data.status == 'success'){
				var html1 = "<option value='-1'>-- Select One --</option>";
				var jarrayTcFields = data.tcfields;
				for(i=0;i<jarrayTcFields.length;i++){
					var json = jarrayTcFields[i];
					var values=json.tcField.split("+");
					var value=values[1];
					var displayValue=values[0];
					
					html1 = html1 + "<option value='" + value + "'>" + displayValue + "</option>";
				}
				
			
				$('#txtTcFilter').html(html1).show();
				$('#extTcFilter').hide();
				$('#txtTcValue').show().prop('disabled',false).val(' ');;
				$('#TcFilterlabel').show();
				$('#TcValuelable').show();
				
				
		}else{
				showMessage(data.message,'error');
			}
		},
	
	error:function(xhr, textStatus, errorThrown){
		showMessage(errorThrown, 'error');			
	}
});
}
function populateTsField(){
	$("select[name='txtTsFilter1']").show();
	var tjnprjId = $('#txttjnPrjId').val();
	var tjnUserId = $('#txttjnUserId').val();
	$.ajax({
		/*url:'ProjectServlet',
	    data :{param:"UPDATE_EXT_PRJ_DETAIL",change2:"edit_ts",tjnprjId:tjnprjId,tjnUserId:tjnUserId},*/
		url:'ProjectServletNew',
		data :{t:"UPDATE_EXT_PRJ_DETAIL",change2:"edit_ts",tjnprjId:tjnprjId,tjnUserId:tjnUserId},
		async:false,
		dataType:'json',
		success:function(data){
			if(data.status == 'success'){
				//showMessage(data.messageTM,'success');
				
				var html2 = "<option value='-1'>-- Select One --</option>";
				var jarrayTsFields = data.tsfields;
				for(i=0;i<jarrayTsFields.length;i++){
					var json = jarrayTsFields[i];
					var values=json.tsField.split("+");
					var value=values[1];
					var displayValue=values[0];
					html2 = html2 + "<option value='" + value + "'>" + displayValue + "</option>";
				}
				
			
				$('#txtTsFilter').html(html2);
				$('#extTsFilter').hide();
		        $('#txtTsValue').show().prop('disabled',false).val(' ');
				$('#TsFilterlable').show();
				$('#TsValuelable').show();
			
				
		}else{
				showMessage(data.message,'error');
			}
		},
	
	error:function(xhr, textStatus, errorThrown){
		showMessage(errorThrown, 'error');			
	}
});
}
