/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ttddownloadoptions.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 19-08-2017           Leelaprasad             T25IT-260
* 20-08-2017           Gangadhar Badagi        Removed alert box
* 20-09-2017		   Roshni				   Tenjincg-351
* 29-06-2018			Sriram Sridharan		T251IT-101
* 
*/

$(document).ready(function(){
	
	/* Added by Sriram for TENJINCG-152 (API Testing Enablement)*/
	$('#templateTypeMaster').prop('checked',true);
	var txnMode = $('#txnMode').val();
	if(txnMode !== 'G') {
		$('#templateTypeInput').prop('disabled', true);
		/*	$('#templateTypeInputCustom').prop('disabled', true);*/
		/*Modified by Roshni for TenjinCG-351 starts */
		$('#templateTypeInputCustom').prop('disabled', false);
		/*Modified by Roshni for TenjinCG-351 ends */
		/*Changed by Leelaprasad for the requirement defect fix T25IT-260 starts */
		$('#mandatorySelector').prop('disabled', true);
		/*Changed by Leelaprasad for the requirement defect fix T25IT-260 starts */
		$('.notForGui').show();
	}
	/* Added by Sriram for TENJINCG-152 (API Testing Enablement) ends */
	
	$('#btnCancel').click(function(){
		window.parent.closeModal();
	});
	
	$('#btnOk').click(function(){
		var mod = $('#func').val();
		var app = $('#app').val();
		var txnMode = $('#txnMode').val();
		var mode = '';
		var checkCount= $("input:checkbox[name=mandatorySelector]:checked").length;
		if(checkCount > 0){
			mode ='M';
		}else{
			mode='A';
		}
		
		var templateType = $("input[type=radio]:checked").val();
		
		if(templateType == 'CUSTOM'){
			/*Removed by Gangadhar Badagi starts */
		/*	alert('Custom Template');*/
			/*Removed by Gangadhar Badagi ends */
		/*	window.open('selectiveFields.jsp?m=' + mod + '&a=' + app,'_blank'); */
			/*Modified by Roshni for TenjinCG-351 starts */
			//Changed by Sriram for T251IT-101
			/*window.open('selectiveFields.jsp?t=' + txnMode + '&m=' + mod + '&a=' + app,'_blank');*/
			window.open('selectiveFields.jsp?t=' + txnMode + '&m=' + mod + '&a=' + app + '&o=' + $('#operation').val(),'_blank');
			//Changed by Sriram for T251IT-101 ends
			/*Modified by Roshni for TenjinCG-351 ends */
			//$c.remove();
		}else{
			
			/* Added by Sriram for TENJINCG-152 (API Testing Enablement)*/
			var url ;
			var reqData;
			
			if(txnMode === 'G') {
				url = 'LearnerServlet';
				reqData = 'param=DOWNLOAD_DATA_TEMPLATE&m=' + mod + '&a=' + app + '&templatetype='+ templateType + '&type=' + mode+ '&txnMode=' + txnMode;
			} else {
				url = 'ApiLearnerServlet';
				reqData = 'param=DOWNLOAD_DATA_TEMPLATE&api=' + mod + '&a=' + app + '&op=' + $('#operation').val() + '&type=' + mode;
			}
			/* Added by Sriram for TENJINCG-152 (API Testing Enablement) ends*/
			
			disableFields();
			$('#progress').html("<b>Tenjin is generating your template. Please wait</b>&nbsp <img src='images/inprogress.gif'/>");
			$.ajax({
				/* Changed by Sriram for TENJINCG-152 (API Testing Enablement)*/
				/*url:'LearnerServlet',
				data:'param=DOWNLOAD_DATA_TEMPLATE&m=' + mod + '&a=' + app + '&templatetype='+ templateType + '&type=' + mode+ '&txnMode=' + txnMode,*/
				url:url,
				data:reqData,
				/* Changed by Sriram for TENJINCG-152 (API Testing Enablement) ends*/
				async:true,
				dataType:'json',
				success:function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
						$("body").append($c);
						$("#downloadFile").get(0).click();
						$c.remove();
						enableFields();
						$('#progress').html();
						window.parent.closeModal();
					}else{
						var message = data.message;
						showMessage(message,'error');
						enableFields();
						$('#progress').html();
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(errorThrown, 'error');
					enableFields();
					$('#progress').html();
				}
			});
		}
	});
	
	$('input[name=templateType]').click(function(){
		if($(this).val() == 'CUSTOM'){
			$('#mandatorySelector').attr('checked',false);
			$('#mandatorySelector').attr('disabled',true);
		}else{
			$('#mandatorySelector').attr('disabled',false);
		}
		/*Added by Roshni for TenjinCG-351 starts */
		if(txnMode!=='G')
			{
			$('#mandatorySelector').attr('checked',false);
			$('#mandatorySelector').attr('disabled',true);

			}
		/*Added by Roshni for TenjinCG-351 ends */
	});
	
});

function disableFields(){
	$('#mandatorySelector').prop('disabled',true);
	$('#templateTypeInput').prop('disabled',true);
	$('#templateTypeMaster').prop('disabled',true);
	$('#btnOk').prop('disabled',true);
	$('#btnCancel').prop('disabled',true);
}

function enableFields(){
	$('#mandatorySelector').prop('disabled',false);
	$('#templateTypeInput').prop('disabled',false);
	$('#templateTypeMaster').prop('disabled',false);
	$('#btnOk').prop('disabled',false);
	$('#btnCancel').prop('disabled',false);
}


function setParameters(appId, funcCode){
	$('#app').val(appId);
	$('#func').val(funcCode);
}