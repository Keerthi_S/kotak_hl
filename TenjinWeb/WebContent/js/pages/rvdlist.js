/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  rvdlist.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

$(document).ready(function(){
	populateAuts();
	$('#rvd-list tbody').html('');
	$('#rvdlist').hide();
	/*********************************88
	 * AUT drop down - change
	 */
	
	$('#lstApplication').change(function(){
		var selAut = $(this).val();
		if(selAut == '-1'){
			$('#rvd-list tbody').html('');
			$('#rvdlist').hide();
		}else{
			$.ajax({
				url:'ResultsServlet',
				data:'param=fetch_all_rvd_for_app&app=' + selAut,
				async:false,
				dataType:'json',
				success:function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						var jArray = data.rvdlist;
						var html = '';
						for(i=0;i<jArray.length;i++){
							var rvd = jArray[i];
							html = html + '<tr>';
							html = html + "<td class='tiny'><input type='checkbox' id='" + rvd.id + "'/></td>";
							html = html + "<td class='tiny'>" + rvd.id + "</td>";
							html = html + "<td><a href='ResultsServlet?param=res_val_detail&rv=" + rvd.id + "'>" + rvd.name + "</a></td>";
							html = html + "<td>" + rvd.appname + "</td>";
							html = html + "</tr>";
						}
						
						$('#rvd-list tbody').html(html);
						$('#rvd-list').dataTable();
						$('#rvdlist').show();
					}else{
						html = "<tr><td colspan='4'>" + data.message + "</td></tr>";
						$('#rvd-list tbody').html(html);
						$('#rvd-list').dataTable();
						$('#rvdlist').show();
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(errorThrown,'error');
				}
			});
		}
	});
	
	/****************************
	 * New Button - Click
	 ****************************/
	$('#btnNew').click(function(){
		window.location.href='newrvd.jsp';
	});
});

function populateAuts(){
	var jsonObj = fetchAllAuts();
	
	var status = jsonObj.status;
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jArray = jsonObj.auts;
		
		for(i=0;i<jArray.length;i++){
			var aut = jArray[i];
			html = html + "<option value='" + aut.id + "'>" + aut.name + "</option>";
		}
		
		$('#lstApplication').html(html);
		
	}else{
		showMessage(jsonObj.message, 'error');
	}
}
