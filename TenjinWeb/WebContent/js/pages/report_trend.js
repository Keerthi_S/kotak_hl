/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  report_trend.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 * DATE 				CHANGED BY 						 DESCRIPTION
 * 10-JAN-2016            Sahana                		 TENJINCG-7(Calendar Icon is to be added for all date fields)
 * 20-03-2019             Padmavathi                     TENJINCG-1014
 */
$(document).ready(function() {
	$('#lstApplication').change(function(){
		clearMessages();
	});

	initializeReport();
	function initializeReport(){
		var appId = $('#lstApplication').val();
		var fromDate = $('#fromDate').val();

		var message = $('#message').val();
		if (message == 'noerror') {
		} else {
			showMessage(message, 'error');
		}

		$("#fromDate").datepicker({
			/*   Changed by sahana for the improvement of TENJINCG-7: starts  */
			showOn: 'button',
			buttonText: 'Show Date',
			buttonImageOnly: true,
			buttonImage: './images/button/calendar.png',
			constrainInput: true,
			/*   Changed by sahana for the improvement of TENJINCG-7: ends  */
			dateFormat: 'dd-M-y',
			maxDate : new Date(),
			onSelect : function(){
				clearMessages();
			}
		});
	}

	$('#btnReset').click(function(){
		$('#lstApplication').val(-1);
		$("#fromDate").val('');
		$('#user-message').hide();
		$('#chart1').empty();
	});

	$('#trend_report').click(function(){
	/* Added by Padmavathi for TENJINCG-1014 starts */
		clearMessages();
		/* Added by Padmavathi for TENJINCG-1014 ends */
		var appId = $('#lstApplication').val();
		var fromDate = $('#fromDate').val();
		/* Added by Padmavathi for TENJINCG-1014 starts */
		var todaysDate=new Date();
		todaysDate.setHours(0,0,0,0);
		/* Added by Padmavathi for TENJINCG-1014 ends */
		if(appId == "" || appId == -1){
			showMessage("Choose Application", "Error");
		}else if(fromDate == ""){
			showMessage("Choose From Date", "error");
		}/* Added by Padmavathi for TENJINCG-1014 starts */
		else if(!validateDate(fromDate)){
				showMessage('Please enter date value in this format DD-MMM-YY.','error');
				return false;
		}else if(new Date(fromDate)>todaysDate){
			showMessage("Date should be less than or equal to Today's Date","error");
			return false;
			/* Added by Padmavathi for TENJINCG-1014 ends */
		}else{
			$.ajax({
				url		: 'ReportServlet',
				type	: 'GET',
				data	: {'param' :'generate_trend', 'app_id' : appId, 'fromdate': fromDate},
				dataType: 'json',
				success	: function(data){
					var status = data.STATUS;
					if(status == 'SUCCESS'){
						var dataArray=[], legendValues=[],arrayOfDataarray=[],xAxisSeriesValue=[];
						var trend_report_json = data.TRNED_REPORT_JSON;
						var trendArray = trend_report_json.trendArray;
						for(var i = 0; i<trendArray.length; i++){
							/*var trendreport = trendArray[i];
						legendValues.push(trendreport.Application);
						dataArray.push(trendreport.Day1);
						dataArray.push(trendreport.Day2);
						dataArray.push(trendreport.Day3);
						dataArray.push(trendreport.Day4);
						dataArray.push(trendreport.Day5);
						dataArray.push(trendreport.Day6);
						dataArray.push(trendreport.Day7);
						xAxisSeriesValue.push
						arrayOfDataarray.push(dataArray);
						dataArray = [];*/

							var trendreport = trendArray[i];
							legendValues.push(trendreport.Application);
							for (var j = 1; j <=7; j++) {
								var data = trendreport['Day'+j];
								if(i==0){
									xAxisSeriesValue.push(data.split('|')[0]);
								}
								dataArray.push(data.split('|')[1]);
							}
							arrayOfDataarray.push(dataArray);
							dataArray = [];
						}
						if(trendArray.length > 0){
							drawChart(arrayOfDataarray, legendValues, xAxisSeriesValue);
						}else{
							var options = 'No Records to display.';
							$('#chart1').html(options);
						}
					}else{
						showMessage(data.MESSAGE, "Error");
					}
				}
			});
		}
	});

	function drawChart(dataArray , legendValues,xAxisSeriesValue){
		var minval = 0,maxVal=0;
		for(var i = 0; i<dataArray.length; i++){
			var seriesArray = dataArray[i];
			for(var j = 0; j<seriesArray.length; j++){
				var temp = seriesArray[j];
				if(temp > maxVal){
					maxVal = temp;
				}
			}
		}

		var mod = maxVal % 10;
		var temp = 0;
		if(mod < 5){
			temp = 5 - mod;
		}else{
			temp = 10 - mod;
		}

		maxVal = Number(maxVal)+Number(temp);

		plot2 = $.jqplot('chart1', dataArray, {
			seriesColors:['#85802b', '#00749F', '#73C774', '#C7754C'],
			seriesDefaults: {
				renderer:$.jqplot.BarRenderer,
				pointLabels: { show: true }
			},
			series:getLabels(legendValues),
			axes: {
				xaxis: {
					renderer: $.jqplot.CategoryAxisRenderer,
					ticks: xAxisSeriesValue,
					tickOptions:{
						formatString:'%s'
					},
					label : 'Date',
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer
				},
				yaxis: {
					tickInterval: 1,
					tickOptions:  {
						formatString:'%d',
						angle : -90
					},
					min: minval,
					max: Number(maxVal),
					label : 'Count',
					labelRenderer: $.jqplot.CanvasAxisLabelRenderer
				}
			},
			legend: { show: true, location: 'ne', placement: 'outsideGrid' },
			highlighter: {
				show: true,
				showMarker : false,
				tooltipFormatString: '%s',
				tooltipContentEditor: function(str, seriesIndex, pointIndex, plot){
					return plot.series[seriesIndex]["label"] + ': ' + str;
				},
				tooltipAxes: 'y',
				location: 'n'
			},

		});

		plot2.replot();
	}

	function getLabels (legendValues){
		var length = legendValues.length;
		var arr = new Array(length);
		for(i=0; i< length; i++){
			arr[i] = {label:legendValues[i]};
		}
		return arr;
	};
	/* Added by Padmavathi for TENJINCG-1014 starts */
	function validateDate(dateValue) {
		  var dtRegex = new RegExp("^([0]?[1-9]|[1-2]\\d|3[0-1])-(JAN|FEB|MAR|APR|MAY|JUN|JULY|AUG|SEP|OCT|NOV|DEC)-[1-2]\\d{1}$", 'i');
		  if( dtRegex.test(dateValue)){
			  var dateArray= dateValue.split('-');
			  var dtDay= dateArray[0];
			  var dtMonth = dateArray[1].toUpperCase();
			  var dtYear = dateArray[2];
			  if ((dtMonth=='APR' || dtMonth=='JUN' || dtMonth=='SEP' || dtMonth=='NOV') && dtDay ==31){
				  showMessage('Please enter valid date','error');
				  return false;
			  }else if (dtMonth == 'FEB')
			     {
			        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
			        if (dtDay> 29 || (dtDay ==29 && !isleap)){
			        	 showMessage('Please enter valid date','error');
			             return false;
			        }
			     }  
			  return true;

		  }else{
			  showMessage('Please enter date value in this format "DD-MMM-YY".','error');
			  return false;
		  }
		 
		  
		}
		/* Added by Padmavathi for TENJINCG-1014 ends */
});