/**

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  email_configuration.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 08-01-2018			    Padmavathi		        Newly added for TENJINCG-575
 08-02-2018			    Padmavathi		        Changed for refresh button 
 13-06-2018			    Padmavathi		        T251IT-5
 14-06-2018			    Padmavathi		        T251IT-32
 27-11-2018				Pushpalatha				TJNUN262-1
 22-03-2019             Padmavathi              TENJINCG-1017
 25-03-2019				Pushpalatha				TENJINCG-968
 04-07-2019             Padmavathi              TV2.8R2-24
 17-03-2020				Lokanath                TENJINCG-1140
*/

$(document).ready(function() {
	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('emailform');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	/*Added by Padmavathi for TENJINCG-1017 starts*/
			$(".ntlmHide").hide();
			/*Added by Padmavathi for TENJINCG-1017 ends*/
			$('.hideemail').hide();
			$("#btnDelete").hide();
			$("#emailLoader").hide();
			$("#emailconfig").hide();
			if($('#delete').val()=='true'){
				$("#btnDelete").show();
				$("#emailconfig").show();
			}else{
				var $label=document.getElementById('pwdLabel');
				$( "<span style='color:red'>&nbsp*</span>" ).appendTo( $label );
			}
			if($("#txtauthenticate").val()==''){
				$("#txtauthenticate").val('Y');
			}
			/*Added by Padmavathi for TENJINCG-1017 starts*/
			if($("#txtntlmauthenticate").val()==''){
				$("#txtntlmauthenticate").val('Y');
			}
			if($('#txtusername').val()!=''){
				 $("#txtntlmauthenticate").attr('checked', true);
				 $(".ntlmHide").show();
			}
			 $('#txtntlmauthenticate').on('change', function(){
				 if ( $(this).is(':checked') ) {
						clearMessages();
				    	 $(".ntlmHide").show();
				    	 $("#txtntlmauthenticate").val('Y');
				    } else {
						clearMessages();
				    	$(".ntlmHide").hide();
				    	$("#txtntlmauthenticate").val('N');
				    }
			 });
			/*Added by Padmavathi for TENJINCG-1017 ends*/
			 $('#txtauthenticate').on('change', function(){
				    if ( $(this).is(':checked') ) {
				    	/*Added by Padmavathi for T251IT-32 starts*/
						clearMessages();
						/*Added by Padmavathi for T251IT-32 ends*/
				    	 $(".hide").show();
				    	 $("#txtauthenticate").val('Y');
				    } else {
				    	/*Added by Padmavathi for T251IT-32 starts*/
						clearMessages();
						/*Added by Padmavathi for T251IT-32 ends*/
				    	$(".hide").hide();
				    	$("#txtauthenticate").val('N');
				    }
				});
			 if($("#txtauthenticate").val()=='N'){
				 $("#txtauthenticate").attr('checked', false);
				 $(".hide").hide();
				} else if($("#txtauthenticate").val()=='Y'){
					 $(this).attr('checked', true);
					 $(".hide").show();
					}

			$(document).on('change','#txttestemail', function() {
				 if ( $(this).is(':checked') ) {
			    	 $(".hideemail").show();
			      } else {
			    	$(".hideemail").hide();
			     }
			});
			$(document).on('click','#btntest',function(){
				clearMessages();
				$("#emailLoader").show();
				var json = new Object();
				json.smtpServer = $('#txtsmtp').val();
				json.port = $('#txtport').val();
				json.authentication = $('#txtauthenticate').val();
				json.userId = $('#txtuserid').val();
				json.password = $('#txtpassword').val();
				json.email = $('#txtemail').val();
				 /* Modified by Padmavathi for  TENJINCG-1017 starts*/
				json.userName = $('#txtusername').val();
				 /* Modified by Padmavathi for  TENJINCG-1017 ends*/
				 
				var jstring = JSON.stringify(json);
				$.ajax({
					url:'EmailServlet',
					data:'t=test&json=' + jstring,
					async:true,
					dataType:'json',
					success:function(data){
						$("#emailLoader").hide();
						if(data.status.toLowerCase() === 'success'){
							$('#testMessage').html(data.message);
							$('#testMessage').css('font-weight', 'bold');
							$('#testMessage').css({'color':'green'});
						}else{
							$('#testMessage').html(data.message);
							$('#testMessage').css('font-weight', 'bold');
							$('#testMessage').css({'color':'red'});
						}
					},
					error:function(data) {
						$("#emailLoader").hide();
						$('#testMessage').html(data.message);
						$('#testMessage').css('font-weight', 'bold');
						$('#testMessage').css({'color':'red'});
					}
				});
			});
			$(document).on('click', '#btnDelete', function(e) {
				e.preventDefault();
				if(confirm('Are you sure you want to delete ? Please note this action cannot be undone.')) {
					/*Modified by Padmavathi for TV2.8R2-24 starts*/
					/*$('#emailform').append($("<input type='hidden' id='del' name='del' value='true' />"))
					$('#emailform').submit();*/
					var csrftoken_form = $("#csrftoken_form").val();
					var $emailForm = $("<form action='EmailServlet' method='POST' />");
					$($emailForm).append($("<input type='hidden' id='del' name='del' value='true' />"))
					$($emailForm).append($("<input type='hidden' id='csrftoken_form' name='csrftoken_form' value='"+csrftoken_form+"' />"))
					$('body').append($emailForm);
					$emailForm.submit();
					/*Modified by Padmavathi for TV2.8R2-24 ends*/
				}else{
					return false;
				}
			});
			$(document).on('click','#btnRefresh',function(){
				
				window.location.href='EmailServlet?t=configuration';
			});
			/*Commented by Pushpalatha for TJNUN262-1 starts*/
			/*Added by Padmavathi for T251IT-5 starts*/
			/*$(document).on('blur','#txtuserid',function(){
				Added by Padmavathi for T251IT-32 starts
				clearMessages();
				Added by Padmavathi for T251IT-32 ends
				if($('#txtuserid').val()!=''){ 
					if(!validateUserId($('#txtuserid').val())){
						showMessage("Please enter valid User ID","error");
						 return false;
						}
				}
			});*/
			/*Commented by Pushpalatha for TJNUN262-1 ends*/
			$(document).on('blur','#txtemail',function(){
				/*Added by Padmavathi for T251IT-32 starts*/
				clearMessages();
				/*Added by Padmavathi for T251IT-32 ends*/
				if($('#txtemail').val()!=''){ 
					if(!validateUserId($('#txtemail').val())){
						showMessage("Please enter valid Recipient E-mail","error");
						 return false;
						}
				}
			});
			/*Added by Padmavathi for T251IT-5 ends*/
			
		});
/*Added by Padmavathi for T251IT-5 starts*/
/*Added by Pushpalatha for TJNUN262-1 starts*/
$(document).on('blur','#txtemail',function(){
	/*Added by Padmavathi for T251IT-32 starts*/
	clearMessages();
	/*Added by Padmavathi for T251IT-32 ends*/
	if($('#txtemail').val()!=''){ 
		if(!validateUserId($('#txtemail').val())){
			showMessage("Please enter valid Recipient E-mail","error");
			 return false;
			}
	}
});
/*Added by Pushpalatha for TJNUN262-1 ends*/
	function validateUserId(email) {
    var atstr = email.indexOf("@");
    var dotstr = email.lastIndexOf(".");
    	if (atstr<1 || dotstr<atstr+2 || dotstr+2>=email.length) {
      
    		return false;
    	}
    	else{
    		clearMessages();
    		return true;
    	}
	}
	/*Added by Lokanath for TENJINCG-1140 starts*/
	/*$(document).on('blur','#txtuserid',function(){
		clearMessages();
		if($('#txtuserid').val()!=''){ 
			if(!validateUserId($('#txtuserid').val())){
				showMessage("Please enter valid User ID","error");
				 return false;
				}
		}
	});*/
	/*Added by Lokanath for TENJINCG-1140 ends*/
	