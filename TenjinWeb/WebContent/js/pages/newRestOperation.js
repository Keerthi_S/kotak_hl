/**
 * 
 * <!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  newRestOperation.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
    23-05-2016          Naveen                 	New Code
    09-08-2017          Leelaprasad            	T25IT-74
    18-08-2017			Pushpalatha				T25IT-233
    19-08-2017         	Leelaprasad             T25IT-86 
    19-08-2017         	Sriram              	T25IT-244
    28-08-2017         	Padmavathi              defect T25IT-326
    07-12-2017			Padmavathi				TCGST-16
    14-02-2018			Preeti					TENJINCG-608
    25-03-2019			Pushpalatha				TENJINCG-968
    18-12-2020          Paneendra               TENJINCG-1242
 */
$(document).ready(function(){
	
	/*Added by Pushpalatha for TENJINCG-968 starts*/
	mandatoryFields('new_rest_operation_form');
	/*Added by Pushpalatha for TENJINCG-968 ends*/
	
	/*Added by Paneendra for TENJINCG-1242 starts*/
	$('#headerBlock').hide();
	
	$('#authType').change(function(){
		var authType=$('#authType').val();
		if(authType=='Apikey'){
			$('#headerBlock').show();
		}else{
			$('#headerBlock').hide();
		}
		
	});
	/*Added by Paneendra for TENJINCG-1242 ends*/
	 $('#tblNaviflowModules_body input').css({
			margin : '3px 0'
	 });
	 var status = $('#scrStatus').val();
	 if(status.toLowerCase() === 'success') {
		 showMessage($('#scrMessage').val(), 'success');
	 } else if(status.toLowerCase() === 'error') {
		 showMessage($('#scrMessage').val(), 'error');
	 }
	 
	 var optMethodVal = $('#optMethodVal').val();
	 $('#optMethod').children('option').each(function(){
		var val = $(this).val();
		if(val == optMethodVal){
			$(this).attr({'selected':true});
		}
	});
	 
	 var optReqTypeVal = $('#optReqTypeVal').val();
	 $('#optReqType').children('option').each(function(){
		var val = $(this).val();
		if(val == optReqTypeVal){
			$(this).attr({'selected':true});
		}
	});
	 
	 var optResTypeVal = $('#optResTypeVal').val();
	 $('#optResType').children('option').each(function(){
		var val = $(this).val();
		if(val == optResTypeVal){
			$(this).attr({'selected':true});
		}
	});
		/*Changed by Leelaprasad for the requiremnt defect fix T25IT-74 starts*/
	 $('#btnSave').click(function() {
			var validated = validateForm('new_rest_operation_form');
			if (validated != 'SUCCESS') {
				showMessage(validated, 'error');
				return false;
			}else{
				//Fix for T25IT-244
				var opName = $('#optName').val();
				if(/\s/g.test(opName)) {
					showMessage('Operation name cannot contain spaces', 'error');
					return false;
				}
				//Fix for T25IT-244 ends
				
				if($('#optMethod').val()==null){
					showMessage("Operation method is required", 'error');
					return false;
				}
			}
		});
	 /*Changed by Leelaprasad for the requiremnt defect fix T25IT-74 ends*/
	 
	 $('#btnBack').click(function(){
			var code = $("#code").val();
			var app=$("#appid").val();
			window.location.href='APIServlet?param=fetch_api'+ '&appid=' + app +'&paramval='+ code;
		});
	 
	 $('#btnRefresh').click(function(){
		 
			 var urlValid = $('#isUrlValid').val();
			 if(urlValid === false || urlValid === 'false') {
				 showMessage('Cannot refresh operations. URL is invalid. Please correct the URL and try again.', 'error');
				 return false;
			 }
			 
		 	$('.modalmask').show();
			$('#confirmation_dialog').show();
			
		});
	 
	 $('#btnCOk').click(function() {
			$('#confirmation_dialog').hide();
			$('#progress_dialog').show();
			$('#processing_icon').html("<img src='images/loading.gif' alt='In Progress..' />");
			$('#progressBar').show();
			$('#message').html('<p>Tenjin is refreshing the Operation. Please Wait...</p>');
			location.reload();
		});
		 
		 $('#btnCCancel').click(function(){
				$('.modalmask').hide();
				$('#confirmation_dialog').hide();
			});
	 
	 $('#btnNew').click(function(){
	 /*Added By padmavathi for T25IT-326 starts*/
		 clearMessagesOnElement($('#user-message'));
		 /*Added By padmavathi for T25IT-326 ends*/
		 var tbody = $("#tblNaviflowModules_body");
		 $("\
					<tr><td class='tiny'><input type='checkbox' name='paramNameCheck'/></td>\
					<td><input type='text' name='paramName' id='paramName' value='' maxlength='100' class='stdTextBox'/></td>\
					<td><input type='text' name='paramType' id='paramType' value='' maxlength='100' class='stdTextBox'/></td>\
					<td><input type='text' name='paramStyle' id='paramStyle' value='' maxlength='100' class='stdTextBox' /></td></tr>").appendTo(tbody);
		 
		 $('#tblNaviflowModules_body input').css({
				margin : '3px 0'
		 });
	});
	 
	$('#btnDelete').click(function(){
	/*Added By padmavathi for T25IT-326 starts*/
		 clearMessagesOnElement($('#user-message'));
		 /*Added By padmavathi for T25IT-326 ends*/
				 var count=0;
				 $("input:checkbox[name=paramNameCheck]:checked").each(function () {
					count++;
				  });
				 if (count == 0)
					 showMessage("Please select the atleast one paramater to Delete", 'error');	
				  else{
						 $("input:checkbox[name=paramNameCheck]:checked").each(function () {
							 $(this).parent().parent().remove();
							 /*Changed by Leelprasad for the requirement Defect fix T25IT-86 starts*/
							 clearMessages();
							 showMessage("Selected parameter(s) are deleted click on save to update the Operation", 'success');
							 /*Changed by Leelprasad for the requirement Defect fix T25IT-86 ends*/
						  });
				  }
				 $("#chk_all_parameters").prop('checked', false); 
	});
	/*Chnaged by Leelaprasad for TENJINCG-406 starts*/
	$('#btnResponseTableNew').click(function(){
		clearMessagesOnElement($('#user-message'));
		var tbody = $("#tblResponse_body");
		
		 $("\
					<tr><td class='tiny'><input type='checkbox' name='responseParamCheck'/></td>\
					<td><input type='text' name='opRespCode' id='txtstatus' value='' maxlength='10' class='stdTextBox respCodeClass'/></td>\
					<td><textarea type='text' name='opRespDesc' id='myIn' value='' placeholder='Enter Response' class='stdTextBox expandable respdesClass' /></td></tr>").appendTo(tbody);
		 
		 $('#tblResponse_body input').css({
				margin : '3px 0'
		 });
		 
	});
	$('#btnResponseTableDelete').click(function(){
		 clearMessagesOnElement($('#user-message'));
				 var count=0;
				 $("input:checkbox[name=responseParamCheck]:checked").each(function () {
					count++;
				  });
				 if (count == 0)
					 showMessage("Please select the atleast one paramater to Delete", 'error');	
				  else{
						 $("input:checkbox[name=responseParamCheck]:checked").each(function () {
							 $(this).parent().parent().remove();
							 clearMessages();
							 showMessage("Selected parameter(s) are deleted click on save to update the Operation", 'success');
						  });
				  }
				 $("#checkAllResponse").prop('checked', false); 
	});
	/*Chnaged by Leelaprasad for TENJINCG-406 ends*/
	
  /*  changed by Padmavathi for TCGST-16 starts*/
	 $(document).on("keyup", "#txtstatus", function() {
		var inputVal = $(this).val();
		var regex = new RegExp(/[^0-9]/g);
		var containsNonNumeric = inputVal.match(regex);
		if(containsNonNumeric){
			alert('Please enter only numbers.');
			$(this).val('');
		}
	});
	 /* changed by Padmavathi for TCGST-16 ends */
	/*Added by Preeti for TENJINCG-608 starts*/
	 $('.expandable').on('focus',function () {
		    $(this).animate({ height: "200px" });
		});
		$('.expandable').on('blur', function() {
			 $(this).animate({ height: "30px" });
		});
	/*Added by Preeti for TENJINCG-608 ends*/
		
});

 

