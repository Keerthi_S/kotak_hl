/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  tsmap_modal.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 13-Jun-2017           Sriram             New file added for TENJINCG-189
  08-Aug-2017			Roshni				T25IT-45
  19-06-2018            Padmavathi          T251IT-80
  06-03-2019			Pushpa				TENJINCG-978
  20-03-2019			Prem				 TNJN27-17
*/

$(document).ready(function() {
	var status = $('#status').val();
	var message = $('#message').val();
	/*Added by Pushpa for TENJINCG-978 starts*/
	$('#tcSequence').hide();
	$('#note').hide();
	$('#note1').hide();
	/*Added by Pushpa for TENJINCG-978 ends*/
	
	//$('#existingTestSet').select2();
	
	if(message.length > 0) {
		if(status.toLowerCase() === 'success'){
			showMessage(message,'success');
		}else if(status.toLowerCase() === 'error'){
			showMessage(message, 'error');
		}else if(status.toLowerCase() === 'complete') {
			showMessage(message, 'success');
		/*	commented by Padmavathi for  T251IT-80 starts*/
			/*$('#btnOk').hide();*/
			/*$('#btnCancel').attr('value','Close');*/
			/*	commented by Padmavathi for  T251IT-80 ends*/
		}
	}
	
	$('#btnCancel').click(function() {
	/* Modified by Roshni for T25IT-45 starts */
		/*window.parent.closeTestSetMapModal();
		var projid=$('#projid').val();
		window.location.href = 'TestCaseServlet?param=INIT_TC_PAGINATION&pid='+projid;*/
		/* Changed by Pushpalatha for TENJINCG-611  starts*/
		window.parent.location.href="TestCaseServletNew?t=list"
		/* window.parent.refreshPage();*/
			/* Changed by Pushpalatha for TENJINCG-611  starts*/
		 /* Modified by Roshni for T25IT-45 ends */
	});
	
	$('#existingTestSet').change(function() {
		if($(this).val() === '-1') {
			$('#newTestSet').prop('disabled',false);
			
			/*Added by Pushpa for TENJINCG-978 starts*/
			$('#newTestSetBlock').show();
			$('#tcSequence').hide();
			$('#note').hide();
			$('#note1').hide()
			$("#tcSequence").empty();
			/*Added by Pushpa for TENJINCG-978 starts*/
		}else{
			$('#newTestSet').prop('disabled',true);
			
			/*Added by Pushpa for TENJINCG-978 starts*/
			$('#newTestSetBlock').hide();
			var tsetRecId=$('#existingTestSet').val();
			$.ajax({
				
				url:'TestCaseServlet',
				data:{param:"fetchMappedTestCase",tsid:tsetRecId},
				dataType:'json',
				success:function(data) {
					$("#tcSequence").empty();
					 var status = data.status;
					 var tcseq=data.TC_SEQ;
					
					 var length=Object.keys(tcseq).length;
			            if(status == 'SUCCESS')
			            {
			            	var $tcSeqLabel=$("<div class='grid_7' style='padding:5px;'><p id='seq' style='font-size:1.2em; font-weight:bold;'>Choose sequence of Testcase in TestSet </p></div>");
			            	$('#tcSequence').append($tcSeqLabel);
			            	var $tcSeq=$("<select id='tcseq' class='stdTextBoxNew' style='width:100%'></select>");
			            	$tcSeq.append($("<option value='-1'>--Select one--</option>"));
			            	
			            	for(var i in tcseq) {
			            		/*Modified by prem for  TNJN27-17 starts*/
			            		$tcSeq.append($("<option value="+i+">"+i+"-"+tcseq[i]+"</option>")); 
			            		/*Modified by prem for  TNJN27-17 ends*/
			            	}
			            	
			            	
			            	$('#tcSequence').append($tcSeq);
			            	if(length>0){
			            		$('#tcSequence').show();
				            	$('#note').show();
				            	$('#note1').hide()
			            	}else{
			            		$('#tcSequence').hide();
				            	$('#note').hide()
				            	$('#note1').show()				            	
			            	}
			            	
			            }
				},
				error:function(data) {
					
				}
				
			});
			/*Added by Pushpa for TENJINCG-978 starts*/
		}
	});
	
	$('#newTestSet').change(function() {
		var val = $(this).val();
		if(val.length > 0) {
			$('#existingTestSet').prop('disabled',true);
			$('#existingTestSet').val('-1');
		}else{
			$('#existingTestSet').prop('disabled',false);
		}
	});
	
	$("#btnOk").click(function() {
		var type = '';
		clearMessages();
		/*Added by Pushpa for TENJINCG-978 starts*/
		var tcSeq=$('#tcseq').val();
		/*Added by Pushpa for TENJINCG-978 starts*/
		if($('#existingTestSet').val() !== '-1'){
			type = 'existing';
			/*Modified by Pushpa for TENJINCG-978 starts*/
			window.location.href='TestCaseServlet?param=add_cases_to_set&type=' + type + '&tsid=' + $('#existingTestSet').val()+'&tcSeq='+tcSeq;
			/*Modified by Pushpa for TENJINCG-978 starts*/
		}else if($('#newTestSet').val().length > 0) {
			type='new';
			window.location.href='TestCaseServlet?param=add_cases_to_set&type=' + type + '&tsname=' + $('#newTestSet').val();
		}else{
			showMessage('Please choose an existing Test Set or specify a name for a new Test Set', 'error');
			return false;
		}
	});
});