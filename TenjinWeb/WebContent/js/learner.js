/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  learner.js


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

$(document).ready(function(){
	populateAuts();
	
	/***********************************
	 * Click download link for test data tempate
	 **********************************/
	/*$('.template-gen').click(function(){
		alert($(this).attr('mod'));
	});*/
	
	$(document).on('click','.template-gen',function(e){
		//alert($('#chk_all:checked').length > 0);
		//alert($('#chk_all').prop('checked'));
		var mod = $(this).attr('mod');
		var app = $(this).attr('app');
		var txnMode = $(this).attr('txnmode');	/*20-Oct-2014 WS*/
		
		var $td = $(this).parent();
		$(this).hide();
		var $img = $("<img src='images/inprogress.gif' alt='Please Wait'/>");
		$td.append($img);
		
		$.ajax({
			url:'NaviflowServlet',
			/*20-Oct-2014 WS: Starts*/
			/*data:'param=DOWNLOAD_DATA_TEMPLATE&m=' + mod + '&a=' + app,*/
			data:'param=DOWNLOAD_DATA_TEMPLATE&m=' + mod + '&a=' + app + '&txnMode=' + txnMode,
			/*20-Oct-2014 WS: Ends*/
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.status;
				if(status == 'SUCCESS'){
					var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
					$("body").append($c);
                    $("#downloadFile").get(0).click();
                    $c.remove();
				}else{
					var message = data.message;
					showMessage(message,'error');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown, 'error');
			}
		});
		
		$img.remove();
		$(this).show();
		
		
	});
	
	/***********************************
	 * Change AUT in the drop down
	 **********************************/
	$('#lstApplication').change(function(){
		var selApp = $(this).val();
		if(selApp == '-1'){
			$('#tblNaviflowModules_body').html('');
			$('#modulesInfo').hide();
			$('#additionalInfo').hide();
			clearMessages();
		}else{
			populateModules(selApp);
			$('#modulesInfo').show();
			$('#additionalInfo').show();
		}
	});
	
	/***********************************
	 * Learn Button - Click
	 ***********************************/
	$('#btnLearn').click(function(){
		$('#learnType').val('NORMAL');
		var selectedModules = '';
		$('#tblNaviflowModules').find('input[type="checkbox"]:checked').each(function () {
		       //this is the current checkbox
		       if(this.id != 'chk_all_modules'){
/*20-Oct-2014 WS: Starts*/
					//selectedModules = selectedModules + this.id + ';';
					var txnMode = 'G';
					if ($(this).parents('tr').find('.wsmode'))
						txnMode = 'W';
					var wsurl = $(this).parents('tr').find('.wsurl').val();
					var wsop = $(this).parents('tr').find('.wsop').val();

					if (this.id != "")
						selectedModules = selectedModules + this.id + '|' +txnMode  + '|' +wsurl + '|' +wsop +';';
/*20-Oct-2014 WS: Ends*/
		       }
		});
		//alert(selectedScripts.charAt(selectedScripts.length-1) == ';');
		if( selectedModules.charAt(selectedModules.length-1) == ';'){
			selectedModules = selectedModules.slice(0,-1);
		}
		if(selectedModules.length > 0){
			//alert(selectedScripts);
			$('#moduleList').val(selectedModules);
			//alert('#moduleList').val();
			
		}
		else{
			showMessage('Please choose atleast one script to continue','error');
			return false;
		}
		
		if($('#txtTarget').val() == ''){
			showMessage('Please specify Target Machine','error');
			return false;
		}
		
		if($('#lstBrowserType').val() == '-1'){
			showMessage('Please specify Browser Type','error');
			return false;
		}
	});
	
	/*20-Oct-2014 WS: Starts*/
	$(document).on('click','.learn-mode',function(){
		var $t = $(this);
		var app = $("#lstApplication").val();
		var mod = $(this).parent('td').next().html();
		var txnMode = $(this).val();
		$.ajax({
			url:'AutServlet',
			data:'param=ISLEARNINGCOMPLETE&mod=' + mod + '&app=' + app + '&txnMode=' + txnMode,
			async:false,
			dataType:'json',
			success:function(data){
				var status = data.learnt;
				if(status == 'YES'){
					if ($t.parents('tr').children('td:eq(4)').children('a').length) {
						$t.parents('tr').children('td:eq(4)').children('a').attr({"href": "NaviflowServlet?param=xmlgen&func=" + mod + "&app=" + app + "&txnMode=" +$t.val()});
						$t.parents('tr').children('td:eq(5)').children('a').attr({txnMode: $t.val()});
					} else {
						$t.parents('tr').children('td:eq(4)').append($("<a />").attr({href: "NaviflowServlet?param=xmlgen&func=" + mod + "&app=" + app + "&txnMode=" +$t.val()}));
						$t.parents('tr').children('td:eq(5)').append($("<a />").attr({app: app, mod: mod, txnMode: $t.val()}).text("Download"));
					}
				}else{
					$t.parents('tr').children('td:eq(4)').html('Not Learnt');
					$t.parents('tr').children('td:eq(5)').html('Not Available');
				}
			},
			error:function(xhr, textStatus, errorThrown){
				showMessage(errorThrown, 'error');
			}
		});
	});
	/*20-Oct-2014 WS: Ends*/
});


function populateAuts(){
	var jsonObj = fetchAllAuts();
	
	var status = jsonObj.status;
	if(status == 'SUCCESS'){
		var html ="<option value='-1'>-- Select One --</option>";
		var jArray = jsonObj.auts;
		
		for(i=0;i<jArray.length;i++){
			var aut = jArray[i];
			html = html + "<option value='" + aut.id + "'>" + aut.name + "</option>";
		}
		
		$('#lstApplication').html(html);
		
	}else{
		showMessage(jsonObj.message, 'error');
	}
}

function populateModules(app){
	var jsonObj = fetchModulesForApp(app);
	
	var status = jsonObj.status;
	
	if(status == 'SUCCESS'){
		var jArray = jsonObj.modules;
		var html = '';
		for(i=0;i<jArray.length;i++){
			var json = jArray[i];
			html = html + "<tr>";
			html = html + "<td class='tiny'><input type='checkbox' id='" + json.code + "|" + json.menu + "|" + json.name + "'/></td>";
			/*20-Oct-2014 WS: Starts*/
			html = html + "<td><input type='radio' name='learn-mode" +i +"' value='G' checked class='tiny-radio learn-mode' />GUI <input type='radio' name='learn-mode" +i +"' value='W' class='tiny-radio learn-mode' />WS </td>";
			/*20-Oct-2014 WS: Ends*/
			html = html + "<td>" + json.code + "</td>";
			html = html + "<td>" + json.name + "</td>";
			var learnt = json.learnt;
			if(learnt == 'YES'){
				/*20-Oct-2014 WS: Starts*/
				//html = html + "<td><a href='NaviflowServlet?param=xmlgen&func=" + json.code + "&app=" + app + "' target='_blank'>Download</a>";
				html = html + "<td><a href='NaviflowServlet?param=xmlgen&func=" + json.code + "&app=" + app + "&txnMode=G' target='_blank'>Download</a>";
				/*20-Oct-2014 WS: Ends*/
				html = html + "<td><a href='#' class='template-gen' app='" + app + "' mod='" + json.code + "' txnMode=G '" +"'>Download</a>";
			}else{
				html = html + "<td>Not Learnt</td>";
				html = html + "<td>Not Available</td>";
			}
			/*20-Oct-2014 WS: Starts*/
			html = html + "<td class='hidden-column'><input type='text' class='wsurl' value='" + json.wsurl + "'/></td>";
			html = html + "<td class='hidden-column'><input type='text' class='wsop' value='" + json.wsop + "'/></td>";
			/*20-Oct-2014 WS: Ends*/
			html = html + "</tr>";
		}
		
		$('#tblNaviflowModules_body').html(html);
		$('#tblNaviflowModules').dataTable();
	}else{
		showMessage(jsonObj.message, 'error');
	}
}