<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  runprogress.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
  
*/

-->

<%@page import="com.ycs.tenjin.util.HatsConstants"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Run Test - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/bootstrap.min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/rprogress.css'/>
		<link rel="Stylesheet" href="css/summarypagetable.css" />
		<link rel="Stylesheet" href="css/css-circular-prog-bar.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		
		<script type="text/javascript">
			var contextPath='<%=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()%>';
			console.log(contextPath);
		</script>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<!-- <script type="text/javascript" src="js/jquery.dataTables.js"></script> -->
		<script type="text/javascript" src="js/formvalidator.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/runprogress.js'></script>
		<script type="text/javascript" src="js/pages/notification.js"></script>
		
	</head>
	<body>
		<input type='hidden' id='runId' value='${run.id }' />
		<c:if test="${not empty manualField }">
			<input type='hidden' value='${manualField.location }' id="mloc"/>
			<input type='hidden' value='${manualField.label }' id='mflbl' />
		</c:if>
		
		<div class='title' style='box-sizing:content-box;'>
			<p>Run Information</p>
		</div>
		<div class='toolbar'>
			<input type='button' id='btnRefresh' value='Refresh Now' class='imagebutton refresh'/>
			<input type='button' id='btnAbortRun' value='Abort Run' class='imagebutton cancel' />
			<c:if test="${run.deviceFarmFlag eq 'Y' }">
				<input type='button' id='btnLiveView' value='Live View' class='imagebutton live'/>
			</c:if>
			<c:if test="${pauseOrContinue eq 'false' }">
				<input type='button' id='btnContinue' value='Continue' class='imagebutton continue'/>
			</c:if>
		</div>
		<div id='user-message'></div>
		<div class='row' id='report-header-row'>
			<div class='col-md-8 field-section'>
				<div class='fs_title'>Run Information</div>
			</div>
			<div class='col-md-4 field-section'>
				<div class='fs_title'>Overall Progress</div>
			</div>
		</div>
		<div class='row' id='run-summary-row'>
			<div class='col-md-4 field-section' style='height:143px;'>
				<table class='layout'>
					<tr>
						<td class='layout-label'>Run ID</td>
						<td class='layout-value'>${run.id }</td>
					</tr>
					<tr>
						<td class='layout-label'>Started By</td>
						<td class='layout-value'>${run.user }</td>
					</tr>
					<tr>
						<td class='layout-label'>Started On</td>
						<td class='layout-value' id="startTimeStamp"><fmt:formatDate value='${runStartTimestamp }' type="both" pattern="dd MMM, yyyy HH:mm:ss" /></td>
					</tr>
					<tr>
						<td class='layout-label'>Ended On</td>
						<td class='layout-value'>Not Available</td>
						
					</tr>
					<tr>
						<td class='layout-label'>Elapsed Time</td>
						<td class='layout-value' id='elapsedTime'></td>
					</tr>
					<c:if test="${run.deviceRecId gt 0 }">
						<c:if test="${not empty device }">
							<td class='layout-label'>Device Id/Name</td>
							<td class='layout-value' title='${device.deviceId }&nbsp;/&nbsp;${device.deviceName }&nbsp;(${device.deviceType })'>${device.deviceId }&nbsp;/&nbsp;${device.deviceName }&nbsp;(${device.deviceType })</td>
						</c:if>
					</c:if>
				</table>
			</div>
			<div class='col-md-4 field-section' style='height:143px;'>
				<table class='layout'>
					<tr>
						<td class='layout-label'>Domain</td>
						<td class='layout-value' title='${sessionScope.TJN_SESSION.project.domain }'>${sessionScope.TJN_SESSION.project.domain }</td>
					</tr>
					<tr>
						<td class='layout-label'>Project</td>
						<td class='layout-value' title='${sessionScope.TJN_SESSION.project.name }'>${sessionScope.TJN_SESSION.project.name }</td>
					</tr>
					<tr>
						<td class='layout-label'>Target IP</td>
						<c:choose>
							<c:when test="${run.deviceFarmFlag eq 'Y' }">
								<td class='layout-value'>Device Farm</td>
							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="${run.machine_ip eq '0:0:0:0:0:0:0:1' }">
										<td class='layout-value'>Localhost</td>
									</c:when>
									<c:otherwise>
										<td class='layout-value'>${run.machine_ip }</td>
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<td class='layout-label'>Port</td>
						<td class='layout-value'>${run.targetPort }</td>
					</tr>
					<tr>
						<td class='layout-label'>Browser</td>
						<td class='layout-value'>${run.browser_type }</td>
					</tr>
					<c:if test="${run.deviceRecId gt 0 }">
						<td class='layout-label'>Platform</td>
						<td class='layout-value'>${device.platform }&nbsp;&nbsp;${device.platformVersion }</td>
					</c:if>
				</table>
			</div>
			<div class='col-md-4 field-section' style='height:143px;text-align:center;border:1px solid #fff'>
				<div id="circular_progress_bar" style='display:flex;align-items:center;justify-content:center;'></div>
			</div>
		</div>
		<div class='row' id='current-execution-block-row'>
			<div class='col-md-6 field-section'>
				<div class='fs_title'>Current Test Case</div>
				<div>
					<table class='layout'>
						<tr>
							<td class='layout-label' width="75px">ID</td>
							<td class='layout-value' id='currentTestCaseId'>${currentTestCase.tcId }</td>
						</tr>
						<tr>
							<td class='layout-label' width="75px">Name</td>
							<td class='layout-value' id='currentTestCaseName'>${currentTestCase.tcName }</td>
						</tr>
					</table>
				</div>
			</div>
			<div class='col-md-6 field-section'>
				<div class='fs_title'>Current Step</div>
				<div>
					<table class='layout'>
						<tr>
							<td class='layout-label' width='75px'>ID</td>
							<td class='layout-value' id='currentTestStep'>${currentStep.id } - ${currentStep.shortDescription }</td>
						</tr>
						<tr>
							<td class='layout-label' width='75px'>Iteration</td>
							<td class='layout-value' id='currentIteration'>${currentStep.dataId }</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class='row' id='test-case-summary-title-row'>
			<div class='col-md-12 field-section'>
				<div class='fs_title'>Test Cases</div>
			</div>
		</div>
		<div class='row' id='test-case-summary-row' style='padding-bottom:50px;'>
			<div class='col-md-12 field-section'>
				<table class='summtable' id='tc-table' style='width:100%;'>
					<thead>
						<tr>
							<th style='width:30px;text-align:center;'>Status</th>
							<th style='width:140px;'>Test Case ID</th>
							<th>Test Case Name</th>
							<th style='width:40px;text-align:center;'>Steps</th>
							<th style='width:40px;text-align:center;'>Executed</th>
							<th style='width:40px;text-align:center;'>Passed</th>
							<th style='width:40px;text-align:center;'>Failed</th>
							<th style='width:40px;text-align:center;'>Error</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="statusPass" value='<%=HatsConstants.ACTION_RESULT_SUCCESS%>' />
						<c:set var="statusFail" value='<%=HatsConstants.ACTION_RESULT_FAILURE%>' />
						<c:set var="statusError" value='<%=HatsConstants.ACTION_RESULT_ERROR%>' />
						<c:set var="statusExecuting" value='<%=HatsConstants.SCRIPT_EXECUTING%>' />
						<c:set var="statusNotStarted" value='Not Started' />
						<c:forEach items="${testCaseSummary }" var="test">
							<tr data-row-id='${test.tcRecId }'>
								<c:set var="imgUrl" value="images/error_20c20.png" />
								<c:choose>
									<c:when test="${test.tcStatus eq statusExecuting }">
										<c:set var="imgUrl" value="images/ajax-loader.gif" />
										<c:set var="endTimestamp" value="N/A" />
									</c:when>
									<c:when test="${test.tcStatus eq statusFail }">
										<c:set var="imgUrl" value="images/failure_20c20.png" />
									</c:when>
									<c:when test="${test.tcStatus eq statusPass }">
										<c:set var="imgUrl" value="images/success_20c20.png" />
									</c:when>
									<c:when test="${test.tcStatus eq statusNotStarted }">
										<c:set var="imgUrl" value="images/queued_20c20.png" />
									</c:when>
									<c:otherwise>
										<c:set var="imgUrl" value="images/error_20c20.png" />
									</c:otherwise>
								</c:choose>
								<td title='${test.tcStatus }' style='text-align:center;'><img id='${test.tcRecId }_status_img' src='${imgUrl }' /></td>
								<td title='${test.tcId }'>${test.tcId }</td>
								<td title='${test.tcName }'>${test.tcName }</td>
								<td style='text-align:center;' id='${test.tcRecId }_totalSteps'>${test.totalSteps }</td>
								<td style='text-align:center;' id='${test.tcRecId }_executedSteps'>${test.totalExecutedSteps }</td>
								<td style='text-align:center;' id='${test.tcRecId }_passedSteps'>${test.totalPassedSteps }</td>
								<c:set var="failedColorClass" value=""/>
								<c:set var="errorColorClass" value=""/>
								<c:if test="${test.totalFailedSteps gt 0 }">
									<c:set var="failedColorClass" value="red"/>
								</c:if>
								<c:if test="${test.totalErrorredSteps gt 0 }">
									<c:set var="errorColorClass" value="red"/>
								</c:if>
								<td style='text-align:center;' id='${test.tcRecId }_failedSteps' class='${failedColorClass }'>${test.totalFailedSteps }</td>
								<td style='text-align:center;' id='${test.tcRecId }_errorSteps' class='${errorColorClass }'>${test.totalErrorredSteps }</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>