<!--
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  changepassword.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright ? 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %> 
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- New File Added by Prafful for Req#TJN_23_05 on 10-Aug-2016 -->

<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION

-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Change Password</title>
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
		<link rel="Stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/data-grids.js'></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/changepassword.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/v2/user_new.js'></script>
	</head>
	<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		<div class='title'>Change Password</div>
		<%
		String userId = request.getParameter("userid");
		String userName = request.getParameter("username");
		%>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
		<form name='change-password-form' action='TenjinPasswordResetServlet' method='POST'>
		
			<input type='hidden' name='userId' value='<%=userId %>'/>
			<input type='hidden' name='userName' value='<%=userName %>'/>
			 <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
				<input type='submit' id='btnChange' value='Change' class='imagebutton login'/>
			</div>
			
			<div id='user-message'></div>
			
			<div class='form container_16'>
				<div class='fieldSection grid_8'>
								
					<div class='grid_3'><label for='newPassword'>New Password</label></div>
					<div class='grid_4'>
						<input type='password' id='newPassword' name='newPassword' class='stdTextBox' />
					</div>
					
					<div class='clear'></div>
					
					<div class='grid_3'><label for='confirmPassword'>Confirm Password</label></div>
					<div class='grid_4'>
						<input type='password' id='confirmPassword' name='confirmPassword' class='stdTextBox' />
					</div>
					
					
					<div class='clear'></div>
				</div>
				
				<div style="width: 200% " >
							<table>
								<thead>
									<tr>
										   <th style="font-weight:bold; color:red">Password must contain</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><p class='ptext'> Minimum 8 characters</p> </td></tr>
									
									<tr>
										<td><p class='ptext'> At least one numeric character(e.g. 0-9)</p></td>
									</tr>
									<tr>
										<td><p class='ptext'> At least one special character(e.g~!@#$%^&*()_-+=)</p> </td>
									</tr>
									<tr>
										<td><p class='ptext'> Combination of upper and lower case alphabetic character(e.g A-Z,a-z)</p></td>
									</tr>

								</tbody>
							</table>
						</div>
						<div id='resetPassword'>
               
                 	<p style='font-weight:bold; font-size:14px; color:blue; padding-left:40px;'> Please choose a security question and answer to reset password</p>
						<br>
					<div class='fieldSection grid_8'>
						<div class='grid_3'><label>Question</label></div>
						<select id='question' name='question' class='stdTextBox '  >
							
							<c:forEach var="entry" items="${securityQueAndAns}">
								<option value='${entry.key}'>${entry.key}</option>
							</c:forEach>
						</select>
						<div class='clear'></div>
						<div class='grid_3'><label>Answer</label></div>
						<input type='text' id='answer' name='answer' class='stdTextBox' />
					</div>
					
					</div>
			</div>
			
		</form>
	</body>
</html>