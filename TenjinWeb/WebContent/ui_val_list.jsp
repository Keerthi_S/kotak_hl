<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: ui_val_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->

<%@page import="com.ycs.tenjin.project.TestStep"%>
<%@page import="com.ycs.tenjin.bridge.pojo.project.UIValidation"%>
<%@page import="java.util.List"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
  <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>UI Validations for Step - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="js/formvalidator.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/ui_val_list.js'></script>
	</head>
	<body>
		<%
		Cache<String, Boolean> csrfTokenCache = null;
	     csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
	     session.setAttribute("csrfTokenCache", csrfTokenCache);
	     UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
		
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		User cUser = tjnSession.getUser();
		Project project = null;
		project = tjnSession.getProject();
		
		String tsRecId = (String) request.getAttribute("TSTEP_REC_ID");
		
		List<UIValidation> vals = (List<UIValidation>) request.getAttribute("VALS");
		TestStep step = (TestStep) request.getAttribute("STEP");
		Map<String, Object> map = (Map<String, Object>)request.getSession().getAttribute("SCR_MAP");
		String status = (String)map.get("STATUS");
		String message = (String)map.get("MESSAGE");
		%>
		
		
		<div class='title'>
			<p>UI Validations for Step</p>
			<div id='prj-info'>
				<span style='font-weight:bold'>Domain: </span>
				<span><%=project.getDomain() %></span>
				<span>&nbsp; &nbsp;</span>
				<span style='font-weight:bold'>Project: </span>
				<span><%=project.getName() %></span>
			</div>
		</div>
		
		<form name='main_form' action='' method='POST'>
	<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		
		
			<input type='hidden' id='tstepRecId' value='<%=tsRecId %>' />
		
			<div class='toolbar'>
				<input type='button' class='imagebutton back' value='Back' id='btnBack' />
				<input type='button' class='imagebutton new' value='New Validation Step' id='btnNew' />
				<input type='button' class='imagebutton delete' value='Delete' id='btnDelete'/>
			</div>
			<%
			if(status != null && status.equalsIgnoreCase("SUCCESS")){
				if(message != null && !message.equalsIgnoreCase("")){
				%>
				<div id='user-message' class='msg-success' style='display:block;'>
					<div class="msg-icon">&nbsp;</div>
					
					<div class="msg-text"><%=message %></div>
					
					
				</div>
				<%
				}
			}else if(status != null && !status.equalsIgnoreCase("SUCCESS")){
				%>
				<%if(message!=null && !message.equalsIgnoreCase("")){%>
				<div id='user-message' class='msg-err' style='display:block;'>
					<div class="msg-icon">&nbsp;</div>
					
					<div class="msg-text"><%=message %></div>
					
				</div>
				<%} %>
				<%
			}else{
				%>
				 <div id='user-message'></div>
				<%
			}
			%>
			
			
			<div id='user-message'></div>
			<div class='form container_16'>
				<fieldset>
					<legend>Test Step Information</legend>
					<div class='fieldSection grid_7'>
						<div class='grid_2' style='text-align:right;'><label for='testCase'>Test Case</label></div>
						<div class='grid_4'>
							<input type='text' id='testCase' name='testCase' class='stdTextBox' readonly value='<%=step.getParentTestCaseId() %>'/>
						</div>
						
						<div class='clear'></div>
						
						<div class='grid_2'  style='text-align:right;'><label for='application'>Application</label></div>
						<div class='grid_4'>
							<input type='text' id='application' name='application' class='stdTextBox' readonly value='<%=step.getAppName() %>'/>
						</div>
						
					</div>
					
					<div class='fieldSection grid_7'>
						<div class='grid_2' style='text-align:right;'><label for='testStep'>Step</label></div>
						<div class='grid_4'>
							<input type='text' id='testStep' name='testStep' class='stdTextBox' readonly value='<%=step.getId() %>'/>
						</div>
						
						<div class='clear'></div>
						
						<div class='grid_2' style='text-align:right;'><label for='function'>Function</label></div>
						<div class='grid_4'>
							<input type='text' id='function' name='function' class='stdTextBox' readonly value='<%=step.getModuleCode() %>'/>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>UI Validations for this step</legend>
					<div class='fieldSection grid_15'>
						<table class='bordered' style='width:100%' id='tblValSteps'>
							<thead>
								<tr>
									<th><input type='checkbox' id='chk_all' class='tbl-select-all-rows' title='Select All' /></th>
									<th>Validation ID</th>
									<th>Validation</th>
									<th>Description</th>
								</tr>
							</thead>
							<tbody>
								<%
								if(vals != null && vals.size() > 0) {
									for(UIValidation val : vals) {
										%>
										<tr>
											<td><input type='checkbox' id='<%=val.getRecordId() %>' class="chk_user" /></td>
											<td><%=val.getRecordId() %></td>
											<td><a href='UIValidationServlet?param=view_ui_val_step&valrecid=<%=val.getRecordId()%>'><%=val.getType().getName() %></a>
											<td style='text-align:left;'><%=val.getType().getDescription() %></td>
										</tr>
										<%
									}
								}
								%>
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
		</form>
		
	</body>
</html>