<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Tenjin - Intelligent Enterprise Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel="SHORTCUT ICON" HREF="images/yethi.png">
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		
		<script>
			
			$(document).ready(function() {
				
				loadDescriptor();
				
				$('#btnCancel').click(function() {
					window.parent.closeModal();
				});
				
				$('#btnSave').click(function() {
					$('#update_descriptor_form').submit();
				});
				
				var status = $('#status').val();
				var message = $('#message').val();
				if(status.toLowerCase() === 'success' && message !== '' ) {
					showMessage(message,'success');
				} else if(status.toLowerCase() === 'error' && message !== '') {
					showMessage(message,'error');
				}
			});
			
			function loadDescriptor() {
				$('#progressImg').show();
				
				$.ajax({
					url:'APIServlet?param=load_descriptor&app=' + $('#app').val() + '&api=' + $('#api').val() + '&op=' + $('#op').val() + '&type=' + $('#type').val(),
					dataType:'json',
					success:function(data) {
						if(data.status.toLowerCase() === 'success') {
							$("#descriptorValue").html(data.descriptor);
							$('#progressImg').hide();
						}else{
							$("#descriptorValue").html('');
							$('#progressImg').hide();
							showMessage('Could not load '+ $('#type').val() + ". Please contact Tenjin Support.", 'error');
						}
					}, 
					
					error:function(xhr, textStatus, errorThrown){
						showMessage('Could not load '+ $('#type').val() + ". Please contact Tenjin Support.", 'error');
						
						$('#progressImg').hide();
					}
				});
			}
		
		</script>
		
	</head>
	<body>
		<%
	    Cache<String, Boolean> csrfTokenCache = null;
	    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
	    session.setAttribute("csrfTokenCache", csrfTokenCache);
	    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
	    session.setAttribute ("csrftoken_session", csrftoken);
		String appId = request.getParameter("app");
		String apiCode = request.getParameter("api");
		String operation = request.getParameter("op");
		String descType = request.getParameter("type");
		String status = Utilities.trim(request.getParameter("status"));
		String message = Utilities.trim(request.getParameter("message"));
		String dType = "";
		if("request".equalsIgnoreCase(descType)) {
			dType = "Request";
		}else {
			dType = "Response";
		}
		%>
	
		<div class='title'>
			<%
			if(descType.equalsIgnoreCase("request")) {
				%>
				<p>Set/Edit Request</p>
				<%
			}else{
				%>
				<p>Set/Edit Response</p>
				<%
			}
			%>
		</div>
		<form id='update_descriptor_form' action='APIServlet' method='POST'>
		<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
				<input type='button' id='btnSave' value='Save' class='imagebutton save' />
				<input type='button' id='btnCancel' value='Cancel' class='imagebutton cancel'/>
				
				<img id='progressImg' src='images/inprogress.gif' style='display:none;'/>
				
			</div>
			<input type='hidden' id='dType' name='dType' value='<%=dType %>' />
			<input type='hidden' id='app' name='app' value='<%=appId %>'/>
			<input type='hidden' id='api' name='api' value='<%=apiCode %>'/>
			<input type='hidden' id='op' name='op' value='<%=operation %>' />
			<input type='hidden' id='type' name='type' value='<%=descType %>' />
			<input type='hidden' id='requesttype' name='requesttype' value='update_descriptor' />
			
			<input type='hidden' id='status' value='<%=status %>'/>
			<input type='hidden' id='message' value='<%=message %>'/>
			
			
			
			<div id='user-message'></div>
			
			<div class='form container_16'>
				<div id="descriptorContainer" style="
				    display: block;
				    width: 561px;
				    margin-left: 16px;
				    margin-right: 16px;
				    margin-top: 2px;
				">
					<textarea id="descriptorValue" name='descriptor' class="stdTextBox" style="
					    width: 100%;
					    min-height: 360px;
					"></textarea>
				</div>
			</div>
		</form>
	</body>
</html>