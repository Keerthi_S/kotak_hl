<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  projectruns.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 
*/

-->
<%@page import="java.util.List"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.ycs.tenjin.bridge.BridgeProcess"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="com.ycs.tenjin.run.TestRun"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<!DOCTYPE html>

<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tenjin - Intelligent Enterprise Testing Engine</title>





<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/bordered.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel='stylesheet' href='css/bootstrap.min.css' />
<link rel='stylesheet' href='css/tenjin-bootstrap.css' />
<link rel='stylesheet' href='css/jquerysctipttop.css' />
<link rel='stylesheet' href='css/jquery-ui.css' />
<link rel="SHORTCUT ICON" HREF="images/yethi.png">

<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src=' js/bootstrap.min.js'></script>
<script type='text/javascript' src=' js/simple-bootstrap-paginator.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src="js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="js/jquery-1.11.4-ui.js"></script>
<script type='text/javascript' src='js/pages/projectruns.js'></script>
<script type="text/javascript">
		$(function() {
   		    $( "#datepicker" ).datepicker();
 			 });
		</script>
<style type="text/css">
.ui-datepicker-trigger {
    margin-bottom:5px;
 
}
</style>
</head>
<body>
	<%
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Project project = tjnSession.getProject();
	
		
		
		Map<String, Object> map = (Map<String, Object>)request.getSession().getAttribute("PRJ_RUN_MAP");
		String status = (String)map.get("STATUS");
		String message = (String)map.get("MESSAGE");
		
		ArrayList<TestRun> runs = (ArrayList<TestRun>)map.get("ALL_RUNS");
		ArrayList<User> userList = (ArrayList<User>)map.get("PRJ_USERS");
		
		String dateFormat = TenjinConfiguration.getProperty("DATE_FORMAT");
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Map<String, String> labelClassMap = new HashMap<String, String>();
		labelClassMap.put("Error", "label-error");
		labelClassMap.put("Fail", "label-fail");
		labelClassMap.put("Pass", "label-pass");
		labelClassMap.put("Aborted", "label-aborted");
		labelClassMap.put("Not Started", "label-not-started");
		labelClassMap.put("Executing", "label-executing");
		
		%>
	<div class='title' style='box-sizing: content-box; font-size: 12px;'>
		<p>Runs in this Project</p>
		<div id='prj-info'>
			<span style='font-weight: bold'>Domain: </span> <span><%=project.getDomain() %></span>
			<span>&nbsp&nbsp</span> <span style='font-weight: bold'>Project:
			</span> <span><%=project.getName() %></span>
		</div>
	</div>

	<div class='toolbar'>
		<input type="button" value="Filter" id="btnfilter"
			class="imagebutton search" data-toggle="modal"
			data-target="#filtermodal"> <input type="button"
			value="Only My Runs" id="btnMyRuns" class="imagebutton user">
		<input type='button' value='Show all Runs' id='showAllRuns'
			class='imagebutton refresh' />
	</div>

	<div class='container-fluid'>
		<div class='col-md-12' style='margin-top: 15px;'>
			<table id='prj_runs' class='table with-pager'>
				<thead>
					<tr>
						<th>Run Id</th>
						<th>Test Set</th>
						<th>Started By</th>
						<th>Start Time</th>
						<th>End Time</th>
						<th>Elapsed Time</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<%
						if(runs != null && runs.size() > 0) {
							for(TestRun run : runs) {
								%>
					<tr>
						<%
								  	String hrefUrl = "";
									if(run.getStatus().equalsIgnoreCase("executing")){
										if(BridgeProcess.APIEXECUTE.equalsIgnoreCase(run.getTaskType())){
											hrefUrl = "ExecutorServlet?param=api_run_progress&paramval=" + run.getId();
										}else{
											hrefUrl = "ExecutorServlet?param=run_progress&paramval=" + run.getId();
										}
									}else{
										hrefUrl = "ResultsServlet?param=run_result&run=" + run.getId() + "&callback=runlist";
									}  
									%>
						<td><a href='<%=hrefUrl%>' title='View Run Summary'><%=run.getId() %></a></td>
						<td><%=run.getTestSet().getName() %></td>
						<td><%=run.getUser() %></td>
						<td><%=run.getStartTimeStamp() != null ? sdf.format(new Date(run.getStartTimeStamp().getTime())) : "NA"%></td>
						<td><%=run.getEndTimeStamp() != null ? sdf.format(new Date(run.getEndTimeStamp().getTime())) : "NA"%></td>
						<td><%=run.getElapsedTime() != null ? run.getElapsedTime() : "NA" %></td>


						<%
								 	String labelClass= labelClassMap.get(run.getStatus());
								 	if(labelClass == null) {
								 		labelClass = "label-default";
								 	}
									%>
						<td><span class='label <%=labelClass %>'><%=run.getStatus() %></span>
					</tr>
					<%
							}
						}
						else{
						%>
						<tr>
						<td colspan='7'>No Records Found</td>
					</tr>
						<% }
						%>
				</tbody>
			</table>
			<nav>
				<ul class="pager">
					<li class="previous"><a href="#"><span aria-hidden="true">&larr;</span>
							Previous</a></li>
					<li class="next"><a href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
				</ul>
			</nav>
		</div>
	</div>





<form action="" id='runFilterForm'>
	<div class="modal fade" id="filtermodal" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">


					<h4 class="modal-title title  "
						style='box-sizing: content-box; font-size: 12px;'>Filter your
						search</h4>
				</div>
				<div id='user-message'></div>
				<div class="modal-body  container_16">

					<fieldset>


						<input type='hidden' id='userId' name='userId'
							value='<%=tjnSession.getUser().getId()%>' /> <input
							type='hidden' id='projectId' name='pid'
							value='<%=project.getId() %>' />
						<div class='fieldSection grid_7'>
							<div class='clear'></div>
							<div class='grid_2'>
								<label for='txtRunId'>Run ID</label>
							</div>
							<div class='grid_4'>
								<input type='number' id='txtRunId' name='runId' value=''
									class='stdTextBox2' />
							</div>


							<div class='clear'></div>
							<div class='grid_2'>
								<label for='txtName'>Started By</label>
							</div>
							<div class='grid_4'>
								<select id='startedby' style="min-width: 200px;" name='runUser'
									class='stdTextBoxNew'>
									<option value=''>-- Select One --</option>
									<%
								int userNo=-1;
								for(User user:userList){
									userNo++;
									%>
									<option value=<%=user.getId() %>><%=user.getId() %></option>
									<%}
								%>

								</select>
							</div>
							 
							<div class='clear'></div>
							<div class='grid_2'>
								<label for='txtName'>Started On</label>
							</div>
							<div class='grid_4' style="margin-bottom: 2px;">
								<input type='text' id='datepicker' name='son'
									style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;'
									value='' readonly />
							</div>


							<div class='clear'></div>
							<div class='grid_2'>
								<label for='txtName'>Test Case</label>
							</div>
							<div class='grid_4'>
								<input type='text' id='txttcid' name='tcid' value=''
									class='stdTextBox' />
							</div>

                            <div class='clear'></div>
							<div class='grid_2'>
								<label for='txtName'>Project Id</label>
							</div>
							<div class='grid_4'>
								<% 	Map<String, Object> map2 = (Map<String, Object>)request.getSession().getAttribute("PRJ_RUN_MAP");%>
								<input type='text' id='txtprojectid' name='projectid'
									value='<%=project.getId() %>' disabled="disable"
									class='stdTextBox' />
							</div>


						</div>
			
						<div class='fieldSection grid_7'>
							<div class='clear'></div>
							<div class='grid_2'>
								<label for='txtName'>Application</label>
							</div>
							<div class='grid_4'>
								 
								<select id='txtapplication' style="min-width: 200px;" name='application' class='stdTextBox'>
									<option value=''>-- Select One --</option>
								<%
										ArrayList<Aut> auts = project.getAuts();
										if (auts != null && auts.size() > 0) {
											for (Aut aut : auts) {
									%>
									<option value='<%=aut.getId()%>'><%=aut.getName()%></option>
									<% }} %>
								</select>
							</div>

							<div class='clear'></div>
							<div class='grid_2'>
								<label for='txtName'>Result</label>
							</div>
							<div class='grid_4'>
								<select id='lstStatus' style="min-width: 200px;" name='status'
									class='stdTextBoxNew'>
									<option value=''>-- Select One --</option>
									<option value='Pass'>Pass</option>
									<option value='Fail'>Fail</option>
									<option value='Error'>Error</option>
									<option value='Executing'>Executing</option>
									<option value='Aborted'>Aborted</option>
								</select>
							</div>


							<div class='clear'></div>
							<div class='grid_2'>
								<label for='txtName'>Test Step</label>
							</div>
							<div class='grid_4'>
								<input type='text' id='txttstepid' name='tstepid' value=''
									class='stdTextBox' />
							</div>




							<div class='clear'></div>
							<div class='grid_2'>
								<label for='txtName'>Function</label>
							</div>
							<div class='grid_4'>
							 
								<select id='txtfunction' style="min-width: 200px;" name='function' class='stdTextBox'>
								<option value=''>-- Select One --</option>
								</select>
							</div>
                          
						</div>


					</fieldset>


				</div>
				<div class="modal-footer">
					<div class='toolbar'>
				 
						<input type='button' value='Search' id='btnSearch'
							class='imagebutton search ' />
						 
						<button type="button" class="imagebutton cancel" id="btnClose"
							data-dismiss="modal" onclick="this.form.reset();">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	</form>
</body>
</html>