<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  defect_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!-- Added file by sahana for TJN_24_06 Requirement -->
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.defect.DefectManagementInstance"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

		<title>Defect Maintenance</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/bordered.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		
		 <script type='text/javascript' src='js/pages/defect_list.js'></script>	
		 <script src="js/tables.js"></script>
		
		<style>
		#txturl{
				width:400px;
				word-break: break-all;
    			height:auto;
    			white-space: normal;
			}
		#txtadminid {
				width:200px;
				word-break: break-all;
    			height:auto;
    			white-space: normal;
			}
		</style>
		
</head>

<body>
     
     <%
     Cache<String, Boolean> csrfTokenCache = null;
     csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
     session.setAttribute("csrfTokenCache", csrfTokenCache);
     UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
     session.setAttribute ("csrftoken_session", csrftoken);
     
        TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
         if(tjnSession!=null){
     %>
         
<div class='main_content'>
		<div class='title'>
			<p>Defect Management Instance List</p>
		</div>
		<form name='def_list_form' action='DefectServlet' method='post'>
		<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
			<input type='button' value='New' id='btnNew' class='imagebutton new'/>
				
				<input type='button' value='Remove' id='btnDelete' class='imagebutton delete'/>				
			</div>
			<div id='user-message'></div>
			<div class='emptySmall'>&nbsp</div>
			
			
		
						<%
							String scrStatus = "";
							String message = "";
							ArrayList<DefectManagementInstance> defectList = null;
							Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
							if(map != null){
								scrStatus = (String)map.get("DEF_STATUS");
								message = (String)map.get("MESSAGE");
								defectList = (ArrayList<DefectManagementInstance>)map.get("DEF_LIST");
							}
						%>
				<input type=hidden value='<%=message%>' id='message' />
			<div class='form container_16' style='margin:0 auto;'>
				<div id='dataGrid'>
					<table id='userTable' class='display' cellspacing='0' width='100%'>
						<thead>
							<tr>
								<th class='nosort'><input type='checkbox' id='chk_all' class='tbl-select-all-rows' title='Select All'></th>
								<th>Name</th>
								<th>Tool</th>
								<th>Url</th>
								<th>Admin ID</th>
		
								<th>Status</th>
		
							</tr>
						</thead>
						<tbody>
						<%
								if(defectList.size() > 0){
									for(DefectManagementInstance df:defectList){
							%>
							<tr>
								<td class='tiny'><input type='checkbox' class= 'checkbox chkBox' id='chk_def' value='<%=Utilities.escapeXml(df.getName()) %>' data-def='<%=Utilities.escapeXml(df.getName()) %>' data-status='<%=Utilities.escapeXml(df.getStatus()) %>'/>
								<td><a href='DefectServlet?param=VIEW_DEF&paramval=<%=Utilities.escapeXml(df.getName())%>' id='<%=Utilities.escapeXml(df.getName())%>' class='edit_record'><%=Utilities.escapeXml(df.getName() )%></a></td>
								<td><%=df.getTool()%></td>
								<td><div id="txturl"><%=Utilities.escapeXml(df.getURL())%></div></td>
								<td><div id="txtadminid"><%=Utilities.escapeXml(df.getAdminId())%></div></td>
								<td><%=df.getStatus() %></td>
								
								
							</tr>
							<%		}
								}else{
									%>
									<%
								}
							%>
						
						</tbody>
					</table>
				</div>
				</div>
						
			
			</form>	
		</div>
	<%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>
		

</body>
</html>