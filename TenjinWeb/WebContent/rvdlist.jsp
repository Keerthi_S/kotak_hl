<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  rvdlist.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


 
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.bridge.pojo.run.ResultValidation"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
    <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
	<head>
		<title>Learner - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/rvdlist.js'></script>
	</head>
	<body>
		<%
		
		Cache<String, Boolean> csrfTokenCache = null;
        csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
        session.setAttribute("csrfTokenCache", csrfTokenCache);
        UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
        session.setAttribute ("csrftoken_session", csrftoken); 
        
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			Project project = null;
			project = tjnSession.getProject();
			
			boolean canMakeChanges = false;
			
			
			if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){
				canMakeChanges = true;
			}
			
			
		%>
		
		<div class='title'>
			<p>Result Validation Functions</p>
			<div id='prj-info'>
				<span style='font-weight:bold'>Domain: </span>
				<span><%=project.getDomain() %></span>
				<span>&nbsp&nbsp</span>
				<span style='font-weight:bold'>Project: </span>
				<span><%=project.getName() %></span>
			</div>
		</div>
		
		<form name='main_form' action='NaviflowServlet' method='POST'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
				<input type='button' value='New' id='btnNew' class='imagebutton validation'/>
			</div>
			<div id='user-message'></div>
			
			<div class='form container_16'>
				<fieldset>
					<legend>Application Information</legend>
					<div class='fieldSection grid_8'>
						<div class='clear'></div>
						<div class='grid_2'><label for='lstApplication'>Application</label></div>
						<div class='grid_5'>
							<select id='lstApplication' name='lstApplication' class='stdTextBox'></select>
						</div>
					</div>
				</fieldset>
				<fieldset id='rvdlist'>
					<legend>Available Result Validation Functions</legend>
					<div id='dataGrid'>
						<table id='rvd-list' class='display' cellspacing='0' width='100%'>
							<thead>
								<tr>
									<th class='tiny'><input type='checkbox' id='chk_all_rvd' title='Select All'/></th>
									<th class='tiny'>ID</th>
									<th>Name</th>
									<th>Application</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</fieldset>
				
			</div>
			
			<input type='hidden' id='moduleList' name='moduleList' value=''/>
			<input type='hidden' id='learnType' name='learnType' value='NORMAL'/>
		</form>
	</body>
</html>