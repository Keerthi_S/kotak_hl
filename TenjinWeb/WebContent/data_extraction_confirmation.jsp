<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  data_extraction_confirmation.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@page import="com.ycs.tenjin.client.RegisteredClient"%>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.ExtractionRecord"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Module"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY          DESCRIPTION
*/

-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Data Extraction - Confirmation</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel='stylesheet' href='css/bordered.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel='stylesheet' href='css/select2.min.css' />
<link rel="SHORTCUT ICON" HREF="images/yethi.png">
<link rel="Stylesheet" href="css/summarypagetable.css" />

<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type="text/javascript" src="js/formvalidator.js"></script>
<script type='text/javascript'
	src='js/pages/data_extraction_confirmation.js'></script>
<script type='text/javascript' src='js/select2.min.js'></script>
</head>
<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
	
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Map<String, Object> map = (Map<String, Object>)request.getSession().getAttribute("EXTRACTOR_SCREEN_MAP");
		String status = (String)map.get("STATUS");
		String message = (String)map.get("MESSAGE");
		if(message == null){
			message = "";
		}
		
		Aut aut = (Aut)map.get("AUT");
		List<Module> modules = (List<Module>) map.get("FUNCTIONS");
		Module module = modules.get(0);
		
		List<String> autLoginTypes = (List<String>) map.get("OWN_CREDS");
		List<RegisteredClient> clients = (List<RegisteredClient>) map.get("CLIENTS");
		
		%>

	<div class='title'>
		<p>Confirm Data Extraction</p>
	</div>

	<form action='ExtractorServlet' name='extraction-form' method='POST'>
		<div class='toolbar'>
			<input type='button' class='imagebutton back' id='btnBack' value='Back' /> 
			<input type='submit' class='imagebutton ok' id='btnExtract' value='Extract Data' />
		</div>

		<input type='hidden' id='screenStatus' value='<%=status %>' /> <input
			type='hidden' id='screenMessage' value='<%=message %>' />
		<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<div id='user-message'></div>

		<div class='form container_16'>
			<fieldset style='margin-top: 10px;'>
				<legend>Application Information</legend>


				<div class='fieldSection grid_7'>
					<div class='grid_2'>
						<label for='txtApplication'>Application</label>
					</div>
					<div class='grid_4'>
						<input type='text' id='txtApplication' name='txtApplication'
							value='<%=Utilities.escapeXml(aut.getName()) %>' class='stdTextBox' disabled />
					</div>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtFunction'>Function</label>
					</div>
					<div class='grid_4'>
						<input style='text-overflow: ellipsis;' type='text'
							id='txtFunction' name='txtFunction'
							value='<%=Utilities.escapeXml(module.getCode()) %> - <%=Utilities.escapeXml(module.getName()) %>'
							class='stdTextBox' disabled />
					</div>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='autLoginType'>AUT Login Type</label>
					</div>
					<div class='grid_4'>
						<select id='autLoginType' name='autLoginType' class='stdTextBox'>
							<%
								if(autLoginTypes != null && autLoginTypes.size() > 0){
									for(String a:autLoginTypes){
										%>
							<option value='<%=a %>'><%=a %></option>
							<%
									}
								}
								%>
						</select>
					</div>

				</div>
				<div class='fieldSection grid_7'>

					<div class='grid_2'>
						<label for='rClient'>Client</label>
					</div>
					<div class='grid_4'>
					<%	String ipAddress=request.getRemoteAddr();
                		String hostName=request.getRemoteHost();
						
                		boolean flag=false;
                		String clientName=null;
                		String host=null;
                		 if(clients != null && clients.size() > 0){
                			for(RegisteredClient client:clients){
                				 if(ipAddress.equalsIgnoreCase(client.getHostName())||hostName.equalsIgnoreCase(client.getHostName())){
                					flag=true;
                					clientName=client.getName();
                					host=client.getHostName();
                				} 
                			}
                		}
                		 
                	%>
						<select id='rClient' name='rClient' class='stdTextBox'
							mandatory='yes' title='Client'>
							<%if(flag){ %>
							<option value='<%=Utilities.escapeXml(clientName)%>'><%=Utilities.escapeXml(clientName) %></option>
							<%}else{ %>
							<option value='-1'>-- Select One --</option>
							<%}
							
								if(clients != null && clients.size() > 0){
									for(RegisteredClient client:clients){
										if(client.getName().equals(clientName)){
											
										}else{
							%>
							
										<option value='<%=Utilities.escapeXml(client.getName()) %>'><%=Utilities.escapeXml(client.getName()) %></option>
							<%
										}
									}
								}
								%>
						</select>
					</div>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='browser'>Browser</label>
					</div>
					<div class='grid_4'>
						<select id='browser' name='browser' class='stdTextBox'>
							<option value='APPDEFAULT'>Use Application Default</option>
							<option value='<%=BrowserType.CHROME %>'>Google Chrome</option>
							<option value='<%=BrowserType.IE %>'>Microsoft Internet
								Explorer</option>
							<option value='<%=BrowserType.FIREFOX %>'>Mozilla
								Firefox</option>
						</select>
					</div>
				</div>

			</fieldset>
			<fieldset>
				<legend>Data to be extracted</legend>
				<input type='hidden' id='selection' name='selection' value='' />
				<div id='dataGrid' style='margin-top: 20px;'>
					<table id='tbl-extraction-data' class='summtable'>
						<thead>
							<tr>
								<th>TDGID</th>
								<th>TDUID</th>
								<th>Query Fields</th>
							</tr>
						</thead>
						<tbody>
							<%
								String qFields = "";
								if(module.getExtractionRecords() != null && module.getExtractionRecords().size() > 0){
									for(ExtractionRecord record:module.getExtractionRecords()){
										qFields = "";
										if(record.getQueryFields() != null){
										int size=record.getQueryFields().keySet().size();
										int i=0;
											for(String key:record.getQueryFields().keySet()){
												i++;
												qFields = qFields + key + " [" + record.getQueryFields().get(key) + "] ";
												if(i!=size){
													qFields+=";";
												}
											}
										}
										
										if(Utilities.trim(qFields).equalsIgnoreCase("")){
											qFields = "Not Specified.";
										}
										String tdGid = record.getTdGid();
										if(Utilities.trim(tdGid).equalsIgnoreCase("")){
											tdGid = "Not Specified.";
										}
									%>
							<tr>
								<td><%=Utilities.escapeXml(tdGid )%></td>
								<td><%=Utilities.escapeXml(record.getTdUid()) %></td>
								<td><%=Utilities.escapeXml(qFields) %></td>

							</tr>
							<%
									}
								}
								%>
						</tbody>
						<input type='hidden' id='QueryField' value='<%=Utilities.escapeXml(qFields) %>' />
					</table>
				</div>
			</fieldset>
		</div>
	</form>
</body>
</html>