<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  maptmattributes.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!-- /*****************************************
* CHANGE HISTORY
* ==============
*

*/ -->
<%@page import="com.ycs.tenjin.testmanager.TestManagerInstance"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.project.TestCase"%>
<%@page import="com.ycs.tenjin.project.TestStep"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel="Stylesheet" href="css/bordered.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<link rel='stylesheet' href='css/select2.min.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/formvalidator.js'></script>
		<script type='text/javascript' src='js/pages/defect_master.js'></script>
		<script type='text/javascript' src='js/pages/maptmattributes.js'></script>
		<script type='text/javascript' src='js/select2.min.js'></script>
		
		<style>
			#selectedFiles
			{
				margin-left:0;
			}
			
			.att_block
			{
				padding:5px;
				border:1px solid #707a86;
				background-color:#b7c0ef;
				border-radius:5px;
				margin-left:0;
			}
			
			.att_block p
			{
				overflow:hidden;
				text-overflow:ellipsis;
			}
			
			#def_review_confirmation_dialog,#def_review_progress_dialog,#def_linkage_dialog
				{
					background-color:#fff;
					
				}
				
				.subframe_content
				{
					display:block;
					width:100%;
					margin:0 auto;
					min-height:120px;
					padding-bottom:20px;
				}
				.subframe_title
				{
					width:644px;
					height:20px;
					background:url('images/bg.gif');
					color:white;
					font-family:'Open Sans';
					padding-top:6px;
					padding-bottom:6px;
					padding-left:6px;
					font-weight:bold;
					margin-bottom:10px;
				}
				
				.subframe_actions
				{
					display:inline-block;
					width:100%;
					background-color:#ccc;
					padding-top:5px;
					padding-bottom:5px;
					font-size:1.2em;
					text-align:center;
				}
				
				.highlight-icon-holder
				{
					display:block;
					max-height:50px;
					margin-bottom:10px;
					text-align:center;
				}
				
				.highlight-icon-holder > img
				{
					height:50px;
				}
				
				.subframe_single_message
				{
					width:80%;
					text-align:center;
					font-weight:bold;
					display:block;
					margin:0 auto;
					font-size:1.3em;
				}
				
				#progressBar
				{
					width:475px;
					margin-top:20px;
					margin-left:auto;
					margin-right:auto;
				}
				
				p.subtitle
				{
					display:block;
					float:right;
					font-weight:bold;
					font-size:12px;
					line-height:22px;
					margin-right:10px;
					color:red;
				}
		</style>
	</head>
	<body>
	
	<%
	
	 TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
	User cUser = tjnSession.getUser();
	Project project = null;
	
	Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
	List<String> tcFields = (ArrayList<String>)map.get("tcFields");
	List<String> tsFields = (ArrayList<String>)map.get("tsFields");
	String message = (String)map.get("message");
	project = tjnSession.getProject();
	
	List<TestManagerInstance> tmInstances = null;
	%>
	
	<div class='title'>Map ETM Attributes</div>
	<div class='toolbar'>
		<input type='button' id='btnBack' value='Back'
			class='imagebutton back' /> 
			<%if(tcFields!=null&&tsFields!=null){ %>
			<input type='button' id='btnSave' value='Save' class='imagebutton save' />
			<%} %>
			<input type='hidden' value='<%=project.getId()%>' id='projectId'>
			<input type='hidden' value='<%=message%>' id='message'>
			<%if(project.getEtmInstance()!=null){ %>
			<input type='hidden' value='<%=project.getEtmInstance()%>' id='btnextTmInt'>
			<%} %>
	</div>
	<%if(tcFields!=null&&tsFields!=null){ %>
	<div id='user-message'></div>
	<%}else{ %>
	<div id='user-message'><%=message %></div>
	<%} %>
	<div class='form container_16'>
	<%if(tcFields!=null){ %>
	<fieldset class = 'container'>
	
		<legend>Test Case Attributes</legend>
		<div class='fieldSection grid_7'>
			<div class='clear'></div>
							<label for='tcIdField' rec_type='TC' id='tcId' >Test Case Id</label>
						
						
			<div class='clear'></div>
							<label for='tcTypeField' rec_type='TC' id='tcType' >Test Case Type</label>			

		</div>
		
		
		<div class='fieldSection grid_7'>
			<div class='clear'></div>
			<select class='stdTextBox tjn_field' id='tcIdField' rec_type='TC' tjnField='Test Case Id' mandatory>
			<option value='-1'>Select One</option>
			
			<% 
			for(String tcField: tcFields){ 
				tmInstances= project.getTmInstances();
				String[] values=tcField.split("\\+");
				String displayValue=values[0];
				String value=values[1];
			if(tmInstances!=null && tmInstances.size()>0){
				for(TestManagerInstance tmInstance : tmInstances){
					if(tmInstance.getTjnField().equalsIgnoreCase("Test Case Id")){
						if(tmInstance.getTmField().equalsIgnoreCase(value)){
						
							%>
						<option  value='<%=value %>' selected><%=displayValue %></option>
						<%	
						}else{
						%>
						<option  value='<%=value %>'><%=displayValue %></option>
						<%
						}
					}
				}
			}else{
			%>
			<option  value='<%=value %>'><%=displayValue %></option>
			<%}
			}%>
			</select>
			<div class='clear'></div>
			<select class='stdTextBox tjn_field' id='tcTypeField' rec_type='TC' tjnField='Test Case Type' mandatory>
			<option value='-1'>Select One</option>
			<%for(String tcField: tcFields){ 
				tmInstances= project.getTmInstances();
				String[] values=tcField.split("\\+");
				String displayValue=values[0];
				String value=values[1];
				if(tmInstances!=null && tmInstances.size()>0){
				for(TestManagerInstance tmInstance : tmInstances){
					if(tmInstance.getTjnField().equalsIgnoreCase("Test Case Type")){
						if(tmInstance.getTmField().equalsIgnoreCase(value)){%>
			<option value='<%=value %>' selected><%=displayValue %></option>
			<%	
						}else{
						%>
						<option  value='<%=value %>'><%=displayValue %></option>
						<%
						}
					}
				}
			}else{
			%>
			<option  value='<%=value %>'><%=displayValue %></option>
			<%} 
			}
			%>
			</select>
		</div>
	</fieldset>
	<%} %>
	<%if(tsFields!=null){ %>
	<fieldset class = 'container'>
	
		<legend>Test Step Attributes</legend>
		<div class='fieldSection grid_7'>
			<div class='clear'></div>
							<label for='tsAppIdField' rec_type='T_STEP' id='appId'>Application Id</label>
							
			<div class='clear'></div>
							<label for='tsFuncCodeField' rec_type='T_STEP' id='funcCode'>Function Code</label>
							
			<div class='clear'></div>
							<label for='tsOperationField' rec_type='T_STEP' id='operation'>Operation</label>
							
			<div class='clear'></div>
							<label for='tsAutLoginTypeField' rec_type='T_STEP' id='autLoginType'>AUT Login Type</label>
							
			<div class='clear'></div>
							<label for='tsTypeField' rec_type='T_STEP' id='stepType' >Step Type </label>
						

		</div>
		
		<div class='fieldSection grid_7'>
			<div class='clear'></div>
			<select class='stdTextBox tjn_field' id='tsAppIdField' rec_type='T_STEP' tjnField='Application Id'>
			<option value='-1'>Select One</option>
			<%
			for(String tsField: tsFields){ 
				tmInstances = project.getTmInstances();
				String[] values=tsField.split("\\+");
				String displayValue=values[0];
				String value=values[1];
				if(tmInstances!=null && tmInstances.size()>0){
				for(TestManagerInstance tmInstance : tmInstances){
					if(tmInstance.getTjnField().equalsIgnoreCase("Application Id")){
						if(tmInstance.getTmField().equalsIgnoreCase(value)){%>
			<option value='<%=value %>' selected><%=displayValue %></option>
			<%	
						}else{
						%>
						<option  value='<%=value %>'><%=displayValue %></option>
						<%
						}
					}
				}
			}else{
			%>
			<option  value='<%=value %>'><%=displayValue %></option>
			<%} 
			}%>
			</select>
			
			<div class='clear'></div>
			<select class='stdTextBox tjn_field' id='tsFuncCodeField' rec_type='T_STEP' tjnField='Function Code'>
			<option value='-1'>Select One</option>
			<%for(String tsField: tsFields){ 
				tmInstances = project.getTmInstances();
				String[] values=tsField.split("\\+");
				String displayValue=values[0];
				String value=values[1];
				if(tmInstances!=null && tmInstances.size()>0){
				for(TestManagerInstance tmInstance : tmInstances){
					if(tmInstance.getTjnField().equalsIgnoreCase("Function Code")){
						if(tmInstance.getTmField().equalsIgnoreCase(value)){%>
			<option value='<%=value %>' selected><%=displayValue %></option>
			<%	
						}else{
						%>
						<option  value='<%=value %>'><%=displayValue %></option>
						<%
						}
					}
				}
			}else{
			%>
			<option  value='<%=value %>'><%=displayValue %></option>
			<%} 
			}%>
			</select>
			
			<div class='clear'></div>
			<select class='stdTextBox tjn_field' id='tsOperationField' rec_type='T_STEP' tjnField='Operation'>
			<option value='-1'>Select One</option>
			<%if(tsFields!=null){
			for(String tsField: tsFields){ 
				tmInstances = project.getTmInstances();
				String[] values=tsField.split("\\+");
				String displayValue=values[0];
				String value=values[1];
				if(tmInstances!=null && tmInstances.size()>0){
				for(TestManagerInstance tmInstance : tmInstances){
					if(tmInstance.getTjnField().equalsIgnoreCase("Operation")){
						if(tmInstance.getTmField().equalsIgnoreCase(value)){%>
			<option value='<%=value %>' selected><%=displayValue %></option>
			<%	
						}else{
						%>
						<option  value='<%=value %>'><%=displayValue %></option>
						<%
						}
					}
				}
			}else{
			%>
			<option  value='<%=value %>'><%=displayValue %></option>
			<%} 
			}
			}%>
			</select>
			
			<div class='clear'></div>
			<select class='stdTextBox tjn_field' id='tsAutLoginTypeField' rec_type='T_STEP' tjnField='AUT Login Type'>
			<option value='-1'>Select One</option>
			<%for(String tsField: tsFields){ 
				tmInstances = project.getTmInstances();
				String[] values=tsField.split("\\+");
				String displayValue=values[0];
				String value=values[1];
				if(tmInstances!=null && tmInstances.size()>0){
				for(TestManagerInstance tmInstance : tmInstances){
					if(tmInstance.getTjnField().equalsIgnoreCase("AUT Login Type")){
						if(tmInstance.getTmField().equalsIgnoreCase(value)){%>
			<option value='<%=value %>' selected><%=displayValue %></option>
			<%	
						}else{
						%>
						<option  value='<%=value %>'><%=displayValue %></option>
						<%
						}
					}
				}
			}else{
			%>
			<option  value='<%=value %>'><%=displayValue %></option>
			<%} 
			}%>
			</select>
			
			<div class='clear'></div>
			<select class='stdTextBox tjn_field' id='tsTypeField' rec_type='T_STEP' tjnField='Step Type'>
			<option value='-1'>Select One</option>
			<%for(String tsField: tsFields){ 
				tmInstances = project.getTmInstances();
				String[] values=tsField.split("\\+");
				String displayValue=values[0];
				String value=values[1];
				if(tmInstances!=null && tmInstances.size()>0){
				for(TestManagerInstance tmInstance : tmInstances){
					if(tmInstance.getTjnField().equalsIgnoreCase("Step Type")){
						if(tmInstance.getTmField().equalsIgnoreCase(value)){%>
			<option value='<%=value %>' selected><%=displayValue %></option>
			<%	
						}else{
						%>
						<option  value='<%=value %>'><%=displayValue %></option>
						<%
						}
					}
				}
			}else{
			%>
			<option  value='<%=value %>'><%=displayValue %></option>
			<%} 
			}
			%>
			</select>
			
			
			
		</div>
	</fieldset>
	<%} %>
	</div>


</body>
</html>