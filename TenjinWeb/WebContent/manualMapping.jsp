<!--

Yethi Consulting Private Ltd. CONFIDENTIAL

Name of this file:  manualMapping.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.bridge.pojo.run.ResultValidation"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@page import="com.ycs.tenjin.project.TestStep"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.project.TestCase"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>  
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Module"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
	<head>
		<title>New Test Case - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="js/formvalidator.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<link rel="stylesheet" href="jstree/themes/default/style.min.css" />
		<link rel="Stylesheet" href="css/manualmap.css" />
		<script src='jstree/jstree.min.js'></script>
		<script type='text/javascript' src='js/pages/manualmap.js'></script>
		
		<script>
		  function isNumberKey(evt)
	      {
	         var charCode = (evt.which) ? evt.which : event.keyCode
	         if (charCode > 31 && (charCode < 48 || charCode > 57))
	            return false;

	         return true;
	      }
		
		</script>
	</head>
	<body>
	
	
		 <%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		
		<%
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			Project project = null;
			project = tjnSession.getProject();
			
			boolean canMakeChanges = false;
			
			
			if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){
				canMakeChanges = true;
			}
			
			Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
			String status = (String)map.get("STATUS");
			String message = (String)map.get("MESSAGE");
			TestStep step = (TestStep)map.get("TEST_STEP");
			ArrayList<Aut> auts = (ArrayList<Aut>)map.get("AUTS");
			ArrayList<ResultValidation> validations = (ArrayList<ResultValidation>)map.get("VALIDATIONS");
			ArrayList<String> autCredentials = (ArrayList<String>)map.get("USER_AUT_CREDS");
			ArrayList<Module> functions=(ArrayList<Module>)map.get("Functions");
			Aut currentAut = null;
		%>
		
		<div class='title'>
			<p>Test Step Details</p>
			<div id='prj-info'>
				<span style='font-weight:bold'>Domain: </span>
				<span><%=project.getDomain() %></span>
				<span>&nbsp&nbsp</span>
				<span style='font-weight:bold'>Project: </span>
				<span><%=project.getName() %></span>
				<input type='hidden' id='prj_name' value='<%=project.getName()%>'/>
				<input type='hidden' id='step_id' value='<%=step.getId()%>'/>
				
			</div>
		</div>
		
		<form name='main_form_manual' action='TestCaseServlet' method='POST'>
			<input type='hidden' value='<%=step.getTestCaseRecordId() %>' id='txtTcRecId' name='txtTcRecId'/>
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			
			<div class='toolbar'>
				<input type='button' value='Back' class='imagebutton back' id='btnBack'/>
				<input type='button' value='Save' class='imagebutton save' id='btnSave'/>
				<input type='button' value='Un-Map' class='imagebutton unmapsingle' id='btnUnMap'/>
					<input type='button' value='Un-Map All' class='imagebutton unmap' id='btnUnMapAll'/>	
			</div>
			
			<%
			if(status != null && status.equalsIgnoreCase("SUCCESS")){
				%>
				<div id='user-message'></div>
				<%
			}else if(status != null && !status.equalsIgnoreCase("SUCCESS")){
				%>
				<div id='user-message' class='msg-err' style='display:block;'>
					<div class="msg-icon">&nbsp;</div>
					<div class="msg-text"><%=message %></div>
				</div>
				
				<%
			}else{
				%>
				<div id='user-message'></div>
				
				<%
			}
			%>
			
			<%if(step!=null){
				%>
				
				
			
			<div class='form container_16'>
				<div class='fieldSection grid_15'>
					<input type='hidden' id='txtRecordId' name='txtRecordId' value='<%=step.getRecordId() %>'/>
					<div class='clear'></div>
					<div class='grid_2'><label for='txtStepId'>Step ID</label></div>
					<div class='grid_5'>
						<input type='text' id='txtStepId' name='txtStepId' class='stdTextBox'  mandatory='no' title='Step ID' value='<%=step.getId()%>' disabled='disabled'/>
					</div>
					
					<div class='grid_2'><label for='txtStepId' style='text-align:right;'>Test Case ID</label></div>
					<div class='grid_5'>
						<input type='text' id='txtTcId' name='txtTcId' class='stdTextBox'  mandatory='no' title='Test Case ID' value='<%=step.getTestCaseName()%>' disabled='disabled'/>
					</div>
					
				
				
						<div class='clear'></div>
						<div class='grid_2'><label for='lstApplication'>Application</label></div>
						<div class='grid_5'>
							<select id='lstApplication' name='lstApplication' class='stdTextBox' disabled='disabled'>
							<option value='<%=step.getAppId() %>'><%=step.getAppName() %></option>
							</select>
							<input type='hidden' id='selApp' value='<%=step.getAppId() %>'/>
						</div>
						
						<div class='grid_2'><label for='lstModules'>Function</label></div>
						<div class='grid_5'>
							
						 	<input type='hidden' id='selModule' value='<%=step.getModuleCode() %>'/>
							<select id='lstModules' name='lstModules' class='stdTextBox' disabled='disabled'>
						    <option value='<%=step.getModuleCode() %>'><%=step.getModuleCode() %></option>
							</select> 
						</div>
					
			</div>
			<%
			}
			
			%>
			
			</div>
			
				
			<div class='form container_16'>
				<div class='fieldSection grid_6' id="src_1" >
					<fieldset style='margin-top:10px'>
						<legend>Current Workbook</legend>
						<input class="search-input form-control stdTextBox" placeholder='Search Field'></input>
						<div id='dTree' style='height:450px;overflow:auto;'>
							<ul>
							   <li>Root node 1
							     <ul>
							       <li id="child_node_1" data-jstree='{"selected":true,"icon":"images/tree.png"}'><a href='#' >Child node 1</a></li>
							       <li>Child node 2</li>
							     </ul>
							   </li>
							   <li data-jstree='{"disabled":true}'>Root node 2</li>
							 </ul>
						</div>
						
					</fieldset>
				</div>
				<div class='fieldSection grid_1'   style='padding-top: 250px;' >
				<input type="button" id="addMap" class="add" style="width:20px;">
				</div>
				<div class='fieldSection grid_8' style='height:450px;overflow:auto;'>

					<fieldset style='margin-top:10px'>
						<legend>Added Manual Field Map</legend>
						<div style='margin-top:30px;'>
						<input type="hidden" id='sheetunmap' name='sheetunmap' class='stdTextBox' mandatory='no' title='Sheet Name' >
						<input class="stdTextBox" id='fieldunmap' name="fieldunmap" mandatory='no' title='Sheet Field' type="hidden"></input>	
						<input class="stdTextBox" id='mappedstatus' name="mappedstatus" mandatory='no' title='Sheet Field' type="hidden"></input>			
						<div class='grid_1'><label for='sheet'>Sheet</label></div>
						<div class='grid_4'>
							<input type="text" id='sheet' name='sheet' class='stdTextBox' mandatory='yes' title='Sheet Name'>			
						</div>
						<div class='clear'></div>
						<div class='grid_1'><label for='field'>Field</label></div>
						<div class='grid_4'>
							<input class="stdTextBox" id='field' name="field" mandatory='yes' title='Sheet Field'></input>
						</div>	
					
						<div class='clear'></div>
							<input type='hidden' id='srcSheet' value='' />
							<input type='hidden' id='srcField' value='' />
							<input type='hidden' id='sFieldNodeId' name="sFieldNodeId" value=''/>
							
							</div>

						<div id ='manualmapfield'>
						<table id="dataMapped" width='100%'  height='100%'  class='display'>
						<thead  >
							<tr>
								<th>Sheet Name</th>
								<th>Field Name</th>
								<th style="text-align:center;">Remove</th>
							</tr>
						</thead>
						<tbody  class="scrollcell">
						</tbody>
						</table>
						
						</div>
						
						<div class='clear'></div>
	<div class='grid_5' style='margin:0 auto;display:inline-block;'>
							
						</div>	</div>
		
		</fieldset>
		</div>
		</div>
		</form>
		
		
	</body>
</html>