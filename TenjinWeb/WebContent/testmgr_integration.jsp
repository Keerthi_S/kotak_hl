<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testmgr_integration.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION

*/

-->


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.ycs.tenjin.testmanager.TestManagerInstance"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
  <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Test Manager</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>

<script type='text/javascript' src='js/pages/testmgr_integration.js'></script>	

<script src="js/formvalidator.js"></script>
</head>
<body>
<%
Cache<String, Boolean> csrfTokenCache = null;
csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
session.setAttribute("csrfTokenCache", csrfTokenCache);
UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);

			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			Project project = null;
			project = tjnSession.getProject();
			
			boolean canMakeChanges = false;
			
			
			if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){
				canMakeChanges = true;
			}
			

			
		%>
	<%
							String scrStatus = "";
							String message = "";
							String etmprjtool ="";
							
							ArrayList<String> instanceList = null;
							
							Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
							if(map != null){
								scrStatus = (String)map.get("SCR_MAP");
								message = (String)map.get("MESSAGE");
								etmprjtool = (String)map.get("etmprjtool");
								instanceList = (ArrayList<String>)map.get("instances");
							}
						
						%>
<div class='main_content'>
<div class='title'>

		<p>External Test Manager Integration  </p>
	</div>
	<form name='external_testmgt_int' action='ProjectServlet' method='get'>
	<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<input type='hidden' id='etmprjtool' name='etmprjtool' value='<%=etmprjtool%>'/>
		<input type='hidden' id='etmprjprj' name='etmprjprj' value='<%=project.getEtmProject()%>'/>
		<input type='hidden' id='etmprjenable' name='etmprjenable'  value='<%=project.getEtmEnable()%>'/>
		<input type='hidden' id='etmprjinstance' name='etmprjinstance'  value='<%=project.getInstance()%>'/>
		<input type='hidden' id='etmtcfilter' name='etmtcfilter' value='<%=project.getTcFilter()%>' />
		<input type='hidden' id='etmtsfilter' name='etmtsfilter' value='<%=project.getTsFilter()%>' />
		<input type='hidden' id='etmtcvalue' name='etmtcvalue' value='<%=project.getTcFilterValue()%>' />
		<input type='hidden' id='etmtsvalue' name='etmtsvalue' value='<%=project.getTsFilterValue()%>' />
		<div class='toolbar'>
			<input type='button' value='Save' id='btnSave' class='imagebutton save' />
			<input type='button' value='Cancel' id='btnCancel' class='imagebutton cancel' />
			<input type='button' value='Map Attributes' id='mapAttributes' class='imagebutton mapAttributes'/>
			<img src='images/inprogress.gif' id='dmLoader' />
        </div>
			
			
		<div id='user-message'></div>
		
		<div class='emptySmall'>&nbsp</div>

			<div class='form container_16' style='margin-top:-10px;'>
				<fieldset>
				
					<legend>Integration Details</legend>

					<div class='fieldSection grid_13' >

 					<div class='clear'></div>
						
						<div class='clear'></div>
						<div class='grid_2'>
							<label for=txtTjnPrjName>Tenjin Project</label>
						</div>
						<div class='grid_4'>
						<input type='hidden' value='<%=project.getId()%>'  id='txttjnPrjId'/>
						<input type='hidden' value='<%=cUser.getId()%>'  id='txttjnUserId'/>
							<input type="text" class='stdTextBox mid'  disabled='disabled'  name="txtTjnPrjName" id="txtTjnPrjName" title='Name' value='<%=project.getName()%>'>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtInstTool'>Instance Name</label>
						</div>
						<div class='grid_4'>
					     <select id='txtInstTool' title='Instance Name' mandatory='yes' name='txtInstTool' class='stdTextBoxNew'>
					     <option value='-1'>-- Select One --</option>
					     <%for(String tm : instanceList){ 
					     if(project.getEtmInstance()!=null &&project.getEtmInstance().equalsIgnoreCase(tm)){
					     %>
					     
					     <option value=<%=tm %> selected><%=tm %></option>
					     <%}else{ %>
					     <option value=<%=tm %>><%=tm %></option>
					     <%}
					     }%>
					     </select>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='extPrjName'>Project Name</label>
						</div>
						<div class='grid_4'>
							<input type='text' id='extPrjName' name='txtUrl'
								class='stdTextBox mid'  mandatory='yes' title='Project Name' placeholder='Domain:Project' />
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtCheck'>Enabled</label>
						</div>
						<div class='grid_4'>
							<input type='checkbox' id='txtCheck' name='txtCheck' checked='checked'
								 title='Select CheckBox'/>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtTcFilter' id='TcFilterlabel'>Test Case Filter </label>
								
								
						</div>
						<div class='grid_7'>
						<%if(project.getTcFilter()!=null){%>
						     
							<input type='text' id='extTcFilter' name='extTcFilter' value='<%=project.getTcDisplayValue() %>'
									class='stdTextBox mid'  disabled  />
									
									<select id='txtTcFilter' title='Test Case Filter' name='txtTcFilter1' class='stdTextBoxNew'>
					     <option value='-1'>--Select One--</option>
					    </select>&nbsp;&nbsp;
					  <a href='#'  onclick="populateTcField();">Change</a>
						<%}else 						
						{ %>
					     <select id='txtTcFilter' title='Test Case Filter' name='txtTcFilter' class='stdTextBoxNew'>
					     <option value='-1'>--Select One--</option>
					    </select>  
					    <%} %>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtTcValue' id='TcValuelable'> Filter Value</label>
						</div>
						<div class='grid_4'>
							<input type='text' value='' id='txtTcValue' name='txtTcValue' class='stdTextBox mid'   title='Case Field Value'  />
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtTsFilter' id='TsFilterlable'>Test Step Filter </label>
							
						</div>
						<div class='grid_7'>
						<%
						
						if(project.getTsFilter()!=null){%>
							<input type='text' id='extTsFilter' name='extTsFilter' value='<%=project.getTsDisplayValue() %>'
									class='stdTextBox mid'  disabled  />
									
									<select id='txtTsFilter' title='Test Case Filter' name='txtTsFilter1' class='stdTextBoxNew'>
					     <option value='-1'>--Select One--</option>
					    </select>&nbsp;&nbsp;
							<a href='#'  onclick="populateTsField();">Change</a>
						<%}else{ %>
					     <select id='txtTsFilter' title='Test Step Filter'  name='txtTsFilter' class='stdTextBoxNew'>
					     <option value='-1'>--Select One--</option>
					    </select>
					    <%} %>
					   
					    
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtTsValue' id='TsValuelable'>  Filter Value</label>
						</div>
						<div class='grid_4'>
							<input type='text' value='' id='txtTsValue' name='txtTsValue' class='stdTextBox mid'   title='Step Field Value'  />
						</div>
		 </fieldset>
		</div>
			
</form>
</div>
</body>
</html>