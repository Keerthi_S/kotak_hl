<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  projectdefects.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
-->

<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.defect.Defect"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>

<!-- /*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 19-11-2020             Priyanka                TENJINCG-1231
  
*/ -->



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Defects</title>
		<title>Tenjin - Intelligent Enterprise Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/rprogress.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel="stylesheet" type="text/css" href="css/progressbarskins/default/progressbar.css">
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/projectdefects.js'></script>
		<script type="text/javascript" src="js/progressbar.js"></script>



<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src=' js/bootstrap.min.js'></script>


		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
			<script src="js/tables.js"></script>
		
		
		<style>
			#project_defects_filter
			{
				display:none;
			}
			#def_review_confirmation_dialog,#def_review_progress_dialog
			{
				background-color:#fff;
				
			}
			
			.subframe_content
			{
				display:block;
				width:100%;
				margin:0 auto;
				min-height:120px;
				padding-bottom:20px;
			}
			.subframe_title
			{
				width:644px;
				height:20px;
				background:url('images/bg.gif');
				color:white;
				font-family:'Open Sans';
				padding-top:6px;
				padding-bottom:6px;
				padding-left:6px;
				font-weight:bold;
				margin-bottom:10px;
			}
			
			.subframe_actions
			{
				display:inline-block;
				width:100%;
				background-color:#ccc;
				padding-top:5px;
				padding-bottom:5px;
				font-size:1.2em;
				text-align:center;
				position: relative;
				bottom: -24px;
			}
			
			.highlight-icon-holder
			{
				display:block;
				max-height:50px;
				margin-bottom:10px;
				text-align:center;
			}
			
			.highlight-icon-holder > img
			{
				height:50px;
			}
			
			.subframe_single_message
			{
				width:80%;
				text-align:center;
				font-weight:bold;
				display:block;
				margin:0 auto;
				font-size:1.3em;
			}
			
			#progressBar
			{
				width:475px;
				margin-top:20px;
				margin-left:auto;
				margin-right:auto;
			}
			
			p.subtitle
			{
				display:block;
				float:right;
				font-weight:bold;
				font-size:12px;
				line-height:22px;
				margin-right:10px;
				color:red;
			}
			.modal-footer{
				text-align:center
			}
		</style>
		 
</head>
<body>

	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>

	<%
		TenjinSession tjnSession = (TenjinSession) request.getSession()
				.getAttribute("TJN_SESSION");
		Map<String, Object> map = (Map<String, Object>) request
				.getSession().getAttribute("DEFECT_SCR_MAP");
		Project project = tjnSession.getProject();
		List<Defect> defects = (ArrayList<Defect>) map.get("defects");
		
		List<Aut> auts = (ArrayList<Aut>)map.get("auts");
		String status = (String) map.get("STATUS");
		
		String message = (String) map.get("message"); 
		String defectsCallback = (String)map.get("defectsCallback");
		String dateFormat = TenjinConfiguration.getProperty("DATE_FORMAT");
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		
	%>


	<form action='DefectServlet' method='POST' name='projectdefectlistform'>

		<input type='hidden' value='<%=project.getId()%>' id='prjId'>
		 	<input type='hidden' value='<%=Utilities.escapeXml(defectsCallback) %>' id='defectsCallback'>
		 	<!-- Added by Priyanka for TENJINCG-1231 starts -->
			<input type='hidden' name='edate' id='endDate' value='${edate }'/>
			<!-- Added by Priyanka for TENJINCG-1231 ends -->
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			
		<div class='title'>
			<p>Defects for this Project</p>
		 
		</div>

		<div class='toolbar'>
			<%if(defects!=null && defects.size()>0){ %>
			<button type="button" class="btn btn-info btn-lg imagebutton search " data-toggle="modal" data-target="#filtermodal"  id='btnfilter'  >Filter</button>
			<input type='button' id='btnSync' value='Synchronize' class='imagebutton refresh'/>
			<input type='button' id='btnDelete' value='Remove' class='imagebutton delete'/>
			<%if(message!=null && message!=""){ %>
				<input type='hidden' id='userMessage' value='<%=Utilities.escapeXml(message)%>'/>
				 <div id='user-message' style='display:block'></div> 
			<%} %>
			<%} %>
			<!-- commented by Priynaka for TCGST-48 and TCGST-47 starts -->
		<%-- 	<a href='DefectServlet?param=fetch_project_defects&pid=<%=project.getId()%>&callback=fetch' target='content_frame' id='allDefects' class=' '><input type='button' value='Reset' class='imagebutton refresh'></a>
 		 
			<img src='images/inprogress.gif' id='dmLoader' style='display:none;'/> --%>
			<!-- commented by Priynaka for TCGST-48 and TCGST-47 ends -->
			<div class="modal fade" id="filtermodal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          
    
          <h4 class="modal-title title  ">Filter your search</h4>
        </div>
               <div id='user-message'></div>
        <div class="modal-body  container_16" >
        
        <fieldset>
					 
						<div class='fieldSection grid_7'>
							<div class='clear'></div>
							<div class='grid_2'><label for='recordId'>Record ID</label></div>
							<div class='grid_4'>
								<input type='number' id='recordId' name='runId' value='' class='stdTextBox2'/>
							</div>
							
							<div class='clear'></div>
							<div class='grid_2'><label for='startedby'>Reporter</label></div>
							<div class='grid_4'>
								<input type='text' id='startedby' name='runUser' value='' class='stdTextBox'/>
							</div>
							<div class='clear'></div>
							<div class='grid_2'><label for='lstStatus'>Review Status</label></div>
							<div class='grid_4'>
								<select id='lstStatus' name='status' class='stdTextBoxNew'>
									<option value=''>-- Select One --</option>
									<option value='POST'>Posted</option>
									<option value='NOREVIEW'>Not Reviewed</option>
									<option value='ONHOLD'>On Hold</option>
									<option value='IGNORE'>Ignored</option>
								</select>
							</div>
 
							<div class='clear'></div>
							<div class='grid_2'><label for='lstPosted'>Posted</label></div>
							<div class='grid_4'>
								<select id='lstPosted' name='lstPosted' class='stdTextBoxNew'>
									<option value=''>-- Select One --</option>
									<option value='YES'>Yes</option>
									<option value='NO'>No</option>
								</select>
							</div>
					
						</div>
			
							 <div class='fieldSection grid_7'>
							<div class='clear'></div>
							<div class='grid_2'><label for='txtappliation'>Application</label></div>
							<div class='grid_4'>
					 	  <select id='txtappliation' name='application' class='stdTextBox'>
					 	  <option value=''>-- Select One --</option>
					 	  <%if(auts!=null && auts.size()>0){
					 	  for(Aut aut:auts){ %>
					 	  	<option value='<%=aut.getId()%>'><%=aut.getName()%></option>
					 	  	<%}
					 	  }else{%>
					 	  <input type='text' id='txtappliation' name='appliation' value='' class='stdTextBox'/>
					 	  <%} %>
					 	  </select>
							 
							</div>
							 <div class='clear'></div>
							<div class='grid_2'><label for='txtfunction'>Function</label></div>
							<div class='grid_4'>
								<input type='text' id='txtfunction' name='function' value='' class='stdTextBox'/>
							</div>
							<div class='clear'></div>
							<div class='grid_2'><label for='txttcid'>Test Case</label></div>
							<div class='grid_4'>
								<input type='text' id='txttcid' name='tcid' value='' class='stdTextBox'/>
							</div>
							
								<div class='clear'></div>
							<div class='grid_2'><label for='txttstepid'>Test Step</label></div>
							<div class='grid_4'>
								<input type='text' id='txttstepid' name='tstepid' value='' class='stdTextBox'/>
							</div>
							
									
							
							
						 	
							
							
							 	
						</div>
						
						
					
				</fieldset>
			
			
        </div>
        <div class="modal-footer">
     <div class='toolbar'>   
      <input type='button' value='Search' id='btnSearch' class='imagebutton search '/>
      <button type="button" id='btnCloseModal' class="imagebutton cancel" data-dismiss="modal" onclick="this.form.reset();">Close</button>
     </div>
        </div>
      </div>
    </div>
  </div>
			
		</div>
	
			
			
		
		<div class='fieldSection grid_15'>
		 
			<div id='dataGrid'>
			
			<table id='project_defects' class='display compact' cellspacing='0' width='100%'>
			<thead>
						<tr>
							<th class='nosort'><input type='checkbox' class='tbl-select-all-rows' id='all_defects' title='Select All'/></th>
							<th class="text-center">Defect ID</th>
 							<th class=" ">Defect Summary</th>
 							<!-- added by shruthi for TENJINCG-1245 starts -->
 						    <th>Instance</th>
 						   <!-- added by shruthi for TENJINCG-1245 ends -->
							<th>Severity</th>
							<th>Status</th>
							 
							<th>Application</th>
						 
							<th>Posted</th>
							<th>Reporter</th>
							 
						</tr>
					</thead>
					
					<tbody>
					
					
					<%if(defects!=null && defects.size()>0){
						for(Defect defect: defects){
						%>
						<tr>
						<td>
						 
						<input type='checkbox' class='chkBox' id='<%=defect.getRecordId() %>' data-id='<%=defect.getRecordId() %>'/>
					 
						<td class="text-center"><a class='defectDetails' href='DefectServlet?param=fetch_defect_details&did=<%=defect.getRecordId() %>&callback=projectdefectlist'><%=defect.getRecordId() %></a></td>
						 
						<td title='<%=defect.getSummary() %>' style="width: 43%"><%=defect.getSummary() %></td>
						<!-- added by shruthi for TENJINCG-1245 starts -->
						<td title='<%=defect.getDttInstanceName() %>' style="width: 43%"><%=defect.getDttInstanceName() %></td>
							<!-- added by shruthi for TENJINCG-1245 ends -->
			 
						<td title='<%=defect.getSeverity() %>'><%=defect.getSeverity()%></td>
						<td><%=defect.getStatus() %></td>
						<td title='<%=defect.getAppName() %>'><%=defect.getAppName() %></td>
						<%if( defect.getFunctionCode()==null || defect.getFunctionCode().equalsIgnoreCase("-1")){
							if(Utilities.trim(defect.getApiCode()).length() > 0) {
								defect.setFunctionCode("API (" +defect.getApiCode()+ ")");
							}else{
								defect.setFunctionCode("All");
							}
							
						} %>
						 
						
						<td><%=defect.getPosted() %></td>
						<td title='<%=defect.getCreatedBy() %>'><%=defect.getCreatedBy() %></td>
						 
						</tr>	
						<%} 
						}else{%>
					
						<%} %>
							
					</tbody>
					
			</table>
			
			</div>

		</div>


	</form>
	<div class='modalmask'></div>
		<div class='subframe' id='def_review_confirmation_dialog'style='left:20%'>
			<div class='subframe_title'><p>Please Confirm...</p></div>
			<div class='subframe_content'>
				<div class='highlight-icon-holder'>
					<img src='images/warning_confirm.png' alt='Please Confirm' />
				</div>
				<div class='subframe_single_message'>
					<p>Synchronizing multiple defects from external DTT will take some time. Do you still want to proceed?</p>
				</div>
			</div>
			<div class='subframe_actions'>
				<input type='button' id='btnCOk' value='Proceed anyway' class='imagebutton ok'/>
				<input type='button' id='btnCCancel' value='Do not Proceed' class='imagebutton cancel'/>
			</div>
		</div>
		<!-- Modified by Pushpa for Tenj210-85 starts -->
		<!-- <div class='subframe' id='def_review_progress_dialog'style='left:20%;height: 300px; overflow-y:auto;'>
 		-->
 			<div class='subframe' id='def_review_progress_dialog'style=' '>
 			<!-- Modified by Pushpa for Tenj210-85 starts -->
			<div class='subframe_title'><p>Processing...</p></div>
			<div class='highlight-icon-holder' id='processing_icon'>
					
				</div>
			<div class='subframe_content'>
				<div class='subframe_single_message' id='def_review_message'>
					<p></p>
				</div>
			</div>
			<div class='subframe_actions' id='def_review_pr_actions' style='display:none;'>
				<input type='button' id='btnPClose' value='Close' class='imagebutton cancel'/>
			</div>
		</div>

</body>
</html>