<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  runvalidate.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


 
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 
*/

-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Validating Run</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel="Stylesheet" href="css/bordered.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/formvalidator.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/runvalidate.js'></script>
	</head>
	<body>
		<%
		
		Cache<String, Boolean> csrfTokenCache = null;
		csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
		session.setAttribute("csrfTokenCache", csrfTokenCache);
		UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
		session.setAttribute ("csrftoken_session", csrftoken);
		
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			Project project = null;
			project = tjnSession.getProject();
			
			boolean canMakeChanges = false;
			String entity_id=request.getParameter("entity_id");
			String entity_name=request.getParameter("entity_name");
			String set_mode=request.getParameter("set_mode");
			String oauthVal=request.getParameter("oauthval");
			if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){
				canMakeChanges = true;
			}
		String callbackParam=request.getParameter("callbackParam");
		String callback=request.getParameter("callback");
		String parentRunId=request.getParameter("parentRunId");
		
		%>
		<input type='hidden' id='callback' value='<%=callback %>'>
		<input type='hidden' id='callbackparam' value='<%=callbackParam %>'>
		<input type='hidden' id='parentRunId' value='<%=parentRunId %>'>
		<input type='hidden' id='entity_id' value='<%=entity_id %>'>
		<input type='hidden' id='entity_name' value='<%=entity_name %>'>
			<input type='hidden' id='set_mode' value='<%=set_mode %>'>
			<input type='hidden' id='oauthVal' value='<%=oauthVal %>'>
		<div class='title'>
			<p id='intializing'>Your test is initializing</p>
			<p id='failedTest'>Your test is failed</p>
			<div id='prj-info'>
				<span style='font-weight:bold'>Domain: </span>
				<span><%=project.getDomain() %></span>
				<span>&nbsp&nbsp</span>
				<span style='font-weight:bold'>Project: </span>
				<span><%=project.getName() %></span>
			</div>
		</div>
		
		
		<form name='main_form' action='ExecutorServlet' method='POST'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
						 <input type='button' value='Back' id='btnBack'class='imagebutton back' />
				 
			</div>
			
			<div id='user-message'>
			</div>
			
			<div id="validator_content" style="display:block;width:90%;height:100px;padding-left:10px;padding-top:10px;">
				<div id='validator_title' style='display:block;font-weight:bold;font-size:1.2em;'>
					<p>Please wait while Tenjin initializes your test. This may take a few minutes.</p>
				</div>
				<div id='validator_body' style='background-color: #ddd;display: block;height: 240px;margin-top:20px;padding-top:40px;'>
					<table id='validator_table'>
						
						<tbody style='display:table;width:100%'>
							<tr>
								<td class='valdesc'>Validating Test Set</td>
								<td class='valimg' id='tset_val'>&nbsp</td>
							</tr>
							<tr>
								<td class='valdesc'>Loading test data files</td>
								<td class='valimg' id='test_data_load'>&nbsp</td>
							</tr>
							<tr>
								<td class='valdesc'>Validating test data</td>
								<td class='valimg' id='test_data_val'>&nbsp</td>
							</tr>
							<tr>
								<td class='valdesc'>Validating AUT credentials</td>
								<td class='valimg' id='aut_cred_val'>&nbsp</td>
							</tr>
							<%if(oauthVal != null && oauthVal.equalsIgnoreCase("Y")){ %>
							<tr>
								<td class='valdesc'>Validating OAuth 2.0 Details</td>
								<td class='valimg' id='oauth_val'>&nbsp</td>
							</tr>
							<%} %>
							 
							<tr>
								<td class='valdesc'>Validating Adapter License</td>
								<td class='valimg' id='adapter_license_val'>&nbsp</td>
							</tr>
							<tr>
								<td class='valdesc'>Initializing Test Run</td>
								<td class='valimg' id='create_run_val'>&nbsp</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div id='validator_result' style='background-color: #ddd;display: block;margin-top:10px;margin-bottom:40px;display:none'>
					<p style='font-weight:bold;font-size:1.2em;'>The test could not be started due to the following errors</p>
					<div id='validator_errors' style='display:block;padding:10px;'>
						
					</div>
				</div>
			</div>
		</form>
		
	</body>
</html>