<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  add_devices.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add devices</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/formvalidator.js'></script>
<script type='text/javascript' src='js/pages/add_devices.js'></script>
</head>
<body>
<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
	<%
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		if (tjnSession != null) {
	%>
	<div class='title'>
		<p>Register Devices</p>
	</div>
	<form name='new_device' action='DeviceServlet' method='get'>

		<div class='toolbar'>
		         <!-- added by shruthi for CSRF token starts -->
		         <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		         <!-- added by shruthi for CSRF token ends -->
		
				<input type='button' value='Register' id='btnRegister' class='imagebutton save' />
				<input type='button' value='Cancel' id='btnBack' class='imagebutton cancel' /> 
		</div>

		<div id='user-message'></div>
		<div class='form container_16'>
			<fieldset>
				<legend>Select Client</legend>

				<div class='fieldSection grid_15'>
					<div class='grid_2'>
						<label for='txtclientregistered'>Client</label>
					</div>
					<div class='grid_4'>
						<select id='txtclientregistered' name='txtclientregistered'
							class='stdTextBoxNew' title="Select Reg Client"></select>
					</div>

					<div class='grid_3'>
						<input type='button' id='btnScan' value='Scan'
							class='imagebutton ok' />&nbsp;&nbsp;&nbsp;&nbsp;
							<img src='images/inprogress.gif' id='inProgress'/>
					</div>
				</div>
			
			</fieldset>
		</div>

	</form>
	<div class='form container_16'>
		<fieldset>
			<legend>List of Devices</legend>
			<div id='dataGrid'>
				<table id='devices-table' class='display' cellspacing='0'
					width='100%'>
					<thead>
						<tr>
							<th class='tiny'><input type='checkbox' id='chk_all' class='deviceCheck'
								title='Select All'></th>
							<th>Device Id</th>
							<th>Device Name</th>
							<th>Device Type</th>
							<th>Platform</th>
							<th>Platform Version</th>
							<th>Registered Status</th>
						</tr>
					</thead>
					<tbody id='devicesList'>

					</tbody>
				</table>
			</div>
		</fieldset>
	</div>

	<%
		} else {
			response.sendRedirect("noSession.jsp");
		}
	%>
</body>
</html>