<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  admin.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India


-->

<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--
/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              	DESCRIPTION
*/
-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tenjin - Intelligent Enterprise Testing Engine V2.0</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel="stylesheet" href="jstree/themes/default/style.min.css" />
<link rel='stylesheet' href='css/accordion.css' />
<link rel="SHORTCUT ICON" HREF="images/yethi.png">
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script src="jstree/jstree.min.js"></script>
<script type='text/javascript' src='js/pages/admin.js'></script>
<script src="js/formvalidator.js"></script>
<style>
.active {
	background-color: rgb(212, 212, 216);
	border: 1px solid #ccc;
	box-shadow: 0 0 5px #ccc;
}
.intergration{
    font-weight: bold;
    padding: 3px;
    background-color: gray;
    color: white;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
}
</style>
</head>
<body>
	<%
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Map<String, String> map = (Map<String,String>)request.getSession().getAttribute("SCR_MAP");
		User user = tjnSession.getUser();
		String param=(String)request.getAttribute("user_View");
		String license_notification = "";
		try{
		Map<String, String> maplice = (Map<String,String>)request.getSession().getAttribute("LICENSE_NOTIFICATION");
			license_notification=(String)maplice.get("MESSAGE");
		}
		catch (NullPointerException e ){
			license_notification = "";
		}
		%>
	<div class='wrapper'>
		<div class='header'>
			<div class='header-left'>
				<img src='images/tenjin_logo.png'
					alt='Tenjin - Intelligent Testing Engine' style='width: 125px;' />
			</div>
			<div class='header-middle'></div>
			<div class='header-actions'>
					<a class='headerLink' id='logout' name='logout' href='#' title='Logout'><img
					src='images/Logout.png' alt='Logout' /></a>
			</div>
			<input type="hidden" id="uesr_view" value=<%=param%> />
			<input type="hidden" id="currentUserId" value=<%=tjnSession.getUser().getId() %> />
			<div class='header-right'>
			<a  href='#' class='switchProject headerLink' id='switchProject' title= 'Click to View/Select project'><img src="images/switch_project.png">&nbsp Go to Project</a>
				<a id='logged-in-user' class='headerLink' name='logged-in-user' target='content_frame'	href='TenjinUserServlet?t=view&key=<%=tjnSession.getUser().getId() %>' title='Click to View/Edit your profile'><%=tjnSession.getUser().getFullName() %></a>
				<%
					if(tjnSession.getUser().getLastLogin() != null && !tjnSession.getUser().getLastLogin().equalsIgnoreCase("")){
					%>
				<div class='note'>
					Last Login:
					<%=tjnSession.getUser().getLastLogin() %>
				</div>
				<%} %>
			</div>
		</div>
		<div class='main'>
			<div id='user-message'></div>
			<div class='main_menu'>
				<div id='accordion'>
					<h3 id='domains_trigger'>Domains & Projects</h3>
					<div>
						<div id='domains_tree'></div>
					</div>
					<h3>Users</h3>
					<div>
						<%if(user.getRoles().equalsIgnoreCase("Site Administrator")){ %>
						<div class='accordion-child-item-standalone'>
								<a href='TenjinUserServlet' target='content_frame'>Tenjin User Maintenance</a>
						</div>
						<div class='accordion-child-item-standalone'>
								<a href='ClientServlet' target='content_frame'>Clients</a>
						</div>
						<div class='accordion-child-item-standalone'>
							<a href='DeviceServlet?param=FETCH_ALL_DEVICES' target='content_frame'>Devices</a>
						</div>
						
						<div class='accordion-child-item-standalone'>
								<a href='TenjinUserServlet?t=ActiveUser' target='content_frame'>Active Users</a>
						</div>
						<%}else{ %>
						<div class='accordion-child-item-standalone'>
						<a href='TenjinUserServlet?t=view&key=<%=user.getId()%>' target='content_frame'>Your Profile</a>
						</div>
						<%} %>
					</div>
					<h3>Applications</h3>
					<div>
						<%if(user.getRoles().equalsIgnoreCase("Site Administrator")){ %>
						<div class='accordion-child-item-standalone'>
								<a href='ApplicationServlet' target='content_frame'>Applications Under Test</a>
						</div>
						<div class='accordion-child-item-standalone'>
								<a href='FunctionServlet' target='content_frame'>Functions</a>
						</div>
						<div class='accordion-child-item-standalone'>
								<a href='APIServlet?param=show_api_list' target='content_frame'>APIs</a>
						</div>
						<!-- Added by Ashiki for TENJINCG-1275 starts -->
						<div class='accordion-child-item-standalone'>
								<a href='MessageServlet?param=list' target='content_frame'>Messages</a>
						</div>
						<!-- Added by Ashiki for TENJINCG-1275 ends -->
						<div class='accordion-child-item-standalone'>
									<a href='GroupServlet' target='content_frame'>Groups</a>
						</div>
						<%} %>
						
						<div class='accordion-child-item-standalone'>
								<a href='ApplicationUserServlet?' target='content_frame'>Application User Maintenance</a>
						</div>
						
						<div class='accordion-child-item-standalone'>
								<a href='ExtractorServlet?param=new' target='content_frame'>Data Extraction</a>
						</div>
						<div class='accordion-child-item-standalone'>
							<a href='PortingServlet?param=new' target='content_frame'>Porting</a>
						</div>
					</div>
					<h3>Integrations</h3>
							<div>
						<div style='padding-right: 10px;'>
							<h2 class='intergration'>Defect Management</h2>
							<div>
								<%if(user.getRoles().equalsIgnoreCase("Site Administrator")){ %>
								<div class='accordion-child-item-standalone'>
									<a href='DefectServlet?param=FETCH_ALL' target='content_frame'>Defect Management Instances</a>
								</div>
								<div class='accordion-child-item-standalone'>
									<a href='DTTSubsetServlet?param=FETCH_ALL_SUBSET' target='content_frame'>DTT Subset</a>
								</div>
								<%} %>
								<div class='accordion-child-item-standalone'>
									<a href='DefectServlet?param=fetch_def_user_mapping_data&userId=<%=user.getId()%>' target='content_frame'>User Credentials</a>
								</div>
							</div>
						</div>
						<div style='padding-right: 10px;'>
							<h2 class='intergration'>Test Management</h2>
							<div>
								<%if(user.getRoles().equalsIgnoreCase("Site Administrator")){ %>
								<div class='accordion-child-item-standalone'>
									<a href='TestManagerServlet?param=FETCH_ALL' target='content_frame'>External TM Instances</a>
								</div>
								<%} %>
								<div class='accordion-child-item-standalone'>
									<a href='TestCaseServlet?param=FETCH_TM_USER_CREDENTIALS&userId=<%=user.getId()%>' target='content_frame'>User Credentials</a>
								</div>
							</div>
						</div>
					</div>
					<h3>Scheduler</h3>
					<div>
						<div class='accordion-child-item-standalone'>
								<a href='schedule_admin_new.jsp' target='content_frame'>Schedule Task</a>
						</div>
						
						<div class='accordion-child-item-standalone'>
								<a href='SchedulerServlet?param=FETCH_ALL_ADMIN_SCH&paramval=All' target='content_frame'>View Task Status</a>
						</div>
					</div>
					<%if(user.getRoles().equalsIgnoreCase("Site Administrator")){ %>
                    <h3>Settings</h3>
                   <div>
						<div class='accordion-child-item-standalone'>
							<a href='EmailServlet?t=configuration' target='content_frame'>E-mail Notifications</a>
						</div>
						<div class='accordion-child-item-standalone'>
							 <a href='LicenseServlet?param=view&param1=view1' target='content_frame'>License Details</a>
						</div>
				  </div>
					<%} %>
					<h3>Help</h3>
					<div>
						<div class='accordion-child-item-standalone'>
								<a href='AutServlet?param=fetch_prerequisites' target='content_frame'>PREREQUISITES</a>
						</div>
						<div class='accordion-child-item-standalone'>
							<a href='AutServlet?param=FETCH_FAQ' target='content_frame'>FAQ's</a>
						</div>
					</div>
				</div>
			</div>
			<div class='content'>
				<iframe class='ifr_Main' name='content_frame' id='content_frame'
					src=''> </iframe>
			</div>
		</div>
		<div class='footer'>
			<div class='footer-left'>
			&nbsp;
			</div>
			<div class='footer-middle'>&nbsp; <b><font color="red" style="font-size:10px"> <%=license_notification%></font></b></div>
			<div class='footer-right'>&copy Copyright 2014 - <%=new java.text.SimpleDateFormat("yyyy").format(new java.util.Date()) %> <a href='http://www.yethi.in' target='_blank' rel="noopener">Yethi Consulting Pvt. Ltd.</a> All Rights Reserved
			</div>
		</div>
	</div>
</body>
</html>