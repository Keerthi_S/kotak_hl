<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  apidetails.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->




<!DOCTYPE html>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="java.util.List"%>
<%@page
	import="com.sun.xml.internal.bind.v2.schemagen.xmlschema.Appinfo"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Api"%>
<html>

<head>
<meta charset="ISO-8859-1">
<title>APIs Maintenance</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type="text/javascript" src="js/formvalidator.js"></script>
<script src="js/pages/apidetails.js"></script>
<script>
		$(document).ready(function(){

			mandatoryFields('api_details_new_form');
			<%
				TenjinSession tjnSession = (TenjinSession) request.getSession()
						.getAttribute("TJN_SESSION");
				User cUser = tjnSession.getUser();
				Api api=null;
				String status="";
				String message="";
				int appId=0;
				String code="";
				String name="";
				String id="";
				String type="";
				String url="";
				String appName="";
				ArrayList<Aut> autList = null;
				List<String> adapters = null;
				Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("API_MAP");
				if(map!=null){
					 status = (String) map.get("STATUS");
					 message = (String) map.get("MESSAGE");
					 api=(Api)map.get("API");
					autList = (ArrayList<Aut>)map.get("AUTS");
					adapters = (List<String>) map.get("ADAPTERS");
					
			  }
				
			%>
			
			if ('<%=message%>' != 'null' && '<%=message%>' != '') {
				console.log("msg---->"+'<%=message%>');
				showMessage('<%=message%>','<%=status%>');
			}
		});
		
		$(document).on("click", "#btnEdit", function(){
			var apiname=$("#txtApiName").val();
			var appid = $("#lstAppId").val();
			window.location="APIServlet?param=fetch_api&paramval="+apiname+"&appid="+appid;	
		});
		
		function populateAuts(){
			
			var jsonObj = fetchAllAuts();
			
			var status = jsonObj.status;
			 clearMessages();
			if(status == 'SUCCESS'){
				var html ="<option value='-1'>-- Select One --</option>";
				var jArray = jsonObj.auts;
				
				for(i=0;i<jArray.length;i++){
					var aut = jArray[i];
					html = html + "<option value='" + aut.id + "' selected> " + aut.name + "</option>";
				}
				
				$('#lstAppId').html(html);
				
			}
		}
		  
		$(document).on('change', '#existingGroup', function() {
			if($(this).val() === 'ALL' || $(this).val() === '-1' ) {
				$('#newGroup').prop('disabled',false);
			}else{
				$('#newGroup').prop('disabled',true);
				$('#txtApiGroup').val($(this).val());
			}
		});	
		
		$(document).on('change', '#newGroup', function() {
			if($(this).val() === '') {
				$('#existingGroup').prop('disabled',false);
			}else{
				$('#existingGroup').prop('disabled',true);
				$('#txtApiGroup').val($(this).val());
			}
		});
	
</script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<div class='title'>
	<p>New API</p>
</div>
<form name='api_details_new_form' id='api_details_new_form'
	action='APIServlet' method='POST'>
	<input type='hidden' id=requesttype name='requesttype' value='New_api' />
	<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
	<div class='toolbar'>
			 <input type='submit' class='imagebutton save'
			value='Save' id='btnSave' /> 
			<input type='button' class='imagebutton cancel' value='Cancel'
			id='btnBack' />
			<input type='button' value='Edit'
			class='imagebutton edit' id='btnEdit' style="display: none;" />
	</div>

	<div id='user-message'></div>

	<div class='form container_16'>
	<fieldset>
		<legend>Application Information</legend>
		<div class='fieldSection grid_16'>
			<div class='clear'></div>
			<div class='grid_3'>
				<label for='lstAppId'>Application</label>
			</div>
			<div class='grid_12'>
				<select id='lstAppId' name='lstAppId' type='select'
					class='stdTextBoxNew' mandatory='yes' title="Select Application Id">
					<% if(autList != null) {
						for(Aut aut:autList) {
							%>
							<option value='<%=aut.getId() %>'><%=aut.getName() %></option>
							<%
						}
					}
					%>
				</select>
			</div>
			</div>
			<div class='clear'></div>
			<div class='fieldSection grid_16'>
			<div class='grid_3'>
				<label for='existingGroup'>Group</label>
			</div>
			<div class='grid_4'>
				<select class='stdTextBoxNew' id='existingGroup' name='txtApiGroup'
					value='' title='API Group' >
				</select>
				
				
			</div>
		
			<div class='grid_2'>
				<label for='newGroup'>New Group</label>
			</div>
			<div class='grid_4'>
				<input type='text' id='newGroup' name='txtApiGroup'  class='stdTextBox' placeholder='Specify a new Group Name'/>
				
			</div>
			
		</div>
	</fieldset>
	<fieldset>
		<legend>Basic Information</legend>
		<div class='fieldSection grid_16'>
			<div class='clear'></div>
			<div class='grid_3'>
				<label for='txtApiCode'>API Code</label>
			</div>
			<div class='grid_12'>
			     <!-- Modified by Priyanka for TENJINCG-1233 starts -->
				<input type='text' class='stdTextBox' name='txtApiCode'
					value='<%=code %>' mandatory='yes' id='txtApiCode' title='API Code'
					maxlength="50" placeholder ="MaxLength 50" />
			    <!-- Modified by Priyanka for TENJINCG-1233 starts -->
			</div>
			<div class='clear'></div>
			<div class='grid_3'>
				<label for='txtApiName'>API Name</label>
			</div>
			<div class='grid_12'>
			   <!-- Modified by Priyanka for TENJINCG-1233 starts -->
				<input type='text' class='stdTextBox' name='txtApiName'
					mandatory='yes' id='txtApiName' value='<%=name %>' title='API Name'
					maxlength="100" placeholder="MaxLenght 100"/>
			   <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					
			</div>
			<div class='clear'></div>
			<div class='grid_3'>
				<label for='txtApiType'>API Type</label>
			</div>
			<div class='grid_12'>
				<select class='stdTextBoxNew' id='txtApiType' name='txtApiType'
					value='<%=type %>' title='API Type' mandatory='yes'>
					<option value='-1'>-- Select One --</option>
					<%
					if(adapters != null) {
						for(String adapter:adapters) {
							%>
							<option value='<%=adapter %>'><%=adapter %></option>
							<%
						}
					}
					%>
				</select>
			</div>
			<div class='clear'></div>
			
			<div class='grid_3'>
				<label for='encryptionType'>ENCRYPTION TYPE</label>
			</div>
			<div class='grid_12'>
				<select class='stdTextBoxNew' id='encryptionType' name='encryptionType'
					 title='ENCRYPTION TYPE' mandatory='yes'>
					<option value='-1'>-- Select One --</option>
					<option value='none'>None</option>
					<option value='medium'>Medium</option>
					<option value='high'>High</option>
					
				</select>
			</div>
			<div class='clear'></div>
			
			<div class='grid_3'>
				<label for='encryptionKey'>ENCRYPTION KEY</label>
			</div>
			<div class='grid_12'>
				<input type='text' class='stdTextBox' name='encryptionKey'
					mandatory='yes' id='encryptionKey' value='<%=url %>' title='encryption Key'
					maxlength="200" />
			</div>
			<div class='clear'></div>
			
			
			
			<div class='grid_3'>
				<label for='txtApiUrl'>API URL</label>
			</div>
			<div class='grid_12'>
				<input type='text' class='stdTextBox' name='txtApiUrl'
					mandatory='yes' id='txtApiUrl' value='<%=url %>' title='API Url'
					maxlength="200" />
			</div>
		</div>
		</fieldset>
	</div>
</form>
</body>

</html>