<!--
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  porting_selection.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
  
*/

-->

<%@page import="com.ycs.tenjin.port.PortingSession"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="java.util.List"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Data Porting</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel='stylesheet' href='css/select2.min.css'/>
		<link rel="SHORTCUT ICON" HREF="images/yethi.png">
		<link rel="Stylesheet" href="css/summarypagetable.css" />
		
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/formvalidator.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<!-- <script type='text/javascript' src='js/pages/porting_selection.js'></script> -->
		<script type='text/javascript' src='js/select2.min.js'></script>
		
		<script>
			$(document).ready(function(){
				$('#btnBack').click(function(){	
					window.location.href='PortingServlet?param=new';
				});
				$('#btnGo').click(function(){
					var selection = $('input[name=portChoice]:checked', '#portForm').val();
					if(selection == 'E' || selection == 'N'){
						return true;
					}else{
						alert('Please choose a valid action before you proceed.');
						return false;
					}
				});
			});
		</script>
	</head>
	<body>
	
		<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>	
		
		<%
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		PortingSession pSession = (PortingSession)request.getSession().getAttribute("PORTING_SESSION");
		%>
		
		
		
		<div class='title'>
			<p>Data Porting - Choose Action</p>
		</div>
		
		<form name='port-form' action='PortingServlet' method='POST' id='portForm'>
			<input type='hidden' id='txnType' name='TXNTYPE' value='portchoice'/>
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			
			<div class='toolbar'>
				<input type='button' id='btnBack' value='Back' class='imagebutton back'/> 
				<input type='submit' id='btnGo' value='Go' class='imagebutton ok'/> 
			</div>
			
			<div id='user-message'></div>
			
			<div class='form container_16'>
			<div class='fieldSection grid_8'>
				  <fieldset>
					<legend>Source Information</legend>
					<div class='grid_2'>
						<label for='sourceApplication'>Application</label>
					</div>
					
					<div class='grid_2'>
						<input readonly type='text' class='stdTextBox'
							id='sourceApplication' name='sourceApplication' value ='<%=pSession.getSourceAppName()%>' disabled="disabled" />
					</div>
					<br>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='sourceFunction'>Function Code</label>
					</div>
					<div class='grid_2'>
						<input readonly type='text' class='stdTextBox'  value ='<%=pSession.getSourceFunction()%>'
							id='sourceFunction' name='sourceFunction' disabled="disabled" />
					</div>


				</fieldset>
				</div>
				<div class='fieldSection grid_8'>
				<fieldset>
				<legend>Target Information</legend>
				<div class='grid_2'>
						<label for='targetApplication'>Application</label>
					</div>
					
					<div class='grid_2'>
						<input readonly type='text' class='stdTextBox'
							id='targetApplication' name='targetApplication'  value ='<%=pSession.getTargetAppName()%>' disabled="disabled" />
					</div>
					<br>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='targetFunction'>Function Code</label>
					</div>
					<div class='grid_2'>
						<input readonly type='text' class='stdTextBox'
							id='targetFunction' name='targetFunction'  value ='<%=pSession.getTargetFunc()%>' disabled="disabled" />
					</div>
				</fieldset>
				</div>
				<fieldset>
					<legend>Choose an action</legend>
					<br>
					<p>
					<%
					if(pSession.doesPortMapExist()){
						%>
						A Mapping between the selected Application Functions already exists. What would you like to do?
						<%
					}else{
						%>
						No Mapping exists for the selected Application functions. 
						<%
					}
					%>
					</p>
					<br>
					<table id='ttd-options-layout'>
					<%
					if(pSession.doesPortMapExist()){
					%>
					<tr>
						<td class='selector'><input type='radio' name='portChoice' id='portChoice1' value='E'/></td>
						<td class='ttd-option-block'>
							<div class='ttd-option-title'>Use the existing map</div>
							<div class='ttd-option-desc'>
								<p>
								Use the already existing map to port data between the functions you have selected. Select this option to save time. You can still edit the map in the following screen.
								</p>
							</div>
						</td>
					</tr>
					<%} %>
					<tr>
						<td class='selector'><input type='radio' name='portChoice' id='portChoice2' value='N'/></td>
						<td class='ttd-option-block'>
							<div class='ttd-option-title'>Create a new Map</div>
							<div class='ttd-option-desc'>
								<p>
								Use this option if you want to overwrite the existing map and let Tenjin map the source and target fields afresh. You will still be able to edit the generated map in the following screen.
								</p>
							</div>
						</td>
					</tr>
				</table>
					
				</fieldset>
				
				
			</div>
			
		</form>
		
		
	</body>
</html>