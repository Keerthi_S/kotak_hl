<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  report_trend.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--
/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
  
*/
-->
<head>
<title>Reports - Tenjin Intelligent Testing Engine</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel='stylesheet' href='css/bordered.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel='stylesheet' href='css/jquery-ui.css' />
<link rel="SHORTCUT ICON" HREF="images/yethi.png">
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/pages/autfunclist_table.js'></script>
<script type='text/javascript' src='js/pages/report_trend.js'></script>
<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script type='text/javascript' src="js/jquery-1.12.4.js"></script>
<script type='text/javascript' src="js/jquery-ui.js"></script>
<script type='text/javascript' src='js/pages/report_table.js'></script>
<script type="text/javascript" src="js/jquery.jqplot.min.js"></script>
<script type="text/javascript"
	src="js/dist/plugins/jqplot.barRenderer.js"></script>
<script type="text/javascript"
	src="js/dist/plugins/jqplot.pieRenderer.js"></script>
<script type="text/javascript"
	src="js/dist/plugins/jqplot.categoryAxisRenderer.js"></script>
<script type="text/javascript"
	src="js/dist/plugins/jqplot.pointLabels.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
<script type="text/javascript"
	src="js/dist/plugins/jqplot.highlighter.js"></script>
</head>
<body>
	<%
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		String param = request.getParameter("param");
		Map<String, Object> reportMap = (Map<String, Object>) request
				.getSession().getAttribute("REPORTS_MAP");
		User cUser = tjnSession.getUser();
		Project project = null;
		project = tjnSession.getProject();
		ArrayList<Aut> auts = null;
		String status = "";
		String message = "";
		boolean canMakeChanges = false;

		if ((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase(
				"Site Administrator"))
				|| project.hasAdminPrivileges(cUser.getId())) {
			canMakeChanges = true;
		}

		 if (reportMap != null) {
			status = (String) reportMap.get("STATUS");
			if (status.equalsIgnoreCase("SUCCESS")) {
				auts = (ArrayList<Aut>) reportMap.get("auts");	
			} else {
				message = (String) reportMap.get("MESSAGE");
			}
		}
	%>
	<div class='title'>
	 
		<p>Trend Reports</p>
 
	</div>
	<%
		String showError = "noerror";
		if (status != null && status.equalsIgnoreCase("error")) {
			showError = message;
		}
	%>
	<input type="hidden" id="message" value="<%=showError%>" name="message">
	<div class='toolbar'>
		<input type='button' value='Generate Report' id='trend_report'
			class='imagebutton download' />
	 
		<input type='button' value='Reset' id='btnReset'
			class='imagebutton reset' />
 
	</div>
	<div id='user-message'></div>


	<div class='clear'></div>
	<div class='form container_16'>
		<fieldset>
			<legend>Search Criteria</legend>
			<div class='grid_2'>
				<label for='lstApplication' id='applicationlabel'>Application</label>
			</div>
			<div class='grid_5'>
				<select id='lstApplication' name='lstApplication' class='stdTextBox'>
					<option value='-1'>-- Select One --</option>
					<option value='0'>-- ALL --</option>
					<%
							if (auts != null) {
								for (Aut aut1 : auts) {
						%>
					<option value="<%=aut1.getId()%>"><%=aut1.getName()%></option>
					<%
								}
							}
						%>
				</select> &nbsp&nbsp
			</div>

			<div class='grid_2'>
		 
				&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
				<label for='fromDate' id='fromDateLabel'>Date</label>
			</div>
			<div class='grid_5'>
				<%
								String todaydate = "";

								Calendar calendar1 = Calendar.getInstance();
								SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");
								todaydate = dateFormat.format(calendar1.getTime());
							%>
			 
					<input type="text" id='fromDate' name='fromDate' value='<%= todaydate%>'
					style='border: 1px solid #ccc; border-radius: 3px; padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 177px;'> 
					&nbsp&nbsp 
			</div>
		</fieldset>
	</div>

	<div class='clear'></div>
	<div class='form container_16'>
		<fieldset>
			<legend>Graph</legend>
			<div id="chart1" style="width: 1000px; height: 400px;"></div>
		</fieldset>
	</div>
</body>
</html>