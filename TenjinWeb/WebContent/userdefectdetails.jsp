<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  userdefectdetails.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!DOCTYPE html>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
     <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
   
   
*/

-->
<head>
<meta charset="ISO-8859-1">
<title>DTT Credential Details</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type="text/javascript" src="js/formvalidator.js"></script>
<script src="js/pages/userdefectdetails.js"></script>


<%
Cache<String, Boolean> csrfTokenCache = null;
csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
session.setAttribute("csrfTokenCache", csrfTokenCache);
UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
	TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
	User cUser = tjnSession.getUser();

	Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
	List<String> instances = (ArrayList<String>) map.get("instances");
%>

</head>
<body>
	<div class='title'>
	 
		<p>New Defect Management User</p>
		 
	</div>
	<form name='defect_user_mapping_form' action='AutServlet' method='POST'>
		<input type='hidden' id='viewType' value='EDIT' />
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<div class='toolbar'>
			 
			<input type='button' class='imagebutton save' value='Save'
				id='btnSave' /> <input type='button' class='imagebutton cancel'
				value='Cancel' id='btnBack' /> <input type='button' value='Edit'
				class='imagebutton edit' id='btnEdit' style="display: none;" />
		</div>

		<div id='user-message'></div>

		<div class='form container_16'>
			<div class='fieldSection grid_16'>
				<div class='clear'></div>
				<div class='grid_3'>
					<label for='txtUserId'>User ID</label>
				</div>
				<div class='grid_12'>
					<input type='text' class='stdTextBox' value=<%=Utilities.escapeXml(cUser.getId())%>
						name='txtUserId' id='txtUserId' title='AUT UserID'
						disabled='disabled' />
				</div>

				<div class='clear'></div>
				<div class='grid_3'>

					<label for='lstInstance'>Instance</label>
				</div>


				<div class='grid_12'>

					<select id='lstInstance' name='lstInstance' type='select'
						class='stdTextBox' mandatory='yes' title="Instance" mandatory>
						<option value='-1'>Select One</option>
						<%
							for (String instance : instances) {
						%>
						<option value='<%=Utilities.escapeXml(instance)%>'><%=Utilities.escapeXml(instance)%></option>
						<%
							}
						%>

					</select>
				</div>

				<div class='clear'></div>
				<div class='grid_3'>
					<label for='DTTUserName'>Login Name</label>
				</div>

				<div class='grid_12'>
                   <!-- Modified by Priyanka for TENJINCG-1233 starts -->
					<input type='text' class='stdTextBox' id='DTTUserName'
						mandatory='yes' name='txtAppUserName' maxlength="50"
						title='Login Name' mandatory  placeholder ='MaxLength 50'/>
				   <!-- Modified by Priyanka for TENJINCG-1233 ends -->

				</div>
				  <!--  	modified by shruthi for TENJINCG-1264 starts -->
				<div class='clear'></div>
				<div class="passwordBlock" >
				<div class='grid_3'>
					<label for='DTTPassword'>Login Password</label>
				</div>

				<div class='grid_12'>
                   <!-- Modified by Priyanka for TENJINCG-1233 starts -->
					<input type='password' id='DTTPassword' name='txtAppPassword'
						mandatory='yes' title='Login Password' autocomplete="new-password"
						class='stdTextBox' maxlength="50" placeholder='MaxLength 50'/>
				   <!-- Modified by Priyanka for TENJINCG-1233 ends -->

				</div>
				</div>
				<div class='clear'></div>
				
				<div class="apikeyBlock" >
				<div class='grid_3'>
					<label for='DTTapikey'>Login API key</label>
				</div>

				<div class='grid_12'>
					<input type='password' id='DTTapikey' name='txtAppPassword'
						mandatory='yes' title='Login Password' autocomplete="new-password"
						class='stdTextBox' maxlength="50" placeholder='MaxLength 50'/>

				</div>
                </div>
          <!--  	modified by shruthi for TENJINCG-1264 ends -->

			</div>
	</form>
</body>

</html>