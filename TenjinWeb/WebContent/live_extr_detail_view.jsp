<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  live_extr_detail_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India



/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->


<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.BridgeProcess"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.ExtractionRecord"%>
<%@page import="com.ycs.tenjin.run.ExtractorProgressView"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.bridge.pojo.TaskManifest"%>
<%@page import="com.ycs.tenjin.run.TestRun"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Module"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Learner - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel="stylesheet" type="text/css" href="css/progressbarskins/default/progressbar.css">
		<link rel='stylesheet' href='css/jquery.dataTables.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/live_extr_detail_view.js'></script>
		<script type='text/javascript' src='js/pages/extr_output.js'></script>
		<script type="text/javascript" src="js/progressbar.js"></script>
		<script src="js/tables.js"></script>
	</head>
	<body>
		<%
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		User cUser = tjnSession.getUser();
		ExtractorProgressView view = (ExtractorProgressView) request.getSession().getAttribute("EXTR_PROGRESS_VIEW");
		String status = view.getScreenStatus();
		String sMessage = view.getMessage();
		if(Utilities.trim(status).equalsIgnoreCase("success")){
			sMessage="noerror";
		}
		TestRun run =  null;

		Module currentFunction = (Module) request.getSession().getAttribute("EXTR_FUNC");
		if(view != null){
			run = view.getRun();
		}
		String progress = "0";
		boolean learningComplete = false;
		ExtractionRecord currentRecord = null;
		progress=view.getProgressPercentage();
		
		if(Double.parseDouble(progress) == 100){
			learningComplete = true;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss");
		%>
		
		<div class='title'>
			<p>Extraction Progress</p>
		</div>
		
		<div class='toolbar'>
			<div style="display:inline;" ><input type='button' id='btnBack' value='Back' class='imagebutton back'/>
			<input type='button' id='download' value='Download Output' class='imagebutton download'/>
			<img src='images/inprogress.gif' id='loader' alt='Loading. Please wait...' style='display:none;'/></div>
			<%if(!learningComplete){ %>
				<div style='font-weight:bold;font-size:0.7em;line-height:25px;display:inline;'>This page will automatically refresh every <select disabled id='refTimeout'>
				<option value='5000'>5 Seconds</option>
				</select></div> 
				
				<input type='hidden' id='autoRefreshFlag' value='yes'/>
			<%}else{ %>
			<input type='hidden' id='autoRefreshFlag' value='no'/>
			<%} %>
		</div>
		
		
		<div id='user-message'></div>
		
		<input type='hidden' id='screenStatus' value='<%=status %>'/>
		<input type='hidden' id='message' value='<%=sMessage %>'/>
		
		
		
		<div class='form container_16'> 
			<fieldset id='run-info'>
				<legend>Extraction Run Information</legend>
				<div class='fieldSection grid_7'>
					<div class='grid_2'><label for='runId'>Run ID</label></div>
					<div class='grid_4'><input type='text'  disabled id='runId' name='runId' class='stdTextBox' value='<%=run.getId() %>' /></div>
					<div class='clear'></div>
					<div class='grid_2'><label for='app'>Application</label></div>
					<div class='grid_4'><input type='text' id='app' name='app' disabled value='<%=run.getAppId() %>' class='stdTextBox'/></div>
					<div class='clear'></div>
					<div class='grid_2'><label for='function'>Function</label></div>
					<div class='grid_4'><input type='text' id='function' name='function' disabled value='<%=currentFunction.getCode() %>' class='stdTextBox'/></div>
				</div>
				
				<div class='fieldSection grid_7'>
					<div class='grid_2'><label for='start'>Start Time</label></div>
					<%
										String runStart = "";
										if(run.getStartTimeStamp() != null){
											runStart = sdf.format(new Date(run.getStartTimeStamp().getTime()));
										}else{
											runStart = " - ";
										}
										%>
					<div class='grid_4'><input type='text' id='start' name='start' disabled value='<%=runStart %>' class='stdTextBox'/></div>
					<div class='clear'></div>
					<div class='grid_2'><label for='start'>End Time</label></div>
					<%
										String runEnd = "";
										if(run.getEndTimeStamp() != null){
											runEnd = sdf.format(new Date(run.getEndTimeStamp().getTime()));
										}else{
											runEnd = " - ";
										}
										%>
					<div class='grid_4'><input type='text' id='end' name='end' disabled value='<%=runEnd %>' class='stdTextBox'/></div>
					<div class='clear'></div>
					<div class='grid_2'><label for='start'>Elapsed Time</label></div>
					<div class='grid_4'><input type='text' id='start' name='start' disabled value='<%=run.getElapsedTime() %>' class='stdTextBox'/></div>
				</div>
			</fieldset>
			<%if(learningComplete){
				
			}else{%>
			<fieldset style='margin-top:10px;'>
				<legend>Overall Extraction Progress</legend>
				<div class='clear'></div>
				<div class='grid_16' style='margin-top:15px;'>
					<div class='lrnr_progress_bar grid_8' style='margin:0 auto;'>
						<input type='hidden' id='progpcnt' value='<%=progress %>'/>
						<div id="progressBar" class="default" style='margin:0 auto;'><div></div></div>
					</div>
					<div class='clear'></div>
				</div>
			</fieldset>
			<%} %>
			<fieldset>
				<legend>Extracted Records</legend>
				<div id='dataGrid' style='margin-top:20px;'>
					<table class='display tjn-default-data-table' id='extr_records_table'>
						<thead>
							<tr>
								<th>Record No.</th>
								<th>TDGID</th>
								<th>TDUID</th>
								<th>Query Fields</th>
								<th>Result</th>
								<th>Message</th>
							</tr>
						</thead>
						<tbody>
							<%
							if(currentFunction.getLastSuccessfulExtractionStatus().getRecords() != null && currentFunction.getLastSuccessfulExtractionStatus().getRecords()
							.size() > 0){
								int seq = 0;
								for(ExtractionRecord record:currentFunction.getLastSuccessfulExtractionStatus().getRecords()){
									String qFields = "";
									int size=record.getQueryFields().keySet().size();
									int i=0;
									if(record.getQueryFields() != null){
										for(String key:record.getQueryFields().keySet()){
											i++;
											qFields = qFields + key + " [" + record.getQueryFields().get(key) + "] ";
											if(i!=size){
												qFields+=";";
											}
										}
									}
									seq++;
									String imgUrl = "";
									String message = "";
									String caption = "";
									String result = Utilities.trim(record.getResult());
									if(result.equalsIgnoreCase("X")){
										imgUrl = "images/ajax-loader.gif";
										caption = "In Progress, Extracting";
									}else if(result.equalsIgnoreCase("N")){
										imgUrl = "images/queued_20c20.png";
										caption = "Queued, Not Started";
									}else if(result.equalsIgnoreCase("S")){
										imgUrl = "images/success_20c20.png";
										message = "Extraction Complete";
										caption = "Completed, Success";
									}else if(result.equalsIgnoreCase("F") || result.equalsIgnoreCase("E")){
										imgUrl = "images/failure_20c20.png";
										caption = "Failed, Error";
										message = record.getMessage();
									}else if(result.equalsIgnoreCase("aborted")){
										imgUrl = "images/failure_20c20.png";
										caption = "Aborted";
										message = record.getMessage();
									}
									%>
									<input type='hidden' value='<%=caption %>' id='btnField'/>
									<tr>
										<td><%=seq %></td>
										<td><%=record.getTdGid() %></td>
										<td record='<%=seq%>'><%=record.getTdUid() %></td>
										<td><%=qFields %></td>
										<td><img src='<%=imgUrl %>' title='<%=caption %>' /></td>
										<td><%=message %></td>
									</tr>
									<%
								}
							}
							%>
						</tbody>
					</table>
				</div>
			</fieldset>
			
		</div>
		<div class='modalmask'></div>
		<div class='subframe' style='left:20%'>
			<iframe frameborder="2" scrolling="no"  marginwidth="5" marginheight="5" style='height:400px;width:600px;' seamless="seamless" id='ttd-options-frame' src=''></iframe>
		</div>
	</body>
</html>