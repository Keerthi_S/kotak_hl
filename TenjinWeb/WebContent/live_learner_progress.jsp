<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  live_learner_progress.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India



/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
  08-04-2020			Lokanath G              TENJINCG-1193
-->

<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.device.RegisteredDevice"%> 
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.bridge.pojo.TaskManifest"%>
<%@page import="com.ycs.tenjin.run.TestRun"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Module"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Learner - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel="stylesheet" type="text/css" href="css/progressbarskins/default/progressbar.css">
		<link rel='stylesheet' href='css/jquery.dataTables.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/live_learner_progress.js'></script>
		<script type="text/javascript" src="js/progressbar.js"></script>
		<script src="js/tables.js"></script>
		<style type="text/css">
		#message{
		width:250px; 
		word-break: break-all;
		height:auto;
		white-space: normal;
		}
		</style>
	</head>
	<body>
	
		<%
	    Cache<String, Boolean> csrfTokenCache = null;
	    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(1200, TimeUnit.SECONDS).build();
	    session.setAttribute("csrfTokenCache", csrfTokenCache);
	    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
	    session.setAttribute ("csrftoken_session", csrftoken);
	    %>
	
		<%
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		User cUser = tjnSession.getUser();
		TestRun run =  (TestRun) request.getSession().getAttribute("LRNR_RUN");
		Module currentFunction = (Module) request.getSession().getAttribute("LRNR_CURRENT_FUNC");
		
		String progress = (String) request.getSession().getAttribute("LRNR_PROGRESS");
		boolean learningComplete = false;
		if(Double.parseDouble(progress) == 100){
			learningComplete = true;
		}
		TaskManifest manifest = (TaskManifest) request.getSession().getAttribute("LRNR_GATEWAY");
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss");
		String callback = (String) request.getSession().getAttribute("callback");
		RegisteredDevice device = (RegisteredDevice) request.getSession().getAttribute("DEVICE");
		%>
		
		<div class='title'>
			<p>Learning Progress</p>
		</div>
		
		<div class='toolbar'>
		<input type='button' id='btnBack' value='Back' class='imagebutton back' />
		<%if(manifest!=null){ %>
		<input type='button' id='btnRelearn' value='Relearn' class='imagebutton learn' />
		<%} %>
		<%if(callback!=null && callback.equalsIgnoreCase("schedule")) {%>
			<input type='hidden' id='aut' value='<%=0 %>'>
		<%} else{%>
			<input type='hidden' id='aut' value='<%=manifest.getAut().getId() %>'>
			<%} %>
			<div id='refreshbar' style='font-weight:bold;font-size:0.7em;float:left;line-height:25px;'>This page will automatically refresh every <select disabled id='refTimeout'>
				<option value='5000'>5 Seconds</option>
			</select></div>
			
			<input type='button' id='btnRefresh' value='Refresh Now' class='imagebutton refresh'/>
			<input type='button' id='toggleRefresh' value='Stop Automatic Refresh' class='imagebutton cancel' ac='stop'/>
			<!--  Added by Lokanath for TENJINCG-1193 starts -->
               <input type='button' value='Abort' id='btnAbort' class='imagebutton abort'/>
                <!--  Added by Lokanath for TENJINCG-1193 ends -->
			<%
				if (run.getDeviceFarmFlag()!=null && run.getDeviceFarmFlag().equalsIgnoreCase("Y")){
			%>
			<input type='button' id='btnLiveView' value='Live View' class='imagebutton live'/>
			<%} %>
		</div>
		
		
		<div id='user-message'></div>
		
		<%
		if(run == null){
			%>
			<input type='hidden' id='user-msg' value='Could not refresh status due to an unexpected error. Please refresh the page'/>
			<%
		}else{
			%>
			<input type='hidden' id='user-msg' value='noerror'/>
			<%
		}
		%>
		<%if(learningComplete){ %>
			<input type='hidden' id='autoRefreshFlag' value='no'/>
		<%} else{%>
			<input type='hidden' id='autoRefreshFlag' value='yes'/>
		<%} %>
		
		<input type='hidden' id='runId' value='<%=run.getId() %>'/>
		
		<div class='form container_16'> 
			<%if(manifest != null){ %>
			<fieldset id='run-info'>
				<legend>Learning Run Information</legend>
				<div class='fieldSection grid_7'>
					<div class='grid_2'><label for='app'>Application</label></div>
					<div class='grid_4'><input type='text' id='app' name='app' disabled value='<%=Utilities.escapeXml(manifest.getAut().getName()) %>' class='stdTextBox'/></div>
					<div class='clear'></div>
					<div class='grid_2'><label for='browser'>Browser</label></div>
					<div class='grid_4'><input type='text' id='browser' name='browser' disabled value='<%=manifest.getBrowser() %>' class='stdTextBox'/></div>
				</div>
				
				<div class='fieldSection grid_7'>
					<div class='grid_2'><label for='start'>Start Time</label></div>
					<%
										String runStart = "";
										if(run.getStartTimeStamp() != null){
											runStart = sdf.format(new Date(run.getStartTimeStamp().getTime()));
										}else{
											runStart = " - ";
										}
										%>
					<div class='grid_4'><input type='text' id='start' name='start' disabled value='<%=runStart %>' class='stdTextBox'/></div>
					<div class='clear'></div>
					<div class='grid_2'><label for='start'>Elapsed Time</label></div>
					<div class='grid_4'><input type='text' id='start' name='start' disabled value='<%=run.getElapsedTime() %>' class='stdTextBox'/></div>
				</div>
			</fieldset>
			<%} %>
			<fieldset style='margin-top:10px;'>
				<legend>Overall Learning Progress</legend>
				<div class='clear'></div>
				<div class='grid_16' style='margin-top:15px;'>
					<div class='lrnr_progress_bar grid_8' style='margin:0 auto;'>
						<input type='hidden' id='progpcnt' value='<%=progress %>'/>
						<div id="progressBar" class="default" style='margin:0 auto;'><div></div></div>
					</div>
					<div class='clear'></div>
				</div>
			</fieldset>
			
			<%if(!learningComplete) {%>
			<fieldset>
				<legend>Currently Learning</legend>
				<div class='fieldSection grid_15'  id='currentModuleInfo'>
					<table class='display tjn-default-data-table' cellspacing='0' width='100%' id='c_modules_table'>
						<thead>
								<tr>
									<th class='tiny'>Code</th>
									<th>Function Name</th>
									<th>Start Time</th>
									<th>Status</th>
								</tr>
						</thead>
						<tbody>
							<%
							if(currentFunction != null){
								%>
								<tr>
									<td class='tiny'><%=Utilities.escapeXml(currentFunction.getCode())%></td>
									<td><%=Utilities.escapeXml(currentFunction.getName())%></td>
									<td>
										<%
										String sTime = "";
										if(currentFunction.getLastSuccessfulLearningResult().getStartTimestamp() != null){
											sTime = sdf.format(new Date(currentFunction.getLastSuccessfulLearningResult().getStartTimestamp().getTime()));
										}else{
											sTime = " - ";
										}
										%>
										<%=sTime %>
									</td>
									<td class='tiny'><img src='images/ajax-loader.gif'/></td>
								</tr>
								<%
							}else{
								%>
								<tr>
									<td colspan='4'>Loading. Please Wait...</td>
								</tr>
								<%
							}
							%>
						</tbody>
					</table>
				</div>
			</fieldset>
			<%} %>
			<%
			List<Module> modules = run.getFunctions();
			if((modules != null && modules.size() > 1) || (modules.size()==1 && modules.get(0).getLastSuccessfulLearningResult() != null && 
					!modules.get(0).getLastSuccessfulLearningResult().getLearnStatus().equalsIgnoreCase("learning"))){
				%>
				<fieldset>
				<legend>Functions for this Run</legend>
				<div class='fieldSection grid_15'  id='modulesInfo'>
					<table class='display tjn-default-data-table' cellspacing='0' width='100%' id='modules_table'>
						<thead>
							<tr>
								<th class='tiny'>#</th>
								<th class='tiny'>Run Id</th>
								<th class='tiny'>Code</th>
								<th>Function Name</th>
								<th>Status</th>
								<th>Start Time</th>
								<th>End Time</th>
								<th>Elapsed Time</th>
								<th>Template</th>
							</tr>
						</thead>
						<tbody>
						<%
							
											int i=0;
											if(modules != null && modules.size() > 0){
											for(Module module:modules){
									i++;
									
									String imgUrl = "";
									String message = "";
									String a = "<a class='template-gen' href='#' app='autName ' mod='module.getCode()' txnMode='G'>Download</a>";
									if(module.getLastSuccessfulLearningResult().getLearnStatus().equalsIgnoreCase("learning")){
										imgUrl = "images/ajax-loader.gif";
									}else if(module.getLastSuccessfulLearningResult().getLearnStatus().equalsIgnoreCase("queued")){
										imgUrl = "images/queued_20c20.png";
									}else if(module.getLastSuccessfulLearningResult().getLearnStatus().equalsIgnoreCase("complete")){
										imgUrl = "images/success_20c20.png";
										if(manifest != null && manifest.getAut() != null){
											/* Changed by leelaprasad  for TENJINCG-935 starts */
											message = "<a class='template-gen' href='#' app='" + manifest.getAut().getId() + "' mod='" + module.getCode() + "' txnMode='G'>Download</a>";
											/* message = "<a class='template-gen' href='#' app='" + manifest.getAut().getId() + "' mod='" + module.getCode() + "' txnMode='G'>Template</a>"; */
											 /* Changed by leelaprasad  for TENJINCG-935 ends */
										
										}else{
											message = "Download from Functions Page";
										}
									%>
									<input type='hidden' id='lrnStatus' value='complete'/>
									<%
									}else if(module.getLastSuccessfulLearningResult().getLearnStatus().equalsIgnoreCase("error")){
										imgUrl = "images/failure_20c20.png";
										message = module.getLastSuccessfulLearningResult().getMessage();
									}else if(module.getLastSuccessfulLearningResult().getLearnStatus().equalsIgnoreCase("aborted")){
										imgUrl = "images/failure_20c20.png";
										message = module.getLastSuccessfulLearningResult().getMessage();
									}
						%>
						<tr>
							<td class='tiny'><%=i%></td>
							<td class='tiny'><%=run.getId()%></td>
							<td class='tiny'><%=Utilities.escapeXml(module.getCode())%></td>
							<td><%=Utilities.escapeXml(module.getName())%></td>
							<td class='tiny'><img src='<%=imgUrl %>'/></td>
							<td>
								<%
								String sTime = "";
								if(module.getLastSuccessfulLearningResult().getStartTimestamp() != null){
									sTime = sdf.format(new Date(module.getLastSuccessfulLearningResult().getStartTimestamp().getTime()));
								}else{
									sTime = " - ";
								}
								%>
								<%=sTime %>
							</td>
							
							<td>
								<%
								String eTime = "";
								if(module.getLastSuccessfulLearningResult().getEndTimestamp() != null){
									eTime = sdf.format(new Date(module.getLastSuccessfulLearningResult().getEndTimestamp().getTime()));
								}else{
									eTime = " - ";
								}
								%>
								<%=eTime %>
							</td>
							<td><%
								if(module.getLastSuccessfulLearningResult().getElapsedTime() != null){
									%>
									<%=module.getLastSuccessfulLearningResult().getElapsedTime() %>
									<%
								}else{
									%>
									
									<%
								}
								%></td>
							<td >
								<div id="message">
									<%=message %>
								</div>
							</td>
						</tr>
						<%
							}
							}else{
								%>
								<tr>
									<td colspan='6'>Loading Functions. Please wait...</td>
								</tr>
								<%
							}
						%>
					</tbody>
					</table>
					<div class='emptysmall'></div>
				</div>
			</fieldset>
				<%
			}
			%>
			
		</div>
		<div class='modalmask'></div>
		<div class='subframe' style='left:20%'>
		    <iframe frameborder="2" scrolling="no"  marginwidth="5" marginheight="5" style='height:450px;width:600px;' seamless="seamless" id='ttd-options-frame' src=''></iframe>
		</div>
		<%if(manifest!=null){ %>
		 <form name='main_form' id='main_form' action='LearnerServlet' method='POST'>
        	<input type='hidden' id='request-type' name='requesttype' value='relearn'/> 
        	 <input type='hidden' value='<%=Utilities.escapeXml(manifest.getRegClientName())%>' id='clientName' name='clientName'/> 
        	 <input type='hidden' value='<%=Utilities.escapeXml(manifest.getAutLoginType())%>' id='lstAutCred' name='lstAutCred'/> 
        	 <input type='hidden' value='<%=manifest.getBrowser()%>' id='lstBrowserType' name='lstBrowserType'/> 
        	 <input type='hidden' value='<%=device%>' id='listDevice' name='listDevice'/>
        	 <input type='hidden' value='reLearn' id='reLearn' name='reLearn'/>  
        	 <input type='hidden' value='<%=manifest.getAut().getId()%>' id='app' name='app'/>
        	  <input type='hidden' value='<%=run.getId()%>' id='runid' name='runid'/>
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
        	  
        </form>
         <%} %>
	</body>
</html>