<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  reg_device_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.ycs.tenjin.device.RegisteredDevice"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION

*/

-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Device Details</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/bordered.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script src="js/formvalidator.js"></script>
<script type='text/javascript' src='js/pages/reg_device_view.js'></script>
</head>
<body>

	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>

	<%
		Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
		String status = (String) map.get("STATUS");
		String message = (String) map.get("MESSAGE");
		RegisteredDevice rd = (RegisteredDevice) map.get("REGISTERED_DEVICE_BEAN");
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		if (tjnSession != null) {
	%>

	<div class='title'>
		<p>Registered Device Details</p>
	</div>

	<form name='device_details' action='DeviceServlet' method='GET'>
		<input type='hidden' id='viewType' value='EDIT' />
		<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		
		<div class='toolbar'>

			<input type='button' class='imagebutton back' value='Back'
				id='btnBack' /> <input type='button' value='Save' id='btnSave'
				class='imagebutton save' />
		</div>
		<%
			if (message != null && !status.equalsIgnoreCase("SUCCESS")) {
		%>
		<div id='user-message' class='msg-success' style='display: block;'>
			<div class="msg-icon">&nbsp;</div>
			<div class="msg-text"><%=message%></div>
		</div>

		<%
			} else {
		%>
		<div id='user-message'></div>

		<%
			}
		%>

		<div class='form container_16'>
			<fieldset>
				<legend>Basic Information</legend>
				<input type='hidden' value='<%=Utilities.escapeXml(rd.getDeviceStatus())%>'
					id='deviceStatus' name='deviceStatus' />
				<div class='fieldSection grid_16'>
					<input type='hidden' id='txtDevRecId'
						value='<%=rd.getDeviceRecId()%>'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtDeviceId'>Device Id</label>
					</div>
					<div class='grid_13'>
						<input type='text' id='txtDeviceId' name='txtDeviceId'
							class='stdTextBox mid'  title='Device Id'
							value='<%=rd.getDeviceId()%>' disabled />
					</div>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtDeviceName'>Device Name</label>
					</div>
					<div class='grid_13'>
						<input type='text' id='txtDeviceName' name='txtDeviceName'
							class='stdTextBox mid' mandatory='yes' title='Device Name'
							value='<%=Utilities.escapeXml(rd.getDeviceName())%>' maxlength='255' />
					</div>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtPlatform'>Device Type</label>
					</div>
					<div class='grid_13'>
						<input type='text' id='txtDevType' name='txtDevType'
							class='stdTextBox mid'  title='Platform'
							value='<%=rd.getDeviceType()%>' disabled />
					</div>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtPlatform'>Platform</label>
					</div>
					<div class='grid_13'>
						<input type='text' id='txtPlatform' name='txtPlatform'
							class='stdTextBox mid'  title='Platform'
							value='<%=rd.getPlatform()%>' disabled />
					</div>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtPlatformVersion'>Platform Version</label>
					</div>
					<div class='grid_13'>
						<input type='text' id='txtPlatformVersion'
							name='txtPlatformVersion' class='stdTextBox mid' 
							title='Platform Version' onkeypress="return isNumberKey(event)"  value='<%=Utilities.escapeXml(rd.getPlatformVersion())%>' maxlength='255' />
					</div>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtDeviceStatus'>Device Status</label>
					</div>
					<div class='grid_13'>
						<select id='txtDeviceStatus' name='txtDeviceStatus'
							class='stdTextBoxNew' title='Device Status'>
							<option value='Active'>Active</option>
							<option value='Inactive'>Inactive</option>
						</select>
					</div>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtRemarks'>Remarks</label>
					</div>
					<div class='grid_13'>
						<%
							if (rd.getDeviceDescription() == null) {
						%>
						<textarea id='txtRemarks' name='txtRemarks' class='stdTextBox'
							style='width: 100%; height: 50px;' maxlength="3000"></textarea>
						<%
							} else {
						%><textarea id='txtRemarks' name='txtRemarks' class='stdTextBox'
							style='width: 100%; height: 50px;' maxlength="3000"><%=Utilities.escapeXml(rd.getDeviceDescription())%></textarea>
						<%
							}
						%>
					</div>
				</div>

			</fieldset>
		</div>


	</form>


	<%
		} else {
			response.sendRedirect("noSession.jsp");
		}
	%>
	<div class='modalmask'></div>
	<div class='subframe'>
		<iframe frameborder="2" marginwidth="5" marginheight="5"
			style='height: 380px; width: 770px;' id='clients_frame' src=''></iframe>

	</div>
</body>
</html>