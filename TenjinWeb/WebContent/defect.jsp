<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  defect.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<%@page import="java.util.UUID"%>
<%@page import="com.google.common.cache.Cache"%>
<%@page import="com.google.common.cache.CacheBuilder"%>
<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.project.TestSet"%>
<%@page import="com.ycs.tenjin.project.TestCase"%>
<%@page import="com.ycs.tenjin.project.TestStep"%>
<%@page import="com.ycs.tenjin.run.TestRun"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
   11-12-2020			Pushpalatha				TENJINCG-1221
 
*/

-->
<head>
<title>New Defect</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel="Stylesheet" href="css/bordered.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel='stylesheet' href='css/select2.min.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/formvalidator.js'></script>
<script type='text/javascript' src='js/pages/defect_master.js'></script>
<script type='text/javascript' src='js/pages/defect.js'></script>
<script type='text/javascript' src='js/select2.min.js'></script>

<style>
#selectedFiles {
	margin-left: 0;
}

.att_block {
	padding: 5px;
	border: 1px solid #707a86;
	background-color: #b7c0ef;
	border-radius: 5px;
	margin-left: 0;
}

.att_block p {
	overflow: hidden;
	text-overflow: ellipsis;
}

#def_review_confirmation_dialog, #def_review_progress_dialog {
	background-color: #fff;
}

.subframe_content {
	display: block;
	width: 100%;
	margin: 0 auto;
	min-height: 120px;
	padding-bottom: 20px;
}

.subframe_title {
	width: 644px;
	height: 20px;
	background: url('images/bg.gif');
	color: white;
	font-family: 'Open Sans';
	padding-top: 6px;
	padding-bottom: 6px;
	padding-left: 6px;
	font-weight: bold;
	margin-bottom: 10px;
}

.subframe_actions {
	display: inline-block;
	width: 100%;
	background-color: #ccc;
	padding-top: 5px;
	padding-bottom: 5px;
	font-size: 1.2em;
	text-align: center;
}

.highlight-icon-holder {
	display: block;
	max-height: 50px;
	margin-bottom: 10px;
	text-align: center;
}

.highlight-icon-holder>img {
	height: 50px;
}

.subframe_single_message {
	width: 80%;
	text-align: center;
	font-weight: bold;
	display: block;
	margin: 0 auto;
	font-size: 1.3em;
}

#progressBar {
	width: 475px;
	margin-top: 20px;
	margin-left: auto;
	margin-right: auto;
}
</style>

</head>
<body>
	<%
		Cache<String, Boolean> csrfTokenCache = null;
		csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS)
				.build();
		session.setAttribute("csrfTokenCache", csrfTokenCache);
		UUID uuid = UUID.randomUUID();
		String csrftoken = uuid.toString();
		csrfTokenCache.put(csrftoken, Boolean.TRUE);
		session.setAttribute("csrftoken_session", csrftoken);
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		User cUser = tjnSession.getUser();

		Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("DEFECT_SCR_MAP");
		String status = (String) map.get("STATUS");
		String message = (String) map.get("MESSAGE");
		List<TestRun> runs = (List<TestRun>) map.get("RUNS");
		List<Aut> auts = (List<Aut>) map.get("AUTS");
		//int defRecId = (Integer) map.get("DEF_REC_ID");
	%>

	<div class='title'>
		<p>Create a new defect</p>
	</div>

	<input type='hidden' id='status'
		value='<%=Utilities.escapeXml(status)%>' />
	<input type='hidden' id='message'
		value='<%=Utilities.escapeXml(message)%>' />


	<input type='hidden' id='prjId'
		value='<%=tjnSession.getProject().getId()%>' />
	<input type='hidden' id='userId'
		value='<%=Utilities.escapeXml(tjnSession.getUser().getId())%>' />

	<form id='new_defect_form' name='new_defect_form'
		action='DefectServlet' method='POST' enctype="multipart/form-data">
		<input type='hidden' id='viewType' name='viewType' value='create' />
		<input type='hidden' id='form_data' name='form_data' value='' /> <input
			type='hidden' id='post_defect' name='post_defect' value='N' /> <input
			type="hidden" id="csrftoken_form" name="csrftoken_form"
			value=<%=csrftoken%> />
		<div class='toolbar'>
			<input type='submit' class='imagebutton save' value='Save'
				id='btnSave' />
			<%
				if (cUser.getRoleInCurrentProject().equals("Project Administrator")) {
			%>
			<input type='submit' class='imagebutton ok' value='Save and Post'
				id='btnSavePost' />
			<%
				}
			%>
			<input type='button' class='imagebutton refresh' value='Refresh'
				id='btnRefresh' /> <input type='button' class='imagebutton cancel'
				value='Cancel' id='btnCancel' />
		</div>

		<div id='user-message'></div>

		<div class='form container_16'>
			<fieldset>
				<legend>Basic Information</legend>
				<div class='fieldSection grid_16'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtSummary'>Summary</label>
					</div>
					<div class='grid_13'>
						<input type='text' id='txtSummary' name='txtSummary'
							class='stdTextBox long' title='Summary' mandatory='yes'
							maxlength='255' />
					</div>
				</div>
			</fieldset>

			<div class='clear'></div>

			<fieldset>
				<legend>Tenjin Information</legend>
				<div class='fieldSection grid_8'>

					<!-- Modified by Priyanka for TCGST-46 starts -->
					<div class='grid_2'>
						<label for='runid'>Run ID<font class="asteriskV"
							color="red">&nbsp;*</font></label>
					</div>
					<!-- Modified by Priyanka for TCGST-46 ends -->
					<div class='grid_4'>
						<select id='runid' name='runId' class='stdTextBoxNew run_info'
							title='Run ID' required='true'>
							<option value='-1'>-- Select One --</option>
							<%
								if (runs != null) {
									for (TestRun run : runs) {
							%>
							<option value='<%=run.getId()%>'><%=run.getId()%> -
								<%=Utilities.escapeXml(run.getTestSet().getName())%></option>
							<%
								}
								}
							%>
						</select>

					</div>

					<div class='clear'></div>
					<!-- Modified by Priyanka for TCGST-46 starts -->
					<div class='grid_2'>
						<label for='testcase'>Test Case<font class="asteriskV"
							color="red">&nbsp;*</font></label>
					</div>
					<!-- Modified by Priyanka for TCGST-46 ends -->
					<div class='grid_4'>
						<select id='testcase' name='testcase'
							class='stdTextBoxNew run_info' title='Test Case'>
							<option value='-1'>-- Select One --</option>
						</select>
					</div>


					<div class='clear'></div>
					<!-- Modified by Priyanka for TCGST-46 starts -->
					<div class='grid_2'>
						<label for='teststep'>Test Step<font class="asteriskV"
							color="red">&nbsp;*</font></label>
					</div>
					<!-- Modified by Priyanka for TCGST-46 ends -->
					<div class='grid_4'>
						<select id='teststep' name='teststep'
							class='stdTextBoxNew run_info' title='Test Step'>
							<option value='-1'>-- Select One --</option>
						</select>
					</div>


				</div>

				<div class='fieldSection grid_7'>
					<!-- Modified by Priyanka for TCGST-46 starts -->
					<div class='grid_2'>
						<label for='appid'>Application <font class="asterisk"
							color="red">&nbsp;*</font></label>
					</div>
					<!-- Modified by Priyanka for TCGST-46 ends -->
					<div class='grid_4'>
						<select id='appid' name='appId' class='stdTextBoxNew app_info'
							title='Application'>
							<option value='-1'>-- Select One --</option>
							<%
								if (auts != null) {
									for (Aut aut : auts) {
							%>
							<option value='<%=aut.getId()%>'><%=Utilities.escapeXml(aut.getName())%></option>
							<%
								}
								}
							%>
						</select>
					</div>
					<div class='clear'></div>
					<!-- Modified by Priyanka for TCGST-46 starts -->
					<div class='grid_2'>
						<label for='functionId'>Functions<font class="asterisk"
							color="red">&nbsp;*</font></label>
					</div>
					<!-- Modified by Priyanka for TCGST-46 ends -->
					<div class='grid_4'>
						<select id='functionId' name='function'
							class='stdTextBoxNew app_info' title='Function'>
							<option value='-1'>-- Select One --</option>

						</select>
					</div>

				</div>
			</fieldset>
			<div class='clear'></div>

			<fieldset>
				<legend>Defect Management Information</legend>
				<div class='fieldSection grid_8'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='dttname'>Instance</label>
					</div>
					<div class='grid_4'>
						<input type='text' id='dttname' name='dttname' title='Instance'
							class='stdTextBox' readonly />
					</div>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='severity'>Severity</label>
					</div>
					<div class='grid_4'>
						<select id='severity' name='severity' class='stdTextBoxNew'
							mandatory='yes' title='Severity'>
							<option value='-1'>-- Select One --</option>

						</select>
					</div>

				</div>

				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='dttproject'>Project</label>
					</div>
					<div class='grid_4'>
						<input type='text' id='dttproject' name='dttproject'
							class='stdTextBox' readonly title='Defect Management Project' />
					</div>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='priority'>Priority</label>
					</div>
					<div class='grid_4'>
						<select id='priority' name='priority' class='stdTextBoxNew'
							mandatory='yes' title='Priority'>
							<option value='-1'>-- Select One --</option>

						</select>
					</div>

				</div>
				<!-- Added by Pushpa for TENJINCG-1221 starts -->
				<div class='fieldSection grid_8' id='subsetBlock'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='dttSubset'>DTT Subset</label>
					</div>
					<div class='grid_4'>
						<input type='text' id='dttSubset' name='dttSubset'
							title='DTT Subset' class='stdTextBox' readonly />
					</div>

				</div>
				<div class='fieldSection grid_7' id='ownerBlock'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='repoOwner'>Repo Owner</label>
					</div>
					<div class='grid_4'>
						<input type='text' id='repoOwner' name='repoOwner'
							title='Repo Owner' class='stdTextBox' readonly />
					</div>

				</div>
				<!-- Added by Pushpa for TENJINCG-1221 starts -->

				<div class='fieldSection grid_16'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='description'>Description</label>
					</div>

					<div class='grid_13'>
						<!-- Modified by Priyanka for TCGST-46 starts -->
						<textarea id='description' name='description' class='stdTextBox'
							title='Description' mandatory='yes' maxlength='4000'
							style='width: 89%; height: 50px; margin-top: 1em;'></textarea>
						<!-- Modified by Priyanka for TCGST-46 starts -->
					</div>

				</div>
			</fieldset>

			<fieldset>
				<legend>Attachments</legend>
				<div class='fieldSection grid_16' id='attachments_block'>
					<div class='grid_6' id='add'>

						<input type='file' id='txtFile' name='Attachment'
							multiple='multiple' class='stdTextBox' />

						<div id='selectedFiles' class='grid_16'>
							<div class='clear'></div>
						</div>
					</div>

				</div>
			</fieldset>
		</div>









	</form>

	<div class='modalmask'></div>
	<div class='subframe' id='def_review_progress_dialog' style='left: 20%'>
		<div class='subframe_title'>
			<p>Defect Status</p>
		</div>
		<div class='highlight-icon-holder' id='processing_icon'></div>
		<div class='subframe_content'>
			<div class='subframe_single_message' id='def_review_message'>
				<p></p>
			</div>
			<div id="progressBar" class="default" style='margin: 0 auto;'>
				<div></div>
			</div>
		</div>
		<div class='subframe_actions' id='def_review_pr_actions'
			style='display: none;'>
			<input type='button' id='btnPClose' value='Close'
				class='imagebutton cancel' callback='undefined' />
		</div>
	</div>

</body>
</html>