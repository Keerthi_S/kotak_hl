<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  live_extr_progress.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India



/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->


<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.BridgeProcess"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.ExtractionRecord"%>
<%@page import="com.ycs.tenjin.run.ExtractorProgressView"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.bridge.pojo.TaskManifest"%>
<%@page import="com.ycs.tenjin.run.TestRun"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Module"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Learner - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel="stylesheet" type="text/css" href="css/progressbarskins/default/progressbar.css">
		<link rel='stylesheet' href='css/jquery.dataTables.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/live_extr_progress.js'></script>
		<script type='text/javascript' src='js/pages/extr_output.js'></script>
		<script type="text/javascript" src="js/progressbar.js"></script>
		<script src="js/tables.js"></script>
	</head>
	<body>
		<%
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		User cUser = tjnSession.getUser();
		ExtractorProgressView view = (ExtractorProgressView) request.getSession().getAttribute("EXTR_PROGRESS_VIEW");
		String status = view.getScreenStatus();
		String sMessage = view.getMessage();
		String appName=(String)request.getSession().getAttribute("APP_NAME");
		if(Utilities.trim(status).equalsIgnoreCase("success")){
			sMessage="noerror";
		}
		TestRun run =  null;

		Module currentFunction = null;
		
		String progress = "0";
		boolean learningComplete = false;
		ExtractionRecord currentRecord = null;
		
		if(status != null && status.equalsIgnoreCase("success")){
			run = view.getRun();
			currentFunction = view.getCurrentFunction();
			currentRecord =view.getCurrentRecord();
			progress = view.getProgressPercentage();
		}
		if(Double.parseDouble(progress) == 100){
			learningComplete = true;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss");
		String runEnd = "";
		if(run.getEndTimeStamp() != null){
			runEnd = sdf.format(new Date(run.getEndTimeStamp().getTime()));
		}else{
			runEnd = " - ";
		}
		
		if(runEnd.equalsIgnoreCase(" - ")){
			learningComplete = false;
		}
		%>
		
		<div class='title'>
			<p>Extraction Progress</p>
		</div>
		
		<div class='toolbar'>
			<%if(learningComplete){ %>
				<input type='button' id='btnBack' value='Back' class='imagebutton back'/>
			<%} else {%>
			<div style='font-weight:bold;font-size:0.7em;float:left;line-height:25px;'>This page will automatically refresh every <select disabled id='refTimeout'>
				<option value='5000'>5 Seconds</option>
			</select></div>
			
			<input type='button' id='btnRefresh' value='Refresh Now' class='imagebutton refresh'/>
			<input type='button' id='toggleRefresh' value='Stop Automatic Refresh' class='imagebutton cancel' ac='stop'/>
			<%} %>
			
			
		</div>
		
		
		<div id='user-message'></div>
		
		<input type='hidden' id='screenStatus' value='<%=status %>'/>
		<input type='hidden' id='message' value='<%=sMessage %>'/>
		
		<%if(learningComplete || Utilities.trim(status).equalsIgnoreCase("error")){ %>
			<input type='hidden' id='autoRefreshFlag' value='no'/>
		<%} else{%>
			<input type='hidden' id='autoRefreshFlag' value='yes'/>
		<%} %>
		
		
		
		<div class='form container_16'> 
			<%if(run != null){ %>
			<fieldset id='run-info'>
				<legend>Extraction Run Information</legend>
				<div class='fieldSection grid_7'>
					<div class='grid_2'><label for='runId'>Run ID</label></div>
					<div class='grid_4'><input type='text'  disabled id='runId' name='runId' class='stdTextBox' value='<%=run.getId() %>' /></div>
					<div class='clear'></div>
					<div class='grid_2'><label for='app'>Application</label></div>
					<div class='grid_4'><input type='text' id='app' name='app' disabled value='<%=Utilities.escapeXml(appName) %>' class='stdTextBox'/></div>
					<div class='clear'></div>
					<div class='grid_2'><label for='browser'>Browser</label></div>
					<div class='grid_4'><input type='text' id='browser' name='browser' disabled value='<%=run.getBrowser_type() %>' class='stdTextBox'/></div>
				</div>
				
				<div class='fieldSection grid_7'>
					<div class='grid_2'><label for='start'>Start Time</label></div>
					<%
										String runStart = "";
										if(run.getStartTimeStamp() != null){
											runStart = sdf.format(new Date(run.getStartTimeStamp().getTime()));
										}else{
											runStart = " - ";
										}
										%>
					<div class='grid_4'><input type='text' id='start' name='start' disabled value='<%=runStart %>' class='stdTextBox'/></div>
					<div class='clear'></div>
					<div class='grid_2'><label for='start'>End Time</label></div>
					<%
										%>
					<div class='grid_4'><input type='text' id='end' name='end' disabled value='<%=runEnd %>' class='stdTextBox'/></div>
					<div class='clear'></div>
					<div class='grid_2'><label for='start'>Elapsed Time</label></div>
					<div class='grid_4'><input type='text' id='start' name='start' disabled value='<%=run.getElapsedTime() %>' class='stdTextBox'/></div>
				</div>
			</fieldset>
			
			<%if(learningComplete){
				
			}else{%>
			<fieldset style='margin-top:10px;'>
				<legend>Overall Extraction Progress</legend>
				<div class='clear'></div>
				<div class='grid_16' style='margin-top:15px;'>
					<div class='lrnr_progress_bar grid_8' style='margin:0 auto;'>
						<input type='hidden' id='progpcnt' value='<%=progress %>'/>
						<div id="progressBar" class="default" style='margin:0 auto;'><div></div></div>
					</div>
					<div class='clear'></div>
				</div>
			</fieldset>
			<%} %>
			<%if(!learningComplete) {%>
			<fieldset>
				<legend>Currently Extracting</legend>
				<div class='fieldSection grid_15'  id='currentModuleInfo'>
					<table class='bordered' cellspacing='0' width='100%' id='c_modules_table'>
						<thead>
								<tr>
									<th class='tiny'>Code</th>
									<th>Function Name</th>
									<th>Status</th>
									<th>Details</th>
								</tr>
						</thead>
						<tbody>
							<%
							if(currentFunction != null){
								%>
								<tr>
									<td class='tiny'><%=Utilities.escapeXml(currentFunction.getCode())%></td>
									<td><%=Utilities.escapeXml(currentFunction.getName())%></td>
									<td class='tiny'><img src='images/ajax-loader.gif'/></td>
									<td><a href='ExtractorServlet?param=extractor_progress&runid=<%=run.getId() %>&func=<%=Utilities.escapeXml(currentFunction.getCode()) %>'>View</a></td>
								</tr>
								<%
							}else{
								%>
								<tr>
									<td colspan='4'>Loading. Please Wait...</td>
								</tr>
								<%
							}
							%>
						</tbody>
					</table>
				</div>
			</fieldset>
			<%} %>
			<%
			List<Module> modules = run.getFunctions();
			if((modules != null && modules.size() > 1) || (modules.size()==1 && modules.get(0).getLastSuccessfulExtractionStatus() != null && 
					!modules.get(0).getLastSuccessfulExtractionStatus().getStatus().equalsIgnoreCase(BridgeProcess.IN_PROGRESS))){
				%>
				<fieldset>
				<legend>Functions for this Run</legend>
				<div class='fieldSection grid_15'  id='modulesInfo'>
					<table class='display tjn-default-data-table' cellspacing='0' width='100%' id='modules_table'>
						<thead>
							<tr>
								<th class='tiny nosort'>#</th>
								<th class='tiny'>Code</th>
								<th style="display:none;"></th>
								<th>Function Name</th>
								<th>Record Count</th>
								<th>Status</th>
								<th>Message</th>
								<th>Details</th>
								<th>Output</th>
							</tr>
						</thead>
						<tbody>
						<%
							
											int i=0;
											if(modules != null && modules.size() > 0){
											for(Module module:modules){
									i++;
									
									String imgUrl = "";
									String message = "";
									String statusview="";
									for(ExtractionRecord record:module.getLastSuccessfulExtractionStatus().getRecords()){
							
										statusview = Utilities.trim(record.getResult());
										
									}
									if(module.getLastSuccessfulExtractionStatus().getStatus().equalsIgnoreCase(BridgeProcess.IN_PROGRESS)){
										imgUrl = "images/ajax-loader.gif";
									}else if(module.getLastSuccessfulExtractionStatus().getStatus().equalsIgnoreCase(BridgeProcess.QUEUED)){
										imgUrl = "images/queued_20c20.png";
									}else if(module.getLastSuccessfulExtractionStatus().getStatus().equalsIgnoreCase("complete")){
										if(statusview.equalsIgnoreCase("s")){
										imgUrl = "images/success_20c20.png";
										message = "Extraction Complete";
										}else if(statusview.equalsIgnoreCase("F") || statusview.equalsIgnoreCase("E")){
											imgUrl = "images/failure_20c20.png";
										
											message = "Extraction completed with errors";
										}else if(statusview.equalsIgnoreCase("aborted")){
											imgUrl = "images/failure_20c20.png";
											message = "Extraction aborted";
										}
									}else if(module.getLastSuccessfulExtractionStatus().getStatus().equalsIgnoreCase("error")){
										imgUrl = "images/failure_20c20.png";
										message = module.getLastSuccessfulExtractionStatus().getMessage();
									}else if(module.getLastSuccessfulExtractionStatus().getStatus().equalsIgnoreCase("aborted")){
										imgUrl = "images/failure_20c20.png";
										message = module.getLastSuccessfulExtractionStatus().getMessage();
									} 
						%>
						
						
						<tr>
							<td class='tiny'><%=i%></td>
							<td class='tiny'><%=Utilities.escapeXml(module.getCode())%></td>
							<td style="display:none;" ><input type='hidden' id='txtModuleCode' value='<%=Utilities.escapeXml(module.getCode())%>'/>
							<input type='hidden' id='txtModuleName' value='<%=Utilities.escapeXml(module.getName())%>'/>
								<input type='hidden' id='selApp' value='<%=run.getAppId() %>'/>
							</td>
							<td><%=Utilities.escapeXml(module.getName())%></td>
							<td><%=module.getLastSuccessfulExtractionStatus().getRecords().size()%></td>
							<td class='tiny'><img src='<%=imgUrl %>'/></td>
							<td><%=Utilities.escapeXml(message) %></td>
							<td>
								<a href='ExtractorServlet?param=extractor_progress&runid=<%=run.getId() %>&func=<%=Utilities.escapeXml(module.getCode()) %>'>View</a>
							</td>
							<td>
								<%if(module.getLastSuccessfulExtractionStatus().getStatus().equalsIgnoreCase("complete")){ %>
									<a class='template-gen' href='#' function='<%=Utilities.escapeXml(module.getCode()) %>'>Download</a>
									<img src='images/ajax-loader.gif' id='loader_<%=Utilities.escapeXml(module.getCode()) %>' alt='Please Wait...' style='display:none;'/>
								<%} else{%>
									Not Available
								<%} %>
							</td>
						</tr>
						<%
							}
							}else{
								%>
								<tr>
									<td colspan='6'>Loading Functions. Please wait...</td>
								</tr>
								<%
							}
						%>
					</tbody>
					</table>
					<div class='emptysmall'></div>
				</div>
			</fieldset>
				<%
			}
			%>
			<%} %>
		</div>
		<div class='modalmask'></div>
		<div class='subframe' style='left:20%'>
			<iframe frameborder="2" scrolling="no"  marginwidth="5" marginheight="5" style='height:400px;width:600px;' seamless="seamless" id='ttd-options-frame' src=''></iframe>
		</div>
	</body>
</html>