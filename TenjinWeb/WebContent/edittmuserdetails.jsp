<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  edittmuserdetails.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!DOCTYPE html>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@page import="com.ycs.tenjin.testmanager.TestManagerInstance"%>
<%@page import="com.ycs.tenjin.defect.DefectManagementInstance"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
   
*/

-->
<head>
<meta charset="ISO-8859-1">
<title>TM Credential Details</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type="text/javascript" src="js/formvalidator.js"></script>
<script src="js/pages/edittmuserdetails.js"></script>


<%
	TenjinSession tjnSession = (TenjinSession) request.getSession()
			.getAttribute("TJN_SESSION");
	User cUser = tjnSession.getUser();

	Map<String, Object> map = (Map<String, Object>) request
			.getSession().getAttribute("SCR_MAP");
	TestManagerInstance tmInstance = (TestManagerInstance) map.get("tmInstance");
	List<String> instances=(List<String>)map.get("instances");
%>

</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<div class='title'>
	<p>TM User Details</p>
</div>
<form name='defect_user_mapping_form' action='AutServlet' method='POST'>
	<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
	<input type='hidden' id='viewType' value='EDIT' />
	<div class='toolbar'>
		<input type='button' class='imagebutton back' value='Back'
			id='btnBack' /> <input type='button' class='imagebutton save'
			value='Save' id='btnSave' /> <input type='button' value='Edit'
			class='imagebutton edit' id='btnEdit' style="display: none;" />
	</div>

	<div id='user-message'></div>

	<div class='form container_16'>
		<div class='fieldSection grid_16'>
			<div class='clear'></div>
			
			<div class='grid_3'>
				
				<label for='txtUserId'>User ID</label>
			</div>
			
			<div class='grid_12'>
				
				<input type='text' class='stdTextBox' value=<%=cUser.getId()%>
					name='txtUserId'  id='txtUserId' title='AUT UserID'
					disabled='disabled' />
			</div>

			<div class='clear'></div>
			<div class='grid_3'>

				<label for='lstAppId'>Instance</label>
			</div>

			<input type="hidden" value='<%=tmInstance.getInstanceName()%>'id="tmInstance"/>
			<div class='grid_12'>

				<select id='lstInstance' name='lstInstance' type='select'
					class='stdTextBox' mandatory='yes' title="Instance" mandatory>
					
					<% 
						for (String instance : instances) {
							if(instance.equalsIgnoreCase(tmInstance.getInstanceName())){
					%>
							<option value='<%=instance%>' selected="selected"><%=instance%></option>
							<%
							}else{ %>
								<option value='<%=instance%>'><%=instance%></option>
							<%
								}
						}
					%>
				</select>
			</div>

			<div class='clear'></div>
			<div class='grid_3'>
				
				<label for='TMUserName'>Login Name</label>
			</div>
			
			
			<div class='grid_12'>
					<input type='text' class='stdTextBox' id='TMUserName'
					mandatory='yes' name='txtAppUserName' title='Login Name' mandatory value = '<%=tmInstance.getUsername()%>'maxlength='30'/>
			</div>
			<div class='clear'></div>
			
			<div class='grid_3'>
				
				<label for='TMPassword'>Login Password</label>
			</div>

			<div class='grid_12'>
					<input type='password' id='TMPassword' name='txtAppPassword'
					mandatory='yes' title='Login Password' class='stdTextBox' mandatory value='<%=tmInstance.getPassword()%>' maxlength='50'/>
			</div>


		</div>
</form>
</body>

</html>