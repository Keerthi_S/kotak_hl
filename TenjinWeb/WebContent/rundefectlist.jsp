<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  rundefectlist.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<%@page import="java.util.TreeMap"%>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.util.HatsConstants"%>
<%@page import="com.ycs.tenjin.run.TestRun"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.defect.Defect"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>

<!-- /*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 
*/ -->



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Defects</title>
		<title>Tenjin - Intelligent Enterprise Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/rprogress.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel="stylesheet" type="text/css" href="css/progressbarskins/default/progressbar.css">
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/rundefectlist.js'></script>
		<script type="text/javascript" src="js/progressbar.js"></script>
	
		<style>
			#def_review_confirmation_dialog,#def_review_progress_dialog
			{
				background-color:#fff;
				
			}
			
			.subframe_content
			{
				display:block;
				width:100%;
				margin:0 auto;
				min-height:120px;
				padding-bottom:20px;
			}
			.subframe_title
			{
				width:644px;
				height:20px;
				background:url('images/bg.gif');
				color:white;
				font-family:'Open Sans';
				padding-top:6px;
				padding-bottom:6px;
				padding-left:6px;
				font-weight:bold;
				margin-bottom:10px;
			}
			
			.subframe_actions
			{
				display:inline-block;
				width:100%;
				background-color:#ccc;
				padding-top:5px;
				padding-bottom:5px;
				font-size:1.2em;
				text-align:center;
			}
			
			.highlight-icon-holder
			{
				display:block;
				max-height:50px;
				margin-bottom:10px;
				text-align:center;
			}
			
			.highlight-icon-holder > img
			{
				height:50px;
			}
			
			.subframe_single_message
			{
				width:80%;
				text-align:center;
				font-weight:bold;
				display:block;
				margin:0 auto;
				font-size:1.3em;
			}
			
			#progressBar
			{
				width:475px;
				margin-top:20px;
				margin-left:auto;
				margin-right:auto;
			}
			
			#tbl-defects_filter
			{
				display:none;
			}
			.text-center{
				text-align:center
			}
		</style>
	</head>
	<body>
	
	
		<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	
		<%
		
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		User cUser = tjnSession.getUser();
		Project project = null;
		project = tjnSession.getProject();
		
		Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
		String status = (String)map.get("STATUS");
		String message = (String)map.get("MESSAGE");
		
		TestRun run = (TestRun)map.get("RUN");
		List<Defect> defects = (List<Defect>) map.get("DEFECTS");
		Map<Integer, List<String>> sevMap = (Map<Integer, List<String>>) map.get("SEV_MAP");
		String callback = request.getParameter("callback");
		
		Map<String, String> actionsMap = new TreeMap<String, String>();
		actionsMap.put("NOREVIEW", "Not Reviewed");
		actionsMap.put("POST", "Post");
		actionsMap.put("ONHOLD", "On-Hold");
		actionsMap.put("IGNORE", "Ignore");
		String tcRecId = request.getParameter("tcRecId");
		%>
		<input type='hidden' id='tcRecId' value='<%=tcRecId %>'/>
		<input type='hidden' id='callback' value='<%=callback %>'/>
		<div class='title'>
			<p>Defects for this Run</p>
			<div id='prj-info'>
				<span style='font-weight:bold'>Domain: </span>
				<span><%=project.getDomain() %></span>
				<span>&nbsp&nbsp</span>
				<span style='font-weight:bold'>Project: </span>
				<span><%=project.getName() %></span>
			</div>
		</div>
	
		<form name='main_form' action='#' method='POST'>
			<div class='toolbar'>
				<input type='button' id='btnBack' value='Back' class='imagebutton back'/>
				<%if(defects!=null && defects.size()>0){ %>
				<input type='button' id='btnSave' value='Save Review' class='imagebutton ok'/>
				<%} %>
			</div>
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			
			
			
			<%
			if(status != null && status.equalsIgnoreCase("SUCCESS") && message != null && !message.equalsIgnoreCase("")){
				%>
				<div id='user-message' class='msg-success' style='display:block;'>
					<div class="msg-icon">&nbsp;</div>
					<div class="msg-text"><%=message %></div>
				</div>
				<%
			}else if(status != null && !status.equalsIgnoreCase("") && !status.equalsIgnoreCase("SUCCESS")){
				%>
				<div id='user-message' class='msg-err' style='display:block;'>
					<div class="msg-icon">&nbsp;</div>
					<div class="msg-text"><%=message %></div>
				</div>
				
				<%
			}else{
				%>
				<div id='user-message'></div>
				
				<%
			}
			%>
			
			<div class='form' style='width:1053px;'>
				<div class='three-col-field-section' style='height:87px;'>
					<input type='hidden' id='runid' name='runid' value='<%=run.getId() %>'/>
					<table class='layout'>
						<tr>
							<td class='layout-label'>Run ID</td>
							<td class='layout-value' id='runId'><%=run.getId() %></td>
						</tr>
						<tr>
							<td class='layout-label'>Started By</td>
							<td class='layout-value'><%=run.getUser() %></td>
						</tr>
						<tr>
							<td class='layout-label'>Started On</td>
							<td class='layout-value'><%=run.getStartTimeStamp() %></td>
						</tr>
						<%
						String rStatus = "";
						String imgUrl = "";
						String elapsedTime ="";
						String endTimeStamp = "";
						if(run.getStatus() != null && run.getStatus().equalsIgnoreCase(HatsConstants.SCRIPT_EXECUTING)){
							endTimeStamp = "N/A";
							imgUrl = "black";
						}else if(run.getStatus() != null && run.getStatus().equalsIgnoreCase(HatsConstants.ACTION_RESULT_ERROR)){
							imgUrl = "red";
						}else if(run.getStatus() != null && run.getStatus().equalsIgnoreCase(HatsConstants.ACTION_RESULT_FAILURE)){
							imgUrl = "red";
						}else if(run.getStatus() != null && run.getStatus().equalsIgnoreCase(HatsConstants.ACTION_RESULT_SUCCESS)){
							imgUrl = "green";
						}else{
							imgUrl = "red";
						}
						%>
						
						
					</table>
				</div>
				<div class='three-col-field-section' style='height:87px;'>
					<table class='layout'>
						 
						<tr>
							<td class='layout-label'>Elapsed Time</td>
							<td class='layout-value'><%=run.getElapsedTime() %></td>
						</tr>
						<tr>
							<td class='layout-label'>Status</td>
							<td class='layout-value' title='<%=run.getStatus()%>' style='font-weight:bold;color:<%=imgUrl%>'><%=run.getStatus() %></td>
						</tr>
						<tr>
							<td class='layout-label'>Ended On</td>
							<%
							if(endTimeStamp != null && endTimeStamp.equalsIgnoreCase("N/A")){
								%>
								<td class='layout-value'><%=endTimeStamp %></td>
								<%
							}else{
								%>
								<td class='layout-value'><%=run.getEndTimeStamp()%></td>
								<%
							}
							%>
							
						</tr>
						
					</table>
				</div>
				<div class='three-col-field-section' style='height:87px;'>
					<table class='layout'>
						<tr>
							<td class='layout-label'>Test Set</td>
							<td class='layout-value'><%=run.getTestSet().getName() %></td>
						</tr>
						<tr>
							<td class='layout-label'>Target</td>
							<%if(run.getTaskType().equalsIgnoreCase("execute")) {
							if(run.getMachine_ip().equalsIgnoreCase("0:0:0:0:0:0:0:1")){%>
								<td class='layout-value'>Localhost</td>
							<%}else{ %>
								<td class='layout-value'><%=run.getMachine_ip() %></td>
							<%}}else {%>
								<td class='layout-value'>N/A</td><%} %>
						</tr>
						 
						<tr>
							<td class='layout-label'>Browser</td>
							<%if(run.getTaskType().equalsIgnoreCase("execute")) {%>
							<td class='layout-value'><%=run.getBrowser_type() %></td>
							<%}else {%>
							<td class='layout-value'>N/A</td><%} %>
						</tr>
					</table>
				</div>
				
				<div class='clear'></div>
			 
				
				<div class='clear'></div>
				
				<div class='one-col-field-section'>
					<table id='tbl-defects' class='display compact'>
						<thead>
							<tr>
							    <th type='hidden'></th>
								<th class="text-center">ID</th>
								<th>Defect Management Identifier</th>
								<th>Summary</th>
								<th>Severity</th>
								 
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<%
							if(defects != null){
								for(Defect defect:defects){
									String dttIdentifier = defect.getIdentifier();
									if(Utilities.trim(dttIdentifier).equalsIgnoreCase("")){
										dttIdentifier = "Not Available";
									}
									%>
									<tr class='defect-row' drecid='<%=defect.getRecordId()%>'>
										<td type='hidden'></td>
										<td class="text-center"><a href='DefectServlet?param=fetch_defect_details&did=<%=defect.getRecordId() %>&callback=rundefectlist'><%=defect.getRecordId() %></a></td>
										<%List<String> severities = sevMap.get(defect.getAppId()); %>
										 
										<td><%= dttIdentifier%>
											<%
											
											if(severities != null){
												%>
												<input type='hidden' id='dtt_instance_<%=defect.getRecordId() %>' value='<%=severities.get(0) %>'/>
												
												<%
											}else{
												%>
												<input type='hidden' id='dtt_instance_<%=defect.getRecordId() %>' value='N/A'/>
												
												<%
											}
											%>
										
										</td>
										<td><%=defect.getAppName()%> - <%=Utilities.trim(defect.getFunctionCode()).length() > 0 ? defect.getFunctionCode() : "API [" + defect.getApiCode() + "]" %> - <%=defect.getSummary() %></td>
										<td>
											<input type='hidden' id='selectedSeverity_<%=defect.getRecordId() %>' value='<%=defect.getSeverity() %>'/>
											<%
											if(defect.getPosted() != null && defect.getPosted().equalsIgnoreCase("YES")){
												%>
												<select id='severity_<%=defect.getRecordId() %>' class='severity_selection' disabled>
												<%
											}else{
												%>
												<select id='severity_<%=defect.getRecordId() %>' class='severity_selection'>
												<%
											}
											%>
												<option value='-1'>-- Select One --</option>
												<%
												
												if(severities != null){
													int index=-1;
													for(String sev:severities){
														index++;
														if(index == 0){
															continue;
														}
														if(!sev.equalsIgnoreCase(defect.getSeverity())){
															%>
															<option value='<%=sev %>'><%=sev %></option>
															<%
														}else{
															%>
															<option value='<%=sev %>' selected><%=sev %></option>
															<%
														}
													}
												}
												%>
											</select>
										
										</td>
										 
										<td>
											<%
											if(Utilities.trim(defect.getTjnStatus()).equalsIgnoreCase("")){
												defect.setTjnStatus("NOREVIEW");
											}
											%>
											<input type='hidden' id='selectedAction_<%=defect.getRecordId() %>' value='<%=defect.getTjnStatus()%>'/>
											<input type='hidden' id='def_posted_<%=defect.getRecordId() %>' value='<%=defect.getPosted() %>'/>
											<%
											if(defect.getPosted() != null&& defect.getPosted().equalsIgnoreCase("YES")){
												%>
												<select id='action_<%=defect.getRecordId() %>' disabled>
												<%
											}else{
												%>
												<select id='action_<%=defect.getRecordId() %>'>
												<%
											}
											%>
											 
												
												<%
													for(String key:actionsMap.keySet()){
														if(key.equalsIgnoreCase(defect.getTjnStatus())){
															%>
															<option value='<%=key %>' selected><%=actionsMap.get(key) %></option>
															<%
														}else{
															%>
															<option value='<%=key %>'><%=actionsMap.get(key) %></option>
															<%
														}
													}
												%>
											</select>
										</td>
									</tr>
									<%
								}
							}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		
		<div class='modalmask'></div>
		<div class='subframe' id='def_review_confirmation_dialog'style='left:20%'>
			<div class='subframe_title'><p>Please Confirm...</p></div>
			<div class='subframe_content'>
				<div class='highlight-icon-holder'>
					<img src='images/warning_confirm.png' alt='Please Confirm' />
				</div>
				<div class='subframe_single_message'>
					<p>You have not specified an action to take for a few defects. Do you still want to proceed?</p>
				</div>
			</div>
			<div class='subframe_actions'>
				<input type='button' id='btnCOk' value='Proceed anyway' class='imagebutton ok'/>
				<input type='button' id='btnCCancel' value='Do not Proceed' class='imagebutton cancel'/>
			</div>
		</div>
		<div class='subframe' id='def_review_progress_dialog'style='left:20%'>
			<div class='subframe_title'><p>Review Status</p></div>
			<div class='highlight-icon-holder' id='processing_icon'>
					
				</div>
			<div class='subframe_content'>
				<div class='subframe_single_message' id='def_review_message'>
					<p></p>
				</div>
				<div id="progressBar" class="default" style='margin:0 auto;'><div></div></div>
			</div>
			<div class='subframe_actions' id='def_review_pr_actions' style='display:none;'>
				<input type='button' id='btnPClose' value='Close' class='imagebutton cancel'/>
			</div>
		</div>
	</body>
</html>