<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  defect_details.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!-- /*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 19-11-2020             Priyanka                TENJINCG-1231
 11-12-2020				Pushpalatha				TENJINCG-1221

*/ -->
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@page import="com.ycs.tenjin.defect.DefectLinkage"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="java.util.TreeMap"%>
<%@page import="com.ycs.tenjin.defect.Attachment"%>
<%@page import="com.ycs.tenjin.defect.Defect"%>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.project.TestSet"%>
<%@page import="com.ycs.tenjin.project.TestCase"%>
<%@page import="com.ycs.tenjin.project.TestStep"%>
<%@page import="com.ycs.tenjin.run.TestRun"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel="Stylesheet" href="css/bordered.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<link rel='stylesheet' href='css/select2.min.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/formvalidator.js'></script>
		<script type='text/javascript' src='js/pages/defect_master.js'></script>
		<script type='text/javascript' src='js/pages/defect_details.js'></script>
		<script type='text/javascript' src='js/select2.min.js'></script>
		
		<style>
			#selectedFiles
			{
				margin-left:0;
			}
			
			.att_block
			{
				padding:5px;
				border:1px solid #707a86;
				background-color:#b7c0ef;
				border-radius:5px;
				margin-left:0;
			}
			
			.att_block p
			{
				overflow:hidden;
				text-overflow:ellipsis;
			}
			
			#def_review_confirmation_dialog,#def_review_progress_dialog,#def_linkage_dialog
				{
					background-color:#fff;
					
				}
				
				.subframe_content
				{
					display:block;
					width:100%;
					margin:0 auto;
					min-height:120px;
					padding-bottom:20px;
				}
				.subframe_title
				{
					width:644px;
					height:20px;
					background:url('images/bg.gif');
					color:white;
					font-family:'Open Sans';
					padding-top:6px;
					padding-bottom:6px;
					padding-left:6px;
					font-weight:bold;
					margin-bottom:10px;
				}
				
				.subframe_actions
				{
					display:inline-block;
					width:100%;
					background-color:#ccc;
					padding-top:5px;
					padding-bottom:5px;
					font-size:1.2em;
					text-align:center;
				}
				
				.highlight-icon-holder
				{
					display:block;
					max-height:50px;
					margin-bottom:10px;
					text-align:center;
				}
				
				.highlight-icon-holder > img
				{
					height:50px;
				}
				
				.subframe_single_message
				{
					width:80%;
					text-align:center;
					font-weight:bold;
					display:block;
					margin:0 auto;
					font-size:1.3em;
				}
				
				#progressBar
				{
					width:475px;
					margin-top:20px;
					margin-left:auto;
					margin-right:auto;
				}
				
				p.subtitle
				{
					display:block;
					float:right;
					font-weight:bold;
					font-size:12px;
					line-height:22px;
					margin-right:10px;
					color:red;
				}
		</style>
	</head>
	<body>
	
		<%
		
	    Cache<String, Boolean> csrfTokenCache = null;
	    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
	    session.setAttribute("csrfTokenCache", csrfTokenCache);
	    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
	    session.setAttribute ("csrftoken_session", csrftoken);
		
		TenjinSession tjnSession = (TenjinSession) request.getSession()
				.getAttribute("TJN_SESSION");
		User cUser = tjnSession.getUser();
		Project project = tjnSession.getProject();
		Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("DEFECT_SCR_MAP");
		String status = (String) map.get("STATUS");
		String message = (String) map.get("MESSAGE");
		List<TestRun> runs = (List<TestRun>) map.get("RUNS");
		List<Aut> auts = (List<Aut>) map.get("AUTS");
		Defect defect = (Defect) map.get("defect");
		String callback = request.getParameter("callback");
		Map<String, String> actionsMap = new TreeMap<String, String>();
		actionsMap.put("NOREVIEW", "Not Reviewed");
		actionsMap.put("POST", "Post");
		actionsMap.put("ONHOLD", "On-Hold");
		actionsMap.put("IGNORE", "Ignore");
		defect.setPosted(Utilities.trim(defect.getPosted()));
		%>
		<div class='title'>
		<p>Defect Details</p>
		</div>
		
		<input type='hidden' id='status' value='<%=status%>' />
		<input type='hidden' id='message' value='<%=message%>' />
		<!-- Added by Priyanka for TENJINCG-1231 starts -->
			<input type='hidden' name='edate' id='endDate' value='${edate }'/>
			<!-- Added by Priyanka for TENJINCG-1231 ends -->
		
		<input type='hidden' id='callback' value='<%=callback %>'/>
		
		<form id='new_defect_form' name='new_defect_form' action='DefectServlet' method='POST' enctype="multipart/form-data">
			<input type='hidden' id='form_data' value=''/>
			<input type='hidden' id='post_defect' name='post_defect' value='N'/>
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
				<input type='button' id='btnBack' value='Back' class='imagebutton back'/>
				<%if(!defect.getPosted().equalsIgnoreCase("YES")){ %>
				 <input type='submit' class='imagebutton save' value='Save' id='btnSave' /> 
				 <%} %>
				<input type='button' id='btnDelete' value='Remove' class='imagebutton delete'/>
				
				<%
			if(Utilities.trim(defect.getPosted()).equalsIgnoreCase("yes")){
				%>
				<p class='subtitle'>This defect cannot be modified from Tenjin as it is already posted to Defect Management.</p>
				<%
			}
			%>
			</div>
			
			<div id='user-message'></div>
			<input type='hidden' id='pid' value='<%=project.getId() %>'/>
			<input type='hidden' id='defPosted' value='<%=defect.getPosted() %>'/>
			<div class='form container_16'>
				<fieldset>
					<legend>Basic Information</legend>
					<div class='fieldSection grid_8'>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='recordId'>Record ID</label>
						</div>
						<div class='grid_4'>
							<input type='text' id='recordId' name='recordId' class='stdTextBox' title='Record ID' value='<%=defect.getRecordId()%>' disabled/>
						</div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='recordId'>Action</label>
						</div>
						<div class='grid_4'>
							<input type='hidden' id='selAction' value='<%=defect.getTjnStatus() %>'/>
							<select id='action' name='action' class='stdTextBoxNew'>
								<%
									for(String key:actionsMap.keySet()){
										if(key.equalsIgnoreCase(defect.getTjnStatus())){
											%>
											<option value='<%=key %>' selected><%=actionsMap.get(key) %></option>
											<%
										}else{
											%>
											<option value='<%=key %>'><%=actionsMap.get(key) %></option>
											<%
										}
									}
								%>
							</select>
						</div>
					</div>
					<div class='fieldSection grid_16'>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtSummary'>Summary</label>
						</div>
						<div class='grid_13'>
							<input type='text' id='txtSummary' name='txtSummary' class='stdTextBox long' title='Summary' mandatory='yes' value="<%=defect.getSummary().replace("\"", "'")%>" maxlength='255'/>
						</div>
					
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='description'>Description</label>
						</div>
						
						<div class='grid_13'>
						<%if(defect.getDescription()==null){
							defect.setDescription("");
							}%>
							<textarea id='description' name='description' class='stdTextBox' style='width: 89%; height: 50px;' maxlength='4000'><%=defect.getDescription() %></textarea>
						</div>
					</div>	
				</fieldset>
				
				<div class='clear'></div>
				
				<fieldset>
					<legend>Tenjin Information (Read-Only)</legend>
					
					<div class='fieldSection grid_8'>
						<div class='grid_2'><label for='runid'>Run ID</label></div>
						<div class='grid_4'>
							<%
							String runDesc = "";
							if(defect.getRunId() == 0){
								runDesc = "General";
							}else{
								runDesc = Integer.toString(defect.getRunId());
							}
							%>
							<input type='text' id='runid' name='runId' class='stdTextBox' disabled value='<%=runDesc %>'/>
							
						</div>
						
						<div class='clear'></div>
						<!-- /*added by shruthi for TENJINCG-1224 starts*/ -->
						<div class='grid_2 show-for-all'><label for='runstatus'>Run Status</label></div>
						<div class='grid_4'>
							<input type='text' id='runstatus' name='runstatus' class='stdTextBox' disabled value='<%=defect.getRunId_status()%>'/>
						</div>
						<div class='clear'></div>
						<!-- /*added by shruthi for TENJINCG-1224 ends*/ -->
						
						<div class='grid_2'><label for='linkages'>Linked Tests</label></div>
						<div class='grid_4'>
						
							<%
							if(defect.getLinkages() != null && defect.getLinkages().size() > 1){
								%>
								<!-- Modified by Priynaka for TCGST-49 starts -->
								<input type='text' value='Multiple Tests' class='stdTextBox' disabled id='linkedTest' name='linkedTest' title='Linked Tests' style='min-width:161px;vertical-align:middle;'/>
								<!-- Modified by Priynaka for TCGST-49 ends -->
								<div style='display:inline;'>
									<input type='button' id='btnList' class='imagebutton template' style='background-position-x:8px;vertical-align:middle'/>
								</div>
								<%
							}else if(defect.getLinkages() != null && defect.getLinkages().size() > 0){
								%>
								<!-- Modified by Priynaka for TCGST-49 starts -->
								<input type='text' value='<%=defect.getLinkages().get(0).getTestCaseId() %> - <%=defect.getLinkages().get(0).getTestStepId() %>' class='stdTextBox' disabled id='linkedTest'  title='Linked Tests'name='linkedTest' style='min-width:161px;vertical-align:middle; ' />
								<div style='display:inline;'>
									<input type='button' id='btnList' class='imagebutton template' style='background-position-x:8px;vertical-align:middle'/>
								</div>
								<!-- Modified by Priynaka for TCGST-49 ends -->
								<%
							}else{
								%>
								<!-- Modified by Priynaka for TCGST-49 starts -->
								<input type='text' value='General' class='stdTextBox' disabled id='linkedTest' name='linkedTest' title='Linked Tests' style='vertical-align:middle;' />
								<!-- Modified by Priynaka for TCGST-49 ends -->
								<%
							}
							%>
							
						</div>
						
						
					</div>
					
					<div class='fieldSection grid_7'>
						
						<div class='grid_2'><label for='appname'>Application</label></div>
						<div class='grid_4'>
							<input type='text' id='appname' name='appname' class='stdTextBox' disabled value='<%=defect.getAppName() %>'/>
							<input type='hidden' id='appid' name='appid' class='stdTextBox' value='<%=defect.getAppId() %>'/>
							
						</div>
						<div class='clear'></div>
						
						<%
						String fLabel = Utilities.trim(defect.getApiCode()).length() > 0 ? "API" : "Function"; 
						String fValue = Utilities.trim(defect.getApiCode()).length() > 0 ? defect.getApiCode() : defect.getFunctionCode(); 
						%>
						
						<div class='grid_2'><label for='function'><%=fLabel %></label></div>
						<div class='grid_4'>
							<input type='text' id='function' name='function' class='stdTextBox' disabled value='<%=fValue %>'/>
						</div>
						
					</div>
				</fieldset>
				
				<div class='clear'></div>
				<fieldset>
						 <legend>Defect Management Information</legend> 
					<div class='fieldSection grid_8'>
						<div class='clear'></div>
						<div class='grid_2'><label for='dttname'>Instance</label></div>
						<input type='hidden' id='selDttInstance' value='<%=defect.getDttInstanceName() %>'/>
						<div class='grid_4'><input type='text' id='dttname' name='dttname' class='stdTextBox' disabled/></div>
						
						<div class='clear'></div>
					<!-- modified by shruthi for TCGST-43 starts -->
						<%
							if(defect.getPosted().equalsIgnoreCase("yes") || defect.getPosted().equalsIgnoreCase("y")){
								%>
						<div class='grid_2'><label for='identifier'>Identifier</label></div>
						<div class='grid_4'>
							<%
							String dttIdentifier = Utilities.trim(defect.getIdentifier());
							%>
							<input type='text' disabled id='identifier' name='identifier' value='<%=dttIdentifier %>' class='stdTextBox'/>
						</div>
						<%} %>
						<!-- modified by shruthi for TCGST-43 ends -->
						<div class='clear'></div>
						<div class='grid_2'><label for='severity'>Severity</label></div>
						<input type='hidden' id='selSeverity' value='<%=defect.getSeverity() %>'/>
						<div class='grid_4'>
							<%
							if(defect.getPosted().equalsIgnoreCase("yes") || defect.getPosted().equalsIgnoreCase("y")){
								%>
								<input type='text' id='severity' name='severity' class='stdTextBox' value='<%=defect.getSeverity() %>' disabled/>
								<%
							}else{
							%>
							<select id='severity' name='severity' class='stdTextBoxNew' mandatory='yes' title='Severity'>
								<option value='-1'>-- Select One --</option>
							</select>
							<%} %>
						</div>
						<div class='clear'></div>
						<div class='grid_2'><label for='priority'>Priority</label></div>
						<div class='grid_4'>
							<input type='hidden' id='selPriority' value='<%=defect.getPriority()%>'/>
							<%
							if(defect.getPosted().equalsIgnoreCase("yes") || defect.getPosted().equalsIgnoreCase("y")){
								%>
								<input type='text' id='priority' name='priority' class='stdTextBox' value='<%=defect.getPriority() %>' disabled/>
								<%
							}else{
							%>
							<select id='priority' name='priority' class='stdTextBoxNew' mandatory='yes' title='Priority'>
								<option value='-1'>-- Select One --</option>
							</select>
							<%} %>
						</div>
						
					</div>
					
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='dttproject'>Project</label></div>
						<input type='hidden' id='selDttProject' value='<%=defect.getEdmProjectName() %>'/>
						<div class='grid_4'><input type='text' id='dttproject' name='dttproject' class='stdTextBox' disabled/></div>
						<div class='clear'></div>
						<div class='grid_2'><label for='posted'>Posted</label></div>
						<div class='grid_4'>
							
							<input type='text' disabled id='posted' name='posted' value='<%=defect.getPosted() %>' class='stdTextBox'/>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'><label for='lstStatus'>Status</label></div>
						<div class='grid_4'>
						<input type='hidden' id='selStatus' value='<%=defect.getStatus() %>'/>
						<%
						if(defect.getPosted().equalsIgnoreCase("y") || defect.getPosted().equalsIgnoreCase("yes")){
							%>
							<input type='text' disabled class='stdTextBox' id='status' name='status' value='<%=defect.getStatus() %>'/>
							<%
						}else{
							%>
							<!-- modified by shruthi for TCGST-43 starts -->
							<select id='lstStatus' name='status' class='stdTextBoxNew' mandatory='yes' title='Status' value=''>
							<!-- modified by shruthi for TCGST-43 ends -->
								<option value='-1'>-- Select One --</option>
								<option value='New'>New</option>
								<option value='Open'>Open</option>
								<option value='In Progress'>In Progress</option>
								<option value='Fixed'>Fixed</option>
								<option value='Closed'>Closed</option>
							</select>
							<%
						}
						%>
						</div>
						<!-- Added by Pushpa for TENJINCG-1221 starts -->
						<%if(defect.getRepoOwner()!=null ){ %>
						<div class='clear'></div>
						<div class='grid_2'><label for='owner'>Owner</label></div>
						<div class='grid_4'>
							
							<input type='text' disabled id='owner' name='owner' value='<%=defect.getRepoOwner() %>' class='stdTextBox'/>
						</div>
						<%} %>
						<!-- Added by Pushpa for TENJINCG-1221 ends -->
					</div>
				</fieldset>
				<div class='clear'></div>
				<fieldset>
					<legend>Attachments</legend>
					<div class='fieldSection grid_16' id='attachments_block'>
						<div class='grid_6' id='add'>
							<%
							if(!defect.getPosted().equalsIgnoreCase("y") && !defect.getPosted().equalsIgnoreCase("yes")){
								%>
								<input type='file' id='txtFile' name='Attachment' multiple='multiple'  class='stdTextBox' />
								<%
							}
							%>
							
							<div id='selectedFiles' class='grid_16'>
								<div class='clear'></div>
								<%
								if(defect.getAttachments() != null){
									for(Attachment att:defect.getAttachments()){
										%>
										<div class='att_block grid_6'>
										<!-- Modified by Pushpalatha for TENJINCG-1203 starts -->
											<p class='grid_4'><a href='<%=att.getAttachmentUrl() %>'  recid='<%=att.getAttachmentRecId()%>'><%=att.getFileName() %></a></p>
										<!-- Modified by Pushpalatha for TENJINCG-1203 ends -->
											<%
											if(!defect.getPosted().equalsIgnoreCase("y") && !defect.getPosted().equalsIgnoreCase("yes")){
												%>
												<a href='#' class='selFile' recid='<%=att.getAttachmentRecId() %>' title='Click to Remove'>Remove</a>
												<%
											}
											%>
										</div>
										<%
									}
								}
								%>
							</div>
						</div>
					</div>
				</fieldset>
				
				<fieldset>
					<legend>Audit Information</legend>
					<div class='fieldSection grid_8'>
						<div class='clear'></div>
						<div class='grid_2'><label for='createdby'>Created By</label></div>
						<div class='grid_4'><input type='text' id='createdby' name='createdby' class='stdTextBox' disabled value='<%=defect.getCreatedBy() %>'/></div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='createdon'>Created On</label></div>
						<%
						SimpleDateFormat sdf = new SimpleDateFormat(TenjinConfiguration.getProperty("DATE_FORMAT"));
						String date = sdf.format(defect.getCreatedOn());
						%>
						<div class='grid_4'><input type='text' id='createdon' name='createdon' class='stdTextBox' disabled value='<%=date %>'/></div>
					</div>
				</fieldset>
			</div>
			
		</form>
		
		<div class='modalmask'></div>
	<div class='subframe' id='def_linkage_dialog' style='left:20%'>
		<div class='subframe_title'><p>Linked Tests</p></div>
		<div class='subframe_content'>
			<div class='subframe_single_message' id='def_linkages_message'>
				<p>The following tests are linked to this defect</p>
				<div id='dataGrid' style='margin-top:10px;max-height:400px;overflow:auto;display:block;'>
					<table id='linkageTable' class='bordered' style='width:100%;font-size:12px;font-weight:normal;'>
						<thead>
							<tr>
								<th>Test Case</th>
								<th>Test Step</th>
								<th>TDUIDs</th>
							</tr>
						</thead>
						<tbody>
							<%
							for(DefectLinkage link:defect.getLinkages()){
								%>
								<tr>
									<td><%=link.getTestCaseId() %></td>
									<td><%=link.getTestStepId() %></td>
									<%
									String tdUids = "";
									int count=1;
									for(String tdUid:link.getTdUids()){
										if(count==link.getTdUids().size()){
											tdUids = tdUids + tdUid;
										}else{
											tdUids = tdUids + tdUid + ", ";
										}
									}
									%>
									<td><%=tdUids %></td>
								</tr>
								<%
							}
							%>
						</tbody>
					</table>
				</div>
			</div>
			
		</div>
		<div class='subframe_actions' id='def_review_pr_actions2'>
			<input type='button' id='btnLClose' value='Close' class='imagebutton cancel' callback='undefined'/>
		</div>
	</div>
	<div class='subframe' id='def_review_progress_dialog'style='left:20%'>
		<div class='subframe_title'><p>Defect Status</p></div>
		<div class='highlight-icon-holder' id='processing_icon'>
				
			</div>
		<div class='subframe_content'>
			<div class='subframe_single_message' id='def_review_message'>
				<p></p>
			</div>
			<div id="progressBar" class="default" style='margin:0 auto;'><div></div></div>
		</div>
		<div class='subframe_actions' id='def_review_pr_actions' style='display:none;'>
			<input type='button' id='btnPClose' value='Close' class='imagebutton cancel' callback='undefined'/>
		</div>
	</div>
	</body>
</html>