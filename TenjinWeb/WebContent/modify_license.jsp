<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  modify_license.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India





<!--
/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
*/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<title>Insert title here</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script>
$(document).ready(function(){
	$('#loader').hide();
	
});
$(document).on('click',	'#btnrenew',	function() {
	
	if($('#licenseKey').val()==""){
		showMessage("License key is required to renewal license.", "error");
		return false;
	}	else{
		$('#loader').show();
	}
});
$(document).on('click',	'#btnBack',	function() {
	window.location.href = 'LicenseServlet?param=view';
});
$(document).on('click',	'#btnmodify',	function() {
	if($('#licenseKey').val()==""){
		showMessage("License key is required to modify license.", "error");
		return false;
	}else{
		$('#loader').show();
		clearMessages();
		var $licenseForm = $("<form action='LicenseServlet?param=modify' method='POST' />");
		$licenseForm.append($("<input type='hidden' id='systemId' name='systemId' value='"+$('#systemId').val()+"' />"));
		$licenseForm.append($("<input type='hidden' id='licenseKey' name='licenseKey' value='"+$('#licenseKey').val()+"' />"))
		//Modified by Priyanka for KTKCBS-8 starts
		$licenseForm.append($("<input type = 'hidden' id='csrftoken_form' name ='csrftoken_form' value='"+$('#csrftoken_form').val()+"'/>"))
       //Modified by Priyanka for KTKCBS-8 ends

		$('body').append($licenseForm);
		$licenseForm.submit();
	}	
});

</script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>

<div class='title'>
		<c:choose>
			<c:when test="${action =='renew'}">
				<p>Renew License</p>
			</c:when>
			<c:otherwise>
				<p>Modify License</p>
			</c:otherwise>
		</c:choose>
		
	</div>

	<form Action='LicenseServlet?param=renewal' method='POST'>
	<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<div class='toolbar'>
		<c:choose>
			<c:when test="${action =='renew'}">
				<input type="submit" id='btnrenew' value='Renew' class='imagebutton save' />
			</c:when>
			<c:otherwise>
				<input type="button" id='btnmodify' value='Modify' class='imagebutton save' />
			</c:otherwise>
		</c:choose>
		<input type='button' id='btnBack' value='Cancel' class='imagebutton cancel' />
			<img src='images/inprogress.gif' id='loader' style="padding-top:6px"/>
		</div>
		<input type='hidden' id='systemId' name="systemId" value='${systemId}' />
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${screenState.message }' />
		<div id='user-message'></div>
		<div class='form container_16'>
			<fieldset>
				<legend>Enter License key to Renewal/Modify the Existing License</legend>
				&nbsp;&nbsp; &nbsp;&nbsp;
				<div class='fieldSection grid_15'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstDomain'>System ID </label>
					</div>
					<div class='grid_12' style='width: 82%; display: inline; padding-right: 10px; overflow:auto; line-height: 2;'>
						<b>${systemId}</b>
					</div>
				<div class='clear'></div>
				<div class='grid_2'><label for='lstDomain'>License Key</label></div>
				<div class='grid_12'>
					<textarea class="stdTextBox" id="licenseKey" name="licenseKey" style="width: 774px;height: 38px;">${licenseKey}</textarea>
				</div>
				
				<div class='clear'></div>
				<div class='grid_14'>
					If you do not have the License key, please send an E-mail to <b>tenjin.support@yethi.in</b>
					with your System ID to request for one.
				</div>
				</div>
			</fieldset>
			
		
		</div>
	 
 </form>
</body>
</html>