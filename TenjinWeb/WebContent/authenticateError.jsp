<!-- /***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  authenticateError.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
*/
 -->
 <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.Map"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tenjin - Intelligent Enterprise Testing Engine</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/landing.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel='stylesheet' href='css/select2.min.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel="SHORTCUT ICON" HREF="images/yethi.png">

</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<div class='wrapper'>
		<div class='header'>
			<div class='header-left'>&nbsp</div>
			<div class='header-middle'>&nbsp</div>
			<div class='header-actions'></div>
			<div class='header-right'></div>
		</div>
		<div class='main'>
			<div class='container_16' id='main-block'>
				<div class='grid_1' style='height: 100%;'></div>
				<div class='grid_14' style='height: 100%;'>
					<div id='landing_image' class='grid_14'>
							<img src='images/tenjin_logo_new.png'
							alt='Tenjin V2.0 - Intelligent Enterprise Testing Engine'
							style='margin: 0 auto; display: block; height: 100%;' />
					</div>

					<div id='landing_action_block' class='grid_14'>
						<div class='grid_3' style='height: 100%;'></div>
						<div class='grid_7' style='height: 100%;'>
							<div id='landing_action_project_selection'
								style="width: 450px; height: 150px;">
								<input type='hidden' id='userId'
									value='<%=(String) request.getAttribute("userid") %>' /> <input
									type='hidden' id='terminal'
									value='<%=(String) request.getAttribute("ipAddress") %>' />

								<form>
									<%			
								    String userName=(String)request.getAttribute("userid");
									String ipAddress=(String)request.getAttribute("ipAddress");
										
											%>

									<div class='statusContainer'>
										<input type='hidden' id='userid' value='<%=userName %>' />
										<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
										<div class='errmsg' id="error">
											<br /> <span style="font-size: 14px;"><%=userName %>&nbspis
												already logged for machine &nbsp<%=ipAddress %></span> <br /> <br />
											<span style="font-size: 14px;"> <a
												href='TenjinSessionServlet?t=clear_session&paramval=<%=userName %>'>Click
													Here</a> to clear the existing session or go back to<a
												href="InitialLaunchServlet"> Login Page</a>&nbspto login
												with other user. <br /> <br /> <b>Note: </b>If you clear
												the existing session, any unsaved data may be lost.
											</span>

										</div>
									</div>
								</form>

							</div>
						</div>

					</div>
				</div>
			</div>

		</div>

		<div class='footer'>
			<div class='footer-left'>&nbsp</div>
			
			
			<div class='footer-middle'>&nbsp</div>
			<div class='footer-right'>&copy Copyright 2014 - <%=new java.text.SimpleDateFormat("yyyy").format(new java.util.Date()) %> <a href='http://www.yethi.in' target='_blank' rel="noopener">Yethi Consulting Pvt. Ltd.</a> All Rights Reserved
			</div>
			
		</div>
	</div>
</body>
</html>