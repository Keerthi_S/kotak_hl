<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_admin_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!-- Added by sahana for Req#TJN_24_03 on 14/06/2016 -->
<%@page import="com.ycs.tenjin.bridge.oem.api.generic.ApiLearnType"%>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="java.util.Map"%>
<%@ page
	import="java.util.Date,java.text.SimpleDateFormat,java.text.ParseException"%>
<%@page import="java.util.Date"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@page import="java.util.Calendar"%>
<%@ page import="java.util.Random"%>
<%@page import="java.util.List"%>
<%@page import="com.ycs.tenjin.client.RegisteredClient"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.scheduler.Scheduler"%>
<%@page import="com.ycs.tenjin.scheduler.SchMapping"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID"%>
<%@page import="com.google.common.cache.Cache"%>
<%@page import="com.google.common.cache.CacheBuilder"%>
<%@page import="java.util.concurrent.TimeUnit"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
     
*/

-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Schedule Job</title>
<link rel='stylesheet' href='css/bordered.css' />
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src="js/jquery-1.12.4.js"></script>
<script type='text/javascript' src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/formvalidator.js'></script>
<script type='text/javascript' src='js/pages/schedule_admin_view.js'></script>

<!-- <script type='text/javascript' src='js/pages/autfunclist_table.js'></script> -->

<script src="js/formvalidator.js"></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">


<script type="text/javascript">

$(document).ready(function(){

	<% 
	
	TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
	User cUser = tjnSession.getUser();
	Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");

	String scrStatus = "";
	
	String status = (String)map.get("STATUS");
	String message = (String)map.get("MESSAGE");
	Scheduler sch = (Scheduler)map.get("SCH_BEAN");
	
	ArrayList<RegisteredClient> clientList = null;
	clientList = (ArrayList<RegisteredClient>) map.get("CLIENT_LIST");
	
	ArrayList<SchMapping> schList =schList = (ArrayList<SchMapping>) map.get("SCH_Map_List");
	String tabType=(String)map.get("TABTYPE");
	
	%>
	document.getElementById("txtclientregistered").value ="<%=sch.getReg_client()%>";
	
});
</script>

</head>
<body>
	<%Cache<String, Boolean> csrfTokenCache = null;
csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
session.setAttribute("csrfTokenCache", csrfTokenCache);
UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
  session.setAttribute ("csrftoken_session", csrftoken);
  %>
	<div class='main_content'>
		<div class='title'>

			<p>Scheduled Job Details</p>
		</div>
		<form name='edit_schedule_admin' action='SchedulerServlet'
			method='get'>
			<input type="hidden" id="csrftoken_form" name="csrftoken_form"
				value=<%= csrftoken %> />

			<div class='toolbar'>
				<input type='button' value='Back' id='btnBack'
					class='imagebutton back' /> <input type='button' value='Save'
					id='btnSave' class='imagebutton save' /> <input type='button'
					value='Edit' class='imagebutton edit' id='btnEdit'
					style="display: none;" />
			</div>


			<div id='user-message'></div>

			<div class='form container_16'>
				<fieldset>

					<legend>Schedule Details</legend>
					<div class='fieldSection grid_16'
						style='margin-top: 5px; height: 28px'>
						<div class='grid_2'>
							<label for='taskName'>Task Name</label>
						</div>
						<div class='grid_13'>
							<%if(sch.getTaskName()==null) {
								sch.setTaskName("");
							}%>
							<input type='text' id="taskName" class='stdTextBox long'
								maxlength="40" value='<%=sch.getTaskName()%>' title='Task Name'
								style='width: 81.7%' />

						</div>
					</div>
					<div class='fieldSection grid_7'>

						<input type='hidden' value='<%=sch.getSchedule_Id() %>'
							id='btnSchId' name='btnSchId' />
						<%if(tabType!=null){ %>
						<input type='hidden' value='<%=tabType%>' id='tabselectedType'
							name='tabselectedType' />
						<%} %>
						<div class='grid_2'>
							<label for='datepicker'>Date</label>
						</div>
						<div class='grid_4'>

							<input type='text' id="datepicker"
								style='border: 1px solid #ccc; border-radius: 3px; padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 177px;'
								value='<%=sch.getSch_date()%>' size="25" mandatory='yes'
								title='Schedule Date' />

						</div>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='btntime'>Time</label>
						</div>
						<div class='grid_4'>
							<select id='btntime' name='btntime' class='stdTextBox'
								style='min-width: 60px; width: 60px'>
								<%String[] timeval=sch.getSch_time().split(":");
							%>
								<option value='<%=timeval[0] %>'><%=timeval[0] %></option>
							</select> &nbsp;&nbsp;<font for='btntime' color='#00008B'>(HH)</font>&nbsp;&nbsp;&nbsp;&nbsp;
							<select id='btntime1' name='btntime1' class='stdTextBox'
								style='min-width: 60px; width: 60px'>
								<option value='<%=timeval[1] %>'><%=timeval[1] %></option>
							</select> &nbsp;&nbsp;<font for='btntime1' color='#00008B'>(MM)</font>
						</div>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtTask'>Task</label>
						</div>
						<div class='grid_4'>

							<select id='txtTask' name='txtTask' mandatory='yes'
								class='stdTextBoxNew' title='Task' disabled='disabled'>
								<%if(sch.getAction().equalsIgnoreCase("learnapi")) {%>
								<option value='LearnAPI'>Learn APIs</option>
								<%}else if(sch.getAction().equalsIgnoreCase("learn")){ %>
								<option value='Learn'>Learn Functions</option>
								<%}else{%>
								<option value='Extract'>Extract</option>
								<%} %>
							</select>
						</div>

						<div class='clear gui-only'></div>
						<div class='grid_2 gui-only'>
							<label for='lstAppUserType'>Aut Login Type</label>
						</div>
						<div class='grid_4 gui-only'>
							<input type='hidden' value='<%=sch.getAutLoginTYpe() %>'
								id='currentAUT' name='currentAUT' /> <select
								class='stdTextBoxNew' id='lstAppUserType'
								title='AUT AppUserType' mandatory='yes'>

								<option value='<%=sch.getAutLoginTYpe() %>'><%=sch.getAutLoginTYpe() %></option>

							</select>
						</div>
						<div class='clear'></div>
						<div class='grid_2' id='lsttask'>
							<label for='lstApplication'>Application</label>
						</div>
						<div class='grid_4'>
							<select id='lstApplication' name='lstApplication' mandatory='yes'
								class='stdTextBoxNew' title='Application'>
								<%
								if(schList != null){
									for(SchMapping sm:schList){
								%>
								<option value='<%=sm.getApp_id() %>'><%=sm.getApp_id() %></option>

								<%
									}
								}
								%>
							</select>
						</div>

						<%if(tabType.equalsIgnoreCase("learnapi") || tabType.equalsIgnoreCase("Cancelled Tasks") || tabType.equalsIgnoreCase("Aborted Tasks") || tabType.equalsIgnoreCase("Completed Tasks") || tabType.equalsIgnoreCase("Scheduled Tasks"))
							/* Added by Gangadhar Badagi for T25IT-369,372,374,376 starts  */
						{%>

						<div class='apiBlock'>
							<div class='clear'></div>
							<div class='grid_2' id='lsttask'>
								<label for='lstAPICode'>API</label>
							</div>
							<div class='grid_4'>
								<%
								if(sch.getAction().equalsIgnoreCase("learnapi")) {
									String apiCode = (String) map.get("API_CODE");
									%>
								<input type='hidden' id='selApiCode' value='<%=apiCode %>' />
								<%
								}
								%>
								<select id='lstAPICode' name='lstAPICode' class='stdTextBoxNew'
									title='API Code' mandatory='yes'>

								</select>
							</div>
						</div>
						<% 	}%>
					</div>

					<div class='fieldSection grid_7'>
						<div class='clear'></div>

						<div class='grid_2 gui-only'>
							<label for='txtclientregistered'>Client</label>
						</div>
						<div class='grid_4 gui-only'>
							<select id='txtclientregistered' name='txtclientregistered'
								class='stdTextBoxNew'>
								<%
								if(clientList != null){
									for(RegisteredClient cl:clientList){
								%>
								<option value='<%=cl.getName() %>' selected='selected'><%=cl.getName() %></option>

								<%
									}
								}
								%>

							</select>

						</div>

						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtCrtBy'>Created By</label>
						</div>
						<div class='grid_4'>
							<input type='text' id='txtCrtBy' name='txtCrtBy'
								class='stdTextBox mid' value='<%=sch.getCreated_by() %>'
								disabled='disabled' title='Created By' />
						</div>

						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtCrtOn'>Created On</label>
						</div>
						<div class='grid_4'>
							<%
						String todaydate="";

						Calendar calendar1 = Calendar.getInstance();
						SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
						todaydate = dateFormat.format(calendar1.getTime());
						
						%>
							<input type='text' value='<%=sch.getCreated_on() %>'
								id='txtCrtOn' name='txtCrtOn' title='Created On' mandatory='no'
								disabled='disabled' class='stdTextBox mid' />
						</div>

						<div class='clear gui-only'></div>
						<div class='grid_2 gui-only'>
							<label for='lstBrowserType'>Browser</label>
						</div>
						<div class='grid_4 gui-only'>

							<input type='hidden' value='<%=sch.getBrowserType() %>'
								id='currentBrowser' name='currentBrowser' /> <select
								id='lstBrowserType' name='lstBrowserType' class='stdTextBoxNew'>

								<option value='APPDEFAULT'>Use Application Default</option>
								<option value='<%=BrowserType.CHROME %>'>Google Chrome</option>
								<!-- Added by paneendra for Firefox browser starts  -->
								<option value="<%=BrowserType.FIREFOX%>"><%=BrowserType.FIREFOX%></option>
								<!-- Added by paneendra for Firefox browser ends  -->
								<option value='<%=BrowserType.IE %>'>Microsoft Internet
									Explorer</option>
								<option value='<%=BrowserType.CE %>'><%=BrowserType.CE %></option>
							</select>
						</div>
						<div class='clear'></div>
						<!-- added by shruthi for TENJINCG-1232 starts-->
						<div class='grid_2'>
							<label for='scheduleType'>Schedule Type</label>
						</div>
						<div class='grid_4'>
							<select class='stdTextBoxNew' disabled='disabled'>
								<%if(sch. getSchRecur().equalsIgnoreCase("Y")) {%>
								<option>Recursive</option>
								<%}
								else{%>
								<option>Non-Recursive</option>
								<%} %>
							</select>
						</div>
						<div class='clear'></div>
						<!-- added by shruthi for TENJINCG-1232 ends-->

						<div class='apiBlock'>
							<div class='clear'></div>
							<div class='grid_2'>
								<label for='lstApiLearnType'>Learn Type</label>
							</div>
							<div class='grid_4'>
								<input type='hidden' id='selApiLearnType'
									value='<%=sch.getApiLearnType() %>' /> <select
									id='lstApiLearnType' name='lstApiLearnType'
									class='stdTextBoxNew'>

									<option value='<%=ApiLearnType.URL.toString() %>'>Learn
										from URL</option>
									<option value='<%=ApiLearnType.REQUEST_XML.toString() %>'>Learn
										from Request and Response XML</option>
									<option value='<%=ApiLearnType.REQUEST_JSON.toString() %>'>Learn
										from Request and Response JSON</option>
								</select>
							</div>
						</div>

						<div class='clear'></div>

						<div class='grid_4'>
							<input type='hidden' value='Scheduled' id='txtStatus'
								name='txtStatus' />

						</div>
					</div>

				</fieldset>
			</div>

		</form>
		<div class='form container_16' id='tasktype'>
			<input type='hidden' id='lstLrnrApplication'
				name='lstLrnrApplication' value='' /> <input type='hidden'
				id='lstLrntGroup' name='lstLrntGroup' value='' />
			<fieldset>
				<%
			if(sch.getAction().equalsIgnoreCase("learnapi")) {%>
				<legend>API(s)</legend>
				<%}else {%>
				<legend>Function(s)</legend>
				<%} %>
				<div class='clear'></div>

				<div class='clear'></div>
				<div class='fieldSection grid_15' id='modulesInfo'>
					<table id='tblNaviflowModules'
						class='display tjn-default-data-table' cellspacing='0'
						width='100%'>
						<div class='funclist' style='display: none;'>
							<%
							  String task = null;
								for(int i=0;i<schList.size();i++){
									
									if (sch != null && i == 0) {
										task = sch.getAction();
									}
									if (task.equalsIgnoreCase("Learn")) {
							%>
							<input type='checkbox' class='funccheckbox'
								value='<%=schList.get(i).getFunc_Code()%>' />
							<%
								} else if(task.equalsIgnoreCase("learnapi")) {
									%>
							<input type='checkbox' class='funccheckbox'
								value='<%=schList.get(i).getApiOperation() %>' />
							<%
								}else {
							%>
							<input type='checkbox' class='funccheckbox'
								value='<%=schList.get(i).getFunc_Code()%>' /> <input
								type='checkbox' class='filepathcheckbox'
								funcCode='<%=schList.get(i).getFunc_Code()%>'
								value='<%=schList.get(i).getExt_file_path()%>' />
							<%
								} 
								}
							%>
						</div>

						<tbody id='tblNaviflowModules_body'>
						</tbody>
					</table>
				</div>
			</fieldset>
		</div>
	</div>
</body>
</html>
