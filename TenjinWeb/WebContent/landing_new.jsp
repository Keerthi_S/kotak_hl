<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  landing_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->



<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.user.FailedLoginAttempts"%>
<%@page import="java.util.List"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Set"%>
<%@page import="com.google.common.collect.Multimap"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tenjin - Intelligent Enterprise Testing Engine</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/landing.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel='stylesheet' href='css/select2.min.css' />
<link rel='stylesheet' href='css/buttons.css' />

<link rel="SHORTCUT ICON" HREF="images/yethi.png">

<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>

<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
<script type='text/javascript' src='js/pages/landing.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/select2.min.js'></script>

<style>
.select2-selection--single {
	width: 250px;
}

.select2-container--default .select2-selection--single .select2-selection__arrow
	{
	left: 230px;
}
</style>

</head>
<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
		TenjinSession tjnSession = (TenjinSession) request.getSession()
				.getAttribute("TJN_SESSION");
		Map<String, Object> map = (Map<String, Object>) request
				.getSession().getAttribute("SCR_MAP");
		String prefrence=(String) map.get("prjPrefrence");
		/*Changed by prem for V2.8-4 starts*/
		String message="";
		String status="";
		if(!(map.get("STATUS1")==null)||!(map.get("MESSAGE1")==null)){
		 message=(String) map.get("MESSAGE1");
		 status=(String) map.get("STATUS1");
		}
		else{
			if(!(map.get("STATUS")==null)||!(map.get("MESSAGE")==null)){
				 message=(String) map.get("MESSAGE");
				 status=(String) map.get("STATUS");
				}
		}
 String license_notification = "";
	try{
		Map<String, Object> maplice = (Map<String, Object>) request.getSession().getAttribute("LICENSE_NOTIFICATION");
		license_notification=(String)maplice.get("MESSAGE");
	}
	catch (NullPointerException e ){
		license_notification = "";
	} 
 %>

	<div class='wrapper'>
		<div class='header'>
			<div class='header-left' style='width:270px;'>
				<img src='images/tenjin_logo.png'
					alt='Tenjin - Intelligent Testing Engine' style='width: 125px;' />
			</div>
			<div class='header-middle'>&nbsp</div>
			<div class='header-actions'>
				<a id='logout' name='logout' href='#' title='Logout'><img
					src='images/Logout.png' alt='Logout' /></a>
			</div>
			<div class='header-right'>
			<a  href='#' class='adminPanel' id='adminPanel' title='Click to view administration panel' ><img src="images/admin.png"> Administration</a>
				<a  href='#' id='logged-in-user' title='Click to View/Edit your profile ' ><%=tjnSession.getUser().getFullName()%></a>
				<%
					if (tjnSession.getUser().getLastLogin() != null
							&& !tjnSession.getUser().getLastLogin()
									.equalsIgnoreCase("")) {
				%>
				<div class='note'>
					Last Login:
					<%=tjnSession.getUser().getLastLogin()%>
				</div>
				<%
					}
				%>
			</div>
		</div>
		<div id='user-message'></div>
		<div class='main'>
			<div class='container_16' id='main-block'>
				<div class='grid_1' style='height: 100%;'></div>
				<div class='grid_14' style='height: 100%;'>
					<div id='landing_image' class='grid_14'>
							<img src='images/tenjin_logo_new.png'
							alt='Tenjin V2.0 - Intelligent Enterprise Testing Engine'
							style='margin: 0 auto; display: block; height: 100%;' />
					</div>
					<input type='hidden' name='prjPrefrer' value='<%=prefrence %>' id='prjprefer'  />
					<input type='hidden' id='tjn-status' value='<%=status %>' />
			        <input type='hidden' id='tjn-message' value='<%=message %>' />
					<div id='landing_action_block' class='grid_14'>
						<div class='grid_3' style='height: 100%;'></div>
						<div class='grid_7' style='height: 100%; padding-top:25px'>
							<div id='landing_action_project_selection'>
								<form name='LoadProjectForm' action='ProjectServlet'
									method='POST'>
									<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
									<div id='proj_selection_title'
										style="padding-bottom: 20px; font-weight: bold; text-align: center; font-size: 1em;">Select
										a Project</div>
									<div>
										<div
											style='width: 30%; text-align: right; float: left; display: inline; line-height: 2; padding-right: 10px;'>
											<label for='lstDomain'>Domain</label>
										</div>
										<div style='width: 50%; float: left; display: inline;'>
											<select id='lstDomain' name='lstDomain'
												style='min-width: 250px;'>
												<%
													Multimap<String, Object> projMap = (Multimap<String, Object>) map
															.get("PRJ_MAP");
													Collection<Object> projects = (Collection<Object>) map.get("PRJS");
													String activeDomain = "";
													if (projects != null && projects.size() > 0) {
														for (Object o : projects) {
															Project p = (Project) o;
															activeDomain = p.getDomain();
															break;
														}
													}
													if (projMap != null) {
														Set<String> keys = projMap.keySet();
														if (keys != null && keys.size() > 0) {
															for (String key : keys) {
																if (activeDomain != null
																		&& activeDomain.equalsIgnoreCase(key)) {
												%>
												<option value='<%=Utilities.escapeXml(key)%>' selected><%=Utilities.escapeXml(key)%></option>
												<%
													} else {
												%>
												<option value='<%=Utilities.escapeXml(key)%>'><%=Utilities.escapeXml(key)%></option>
												<%
													}
															}
														}
													}
												%>
											</select>
										</div>
									</div>
									<div style='height: 40px;'></div>
									<div>
										<div
											style='width: 30%; text-align: right; float: left; display: inline; line-height: 2; padding-right: 10px;'>
											<label for='lstProject'>Project</label>
										</div>
										<div style='width: 50%; float: left; display: inline;'>
											<select id='lstProject' name='lstProject'
												style='min-width: 250px;'>
												<%
													if (projects != null && projects.size() > 0) {
														for (Object o : projects) {
															Project p = (Project) o;
												%>
												<option value='<%=p.getId()%>'><%=Utilities.escapeXml(p.getName())%></option>
												<%
													}
													}
												%>
											</select>
										</div>
									</div>
									<div style='height: 40px;'></div>
									<div id='landing_buttons'>
										<input type='button' class='imagebutton ok' id='btnLoadProject' value='Load' />
									   <input type='button' class='imagebutton reset' id='btnReset' value='Clear' />
									
									</div>
									<div style='height: 10px;'></div>
									<div>
									
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' id='rememberPrj' name='rememberPrj'/>&nbsp;&nbsp;&nbsp;<b>Remember most recent project</b> 
									</div>
								</form>
							</div>
							
							<div><form name='admin-form' id='admin-form'
									action='AdminLaunchServlet' method='POST'>
									<input type="hidden" value="" name ="param" id="param"/>
									<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
									</form>
						</div>
	

							<%if (tjnSession.getUser().getFailedLoginAttempts() != null) {

				List<FailedLoginAttempts> failedAttempts = tjnSession.getUser()
						.getFailedLoginAttempts();
				int count = failedAttempts.size();

				if (failedAttempts != null && count > 0) {%>

							<style>
table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	text-align: left;
	padding: 8px;
	background-color: #f2f2f2
}
</style>


							<b><P align="center" style="color: 'red'">
									<%out.println("Number of failed attempts:" + count);%>
								</P></b>
							<table style="border: 1px solid black">


								<%for (FailedLoginAttempts failedLoginAttempts : failedAttempts) {%>
								<tr>
									<td><%=failedLoginAttempts.getIpAddress()%></td>
									<%String S = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
								.format(failedLoginAttempts.getTimeStamp());%>
									<td><%=S%></td>
								</tr>
								<%}%>
							</table>
							<%}
			}%>
						
							
						</div>

						<div class='grid_3' style='height: 100%;'></div>
					</div>
				</div>
				<div class='grid_1' style='height: 100%;'></div>
			</div>

			

		</div>
		<div class='footer'>
			
			<div class='footer-left'> 
			&nbsp;
			</div>
			<div class='footer-middle' style="padding-top: 7.5px">&nbsp;
				<b><font color="red"  style="align-items: center;font-size:10px"> <%=license_notification%></font></b>
		 	</div>
			<div class='footer-right'>
				&copy Copyright 2014 -
				<%=new java.text.SimpleDateFormat("yyyy")
					.format(new java.util.Date())%>
				<a href='http://www.yethi.in' target='_blank' rel="noopener">Yethi Consulting
					Pvt. Ltd.</a> All Rights Reserved
			</div>
			
		</div>
</div>


</body>
</html>