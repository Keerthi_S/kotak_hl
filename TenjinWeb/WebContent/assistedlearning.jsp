<!-- 

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:   assistedlearning.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 -->
 <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.TestObject"%>
<%@page import="com.google.common.collect.Multimap"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Assisted Learning</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel="Stylesheet" href="css/bordered.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/formvalidator.js'></script>
		<script type='text/javascript' src='js/pages/assistedlearning.js'></script>
		
		<style>
			#uploadBlock
			{
				display:none;
			}
		</style>
	</head>
	<body>
		<%
	    Cache<String, Boolean> csrfTokenCache = null;
	    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
	    session.setAttribute("csrfTokenCache", csrfTokenCache);
	    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
	    session.setAttribute ("csrftoken_session", csrftoken);
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			
			Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
			String status = (String)map.get("STATUS");
			String message = (String)map.get("MESSAGE");
			
			Multimap<String, TestObject> learningDataMap = (Multimap<String, TestObject>) map.get("LEARNING_DATA");
			
			if(learningDataMap == null){
				status = "ERROR";
				message = "Could not get assisted learning data for this function";
			}
			
			Aut aut = (Aut) map.get("AUT");
			String functionCode = (String) map.get("FUNC");
			/*Added by Padmavathi starts*/
			String learnStatus =(String)map.get("LEARN_STATUS");
		%>
		<input type="hidden" id="learnstatus" value="<%=learnStatus %>"/>
		<div class='title'>Assisted Learning Data</div>
		<form name='main_form' action='AssistedLearningDataServlet' method='POST' enctype='multipart/form-data'onsubmit= "return validate()">
		<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
				<input type='button' value='Back' class='imagebutton back' id='btnBack'/>
				<input type='button' value='Export to Excel' class='imagebutton download' id='btnDownload'/>
				<input type='button' value='Import from Excel' class='imagebutton upload' id='btnUpload'/>
			</div>
			<%
				if(status != null && status.equalsIgnoreCase("SUCCESS") && message != null && !message.equalsIgnoreCase("")){
					%>
					<div id='user-message' class='msg-success' style='display:block;'>
						<div class="msg-icon">&nbsp;</div>
						<div class="msg-text"><%=message %></div>
					</div>
					<%
				}else if(status != null && !status.equalsIgnoreCase("") && !status.equalsIgnoreCase("SUCCESS")){
					%>
					<div id='user-message' class='msg-err' style='display:block;'>
						<div class="msg-icon">&nbsp;</div>
						<div class="msg-text"><%=message %></div>
					</div>
					
					<%
				}else{
					%>
					<div id='user-message'></div>
					
					<%
				}
				%>
		
			<div class='form container_16'>
			
				<fieldset id='uploadBlock'>
					<legend>Import from Spreadsheet</legend>
					<div class='fieldSection grid_10'>
						<div class='grid_2'><label>Spreadsheet Path</label></div>
						<div class='grid_5'>
							<input type='file' id='txtFile' name='txtFile' class='stdTextBox'/>
							</br><span id="lblError" style="color: red;"></span>
							
						</div>
						<div class='grid_2'>
						
						 <input type='submit' value='Upload' id='btnUpload' class='imagebutton upload'/> 
							 						 
						</div>
					</div>
				</fieldset>
				</form>
			
				<fieldset>
					<legend>Application Information</legend>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='lstApplication'>Application</label></div>
						<div class='grid_4'>
							<select id='lstApp' name='lstApp' class='stdTextBox' disabled>
								<option value='<%=aut.getId() %>'><%=aut.getName() %></option>
							</select>
							<input type='hidden' id='appId' name='appId' value='<%=aut.getId() %>'/>
							<input type='hidden' id='func' name='func' value='<%=functionCode %>'/>
						</div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_1'><label for='txtFunc'>Function</label></div>
						<div class='grid_5'>
							<input type='text' id='txtFunc' name='txtFunc' value='<%=functionCode %>' class='stdTextBox' disabled/>
						</div>
					</div>
				</fieldset>
				
				<fieldset>
					<legend>Assisted Learning Data</legend>
					<div class='fieldSection grid_15'>
						<table class='bordered' width="100%"  cellspacing='0'>
							<thead>
								<tr>
									
									<th class='tiny'>Sequence</th>
									<th class='tiny'>Page</th>
									<th class='tiny'>Field</th>
									<th class='tiny'>Identified By</th>
									<th>Data</th>
								</tr>
							</thead>
							<tbody>
								<%
								int sequence=1;
								if(learningDataMap != null){
									for(String key:learningDataMap.keySet()){
										for(TestObject t:learningDataMap.get(key)){
											%>
											<tr>
												
												
												<td class='tiny'><%=sequence++ %></td>
												<td><%=key %></td>
												<td><%=t.getLabel() %></td>
												<td class='tiny'>
													<%=t.getIdentifiedBy() %>
												</td>
												<td><%=t.getData() %></td>
											</tr>
											<%
										}
									}
								}
								%>
							</tbody>
						</table>
					</div>
				</fieldset>
				
			</div>
		</form>
	</body>
</html>