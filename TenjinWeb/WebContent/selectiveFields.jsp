<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  selectiveFields.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<%@page import="com.itextpdf.text.log.SysoCounter"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--
/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 
*/

-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<!-- <title>FieldSelector</title> -->
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel="stylesheet" href="jstree/themes/default/style.min.css" />
		<link rel="SHORTCUT ICON" HREF="images/yethi.png">

 <script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type="text/javascript" src="js/formvalidator.js"></script>
<script type="text/javascript" src="js/pages/selectiveFields.js"></script>
<script src="jstree/jstree.min.js"></script>

<title>Custom Test Data Template - Tenjin</title>
</head>


<body>
     <%
     Cache<String, Boolean> csrfTokenCache = null;
     csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
     session.setAttribute("csrfTokenCache", csrfTokenCache);
     UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
     
        TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
         if(tjnSession!=null){
     %>

<div class='title'>Learnt Fields</div>
	<%
	String appId = request.getParameter("a");
	String funcCode = request.getParameter("m");
	String txnMode = request.getParameter("t");
	
	String operation = request.getParameter("o");
	%>
		<input type='hidden' id='txnMode' value='<%=txnMode %>'/>
  	<div class='form container_16'>
			<div class='fieldSection grid_16'>
			
			<form id='selectiveFieldsform' >
				<input type='hidden' name='userId' value=''/>
				<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
					<div class='toolbar'>
						<input type='button' id='createTemplate' value='Create Template' class='imagebutton template'/>
					 	<input type='button' id='btnRefresh' value='Refresh' class='imagebutton refresh'/> 
					</div>	
			</form>

			<div id='user-message'></div>	
			<div class='grid_2' id='mandatoryField'><label for='mandatorySelector' style='display: block;padding-left:9.4px;text-indent: -15px;'>Mandatory Fields &nbsp&nbsp</label></div>
			<input type='checkbox' name='mandatorySelector' id='mandatorySelector'style='vertical-align: bottom;margin-top:7px;'/>
			
			<input type='hidden' id='app' value='<%=appId %>'/>
			<input type='hidden' id='func' value='<%=funcCode %>'/>
			<input type='hidden' id='operation' value='<%=operation %>' />
			<div id="jstree_demo_div">
 				  <div class='clear'></div>
 		  	</div>
	</div>
</div>
	
	<%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>
	
</body>
</html>