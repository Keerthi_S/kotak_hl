<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  selectiveFieldsInput.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.db.MetaDataHelper"%>
<%@page import="com.ycs.tenjin.util.Constants"%>
<%@page import="com.ycs.tenjin.db.DatabaseHelper"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
     <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--
/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->

<head>

<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/bordered.css'/>
<link rel='stylesheet' href='css/select2.min.css'/>
<link rel='stylesheet' href='css/960_16_col.css' />
		<link rel="SHORTCUT ICON" HREF="images/yethi.png"/>
<link rel="stylesheet" href="jstree/themes/default/style.min.css" />

 <script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type="text/javascript" src="js/formvalidator.js"></script>
<script type="text/javascript" src="js/pages/selectiveFieldsInput.js"></script>
<script src="jstree/jstree.min.js"></script>

<title>Custom Test Data Template - Tenjin</title>

</head>
<body>	<%
Cache<String, Boolean> csrfTokenCache = null;
csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
session.setAttribute("csrfTokenCache", csrfTokenCache);
UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);

		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		String appId = request.getParameter("a");
		String funcCode = request.getParameter("m");
		%>
		<div class='title'>
			<p>Custom Template Generation</p>
		</div>
		<form action='ExecutionSchemeServlet' name='new_exec_scheme' id='new_exec_scheme' method='POST'>
			<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
			 <input type='reset' value='Reset' class='imagebutton reset' id='btnReset'/>
			</div>
			<div class='form container_16'>
				<fieldset>
					<legend>Basic Information</legend>
					<div class='fieldSection grid_10'>
						<div class='clear'></div>
						<div class='grid_2'><label for='lstApplication' style='text-align:right;'>Application Id</label></div>
						<div class='grid_7'>
							<select id='lstApplication' name='lstApplication' class='stdTextBox'>
								
							</select>
						</div>
						<div class='clear'></div>
						<div class='grid_2'><label for='lstModules' style='text-align:right;'>Module Code</label></div>
						<div class='grid_4'>
							<select id='lstModules' name='lstModules' title='Function' class='stdTextBox' style='max-width:200px;width:200px;text-overflow:ellipsis;' mandatory='yes'>
								
							</select>
						</div>
					
						
						<div class='clear'></div>
						<div class='grid_2'></div>
						<div class='grid_4'>
							<input type='button' value='Search' class='imagebutton search' id='btnSearch'/>
							
						</div>
					</div>
				</fieldset>
		</div>
	</form>
	
		<div class='modalmask'></div>
			<div class='subframe'>
			<iframe frameborder="2" scrolling="yes"  marginwidth="5" marginheight="5" style='height:400px;width:600px;' seamless="seamless" id='ttd-options-frame' src=''></iframe>
			</div>
			
</body>
</html>