<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  compare.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


 
<%@page import="com.ycs.tenjin.project.TestStep"%>
<%@page import="com.ycs.tenjin.bridge.pojo.run.ValidationResult"%>
<%@page import="com.ycs.tenjin.bridge.pojo.run.ResultValidation"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel="Stylesheet" href="css/bordered.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="js/formvalidator.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<title>Swift Message Validation</title>
	</head>
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
<style type="text/css">

.actdiff {
	background-color: rgb(244, 26, 216);
}
.expdiff {
	background-color: yellow;
}
#actFiletable,#expFiletable {
	border-color: black;
	border-style: solid;
	width: 100%;
}

body {
	font-size: 12px;
	font-family: Sans-Serif;
}

h2 {
	margin: 0.5em 0 0.1em;
	text-align: center;
}

.compareInput {
	display: block;
	width: 49%;
	float: left;
}

.textInputExpFile {
	display: block;
	width: 30%;
	float: left;
	margin-left:20%;
}
.textInputActFile {
	display: block;
	width: 15%;
	margin-left:98%;
}
.spacer {
	margin-left: 10px;
}

}
</style>
<script type="text/javascript">
$(document).ready(function () {
	<%	
		Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("FILE_DETAILS");
		Map<Integer, String> bodyBlkExpFile = (Map<Integer, String>) request.getSession().getAttribute("BODYEXP");
		Map<Integer, String> bodyBlkActFile = (Map<Integer, String>) request.getSession().getAttribute("BODYACT");
		String[] headerActFile = (String[]) request.getSession().getAttribute("HEADERACT");
		String[] headerExpFile = (String[]) request.getSession().getAttribute("HEADEREXP");
		
		ArrayList<Integer> diffheader  = (ArrayList<Integer>) map.get("HEADERDIFF");
		ArrayList<Integer> diffbody    = (ArrayList<Integer>) map.get("BODYDIFF");		
		ArrayList<Integer> bodyindx    = (ArrayList<Integer>) map.get("BODYINDEX");
		Integer headerindx      	   = (Integer) map.get("HEADERINDEX");
		String msgType = (String) map.get("MSGTYPE");

		%>

		var diffheader = <%=diffheader%>;
		var diffbody = <%=diffbody%>;
		var bodyindx = <%=bodyindx%>;
		var headerindx = <%=headerindx%>;
		var msgType = "<%=msgType%>";
		
		var $actFiletable = $("<table id='actFiletable'/>");
		var $expFiletable = $("<table id='expFiletable'/>");

		<%
		for(int i=0;i<headerindx-1;i++){
		%>
		
			var headerExpCont = "<%=headerExpFile[i]%>";
			var headerActCont = "<%=headerActFile[i]%>";
			
			if(diffheader[<%=i%>] === -1){
			
			
				if(headerExpCont.trim().length === 0  && headerActCont.trim().length != 0){
					
					var $expFiletr = $("<tr />").append("<span class='expdiff'><br/></span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span>"+headerActCont+"</span>");			
					$actFiletable.append($actFiletr);
				}
				else if (headerExpCont.trim().length != 0  && headerActCont.trim().length === 0){
				
					var $expFiletr = $("<tr />").append("<span>"+headerExpCont+"</span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span class='actdiff'><br/></span>");			
					$actFiletable.append($actFiletr);
				}
				
				else {
					var $expFiletr = $("<tr />").append("<span>"+headerExpCont+"</span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span>"+headerActCont+"</span>");			
					$actFiletable.append($actFiletr);
				}				
			}			
			else if(diffheader[<%=i%>] === 0){
				
				if(headerExpCont.trim().length === 0  && headerActCont.trim().length != 0){
					
					var $expFiletr = $("<tr />").append("<span class='expdiff'><br/></span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span>"+headerActCont+"</span>");			
					$actFiletable.append($actFiletr);
				}
				else if (headerExpCont.trim().length != 0  && headerActCont.trim().length === 0){
				
					var $expFiletr = $("<tr />").append("<span class='expdiff'>"+headerExpCont+"</span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span class='actdiff'><br/></span>");			
					$actFiletable.append($actFiletr);
				}
				
				else {
					var $expFiletr = $("<tr />").append("<span class='expdiff'>"+headerExpCont+"</span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span class='actdiff'>"+headerActCont+"</span>");			
					$actFiletable.append($actFiletr);
				}
				
			}
			else {
				if(headerExpCont.trim().length === 0  && headerActCont.trim().length != 0){
					
					var $expFiletr = $("<tr />").append("<span class='expdiff'><br/></span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span>"+headerActCont.slice(0,diffheader[<%=i%>])+"</span>").append("<span class='actdiff'>"+headerActCont.slice(diffheader[<%=i%>],headerActCont.length)+"</span>");			
					$actFiletable.append($actFiletr);
				}
				else if (headerExpCont.trim().length != 0  && headerActCont.trim().length === 0){
				
					var $expFiletr = $("<tr />").append("<span>"+headerExpCont.slice(0,diffheader[<%=i%>])+"</span>").append("<span class='expdiff'>"+headerExpCont.slice(diffheader[<%=i%>],headerExpCont.length)+"</span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span class='actdiff'><br/></span>");			
					$actFiletable.append($actFiletr);
				}
				
				else {
					var $expFiletr = $("<tr />").append("<span>"+headerExpCont.slice(0,diffheader[<%=i%>])+"</span>").append("<span class='expdiff'>"+headerExpCont.slice(diffheader[<%=i%>],headerExpCont.length)+"</span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span>"+headerActCont.slice(0,diffheader[<%=i%>])+"</span>").append("<span class='actdiff'>"+headerActCont.slice(diffheader[<%=i%>],headerActCont.length)+"</span>");			
					$actFiletable.append($actFiletr);	
				}
			}
			
		<%}%>		

		if(msgType === "DESC"){
			
			var $expFiletr = $("<tr />").html("----------------------------------Message Text----------------------------------");
			$expFiletable.append($expFiletr);
			var $actFiletr = $("<tr />").html("----------------------------------Message Text----------------------------------");
			$actFiletable.append($actFiletr);
		}
		else{
			var $expFiletr = $("<tr />").html("{4:");
			$expFiletable.append($expFiletr);
			var $actFiletr = $("<tr />").html("{4:");
			$actFiletable.append($actFiletr);
		}
				
		<%
		for(int i=0;i<bodyindx.size();i++){
		%>
		
			var bodyExpCont = "<%=bodyBlkExpFile.get(bodyindx.get(i))%>";
			var bodyActCont = "<%=bodyBlkActFile.get(bodyindx.get(i))%>";
			
			if(diffbody[<%=i%>] === -1){
				
				if(bodyExpCont.trim().length === 0  && bodyActCont.trim().length != 0){
					
					var $expFiletr = $("<tr />").append("<span class='expdiff'><br/></span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span>"+bodyActCont+"</span>");			
					$actFiletable.append($actFiletr);
				}
				else if (bodyExpCont.trim().length != 0  && bodyActCont.trim().length === 0){
				
					var $expFiletr = $("<tr />").append("<span>"+bodyExpCont+"</span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span class='actdiff'><br/></span>");			
					$actFiletable.append($actFiletr);
				}
				
				else {
					var $expFiletr = $("<tr />").append("<span>"+bodyExpCont+"</span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span>"+bodyActCont+"</span>");			
					$actFiletable.append($actFiletr);
				}
				

			}			
			else if(diffbody[<%=i%>] === 0){
				
				if(bodyExpCont.trim().length === 0  && bodyActCont.trim().length != 0){
					
					var $expFiletr = $("<tr />").append("<span class='expdiff'><br/></span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span class='actdiff'>"+bodyActCont+"</span>");			
					$actFiletable.append($actFiletr);
				}
				else if (bodyExpCont.trim().length != 0  && bodyActCont.trim().length === 0){
				
					var $expFiletr = $("<tr />").append("<span class='expdiff'>"+bodyExpCont+"</span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span class='actdiff'><br/></span>");			
					$actFiletable.append($actFiletr);
				}
				
				else {
					var $expFiletr = $("<tr />").append("<span class='expdiff'>"+bodyExpCont+"</span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span class='actdiff'>"+bodyActCont+"</span>");			
					$actFiletable.append($actFiletr);
				}
			}
			else {
				
				if(bodyExpCont.trim().length === 0  && bodyActCont.trim().length != 0){
					
					var $expFiletr = $("<tr />").append("<span class='expdiff'><br/></span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span>"+bodyActCont.slice(0,diffbody[<%=i%>])+"</span>").append("<span class='actdiff'>"+bodyActCont.slice(diffbody[<%=i%>],bodyActCont.length)+"</span>");			
					$actFiletable.append($actFiletr);
				}
				else if (bodyExpCont.trim().length != 0  && bodyActCont.trim().length === 0){
				
					var $expFiletr = $("<tr />").append("<span>"+bodyExpCont.slice(0,diffbody[<%=i%>])+"</span>").append("<span class='expdiff'>"+bodyExpCont.slice(diffbody[<%=i%>],bodyExpCont.length)+"</span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span class='actdiff'><br/></span>");			
					$actFiletable.append($actFiletr);
				}
				
				else {
					var $expFiletr = $("<tr />").append("<span>"+bodyExpCont.slice(0,diffbody[<%=i%>])+"</span>").append("<span class='expdiff'>"+bodyExpCont.slice(diffbody[<%=i%>],bodyExpCont.length)+"</span>");
					$expFiletable.append($expFiletr);
					var $actFiletr = $("<tr />").append("<span>"+bodyActCont.slice(0,diffbody[<%=i%>])+"</span>").append("<span class='actdiff'>"+bodyActCont.slice(diffbody[<%=i%>],bodyActCont.length)+"</span>");			
					$actFiletable.append($actFiletr);	
				}

			}
			
		<%}%>			
							
		$("#actFilediff").append($actFiletable);
		$("#expFilediff").append($expFiletable);
		
	});
</script>

	<body>
     <%
        TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
         if(tjnSession!=null){
     %>
         
	
		<div class='title'>
			Swift Message Validation
		</div>
		<div class='form container_16'>
			<div class="textInputExpFile ">
				<h2>Expected File</h2>			
			</div>
			<div class="textInputActFile">
				<h2>Actual File</h2>			
			</div>
		</div>	
		<div>
			<div id="expFilediff" class="compareInput spacer"></div>
			<div id="actFilediff" class="compareInput spacer"></div>
		</div>
	<%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>


		
	</body>
</html>