<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  runinit.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


<%@page import="com.ycs.tenjin.device.RegisteredDevice"%>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.client.RegisteredClient"%>
<%@page import="com.ycs.tenjin.project.TestCase"%>
<%@page import="com.ycs.tenjin.project.TestStep"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.project.TestSet"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 
*/

-->
	<head>
		<title>Run Test - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel="Stylesheet" href="css/bordered.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="js/formvalidator.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/runinit.js'></script>
		<script  type='text/javascript' src="js/tables.js"></script>
	</head>
	<body>
	
	
		<%
		
		Cache<String, Boolean> csrfTokenCache = null;
		csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
		session.setAttribute("csrfTokenCache", csrfTokenCache);
		UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
		session.setAttribute ("csrftoken_session", csrftoken);
		
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			Project project = null;
			project = tjnSession.getProject();
			
			boolean canMakeChanges = false;
			
			
			if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){
				canMakeChanges = true;
			}
			
			
			
			Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
			String status = (String)map.get("STATUS");
			String message = (String)map.get("MESSAGE");
			TestSet testSet = (TestSet)map.get("TEST_SET");
			String callback=(String)map.get("CALL_BACK");
			String stepRecId=(String)map.get("STEP_REC_ID");
			String tcRecId=(String)map.get("TESTCASE_ID");
			String backStatus=(String)map.get("Invoke_By");
			String entity_id=(String)map.get("ENTITY_ID");
			String entity_name=(String)map.get("ENTITY_NAME");
			
			ArrayList<RegisteredClient> clientList = null;
			clientList = (ArrayList<RegisteredClient>) map.get("CLIENT_LIST");
		 
		%>
		
		<div class='title'>
			<p>Run Information</p>
			<div id='prj-info'>
				<span style='font-weight:bold'>Domain: </span>
				<span><%=project.getDomain() %></span>
				<span>&nbsp&nbsp</span>
				<span style='font-weight:bold'>Project: </span>
				<span><%=project.getName() %></span>
			</div>
		</div>
	
		<form name='main_form' action='ExecutorServlet' method='POST'>
			<div class='toolbar'>
			 <input type='button' class='imagebutton back' value='Back' id='btnBack' />
			 
				<input type='hidden' id='entity_id' name='entity_id' value='<%=entity_id %>'>
				<input type='hidden' id='entity_name' name='entity_name' value='<%=entity_name %>'>
			 
				<input type='hidden' id='set_mode' name='set_mode' value='<%=testSet.getMode() %>'>
				<input type='submit' value='Run Test' class='imagebutton runtestset' id='btnRun'/>
				<input type='hidden' value='<%=backStatus %>'  id='backStatus'/>
				<input type='hidden' value=''  id='client' name='client'/>
				 
				<input type='hidden' value='<%=callback %>'  id='callback'/>
				<input type='hidden' value='<%=stepRecId %>'  id='stepRecId'/>
				<input type='hidden' value='<%=tcRecId %>'  id='tcRecId'/>
			 	<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			</div>
			<%
			if(status != null && status.equalsIgnoreCase("SUCCESS")){
				%>
				<div id='user-message' class='msg-success' style='display:block;'>
					<div class="msg-icon">&nbsp;</div>
					<div class="msg-text"><%=message %></div>
				</div>
				<%
			}else if(status != null && !status.equalsIgnoreCase("") && !status.equalsIgnoreCase("SUCCESS")){
				%>
				<div id='user-message' class='msg-err' style='display:block;'>
					<div class="msg-icon">&nbsp;</div>
					<div class="msg-text"><%=message %></div>
				</div>
				
				<%
			}else{
				%>
				<div id='user-message'></div>
				
				<%
			}
			%>
			
			<div class='form container_16'>
				
				<%-- <input type='hidden' id='executionMode' name='executionMode' value='<%=testSet.getMode() %>' /> --%>
				<fieldset>
				<legend>Test Set Details</legend>
				<div class='fieldSection grid_16'>
					<input type='hidden' id='txtTsRecId' value='<%=testSet.getId() %>' name='txtTsRecId'/>
					<div class='clear'></div>
					<div class='grid_2'><label for='txtTestSetId'>Test Set ID</label></div>
					<div class='grid_4'>
						 
						<%
						if(testSet.getId() > 0){
							%>
							<input type='text' id='txtTestSetId' name='txtTestSetId' disabled='disabled' class='stdTextBox info-holder' value='<%=testSet.getId()%>'/>
							<input type='hidden' id='txtCreateTestSet' name='createTestSet' value='no'/>
							<%
						}else{
							%>
							<input type='text' id='txtTestSetId' name='txtTestSetId' disabled='disabled' class='stdTextBox info-holder' value='TBD'/>
							<input type='hidden' id='txtCreateTestSet' name='createTestSet' value='yes'/>
							<%
						}
						%>
					</div>
					
					<div class='clear'></div>
					<div class='grid_2'><label for='txtTestSetName'>Name</label></div>
					<div class='grid_13'>
						<input type='text' id='txtTestSetName' name='txtTestSetName' value='<%=testSet.getName() %>' disabled='disabled' class='stdTextBox info-holder long' mandatory='yes' title='Name'/>
					</div>
					 
				</div>
				</fieldset>
				<%
				/* if(testSet.getMode().equalsIgnoreCase("API")){
					
				}else{ */
					%>
					<fieldset>
						<legend>Settings</legend>
						<div class='fieldSection grid_8'>
							
							
					
						
							
							<div class='clear'></div>
							<div class='grid_2'><label for='txtclientregistered'>Client</label></div>
							<div class='grid_4'>
							<%
							
							String ipAddress=request.getRemoteAddr();
                        	String hostName=request.getRemoteHost();
							
                        	boolean flag=false;
                        		String clientName=null;
                        		String host=null;
                        		String deviceFarmCheck=null;
							if (clientList.size() > 0) { %>
								<select id='txtclientregistered' name='txtclientregistered' class='stdTextBox'>
								<%for (RegisteredClient rc : clientList)  { 
								
									if(ipAddress.equals(rc.getHostName())|| hostName.equals(rc.getHostName()))
                    				{
                    					flag=true;
                    					clientName=rc.getName();
                    					host=rc.getHostName();
                    					deviceFarmCheck=rc.getDeviceFarmCheck();
                    					break;
                    				}
								}
								
								if(flag){
								
								%>
									 
                                  	<option data_deviceFarm='<%=deviceFarmCheck%>' host='<%=host%>' value='<%=host%>'><%=clientName%></option>
                                   	<% }else
                                   		{
                                   	%>
											<option  value='-1'>---Select One---</option>
									<%  } 
									for (RegisteredClient rc : clientList)  {
										if(Utilities.trim(rc.getName()).equalsIgnoreCase(Utilities.trim(clientName)))  
										{
											
										}
										else
										{
											if(rc.getDeviceFarmCheck()!=null && rc.getDeviceFarmCheck().equalsIgnoreCase("Y")){
									%>
										 
												<option data_deviceFarm='<%=rc.getDeviceFarmCheck()%>' host='<%=rc.getHostName()%>' value='<%=rc.getHostName()%>'><%=rc.getName()%>&nbsp;[Device Farm]</option>
											<%}else{ %>
                                				<option data_deviceFarm='<%=rc.getDeviceFarmCheck()%>' host='<%=rc.getHostName()%>' value='<%=rc.getHostName()%>'><%=rc.getName()%></option>
									<% 	}
									}
									}
									%>
								</select>	
							<%} 
							%>		
							 
							</div>  
							
							 
					<div class='clear'></div>
					
					<div id="lstDevice">
					<div class='grid_2'>
						<label for='listDevice'>Device</label>
					</div>
					<div class='grid_4'>
						<select id='listDevice' name='listDevice' class='stdTextBox'>
						<option value='-1'>---Select One---</option>
						 
						</select>
					</div>
					</div>
	 		
							 	<div class='clear'></div>
							<div class='grid_2'><label for='Screen'>Screenshot</label></div>
							<div class='grid_4'>
								<%
								String scrShotOption = tjnSession.getProject().getScreenshotoption();
								%>
							   <input type='hidden' id='defaultScreenshotOption' value='<%=scrShotOption %>'/>
								<select id='Screen' name='Screen' class='stdTextBox'>
									<option value='0'>Never Capture</option>
									<option value='1' selected>Capture Only for Failures / Errors</option>
									<option value='2'>Capture For All Messages
									<option value='3'>Capture At the End of Each Page</option>
								</select>
							</div>    
		 
						 
							<input type='hidden' id='videoCaptureFlag' name='videoCaptureFlag' value='N'/>
						</div>
						<div class='fieldSection grid_7'>
							<div class='clear'></div>
							<div class='grid_2'><label for='lstBrowserType'>Browser</label></div>
							<div class='grid_4'>
								<select id='lstBrowserType' name='lstBrowserType' class='stdTextBox'>
									<option value='APPDEFAULT'>Use Application Default</option>
									<option value='<%=BrowserType.CHROME %>'><%=BrowserType.CHROME %></option>
									<!-- Added by paneendra for Firefox browser starts  -->
								   <option value="<%=BrowserType.FIREFOX%>"><%=BrowserType.FIREFOX%></option>
								   <!-- Added by paneendra for Firefox browser ends  -->
								<!-- Modified by Prem for TENJINCG-1246 starts -->
									<option value='<%=BrowserType.IE %>'><%=BrowserType.IE %></option>
									  <option value='<%=BrowserType.CE %>'>Microsoft Edge</option>
									 <!--  Modified by Prem for TENJINCG-1246 ends -->
								</select>
							</div>  
							 <div class='clear'></div>
						 
						</div>
					</fieldset>
					<%
				/* } */
				%>
			<fieldset id='deviceFilter'>
				<legend>Device Filters Information</legend>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstOs'>Operating System</label>
					</div>
					<div class='grid_4'>
						<select id='lstOs' name='lstOs' class='stdTextBox'>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstOEM'>OEM</label>
					</div>
					<div class='grid_4'>
						<select id='lstOEM' name='lstOEM' class='stdTextBox'>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstScrnSize'>Screen Size</label>
					</div>
					<div class='grid_4'>
						<select id='lstScrnSize' name='lstScrnSize' class='stdTextBox'>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstNetwork'>Network</label>
					</div>
					<div class='grid_4'>
						<select id='lstNetwork' name='lstNetwork' class='stdTextBox'>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
				 
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'></div>
					<div class='grid_4'>
						<input type="button" id="btnSearchFilter" value="Go"
							class="imagebutton ok">
							<input type='button' id='btnReset' value='Reset' class='imagebutton reset'/>
					</div>
				</div>
			</fieldset>
				
				<fieldset>
					<legend>Tests</legend>
					<p style='margin-top:10px;margin-bottom:10px;'>The following tests will be included as part of this Test Set.</p>
					<div id='dataGrid'>
						<table class='display tjn-default-data-table'>
							<thead>
								<tr>
									<th class='tiny nosort'>#</th>
									<th class='nosort'>Test Case ID</th>
									<th class='nosort'>Name</th>
								</tr>
							</thead>
							<tbody>
								<%
								ArrayList<TestCase> steps = testSet.getTests();
								if(steps != null && steps.size() > 0){
									int counter=0;
									for(TestCase step:steps){
										counter++;
									%>
										<tr>
											<td class='tiny' id='<%=step.getTcRecId()%>'><%=counter %></td>
											<td><%=step.getTcId() %></td>
											<td><%=step.getTcName() %></td>
										</tr>
									<%
									}
								}else{
									%>
									<tr>
										<td colspan='3'>No test cases are mapped to this Test Set  yet. You can start mapping Test Cases now.</td>
									</tr>
									<%
								}
								%>
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<input type='hidden' id='moduleList' name='moduleList' value=''/>
			<input type='hidden' id='learnType' name='learnType' value='NORMAL'/>
		</form> 
	</body>
</html>