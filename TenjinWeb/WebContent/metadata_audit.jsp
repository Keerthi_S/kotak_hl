<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  metadata_audit.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->





<%@page import="com.ycs.tenjin.aut.MetadataAudit"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.ModuleBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============

 * DATE                 CHANGED BY              DESCRIPTION
*/

-->
<head>
<title>AUT Functions - Tenjin Intelligent Testing Engine</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel='stylesheet' href='css/bordered.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel="SHORTCUT ICON" HREF="images/yethi.png">
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type="text/javascript" src="js/progressbar.js"></script>
<!-- <script type='text/javascript' src='js/pages/funclist.js'></script> -->
<script type='text/javascript' src='js/pages/metadataExportSelection.js'></script>
</head>
<body>
	<%
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			Project project = null;
			project = tjnSession.getProject();
			
			boolean canMakeChanges = false;
			
			
			if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){
				canMakeChanges = true;
			}
			Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("AUDIT_MAP");
			ArrayList<MetadataAudit> audits = (ArrayList<MetadataAudit>) map.get("audit_map");
			String type = "";
			if(audits !=null){
				int i = 1;
				for (MetadataAudit audit : audits) {
				type = audit.getType();	
				}
				}

			if(type.equalsIgnoreCase("EXPORT")){
		%>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#btnBack').click(function(){
				window.location.href = "metadata_export.jsp";
			});
		});
		</script>
		<div class='title'>
	<p>Export History</p>
	</div>
	
		<%
			}else{
		%>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#btnBack').click(function(){
				window.location.href = "metadata_import.jsp";
			});
		});
		</script>
		<div class='title'>
	<p>Import History</p>
	</div>
			
		<%} %>
	
	<div class='toolbar'>
		<input type="submit" value='Back' id='btnBack' class='imagebutton back'/>
	</div>
	
	<div id='user-message'></div>
	
	<div class='fieldSection grid_15'>
						<table id='tblAudit' class='bordered' cellspacing='0' width='100%'>
							<thead>
								<tr>
									<th>S.No</th>
									<th>User Name</th>
									<th>Date</th>
									<th>Action</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody id='tblAudit_body'>
								<%
									if(audits !=null){
										int i = 1;
										for (MetadataAudit audit : audits) {
											
											%>
											<tr>
												<td><%=i++ %></td>
												<td><%= audit.getUser() %></td>
												<td><%=audit.getDate() %></td>
												<td><%=audit.getType() %></td>
												<td><%=audit.getStatus() %></td>
											</tr>
										<% 
										}
									}
								%>
							</tbody>
						</table>
					</div>
</body>
</html>