<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  apilist.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.ApiLearnerResultBean"%>
<%@page import="java.util.Date"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Api"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>API's Maintenance</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css'/>
<link rel='stylesheet' href='css/bordered.css'/>
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script src="js/tables.js"></script>
<script type='text/javascript' src='js/pages/apilist.js'></script>

		<style>
			table, th, td {
				cellpadding:1px;
				cellspacing:1px;
			}
			.active {
				background:red;
			}
			#name,#url{
   			 width:200px;
    
    		height: 2em; 
    		cursor: pointer;
   			 word-break: break-all;
    		height:auto;
    		white-space: normal;
			}
			#name,#url{
   			 overflow: visible; 
    		}
	 	</style>
</head>
<body>

     <%
        TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
         if(tjnSession!=null){
     %>

<%
Cache<String, Boolean> csrfTokenCache = null;
csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
session.setAttribute("csrfTokenCache", csrfTokenCache);
UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
session.setAttribute ("csrftoken_session", csrftoken);

		String lrnrPage = "";
		Map<String, Object> map = (Map<String, Object>) request.getAttribute("API_MAP");
		List<Aut> auts = (List<Aut>) map.get("AUTS");
		List<Api> apis = (List<Api>) map.get("API_LIST");
		String currentAppId = (String) map.get("CURRENT_APP_ID");
		/* Added by Preeti for TENJINCG-600 starts */
		String apiGroup = (String) map.get("CURRENT_GRP_NAME");
		/* Added by Preeti for TENJINCG-600 ends */
		/*added by Preeti for TENJINCG-366 starts*/
		String apiType=(String) map.get("CURRENT_API_TYPE");
		List<String> adapters=(List<String>) map.get("ADAPTERS");
		String apiTypeFilter=null;
		/*added by Preeti for TENJINCG-366 ends*/
		int cAppId = 0;
		try{
			cAppId = Integer.parseInt(currentAppId);
		}catch(NumberFormatException e){
			cAppId = 0;
		}
		
		String status = (String) map.get("STATUS");
		String message = (String) map.get("MESSAGE");
		String dateFormat = TenjinConfiguration.getProperty("DATE_FORMAT");
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
	%>
	<div class='main_content'>
		<div class='title'>
			<p>API List</p>
		</div>
		
		<div class='toolbar'>
			<input type='button' value='New' id='btnNewApi' class='imagebutton new' /> 
			<input type='button' value='Remove' id='btnDelete' class='imagebutton delete' />
			<!-- Added by Ashiki TENJINCG-1213 starts -->
			<input type='button' id='btnLearn' class='imagebutton learn' value='Learn' />
			<!-- Added by Ashiki TENJINCG-1213 ends -->
		</div>
		
		<input type='hidden' id='status' value='<%=status %>'/>
		<input type='hidden' id='message' value='<%=Utilities.escapeXml(message )%>'/>
		
		<div id='user-message'></div>
		
		
		<form name='api_list_form' id='api_list_form' action='APIServlet' method='POST'>
			<input type="hidden" id="requesttype" name="requesttype" value="learning_api">
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='form container_16'>
				<fieldset>
					<legend>AUT Information</legend>
						<div class='fieldSection grid_15'>
							<div class='grid_2'><label for='lstAppId' style='text-align:right;'>Application</label></div>
							<div class='grid_3' >
								<input type='hidden' id='lstSelAut' value='<%=cAppId%>'/>
								<select id='lstAppId' name='lstAppId' class='stdTextBox'>
									<option value='-1'>-- Select One --</option>
									<%
									if(auts != null){
										for(Aut aut:auts){
											if(aut.getId() == cAppId){
												%>
												<option value='<%=aut.getId() %>' selected><%=Utilities.escapeXml(aut.getName()) %></option>
												<%
											}else{
											%>
											<option value='<%=aut.getId() %>'><%=Utilities.escapeXml(aut.getName()) %></option>
											<%
											}
										}
									}
									 %>
								</select>
							</div><div class='clear'></div>
							<div class='grid_2'><label for='apiGroup'>Group</label></div>
							<div class='grid_3'>
								<input type='hidden' id='selApiGroup' value='<%=Utilities.escapeXml(apiGroup)%>'/>
								<select class='stdTextBox' id='apiGroup' name='txtApiGroup'  title='API Group' >
									
								</select>
							</div>
							<div class='grid_3' style='text-align:center'>
								<input type='button' name='btnGo' id='btnGo' value='Go' class='imagebutton ok'/>
							</div>
							</div>
				</fieldset>
			</div>
		</form>
		
		<form name='main_form' id='main_form' action='ApiServlet' method='POST'>
			<div class='form container_16'>
				<input type='hidden' id='lstLrnrApplication' name='lstLrnrApplication' value=''/>
				<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
				<fieldset>
					<legend>API Filter</legend>
					<div class='fieldSection grid_12'>
							<div class='grid_2'><label for='apiType'  >API Type</label></div>
							<div class='grid_4'>
								<input type='hidden' id='selApiType' value='<%=apiType%>'/>
								<select id='apiType' name='apiType' class='stdTextBox'>
									<option value='ALL'>-- Select One --</option>
									<%
									if(adapters != null) 
									{
										for(String adapter:adapters) 
										{
											if(adapter.equalsIgnoreCase(apiType))
											{
												apiTypeFilter=apiType;
									%>
									<option value='<%=Utilities.escapeXml(adapter) %>' selected><%=Utilities.escapeXml(adapter) %></option>
									<%
										}else{
												apiTypeFilter=apiType;
											%>
											<option value='<%=Utilities.escapeXml(adapter) %>'><%=Utilities.escapeXml(adapter) %></option>
											<%
											}
										}
									}
								%>
								</select>
								
							</div>
									<input type='button' name='btnClear' id='btnClear' value='Clear' class='imagebutton reset'/>
						</div> 
						</fieldset>
					<fieldset>
					<legend>APIs</legend>
					<div class='clear'></div>
					
					<div class='fieldSection grid_15' id='modulesInfo'>
						<input type='hidden' id='pagination_value' value='<%=lrnrPage%>'/>
						<table id='tblNaviflowModules' class='display tjn-default-data-table' cellspacing='0' width='100%'>
							<thead>
								<tr>
									<th class='nosort'><input type='checkbox' class='tbl-select-all-rows tiny' id='chk_all_api'/></th>
									<th>API Code</th>
									<th style="width:200px">API Name</th>
									<th>Group</th>
									<th>API Type</th>
									<th style="width:200px"> API Url </th>
									<th>Last Learnt</th>
								</tr>
							</thead>
							 <tbody id='tbodyapi'>
							<%
								if (apis != null && apis.size() > 0) {
									int i=0;
									for (Api api : apis) {
										            i++;
										%>
							<tr>
								<!-- Added by Ashiki TENJINCG-1213 starts -->
								<td class='tiny'><input type='checkbox' name='chk_user'  class= 'checkbox chkBox' id='<%=api.getCode()%>' data-apitype='<%=api.getType()%>'   data-api-code='<%=api.getCode()%>' value='<%=api.getCode()%>' /></td>
							 	<!-- Added by Ashiki TENJINCG-1213 ends -->
							 	<td><%=api.getCode()%></td>
							 	<td style="width:200px">
							 	<div id="name">
								 <a href='APIServlet?param=fetch_api&paramval=<%=api.getCode()%>&appid=<%=api.getApplicationId()%>&apitypefilter=<%=Utilities.escapeXml(apiTypeFilter) %>&apigroup=<%=Utilities.escapeXml(apiGroup) %>'
									id='<%=Utilities.escapeXml(api.getName())%>' class='edit_record'><%=Utilities.escapeXml(api.getName())%></a></div></td>
								<td><%=Utilities.escapeXml(api.getGroup()) %></td>
								<td><%=Utilities.escapeXml(api.getType())%></td>
								<td style="width:200px">
								<div id="url">
								<%
								if(api.isUrlValid()) {
									%>
									<a href='<%=api.getUrl() %>' target='_blank' rel="noopener"><%=Utilities.escapeXml(api.getUrl())%></a>
									<%
								}else{
									%>
									<span style='color:red;' title='URL is invalid.'><%=Utilities.escapeXml(api.getUrl()) %></span>
									<%
								}
								%>
								</div>
								</td>
								<%
								if(api.getLearningHistory() != null && api.getLearningHistory().size() > 0) {
									ArrayList<Long> successLearnts=new ArrayList<Long>();
									for(ApiLearnerResultBean learnHistory:api.getLearningHistory()){
										if(learnHistory.getStatus().equalsIgnoreCase("Complete")){
											successLearnts.add(learnHistory.getStartTimestamp().getTime());
										}
									}
								%>
								<%
								if(successLearnts.size()>0){%>
									<td><%=sdf.format(new Date(successLearnts.get(0)))%></td>
								<% }
								else{%>
								<td>Not Learnt</td>
								<% }%>
								<% }
								
								else{%>
									<td>Not Learnt</td>
								<%}
								
								%>
							</tr>
							<%
								 }
								 }else {
							%>
							<%
								}
							%>
						</tbody> 
						</table>
					</div>
				</fieldset>
			</div>
			<input type='hidden' id='learnType' name='learnType' value='NORMAL'/>
		</form>
</div>
	 <%}else{
		response.sendRedirect("noSession.jsp");
	}
	%> 
</body>
</html>