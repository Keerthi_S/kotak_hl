<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_admin_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%@page import="com.ycs.tenjin.scheduler.Scheduler"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
  DATE                 CHANGED BY              DESCRIPTION
*/

-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

		<title>Scheduler Maintenance</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/bordered.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/schedule_admin_list.js'></script>	

</head>
<body>
     <%
     Cache<String, Boolean> csrfTokenCache = null;
     csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
     session.setAttribute("csrfTokenCache", csrfTokenCache);
     UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
     session.setAttribute ("csrftoken_session", csrftoken); 
     
        TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
         if(tjnSession!=null){
     %>

<div class='main_content'>
		<div class='title'>
			<p>Scheduler List in Admin</p>
		</div>
		<form name='sch_list_form' action='SchedulerServlet' method='post'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
			<input type='button' value='New' id='btnNewSch' class='imagebutton new'/>
				
				<input type='button' value='Remove' id='btnDelete' class='imagebutton delete'/>				
			</div>
			<div id='user-message'></div>
			<div >
						<%
							String scrStatus = "";
							String message = "";
							ArrayList<Scheduler> scheduleList = null;
							Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_SCH");
							if(map != null){
								scrStatus = (String)map.get("REQ_STATUS");
								message = (String)map.get("MESSAGE");
								scheduleList = (ArrayList<Scheduler>)map.get("SCH_LIST");
							}
						%>
				
				
				
				
				<div class='emptySmall'>&nbsp</div>
				
				<div class='form container_16'>
				<div id='dataGrid'>
					<table id='schTable' class='display dataTable no-footer' cellspacing='0' width='100%'>
						<thead>
							<tr>
								<th class='tiny'><input type='checkbox' id='chk_all' title='Select All'></th>
								<th>Schedule Id</th>
								<th>Date</th>
								<th>Time</th>
								<th>Task</th>
								<th>Registered Client</th>
								<th>Status</th>
								<th>Created By</th>
								<th>Created On</th>
							</tr>
						</thead>
						<tbody>
							<%
								if(scheduleList.size() > 0){
									for(Scheduler scheduler:scheduleList){
							%>
							<tr>
								<td class='tiny'><input type='checkbox' class= 'checkbox' id='chk_sch' value='<%=scheduler.getSchedule_Id() %>' />
								<td><a href='SchedulerServlet?param=VIEW_SCH_ADMIN&paramval=<%=scheduler.getSchedule_Id()%>' id='<%=scheduler.getSchedule_Id()%>' class='edit_record'><%=scheduler.getSchedule_Id() %></a></td>
								<td><%=scheduler.getSch_date()%></td>
								<td><%=scheduler.getSch_time()%></td>
								<td><%=scheduler.getAction()%></td>
								
								<td><%=scheduler.getReg_client()%></td>
							
								<td><%=scheduler.getStatus()%></td>
								<td><%=scheduler.getCreated_by()%></td>
								<td><%=scheduler.getCreated_on()%></td>
							
							
							</tr>
							<%		}
								}else{
									%>
									<tr>
										<td colspan='3'>No Records Found</td>
									</tr>
									<%
								}
							%>
						</tbody>
					</table>
				</div>
				</div>
			</div>			
			
			</form>	
		</div>
		<div class='sub_content' style='height:500px;'>
			<iframe class='ifr_Main' name='content_frame' id='content_frame'
				src=''></iframe>
		</div>
		
	<%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>
		

</body>
</html>