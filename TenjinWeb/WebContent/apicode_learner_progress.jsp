<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  apicode_learner_progress.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 18-11-2020		   Ashiki				 Newly added for TENJINCG-1213
*/

-->

<%@page import="com.ycs.tenjin.bridge.pojo.aut.ApiOperation"%>
<%@page import="com.ycs.tenjin.util.HatsConstants"%>
<%@page import="com.ycs.tenjin.run.TestRun"%>
<%@page import="com.ycs.tenjin.run.LearnerGateway"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Api"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@ page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>API Learning Progress</title>
		
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		
		<link rel='stylesheet' href='css/rprogress.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel="Stylesheet" href="css/summarypagetable.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/list.css'/>
		
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/CircularLoader-v1.3.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		
		<style>
			.current
			{
			    -webkit-animation-name: example; 
			    -webkit-animation-duration: 1s; 
			    animation-name: example;
			    animation-duration: 1s;
			    animation-iteration-count: 1000;
			    
			}
			
			@keyframes example {
			    
			    
			    from {background-color: #f79232; }
    			to {background-color: #f6c342; }
			}
			
			@-webkit-keyframes example {
			    
			    from {background-color: red;}
    			to {background-color: yellow;}
			}
		</style>
		
		<script>
			var loop;
			$(document).ready(function() {
				
				$(document).on('click','.template-gen',function(e){
					var mod = $(this).attr('mod');
					var app = $(this).attr('app');
					var txnMode=$(this).attr('txnMode');
					var operation = $(this).attr('operation');
					$('#ttd-options-frame').attr('src','ttd_download_options.jsp?a=' + app + '&m=' +mod + '&t=' + txnMode + '&operation=' + operation);
			    	$('.modalmask').show();
			    	$('#ttd_download_options').show();
				});
				
				var runId = $('#runId').val();
				loop = window.setInterval(function(){
					$.ajax({
						url:'ApiLearnerServlet?param=check_progress&runId=' + runId,
						dataType:'json',
						success:function(data) {
							if(data.status.toLowerCase() === 'success') {
								clearMessages();
								repaintStatus(data.progress);
							}else{
								clearInterval(loop);
								window.location.href='ApiLearnerServlet?param=apicode_learner_result&runid=' + $('#runId').val();
							}
						},
						
						error:function(xhr, textStatus, errorThrown) {
							showMessage('Could not load progress. Retrying in a second...', 'error');
						}
					}); 
				}, 750);
				
			});
			
			
			function repaintStatus(progress) {
				$('.current').each(function(){
					$(this).removeClass('current');
				});
				$('#elapsedTime').html(progress.elapsedTime);
				var $currentOperationElement = $('#allOperations_ul>li[data-operation-name=' + progress.currentOperation +']');
				$currentOperationElement.addClass('current');
				
				var appId = $('#appId').val();
				var apiCode = $('#apiCode').val();
				
				var completedOperations = progress.operations;
				for(var i=0; i<progress.operations.length; i++) {
					var op = progress.operations[i];
					
					var opName = op.name;
					var opStatus = op.lastSuccessfulLearningResult.status;
					
					var $opElement = $('#allOperations_ul>li[data-operation-name=' + opName +']');
					if($opElement !== undefined) {
						if(opStatus.toLowerCase() === 'error'){
							var $opIcon = $opElement.find('.entity-icon');
							$opIcon.html("<img src='images/error_20c20.png'/>");
							$opElement.removeClass('current');
							$opElement.remove();
							$opElement.append($("<span class='entity-summary'>"+ op.lastSuccessfulLearningResult.message +"</span>"));
							$('#failedOperations_ul').append($opElement);
						}else if(opStatus.toLowerCase() === 'complete'){
							var $opIcon = $opElement.find('.entity-icon');
							$opIcon.html("<img src='images/success_20c20.png'/>");
							$opElement.removeClass('current');
							$opElement.remove();
							
							var $opAction = $("<span class='entity-action' />");
							$opAction.append($("<a class='template-gen' href='#' app='" + appId + "' mod='"+ apiCode +"' txnMode='API' operation='"+opName+"'>Download</a>"));
							$opElement.append($opAction);
							$('#passedOperations_ul').append($opElement);
						}
					}
				}
				$('#diagram-id-2').circularloader({
					progressPercent: progress.percentage,
		            backgroundColor: "#ffffff",
		            fontColor: "#000000",
		            fontSize: "20px",
		            radius: 60,
		            progressBarBackground: "#fff",
		            progressBarColor: "#14892c",
		            progressBarWidth: 3,
		            speed: 10,
		            progressvalue: progress.percentage,
		            showText: true
				});
				
				if(progress.percentage >= 100) {
					clearInterval(loop);
					$('.current').each(function(){
						$(this).removeClass('current');
					});
					
					window.location.href='ApiLearnerServlet?param=apicode_learner_result&runid=' + $('#runId').val();
				}
				
				var remainingCount = $('#allOperations_ul').find('li').length;
				var successCount = $('#passedOperations_ul').find('li').length;
				var errorCount = $('#failedOperations_ul').find('li').length;
				
				$('#successCount').html(successCount);
				$('#errorCount').html(errorCount);
				$('#remainingCount').html(remainingCount);
			}
			
			function closeModal()
			{
				$('.modalmask').hide();
				$('#ttd_download_options').hide();
				$('#ttd-options-frame').attr('src','');
			}
		</script>
		
	</head>
	<body>
		
		<%
		String runId = (String) request.getSession().getAttribute("run_id");
		String runStatus = (String) request.getSession().getAttribute("run_status");
		
		if(Utilities.trim(runStatus).length() < 1) {
			runStatus = "Please wait...";
		}
		
		Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("API_LRNR_MAP");
		String appId = (String) map.get("LRNR_AUT_ID");
		String appName = (String) map.get("LRNR_AUT");
		List<Api> apis = (List<Api>) map.get("LRNR_API");
		String apiType = (String) map.get("LRNR_API_Type");
		LearnerGateway gateway = (LearnerGateway) request.getSession().getAttribute("API_LRNR_GATEWAY");
		TestRun run = gateway.getTestRun();
		%>
		
		<div class='title'>
			<p>API Learner Progress</p>
		</div>
		
		<div id='user-message'></div>
		
		<div class='form' style='width:1053px;'>
			<div class='twoplusone-col-field-section'>
				<div class='fs_title'>Run Information</div>
			</div>
			<div class='three-col-field-section'>
				<div class='fs_title'>Overall Progress</div>
			</div>
			<div class='three-col-field-section'>
				<input type='hidden' id='runId' value='<%=run.getId() %>'/>
				<input type='hidden' id='appId' value='<%=appId %>'/>
				<%-- <input type='hidden' id='apiCode' value='<%=api.getCode() %>'/> --%>
				<table class='layout'>
					<tr>
						<td class='layout-label'>Run ID</td>
						<td class='layout-value'><%=run.getId() %></td>
					</tr>
					<tr>
						<td class='layout-label'>Started On</td>
						<td class='layout-value'><%=run.getStartTimeStamp() %></td>
					</tr>
					<tr>
						<td class='layout-label'>Elapsed Time</td>
						<td class='layout-value'><span id='elapsedTime'>00:00:00</span></td>
					</tr>
					<%-- <tr>
						<td class='layout-label'>API Code</td>
						<td class='layout-value'><%=api.getCode() %></td>
					</tr> --%>
					
				</table>
			</div>
			
			<div class='three-col-field-section' >
				<table class='layout'>
					<tr>
						<td class='layout-label'>Started By</td>
						<td class='layout-value'><%=run.getUser() %></td>
					</tr>
					<tr>
						<td class='layout-label'>Ended On</td>
						<td class='layout-value'><span id='endTimestamp'></span></td>
					</tr>
					<tr>
						<td class='layout-label'>Application</td>
						<td class='layout-value'><%=appName %></td>
					</tr>
					<tr>
						<td class='layout-label'>Type</td>
						<td class='layout-value'><%=apiType %></td>
					</tr>
				</table>
			</div>
			<div class='three-col-field-section' style='height:141px;text-align:center;border:1px solid #fff'>
				<input type='hidden' id='percent-display' value=''/>
				<div id="diagram-id-2" ></div>
			</div>
			<div class='clear'></div>
			
			<div class='three-col-field-section' id='allOperations'>
				<div class='fs_title'>To Learn (<span id='remainingCount'></span>)</div>
				
				<ul class='entity-list' id='allOperations_ul'>
				<table class='layout'>
					<tr>
						<td class='layout-label'>API Code</td>
						<td class='layout-label'>Operation</td>
						<td class='layout-label'></td>
						
					</tr>
					<%for(Api api:apis){
						for(ApiOperation operation: api.getOperations()) {
						%>
						<tr>
						<td><div class='entity-title'><%=api.getCode() %></div> </td>
						<td><%=operation.getName() %></td>
						</tr>
						
					<%}
						}%>
				</table>
					<%-- <%
					for(Api api:apis){
						for(ApiOperation operation: api.getOperations()) {
						%> --%>
						<%-- <li><span class='entity-icon'></span>
							<div class='entity-title'><%=api.getCode() %></div></li>
						<li data-operation-name='<%=operation.getName() %>'>
							
							<div><span class='entity-title'><%=operation.getName() %></span></div>
							
						</li> --%>
						
						<%-- <%
					}
					}
					%> --%>
				</ul>
			</div>
			
			<div class='three-col-field-section' id='passedOperations'>
				<div class='fs_title'>Learnt successfully (<span id='successCount'></span>)</div>
				<ul class='entity-list' id='passedOperations_ul'>
				</ul>
			</div>
			
			<div class='three-col-field-section' id='failedOperations'>
				<div class='fs_title'>Errors (<span id='errorCount'></span>)</div>
				<ul class='entity-list' id='failedOperations_ul'>
				</ul>
			</div>
		</div>
		
		<div class='subframe' id='ttd_download_options' style='left:20%'>
			<iframe frameborder="2" scrolling="no"  marginwidth="5" marginheight="5" style='height:500px;width:600px;' seamless="seamless" id='ttd-options-frame' src=''></iframe>
		</div>
		
		<div class='modalmask'></div>
	</body>
</html>