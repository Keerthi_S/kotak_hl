<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  port_download.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 
-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Tenjin - Intelligent Enterprise Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel="SHORTCUT ICON" HREF="images/yethi.png">
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/port_download.js'></script>
	</head>
	<body>
		<div class='title'>
			<p>Data Porting template download options</p>
		</div>
		<%
	
		%>
		<div id='user-message'></div>		
		<div  class='form' id='ttd-options-wrapper'>
			<fieldset>
				<legend>Choose a Template Type</legend>
			
				<table id='ttd-options-layout'>
					<tr>
						<td class='selector'><input type='radio' name='templateType' id='templateTypeMaster' value='DATASTYLE'/></td>
						<td class='ttd-option-block'>
							<div class='ttd-option-title'>Complete Template</div>
							<div class='ttd-option-desc'>
								<p>
								A template with all fields with data included with css styles.
								</p>
							</div>
						</td>
					</tr>
					<tr>
						<td class='selector'><input type='radio' name='templateType' id='templateTypeInput' value='DATAONLY'/></td>
						<td class='ttd-option-block'>
							<div class='ttd-option-title'>Data Template</div>
							<div class='ttd-option-desc'>
								<p>
								A template with all fields with only data.
								</p>
							</div>
						</td>
					</tr>
					
				</table>
			</fieldset>
			<fieldset>
				 
			</fieldset>
			
			<div id='progress'></div>
			<div id='buttons' style='margin-top:20px;text-align:center;'>
				<input type='button' id='btnOk' class='imagebutton ok' value='Ok'/>
				<input type='button' id='btnCancel' class='imagebutton cancel' value='Cancel'/>
			</div>
		</div>
	</body>
</html>