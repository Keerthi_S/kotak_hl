
<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  email_configuration.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION

-->
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
	<head>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/email_configuration.js'></script>
	</head>
	<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<div class='title'>
	<p>E-mail Configuration</p>
	</div>
	<form id='emailform' name='emailform' action='EmailServlet' method='post' class='tovalidate'>
	<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<div class='toolbar'>
			<input type="submit" id='btnsave' value='Save' class='imagebutton save' />
			<input type="button" id='btnRefresh' value='Refresh' class='imagebutton reset' />  
			<input type="button" id='btnDelete' value='Remove' class='imagebutton delete' /> 
			
		</div>
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message )}' />
		<input type='hidden' id='delete' value='${fn:escapeXml(delete)}' />
		<div id='user-message'></div>

		<div class='form container_16'>
			<fieldset>
				<legend>Configuration Details</legend>
					<div class='fieldSection grid_16'>
						<div class='clear'></div>
						<div class='grid_3'><label for='txtsmtp'>SMTP Server</label></div>
						<div class='grid_4'><input type='text' id='txtsmtp' name='SMTPServer'value='${fn:escapeXml(map.smtpServer)}' class='stdTextBox' mandatory='yes' title='SMTP Server' /></div>
	
						<div class='clear'></div>
						<div class='grid_3'><label for='txtport'>Port</label></div>
						<div class='grid_4'><input type='text' id='txtport' name='port' value='${fn:escapeXml(map.port)}'  class='stdTextBox' mandatory='yes' title='Port' /></div>
					
						<div class='clear'></div>
						<div class='grid_3'><label for='txtauthenticate'>Authentication Required</label></div>
						<div class='grid_4'><input type='checkbox' id='txtauthenticate' name='authentication'style='vertical-align: bottom;margin-top:6px;' value='${fn:escapeXml(map.authentication)}' checked="checked"/></div>
					
						<div class='clear hide'></div>
						<div class='grid_3 hide'><label for='txtuserid'  >E-mail</label></div>
						<div class='grid_4 hide'><input type='text' id='txtuserid' name='userId' value='${fn:escapeXml(map.userId)}'  class='stdTextBox' title='E-mail' autocomplete="off" mandatory='yes'/></div>
					
						<div class='clear hide'></div>
						<div class='grid_3 hide'><label for='txtpassword' id='pwdLabel'>Password</label></div>
						<div class='grid_4 hide'><input type="password" id='txtpassword' name='password' value='' placeholder="Enter password to update" autocomplete="new-password" class='stdTextBox'  title='Password'  /></div>
						<div class='grid_3 hide'><label for='emailconfig' id='emailconfig'>Email is already configured</label></div>
						
						<div class='clear hide'></div>
						<div class='grid_3 hide'><label for='txtusername'>User Name</label></div>
						<div class='grid_4 hide'><input type="text" id='txtusername' name='userName' value='${fn:escapeXml(map.userName)}'  class='stdTextBox'  title='User Name' placeholder="Mandatory for NTLM Authentication"  /></div>
						
						<div class='clear'></div>
						<div class='grid_3 '><label for='txtemail'>E-mail</label></div>
						<div class='grid_7 '><input type='text' id='txtemail' name='email'value='${fn:escapeXml(map.email)}' class='stdTextBox long'  title='E-mail'  /></div>
						<div class='grid_2 '><input type='button' id='btntest' value='Test-Configuration' class='imagebutton save'  /></div>&nbsp
					   <img src='images/inprogress.gif' id='emailLoader' style="padding-top:6px"/>
					   <div class='grid_6 ' >
					  		 <p id='testMessage' align="center" ></p>
					   </div>
					  
					</div>
					
			</fieldset>
			
			</div>
	</form>

</body>
</html>