<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  projectlanding.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->



<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import=" java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY             		DESCRIPTION
 
 19-11-2020             Priyanka                    TENJINCG-1231   
  
*/

-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tenjin - Intellilgent Enterprise Testing Engine V2.0</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel='stylesheet' href='css/accordion.css' />
<link rel='stylesheet' href='css/ttree/ttree.css' />
<link rel="SHORTCUT ICON" HREF="images/yethi.png">
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/pages/projectlanding.js'></script>
<script type='text/javascript' src='js/ttree.js'></script>
<script src="js/formvalidator.js"></script>
<style>
.active {
	background-color: rgb(212, 212, 216);
	border: 1px solid #ccc;
	box-shadow: 0 0 5px #ccc;
}
 
 .header-middle{
	float:left;
	line-height:50px;
	width:430px;
	text-align:left;
	text-overflow: ellipsis;
 	overflow: hidden;
 	white-space: nowrap; 
 	max-width: 430px;
 }
 .header-right
{
	width:560px;
	float:right;
	display:inline-block;
	margin-top:15px;
	text-align:right;
	text-overflow: ellipsis;
 	overflow: hidden;
 	white-space: nowrap; 
 	max-width:560px;
}
</style>
</head>
<body>


	<%
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Project project = tjnSession.getProject();
		Map<String, String> map = (Map<String,String>)request.getSession().getAttribute("SCR_MAP"); 
		 String prefer=null;
           if(map!=null){
        	   prefer=map.get("prefrence");
           }
		
		User user = tjnSession.getUser();
		boolean projectActive = true;
		if(Utilities.trim(project.getState()).equalsIgnoreCase("I")) {
			projectActive = false;
		}
		 String license_notification = "";
		try{
		Map<String, String> maplice = (Map<String,String>)request.getSession().getAttribute("LICENSE_NOTIFICATION");
			license_notification=(String)maplice.get("MESSAGE");
		}
		catch (NullPointerException e ){
			license_notification = "";
		}
		String projectName=Utilities.escapeXml(project.getName());
		String domainName=Utilities.escapeXml(project.getDomain());
		%>
	<input type="hidden" value="" name ="param" id="param"/>
	<div class='wrapper'>
		<div class='header'>
			<div class='header-left' style='width:270px;'>
				<img src='images/tenjin_logo.png'
					alt='Tenjin - Intelligent Testing Engine' style='width: 125px;' />
			</div>
			<div class='header-middle' style='width:630px;text-align:left'>
				<span style='color:#070e31;font-weight:bold; font-size:13px;' title='Domain:<%=Utilities.escapeXml(project.getDomain())%> &nbsp  Project:<%=Utilities.escapeXml(project.getName())%>'> Domain:<%=domainName.length()>25? domainName.substring(0,25):domainName %> &nbsp  Project:<%=projectName.length()>30?projectName.substring(0,30):projectName %></span>
			</div>
			<div class='header-actions'>
				<a class="headerLink" id='logout' name='logout' href='#'
					title='Logout'><img src='images/Logout.png' alt='Logout' /></a>
			</div>
			<div class='header-right' style="width:380px">
				 
					 
					<a href='#' class='switchProject headerLink' id='switchProject' title='Click to switch the project'><img src="images/switch_project.png">&nbsp Switch
					Project</a> <a href='#' class='adminPanel headerLink' id='adminPanel' title='Click to view administration panel'><img src="images/admin.png">&nbspAdministration</a>
			 
				<a id='logged-in-user' class='headerLink'
					onclick='displayView()'
					target='content_frame' title='Click to View/Edit your profile'><%=tjnSession.getUser().getFullName() %></a>
				 
				<%
					if(tjnSession.getUser().getLastLogin() != null && !tjnSession.getUser().getLastLogin().equalsIgnoreCase("")){
					%>
				<div class='note'>
					Last Login:
					<%=tjnSession.getUser().getLastLogin() %>
				</div>
				<%} %>
			</div>
		</div>
		<div class='main'>
			<input type='hidden' id='projectid' name='projectid'
				value='<%=project.getId() %>' /> <input type='hidden'
				id='projectstate' name='projectstate'
				value='<%=project.getState() %>' /> <input type='hidden'
				id='preference' name='preference' value='<%=Utilities.escapeXml(prefer)%>' />
			<div id='user-message'></div>
			<div class='main_menu'>
				<div id='accordion'>
					<h3>Project</h3>
					<div>
						<div class='accordion-child-item-standalone'>
								<a href='ProjectDashBoardServlet?t=project_dashboard&paramval=<%=project.getId()%>'
								target='content_frame' class='project'>Project Dashboard</a>
						</div>
						<div class='accordion-child-item-standalone'>
							<a
								href='ProjectServletNew?t=load_project&paramval=<%=project.getId()%>&view_type=project'
								target='content_frame' class='project'><%=Utilities.escapeXml(project.getName())%></a>
						</div>
						<div class='accordion-child-item-standalone'>
							<a
								href='TestDataUploadServlet?param=load_testdatalist&paramval=<%=project.getId()%>&view_type=project'
								target='content_frame' class='project'>Test Data</a>
						</div>
						<div class='accordion-child-item-standalone'>
							<a href='ProjectMailServlet?param=view&projectid=<%=project.getId() %>' target='content_frame' class='project'> Project E-mail Settings</a>
						</div>
						<div class='accordion-child-item-standalone'>
							<a href='TestRunServlet' target='content_frame' class='project'>Runs</a>
						</div>
					</div>
					<h3 id='tc-trigger'>Test Cases</h3>
					<div>
					<!--  Added by Priyanka for TENJINCG-1231 starts -->
                     <%SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");  
           			  Date date = new Date();  
        			  String currentdate= formatter.format(date);
        			  String EndDate =  formatter.format(project.getEndDate());
        			  long tdyDate = Date.parse(currentdate);
        			  long prjEndDate  = Date.parse(EndDate);
        			 
        			 
        			   if(prjEndDate>=tdyDate) { %> 
        			    <!--  Added by Priyanka for TENJINCG-1231 ends -->
						<div class='accordion-child-item-standalone'>
							<a href='TestCaseServletNew?t=init_new_testcase'
								target='content_frame' class=' '>Create Test Case </a>

						</div>
						<div class='accordion-child-item-standalone'>
							<a href='TestCaseServletNew?t=list' target='content_frame'
								id='allTestCases' class=' '>All Test Cases</a>
						</div>
						<!--  Added by Priyanka for TENJINCG-1231 starts -->
						<%}else{ %>
						<div class='accordion-child-item-standalone'>
							<a href='TestCaseServletNew?t=list' target='content_frame'
								id='allTestCases' class=' '>All Test Cases</a>
						</div>
						<%} %>
						<!--  Added by Priyanka for TENJINCG-1231 ends -->
					</div>
					<h3 id='ts-trigger'>Test Sets</h3>
					
					<div><!--  Added by Priyanka for TENJINCG-1231 starts -->
					<%if(prjEndDate>=tdyDate) { %> 
					<!--  Added by Priyanka for TENJINCG-1231 ends -->
						<div class='accordion-child-item-standalone'>
							<a href='TestSetServletNew?t=init_new_testset'
								target='content_frame' class='New Test Set '>Create Test Set
							</a>
						</div>
						<div class='accordion-child-item-standalone'>
							<a href='TestSetServletNew?t=list' target='content_frame'
								id='allTestsets' class=' '>All Test Sets</a>
						</div>
						<!--  Added by Priyanka for TENJINCG-1231 starts -->
						<%}else{ %>
						<div class='accordion-child-item-standalone'>
							<a href='TestSetServletNew?t=list' target='content_frame'
								id='allTestsets' class=' '>All Test Sets</a>
						</div>
						<%} %>
						<!--  Added by Priyanka for TENJINCG-1231 ends -->
					</div>
					<h3 id='ts-trigger'>Defect Management</h3>
					<div>
					<!--  Added by Priyanka for TENJINCG-1231 starts -->
					<%if(prjEndDate>=tdyDate) { %> 
					<!--  Added by Priyanka for TENJINCG-1231 ends -->
						<div class='accordion-child-item-standalone'>
							<a href='DefectServlet?param=init_new_defect'
								target='content_frame' class=' '>Create Defect </a>
						</div>
						<div class='accordion-child-item-standalone'>
							<a
								href='DefectServlet?param=fetch_project_defects&pid=<%=project.getId()%>&callback=fetch'
								target='content_frame' id='allDefects' class=' '>Search
								Defect</a>
						</div>
						<!--  Added by Priyanka for TENJINCG-1231 starts -->
						<%}else{ %>
						<div class='accordion-child-item-standalone'>
							<a
								href='DefectServlet?param=fetch_project_defects&pid=<%=project.getId()%>&callback=fetch'
								target='content_frame' id='allDefects' class=' '>Search
								Defect</a>
						</div>
						<%} %>
						<!--  Added by Priyanka for TENJINCG-1231 ends -->
					</div>
					<h3>Scheduler</h3>
					<div>
						<div class='accordion-child-item-standalone'>
							<%
								if (projectActive) {
							%>
							<a href='SchedulerServlet?param=NEW_SCHEDULE'
								target='content_frame'>Schedule Task</a>
							<%
								}
							%>
						</div>
						<div class='accordion-child-item-standalone'>
							<a
								href='SchedulerServlet?param=FETCH_ALL_SCHEDULES&paramval=All&id=<%=project.getId()%>'
								target='content_frame'>View Task Status</a>
						</div>
					</div> 
					<h3>Reports</h3>
					<div>
						<div class='accordion-child-item-standalone'>
							<a href='TestReportServlet?param=test_set_report' target='content_frame'>Test Set Report</a>
						</div>
						<div class='accordion-child-item-standalone'>
							<a href='TestReportServlet?param=test_case_report' target='content_frame'>Test Case Report</a>
						</div>
						<div class='accordion-child-item-standalone'>
							<a href='TestReportServlet?param=miscellaneous_report' target='content_frame'>Miscellaneous Report </a>
						</div>
					</div>
					<h3>Help</h3>
					<div>
						<div class='accordion-child-item-standalone'>
							<a href='AutServlet?param=fetch_prerequisites'
								target='content_frame'>PREREQUISITES</a>
						</div>
						<div class='accordion-child-item-standalone'>
							<a href='AutServlet?param=FETCH_FAQ' target='content_frame'>FAQ's</a>
						</div>

					</div>
				</div>
				<div>
					<form name='admin-form' id='admin-form' action='AdminLaunchServlet'
						method='POST'>
						<input type='hidden' name='entry' value='project' />
						<input type="hidden" value="" name ="param" id="param"/>						
					</form>
				</div>

			</div>
			<div class='content'>
				<iframe class='ifr_Main' name='content_frame' id='content_frame'
					src=''> </iframe>
			</div>
		</div>
		<div class='footer'>
			<div class='footer-left'>
			&nbsp;
		 
			</div>
			 
			<div class='footer-middle'>
				&nbsp; <b><font color="red" style="align-items: center;font-size:10px"> <%=license_notification%></font></b>
			</div>
			 
			<div class='footer-right'>
				&copy Copyright 2014 -
				<%=new java.text.SimpleDateFormat("yyyy").format(new java.util.Date())%>
				<a href='http://www.yethi.in' target='_blank' rel="noopener">Yethi Consulting
					Pvt. Ltd.</a> All Rights Reserved
			</div>
		</div>
	</div>
</body>
</html>