

<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  api_learning_history.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.ApiLearnerResultBean"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.project.TestSet"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--
 New file Added by Leelaprasad for the requirement tjn_243_09(Learning history screen) 
/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION

*/

-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<head>
<title>Function Details - Tenjin Intelligent Testing Engine</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/formvalidator.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/pages/api_learning_history.js'></script>
<style>
			#name{
				width:200px;
				word-break: break-all;
    			height:auto;
    			white-space: normal;
			}
			.text-center{
				text-align:center
			}
</style>
</head>
<body>
	<%
				
   				 Cache<String, Boolean> csrfTokenCache = null;
   				 csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    			session.setAttribute("csrfTokenCache", csrfTokenCache);
    			UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
   				 session.setAttribute ("csrftoken_session", csrftoken);
				Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
				String appId = (String)map.get("APPID");	
				String api_Code= (String)map.get("API_CODE");
			    String api_Name=(String)map.get("API_NAME");
			    String appName=(String)map.get("APP_NAME");
				String status = (String)map.get("STATUS");
				String message = (String)map.get("MESSAGE");
				ArrayList<ApiLearnerResultBean> lrnr_runs = (ArrayList<ApiLearnerResultBean>)map.get("SUMMARY");
				ApiLearnerResultBean lrnrBean=null;
				if (lrnr_runs != null && lrnr_runs.size() > 0) {
					lrnrBean = lrnr_runs.get(0);
				}
			%>
			
     <%
        TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
         if(tjnSession!=null){
     %>

			
	<div class="title">
		<p>API Learning History</p>

	</div>
	<div class='toolbar'>
		<input type='button' id='btnBack' class='imagebutton back'
			value='Back' />
	</div>
	<div class='form container_16'>

		<%
			if(status != null && status.equalsIgnoreCase("SUCCESS")){
		%>

		<% 
					if (lrnrBean == null) {
		%>
		<div id='user-message'>This API Code has not been learned yet</div>
		<%
					} else {
		%>
		<div id='user-message'><%=message %></div>
		<%
					}
		%>

		<%
			
			}else if(status != null && !status.equalsIgnoreCase("") && !status.equalsIgnoreCase("SUCCESS")){
				%>
				<div id='user-message' class='msg-err' style='display:block;'>
					<div class="msg-icon">&nbsp;</div>
					<div class="msg-text"><%=message %></div>
				</div>
				<input type='hidden' id='appID' value='<%=Utilities.escapeXml(appId) %>'/>
		<input type='hidden' id='txtFunctionId' value='<%=Utilities.escapeXml(api_Code) %>' />
				
		<%}else{
		%>
		<div id='user-message'></div>

		<%
			}
		%>


		<%
			 if (lrnrBean != null ){
		%>
         <form action='AutServlet' name='aut_funcs_form' id='aut_funcs_form' method='POST'>
		<fieldset>
			<legend>API Details</legend>

			<% } %>

			<div class='fieldSection grid_7' id='functionDetails'>

				<%
					 if (lrnrBean != null ){
				%>
				
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='txtApplicationID'>Application</label>
				</div>
				<div class='grid_4'>
				 <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
				
					<input type='text' id='txtApplicationID' name='txtApplicationID'
						value='<%=Utilities.escapeXml(appName) %>' class='stdTextBox info-holder' mandatory='yes'
						title='Application' disabled='disabled' />
				</div>
			</div>
			<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='txtFunctionId'>API Code</label>
				</div>
				<div class='grid_4'>
					<input type='text' id='txtFunctionId' name='txtFunctionId'
						value='<%=Utilities.escapeXml(api_Code) %>' class='stdTextBox info-holder'
						mandatory='yes' title='Function Code' disabled='disabled' />
				</div>

				<div class='clear'></div>
				<div class='grid_2'>
					<label for='txtFunctionName'>API Name</label>
				</div>
				<div class='grid_4'>
					<input type='text' id='txtFunctionName' name='txtFunctionName'
						value='<%=Utilities.escapeXml(api_Name) %>' class='stdTextBox info-holder'
						mandatory='yes' title='Function Name' disabled='disabled' />
				</div>
			</div>



			<% } %>



			<%
			 if (lrnrBean != null ){
			%>

		</fieldset>
		<fieldset>
			<legend>API Learning History</legend>

			<div id='dataGrid'>
				<table id='summary-table' class='display' cellspacing='0'
					width='100%'>
					<thead>
						<tr><th class='tiny'><input type='hidden' ></th>
							<th class="text-center">Run ID</th>
							<th><div id="name">Operation</div></th>
							<th class="text-center">Start Time</th>
							<th class="text-center">End Time</th>
							<th class="text-center">Elapsed time</th>
							<th>User ID</th>
							<th>Status</th>
							<th>Message</th>
							<th>No.of Fields Learnt </th>
						</tr>
					</thead>
					<tbody>
						<%int i=0;
							if(lrnr_runs != null && lrnr_runs.size() > 0){
								for(ApiLearnerResultBean run:lrnr_runs){
									%>
						<tr>
						<td class='tiny'><input type='hidden'/></td>
							<td class="text-center"><%=run.getId() %></td>
						
											<%
						             	String df = TenjinConfiguration.getProperty("DATE_FORMAT");
										
										SimpleDateFormat sdf = new SimpleDateFormat(df);
										
										String cTime = "";
										if(run.getStartTimestamp() != null){
											cTime = sdf.format(run.getStartTimestamp());
										}
										else{
											cTime="N/A";
										}
										%>
										<td><div id="name"><%=Utilities.escapeXml( run.getOperationName())%></div></td>
							<td class="text-center"><%=cTime %></td>

							<% 
										
										cTime = "";
										if(run.getEndTimestamp() != null){
											cTime = sdf.format(run.getEndTimestamp());
										}
										else{
											cTime="N/A";
										}
										%>
							<td class="text-center"><%=cTime %></td>
							<%if(run.getStartTimestamp() == null||run.getEndTimestamp() == null){%>
							<td class="text-center">N/A</td>
							<%
							}
							else
							{
							%>
							<td class="text-center"><%=run.getElapsedTime() %></td>
							<%} %>
							<td><%=Utilities.escapeXml(run.getUser()) %></td>
							<td><%=run.getStatus() %></td>
							<%/* }
							else */
							if(run.getStatus().equalsIgnoreCase("Complete")){%>
							<td>Success</td>
							<td><%=run.getLastLearntFieldCount() %></td>
							<%}
							else if(run.getMessage()!=null){%>
							<td><%=run.getMessage() %></td>
							<td>N/A</td>
							<%	
							}
							
							else{%>
							<td>N/A</td>
							<%}%>
								
                         
                         </tr>
						<%
								}
							
							}%>

					</tbody>
				</table>
			</div>
		</fieldset>
		<input type='hidden' id='appID' value='<%=Utilities.escapeXml(appId) %>'/>
		<input type='hidden' id='txtFunctionId' value='<%=Utilities.escapeXml(api_Code) %>' />
						 
     </form>
     <div class='modalmask'></div>
		<div class='subframe' style='left:20%'>
		
			
			<iframe frameborder="2" scrolling="no"  marginwidth="5" marginheight="5" style='height:500px;width:600px;' seamless="seamless" id='ttd-options-frame' src=''></iframe>
	
		</div>

		<% } %>
	<%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>
	</div>
</body>
</html>