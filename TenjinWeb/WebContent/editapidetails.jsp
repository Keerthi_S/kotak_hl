<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  editapidetails.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY             	DESCRIPTION
*/

-->
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
 <%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.ApiOperation"%>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Api"%>
<%@page import="java.util.*"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta charset="ISO-8859-1">
		<title>API's Maintenance</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type="text/javascript" src="js/formvalidator.js"></script>
		<script src="js/pages/editapidetails.js"></script>
		<script src="js/tables.js"></script>
		<style>
			#progress_dialog, #confirmation_dialog
			{
				background-color:#fff;
			}
			
			.subframe_content
			{
				display:block;
				width:100%;
				margin:0 auto;
				min-height:120px;
				padding-bottom:20px;
			}
			.subframe_title
			{
				width:644px;
				height:20px;
				background:url('images/bg.gif');
				color:white;
				font-family:'Open Sans';
				padding-top:6px;
				padding-bottom:6px;
				padding-left:6px;
				font-weight:bold;
				margin-bottom:10px;
			}
			
			.subframe_actions
			{
				display:inline-block;
				width:100%;
				background-color:#ccc;
				padding-top:5px;
				padding-bottom:5px;
				font-size:1.2em;
				text-align:center;
			}
			
			.highlight-icon-holder
			{
				display:block;
				max-height:50px;
				margin-bottom:10px;
				text-align:center;
			}
			
			.highlight-icon-holder > img
			{
				height:50px;
			}
			
			.subframe_single_message
			{
				width:80%;
				text-align:center;
				font-weight:bold;
				display:block;
				margin:0 auto;
				font-size:1.3em;
			}
			
			#progressBar
			{
				width:475px;
				margin-top:20px;
				margin-left:auto;
				margin-right:auto;
			}
			#name{
				width:200px;
				word-break: break-all;
    			height:auto;
    			white-space: normal;
			}
		</style>
		
		<script type="text/javascript">
		 
		
		</script>
	</head>
	<body>
		 
			<%
		    Cache<String, Boolean> csrfTokenCache = null;
		    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
		    session.setAttribute("csrfTokenCache", csrfTokenCache);
		    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
		    session.setAttribute ("csrftoken_session", csrftoken);
			ArrayList<Aut> autList = null; 
			TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();

			Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
			String status = (String) map.get("STATUS");
			String message = (String) map.get("MESSAGE");
			Api api=(Api)map.get("API");
			String lrnrPage = "";
			
			String lrnrInitStatus = (String) request.getSession().getAttribute("LRNR_INIT_STATUS");
			String lrnrInitMessage = (String) request.getSession().getAttribute("LRNR_INIT_MESSAGE");
			String apiFilterType=(String) map.get("API_FILTER_TYPE"); 
			String apiGroup = (String) map.get("API_GROUP");
			%>
		<div class='title'>
			<p>API's Details</p>
		</div>
		<form name='api_details_edit_form' id='api_details_edit_form' action='APIServlet' method='POST'>
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<input type='hidden' id='viewType' value='EDIT' />
			<input type='hidden'  id=requesttype  name='requesttype' value='Edit_Api' />
			<input type='hidden' id='isUrlValid' value='<%=api.isUrlValid() %>'/>
			<input type='hidden' id='apiFilterType' value='<%=apiFilterType %>'/>
			<input type='hidden' id='apiGroup' value='<%=apiGroup %>'/>
			<div class='toolbar'>
				<input type='button' class='imagebutton back' value='Back'
					id='btnBack' /> 
					<input type='button' class='imagebutton save'value='Save' id='btnSave' />
				<input type='button' id='btnRefreshOperations' value='Refresh Operations' class='imagebutton refresh' />
				<input type='button' value='Learn' id='btnLearn' class='imagebutton learn' />
				<input type='button' value='API Learning History' class='imagebutton history' id='btnApiLearningHistory'/>
				
				<!-- Modified by sumit for TJN27-166 start -->
				<%
				if(api.getType().startsWith("rest")) {
					%>
					<input type='button' value='New Operation' id='btnNew' name='btnNew' class='imagebutton new'/>
					<%
				}
				
				else {
					%>
					<input type='button' value='New Operation' id='btnNew' name='btnNew' class='imagebutton new'/>
					<%
				}
				//Modified by sumit for TJN27-166 end
				%>
				<input type='button' value='Remove' id='btnDelete' class='imagebutton delete' />
				
				
			</div>
	
			<div id='user-message'></div>
			<input type='hidden' id='scrStatus' value='<%=Utilities.escapeXml(status) %>'/>
			<input type='hidden' id='scrMessage' value='<%=Utilities.escapeXml(message) %>'/>
			
			<div class='form container_16'>
				<fieldset>
					<legend>Application Information</legend>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='txtApiCode'>API Code</label></div>
						<div class='grid_3'><input type='text' id='txtApiCode' readonly name='txtApiCode' value='<%=((api.getCode() == null) ? "" : api.getCode())%>' class='stdTextBox'/></div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='lstAppId'>Application</label></div>
						<div class='grid_3'>
							<input type='hidden' id='lstLrnrApplication' name='lstAppId' value='<%=api.getApplicationId() %>'/>
							<input type='hidden' id='lstAppId1' name='lstAppId1' class='stdTextBox' readonly value='<%=api.getApplicationId() %>'/>
							<input type='text' id='lstAppId' name='lstAppId' class='stdTextBox' readonly value='<%=Utilities.escapeXml(api.getApplicationName()) %>'/>
						</div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='lstAppId'>Group</label></div>
						<div class='grid_3'>
							<select class='stdTextBoxNew' id='existingGroup' name='txtApiGroup' value='' title='API Group' disabled>
							 <option value='<%=Utilities.escapeXml(api.getGroup()) %>'><%=Utilities.escapeXml(api.getGroup()) %></option>
							</select>
							 <input type='hidden' id='group' name='group' class='stdTextBox' readonly value='<%=Utilities.escapeXml(api.getGroup()) %>'/> 
						</div>
					</div>
				</fieldset>
				
				<div class='clear'></div>
				
				<fieldset>
					<legend>API Information</legend>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='txtApiName'>API Name</label></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_3'><input type='text' id='txtApiName' name='txtApiName' value='<%=((Utilities.escapeXml(api.getName()) == null) ? "" : Utilities.escapeXml(api.getName()))%>'maxlength="100" class='stdTextBox' mandatory='yes' maxlength ="100"/></div> 
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						<div class='clear'></div>
					</div>
					
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='txtApiType'>API Type</label></div>
						<div class='grid_3'>
							<input type='hidden' id='selType' value='<%=api.getType() %>'/>
							<select disabled id='txtApiType' name='txtApiType' class='stdTextBoxNew'>
								<%
								List<String> adapters = (List<String>) map.get("ADAPTERS");
								if(adapters != null) {
									for(String adapter : adapters) {
										%>
										<option value='<%=adapter %>'><%=adapter %></option>
										<%
									}
								}
								%>
							</select>
						</div>
					</div>
					
				<div class='clear'></div>
					
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='encryptionType'>Encode Type</label></div>
						<div class='grid_3'> 
						<select disabled id='encryptionType' name='encryptionType' class='stdTextBoxNew'>
							<option value='<%=Utilities.escapeXml(api.getEncryptionType()) %>'><%=Utilities.escapeXml(api.getEncryptionType()) %></option>
							</select>
							</div>
					</div>
					
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='encryptionKey'>Encode Key</label></div>
						<div class='grid_3'> 
						<input type='text' id='encryptionKey' name='encryptionKey' value='<%=((api.getEncryptionKey() == null) ? "" : api.getEncryptionKey())%>' class='stdTextBox'/>
							</div>
					</div>
					
					
			<div class='clear'></div>
			
					<div class='fieldSection grid_14'>
						<div class='clear'></div>
						<div class='grid_2'><label for='txtApiUrl'>URL</label></div>
						<div class='grid_11'>
							<%
							if(api.isUrlValid()) {
								%>
								<input type='text' class='stdTextBox long' style='width:623px;' value='<%=((api.getUrl() == null) ? "" : api.getUrl())%>' id='txtApiUrl' name='txtApiUrl' maxlength="200" mandatory='yes'/>
								<%
							} else {
								%>
								<input type='text' class='stdTextBox long invalid' title='URL is invalid or not reachable. Please correct the URL.' style='width:623px;' value='<%=((api.getUrl() == null) ? "" : api.getUrl())%>' id='txtApiUrl' name='txtApiUrl' maxlength="200" mandatory='yes'/>
								<%
							}
							%>
						</div>
					</div>
				</fieldset>
				
				<div class='clear'></div>
				
				<%
				if(api.getOperations() != null && api.getOperations().size() > 0) {
					%>
					
					<fieldset>
						<legend>Operations (<%=api.getOperations().size() %>)</legend>
						
						<div class='clear'></div>
						
						<div class='clear'></div>
						
						<div class='fieldSection grid_15' id='operationsInfo'>
							<input type='hidden' id='pagination_value' value='<%=lrnrPage%>'/>
							<table id='tblApiOperations' class='display tjn-default-data-table' cellspacing='0' width='100%'>
								<thead>
									<tr>
										<th class='nosort'><input type='checkbox' class='tbl-select-all-rows tiny' id='chk_all_operations'/></th>
										<th style="width:200px">Operation</th>
										<th class="text-center">Last Successful Learning</th>
										<th>Template</th>
										<th>No.of Fields Learnt </th>
									</tr>
								</thead>
								<tbody id='tblApiOperations_body'>
									<%
									for(ApiOperation operation:api.getOperations()) {
										String lastLearntMarkup = "Not Learnt";
										boolean template = false;
										if(operation.getLastSuccessfulLearningResult() != null) {
											SimpleDateFormat sdf = new SimpleDateFormat(TenjinConfiguration.getProperty("DATE_FORMAT"));
											lastLearntMarkup = sdf.format(operation.getLastSuccessfulLearningResult().getStartTimestamp().getTime());
											template = true;
										}
										
										%>
										<tr>
											<td><input type='checkbox' id='<%=Utilities.escapeXml(operation.getName()) %>' data-app-id='<%=api.getApplicationId() %>' data-api-code='<%=Utilities.escapeXml(api.getCode()) %>' ' data-operation-name='<%=Utilities.escapeXml(operation.getName()) %>'/></td>
											<td style="width:200px"><div id="name"><a data-app-id='<%=api.getApplicationId() %>' data-api-code='<%=Utilities.escapeXml(api.getCode()) %>' data-operation-name='<%=Utilities.escapeXml(operation.getName()) %>' class='edit-operation' href='#'><%=operation.getName() %></a></div></td>
											<td class="text-center"><%=lastLearntMarkup %></td>
											<%
											if(template){
												%>
												<td><a class='template-gen' href='#' app='<%=api.getApplicationId() %>' mod='<%=Utilities.escapeXml(api.getCode())%>' txnMode='API' operation='<%=Utilities.escapeXml(operation.getName()) %>'>Download</a></td>
												<%
											}else{
												%>
												<td>Not Available</td>
												<%
											}
											%>
											<%if(operation.getLastSuccessfulLearningResult() != null) { %>
											<td><%=operation.getLastSuccessfulLearningResult().getLastLearntFieldCount() %> </td>
											<%}else{ %>
											<td>N/A</td>
											<%} %>
										</tr>
										<%
									}
									%>
								</tbody>
							</table>
						</div>
						
							
					</fieldset>
					
					<%
				}
				%>
			</div>
			
		</form>
		
		<div class='subframe' id='ttd_download_options' style='left:20%'>
			<iframe frameborder="2" scrolling="no"  marginwidth="5" marginheight="5" style='height:500px;width:600px;' seamless="seamless" id='ttd-options-frame' src=''></iframe>
		</div>
		
		<div class='modalmask'></div>
		<div class='subframe' id='confirmation_dialog'style='left:20%'>
			<div class='subframe_title'><p>Please Confirm...</p></div>
			<div class='subframe_content'>
				<div class='highlight-icon-holder'>
					<img src='images/warning_confirm.png' alt='Please Confirm' />
				</div>
				<div class='subframe_single_message'>
					<p>Refreshing operations may remove manually created Operations. Do you still want to proceed?</p>
				</div>
			</div>
			<div class='subframe_actions'>
				<input type='button' id='btnCOk' value='Proceed anyway' class='imagebutton ok'/>
				<input type='button' id='btnCCancel' value='Do not Proceed' class='imagebutton cancel'/>
			</div>
		</div>
		<div class='subframe' id='progress_dialog'style='left:20%'>
			<div class='subframe_title' id='refreshTitle'><p>Refreshing Operations...</p></div>
			<div class='highlight-icon-holder' id='processing_icon'>
					
				</div>
			<div class='subframe_content'>
				<div class='subframe_single_message' id='message'>
					<p></p>
				</div>
				<div id="progressBar" class="default" style='margin:0 auto;'><div></div></div>
			</div>
			<div class='subframe_actions' id='pr_actions' style='display:none;'>
				<input type='button' id='btnPClose' value='Close' class='imagebutton cancel' callback='undefined'/>
			</div>
		</div>
	</body>
</html>