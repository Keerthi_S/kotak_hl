<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  prj_mail_setting.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
 
*/

-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Project E-mail Configuration</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel="SHORTCUT ICON" HREF="images/yethi.png">
<link rel='stylesheet' href='css/paginated-table.css' />
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/paginated-table.js'></script>
<script type='text/javascript' src='js/pages/prj_mail_setting.js'></script>
<script src="js/tables.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>


	
		
<style>
#errmsg
{
color: red;
}
</style>
</head>
<body>
		
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>

	<div class='title'>
		<p>Project E-mail Configuration</p>
		
	 
	</div>
	<form name='main_form' id='main_form' action='ProjectMailServlet' method='POST'>
		<div class='toolbar'>
			<input type='button' id='btnBack' value='Back' class='imagebutton back' />
			<input type='submit' id='btnSave' value='Save' class='imagebutton save' />
			<input type='hidden' id='projectid' name ='projectid' value='${projectMail.prjId}'/>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			
		</div>
		<div id='user-message'></div>
		<div class='form container_16'>
			<fieldset>
				<legend>Configuration Settings</legend>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_3'><label for='txtemail'>Enable E-mail Notifications</label></div>
					<div class='grid_2' style="margin-left:30px;"><input type="checkbox" id='txtemail' name='txtemail' value='${projectMail.prjMailNotification}'style='vertical-align: bottom;margin-top:6px;'/></div>
				</div>
				<div id="txtSub">
				<div class='fieldSection grid_16'>
					<div class='clear'></div>
					<div class='grid_3'><label for='txtmailsubject'>E-mail Subject</label></div>
					<div class='grid_12'><input type='text' id='txtmailsubject' name='txtmailsubject' value='${projectMail.prjMailSubject}' class='stdTextBox long' title='E-mail Subject' maxlength='256'/></div>
					<div class='clear'></div>
				</div>	
				<div class='grid_13' style='margin-left:20%; margin-top:15px'>
				<div id='dataGrid'>
				 <table id='txtDate' class='bordered' width='70%'>
					<thead>
						<tr>
						   <th style='text-align:center' colspan='2'>Help</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><b> $domain_name$</b></td><td>Name of the Domain</td>
						</tr>
						<tr>
							<td><b> $project_name$</b></td><td>Name of the Project</td>
						</tr>
						<tr>
							<td><b> $run_id$</b></td><td>Id of the Run</td>
						</tr>
						<tr>
							<td><b> $run_status$</b></td><td>Status of the Run</td>
						</tr>
						<tr>
							<td><b> $user_name$</b></td><td>Name of the User executed the test</td>
						</tr>
						<tr>
							<td><b> $test_set$</b></td><td>Name of the Test Set</td>
						</tr>
						<tr>
						<td><b>Note</b></td>
						<td><b>1. </b>Use space to separate continuous keywords.<br>
						<b>2. </b>If using '$' or '_' before or after keywords, give a space.
						</td>
						</tr>
					</tbody>
				</table>
				</div>
			</div>
			</div>
			</fieldset>
			
			<fieldset>
			<legend>Escalation Rules</legend>
			<div class='fieldSection grid_7'>
			
					<div class='clear'></div>
					<div class='grid_3'><label for='txtEscRule'>Escalation Rules</label></div>
					<div class='grid_2' style="margin-left:30px;"><input type="checkbox" id='txtEscRule' name='txtEscRule' value='${projectMail.prjMailEscalation}'style='vertical-align: bottom;margin-top:6px;'/></div>
			</div>
			<div class='clear'></div>
			<div id="txtEsc">
			<div class='fieldSection grid_16'>
			<div id='dataGrid'>
						<table id='emailEscalation-table' class='display tjn-default-data-table' style='width:100%;'>
							<thead>
								<tr>
									<th class='table-row-selector nosort'><input type='checkbox'  id='tbl-select-all-rows-tc' class='tbl-select-all-rows'/></th>
									<th>Escalation Rules</th>
									<th>Escalation Condition </th>
									
								</tr>
							</thead>
							<tbody>
									<c:forEach items="${EscalationRules }" var="EscalationRules">
									<c:set var = "index" scope = "session" value = '${fn:indexOf(EscalationRules.value, ",")}'/>
									<c:set var = "length" scope = "session" value = '${fn:length(EscalationRules.value)}'/>
									<c:set var = "i" scope = "session" value = '${fn:indexOf(projectMail.prjEscalationRuleDesc, "$")}'/>
									<c:set var = "j" scope = "session" value = '${fn:length(projectMail.prjEscalationRuleDesc)}'/>
								<tr>
									<td> 
											<input type="checkbox" name="escalationRules" value="${EscalationRules.key }" class="escalationRules" id="${ EscalationRules.key}"> </td>
											 
									<td>
									<div class='grid_12'>${fn:substring(EscalationRules.value, index+1,length)}
									</div>
									</td>
											<c:if test="${count>'0' }">
											 	<td> <input type="text" id="${ EscalationRules.key}${count}${count}" class="rulePercentage" name="rulePercentage"  value='${fn:substring(projectMail.prjEscalationRuleDesc,i+1,j-1)}'  maxlength='3'> 
											</c:if>
											
									
								</tr>
								<c:set var="count" value="${count +1}" scope="request"/>
								</c:forEach>
							</tbody>
					</table>
				</div>
			
						<div class='grid_3'><label for='txtEscalationMail'>Escalation Mail-ids</label></div>
						<div class='grid_12' style='padding-bottom:30px'>
								<input type='text' id='txtEscalationMail' name='txtEscalationMail' value='${projectMail.prjEscalationMails}' class='stdTextBox long' title='E-mail Subject' maxlength='1024'/>
								<div class='clear'></div>
								*Separate Mail-id's by comma and make sure cc mail users are not there in project users.
						</div>
				</div>	
			</div>
			
			</fieldset>
		</div>
	</form>
</body>
</html>