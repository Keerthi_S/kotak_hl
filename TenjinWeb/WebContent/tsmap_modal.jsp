<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  tsmap_modal.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!-- New File added for TENJINCG-189 (Sriram, 13th June, 2017) -->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
  
*/

-->

<%@page import="com.ycs.tenjin.project.TestSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
 <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Insert title here</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel='stylesheet' href='css/select2.min.css' />
		
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/select2.min.js'></script>
		<script type='text/javascript' src='js/pages/tsmap_modal.js'></script>
		<style>
			#footer {
   				 width:100%;
   				 position: absolute;
   				 left: 0;
    			 bottom: 0;
    			 text-align:center
			}
		</style>
	</head>
	<body>
		<%
		Cache<String, Boolean> csrfTokenCache = null;
	     csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
	     session.setAttribute("csrfTokenCache", csrfTokenCache);
	     UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
	     
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		
		Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("TSMAP_MODAL_MAP");
		
		List<TestSet> testSets = new ArrayList<TestSet> ();
		String status = "";
		String message = "";
		String selectedTests = "";
		String mode = "";
		if(map != null) {
			testSets = (List<TestSet>) map.get("TEST_SETS");
			status = (String) map.get("STATUS");
			message = (String) map.get("MESSAGE");
			selectedTests = (String)map.get("SELECTED_TESTS");
			mode = (String) map.get("MODE");
		}
		%>
		
		<input type='hidden' id='status' value='<%=status %>' />
		<input type='hidden' id='message' value='<%=message %>' />
		<input type='hidden' id='selectedTests' value='<%=selectedTests %>' />
		<input type='hidden' id='mode' value='<%=mode %>' />
		<input type='hidden' id='projid' value='<%=tjnSession.getProject().getId() %>'/>
		<div class='title'>
			<p>Add Test Cases to Test Set</p>
		</div>
		
		<form name='main_form' method='POST'>
			 <input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div id='user-message'></div>
			
			<div class='form container_16'>
			 
				
				<div class='grid_8' style='height:214px;margin-left:50px;text-align:center;'>
					<div id='existingTestSetsBlock'>
						<div class='grid_7' style='padding:5px;'>
							<p style='font-size:1.2em; font-weight:bold;'>Choose an existing Test Set</p>
						</div>
						<div class='clear'></div>
						<div class='grid_7'  style='padding:5px;'>
							<select name='existingTestSet' id='existingTestSet' class='stdTextBoxNew' style='width:100%;'>
								<option value='-1'>-- Choose a Test Set --</option>
								<%
								if(testSets != null){
									for(TestSet testSet : testSets) {
										%>
										<option value='<%=testSet.getId() %>'><%=testSet.getName() %></option>
										<%
									}
								}
								%>
							</select>
						</div>
						<div class='grid_7'  id='tcSequence' style='padding:5px;'>
						</div>
						
						
						<div class='grid_8'  id='note' style='text-align:left'>
						<p style='font-weight:bold;'>&nbsp Note:</p>
						<ul style="list-style-type:disc;">
 							 <li>If user did not select any sequence then Test cases will be added at last in Test set</li>
 							 <li>If user selected a sequence of Test case then selected Test cases will be mapped from that sequence</li>
						</ul>
						</div>
						<div class='grid_8'  id='note1' style='text-align:left'>
						<p style='font-weight:bold;'>&nbsp Note:</p>
						<li>
						The selected Testset does not have any mapped testcases so user can not select the sequence.
						</li>
						<ul style="list-style-type:disc;">
						</ul>
						</div>
						
					</div>
					
					<div id='newTestSetBlock'>
						<div class='grid_7' style='padding:5px;margin-top:20px;'>
							<p style='font-size:1.2em; font-weight:bold;'>Or, create a new Test Set</p>
						</div>
						<div class='clear'></div>
						<div class='grid_7'  style='padding:5px;'>
							<input type='text' id='newTestSet' name='newTestSet' class='stdTextBox' placeholder='Enter new Test Set Name' style='width:100%;'/>
						</div>
					</div>
				</div>
				<div class='toolbar' id="footer">
					<input type='button' id='btnOk' class='imagebutton ok' value='Go' />
					<input type='button' id='btnCancel' class='imagebutton cancel' value='Cancel'/>
				</div>
			
			</div>
		</form>
	</body>
</html>