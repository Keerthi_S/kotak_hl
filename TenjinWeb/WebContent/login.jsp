<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  login.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION

-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tenjin - Intelligent Enterprise Testing Engine - V 2.12</title>
<link rel='stylesheet' href='css/login.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/cssreset-min.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script src="js/formvalidator.js"></script>
<link rel="SHORTCUT ICON" HREF="images/yethi.png" />
<script type='text/javascript'>
	$(document).ready(
			function() {
				var login=$('#login').val();
				if(login=='fail'){
					
					$('#logincontrols').hide();
				}
				$('#txtLoginName').focus();

				$('#btnLogin').click(
						function() {
							/*added by Prem for VAPT issue fix starts*/
							//	var password=$('#txtPassword').val();
								/* var b64 = btoa(password);
								$('#txtPassword').val(b64);	         
								console.log(b64); */
								/* var password=document.getElementById('txtPassword');
								var hashObj = new jsSHA("SHA-256", "TEXT", {numRounds: 1});
								hashObj.update(password.value);
								var hash = hashObj.getHash("HEX");
								password.value = hash;
								 $('#txtPassword').val(hash); */
									/*added by Prem for VAPT issue fix ends*/

							var valres = validateForm('login-form');
							if (valres == 'SUCCESS') {
								return true;
							} else {
								$('.statusContainer').html(
										"<div class='errmsg'>" + valres
												+ "</div>");
								$('.statusContainer').show('fast');
								return false;
							}
						});

				$('#btnReset').click(function() {
					$('#login-form')[0].reset();
					$('#txtLoginName').focus();
				});
				 var message = $('#tjn-message').val();
				var screenState = $('#tjn-status').val();
				if(screenState === 'error') {
					var $errMessage = $("<div class='errmsg' />");
					$errMessage.text(message);
					$errMessage.appendTo($('.statusContainer'));
						}
				if(screenState === 'success') {
					if(message!="SUCCESS"){
					$('.statusContainer').html(
							"<div class='successmsg'>" + message
									+ "</div>");
					$('.statusContainer').show('fast');
						}
				}
				$('#btnGoToLogin').click(function(){
					window.location.href='login.jsp'
				});
				$('#btnResetPassword').click(function(){
					var question=$('#question').val();
					var answer=$('#answer').val();
					window.location.href='AuthenticationServlet?t=resetPassword&question='+question+'&answer='+answer;
				});
			});
</script>
</head>
<body>


	 <%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(120, TimeUnit.HOURS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
    
	<%
		String param=request.getParameter("param");
	%>
	<div class='wrapper'>


		<div class='content-pane' id='leftbar'>
			<div id='leftbar-top'></div>
			<div id='leftbar-center'></div>
			<div id='leftbar-bottom'></div>
		</div>
        <input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${screenState.message }' />
		
		<div class='content-pane' id='main'>
			<div id='loginbox'>
				<div id='applogo'>
					<img src='images/tenjin_logo_new.png'
						alt='Tenjin - Intelligent Testing Engine - V 1.0' />
				</div>

	

				<div id='loginform'>
					<form name='login-form' id='login-form'
						Action='AuthenticationServlet' method='POST'>
						<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
						
						<div id='logincontrols'>
							<div>
								<label for='txtLoginName'>User ID</label> <input type='text'
									id='txtLoginName' autocomplete="off" name='txtLoginName'
									title='User ID' mandatory='yes' class='stdTextBox'
									style='width: 200px' placeholder='Enter your UserID' />

							</div>
							<div>
								<label for='txtPassword'>Password</label> <input type='password'
									id='txtPassword' autocomplete="new-password" name='txtPassword'
									title='Password' mandatory='yes' class='stdTextBox'
									style='width: 200px' placeholder='Password' />

							</div>
							<div
								style='text-align: right; margin-top: 10px; padding-right: 75px;'>
								<input type='submit' value='Log In' class='imagebutton login'
									id='btnLogin' /> <input type='button' value='Clear'
									class='imagebutton reset' id='btnReset' />
									
							</div>
						</div>
						 <div style="font-size:0.9em; padding-left: 85px;">
                  <%if(param!=null&&!param.equalsIgnoreCase("")&&param.equalsIgnoreCase("resetpwd")){
                	  User user=(User)request.getSession().getAttribute("user");  
                	 %>
                	 <!-- modified by paneendra for VAPT fix starts -->
                	 <b>Please <a style="color:red;" href="TenjinPasswordResetServlet?param=reqresetpassword">click here</a> to reset your password</b>
                	 <%} %><!-- modified by paneendra for VAPT fix ends -->
                 </div>
					</form>
                 <input type='hidden' id='login' value='${login }'> 
                 <c:if test='${login eq "fail"}'>
                 <div id='resetPassword'>
               
                 	<p style='font-weight:bold; font-size:14px; color:blue; padding-left:40px;'> Please choose a security question and answer to reset password</p>
						<br>
					<div>
						<label>Question</label>
						<select id='question' name='question' class='stdTextBox long' style="width:300px" >
							
							<c:forEach var="entry" items="${securityQueAndAns}">
								<option value='${entry.key}'>${entry.key}</option>
							</c:forEach>
						</select>
						<div class='clear'></div>
						<label>Answer</label>
						<input type='text' id='answer' name='answer' class='stdTextBox long' style="width:290px"/>
					</div>
					<div style='text-align: right; margin-top: 10px; padding-right: 75px;'>
						<input type='button' value='Reset Password' class='imagebutton reset' id='btnResetPassword' /> 
						<input type='button' value='Go to login' class='imagebutton login' id='btnGoToLogin' />
					</div>
					</div>
				</c:if>
				   <div class='statusContainer'>                          
                 </div>
				</div>
				
				
			</div>

			<div style="width: 500px; margin: 0 auto;">
				<div id='copyrightbox'>
					<div id='ycslogo'>

						<%
							String tenjinProductType = TenjinConfiguration
									.getProperty("TENJIN_PRODUCT_TYPE");
							if (Utilities.trim(tenjinProductType).equalsIgnoreCase("MOBILE")
									|| Utilities.trim(tenjinProductType).equalsIgnoreCase(
											"ENTERPRISE")) {
						%>
						<img src='images/Mobile_Image.png'
							alt='Tenjin - Intelligent Testing Engine - Mobile'
							style="width: 75px; padding-bottom: 10px;" />
						<%
							}
						%>


						<img src='images/ycs_logo_full.png'
							alt='Yethi Consulting - People, Process, Technology' />
					</div>
					<div id='copyright-clause'>

						<p>
							Copyright &copy 2014 -
							<%=new java.text.SimpleDateFormat("yyyy").format(new java.util.Date()) %>
							Yethi Consulting Private Ltd.&nbsp;&nbsp;All Rights Reserved
						</p>

					</div>
				</div>
			</div>


		</div>
	</div>
</body>
</html>