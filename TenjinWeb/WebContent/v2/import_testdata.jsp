<!-- 
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:import_testdata.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
  -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="java.util.UUID"%>
<%@page import="com.google.common.cache.Cache"%>
<%@page import="com.google.common.cache.CacheBuilder"%>
<%@page import="java.util.concurrent.TimeUnit"%>
<!DOCTYPE html>
<html>
<head>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-2.1.1.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<link rel="stylesheet" href="css/jquery.dataTables.css">
<script type='text/javascript' src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src="js/tables.js"></script>


<script type='text/javascript' src='js/pages/v2/import_testdata.js'></script>
</head>
<body>

	<%
	Cache<String, Boolean> csrfTokenCache = null;
	csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
	session.setAttribute("csrfTokenCache", csrfTokenCache);
	UUID uuid = UUID.randomUUID();
	String csrftoken = uuid.toString();
	csrfTokenCache.put(csrftoken, Boolean.TRUE);
	session.setAttribute("csrftoken_session", csrftoken);
	%>
	<div class='title'>
		<p>Test Data</p>
	</div>
	<div class='toolbar'>
		<input type='button' value='Copy' id='btnImportFile'
			class="imagebutton copy1" />
	</div>
	<div id='user-message'></div>
	<input type='hidden' id='tjn-status' value='${screenState.status }' />
	<input type='hidden' id='tjn-message'
		value='${fn:escapeXml(screenState.message) }' />
	<div id='user-message'></div>

	<div class='form container_16'>
		<fieldset>
		<legend>Selection Criteria</legend>
		<div class="fieldSection grid_15">
		
			
			
			<div class='grid_3'>
				<label for='lstProjectUsers'>Project Users <span
					style="color: red">&nbsp;*</span></label>
			</div>
			<div class='grid_5'>
				<input type='hidden' id='SelectedUser' value='${SelectedUser}' /> 
				<select
					id='lstProjectUsers' name='lstProjectUsers'
					class='stdTextBox lstProjectUsers' title="User" mandatory='yes'>
					<option value='-1'>-- Select One --</option>
					<c:forEach items="${ProjectUsers}" var="ProjectUser">
						<option value='${ProjectUser}'>${ProjectUser}</option>
						
						<%-- <input type='hidden' id='projectUserId' name='projectUserId' value='${fn:escapeXml(ProjectUser.id)}'/> --%>
					</c:forEach>
				</select>
			</div>
			<div class="clear"></div>
			<div class='grid_3'>
				<label for='lstProjectApps'>Project Applications <span
					style="color: red">&nbsp;*</span></label> 
			</div>
			<div class='grid_4'>
				<input type='hidden' id='SelectedApp' value='${SelectedApp}' name = '${ProjectAut.id}' /> <select
					id='lstProjectApps' name='lstProjectApps'
					class='stdTextBox lstProjectApps' title="App"  mandatory='yes'>
					<option value='-1'>-- Select One --</option>
					<c:forEach items="${ProjectAuts}" var="ProjectAut">
					
						<option value='${ProjectAut.name}'>${ProjectAut.name}</option> 
						<%-- <input type="hidden" id='prjAppId' value='${ProjectAut.id}'> --%>
					</c:forEach>
				</select>
			</div>
			<div class='grid_3'>
				<input type='button' value='Show available data' id='showtestdata'
					class='imagebutton ok' />
			</div>
			</div>
		</fieldset>
	</div>

	<div id='user-message'></div>
	<div id='user-message'></div>

	<div class='form container_16'>
		<fieldset>
			<legend>Available Test Data</legend>
			<input type="hidden" id="csrftoken_form" name="csrftoken_form"
				value=<%=csrftoken%> />
			<div id='dataGrid'>
				<table id='testdatatable' class='display tjn-default-data-table'>
					<thead>
						<tr>
							<th class='nosort'><input type='checkbox'
								class='tbl-select-all-rows' /></th>
							<th>Application</th>
							<th>Function / API</th>
							<th>Test Data Template</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${testDataList}" var="testdatalist">
							<input type='hidden' id='uacId' name='uacId'
								value='${fn:escapeXml(testdatalist.projectId)}' />
							<tr>

								<td class='table-row-selector'><input type='checkbox'
									id='funCode' data-function-id='${testdatalist.function}'
									data-app-id='${testdatalist.appId}'
									value='${fn:escapeXml(testdatalist.function) } ' /></td>
								<td>${fn:escapeXml(testdatalist.appName)}</td>
								<td>${fn:escapeXml(testdatalist.function)}</td>
								<td><a href='#' class='template-gen' id="download"
									data-app='${fn:escapeXml(testdatalist.appId)}'
									data-func='${fn:escapeXml(testdatalist.function)}'
									data-fname='${fn:escapeXml(testdatalist.fileName)}'>${fn:escapeXml(testdatalist.fileName)}</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</fieldset>
	</div>
</body>

</html>