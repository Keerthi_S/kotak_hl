<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  group_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
/******************************************
* CHANGE HISTORY
* ==============
  

-->



<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Group</title>
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
		<link rel="Stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/data-grids.js'></script>
		<script type='text/javascript' src='js/tenjin-aut-selector.js'></script>
		<script>
		$(document).ready(function(){
		mandatoryFields('groupForm');
		});
		$(document).on('click','#btnCancel',function(){
			window.location.href="GroupServlet?t=list";
		});
		</script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<div class='title'>New Group</div>
	<input type='hidden' id='callback' value='${callback }' />
	
	<form name='groupForm' action='GroupServlet' method='POST' id='groupForm'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<div class='toolbar'>
			<input type='submit' value='Save' class='imagebutton save' id='btnSave'/>
			<input type='button' value='Cancel' class='imagebutton cancel' id='btnCancel'/>
		</div>
		
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
		<input type='hidden' name='del' value='new' />
		
		<div id='user-message'></div>
		<div class='form container_16'>
			<fieldset>
					<legend>Application Information</legend>
					<div class='fieldSection grid_8'>
						<div class='clear'></div>
						<div class='grid_2'><label for='lstApplication'>Application</label></div>
						<div class='grid_5'>
							<tjn:applicationSelector mandatory='yes' title='Application' cssClass="stdTextBoxNew" groupSelector="existingGroup" loadFunctionGroups="true" id="aut" name="aut" defaultValue="${aut.id }" />
						</div>
					</div>
			</fieldset>
			<fieldset>
					<legend>Group Information</legend>
					<div class='fieldSection grid_8'>
						<div class='clear'></div>
						<div class='grid_2'><label for='txtGroupName'>Group Name</label></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_4'><input type='text' class='stdTextBox' id='txtGroupName' value='${fn:escapeXml(group.groupName) }' name='groupName' title='Group Name' mandatory='yes'Maxlength ='100' placeholder='MaxLength 100'/></div>
					     <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>
					<div class='fieldSection grid_16'>
						<div class='clear'></div>
						<div class='grid_2'><label for='txtGroupDesc'>Description</label></div>
						<!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_13'><textarea name='groupDesc' id='txtGroupDesc' class='stdTextBox long' style='height:72px;' maxlength="1000">${fn:escapeXml(group.groupDesc) }</textarea></div>					
					    <!-- Modified by Priyanka for TENJINCG-1233 starts -->
					</div>
			</fieldset>		
		</div>
	</form>

</body>
</html>