<!-- 

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  test_case_report.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 *
 * DATE              	CHANGED BY              DESCRIPTION
   03-02-2021			Paneendra	            Tenj210-158 
  
 --> 

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Test Case Report</title>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
	<link rel="Stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/jquery-ui.css' />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/data-grids.js'></script>
	<script type="text/javascript" src='${pageContext.request.contextPath}/js/jquery-1.11.4-ui.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/v2/test_case_report.js'></script>
</head>
<body>
<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
	<div class='title'>
		<p>Test Case Report</p>
	</div>
	<form name='main_form' action='TestReportServlet' method='POST'>
		<div class='toolbar'>
		      <!-- added by shruthi for CSRF token starts -->
		     <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		     <!-- added by shruthi for CSRF token ends -->
			<input type='button' value='Generate' id='btnGenerate' class='imagebutton download' />
			<img src='images/inprogress.gif' id='img_report_loader' />
		</div>
		<input type='hidden' id='tjn-status' value='${fn:escapeXml(screenState.status) }' />
		<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
		<div id='user-message'></div>
		<div class='form container_16'>
			<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'><label for='tcReport'>Report For</label></div>
				<div class='grid_4'>
					<select class='stdTextBoxNew' id='tcReport' name='tcReport' mandatory='yes'>
						<option value=''>--Select One--</option>
						<option value='1'>Test Case Execution Detail</option>
						<option value='2'>Test Case Execution History</option>
					</select>
				</div>
			</div>
			<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'><label for='reportType'>Report Type</label></div>
				<div class='grid_4'>
					<select class='stdTextBoxNew' id='reportType' name='reportType'>
						<option value='pdf'>PDF report</option>
						<option value='excel'>Excel report</option>
					</select>
				</div>
			</div><div class='clear'></div>
			<fieldset>
				<legend>Report Criteria</legend>
			</fieldset>
			<div id='caseExecDetail'>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='execDetailTC'>Test Case</label></div>
					<div class='grid_4'>
						<select class='stdTextBoxNew' id='execDetailTC' name='execDetailTC' mandatory='yes'>
							<option value=''>--Select One--</option>
							<c:forEach items="${testcases}" var="tc">	
								<option value='${fn:escapeXml(tc.tcRecId)}:${fn:escapeXml(tc.tcId)}:${fn:escapeXml(tc.tcName)}'>${fn:escapeXml(tc.tcId)}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='execDetailExOn'>Executed On</label></div>
					<!-- 	Modified by Paneendra for  Tenj210-158 starts -->
					<!-- <div class='grid_4'>
						<input type='text' id='execDetailExOn' name='execDetailExOn' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
					</div> -->
					
					<div class='grid_4'>
							<select class='stdTextBoxNew' id='execDetailExOn' name='execDetailExOn' mandatory='yes'>
								<option value=''>--Select One--</option>
							</select>
						</div>
						<!-- 	Modified by Paneendra for  Tenj210-158 ends -->
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='execDetailExBy'>Executed By</label></div>
					<div class='grid_4'>
						<select class='stdTextBoxNew' id='execDetailExBy' name='execDetailExBy' mandatory='yes'>
							<option value='all'>All</option>
							<c:forEach items="${users }" var="user">	
								<option value='${fn:escapeXml(user.id)}'>${fn:escapeXml(user.id)}</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			<div id='caseExecHistory'>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='execDetailTC1'>Test Case</label></div>
					<div class='grid_4'>
						<select class='stdTextBoxNew' id='execDetailTC1' name='execDetailTC1' mandatory='yes'>
							<option value=''>--Select One--</option>
							<c:forEach items="${testcases}" var="tc">	
								<option value='${fn:escapeXml(tc.tcRecId)}:${fn:escapeXml(tc.tcId)}:${fn:escapeXml(tc.tcName)}'>${fn:escapeXml(tc.tcId)}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='execHistExecBy'>Executed By</label></div>
					<div class='grid_4'>
						<select class='stdTextBoxNew' id='execHistExecBy' name='execHistExecBy' mandatory='yes'>
							<option value='all'>All</option>
							<c:forEach items="${users }" var="user">	
								<option value='${fn:escapeXml(user.id)}'>${fn:escapeXml(user.id)}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='execHistDateFrom'>Date From</label></div>
					<!-- 	Modified by Paneendra for  Tenj210-158 starts -->
					<!-- <div class='grid_4'>
						<input type='text' id='execHistDateFrom' name='execHistDateFrom' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
					</div> -->
					<div class='grid_4'>
							<select class='stdTextBoxNew' id='execHistDateFrom' name='execHistDateFrom' mandatory='yes'>
								<option value=''>--Select One--</option>
							</select>
						</div>
						<!-- 	Modified by Paneendra for  Tenj210-158 ends -->
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='execHistDateto'>Date To</label></div>
					<!-- 	Modified by Paneendra for  Tenj210-158 starts -->
					<!-- <div class='grid_4'>
						<input type='text' id='execHistDateto' name='execHistDateto' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
					</div> -->
					<div class='grid_4'>
							<select class='stdTextBoxNew' id='execHistDateto' name='execHistDateto' mandatory='yes'>
								<option value=''>--Select One--</option>
							</select>
						</div>
						<!-- 	Modified by Paneendra for  Tenj210-158 ends -->
				</div>
				
			</div>
		</div>
	</form>
</body>
</html>