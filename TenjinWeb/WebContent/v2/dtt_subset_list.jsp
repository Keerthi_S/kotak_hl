<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  dtt_subset_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
  -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DTT Subset List</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src=' js/bootstrap.min.js'></script>
		<script src="js/tables.js"></script>
		<script >
		$(document).on('click','#btnNew', function() {
				window.location.href ='DTTSubsetServlet?param=new';
			});
		
		$(document).on('click','#btnDelete', function() {
			if($('#subset-table').find('input[type="checkbox"]:checked').length === 0 || ($('#subset-table').find('input[type="checkbox"]:checked').length == 1 && $('#subset-table').find('input[type="checkbox"]:checked').hasClass('tbl-select-all-rows'))) {
				showLocalizedMessage('no.records.selected', '', 'error');
				return false;
			}
			
			clearMessages();
			
			if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
				var $subsetForm = $("<form action='DTTSubsetServlet' method='POST' />");
				$subsetForm.append($("<input type='hidden' id='del' name='del' value='true' />"))
				$subsetForm.append($("<input type = 'hidden' id='csrftoken_form' name ='csrftoken_form' value='"+$('#csrftoken_form').val()+"'/>"));
				
				$('#subset-table').find('input[type="checkbox"]:checked').each(function() {
					if(!$(this).hasClass('tbl-select-all-rows')) {
						$subsetForm.append($("<input type='hidden' name='selectedSubset' value='"+ $(this).data('subsetrecid') +"' />"));
						console.log( $('#subsetName').val());
						$subsetForm.append($("<input type='hidden' name='selectedsubsetName' value='"+ $(this).data('subsetName') +"' />"));
					}
				});
				$('body').append($subsetForm);
				$subsetForm.submit();
	}
		});
		
		</script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<div class='title'>
<p>DTT Subset List</p>
</div>

<form name='main_form' action='DTTSubsetServlet' method='POST'>
<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
	<div class='toolbar'>
		<input type='button' value='New' id='btnNew' class='imagebutton new'/>
		<input type='button' value='Remove' id='btnDelete' class='imagebutton delete'/>
	</div>
	<input type='hidden' id='tjn-status' value='${screenState.status }' />
	<input type='hidden' id='tjn-message' value='${screenState.message }' />
	<div id='user-message'></div>
	<div class='form container_16'>
		<fieldset>
			<legend>DTT Subsets</legend>
		</fieldset>
		<div id='dataGrid'>
			<table id='subset-table' class='display tjn-default-data-table'>
				<thead>
					<tr>
						<th class='table-row-selector nosort'><input type='checkbox'  id='tbl-select-all-rows-subset' class='tbl-select-all-rows'  /></th>
						<th>Subset Name</th>
						<th>Instance Name </th>
						<th>Tool</th>
						<th>Number of Projects</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach items="${subsetLists}" var="subset">
				 
				<input type='hidden' id='subsetName' name='subsetName' value='${fn:escapeXml(subset.subSetName) }' />
				<tr>
				<td class='table-row-selector'><input type='checkbox' id='${subset.prjId }' data-subsetrecid='${subset.prjId}' data-subset-name='${ fn:escapeXml(subset.subSetName)}' /></td>
				<td><a href='DTTSubsetServlet?param=view&recId=${subset.prjId}' title='${fn:escapeXml(subset.subSetName) }' >${fn:escapeXml(subset.subSetName) }</a></td>
				<td>${fn:escapeXml(subset.instanceName)}</td>
				<td>${fn:escapeXml(subset.instance)}</td>
				<c:forEach items="${ProjectCount}" var="projectCount">
				<c:choose>
				<c:when test="${projectCount.key eq subset.subSetName}">
				<td>${fn:escapeXml(projectCount.value)}</td>
				 
				</c:when>
				</c:choose>
				</c:forEach>
				
				</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</form>
</body>
</html>