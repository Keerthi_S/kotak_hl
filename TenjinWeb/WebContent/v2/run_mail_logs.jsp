
<!-- 

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: run_mail_logs.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright &copy 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 
 -->


<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Insert title here</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/accordion.css' />
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel="Stylesheet" href="css/bordered.css" />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<style>
			h3.ui-accordion-header
			{
				font-weight:bold;
				margin:5px;
				background:#708090;
				padding:10px;
				color:white;
				border-top-right-radius:5px;
				border-bottom-right-radius:5px;
				cursor:pointer;
			}

			h3.ui-state-active
			{
				background:#778899;
			}
		</style>
		<script>
		$(document).on('click','#btnBack',function() {
			var runId=$('#runId').val();
			var callback=$('#callback').val();
			window.location.href="ResultsServlet?param=run_result&run="+runId+'&callback='+callback;
		});
		$(document).ready(function(){
			$('#accordion').accordion({heightStyle:'content'});
		});
		
		</script>
	</head>
	<body>
		<div class='title'>
			<p>Mail Logs for runId : ${runId}</p>
		</div>
		<div class='toolbar'>
			<input type='button' id='btnBack' value='Back' class='imagebutton back' /></div>
		<input type="hidden" id='runId' value='${runId}'/>
		<input type="hidden" id='callback' value='${callback}'/>
		<div class='form container_16' id='bodyContainer'>
		<c:choose>
				 <c:when test="${ empty mailInfo}">
					<div id='user-message' class='msg-err' style='display:block;'>
						<div class="msg-icon">&nbsp;</div>
						<div class="msg-text">No Mail Information for this run</div>
					</div>
				  </c:when>
				  <c:otherwise>
					<fieldset>
						<legend>Run Mail Logs</legend>
					</fieldset>
					<div id='dataGrid' style="padding-top: 10px">
					<div id='accordion'>
						<c:forEach items='${mailInfo}' var='mailInfo'>
							<c:choose>
								<c:when test="${mailInfo.sentStatus=='Fail'}">
									<h3 id="header-red" >${mailInfo.sentStatus}&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;${fn:substring(mailInfo.sentTime,0,19)}&nbsp;&nbsp;(Mail sending try time)</h3>
								</c:when>
								<c:otherwise>
									<h3 id="header-pass" >${mailInfo.sentStatus}&nbsp;&nbsp;-&nbsp;&nbsp;${fn:substring(mailInfo.sentTime,0,19)}</h3>
								</c:otherwise>
							</c:choose>
							<div>
								<div class='fieldSection grid_15'>
									<div class='grid_2'><label for='txtsentId'>Sender Id</label></div>
									<div class='grid_8'>
										<input type='text' id='txtsentId' name='SenderId' style="width:750px;"value='${mailInfo.senderId}' class='stdTextBox long info-holder' disabled= 'disabled'/>
									</div>
									<div class='clear'></div>
									<div class='clear'></div>
									<div class='grid_2'><label for='txtrecipientList'>Recipient List</label></div>
									<div class='grid_8'>
										<textarea  id='txtrecipientList' style="width:750px;height:70px;resize:none"name='RecipientList' class='stdTextBox long info-holder' disabled= 'disabled'>${mailInfo.recipientIds}</textarea>
									</div>
									<div class='clear'></div>
									<div class='clear'></div>
									
									<div class='grid_2'><label for='txtccList'>CC List</label></div>
									<div class='grid_8'>
										<textarea  id='txtccList' style="width:750px;height:70px;resize:none" name='CCList' class='stdTextBox long info-holder' disabled= 'disabled'><c:if test="${mailInfo.ccList != null}">${mailInfo.ccList}</c:if></textarea>
									</div>
									
								</div>
							</div>
						</c:forEach>
					</div>
					</div>
			 </c:otherwise>
			</c:choose>
		</div>
</body>
</html>