<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testset_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
 19-11-2020             Priyanka                TENJINCG-1231
  -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.project.TestCase"%>
<%@page import="com.ycs.tenjin.project.TestStep"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TestCase Details</title>

<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/ifr_main.css' />
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/style.css' />
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/tabs.css' />
<link rel="Stylesheet"
	href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/buttons.css' />
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/paginated-table.css' />
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/960_16_col.css' />
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/testcase.css' />
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/jquery-ui-1.13.0.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/paginated-table.js'></script>
<script type='text/javascript'
	src=' ${pageContext.request.contextPath}/js/bootstrap.min.js'></script>
<script type='text/javascript' src='js/pages/v2/testset_view.js'></script>
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/accordion.css' />



</head>
<body>
<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->

	<div class='title'>
		<p>Test Set Details</p>
		 
	</div>
	<form name='main_form' id='main_form' action='TestSetServletNew'
		method='POST'>
		<div class='toolbar'>
		    <!-- added by shruthi for CSRF token starts -->
		    <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		    <!-- added by shruthi for CSRF token ends -->
			<input type='button' value='Back' id='btnBack'
				class='imagebutton back' /> 
			<input type='button' value='Save'
				id='btnSave' class='imagebutton save' /> 
			 
				<input type='button' value='Copy' id='btnCopy' class='imagebutton copy1'/>
				<input type='hidden' id='downloadPath' name='downloadPath' value='${fn:escapeXml(downloadPath) }'/>
			 
				<c:if test="${not empty testset.tests && project.state=='A'}">
					<c:if test="${teststepCount!=0 }">
						<input type='button' value='Run Test' class='imagebutton runtestset' id='btnRun'/>
					</c:if>
				</c:if>
				
			<input type='button'
				value='Execution History' id='btnExecuteHistory'
				class='imagebutton history' /> 
				<input type='button' value='Upload' id='btnUpload' class='imagebutton upload'/>
				<c:if test="${not empty testset.tests }">
					<input type='button' value='Download' id='btnDownload' class='imagebutton download'/>
				</c:if>
				<img src='images/inprogress.gif'
				id='img_download_loader' style='display: none;' />
				<input type='button' value='Change History' id='history' class='imagebutton download'/>
		</div>
		<!--   Added by Priyanka for TENJINCG-1231 starts -->
        <input type='hidden' name='edate' id='endDate' value='${edate }'/>
        <!--   Added by Priyanka for TENJINCG-1231 ends -->
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
		<input type='hidden' id='domain' name='domain'
		 
			value='${project.domain}' /> <input type='hidden' id='projectName'
			name='projectName' value='${project.name }' /> <input type='hidden'
			id='projectId' name='projectId' value='${project.id }' /> <input
			type='hidden' id='user' name='user' value='${user }' />

		<div id='user-message'></div>

		<div class='form container_16'>
			<div class='fieldSection grid_8'>
				<div class='grid_2'>
					<label for='txtTestSetId'>Test Set ID</label>
				</div>
				<div class='grid_4'>
					<input type='text' id='txtTestSetId' name='id'
						value='${testset.id }' disabled='disabled' class='stdTextBox'
						 title='Test Set ID' />
				</div>
				</div>
				<div class='fieldSection grid_7'>
				 
				</div>
				<input type='hidden' id=txtTsRecId name='id'
					value='${testset.id }' />
				<div class='fieldSection grid_16'>
				<div class='clear'></div>

				<div class='grid_2'>
					<label for='txtTestSetName'>Name</label>
				</div>
				<div class='grid_13'>
				      <!-- Modified by Priyanka for TENJINCG-1233 starts -->
					<input type='text' id='txtTestSetName' name='name'
						value='${fn:escapeXml(testset.name) }' class='stdTextBox long' title='Name'
						maxlength="100" mandatory='yes'/>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					<input type='hidden'id='tc_original' value='${fn:escapeXml(testset.name) }'/>
				</div>
				</div>
				<div class='fieldSection grid_16'>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='txtTestSetDesc'>Description</label>
				</div>
				<div class='grid_13'>
					<textarea id='txtTestSetDesc' name='description' class='stdTextBox'
						style='width: 89%; height: 50px;' maxlength="3000">${testset.description }</textarea>
				</div>
				</div>
				<div class='clear'></div>
				<div class='fieldSection grid_8'>
				<div class='grid_2'><label for='lstType'>Type</label></div>
				<div class='grid_4'>
					<input type='hidden' id='selectedType' value='${testset.type }' /> 
					<select class='stdTextBoxNew' id='lstType' name='type' onload="defaultselect()">
						<option value='Functional'>Functional</option>
					</select>
				</div>
				</div>
				<div class='fieldSection grid_7'>
				<div class='grid_2'><label for='lstPriority'>Priority</label></div>
				<div class='grid_4'>
				   <!-- Modified by Priyanka for TENJINCG-1233 starts -->
					<input type='hidden' id='selectedPriority' value='${testset.priority }'maxlength="10" />
					 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					<select class='stdTextBoxNew' id='lstPriority' name='priority' onload="defaultselect()">
						<option value='Low'>Low</option>
						<option value='Medium'>Medium</option>
						<option value='High'>High</option>
					</select>
				</div>
				</div>
				<div class='clear'></div><div class='clear'></div>
			<fieldset>
				<legend>Details</legend>
				<div>
					<ul id='tabs'>
						<li>
						<li><a href='#test-steps-tab' class='teststeps maptcs'
							id='test-steps-tab-anchor'>Execution Blueprint</a>
						<li>
						<a href='#test-details-tab' class='testdetails details'
							id='test-details-tab-anchor'>Audit History</a>
					</ul>
					<div id='test-steps-tab' class='tab-section'>
						<div class='toolbar'>
							<input type='button' value='Edit' id='btnMapCases'
								class='imagebutton edit' /> 
						 
								<div style="float:right; padding-right:5px "><b><span style="font-size:12px">${tcCount } Test Case(s),</span> &nbsp
								<span style="font-size:12px"> ${teststepCount } Test Step(s)</span></b></div>
						</div>
						<div id='user-message-steps'></div>
						<div id='dataGrid'>
						<div id='accordion'>
							<c:if test="${not empty testset && not empty testcase}">
								<c:forEach items="${testcase }" var="testcases">
									<h3>${testcases.tcId }-${testcases.tcName }</h3>
										<div>
											<div>
		                         				<table id='teststeps-table' class='bordered'>
													<thead>
														<tr>
															<th class='text-center'>Step No.</th>
															<th>ID</th>
															<th>Description</th>
															<th>Type</th>
															<th>Application</th>

														</tr>
													</thead>
													<tbody id='tblNaviflowModules_body'>
															<c:if test="${empty testcases.tcSteps}">
																<tr>
																	<td colspan='5'>This test case has no steps</td>
																</tr>
															</c:if>
															<c:forEach items="${testcases.tcSteps}" var="teststep">
															<tr>
																			<td class='text-center'>${teststep.sequence }</td>
																		 <td><a href='TestStepServlet?t=view&key=${teststep.recordId}&callback=testset&setId=${testset.id }'>${teststep.id }</a></td>
																			<td>${teststep.shortDescription }</td>
																			<td>${teststep.type }</td>
																			<td>${teststep.appName }</td>
																
															</tr>
															</c:forEach> 
													</tbody>
												</table>
											</div>
										</div>
									</c:forEach> 
								</c:if>
							</div>
						</div>
					</div>

					

					
					<div id='test-details-tab' class='tab-section'>
						<div class='fieldSection grid_7'>
							<div class='clear'></div>
							<div class='grid_2'>
								<label for='txtCreatedBy'>Created By</label>
							</div>
							<div class='grid_4'>
								 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
								<input type='text' id='txtCreatedBy' name='createdBy'
									class='stdTextBox' title='Test Case ID'
									value='${testset.createdBy }' disabled='disabled' maxlength="40"/>
									 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
									<input type='hidden' id='txtCreatedBy' name='createdBy'
									class='stdTextBox' 
									value='${testset.createdBy }'  />
							</div>
						</div>

						<div class='fieldSection grid_7'>
							<div class='clear'></div>
							<div class='grid_2'>
								<label for='txtCreatedOn'>Created On</label>
							</div>
							<div class='grid_4'>
								<input type='text' value='${createdOn }'
									id='txtCreatedOn' name='createdOn' 
									mandatory='no' disabled='true' class='stdTextBox' />
								
							</div>
						</div>
						<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label for='txtUpdatedBy'>Last Updated By</label></div>
								<div class='grid_4'>
									<input type='text' id='txtUpdatedBy' name='tsUpdatedBy' class='stdTextBox'  title='Updated By' value='${testset.auditRecord.auditId eq 0 ? "N/A" : testset.auditRecord.lastUpdatedBy }' disabled='disabled'/>
								</div>
							</div>
							
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label for='txtUpdatedOn'>Last Updated On</label></div>
								<div class='grid_4'>
									<input type='text' value='${testset.auditRecord.auditId eq 0 ? "N/A" : testset.auditRecord.lastUpdatedOn }' id='txtUpdatedOn' name='tsUpdatedOn' title='Updated On' mandatory= 'no' disabled='true' class='stdTextBox' />
								</div>
							</div>
					
					</div>
				</div>
			</fieldset>
			
			<input type='hidden' id='moduleList' name='moduleList' value=''/>
			<input type='hidden' id='learnType' name='learnType' value='NORMAL'/>

		</div>
	</form>
	<div class='modalmask'></div>
	<div class='subframe' style='left: 3%'>
		<iframe frameborder="2" scrolling="auto" marginwidth="5"
			marginheight="5" style='height: 500px; width: 900px;'
			seamless="seamless" id='prj-sframe' src=''></iframe>
	</div>

	<div class='subframe' id='uploadExcel' style='position:absolute;top:100px;left:250px;'>
				 <iframe src='' scrolling="no" seamless="seamless"></iframe> 
			</div>
	<div class='subframe' id='historyCases'>
		<iframe frameborder="2" scrolling="no" marginwidth="5"
				marginheight="5" style='height: 350px; width: 600px; overflow: hidden;'
				seamless="seamless" id='history-sframe' src=''>
		</iframe>
	</div>
</body>
</html>