<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: ui_val_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 

-->


<%@page import="com.ycs.tenjin.util.Utilities"%>

<%@page import="com.ycs.tenjin.project.Project"%>

<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
    <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>New UI Validation - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/paginated-table.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/paginated-table.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/ui_val_master.js'></script>
		<script type='text/javascript' src='js/pages/ui_val_edit.js'></script>
	</head>
	<body>
	
<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
		<%
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		Project project = null;
		project = tjnSession.getProject();
		%>
		<div class='title'>
			<p>UI Validation Step Details</p>
		 
		</div>
		
		<form name='main_form' id='main_form' action='UIValidationServlet' method='POST'>
		   <!-- added by shruthi for CSRF token starts -->
		    <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		    <!-- added by shruthi for CSRF token ends -->
			<input type='hidden' id='action' name='action' value='update' />
			<input type='hidden' id='tstepRecId' name='testStepRecordId' value='${testStep.recordId}' />
			<input type='hidden' id='appId' value='${testStep.appId}' />
			<input type='hidden' id='functionCode' value='${testStep.moduleCode}' />
			<input type='hidden' id='recordId' name='recordId' value='${val.recordId}' />
		
			<div class='toolbar'>
				<input type='button' class='imagebutton back' value='Back' id='btnBack' />
				<input type='submit' class='imagebutton save' value='Save' id='btnSave' />
				<input type='button' class='imagebutton delete' value='Remove' id='btnDelete'/>
				
				<c:set var = "tcId" value="${testStep.parentTestCaseId}" ></c:set>
				 
				
				<c:set var = "tstepName" value="${testStep.id}" ></c:set>
				 
				 
			</div>
			
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			
			<div id='user-message'></div>
			<div class='toolbar-right'><p>Test Case: <span style="font-style:italic;font-size:1em;"title="${testStep.parentTestCaseId}"><c:out value="${tcId}"></c:out></span>, Step: <span style="font-style:italic;font-size:1em;margin-right:10px;"title="${testStep.id}"><c:out value="${tstepName}"></c:out></span></p></div>
			<div class='form container_16' style='float: left;' >
				<fieldset>
					<legend>Validation Type</legend>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='validationType'>Validation Type</label></div>
						<div class='grid_4'>
							<input type='text' title='${val.type.description}' readonly id='valiationTypeName' name='validationTypeName' value='${val.type.name }' class='stdTextBox' />
							<input type='hidden' id='validationType' name='validationType' value='${val.type.id}' />
							<div class='clear'></div>
							<p class='muted field_help' id='validationTypeHelp'></p>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'><label for='pageArea'>Page Area</label></div>
						<div class='grid_4'>
							<input type='text' id='pageArea' name='pageArea' value='${val.pageArea}' readonly class='stdTextBox' />
						</div>
					</div>
					
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='application'>Application</label></div>
						<div class='grid_4'>
							<input type='text' readonly id='application' name='application' value='${testStep.appName}' class='stdTextBox' />
							<input type='hidden' id='applicationId' name='applicationId' value='${testStep.appId}' />
							<div class='clear'></div>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'><label for='function'>Function</label></div>
						<div class='grid_4'>
							<input type='text' id='function' name='function' value='${testStep.moduleCode}' readonly class='stdTextBox' />
						</div>
					</div>
				</fieldset>
				
				<div class='clear'></div>
				
				<div id='fieldSelectionBlock'>
					<div class='fieldSection grid_7'>
						<fieldset>
							<legend>Fields to Validate</legend>
							<div class='fieldSection grid_7'>
								<p class='muted field_help' id='fieldToValidateHelp'>Select the fields you want to validate and specify expected outcome for each field</p>
								<img src='images/inprogress.gif' alt='Loading. Please Wait...' id='img_loadPages'/>
								<div class='clear'></div>
								<table id='tbl_fieldsToValidate' class='bordered'>
									<thead>
										<tr>
											<th><input type='checkbox' id='chk_all' class='tbl-select-all-rows' title='Select All Fields' /></th>
											<th style='width:170px;'>Field Name</th>
											<th style='width:170px;'>Expected Outcome</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</fieldset>
					</div>
					<div class='fieldSection grid_1'></div>
					<div class='fieldSection grid_7'>
						<fieldset>
							<legend>Pre-Validation Steps</legend>
							<div class='fieldSection grid_7'>
								<p class='muted field_help' id='preValidationStepsHelp'>Specify the list of actions to be performed before the field validations are performed.</p>
								<div class='clear'></div>
								<table id='tbl_preSteps' class='bordered'>
									<thead>
										<tr>
											<th ><input type='checkbox' id='pre_chk_all' class='tbl-select-all-rows' title='Select All Fields' /></th>
											<th style='width:170px;'>Field Name</th>
											<th style='width:170px;'>Input Data</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			
			<input type='hidden' id='valStepsJson' name='valStepsJson' value='${val.steps}' />
			<input type='hidden' id='preStepsJson' name='preStepsJson' value='${val.preSteps}' />
			<input type='hidden' id='val_formula' name='val_formula' value='${val.type.formula}' />
			<input type='hidden' id='val_def_exp_values' name='val_def_exp_values' value='${val.type.defaultExpectedValues}' />
		</form>
		
	</body>
</html>