<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testcase_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
  -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
	<link rel="Stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/data-grids.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/tenjin-aut-selector.js'></script>
	<script>
	/* Added by Ruksar for Tenj210-06 starts*/
	$(document).on('click','#btnSave',function(){
		var tcId=$('#txtTestCaseId').val();
		if(tcId!=""){
            if(/[^a-zA-Z0-9_]/.test(tcId)) {
                 showMessage("Only characters,numbers and '_'  are allowed for Testcase Id","error");
                 return false;
            }
        }
	});
	/* Added by Ruksar for Tenj210-06 ends*/
	$(document).on('click','#btnCancel',function(){
		var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
		if(confirmResult) {
		if($('#testCaseCopy').val()=='true'){
			window.location.href="TestCaseServletNew?t=testCase_View&paramval="+$('#txtTcRecId').val();
		}
		else{
			var user=$('#user').val();
			var projectName=$('#projectName').val();
			var domain=$('#domain').val();
			var pid=$('#projectId').val();
			window.location.href='TestCaseServletNew?t=list&user='+user+'&project='+projectName+'&domain='+domain+'&pid='+pid;
		}
		}else{
			return false;
		}
	});
	$(document).ready(function(){
		
		mandatoryFields('main_form');
		
		    if($('#stepSize').val()>0){
		    	$('#txtTestMode').prop('disabled',true);
		    	$('#txtTestMode').prop('title','Test case mode cannot be changed as it contains Test steps');
		    	
		    }
			$('#lstPriority').children('option').each(function(){
		        var tcPriority=$('#txtPriority').val();
		        var val = $(this).val();
		        if(val == tcPriority){
		            $(this).attr({'selected':true});
		        }
		    });
			
			$('#lstTestCaseType').children('option').each(function(){
		        var tcType=$('#txtTcType').val();
		        var val = $(this).val();
		        if(val == tcType){
		            $(this).attr({'selected':true});
		        }
		    });
			
			$('#txtTestMode').children('option').each(function(){
		        var tcMode=$('#lstTcMode').val();
		        var val = $(this).val();
		        if(val == tcMode){
		            $(this).attr({'selected':true});
		        }
		    });
			
			$(document).on('click','#btnRefresh',function(){
				window.location.href ="TestCaseServletNew?t=init_new_testcase";
			});
	
	});
	</script>
</head>
<body>
 <!-- commented by shruthi for TCGST-52 starts -->
<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
<!--  commented by shruthi for TCGST-52 ends -->
	<div class='title'>
		<p>New Test Case</p>
		 
	</div>
	<input type='hidden' id='callback' value='${callback }' />
	<form name='main_form' action='TestCaseServletNew' method='POST'>
		<div class='toolbar'>
		 <!-- commented by shruthi for TCGST-52 starts -->
		    <!-- added by shruthi for CSRF token starts -->
		     <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		     <!-- added by shruthi for CSRF token ends -->
			<!-- commented by shruthi for TCGST-52 ends -->
			<input type='submit' value='Save' id='btnSave' class='imagebutton save'/>
			<input type='button' value='Cancel' id='btnCancel'class='imagebutton cancel' />
			<input type="button" value="Refresh" id="btnRefresh" class="imagebutton refresh">
		</div>
		
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
		<input type='hidden' id='domain' name='domain' value='${project.domain}'/>
		<input type='hidden' id='projectName' name='projectName' value='${project.name }'/>
		<input type='hidden' id='projectId' name='projectId' value='${project.id }'/>
		<input type='hidden' id='user' name='user' value='${user }'/>
		<input type='hidden' id='txtParent' name='txtParent' value='-1'/>
		<input type='hidden' id='txtRecType' name='recordType' value='TC'/>
		<input type='hidden' id='txtTcRecId' name='tcRecId' value='${fn:escapeXml(testcase.tcRecId) }'/>
		<input type='hidden' id='stepSize' name='stepSize' value='${stepSize}'/>
		<input type='hidden' id='testCaseCopy' name='testCaseCopy' value='${testCaseCopy}'/>
		<input type='hidden' id='presetRules' name='presetRules' value='${testcase.tcPresetRules }'/>
		
 			
		<div id='user-message'></div>
		
		<div class='form container_16'>
		<fieldset>
			<legend>Test Case Details</legend> 
			
				<div class='fieldSection grid_8'>
				<div class='clear'></div>
				<div class='grid_2'><label for='txtTestCaseId'>Test Case ID</label></div>
				<div class='grid_4'>
					<input type='text' id='txtTestCaseId' name='tcId' class='stdTextBox'  title='Test Case ID' maxlength="100" value='${fn:escapeXml(testcase.tcId) }' mandatory='yes'/>
				</div>
				</div>
				<div class='fieldSection grid_16'>
				<div class='clear'></div>
				<div class='grid_2'><label for='txtTestCaseName'>Name</label></div>
				<div class='grid_13'>
					<input type='text' id='txtTestCaseName' name='tcName' class='stdTextBox long' mandatory='yes' title='Name' maxlength="200" value='${fn:escapeXml(testcase.tcName) }'/>
				</div>
				</div>
				<div class='fieldSection grid_16'>
				<div class='clear'></div>
				<div class='grid_2'><label for='txtTestCaseDesc'>Description</label></div>
				<div class='grid_13'>
					<textarea id='txtTestCaseDesc' name='tcDesc' class='stdTextBox' style='width:89%;height:100px;' maxlength="3000">${testcase.tcDesc }</textarea>
				</div>
				</div>
				<div class='fieldSection grid_8'>
					<div class='grid_2'><label for='aut'>Label</label></div>
					<div class='grid_4'>
						<input type='text' id='label' name='label' class='stdTextBox'  title='Lable' maxlength="100" value='${fn:escapeXml(testcase.label) }' />
					</div>
				</div>
				<div class='fieldSection grid_8'>
				<div class='grid_2'>
				<label for='aut'>Application</label></div>
				<div class='grid_4'>
					<tjn:applicationSelector  title='Application'  cssClass="stdTextBoxNew"  projectId='${project.id }'  id="aut" name="aut" defaultValue="${testcase.appId}"/>						
				</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Basic Information</legend>
				<div class='fieldSection grid_8'>
					<div class='clear'></div>
					<div class='grid_2'><label for='txtCreatedBy'>Created By</label></div>
					<div class='grid_4'>
						<input type='text' id='txtCreatedBy' name='tcCreatedBy' class='stdTextBox'  title='Test Case ID' value='${user }' disabled='disabled'/>
						<input type='hidden' id='txtCreatedBy' name='tcCreatedBy' value='${user }'/>
					</div>
						
					<div class='clear'></div>
					<div class='grid_2'><label for='lstTestCaseType'>Type</label></div>
					<div class='grid_4'>
					        <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<input type='hidden' id='txtTcType' name='txtTcType' value='${testcase.tcType }' maxlength="60">
							 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
							<select class='stdTextBoxNew' id='lstTestCaseType' name='tcType'>
								<option value='Acceptance'>Acceptance</option>
								<option value='Accessibility'>Accessibility</option>
								<option value='Compatibility'>Compatibility</option>
								<option value='Regression'>Regression</option>
								<option value='Functional'>Functional</option>
								<option value='Smoke'>Smoke</option>
								<option value='Usability'>Usability</option>
								<option value='Other'>Other</option>
							</select>
					</div>
				</div>
				<div class='fieldSection grid_8'>
					<div class='clear'></div>
					<div class='grid_2'><label for='txtCreatedOn'>Created On</label></div>
					<div class='grid_4'>
						<input type='text' value='<%=new Date() %>' id='txtCreatedOn' name='tcCreatedOn' title='Created On' mandatory= 'no' disabled='true' class='stdTextBox' />
					</div>
						
					<div class='clear'></div>
					<div class='grid_2'><label for='lstPriority'>Priority</label></div>
					<div class='grid_4'>
					 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
					  <input type='hidden' id='txtPriority'  name='txtPriority' value='${testcase.tcPriority }' maxlength="10">
					 <!-- Modified by Priyanka for TENJINCG-1233 ends -->	
						<select class='stdTextBoxNew' id='lstPriority' name='tcPriority' >
							<option value='Low'>Low</option>
							<option value='Medium'>Medium</option>
							<option value='High'>High</option>
						</select>
					</div>
				</div>
			</fieldset>
		</div>
	</form>

</body>
</html>