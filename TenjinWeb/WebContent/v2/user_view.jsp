<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  user_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 				

-->

<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/v2/user_view.js'></script>	
		<link rel="stylesheet" href="css/jquery.dataTables.css">
		<script type='text/javascript' src="js/jquery.dataTables.js"></script>
		<script  type='text/javascript' src="js/tables.js"></script>
		
		<link rel='stylesheet' href='css/jquery-ui.css' />
		<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">

		<style>
		#selectedFiles {
			margin-left: 0;
		}
		.att_block {
			padding: 5px;
			border: 1px solid #707a86;
			background-color: #b7c0ef;
			border-radius: 5px;
			margin-left: 0;
		}
		.att_block p {
			overflow: hidden;
			text-overflow: ellipsis;
		}

		#uploadTestCases, #syncTestCases, #def_linkage_dialog {
			background-color: #fff;
		}
		.subframe_content {
			display: block;
			width: 100%;
			margin: 0 auto;
			min-height: 120px;
			padding-bottom: 50px;
			padding-left: 116px;
			
		}
		.subframe_title {
			width: 644px;
			height: 20px;
			background: url('images/bg.gif');
			color: white;
			font-family: 'Open Sans';
			padding-top: 6px;
			padding-bottom: 6px;
			padding-left: 6px;
			font-weight: bold;
			margin-bottom: 10px;
		}
		.subframe_actions {
			display: inline-block;
			width: 100%;
			background-color: #ccc;
			padding-top: 5px;
			padding-bottom: 5px;
			font-size: 1.2em;
			text-align: center;
		}
		.highlight-icon-holder {
			display: block;
			max-height: 50px;
			margin-bottom: 10px;
			text-align: center;
		}
		.highlight-icon-holder>img {
			height: 50px;
		}
		.subframe_single_message {
			width: 80%;
			text-align: center;
			
			display: block;
			margin: 0 auto;
			font-size: 1.0em;
		}
		#progressBar {
			width: 475px;
			margin-top: 20px;
			margin-left: auto;
			margin-right: auto;
		}
		p.subtitle {
			display: block;
			float: right;
			font-weight: bold;
			font-size: 12px;
			line-height: 22px;
			margin-right: 10px;
			color: red;
		}
		.import_message{
			font-size: 1.1em;
		}
		.importbutton{
			font-size: 1.24em;
   	    	height: 30px;
    		margin-right: 18px
		}
		#import_buttons{
			padding-top:39px ; 
			padding-left:60px;
		}
		

/
.modal-open {
	overflow: hidden;
}

.modal {
	display: none;
	overflow: hidden;
	position: fixed;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	z-index: 1050;
	-webkit-overflow-scrolling: touch;
	outline: 0;
}

.modal.fade .modal-dialog {
	-webkit-transform: translate(0, -25%);
	-ms-transform: translate(0, -25%);
	-o-transform: translate(0, -25%);
	transform: translate(0, -25%);
	-webkit-transition: -webkit-transform 0.3s ease-out;
	-o-transition: -o-transform 0.3s ease-out;
	transition: transform 0.3s ease-out;
}

.modal.in .modal-dialog {
	-webkit-transform: translate(0, 0);
	-ms-transform: translate(0, 0);
	-o-transform: translate(0, 0);
	transform: translate(0, 0);
}

.modal-open .modal {
	overflow-x: hidden;
	overflow-y: auto;
}

.modal-dialog {
	position: relative;
	width: auto;
	margin: 10px;
}

.modal-content {
	position: relative;
	background-color: #ffffff;
	border: 1px solid #999999;
	border: 1px solid rgba(0, 0, 0, 0.2);
	border-radius: 6px;
	-webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
	box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
	-webkit-background-clip: padding-box;
	background-clip: padding-box;
	outline: 0;
}

.modal-backdrop {
	position: fixed;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	z-index: 1040;
	background-color: #000000;
}

.modal-backdrop.fade {
	opacity: 0;
	filter: alpha(opacity = 0);
}

.modal-backdrop.in {
	opacity: 0.5;
	filter: alpha(opacity = 50);
}

.modal-header {
	padding: 15px;
	border-bottom: 1px solid #e5e5e5;
	min-height: 16.42857143px;
}

.modal-header .close {
	margin-top: -2px;
}

.modal-title {
	margin: 0;
	line-height: 1.42857143;
}

.modal-body {
	position: relative;
	padding: 15px;
}

.modal-footer {
	padding: 15px;
	text-align: right;
	border-top: 1px solid #e5e5e5;
}

.modal-footer .btn+.btn {
	margin-left: 5px;
	margin-bottom: 0;
}

.modal-footer .btn-group .btn+.btn {
	margin-left: -1px;
}

.modal-footer .btn-block+.btn-block {
	margin-left: 0;
}

.modal-scrollbar-measure {
	position: absolute;
	top: -9999px;
	width: 50px;
	height: 50px;
	overflow: scroll;
}

.close {
	float: right;
	font-size: 21px;
	font-weight: bold;
	line-height: 1;
	color: #000;
	text-shadow: 0 1px 0 #fff;
	filter: alpha(opacity = 20);
	opacity: .2;
}

button.close {
	-webkit-appearance: none;
	padding: 0;
	cursor: pointer;
	background: transparent;
	border: 0;
}

.close:hover, .close:focus {
	color: #000;
	text-decoration: none;
	cursor: pointer;
	filter: alpha(opacity = 50);
	opacity: .5;
}

#btnImportLogs {
	background-colour: blue;
}
.text-center{
	text-align:center
}
</style>
<style type="text/css" media="all">
.ui-autocomplete { 
    position: absolute; 
    cursor: default; 
    height: 200px; 
    overflow-y: scroll; 
    overflow-x: hidden;
    }
</style>
		<style>
		 
		.text-center{
			text-align:center
		}
		</style>
		</head>
		<c:set var="loggedInUser" value="${sessionScope.TJN_SESSION.user.id }"></c:set>
		<c:set var="userRole" value="${sessionScope.TJN_SESSION.user.roles }"></c:set>
	<body>
	<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
		<div class='title'>
			<p>User Details</p>
		</div>
	<input type='hidden' id='status' value='${user.enabledStatus}' />
	<input type='hidden' id='cUserId' value='${loggedInUser}' />
	<input type='hidden' id='userRole' value='${fn:escapeXml(userRole)}' />
		<form id='userForm' name='userForm' action='TenjinUserServlet' method='POST'>
			<div class='toolbar'>
		    <!-- added by shruthi for CSRF token starts -->
		    <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		    <!-- added by shruthi for CSRF token ends -->
			<input type='button' id='btnBack' value='Back' class='imagebutton back' />
			<input type='submit' id='btnSave' value='Save' class='imagebutton save' />
			
				
				<c:choose>
				<c:when test="${user.enabledStatus =='N'}">
				<input type='button' class='imagebutton enable' value='Enable' id='btnEnable' />
				</c:when>
				<c:otherwise>
				<input type='button' class='imagebutton diasble' value='Disable' id='btnEnable' />
				</c:otherwise>
				</c:choose>
	
				
				<input type='button' id='btnChangePwd' value='Change Password' class='imagebutton login'/>
				<!-- commented by shruthi for TCGST-67 starts -->
				<!-- <input type='button' id='btnPwdReport' value='Password Report' class='imagebutton download'/> -->
				<!-- commented by shruthi for TCGST-67 ends -->
				 			</div>
			
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message )}' />
			<input type="hidden" name="operation" value="update"/>
			
			<div id='user-message'></div>
			
			<div class='form container_16'>
				
					<fieldset>
					<legend>Basic Information</legend>
					<div class='fieldSection grid_7'>
					
						<input type='hidden' id='id' name='id' value='${fn:escapeXml(user.id) }' />
						<div class='grid_2'><label for='id'>User Id</label></div>
						<div class='grid_4'><input type='text' class='stdTextBox' value='${fn:escapeXml(user.id )}' id='id' name='id' disabled="disabled"/></div>
					</div>
					<div class='fieldSection grid_7'>
					<div class='grid_2'><label for='id'>User Status</label></div>
						<div class='grid_4'>
							<c:choose>
								<c:when test="${user.enabledStatus eq 'Y'}">
									<b><input type='text' class='stdTextBox' value='Enabled' style="color:green;" disabled="disabled"/></b>
								</c:when>
								<c:otherwise>
									<b><input type='text' class='stdTextBox' value='Disabled' style="color:red;" disabled="disabled"/></b>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
						<div class='clear'></div>
					<div class='fieldSection grid_7'>		
						<div class='grid_2'><label for='firstName'>First Name</label></div>
						<!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_4'><input type='text' class='stdTextBox' value='${fn:escapeXml(user.firstName) }' id='firstName' name='firstName' maxlength='15' mandatory='yes' placeholder="MaxLength 15"/></div>
					     <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>	
					
					<div class='fieldSection grid_7'>	
						<div class='grid_2'><label for='lastName'>Last Name</label></div>
						<!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_4'><input type='text' class='stdTextBox' value='${fn:escapeXml(user.lastName) }' id='lastName' name='lastName' maxlength='15'placeholder="MaxLength 15"/></div>
					    <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>
					</fieldset>	
					<fieldset>
					<legend>Contact Information</legend>
					<div class='fieldSection grid_16'>	
					
							
							<div class='clear'></div>
							
							<div class='grid_2'><label for='primaryPhone'>Contact No</label></div>
							<!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<div class='grid_4'><input type='text' class='stdTextBox' value='${fn:escapeXml(user.primaryPhone) }' id='primaryPhone' name='primaryPhone' maxlength='15' placeholder="MaxLength 15"/></div>
							<!-- Modified by Priyanka for TENJINCG-1233 ends -->
							<div class='clear'></div>
							
							<div class='grid_2'><label for='email'>Email Id</label></div>
							<div class='grid_12'><input type='text' class='stdTextBox long' value='${fn:escapeXml(user.email )}' id='email' name='email'maxlength='100' mandatory='yes'/></div>
					</div>
					</fieldset>	
					<fieldset>
					<legend>Security Information</legend>
					<div class='fieldSection grid_7'>
						
							<div class='grid_2'><label for='roles'>Roles</label></div>
							<div class='grid_4' style='margin-top:2px;'>
							 
							 
							<c:choose>
							<c:when test="${user.roles=='Site Administrator' }">
							<select name="roles" id="roles" class='stdTextBoxNew'>
							<option selected="selected" value='1'>Site Administrator</option>
							<option value='3'>Tenjin User</option>
							</select>
							</c:when>
							<c:otherwise>
							<select name="roles" id="roles" class='stdTextBoxNew'>
							
							<option value='1'>Site Administrator</option>
							<option selected="selected" value='3'>Tenjin User</option>
							</select>
							</c:otherwise>
							</c:choose>
							</div>
							<div class='clear'></div>
							
							
							
							<div class='clear'></div>
							
					</div>
				</fieldset>
				<fieldset>
					<legend>Assigned Projects</legend>
					<div class='fieldSection grid_16'>
					<table class='display tjn-default-data-table'>
						<thead>
							<tr>
								<th class="text-center">S.No.</th>
								<th>Domain Name</th>
								<th>Project Name</th>
								<th>Status</th>
								<th>Project User Role</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${projects.size() eq 0}">
									<tr>
										<td colspan='4' align='center'>No Project assigned to user.</td>
									</tr>
								</c:when>
								<c:otherwise>
								<c:set var="count" value="0" />
									<c:forEach items="${projects }" var="project">
									<c:set var="count" value="${count + 1}"/>
										<tr>
											<td class="text-center">${count}</td>
											<td>${fn:escapeXml(project.domain )}</td>
											<td>${fn:escapeXml(project.name )}</td>
											<c:choose>
												<c:when test="${project.state eq 'A'}">
													<td><b><span style="color:green;">Active</span></b></td>
												</c:when>
												<c:otherwise>
													<td><b><span style="color:red;">Inactive</span></b></td>
												</c:otherwise>
											</c:choose>
											<td>${project.userRole}</td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					</div>
				</fieldset>
			</div>
		</form>
		<form id='userForm1' name='userForm1' >
		<div class='modalmask'></div>
		<div class='subframe' id='uploadTestCases' style='position:absolute;top:100px;left:250px;'>
				<div class='subframe_title'><p>Please Choose start and end Date</p></div>
				<div class='subframe_content'>
					
				<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'><label for='reportType'>Report Type</label></div>
				<div class='grid_4'>
					<select class='stdTextBoxNew' id='reportType' name='reportType'>
						<option value='pdf'>PDF report</option>
						<option value='excel'>Excel report</option>
					</select>
				</div>
				</div><div class='clear'></div>
				
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='appReportDateFrom'>Date From</label></div>
					<div class='grid_4'>
						<input type='text' id='appReportDateFrom' name='appReportDateFrom' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
					</div>
				</div>
				<div class='clear'></div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='appReportDateTo'>Date To &nbsp&nbsp&nbsp&nbsp</label></div>
					<div class='grid_4'>
						<input type='text' id=appReportDateTo name='appReportDateTo' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
					</div>
				</div>	
				</div> 
					
			
				<div class='subframe_actions'>
					<input type='button' id='btnOk' value='Ok' class='imagebutton ok'/>
					<input type='button' id='btnCCancel' value='Cancel' class='imagebutton cancel'/> 
				</div>
	 	</div>
		</form>
	</body>
</html>