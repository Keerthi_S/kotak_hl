<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  project_dashboard.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright ? 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 09-06-2020				Ruksar					v210Reg-1
 

-->


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
<link rel='stylesheet' href='css/project_dashboard.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script src="js/pages/v2/project_dashboard.js"></script>
<script type="text/javascript" src="js/jquery.jqplot.min.js"></script>
<script type="text/javascript"
	src="js/dist/plugins/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript"
	src="js/dist/plugins/jqplot.categoryAxisRenderer.js"></script>
<script type="text/javascript"
	src="js/dist/plugins/jqplot.pointLabels.js"></script>
<script type="text/javascript" src="js/dist/plugins/jqplot.cursor.js"></script>
<script type="text/javascript"
	src="js/dist/plugins/jqplot.highlighter.js"></script>
<script type='text/javascript' src='js/jqplot.pieRenderer.min.js'></script>
<script type="text/javascript"
	src="js/dist/plugins/jqplot.canvasAxisLabelRenderer.js"></script>
<script type="text/javascript"
	src="js/dist/plugins/jqplot.barRenderer.min.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>



</head>
<body>
<%
	TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
	Project project = tjnSession.getProject();
	String projectName=Utilities.escapeXml(project.getName());
	%>
	<div class='title'>
		<p>Project Dashboard</p>

	</div>
	<c:choose>
		<c:when test="${empty testSets}">
			<div id='infoTable'>
				<table>
					<tr>
						<td><b style='font-size: 22px; color: #3b73af'>Welcome to
								Tenjin Project Dashboard</b><br>
						<br> <b style='font-size: 17px; margin-left: 25px;'>No
								Test executed for the Project : <%=projectName%></b></td>
					</tr>
					<tr>
						<td style="padding-top: 15px; padding-left: 25px;"><span
							class="title-normal">Project Linkages</span></td>
					</tr>
				</table>
				<table class="summtable_linkages"
					style="margin-top: 10px; margin-left: 30px">
					<tbody>
						<tr>
							<td><img src="images/button/mail.png"></td>
							<td class="bold_linkages">Project Mail Notifications</td>
							<c:choose>
								<c:when test="${project.projectMail.prjMailNotification eq 'Y'}">
									<td><img src="images/success_20c20.png"></td>
								</c:when>
								<c:otherwise>
									<td><img src=" images/failure_20c20.png"></td>
								</c:otherwise>
							</c:choose>

						</tr>
						<tr>
							<td><img src="images/button/defect.png"></td>
							<td class="bold_linkages">Defect Management</td>
							<c:choose>
								<c:when test="${project.dttEnable eq 'Y'}">
									<td><img src="images/success_20c20.png"></td>
								</c:when>
								<c:otherwise>
									<td><img src=" images/failure_20c20.png"></td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr>
							<td><img src="images/button/tm_mapping.png"></td>
							<td class="bold_linkages">External Test Manager Integration</td>
							<c:choose>
								<c:when test="${project.etmEnable eq 'Y'}">
									<td><img src="images/success_20c20.png"></td>
								</c:when>
								<c:otherwise>
									<td><img src=" images/failure_20c20.png"></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</tbody>
				</table>
			</div>
		</c:when>
		<c:otherwise>

			<div class='form dashboard_container_16'>
				<div>
					<table id='dashboardTable' align='center' width='70%'
						style='margin-top: 10px'>
						<tr>
							<td>${entityCount['autCount'] }</td>
							<td>${entityCount['funCount'] }</td>
							<td>${entityCount['tsCount'] }</td>
							<td>${entityCount['tcCount'] }</td>
							<td>${entityCount['stepCount'] }</td>
							<td>${entityCount['defectCount'] }</td>
						</tr>
						<tr>
							<td>Applications</td>
							<!-- Modified By Priyanka for TCGST-64 starts -->
							<td>Functions/API</td>
							<!-- Modified By Priyanka for TCGST-64 ends -->
							<td>Test Sets</td>
							<td>Test Cases</td>
							<td>Test Steps</td>
							<td>Defects</td>
						</tr>
					</table>
				</div>
				<div>
					<table id='chartTable'>
						<tr>
							<td><div class='dashboard_exec_status_chart'>
									<span style='font-size: 17px;'>Execution Status</span>
									<div class='clear'></div>
									<div class='container'>
										<span>Test Set</span> <select id='exStatusTS'
											name='exStatusTS' style="max-width: 190px;"
											class='stdTextBoxNew'>
											<c:forEach items="${testSets}" var="ts">
												<option value="${ts['value']}">${ts.key.name}</option>
											</c:forEach>
										</select> <span style='margin-left: 150px;'>Run Id</span> <select
											id='exStatusRun' name='exStatusRun' class='stdTextBoxNew'>
										</select>
									</div>
									<input type='hidden' id='passedTests'
										value='${run.passedTests }' /> <input type='hidden'
										id='failedTests' value='${run.failedTests }' /> <input
										type='hidden' id='erroredTests' value='${run.erroredTests }' />
									<input type='hidden' id='totalCases' value='${run.totalTests }' />
									<c:set var="notExecutedTests"
										value="${run.totalTests - run.executedTests}" />
									<input type='hidden' id='notExecutedTests'
										value='${notExecutedTests }' />
									<div id="pie_chart"
										style="width: 400px; height: 200px; margin-top: 5px; margin-left: 40px"></div>
								</div></td>
							<td><div class='dashboard_exec_trend_chart'>
									<span style='font-size: 17px;'>Test Set Execution Trend</span>
									<div class='clear'></div>
									<div class='container'>
										<span>Test Set</span> <select id='exTrendTS' name='exTrendTS'
											style="max-width: 100px;" class='stdTextBoxNew'>
											<option value="0">All</option>
											<c:forEach items="${testSets}" var="ts">
												<c:if test="${ts.key.recordType eq 'TS' }">
													<option value="${ts.key.id }">${ts.key.name }</option>
												</c:if>
											</c:forEach>
										</select> <span style='margin-left: 150px;'>view</span> <select
											class='stdTextBoxNew' title='View' id='view'>
											<option value='1'>Weekly Report</option>
											<option value='2'>Monthly Report</option>
										</select>
									</div>
									<div id="line_chart_day" style="width: 400px; height: 200px;"></div>
									<div id="line_chart_week" style="width: 400px; height: 200px;"></div>
								</div></td>
						</tr>
						<tr>
							<td><div class='dashboard_def_severity_chart'>
									<span style='font-size: 17px;'>Defects by Severity</span>
									<div class='clear'></div>
									<div class='container'>
										<c:choose>
											<c:when test="${empty testSetsForDefect}">
												<span> No Defects posted for this Project</span>
											</c:when>
											<c:otherwise>
												<span>Test Set</span>
												<select id='defSevTS' name='defSevTS'
													style="max-width: 190px;" class='stdTextBoxNew'>
													<c:forEach items="${testSetsForDefect}" var="tsDefect">
														<option value="${tsDefect['value']}">${tsDefect.key.name }</option>
													</c:forEach>
												</select>
												<span style='margin-left: 150px;'>Run Id</span>
												<select id='defSevRun' name='defSevRun'
													class='stdTextBoxNew'>
												</select>
												<input type='hidden' id='defectsSeverity'
													value="${defects['severity']}" />
												<div id="barchart1"
													style="width: 400px; height: 200px; margin-top: 5px"></div>
											</c:otherwise>
										</c:choose>
									</div>
								</div></td>
							<td><div class='dashboard_def_status_chart'>
									<span style='font-size: 17px;'>Defects by Status</span>
									<div class='clear'></div>
									<div class='container'>
										<c:choose>
											<c:when test="${empty testSetsForDefect}">
												<span> No Defects posted for this Project</span>
											</c:when>
											<c:otherwise>
												<span>Test Set</span>
												<select id='defStatusTS' name='defStatusTS'
													style="max-width: 190px;" class='stdTextBoxNew'>
													<c:forEach items="${testSetsForDefect}" var="tsDefect">
														<option value="${tsDefect.key.id }">${tsDefect.key.name }</option>
													</c:forEach>
												</select>
												<span style='margin-left: 150px;'>Run Id</span>
												<select id='defStatusRun' name='defStatusRun'
													class='stdTextBoxNew'>
												</select>
												<input type='hidden' id='defectsStatus'
													value="${defects['status']}" />
												<div id="barchart2"
													style="width: 400px; height: 200px; margin-top: 5px"></div>
											</c:otherwise>
										</c:choose>
									</div>
								</div></td>
						</tr>
					</table>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</body>
</html>