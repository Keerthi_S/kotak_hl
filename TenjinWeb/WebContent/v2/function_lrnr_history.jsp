<!-- 

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: function_lrnr_history.jsp

Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright &copy 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 
 -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<link rel='stylesheet' href='css/lozenges.css' />
		<link rel="stylesheet" href="css/jquery.dataTables.css">
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script src="js/jquery.dataTables.js"></script>
		<script src="js/tables.js"></script>
		<script>
			 
			$(document).ready(function(){
				$('#tbl_learning_history').DataTable( {
			        "order": [[ 0, "desc" ]]
			    } );
			});
			$(document).on('click', '#linkDevFrmRprt', function() {
					var runId = $('#linkDevFrmRprt').attr("runId");
					var lrnrIp= $('#linkDevFrmRprt').attr("lrnrIp");
					var pCloudySessionName="Learning(RunId:"+runId+")";
					var reportLink="";
					$.ajax({
						url:'ResultsServlet',
						data :{param:"DEVICE_FARM_REPORT",sessionName:pCloudySessionName,machineIp:lrnrIp},
						async:false,
						dataType:'json',
						success: function(data){
							var status = data.status;
							if(data.status == 'SUCCESS'){
								var jsonReport=data.pCloudyReport;
								reportLink=jsonReport.report_path;
								var win = window.open(reportLink, '_blank');
								win.focus();
							}else{
								showMessage(data.message,'error');
							}	
						},
						error:function(xhr, textStatus, errorThrown){
							showMessage(errorThrown, 'error');			
						}
					});
				});
		
			
			$(document).on('click', '#btnBack', function() {
				window.location.href='FunctionServlet?t=view&key='+ $('#functionCode').val() +'&appId=' + $('#aut').val() ;
			});
		</script>
		
	</head>
	<body>
		<div class='title'>
			<p>Learning History for ${fn:escapeXml(aut.name) }</p>
		</div>
		<div class='toolbar'>
			<input type='button' id='btnBack' value='Back' class='imagebutton back' />
		</div>
		
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${screenState.message }' />
			
		<div class='form container_16'>
		<div id='user-message'></div>
		<c:choose>
			<c:when test="${screenState.status.equalsIgnoreCase('error') }">
					<input type='hidden' id='aut' name='aut' value='${aut.id }' />
					<input type='hidden' id='functionCode' name='functionCode' value='${fn:escapeXml(module.code) }' />
			</c:when>
			<c:otherwise>		
			<fieldset>
				<legend>Function Information</legend>
				<div class='fieldSection grid_7'>
					<div class='grid_2'><label for='functionCode'>Function Code</label></div>
					<div class='grid_4'><input readonly type='text' class='stdTextBox' value='${fn:escapeXml(module.code) }' id='functionCode' name='functionCode'  disabled="disabled"/></div>
					<input type='hidden' id='aut' name='aut' value='${aut.id }' />
				</div>
				<div class='fieldSection grid_7'>
					<div class='grid_2'><label for='name'>Name</label></div>
					<div class='grid_4'>
						<input type='text' readonly type='text' class='stdTextBox' value='${fn:escapeXml(module.name) }' id='name' name='name'  disabled="disabled" />
					</div>
					
				</div>
			</fieldset>
			<fieldset>
				<legend>Learning History</legend>
				<div id='dataGrid'>
				 
					<table class='display' id='tbl_learning_history'>
				 
						<thead>
							<tr>
								<th class="text-center">Run ID</th>
							  
								<th >Status</th>
								<th class="text-center">Start Time</th>
								<th class="text-center">End Time</th>
								<th class="text-center">Duration</th>
								<th>User</th>
								<th>Message</th>
								<th>No.of Fields Learnt</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${results }" var='result'>
								<tr>
									<td class="text-center">${result.id }</td>
									<td ><span class='lozenge ${result.learnStatus }'>${result.learnStatus }</span></td>
									<td class="text-center"><fmt:formatDate value="${result.startTimestamp }" type = "both" dateStyle = "long" timeStyle = "long"/></td>
									<td class="text-center"><fmt:formatDate value="${result.endTimestamp }" type = "both" dateStyle = "long" timeStyle = "long"/></td>
									<td class="text-center">${result.elapsedTime }</td>
									
									<td>${result.userID }</td>
									<c:choose>
										<c:when test="${result.deviceFarmFlag eq 'Y' && result.learnStatus eq 'COMPLETE'}">
											<td>${result.message }<a id='linkDevFrmRprt' href='' runId='${result.id}' lrnrIp='${result.learnerIpAddr}'>Device Farm Report</a> </td>
										</c:when>
										<c:otherwise>
											<td>${result.message }</td>
										</c:otherwise>
									</c:choose>
									<c:choose>
									<c:when test="${result.learnStatus eq 'COMPLETE'}">
									<td>${result.lastLearntFieldCount }</td>
									</c:when>
									<c:otherwise>
									<td>N/A</td>
									</c:otherwise>
									</c:choose>
									
									
								</tr>							
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
			</c:otherwise>
		</c:choose>
		</div>
		
	</body>
</html>