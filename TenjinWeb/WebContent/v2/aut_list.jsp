<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  aut_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
	<head>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<link rel="stylesheet" href="css/jquery.dataTables.css">
		<script type='text/javascript' src="js/jquery.dataTables.js"></script>
		<script  type='text/javascript' src="js/tables.js"></script>
		
		
		<script>
		
			 
			
			 $('#autTable').DataTable( {
				 "aoColumnDefs": [{ "bSortable": false, "aTargets": [1]}],

			    } );
		
			$(document).on('click','#btnAddNewAut', function() {
				window.location.href ='ApplicationServlet?t=new';
			});
			$(document).on('click','#btnRefreshAut', function() {
				window.location.href ='ApplicationServlet';
			});
                $(document).on("click", "#btnDelete", function() {
				
				if($('#autTable').find('input[type="checkbox"]:checked').length < 1) {
					showLocalizedMessage('no.records.selected', 'error');
					return false;
				}else if($('#autTable').find('input[type="checkbox"]:checked').length < 2 && $('#autTable.tbl-select-all-rows').is(':checked')) {
					showLocalizedMessage('no.records.selected', 'error');
					return false;
				}
			if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
				var $autForm = $("<form action='ApplicationServlet' method='POST' />");
				$autForm.append($("<input type='hidden' id='del' name='del' value='true' />"))
				$autForm.append($("<input type = 'hidden' id='csrftoken_form' name ='csrftoken_form' value='"+$('#csrftoken_form').val()+"'/>"));
			 
		
				 for (var i in selectedItems) {
					 $autForm.append($("<input type='hidden' name='selectedAutCodes' value='"+ selectedItems[i] +"' />"));
				    }
				
				$('body').append($autForm);
				$autForm.submit();
			}
		});
			
		
		</script>
	</head>
	<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	
		<div class='title'>
			<p>Applications Under Test</p>
		</div>
		<div class='toolbar'>
			<input type='button' value='New' id='btnAddNewAut' class='imagebutton new' /> 
            <input type='button' value='Remove' id='btnDelete' class='imagebutton delete' />
		    <input type='button' value='Refresh' id='btnRefreshAut' class='imagebutton refresh' />
		</div>
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
		
		<div id='user-message'></div>
		<div class='form container_16'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div id='dataGrid'>
				<table id='autTable' class="display tjn-default-data-table">
					<thead>
						<tr>
							<th class='table-row-selector nosort sorting_disabled '><input type='checkbox' class='tbl-select-all-rows '/></th>
							<th>Type</th>
							<th>Name</th>
							<th>URI</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${aut}" var="aut">
							<tr>
								<td class='table-row-selector '><input type='checkbox' class='chkBox' data-aut-rec='${fn:escapeXml(aut.id) }' /></td>
								<td>${fn:escapeXml(aut.getApplicationTypeValue())}</td>
								<td title='${fn:escapeXml(aut.name)}'><a href='ApplicationServlet?t=view&key=${fn:escapeXml(aut.id)}'>${fn:escapeXml(aut.name) }</a></td>
								<c:choose>
								    <c:when test="${aut.applicationType == 1||aut.applicationType ==2}">
								    	<td title='${fn:escapeXml(aut.URL)}' ><a href='${fn:escapeXml(aut.URL)}'target="_blank" rel="noopener">${fn:escapeXml(aut.URL)}</a></td>
								    </c:when>
								    <c:otherwise>
								    	 <td title='${fn:escapeXml(aut.applicationPackage)}'>${fn:escapeXml(aut.applicationPackage)}</td>
								    </c:otherwise>
								</c:choose>
								 
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>