<!-- 
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  applicationuser_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION

  -->

<!DOCTYPE html>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<html>
	<head>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/formvalidator.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/tenjin-aut-selector.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script >

		$(document).ready(function() {
			
			mandatoryFields('aut_user_new_form');
			
			$(document).on('click', '#btnCancel', function() {
				var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
				if(confirmResult) {
				window.location.href='ApplicationUserServlet';
				} else{
						return false;
				}
			});
			
			$('#aut').change(function(){
				clearMessages();
				var app = $(this).val();
				$('#appId').val(app);
				
				var val = $(this).val();
				if(val=='-1'){
					$("#autLogintypes option").remove();
					return false;
				}
				});
		});
		</script>
	</head>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<div class='title'>
		<p>New Application User</p>
	</div>
	<form name='aut_user_new_form' action='ApplicationUserServlet' method='POST' class='tovalidate'>
	<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
				<input type='submit' class='imagebutton save' value='Save' id='btnSave' />
				<input type='button' value='Cancel'id='btnCancel' class='imagebutton cancel' />
			</div>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<input type='hidden' id='appId' name='appId' value=''>
			<div id='user-message'></div>
			<div class='form container_16'>
				<div class='fieldSection grid_16'>
					<div class='clear'></div>
						<div class='grid_3'>
							<label for='userId'>User ID</label>							
						</div>
					<div class='grid_12'>
						<input type='text' class='stdTextBox' value = '${fn:escapeXml(userId)}'name='userId' id='userId' title='UserID' disabled = 'disabled'/>					
					</div>
					<div class='clear'></div>
					<div class='grid_3'>
						<label for='aut'>Application Name</label>
					</div>
					<div class='grid_12'>
					<tjn:applicationSelector mandatory='yes' title='Application Name' cssClass="stdTextBoxNew"  id="aut" name="aut" defaultValue="${fn:escapeXml(applicationUser.appId )}"  loadAutLoginTypes='true' autLoginTypeSelector='autLogintypes' />
					</div>
					<div class='clear'></div>
					<div class='grid_3'>
						<label for='txtAppUserName'>Login Name</label>
					</div>
					<div class='grid_12'>
					 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<input type='text' class='stdTextBox'  id='txtAppUserName'  value='${fn:escapeXml(applicationUser.loginName )}' mandatory='yes' name='loginName' title='Login Name ' maxlength="50" placeholder='MaxLength 50'/>
					 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>
					<div class='clear'></div>
					<div class='grid_3'>
						<label for='loginPassword'>Login Password</label>
					</div>
					<div class='grid_12'>
					 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
				    	<input type='password' id='loginPassword' name='loginPassword' mandatory='yes' title = 'Login Password' class='stdTextBox' autocomplete="new-password"  value='${applicationUser.password }' maxlength="50" placeholder='MaxLength 50'/>
					 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>
					<div class='clear'></div>
					<div class='grid_3'>
						<label for='transactionPassword'>Transaction Password</label>
					</div>
					<div class='grid_12'>
					<!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<input type='password' id='transactionPassword' name='transactionPassword'  title = 'Transaction Password' class='stdTextBox'  value='${applicationUser.txnPassword }' maxlength="20"  placeholder ="MaxLength 20"/>
					<!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>
					<div class='clear'></div>
					<div class='grid_3'>
						<label for='autLogintypes'>User Type</label>
					</div>
					<div class='grid_12'>
					
						<tjn:autLogintypeSelector mandatory='yes' title='User Type' applicationId="${fn:escapeXml(applicationUser.appId)}" cssClass="stdTextBoxNew" id='autLogintypes' name="autLoginType" defaultValue="${fn:escapeXml(applicationUser.loginUserType )}" />
							 
					</div>
			</div>
		</div>
</form>
	
</html>