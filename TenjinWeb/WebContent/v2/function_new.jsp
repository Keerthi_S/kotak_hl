<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  function_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 

-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %> 
<!DOCTYPE html>
<html>
	<head>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.4.1.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/formvalidator.js'></script>
		<script type='text/javascript' src='js/data-grids.js'></script>
		<script type='text/javascript' src='js/tenjin-aut-selector.js'></script>
		
		<script>
		$(document).ready(function() {
			mandatoryFields('functionForm');
		$('#txtDate').hide();
		$('#dateFormat').keyup(function() {
		}).focus(function() {
			$('#txtDate').show();
		}).blur(function() {
			$('#txtDate').hide();
		});
		$('#btnSave').click(function() {
			var dateFormat=$('#dateFormat').val()
			var regex = new RegExp(/[0-9]/g);
			var containsNonNumeric = dateFormat.match(regex);
			if(containsNonNumeric){
				showMessage("Please enter valid date format", 'error');
				return false;
			}
			var dateValue=isValidDate($('#dateFormat').val());
			if(!dateValue){
				showMessage("Please enter valid date format", 'error');
				return false;
			}
		});
		});
		$(document).on('click', '#btnSave', function() {
			if($('#existingGroup').val() === 'ALL'){
				$("#existingGroup").val('Ungrouped');
			}
			
		});
		$(document).on('click', '#btnBack', function() {
			var appId=0;
			var groupName="Ungrouped";
			window.location.href='FunctionServlet?appId=' + appId + '&group=' + groupName;
		});
		
		
		$(document).on('change', '#existingGroup', function() {
			if($(this).val() === 'ALL' || $(this).val() === '-1' || $(this).val() === 'Ungrouped') {
				$('#newGroup').prop('disabled',false);
			}else{
				$('#newGroup').prop('disabled',true);
				$('#group').val($(this).val());
			}
		});	
		
		$(document).on('change', '#newGroup', function() {
			if($(this).val() === '') {
				$('#existingGroup').prop('disabled',false);
			}else{
				$('#existingGroup').prop('disabled',true);
				$('#group').val($(this).val());
			}
		});
		
		$(document).on('click','#btnSave',function(){
			var FuncId=$('#functionCodeValue').val();
			if(FuncId!=""){
	            if(/[^a-zA-Z0-9_]/.test(FuncId)) {
	                 showMessage("Only characters,numbers and '_'  are allowed for Function Code ","error");
	                 return false;
	            }
	        }
		});
		</script>
		
	</head>
	<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		<div class='title'>
			<p>New Function</p>
		</div>
		
		<input type='hidden' id='callback' value='${callback }' />
		<form id='functionForm' name='functionForm' action='FunctionServlet' method='POST' class='tovalidate'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			
			<div class='toolbar'>
				<input type='submit' id='btnSave' value='Save' class='imagebutton save' />
				<input type='button' id='btnBack' value='Cancel' class='imagebutton cancel' />
			</div>
			
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			
			<div id='user-message'></div>
			
			<div class='form container_16'>
				<fieldset>
					<legend>Application Information</legend>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='aut'>Application</label></div>
						<div class='grid_4'>
							<tjn:applicationSelector mandatory='yes' title='Application' cssClass="stdTextBoxNew" groupSelector="existingGroup" loadFunctionGroups="true" id="aut" name="aut" defaultValue="${aut.id }"/>						
						</div>
					</div>
					<div class='clear'></div>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='existingGroup'>Group</label></div>
						<div class='grid_4'>
							<tjn:functionGroupSelector mandatory='false' title='Group' applicationId="${aut.id }" showAllGroupsOption="false" cssClass="stdTextBoxNew" id="existingGroup" name="group" defaultValue="${module.group }"/>
							<input type='hidden' id='originalGroup' value='${module.group }' />
						</div>
						
					</div>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='newGroup'>New Group</label></div>
						<div class='grid_4'>
						    <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<input type='text' id='newGroup' name='group' value='${module.group }' class='stdTextBox' placeholder='Specify a new Group Name' maxlength="100"/>
							<!-- Modified by Priyanka for TENJINCG-1233 ends -->
						</div>
						
					</div>
					<input type='hidden' id='group' name='groupd' value='' />
				</fieldset>
				<fieldset>
					<legend>Basic Information</legend>
					<div class='fieldSection grid_15'>
						
						<div class='grid_2'><label for='functionCodeValue'>Function Code</label></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_4'><input title='Function Code' type='text' id='functionCodeValue' name='functionCodeValue' placeholder="MaxLength 50" value='${fn:escapeXml(module.code) }' class='stdTextBox' mandatory='yes' maxlength="50"/></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						<div class='clear'></div>
						
						<div class='grid_2'><label for='functionName'>Name</label></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_4'><input title='Function Name' type='text' id='functionName' name='functionName' value='${fn:escapeXml(module.name) }' class='stdTextBox' mandatory='yes' maxlength='100' placeholder='MaxLength 100'/></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						<div class='clear'></div>
						
						<div class='grid_2'><label for='menu'>Menu</label></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_12'><input title='Menu' type='text' id='menu' name='menu' value='${fn:escapeXml(module.menuContainer) }' class='stdTextBox long' mandatory='yes' maxlength='100' placeholder ='MaxLength 100'/></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						<div class='clear'></div>
						
						<div class='grid_2'><label for='dateFormat'>Date Format</label></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_4'><input title='Date Format' type='text' id='dateFormat' name='dateFormat' value='${fn:escapeXml(module.dateFormat) }' class='stdTextBox' maxlength="50" placeholder="MaxLength 50"/></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						<div class='grid_12'>
							<table id='txtDate' class='border' width='43%'>
								<thead>
									<tr>
										   <th style="font-weight:bold">Suggestions</th>
									</tr>
								</thead>
								<tbody>
								<tr>
								<td><p class='ptext'>'d' - Date, 'M' - Month, 'y' -
												Year</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>You must enter Date Format not
												Date value</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>Some valid examples:</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>dd-MM-yyyy (13-01-2017)</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>dd/MM/yyyy (13/01/2017)</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>dd MMM yyyy (13 JAN 2017)</p></td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</fieldset>
			</div>
		</form>
	</body>
</html>