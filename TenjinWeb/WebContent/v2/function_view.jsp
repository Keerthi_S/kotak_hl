<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  function_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 

-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/data-grids.js'></script>
		<script type='text/javascript' src='js/pages/v2/function_view.js'></script>
		
		<script>
		
		</script>
		
	</head>
	<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		<div class='title'>
			<p>Function Details</p>
		</div>
		
		<input type='hidden' id='callback' value='${callback }' />
		<form id='functionForm' name='functionForm' action='FunctionServlet' method='POST'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<input type='hidden' id='transactionType' name='transactionType' value='Update' />
			<div class='toolbar'>
				<input type='button' id='btnBack' value='Back' class='imagebutton back' />
				<input type='submit' id='btnSave' value='Save' class='imagebutton save' />
				<input type='button' id='btnAssistedLearning' value='Assisted Learning' class='imagebutton template' />
				<input type='button' id='btnLearningHistory' value='Learning History' class='imagebutton history' />
				<input type='button' id='btnExtractionHistory' value='Extraction History' class='imagebutton history' />
			<input type='button' value='Download' id='download' class='imagebutton download'/>
			<!-- Modified By Prem for Tenj212-31 Start-->
			<input type='button' id='importMetadata' value='Upload Metadata' class='imagebutton upload' />
				<c:choose>
					<c:when test="${!empty module.lastSuccessfulLearningResult }">
						<input type='button' id='exportMetadata' value='Download Metadata' class='imagebutton download' /> 
					</c:when>
				</c:choose>
				<!-- Modified By Prem for Tenj212-31 End-->	
				 
				<span class='progressbar'></span>
				
				<c:choose>
					<c:when test="${!empty module.lastSuccessfulLearningResult }">
						<input type="button" value="Field Wait Time" class="imagebutton manual" id="btnWaitTime">
						<input type='button' id='template' value='Test Data Template' class='imagebutton template' />
						<input type='button' id='btnMetadata' value='Template Settings' class='imagebutton metadata'/>
					</c:when>
				</c:choose>
				
			</div>
			
			
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			
			<div id='user-message'></div>
			
			<div class='form container_16'>
				<fieldset>
					<legend>Application Information</legend>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='applicationName'>Application</label></div>
						<div class='grid_4'><input readonly type='text' class='stdTextBox' value='${fn:escapeXml(aut.name) }' id='applicationName' name='applicationName'  disabled="disabled" /></div>
						<input type='hidden' id='aut' name='aut' value='${aut.id }' />
					</div>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='group'>Group</label></div>
						<div class='grid_4'>
							<tjn:functionGroupSelector mandatory='false' showAllGroupsOption="false" title='Group' applicationId="${aut.id }" cssClass="stdTextBoxNew" id="group" name="group" defaultValue="${fn:escapeXml(module.group) }"/>
							<!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<input type='hidden' id='originalGroup' value='${fn:escapeXml(module.group) }' maxlength="100" />
						    <!-- Modified by Priyanka for TENJINCG-1233 starts -->    
						</div>
						
					</div>
				</fieldset>
				<fieldset>
					<legend>Basic Information</legend>
					<div class='fieldSection grid_15'>
						<div class='grid_2'><label for='functionCode'>Function Code</label></div>
						<div class='grid_4'><input type='text' id='functionCode' name='functionCode' value='${fn:escapeXml(module.code) }' class='stdTextBox' disabled="disabled"/></div>
						<input type='hidden' id='functionCodeValue' name='functionCodeValue' value='${fn:escapeXml(module.code) }'/>
						<div class='clear'></div>	
						
						<div class='grid_2'><label for='functionName'>Name</label></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_4'><input type='text' id='functionName' name='functionName' value='${fn:escapeXml(module.name) }' class='stdTextBox' mandatory='yes' maxlength='100' placeholder ='MaxLength 100'/></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						<div class='clear'></div>
						
						<div class='grid_2'><label for='menu'>Menu</label></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_12'><input type='text' id='menu' name='menu' value='${fn:escapeXml(module.menuContainer)} ' class='stdTextBox long' mandatory='yes' maxlength ='100' placeholder='MaxLength 100' /></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						<div class='clear'></div>
						
						<div class='grid_2'><label for='dateFormat'>Date Format</label></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_4'><input type='text' id='dateFormat' name='dateFormat' value='${fn:escapeXml(module.dateFormat) }' class='stdTextBox' maxlength="50" placeholder='MaxLength 50'/></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						<div class='grid_12'>
							<table id='txtDate' class='border' width='43%'>
								<thead>
									<tr>
										   <th style="font-weight:bold">Suggestions</th>
									</tr>
								</thead>
								<tbody>
								<tr>
								<td><p class='ptext'>'d' - Date, 'M' - Month, 'y' -
												Year</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>You must enter Date Format not
												Date value</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>Some valid examples:</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>dd-MM-yyyy (13-01-2017)</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>dd/MM/yyyy (13/01/2017)</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>dd MMM yyyy (13 JAN 2017)</p></td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</fieldset>
			</div>
		</form>
		<div class='modalmask'></div>
		<div class='subframe' style='left:20%' id='templateDownloadModal'>
			<iframe frameborder="2" scrolling="no"  marginwidth="5" marginheight="5" style='height:500px;width:600px;' seamless="seamless" id='ttd-options-frame' src=''></iframe>
		</div>
		<div class='subframe' id='importMetadataModal'>
			<div class='title'><p>Import Metadata from Spreadsheet</p></div>
			<div class='toolbar'>
				<input type='button' id='modal_import' class='imagebutton ok' value='Go' />
				<input type='button' id='modal_cancel' class='imagebutton cancel' value='Cancel' />
			</div>
			<div id='user-message-import'></div>
			<div class='form container_16'>
				<fieldset>
					<legend>Choose your file</legend>
					<form>
						<div class='fieldSection grid_7'>
							<div class='grid_2'><label for='uploadedFile'>Choose File</label></div>
							<div class='grid_4'><input type='file' id='uploadedFile' class='stdTextBox' /></div>
						</div>
					</form>
					<div class='fieldSection grid_7' id='import_progress_dialog' style='display:none;'>
						<div id='processing_icon' style='width:25px;margin-left:0;display:inline;float:left;'><img src='images/loading.gif' alt='In Progress..' style='width:25px;'/></div>
						<div id='processing_message' style="display: inline;float: left;margin-left: 10px;margin-top: 5px;font-weight: bold;"><p>Uploading...</p></div>
					</div>
				</fieldset>
				
				<div class='clear'></div>
				<fieldset>
					<legend style='color:red;'>Caution</legend>
					<div class='grid_10' id='import_instructions'>
						<ul style='list-style-type: disc;'>
							<li>Uploading metadata using this method will erase all existing metadata for this function, and replace them with the metadata loaded from the spreadsheet being uploaded.</li>
							<li>Only Excel spreadsheets having extensions .xlsx or .xls are allowed.</li>
						</ul>
					</div>
				</fieldset>
				
				
				 
			</div>
		</div>
	</body>
</html>