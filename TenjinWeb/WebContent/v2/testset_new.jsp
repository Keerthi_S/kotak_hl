<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testset_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
  -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Test Set</title>
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/cssreset-min.css' />
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/ifr_main.css' />
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/style.css' />
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/tabs.css' />
<link rel="Stylesheet"
	href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/buttons.css' />
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/css/960_16_col.css' />
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/data-grids.js'></script>
<script>
	$(document).on(
			'click',
			'#btnCancel',
			function() {
				var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
				if(confirmResult) {
				var user = $('#user').val();
				var projectName = $('#projectName').val();
				var domain = $('#domain').val();
				var pid = $('#projectId').val();
				var flag = $('#copyFlag').val();
				var tsid = $('#oldTsId').val();
				if(flag=='yes')
					window.location.href = 'TestSetServletNew?t=testset_view&paramval='+tsid;
				else
					window.location.href = 'TestSetServletNew?t=list&user=' + user
						+ '&project=' + projectName + '&domain=' + domain
						+ '&pid=' + pid;
				}else{
					return false;
				}
			});
	
	$(document).ready(function(){
		
		mandatoryFields('main_form');
		
		 $('#txtTestMode').children('option').each(function(){
		        var tcMode=$('#selectedMode').val();
		        var val = $(this).val();
		        if(val == tcMode){
		            $(this).attr({'selected':true});
		        }
		    });
		 var tcCount = $('#tcCount').val();
		 if(tcCount>0){
			 document.getElementById("txtTestMode").disabled=true;
			 document.getElementById("txtTestMode").title="Test Set mode cannot be changed as it contains Test Cases.";
		 }
		 $('#lstPriority').children('option').each(function(){
		        var tcPriority=$('#selectedPriority').val();
		        var val = $(this).val();
		        if(val == tcPriority){
		            $(this).attr({'selected':true});
		        }
		    });
		 
			$(document).on('click','#btnRefresh',function(){
				window.location.href ="TestSetServletNew?t=init_new_testset";
			});
	})
	
	
	 
</script>
</head>
<body>
<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
	<div class='title'>
		<p>New Test Set</p>
		 
	</div>
	<input type='hidden' id='callback' value='${callback }' />
	<form name='main_form' action='TestSetServletNew' method='POST'>
		<div class='toolbar'>
		     <!-- added by shruthi for CSRF token starts -->
		     <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		     <!-- added by shruthi for CSRF token ends -->
			<input type='submit' value='Save' id='btnSave'
				class='imagebutton save' /> <input type='button' value='Cancel'
				id='btnCancel' class='imagebutton cancel' />
				<input type="button" value="Refresh" id="btnRefresh" class="imagebutton refresh">
		</div>

		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${screenState.message }' />
		<input type='hidden' id='domain' name='domain'
			value='${project.domain}' /> <input type='hidden' id='projectName'
			name='projectName' value='${project.name }' /> <input type='hidden'
			id='projectId' name='projectId' value='${project.id }' /> <input
			type='hidden' id='user' name='user' value='${user }' />
		<input type='hidden' id='copyFlag' name='copyFlag' value='${flag }' />
		<input type='hidden' id='tcCount' value='${testcasecount }'/>
		<input type='hidden' id='oldTsId' name='testSetId' value='${testSetId }'/>
		<div id='user-message'></div>

		<div class='form container_16'>
			<fieldset>
			<legend>Test Set Details</legend>
			<div class='fieldSection grid_8'>

				<div class='grid_2'>
					<label for='txtTestSetName'>Test Set Name</label>
				</div>
				<div class='grid_4'>
				 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
					<input type='text' id='txtTestSetName' name='name'
						class='stdTextBox' title='Test Set Name' value='${testset.name }' mandatory='yes'maxlength ='100'placeholder ="MaxLength 100"/>
				 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
				</div>
				</div>
				<div class='fieldSection grid_7'>
				
			 
				</div>
				<div class='fieldSection grid_16'>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='txtTestSetDesc'>Description</label>
				</div>
				<div class='grid_13'>
					<textarea id='txtTestSetDesc' name='description' class='stdTextBox'
						style='width: 89.5%; height: 100px;' maxlength="3000">${testset.description }</textarea>
				</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Basic Information</legend>
				<div class='fieldSection grid_8'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtCreatedBy'>Created By</label>
					</div>
					<div class='grid_4'>
					 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<input type='text' id='txtCreatedBy' name='createdBy'
							class='stdTextBox' title='Test created by'
							value='${user }' disabled='disabled' /> <input type='hidden'
							id='txtCreatedBy' name='createdBy' value='${user }' maxlength="40"/>
					 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstTestCaseType'>Type</label>
					</div>
					<div class='grid_4'>
						<select class='stdTextBoxNew' id='lstTestSetType' name='type'>

							<option value='Functional'>Functional</option>
							

						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtCreatedOn'>Created On</label>
					</div>
					<div class='grid_4'>
						<input type='text' value='<%=new Date()%>' id='txtCreatedOn'
							name='createdOn' title='Created On' mandatory='no'
							disabled='true' class='stdTextBox' />
					</div>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstPriority'>Priority</label>
					</div>
					<div class='grid_4'>
					 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
					<input type='hidden' id='selectedPriority' value='${testset.priority }' maxlength="10"/>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						<select class='stdTextBoxNew' id='lstPriority' name='priority'>
							<option value='Low'>Low</option>
							<option value='Medium'>Medium</option>
							<option value='High'>High</option>
						</select>
					</div>
				</div>
			</fieldset>
		</div>
	</form>

</body>
</html>