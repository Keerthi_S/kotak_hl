<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  project_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 05-02-2021            Shruthi A N             TENJINCG-1255   

-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Project New</title>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
		<link rel="Stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/data-grids.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/moment.min.js'></script>
		<script type='text/javascript' src='js/tenjin-aut-selector.js'></script>
		
		
		


<link rel='stylesheet' href='css/bordered.css' />


<link rel='stylesheet' href='css/jquery-ui.css' />
<link rel="SHORTCUT ICON" HREF="images/yethi.png">



<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">

<script type='text/javascript' src="js/jquery-1.12.4.js"></script>
<script type='text/javascript' src="js/jquery-ui.js"></script>
		<script>
		
		$(document).ready(function() {
			mandatoryFields('projectForm');
			
			/* added by shruthi for TENJINCG-1255 starts */
			$('.show-for-all').hide();
			$('.show-for-native').hide();
			$('.show-for-accesskey').hide();
			$('.show-for-accessSeckey').hide();
			$('.show-for-bucket').hide();
			/* added by shruthi for TENJINCG-1255 ends */
			$(function() {
				
				$("#startDate").datepicker({
					
					showOn: 'button',
					buttonText: 'Show Date',
					buttonImageOnly: true,
					buttonImage: './images/button/calendar.png',
					dateFormat: 'dd-M-yy',
					minDate:0,
					constrainInput: true
				}); 

			});
			
			$(function() {
				
				$("#endDate").datepicker({
					
					showOn: 'button',
					buttonText: 'Show Date',
					buttonImageOnly: true,
					buttonImage: './images/button/calendar.png',
					
					dateFormat: 'dd-M-yy',
					minDate:0,
					constrainInput: true
				}); 

			});
			
			/* added by shruthi for TENJINCG-1255 starts */
			$('#repotype').on('change', function() {
				var repoType= $('#repotype').val();
				var html='';
				var AccessKey =  $('#Acckey').val();
			    var AccessSecKey = $('#AccSeckey').val();
				if(repoType=='aws')
					{
					$('.show-for-all').show();
					$('.show-for-native').hide();
					if(AccessKey){
					$.ajax({
						url:'TestDataUploadServlet?param=fetchAWSBucket',
						async:false,
						dataType:'json',
						success:function(data){
							if(data.status.toLowerCase() === 'success') {
								/* awsBuckets */
								var bucketList=data.awsBuckets;
								for(var i=0; i<=bucketList.length-1; i++){
					              	var name=bucketList[i];
					                 html=html+"<option value="+bucketList[i]+">"+bucketList[i]+"</option>";
					             }
					             $('#bucket').append(html);
					             $('#btnfetch').hide();
					           
					             $('.show-for-bucket').show();
					             var accesskey = document.getElementById("accesskey");
					             var accessSeckey = document.getElementById("accessSeckey");
					             accesskey.setAttribute('value', AccessKey);
					             accessSeckey.setAttribute('value', AccessSecKey)
					             $('.show-for-accesskey').show();
								 $('.show-for-accessSeckey').show();
								$('#accesskey')[0].disabled = true;
								$('#accessSeckey')[0].disabled = true;
								
							}else{
								showMessage(data.message,'error');
							}
						},
						error:function(data) {
							showMessage(data.message,'error');
						}
						
					});	
					
					}
					else{
						$('.show-for-accesskey').show();
						$('.show-for-accessSeckey').show();
						$('#btnfetch').show();
						$(document).on('click','#btnfetch', function() {
							var AccessKey =  $('#accesskey').val();
							var AccessSecKey = $('#accessSeckey').val();
							var html;
							$.ajax({
								url:'TestDataUploadServlet?param=fetchAWSBucket&AccessKey='+AccessKey+'&AccessSecKey='+AccessSecKey,
								
								async:false,
								dataType:'json',
								success:function(data){
									if(data.status.toLowerCase() === 'success') {
										/* awsBuckets */
										 $('.show-for-bucket').show();	
										var bucketList=data.awsBuckets;
										for(var i=0; i<=bucketList.length-1; i++){
							              	var name=bucketList[i];
							                 html=html+"<option value="+bucketList[i]+">"+bucketList[i]+"</option>";
							             }
							             $('#bucket').append(html);
							            
										
									}else{
										showMessage(data.message,'error');
									}
								},
								error:function(data) {
									showMessage(data.message,'error');
								}
								
							});	  
							
						});
						
					}
					
				}
				else if(repoType=='native')
					{
					$('.show-for-all').hide();
					$('.show-for-native').show();
					$('.show-for-accesskey').hide();
					$('.show-for-accessSeckey').hide();
					}
				else
					{
					$('.show-for-all').hide();
					$('.show-for-native').hide();
					$('.show-for-accesskey').hide();
					$('.show-for-accessSeckey').hide();
					}
			});
			/* added by shruthi for TENJINCG-1255 ends */
			var doaminName=$('#callback').val();
			var type=$('#selectedType').val();
		 
			$('#btnCancel').click(function(){
				var projectType=$('#type').val();
				window.location.href='DomainServletNew?param=domain_view&paramval='+doaminName+'&type='+projectType;
			});
			$('#btnSave').click(function(){
				var projectName=$('#name').val();
				var projectType=$('#type').val();
				var projectOwner=$('#owner').val();
				/* modified by shruthi for TCGST-39 starts */
				if($('#email').val()!=''){ 
					if(!validateUserId($('#email').val())){
						showMessage("Please enter valid E-mail","error");
						 return false;
						}
					/*  modified by shruthi for TCGST-39 ends */
				}
				var prjOwnerEmail=$('#email').val();
				var projectStartDate=$('#datepicker').val();
				var projectEndDate=$('#edate').val();
				var prjStartDate=$('#startDate').val();
				var selProjectStartDate=moment(prjStartDate, "DD-MMM-YYYY").format("YYYY-MM-DD");
				var selProjectEndDate=moment(projectEndDate, "DD-MMM-YYYY").format("YYYY-MM-DD");
				var todaysDate=new Date();
				/* added by shruthi for TENJINCG-1255 starts */
				var repoType= $('#repotype').val();
				var bucketName= $('#bucket').val();
				var rootFolder= $('#rootfolder').val();
				/* added by shruthi for TENJINCG-1255 ends */
				
				todaysDate=moment(todaysDate, "DD-MMM-YYYY").format("YYYY-MM-DD");
			 
				if(projectName==''){
					showMessage('Project name is mandatory','error');
					return false;
				}
				if(projectType=='Private' && projectOwner==''){
					showMessage('Project select project owner','error');
					return false;
				}
				if(prjStartDate==''|| prjStartDate==undefined){
					showMessage('Project Start date is mandatory','error');
					return false;
				}else if(!validateDate(prjStartDate)){
						showMessage("Please enter valid Start Date in this format DD-MMM-YYYY","error");
						return false;
				}else if(selProjectStartDate <todaysDate){
					showMessage('Please enter Start Date should be greater than or equal to Todays Date','error');
					return false;
				}
				/* added by shruthi for TENJINCG-1255 starts */
				if(repoType=='')
				{
					showMessage('Please select Repository type','error');
					return false;
			    }
				else if(repoType=='aws' && bucketName=='')
					{
					showMessage('Please select Bucket Name','error');
					return false;
					}
				else if(repoType=='native' && rootFolder=='')
					{
					showMessage('Please select Root Folder','error');
					return false;
					}
				/* added by shruthi for TENJINCG-1255 ends */
				
				if(projectEndDate=='' || projectEndDate==undefined){
					showMessage('Project end date is mandatory','error');
					return false;
				} 
				else if(!validateDate(projectEndDate)){
						showMessage('Please enter valid End Date in this format DD-MMM-YYYY.','error');
						return false;
				}else if(selProjectEndDate < selProjectStartDate){
					showMessage('End Date should be greater than or equal to Start Date','error');
					return false;
				}
				
			});
			/*  modified by shruthi for TCGST-39 starts */
				function validateUserId(email) {
			    var atstr = email.indexOf("@");
			    var dotstr = email.lastIndexOf(".");
			    	if (atstr<1 || dotstr<atstr+2 || dotstr+2>=email.length) {
			      
			    		return false;
			    	}
			    	else{
			    		clearMessages();
			    		return true;
			    	}
				}
			/* 	 modified by shruthi for TCGST-39 ends  */
			
			
			$('#startDate').change(function(){
				var statDate=$('#startDate').val();
				$('#sdate').val(statDate);
			});
			
			$('#endDate').change(function(){
				var endDate=$('#endDate').val();
				$('#edate').val(endDate);
			});
		 
			 $('#type').children('option').each(function(){
		         var projectType=$('#selectedType').val();
		         var val = $(this).val();
		         if(val == projectType){
		             $(this).attr({'selected':true});
		             $('#type').attr('disabled',true);
		         }
		     });
			 
			 $('#owner').children('option').each(function(){
		         var currentUser=$('#currentuser').val();
		         var val = $(this).val();
		         if(val == currentUser){
		             $(this).attr({'selected':true});
		            
		         }
		     });
			
		});
		
		</script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<div class='title'>New Project</div>
	<form name='projectForm' action='ProjectServletNew' method='POST'
	
		id='projectForm'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<div class='toolbar'>
			<input type='submit' value='Save' class='imagebutton save'
				id='btnSave' /> 
			<input type='button' value='Cancel'
				class='imagebutton cancel' id='btnCancel' />
		</div>
		<input type='hidden' id='Acckey' value='${fn:escapeXml(AccessKey)}' />
		<input type='hidden' id='AccSeckey' value='${fn:escapeXml(AccessSecKey)}' />
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${screenState.message }' />
		<input type='hidden' name='t' value='create' />
		<input type='hidden' id='callback' name='callback' value='${fn:escapeXml(callback)}'/>
		<input type='hidden' id='currentuser' value='${currentUser.id }'/>
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		
		<div id='user-message'></div>
		<div class='form container_16'>
			<fieldset>
				<legend>Basic Information</legend>
			
			<div class='fieldSection grid_7'>
			<div class='clear'></div>
				<div class='grid_2'>
					<label for='name'>Project</label>
				</div>
				<div class='grid_4'>
				    <!-- Modified by Priyanka for TENJINCG-1233 starts -->
					<input type='text' name='name' id='name' value=''
						class='stdTextBox' maxlength="60" mandatory='yes' placeholder ="MaxLenght 60"/>
				    <!-- Modified by Priyanka for TENJINCG-1233 ends -->
				</div>
			</div>
			
			<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='lstDomain'>Domain</label>
				</div>
				<div class='grid_4'>
                    <!-- Modified by Priyanka for TENJINCG-1233 starts -->
					<input type='text' id='lstDomain' name='lstDomain' maxlength="300"
						value='${fn:escapeXml(callback) }'class='stdTextBox' disabled='disabled' /> 
					 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
				</div>
			</div>
			<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='description'>Description</label>
				</div>
				<div class='grid_4'>
				    <!-- Modified by Priyanka for TENJINCG-1233 starts -->
					<input type='text' name='description' id='description'
						class='stdTextBox' maxlength ="1000"/>
					<!-- Modified by Priyanka for TENJINCG-1233 ends -->
				</div>
			</div>
			<div class='fieldSection grid_7'>
				<div id='projectTypeBlock'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='type'>Type</label>
					</div>
					<div class='grid_4'>
					<!-- Modified by Priyanka for TENJINCG-1233 ends -->
					    <input type='hidden' id='selectedType' value='${projectType }'/>
						<select name='type' id='type'
							class='stdTextBoxNew' maxlength='7'>
					<!-- Modified by Priyanka for TENJINCG-1233 ends -->
							
							<option value='Public'>Public</option>
							<option value='Private'>Private</option>
							
						</select>
					</div>
				</div>
				</div>
	 
				</fieldset>
				<fieldset>
				<legend>Project Information</legend>
				<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='startDate'>Start Date</label>
				</div>
				<div class='grid_4'>
					<%
						String todaydate1 = "";
						Calendar calendar = Calendar.getInstance();
						SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
						todaydate1 = dateFormat1.format(calendar.getTime());
					%>
				 
						<input type='text' id="startDate"  value='<%=todaydate1%>'
						style='border: 1px solid #ccc; border-radius: 3px; padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 177px;'
						mandatory='yes' title='Start Date'/>
					<input type='hidden' name='sdate' id='sdate' value='<%=todaydate1%>'/>
				</div>
				</div>
				<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='endDate'>End Date</label>
				</div>
				<div class='grid_4'>
					 
						<input type='text' id="endDate"  value='' autocomplete="off"
						style='border: 1px solid #ccc; border-radius: 3px; padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 177px;'
						mandatory='yes' title='Schedule Date'  />
					<input type='hidden' name='edate' id='edate' value=''/>
				</div>
				</div>
				<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='email'>Email</label>
				</div>
				<div class='grid_4'>
				<!-- Modified by Priyanka for TENJINCG-1233 starts -->
					<input type='text' name='email' id='email'
						class='stdTextBox' maxlength="100" />
				<!-- Modified by Priyanka for TENJINCG-1233 ends -->
				</div>
				</div>
					<div class='fieldSection grid_7'>
				<div id='projectOwner'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='owner'>Project Owner</label>
					</div>
					<!-- Modified by Priyanka for TENJINCG-1233 starts -->
					<div class='grid_4' style='margin-top: 2px;'>
						<select name='owner' id='owner'
							class='stdTextBoxNew' maxlength="60">
					<!-- Modified by Priyanka for TENJINCG-1233 ends -->
						
							<c:forEach items="${user }" var="user">
								<option value='${fn:escapeXml(user.id) }'>${fn:escapeXml(user.id) }</option>
							</c:forEach>
							
						</select>
					</div>
				</div>
				</div>
					 <!-- added by shruthi for TENJINCG-1255 starts  -->
				<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'>
					<label for='repotype'>Repository Type</label>
				</div>
				<div class='grid_4'>
						<select name='repotype' id='repotype' class='stdTextBox' name='repotype' mandatory='yes'>
							<option value=''>--Select One--</option>
							<option value='aws'>AWS S3</option>
							<option value='native'>Native</option>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7 show-for-all'>
				<div class='clear show-for-bucket'></div>
				<div class='grid_2 show-for-bucket'>
					<label for='bucket'>Bucket Name</label>
				</div>
				<div class='grid_4 show-for-bucket'>
					<select name='bucket' id='bucket' class='stdTextBox' name='bucket' mandatory='yes'>
							<option value='-1'>--Select One--</option>
							
					</select>
				</div>
				</div>
				
				<div class='fieldSection grid_7 show-for-native'>
				<div class='clear show-for-native'></div>
				<div class='grid_2 show-for-native'>
					<label for='rootfolder'>Root Path</label>
				</div>
				<div class='grid_4 show-for-native'>
					<input type='text' name='rootfolder' id='rootfolder' class='stdTextBox' mandatory='yes'/>
				</div>
				</div>
				<!-- added by shruthi for TENJINCG-1255 ends  -->
				<div class='fieldSection grid_7 show-for-accesskey'>
				<div class='clear show-for-accesskey'></div>
				<div class='grid_2 show-for-accesskey'>
					<label for='accesskey'>Access Key Id </label>
				</div>
				<div class='grid_4 show-for-accesskey'>
					<input type='text' name='accesskey' id='accesskey' class='stdTextBox' mandatory='yes'/>
				</div>
				</div>
				<div class='fieldSection grid_7 show-for-accessSeckey'>
				<div class='clear show-for-accessSeckey'></div>
				<div class='grid_2 show-for-accessSeckey'>
					<label for='accessSeckey'>Access Sec Key</label>
				</div>
				<div class='grid_4 show-for-accessSeckey'>
					<input type='text' name='accessSeckey' id='accessSeckey' class='stdTextBox' mandatory='yes'/>
				</div>
				<div class='grid_3'>
						<input type='button' id='btnfetch' value='Go' class='imagebutton ok' />
					</div>
				</div>
			</fieldset>
		</div>
		
	</form>

</body>
</html>