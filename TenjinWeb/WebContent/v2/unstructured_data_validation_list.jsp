<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  unstructured_data_validation_list.jsp.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 08-04-2020				Ashiki					TENJINCG-1204
*/

-->


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>File Validation List</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src=' js/bootstrap.min.js'></script>
<script src="js/tables.js"></script>
<link rel='stylesheet' href='css/paginated-table.css' />
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
<script>
		$(document).ready(function() {
			$('#btnNew').click(function(){
				 var stepRecordcId = $('#stepRecId').val();
				window.location.href='UnstructuredDataServlet?t=new&stepRecId='+stepRecordcId;
			});
			
			$('#btnDelete').click(function(){

				if($('#unstructured-data-table').find('input[type="checkbox"]:checked').length === 0 || ($('#unstructured-data-table').find('input[type="checkbox"]:checked').length == 1 && $('#unstructured-data-table').find('input[type="checkbox"]:checked').hasClass('tbl-select-all-rows'))) {
					showLocalizedMessage('no.records.selected', '', 'error');
					return false;
				}
				
				clearMessages();
				
				if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
					var $unstructuredDataForm = $("<form action='UnstructuredDataServlet' method='POST' />");
					$unstructuredDataForm.append($("<input type='hidden' id='del' name='del' value='true' />"))
					$unstructuredDataForm.append($("<input type = 'hidden' id='csrftoken_form' name ='csrftoken_form' value='<c:out value='${csrftoken}'/>'/>"))
					
					$('#unstructured-data-table').find('input[type="checkbox"]:checked').each(function() {
						if(!$(this).hasClass('tbl-select-all-rows')) {
							$unstructuredDataForm.append($("<input type='hidden' name='stepRecId' value='"+ $(this).data('stepid') +"' />"));
							$unstructuredDataForm.append($("<input type='hidden' name='recId' value='"+ $(this).data('recordid') +"' />"));
						}
					});
					$('body').append($unstructuredDataForm);
					$unstructuredDataForm.submit();
		}
			
			});
			
			 $('#btnBack').click(function(){
				  var stepRecId = $('#stepRecId').val();
				  window.location.href='TestStepServlet?t=view&key='+stepRecId+'&&callback=testcase';
			  })
			
		});
		</script>
</head>
<body>

<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
	<div class='title' style='box-sizing: content-box;'>
		<p>File Validation List</p>
	</div>

	<form name='main_form' action='UnstructuredDataServlet.java'
		method='POST'>
		<div class='toolbar'>
		    <!-- added by shruthi for CSRF token starts -->
		    <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		    <!-- added by shruthi for CSRF token ends -->
			<input type='button' id='btnBack' value='Back' class='imagebutton back' /> 
			<input type='button' value='New' id='btnNew' class='imagebutton new' />
			<input type='button' value='Remove' id='btnDelete' class='imagebutton delete'/>
		</div>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<div id='user-message'></div>
		<input type='hidden' id='stepRecId' name='stepRecId' value='${stepRecId}'>
		
		<div id='dataGrid'>
			<table id='unstructured-data-table'
				class='display sortable-table tjn-default-data-table'>
				<thead>
					<tr>
						<th class='table-row-selector nosort'><input type='checkbox'
							id='chk_all_data' class='tbl-select-all-rows' /></th>
						<th>Validation Name</th>
						<th>validation Type</th>
						<th>Text to Search</th>
						<th>Sheet Name</th>
						<th>Page Number</th>



					</tr>
				</thead>
				<tbody id='tblNaviflowModules_body'>
					<c:forEach items="${UnstructuredData }" var="UnstructuredData">
						<tr>
							<td class='table-row-selector'><input type='checkbox' data-recordid='${UnstructuredData.recordId}' data-stepid= '${UnstructuredData.testStepId}'/></td>
							<td><a
								href='UnstructuredDataServlet?t=view &recId=${UnstructuredData.recordId}&stepRecId=${stepRecId}'>${UnstructuredData.validationName }</a></td>
							<td>${UnstructuredData.fileType }</td>
							<td>${UnstructuredData.searchText }</td>
							<c:choose>
				 				<c:when test="${empty UnstructuredData.sheetName}">
				 				<td>N/A</td>
				 				</c:when>
				 				<c:otherwise><td>${UnstructuredData.sheetName }</td></c:otherwise>
				 			</c:choose>
							<c:choose>
				 				<c:when test="${UnstructuredData.pageNumber eq 0}">
				 				<td>N/A</td>
				 				</c:when>
				 				<c:otherwise><td>${UnstructuredData.pageNumber }</td></c:otherwise>
				 			</c:choose>
							<input type='hidden' id='recId' name='recId' value='${UnstructuredData.recordId}'>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</form>

</body>
</html>