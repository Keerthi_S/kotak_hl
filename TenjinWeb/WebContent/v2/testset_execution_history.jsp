<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testset_execution_history.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
* DATE                 	CHANGED BY              DESCRIPTION
 
-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TestSet Execution History</title>
	
	<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
		<link rel="Stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-ui-1.13.0.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/bootstrap.min.js'></script>
		<script src="js/tables.js"></script>
		<script>
		$(document).ready(function(){
			 
			$('#btnBack').click(function(){
				var tsrecid = $('#txtTsRecId').val();
				window.location.href = 'TestSetServletNew?t=testSet_View&paramval=' + tsrecid;
			});
			 $('#summary-table').DataTable( {
			        "order": [[ 0, "desc" ]]
			    } );
		});
		</script>
</head>
<body>
	<div class='title'>
		<p>Execution History</p>
	 
	</div>
	<div class='toolbar'>
		<input type='button' id='btnBack' class='imagebutton back' value='Back'/>
		<input type='hidden' id='txtTsRecId' value='${testset.id }' name='txtTsRecId'/>
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${screenState.message }' />
	</div>
	<div class='form container_16'>
		<div id='user-message'></div>
		<fieldset>
			<legend>Test Set Details</legend>
			<div class='fieldSection grid_16' id='tcDetails'>
				<div class='clear'></div>
				<div class='grid_2'><label for='txtTestSetId'>Test Set ID</label></div>
				<div class='grid_4'>
					<input type='text' id='txtTestSetId' name='txtTestSetId' value='${testset.id }' class='stdTextBox info-holder' title='Test Set ID' disabled='disabled'/>
				</div>
				<div class='clear'></div>
				<div class='grid_2'><label for='txtTestSetName'>Name</label></div>
				<div class='grid_13'>
					<input type='text' id='txtTestSetName' name='txtTestSetName' value='${testset.name }' class='stdTextBox long info-holder' title='Name' disabled= 'disabled'/>
				</div>
			</div>	
		</fieldset>
		<fieldset>
			<legend>Execution History</legend>
			<div id='dataGrid'>
				 
				<table id='summary-table' class='display'>
					<thead>
						<tr>
							<th class="text-center">Run ID</th>
							<th class="text-center">Start Time</th>
							<th class="text-center">End Time</th>
							<th class="text-center">Elapsed time</th>
							<th class="text-center">Tests</th>
							<th class="text-center">Executed</th>
							<th class="text-center">Passed</th>
							<th class="text-center">Failed</th>
							<th class="text-center">Error</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
					<c:choose>
						<c:when test="${not empty runs }">
							<c:forEach items="${runs }" var="run">
								<tr>
									<td class="text-center">${run.id}</td>
									<c:set var="startTime" value=""></c:set>
									<c:if test="${not empty run.startTimeStamp}">
										<fmt:formatDate value="${run.startTimeStamp}" var="startTime" type="both"/>
									</c:if>
									<c:choose>
										<c:when test="${run.status eq 'executing' }">
											<td class="text-center"><a href='ExecutorServlet?param=run_progress&paramval=${run.id }'>${startTime}</a></td>
										</c:when>
										<c:otherwise>
											<td class="text-center"><a href='ResultsServlet?param=run_result&run=${run.id }&callback=tsehistory&tcRecId=${run.tcRecId }'>${startTime}</a></td>
										</c:otherwise>
									</c:choose>
									<c:set var="endTime" value=""></c:set>
									<c:if test="${not empty run.endTimeStamp}">
										<fmt:formatDate value="${run.endTimeStamp}" var="endTime" type="both"/>
									</c:if>									
									<td class="text-center">${endTime}</td>
									<td class="text-center">${run.elapsedTime}</td>
									<td class="text-center">${run.totalTests}</td>
									<td class="text-center">${run.executedTests}</td>
									<td class="text-center">${run.passedTests}</td>
									<td class="text-center">${run.failedTests}</td>
									<td class="text-center">${run.erroredTests}</td>
									<td>${run.status}</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr><td colspan='10'>This test set has not been executed yet.</td></tr>
						</c:otherwise>
					</c:choose>
					</tbody>
				</table>
			</div>
		</fieldset>
	</div>
</body>
s</html>