<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  domain_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 
-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.project.Domain"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Domain New</title>

		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script src="js/formvalidator.js"></script>
		<script src='js/pages/tjnmaster.js'></script>
		<script type="text/javascript">
		$(document).ready(function() {
			mandatoryFields('main_form');
			var doaminName=$('#callback').val();
			$('#btnCancel').click(function(){
				window.location.href='DomainServletNew?param=domain_view&paramval='+doaminName;
			});
			
			$('#btnSave').click(function(){
				var domain=$.trim($('#txtDomainName').val());
				if(domain==null || domain==""){
					showMessage("Domain Name is mandatory", 'error');
					return false;
				}
			});
			
		});
		</script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>

	<div class='title'>
		<p>New Domain</p>
	</div>
	
	<form name='main_form' action='DomainServletNew' method='POST'>
	<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
	
		<div class='toolbar'>
			<input type='submit' value='Save' class='imagebutton save'
				id='btnSave' /> 
			<input type='button' value='Cancel'
				class='imagebutton cancel' id='btnCancel' />
		</div>
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${screenState.message }' />
			
			<div id='user-message'></div>
		<input type='hidden' id='callback' value='${callback}'/>
		<div class='form'>
			<div class='form container_16'>
				<fieldset>
					<legend>Basic Information</legend>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtDomainName' >Name</label>
						</div>
						<div class='grid_4'>
						       <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							   <input type='text' class='stdTextBox' value='' id='txtDomainName'
								name='txtDomainName' title='Domain Name' mandatory='yes'
								maxlength='50' placeholder ="MaxLength 50"  />
								 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						</div>
					</div>
				</fieldset>
			</div>
		</div>
		
	</form>
</body>
</html>