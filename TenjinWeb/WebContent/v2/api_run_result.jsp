<!-- 

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: api_run_result.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright &copy 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 
 -->
<%@page import="com.ycs.tenjin.util.HatsConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
	<head>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/rprogress.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel="Stylesheet" href="css/summarypagetable.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/list.css'/>
		
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/CircularLoader-v1.3.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/jquery.jqplot.min.js'></script>
		<script type='text/javascript' src='js/jqplot.pieRenderer.min.js'></script>
		<script type='text/javascript' src='js/pages/v2/api_run_result.js'></script>
		
		
		<style>
			.current
			{
			    -webkit-animation-name: example; 
			    -webkit-animation-duration: 1s; 
			    animation-name: example;
			    animation-duration: 1s;
			    animation-iteration-count: 1000;
			    
			}
			
			@keyframes example {
			    
			    
			    from {background-color: #f79232; }
    			to {background-color: #f6c342; }
			}
			
			@-webkit-keyframes example {
				/* from {background-color:#205081;}
			    to {background-color:##4393e4;} */
			    
			    from {background-color: red;}
    			to {background-color: yellow;}
			}
			
			.txn-validations-view.Pass {
				font-weight:bold;
				color:green;
			}
			
			.txn-validations-view.Fail {
				font-weight:bold;
				color:red;
			}
			
			.txn-validations-view.Error {
				font-weight:bold;
				color:yellow;
			}
			
			.summtable tbody td
			{
				cursor:default;
			}
			
			
			td.fs_group_heading, td.txn
			{
				padding:5px;
			}
			
			td.fs_group_heading
			{
				font-size:1em;
			}
			
			td.txn-status-icon
			{
				text-align:center;
			}
			td.txn-status-icon > img
			{
				width:14px;
			}
			
			.subframe_title1{
			    width: auto;
				height: 20px;
				background: url('images/bg.gif');
				color: white;
				font-family: 'Open Sans';
				padding-top: 6px;
				padding-bottom: 6px; 
				padding-left: 6px;
				font-weight: bold;
			}
			.layout-value{
				text-overflow: ellipsis; 
				overflow: hidden; 
				white-space: nowrap; 
				max-width: 150px;
			}
		</style>
	</head>
	<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		<div class='title'>
			<p>API Execution Results</p>
			 
			 
		</div>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<c:set var="projectType" value="${sessionScope.TJN_SESSION.project.type}"></c:set>
		<c:set var="currentRoleInProject" value="${sessionScope.TJN_SESSION.user.roleInCurrentProject}"></c:set>
		<div class='toolbar'>
			<c:set var="passStatus" value="<%=HatsConstants.ACTION_RESULT_SUCCESS %>" />
			<input type='button' id='btnClose' value='Back' class='imagebutton back'/>
			<input type='button' id='report-gen' value='Report' class='imagebutton pdfreport'/>
			<input type='button' id='downloadMessages' value='Download All API Messages' class='imagebutton download'/>
			<input type='button'  value='Defects' class='imagebutton defect' id='btnDefect'/>
			<c:choose>
				<c:when test="${run.status eq passStatus }">
					<input type='button' value='Re-Run Test' class='imagebutton runtestset' id='btnRun' disabled/>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${not empty run.reRunIds }">
							<input type='button' value='Re-Run Test' class='imagebutton runtestset' id='btnRun' disabled/>
						</c:when>
						<c:otherwise>
							<input type='button' value='Re-Run Test' class='imagebutton runtestset' id='btnRun' />
						</c:otherwise>
					</c:choose>					
				</c:otherwise>
			</c:choose>
			<c:if test="${(projectType eq 'Public') || (projectType eq 'Private' && currentRoleInProject ne 'Test Engineer')}">
				<input type='button' value='Send Mail' class='imagebutton mail' id='btnSendMail'/>
				<input type='button' value='Mail Log' class='imagebutton list' id='btnMailLog'  />
			</c:if>
			<img src='images/inprogress.gif' id='img_report_loader' />
			<div style="float: right; padding-right: 80px;font-size:0.7em;line-height:22px;">
				<c:if test="${not empty run.allParentRunIds || not empty run.reRunIds}">
					<span style='font-weight:bold;margin-right:10px;' id='reRunMsg'>This run has associated runs</span>
					<c:forEach items="${run.allParentRunIds }" var="parentRunId">
						<span><a href='ResultsServlet?param=run_result&run=${parentRunId }&callback=${callback}'>${parentRunId }</a></span> /
					</c:forEach>
					<span><b>${run.id }</b></span>
					<c:if test="${not empty run.reRunIds }">
						<c:forEach items="${run.reRunIds }" var="reRunId">
							 / <span><a href='ResultsServlet?param=run_result&run=${reRunId }&callback=${callback}'>${reRunId }</a></span>
						</c:forEach>
					</c:if>
				</c:if>
			</div>
		</div>
		<div id='user-message'></div>
		<div class='form' style='width:1053px;'>
			<div class='twoplusone-col-field-section'>
				<div class='fs_title'>Run Information</div>
			</div>
			<div class='three-col-field-section'>
				<div class='fs_title'>Overall Status</div>
			</div>
			<div class='three-col-field-section'>
				<table class='layout'>
					<tr>
						<td class='layout-label'>Run ID</td>
						<td class='layout-value'>${run.id }
					</tr>
					<tr>
						<td class='layout-label'>Started On</td>
						<td class='layout-value'>${run.startTimeStamp }</td>
					</tr>
					<tr>
						<td class='layout-label'>Elapsed Time</td>
						<td class='layout-value'>${run.elapsedTime }</td>
					</tr>
					<tr>
						<td class='layout-label'>Domain</td>
						<td class='layout-value' title='${sessionScope.TJN_SESSION.project.domain }'>${sessionScope.TJN_SESSION.project.domain }</td>
					</tr>
					<tr>
						<td class='layout-label'>Status</td>
						<td class='layout-value'>${run.status }</td>
					</tr>
					
				</table>
			</div>
			<div class='three-col-field-section' >
				<table class='layout'>
					<tr>
						<td class='layout-label'>Started By</td>
						<td class='layout-value'>${run.user }</td>
					</tr>
					<tr>
						<td class='layout-label'>Ended On</td>
						<td class='layout-value'>${run.endTimeStamp }</td>
					</tr>
					<tr>
						<td class='layout-label'>Test Set</td>
						<td class='layout-value' title='${run.testSet.name }'>${run.testSet.name }</td>
					</tr>
					<tr>
						<td class='layout-label'>Project</td>
						<td class='layout-value' title='${sessionScope.TJN_SESSION.project.name }'>${sessionScope.TJN_SESSION.project.name }</td>
					</tr>
					<tr>
						<td class='layout-label'>Test Type</td>
						<td class='layout-value'>API</td>
					</tr>
				</table>
			</div>
			<div class='three-col-field-section' style='height:141px;text-align:center;border:1px solid #fff'>
				<div id='run-summary-chart'></div>
			</div>
			<div class='clear'></div>
			<div class='one-col-field-section' id='allTransactions' style='height:400px;overflow-y:auto;max-height:400px;'>
				<table class='summtable' style='width:100%;'>
					<thead>
						<tr>
							<th>Status</th>
							<th style='width:50px;'>Step ID</th>
							<th>Summary</th>
							<th>TDUID</th>
							<th>Elapsed Time</th>
							<th>Message</th>
							<th>Request</th>
							<th>Response</th>
							<th>Validations</th>
							<th>Run-time Values</th>
						</tr>
					</thead>
					<c:set var="statusPass" value='S' />
					<c:set var="statusFail" value='F' />
					<c:set var="statusError" value='E' />
					<c:set var="statusExecuting" value='X' />
					<c:set var="statusNotStarted" value='N' />
					<tbody>
						<c:forEach items="${run.testSet.tests }" var="test">
							<tr><td colspan='10' class='fs_group_heading'>${test.tcId } - ${test.tcName }</td></tr>
							
							<c:forEach items="${test.tcSteps }" var="step">
								<c:forEach items="${step.detailedResults }" var="iteration">
									<tr>
										<td class='txn-status-icon'>
											<c:set var="imgUrl" value="images/error_20c20.png" />
											<c:set var="cellColor" value="red" />
											<c:choose>
												<c:when test="${iteration.result eq statusExecuting }">
													<c:set var="imgUrl" value="images/ajax-loader.gif" />
													<c:set var="endTimestamp" value="N/A" />
													<c:set var="cellColor" value="#000" />
												</c:when>
												<c:when test="${iteration.result eq statusFail }">
													<c:set var="imgUrl" value="images/failure_20c20.png" />
													<c:set var="cellColor" value="red" />
												</c:when>
												<c:when test="${iteration.result eq statusPass }">
													<c:set var="imgUrl" value="images/success_20c20.png" />
													<c:set var="cellColor" value="green" />
												</c:when>
												<c:when test="${iteration.result eq statusNotStarted }">
													<c:set var="imgUrl" value="images/queued_20c20.png" />
													<c:set var="cellColor" value="#000" />
												</c:when>
												<c:otherwise>
													<c:set var="imgUrl" value="images/error_20c20.png" />
													<c:set var="cellColor" value="red" />
												</c:otherwise>
											</c:choose>
											<img src='${imgUrl }' alt='${iteration.result }'/>
										</td>
										<td>${step.id }</td>
										<td>${step.shortDescription }</td>
										<td>${iteration.dataId }</td>
										<td>
											<c:choose>
												<c:when test="${not empty iteration.elapsedTime }">
													${iteration.elapsedTime }
												</c:when>
												<c:otherwise>
													Not Available
												</c:otherwise>
											</c:choose>
										</td>
										<td class='txn-message' style='color:${cellColor};'>${iteration.message }</td>
										<td class='txn-request'>
										<c:choose>
											<c:when test="${not empty iteration.wsReqMessage }">
												<a href='ResultsServlet?param=api_request&run=${run.id }&tsrecid=${step.recordId}&tduid=${iteration.dataId}'>Download</a>
											</c:when>
											<c:otherwise>N-A</c:otherwise>
										</c:choose>
										<td class='txn-response'>
										<c:choose>
											<c:when test="${not empty iteration.wsResMessage }">
												<a href='ResultsServlet?param=api_response&run=${run.id }&tsrecid=${step.recordId}&tduid=${iteration.dataId}'>Download</a>
											</c:when>
											<c:otherwise>N-A</c:otherwise>
										</c:choose>
										 </td>
										<td class='txn-validations'>
											<c:set var="valStatusPass" value='Pass' />
											<c:set var="valStatusFail" value='Fail' />
											<c:set var="valStatusError" value='Error' />
											
											<c:choose>
												<c:when test="${iteration.totalResultValidations gt 0 }">
													
													<c:choose>
														<c:when test="${iteration.validationStatus eq valStatusPass }">
															<a href='#' class='vValidation' data-step-rec-id='${step.recordId }' data-iteration='${iteration.iterationNo }' data-tduid='${iteration.dataId }' data-step-id='${step.id }' data-test-case-id='${test.tcId }' style='color:green;font-weight:bold;'>${iteration.validationStatus }</a>
														</c:when>
														<c:otherwise >
															<a href='#' class='vValidation' data-step-rec-id='${step.recordId }' data-iteration='${iteration.iterationNo }' data-tduid='${iteration.dataId }' data-step-id='${step.id }' data-test-case-id='${test.tcId }' style='color:red;font-weight:bold;'>${iteration.validationStatus }</a>
														</c:otherwise>
														
													</c:choose>
												</c:when>
												<c:otherwise>
													N-A
												</c:otherwise>
											</c:choose>
										</td>
										<td class='txn-runtimevalues'>
											<c:choose>
												<c:when test="${iteration.totalRuntimeValues gt 0 }">
													<a href='#' class='vRuntimeValues' data-step-rec-id='${step.recordId }' data-iteration='${iteration.iterationNo }' data-tduid='${iteration.dataId }' data-step-id='${step.id }' data-test-case-id='${test.tcId }'>View</a>
												</c:when>
												<c:otherwise>
													N-A
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
								</c:forEach>
							</c:forEach>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class='modalmask'></div>
		<div class='subframe' id='val_frame' style='left:3%'>
			<iframe style='height:500px;width:650px;overflow:hidden;' scrolling='no' seamless='seamless' id='val-res-frame' src=''></iframe>
		</div>
		
		<input type='hidden' id='h-runId' value='${run.id }' />
		<input type='hidden' id='h-projectId' value='${sessionScope.TJN_SESSION.project.id }' />
		<input type='hidden' id='h-testSetRecordId'value='${run.testSet.id }' /> 
		<input type='hidden' id='h-callback' value='${callback }' />
		<input type='hidden' id='h-testSetexecutionType' value='${testSet.recordType }' />
		<input type='hidden' id='passedTests' value='${run.passedTests }'/>
		<input type='hidden' id='failedTests' value='${run.failedTests }'/>
		<input type='hidden' id='erroredTests' value='${run.erroredTests }'/>
		<c:set var="notExecutedTests" value="${run.totalTests - run.executedTests}" />
		<input type='hidden' id='notExecutedTests' value='${notExecutedTests }'/>
		
		
	</body>
</html>