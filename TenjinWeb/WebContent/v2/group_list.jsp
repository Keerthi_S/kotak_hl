<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  group_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Insert title here</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/tenjin-aut-selector.js'></script>
		<script src="js/tables.js"></script>
		<script>
			 
			
			$(document).on('click','#btnNew',function(){
				window.location.href='GroupServlet?t=new';
				
			});
			
			$(document).on('click','#btnDelete',function(){
				clearMessagesOnElement($('#user-message'));
				if($('#group-table').find('input[type="checkbox"]:checked').length < 1) {
					showLocalizedMessage('no.records.selected', 'error');
					return false;
				}else if($('#group-table').find('input[type="checkbox"]:checked').length < 2 && $('#group-table .tbl-select-all-rows').is(':checked')) {
					showLocalizedMessage('no.records.selected', 'error');
					return false;
				}
				
				if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
					var $functionForm = $("<form action='GroupServlet' method='POST' />");
					$functionForm.append($("<input type='hidden' id='del' name='del' value='true' />"))
					$functionForm.append($("<input type='hidden' id='csrftoken_form' name='csrftoken_form' value='" + $('#csrftoken_form').val() + "' />"))
					if($('#group-table .tbl-select-all-rows').is(':checked')){
					$('#group-table').find('input[type="checkbox"]:checked').each(function() {
						if(!$(this).hasClass('tbl-select-all-rows')) {
							$functionForm.append($("<input type='hidden' name='selectedGroups' value='"+ $(this).data('group-Name') +"' />"));
						}
					});
					}else{
						for (var i in selectedItems) {
							$functionForm.append($("<input type='hidden' name='selectedGroups' value='"+ selectedItems[i] +"' />"));
						}
					}
					$('body').append($functionForm);
					$functionForm.submit();
				}
			});
			
			
		</script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<div class='title'>
			<p>AUT Groups</p>
	</div>
	
	<div class='toolbar'>
			<input type='button' value='Save' id='Save' class='imagebutton learn' style='display:none;'/>			
			<input type='button' value='New' id='btnNew' name='btnNew' class='imagebutton new'/>
			<input type='button' value='Remove' id='btnDelete' name='btnDelete' class='imagebutton delete'/>
	</div>
	
	<input type='hidden' id='callback' value='${callback }' />
	<input type='hidden' id='tjn-status' value='${screenState.status }' />
	<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
	<div id='user-message'></div>
	<div class='form container_16'>
	<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<fieldset>
			
			<legend>Groups</legend>
	
			<div class='clear'></div>
				<div id='dataGrid'>
					<table id='group-table' class='display tjn-default-data-table'>
						<thead>
								<tr>
								<th class='table-row-selector nosort'><input type='checkbox' class='tbl-select-all-rows'/></th>
								<th>Group Name</th>
								<th>Application Name</th>
								<th>Group Description</th>	
								</tr>
						</thead>
						<tbody id='tblNaviflowModules_body'>
							<c:forEach items="${groups }" var="group">
								<tr>
									<td class='table-row-selector'><input type='checkbox' class='chkBox' data-group-name='${fn:escapeXml(group.aut) }|${fn:escapeXml(group.groupName )}'/></td>
									 
									<td title="${fn:escapeXml(group.groupName)}"><a href='GroupServlet?t=view&paramval=${fn:escapeXml(group.groupName) }&a=${fn:escapeXml(group.aut)}&type=function'>${fn:escapeXml(group.groupName) }</a></td>
									<td title="${fn:escapeXml(group.appName)}">${fn:escapeXml(group.appName) }</td>
									<td title="${fn:escapeXml(group.groupDesc)}">${fn:escapeXml(group.groupDesc )}
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>					
		
	
	</div>
</body>
</html>