<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  project_users.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 

-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Project Users</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link href="css/jquery-ui.css" rel="Stylesheet">
		<link rel="SHORTCUT ICON" HREF="images/yethi.png">
		
		<script type='text/javascript' src=' js/bootstrap.min.js'></script>
		<script type='text/javascript' src='js/pages/v2/project_view.js'></script>
		<script type='text/javascript' src='js/select2.min.js'></script>
	 	<script type='text/javascript' src="js/jquery-ui.js" ></script>
	 	<script type='text/javascript' src='js/tenjin-aut-selector.js'></script>
	 	
	 	
		
		
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		
	    <link rel="stylesheet" href="css/jquery.dataTables.css">
		<script type='text/javascript' src="js/jquery.dataTables.js"></script>
		<script  type='text/javascript' src="js/tables.js"></script>
	 	
	 	
		
		<script src="js/formvalidator.js"></script>
		<style>
			#footer {
  	 			 position: fixed;
   	 			 bottom: 0;
  		     	 width: 100%;
  		     	 text-align:center
			}
		</style>
		<script type="text/javascript">
		$(document).ready(function(){
			
			$('#btnClose').click(function(){
				window.parent.closeModal();
			});
			$('#btnOk').click(function(){
				var selectedUsers = '';
				var projectId=$('#projectId').val();
				if($('#unmapped-users-table').find('input[type="checkbox"]:checked').length ==1&& $('#unmapped-users-table .tbl-select-all-rows').is(':checked')){
					   showLocalizedMessage('no.records.selected', 'error');
		  				return false;
				 }
				
				if($('#unmapped-users-table').find('input[type="checkbox"]:checked').length < 1) {
					showLocalizedMessage('no.records.selected', 'error');
					return false;
				}else if($('#unmapped-users-table').find('input[type="checkbox"]:checked').length < 2 && $('#unmapped-users-table .tbl-select-all-rows').is(':checked')) {
					showLocalizedMessage('no.records.selected', 'error');
					return false;
				}else{
					var i=0;
					$('#unmapped-users-table').find('input[type="checkbox"]:checked').each(function() {
						if(!$(this).hasClass('tbl-select-all-rows')) {
							var role='#'+$(this).data('userId');
							if(i==0)
							{
								role=$(role).val();
								 
								selectedUsers=$(this).data('userId')+":"+role;
							 
								i++;
							}else{
								role=$(role).val()
								selectedUsers=selectedUsers+","+$(this).data('userId')+":"+role;
							}
						}
					});
					
					
					$('#selectedUsers').val(selectedUsers);
					var formData = $('#associate_users_form').serialize();
					var csrftoken_form = $('#csrftoken_form').val();
					$.ajax({
						url:'ProjectAjaxServlet', 
						method:'POST',
						data:formData+'&csrftoken_form='+csrftoken_form,
						dataType:'json',
						success:function(result) {
							if(result.status === 'success') {
								showMessageOnElement(result.message,'success',window.parent.$('#user-message-users'));
								window.parent.reloadProjectUserTable();
								window.parent.closeModal();
							}else{
								showMessage(result.message, 'error');
							}
						}, error:function(xhr, textStatus, errorThrown) {
							showMessage("An internal error occurred", 'error');
						}
					});
					
				}
				
			});
			
		})
		</script>
</head>
<body>
<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
	<input type='hidden' id='projectType' value='${project.type }'/>
	<input type='hidden' id='roleInCurrentProject' value='${user.roleInCurrentProject}' />
	<input type='hidden' id='currentuserid' value=''/>
	
	<input type='hidden' id='viewType' value=''/>
		<div class='title'>
			<p>Associate Users to Project</p>
		</div>				
		
		<form id='associate_users_form' name='associate_users_form' action='ProjectAjaxServlet' method='POST'>
		<!-- added by shruthi for CSRF token starts -->
		<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<!-- added by shruthi for CSRF token ends -->
	 
			<input type='hidden' id='t' name='t' value='mapUserToProject' />
			<input type='hidden' id='selectedUsers' name='selectedUsers' value='' />
			<input type='hidden' id='projectId' name='projectId' value='${projectId}'/>
			<div id='user-message'></div>
			
			<div class='form container_16'>
				 
					<div id='dataGrid' style='width:75% ;max-height:280px; overflow:auto;'>
						<table id='unmapped-users-table' class='display tjn-default-data-table' cellspacing='0' width='100%'>
							<thead>
								<tr>
									<th class='nosort'><input type='checkbox' class='tbl-select-all-rows' id='chk_all_users' title='Select All'/></th>
									<th>User ID</th>
									<th>Full Name</th>
									<th>Email</th>
									<th>Role</th>
								</tr>
								
							</thead>
							<tbody>
								<c:forEach items="${unMappedProjectUser }" var="user">
								<tr>
									<td class='table-row-selector'><input type='checkbox'  data-user-id='${fn:escapeXml(user.id) }'/></td>
									<td>${fn:escapeXml(user.id) }</td>
									<td>${fn:escapeXml(user.fullName) }</td>
									<td>${fn:escapeXml(user.email) }</td>
									<c:choose>
										<c:when test="${project.type eq 'Private'}">
											<td><select class='projectUserRole' id='${fn:escapeXml(user.id) }'><option value='Test Engineer'>Test Engineer</option></select>
										</c:when>
										<c:otherwise>
											<td><select class='projectUserRole' id='${fn:escapeXml(user.id) }'><option value='Test Engineer'>Test Engineer</option><option value='Project Administrator'>Project Administrator</option></select></td>
										</c:otherwise>
									</c:choose>
								</tr>
								</c:forEach>
							</tbody>
				
							
						</table>
						
					</div>
					<div class='toolbar' id="footer">
						<input type='button' value='Go' class='imagebutton ok' id='btnOk' />
						<input type='button' value='Cancel' class='imagebutton cancel' id='btnClose' />
					</div>	
			</div>
		</form>

</body>
</html>