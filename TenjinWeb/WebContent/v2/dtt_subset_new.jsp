<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  dtt_subset_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
*29-May-2020           Leelaprasad             TJN210-97
  -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
    <%@page import="java.util.List"%>
    <%@page import="java.util.Map"%>
    <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DTT Subset New</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src=' js/bootstrap.min.js'></script>
		<script type='text/javascript' src='js/pages/v2/dtt_subset_new.js'></script>
		<link rel='stylesheet' href='css/paginated-table.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<div class='title'>
<p>DTT Subset New</p>
</div>

<form name='main_form' action='DTTSubsetServlet' method='POST'>
<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
	<div class='toolbar'>
	<input type='button' class='imagebutton save'value='Save' id='btnSaveProjects' />
	<input type='button' value='Cancel'
					id='btnCancel' class='imagebutton cancel' />
		<img src='images/inprogress.gif' id='dmLoader' />
	</div>
	<input type='hidden' id='tjn-status' value='${screenState.status }' />
	<input type='hidden' id='tjn-message' value='${screenState.message }' />
	<div id='user-message'></div>
	
	
	<div class='form container_16'>
		<fieldset>
			<legend>DTT Subsets</legend>
		</fieldset>
		<p  style="color:red; text-align: right;">Note: For ALM Subset functionality cannot be implemented</p>
		<div class='fieldSection grid_7'>
			<div class='grid_2'>
				<label for='lstInstance'>Instance</label>
			</div>
			<div class='grid_4'>
			     <!-- Modified by Priyanka for TENJINCG-1233 starts -->
				<select id='lstInstance' name='lstInstance' class='stdTextBoxNew' mandatory='yes' title="Instance"  maxlength="20"> 
				<!-- Modified by Priyanka for TENJINCG-1233 starts -->	
					<option value='-1'>--Select One--</option>
					<c:forEach items="${instances}" var="instance">
					<option value='${fn:escapeXml(instance) }'>${fn:escapeXml(instance) }</option>
					</c:forEach>
				</select>
				
			</div>
			
			<div class='clear'></div>
				<div class='grid_2'>
					<label for='subSetName' >Subset Name</label>							
				</div>
				<div class='grid_4'>
				    <!-- Modified by Priyanka for Tenj211-36 starts -->	
				    <input type='text' class='stdTextBox' value='${fn:escapeXml(Subset.subSetName)}' id='subSetName' name='subSetName' maxlength='15' mandatory='yes' placeholder="MaxLength 15" title="Subset Name"/>
					<%-- <input type='text' id='subSetName' name='subSetName' title='SubSet Name' class='stdTextBox' value='${fn:escapeXml(Subset.subSetName)}' mandatory='yes' placeholder="MaxLength 15" maxlenght="15"/> --%>			
				    <!-- Modified by Priyanka for Tenj211-36 ends -->	
				</div>
				
		</div>
	</div>
</form>

<div class='clear'></div>
	<div class='form container_16' >
		<fieldset>
		<legend>Defect Management Projects</legend>
		<div id="projects_table">
			<div id='dataGrid' class='fieldSection grid_6'>
				<div id='test-steps-tab' class='tab-section'>
					<table id='defectManagement-table' class='bordered' align="center" style='width:320px'>
						<thead >
							<tr  >
							<!-- Changed by leelaprasad for TJN210-97 starts -->
								<!-- <th class='table-row-selector' style='width:10px;'><input type='checkbox'  id='tbl-select-all-rows-tc' class='tbl-select-all-rows'/></th> -->
								<th class='table-row-selector' style='width:10px;'><input type='checkbox'  id='chk_all' class='tbl-select-all-rows'/></th>
							<!-- Changed by leelaprasad for TJN210-97 ends -->
								<th style='width:200px;'>Defect Management Projects / Repositories</th>
							</tr>
						</thead>
						<tbody id='projectList'>
						</tbody>
					</table>
				</div>
		</div>
	</div>
		
		</fieldset>
	</div>
</body>
</html>