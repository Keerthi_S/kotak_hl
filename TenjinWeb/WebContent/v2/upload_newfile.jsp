<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  upload_newfile.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 

-->
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Api"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Test Data Upload</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link href="css/jquery-ui.css" rel="Stylesheet">
		<link rel="SHORTCUT ICON" HREF="images/yethi.png">
		 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<script type='text/javascript' src=' js/bootstrap.min.js'></script>
		<script type='text/javascript' src='js/select2.min.js'></script>
	 	<script type='text/javascript' src="js/jquery-ui.js" ></script>
	 	<script type='text/javascript' src='js/tenjin-aut-selector.js'></script>
	 	
		<script type="text/javascript" src='js/formvalidator.js'></script>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/v2/upload_view.js'></script>
		
	    <link rel="stylesheet" href="css/jquery.dataTables.css">
		<script type='text/javascript' src="js/jquery.dataTables.js"></script>
		<script  type='text/javascript' src="js/tables.js"></script>
		<script src="js/formvalidator.js"></script>
		<style>
			#footer {
  	 			 position: fixed;
   	 			 bottom: 0;
  		     	 width: 100%;
  		     	 text-align:center
			}
			
			
			.buttonload {
       /* background-color: #4CAF50; */ /* Green background */
          background-color: #FFFFFF;
        border: none; /* Remove borders */
       color: black; /* White text */
       padding: 12px 24px; /* Some padding */
       font-size: 16px; /* Set a font-size */
   }

      /* Add a right margin to each icon */
   .fa {
    margin-left: -12px;
     margin-right: 8px;
     }
		</style>
		
</head>
<body>

<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
	<%
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Map<String, Object> map = (Map<String, Object>)request.getSession().getAttribute("EXTRACTOR_SCREEN_MAP");
		String status = (String)map.get("STATUS");
		String message = (String)map.get("MESSAGE");
		if(message == null){
			message = "";
		}
		 List<Aut> auts = (List<Aut>)map.get("AUTS");
			List<Api> apis = (List<Api>) map.get("API_LIST");
			String currentAppId = (String) map.get("CURRENT_APP_ID");
			
			String apiGroup = (String) map.get("CURRENT_GRP_NAME");
			String apiType=(String) map.get("CURRENT_API_TYPE");
			List<String> adapters=(List<String>) map.get("ADAPTERS");
		%>
     <%
       
         if(tjnSession!=null){
     %>

		
		<div class='title'>
			<p>Test Data</p>
		</div>
		<form name='upload-form' action='TestDataUploadServlet' method='POST' enctype='multipart/form-data' onsubmit="return validate()">
			<div class='toolbar'>
			<!-- added by shruthi for CSRF token starts -->
		    <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		    <!-- added by shruthi for CSRF token ends -->
	        <input type='button' class='imagebutton cancel' value='Cancel' id='btnCancel'/>
			<input type='button' class='imagebutton refresh' value='Reset' id='btnRefresh'/>
		</div>
			
			<input type='hidden' id='screenStatus' value='<%=status %>'/>
			<input type='hidden' id='screenMessage' value='<%=Utilities.escapeXml(message )%>'/>
			<%
			 if(status.equalsIgnoreCase("Failure")){
				%>
				<div id='user-message' class='msg-err' style='display:block;'>
				<div class="msg-icon">&nbsp;</div>
					<div class="msg-text"><%=message %></div>
				</div>
				<%} else{%>
					<div id='user-message'></div>
				<%}
				%>
	    
			
			<div class='form container_16'>
				<fieldset >
				<legend>Basic Information</legend>
					
					<div class='fieldSection grid_12'>
				
						<div class='grid_3'>
							<label for='lstApplication'>Application</label>
						</div>
						<div class='grid_8'>
								<select id='lstApplication' name='lstApplication' class='stdTextBox' mandatory='yes'  title="Application">
								<option value='-1'>-- Select One --</option>
								 <%
								for(Aut aut:auts){
									%>
									<option value='<%=aut.getId() %>'><%=Utilities.escapeXml(aut.getName()) %></option>
									<%
								}
								%> 
							</select>
						</div>
							<div id='validationblock'>
					<div class='grid_3'>
							<label for='validationType'>Function/API/Message</label>
						</div>
						<div class='grid_8'>
								<select id='validationType' name='validationType' class='stdTextBox' mandatory='yes' title="Validation">
								<option value=''>-- Select One --</option>
								<option value='Gui'> Function </option>
								<option value='Api'> API </option>
								<!-- Added by Ashiki for TENJINCG-1275 starts-->
								<option value='MSG'> Message </option>
								<!-- Added by Ashiki for TENJINCG-1275 ends-->
							</select>
						</div>
						<!-- <div class='clear'></div> -->
						</div>
						<!-- <div class='clear'></div> -->
						<div id='functiontype'>
						<div class='grid_3'>
							<label for='lstModules'>Function</label>
						</div>
						<div class='grid_8'>
							<select id='lstModules' name='lstModules' class='stdTextBox'  title="Function" mandatory='yes'>
								<option value='-1'>-- Select One --</option>
								
							</select>
						</div>
						<!-- <div class='clear'></div> -->
						</div>
						<div id='apitype'>
						<div class='grid_3'>
							<label for='Apitype'>API</label>
						</div>
						<div class='grid_8'>
						<%-- <input type='hidden' id='Apitype' value='<%=apiType%>'/> --%>
								<select id='Apitype' name='Apitype' class='stdTextBox'  title="Apitype" mandatory='yes'>
									<option value=''>-- Select One --</option>
								
								</select>
						</div>
						<!-- <div class='clear'></div> -->
						</div>
						<!-- Added by Ashiki for TENJINCG-1275 starts-->
						<div id='MsgFormat'>
						<div class='grid_3'>
							<label for='lstModules'>Message</label>
						</div>
						<div class='grid_8'>
							<select id='msgFormat' name='msgFormat' class='stdTextBox'  title="Format" mandatory='yes'>
								<option value='-1'>-- Select One --</option>
								
							</select>
						</div>
						<div class='clear'></div>
						</div>
						<!-- Added by Ashiki for TENJINCG-1275 ends-->
						<div class='grid_3'>
							<label for='txtInputFile'>Input File</label>	
						</div>
						<div class='grid_8'>
							<input type='file' id='txtInputFile' name="inputFilePath" class='stdTextBox' mandatory='yes' title="Input File">	
							 </br><span id="lblError" style="color: red;"></span>
							<div class='clear'></div>
						<input type='submit' id='uploadFile' value='Upload' class='imagebutton ok'/> <img src="images/inprogress.gif" class="loader-image" id="imgLoad_domain">
						</div>
					</div>
				</fieldset>
			</div>
		</form>
	<%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>
		
</body>
</html>