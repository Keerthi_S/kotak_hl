<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  user_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 
-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>User Profile Maintenance</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
	    <link rel="stylesheet" href="css/jquery.dataTables.css">
		<script type='text/javascript' src="js/jquery.dataTables.js"></script>
		<script  type='text/javascript' src="js/tables.js"></script>
		<script type="text/javascript">
	 $(document).on('click','#btnNew', function() {
			window.location.href ='TenjinUserServlet?t=new';
		});</script>
		
		
</head>
<body>
<div class='main_content'>
	<div class='title'>
			<p>User Profiles</p>
	</div>
	<div class='toolbar'>
			<input type='button' id='btnNew' class='imagebutton new' value='New User' />
			
	</div>
	<input type='hidden' id='tjn-status' value='${screenState.status }' />
	<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
	
	<div id='user-message'></div>
	<div class='form container_16'>
		<div id='dataGrid'>
			<table id='userTable' class='display tjn-default-data-table' width='100%'>
				<thead>
					<tr>
						
						<th>User ID</th>
						<th>User Name</th>
						<th>Roles</th>
						<th>Email ID</th>
						<th class="text-center">Contact No</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items='${users }' var="user">
					<tr>
						
						
						<td><a href='TenjinUserServlet?t=view&key=${fn:escapeXml(user.id) }'>${fn:escapeXml(user.id) }</a></td>
						<td><c:out value="${fn:escapeXml(user.fullName)}"></c:out></td>
						<td>${fn:escapeXml(user.roles )}</td>
						<td title='${fn:escapeXml(user.email) }'>${fn:escapeXml(user.email) }</td>
						<td class="text-center">${fn:escapeXml(user.primaryPhone )}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div> 
	</div>
</body>
</html>