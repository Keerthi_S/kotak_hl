<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  group_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Group Details - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
		<link rel="Stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
		<link rel='stylesheet' href='css/accordion.css' />
		<link rel="Stylesheet" href="css/bordered.css" />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/data-grids.js'></script>
		<script type='text/javascript' src='js/pages/v2/group_view.js'></script>
		<script type='text/javascript' src='js/tenjin-aut-selector.js'></script>
		<script type='text/javascript' src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
		<script>
		$(document).ready(function() {
 			var $tabs = $('#t_draggable2')
  			$("tbody.t_sortable").sortable({
    			connectWith: ".t_sortable",
    			items: "> tr:not(:first)",
   				 appendTo: $tabs,
   				 helper:"clone",
    			 zIndex: 999990
  			}).disableSelection();
  
 			 var $tab_items = $(".nav-tabs > li", $tabs).droppable({
   				 accept: ".t_sortable tr",
   				 hoverClass: "ui-state-hover",
   				 drop: function( event, ui ) { return false; }
 		 	});
 			 
 			
		});
		</script>
		
		
		<style type="text/css">
		.grp-func-tab{
			display:inline-block;
		}
		tbody{
     min-height:200px;
     display:block;
     min-width:400px;
    }

		.dragging li.ui-state-hover {
 			 min-width: 240px;
		}
		.dragging .ui-state-hover a {
  			color:green !important;
  			font-weight: bold;
		}

		.t_sortable tr, .ui-sortable-helper {
  			cursor: move;
		}
		.t_sortable tr:first-child {
  			cursor: default;
		}
		.ui-sortable-placeholder {
 			 background: yellow;
		}
		 
		 #t_draggable1 tr td,#t_draggable2 tr td{
			border-collapse: separate;
			 /* border-spacing:0 1em;  */
			 border-left: 0px solid;
    		border-right: 0px solid;
   		 	border-bottom:0.4em solid; 
   			border-color:		#B8B8B8;
		} 
		.bordered{
			min-width:430px;
			
		}
		.left {
  			float: left;
  			width: 425px;
  			min-height:100px;
  			text-align: right;
  			margin: 2px 10px;
  			display: inline;
			border: 1px solid #B8B8B8;
			padding-top: 5px;
    		padding-right:5px;
    		padding-bottom: 5px; 
    		padding-left: 5px; 
		}

		.right {
  			float: right;
  			width:425px;
  			min-height:100px;
 			text-align: left;
 			margin: 2px 10px;
 			display: inline;
 			border: 1px solid 	#B8B8B8;
 			padding-top: 5px;
   			padding-right:5px;
    		padding-bottom: 5px; 
    		padding-left: 5px; 
		}
		.layout-value{
			text-overflow: ellipsis; 
			overflow: hidden; 
			white-space: nowrap; 
			max-width: 150px;
		}
</style>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		<%String type=request.getParameter("type"); %>
		<div class='title'>
			<p>Group Details</p>		
		</div>
		
		<form name='main_form' id='main_form' action='GroupServlet' method='POST'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
				<input type='button' value='Back' class='imagebutton back' id='btnBack'/>
				<input type='button' value='Save' class='imagebutton save' id='btnSave'/>
			</div>
			
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message )}' />
			<input type='hidden' id='type' value='${type }'/>
		
		<div id='user-message'></div>
			<div class='form container_16' id='bodyContainer'>
				<fieldset>
					<legend>Application Information</legend>
					<div class='fieldSection grid_8'>
						<div class='clear'></div>
						<div class='grid_2'><label for='lstApplication'>Application</label></div>
						<div class='grid_5'>
							<select id='lstApplication' name='lstApplication' class='stdTextBox' value='${fn:escapeXml(group.appName) }' disabled>
							<option value='${fn:escapeXml(group.appName )}' selected>${fn:escapeXml(group.appName )}</option>
							</select>
							<input type='hidden' id='selAut' value='${fn:escapeXml(group.aut) }'/>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Group Information</legend>
				
					<div class='fieldSection grid_8'>
						<div class='clear'></div>
						<div class='grid_2'><label for='txtGroupName'>Group Name</label></div>
						<div class='grid_4'>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<input type='text' class='stdTextBox' id='txtGroupName' name='groupName' mandatory='yes' maxlength="100" title='Group Name' value ='${fn:escapeXml(group.groupName) }' disabled/>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						</div>
						</div>
						<div class='fieldSection grid_8'>
						<div class='grid_2'><label for='type'>Type</label></div>
						<div class='grid_4'>
						<%if(type.equalsIgnoreCase("function")){ %>
							<select class='stdTextBoxNew' id='type' name='type'  title='Type' >
								 
								<option value='function' selected>Function</option>
								 
								<option value='API'>API</option>
							</select>
						<%}else{ %>
							<select class='stdTextBoxNew' id='type' name='type'  title='Type' >
								<option value='function' >Function</option>
								<option value='API' selected>API</option>
							</select>
						<%} %>	
						</div>
					</div>
					<div class='fieldSection grid_16'>
					<div class='grid_2'><label for='txtGroupDesc'>Description</label></div>
					<div class='grid_13'>
					<!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<textarea name='groupDesc' id='txtGroupDesc' value='${fn:escapeXml(group.groupDesc) }'class='stdTextBox long' style='height:72px; width:89%;'maxlength="1000">${fn:escapeXml(group.groupDesc) }</textarea>
					<!-- Modified by Priyanka for TENJINCG-1233 starts -->
					</div>
					</div>
				</fieldset>
				<%if(type.equalsIgnoreCase("function")){ %>
				<fieldset>
					<legend>Function Mapped</legend>
					<div class='clear'></div>
					<br/>
					<div id='mapping' style="display:inline-block;position:relative;  max-height:300px;overflow-y: scroll;box-shadow: 0 1px 1px #D8D8D8 ;marigin-top:5px;">
						<div class="left">	
						<table id=unmap class='bordered' cellspacing='0' width='100%'>
							<thead>
								<tr>																
									<th>UnMapped Function</th>
								</tr>
							</thead>
						</table>				
						<table class="bordered" id="t_draggable1">
							<tbody class="t_sortable">
  								<tr>								
									<th Style="border-collapse: separate; border-left: 0px solid; border-right: 0px solid; min-width:195px">Function Code</th>
									<th Style="border-collapse: separate; border-left: 0px solid; border-right: 0px solid; min-width:195px">Function Name</th>
									
 								 </tr>
 								 <c:forEach items="${UNMAPPEDMODULE }" var="unMappedMod">
  								<tr>
  								
									<td class="func layout-value" Style="min-width:195px;text-align: left; " title='${fn:escapeXml(unMappedMod.moduleCode )}'>${fn:escapeXml(unMappedMod.moduleCode) }</td> 
									<td class='layout-value' Style="min-width:195px; text-align: left;" title='${fn:escapeXml(unMappedMod.moduleName) }'>${fn:escapeXml(unMappedMod.moduleName) }</td>                                                           
                                 </c:forEach>
							</tbody>
						</table>
						</div>	
						<div class="right">
						<table id=map class='bordered' cellspacing='0' width='100%'>
							<thead>
								<tr>																
									<th>Mapped Function</th>
								</tr>
							</thead>
						</table>
						<table class="bordered" id="t_draggable2">
						<tbody class="t_sortable">
   						<tr>
  							 									
							<th Style="border-collapse: separate; border-left: 0px solid; border-right: 0px solid; min-width:195px">Function Code</th>
							<th Style="border-collapse: separate; border-left: 0px solid; border-right: 0px solid; min-width:195px">Function Name</th> 
 						 </tr>
 						 <c:forEach items="${MAPPEDMODULE }" var="mappedMod">
  						 <tr> 
    							<td class="func layout-value" Style="min-width:195px" title='${fn:escapeXml(mappedMod.moduleCode )}'>${fn:escapeXml(mappedMod.moduleCode) }</td>
    							<td class='layout-value' Style="min-width:195px" title='${fn:escapeXml(mappedMod.moduleName )}'>${fn:escapeXml(mappedMod.moduleName )}</td>
  						 </tr>
  						  </c:forEach>
						</tbody>
						</table>
						</div>
					</div>
				</fieldset>
				<%} else{ %>
				
				<fieldset>
					<legend>API Mapped</legend>
					<div class='clear'></div>
					<br/>
					<div id='mapping' style="display:inline-block;position:relative; max-height:300px;overflow-y: scroll;box-shadow: 0 1px 1px #D8D8D8 ;marigin-top:7px;">
						<div class="left" >	
						<table id=unmap class='bordered' cellspacing='0' width='100%'>
							<thead>
								<tr>																
									<th>UnMapped API</th>
								</tr>
							</thead>
						</table>					
						<table class="bordered" id="t_draggable1">
							<tbody class="t_sortable">
  								<tr>
  																		
									<th Style="border-collapse: separate; border-left: 0px solid; border-right: 0px solid; min-width:195px">API Code</th>
									<th Style="border-collapse: separate; border-left: 0px solid; border-right: 0px solid; min-width:195px">API Name</th>
 								 </tr>
 								 <c:forEach items="${UNMAPPEDAPI }" var="unMappedMod">
  								<tr>
									<td class="func layout-value" Style="min-width:195px" title='${fn:escapeXml(unMappedMod.code) }'>${fn:escapeXml(unMappedMod.code) }</td>  
									<td class='layout-value' Style="min-width:195px" title='${fn:escapeXml(unMappedMod.name )}'>${fn:escapeXml(unMappedMod.name) }</td>                                                            
                                 </tr>
                                 </c:forEach>
							</tbody>
						</table>
						</div>	
						<div class="right">
						<table id=map class='bordered' cellspacing='0' width='100%'>
							<thead>
								<tr>																
									<th>Mapped API</th>
								</tr>
							</thead>
						</table>
						<table class="bordered" id="t_draggable2">
						<tbody class="t_sortable">
   						<tr>
  							 									
							<th Style="border-collapse: separate; border-left: 0px solid; border-right: 0px solid; min-width:195px">API Code</th>
							<th Style="border-collapse: separate; border-left: 0px solid; border-right: 0px solid; min-width:195px">API Name</th>
 						 </tr>
 						 <c:forEach items="${MAPPEDAPI }" var="mappedMod">
  						 <tr> 
    							<td class="func layout-value" Style="min-width:195px" title='${fn:escapeXml(mappedMod.code) }'>${fn:escapeXml(mappedMod.code) }</td>
    							<td class='layout-value' Style="min-width:195px" title='${fn:escapeXml(mappedMod.name) }'>${fn:escapeXml(mappedMod.name) }</td>
  						 </tr>
  						  </c:forEach>
						</tbody>
						</table>
						</div>
					</div>
				</fieldset>
				<%} %>
			</div>
		</form>
</body>
</html>