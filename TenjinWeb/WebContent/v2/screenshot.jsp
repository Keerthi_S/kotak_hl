<!-- Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  screenshot.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
 18-03-2020            Lokanath                TENJINCG-1190

 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@page import="com.ycs.tenjin.bridge.pojo.run.RuntimeScreenshot"%>   
 <%@page import="java.util.LinkedHashSet"%>   
 <%@page import="java.util.Map"%>
 <%@page import="com.ycs.tenjin.project.Project"%>
 <%@page import="com.ycs.tenjin.TenjinSession"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List SreenShot</title>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
<link href="css/thumbnail-slider.css" rel="stylesheet" type="text/css" />
<script src="js/thumbnail-slider.js" type="text/javascript"></script>
<script type='text/javascript' src='js/pages/screenshot.js'></script>
<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
	<link rel="Stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/data-grids.js'></script>

<style>
.image{
display:none;
position:absolute;
 
width:auto;
 

height:65%;
}
.selected{
   filter: brightness(70%);
   border:1px solid black;
}
</style>
</head>
<body>
<%
TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
Project project = null;
project = tjnSession.getProject();
 
Map<RuntimeScreenshot, String> imageMap = (Map<RuntimeScreenshot, String>)request.getAttribute("imageMap");
String status = "";
String message = "";
 
%>
<div class='title'>
	<p>Run screenshots</p>
 </div>
<div class='toolbar'>
	<input type='button' value='Back' id='btnBack' class='imagebutton back'/>
</div>
<%if(imageMap.size()==0){ %>
 
<div id='user-message' class='msg-err' style='display: block;'>No screenshots were found for this transaction</div>
<%}else{ %>
<div>
    <div id="thumbnail-slider">
        <div class="inner" >
            <ul>
            <%
            int screenshotCount=1;
					for(RuntimeScreenshot v : imageMap.keySet()){
					%>
					<li><img class="thumb" src="<%=imageMap.get(v) %>" _seqno=<%=v.getSequence()%>>
					<!-- Added by Lokanath for TNJINCG-1190 starts-->
					<div> <%=screenshotCount++%>/<%=imageMap.size()%></div></li>
					<!-- Added by Lokanath for TNJINCG-1190 ends-->
					
					<%
			}
			%>
          	</ul>
        </div>
    </div>
    <div style='margin:20px; '>
 		<%
 			String seqNum="";
					for(RuntimeScreenshot v : imageMap.keySet()){
						seqNum=seqNum + "," + v.getSequence();
						%>
							<a href="<%=imageMap.get(v) %>" target='_blank' rel="noopener"><img class="image" id="image_<%=v.getSequence()%>" src="<%=imageMap.get(v) %>" ></a>
						<%
						}
		%>		
		<input type='hidden' value='<%=seqNum%>' id="seqNo">			
	</div>
</div>
<%} %>
</body>
</html>