<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  project_auts.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 

-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Project AUT's</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />

<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script  type='text/javascript' src="js/tables.js"></script>
<script src="js/formvalidator.js"></script>
		<style>
			#footer {
  	 			 position: fixed;
   	 			 bottom: 0;
  		     	 width: 100%;
  		     	 text-align:center
			}
		</style>
<script type="text/javascript">
		$(document).ready(function(){
			$('#btnClose').click(function(){
				window.parent.closeModal();
			});
			$('#btnOk').click(function(){
				var selectedAuts = '';
				$('#unmapped-auts-table').find('input[type="checkbox"]:checked').each(function () {
				       if(this.id != 'chk_all_auts'){
				    	   selectedAuts = selectedAuts + this.id + ';';
				       }
				       
				});
				
				if(selectedAuts == ''){
					showMessage('Please choose at least one application to continue','error');
					return false;
				}
				
				clearMessages();
				showMessage('Please wait while your request is being processed','progress');
				var projectId = $('#projectId').val();
				var csrftoken_form = $("#csrftoken_form").val();
				$.ajax({
					url:'ProjectServletNew',
					data:'t=mapAutToProject&projectId=' + projectId + '&selectedAuts=' + selectedAuts+'&csrftoken_form='+csrftoken_form,
					dataType:'json',
					async:false,
					type:'POST',
					success:function(data){
						var status = data.status;
						if(status == 'SUCCESS'){
							clearMessages();
							showMessageOnElement(data.message,'success',window.parent.$('#user-message-auts'));
							window.parent.reloadProjectAutTable();
							window.parent.closeModal();
							
						}else{
							clearMessages();
							showMessage(data.message);
						}
					},
					error:function(xhr,textStatus,errorThrown){
						clearMessages();
						showMessage('An unexpected system error occurred. Please contact your System Administrator');
					}
				});
				
			});

		});
		</script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<input type='hidden' id='projectId' value='${projectId}'/>
	<input type='hidden' id='currentuserid' value='' />
	<input type='hidden' id='viewType' value='' />
	<div class='title'>
		<p>Add Applications to Project</p>
	</div>

	<form name='associate_users_form' action='ProjectServlet' method='POST'>
	<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
	
		<div id='user-message'></div>

		<div class='form container_16'>
			<fieldset style='border: 0px; margin-top: 15px;'>
				<div id='dataGrid' style='width: 70%;max-height:280px; overflow:auto;'>
					<table id='unmapped-auts-table' class='display tjn-default-data-table' cellspacing='0'
						width='100%'>
						<thead>
							<tr>
								<th class='nosort'><input type='checkbox' class='tbl-select-all-rows'
									id='chk_all_auts' title='Select All' /></th>
								<th class="text-center">ID</th>
								<th>Name</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${unMappedProjectAuts }" var="aut">
						
						<tr>
							<td class='table-row-selector'><input type='checkbox' id='${aut.id }' data-aut-id=''/></td>
							<td class="text-center">${aut.id }</td>
							<td>${fn:escapeXml(aut.name )}</td>
							<td>${aut.getApplicationTypeValue() }</td>
						</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
		</div>
		<div class='toolbar' id="footer">
			 
			<input type='button' value='Go' class='imagebutton ok' id='btnOk' />
			<input type='button' value='Cancel' class='imagebutton cancel' id='btnClose' />
		</div>
	</form>
</body>
</html>