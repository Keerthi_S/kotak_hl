<!-- Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  assistedlearning_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============

 -->


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Assisted Learning</title>
	<link rel='stylesheet' href='css/cssreset-min.css' />
	<link rel='stylesheet' href='css/ifr_main.css' />
	<link rel='stylesheet' href='css/style.css' />
	<link rel='stylesheet' href='css/tabs.css' />
	<link rel="Stylesheet" href="css/jquery.dataTables.css" />
	<link rel="Stylesheet" href="css/bordered.css" />
	<link rel='stylesheet' href='css/buttons.css' />
	<link rel='stylesheet' href='css/960_16_col.css' />
	<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
	<script type="text/javascript" src="js/jquery.dataTables.js"></script>
	<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
	<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
	<script type='text/javascript' src='js/pages/v2/assistedlearning_new.js'></script>
	<style>
	 
		td > input[type='text']
		{
    		width:98%;
    		height:25px;
		}
		
		table.dataTable tbody td{
   			padding :0px;
		}	
	</style>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<div class='title'>Assisted Learning Data</div>
		<form name='main_form' action='AssistedLearningDataServlet' method='POST' enctype='multipart/form-data' onsubmit= "return validate()">
			<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
				<input type='button' value='Back' class='imagebutton back' id='btnBack'/>
				<input type='button' value='Save' class='imagebutton save' id='btnSave'/>
				<input type='button' value='Import' class='imagebutton upload' id='btnUpload'/>
				<input type='button' value='Export' class='imagebutton download' id='btnDownload'/>
				<input type='button' value='Refresh' class='imagebutton refresh' id='btnRefresh'/>
				<img src='images/inprogress.gif' alt='In Progress..' id ='imgProgress'/>
			</div>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<div id='user-message'></div>
			<div class='form container_16'>
				<fieldset id='uploadBlock'>
					<legend>Import from Spreadsheet</legend>
					<div class='fieldSection grid_12'>
						<div class='grid_2'><label>Spreadsheet Path</label></div>
						<div class='grid_5'>
							<input type='file' id='txtFile' name='txtFile' class='stdTextBox'/>
							<br><span id="lblError" style="color: red;"></span>
						</div>
						<div class='grid_4'>	
							<input type='submit' value='Upload' id='btnUploadFile' class='imagebutton upload'/> 
							<img src='images/inprogress.gif' alt='In Progress..' id ='imgProgress1'/>						 
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Application Information</legend>
					<div class='fieldSection grid_16'>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='lstApplication'>Application</label></div>
						<div class='grid_4'>
							<input type='text' id='txtApp' name='txtApp' value='${fn:escapeXml(aut.name) }' class='stdTextBox' disabled/>
							<input type='hidden' id='appId' name='appId' value='${fn:escapeXml(aut.id )}'/>
							<input type='hidden' id='func' name='func' value='${fn:escapeXml(funcCode )}'/>
							<input type='hidden' id='flag' name='flag' value='${fn:escapeXml(records.size()) }'/>
						</div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='txtFunc'>Function</label></div>
						<div class='grid_4'>
							<input type='text' id='txtFunc' name='txtFunc' value='${fn:escapeXml(funcCode) }' class='stdTextBox' disabled/>
						</div>
					</div>
					</div>
				</fieldset>
				
				<fieldset>
					<legend>Assisted Learning Data</legend>
					<div class='fieldSection grid_16'>
					<input type='button' value='Add' class='imagebutton new' id='btnNew'/>
					<input type="button" value="Remove" id="btnDelete" class="imagebutton delete">
						<table id='assistedLearningTable' class='sortable-table display'>
							<thead>
								<tr>
									<th width='5%' style='padding:0px;'><input type='checkbox' class='tbl-select-all-rows'/></th>
									<th width='5%' style='padding-left:0px;'>Sequence</th>
									<th width='20%'>Page</th>
									<th width='20%'>Field Identifier</th>
									<th width='20%'>Identified By/Action</th>
									<th width='20%'>Input Data</th>
								</tr>
							</thead>
							<tbody id='tblNaviflowModules_body'>
							<%int seq=1;%>
								<c:forEach items="${records}" var="record">
								<tr>
									<td><input type='checkbox'/></td>
									<td><span class='sequence' id='txtSeq_<%=seq%>'><%=seq%></span></td>
									<td><input type='text' class='page' id='txtPage_<%=seq%>' value='${fn:escapeXml(record.pageAreaName) }'/></td>
									<td><input type='text' class='field' id='txtField_<%=seq%>' value='${fn:escapeXml(record.fieldName) }'/></td>
									<td><input type='text' class='identified' id='txtIdentifiedBy_<%=seq%>' value='${fn:escapeXml(record.identifiedBy) }'/></td>
									<td><input type='text' class='data' id='txtData_<%=seq%>' value='${fn:escapeXml(record.data) }'/></td>
									<%seq++;%>
								</tr>
								</c:forEach>
							</tbody>
						</table>
						<input type='hidden' id='sequenceAdd' value='<%=--seq%>'/>
					</div>
				</fieldset>
				
			</div>
		</form>
</body>
</html>