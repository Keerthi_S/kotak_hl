<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  runresult.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
 

-->
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="com.ycs.tenjin.util.HatsConstants"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib prefix = "dfmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/bootstrap.min.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/rprogress.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/jquery.jqplot.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/v2/runresult.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/jquery.jqplot.min.js'></script>
		<script type='text/javascript' src='js/jqplot.pieRenderer.min.js'></script>
	</head>
	<body style='overflow-x:hidden;'>
	<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
		<%
					
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Map<String, String> map = (Map<String,String>)request.getSession().getAttribute("SCR_MAP");
		User user = tjnSession.getUser();
		Project project=tjnSession.getProject();
		String projectType=project.getType();
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
		%>
		<input type='hidden' id='status' value='${run.status }' />
		<div class='title' style='box-sizing:content-box;'>
			<p>Test Run Summary</p>
		</div>
		
		<form name='main_form'>
			<div class='toolbar'>
			<!-- added by shruthi for CSRF token starts -->
		    <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		    <!-- added by shruthi for CSRF token ends -->
				<input type='button' id='btnBack' value='Back' class='imagebutton back' /> 
				<input type='button' id='report-gen' value='Report' class='imagebutton pdfreport' />
				<c:choose>
					<c:when test="${run.deviceFarmFlag eq 'Y'}">
							<input type='button' id='btnDeviceFarmReport' value='Device Farm Report' class='imagebutton pdfreport' />
					</c:when>
				</c:choose>
				<input type='button' value='Defects' class='imagebutton defect' id='btnDefect' />
				<input type='button' value='Re-Run Test' class='imagebutton runtestset' id='btnRun' />
				<%
				if(project.getType().equals("Public")||(project.getType().equals("Private")&& !user.getRoleInCurrentProject().equals("Test Engineer"))){ 
				%>
				<input type='button' value='Send Mail' class='imagebutton mail' id='btnSendMail'/>
				<input type='button' value='Mail Log' class='imagebutton list' id='btnMailLog'  />
				<c:if test="${run.passedTests gt 0 }">
				<input type='button' value='RunTime Values' class='imagebutton download' id='btnRunTimeValues'  />
				</c:if>
				
				<%} %>
				
				<img src='images/inprogress.gif' id='img_report_loader' />
				<c:set var="hasChildRuns" value="n" />
				<div style="float: right; padding-right: 80px;font-size:0.7em;line-height:22px;">
					<c:if test="${not empty run.allParentRunIds || not empty run.reRunIds}">
						<span style='font-weight:bold;margin-right:10px;' id='reRunMsg'>This run has associated runs</span>
						<c:forEach items="${run.allParentRunIds }" var="parentRunId">
							<span><a href='ResultsServlet?param=run_result&run=${parentRunId }&callback=${callback}'>${parentRunId }</a></span> /
						</c:forEach>
						<span><b>${run.id }</b></span>
						<c:if test="${not empty run.reRunIds }">
							<c:set var="hasChildRuns" value="y" />
							<c:forEach items="${run.reRunIds }" var="reRunId">
								 / <span><a href='ResultsServlet?param=run_result&run=${reRunId }&callback=${callback}'>${reRunId }</a></span>
							</c:forEach>
						</c:if>
					</c:if>					
				
				</div>
			</div>
			
			<input type='hidden' id='hasChildRuns' value='${hasChildRuns }' />
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<input type='hidden' id='tSetRecId' value='${run.testSet.id }' />
			<input type='hidden' id='callback' value='${callback }' />
			<input type='hidden' id='prjId' value='${sessionScope.TJN_SESSION.project.id }' /> 
			<input type='hidden' id='clientIp' value='${run.machine_ip }' /> 
			<input type='hidden' id='runid' name='runid' value='${run.id }' />
			
			
			<div id='user-message'></div>
			<div class='row' id='report-header-row'>
				<div class='col-md-8'>
					<div class='row'>
						<div class='col-md-4 field-section'>
							<table class='layout'>
								<tr>	
									<td class='layout-label' width="140px;">Run ID</td>
									<td class='layout-value' id='runId'>${run.id }</td>
								</tr>
								
								<tr>
									<td class='layout-label' width="140px;">Status</td>
									<!-- TENJINCG-886 (Sriram) -->
									<%-- <td class='layout-value' title='${run.status }' style='font-weight:bold;'>${run.status }</td> --%>
									<td class='layout-value' title='${run.status }' style='font-weight:bold;' id='runStatusCell'>${run.status }</td>
									<!-- TENJINCG-886 (Sriram) ends-->
								</tr>
								<tr>
									<td class='layout-label' width="140px;">Started By</td>
									<td class='layout-value'>${run.user }</td>
								</tr>
								
								<c:if test="${run.deviceRecId gt 0 }">
								</c:if>
									<c:choose>
										<c:when test="${not empty device }">
											<tr>
												<td class='layout-label' width="140px;">Device ID/Name</td>
												<td class='layout-value'>${device.deviceId }&nbsp;/&nbsp;${device.deviceName }&nbsp;(${device.deviceType })</td>
											</tr>
										</c:when>
										<c:otherwise>
											<tr>
												<td class='layout-label' width="140px;">Device ID/Name</td>
												<td class='layout-value'>N/A</td>
											</tr>
										</c:otherwise>
									</c:choose>
								
							</table>
						</div>
						<div class='col-md-8 field-section'>
					     <table class='layout'>
								<tr>
									<td class='layout-label' width="140px;">Test Set</td>
									<td class='layout-value' title='${run.testSet.name }'>${run.testSet.name }</td>
								</tr>
								<tr>
									<td class='layout-label' width="140px;">Run Time</td>
									<td class='layout-value'><span class='formatted-time-stamp'>${run.startTimeStamp }</span> to <span class='formatted-time-stamp'>${run.endTimeStamp }</span></td>
								</tr>
								<tr>
									<td class='layout-label'>Duration</td>
									<td class='layout-value'>${run.elapsedTime }</td>
								</tr>
								<tr>
									<td class='layout-label'>Client</td>
									<%-- <td class='layout-value'>${run.machine_ip eq "0:0:0:0:0:0:0:1" ? "Local" : run.machine_ip + ":" + run.targetPort}</td> --%>
									<td class='layout-value'>${run.machine_ip} : ${run.targetPort}</td>
								</tr>
							</table>
						</div>
					</div>
					<div class='row'>
						<div class='col-md-12' style='padding-right:0 !important'>
							<div class='test_case_summary_title field-section'>
								<span>Summary of Test Cases</span>
							</div>
						</div>
					</div>
				</div>
				<div class='col-md-4 field-section' style='padding-right:15px;'>
					<input type='hidden' id='passedTests' value='${run.passedTests }' /> 
					<input type='hidden' id='failedTests' value='${run.failedTests }' /> 
					<input type='hidden' id='erroredTests' value='${run.erroredTests }' />
					<c:set var="notExecutedTests" value="${run.totalTests - run.executedTests}" />
					<input type='hidden' id='notExecutedTests' value='${notExecutedTests }' />
					<div id='run-summary-chart'>
						
					</div>
				</div>
			</div>
			<div class='row'>
				<div class='col-md-12 field-section' style='padding-left:10px !important; padding-right: 10px !important;'>
					<table class='display' id='tc-table'>
						<thead>
							<tr>
								<th width="20px">Status</th>
								<th width="120px">Test Case ID</th>
								<th>Test Case Name</th>
								
								<th class="text-center" width="20px">Steps</th>
								<th class="text-center"  width="20px">Executed</th>
								<th class="text-center" width="20px">Passed</th>
								<th class="text-center" width="20px">Failed</th>
								<th class="text-center" width="20px">Error</th>
							
							</tr>
						</thead>
						<tbody>
							<c:set var="statusPass" value='<%=HatsConstants.ACTION_RESULT_SUCCESS%>' />
							<c:set var="statusFail" value='<%=HatsConstants.ACTION_RESULT_FAILURE%>' />
							<c:set var="statusError" value='<%=HatsConstants.ACTION_RESULT_ERROR%>' />
							<c:set var="statusExecuting" value='<%=HatsConstants.SCRIPT_EXECUTING%>' />
							<c:set var="statusNotStarted" value='Not Started' />
						
							<c:forEach items="${run.testSet.tests }" var="test">
								<c:set var="imgUrl" value="images/error_20c20.png" />
								<c:set var="endTimestamp" value="" />
								<c:choose>
									<c:when test="${test.tcStatus eq statusExecuting }">
										<c:set var="imgUrl" value="images/ajax-loader.gif" />
										<c:set var="endTimestamp" value="N/A" />
									</c:when>
									<c:when test="${test.tcStatus eq statusFail }">
										<c:set var="imgUrl" value="images/failure_20c20.png" />
									</c:when>
									<c:when test="${test.tcStatus eq statusPass }">
										<c:set var="imgUrl" value="images/success_20c20.png" />
									</c:when>
									<c:when test="${test.tcStatus eq statusNotStarted }">
										<c:set var="imgUrl" value="images/queued_20c20.png" />
									</c:when>
									<c:otherwise>
										<c:set var="imgUrl" value="images/error_20c20.png" />
									</c:otherwise>
								</c:choose>
								
								<tr class='${test.tcStatus }-row tcrow' _tcrecid='${test.tcRecId }'>
									<td title='${test.tcStatus }' style='text-align:center'>
										<img src='${imgUrl }' />
									</td>
									<td style='text-overflow: ellipsis; overflow: hidden; white-space: nowrap; max-width: 150px;' title='${test.tcId }'><a href='ResultsServlet?param=tc_result&tc=${test.tcRecId }&run=${run.id }&callback=${callback }'>${test.tcId }</a></td>
									<td style='text-overflow: ellipsis; overflow: hidden; white-space: nowrap; max-width: 150px;' title='${test.tcName }'><a href='ResultsServlet?param=tc_result&tc=${test.tcRecId }&run=${run.id }&callback=${callback }'>${test.tcName }</a></td>

									<td class="text-center">${test.totalSteps }</td>
									<td class="text-center">${test.totalExecutedSteps }</td>
									<td class="text-center">${test.totalPassedSteps }</td>
									<c:choose>
										<c:when test="${test.totalFailedSteps gt 0 }">
											<td class='red text-center'>${test.totalFailedSteps }</td>
										</c:when>
										<c:otherwise>
											<td class="text-center">${test.totalFailedSteps }</td>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${test.totalErrorredSteps gt 0 }">
											<td class='red text-center'>${test.totalErrorredSteps }</td>
										</c:when>
										<c:otherwise>
											<td class="text-center">${test.totalErrorredSteps }</td>
										</c:otherwise>
									</c:choose>
								</tr>
									
								
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<div class='modalmask'></div>
		<div class='subframe' id='screenShotOption'
			style='position: absolute; top: 100px; left: 300px; width: 450px'>
	
			<div class='subframe_title'>
				<p>Please Confirm...</p>
			</div>
			<div class='subframe_content'>
				<div class='subframe_single_message'>
					<p>Do you want Screenshot to be included in Tenjin report?</p>
				</div>
			</div>
			<div class='subframe_actions'>
				<input type='button' id='btnYes' value='Yes' class='imagebutton ok' />
				<input type='button' id='btnNo' value='No' class='imagebutton cancel' />
			</div>
		</div>
	
		<div class='subframe' id='val_frame' style='left:3%'>
			<iframe style='height:500px;width:650px;overflow:hidden;' scrolling='no' seamless='seamless' id='val-res-frame' src=''></iframe>
		</div>
	</body>
</html>

