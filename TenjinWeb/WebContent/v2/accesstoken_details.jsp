<!-- 
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  accesstoken_details.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
 
*  -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %> 
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>oAuth Details</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/formvalidator.js'></script>
		<script src="js/pages/v2/accesstoken_details.js"></script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<div class='title'>
		<p>Access Token Information</p>
	</div>
	<form id='oauthform' name='oauthform' action='ApplicationUserServlet' method='POST'>
	
	 <input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<div class='toolbar'>
			<input type='button' id='btnBack' value='Back' class='imagebutton back' />
		    <input type='submit' id='btnSave' value='Save' class='imagebutton save' />
		    <input type='button' id='btnEdit' value='Edit' class='imagebutton edit' />
		    <input type='button' id='btnAccessToken' value='Get Access Token' class='imagebutton new' />
			<input type='hidden' value='${oauth.uacId}' id='uacId' name='uacId'>
			<input type='hidden' value='${oauth.appId}' id='appId' name='appId'>
			<input type='hidden' value='${oauth.loginUserType}' id='userType' name='userType'>
			<input type='hidden' value='<%=request.getParameter("caller") %>' id='caller' name='caller'>
			<input type='hidden' id='closeFlag' name='closeFlag' value='${closeFlag}'>
			<input type='hidden' value='saveAccessToken' id='del' name='del'>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<input type='hidden' id='grantTypeHide' name='grantTypeHide' value='${grantType}'>
		</div>
		<div id='user-message'></div>
		<div class='form container_16'>
				<div class='fieldSection grid_16'>
					<div class='clear'></div>
					<div class='grid_3'>
						<label for='accessToken'>Access Token</label>							
					</div>
					<div class='grid_12'>
				    	<input type='text' id='accessToken' name='accessToken' class='stdTextBox medium' value='${oauth.accessToken}' mandatory ='yes' readonly="readonly"/>
					</div>
				</div>
				 <div class='fieldSection grid_16'>
					<div class='clear'></div>
					<div class='grid_3'>
						<label for='tokenGenTime'>Token Generation Time</label>							
					</div>
					<div class='grid_3'>
				    	<input type='text' id='tokenGenTime' name='tokenGenTime' class='stdTextBox' value= '<fmt:formatDate value="${oauth.tokenGenTime}" pattern="dd-MM-yyyy HH:mm:ss" />' readonly="readonly" placeholder="dd-MM-yyyy HH:mm:ss"/>
					</div>
				</div>
				<div class='fieldSection grid_16'>
					<div class='clear'></div>
					<div class='grid_3'>
						<label for='tokenValidity'>Token Validity</label>							
					</div>
					<div class='grid_5'>
				    	<input type='text' id='tokenValidity' name='tokenValidity' class='stdTextBox' value='${oauth.tokenValidity}' readonly="readonly"/> &nbsp <b>Seconds</b>
					</div>
				</div>
				<div class='clear'></div>
				<div class='fieldSection grid_16'>
					<div class='clear'></div>
					<div class='grid_3'>
						<label for='refreshToken'>Refresh Token</label>							
					</div>
					<div class='grid_12'>
				    	<input type='text' id='refreshToken' name='refreshToken' class='stdTextBox medium' value='${oauth.refreshToken}' mandatory ='yes' readonly="readonly"/>
					</div>
					
				</div>
				 
		</div>
	</form>
</body>
</html>