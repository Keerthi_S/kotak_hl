<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testcase_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
 19-11-202              Priyanka                TENJINCG-1231
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TestCase List</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src=' js/bootstrap.min.js'></script>
		<script type='text/javascript' src='js/pages/v2/testcase_list.js'></script>
		<script type='text/javascript' src='js/pages/v2/testcase_master.js'></script>
		<link href="css/jquery-ui.css" rel="Stylesheet">
		<script type='text/javascript' src="js/jquery-ui.js" ></script>
		<script type='text/javascript' src='js/select2.min.js'></script>
		<style>
		#selectedFiles {
			margin-left: 0;
		}
		.att_block {
			padding: 5px;
			border: 1px solid #707a86;
			background-color: #b7c0ef;
			border-radius: 5px;
			margin-left: 0;
		}
		.att_block p {
			overflow: hidden;
			text-overflow: ellipsis;
		}

		#uploadTestCases, #syncTestCases, #def_linkage_dialog {
			background-color: #fff;
		}
		.subframe_content {
			display: block;
			width: 100%;
			margin: 0 auto;
			min-height: 120px;
			padding-bottom: 20px;
		}
		.subframe_title {
			width: 644px;
			height: 20px;
			background: url('images/bg.gif');
			color: white;
			font-family: 'Open Sans';
			padding-top: 6px;
			padding-bottom: 6px;
			padding-left: 6px;
			font-weight: bold;
			margin-bottom: 10px;
		}
		.subframe_actions {
			display: inline-block;
			width: 100%;
			background-color: #ccc;
			padding-top: 5px;
			padding-bottom: 5px;
			font-size: 1.2em;
			text-align: center;
		}
		.highlight-icon-holder {
			display: block;
			max-height: 50px;
			margin-bottom: 10px;
			text-align: center;
		}
		.highlight-icon-holder>img {
			height: 50px;
		}
		.subframe_single_message {
			width: 80%;
			text-align: center;
			font-weight: bold;
			display: block;
			margin: 0 auto;
			font-size: 1.3em;
		}
		#progressBar {
			width: 475px;
			margin-top: 20px;
			margin-left: auto;
			margin-right: auto;
		}
		p.subtitle {
			display: block;
			float: right;
			font-weight: bold;
			font-size: 12px;
			line-height: 22px;
			margin-right: 10px;
			color: red;
		}
		.import_message{
			font-size: 1.1em;
		}
		.importbutton{
			font-size: 1.24em;
   	    	height: 30px;
    		margin-right: 18px
		}
		#import_buttons{
			padding-top:39px ; 
			padding-left:60px;
		}
		

/
.modal-open {
	overflow: hidden;
}

.modal {
	display: none;
	overflow: hidden;
	position: fixed;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	z-index: 1050;
	-webkit-overflow-scrolling: touch;
	outline: 0;
}

.modal.fade .modal-dialog {
	-webkit-transform: translate(0, -25%);
	-ms-transform: translate(0, -25%);
	-o-transform: translate(0, -25%);
	transform: translate(0, -25%);
	-webkit-transition: -webkit-transform 0.3s ease-out;
	-o-transition: -o-transform 0.3s ease-out;
	transition: transform 0.3s ease-out;
}

.modal.in .modal-dialog {
	-webkit-transform: translate(0, 0);
	-ms-transform: translate(0, 0);
	-o-transform: translate(0, 0);
	transform: translate(0, 0);
}

.modal-open .modal {
	overflow-x: hidden;
	overflow-y: auto;
}

.modal-dialog {
	position: relative;
	width: auto;
	margin: 10px;
}

.modal-content {
	position: relative;
	background-color: #ffffff;
	border: 1px solid #999999;
	border: 1px solid rgba(0, 0, 0, 0.2);
	border-radius: 6px;
	-webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
	box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
	-webkit-background-clip: padding-box;
	background-clip: padding-box;
	outline: 0;
}

.modal-backdrop {
	position: fixed;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	z-index: 1040;
	background-color: #000000;
}

.modal-backdrop.fade {
	opacity: 0;
	filter: alpha(opacity = 0);
}

.modal-backdrop.in {
	opacity: 0.5;
	filter: alpha(opacity = 50);
}

.modal-header {
	padding: 15px;
	border-bottom: 1px solid #e5e5e5;
	min-height: 16.42857143px;
}

.modal-header .close {
	margin-top: -2px;
}

.modal-title {
	margin: 0;
	line-height: 1.42857143;
}

.modal-body {
	position: relative;
	padding: 15px;
}

.modal-footer {
	padding: 15px;
	text-align: right;
	border-top: 1px solid #e5e5e5;
}

.modal-footer .btn+.btn {
	margin-left: 5px;
	margin-bottom: 0;
}

.modal-footer .btn-group .btn+.btn {
	margin-left: -1px;
}

.modal-footer .btn-block+.btn-block {
	margin-left: 0;
}

.modal-scrollbar-measure {
	position: absolute;
	top: -9999px;
	width: 50px;
	height: 50px;
	overflow: scroll;
}

.close {
	float: right;
	font-size: 21px;
	font-weight: bold;
	line-height: 1;
	color: #000;
	text-shadow: 0 1px 0 #fff;
	filter: alpha(opacity = 20);
	opacity: .2;
}

button.close {
	-webkit-appearance: none;
	padding: 0;
	cursor: pointer;
	background: transparent;
	border: 0;
}

.close:hover, .close:focus {
	color: #000;
	text-decoration: none;
	cursor: pointer;
	filter: alpha(opacity = 50);
	opacity: .5;
}

#btnImportLogs {
	background-colour: blue;
}
.text-center{
	text-align:center
}
</style>
<style type="text/css" media="all">
.ui-autocomplete { 
    position: absolute; 
    cursor: default; 
    height: 200px; 
    overflow-y: scroll; 
    overflow-x: hidden;
    }
</style>
		
</head>
<body>
<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
<%Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
	List<String> listExceptions =null;
	if(map!=null)
	{
 	listExceptions = (List<String>) map.get("listExceptions");
	}
%>
	<div class='title'>
			
			<p>All Test Cases</p>
			  
			
		</div>
		<form name='main_form' action='NaviflowServlet' method='POST'>
			<div class='toolbar'>
			 <!-- added by shruthi for CSRF token starts -->
		     <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		     <!-- added by shruthi for CSRF token ends -->
				<input type='button' value='New' id='btnNew' class='imagebutton new'/>
				
				<input type='button' value='Remove' id='btnDelete' class='imagebutton delete'/>
				
				<input type='button' value='Upload' id='import' class='imagebutton upload'/>
				<input type='button' value='Download' id='Download' class='imagebutton download'/>
				<input type='button' id='btnAddToTestSet' value='Add to Test Set' class='imagebutton new'/>
				<input type='button' value='Synchronize' id='btnSync' class='imagebutton refresh'/>
				<!-- Added by Priyanka starts -->
				<input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh'/>
				<!-- Added by Priyanka ends -->
				 
				<%if(listExceptions!=null&&listExceptions.size()>0){ %>
					<input type="button" value="Logs" id="btnImportLogs"
						class="imagebutton list" > 
				<%} %>
				
				
					
			<%if(listExceptions!=null&&listExceptions.size()>0){ %>
		<div id="importLogs" class="modal fade">
					<div class="modal-dialog" style='height: 400px; width:600px;'>

						<div class="modal-content">
							<div class="modal-header">
								<p>Import Logs</p>
							</div>
							
							<div class="modal-body">
							<%for(int i=0;i<listExceptions.size();i++){ %>
								<p><%=listExceptions.get(i) %></p>
							<%} %>
							</div>
							<div class="modal-footer">
								<input type="button" value='Close' class="imagebutton cancel" data-dismiss="modal"> 
							</div>
						</div>

					</div>
				</div>
			<%} %>
			</div>
			
			
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<input type='hidden' id='domain' name='domain' value='${project.domain}'/>
			<input type='hidden' id='projectName' name='projectName' value='${project.name }'/>
			<input type='hidden' id='pid' name='pid' value='${project.id }'/>
			<input type='hidden' id='user' name='user' value='${user }'/>
			<input type='hidden' id='downloadPath' name='downloadPath' value='${fn:escapeXml(downloadPath) }'/>
			<!-- Added by Priyanka for TENJINCG-1231 starts -->
			<input type='hidden' name='edate' id='endDate' value='${edate }'/>
			<!-- Added by Priyanka for TENJINCG-1231 ends -->
			<div id='user-message'></div>
			<div class='form container_16'>
				<fieldset>
					<legend >Search</legend>
					<div id='searchArea'>
						<div class='fieldSection grid_7 alpha omega'>
							<div class='grid_2' style='text-align:right;'><label for='searchTcId'>Test Case ID</label></div>
							<div class='grid_5 alpha omega'><input type='text' class='stdTextBox' id='searchTcId' name='txtTcId' value='${tcId }'/></div>
							
							<div class='clear'></div>
							<div class='grid_2' style='text-align:right;'>
								<label for='searchTcName'>Contains text</label>
							</div>
							<div class='grid_5 alpha omega'><input type='text' class='stdTextBox' id='searchTcName' name='txtTcName' value='${tcName }'/></div>
						</div>
						
						<div class='fieldSection grid_7 alpha omega'>
							<div class='grid_2' style='text-align:right;'><label for='lstType'>Type</label></div>
							<div class='grid_5 alpha omega'>
								<input type='hidden' id='selTcTypeFilter' value='${tcType}'/>
								
								<select id='lstType' name='searchTcType' class='stdTextBox' value='tcType'>
									<option value=''>-- Select One --</option>
								    <option value='Acceptance'>Acceptance</option>
									<option value='Accessibility'>Accessibility</option>
									<option value='Compatibility'>Compatibility</option>
									<option value='Regression'>Regression</option>
									<option value='Functional'>Functional</option>
									<option value='Smoke'>Smoke</option>
									<option value='Usability'>Usability</option>
									<option value='Other'>Other</option>
								</select>
							</div>
							
							<div class='clear'></div>
						 
							<div class='grid_2' >
								<label for='searchLabel'>Label</label>
							</div>
							<div class='grid_5 alpha omega'>
							<input type="search" id="searchLabel" class='stdTextBox' name="searchLabel">
							</div>
							<div class='clear'></div>
							
							<div class='fieldSection grid_5 alpha omega' style='float:left'>
								<input type='button' id='btnSearch' value='Search' class='imagebutton search' style='text-align:right;' />
								<input type='button' id='btnReset' value='Reset' class='imagebutton reset' style='text-align:right;'/>
							</div> 
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>All Test Cases <span id='Selected_count'></span>
					</legend>
				</fieldset>
				<div id='dataGrid'>
					<table id='testcase-table' class='display' style='width:100%;'>
							<thead>
								<tr>
									<th class='table-row-selector nosort'><input type='checkbox'  id='tbl-select-all-rows-tc' class='tbl-select-all-rows'/></th>
									<th>Test Case ID</th>
									<th>Name</th>
									<th>Type</th>
									<th>Created On</th>
									<th>Storage</th>
								</tr>
							</thead>
						 
						</table>
				</div>
			</div>
		</form>
		<div class='modalmask'></div>
			<div class='subframe' id='uploadTestCases' style='position:absolute;top:100px;left:250px;'>
				
				<div class='subframe_title'><p>Please confirm the source</p></div>
				<div class='subframe_content'>
				
					 
					<div class='subframe_single_message import_message'>
						<p>Choose the source from which you want to import?</p>
					</div>
					<div id='import_buttons' >
						<input type='button' id='btnExcel' value='From Excel' class='imagebutton template importbutton'/>
						<input type='button' id='btnTM' value='From Test Manager' class='imagebutton tm_mapping importbutton'/>
						<input type='button' id='btnPrj' value='From another Project' class='imagebutton project importbutton'/>
					</div>
				</div>
				<div class='subframe_actions'>
				 
				
				<input type='button' id='btnCCancel' value='Cancel' class='imagebutton cancel'/> 
			</div>
			</div>
		
			
			<div class='subframe' id='syncTestCases' style='position:absolute;top:100px;left:250px;'>
			 
				<div class='subframe_title'><p>Please confirm  the synchronization</p></div>
				<div class='subframe_content'>
					<div class='highlight-icon-holder'>
						<img src='images/warning_confirm.png' alt='Please Confirm' />
					</div>
					<div class='subframe_single_message'>
					
					<p>Synchronization will make you lose all the changes made to selected test cases, and It will take some time. Do you still want to proceed?</p>
						 
					</div>
				</div>
			<div class='subframe_actions'>
				<input type='button' id='btnCOk' value='Proceed anyway' class='imagebutton ok'/>
				<input type='button' id='btnTCancel' value='Do not Proceed' class='imagebutton cancel'/> 
			</div>
			</div>
			
			<div class='subframe' id='uploadExcel' style='position:absolute;top:100px;left:250px;'>
				 <iframe src='' scrolling="no" seamless="seamless"></iframe> 
			</div>
			
			
			<div class='subframe' id='addToTestSet' style='position:absolute;top:100px;left:250px;'>
				<iframe src='' scrolling='no' seamless='seamless' style='height:450px;width:557px;'></iframe>
			</div>
			
			
</body>
</html>