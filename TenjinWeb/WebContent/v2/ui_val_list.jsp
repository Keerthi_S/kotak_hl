<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: ui_val_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 

-->

<%@page import="com.ycs.tenjin.project.Project"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>UI Validations for Step - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<link rel="stylesheet" href="css/jquery.dataTables.css">
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/ui_val_list.js'></script> 
		<script src="js/jquery.dataTables.js"></script>
		<script src="js/tables.js"></script>
	</head>
	<body>
	<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
		<%
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		Project project = null;
		project = tjnSession.getProject();
		%>
		
		
		<div class='title'>
			<p>UI Validations for Step</p>
			 
		</div>
		
		<form name='main_form' action='' method='POST'>
		 <!-- added by shruthi for CSRF token starts -->
		    <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		    <!-- added by shruthi for CSRF token ends -->
		<input type='hidden' id='tstepRecId' value='${stepRecId}' />
		<div class='toolbar'>
				<input type='button' class='imagebutton back' value='Back' id='btnBack' />
				<input type='button' class='imagebutton new' value='New Validation Step' id='btnNew' />
				 
				<input type='button' class='imagebutton delete' value='Remove' id='btnDelete'/> 
			 
			</div>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<div id='user-message'></div>
			<div class='form container_16'>
				<fieldset>
					<legend>Test Step Information</legend>
					<div class='fieldSection grid_7'>
						<div class='grid_2' style='text-align:right;'><label for='testCase'>Test Case</label></div>
						<div class='grid_4'>
							<input type='text' id='testCase' name='testCase' class='stdTextBox' readonly value='${step.parentTestCaseId }'/>
						</div>
						
						<div class='clear'></div>
						
						<div class='grid_2'  style='text-align:right;'><label for='application'>Application</label></div>
						<div class='grid_4'>
							<input type='text' id='application' name='application' class='stdTextBox' readonly value='${step.appName}'/>
						</div>
						
					</div>
					
					<div class='fieldSection grid_7'>
						<div class='grid_2' style='text-align:right;'><label for='testStep'>Step</label></div>
						<div class='grid_4'>
							<input type='text' id='testStep' name='testStep' class='stdTextBox' readonly value='${step.id}'/>
						</div>
						
						<div class='clear'></div>
						
						<div class='grid_2' style='text-align:right;'><label for='function'>Function</label></div>
						<div class='grid_4'>
							<input type='text' id='function' name='function' class='stdTextBox' readonly value='${step.moduleCode}'/>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>UI Validations for this step</legend>
					<div class='form container_16'>
			<div id='dataGrid'>
				<table id='ui_val_Table' class='display tjn-default-data-table'>
					<thead>
						<tr>
							<th class='table-row-selector nosort'><input type='checkbox' class='tbl-select-all-rows'/></th>
							<th>Validation ID</th>
							<th>Validation</th>
							<th>Description</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${vals}" var="vals">
							<tr>
								<td class='table-row-selector'><input type='checkbox' data-ui-val-rec='${vals.recordId }' /></td>
								<td style='width:80px;'>${vals.recordId }</td>
								<td style='width:130px;' title='${vals.type.name}'><a href='UIValidationServlet?param=view_ui_val_step&valrecid=${vals.recordId }'>${vals.type.name}</a></td>
							   <td title='${vals.type.description}'>${vals.type.description}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
				</fieldset>
			</div>
		</form>
		
	</body>
</html>