<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  project_runs.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
 28-09-2020				Ashiki					TENJINCG-1211
 
 
*/ --> 

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>

<!DOCTYPE html>
<html>
	<head>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/jquery-ui.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<link rel='stylesheet' href='css/bootstrap.min.css' />
		<link rel='stylesheet' href='css/tenjin-bootstrap.css' />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery-1.11.4-ui.js"></script>
		<script type='text/javascript' src=' js/bootstrap.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/tenjin-aut-selector.js'></script>
		<script type='text/javascript' src='js/pages/v2/project_runs.js'></script>
		
		<style>
		.subframe_title1{
		    width: auto;
			height: 20px;
			background: url('images/bg.gif');
			color: white;
			font-family: 'Open Sans';
			padding-top: 6px;
			padding-bottom: 6px; 
			padding-left: 6px;
			font-weight: bold;
		}
		.ui-datepicker-trigger {
		    margin-bottom:5px;
		 
		}
		.modal-footer{
		 text-align: center; 
		}  
		.text-center{
			text-align:center
		}
		</style>
	</head>
	<body>
		<input type='hidden' id='h-projectId' value='${sessionScope.TJN_SESSION.project.id }' />
		<input type='hidden' id='h-currentUserId' value='${sessionScope.TJN_SESSION.user.id }' />
		<!-- Added by Pushpalatha for TENJING-1223 starts -->
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${screenState.message }' />
		<!-- Added by Pushpalatha for TENJING-1223 ends -->
		<div class='title' style='box-sizing: content-box; font-size: 12px;'>
			<p>Test Runs in this Project</p>
			 
		</div>
		<div class='toolbar'>
			<input type="button" value="Filter" id="btnfilter" class="imagebutton search" data-toggle="modal" data-target="#filtermodal" /> 
			<input type="button" value="Only My Runs" id="btnMyRuns" class="imagebutton user" />
			<input type='button' value='Show all Runs' id='showAllRuns' class='imagebutton refresh' />
			<!-- Added by Pushpa for TENJINCG-1210 starts -->
			<input type='button' value='Download' id='download' class='imagebutton download'/>
			<!-- Added by Pushpa for TENJINCG-1210 ends -->
			<!-- Added by Pushpa for TENJINCG-1223 starts -->
			<input type="button" value="Delete" id="btnDelete" class='imagebutton delete' data-toggle="modal" data-target="#deletemodal" /> 			
			<!-- Added by Pushpa for TENJINCG-1223 ends -->
			<!-- Added by Ashiki for TENJINCG-1211 starts -->
			<input type='button' value='consolated' id='consolated' class='imagebutton download'/>
			<img src='images/inprogress.gif' id='inProgress'/>
			<!-- Added by Ashiki for TENJINCG-1211 ends -->
			
		</div>
		<!-- Added by Pushpalatha for TENJING-1223 starts -->
		<div id='user-message'></div>
		<!-- Added by Pushpalatha for TENJING-1223 starts -->
		
		<div class='form'>
			<div class='dataGrid' style='margin-top: 5px;padding:10px;'>
				<table class='display' id='prj_runs' style='width:100%;'>
					<thead>
						<tr>
							<th class='table-row-selector nosort'><input type='checkbox'  id='tbl-select-all-rows-runs' class='tbl-select-all-rows'/></th>
							<th class="text-center">Run Id</th>
							<th>Test Set</th>
							<th>Started By</th>
							 
							<th>Start Time</th>
							<th>End Time</th>
							<th class="text-center">Elapsed Time</th>
							<th>Status</th>
							<th>Execution Level</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		
		
		
		<div class="modal fade" id="filtermodal" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title title  " style='box-sizing: content-box; font-size: 12px;'>Filter your search</h4>
				</div>
				<div id='user-message'></div>
				<form>
					<div class="modal-body  container_16">
						
						<fieldset>
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label for='txtRunId'>Run ID</label></div>
								<div class='grid_4'> <input type='number' id='txtRunId' name='runId' value='' class='stdTextBox2' /> </div>
								
								<div class='clear'></div>
								<div class='grid_2'><label for='startedBy'>Started By</label></div>
								<div class='grid_4'>
									<select class='stdTextBox' id='startedBy' name='startedBy'>
										<option value=''>-- Select One --</option>
									</select>
								</div>
								
								<div class='clear'></div>
								<div class='grid_2'><label for='startedOn'>Started On</label></div>
								<div class='grid_4'> <input type='text' id='startedOn' name='startedOn' value='' class='stdTextBox2' style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value=''/> </div>
								<div class='clear'></div>
								<div class='grid_2'><label for='result'>Result</label></div>
								<div class='grid_4'>
									<select class='stdTextBox' id='result' name='result'>
										<option value=''>--Select One--</option>
										<option value='Pass'>Pass</option>
										<option value='Fail'>Fail</option>
										<option value='Error'>Error</option>
										<option value='Not Started'>Not Started</option>
										<option value='Executing'>Executing</option>
									</select>
								</div>
							</div>
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label for='testCaseId'>Test Case ID</label></div>
								<div class='grid_4'> <input type='text' id='testCaseId' name='testCaseId' value='' class='stdTextBox' /> </div>
								
								<div class='clear'></div>
								<div class='grid_2'><label for='testStepId'>Test Step ID</label></div>
								<div class='grid_4'> <input type='text' id='testStepId' name='testStepId' value='' class='stdTextBox' /> </div>
								
								<div class='clear'></div>
								<div class='grid_2'><label for='application'>Application</label></div>
								<div class='grid_4'>
									 
									<tjn:applicationSelector title='Application' functionSelector="function" cssClass="stdTextBox" projectId='${sessionScope.TJN_SESSION.project.id }' loadFunctions='true' id="application" name="application"/>
								</div>
								
								<div class='clear'></div>
								<div class='grid_2'><label for='function'>Function</label></div>
								<div class='grid_4'>
									 
									
									<tjn:functionSelector title='Function' cssClass="stdTextBox" id='function' name="function" />
								</div>
							</div>
						</fieldset>
						
					</div>
					<div class="modal-footer">
						<div class='toolbar'>
							<input type='button' value='Search' id='btnSearch' class='imagebutton search ' />
							<input type='reset' value='Reset' id='btnReset' class='imagebutton reset' />
							<button type="button" class="imagebutton cancel" id="btnClose" data-dismiss="modal" onclick="this.form.reset();">Close</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Added by Pushpa for TENJINCG-1223 starts -->
	<div class="modal fade" id="deletemodal" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header ">
					<h4 class="modal-title title  " style='box-sizing: content-box; font-size: 12px;'>Delete Runs </h4>
				</div>
				<div id='user-message'></div>
				<form>
					<div class="modal-body  container_16">
						
						<fieldset>
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label for='txtDefect'>Keep defects.</label></div>
								<div class='grid_4'> <input type='checkbox'  id='preserveDefect'  value='yes' class='tbl-select-all-rows'/></div>
							
							</div>
							<div class='clear'></div>
							<div class='fieldSection grid_7'>
							<div class='grid_9'>
							<span style='font-size:12px; color:red;'>* Runs deletion is permanent, Please take back up of runs if needed</span>
							<div class='clear'></div>
							<span style='font-size:12px; color:red;'>* Deletion runs will delete the consecutive child runs</span>
							</div>
							</div>
						</fieldset>
						
					</div>
					<div class="modal-footer">
						<div class='toolbar'>
							<input type='button' value='Ok' id='btnOk' class='imagebutton ok ' />
							<button type="button" class="imagebutton cancel" id="btnClose" data-dismiss="modal" onclick="this.form.reset();">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Added by Pushpa for TENJINCG-1223 starts -->
		
	</body>
</html>