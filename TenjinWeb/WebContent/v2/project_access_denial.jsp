<!-- /***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  project_access_denial.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page import="java.util.Map"%>
	<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tenjin - Intelligent Enterprise Testing Engine</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/landing.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel='stylesheet' href='css/select2.min.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<link rel="SHORTCUT ICON" HREF="images/yethi.png">
<script>
	$(document).ready(function(){
		$('#logout').click(function(){
			window.location.href='TenjinSessionServlet?t=logout';
		});
		$('#adminPanel').click(function(){
			$('#admin-form').submit();
		});
		$('#logged-in-user').click(function(){
			$('#param').val('user_view');
			$('#entry').val('');
			$('#admin-form').submit();
			
		})
		$('#adminPanel').click(function(){
			$('#admin-form').submit();
		});
		
		 $('#adminPanel').hover(function () {
	                $(this).css({"text-decoration":"underline"});
	        });
		 $('#logged-in-user').hover(function () {
	          $(this).css({"text-decoration":"underline"});
	      });
	      $('#logged-in-user').mouseout(function() {
		 $(this).css({"text-decoration":"none"});
		 });
	
	});
	</script>
</head>
<body>
<% 
String license_notification = "";
try{
Map<String, String> maplice = (Map<String,String>)request.getSession().getAttribute("LICENSE_NOTIFICATION");
	license_notification=(String)maplice.get("MESSAGE");
}
catch (NullPointerException e ){
	license_notification = "";
}
  %>
  <%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		<div class="wrapper">
		<div class='header'>
				<div class='header-left' style='width:270px;'>
			
				<img src='images/tenjin_logo.png'
					alt='Tenjin - Intelligent Testing Engine' style='width: 125px;' />
					</div>
				<div class='header-middle'></div>
				<div class='header-actions'><a id='logout' name='logout' href='#' title='Logout'><img src='images/Logout.png' alt='Logout'/></a></div>
				<div class='header-right'>
				 
					<a  href='#' class='adminPanel' id='adminPanel' title='Click to view administration panel'>Administration</a>
					<a  href='#' id='logged-in-user' title='Click to View/Edit your profile' >${sessionScope.TJN_SESSION.user.fullName }</a>	
					<c:if test="${sessionScope.TJN_SESSION.user.lastLogin ne 'null' }">
					<div class='note'>
						Last Login:${sessionScope.TJN_SESSION.user.lastLogin }
						</div>
					</c:if>
				</div>
			</div>
		<div class='main'>
			<div class='container_16' id='main-block'>
				<div class='grid_1' style='height: 100%;'></div>
				<div class='grid_14' style='height: 100%;'>
					<div id='landing_image' class='grid_14'>
					<img src='images/tenjin_logo_211.png'alt='Tenjin V2.0 - Intelligent Enterprise Testing Engine'style='margin: 0 auto; display: block; height: 100%;' />
					</div>
					
					<div id='landing_action_block' class='grid_14'>
						<div class='grid_3' style='height: 100%;'></div>
							<div class='grid_7' style='height: 100%;'>
								<div id='landing_action_project_selection' style="width: 470px; height: 150px; padding-top:10px">
									<b><p style="color:red">Sorry!!! Tenjin is not able to display the Project that you set as preference.</p><br>
									<div style="line-height:20px;">
									This may be because any of the following reasons:</b><br>
									1.You may be unmapped from the project.<br>
									2.Project may be deactivated.
									</div>
									<br>
									Please click <b><a href='UserLandingServlet?key=landing'>Go to Project</a> </b>for project selection.
								</div>
					 </div>

					</div>
					<div>
						<form name='admin-form' id='admin-form'action='AdminLaunchServlet' method='POST'>
						<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
							<input type='hidden' name='entry' id="entry" value='project'/>
							<input type="hidden" value="" name ="param" id="param"/>
						</form>
					</div>
				</div>
			</div>
			
		<div class='footer'> 
				<div class='footer-left'>&nbsp;
				</div>
				<div class='footer-middle' style="padding-top: 7.5px">&nbsp; <b><font color="red" style="font-size:10px"> <%=license_notification%></font></b></div>
				 
				<div class='footer-right'>&copy Copyright 2014 - <%=new java.text.SimpleDateFormat("yyyy").format(new java.util.Date()) %> <a href='http://www.yethi.in' target='_blank' rel="noopener">Yethi Consulting Pvt. Ltd.</a> All Rights Reserved</div>
			</div>
			</div>
	
</body>
</html>