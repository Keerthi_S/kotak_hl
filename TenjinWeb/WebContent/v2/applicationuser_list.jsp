<!-- 
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  applicationuser_list.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
  -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<title>Application Credentials</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<link rel="stylesheet" href="css/jquery.dataTables.css">
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script src="js/jquery.dataTables.js"></script>
		<script src="js/tables.js"></script>
		<script >


			$(document).on('click','#btnNewUser', function() {
				window.location.href ='ApplicationUserServlet?t=new';
			});
			$(document).on("click", "#btnDelete", function() {
				if($('#applicationUserTable').find('input[type="checkbox"]:checked').length === 0 || ($('#applicationUserTable').find('input[type="checkbox"]:checked').length == 1 && $('#applicationUserTable').find('input[type="checkbox"]:checked').hasClass('tbl-select-all-rows'))) {
					showLocalizedMessage('no.records.selected', '', 'error');
					return false;
				}
				
				clearMessages();
				
				if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
					var $applicationUserForm = $("<form action='ApplicationUserServlet' method='POST' />");
					$applicationUserForm.append($("<input type='hidden' id='del' name='del' value='true' />"))
					$applicationUserForm.append($("<input type='hidden' id='csrftoken_form' name='csrftoken_form' value='" + $('#csrftoken_form').val() + "' />"))
					
					if($('#applicationUserTable .tbl-select-all-rows').is(':checked')){
					$('#applicationUserTable').find('input[type="checkbox"]:checked').each(function() {
						if(!$(this).hasClass('tbl-select-all-rows')) {
							$applicationUserForm.append($("<input type='hidden' name='selectedApplicationUser' value='"+ this.id +"' />"));
							
						}
					});
					}else{
						for (var i in selectedItems) {
							$applicationUserForm.append($("<input type='hidden' name='selectedApplicationUser' value='"+ selectedItems[i] +"' />"));
						}
					}
					$('body').append($applicationUserForm);
					$applicationUserForm.submit();
				}
			});
		</script>
	</head>
	<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		
			<div class='title'>
				<p>Application Credentials</p>
			</div>
			<div class='toolbar'>
				<input type='button' value='New' id='btnNewUser'class='imagebutton new' /> 
				<input type='button' value='Remove'id='btnDelete' class='imagebutton delete' />
			</div>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
			<div id='user-message'></div>
			<div class='form container_16'>
			   <input type ="hidden" name = "csrftoken_form" id = "csrftoken_form" value = <%=csrftoken %> />
				<div id='dataGrid'>
					<table id='applicationUserTable' class='display tjn-default-data-table'>
						<thead>
							<tr>
								<th class='table-row-selector nosort'><input type='checkbox' class='tbl-select-all-rows'></th>
								<th>User ID</th>
								<th>Application Name</th>
								<th>User Type</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${applicationUsers}" var="applicationUsers">
						<input type='hidden' id='uacId' name='uacId' value='${fn:escapeXml(applicationUsers.uacId )}'/>
						<tr>
						<td><input type='checkbox' class='checkbox chkBox' id='${fn:escapeXml(applicationUsers.uacId) }' value='${fn:escapeXml(applicationUsers.loginName) }:${fn:escapeXml(autLists.appId)}' data-uacId='${fn:escapeXml(applicationUsers.uacId) }'/>
						<td><a href='ApplicationUserServlet?t=view&key=${fn:escapeXml(applicationUsers.uacId) }'>${fn:escapeXml(applicationUsers.loginName)}</a></td>
						<td>${fn:escapeXml(applicationUsers.appName)}</td>
						<td>${fn:escapeXml(applicationUsers.loginUserType)}</td>
						</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

</body>

</html>