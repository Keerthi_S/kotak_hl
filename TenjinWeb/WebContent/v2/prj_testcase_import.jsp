<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  prj_testcase_import.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
 -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Import Test Cases</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css'/>
<link rel='stylesheet' href='css/tabs.css'/>
<link rel='stylesheet' href='css/buttons.css'/>
<link rel='stylesheet' href='css/960_16_col.css'/>
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/v2/prj_testcase_import.js'></script>
<script type='text/javascript' src='js/pages/v2/testcase_master.js'></script>
<style>
			#import_progress_dialog
			{
				background-color:#fff;
				
			}
			
			.subframe_content
			{
				display:block;
				width:100%;
				margin:0 auto;
				min-height:120px;
				padding-bottom:20px;
			}
			.subframe_title
			{
				width:644px;
				height:20px;
				background:url('images/bg.gif');
				color:white;
				font-family:'Open Sans';
				padding-top:6px;
				padding-bottom:6px;
				padding-left:6px;
				font-weight:bold;
				margin-bottom:10px;
			}
			
			.subframe_actions
			{
				display:inline-block;
				width:100%;
				background-color:#ccc;
				padding-top:5px;
				padding-bottom:5px;
				font-size:1.2em;
				text-align:center;
				position: relative;
				bottom: -24px;
			}
			
			.highlight-icon-holder
			{
				display:block;
				max-height:50px;
				margin-bottom:10px;
				text-align:center;
			}
			
			.highlight-icon-holder > img
			{
				height:50px;
			}
			
			.subframe_single_message
			{
				width:80%;
				text-align:center;
				font-weight:bold;
				display:block;
				margin:0 auto;
				font-size:1.3em;
			}
			
			#progressBar
			{
				width:475px;
				margin-top:20px;
				margin-left:auto;
				margin-right:auto;
			}
			
			p.subtitle
			{
				display:block;
				float:right;
				font-weight:bold;
				font-size:12px;
				line-height:22px;
				margin-right:10px;
				color:red;
			}
			.text-center{
				text-align:center
			}
		</style>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<div class='title'>
		<p>Import Test Cases</p>
		<div id='prj-info'>
			<span style='font-weight: bold'>Domain: </span> <span>${project.domain }</span>
			<span>&nbsp&nbsp</span><span style='font-weight: bold'>Project:</span> 
			<span>${project.name }</span>
		</div>
	</div>
	<form>
		<div class='toolbar'>
			<input type='button' value='Back' id='btnBack' class='imagebutton back' />
			<input type='button' value='Import' id='btnImport' class='imagebutton upload' />
			<input type='button' value='Refresh' id='btnRefresh' class='imagebutton refresh' />
		</div>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<input type='hidden' id='projectId' name='projectId' value='${project.id }'/>
		<input type='hidden' id='user' name='user' value='${user }'/>
		<div id='user-message'></div>
		<div class='form container_16'>
			<div class='fieldSection grid_12'>
				<div class='grid_2'><label for='lstPrj'>Project</label></div>
				<div class='grid_4'>
					<select id='lstPrj' class='stdTextBox'>
						<option value=''>-- Select One --</option>
						<c:forEach items="${projectList}" var="project">
							<option value='${project.id}'>${project.name} [${project.domain }]</option>
						</c:forEach>
					</select>
				</div>
				<div class='grid_2'><input type='button' id='btnSearch' value='Search Test Cases' class='imagebutton search'/></div>
			</div>
			<div class='clear'></div>
			<div id='tcTable' style='margin-top:20px'>
			<fieldset>
				<legend>Project Test Cases <span id='Selected_count'></span>
				</legend>
			</fieldset>
			<div id='dataGrid'>
				<table id='testcase-table' class='display' style='width:100%;'>
					<thead>
						<tr>
							<th class='table-row-selector'><input type='checkbox'  id='tbl-select-all-rows-tc' class='tbl-select-all-rows'/></th>
							<th>Test Case ID</th>
							<th>Name</th>
							<th>Type</th>
							<th class="text-center">Created On</th>	
							<th>Mode</th>
							<th>Storage</th>
						</tr>
					</thead>
				</table>
			</div>
			</div>
		</div>
	</form>
	<div class='modalmask'></div>
	<div class='subframe' id='import_progress_dialog' style='left:20%; height: 300px;'>
			<div class='subframe_title'><p>Processing...</p></div>
			<div class='highlight-icon-holder' id='processing_icon'></div>
			<div class='subframe_content'>
				<div class='subframe_single_message' id='import_message'>
					<p></p>
				</div>
			</div>
			<div class='subframe_actions' id='import_pr_actions' style='display:none;'>
				<input type='button' id='btnPClose' value='Close' class='imagebutton cancel'/>
			</div>
		</div>
</body>
</html>