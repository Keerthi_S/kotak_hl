<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  unstructured_data_validation_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 08-04-2020				Ashiki					TENJINCG-1204
*/

-->

<%@page import="com.ycs.tenjin.util.HatsConstants"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>File Validation Details</title>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/jquery-ui.css' />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/data-grids.js'></script>
	<script type="text/javascript" src='${pageContext.request.contextPath}/js/jquery-1.11.4-ui.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/v2/unstructured_data_validation_view.js'></script>
<body>

<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
		<div class='title' style='box-sizing:content-box;'>
			<p>File Validation Details</p>
		</div>
		
		<form name='validate_form' action='UnstructuredDataServlet' method='POST' class='tovalidate'>
			<div class='toolbar'>
			    <!-- added by shruthi for CSRF token starts -->
		        <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		        <!-- added by shruthi for CSRF token ends -->
				<input type='button' id='btnBack' value='Back' class='imagebutton back' /> 
				<input type='submit' id='btnSave' value='Save' class='imagebutton save' /> 
				<input type='hidden' id='validateName' value='${validation.validationName }'/>
			</div>
			
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<div id='user-message'></div>
			
			<input type='hidden' id='stepRecId' name='stepRecId' value='${stepRecId}'>
			<input type='hidden' id='recId' name='recId' value='${validation.recordId}'>
			<input type='hidden' id='tjn-status' value='${fn:escapeXml(screenState.status) }' />
			<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message)}' />
			<input type='hidden' id='fileType' name='fileType' value='${validation.fileType}'>
			<input type='hidden' id='validationName' name='validationName' value='${validation.validationName}'>
			<input type='hidden' id='del' name='del' value='update'>
			<div class='form container_16'>
			<fieldset>
			<legend>Type</legend>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='fileType' style='float:right'>Validation Type</label></div>
					<div class='grid_4'>
					
						<select class='stdTextBoxNew' id='fileType' name='fileType' disabled mandatory='yes'>
						<c:choose>
						<c:when test="${validation.fileType eq 'excel'}">
							<option value='excel' selected>Excel</option>
							<option value='pdf'>PDF</option>
							</c:when>
							<c:otherwise>
							<option value='excel'>Excel</option>
							<option value='pdf' selected>PDF</option>
							</c:otherwise>
							</c:choose>
						</select>
						
					</div>
				</div>
				<div class='clear'></div>
				
				
				
				<div id='pdfVldtn'>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='pdfvalidation' style='float:right'>PDF Type</label></div>
						<div class='grid_4'>
							<select class='stdTextBoxNew' id='pdfvalidation' name='pdfvalidation' value='${validation.validationName }' disabled mandatory='yes' >
								<!-- <option value='Text occurrence'>Text occurrence</option> -->
								<option value='Text occurrence validation'>Text occurrence validation</option>
								<!-- <option value='Text occurrence in page'>Text occurrence in page</option> -->
								<option value='Text occurrence validation in page'>Text occurrence validation in page</option>
							</select>
						</div>	
					</div>
				</div>
					
				<div class='clear'></div>
				<div id='excelVldtn'>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='excelValidation' style='float:right'>Excel Type</label></div>
						<div class='grid_4'>
							<select class='stdTextBoxNew' id='excelValidation' name='excelValidation' value='${validation.validationName }' disabled mandatory='yes'>
								<!-- <option value='Text occurrence'>Text occurrence</option> -->
								<option value='Text validation'>Text validation</option>
								<!-- <option value='Text occurrence in cell range'>Text occurrence in cell range</option>
								 --><option value='Text occurrence validation'>Text occurrence validation</option>
								<option value='Text occurrence validation in cell range'>Text occurrence validation in cell range</option>
							</select>
						</div>
					</div>
				</div>
				
				<div class='fieldSection grid_7' style='float:right;'>
				
				<div id='validation1' style=' color:red;'>Compare the given text against a specific Cell Reference. </div>
				<div id='validation2' style=' color:red;'> Compare the number of times the specified Text occurs in the given sheet, against an expected value.</div>
				<div id='validation3' style=' color:red;'>Compare the number of times the specified Text occurs in a given Range in the given sheet, against an expected value. </div>
				<div id='validation4' style='color:red; '>Compare the number of times the specified Text occurs in the specified pdf file, against an expected value.</div>
				<div id='validation5' style=' color:red;'>Compare the number of times the specified Text occurs in the specified page of a pdf file, against an expected value.</div>
				</div>
				</fieldset>
				
				<fieldset>
			<legend>Validation Data</legend>
					<div class='fieldSection grid_7' id ='sheetNme'>	
						<div class='grid_2'><label for='sheetName' >Sheet Name</label></div>
						<div class='grid_4'>
							<input type='text' id='sheetName' name='sheetName' class='stdTextBox'  title='Sheet Name' maxlength="100"  value='${validation.sheetName }' mandatory='yes'  />
						</div>
					</div>
					
					<div class='fieldSection grid_7' id ='pageNo'>	
						<div class='grid_2'><label for='pageNumber'>Page Number</label></div>
						<div class='grid_4'>
							<input type='text' id='pageNumber' name='pageNumber' class='stdTextBox'  title='Page Number Name' maxlength="100"  value='${validation.pageNumber }' mandatory='yes' />
						</div>
					</div>
					
						<div class='fieldSection grid_7'id ='cellRefrnce'>
							<div class='grid_2'><label for='cellReference' style='float:right'>Cell Reference</label></div>
							<div class='grid_4'>
								<input type='text' id='cellReference' name='cellReference' class='stdTextBox'  title='Cell Reference' maxlength="100" value='${validation.cellReference }' style='float:right' mandatory='yes' />
							</div>
						</div>
					
						<div class='fieldSection grid_7' id ='cellRnge'>
							<div class='grid_2'><label for='cellRange' >Cell Range</label></div>
							<div class='grid_4'>
								<input type='text' id='cellRange' name='cellRange' class='stdTextBox'  title='Cell Range' maxlength="100" value='${validation.cellRange }' mandatory='yes' />
							</div>
						</div>
					
						<div class='fieldSection grid_7' id ='txtToSearch'>
							<div class='grid_2'><label for='searchText' >Search Text</label></div>
							<div class='grid_4'>
								<input type='text' id='searchText' name='searchText' class='stdTextBox'  title='Search Text' maxlength="100" value='${validation.searchText }' mandatory='yes'  />
							</div>
						</div>
					
						<div class='fieldSection grid_8' id ='expctdOccrnce'>
							<div class='grid_2'><label for='expectedOccurance'  >Exp Occurrence</label></div>
							<div class='grid_4'>
								<input type='text' id='expectedOccurance' name='expectedOccurance' class='stdTextBox'   title='Expected Occurance' maxlength="3"  value='${validation.expectedOccurance }' mandatory='yes' />
							</div>
						</div>
					
					<div class='clear'></div>
					<div class='fieldSection grid_7' id=chckPassword>
						<div class='grid_2'><label for='passwordProtected' > File Password</label></div>
						<div class='grid_4'><input type="checkbox" id='password' name='passwordProtected' value ='' style='vertical-align: bottom;margin-top:6px;'/></div>
					</div>
					<div class='fieldSection grid_7' id ='passwd'>
					<div class='grid_2'>
						<label for='password'>Password</label>
					</div>
					<div class='grid_4'>
				    	<input type='password' id='password' name='password' mandatory='yes' title = 'password' class='stdTextBox' autocomplete="new-password"   value='${validation.password }' maxlength="20"/>
					</div>
					</div>
					</fieldset>
			</div>
		</form>

</body>
</html>