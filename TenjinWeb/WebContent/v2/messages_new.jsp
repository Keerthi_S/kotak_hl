<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  messages_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 

-->
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.ApiLearnerResultBean"%>
<%@page import="java.util.Date"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Api"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<!DOCTYPE html>
<html>
	<head>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/formvalidator.js'></script>
		<script type='text/javascript' src='js/data-grids.js'></script>
		<script type='text/javascript' src='js/tenjin-aut-selector.js'></script>
		 <script type='text/javascript' src='js/pages/messages_new.js'></script> 
		<script>
		
		
		</script>
		
	</head>
	<body>
	 
		<div class='title'>
			<p>Messages</p>
		</div>
		
		<input type='hidden' id='callback' value='${callback }' />
		<form id='messageForm' name='messageForm' action='MessageServlet' method='POST' enctype='multipart/form-data'>
			
			<div class='toolbar'>
				<input type='submit' id='btnUpload' value='Learn' class='imagebutton ok' />
				<input type='button' id='btnBack' value='Cancel' class='imagebutton cancel' />
			</div>
			
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			
			<div id='user-message'></div>
			
			<div class='form container_16'>
				<fieldset>
				<input title='message' type='hidden' id='messageSave' name='messageSave'  class='stdTextBox' value="saveLearn"/>
					<legend>Basic Information</legend>
				</fieldset>
				<div class='fieldSection grid_15'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstAppId'>Application</label>
					</div>
					<div class='grid_4'>
			
						<tjn:applicationSelector mandatory='yes' title='Application Name' cssClass="stdTextBoxNew"  id="aut" name="aut" defaultValue="${fn:escapeXml(applicationUser.appId )}"  loadAutLoginTypes='true' autLoginTypeSelector='autLogintypes' />
					</div>
					<div class='clear'></div>
					<div class='grid_2'><label for='message'>Name</label></div>
					<div class='grid_4'><input title='Name' type='text' id='messageName' name='messageName'  class='stdTextBox' mandatory='yes'/></div>
					<div class='clear'></div>
					<div class='grid_2'><label for='msgFormat'>Format</label></div>
					<div class='grid_4'>	
						<select id='msgFormat' name='msgFormat' class='stdTextBoxNew' mandatory='yes' title="Format"  maxlength="20"> 
							<option value='-1'>--Select One--</option>
							<c:forEach items="${formats}" var="format">
							<option value='${fn:escapeXml(format) }'>${fn:escapeXml(format) }</option>
							</c:forEach>
							<option value='others'>others</option>
						</select>
					</div>
					
					<%----------ISO20022 message validation--------------%>
					<div id="messageTypeDivIso20022">
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtStepMsgType'>Message type</label></div>
						<div class='grid_4' >
							<select id="txtStepMsgType" name="txtStepMsgType" class='stdTextBox' title='MessageType' mandatory='yes'>
								<option value="-- Select One --">-- Select One --</option>
								<option value="PAIN.001">PAIN.001</option>
								<option value="PACS.008">PACS.008</option>
								<option value="PACS.003">PACS.003</option>
								<option value="PACS.004">PACS.004</option>
								<option value="PACS.009">PACS.009</option>
								<option value="PACS.010">PACS.010</option>
								<option value="CAMT.029">CAMT.029</option>
								<option value="CAMT.054">CAMT.054</option>
								<option value="CAMT.056">CAMT.056</option>
							</select>
						</div>
					 </div>
						 
						 <%----------ISO8583 message validation--------------%>
					<div id="messageTypeDivIso8583">
						<div class='clear'></div>
						<div class='grid_2'>
						<label for='txtStepMsgType1'>Message type</label></div>
						<div class='grid_4' >
							<select id="txtStepMsgType1" name="txtStepMsgType1" class='stdTextBox' title='MessageType' mandatory='yes'>
								<option value="-- Select One --">-- Select One --</option>
								<option value="103">103</option>
							</select>
						</div>
						 </div>
								
								<%----------SWIFT message validation--------------%>
						 <div id="messageTypeDivSwift">
						<div class='clear'></div>
						<div class='grid_2'>
						<label for='txtStepMsgType2'>Message type</label></div>
						<div class='grid_4' >
							<select id="txtStepMsgType2" name="txtStepMsgType2" class='stdTextBox' title='MessageType' mandatory='yes'>
								<option value="-- Select One --">-- Select One --</option>
								<option value="MT 101">MT 101</option>
								<option value="MT 102">MT 102</option>
								<option value="MT 103">MT 103</option>
								<option value="MT 104">MT 104</option>
								<option value="MT 110">MT 110</option>
								<option value="MT 111">MT 111</option>
								<option value="MT 112">MT 112</option>
								<option value="MT 192">MT 192</option>
								<option value="MT 200">MT 200</option>
								<option value="MT 201">MT 201</option>
								<option value="MT 202">MT 202</option>
								<option value="MT 202_COVER">MT 202_COVER</option>
								<option value="MT 203">MT 203</option>
								<option value="MT 204">MT 204</option>
								<option value="MT 205">MT 205</option>
								<option value="MT 900">MT 900</option>
								<option value="MT 910">MT 910</option>
								<option value="MT 920">MT 920</option>
								<option value="MT 940">MT 940</option>
								<option value="MT 941">MT 941</option>
								<option value="MT 942">MT 942</option>
								<option value="MT 950">MT 950</option>
							</select>
						</div>
						 </div> 
								
								
								
						<div class='clear'></div>
						
						<div class='grid_2'>
							<label for='txtInputFile'>File</label>
						</div>
						
						<div class='grid_7'>
							<input type='file'  id='txtInputFile' name='txtInputFile' class='stdTextBox' mandatory='yes'/>
							</br><span id="lblError" style="color: red;"></span>
						</div> 
								</tbody>
							</table>
						</div>
					</div>
				</fieldset>
			</div>
		</form>
		<%--  <%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>  --%>
	</body>
</html>