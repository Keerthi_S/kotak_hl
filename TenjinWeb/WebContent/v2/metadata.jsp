<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  metadata.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
  

-->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Template Settings</title>
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
		<link rel="Stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ui-res-page.css' />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/jstree/themes/default/style.min.css" />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src="${pageContext.request.contextPath}/jstree/jstree.min.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/v2/metadata.js'></script>
		
		
		<style>
		#metadataContainer
		{
			display:flex;
			flex-direction:row;
		}
		
		#m_tree_container, #m_details_container
		{
			max-height:350px;
			overflow:auto;		
		}
		
		.section-heading
		{
			font-weight:bold;
		}
		
		.fieldDetailsBlock
		{
			display:table;
			width:100%;
		}
		
		.fieldDetails_row
		{
			display:table-row;
		}
		
		.fieldDetails_header
		{
			display:table-cell;
			width:100%;
		}
		
		.fieldDetails_header p
		{
			margin:5px 0px;
			font-weight:bold;
			border-bottom:2px solid #ccc;
		}
		
		.fieldDetails_label, .fieldDetails_value
		{
			display:table-cell;
		}
		
		.fieldDetails_label
		{
			width:130px;
		}
		
		.fieldDetails_label label
		{
			text-align:left;
		}
		
		.fieldDetails_value
		{
			width:calc(100% - 130px);
		}
		
		#loadingBlock
		{
			width:99%;
			min-height:200px;
			background-color:#ddd;
			border:2px dashed #ccc;
			position:relative;
			margin:0 auto;
			text-align:center;
			color:#525252;
			font-size:2em;
		}
		
		#loadingBlock p
		{
			margin-top:65px;
		}
		
		
		</style>
	</head>
	<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		<div class='title'>
			<p>Template Settings</p>
		</div>
		
		<input type='hidden' id='h-appId' value='${aut.id }' />
		<input type='hidden' id='h-functionCode' value='${module.code }' />
		
		<form action=''>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
				<input type='button' id='btnBack' value='Back' class='imagebutton back' />
				<input type='button' id='btnExpandAll' value='Expand All' class='imagebutton expand1' />
				<input type='button' id='btnCollapseAll' value='Collapse All' class='imagebutton collapse' />
			</div>
			<div id='user-message'></div>
			<div class='form container_16'>
				<fieldset>
					<legend>Application Information</legend>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='applicationName'>Application</label></div>
						<div class='grid_4'><input readonly type='text' id='applicationName' class='stdTextBox' value='${aut.name }' /></div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='function'>Function</label></div>
						<div class='grid_4'><input readonly type='text' id='function' class='stdTextBox' value='${module.code }' title='${module.code } - ${module.name }'/></div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Field Settings</legend>
					<div id='metadataContainer'>
						<div class='fieldSection grid_7' id='m_tree_container'>
							<div id='m_tree'>
								
							</div>
						</div>
						<div class='fieldSection grid_7' id='m_details_container'>
							<div id='loadingBlock'>
								<p>Loading. Please Wait...</p>
								<img src='images/inprogress.gif' alt='Loading' />
							</div>
							<div id='fieldDetails'>
							<!-- Added by Prem to disable button fields while Tenjin 2.10 testing start-->
							<div class='fieldDetailsBlock' id='fieldDetailsButton'>
									<div class=''>
										<div class=''>
											<font color="red"><p>Note : Remarks are not allowed for button</p></font>
										</div>
									</div>
								</div>
								<!-- Added by Prem to disable button fields while Tenjin 2.10 testing End-->
								<div class='fieldDetailsBlock'>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_header'>
											<p>Basic Information</p>
										</div>
									</div>
								</div>
								<div class='fieldDetailsBlock'>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'>
											<label for='pageArea'>Page Area</label>
										</div>
										<div class='fieldDetails_value'>
											<input type='text' readonly class='stdTextBox' value='' id='pageArea'/>
										</div>
									</div>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'>
											<label for='fieldUniqueName'>Field</label>
										</div>
										<div class='fieldDetails_value'>
											<input type='text' readonly class='stdTextBox' value='' id='fieldUniqueName'/>
										</div>
									</div>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'><label for='label'>On-Screen Label</label></div>
										<div class='fieldDetails_value'>
											<input type='text' id='label' class='stdTextBox' value='' readonly />
										</div>
									</div>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'>
											<label for='fieldType'>Type</label>
										</div>
										<div class='fieldDetails_value'>
											<span id='fieldTypeIndicator' class='lozenge Pass'>Textbox</span>
											<span id='mandatoryIndicator' class='lozenge Fail'>Mandatory</span>
											<span id='readOnlyIndicator' class='lozenge page-area'>Read-Only</span>
										</div>
									</div>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'>
											<label for='fieldUserRemarks'>Remarks</label>
										</div>
										<div class='fieldDetails_value'>
											<textarea id='fieldUserRemarks' class='stdTextBox' style='vertical-align:top;resize:none;'></textarea>
										</div>
									</div>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'>
										</div>
										<div class='fieldDetails_value'>
											<div style='margin-top:3px;display:inline-block;float:left;'>
												<input type='button' id='btnSave' value='Save' class='imagebutton save' />
												<img src='images/inprogress.gif' id='save_progress'/>
											</div>
											<div style='margin-top:5px;margin-left:10px;display:inline-block;float:left;'>
												<img src='images/success_20c20.png' id='save_success'/>
											</div>
										</div>
									</div>
								</div>
								<div class='fieldDetailsBlockHide'>
								<div class='fieldDetailsBlock'>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_header'>
											<p>Identification Properties</p>
										</div>
									</div>
								</div>
								<div class='fieldDetailsBlock'>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'><label for='sequence'>Sequence</label></div>
										<div class='fieldDetails_value'>
											<input type='text' id='sequence' class='stdTextBox' value='' readonly />
										</div>
									</div>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'><label for='identifiedBy'>Identifier</label></div>
										<div class='fieldDetails_value'>
											<input type='text' id='identifiedBy' class='stdTextBox' value='' readonly />
										</div>
									</div>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'><label for='uniqueId'>Value</label></div>
										<div class='fieldDetails_value'>
											<input type='text' id='uniqueId' class='stdTextBox' value='' readonly />
										</div>
									</div>
								</div>
								<div class='fieldDetailsBlock'>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_header'>
											<p>Attributes</p>
										</div>
									</div>
								</div>
								<div class='fieldDetailsBlock'>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'><label for='label'>On-Screen Label</label></div>
										<div class='fieldDetails_value'>
											<input type='text' id='label' class='stdTextBox' value='' readonly />
										</div>
									</div>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'><label for='defaultOptions'>Allowed Values</label></div>
										<div class='fieldDetails_value'>
											<input type='text' id='defaultOptions' class='stdTextBox' value='' readonly />
										</div>
									</div>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'><label for='lov'>LOV</label></div>
										<div class='fieldDetails_value'>
											<input type='text' id='lov' class='stdTextBox' value='' readonly />
										</div>
									</div>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'><label for='autoLov'>Auto-LOV</label></div>
										<div class='fieldDetails_value'>
											<input type='text' id='autoLov' class='stdTextBox' value='' readonly />
										</div>
									</div>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'><label for='multiRecord'>Multi-Record</label></div>
										<div class='fieldDetails_value'>
											<input type='text' id='multiRecord' class='stdTextBox' value='' readonly />
										</div>
									</div>
									<div class='fieldDetails_row'>
										<div class='fieldDetails_label'><label for='group'>Group</label></div>
										<div class='fieldDetails_value'>
											<input type='text' id='group' class='stdTextBox' value='' readonly />
										</div>
									</div>
								</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
		</form>
	</body>
</html>