<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  user_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 

-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/v2/user_new.js'></script>
		
		
		
		</head>
		<body>
		
<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
			<div class='title'>
				<p>Create New User</p>
			</div>
			<form action='TenjinUserServlet' method='POST' id='userForm' name='userForm'>
			<div class='toolbar'>
			    <!-- added by shruthi for CSRF token starts -->
		        <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		        <!-- added by shruthi for CSRF token ends -->
				<input type='submit' id='btnSave' value='Save' class='imagebutton save' />
				<input type='button' id='btnBack' value='Cancel' class='imagebutton cancel' />
			</div>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
			<input type="hidden" name="operation" value="new"/>
			
			<div id='user-message'></div>
			<div class='form container_16'>
				<fieldset>
					<legend>Basic Information</legend>
					<div class='fieldSection grid_16'>
						
							<div class='grid_2'><label for='id'>User Id</label></div>
							  <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<div class='grid_4'><input type='text' class='stdTextBox' value='${fn:escapeXml(user.id )}' id='id' name='id'maxlength='20' autocomplete="new-password" mandatory='yes' placeholder="MaxLength 20"/></div>
							  <!-- Modified by Priyanka for TENJINCG-1233 ends -->
							</div>
							<div class='clear'></div>
							
					<div class='fieldSection grid_7'>		
							<div class='grid_2'><label for='firstName'>First Name</label></div>
							  <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<div class='grid_4'><input type='text' class='stdTextBox' value='${fn:escapeXml(user.firstName) }' id='firstName' name='firstName'maxlength='15' mandatory='yes'  placeholder="MaxLength 15"/></div>
				             <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>	
						
					<div class='fieldSection grid_7'>									
							<div class='grid_2'><label for='lastName'>Last Name</label></div>
							 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<div class='grid_4'><input type='text' class='stdTextBox' value='${fn:escapeXml(user.lastName) }' id='lastName' name='lastName' maxlength='15' placeholder="MaxLength 15"/></div>
				            <!-- Modified by Priyanka for TENJINCG-1233 starts -->
					</div>	
					</fieldset>
					<fieldset>
					<legend>Contact Information</legend>
					<div class='fieldSection grid_16'>
					
							<div class='clear'></div>
							
							<div class='grid_2'><label for='primaryPhone'>Contact No</label></div>
							 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<div class='grid_4'><input type='text' class='stdTextBox' value='${fn:escapeXml(user.primaryPhone )}' id='primaryPhone' name='primaryPhone' maxlength='15' placeholder="MaxLength 15"/></div>
							 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
							<div class='clear'></div>
							
							<div class='grid_2'><label for='email'>Email Id</label></div> 
							<div class='grid_12'><input type='text' class='stdTextBox long' value='${fn:escapeXml(user.email) }' id='email' name='email'maxlength='100' autocomplete="new-password" mandatory='yes'  /></div>  
							<div class='clear'></div>
					</div>
					</fieldset>	
					<fieldset>
						<legend>Security Information</legend>
						<div class='fieldSection grid_7'>
						
							<div class='grid_2'><label for='password'>Password</label></div>
							  <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<div class='grid_4'><input type='password' class='stdTextBox' value='${fn:escapeXml(user.password) }' id='password' name='password' maxlength='30' autocomplete="new-password" mandatory='yes'  placeholder="MaxLength 30"/></div>
							  <!-- Modified by Priyanka for TENJINCG-1233 ends -->
							<div class='clear'></div>
							</div>
							
							<div class='fieldSection grid_7'>				
							<div class='grid_2'><label for='roles'>Roles</label></div>
							<div class='grid_4' style='margin-top:2px;'>
								<select name='roles' id='roles' class='stdTextBoxNew'>
								<option value='Site Administrator'>Site Administrator</option>
								<option value='Tenjin User'>Tenjin User</option>
								</select>
						</div>
					</div>
					<div class='grid_12' style="width:150%;margin-left: 140px; ">
					<div class='clear'></div>
							<table class='border' width='100%' ">
								<thead >
									<tr>
										   <th style="font-weight:bold; color:red">Password must contain</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><p class='ptext'> Minimum 8 characters</p> </td></tr>
									
									<tr>
										<td><p class='ptext'> At least one numeric character(e.g. 0-9)</p></td>
									</tr>
									<tr>
										<td><p class='ptext'> At least one special character(e.g~!@#$%^&*()_-+=)</p> </td>
									</tr>
									<tr>
										<td><p class='ptext'> Combination of upper and lower case alphabetic character(e.g A-Z,a-z)</p></td>
									</tr>

								</tbody>
							</table>
						</div>
				</fieldset>
			</div>
		</form>
</body>
</html>