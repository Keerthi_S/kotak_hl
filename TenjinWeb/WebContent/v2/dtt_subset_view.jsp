<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  dtt_subset_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
  -->


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
 <%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DTT Subset View</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src=' js/bootstrap.min.js'></script>
		<script src="js/tables.js"></script>
		<script type='text/javascript' src='js/pages/v2/dtt_subset_view.js'></script>
		<link rel='stylesheet' href='css/paginated-table.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<div class='title'>
<p>DTT Subset view</p>
</div>
<form name='main_form' action='DTTSubsetServlet' method='POST'>
<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
	<div class='toolbar'>
		<input type='button' class='imagebutton back' value='Back' id='btnBack' /> 
		<input type='button' class='imagebutton add'value='Add' id='btnAddProjects' />
		<input type='button' class='imagebutton delete'value='Remove' id='btnRemoveProjects' />
		<input type='button' class='imagebutton defect'value='Fetch Projects' id='fetchProjects' />
		<img src='images/inprogress.gif' id='dmLoader' />
	</div>
	<input type='hidden' id='status' value='${Status }' />
	<input type='hidden' id='tjn-status' value='${screenState.status }' />
	<input type='hidden' id='tjn-message' value='${screenState.message }' />
	
	<div id='user-message'></div>
	<input type='hidden' id='recId' name='recId' value='${Subset.prjId}' />
	<div class='form container_16'>
		<fieldset>
			<legend>DTT Subsets</legend>
		</fieldset>
		
		<input type='hidden' id='instanceName' name='instanceName' value='${fn:escapeXml(Subset.instanceName)}' />	
		
		<div class='fieldSection grid_7'>
			<div class='grid_2'>
					<label for='subSetName' >Subset Name</label>							
				</div>
				<div class='grid_4'>
					<input type='text' id='subSetName' name='subSetName' title='SubSet Name' size='15' class='stdTextBox' value='${fn:escapeXml(Subset.subSetName)}' mandatory='yes' />			
				</div>
			<div class='clear'></div>
			<div class='grid_2'>
				<label for='txtTool'>Instance</label>
			</div>
			<div class='grid_4'>
			    <!-- Modified by Priyanka for TENJINCG-1233 starts -->
				<input type='text' id='lstInstance' name='lstInstance' title='Instance Name' class='stdTextBox' disabled='disabled' value='${fn:escapeXml(Subset.instanceName)}' maxlength="20"/>
			   <!-- Modified by Priyanka for TENJINCG-1233 ends -->
			</div>
		</div>
	</div>
		<div class='form container_16'>
		<fieldset>
			<legend>DTT Subsets Projects</legend>
		</fieldset>
		<div id='dataGrid'>
			<table id='projects-table' class='display tjn-default-data-table'>
				<thead>
					<tr>
						<th class='table-row-selector nosort'><input type='checkbox'  class='tbl-select-all-rows' id='chk_all' /></th>
						<th>Projects </th>
						<th>Status </th>
					</tr>
				</thead>
				<tbody>
				
			<c:choose>
				 <c:when test="${mappedProjects eq 'N'}">
				 
				 <c:forEach items="${Projects}" var="Project">
				<tr>
				<td><input class='table-row-selector' type='checkbox' name='${fn:escapeXml(Project) }' value='${fn:escapeXml(Project) }'  id='${fn:escapeXml(Project) }'></input></td>
				<td>${Project.split(":")[0]}</td>
				<td><img src="images/success_20c20.png"></td>
				</tr>
				</c:forEach>
				 </c:when>
				
				<c:otherwise>
					
						<c:forEach items='${PROJECT_MAP}' var='project'>
						<tr>
							<td><input class='table-row-selector' type='checkbox' name='projects' value='${project.key }'  id='${project.key }'></input></td>
							<td>${project.key.split(":")[0]}</td>
						
						<c:choose>
				 			<c:when test="${project.value eq 'Y'}">
				 				<td><img src="images/success_20c20.png"></td>
				 			</c:when>
							<c:otherwise>
				 				<td><img src="images/failure_20c20.png"></td>
							</c:otherwise>
						</c:choose>
						</tr>
						</c:forEach>
					
				</c:otherwise>
			</c:choose>
					
				</tbody>
			</table>
		</div>
	</div>
</form>
</body>
</html>