<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  importedtclistNew.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
-->
<%@page import="java.util.ArrayList"%>
<%@page import="org.slf4j.LoggerFactory"%>
<%@page import="org.slf4j.Logger"%>
<%@page import="com.ycs.tenjin.project.TestCase"%>
<%@page import="java.util.List"%>
<%@page import="com.ycs.tenjin.db.TestCasePaginator"%>
<%@page import="java.util.Map"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
 

-->


<head>
		<title>Import Tea - Tenjin Intelligent Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/tabs.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel="Stylesheet" href="css/summarypagetable.css" />
		<link rel="stylesheet" href="css/jquery.dataTables.css">
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/importedtclist.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script src="js/tables.js"></script>
	<style type="text/css">
	.tc_anchor {
    color: -webkit-link;
    cursor: pointer;
    text-decoration: underline;
}
		</style>
</head>


<body>

<%
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();
			Project project = null;
			project = tjnSession.getProject();
			
			boolean canMakeChanges = false;
			
			
			if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){
				canMakeChanges = true;
			}
			
			Map<String, Object> map = (Map<String, Object>)request.getSession().getAttribute("SCR_MAP");
			String status = (String)map.get("STATUS");
			String message = (String)map.get("MESSAGE");
			List<com.ycs.tenjin.testmanager.TestCase> tcs = (ArrayList<com.ycs.tenjin.testmanager.TestCase>)map.get("tcList");
			List<com.ycs.tenjin.testmanager.TestStep> tSteps = (ArrayList<com.ycs.tenjin.testmanager.TestStep>)map.get("tsList"); 
			
		%>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<div class='title'>
		<p>TM Test Cases</p>
	</div>

	<form name='main_form' action='' method=''>
	<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
	
	<div class='toolbar'>
				<input type='hidden' id='prjId' value='<%=project.getId()%>'/>
				<input type='button' value='Back' id='btnBack' class='imagebutton back'/>
				<input type='button' value='Import' id='btnImport' class='imagebutton upload'/>
				<img src='images/inprogress.gif' id='dmLoader' />
				<img src='images/inprogress.gif' id='img_download_loader' style='display:none;'/>
			</div>
	<div id='user-message'></div>
	<div class='form container_16'>
		<div class='form container_10' id='tcListDiv'>
	
					<div style='font-weight:bold; margin-left:20px'>Test Cases</div>
					<div class='fieldSection grid_9' id='tcInfo' style='height: 400px; overflow-y:auto;'>
					<table id='tblTestCases' class='display tjn-default-data-table'>
					<thead>
					<tr>
									<th class='tiny nosort'>
										<input type='checkbox' id='tbl-select-all-rows-tc' class='tbl-select-all-rows'/>
									</th>
									
									<th class='tiny'>Test Case ID</th>
									<th>Name</th>
									<th>Type</th>
									<th class="text-center">Created On</th>
								</tr>
					</thead>
					
					<tbody>
							<c:choose>
							<c:when test="${not empty tcList}">
							<c:forEach items="${tcList }" var="tcList">
							<tr>
							<p type='hidden' id='${tcList.tcId }' style='display:none;'>${tcList.stepsArray }</p>
							<td class='selector'><input type='checkbox'
								id='${tcList.tcId }' /></td>
							
							 
							 
							
							<td class='tc_anchor' tStepArray='${tcList.stepsArray }' tstep='${tcList.tcSteps }' recid='${tcList.tcId }'>${tcList.tcId }</td>
							<td class='tc_anchor' tStepArray='${tcList.stepsArray }' tstep='${tcList.tcSteps }' recid='${tcList.tcId }'>${tcList.tcName }</td>
							<td tStepArray='${tcList.stepsArray }' tstep='${tcList.tcSteps }' recid='${tcList.tcId }'>${tcList.tcType }</td>
							 <td class='text-center' tstep='${tcList.tcSteps }' recid='${tcList.tcId }'>${tcList.tcCreatedOn }</td> 
							</tr>
							</c:forEach>
							 </c:when>
							 <c:otherwise>
							<tr>
							<td>No Cases were found</td>
							</tr>
							
							</c:otherwise>
							</c:choose>
							</tr>
						</tbody>
					</table>
					</div>
					
	
	</div>
	<div class='form container_8' id='tsListDiv'>

				<div style='font-weight: bold;'>Test Steps</div>
				<div class='fieldSection grid_7' id='tcInfo'  style='height: 400px; overflow-y:auto;'>
					<table id='tblTestSteps' class='display tjn-default-data-table'>
					<thead></thead>
					<tbody></tbody>
						
					</table>
				</div>

			</div>
	</div>
	
	
	
	</form>

</body>