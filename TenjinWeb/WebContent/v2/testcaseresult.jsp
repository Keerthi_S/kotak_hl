<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testcaseresult.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!-- 
/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 

-->
<%@page import="com.ycs.tenjin.util.HatsConstants"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Test Case Result - Tenjin Intelligent Testing Engine</title>
	<link rel='stylesheet' href='css/cssreset-min.css' />
	<link rel='stylesheet' href='css/bootstrap.min.css' />
	<link rel='stylesheet' href='css/ifr_main.css' />
	<link rel='stylesheet' href='css/style.css' />
	<link rel='stylesheet' href='css/rprogress.css' />
	<link rel="Stylesheet" href="css/jquery.dataTables.css" />
	<link rel='stylesheet' href='css/buttons.css' />
	<link rel='stylesheet' href='css/summarypagetable.css'/>
	<link rel='stylesheet' href='css/ui-res-page.css' />
	<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
	<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
	<script type="text/javascript" src="js/jquery.dataTables.js"></script>
	<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
	<script type='text/javascript' src='js/pages/v2/testcaseresult.js'></script>
	
	<body>
	<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
		<c:set var="statusPass" value='<%=HatsConstants.ACTION_RESULT_SUCCESS%>' />
		<c:set var="statusFail" value='<%=HatsConstants.ACTION_RESULT_FAILURE%>' />
		<c:set var="statusError" value='<%=HatsConstants.ACTION_RESULT_ERROR%>' />
		<c:set var="statusExecuting" value='<%=HatsConstants.SCRIPT_EXECUTING%>' />
		<c:set var="statusNotStarted" value='Not Started' />
		<c:choose>
			<c:when test="${testCase.tcStatus eq statusExecuting }">
				<c:set var="imgUrl" value="images/ajax-loader.gif" />
				<c:set var="endTimestamp" value="N/A" />
			</c:when>
			<c:when test="${testCase.tcStatus eq statusFail }">
				<c:set var="imgUrl" value="images/failure_20c20.png" />
			</c:when>
			<c:when test="${testCase.tcStatus eq statusPass }">
				<c:set var="imgUrl" value="images/success_20c20.png" />
			</c:when>
			<c:when test="${testCase.tcStatus eq statusNotStarted }">
				<c:set var="imgUrl" value="images/queued_20c20.png" />
			</c:when>
			<c:otherwise>
				<c:set var="imgUrl" value="images/error_20c20.png" />
			</c:otherwise>
		</c:choose>
		<div class='title' style='box-sizing:content-box;'>
			<p>Test Case Status</p>
		</div>
		<form name='main_form'>
			<div class='toolbar'>
			     <!-- added by shruthi for CSRF token starts -->
		     <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		     <!-- added by shruthi for CSRF token ends -->
			   <input type="button" id='btnBack' value="Back" class= 'imagebutton back' onclick="window.history.back()" /> 
				<!--  <input type='button' id='btnBack' value='Back' class='imagebutton back' /> -->
				<c:set value="Not Started" var="notStarted" />
				<c:if test="${run.status ne notStarted }">
					<input type='button' id='report-gen' value='Report' class='imagebutton pdfreport' />
				</c:if>
				
				<img src='images/inprogress.gif' id='img_report_loader' style='display:none;'/>
				<div style="float: right; padding-right: 80px;font-size:0.7em;line-height:22px;">
					<c:if test="${not empty run.allParentRunIds || not empty run.reRunIds}">
						<span style='font-weight:bold;margin-right:10px;' id='reRunMsg'>This run has associated runs</span>
						<c:forEach items="${run.allParentRunIds }" var="parentRunId">
							<span><a href='ResultsServlet?param=run_result&run=${parentRunId }&callback=${callback}'>${parentRunId }</a></span> /
						</c:forEach>
						<span><b>${run.id }</b></span>
						<c:if test="${not empty run.reRunIds }">
							<c:forEach items="${run.reRunIds }" var="reRunId">
								 / <span><a href='ResultsServlet?param=run_result&run=${reRunId }&callback=${callback}'>${reRunId }</a></span>
							</c:forEach>
						</c:if>
					</c:if>					
				</div>
			</div>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			
			<div id='user-message'></div>
			
			<div class='form'>
				<div class='row' id='report-header-row'>
					<div class='col-md-2 field-section'>
						<table class='layout'>
							<tr>
								<td class='layout-label' width='100px'>Run ID</td>
								<td class='layout-value' id='runId'>${run.id }</td>
							</tr>
							<tr>
								<td class='layout-label' width="100px">Test Set</td>
								<td class='layout-value' title='${run.testSet.name }'>${run.testSet.name }</td>
							</tr>
						</table>
					</div>
					<div class='col-md-6 field-section'>
						<table class='layout'>
							<tr>
								<td class='layout-label' width="120px">Test Case</td>
								<td class='layout-value' title='${testCase.tcId }'>${testCase.tcId } - ${testCase.tcName }</td>
							</tr>
							<tr>
								<td class='layout-label'>Client</td>
								<%-- <td class='layout-value'>${run.machine_ip eq "0:0:0:0:0:0:0:1" ? "Local" : run.machine_ip + ":" + run.targetPort}</td> --%>
								<td class='layout-value'>${run.machine_ip} : ${run.targetPort}</td>
							</tr>
							<!-- Modified by Prem for Tenj210-75 starts-->
							<tr>
								<td class='layout-label'>Browser</td>
								<td class='layout-value'>${run.browser_type} </td>
							</tr>
							<!-- Modified by Prem for Tenj210-75 Ends-->
						</table>
					</div>
					<div class='col-md-4 field-section'>
						<table class='layout'>
							<tr>
								<td class='layout-label' width="120px">Status</td>
								<td class='layout-value' title='${testCase.tcStatus }' style='font-weight:bold;color:${imgUrl}'>${testCase.tcStatus }</td>
							</tr>
							<tr>
								<td class='layout-label'>Step Summary</td>
								<td class='layout-value' id='stepSummaryLabels'>
									
								</td>
							</tr>
							<tr>
								<td class='layout-label' width="120px">ElapsedTime</td>
								<td class='layout-value'>${run.elapsedTime }</td>
							</tr>
					   </table>
					</div>
				</div>
				<c:set var="statusPass" value='<%=HatsConstants.ACTION_RESULT_SUCCESS%>' />
				<c:set var="statusFail" value='<%=HatsConstants.ACTION_RESULT_FAILURE%>' />
				<c:set var="statusError" value='<%=HatsConstants.ACTION_RESULT_ERROR%>' />
				<c:set var="statusExecuting" value='<%=HatsConstants.SCRIPT_EXECUTING%>' />
				<c:set var="statusNotStarted" value='Not Started' />
				<div class='row' id='step-details-row'>
					<div class='col-md-6 field-section' id='steps-list' style='border-right:1px solid #ccc;'>
						<div class='test_case_summary_title'>Steps in this Test Case</div>
						<div class='dataGrid'>
							<table class='table' id='steps_table'>
								<thead>
									<tr>
										<th width="10px">Status</th>
										<th width="40px">Step ID</th>
										<th width="20px">Mode</th>
										<th>Summary</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${testCase.tcSteps }" var="step">
										<c:set var="imgUrl" value="images/error_20c20.png" />
										<c:set var="endTimestamp" value="" />
										<c:choose>
											<c:when test="${step.status eq statusExecuting }">
												<c:set var="imgUrl" value="images/ajax-loader.gif" />
												<c:set var="endTimestamp" value="N/A" />
											</c:when>
											<c:when test="${step.status eq statusFail }">
												<c:set var="imgUrl" value="images/failure_20c20.png" />
											</c:when>
											<c:when test="${step.status eq statusPass }">
												<c:set var="imgUrl" value="images/success_20c20.png" />
											</c:when>
											<c:when test="${step.status eq statusNotStarted }">
												<c:set var="imgUrl" value="images/queued_20c20.png" />
											</c:when>
											<c:otherwise>
												<c:set var="imgUrl" value="images/error_20c20.png" />
											</c:otherwise>
										</c:choose>
										<tr>
											<td title='${step.status }' style='text-align:center' ><img src='${imgUrl }' /></td>
											<td><a data-step-id='${step.recordId }' data-step-mode='${step.txnMode }' class='test-step-detail-trigger' href='#!'>${step.id }</a></td>
											<td>${step.txnMode }</td>
											<td>${step.shortDescription }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class='col-md-6 field-section' id='step-details' style='padding-left:15px !important;'>
						<div class='test_case_summary_title'>Detailed Results</div>
						<div id='test_step_result' style='padding-bottom:20px !important;'>
						
						</div>
					</div>
				</div>
			</div>
		</form>
		
		<div class='modalmask'></div>
		<div class='subframe' id='val_frame' style='left:3%'>
			<iframe style='height:500px;width:650px;overflow:hidden;' scrolling='no' seamless='seamless' id='val-res-frame' src=''></iframe>
		</div>
		
		<div class='subframe' id='ui-field-results'>
			<div class='title'>
				<p>UI Validation Results - Fields</p>
			</div>
			<div class='toolbar'>
				<input type='button' value='Close' id='btnUIClose' class='imagebutton cancel' />
			</div>
			
			<div class='grid_7 ui-field-results-header' style='margin-top:10px;'>
				<span id='pageName' class='ui-field-results-title'></span>
				<span id='valName' class='lozenge ui-field-results-summary'></span>
			</div>
			
			<div class='clear'></div>
			
			<div id='valResultsTable' class='fieldSection grid_6' style='padding-bottom:15px'>
				<table class='summtable' id='valResults' style='width:96%;padding-bottom:5px;'align="center">
					<thead>
						<tr>
							<th>Field</th>
							<th>Expected</th>
							<th>Actual</th>
							<th>Status</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		 
		<div class='subframe' id='screenShotOption'
			style='position: absolute; top: 100px; left: 300px; width: 450px'>
	
			<div class='subframe_title'>
				<p>Please Confirm...</p>
			</div>
			<div class='subframe_content'>
				<div class='subframe_single_message'>
					<p>Do you want Screenshot to be included in Tenjin report?</p>
				</div>
			</div>
			<div class='subframe_actions'>
				<input type='button' id='btnYes' value='Yes' class='imagebutton ok' />
				<input type='button' id='btnNo' value='No' class='imagebutton cancel' />
			</div>
		</div>
		 
		<input type='hidden' id='json-data' value='${testCaseJson }' />
		<input type='hidden' id='h-runId' value='${run.id }' />
		<input type='hidden' id='h-callback' value='<%=request.getParameter("callback") %>' />
		<input type='hidden' id='h-testCaseId' value='${testCase.tcId }' />
	</body>
</html>