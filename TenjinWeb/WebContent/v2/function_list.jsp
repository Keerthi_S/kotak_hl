<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  function_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 

-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
	<head>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/tenjin-aut-selector.js'></script>
		<script src="js/tables.js"></script>
		
		<script>
			$(document).on('click','#btnSearchFunctions', function() {
				var appId = $('#application').val();
				if(appId==-1)
					{
					showLocalizedMessage('Please select application.', 'error');
					return false;
					}
				window.location.href='FunctionServlet?appId=' + $('#application').val() + '&group=' + $('#functionGroup').val();
			}); 
			
			$(document).ready(function() {
				 
				var path=$('#downloadPath').val();
				if(path!=''&& path!=undefined){
					/*modified by paneendra for VAPT fix starts*/
					/* var $c = $("<a id='downloadFile' href="+path+" target='_blank' download />"); */
					var $c = $("<a id='downloadFile' href='DownloadServlet?param="+path+"' target='_blank' download />");
					/*modified by paneendra for VAPT fix ends*/
					$("body").append($c);
						$c.get(0).click();
				}
				 
				$('#upload').click(function(){
					
						clearMessagesOnElement($('#user-message'));
						
						$('.subframe > iframe').attr('src','uploadFunctions.jsp');
						
						$('.subframe').width(400).height(180);
						
						$('.subframe > iframe').width(400).height(180);
						
						$('.modalmask').show();
						$('.subframe').show();
				});
				
				
				$('#download').click(function(){
					clearMessagesOnElement($('#user-message'));
					$('.subframe > iframe').attr('src','downloadFunctions.jsp');
					
					
					$('.subframe').width(400).height(180);
					
					$('.subframe > iframe').width(400).height(180);
					
					$('.modalmask').show();
					$('.subframe').show();
				});
				
				
			});
			
			$(document).on("click", '#btnNew', function() {
				window.location.href='FunctionServlet?t=new';
			});
			
			$(document).on("click", "#btnDelete", function() {
				
				if($('#functionsTable').find('input[type="checkbox"]:checked').length < 1) {
					showLocalizedMessage('no.records.selected', 'error');
					return false;
				}else if($('#functionsTable').find('input[type="checkbox"]:checked').length < 2 && $('#functionsTable .tbl-select-all-rows').is(':checked')) {
					showLocalizedMessage('no.records.selected', 'error');
					return false;
				}
				
				
				
				if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
					var $functionForm = $("<form action='FunctionServlet' method='POST' />");
					$functionForm.append($("<input type='hidden' id='del' name='del' value='true' />"))
					$functionForm.append($("<input type='hidden' id='aut' name='aut' value='" + $('#application').val() + "' />"));
					$functionForm.append($("<input type='hidden' id='group' name='group' value='" + $('#functionGroup').val() + "' />"));
					$functionForm.append($("<input type = 'hidden' id='csrftoken_form' name ='csrftoken_form' value='"+$('#csrftoken_form').val()+"'/>"));
			 
					if($('#functionsTable .tbl-select-all-rows').is(':checked')){
						$('#functionsTable').find('input[type="checkbox"]:checked').each(function() {
						if(!$(this).hasClass('tbl-select-all-rows')) {
							$functionForm.append($("<input type='hidden' name='selectedFunctionCodes' value='"+ $(this).data('functionCode') +"' />"));
						}
						});
					}else{
						for (var i in selectedItems) {
							 $functionForm.append($("<input type='hidden' name='selectedFunctionCodes' value='"+ selectedItems[i] +"' />"));
						}
					}
					$('body').append($functionForm);
					$functionForm.submit();
				}
			});
			
			$(document).on('click', '#btnLearn', function() {
				$('#learnType').val('NORMAL');
				var selectedModules = $('#moduleList').val();
				if(selectedModules == 'null'){
					selectedModules = '';
				}
				
				$('#functionsTable').find('input[type="checkbox"]:checked').each(function () {
				       if(this.id != 'selectAllFunctions'){
							var txnMode = 'G';

							if (this.id != "")
								selectedModules = selectedModules + this.id + '|' +txnMode  + '||' +';';
				       }
				});
				if( selectedModules.charAt(selectedModules.length-1) == ';'){
					selectedModules = selectedModules.slice(0,-1);
				}
				if(selectedModules.length > 0){
					$('#moduleList').val(selectedModules);
					
				}
				else{
					showLocalizedMessage('no.records.selected', 'error');
					return false;
				}
				
				$('#main_form').submit();
			});
			
			$(document).on('click','.template-gen',function(e){
				var mod = $(this).attr('mod');
				var app = $(this).attr('app');
				var txnMode=$(this).attr('txnMode');
				$('#ttd-options-frame').attr('src','ttd_download_options.jsp?a=' + app + '&m=' +mod + '&t=' + txnMode);
		    	$('.modalmask').show();
		    	$('.subframe').show();
			});
			function closeModal() {
				$('.modalmask').hide();
				$('.subframe').hide();
				$('#ttd-options-frame').attr('src', '');
			}
		</script>
	</head>
	<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		<div class='title'>
			<p>Functions</p>
		</div>
		<div class='toolbar'>
			<input type='button' id='btnNew' class='imagebutton new' value='New Function' />
			<input type='button' id='btnDelete' class='imagebutton delete' value='Remove' />
			<input type='button' id='btnLearn' class='imagebutton learn' value='Learn' />
			<input type='button' value='Upload' id='upload' class='imagebutton upload'/>
			<input type='button' value='Download' id='download' class='imagebutton download'/>
		</div>
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
		<input type='hidden' id='downloadPath' name='downloadPath' value='${fn:escapeXml(downloadPath) }'/>
		<div id='user-message'></div>
		
		<div class='form container_16'>
			<fieldset>
				<legend>Criteria</legend>
				<div class='fieldSection grid_15'>
					<div class='grid_2'><label for='application'>Application</label></div>
					<div class='grid_4'>						
						<tjn:applicationSelector defaultValue="${fn:escapeXml(applicationId) }" cssClass="stdTextBoxNew" groupSelector="functionGroup" loadFunctionGroups="true" id='application' name='application' title='Application'/>
					</div>
					
					<div class='clear'></div>
					<div class='grid_2'><label for='functionGroup'>Group</label></div>
					<div class='grid_4'>
						<tjn:functionGroupSelector cssClass="stdTextBoxNew" applicationSelector="application" id='functionGroup' name='functionGroup' applicationId="${fn:escapeXml(applicationId) }" defaultValue="${fn:escapeXml(selectedGroup) }" title='Function Group'/>
					</div>
					
					<div class='grid_3'>
						<input type='button' id='btnSearchFunctions' value='Go' class='imagebutton ok' />
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Functions</legend>
				<div class='fieldSection grid_15'>
						<table class='display tjn-default-data-table' id='functionsTable'>
							<thead>
								<tr>
									<th class='table-row-selector nosort'><input type='checkbox' id='selectAllFunctions' class='tbl-select-all-rows' /></th>
									<th>Function Code</th>
									<th>Function Name</th>
									<th>Group</th>
									<th>Last Successfully Learnt On</th>
									<th>Template</th>
									<th>No.of Fields Learnt</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${modules }" var="module">
									<tr>
										<td class='table-row-selector'><input type='checkbox' class='chkBox' id='${fn:escapeXml(module.code) }' data-function-code='${fn:escapeXml(module.code) }'/></td>
										<td>${fn:escapeXml(module.code) }</td>
										<td title='${fn:escapeXml(module.name) }'><a href='FunctionServlet?t=view&key=${fn:escapeXml(module.code) }&appId=${fn:escapeXml(applicationId) }'>${fn:escapeXml(module.name) }</a></td>
										<td>${fn:escapeXml(module.group) }</td>
										<c:choose>
											<c:when test="${empty module.lastSuccessfulLearningResult }">
												 
												<td>Not Learnt</td>
												<td>N/A</td>
												<td>N/A</td>
											</c:when>
											<c:otherwise>
												<td><fmt:formatDate value="${module.lastSuccessfulLearningResult.endTimestamp }" type = "both" dateStyle = "long" timeStyle = "long"/></td>
												<td><a href='#' class='template-gen' data-function-code='${fn:escapeXml(module.code) }' app='${fn:escapeXml(applicationId)}' mod='${fn:escapeXml(module.code) }' txnMode='G'>Download</a></td>
												<td>${fn:escapeXml(module.lastSuccessfulLearningResult.lastLearntFieldCount)}</td>
											</c:otherwise>
										</c:choose>
									</tr>
								</c:forEach>
							</tbody>
						</table>
				</div>
			</fieldset>
		</div>
		<div class='modalmask'></div>
		<div class='subframe' style='left:20%'>
		    <iframe frameborder="2" scrolling="no"  marginwidth="5" marginheight="5" style='height:450px;width:600px;' seamless="seamless" id='ttd-options-frame' src=''></iframe>
		</div>
		<form id='main_form' action='LearnerServlet' method='POST'>
	<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<input type='hidden' id='moduleList' name='moduleList' value=''/>
			<input type='hidden' id='learnType' name='learnType' value='NORMAL'/>
			<input type='hidden' id='request-type' name='requesttype' value='initlearner'/>
			<input type='hidden' id='lstLrnrApplication' name='lstLrnrApplication' value='${fn:escapeXml(applicationId) }'/>
		</form>
		
	</body>
</html>