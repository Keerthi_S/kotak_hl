<!-- 

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  test_set_report.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 *
 * DATE              	CHANGED BY              DESCRIPTION
   03-02-2021			Paneendra	            Tenj210-158 
   15-03-2021			Ashiki					TENJINCG-1266
 --> 

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Test Set Report</title>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/jquery.dataTables.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/jquery-ui.css' />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/data-grids.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/v2/test_set_report.js'></script>
	<script type="text/javascript" src='${pageContext.request.contextPath}/js/jquery-1.11.4-ui.js'></script>
</head>
<body>
<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
	<div class='title'>
		<p>Test Set Report</p>
	</div>
	<form name='main_form' action='TestReportServlet' method='POST'>
		<div class='toolbar'>
		       <!-- added by shruthi for CSRF token starts -->
		     <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		     <!-- added by shruthi for CSRF token ends -->
			<input type='button' value='Generate' id='btnGenerate' class='imagebutton download' />
			<img src='images/inprogress.gif' id='img_report_loader' />
		</div>
		<input type='hidden' id='tjn-status' value='${fn:escapeXml(screenState.status) }' />
		<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
		<div id='user-message'></div>
		<div class='form container_16'>
			<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'><label for='tsReport'>Report For</label></div>
				<div class='grid_4'>
					<select class='stdTextBoxNew' id='tsReport' name='tsReport' mandatory='yes'>
						<option value=''>--Select One--</option>
						<option value='1'>Test Set Run History</option>
						<option value='2'>Test Set Execution Detail</option>
						<option value='3'>Test Set Execution Summary</option>
						<!-- Added by Ashiki for TENJINCG-1266 starts -->
						<option value='4'>Test Set Run Summary Report</option>
						<!-- Added by Ashiki for TENJINCG-1266 ends -->
						<option value='5'>Test Execution Report by Tester</option>
					</select>
				</div>
			</div>
			<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<!-- Added by Ashiki for TENJINCG-1266 starts -->
				<div id='reportType'>
				<!-- Added by Ashiki for TENJINCG-1266 ends -->
				<div class='grid_2'><label for='reportType'>Report Type</label></div>
				<div class='grid_4'>
				<!-- modified by shruthi for TCGST-42 starts -->
					<select class='stdTextBoxNew' id='reprtType' name='reportType'>
				<!-- modified by shruthi for TCGST-42 ends -->
						<option value='pdf'>PDF report</option>
						<option value='excel'>Excel report</option>
					</select>
				</div>
				</div>
			</div><div class='clear'></div>
			<fieldset>
				<legend>Report Criteria</legend>
			</fieldset>
			<div id='setRunHistory'>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='runHistTSet'>Test Set</label></div>
					<div class='grid_4'>
						<select class='stdTextBoxNew' id='runHistTSet' name='runHistTSet' mandatory='yes'>
							<option value=''>--Select One--</option>
							<c:forEach items="${testsets }" var="ts">	
								
								<option value='${(ts.id)}:${(ts.name)}'>${(ts.name)}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='runHistFilter'>Search By</label></div>
					<div class='grid_4'>
						<select class='stdTextBoxNew' id='runHistFilter' name='runHistFilter'>
							<option value='run'>Run</option>
							<option value='date'>Date</option>
						</select>
					</div>
				</div><div class='clear'></div>
				<div id='runDiv'>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='runHistRunFrom'>Run From</label></div>
						<div class='grid_4'>
							<select class='stdTextBoxNew' id='runHistRunFrom' name='runHistRunFrom' mandatory='yes'>
								<option value=''>--Select One--</option>
							</select>
						</div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='runHistRunTo'>Run To</label></div>
						<div class='grid_4'>
							<select class='stdTextBoxNew' id='runHistRunTo' name='runHistRunTo' mandatory='yes'>
								<option value=''>--Select One--</option>
							</select>
						</div>
					</div>
				</div>
				<div id='dateDiv'>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='runHistDateFrom'>Date From</label></div>
						<!-- 	Modified by Paneendra for  Tenj210-158 starts -->
						<!-- <div class='grid_4'>
							<input type='text' id='runHistDateFrom' name='runHistDateFrom' value='' class='stdTextBox2' 
							style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
							padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
						</div> -->
						<div class='grid_4'>
							<select class='stdTextBoxNew' id='runHistDateFrom' name='runHistDateFrom' mandatory='yes'>
								<option value=''>--Select One--</option>
							</select>
						</div>
						<!-- 	Modified by Paneendra for  Tenj210-158 ends -->
					</div>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='runHistDateTo'>Date To</label></div>
						<!-- 	Modified by Paneendra for  Tenj210-158 starts -->
						<!-- <div class='grid_4'>
							<input type='text' id='runHistDateTo' name='runHistDateTo' value='' class='stdTextBox2' 
							style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
							padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
						</div> -->
						<!-- 	Modified by Paneendra for  Tenj210-158 ends -->
						<div class='grid_4'>
							<select class='stdTextBoxNew' id='runHistDateTo' name='runHistDateTo' mandatory='yes'>
								<option value=''>--Select One--</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div id='setExecDetail'>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='execDetailTSet'>Test Set</label></div>
					<div class='grid_4'>
						<select class='stdTextBoxNew' id='execDetailTSet' name='execDetailTSet' mandatory='yes'>
							<option value=''>--Select One--</option>
							<c:forEach items="${testsets }" var="ts">	
								<option value='${fn:escapeXml(ts.id) }:${fn:escapeXml(ts.name)}'>${fn:escapeXml(ts.name)}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='execDetailExOn'>Executed On</label></div>
					<!-- 	Modified by Paneendra for  Tenj210-158 starts -->
					<!-- <div class='grid_4'>
						<input type='text' id='execDate' name='execDate' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
					</div> -->
					<div class='grid_4'>
							<select class='stdTextBoxNew' id='execDetailExOn' name='execDetailExOn' mandatory='yes'>
								<option value=''>--Select One--</option>
							</select>
						</div>
						<!-- 	Modified by Paneendra for  Tenj210-158 ends -->
				</div>
			</div>
			<div id='setExecSummary'>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='execSummaryDatefrom'>Date From</label></div>
					<div class='grid_4'>
						<input type='text' id='execSummaryDatefrom' name='execSummaryDatefrom' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='execSummaryDateTo'>Date To</label></div>
					<div class='grid_4'>
						<input type='text' id='execSummaryDateTo' name='execSummaryDateTo' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='execSummaryUser'>Executed By</label></div>
					<div class='grid_4'>
						<select class='stdTextBoxNew' id='execSummaryUser' name='execSummaryUser' mandatory='yes'>
							<option value='all'>All</option>
							<c:forEach items="${users }" var="user">	
								<option value='${fn:escapeXml(user.id)}'>${fn:escapeXml(user.id)}</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			<div id='testerWiseSetExecSummary'>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='testerExecSummaryDateFrom'>Date From</label></div>
					<div class='grid_4'>
						<input type='text' id='testerExecSummaryDateFrom' name='testerExecSummaryDateFrom' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='testerExecSummaryDateTo'>Date To</label></div>
					<div class='grid_4'>
						<input type='text' id='testerExecSummaryDateTo' name='execSummaryDateTo' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='execSummaryUser'>Executed By</label></div>
					<div class='grid_4'>
						<select class='stdTextBoxNew' id='execSummaryTester' name='execSummaryTester' mandatory='yes'>
							<option value='-1'>---Select One--</option>
							<c:forEach items="${users }" var="user">	
								<c:if test="${user.roleInCurrentProject eq 'Test Engineer' }">
									<option value='${fn:escapeXml(user.id)}'>${fn:escapeXml(user.id)}</option>
								</c:if>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			<!-- Added by Ashiki for TENJINCG-1266 starts -->
			<div id='setRunSummary'>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='setRunSummaryDateFrom'>Date From</label></div>
					<div class='grid_4'>
						<input type='text' id='setRunSummaryDateFrom' name='setRunSummaryDateFrom' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='setRunSummaryDateTo'>Date To</label></div>
					<div class='grid_4'>
						<input type='text' id='setRunSummaryDateTo' name='setRunSummaryDateTo' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
					</div>
				</div>
			</div>
			<!-- Added by Ashiki for TENJINCG-1266 ends -->
		</div>
	</form>
</body>
</html>