<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  user_password_change.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright ? 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 

-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@page import="com.ycs.tenjin.TenjinSession"%>   
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
<title>Change Password</title>
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
		<link rel="Stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/data-grids.js'></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/pages/user_password.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/v2/user_new.js'></script>


</head>

<body>

<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->

<c:set var="loggedInUser" value="${sessionScope.TJN_SESSION.user.id }"></c:set>	
	<div class='title'>Change Password</div>
		<form name='change-password-form' action='UserPasswordServlet' method='POST'>
		    <!-- added by shruthi for CSRF token starts -->
		    <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		    <!-- added by shruthi for CSRF token ends -->
			<input type='hidden' name='userid' id='userid' value='${userid}'/>
			
			
			<div class='toolbar'>
				<input type='button' id='btnBack' value='Back' class='imagebutton back' /> 
				<input type='submit' id='btnChange' value='Change' class='imagebutton login'/>
			</div>
			<input type='hidden' name='entryPoint' id='entryPoint' value='${entryPoint}'/>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<div id='user-message'></div>
			
			<div class='form container_16'>
			<fieldset>
					<legend >Password Details</legend>
				<div class='fieldSection grid_8'>
				
			
					<c:if test='${loggedInUser eq userid}'>
					<div class='grid_3'><label for='currentPassword'>Current Password</label></div>
					<div class='grid_4'>
					    <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<input type='password' id='currentPassword' name='currentPassword' class='stdTextBox' mandatory='yes' maxlength="30" placeholder="MaxLength 30"/>
					    <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>
					
					<div class='clear'></div>
					
					</c:if>
					<div class='grid_3'><label for='newPassword'>New Password</label></div>
					<div class='grid_4'>
					    <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<input type='password' id='newPassword' name='newPassword' class='stdTextBox' mandatory='yes' maxlength="30" placeholder="MaxLength 30"/>
					    <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>
					
					<div class='clear'></div>
					
					<div class='grid_3'><label for='confirmPassword'>Confirm Password</label></div>
					<div class='grid_4'>
					    <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<input type='password' id='confirmPassword' name='confirmPassword' class='stdTextBox' mandatory='yes'/maxlength="30" placeholder="MaxLength 30">
					   <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>
					<div class='clear'></div>
					
					<div class='grid_3'><label for='changeReason'>Change Reason</label></div>
					<div class='grid_4'>
						<input type='text' id='changeReason' name='changeReason' class='stdTextBox' mandatory='yes'/>
					</div>
					</div>
					<div style="width: 200% " >
							<table>
								<thead>
									<tr>
										   <th style="font-weight:bold; color:red">Password must contain</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><p class='ptext'> Minimum 8 characters</p> </td></tr>
									
									<tr>
										<td><p class='ptext'> At least one numeric character(e.g. 0-9)</p></td>
									</tr>
									<tr>
										<td><p class='ptext'> At least one special character(e.g~!@#$%^&*()_-+=)</p> </td>
									</tr>
									<tr>
										<td><p class='ptext'> Combination of upper and lower case alphabetic character(e.g A-Z,a-z)</p></td>
									</tr>

								</tbody>
							</table>
						</div>
					
					
					
				</div>
				</fieldset>
				</div>
				<div class='form container_16'>
				<c:if test='${entryPoint eq "first"}'>
				<fieldset>
					<legend >Secuity Questions</legend>
					<div class='fieldSection grid_16'>
						<div class='clear'></div>
					
						<div class='grid_3'><label for='question1'>Question1</label></div>
						<div class='grid_8'>
							<select id='question1' name='question1' class='stdTextBox long' mandatory='yes'>
								<option value='-1'>-- Select One --</option>
								<option value='What was the name of your first school?'>What was the name of your first school?</option>
								<option value='When is your birthday?'>When is your birthday?</option>
								<option value='Which is your favourite city?'>Which is your favourite city?</option>
								<option value='What was your first car?'>What was your first car?</option>
								<option value='Who is your favourite superhero?'>Who is your favourite superhero?</option>
								<option value='Who is your favourite author?'>Who is your favourite author?</option>
							</select>
						</div>
						
						<div class='clear'></div>
						
						<div class='grid_3'><label for='answer1'>Answer</label></div>
						<div class='grid_8'>
							<input type='text' id='answer1' name='answer1' class='stdTextBox long' mandatory='yes'/>
						</div>
						
						
						
						<div class='clear'></div>
						
						<div class='grid_3'><label for='question2'>Question2</label></div>
						<div class='grid_8'>
							<select id='question2' name='question2' class='stdTextBox long' mandatory='yes'>
								<option value='-1'>-- Select One --</option>
								<option value='What was the name of your first school?'>What was the name of your first school?</option>
								<option value='When is your birthday?'>When is your birthday?</option>
								<option value='Which is your favourite city?'>Which is your favourite city?</option>
								<option value='What was your first car?'>What was your first car?</option>
								<option value='Who is your favourite superhero?'>Who is your favourite superhero?</option>
								<option value='Who is your favourite author?'>Who is your favourite author?</option>
							
							</select>
						</div>
						
						<div class='clear'></div>
						
						<div class='grid_3'><label for='answer2'>Answer</label></div>
						<div class='grid_8'>
							<input type='text' id='answer2' name='answer2' class='stdTextBox long' mandatory='yes'/>
						</div>
						
						<div class='clear'></div>
					<div class='grid_12'>
					<div class='grid_4'></div>
					<input type='submit' id='btnChangeFirst' value='Change' class='imagebutton login'/>
					</div>
					</div>
				</fieldset>
				</c:if>
			</div>		
		</form>
</body>
</html>