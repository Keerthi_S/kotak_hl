<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  runtimevalues.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

-->

<!--  
/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 
-->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Runtime Values</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel='stylesheet' href='css/summary.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="js/formvalidator.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/rvresult.js'></script>
		
		<script>
			$(document).ready(function() {
				$('#rv-results-table').DataTable({
					searching:false,
					paging:false,
					length:false,
					sorting:false
				});
			});
		</script>
	</head>
	<body>
	<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
		<div class='title'>
			<p>Run-Time Values</p>
		</div>
		<form name='main_form'>
		 	<div class='toolbar'>
			 <!-- added by shruthi for CSRF token starts -->
		     <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		     <!-- added by shruthi for CSRF token ends -->
				<input type='button' id='btnExit' class='imagebutton cancel' value='Exit'/>
			</div>
			
			<div class='three-col-field-section' style='min-height:120px;'>
				<div class='form-row'>
					<label>Run ID</label>
					<span id='spn_runid'>${runId }</span>
				</div>
				<div class='form-row'>
					<label>Test Case</label>
					<span id='spn_rcid'>${testCaseId }</span>
				</div>
			</div>
			<div class='three-col-field-section' style='min-height:120px;'>
				<div class='form-row'>
					<label>Test Step</label>
					<span id='spn_tstepid'>${runId }</span>
				</div>
				<div class='form-row'>
					<label>Transaction</label>
					<span id='spn_txn'>${testCaseId }</span>
				</div>
			</div>
			<div class='twoplusone-col-field-section' style='overflow:auto;max-height:290px;'>
				<h2>Run-Time Data</h2>
				<div style='padding:10px;font-size:0.9em;'>
					<table class='display' id='rv-results-table' style='width:100%;'>
						<thead>
							<tr>
								<th>Field</th>
								<th>Value</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${runtimevalues }" var="rtv">
								<tr>
									<td><b>${rtv.field }</b></td>
									<td>${rtv.value }</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</form>
	</body>
</html>