<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  current_users.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 
-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Currently Logged-on Users - Tenjin Intelligent Enterprise Testing Engine</title>
	<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		 <link rel="stylesheet" href="css/jquery.dataTables.css">
		<script type='text/javascript' src="js/jquery.dataTables.js"></script>
		<script  type='text/javascript' src="js/tables.js"></script>
		
		<script>
		$(document).ready(function(){
			 $('#btnClear').click(function(){
			if($('#tblCurrentUsers').find('input[type="checkbox"]:checked').length === 0 || ($('#tblCurrentUsers').find('input[type="checkbox"]:checked').length == 1 && $('#tblCurrentUsers').find('input[type="checkbox"]:checked').hasClass('tbl-select-all-rows'))) {
				showLocalizedMessage('no.records.selected', '', 'error');
				return false;
			}
			
			clearMessages();
			
			if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
				var $main_form = $("<form action='ActiveUserServletNew' method='POST' />");
				$main_form.append($("<input type = 'hidden' id='csrftoken_form' name ='csrftoken_form' value='"+$('#csrftoken_form').val()+"'/>/>"))
				if($('#tblCurrentUsers .tbl-select-all-rows').is(':checked')){
				$('#tblCurrentUsers').find('input[type="checkbox"]:checked').each(function() {
					if(!$(this).hasClass('tbl-select-all-rows')) {
						$main_form.append($("<input type='hidden' name='selectedUsers' value='"+ $(this).data('userRecId') +"' />"));
					}
				});
				}else{
					for (var i in selectedItems) {
						$main_form.append($("<input type='hidden' name='selectedUsers' value='"+ selectedItems[i] +"' />"));

					}
				}
				$('body').append($main_form);
				$main_form.submit();
			}
		});
		});
		</script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<div class='title'>
			<p>Active Users</p>
	</div>
	<form name='main_form' action='ActiveUserServletNew' method='POST'>
	<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
	<div class='toolbar'>
	 
		<input type='button' value='Remove' id='btnClear' class='imagebutton delete'/>
		 
	</div>
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
		<div id='user-message'></div>
		<div class='form container_16'>
			<div id='dataGrid'>
				<table id='tblCurrentUsers' class='display tjn-default-data-table' width='100%'>
					<thead>
						<tr>
							<th class='table-row-selector nosort'><input type='checkbox' class='tbl-select-all-rows'/></th>
							<th>User ID</th>
							<th>Name</th>
							<th class="text-center">Terminal</th>
							<th>Current Task</th>
							<th>Logged on Since</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${users }" var="user">
							<tr>
								<td  class='table-row-selector'><input type='checkbox' class='chkBox' data-user-rec-id='${fn:escapeXml(user.id )}'/></td>
								<td><c:out value="${fn:escapeXml(user.id) }"></c:out></td>
								<td><c:out value="${fn:escapeXml(user.fullName) }"></c:out></td>
								<td class="text-center">${fn:escapeXml(user.currentTerminal) eq "0:0:0:0:0:0:0:1" ? "Localhost" : fn:escapeXml(user.currentTerminal)}</td>
								<td>${currentTask[fn:escapeXml(user.id)] eq null ? "N/A" : currentTask[fn:escapeXml(user.id)]}</td>
								<td>${fn:escapeXml(user.lastLogin) }</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</form>
</body>
</html>