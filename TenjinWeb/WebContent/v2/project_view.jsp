<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  project_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
05-02-2021            Shruthi A N             TENJINCG-1255 
02-02-2021            Shruthi A N             Tenj212-4
 

-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
	<link rel='stylesheet' href='css/cssreset-min.css' />
	<link rel='stylesheet' href='css/ifr_main.css' />
	<link rel='stylesheet' href='css/style.css' />
	<link rel='stylesheet' href='css/tabs.css' />
	<link rel='stylesheet' href='css/buttons.css' />
	<link rel="Stylesheet" href="css/jquery.dataTables.css" />
	<link rel='stylesheet' href='css/buttons.css' />	
	<link rel='stylesheet' href='css/960_16_col.css' />

    <link href="css/jquery-ui.css" rel="Stylesheet">
	<link rel="SHORTCUT ICON" HREF="images/yethi.png">
	<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
	<script type="text/javascript" src="js/jquery.dataTables.js"></script>
	<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
	<script type='text/javascript' src=' js/bootstrap.min.js'></script>
	<script type='text/javascript' src='js/pages/v2/project_view.js'></script>
	 <script type='text/javascript' src='js/select2.min.js'></script>
	 <script type='text/javascript' src="js/jquery-ui.js" ></script>
	<script type='text/javascript' src='js/tenjin-aut-selector.js'></script>
	<link rel="SHORTCUT ICON" HREF="images/yethi.png">	
		
 	<link rel='stylesheet' href='css/jquery-ui.css' />




<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<style>
.text-center{
	text-align:center
}
</style>


 
 

</head>
<body>
<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
	<div class='title'>
	             <p>Project Details</p>
	        
	</div> 	
	<form name='main_form' id='main_form' action='ProjectServletNew'
		method='POST'>
		<!-- added by shruthi for CSRF token starts -->
		<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<!-- added by shruthi for CSRF token ends -->
		<input type='hidden' name='t' value='update'/>
		<div class='toolbar'>
			<input type='submit' value='Save' id='btnSave'
				class='imagebutton save' style='display: none;' />
				<input type='button' value='Cancel' id='btnCancel'
				class='imagebutton cancel' style='display: none;' />
			<c:choose>
				<c:when test="${project.state eq 'A'}">
				<c:if test="${currentUser.roleInCurrentProject eq 'Project Administrator' }">
			  <input type='button' value='Edit' id='btnAmmend' class='imagebutton edit' />
			<c:if test="${currentUser.roleInCurrentProject eq 'Project Administrator' }">
					<input type='button' value='Deactivate' id='btnDeactive'
						class='imagebutton InactiveState' />
						</c:if>
						 
						</c:if>
				</c:when>
				<c:otherwise>
				<c:if test="${currentUser.roles eq 'Site Administrator' || fn:escapeXml(currentUser.id) eq fn:escapeXml(project.owner) }">
					<c:if test="${currentUser.roleInCurrentProject eq 'Project Administrator' }">
					<input type='button' value='Activate' id='btnActive'
						class='imagebutton ActiveState' />
					</c:if>
					<c:if test="${project.state eq 'A'}">
						<input type='button' value='E-mail Settings' id='btnMail' 
				class='imagebutton new'/>
						</c:if>
						</c:if>
				</c:otherwise>
			</c:choose>
			<c:if test="${project.state eq 'A' && currentUser.roleInCurrentProject eq 'Project Administrator'}">
             	<input type='button' value='External Test Manager Integration' id='btnextTmInt'
				class='imagebutton tm_mapping' />
				</c:if>
		 
			<input type='button' value='Change History' id='history' class='imagebutton download'/>	
		</div>
		<input type='hidden' id='currentUser' name='currentUser' value='${fn:escapeXml(currentUser.id) }' />
		
		<input type='hidden' id='prjOwner' name='prjOwner' value='${fn:escapeXml(project.owner) }' />
		<input type='hidden' id='userRole' name='userRole' value='${currentUser.roles }' />
		<input type='hidden' id='projectOwner' name='projectOwner' value='${fn:escapeXml(project.owner) }' />
		<input type='hidden' id='prjType' name='prjType' value='${project.type }' />
		<input type='hidden' id='scrnshot' name='scrnShot' value='${project.screenshotoption }' />
		<input type='hidden' id='MSG_DTT' name='scrnShot' value='${MSG_DTT }' />
		<input type='hidden' id='projectId' name='projectId' value='${project.id }' />
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
		<input type='hidden' id='view_type' value='${fn:escapeXml(View_Type)}' />
		<input type='hidden' id='h-refreshTree' value='${fn:escapeXml(refresh_tree)}' />
		<input type='hidden' id='h-selectProject' value='${fn:escapeXml(select_project)}' />
		<input type='hidden' id='currentprjrole' value='${currentUser.roleInCurrentProject}'>	
		<input type='hidden' id='prjName' value='${fn:escapeXml(project.name) }'>
		<div id='user-message'></div>
		<div class='form container_16'>
			<fieldset>
				<legend>Basic Information</legend>
				<div class='fieldSection grid_7'>
				<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtName'>Name</label>
					</div>
					<div class='grid_4'>
					<!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<input type='text' id='txtName' name='name'
							value='${fn:escapeXml(project.name) }' class='stdTextBox' disabled='disabled' maxlength='60' mandatory='yes' placeholder="MaxLength 60"/>
					<!-- Modified by Priyanka for TENJINCG-1233 ends -->	
					</div>
				</div>
			
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstType'>Type</label>
					</div>
					<div class='grid_4'>
						<input type='hidden' id='selPrjType'
							value='' /> 
							<!-- Modified by Priyanka for TENJINCG-1233 starts -->
						    <select id='lstType' name='type' class='stdTextBox' disabled='disabled' maxlength="7">
							<!-- Modified by Priyanka for TENJINCG-1233 ends -->
							<option value='Private'>Private</option>
							<option value='Public'>Public</option>
						</select>
					</div>
				</div>
				
				
				
				<div class='fieldSection grid_14'>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtDescription'>Description</label>
						</div>
						<div class='grid_11'>
							<input type='text' id='txtDescription' name='description'
								class='stdTextBox long' disabled='disabled' value='${fn:escapeXml(project.description) }'
								maxlength='800' style='width:620px'/>
						</div>
				</div>
				
				<div class='fieldSection grid_7'>
				<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtState'>Project Status</label>
					</div>
					<c:choose>
					<c:when test="${project.state eq 'A'}">
					<div class='grid_4'>
						<input type='text' id='txtStartDate' name='startDate'
							value='Active' class='stdTextBox' disabled='disabled' maxlength='60' style='color:green;font-weight: bold;'/>
						
					</div>
					</c:when>
					<c:otherwise>
					<div class='grid_4'>
						<input type='text' id='txtStartDate' name='startDate'
							value='Inactive' class='stdTextBox' disabled='disabled' maxlength='60' style='color:red;font-weight: bold;'/>
						
					</div>
					</c:otherwise>
				</c:choose>
				</div>
				
				</fieldset>
				<fieldset>
				<legend>Other Information</legend>
				
				<div class='fieldSection grid_7'>
				<div class='clear'></div>
					<div class='grid_2'>
						<label for='startDate'>Start Date</label>
					</div>
					
					
					<div class='grid_4'>
					 
							<input type='text' id='startDate' name='startDate'
							value='${sdate}' class='stdTextBox' disabled='disabled' maxlength='60'/>
						
					</div>
				</div>
				
				<div class='fieldSection grid_7'>
				<div class='clear'></div>
					<div class='grid_2'>
						<label for='endDate'>End Date</label>
					</div>
					<div class='grid_4'>
					 
						<input type='text' id="endDate"  value='${edate }'
						style='border: 1px solid #ccc; border-radius: 3px; padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 177px;'
						mandatory='yes' title='Schedule Date' />
						<input type='hidden' name='edate' id='edate' value='${edate }'/>
						
					</div>
				</div>
				
				<div class='fieldSection grid_7'>
				<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtOwner'>Project Owner</label>
					</div>
					<div class='grid_4'>
						 
							<input type='text' id='txtOwner' name='owner'
							value='${project.owner }' class='stdTextBox' disabled='disabled' maxlength='60'/>
						
					</div>
				</div>
				
				<div class='fieldSection grid_7'>
				<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtEmail'>Email</label>
					</div>
					<div class='grid_4'>
						<input type='text' id='txtEmail' name='projectOwnerEmail'
							value='${fn:escapeXml(project.projectOwnerEmail) }' class='stdTextBox' disabled='disabled' maxlength='60'/>
						
					</div>
				</div>
				
				<!-- Added by shruthi for TENJINCG-1255 starts -->
				<div class='fieldSection grid_7'>
				<div class='clear'></div>
					<div class='grid_2'>
						<label for='repotype'>Repository Type</label>
					</div>
					<div class='grid_4'>
						 <select name='repotype' id='repotype' class='stdTextBoxNew' disabled='disabled' value=''>
							<option>${fn:escapeXml(project.repoType) }</option>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7 show-for-all'>
				<div class='clear show-for-all'></div>
				<div class='grid_2 show-for-all'>
					<label for='bucket'>Bucket Name</label>
				</div>
				<div class='grid_4 show-for-all'>
					<input  name='bucket' id='bucket' class='stdTextBox' disabled='disabled' value='${fn:escapeXml(project.repoRoot)}'/>
				</div>
				</div>
				
				<div class='fieldSection grid_7 show-for-native'>
				<div class='clear show-for-native'></div>
				<div class='grid_2 show-for-native'>
					<label for='rootfolder'>Root Folder</label>
				</div>
				<div class='grid_4 show-for-native'>
					<input type='text' name='rootfolder' id='rootfolder' class='stdTextBox' disabled='disabled' value='${fn:escapeXml(project.repoRoot)}'/>
				</div>
		
				</div>
				<!-- Added by shruthi for TENJINCG-1255 ends -->
				
				
			</fieldset>
			 <div class='form container_16'>
				<fieldset style='margin-top: 10px;'>
					<legend>Screenshot Configuration</legend>
					<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtScreenShot'>Screenshot</label>
					</div>
					<div class='grid_4'>
						<select id='txtScreenShot' name='txtScreenShot' class='stdTextBox'
							disabled='disabled'>
							
							<option value='0' selected>Never Capture</option>
							<option value='1'>Capture Only for Failures / Errors</option>
							<option value='2'>Capture For All Messages
							<option value='3'>Capture At the End of Each Page</option>
							
						</select>
					</div>
				</div>
				</fieldset>
			</div>
			<div style='width:108%'>
			<fieldset>
				<legend>Linkages</legend>
				<div>
					<ul id='tabs'>
						<li><a href='#project-users-tab' class='user'
							id='project-users-tab-anchor'>Project Users</a></li>
						<li><a href='#project-aut-tab' class='aut'
							id='project-aut-tab-anchor'>Applications Under Test</a></li>
							<!-- commeted by shruthi for  Tenj212-4 starts -->
						<!-- <li><a href='#project-tdp-tab' class='template'
							id='project-tdp-tab-anchor'>Test Data Paths</a></li> -->
					        <!-- commeted by shruthi for  Tenj212-4 ends -->
					 
							<c:if test="${currentUser.roleInCurrentProject eq 'Project Administrator'|| fn:escapeXml(currentUser.id) eq fn:escapeXml(project.owner)  }">
							 
							<li><a href='#project-defect-tab' class='defect'
							id='project-defect-tab-anchor'>Defect Management</a></li>
							</c:if>
						<li>
						<a href='#project-details-tab' class='projectdetails details'
							id='project-details-tab-anchor'>Audit History</a></li>
					</ul>
					<div id='project-users-tab' class='tab-section'>
						<c:if  test="${project.state eq 'A'}">
						<div class='toolbar'>
						<c:if test="${currentUser.roleInCurrentProject eq 'Project Administrator' }">
							<input type='button' value='Add' id='btnAddNewProjectUser'
								class='imagebutton new' /> <input type='button' value='Remove'
								id='btnRemoveProjectUser' class='imagebutton delete' />
								</c:if>
							<input type='button' value='Refresh' id='btnRefreshProjectUser'
								class='imagebutton refresh' />
						</div>
						</c:if>
						<div id='user-message-users'></div>

						<div id='dataGrid'>
							<table id='project-users-table' class='display' cellspacing='0'
								width='100%'>
								<thead>
									<tr>
										<c:choose>
											  <c:when test="${currentUser.roles eq 'Site Administrator' || fn:escapeXml(currentUser.id) eq fn:escapeXml(project.owner) }">
											    <th class='nosort'><input type='checkbox'
													class='tbl-select-all-rows' id='chk_all_asc_users' /></th>
											  </c:when>
											  <c:otherwise>
											    <th ><input type='checkbox'
													class='tbl-select-all-rows' id='chk_all_asc_users' disabled/></th>
											  </c:otherwise>
										</c:choose>
										
										<th>User ID</th>
										<th>Full Name</th>
										<th>Email ID</th>
										<th>Role</th>
									</tr>
								</thead>
								 
							</table>
						</div>
					</div>
					<div id='project-aut-tab' class='tab-section'>
					<c:if  test="${project.state eq 'A'}">
						<div class='toolbar'>
						<c:if test="${currentUser.roleInCurrentProject eq 'Project Administrator' }">
							<input type='button' value='Add' id='btnAddNewAut'
								class='imagebutton new' />
							<input type='button' value='Save' id='btnSaveURL'
								class='imagebutton save' />
							<input type='button' value='Remove' id='btnRemoveAut'
								class='imagebutton delete' />
						</c:if>
							<input type='button' value='Refresh' id='btnRefreshAut'
								class='imagebutton refresh' />
                              
						</div>
						</c:if>
						<div id='user-message-auts'></div>
						<div class="clear"></div>
						<div id='dataGrid'>
							<table id='project-aut-table' class='display' cellspacing='0'
								width='100%'>
								<thead>
									<tr>
										<th class='nosort'><input type='Checkbox' id='chk_all_pr_aut'
											class='tbl-select-all-rows' title='Select All' /></th>
										<th class="text-center">Application ID</th>
										<th>Application Name</th>
										<th>TTD Password</th>
										<th>URI/Package</th>
										<!-- commeted by shruthi for  Tenj212-4 starts -->
										<!-- <th>Test Data Path</th> -->
										<!-- commeted by shruthi for  Tenj212-4 ends -->
									</tr>
								</thead>
							</table>
						</div>
					</div>
					
					<!-- commeted by shruthi for  Tenj212-4 starts -->
					<%-- <div id='project-tdp-tab' class='tab-section'>
					
					 
						<c:if  test="${project.state eq 'A' && currentUser.roleInCurrentProject eq 'Project Administrator' }">
						<div class='toolbar'>
							 
							<input type='button' id='btnSaveTDP' class='imagebutton save' value='Save' /> 
							<input type='button' id='btnCancelTDP' class='imagebutton cancel' value='Cancel' />
						</div>
						</c:if>
						
						<div id='user-message-tdps' style='height: 20px; font-size: 12px; padding:5px;margin:5px;'></div>
						<div class='fieldSection grid_15' id='pagination_block'>
							<div style='display: table-cell; float: left;'>
								<br/>
								<label style='display:inline;'>Application&nbsp;</label> 
								<tjn:applicationSelector defaultValue="${applicationId }" cssClass="stdTextBoxNew" projectId='${project.id }' groupSelector="functionGroup" loadFunctionGroups="true" id='application' name='application' title='Application'/>
								
								<label style='display:inline;'>&nbsp;&nbsp;Groups&nbsp;</label>
								<tjn:functionGroupSelector cssClass="stdTextBoxNew" applicationSelector="application" id='functionGroup' name='functionGroup' applicationId="${applicationId }" defaultValue="${fn:escapeXml(selectedGroup) }" title='Function Group'/>

								
								<label style='display:inline;'>Path&nbsp;</label>
								<input type="text" id='map_url_path' class='stdTextBox' >
								&nbsp;&nbsp;&nbsp;
								<input type='button' value="AddPath" id="addpath" class="imagebutton add" onclick="addPath()" >
								
							</div>
						</div>
						 
						<div class='clear'></div>
						<div id='dataGrid'>
						
							<table id='project-tdp-table' class='display' cellspacing='0'
								width='100%'>
								<thead>
									<tr>
										 
										<th class="text-center">Application Id</th>
										<th>Application Name</th>
										<th>Function/API </th>
										<th>Group Name</th>
										<th>Test Data Path</th>
									</tr>
								</thead>
								
							</table>
						</div>
					</div> --%>
				<!--  commeted by shruthi for  Tenj212-4 ends -->
				
				<div id='project-defect-tab' class='tab-section'>
					<c:if  test="${project.state eq 'A'}">
						<div class='toolbar'>
							<input type='button' id='btnSaveDef' class='imagebutton save'
								value='Save' />
							<img src='images/inprogress.gif' id='dmLoader' />
						</div>
						</c:if>
					 
						<div id='user-message-def'></div>
						<div class="clear"></div>
						<div id='dataGrid'>
						
							<table id='project-def-table' class='display' cellspacing='0'
								width='100%'>
								<thead>
									<tr>
										<th>Application</th>
										 
										<th>Defect Management Instance</th>
										<th>Subset</th>
										<th>Defect Management Project / Repositories</th>
										<th>Enable Status</th>
										<th>Actions</th>
										<th>Un-Map</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
					<div id='project-details-tab' class='tab-section'>
						<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label for='txtUpdatedBy'>Created By</label></div>
								<div class='grid_4'><input type='text' id='txtUpdatedBy' name='txtUpdatedBy'class='stdTextBox' disabled='disabled' value='${fn:escapeXml(project.owner) }' /></div>
							</div>
						<div class='fieldSection grid_7'>
					
							<div class='clear'></div>
							<div class='grid_2'><label for='txtCreatedDate'>Created On</label></div>
							<div class='grid_4'>
								<input type='text' id='txtCreatedDate' name='txtCreatedDate'class='stdTextBox' disabled='disabled' value='${project.createdDate }' /></div>
							</div>
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label for='txtCreatedBy'>Last Updated By</label></div>
								<div class='grid_4'><input type='text' id='txtUpdatedBy' name='txtUpdatedBy'class='stdTextBox' disabled='disabled' value='${fn:escapeXml(project.auditRecord.auditId) eq 0 ? "N/A" : fn:escapeXml(project.auditRecord.lastUpdatedBy) }' /></div>
							</div>						
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label for='txtUpdatedDate'>Last Updated On</label></div>
								<div class='grid_4'>
									<input type='text' id='txtUpdatedDate' name='txtUpdatedDate'class='stdTextBox' disabled='disabled' value='${project.auditRecord.auditId eq 0 ? "N/A" : project.auditRecord.lastUpdatedOn }' />
								</div>
							</div>		
						</div>
					
				</div>
			</fieldset>
			</div>
		</div>
	</form>
	<div class='modalmask'></div>
	<div class='subframe'>
	 
		<iframe frameborder="2" scrolling="no" marginwidth="5"
			marginheight="5"
			style='height: 380px; width: 770px; overflow: hidden;'
			seamless="seamless" id='prj-sframe' src=''></iframe>
		<iframe frameborder="2" scrolling="no" marginwidth="5"
				marginheight="5" style='height: 350px; width: 600px; overflow: hidden;'
				seamless="seamless" id='history-sframe' src=''>
		</iframe>
		
	</div>
</body>
</html>