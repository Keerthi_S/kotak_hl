<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  client_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 

-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
	<head>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<link rel="stylesheet" href="css/jquery.dataTables.css">
		<script type='text/javascript' src="js/jquery.dataTables.js"></script>
		<script  type='text/javascript' src="js/tables.js"></script>
		
		
		<script>
		
			 

			$(document).on('click','#btnNew', function() {
				window.location.href ='ClientServlet?t=new';
			});
			
			$(document).on("click", "#btnDelete", function() {
				 
				
				if($('#client-table').find('input[type="checkbox"]:checked').length === 0 || ($('#client-table').find('input[type="checkbox"]:checked').length == 1 && $('#client-table').find('input[type="checkbox"]:checked').hasClass('tbl-select-all-rows'))) {
					showLocalizedMessage('no.records.selected', '', 'error');
					return false;
				}
				
				clearMessages();
				
				if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
					var $clientForm = $("<form action='ClientServlet' method='POST' />");
					$clientForm.append($("<input type='hidden' id='del' name='del' value='true' />"))
					$clientForm.append($("<input type='hidden' id='csrftoken_form' name='csrftoken_form' value='" + $('#csrftoken_form').val() + "' />"))
					
					$('#client-table').find('input[type="checkbox"]:checked').each(function() {
						if(!$(this).hasClass('tbl-select-all-rows')) {
							$clientForm.append($("<input type='hidden' name='selectedClients' value='"+ $(this).data('clientRecId') +"' />"));
						}
					});
					$('body').append($clientForm);
					$clientForm.submit();
				}
			});
		</script>
	</head>
	<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		<div class='title'>
			<p>Registered Clients</p>
		</div>
		<div class='toolbar'>
			<input type='button' id='btnNew' class='imagebutton new' value='New Client' />
			<input type='button' id='btnDelete' class='imagebutton delete' value='Remove' />
		</div>
		<input type='hidden' id='callback' value='${callback }' />
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message)}' />
		
		<div id='user-message'></div>
		<div class='form container_16'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div id='dataGrid'>
				<table id='client-table' class="display tjn-default-data-table">
					<thead>
						<tr>
							<th class='nosort'><input type='checkbox' class='tbl-select-all-rows'/></th>
							<th>Client Name</th>
							<th class="text-center">Host Name</th>
							<th class="text-center">Port </th>
							<th>OS Type</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${clients }" var="client">
							<tr>
								<td class='table-row-selector'><input type='checkbox' data-client-rec-id='${client.recordId }' /></td>
								<td><a href='ClientServlet?t=view&key=${fn:escapeXml(client.recordId )}'><c:out value="${fn:escapeXml(client.name) }"></c:out></a></td>
								<c:choose>
									<c:when test="${client.deviceFarmCheck eq 'Y'}">
										<td class="text-center">Device Farm</td>
										<td class="text-center">N/A</td>
									</c:when>
									<c:otherwise>
										<td class="text-center">${fn:escapeXml(client.hostName) eq "0:0:0:0:0:0:0:1" ? "Localhost" : fn:escapeXml(client.hostName) }</td>
										<td class="text-center"><c:out value="${fn:escapeXml(client.port) }"></c:out></td>
									</c:otherwise>
								</c:choose>
								<td><c:out value="${fn:escapeXml(client.osType) }"></c:out></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>