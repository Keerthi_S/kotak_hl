<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  messages_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 15-06-2021				Ashiki				Newly added for TENJINCG-1257

-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/tenjin-aut-selector.js'></script>
		<script src="js/tables.js"></script>
		
		<script>
		
		 /* $(document).on('click','.messageCode',function(e){
			 var appId=$('#app').val();
	          var moduleCode=encodeURIComponent($(this).data('messageCode'));
            window.location.href='MessageServlet?t=view&key='+moduleCode+'&appId='+appId;
 	        }); */
 	       $(document).on("click", "#btnDelete", function() {
 	    		clearMessagesOnElement($('#user-message'));
 	    		 var count=0;
 	    		  var url;
 	    		
 	    		  count=selectedItems.length;
 	    			
 	    		 if ($("#chk_all").is(':checked')) {
 	    			 
 	    			 var record = JSON.stringify(selectedItems);
 	    	         var name = JSON.parse(record);
 	    	         url='param=deleteall&paramval='+selectedItems;
 	    		   }
 	    		
 	    		 else if (count>0) {
 	    			 var val =selectedItems;
 	    			 var instance = [];
 	    			 var i=0;
 	    			 $("#chk_def:checked").each(function () {
 	    				 var tmp = $(this).val().split(",");
 	    				 val[i] = tmp[0];
 	    				 i++;
 	    			    });
 	    			url='param=deleteall&paramval='+val;
 	    			}
 	    		 if (count==0) {
 	    			 showMessage("Please select at least one Message to delete", 'error');	
 	    		 }
 	    		 
 	    		 if(count==1 || count>1){
 	    			 if (window.confirm('Are you sure you want to delete?')){
 	    				 var message="";	 
 	    		  $.ajax({
 	    			url:'MessageServlet',
 	    			data :url,
 	    			async:false,
 	    			dataType:'json',
 	    			
 	    			success: function(data){
 	    			var status = data.status;
 	    			if(status == 'SUCCESS')
 	    			  {			
 	    				message=data.message;
 	    				window.location='MessageServlet?param=list';
 	    				showMessage(data.message,'success');
 	    			  }
 	    			  else
 	    			  {
 	    				showMessage(data.message, 'error');				
 	    			  }
 	    		    },
 	    		
 	    		   error:function(xhr, textStatus, errorThrown){
 	    			   showMessage(data.message, 'error');			
 	    		  }
 	    	     });
 	    		 }
 	    		 }
 	    	 });
		
	 	$(document).ready(function() {
			var path=$('#downloadPath').val();
			if(path!=''&& path!=undefined){
				var $c = $("<a id='downloadFile' href="+path+" target='_blank' download />");
				$("body").append($c);
					$c.get(0).click();
			}
			/* $('#upload').click(function(){
			$('#download').click(function(){
				clearMessagesOnElement($('#user-message'));
				$('.subframe > iframe').attr('src','downloadFunctions.jsp');
				$('.subframe').width(400).height(180);
				$('.subframe > iframe').width(400).height(180);
				$('.modalmask').show();
				$('.subframe').show();
			});
		}); */
		 
		$(document).on("click", '#btnNew', function() {
			window.location.href='MessageServlet?param=new';
		});
		
		
		});
	 	$(document).on('click','.template-gen',function(e){
			var $td = $(this).parent();
			$(this).hide();
			var $img = $("<img src='images/inprogress.gif' alt='Please Wait'/>");
			$td.append($img);
			var msgId = $(this).data('msgid');
			$.ajax({
				url:'MessageServlet',
				data:'param=DOWNLOAD_DATA_TEMPLATE&msgId='+msgId,
				async:false,
				dataType:'json',
				success:function(data){
					var status = data.status;
					if(status == 'SUCCESS'){
						 showMessage(data.message,'success');
						var $c = $("<a id='downloadFile' href="+data.path+" target='_blank' download />");
						$("body").append($c);
	                    $("#downloadFile").get(0).click();
	                    $c.remove();
	                   
					}else{
						var message = data.message;
						showMessage(message,'error');
					}
				},
				error:function(xhr, textStatus, errorThrown){
					showMessage(errorThrown, 'error');
				}
			});
			
			$img.remove();
			$(this).show();
			
		});
		</script>
	</head>
	<body>
	
		<div class='title'>
			<p>Messages</p>
		</div>
		<div class='toolbar'>
			<input type='button' id='btnNew' class='imagebutton new' value='New Message' />
			<input type='button' id='btnDelete' class='imagebutton delete' value='Remove' />
		</div>
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
		<input type='hidden' id='downloadPath' name='downloadPath' value='${downloadPath }'/>
		
		<div id='user-message'></div>
		
		<div class='form container_16'>
		
			<fieldset>
				<legend>Messages</legend>
				<div class='fieldSection grid_15'>
						<table class='display tjn-default-data-table' id='messagesTable'>
							<thead>
								<tr>
									<th class='table-row-selector nosort'><input type='checkbox' id='chk_all' class='tbl-select-all-rows' value='' /></th>
									<th>Message Id</th>
									<th>Name</th>
									<th>Format</th>
 									<th>Message type</th>
 									<th>Template</th>
                                    
								</tr>
							</thead>
							 <tbody>
								<c:forEach items="${messagelist}" var="message">
									<tr>
										<td class='table-row-selector'><input type='checkbox' class='checkbox chkBox' id='chk_def' value='${fn:escapeXml(message.messageId) }'/></td>
										<td>${fn:escapeXml(message.messageId) }</td>
										<td title='${fn:escapeXml(message.messageName) }' >${fn:escapeXml(message.messageName) }</td>
										<td>${fn:escapeXml(message.messageFormat) }</td>
										<td>${fn:escapeXml(message.subType)}</td>
										<td><a href='#'class='template-gen' data-msgId='${fn:escapeXml(message.messageId)}'>Download</a></td>
									</tr>
								</c:forEach>
							</tbody> 
						</table>
				</div>
			</fieldset>
		</div>
	</body>
</html>