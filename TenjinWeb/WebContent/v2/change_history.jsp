<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  change_history.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
* DATE                 	CHANGED BY              DESCRIPTION
 

-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Audit Report</title>
	<link rel='stylesheet' href='css/cssreset-min.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/jquery-ui.css' />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/v2/change_history.js'></script>
	<script type="text/javascript" src='${pageContext.request.contextPath}/js/jquery-1.11.4-ui.js'></script>
		<style>
			#footer {
  	 			 position: fixed;
   	 			 bottom: 0;
  		     	 width: 100%;
  		     	 text-align:center
			}
		</style>
</head>
<body>
	<div class='title'>
		<p>Change History Report</p>
	</div>
	<form name='history_form'>
		<div id='user-message'></div>
		<div class='form container_16' id='historyReport'>
			<input type='hidden' id='activityArea' value='${activityArea }' />
			<input type='hidden' id='entityRecId' value='${entityRecId }' />
			<input type='hidden' id='entityId' value='${entityId }' />
			<input type='hidden' id='entityType' value='${entityType }' />
			<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'><label for='reportType'>Report Type</label></div>
				<div class='grid_4'>
					<select class='stdTextBoxNew' id='reportType' name='reportType'>
						<option value='pdf'>PDF report</option>
						<option value='excel'>Excel report</option>
					</select>
				</div>
			</div><div class='clear'></div>
				
			<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'><label for='historyDateFrom'>Date From</label></div>
				<div class='grid_4'>
					<input type='text' id='historyDateFrom' name='historyDateFrom' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
				</div>
			</div>
			<div class='clear'></div>
			<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'><label for='historyDateTo'>Date To &nbsp&nbsp&nbsp&nbsp</label></div>
				<div class='grid_4'>
					<input type='text' id=historyDateTo name='historyDateTo' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
				</div>
			</div>	
		</div> 
		<div class='toolbar' id="footer">
			<input type='button' id='btnOk' value='Go' class='imagebutton ok'/>
			<input type='button' id='btnCancel' value='Cancel' class='imagebutton cancel'/> 
		</div>
	 </form>
</body>
</html>