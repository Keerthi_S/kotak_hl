<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  swagger_file_upload.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 

-->


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Swagger Api File Upload</title>

		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
		<link rel="Stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/data-grids.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script  type='text/javascript' src="js/tables.js"></script>
		
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/v2/swagger_file_upload.js'></script>
</head>
<body>
<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
		
		<div class='title'>
			<p>Import REST APIs</p>
		</div>
		
		<input type='hidden' id='callback' value='${callback }' />
		
		<form id='SwaggerForm' action='RestProcessServlet' method='POST' enctype="multipart/form-data">
		<input type='hidden' id='appid' name='appId' value='${APP }' />
		     <!-- added by shruthi for CSRF token starts -->
		     <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		     <!-- added by shruthi for CSRF token ends -->
			<div class='toolbar'>
				<a style="text-decoration: none;" href='aut_api_information.jsp'><input type='button' id='btnBack' value='Back' class='imagebutton back' />
				</a>
				<input type='button' id='btnSave' value='Save' class='imagebutton save' />
				 <img src='images/inprogress.gif' id='btnSaveLoader' class='ajx-loader' />
			</div>
			
			
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			
			<div id='user-message'></div>
			<div class='form container_16'>
				<fieldset>
					<legend>Source</legend>
					<div class='fieldSection grid_13'>
						<div class='grid_2'><label for='swaggerURL'>Import from URL</label></div>
						<div class='grid_6'>
							<input type='text' class='stdTextBox' value='${url }'' id='swaggerURL' name='url' />
							<input type='button' id='btnImportFromUrl' value='Import' class='imagebutton upload'/>
							 <img src='images/inprogress.gif' id='swaggerURLLoader' class='ajx-loader'/>
						</div>
						
						<div class='clear'></div>
						<div class='clear'></div>
						
						<div class='grid_2'><label for='swaggerJSON'>Upload</label></div>
						<div class='grid_6'>
							<input type='file' id='swaggerJSON' name='json' class='stdTextBox' mandatory='yes' title='JSON File'/>
							<input type='submit' id='btnUploadSwaggerJSON' value='uploadJSON' class='imagebutton upload'/>
							
							
						</div>
						
					</div>
			
					
				</fieldset>
				
				<fieldset id='quickSelectionBlock' class='post-import'>
					<legend>Quick Import</legend>	
					
					<div class='fieldSection grid_13'>
						<div class='grid_3'>
							<input type='radio' name='quickImportSelection' value='ALL' id='importAll' class='inline-checkbox' /><label for='importAll' class='inline'>Import all <span id='allApisCount'></span> APIs</label>
						</div>
						<div class='clear'></div>
						<div class='grid_3'>
							<input type='radio' name='quickImportSelection' value='NEW' id='importNew' class='inline-checkbox' /><label for='importNew' class='inline'>Import <span id='newApisCount'></span> new APIs</label>
						</div>
						<div class='clear'></div>
						<div class='grid_3'>
							<input type='radio' name='quickImportSelection' value='METHOD' id='importByMethod' class='inline-checkbox' /><label for='importByMethod' class='inline'>Import By Method</label>
						</div>
						<div class='grid_9' id='quickImportMethods'>
							
						</div>
					</div>
					
				</fieldset>
				
				<fieldset id="apisBlock" class='post-import'>
					<legend>Manual Selection</legend>
					<div class='clear'></div>
					<div class='clear'></div>
					<div id='dataGrid'>
						<table id='api-table' class="display" >
							<thead>
								<tr>
									<th class='table-row-selector nosort'><input type='checkbox' class='tbl-select-all-rows' id='chk_all_api'/></th>
									<th>API Code</th>
									<th>Method</th>
									<th>URL</th>
									<th>Name</th>
									<th>Availability</th>
								</tr>
							</thead>
							 
						</table>
					</div>					
				</fieldset>
			</div>
		</form>

</body>
</html>