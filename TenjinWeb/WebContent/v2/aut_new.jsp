<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  aut_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION

-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
	<head>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/formvalidator.js'></script>
		<script type='text/javascript' src='js/tenjin-aut-selector.js'></script>
		<script src="js/pages/v2/aut_new.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/v2/user_new.js'></script>
	</head>
	<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		<div class='title'>
			<p>New Application</p>
		</div>
		<form id='autform' name='autform' action='ApplicationServlet' method='POST'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
			    <input type='submit' id='btnSave' value='Save' class='imagebutton save' />
				<input type='button' id='btnCancel' value='Cancel' class='imagebutton cancel' />
			</div>
			
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<input type='hidden' id='txtPlatform' name='platform' />
			<input type='hidden' id='transactionType' name='transactionType' value='New' />
			<div id='user-message'></div>
			
			<div class='form container_16'>
				<fieldset>
					<legend>Basic Information</legend>
					<div class='fieldSection grid_16'>
					  <div class='clear'></div>
						<div class='grid_3'><label for='txtAutName'>Application Name</label></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_12'><input type='text' id="txtAutName" name="name" value="${fn:escapeXml(aut.name)}"  class='stdTextBox' mandatory='yes' title='Application Name' maxlength='100' /></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>
					<div class='fieldSection grid_8'>
					    <div class='clear'></div>
						<div class='grid_3'><label for='txtAppType'>Application Type</label></div>
						<div class='grid_4'>
						<input type='hidden' id='lstAppType' name='lstAppType' value='${aut.applicationType }'>
							<select id='txtAppType' name='type' value='${aut.applicationType }'class='stdTextBoxNew'  title='Application Type' >
								<option value='1'>Web Application</option>
								<option value='2'>Windows</option>
								<option value='3'>Mobile-Android</option>
								<option value='4'>Mobile-iOS</option>
								
							</select>
						</div>
					</div>
						<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='adapterPackage'>Adapter</label></div>
						<div class='grid_4'>
						<input type='hidden' id='lstAdapter' name='lstAdapter' value='${aut.adapterPackage }'>
							<select id='adapterPackage'name='adapterPackage' class='stdTextBoxNew'  title='Adapter Package' >
									 <c:forEach items="${adapters}" var="adapters">
                                           <option value="${adapters}">${adapters}</option>
                                      </c:forEach>
							</select> 
						</div>
					</div>
				</fieldset>
				<fieldset>
				 <legend>Application Information</legend>
					<div class='fieldSection grid_16'>
						<div class='clear show-for-all'></div>
						<div class='grid_3 show-for-all'><label for='txtAutURL'>URI</label></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_12 show-for-all'><input type='text' id='txtAutURL' name='URL' value='${fn:escapeXml(aut.URL)}' class='stdTextBox long' title='AUT URI' maxlength='199' mandatory='yes' maxlength='200' /></div>
					     <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>
					<div class='clear'></div>
					<div class='fieldSection grid_8'>
					<div class='clear show-for-all'></div>
						<div class='grid_3 show-for-all browser'><label for='txtAutBrowser'>Browser</label></div>
						<div class='grid_4 show-for-all browser'>
						<input type='hidden' id='lstBrowser' name='lstBrowser' value='${aut.browser }'>
							<select id='txtAutBrowser' name='browser' class='stdTextBoxNew'  title='AUT Browser' >
								<option value='<%=BrowserType.CHROME%>'><%=BrowserType.CHROME%></option>
								<option value='<%=BrowserType.FIREFOX%>'><%=BrowserType.FIREFOX%></option>
								<option value='<%=BrowserType.IE%>'><%=BrowserType.IE%></option>
								<option value='<%=BrowserType.CE%>'><%=BrowserType.CE%></option>
							</select>
						</div>
						<div class='clear mobileTxt'></div>
						<div class='grid_3 mobileTxt'>
							<label for='txtPackage' id='txtPackageAnd'>Package</label>
					    	<label for='txtPackage' id='txtPackageIos'>Bundle Id</label>
						</div>
						  <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_4 mobileTxt'><input type='text' id='txtPackage' name='applicationPackage' value='${fn:escapeXml(aut.applicationPackage) }' class='stdTextBox' title='Package or Bundle Id' mandatory='yes' maxlength="255" /></div>
						  <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						<div class='clear'></div>
						
					</div>
					<div class='fieldSection grid_7'>
					
						
						<div class='clear mobileTxt android'></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_2 mobileTxt android'><label for='txtActivity'>Activity</label></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						<div class='grid_4 mobileTxt android'><input type='text' id='txtActivity' name='applicationActivity' value='${fn:escapeXml(aut.applicationActivity) }' class='stdTextBox' title='Activity' mandatory='yes' maxlength="225"/>
						</div>

						</div>
				</fieldset>
				<fieldset>
					<legend>Preferences</legend>
					<div class='fieldSection grid_8'>
						<div class='clear'></div>
						<div class='grid_3'><label for='txtPauseTime'>Pause Time</label></div>
						<div class='grid_4'><input type='text' id='txtPauseTime' name='pauseTime' value='${aut.pauseTime==0 ? "" : aut.pauseTime }' class='stdTextBox' Title='Pause Time' placeholder='in seconds'/></div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='txtLocation'>Pause Location</label></div>
						<div class='grid_4'>
						<input type='hidden' id='txtPauselocation' value='${aut.pauseLocation}'/>
						         <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<select id='txtLocation' name='pauseLocation' class='stdTextBoxNew' title='Pause Location' maxlength="20">
								 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
								<option value=''>-- Select One --</option>
								<option value='before login'>Before login</option>
								<option value='after login'>After login</option>
							</select>
						</div>
					</div>
					<div class='fieldSection grid_16'>
		 
						<div class='clear'></div>

					<div class='grid_3'>
						<label for='txtAutOperation'>AUT Operations</label>
					</div>
					<div class='grid_12'>
					        <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<input type='text' id='txtAutOperation' name='operation'
							value='${fn:escapeXml(aut.operation)}' class='stdTextBox long'
							title='AUT Operations' mandatory='yes' maxlength ='500'/>
							 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>

					<div class='clear'></div>
					<div class='clear'></div>
					<div class='grid_3'>
						<label for='txtAutUserType'>AUT User Types</label>
					</div>
					<div class='grid_12'>
					<!-- Modified By Priyanka for KTKCBS-7 starts -->
					         <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<input type='text' id='txtAutUserType' name='loginUserType'
							value='${fn:escapeXml(aut.loginUserType)}' class='stdTextBox long'
							title='AUT User Types' mandatory='yes' maxlength ='500'/>
							 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
							 <!-- Modified By Priyanka for KTKCBS-7 ends -->
					</div>
					<div class='clear'></div>
						<div class='grid_3'>
						<label for='txtTemplatePwd'>TTD Password</label>
					</div>
					<div class='grid_12'>
					    <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<input type='password' id='txtTemplatePwd' name='templatePwd' auto-complete='new-password'
							value='${aut.templatePwd}' class='stdTextBox'
							title='Template Password' maxlength="200" autocomplete="new-password"/>
					    <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>
					<!-- Added by paneendra for VAPT fix starts -->
					<table>
								<thead>
									<tr>
										   <th style="font-weight:bold; color:red">Password must contain</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><p class='ptext'> Minimum 8 characters</p> </td></tr>
									
									<tr>
										<td><p class='ptext'> At least one numeric character(e.g. 0-9)</p></td>
									</tr>
									<tr>
										<td><p class='ptext'> At least one special character(e.g~!@#$%^&*()_-+=)</p> </td>
									</tr>
									<tr>
										<td><p class='ptext'> Combination of upper and lower case alphabetic character(e.g A-Z,a-z)</p></td>
									</tr>

								</tbody>
							</table>
					<!-- Added by paneendra for VAPT fix ends -->
					<div class='clear'></div>
						<div class='grid_3'><label for='date'>Date Format</label></div>
						
						<div class='grid_4'><input type="text" id='dateFormat' name='dateFormat' value='${fn:escapeXml(aut.dateFormat) }' class='stdTextBox' Title='Date Format' placeholder='Application Date Format' maxlength="50"></div>
						<div class='grid_12'>
							<table id='txtDate' class='border' width='43%'>
								<thead>
									<tr>
										   <th style="font-weight:bold">Suggestions</th>
									</tr>
								</thead>
								<tbody>
								<tr>
								<td><p class='ptext'>'d' - Date, 'M' - Month, 'y' -
												Year</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>You must enter Date Format not
												Date value</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>Some valid examples:</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>dd-MM-yyyy (13-01-2017)</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>dd/MM/yyyy (13/01/2017)</p></td>
									</tr>
									<tr>
										<td><p class='ptext'>dd MMM yyyy (13 JAN 2017)</p></td>
									</tr>

								</tbody>
							</table>
						</div> 
					</div>
				</fieldset>
			</div>
		</form>
	</body>
</html>