<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  project_mail_setting.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright ? 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
 12-03-2020             Lokanath			    TENJINCG-1185
 13-03-2020				Jagadish                TENJINCG-1184
 17-03-2020				Lokanath				TENJINCG-1184
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<link rel='stylesheet' href='css/paginated-table.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/paginated-table.js'></script>
		<link rel="stylesheet" href="css/jquery.dataTables.css">
		<script type='text/javascript' src="js/jquery.dataTables.js"></script>
		<script  type='text/javascript' src="js/tables.js"></script>
		 <script type='text/javascript' src="js/jquery-ui.js" ></script>
		<script type='text/javascript' src='js/pages/v2/project_mail_setting.js'></script>
		<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
		<style type="text/css">
			#admin_config_actions tbody tr.highlight td {
  				background-color: #ccc;
			}
		
		</style>
		
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<c:set var="roleInCurrentProject" value="${sessionScope.TJN_SESSION.user.roleInCurrentProject }"></c:set>
	<div class='title'>
		<p>Project E-mail Settings</p>
	</div>
	<fieldset>
		<div>
			<ul id='tabs'>
			<c:if test="${roleInCurrentProject eq 'Project Administrator'}">
				<li><a href='#project-mail-settings-tab' class='new' id='project-mail-settings-tab-anchor'>Mail Settings</a></li>
				<li><a href='#project-admin-config-tab' class='user' id='project-admin-config-tab-anchor'>Admin level Configuration</a></li>
			</c:if>	
			<li><a href='#project-user-config-tab' class='user' id='project-user-config-tab-anchor'>User level Configuration</a></li>
			</ul>
			<div id='project-mail-settings-tab' class='tab-section'>
				<form name='main_form' id='main_form' action='ProjectMailServlet' method='POST'>
				<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
					<div class='toolbar'>
						<input type='submit' id='btnSave' value='Save' class='imagebutton save' /> 
						<input type='hidden' id='projectid' name ='projectid' value='${projectMail.prjId}'/> 
						<input type='hidden' id='tjn-status' value='${screenState.status }' />
						<input type='hidden' id='seltestset' value="${escaltionRuleDescp['105'] }" />
						<input type='hidden' id='selaut' value="${escaltionRuleDescp['106'] }" />
						<input type='hidden' id='seltestset' value="${escaltionRuleDescp['105'] }" />
						<input type='hidden' id='selaut' value="${escaltionRuleDescp['106'] }" />
						<input type='hidden' id='selEscaltionRuleDescp' value="${escaltionRuleDescp}" />
						<input type='hidden' id='tjn-message' value='${screenState.message }' />
					</div>
					<div id='user-message'></div>
					<div class='form container_16'>
						<fieldset>
							<legend>Configuration Settings</legend>
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_3'>
									<label for='txtemail'>Enable E-mail Notifications</label>
								</div>
								<div class='grid_2' style="margin-left: 30px;">
									<input type="checkbox" id='txtemail' name='txtemail'
										value='${projectMail.prjMailNotification}'
										style='vertical-align: bottom; margin-top: 6px;' />
								</div>
							</div>
							<div id="txtSub">
								<div class='fieldSection grid_16'>
									<div class='clear'></div>
									<div class='grid_3'>
										<label for='txtmailsubject'>E-mail Subject</label>
									</div>
									<div class='grid_12'>
										<input type='text' id='txtmailsubject' name='txtmailsubject' value='${projectMail.prjMailSubject}' class='stdTextBox long'title='E-mail Subject' maxlength='256' />
									</div>
									<div class='clear'></div>
								</div>
								<div class='grid_13' style='margin-left: 20%; margin-top: 15px'>
									<div id='dataGrid'>
										<table id='txtDate' class='bordered' width='70%'>
											<thead>
												<tr>
													<th style='text-align: center' colspan='2'>Help</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><b> $domain_name$</b></td>
													<td>Name of the Domain</td>
												</tr>
												<tr>
													<td><b> $project_name$</b></td>
													<td>Name of the Project</td>
												</tr>
												<tr>
													<td><b> $run_id$</b></td>
													<td>Id of the Run</td>
												</tr>
												<tr>
													<td><b> $run_status$</b></td>
													<td>Status of the Run</td>
												</tr>
												<tr>
													<td><b> $user_name$</b></td>
													<td>Name of the User executed the test</td>
												</tr>
												<tr>
													<td><b> $test_set$</b></td>
													<td>Name of the Test Set</td>
												</tr>
												<tr>
													<td><b>Note</b></td>
													<td><b>1. </b>Use space to separate continuous
														keywords.<br> <b>2. </b>If using '$' or '_' before or
														after keywords, give a space.</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</fieldset>
							<!-- 	Added by lokanath for TENJINCG-1185 starts-->
				<fieldset>
			<legend>Additional Mail Settings</legend>
			<div class="fieldSection grid_8">
				<div class="grid_2">
					<label for="typeSelection">TypeSelection</label>
				</div>
				<div class="grid_5 alpha omega">
					<input type='hidden' id='selectedPriority' value='${projectMail.prjMailtype}'/>
								<select id="selectOperationType" name="selectOperationType" class="stdTextBox" style='width:132px;' onload="defaultselect()">
								<!-- 	Added by Jagadish for TENJINCG-1184 starts-->
									<option value='Mail To All'>Always Send Mail To All</option>
									<option value='Mail To Schedule Run'>Only Send To Schedule Run</option>
								<!-- 	Added by Jagadish for TENJINCG-1184 ends-->
								
								</select>
							</div>
				</div>
				<div class="fieldSection grid_7">
				</div>
				<div class="fieldSection grid_16">
				<div class="clear"></div>
				<div class="grid_2">
					<label for="addAddditionalMail">Add More Mails</label>
				</div>
				<div class="grid_13">
					<textarea id="addAddditionalMailIds" name="addAddditionalMailIds" class="stdTextBox" style="width: 89.5%; height: 100px;" maxlength="3000">${projectMail.prjAddAdditionalMails}</textarea>
				</div>
				</div>
			</fieldset>
			<!-- Added by lokanath for TENJINCG-1185 ends -->
						<fieldset>
							<legend>Escalation Rules</legend>
							<div class='fieldSection grid_7'>

								<div class='clear'></div>
								<div class='grid_3'>
									<label for='txtEscRule' style='font-size:14px;'>Enable Escalation Rules</label>
								</div>
								<div class='grid_2' style="margin-left: 30px;">
									<input type="checkbox" id='txtEscRule' name='txtEscRule'
										value='${projectMail.prjMailEscalation}'
										style='vertical-align: bottom; margin-top: 6px;' />
								</div>
							</div>
							<div class='clear'></div>
							<div id="txtEsc">
							<div class='fieldSection grid_16'>
								<div id='dataGrid'>
									<table id='emailEscalation-table' class='display tjn-default-data-table' style='width:100%;'>
										<thead>
											<tr>
												<th class='table-row-selector nosort'><input type='checkbox'  id='tbl-select-all-rows-email' class='tbl-select-all-rows' data-escalationRule='${EscalationRules}'/></th>
												<th>Escalation Rules</th>
												<th>Escalation Condition </th>
												<th>Escalation Mail-ids</th>
											</tr>
										</thead>
										<tbody>
											<c:set var = "cnt" value = '1'/>
											<c:forEach items="${EscalationRules }" var="EscalationRules">
												<c:set var = "index" scope = "session" value = '${fn:indexOf(EscalationRules.value, ",")}'/>
												<c:set var = "length" scope = "session" value = '${fn:length(EscalationRules.value)}'/>
												<c:set var = "i" scope = "session" value = '${fn:indexOf(projectMail.prjEscalationRuleDesc, "$")}'/>
												<c:set var = "j" scope = "session" value = '${fn:length(projectMail.prjEscalationRuleDesc)}'/>
												<tr>
													<td> 
														<input type="checkbox" value="${EscalationRules.key }" class="escalationRules${cnt}" id="${ EscalationRules.key}"> 
													</td>
													<td>
														<div>${fn:substring(EscalationRules.value, index+1,length)} </div>
													</td>
													<td>
														 
											
														<c:if test="${EscalationRules.key < '105' }">
															<input type="text" id="${ EscalationRules.key}${count}${count}" class="rulePercentage${cnt}" name="rulePercentage"  value='${escaltionRuleDescp[EscalationRules.key]}'  maxlength='3'>  
														</c:if>
														<c:if test="${ EscalationRules.key eq '105' }">
															<select id="${ EscalationRules.key}${count}${count}" class="rulePercentage${cnt}" name='rulePercentage' style='width:132px;'>
																<option value="">--Select One--</option>
																<c:forEach items="${testsets}" var="ts">
																	<option value="${ts.id}">${ts.name }</option>
																</c:forEach>
															</select>
														</c:if>
														<c:if test="${ EscalationRules.key eq '106' }">
															<select id="${ EscalationRules.key}${count}${count}" class="rulePercentage${cnt}" name='rulePercentage' style='width:132px;'>
																<option value="">--Select One--</option>
																<c:forEach items="${auts}" var="aut">
																	<option value="${aut.id}">${aut.name }</option>
																</c:forEach>
															</select>
														</c:if>
													</td>
													<td>
														<input type='text' id='escalationMailId' class="escalationMailId${cnt}" name='escalationMailId' value='${escaltionMailId[EscalationRules.key]}' class='stdTextBox' title='${escaltionMailId[EscalationRules.key]}' style='width:250px;' maxlength='2048'/>
													</td>
												</tr>
												<c:set var="count" value="${count +1}" scope="request"/>
												<c:set var="cnt" value="${cnt +1}"/>
											</c:forEach>
										</tbody>
									</table>
								</div>
							
				 
					 
								<div class='grid_12' style='padding-bottom:30px; color:blue'>
									<div class='clear'></div>
										* Separate Escalation Mail-id's by comma and make sure CC mail users are not there in project users.
									</div>
								</div>	
							</div>
						</fieldset>
					</div>
				</form>
			</div>
			<div id='project-admin-config-tab' class='tab-section '>
			<div class='toolbar'>
				<input type='button' value='Save' id='btnSaveAdminAction' class='imagebutton save' />
			</div>
			<div id='user-message-action'></div>
				<div class='form container_15' id='bodyContainer'>
					<fieldset>
		   				<legend>Project User action</legend>
		   				 <div class='form container_15' style="margin-left: 95px;" >
		    				 <div id='dataGrid' class='grid_6' style="margin-left: -50px;width: 350px;">
		                       <p style='margin-bottom:10px;'><b>Actions</b></p>  	
		                         	<table id='admin_config_actions' class='display' >
								<thead>
									<tr>
										 <th class='tiny'><input class='tbl-select-all-rows tiny' type='checkbox' id='check_all' title='Select All'></th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr id='Execution'>
										 <td class='table-row-selector tiny' ><input type='checkbox' id='Execution' /></td>
										<td>Execution</td>
									</tr>
									<tr id='DefectCreation'>
										 <td class='table-row-selector tiny' ><input type='checkbox' id='DefectCreation' /></td>
										<td>Defect creation</td>
									</tr>
									<tr id='ScheduleCreation'>
									  <td class='table-row-selector tiny' ><input type='checkbox' id='ScheduleCreation' /></td>
										<td>Schedule Task Creation</td>
									</tr>
								</tbody>
							</table>
						</div>	
						<div id='dataGrid_users' class='grid_7' style="width: 450px; padding-top: 27.5px;">
							<p style='margin-bottom:10px;'><b>Project Users</b></p>
		    				<table id='prj_users-Table'  class="display">
								<thead>
									<tr>
                         			  <th class='tiny'><input class='tbl-select-all-rows tiny' type='checkbox' id='check_all' title='Select All'></th>						
	                      	 		<th>User Id</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</fieldset>
			</div>
			</div>
			<div id='project-user-config-tab' class='tab-section'>
			<div class='toolbar'>
				<input type='button' value='Save' id='btnSaveUserAction' class='imagebutton save' />
			</div>
			<div id='user-message-user-action'></div>
				<div class='form container_15' id='bodyContainer'>
					<fieldset>
		   				<legend>Project User action</legend>
		   				 <div class='form container_15' style="margin-left: 95px;" >
		    				 <div id='dataGrid' class='grid_7' style="margin-left: -50px;width: 500px;">
		                         	<table id='user_config_actions' class='display' >
								<thead>
									<tr>
										<th class='tiny'><input class='tbl-select-all-rows tiny' type='checkbox' id='check_all' title='Select All'></th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr >
										<td class='table-row-selector tiny' ><input type='checkbox' id='TestPlanEdit' /></td>
										<td>Test Plan Update</td>
									</tr>
									<tr >
										<td class='table-row-selector tiny' ><input type='checkbox' id='TestPlanRemove'  /></td>
										<td>Test Plan Delete</td>
									</tr>
									<tr>
										<td class='table-row-selector tiny' ><input type='checkbox'  id='DefectCreation'/></td>
										<td>Defect Creation</td>
									</tr>
									<tr >
										<td class='table-row-selector tiny' ><input type='checkbox' id='DefectUpdate' /></td>
										<td>Defect Update</td>
									</tr>
									<tr >
										<td class='table-row-selector tiny' ><input type='checkbox' id='DefectDelete' /></td>
										<td>Defect Delete</td>
									</tr>
									<tr >
										<td class='table-row-selector tiny' ><input type='checkbox' id='ScheduleCreation'  /></td>
										<td>Schedule Task Creation</td>
									</tr>
									<tr >
										<td class='table-row-selector tiny' ><input type='checkbox' id='ScheduleTriggered' /></td>
										<td>Schedule Task Triggered</td>
									</tr>
									<tr >
										<td class='table-row-selector tiny' ><input type='checkbox' id='Reschedule' /></td>
										<td>Reschedule Task</td>
									</tr>
									<tr >
										<td class='table-row-selector tiny' ><input type='checkbox' id='CancelSchedule' /></td>
										<td>Cancel Scheduled Task</td>
									</tr>
									<tr>
										<td>#</td>
										<td>Execution&nbsp&nbsp&nbsp(user cannot optout)</td>
									</tr>
								</tbody>
							</table>
						</div>	
					</div>
				</fieldset>
			</div>
			</div>
		</div>
	</fieldset>
</body>
</html>
