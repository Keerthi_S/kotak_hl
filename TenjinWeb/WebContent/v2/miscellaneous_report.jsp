<!-- 

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  miscellaneous_report.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 *
 * DATE              	CHANGED BY              DESCRIPTION
 
 --> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Miscellaneous Report</title>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/jquery.dataTables.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/jquery-ui.css' />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/data-grids.js'></script>
	<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/v2/miscellaneous_report.js'></script>
	<script type="text/javascript" src='${pageContext.request.contextPath}/js/jquery-1.11.4-ui.js'></script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<div class='title'>
		<p> Miscellaneous Report</p>
	</div>
	<form name='main_form' action='TestReportServlet' method='POST'>
	<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<div class='toolbar'>
			<input type='button' value='Generate' id='btnGenerate' class='imagebutton download' />
			<img src='images/inprogress.gif' id='img_report_loader' />
		</div>
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${screenState.message }' />
		<div id='user-message'></div>
		<div class='form container_16'>
			<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'><label for='appRunReport'>Report For</label></div>
				<div class='grid_4'>
					<select class='stdTextBoxNew' id='appRunReport' name='appRunReport' mandatory='yes'>
						<option value='-1'>--Select One--</option>
						<option value='1'>Application Wise Execution Report</option>
						<option value='2'>Severity Wise Defects Report</option>
						<option value='3'>Scheduler Report</option>
					</select>
				</div>
			</div>
			<div class='fieldSection grid_7'>
				<div class='clear'></div>
				<div class='grid_2'><label for='reportType'>Report Type</label></div>
				<div class='grid_4'>
					<select class='stdTextBoxNew' id='reportType' name='reportType'>
						<option value='pdf'>PDF report</option>
						<option value='excel'>Excel report</option>
					</select>
				</div>
			</div>
			<div class='clear'></div>
			<fieldset>
				<legend>Report Criteria</legend>
				<div id='application_wise_report'>
					<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='application'>Application<span style='color:red'>&nbsp*</span></label></div>
					<div class='grid_4'>
						<tjn:applicationSelector mandatory='yes' title='Application' cssClass="stdTextBoxNew" groupSelector="existingGroup" loadFunctionGroups="true" id="aut" name="aut" defaultValue="${aut.id }" />
					</div>
					</div>
					<div class='clear'></div>
					<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'><label for='runDateFrom'>Date From</label></div>
					<div class='grid_4'>
						<input type='text' id='runDateFrom' name='runDateFrom' value='' class='stdTextBox2' 
						style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
						padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
					</div>
					</div>
				
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='runDateTo'>Date To</label></div>
						<div class='grid_4'>
							<input type='text' id='runDateTo' name='runDateTo' value='' class='stdTextBox2' 
							style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
							padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
						</div>
					</div>
					
					
				    <div class='clear'></div>
				</div>
				<div id='defect_wise_report'>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='defectTS'>Test Set</label></div>
						<div class='grid_4'>
							<select class='stdTextBoxNew' id='defectTS' name='defectTS' mandatory='yes'>
								<option value=''>--Select One--</option>
							</select>
						</div>
					</div><div class='clear'></div>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='defRunFrom'>Run From</label></div>
						<div class='grid_4'>
							<select class='stdTextBoxNew' id='defRunFrom' name='defRunFrom' mandatory='yes'>
								<option value=''>--Select One--</option>
							</select>
						</div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='defRunTo'>Run To</label></div>
						<div class='grid_4'>
							<select class='stdTextBoxNew' id='defRunTo' name='defRunTo' mandatory='yes'>
								<option value=''>--Select One--</option>
							</select>
						</div>
					</div>
				</div>
				<div id='scheduler_report'>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='taskType'>Task Type</label></div>
						<div class='grid_4'>
							<select class='stdTextBoxNew' id='taskType' name='taskType' mandatory='yes'>
								<option value='Execute'>Execute</option>
							</select>
						</div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='taskStatus'>Task Status</label></div>
						<div class='grid_4'>
							<select class='stdTextBoxNew' id='taskStatus' name='taskStatus' mandatory='yes'>
								<option value=''>--Select One--</option>
								<option value='Scheduled'>Scheduled</option>
								<option value='Executing'>Executing</option>
								<option value='Completed'>Completed</option>
								<option value='Cancelled'>Cancelled</option>
								<option value='Aborted'>Aborted</option>
							</select>
						</div>
					</div><div class='clear'></div>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='schUser'>Scheduled By</label></div>
						<div class='grid_4'>
							<select class='stdTextBoxNew' id='schUser' name='schUser' mandatory='yes'>
								<option value='All'>All</option>
							</select>
						</div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='schType'>Schedule type</label></div>
						<div class='grid_4'>
							<select class='stdTextBoxNew' id='schType' name='schType' mandatory='yes'>
								<option value='Recurrence'>Recurrence</option>
								<option value='Non Recurrence'>Non Recurrence</option>
							</select>
						</div>
					</div><div class='clear'></div>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='schDateFrom'>Date From</label></div>
						<div class='grid_4'>
							<input type='text' id='schDateFrom' name='schDateFrom' value='' class='stdTextBox2' 
							style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
							padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
						</div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='schDateTo'>Date To</label></div>
						<div class='grid_4'>
							<input type='text' id='schDateTo' name='schDateTo' value='' class='stdTextBox2' 
							style='text-transform: uppercase; border: 1px solid #ccc; border-radius: 3px; 
							padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 175px;' value='' mandatory='yes'/>
						</div>
					</div>
				</div>
			</fieldset>
			
		</div>	
	</form>
</body>
</html>