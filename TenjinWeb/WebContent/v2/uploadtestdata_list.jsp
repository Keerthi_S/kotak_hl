<!-- 
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:uploadtestdata_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
  -->
  <%@page import="com.ycs.tenjin.util.Utilities"%>
  <%@page import="java.util.Map"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<title>Application Credentials</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<link rel="stylesheet" href="css/jquery.dataTables.css">
		<script type='text/javascript' src='js/jquery-2.1.1.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/pages/v2/upload_view.js'></script>
		<script src="js/jquery.dataTables.js"></script>
		<script src="js/tables.js"></script>
		
		<script type='text/javascript' src='js/pages/uploadtestdata_list.js'></script>
		<script >


			$(document).on('click','#btnNewFile', function() {
				window.location.href ='TestDataUploadServlet?param=new';
			});
			
			$(document).on('click','#btnImportFile', function() {
				window.location.href ='TestDataUploadServlet?param=import';
			});
			
			$(document).on("click", "#btnDelete", function() {
				if($('#applicationUserTable').find('input[type="checkbox"]:checked').length === 0 || ($('#applicationUserTable').find('input[type="checkbox"]:checked').length == 1 && $('#applicationUserTable').find('input[type="checkbox"]:checked').hasClass('tbl-select-all-rows'))) {
					showLocalizedMessage('no.records.selected', '', 'error');
					return false;
				}
				
				clearMessages();
				
				if(confirm('Are you sure you want to delete the selected record(s)? Please note that this action cannot be undone.')) {
					var $applicationUserForm = $("<form action='ApplicationUserServlet' method='POST' />");
					$applicationUserForm.append($("<input type='hidden' id='del' name='del' value='true' />"))
					$applicationUserForm.append($("<input type = 'hidden' id='csrftoken_form' name ='csrftoken_form' value='<c:out value='${csrftoken}'/>'/>"))
					if($('#applicationUserTable .tbl-select-all-rows').is(':checked')){
					$('#applicationUserTable').find('input[type="checkbox"]:checked').each(function() {
						if(!$(this).hasClass('tbl-select-all-rows')) {
							$applicationUserForm.append($("<input type='hidden' name='selectedApplicationUser' value='"+ this.id +"' />"));
							
						}
					});
					}else{
						for (var i in selectedItems) {
							$applicationUserForm.append($("<input type='hidden' name='selectedApplicationUser' value='"+ selectedItems[i] +"' />"));
						}
					}
					$('body').append($applicationUserForm);
					$applicationUserForm.submit();
				}
			});
		</script>
	</head>
	<body>
		
			<div class='title'>
				<p>Test Data</p>
			</div>
			<div class='toolbar'>
				<input type='button' value='Upload' id='btnNewFile'class='imagebutton new' /> 
				<!-- <input type='button' value='Remove'id='btnDelete' class='imagebutton delete' /> -->
				<input type='button' value='Copy From Another User' id='btnImportFile' class='imagebutton copy1' />
			</div>
			<div id='user-message'></div>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
			<div id='user-message'></div>
			<div class='form container_16'>
				<div id='dataGrid'>
					<table id='applicationUserTable' class='display tjn-default-data-table'>
						<thead>
							<tr>
								<th class='table-row-selector nosort'><input type='checkbox' class='tbl-select-all-rows'></th>
								<th>Application</th>
								<th>Function / API </th>
								<th>Test Data Template</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${testDataList}" var="testdatalist">
						<%-- <input type='hidden' id='uacId' name='uacId' value='${fn:escapeXml(testdatalist.projectId)}'/> --%>
						<tr>
						<td><input type='checkbox' class='checkbox chkBox' id='${fn:escapeXml(testdatalist.projectId) }' data-app='${fn:escapeXml(testdatalist.appId)}' data-funcCode='${fn:escapeXml(testdatalist.function)}' value='${fn:escapeXml(testdatalist.projectId) }' />
						<td>${fn:escapeXml(testdatalist.appName)}</td>
						<td>${fn:escapeXml(testdatalist.function)}</td>
						<td><a href='#' class='template-gen' id="download" data-app='${fn:escapeXml(testdatalist.appId)}' data-func='${fn:escapeXml(testdatalist.function)}' data-fname='${fn:escapeXml(testdatalist.fileName)}' >${fn:escapeXml(testdatalist.fileName)}</a></td>
						</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
				<div class='modalmask'></div>
	           <div class='subframe'>
	 
		     <iframe frameborder="2" scrolling="no" marginwidth="5" marginheight="5"
			style='height: 380px; width: 770px; overflow: hidden;'
			seamless="seamless" id='tst-sframe' src=''></iframe>
		    <iframe frameborder="2" scrolling="no" marginwidth="5"
				marginheight="5" style='height: 350px; width: 600px; overflow: hidden;'
				seamless="seamless" id='history-sframe' src=''>
		</iframe>
		
	</div>
			</div>

</body>

</html>