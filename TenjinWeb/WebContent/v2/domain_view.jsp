<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  domain_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 
-->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>



<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Domain Details</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<link rel='stylesheet' href='css/paginated-table.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script src="js/formvalidator.js"></script>
		<script  type='text/javascript' src="js/tables.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		
		<script>
		$(document).ready(function() {
			checkForTreeChange();
			$('#btnNewDomain').click(function(){
				var domainName=$('#txtDomainName').val();
				window.location.href='DomainServletNew?param=domain_new&name='+domainName;
			});
			
			$('#btnNewProject').click(function(){
				var domainName=$('#txtDomainName').val();
				var projectType=$('#txtProjType').val();
				window.location.href='ProjectServletNew?t=project_new&name='+domainName+'&projectType='+projectType;
			});
			
			
		});
		function checkForTreeChange() {
			var refreshTree = $('#h-refreshTree').val();
			if(refreshTree || refreshTree === 'true') {
				window.parent.refreshDomainsTree();
				window.setTimeout(function() {
					window.parent.selectDomainInTree($('#txtDomainName').val());
				}, 1000);
			}
		}
		</script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		
		<div class='title'>
			<p>Domain Details</p>
		</div>
		<form name='main_form' action='DomainServletNew' method='POST'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<input type='hidden' id='viewType' value='NEW'/>
			<c:if test="${current_user.roles  == 'Site Administrator'}">
					<div class='toolbar'>
						<input type='button' value='New Domain' id='btnNewDomain' class='imagebutton newdomain'/>
						<input type='button' value='New Project' id='btnNewProject' class='imagebutton newproject'/>
					</div>	                      	 	 	
			</c:if>	
			
			
			<input type='hidden' id='callback' value='${callback }' />
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<input type='hidden' id='h-refreshTree' value='${refresh_tree }' />
			<div id='user-message'></div>
			
			<div class='emptySmall'>&nbsp</div>
			<div class='form container_16'> 
			 	<fieldset style='margin-top:-10px;' >
					<legend>Basic Information</legend>
					
					 <div class='fieldSection grid_7'> 
						<div class='clear'></div>
						<div class='grid_2'><label for='txtDomainName'>Name</label></div>
						<div class='grid_4'>
							<input type='text' class='stdTextBox' value='${fn:escapeXml(domain.name )}' id='txtDomainName' name='txtDomainName' title='Domain Name' mandatory='yes' disabled='disabled'/>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'><label for='txtCreatedBy'>Created By</label></div>
						<div class='grid_4'>
							<input type='text' class='stdTextBox' value='${fn:escapeXml(domain.owner )}' id='txtCreatedBy' name='txtCreatedBy' title='Created By' mandatory='yes' disabled='disabled'/>
						</div>
					</div> 
					
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='txtCreatedOn'>Created On</label></div>
						<div class='grid_4'>
						    <input type='text' class='stdTextBox' value='${domain.createdDate }' id='txtCreatedOn' name='txtCreatedOn' title='Created On' mandatory='yes' disabled='disabled'/>
						</div>
						
						<div class='clear'></div>						
						<div class='grid_4'>
							<input type='hidden' class='stdTextBox' value='${PROJTYPE }' id='txtProjType' name='txtProjType' title='Project type' mandatory='yes' disabled='disabled'/>
						</div>
						
					</div>
				</fieldset>
				
				<fieldset>
					<legend>Projects</legend>
					<div id='fieldSection' style='width:100%'>
						
					</div>
					
					<div id='dataGrid'>
						<table id='domainProjects-table'  class='display tjn-default-data-table' >
							<thead>
								<tr>
									<th>Project Name</th>
									<th>Type</th>
									<th>Owner</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${projects }" var="project">
								<tr>
									<td><a id='${ project.id}' class='d_project_item_trigger' href='ProjectServletNew?t=load_project&paramval=${ project.id}&view_type=ADMIN' >${fn:escapeXml(project.name)}</a></td>
									<td>${project.type }
									<td>${fn:escapeXml(project.owner )}</td>
									<c:choose>
									<c:when test="${project.state eq 'A'}">
									<td><b><span style="color:green;">Active</span></b></td>
									</c:when>
									<c:otherwise>
									<td><b><span style="color:red;">Inactive</span></b></td>
									</c:otherwise>
									</c:choose>
								</tr>	
								</c:forEach>	
							</tbody>
						</table>
					</div>
				</fieldset>
				
			
			
		</div>
		
		</form>
		
		<div class='modalmask'></div>
		<div class='subframe'>
			<iframe src='' id='subFrame' scrolling="no" seamless="seamless"></iframe>
		</div>
	</body>
</html>