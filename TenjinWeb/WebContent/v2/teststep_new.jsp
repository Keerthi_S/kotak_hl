<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  teststep_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 * 15-06-2021			Ashiki					TENJINCG-1275

-->
 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>  
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html>
<html>
	<head>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/formvalidator.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/tenjin-aut-selector.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/v2/teststep_new.js'></script>
		
	</head>
	<body>
	<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
	<%
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			Project project = null;
			project = tjnSession.getProject();
		%>
		<div class='title'>
			<p>New Step</p>
			 
		</div>
		<form action='TestStepServlet' id='teststepform' name='teststepform' method='POST' >
			<div class="toolbar">
			    <!-- added by shruthi for CSRF token starts -->
		       <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		        <!-- added by shruthi for CSRF token ends -->
				<input type='submit' id='btnSave' value='Save' class='imagebutton save' />
				<input type='button' id='btnCancel' value='Cancel' class='imagebutton cancel' />
			</div>
			
			<input type='hidden' id='tcRecId' name='tcRecId' value='${tcRecId }'>
			<input type='hidden' id='presetRule' name='presetRule' value='${ presetRule}'>
			<input type='hidden' id='totalSteps' name='totalSteps' value='${totalSteps }'>
			<input type='hidden' id='aut1' name='aut1' value='${aut }'>
			<input type='hidden' id='tcId1' name='tcId1' value='${tcId }'>
			
			<input type='hidden' id='prjId' name='prjId' value='<%=project.getId() %>'>
			<input type='hidden' id='txnTcRecTd' name='txnTcRecTd' value='${teststep.testCaseRecordId }'>
			<input type='hidden' id='appId' name='appId' value='${teststep.appId}'>
			<input type='hidden' id='apiCode' name='apiCode' value=''>
			<input type='hidden' id='txnMode' name='txnMode' value='${teststep.txnMode }'>
			<input type='hidden' id='txtRecordId' name='txtRecordId' value='${teststep.recordId }'>
			<input type='hidden' id='stepCopy' name='stepCopy' value='${ stepCopy }'>
			<input type='hidden' id='selectedValType' name='selectedValType' value='${ teststep.validationType }'>
			<input type='hidden' id='sourceTcRecTd' name='sourceTcRecTd' value='${teststep.testCaseRecordId }'>
			<input type='hidden' id='childEntityCopy' name='childEntityCopy' value='true'>
			<input type='hidden' id='funCode' name='funCode' value='${teststep.moduleCode}'>
			<input type='hidden' id='sourceStepId' name='sourceStepId' value='${sourceStepId}'>
			<input type="hidden" id="rerunStep"  value='${teststep.rerunStep}'/>
			
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<div id='user-message'></div>
			
			<div class='form container_16'>
			<fieldset>
			<legend>Test Step Details</legend>
				<div class='fieldSection grid_16' id='tcId'>
					<div class='grid_2'><label for='txtStepId'>Test Case</label></div> 
					<div class='grid_4'>
						<select  id='lstTestcase' name='lstTestcase' class='stdTextBoxNew' >
							 <c:forEach items="${testCases}" var="testCase">
									<option value="${testCase.tcRecId}" >${testCase.tcId}</option>
                             </c:forEach>
						</select>
					</div>
					</div>
				<div class='fieldSection grid_8'>
					<div class='grid_2'><label for='txtStepId'>Step ID</label></div>
					 
					<c:set var="tcid" value="TS_${tcId }_ST_${totalSteps+1 }" />
					<div class='grid_4'><input type='text' name='id' class='stdTextBox' id='txtStepId' placeholder='${tcId eq null ? teststep.id : tcid }' mandatory='yes' title='Step ID' maxlength="30"/></div>
					<input type='hidden' id='defaultstepid' value='${tcId eq null ? teststep.id : tcid }'  />
					<input type='hidden' id='teststepid' value='${teststep.id}'  />
				</div>
				
				<div class='fieldSection grid_8'>
					<div class='grid_2'><label for='lstValType'>Validation Type</label></div>
					<div class='grid_4'>
						<select  id='lstValType' name='validationType' class='stdTextBoxNew' >
							<option value='Functional'>Functional</option>
							<option value='UI'>UI</option>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_16'>
					 
					<div class='grid_2'><label for='txtStepdesc'>Summary</label></div>
					<!-- Modified by Priyanka for TENJINCG-1233 starts -->
					<div class='grid_13'><input type='text' id='txtStepdesc' name='shortDescription' value='${teststep.shortDescription}' class='stdTextBox long'  mandatory='yes' title='Short Description' maxlength="100" placeholder="MaxLength 100" /></div>
				   <!-- Modified by Priyanka for TENJINCG-1233 ends -->
				</div>
				</fieldset>
				</div>
				<div class='form container_16'>
				<fieldset>
					<legend>Basic Information</legend>
					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div class='grid_2'><label for='aut'>Application</label></div>
						<div class='grid_4'>
						<c:choose>
						<c:when test="${teststep.txnMode eq 'GUI'}">
						<tjn:applicationSelector mandatory='yes' title='Application' apiSelector ="api" functionSelector="function" operationSelector="operation"  cssClass="stdTextBoxNew" projectId='<%=project.getId() %>' loadFunctions='true' loadAutLoginTypes='true' autLoginTypeSelector='autLogintypes' loadOperations='true' id="aut" name="aut" defaultValue="${teststep.appId}"/>						
					   </c:when>
					   
					   <c:when test="${teststep.txnMode eq 'MSG'}">
						<tjn:applicationSelector mandatory='yes' title='Application'  cssClass="stdTextBoxNew" projectId='<%=project.getId() %>' loadMsg='true'  id="aut" name="aut" defaultValue="${teststep.appId}"/>						
					   </c:when>
					   <c:otherwise><tjn:applicationSelector mandatory='yes' title='Application' apiSelector ="api" functionSelector="function" cssClass="stdTextBoxNew" projectId='<%=project.getId() %>' loadApis='true' loadAutLoginTypes='true' autLoginTypeSelector='autLogintypes'  id="aut" name="aut" defaultValue="${teststep.appId}"/>  </c:otherwise>
					   </c:choose>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'><label  for='function' id="module_label">Function</label></div>
						<div class='grid_4'>
						<c:choose>
						<c:when test="${teststep.txnMode eq 'GUI'}">
							<tjn:functionSelector mandatory='yes' title='Function' applicationId="${teststep.appId}" cssClass="stdTextBoxNew" id='function' name="moduleCode" defaultValue="${teststep.moduleCode }"/>
						</c:when>
						<c:when test="${teststep.txnMode eq 'MSG'}">
							<tjn:msgFormatSelector title='Format' applicationId="${teststep.appId}" cssClass="stdTextBoxNew" id='msgFormat' name="msgFormat" defaultValue="${teststep.actualResult}"/>					
							</c:when>
						<c:otherwise>
						<tjn:apiSelector mandatory='yes' title='API' applicationId="${teststep.appId}" cssClass="stdTextBoxNew" id='api' name="moduleCode" apiOperationSelector="apiOperation" loadApiOperations='true' defaultValue="${teststep.moduleCode}"/>
						</c:otherwise>
						</c:choose>
						</div>
						
						<div class='clear'></div>
						<div class='grid_2'><label for='operation' id='operationId'>Operation</label></div>
						<div class='grid_4'> 
						
						<c:choose>
						<c:when test="${teststep.txnMode eq 'GUI'}">
							<tjn:operationSelector mandatory='yes' title='Operation' applicationId="${teststep.appId}" cssClass="stdTextBoxNew" id='operation' name="operation" defaultValue="${teststep.operation }"/>
						</c:when>
						<c:when test="${teststep.txnMode eq 'MSG'}">
							<tjn:msgSelector mandatory='yes' title='Message' applicationId="${teststep.appId}" cssClass="stdTextBoxNew" id='Msg' name="moduleCode" defaultValue="${teststep.moduleCode }"/>
						</c:when>
						<c:otherwise>
						<tjn:apiOperationSelector mandatory='yes' title='Operation' applicationId="${teststep.appId}" apiCode="${teststep.moduleCode}" loadResponseType='true' responseTypeSelector='lstResponseType' cssClass="stdTextBoxNew" id='apiOperation' name="operation" defaultValue="${teststep.operation }"/>
						</c:otherwise>
						</c:choose>
						
						
						</div>
						<c:choose>
						<c:when test="${teststep.txnMode ne 'MSG'}">
						<div class='clear'></div>
						<div class='grid_2'><label for='autLogintypes'>AUT Login Type</label></div>
						<div class='grid_4'>
						
						<tjn:autLogintypeSelector mandatory='yes' title='AUT Login Type' applicationId="${teststep.appId}" cssClass="stdTextBoxNew" id='autLogintypes' name="autLoginType" defaultValue="${teststep.autLoginType }"/>
						
						</div>
						 </c:when>
						 </c:choose>
						
						<c:choose>
							<c:when test="${teststep.txnMode eq 'MSG'}">
								<div class='clear'></div>
								<div class='grid_2'><label for='fileName'>File Name</label></div>
								<div class='grid_4'><input type='text' id='fileName' name='fileName' <%-- value='${fn:escapeXml(teststep.fileName)}'  --%>class='stdTextBoxNew' mandatory='yes' title='File Name' maxlength="40"/></div>
							</c:when>
						</c:choose>
						
						<c:if test="${teststep.txnMode eq 'API'}">
						<div class='clear'></div>
						<div class='grid_2'><label for='lstResponseType' id='ResponseType'>Response Type</label></div>
						<div class='grid_4'><%-- <select id='lstResponseType' name='responseType' value='${teststep.responseType} ' class='stdTextBoxNew'></select> --%>
						
						<tjn:responseTypeSelector title='Response Type' applicationId="${teststep.appId}" apiCode="${teststep.moduleCode}" operation="${teststep.operation}" cssClass="stdTextBoxNew" id='lstResponseType' name="responseType" defaultValue="${teststep.responseType}"/>
						
						</div>
						</c:if>
					</div>
					
					<div class='fieldSection grid_8'>
					<c:choose>
						<c:when test="${teststep.txnMode ne 'MSG'}">
						<div class='clear'></div>
						<div class='grid_3'><label for='lstTestType'>Type</label></div>
						<div class='grid_4'>
						<input type='hidden' id='txtTsType' name='txtTsType' value='${teststep.type }'>
							<select id='lstTestType' name='type' class='stdTextBoxNew'>
								<!-- Modified by Prem for Tenj210-125 start -->
								<option value='Positive'>Positive</option>
							    <option value='Negative'>Negative</option>
							<!-- Modified by Prem for Tenj210-125 end -->
								
							</select>
						</div>
						</c:when>
						</c:choose>
						<div class='clear'></div>
						<div class='grid_3'><label for='txtDataId'>Test Data ID</label></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<div class='grid_4'><input type='text' id='txtDataId' name='dataId' value='${teststep.dataId}' class='stdTextBoxNew' mandatory='yes' title='Test Data ID' maxlength="30" placeholder="MaxLength 30"/></div>
						 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						<div class='clear'></div>
			 
						
						<div class='grid_3'><label for='lstTestMode'>Mode</label></div>
						<c:choose>
						<c:when test="${teststep.txnMode eq 'API'}">
						<div class='grid_4'>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<select id='txnMode' name='type' class='stdTextBoxNew lstTestMode' maxlength="8">
								 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
								<option value='GUI' >GUI</option>
								<option value='API' selected>API</option>
								<option value='MSG' >MSG</option>
							</select>
						</div>
						</c:when>
						<c:when test="${teststep.txnMode eq 'MSG'}">
						<div class='grid_4'>
							<select id='txnMode' name='type' class='stdTextBoxNew lstTestMode' maxlength="8">
								<option value='GUI' >GUI</option>
								<option value='API' >API</option>
								<option value='MSG' selected>MSG</option>
							</select>
						</div>
						</c:when>
						<c:otherwise>
						
						<div class='grid_4'>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<select id='txnMode' name='type' class='stdTextBoxNew lstTestMode' maxlength="8">
								 <!-- Modified by Priyanka for TENJINCG-1233 ends -->
								<option value='GUI' selected>GUI</option>
								<option value='API' >API</option>
								<option value='MSG' >MSG</option>
							</select>
						</div>
						</c:otherwise>
						</c:choose>
						<div class='clear'></div>
						<div class='grid_3'><label for='txtNoOfRows'>Rows To Execute</label></div>
						<div class='grid_4'><input type='text' id='txtNoOfRows' name='rowsToExecute'  title='Numer of Rows' class='stdTextBoxNew' value ='${teststep.rowsToExecute eq 0?"1":teststep.rowsToExecute }' onkeypress="return isNumberKey(event)" maxlength="4" />
					</div>
				<c:choose>
							<c:when test="${teststep.txnMode eq 'MSG'}">
								<div class='clear'></div>
								<div class='grid_3'><label for='filePath'>File Path</label></div>
								<div class='grid_4'><input type='text' id='filePath' name='filePath' <%-- value='${fn:escapeXml(teststep.filePath)}' --%> class='stdTextBoxNew' mandatory='yes' title='File Path' maxlength="60"/></div>
							</c:when>
						</c:choose>
						</div>
				</fieldset>
				<fieldset>
					<legend>Dependency Rules</legend>
					<input type="hidden" id='selectedRules'  name='dependencyRules' value='${teststep.customRules}'>
					<input type="hidden" id='totalSteps'  value='${totalSteps}'>
					<div class='fieldSection grid_8'>
					 
						<div class='grid_2'><label for='dependencyRules'>Rules</label></div>
				   	 	<div class='grid_4'>
							<select class='stdTextBoxNew' id='dependencyRules' name='dependencyRules' disabled="disabled">
							   <option value='-1'>Always Execute</option>
							   <option value='Pass'>Execute if previous step Pass</option>
							   <option value='Fail'>Execute if previous step Fail</option>
							   <option value='Error'>Execute if previous step Error</option>
							</select>
						</div>
						</div>
					<div class='fieldSection grid_7'>
						<div class='grid_3'><label for='lstAutLoginType'> Always Show for Re-Run</label></div>
						<div class='grid_2'><input type="checkbox" id='showForRerun' name='rerunStep' value ='' style='vertical-align: bottom;margin-top:6px;'/></div>
					</div>
				</fieldset>
				 
				<fieldset>
					<legend>Additional Information</legend>
					<div class='fieldSection grid_16'>
						<div class='clear'></div>
						<div class='grid_2'><label for='txtStepDescription'>Description</label></div>
						<div class='grid_13'><textarea id='txtStepDescription' name='description'  class='stdTextBox long' style='height:72px; margin: 0px; width: 89%;' >${teststep.description}</textarea></div>
						
						<div class='clear'></div>
						<div class='grid_2'><label for='txtExpResult'>Expected Result</label></div>
						<div class='grid_13'><textarea id='txtExpResult' name='expectedResult' class='stdTextBox long' style='height:72px; margin: 0px; width: 89%;'>${teststep.expectedResult}</textarea></div>
					</div>
				</fieldset>
			</div>
		</form>

</body>

</html>
