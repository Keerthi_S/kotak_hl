<!-- 
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  oauth_details.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 ADDED BY              DESCRIPTION
  -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
    <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>OAuth 2.0 Details</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='js/formvalidator.js'></script>
		<script src="js/pages/v2/oauth_details.js"></script>
</head>
<body>
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
	<div class='title'>
		<p>OAuth 2.0 Details</p>
	</div>
	<form id='oauthform' name='oauthform' action='ApplicationServlet' method='POST'>
	<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		<div class='toolbar'>
			<input type='button' id='btnBack' value='Back' class='imagebutton back' />
		    <input type='submit' id='btnSave' value='Save' class='imagebutton save' />
			<input type='hidden' value='${appId}' id='appId' name='appId'>
			<input type='hidden' id='selectedAuthData' name='selectedAuthData' value='${oauth.addAuthDataTo}'>
			<input type='hidden' id='selectedGrantType' name='selectedGrantType' value='${oauth.grantType}'>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' value='saveoAuthDetails' id='del' name='del'>
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
		</div>
		<div id='user-message'></div>
		<div class='form container_16'>
			<fieldset>
				<legend>Basic Information</legend>
				<div class='fieldSection grid_8'>
					<div class='clear'></div>
					<div class='grid_3'><label for='txtAutId'>Application ID</label></div>
					<div class='grid_4'><input type='text' id="txtAutId" name="txtAutId" value="${appId}"  class='stdTextBox' disabled/></div>
				</div>
				<div class='fieldSection grid_8'>
					<div class='clear'></div>
					<div class='grid_2'><label for='grantType'>Grant Type</label></div>
					<div class='grid_4'>
						<select id='grantType' name='grantType' value='${oauth.grantType}' class='stdTextBoxNew'  title='Grant Type' onchange='checkGrantType(this.value);'>
							<option value='authorization_code'>Authorization Code</option>
							<option value='implicit'>Implicit</option>
							<option value='password'>Password Credentials</option>
							<option value='client_credentials'>Client Credentials</option>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_16'>
					<div class='clear'></div>
					<div class='grid_3'><label for='clientId'>Client ID</label></div>
					<div class='grid_12'><input type='text' id="clientId" name="clientId" value="${oauth.clientId}"  class='stdTextBox medium' mandatory='yes'/></div>
				</div>
				<div class='fieldSection grid_16'>
					<div class='clear'></div>
					<div class='grid_3'><label for='clientSecret'>Client Secret</label></div>
					<div class='grid_12'><input type='text' id="clientSecret" name="clientSecret" value="${oauth.clientSecret}"  class='stdTextBox medium' mandatory='yes'/></div>
				</div>
				<div class='fieldSection grid_16'>
					<div class='clear'></div>
					<div class='grid_3'><label for='callBackUrl'>Callback URL</label></div>
					<div class='grid_12'><input type='text' id="callBackUrl" name="callBackUrl" value="${oauth.callBackUrl}"  class='stdTextBox long' mandatory='yes'/></div>
				</div>
				<div class='fieldSection grid_16'>
					<div class='clear'></div>
					<div class='grid_3'><label for='authUrl'>Auth URL</label></div>
					<div class='grid_12'><input type='text' id="authUrl" name="authUrl" value="${oauth.authUrl}"  class='stdTextBox long' mandatory='yes'/></div>
				</div>
				<div class='fieldSection grid_16' >
					<div class='clear'></div>
					<div class='grid_3' ><label for='accessToken'>Access Token URL</label></div>
					<div class='grid_12'><input type='text' id="accessToken" name="accessToken" value="${oauth.accessTokenUrl}"  class='stdTextBox long' mandatory='yes'/></div>
				</div>
				<div class='fieldSection grid_8'>
					<div class='clear'></div>
					<div class='grid_3'><label for='authData'>Add Auth Data to</label></div>
					<div class='grid_4'>
						<select id='authData' name='authData' value='${oauth.addAuthDataTo}' class='stdTextBoxNew'  title='Add Auth Data to'>
							<option value='URL'>Request URL</option>
							<option value='Header'>Request Headers</option>
						</select>
					</div>
				</div>
			</fieldset>
		</div>
	</form>
</body>
</html>