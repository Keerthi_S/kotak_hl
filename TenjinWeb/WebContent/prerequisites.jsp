<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  prerequisites.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->
<%--New prerequisites.jsp file Created By Leelaprasad For The requirment Defect #823 on 10-11-2016 --%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--
/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
  
 **/
 -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>PREREQUISITES - Tenjin Intelligent Enterprise Testing Engine</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/borde1red.css'/>
		<link rel='stylesheet' href='css/style.css'/>
		<link rel="SHORTCUT ICON" HREF="images/yethi.png">
		<style type="text/css">
		.ptext:before{
		content: "\261B";
		font-size:20px;
		
		}
		.ptext{
		font-family:'Verdana',sans-serif;
		font-weight: bold;
		}
		.font{
		font-family:Italic;
		color: #6A5ACD;
		font-size:15px;
		font-weight: bold;
		margin-top:30px;
		
		
		}
		.headerpage {
		 background: #f8f8f8;
   		 border-top: 1px solid #ececec;
   		 border-bottom: 1px solid #ececec;
   		 padding: 10px 10px;
    
		color: blue;
		border:1px solid #5F9EA0;
		font-weight: bold;
		font-size: 15px;
		margin-top:30px;
		}
		
		</style>
		
</head>
<body>

     <%
        TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
         if(tjnSession!=null){
     %>


		<div class='title'>
		<p>PREREQUISITES</p>	
		</div>
		<div class='form container_16'>
				<fieldset style='margin-top:10px'>
					<legend>Prerequisites</legend>
			<table class='border' width='50%' style='margin-left: 25px;'>
			<thead>
								<tr >
								<th class='headerpage'>Attention Tenjin Users:  Kindly check the points before execution.</th>
									
								</tr>
							</thead>
			
			<tbody>
			
						<tr><td><p class='ptext'>  Ensure User profile is linked to the Application Under Test (AUT).</p></td></tr>
							<tr><td><p class='ptext'>   Define the functions required for the AUT.</p></td></tr>
							<tr><td><p class='ptext'>   Ensure Test data path is defined correctly.</p></td></tr>
							<tr><td><p class='ptext'>   Ensure Test Case and Test Set is defined appropriately.</p></td></tr>
							<tr><td><p class='ptext'>   Ensure Test Case ID defined in Tenjin is same as the required Test Data Set.</p></td></tr>
							
					</tbody>	</table>
					
					<table width='100%' style='margin-left: 33px;'>
			<thead>
			
			</thead>
			
			<tbody>  
			<tr><td><p class='font'>Contact Tenjin support in case of issues even after the above points are taken care of.</p></tr>
			</tbody>
			</table>
					
					</fieldset>
					</div>	
					
	<%}else{
		response.sendRedirect("noSession.jsp");
	}
	%>
</body>
</html>