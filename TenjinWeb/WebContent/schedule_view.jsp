<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!-- Added by sahana for Req#TJN_24_03 on 14/06/2016 -->
<%@page import="com.ycs.tenjin.project.TestSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.scheduler.Scheduler"%>
<%@page import="com.ycs.tenjin.scheduler.SchMapping"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.client.RegisteredClient"%>
<%@page import="java.util.Map"%>
<%@ page
	import="java.util.Date,java.text.SimpleDateFormat,java.text.ParseException"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@ page import="java.util.Random"%>
<%@page import="com.ycs.tenjin.scheduler.SchMapping"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	  <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 
*/

-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel="Stylesheet" href="css/bordered.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src="js/jquery-1.12.4.js"></script>
<script type='text/javascript' src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/formvalidator.js'></script>
<!-- <script type='text/javascript' src='js/pages/autfunclist_table.js'></script> -->
<script type='text/javascript' src='js/pages/schedule_view.js'></script>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<script type="text/javascript">
  $(document).ready(function(){

	<%TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
	User cUser = tjnSession.getUser();
	Project project = null;
	project = tjnSession.getProject();
	Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");

	String scrStatus = "";
	String projState=project.getState();
	String status = (String)map.get("STATUS");
	String message = (String)map.get("MESSAGE");
	Scheduler sch = (Scheduler)map.get("SCH_BEAN");
	String recType=(String)map.get("RECTYPE");
	int testSet=(Integer)map.get("ID");
	int tcId=(Integer)map.get("TC_ID");
	
	 ArrayList<SchMapping> scheduleList = (ArrayList<SchMapping>) map.get("SCH_Map_List");
	 String tabType=(String)map.get("TABTYPE");
	
	ArrayList<RegisteredClient> clientList = null;
	clientList = (ArrayList<RegisteredClient>) map.get("CLIENT_LIST");%>
	
	
	document.getElementById("txtclientregistered").value = "<%=sch.getReg_client()%>";
					});
</script>

<title>Schedule Details</title>
</head>
<body>
<%
Cache<String, Boolean> csrfTokenCache = null;
csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
session.setAttribute("csrfTokenCache", csrfTokenCache);
UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
session.setAttribute ("csrftoken_session", csrftoken);
%>
<input type='hidden' id='projState' value='<%=project.getState()%>' />
	<div class='main_content'>
		<div class='title'>

			<p>Schedule Task</p>
		 
		</div>
		<form name='edit_schedule' action='SchedulerServlet' method='get'>
	<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
				<input type='button' value='Back' id='btnBack'
					class='imagebutton back' /> <input type='button' value='Save'
					id='btnSave' class='imagebutton save' />

				<div class='grid_2' style='float: right; font-size: 0.7em;'>
					<label id='date'></label>
				</div>
			</div>


			<div id='user-message'></div>

			<div class='form container_16'>
				<fieldset>

					<legend>Schedule Details</legend>
					<div class='fieldSection grid_16'
						style='margin-top: 5px; height: 28px'>
						<div class='grid_2'>
							<label for='taskName'>Task Name</label>
						</div>
						<div class='grid_13'>
							<%
								if (sch.getTaskName() == null) {
									sch.setTaskName("");
								}
							%>
							<input type='text' id="taskName" class='stdTextBox long'
								maxlength="40" value='<%=sch.getTaskName()%>' mandatory='yes'
								title='Task Name' style='width: 81.7%' />

						</div>
					</div>
					<div class='fieldSection grid_7'>
						<input type='hidden' value='<%=sch.getSchedule_Id()%>'
							id='btnSchId' name='btnSchId' />
						<%
							if (tabType != null) {
						%>
						<input type='hidden' value='<%=tabType%>' id='tabselectedType'
							name='tabselectedType' />
						<%
							}
						%>

						<%
							if (project != null) {
						%>
						<input type='hidden' value='<%=project.getId()%>' id='projId'
							name='projId' />
						<%
							}
						%>
						<div class='grid_2'>
							<label for='datepicker'>Date</label>
						</div>
						<div class='grid_4'>
							 
								<input type='text' id="datepicker"
								style='border: 1px solid #ccc; border-radius: 3px; padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 177px;'
								value='<%=sch.getSch_date()%>' size="25" mandatory='yes'
								title='Schedule Date' />
						</div>

						<div class='clear'></div>
						<div class='grid_2'>
							<label for='btntime'>Time</label>
						</div>
						<div class='grid_4'>
							<select id='btntime' name='btntime' class='stdTextBox'
								style='min-width: 60px; width:60px'>
								<%
									String[] timeval = sch.getSch_time().split(":");
								%>
								<option value='<%=timeval[0]%>' selected='selected'><%=timeval[0]%></option>
							</select> &nbsp;&nbsp;<font for='btntime' color='#00008B'>(HH)</font>&nbsp;&nbsp;&nbsp;&nbsp;
							<select id='btntime1' name='btntime1' class='stdTextBox'
								style='min-width: 60px; width:60px'>
								<option value='<%=timeval[1]%>'><%=timeval[1]%></option>
							</select> &nbsp;&nbsp;<font for='btntime1' color='#00008B'>(MM)</font>
						</div>

						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtTask'>Task</label>
						</div>
						<div class='grid_4'>
							<input type='text' class='stdTextBox'
								value='<%=sch.getAction()%>' disabled='disabled' id='txtTask'
								name='txtTask' title='Task' />
						</div>

                    <input type='hidden' value='<%=recType%>'
							id='recTypeVal' name='recTypeVal' />

                   <div class='clear'></div>
						<div class='grid_2'>
							<label for='type'>Type</label>
						</div>
                          <div class='grid_4'>
							<select id='type' name='type' class='stdTextBoxNew' mandatory='yes'>
								<option value='0' >--Select One--</option>
								
								<%if(recType.equalsIgnoreCase("TSA")){ %>
								<option value='1' selected>Test Case</option>
								<option value='2' >Test Set</option>
								<%}else{ %>
								<option value='2' selected>Test Set</option>
								<option value='1' >Test Case</option>
								<%} %>
							</select>
						</div>
						
						 <input type='hidden' value='<%=testSet%>'
							id='testSet' name='testSet' />
						<input type='hidden' value='<%=tcId%>'
							id='tcId' name='tcId' />
						
						<div id="tset">		
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtTestSet' >Test Set</label>
							
						</div>

						<div class='grid_4'>
							<select id='txtTestSet' name='txtTestSet' type='select'
								class='stdTextBoxNew' mandatory='yes' title="Select TestSet">
								<%
									if (scheduleList != null) {
										for (SchMapping sm : scheduleList) {
								%>
								<option value='<%=sm.getTestStepId()%>' selected='selected'><%=sm.getTset_Id()%></option>

								<%
									}
									}
								%>
							</select>

						</div>
						</div>
						<div id="tCase">	
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtTestCase' >Test Case</label>
							
						</div>

						<div class='grid_4'>
								<select id='txtTestCase' name='txtTestCase' type='select'
								class='stdTextBoxNew' mandatory='yes' title="Select TestCase">
								<%
									if (scheduleList != null) {
										for (SchMapping sm : scheduleList) {
								%>
								<option value='<%=sm.getTestStepId()%>' selected='selected'><%=sm.getTset_Id()%></option>

								<%
									}
									}
								%>
							</select>

						</div>	
						</div>
						 
						 
					</div>

					<div class='fieldSection grid_7'>
						<div class='clear'></div>
						<div id="txtClient">
						<div class='grid_2'>
							<label for='txtclientregistered'>Client</label>
						</div>
						<div class='grid_4'>
							<input type='hidden' value='<%=sch.getReg_client()%>'
								id='currentClient' name='currentClient' /> <select
								id='txtclientregistered' name='txtclientregistered'
								class='stdTextBoxNew'>
								<%
									if (clientList != null) {
										for (RegisteredClient cl : clientList) {
								%>
								<option value='<%=cl.getName()%>' selected='selected'><%=cl.getName()%></option>

								<%
									}
									}
								%>

							</select>

						</div>
						</div>


						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtCrtBy'>Created By</label>
						</div>
						<div class='grid_4'>
							<input type='text' id='txtCrtBy' name='txtCrtBy'
								class='stdTextBox mid' value='<%=sch.getCreated_by()%>'
								disabled='disabled' mandatory='yes' title='Created By' />
						</div>

						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtCrtOn'>Created On</label>
						</div>
						<div class='grid_4'>
							<%
								String todaydate = "";

								Calendar calendar1 = Calendar.getInstance();
								SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
								todaydate = dateFormat.format(calendar1.getTime());
							%>
							<input type='text' value='<%=sch.getCreated_on()%>'
								id='txtCrtOn' name='txtCrtOn' title='Created On' mandatory='no'
								disabled='disabled' class='stdTextBox mid' />
						</div>

						<div class='clear'></div>

						<input type='hidden' value='Scheduled' id='txtStatus'
							name='txtStatus' />
							<div id="txtBrowser">
						<div class='grid_2'>
							<label for='lstBrowserType'>Browser</label>
						</div>
						<div class='grid_4'>
							<input type='hidden' value='<%=sch.getBrowserType()%>'
								id='currentBrowser' name='currentBrowser' /> <select
								id='lstBrowserType' name='lstBrowserType' class='stdTextBoxNew'>

								<option value='APPDEFAULT'>Use Application Default</option>
								<option value='<%=BrowserType.CHROME%>'>Google Chrome</option>
								<!-- Added by paneendra for Firefox browser starts  -->
								<option value="<%=BrowserType.FIREFOX%>"><%=BrowserType.FIREFOX%></option>
								<!-- Added by paneendra for Firefox browser ends  -->
								<option value='<%=BrowserType.IE%>'>Microsoft Internet
									Explorer</option>
								<option value='<%=BrowserType.CE %>'><%=BrowserType.CE %></option>
							</select>
						</div>
						</div>
                        <div class="clear"></div>
						<div class='grid_2'>
							<label for='screenShot'>Screenshot</label>
						</div>

						<div class='grid_4'>
							<input type='hidden' value='<%=sch.getScreenShotOption() %>'
								id='currentScreenShot' name='currentScreenShot' /> <select
								id='screenShot' name='screenShot' class='stdTextBoxNew'>
								<option value='0'>Never Capture</option>
								<option value='1'>Capture Only for Failures / Errors</option>
								<option value='2'>Capture For All Messages
								<option value='3'>Capture At the End of Each Page</option>
							</select>
						</div>

					</div>

				</fieldset>
			</div>

		</form>
		<div class='form container_16' id='tasktype'>
			<fieldset>
				<legend>Test Cases</legend>

				<div class='clear'></div>
			 
				<div class='clear'></div>
				<div class='fieldSection grid_15' id='modulesInfo'>
					<table id='tblNaviflowModules' class='display tjn-default-data-table' cellspacing='0'
						width='100%'>
						<tbody id='tblNaviflowModules_body'>
						</tbody>
					</table>
				</div>
			</fieldset>
		</div>

	</div>

</body>
</html>