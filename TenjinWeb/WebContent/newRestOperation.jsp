<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  newRestOperation.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY             	DESCRIPTION
 10-12-2020             paneendra               TENJINCG-1242 
*/

-->
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Parameter"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.ApiOperation"%>
<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.project.Project"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Api"%>
<%@page import="java.util.*"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta charset="ISO-8859-1">
		<title>New Operation</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/bordered.css'/>
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type="text/javascript" src="js/formvalidator.js"></script>
		<script src="js/pages/newRestOperation.js"></script>
		<script src="js/pages/newRestOperation_table.js"></script>
		<style>
			#progress_dialog, #confirmation_dialog
			{
				background-color:#fff;
			}
			
			.subframe_content
			{
				display:block;
				width:100%;
				margin:0 auto;
				min-height:120px;
				padding-bottom:20px;
			}
			.subframe_title
			{
				width:644px;
				height:20px;
				background:url('images/bg.gif');
				color:white;
				font-family:'Open Sans';
				padding-top:6px;
				padding-bottom:6px;
				padding-left:6px;
				font-weight:bold;
				margin-bottom:10px;
			}
			
			.subframe_actions
			{
				display:inline-block;
				width:100%;
				background-color:#ccc;
				padding-top:5px;
				padding-bottom:5px;
				font-size:1.2em;
				text-align:center;
			}
			
			.highlight-icon-holder
			{
				display:block;
				max-height:50px;
				margin-bottom:10px;
				text-align:center;
			}
			
			.highlight-icon-holder > img
			{
				height:50px;
			}
			
			.subframe_single_message
			{
				width:80%;
				text-align:center;
				font-weight:bold;
				display:block;
				margin:0 auto;
				font-size:1.3em;
			}
			
			#progressBar
			{
				width:475px;
				margin-top:20px;
				margin-left:auto;
				margin-right:auto;
			}
		.respCodeClass{
		     min-width: 40px!important;
		     width:40px!important;
		 }
		 
		 .respdesClass{
		  min-width: 400px!important;
		   width:400px!important;
		 }
			
		</style>
		
		<script type="text/javascript">
		 
		
		</script>
	</head>
	<body >
			<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		
			<%

			ArrayList<Aut> autList = null; 
			TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
			User cUser = tjnSession.getUser();

			Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
			String status = (String) map.get("STATUS");
			String message = (String) map.get("MESSAGE");
			ApiOperation operation =(ApiOperation)map.get("API_OPERATION");
			List<Parameter> listParameters=(List<Parameter>)map.get("API_OPERATION_PARAMLIST");
			String lrnrPage = "";
			String appid = request.getParameter("appid");
			String code = request.getParameter("code");
			String optName = null;
			optName = request.getParameter("optName");
			
			%>
		<div class='title'>
			<p>New Operation</p>
		</div>
		<form name='new_rest_operation_form' id='new_rest_operation_form' action='APIServlet' method='POST'>
			<input type='hidden' id='appid' name='appid' value='<%=appid%>' />
			<input type='hidden'  id='code'  name='code' value='<%=code%>' />
			<input type='hidden'  id='requesttype'  name='requesttype' value='new_rest_operation_form' />
			<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			
			<div class='toolbar'>
				<input type='button' class='imagebutton back' value='Back'
					id='btnBack' /> <input type='submit' class='imagebutton save'
					value='Save' id='btnSave' />
			</div>
	
			<div id='user-message'></div>
			<input type='hidden' id='scrStatus' value='<%=status == null? "" : status %>'/>
			<input type='hidden' id='scrMessage' value='<%=message == null? "" : message %>'/>
			
			<div class='form container_16'>
				<fieldset>
					<legend>Basic Details</legend>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='optName'>Name</label></div>
						<div class='grid_4'>
						<input type='hidden' id='optName1'  name='optName1' value='<%=operation == null? "" : operation.getName()%>' />
						<input type='text' id='optName'  name='optName' class='stdTextBox' value='<%=operation == null? "" : operation.getName()%>' mandatory='yes' title='Operation name' maxlength="100"/></div>
					</div>
					<div class='fieldSection grid_7'>
						<div class='grid_2'><label for='optMethod'>Method</label></div>
						<div class='grid_2'>
							<input type='hidden' id='optMethodVal' value='<%=operation == null? "" : Utilities.trim(operation.getMethod())%>'/>
							<select id='optMethod' name='optMethod' class='stdTextBoxNew' mandatory='yes' title='Operation method'>
								<option value="" disabled>--Select One--</option>
								<option value='GET'>GET</option>
								<option value='POST'>POST</option>
								<option value='PUT'>PUT</option>
								<option value='DELETE'>DELETE</option>
							</select>
						</div>
					</div>
				</fieldset>
				
				<div class='clear'></div>
				<!-- Added by Paneendra for TENJINCG-1242 starts
				 -->
				<fieldset>
					<legend>Authentication</legend>
					<div class='fieldSection grid_15'>
						<div class='grid_4'>Authentication Type</div>
						<div class='grid_8'>
							<select id='authType' name='authType' class='stdTextBoxNew'>
								<option value='NOAUTH'>No Authentication</option>
								<!-- Added by Priyanka for Tenj211-13 starts -->
								<option value='Basic'selected>Basic Authentication</option>
								<!-- Added by Priyanka for Tenj211-13 ends -->
								<option value='oAuth2'>OAuth 2.0</option>
								<option value='Apikey'>Api Key</option>
							</select>
						</div>
					</div>
				</fieldset>
				
			<!-- Added by Paneendra for TENJINCG-1242 ends
				 -->
				
				<fieldset>
					<legend>Resource Parameter</legend>
					<input type='button' value='New' id='btnNew' name='btnNew' class='imagebutton new'/>
					<input type='button' value='Remove' id='btnDelete' name='btnDelete' class='imagebutton delete' />
					
					<div class='clear'></div>
					
					<div class='fieldSection grid_15' id='resourceParametersInfo'>
						<input type='hidden' id='pagination_value' value='<%=lrnrPage%>'/>
						<table id='' class='bordered' cellspacing='0' style='width:100%; margin-bottom:40px;'>
							<thead>
								<tr>
								<th><input type='checkbox' class='tbl-select-all-rows tiny' id='chk_all_parameters'/></th>
								<th>Name</th>
								<th>Type</th>
								<th>Style</th>	
								</tr>
							</thead>
							<tbody id='tblNaviflowModules_body'>
							<%if(listParameters!=null && listParameters.size()>0){
								for(Parameter param:listParameters){
							
								if(param.getName()!=null && param.getType()!=null && param.getStyle()!=null){
								 %>
								<tr>
									<td class='tiny'><input type='checkbox' name='paramNameCheck' /></td>
									
									<td><input type='text' name='paramName' id='paramName' maxlength="100"  value='<%=param.getName() %>' class='stdTextBox'/></td>
									<td><input type='text' name='paramType' id='paramType' maxlength="100" value='<%=param.getType() %>' class='stdTextBox'/></td>
									<td><input type='text' name='paramStyle' id='paramStyle' maxlength="100" value='<%=param.getStyle() %>' class='stdTextBox'/></td>
								</tr>
						<%}}}else{ %>
						<tr>
									<td class='tiny'><input type='checkbox' name='paramNameCheck' /></td>
									
									<td><input type='text' name='paramName' id='paramName' maxlength="100" class='stdTextBox'/></td>
									<td><input type='text' name='paramType' id='paramType' maxlength="100" class='stdTextBox'/></td>
									<td><input type='text' name='paramStyle' id='paramStyle' maxlength="100" class='stdTextBox'/></td>
								</tr>
					     <%} %>
					     <!-- changes done by Parveen for parameter List ends -->
							</tbody>
						</table>
					</div>
				</fieldset>	
				
				
				<div class='clear'></div>
				<!-- Added by Paneendra for TENJINCG-1242 starts -->
				<div id='headerBlock'>
				<fieldset>
					<legend>Header</legend>
					<div class='fieldSection grid_7'>
					<div class='grid_3'><label for='optHeadType'>Header Media Type</label></div>
						<div class='grid_3'>
							<select id='optHeadType' name='optHeadType' class='stdTextBoxNew'>
								<option value='application/json'>application/json</option>
								<option value='application/xml'>application/xml</option>
							</select>
						</div>
					</div>
					
					
					<div class='clear'></div>
					
					<div class='fieldSection grid_15'>
						<div class='grid_13'>
						<textarea rows="10" cols="61" class='stdTextBox' id='optHeadDesc' style="width:863px" name='optHeadDesc'></textarea>
						</div>
					</div> 
					
				</fieldset>
				</div>
				
				<div class='clear'></div>
				<!-- Added by Paneendra for TENJINCG-1242 ends -->
				
				<fieldset>
					<legend>Request</legend>
					<div class='fieldSection grid_7'>
					<div class='grid_3'><label for='optReqType'>Request Media Type</label></div>
						<div class='grid_3'>
							<select id='optReqType' name='optReqType' class='stdTextBoxNew'>
								<option value='application/json'>application/json</option>
								<option value='application/xml'>application/xml</option>
								<!-- Added by Pushpalatha for Flexiloans api starts -->
								<option value='application/x-www-form-urlencoded'>application/x-www-form-urlencoded</option>
								<!-- Added by Pushpalatha for Flexiloans api ends -->
							</select>
						</div>
					</div>
					
					
					<div class='clear'></div>
					
					<div class='fieldSection grid_15'>
						<div class='grid_13'>
						<textarea rows="10" cols="61" class='stdTextBox' id='optReqDesc' style="width:863px" name='optReqDesc'></textarea>
						</div>
					</div> 
					
				</fieldset>
				
				<div class='clear'></div>
				
				<fieldset>
					<legend>Response</legend>
					
					<input type='button' value='New' id='btnResponseTableNew' name='btnResponseTableNew' class='imagebutton new'/>
					<input type='button' value='Remove' id='btnResponseTableDelete' name='btnResponseTableDelete' class='imagebutton delete' />
					<div class='clear'></div>
					<div class='fieldSection grid_7'>
						<div class='grid_3'><label for='optResType'>Response Media Type</label></div>
						<div class='grid_3'>
							<select id='optResType' name='optResType' class='stdTextBoxNew'>
								<option value='application/json'>application/json</option>
								<option value='application/xml'>application/xml</option>
							</select>
						</div>
					</div>
					
					
					<div class='clear'></div>

				<div class='fieldSection grid_15'>

					<table class='bordered' cellspacing='0' style='width:100%; margin-bottom:40px;'>
						<thead>
							<tr>
								<th><input type='checkbox' class='tbl-select-all-rows' id=''/></th>
								<th>Status</th>
								<th>Response</th>
							</tr>
						</thead>
						<tbody id='tblResponse_body'>

							<tr>
								<td><input type="checkbox" name="responseParamCheck"></td>
								<td><input type='text' name='opRespCode' id='txtstatus' value='' maxlength='10' class='stdTextBox respCodeClass'/></td>
								<td><textarea class="stdTextBox expandable respdesClass" type="text"
										id="myIn" placeholder="Enter Response" rows="2" name="opRespDesc"></textarea></td>
							</tr>
						</tbody>
					</table>

				</div>

				</fieldset>
				
				<div class='clear'></div>
				
				
			</div>
			
		</form>
		
		<div class='subframe' id='ttd_download_options' style='left:20%'>
			<iframe frameborder="2" scrolling="no"  marginwidth="5" marginheight="5" style='height:500px;width:600px;' seamless="seamless" id='ttd-options-frame' src=''></iframe>
		</div>
		
		<div class='modalmask'></div>
		<div class='subframe' id='confirmation_dialog'style='left:20%'>
			<div class='subframe_title'><p>Please Confirm...</p></div>
			<div class='subframe_content'>
				<div class='highlight-icon-holder'>
					<img src='images/warning_confirm.png' alt='Please Confirm' />
				</div>
				<div class='subframe_single_message'>
					<p>Refreshing may remove manually entered Data. Do you still want to proceed?</p>
				</div>
			</div>
			<div class='subframe_actions'>
				<input type='button' id='btnCOk' value='Proceed anyway' class='imagebutton ok'/>
				<input type='button' id='btnCCancel' value='Do not Proceed' class='imagebutton cancel'/>
			</div>
		</div>
	</body>
</html>