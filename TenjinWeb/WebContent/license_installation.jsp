<!--


Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  license_insatllation.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India





<!--
/*****************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              	DESCRIPTION

*/
-->

<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@page import="java.util.Map"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tenjin - Intelligent Enterprise Testing Engine</title>
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/license.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<link rel='stylesheet' href='css/buttons.css' />
<link rel="SHORTCUT ICON" HREF="images/yethi.png">
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='js/jquery-ui-1.13.0.js'></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
</head>
<script>
$(document).ready(function(){
	var status=$('#tjn-status').val();
	if(status!=""){
		if(status=='error'){
			var msg=$('#tjn-message').val();
			$('.statusContainer').html("<div class='errmsg'>"+msg+"</div>");
			$('.statusContainer').show('fast');
		}
	}
	$(document).on('click', '#btnActive', function() {
		if(!$("#licenseKey").val()){
			$('.statusContainer').html("<div class='errmsg'> License Key is required.</div>");
			$('.statusContainer').show('fast');
			return false;
		}
	});
});

</script>
<body>

 <%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>

	<div class='wrapper'>
		<div class='main'>
			<div class='container_16' id='main-block'>
				<div class='grid_1' style='height: 100%;'></div>
				<div class='grid_14' style='height: 100%;'>
					<div id='landing_image' class='grid_14'>
						<img src='images/tenjin_logo_new.png' alt='Tenjin - Intelligent Testing Engine - V 1.0'
							style='margin: 0 auto; display: block; height: 100%;' />
					</div>
					&nbsp;&nbsp; &nbsp;&nbsp;
					<div id='landing_action_block' class='grid_14'>
						<div class='grid_10' style='padding-top: 35px;'>
							<div id='landing_action_project_selection'>
								&nbsp;&nbsp;
								<form Action='LicenseServlet' method='POST'>
								<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />								<input type='hidden' id='systemId' name="systemId" value='${systemId}' />
								<input type='hidden' id='param' name="param" value='activateLicense' />
								<input type='hidden' id='tjn-status' value='${status }' />
								<input type='hidden' id='tjn-message' value='${message }' />
									<div id='proj_selection_title' style="color: red;">Enter License key</div>
									<div class='clear'></div>
									<div style="padding-bottom:10px;">
										<div class='grid_2' style='display: inline; line-height: 2; padding-right: 10px;'>
											<label for=systemId>System ID </label>
										</div>
										<div class='grid_12' style='width: 82%; display: inline; padding-right: 10px; overflow:auto;line-height: 2;'>
											<b> ${systemId}</b>
										</div>
									</div>
									<div>
										<div class='grid_2' style=' display: inline; line-height: 2;'>
											<label for='licenseKey'>License Key </label>
										</div>
										<div class='grid_6' style='width: 50%; float: left; display: inline; '>
											<textarea style="resize:none" class="stdTextBox" id="licenseKey" name="licenseKey">${licenseKey}</textarea>
										</div>
									</div>
									<div id='landing_buttons' style="padding-top:80px">
										<input type='submit' class='imagebutton ok' id='btnActive' value='Activate' />
									</div>
								</form>
							</div>
					 		<div class='statusContainer'></div>
							 <div class="copyrightbox" >
					 			<div id='ycslogo'>
									<img src='images/ycs_logo_full.png'
										alt='Yethi Consulting - People, Process, Technology' />
								</div>
								<div id='copyright-clause'>
									<p>
										Copyright &copy 2014 -
										<%=new java.text.SimpleDateFormat("yyyy").format(new java.util.Date()) %>
										Yethi Consulting Private Ltd.&nbsp;&nbsp;All Rights Reserved
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>