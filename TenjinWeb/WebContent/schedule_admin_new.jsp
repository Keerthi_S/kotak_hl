<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  schedule_admin_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<%@page import="com.ycs.tenjin.bridge.oem.api.generic.ApiLearnType"%>
<%@page import="java.util.Map"%>
<%@ page
	import="java.util.Date,java.text.SimpleDateFormat,java.text.ParseException"%>
<%@page import="java.util.Date"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@page import="java.util.Calendar"%>
<%@ page import="java.util.Random"%>
<%@page import="java.util.List"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%>
<%@page import="com.ycs.tenjin.scheduler.Scheduler"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ycs.tenjin.TenjinConfiguration"%>
<%@page import="com.ycs.tenjin.user.User"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
	 <%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
   
   
*/

-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Schedule Task</title>
<link rel='stylesheet' href='css/bordered.css' />
<link rel='stylesheet' href='css/cssreset-min.css' />
<link rel='stylesheet' href='css/ifr_main.css' />
<link rel='stylesheet' href='css/style.css' />
<link rel='stylesheet' href='css/tabs.css' />
<link rel="Stylesheet" href="css/jquery.dataTables.css" />
<link rel='stylesheet' href='css/buttons.css' />
<link rel='stylesheet' href='css/960_16_col.css' />
<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src="js/jquery-1.12.4.js"></script>
<script type='text/javascript' src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
<script type='text/javascript' src='js/pages/schedule_admin_new.js'></script>
 
<script src="js/formvalidator.js"></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">


</head>
<body>
	<%
	Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();	String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
		TenjinSession tjnSession = (TenjinSession) request.getSession()
				.getAttribute("TJN_SESSION");
		User cUser = tjnSession.getUser();
	%>
	<div class='main_content'>
		<div class='title'>

			<p>Schedule Task</p>
		</div>
		<form name='new_schedule_admin' action='SchedulerServlet' method='get'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
				<input type='button' value='Save' id='btnSave'
					class='imagebutton save' />
				<input type='button' value='Recurrence' id='btnRecurrence'
					class='imagebutton recurrence' />
				<input type='button' value='Cancel' id='btnCancelTask' class='imagebutton cancel' />
				 
				 <input type='button' value='Refresh' id='btnRefreshAut' class='imagebutton refresh' />
			</div>


			<div id='user-message'></div>
			<div class='grid_2' style='float: right; font-size: 0.7em;'>
					<label id='date'> </label>
			</div>
			<div class='form container_16'>
				<fieldset>

					<legend>Schedule Details</legend>
					<div class='fieldSection grid_16'
						style='margin-top: 5px; height: 28px'>
						<div class='grid_2'>
							<label for='taskName'>Task Name</label>
						</div>
						<div class='grid_13'>
                            <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<input type='text' id="taskName" class='stdTextBox long'
								maxlength="40" mandatory='yes' title='Task Name'
								style='width: 81.7%' placeholder="MaxLength 40" />
						    <!-- Modified by Priyanka for TENJINCG-1233 ends -->

						</div>
					</div>
					<div class='fieldSection grid_7'>

						<div class='grid_2'>
							<label for='datepicker'>Date</label>
						</div>
						<div class='grid_4'>
							<%
								String todaydate1 = "";

								Calendar calendar = Calendar.getInstance();
								SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
								todaydate1 = dateFormat1.format(calendar.getTime());
							%>
						 
							 
								<input type='text' id="datepicker"  value='<%=todaydate1%>'
								style='border: 1px solid #ccc; border-radius: 3px; padding: 3px; box-shadow: 0 0 2px #ccc; min-width: 177px;'
								mandatory='yes' title='Schedule Date' />
							 
						</div>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='btntime'>Time</label>
						</div>
						<div class='grid_4'>
							<select id='btntime' name='btntime' class='stdTextBox'
								style='min-width: 60px; width:60px'>
							</select> &nbsp;&nbsp;<font for='btntime' color='#00008B'>(HH)</font>&nbsp;&nbsp;&nbsp;&nbsp;
							<select id='btntime1' name='btntime1' class='stdTextBox'
								style='min-width: 60px; width:60px' mandatory='yes'>
							</select> &nbsp;&nbsp;<font for='btntime1' color='#00008B'>(MM)</font>
						</div>
						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtTask'>Task</label>
						</div>
						<%if(cUser.getRoles().equalsIgnoreCase("Site administrator")){ %>
						<div class='grid_4'>
							<select id='txtTask' name='txtTask' mandatory='yes'
								class='stdTextBoxNew' title='Task'>
								<option value='-1'>-Select One-</option>
								<option value='Learn'>Learn Functions</option>
								<option value='LearnAPI'>Learn APIs</option>
								<option value='Extract'>Extract</option>
							</select>
						</div>
						<%}
						else{%>
						<div class='grid_4'>
							<select id='txtTask' name='txtTask' mandatory='yes'
								class='stdTextBoxNew' title='Task'>
								<option value='-1'>-Select One-</option>
								<option value='Extract'>Extract</option>
							</select>
						</div>
						<%} %>
						 
						<div class='clear show-for-all'></div>
						<div class='grid_2 show-for-all' id='lsttask'>
							<label for='lstApplication'>Application</label>
						</div>
						<div class='grid_4 show-for-all'>
							<select id='lstApplication' name='lstApplication' mandatory='yes'
								class='stdTextBoxNew' title='Application'>
							</select>
						</div>
						<div class='clear gui-only'></div>
						<div class='grid_2 gui-only' id='lastAutType'>
							<label for='lstAppUserType'>Aut Login Type</label>
						</div>
						<div class='grid_4 gui-only'>
						 
							<select class='stdTextBoxNew' id='lstAppUserType'
								title='AUT AppUserType' mandatory='yes'>
							 
							</select>
						</div>
						
						<div class='apiBlock' style='display:none;'>
							<div class='clear'></div>
							<div class='grid_2'>
								<label for='lstAPICode'>API</label>
							</div>
							<div class='grid_4'>
								<select class='stdTextBoxNew' id='lstAPICode'
									title='API Code' mandatory='yes'>
								</select>
							</div>
						</div>
						
					</div>
					<div class='fieldSection grid_7'>
						<div class='grid_2 gui-only'>
							<label for='txtclientregistered'>Client</label>
						</div>
						<div class='grid_4 gui-only'>
						 
								<select id='txtclientregistered' name='txtclientregistered'
								type='select' class='stdTextBoxNew' 
								title="Select Reg Client" mandatory='yes'></select>
								
						</div>

						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtCrtBy'>Created By</label>
						</div>
						<div class='grid_4'>
							<input type='text' id='txtCrtBy' name='txtCrtBy'
								class='stdTextBox mid' value='<%=cUser.getId()%>'
								disabled='disabled'  title='Created By' />
						</div>

						<div class='clear'></div>
						<div class='grid_2'>
							<label for='txtCrtOn'>Created On</label>
						</div>
						<div class='grid_4'>
						    <%
								String todaydate = "";

								Calendar calendar1 = Calendar.getInstance();
								SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
								todaydate = dateFormat.format(calendar1.getTime());
							%> 
							<input type='text' value='<%=todaydate%>' id='txtCrtOn'
								name='txtCrtOn' title='Created On' mandatory='no'
								disabled='disabled' class='stdTextBox mid' />
						</div>

						<div class='clear gui-only'></div>
						<div class='grid_2 gui-only'>
							<label for='lstBrowserType'>Browser</label>
						</div>
						<div class='grid_4 gui-only'>
							<select id='lstBrowserType' name='lstBrowserType'
								class='stdTextBoxNew'>

								<option value='APPDEFAULT'>Use Application Default</option>

								<option value='<%=BrowserType.CHROME%>'>Google Chrome</option>
								<!-- Added by paneendra for Firefox browser starts  -->
								<option value="<%=BrowserType.FIREFOX%>"><%=BrowserType.FIREFOX%></option>
								<!-- Added by paneendra for Firefox browser ends  -->
								<option value='<%=BrowserType.IE%>'>Microsoft Internet
									Explorer</option>
									<!-- Modified by Prem for TENJINCG-1246 starts -->
								<%-- <option value='<%=BrowserType.FIREFOX%>'>Mozilla
									Firefox</option> --%>
									<option value='<%=BrowserType.CE %>'><%=BrowserType.CE %></option>
							
							<!-- Modified by Prem for TENJINCG-1246 ends -->
							</select>
						</div>
						<div class='apiBlock'>
						<div id='learnerTypeBlock'>
							<div class='clear'></div>
							<div class='grid_2'>
								<label for='lstApiLearnType'>Learn Type</label>
							</div>
							<div class='grid_4'>
								<select id='lstApiLearnType' name='lstApiLearnType'
									class='stdTextBoxNew'>
	
									<option value='<%=ApiLearnType.URL.toString() %>'>Learn from URL</option>
									<%-- <option value='<%=ApiLearnType.XSD.toString() %>'>Learn from XSD</option> --%>
									<option value='<%=ApiLearnType.REQUEST_XML.toString() %>'>Learn from Request and Response XML</option>
									<!-- Added by Preeti for T251IT-181 starts -->
									<option value='<%=ApiLearnType.REQUEST_JSON.toString() %>'>Learn from Request and Response JSON</option>
									<!-- Added by Preeti for T251IT-181 ends -->
								</select>
							</div>
						</div>	
						</div>

						<div class='clear'></div>

						<div class='grid_4'>
							<input type='hidden' value='Scheduled' id='txtStatus'
								name='txtStatus' />

						</div>
					</div>

				</fieldset>

			</div>

		</form>
		<div class='form container_16' id='tasktype'>
			<input type='hidden' id='lstLrnrApplication'
				name='lstLrnrApplication' value='' /> <input type='hidden'
				id='lstLrntGroup' name='lstLrntGroup' value='' />
			<fieldset>
				<!-- Modified by Preeti for T251IT-102 starts -->
				<legend id='func'>Function(s)</legend>
				<legend id='api'>API(s)</legend>
				<!-- Modified by Preeti for T251IT-102 ends -->
				<div class='clear'></div>
				<!-- Commented by Preeti for V2.8-113 starts -->
				<!-- <div class='fieldSection grid_15' id='pagination_block'>
					<div style='display: table-cell; float: left;'>
						Show <select id='pages_max_rows'>
							<option value='10'>10</option>
							<option value='20'>20</option>
							<option value='50'>50</option>
							<option value='100'>100</option>
						</select> entries &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search <input
							type='text' class='stdTextBox' id='search' />
					</div>

					<div style='display: table-cell; float: right;'>
						<a href='#' class='pagination' id='first' title='First Page'><<</a>
						<a href='#' class='pagination' id='prev' title='Previous Page'><</a>
						<a href='#' class='pagination' id='next' title='Next Page'>></a> <a
							href='#' class='pagination' id='last' title='Last Page'>>></a>
						&nbsp;Page <input type='text' id='curpage'> <a href='#'
							class='pagination' id='gotopage'>Go</a> out of <span
							id='totpages'></span>
					</div>
				</div> -->
				<!-- Commented by Preeti for V2.8-113 ends -->
				<div class='clear'></div>
				<div class='fieldSection grid_15' id='modulesInfo'>
					<!-- Modified by Preeti for V2.8-113 starts -->
					<table id='tblNaviflowModules' class='display tjn-default-data-table' cellspacing='0'
						width='100%'>
					<!-- Modified by Preeti for V2.8-113 ends -->
						<tbody id='tblNaviflowModules_body'>
						</tbody>
					</table>
				</div>
			</fieldset>
		</div>
	</div>
	<!--changed by sahana for req#TJN_243_03 starts-->
	<div class='modalmask'></div>
	<div class='subframe' style='left: 40%'>
		<!-- <iframe frameborder="2" scrolling="no" marginwidth="5"
			marginheight="5" style='height: 300px; width: 600px;'
			seamless="seamless" id='recur-sframe' src=''></iframe> -->
			<!-- Modified by Roshni for T25IT-61 starts -->
			<iframe frameborder="2" scrolling="no" marginwidth="5"
			marginheight="5" style='height: 350px; width: 600px;'
			seamless="seamless" id='recur-sframe' src=''></iframe>
			<!-- Modified by Roshni for T25IT-61 ends -->
	</div>
	<!--changed by sahana for req#TJN_243_03 ends-->
</body>
</html>
