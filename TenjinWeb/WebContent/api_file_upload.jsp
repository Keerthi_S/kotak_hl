<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  api_file_upload.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


<%@page import="java.util.List"%>
<%@page import="com.ycs.tenjin.TenjinSession"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE             CHANGED BY              	DESCRIPTION
*/ -->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css'/>
		<link rel='stylesheet' href='css/buttons.css'/>
		<link rel="Stylesheet" href="css/bordered.css" />
		<link rel='stylesheet' href='css/960_16_col.css'/>
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script src="js/formvalidator.js"></script>
		
		<style>
				#selectedFiles
				{
					margin-left:0;
				}
				
				.att_block
				{
					padding:5px;
					border:1px solid #707a86;
					background-color:#b7c0ef;
					border-radius:5px;
					margin-left:0;
				}
				
				.att_block p
				{
					overflow:hidden;
					text-overflow:ellipsis;
				}
				
				#def_review_confirmation_dialog,#def_review_progress_dialog
				{
					background-color:#fff;
					
				}
				
				.subframe_content
				{
					display:block;
					width:100%;
					margin:0 auto;
					min-height:120px;
					padding-bottom:20px;
				}
				.subframe_title
				{
					width:644px;
					height:20px;
					background:url('images/bg.gif');
					color:white;
					font-family:'Open Sans';
					padding-top:6px;
					padding-bottom:6px;
					padding-left:6px;
					font-weight:bold;
					margin-bottom:10px;
				}
				
				.subframe_actions
				{
					display:inline-block;
					width:100%;
					background-color:#ccc;
					padding-top:5px;
					padding-bottom:5px;
					font-size:1.2em;
					text-align:center;
				}
				
				.highlight-icon-holder
				{
					display:block;
					max-height:50px;
					margin-bottom:10px;
					text-align:center;
				}
				
				.highlight-icon-holder > img
				{
					height:50px;
				}
				
				.subframe_single_message
				{
					width:80%;
					text-align:center;
					font-weight:bold;
					display:block;
					margin:0 auto;
					font-size:1.3em;
				}
				
				#progressBar
				{
					width:475px;
					margin-top:20px;
					margin-left:auto;
					margin-right:auto;
				}
		</style>
		
		
		<script type='text/javascript' src='js/pages/api_file_upload.js'></script>
		
	</head>
	<body>
	<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
		  <%
		  	String resourceType = request.getParameter("rtype");
		  	String app = request.getParameter("app");
	     %>
	     	<div class='title'>
				<p>Upload API Resource</p>
			</div>
			
			<form name='api_upload_form' method="POST" action='ApiUploadServlet' enctype="multipart/form-data" onsubmit="return validate()">
				<div class='toolbar'>
				 <!-- added by shruthi for CSRF token starts -->
		         <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		         <!-- added by shruthi for CSRF token ends -->
				
				<input type='button' id='btnBack' class='imagebutton back' value='Back'/>
					<input type='submit' id='btnUpload' value='Upload' class='imagebutton ok'/>
				</div>
				
				<div class='form container_16'>
					<div id='user-message'></div>
					
					
					<fieldset style='border:0px;margin-top:10px;'>
						<div class='fieldSection' class='grid_8'>
							<div class='grid_2'><label for='resourceType'>Resource Type</label></div>
							<input type='hidden' id='resourceType' name='resourceType' value='<%=resourceType %>'/>
							<input type='hidden' id='appId' name='appId' value='<%=app %>' />
							<div class='grid_5'><input type='text' disabled id='resource_type' value='<%=resourceType %>' class='stdTextBox'/></div>
						</div>
						<div class='clear'></div>
						<% if(resourceType.toLowerCase().equalsIgnoreCase("wsdl") || resourceType.toLowerCase().equalsIgnoreCase("wadl")){ %>
						<div class='fieldSection' class='grid_8'>
							<div class='grid_2'><label for='createApi'>Create APIs</label></div>
							<div class='grid_5'>
								<select id='createApi' name='createApi' class='stdTextBoxNew'>
									<option value='N'>Do not create</option>
									<option value='Y'>Create</option>
								</select>
							</div>
						</div>
						<%} %>
						
						<div id='apiTypeBlock'>
							<div class='clear'></div>
							<div class='fieldSection' class='grid_8'>
								<div class='grid_2'><label for='apiType'>API Type</label></div>
								<div class='grid_5'>
									<select id='apiType' name='apiType' class='stdTextBoxNew'>
										<option value='-1'>-- Select One --</option>
										<%
										List<String> apiAdapters = (List<String>) request.getSession().getAttribute("AVL_API_ADAPTERS");
										if(apiAdapters != null) {
											for(String adapter : apiAdapters) {
												%>
												<option value='<%=adapter %>'><%=adapter %></option>
												<%
											}
										}
										%>
									</select>
								</div>	
							</div>
						</div>
					
					</fieldset>
					
					<fieldset id='filesToUpload'>
						<legend>File(s)</legend>
						<div id='fieldSection  grid_8'>
							<input type='file' multiple='multiple' id='txtFile' class='stdTextBox'/>
						</div>
					
						<div class='clear'></div>
						
						<div id='selectedFiles' class='grid_8'>
							
						</div>
						<div id="uploadedFiles">
							<table id='infoFiles' class='bordered' style="width:940px;">
							</table>
						</div>
					</fieldset>
					
					<fieldset id='uploadProgress'>
						<legend>Upload in progress</legend>
						<p>Tenjin is uploading your resources. Please wait... <img src='images/inprogress.gif' /> </p>
					</fieldset>
				</div>
			</form>
	     
	</body>
</html>