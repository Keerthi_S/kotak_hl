<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  learnerprogress.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->


<%@page import="com.ycs.tenjin.util.Utilities"%>
<%@page import="com.ycs.tenjin.client.RegisteredClient"%>
<%@page import="com.ycs.tenjin.device.RegisteredDevice"%>
<%@page import="com.ycs.tenjin.client.RegisteredClient"%>
<%@page import="java.util.List"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Module"%>
<%@page import="com.ycs.tenjin.bridge.constants.BrowserType"%>
<%@page import="com.ycs.tenjin.bridge.pojo.aut.Aut"%> 
<%@page import="com.ycs.tenjin.bridge.pojo.aut.ModuleBean"%> 


<%@page import="java.util.ArrayList"%> 
<%@page import="java.util.Map"%> 
<%@page import="com.ycs.tenjin.project.Project"%> 
<%@page import="com.ycs.tenjin.user.User"%> 
<%@page import="com.ycs.tenjin.TenjinSession"%> 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
    pageEncoding="ISO-8859-1"%> 
<%@page import="java.net.Socket"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html> 
<!--

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
   18-12-2020			Pushpalatha				TENJINCG-1246
*/

-->
    <head> 
        <title>Learner - Tenjin Intelligent Testing Engine</title> 
        <link rel='stylesheet' href='css/cssreset-min.css' /> 
        <link rel='stylesheet' href='css/ifr_main.css' /> 
        <link rel='stylesheet' href='css/style.css'/> 
        <link rel='stylesheet' href='css/tabs.css'/> 
        <link rel='stylesheet' href='css/buttons.css'/> 
        <link rel='stylesheet' href='css/960_16_col.css'/> 
        <link rel='stylesheet' href='css/jquery.dataTables.css'/> 
        <script type='text/javascript' src='js/jquery-3.6.0.min.js'></script> 
        <script type="text/javascript" src="js/jquery.dataTables.js"></script> 
        <script type='text/javascript' src='js/pages/tjnmaster.js'></script> 
        <script type='text/javascript' src='js/pages/learnerprogress.js'></script> 
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-ui-1.13.0.js'></script>
    </head> 
    <body> 
    
    <%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(1200, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
    
        <% 
            TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION"); 
            User cUser = tjnSession.getUser(); 
            Project project = null; 
            project = tjnSession.getProject(); 
             
            boolean canMakeChanges = false; 
             
             
            if((cUser.getRoles() != null && cUser.getRoles().equalsIgnoreCase("Site Administrator")) || project.hasAdminPrivileges(cUser.getId())){ 
                canMakeChanges = true; 
            } 
            
			String lrnrInitStatus = (String) request.getSession().getAttribute("LRNR_INIT_STATUS");
			String lrnrInitMessage = (String) request.getSession().getAttribute("LRNR_INIT_MESSAGE");
			String funcList = (String) request.getSession().getAttribute("funList");
			String app = (String) request.getSession().getAttribute("app");
        %> 
         
        <div class='title'> 
            <p>Additional Learning Information</p> 
        </div> 
        <div class='toolbar'> 
                <input type='button' value='Learn' id='btnLearn' class='imagebutton learn'/> 
            </div> 
        <form name='main_form' id='main_form' action='LearnerServlet' method='POST'>
        	
        	<input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
        	<input type='hidden' id='request-type' name='requesttype' value='learn'/> 
        	<input type='hidden' id='funcList' name='funcList' value='<%=funcList %>'/> 
        	<input type='hidden' id='app' name='app' value='<%=app %>'/>
            
            <%
		String showError="noerror";
		if(lrnrInitStatus != null && lrnrInitStatus.equalsIgnoreCase("error")){
			showError = lrnrInitMessage;
		}
		%>
		<input type='hidden' id='lrnrInitMessage' value='<%=showError %>'/>
		 <input type='hidden' value='' id='clientName' name='clientName' class='imagebutton learn'/> 
            <div id='user-message'></div> 
            <div class='form container_16'> 
            <% 
                        Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("LRNR_MAP"); 
                        ArrayList<Module> modules = (ArrayList<Module>)map.get("LRNR_FUNCS"); 

                        ArrayList<String> userCreds = null; 
                        ArrayList<Aut> otherCreds = null; 
                        userCreds = (ArrayList<String>) map.get("OWN_CREDS"); 
                         List<RegisteredClient> clients = (List<RegisteredClient>)map.get("LRNR_CLIENTS");
         				Integer appType=(Integer)map.get("LRNR_AUT_TYPE");
         				String appId=(String)map.get("LRNR_AUT_ID");
                    %> 
                <fieldset> 
                    <legend>Additional Information</legend> 
                        <div class='fieldSection grid_7'> 
                            <div class='clear'></div> 
                            <div class='grid_2'>
                            	<label for='lstAutCred'>AUT Login Type</label>
                            </div> 
                            <div class='grid_4'> 
                                <select id='lstAutCred' name='lstAutCred' class='stdTextBox'> 
                                    <% 
                                    if(userCreds != null && userCreds.size() > 0){ 
                                        %> 
                                         
                                        <% 
                                        for(String aut:userCreds){ 
                                            %> 
                                            <option value='<%=Utilities.escapeXml(aut)%>'><%=Utilities.escapeXml(aut)%> </option> 
                                            <% 
                                        } 
                                         
                                    } 
                                     
                                     
                                    %> 
                                </select> 
                            </div> 
                             
                            
                            <div class='clear'></div>
                            <div class='grid_2'><label for='client'>Client</label></div>
                            <div class='grid_4'>
                            <%String ipAddress=request.getRemoteAddr();
                            	String hostName=request.getRemoteHost();
                            	boolean flag=false;
                            		String clientName=null;
                            		String clientDeviceFarm=null;
                            		String clientHost=null;
                            		if(clients != null){
                            			for(RegisteredClient client:clients){
                            				
                            				if(ipAddress.equals(client.getHostName())|| hostName.equals(client.getHostName()))
                            				{
                            					flag=true;
                            					clientName=client.getName();
                            					clientHost=client.getHostName();
                            					clientDeviceFarm=client.getDeviceFarmCheck();
                            				break;
                            				}
                            				
                       					}
                            			
                            		 %>
                                   	<select id='client' name='client' class='stdTextBox'>
                                   	<% 
                                   	if(flag){
                                   	%>
                                   		<option data_deviceFarm='<%=Utilities.escapeXml(clientDeviceFarm)%>' value='<%=Utilities.escapeXml(clientHost)%>'><%=Utilities.escapeXml(clientName)%></option>
                                   	<% }else
                                   		{
                                   		%>
                                   		<option value='-1'>-- Select One --</option>
                                   		<% 
                                   		}
                                   	
                            		
                            			if(clients != null){
                                			for(RegisteredClient client:clients){
                                				if(client.getName().equals(clientName))
                                				{
                                					
                                				}
                                				else
                                				{
													if(client.getDeviceFarmCheck()!=null && client.getDeviceFarmCheck().equalsIgnoreCase("Y")){
                                				%>
                     								<option data_deviceFarm='<%=Utilities.escapeXml(client.getDeviceFarmCheck())%>' value='<%=Utilities.escapeXml(client.getHostName())%>'><%=Utilities.escapeXml(client.getName())%>&nbsp;[Device Farm]</option>
                     								<%}else{ %>
                                					<option data_deviceFarm='<%=Utilities.escapeXml(client.getDeviceFarmCheck())%>' value='<%=Utilities.escapeXml(client.getHostName())%>'><%=Utilities.escapeXml(client.getName())%></option>
                                				<%
                     								}
                                				}
                                			}
                                		}
                            		}		
                            		
                            		
                            		
                            %>
                            </select>
                            	
                            </div>
                            
                        </div> 
                        <div class='fieldSection grid_7'> 
                            <div class='clear'></div> 
                            <div class='grid_2'><label for='lstBrowserType'>Browser</label></div> 
                            <div class='grid_4'> 
                                <select id='lstBrowserType' name='lstBrowserType' class='stdTextBox'>
                                	<option value='APPDEFAULT'>Use Application Default</option> 
                                    <option value='<%=BrowserType.CHROME %>'>Google Chrome</option> 
                                    <!-- Added by paneendra for Firefox browser starts  -->
								   <option value="<%=BrowserType.FIREFOX%>"><%=BrowserType.FIREFOX%></option>
								   <!-- Added by paneendra for Firefox browser ends  -->
                                    <option value='<%=BrowserType.IE %>'>Microsoft Internet Explorer</option> 
                                   <!-- Modified by Prem for TENJINCG-1246 starts -->
                                    <option value='<%=BrowserType.CE %>'>Microsoft Edge</option>
                                   <%--  <option value='<%=BrowserType.FIREFOX %>'>Mozilla Firefox</option>  --%>
                                <!-- Modified by Prem for TENJINCG-1246 ends -->
                                </select> 
                            </div> 
                            
					<input type="hidden" value='<%=appType%>' id='appType'>
					<input type="hidden" value='<%=appId%>' id=appId>
					<%if(appType==3 || appType==4) {%>
					<div class='clear'></div>
					<div class='grid_2 regDevice'>
						<label for='listDevice'>Device</label>
					</div>
					<div class='grid_4 regDevice'>
						<select id='listDevice' name='listDevice' class='stdTextBox'>
							<option value='-1'>-- Select One --</option>
							
						</select>
					</div>
					<%}%>
					
                            <div class='clear'></div> 
                            <div class='grid_4'> 
                                <input type='hidden' class='stdTextBox' id='txnMode' name='txnMode' value='<%=request.getParameter("txnMode")%>'/> 
                            </div> 
                        </div> 
                </fieldset> 
			<fieldset id='deviceFilter'>
				<legend>Device Filters Information</legend>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstOs'>Operating System</label>
					</div>
					<div class='grid_4'>
						<select id='lstOs' name='lstOs' class='stdTextBox'>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstOEM'>OEM</label>
					</div>
					<div class='grid_4'>
						<select id='lstOEM' name='lstOEM' class='stdTextBox'>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstScrnSize'>Screen Size</label>
					</div>
					<div class='grid_4'>
						<select id='lstScrnSize' name='lstScrnSize' class='stdTextBox'>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='lstNetwork'>Network</label>
					</div>
					<div class='grid_4'>
						<select id='lstNetwork' name='lstNetwork' class='stdTextBox'>
						</select>
					</div>
				</div>
				<div class='fieldSection grid_7'>
				</div>
				<div class='fieldSection grid_7'>
					<div class='clear'></div>
					<div class='grid_2'></div>
					<div class='grid_4'>
						<input type="button" id="btnSearchFilter" value="Go"
							class="imagebutton ok">
							<input type='button' id='btnReset' value='Reset' class='imagebutton reset'/>
					</div>
				</div>
			</fieldset>
                <fieldset> 
                    <legend>The following functions are currently being learnt</legend> 
                     
                     
                     
                    <div class='fieldSection grid_15' id='modulesInfo'> 
                        <table class='display sortable-table tjn-default-data-table sequence' cellspacing='0' width='100%' id='modules_table'> 
                            <thead> 

                            <% if(modules != null)   
                            {   
                            %>   

                                <tr> 
                                    <th>Transaction Mode</th> 
                                    <th>Function Code</th> 
                                    <th>Function Name</th> 
                                    <th>Current Status</th> 
                                </tr> 

                            <%}   
                            %>   
                                
                            </thead> 
                            <tbody> 
                                <% int i=0; 

                                if(modules != null)   
                                {   

                                    for(Module m:modules){
                                    	ModuleBean module = m.convertToBean();
                                        i++; 
                                    %> 
                                    <tr data-module-code='<%= Utilities.escapeXml(module.getModuleCode()) %>' > 
                                       	<td>GUI</td>
                                        <td class='tiny'><%=Utilities.escapeXml( module.getModuleCode()) %></td> 
                                        <td><%=Utilities.escapeXml( module.getModuleName()) %></td> 
                                        <td> </td> 
                                    </tr> 
                                    <% 

                                    }   
                                }   
                                  
                               
                               
                                %> 
                            </tbody> 
                        </table> 
                        <div class='emptysmall'></div> 
                        <div id='progressbar' style='display:none;text-align:center'> 
                            <img src='images/inprogress.gif' alt='Task in Progress. Please wait' title='Please wait' id='imgLoader'/> 
                        </div> 
                    </div> 
                     
                </fieldset> 
            </div> 
        </form> 
    </body> 
</html> 
