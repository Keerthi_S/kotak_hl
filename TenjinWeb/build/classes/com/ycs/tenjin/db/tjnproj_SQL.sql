--------------------------------------------------------
--  File created - Monday-March-09-2015   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table DOMAINS
--------------------------------------------------------
CREATE TABLE [dbo].[DOMAINS](
	[DOMAIN_NAME] [varchar](100) NULL,
	[DOMAIN_CREATE_DATE] [datetime] NULL,
	[DOMAIN_CREATED_BY] [varchar](10) NULL
)

--------------------------------------------------------
--  DDL for Table HIST_TESTDATA
--------------------------------------------------------
CREATE TABLE [dbo].[HIST_TESTDATA](
	[SCRIPT_ID] [varchar](20) NULL,
	[ITERATION] [numeric](18, 0) NULL,
	[FIELD_NAME] [varchar](300) NULL,
	[DATA_VAL] [varchar](500) NULL,
	[DATA_SEQ] [numeric](18, 0) NULL,
	[RUN_ID] [numeric](18, 0) NULL
)

--------------------------------------------------------
--  DDL for Table ITERATIONLOG
--------------------------------------------------------
CREATE TABLE [dbo].[ITERATIONLOG](
	[ITERATION_NO] [numeric](18, 0) NULL,
	[CONTEXT] [varchar](250) NULL,
	[RESULT] [char](1) NULL,
	[ERRORMSG] [varchar](4000) NULL,
	[SCREENSHOT] [varbinary](max) NULL,
	[SCRIPT_ID] [varchar](30) NULL,
	[RUN_ID] [numeric](18, 0) NULL
)

--------------------------------------------------------
--  DDL for Table MASPROJECTROLES
--------------------------------------------------------
CREATE TABLE [dbo].[MASPROJECTROLES](
	[ROLE_ID] [numeric](18, 0) NULL,
	[ROLE_DESC] [varchar](30) NULL
) 

--------------------------------------------------------
--  DDL for Table MASSCRIPT
--------------------------------------------------------
CREATE TABLE [dbo].[MASSCRIPT](
	[PRJ_ID] [numeric](18, 0) NULL,
	[SCRIPT_ID] [varchar](25) NULL,
	[SCRIPT_NAME] [varchar](250) NULL,
	[SCRIPT_MODULE] [varchar](10) NULL,
	[SCRIPT_OWNER] [varchar](100) NULL,
	[SCRIPT_RUN_STAT] [varchar](3) NULL,
	[SCRIPT_DESCRIPTION] [varchar](1000) NULL,
	[SCRIPT_LATEST_STATUS] [varchar](10) NULL,
	[DATA_FILE_PATH] [varchar](500) NULL,
	[CONTEXT] [varchar](250) NULL,
	[SCRIPT_APP] [numeric](18, 0) NULL,
	[EXEC_FLAG] [varchar](1) NULL,
	[DATA_CONTENT] [varchar](4000) NULL,
	[EXEC_CONTEXT] [varchar](30) NULL,
	[SUB_CONTEXT] [varchar](250) NULL,
	[TEST_TYPE] [varchar](20) NULL,
	[EXP_RESULT] [varchar](200) NULL
)

--------------------------------------------------------
--  DDL for Table MASTESTRUNS
--------------------------------------------------------
CREATE TABLE [dbo].[MASTESTRUNS](
	[RUN_ID] [numeric](18, 0) NULL,
	[RUN_PRJ_ID] [numeric](18, 0) NULL,
	[RUN_TS_REC_ID] [numeric](18, 0) NULL,
	[RUN_START_TIME] [datetime] NULL,
	[RUN_END_TIME] [datetime] NULL,
	[RUN_USER] [varchar](20) NULL,
	[RUN_STATUS] [varchar](10) NULL
)

--------------------------------------------------------
--  DDL for Table PROJECTAUTS
--------------------------------------------------------
CREATE TABLE [dbo].[PROJECTAUTS](
	[PRJ_ID] [numeric](18, 0) NULL,
	[APP_ID] [numeric](18, 0) NULL
)

--------------------------------------------------------
--  DDL for Table PROJECTS
--------------------------------------------------------
CREATE TABLE [dbo].[PROJECTS](
	[PRJ_ID] [numeric](18, 0) NULL,
	[PRJ_NAME] [varchar](60) NULL,
	[PRJ_TYPE] [varchar](7) NULL,
	[PRJ_CREATE_DATE] [datetime] NULL,
	[PRJ_DOMAIN] [varchar](300) NULL,
	[PRJ_OWNER] [varchar](60) NULL,
	[PRJ_DB_SERVER] [varchar](200) NULL,
	[PRJ_DESC] [varchar](1000) NULL
)

--------------------------------------------------------
--  DDL for Table PROJECTUSERS
--------------------------------------------------------
CREATE TABLE [dbo].[PROJECTUSERS](
	[PRJ_ID] [numeric](18, 0) NULL,
	[USER_ID] [varchar](30) NULL,
	[USER_ROLE] [varchar](30) NULL
)

--------------------------------------------------------
--  DDL for Table RESVALMAP
--------------------------------------------------------
CREATE TABLE [dbo].[RESVALMAP](
	[TSTEP_REC_ID] [numeric](18, 0) NULL,
	[RES_VAL_ID] [numeric](18, 0) NULL
)

--------------------------------------------------------
--  DDL for Table RESVALRESULTS
--------------------------------------------------------
CREATE TABLE [dbo].[RESVALRESULTS](
	[RUN_ID] [numeric](18, 0) NULL,
	[RES_VAL_ID] [numeric](18, 0) NULL,
	[TSTEP_REC_ID] [numeric](18, 0) NULL,
	[RES_VAL_PAGE] [varchar](100) NULL,
	[RES_VAL_FIELD] [varchar](100) NULL,
	[RES_VAL_EXP_VAL] [varchar](1000) NULL,
	[RES_VAL_ACT_VAL] [varchar](1000) NULL,
	[RES_VAL_STATUS] [varchar](30) NULL,
	[ITERATION] [numeric](18, 0) NULL
)

--------------------------------------------------------
--  DDL for Table RUNRESULT_TS_TXN
--------------------------------------------------------
CREATE TABLE [dbo].[RUNRESULT_TS_TXN](
	[RUN_ID] [numeric](18, 0) NULL,
	[TXN_NO] [numeric](18, 0) NULL,
	[TSTEP_REC_ID] [numeric](18, 0) NULL,
	[TSTEP_RESULT] [char](1) NULL,
	[TSTEP_MESSAGE] [varchar](4000) NULL,
	[TSTEP_SCRSHOT] [varbinary](max) NULL
)

--------------------------------------------------------
--  DDL for Table RUNRESULTS_TC
--------------------------------------------------------
CREATE TABLE [dbo].[RUNRESULTS_TC](
	[RUN_ID] [numeric](18, 0) NULL,
	[TC_REC_ID] [numeric](18, 0) NULL,
	[TC_TOTAL_STEPS] [numeric](18, 0) NULL,
	[TC_TOTAL_EXEC] [numeric](18, 0) NULL,
	[TC_TOTAL_PASS] [numeric](18, 0) NULL,
	[TC_TOTAL_FAIL] [numeric](18, 0) NULL,
	[TC_TOTAL_ERROR] [numeric](18, 0) NULL,
	[TC_STATUS] [varchar](30) NULL
)

--------------------------------------------------------
--  DDL for Table RUNRESULTS_TS
--------------------------------------------------------
CREATE TABLE [dbo].[RUNRESULTS_TS](
	[RUN_ID] [numeric](18, 0) NULL,
	[TC_REC_ID] [numeric](18, 0) NULL,
	[TSTEP_REC_ID] [numeric](18, 0) NULL,
	[TSTEP_TOTAL_TXN] [numeric](18, 0) NULL,
	[TSTEP_TOTAL_EXEC] [numeric](18, 0) NULL,
	[TSTEP_TOTAL_PASS] [numeric](18, 0) NULL,
	[TSTEP_TOTAL_FAIL] [numeric](18, 0) NULL,
	[TSTEP_TOTAL_ERROR] [numeric](18, 0) NULL,
	[TSTEP_STATUS] [varchar](30) NULL
)

--------------------------------------------------------
--  DDL for Table TCFOLDER
--------------------------------------------------------
CREATE TABLE [dbo].[TCFOLDER](
	[TC_FOLDER_ID] [numeric](18, 0) NULL,
	[TC_REC_ID] [numeric](18, 0) NULL,
	[TC_PAR_FOLDER_ID] [numeric](18, 0) NULL,
	[TC_CHILD_FOLDER_ID] [numeric](18, 0) NULL,
	[TC_FOLDER_NAME] [varchar](30) NULL
)

--------------------------------------------------------
--  DDL for Table TESTCASES
--------------------------------------------------------
CREATE TABLE [dbo].[TESTCASES](
	[TC_REC_ID] [numeric](18, 0) NULL,
	[TC_REC_TYPE] [varchar](3) NULL,
	[TC_ID] [varchar](100) NULL,
	[TC_FOLDER_ID] [numeric](18, 0) NULL,
	[TC_PRJ_ID] [numeric](18, 0) NULL,
	[TC_NAME] [varchar](200) NULL,
	[TC_TYPE] [varchar](60) NULL,
	[TC_PRIORITY] [varchar](10) NULL,
	[TC_REVIEWED] [varchar](3) NULL,
	[TC_STATUS] [varchar](60) NULL,
	[TC_CREATED_ON] [datetime] NULL,
	[TC_MODIFIED_ON] [datetime] NULL,
	[TC_DESC] [varchar](3000) NULL,
	[TC_CREATED_BY] [varchar](40) NULL,
	[TC_MODIFIED_BY] [varchar](40) NULL
)

--------------------------------------------------------
--  DDL for Table TESTDATA
--------------------------------------------------------
CREATE TABLE [dbo].[TESTDATA](
	[SCRIPT_ID] [varchar](20) NULL,
	[ITERATION] [numeric](18, 0) NULL,
	[FIELD_NAME] [varchar](300) NULL,
	[DATA_VAL] [varchar](500) NULL,
	[DATA_SEQ] [numeric](18, 0) NULL
)

--------------------------------------------------------
--  DDL for Table TESTSETMAP
--------------------------------------------------------
CREATE TABLE [dbo].[TESTSETMAP](
	[TSM_PRJ_ID] [numeric](18, 0) NULL,
	[TSM_TS_REC_ID] [numeric](18, 0) NULL,
	[TSM_TC_REC_ID] [numeric](18, 0) NULL,
	[TSM_TC_SEQ] [numeric](18, 0) NULL
)

--------------------------------------------------------
--  DDL for Table TESTSETS
--------------------------------------------------------
CREATE TABLE [dbo].[TESTSETS](
	[TS_NAME] [varchar](100) NULL,
	[TS_REC_ID] [numeric](18, 0) NULL,
	[TS_REC_TYPE] [varchar](10) NULL,
	[TS_FOLDER_ID] [numeric](18, 0) NULL,
	[TS_STATUS] [varchar](60) NULL,
	[TS_CREATED_ON] [datetime] NULL,
	[TS_MODIFIED_ON] [datetime] NULL,
	[TS_CREATED_BY] [varchar](40) NULL,
	[TS_MODIFIED_BY] [varchar](40) NULL,
	[TS_PRIORITY] [varchar](10) NULL,
	[TS_REVIEWED] [varchar](3) NULL,
	[TS_DESC] [varchar](3000) NULL,
	[TS_PRJ_ID] [numeric](18, 0) NULL,
	[TS_TYPE] [varchar](60) NULL
)

--------------------------------------------------------
--  DDL for Table TESTSTEPS
--------------------------------------------------------
CREATE TABLE [dbo].[TESTSTEPS](
	[TSTEP_REC_ID] [numeric](18, 0) NULL,
	[TC_REC_ID] [numeric](18, 0) NULL,
	[TSTEP_SHT_DESC] [varchar](100) NULL,
	[TSTEP_DATA_ID] [varchar](20) NULL,
	[TSTEP_TYPE] [varchar](50) NULL,
	[TSTEP_DESC] [varchar](1000) NULL,
	[TSTEP_EXP_RESULT] [varchar](1000) NULL,
	[TSTEP_ACT_RESULT] [varchar](1000) NULL,
	[APP_ID] [varchar](40) NULL,
	[FUNC_CODE] [varchar](40) NULL,
	[TXN_CONTEXT] [varchar](1000) NULL,
	[EXEC_CONTEXT] [varchar](1000) NULL,
	[TXNMODE] [char](1) NULL,
	[TSTEP_ID] [varchar](20) NULL,
	[TSTEP_OPERATION] [varchar](50) NULL,
	[TSTEP_AUT_LOGIN_TYPE] [varchar](30) NULL
)

--------------------------------------------------------
--  DDL for Table MASTESTDATAPATH
--------------------------------------------------------
CREATE TABLE [dbo].[MASTESTDATAPATH](
	[PRJ_ID] [numeric](18, 0) NULL,
	[APP_ID] [numeric](18, 0) NULL,
	[FUNC_CODE] [varchar](20) NULL,
	[TEST_DATA_PATH] [varchar](500) NULL
)