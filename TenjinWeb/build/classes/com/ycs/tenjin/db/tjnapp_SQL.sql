--------------------------------------------------------
--  File created - Monday-March-09-2015   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table FIELDS
--------------------------------------------------------

CREATE TABLE [dbo].[FIELDS](
	[FLD_APP] [numeric](18, 0) NULL,
	[FLD_FUNC_CODE] [varchar](10) NULL,
	[FLD_SEQ_NO] [numeric](18, 0) NULL,
	[FLD_UNAME] [varchar](100) NULL,
	[FLD_LABEL] [varchar](100) NULL,
	[FLD_TAB_ORDER] [numeric](18, 0) NULL,
	[FLD_INSTANCE] [numeric](18, 0) NULL,
	[FLD_INDEX] [numeric](18, 0) NULL,
	[FLD_UID_TYPE] [varchar](20) NULL,
	[FLD_UID] [varchar](100) NULL,
	[FLD_TYPE] [varchar](100) NULL,
	[FLD_PAGE_AREA] [varchar](100) NULL,
	[TXNMODE] [char](1) NULL
)

--------------------------------------------------------
--  DDL for Table MAPDETAIL
--------------------------------------------------------
CREATE TABLE [dbo].[MAPDETAIL](
	[MAP_ID] [numeric](18, 0) NULL,
	[MODE1_PAGEAREA] [varchar](100) NULL,
	[MODE1_FIELD] [varchar](100) NULL,
	[MODE2_PAGEAREA] [varchar](100) NULL,
	[MODE2_FIELD] [varchar](100) NULL
) 

--------------------------------------------------------
--  DDL for Table MAPMASTER
--------------------------------------------------------
CREATE TABLE [dbo].[MAPMASTER](
	[MAP_ID] [numeric](18, 0) NULL,
	[APP_ID] [numeric](18, 0) NULL,
	[MODULE_CODE] [varchar](100) NULL,
	[MODE1] [char](1) NULL,
	[MODE2] [char](1) NULL
)

--------------------------------------------------------
--  DDL for Table MASAPPLICATION
--------------------------------------------------------
CREATE TABLE [dbo].[MASAPPLICATION](
	[APP_ID] [numeric](18, 0) NULL,
	[APP_NAME] [varchar](100) NULL,
	[APP_URL] [varchar](200) NULL,
	[APP_LOGIN_REQD] [varchar](3) NULL,
	[APP_LOGIN_NAME] [varchar](50) NULL,
	[APP_PASSWORD] [varchar](200) NULL
)

--------------------------------------------------------
--  DDL for Table MASCONNPOOL
--------------------------------------------------------
CREATE TABLE [dbo].[MASCONNPOOL](
	[POOL_ID] [varchar](40) NULL,
	[POOL_NAME] [varchar](100) NULL,
	[HOST_NAME] [varchar](100) NULL,
	[SID_NAME] [varchar](40) NULL,
	[USER_NAME] [varchar](40) NULL,
	[PASS_WORD] [varchar](40) NULL
)

--------------------------------------------------------
--  DDL for Table MASMODULE
--------------------------------------------------------
CREATE TABLE [dbo].[MASMODULE](
	[APP_ID] [numeric](18, 0) NULL,
	[MODULE_CODE] [varchar](100) NULL,
	[MODULE_NAME] [varchar](200) NULL,
	[MODULE_MENU_CONTAINER] [varchar](200) NULL,
	[WS_URL] [varchar](1000) NULL,
	[WS_OPERATION] [varchar](500) NULL,
	[WS_XML] [varchar](max) NULL,
	[END_TIMESTAMP] [datetime] NULL,
	[WS_END_TIMESTAMP] [datetime] NULL
)

--------------------------------------------------------
--  DDL for Table MASRESVALDEF
--------------------------------------------------------
CREATE TABLE [dbo].[MASRESVALDEF](
	[RVD_ID] [numeric](18, 0) NULL,
	[RVD_NAME] [varchar](100) NULL,
	[RVD_APP_ID] [numeric](18, 0) NULL,
	[RVD_DESC] [varchar](1000) NULL,
	[RVD_TYPE] [varchar](100) NULL
)

--------------------------------------------------------
--  DDL for Table PAGEAREAS
--------------------------------------------------------
CREATE TABLE [dbo].[PAGEAREAS](
	[PA_APP] [numeric](18, 0) NULL,
	[PA_FUNC_CODE] [varchar](10) NULL,
	[PA_NAME] [varchar](100) NULL,
	[PA_PARENT] [varchar](100) NULL,
	[PA_SEQ_NO] [numeric](18, 0) NULL,
	[PA_TYPE] [varchar](25) NULL,
	[PA_WAY_IN] [varchar](100) NULL,
	[PA_WAY_OUT] [varchar](100) NULL,
	[PA_SOURCE] [varchar](max) NULL,
	[PA_PATH] [varchar](400) NULL,
	[TXNMODE] [char](1) NULL,
	[PA_LABEL] [varchar](30) NULL,
	[PA_SCREEN_TITLE] [varchar](200) NULL
)

--------------------------------------------------------
--  DDL for Table RESVALCONFIG
--------------------------------------------------------
CREATE TABLE [dbo].[RESVALCONFIG](
	[RES_VAL_ID] [numeric](18, 0) NULL,
	[RES_VAL_SEQ] [numeric](18, 0) NULL,
	[RES_VAL_TYPE] [varchar](30) NULL,
	[RES_VAL_FUNC] [varchar](30) NULL,
	[RES_VAL_FLD] [varchar](100) NULL,
	[RES_VAL_PA] [varchar](100) NULL
)

--------------------------------------------------------
--  DDL for Table TABLECOLS
--------------------------------------------------------
CREATE TABLE [dbo].[TABLECOLS](
	[APP_ID] [numeric](18, 0) NULL,
	[FUNC_CODE] [varchar](30) NULL,
	[TABLE_UID] [varchar](100) NULL,
	[TABLE_COL_NAME] [varchar](100) NULL,
	[TABLE_COL_TYPE] [varchar](50) NULL,
	[COL_INDEX] [numeric](18, 0) NULL
)

--------------------------------------------------------
--  DDL for Table USRAUTCREDENTIALS
--------------------------------------------------------
CREATE TABLE [dbo].[USRAUTCREDENTIALS](
	[USER_ID] [varchar](20) NULL,
	[APP_ID] [numeric](18, 0) NULL,
	[APP_LOGIN_NAME] [varchar](50) NULL,
	[APP_PASSWORD] [varchar](200) NULL,
	[APP_USER_TYPE] [varchar](7) NULL
)

--------------------------------------------------------
--  DDL for Table WRK_FIELDS
--------------------------------------------------------
CREATE TABLE [dbo].[WRK_FIELDS](
	[FLD_APP] [numeric](18, 0) NULL,
	[FLD_FUNC_CODE] [varchar](10) NULL,
	[FLD_SEQ_NO] [numeric](18, 0) NULL,
	[FLD_UNAME] [varchar](100) NULL,
	[FLD_LABEL] [varchar](100) NULL,
	[FLD_TAB_ORDER] [numeric](18, 0) NULL,
	[FLD_INSTANCE] [numeric](18, 0) NULL,
	[FLD_INDEX] [numeric](18, 0) NULL,
	[FLD_UID_TYPE] [varchar](20) NULL,
	[FLD_UID] [varchar](100) NULL,
	[FLD_TYPE] [varchar](100) NULL,
	[FLD_PAGE_AREA] [varchar](100) NULL,
	[TXNMODE] [char](1) NULL
)

