<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testcase_view.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
 19-11-2020             Priyanka                TENJINCG-1231
  -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TestCase Details</title>
	
	<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
		<link rel="Stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/testcase.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-ui-1.13.0.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src=' ${pageContext.request.contextPath}/js/bootstrap.min.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/tenjin-aut-selector.js'></script>
		<script type='text/javascript' src='js/pages/v2/testcase_view.js'></script>
		<script src="js/tables.js"></script>
</head>
<body>
 <!-- commented by shruthi for TCGST-52 starts -->
<!-- added by shruthi for CSRF token starts -->
<!--  uncommented by Priyanka for TestStep Deletion during DB Migration starts -->
 <%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %> 
    <!--  uncommented by Priyanka for TestStep Deletion during DB Migration ends -->
<!-- added by shruthi for CSRF token ends -->
<!--  commented by shruthi for TCGST-52 ends -->
	<%	Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
		List<String> listExceptions =null;
		if(map!=null)
		{
			listExceptions = (List<String>) map.get("listExceptions");
		}
	%>
	<div class='title'>
		<p>Test Case Details</p>
			 
	</div>
	<form name='main_form' id='main_form' action='TestCaseServletNew' method='POST'>
		<div class='toolbar'>
			<!--  commented by shruthi for TCGST-52 starts -->
			<!-- added by shruthi for CSRF token starts -->
			<!--  uncommented by Priyanka for TestStep Deletion during DB Migration starts -->
		      <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		      <!--  uncommented by Priyanka for TestStep Deletion during DB Migration ends -->
		     <!-- added by shruthi for CSRF token ends -->
		      <!-- commented by shruthi for TCGST-52 ends -->
			<input type='button' value='Back' id='btnBack' class='imagebutton back'/>
			<input type='button' value='Save' id='btnSave' class='imagebutton save'/>
			 
				<input type='button' value='Copy' class='imagebutton copy1' id='btnSaveAs'/>
				 
			<input type='button' value='Execution History' id='btnTcExecuteHistory' class='imagebutton history'/>
			 
			<c:if test="${not empty testcase && not empty teststep && project.state=='A'}">
			<input type='button' value='Run' class='imagebutton runtestset' id='btnRun'/>
			</c:if>
			<c:if test="${testcase.storage eq 'Remote'}">
            <input type='button' value='Synchronize' class='imagebutton refresh' id='btnSync'/>
            
            </c:if>
            <input type='button' value='Change History' id='history' class='imagebutton download'/>
			<%if(listExceptions!=null && listExceptions.size()>0){ %>
				<input type='button' value='Logs' class='imagebutton list' id='btnLogs'/>
			<%} %>
			<img src='images/inprogress.gif' id='img_download_loader' style='display:none;'/>
		</div>
		
		<input type='hidden' id='tjn-status' value='${screenState.status }' />
		<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
		<input type='hidden' id='domain' name='domain' value='${project.domain}'/>
		<input type='hidden' id='projectName' name='projectName' value='${project.name }'/>
		<input type='hidden' id='projectId' name='projectId' value='${project.id }'/>
		<input type='hidden' id='user' name='user' value='${user }'/>
		<input type='hidden' id='tcid' name='tcid' value='${fn:escapeXml(testcase.tcId) }'/>
		<!-- Added by Priyanka for TENJINCG-1231 starts -->
		<input type='hidden' name='edate' id='endDate' value='${edate }'/>
		<!-- Added by Priyanka for TENJINCG-1231 ends -->
		<div id='user-message'></div>
		
		<%if(listExceptions!=null && listExceptions.size()>0){ %>
			<div id="importLogs" class="modal fade">
					<div class="modal-dialog" style='height: 400px; width:600px;'>
						<div class="modal-content">
							<div class="modal-header">
								<p>Import Logs</p>
							</div>
							
							<div class="modal-body">
							<%for(int i=0;i<listExceptions.size();i++){ %>
								<p><%=listExceptions.get(i) %></p>
							<%} %>
							</div>
							<div class="modal-footer">
								<input type="button" value='Close' class="imagebutton cancel" data-dismiss="modal"> 
							</div>
						</div>
					</div>
				</div>
				<%} %>
		
		<div class='form container_16'>
					<div class='fieldSection grid_8'>
						<div class='clear'></div>
						<div class='grid_2'><label for='txtTestCaseId'>Test Case ID</label></div>
						<div class='grid_5'>
							<input type='text' id='txtTestCaseId' name='tcId' value='${fn:escapeXml(testcase.tcId) }' disabled='disabled' class='stdTextBox'  title='Test Case ID'/>
						</div>
					</div>
					<input type='hidden' id='txtTcRecId' name='tcId' value='${testcase.tcRecId }' disabled='disabled' class='stdTextBox' mandatory='yes' title='Test Case ID'/>
					
					<div class='fieldSection grid_8'>
						<div class='clear'></div>
						<div class='grid_3'><label for='lstTestCaseType'>Type</label></div>
						<div class='grid_4'>
						<input type='hidden' id='selectedCaseType' value='${testcase.tcType }'/>
						 <select class='stdTextBoxNew' id='lstTestCaseType' name='tcType' onload="defaultselect()">
							<option value='Acceptance'>Acceptance</option>
								<option value='Accessibility'>Accessibility</option>
								<option value='Compatibility'>Compatibility</option>
								<option value='Regression'>Regression</option>
								<option value='Functional'>Functional</option>
								<option value='Smoke'>Smoke</option>
								<option value='Usability'>Usability</option>
								<option value='Other'>Other</option>
						</select>
						</div>
					</div>
					<div class='fieldSection grid_16'> 
						<div class='grid_2'><label for='txtTestCaseName'>Name</label></div>
						<div class='grid_12'>
								<input type='text' id='txtTestCaseName' name='tcName' value='${fn:escapeXml(testcase.tcName) }' class='stdTextBox long'  title='Name' maxlength="200"  style='width:740px' mandatory='yes'/>
						</div>
					
						<div class='clear'></div>
						<div class='grid_2'><label for='txtTestCaseDesc'>Description</label></div>
						<div class='grid_12'>
							<textarea id='txtTestCaseDesc' name='tcDesc' class='stdTextBox' style='width:740px;height:50px;' maxlength="3000">${testcase.tcDesc }</textarea>
						</div>
					</div>
					<div class='fieldSection grid_8'> 
					 
						<div class='clear'></div>
						<div class='grid_2'><label for='lstPriority'>Priority</label></div>
				   	 <div class='grid_5'>
				   	 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<input type='hidden' id='selectedPriority' value='${testcase.tcPriority }' maxlength="10"/>
						 <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<select class='stdTextBoxNew' id='lstPriority' name='tcPriority' onload="defaultselect()">
							   <option value='Low'>Low</option>
							   <option value='Medium'>Medium</option>
							   <option value='High'>High</option>
							</select>
						</div>
						<div class='clear'></div>
						<div class='grid_2'>
						<label for='label'>Label</label></div>
						<div class='grid_4'>
							<input type='text' id='label' name='label' class='stdTextBox'  title='label' maxlength="100" value='${fn:escapeXml(testcase.label) }' />
						</div>
					</div>
					<div class='fieldSection grid_8'>
						<div class='grid_3'>
						<label for='aut'>Application</label></div>
						<div class='grid_4'>
							<tjn:applicationSelector  title='Application'  cssClass="stdTextBoxNew"  projectId='${project.id }'  id="aut" name="aut" defaultValue="${testcase.appId}"/>						
						</div>
						<c:if test="${testcase.totalSteps gt 1}">
						<div class='clear'></div>
						<div class='grid_3'><label for='dependencyRules'>Default Dependency Rules</label></div>
				   	 	<div class='grid_4'>
							<select class='stdTextBoxNew' id='dependencyRules' name='dependencyRules'>
							   <option value='-1'>Always Execute</option>
							   <option value='Pass'>Execute if previous step Pass</option>
							   <option value='Fail'>Execute if previous step Fail</option>
							   <option value='Error'>Execute if previous step Error</option>
							</select>
						</div>
						</c:if>
					</div>
					
					<input type="hidden" value='${testcase.tcPresetRules}' id='selectedRules'/>
					<input type="hidden" value='${testcase.totalSteps}' id='totalSteps'/>
			 
					<div class='fieldSection grid_16'> 
				<fieldset>
					<legend>Details</legend>
					<div>
						<ul id='tabs'>
							<li>
							<a href='#test-steps-tab' class='teststeps teststep' id='test-steps-tab-anchor'>Test Steps</a>
							<li>
							<a href='#test-details-tab' class='testdetails details' id='test-details-tab-anchor'>Audit History</a>
						</ul>
						<div id='test-steps-tab' class='tab-section'>
							<div class='toolbar'>
								<input type='button' value='New' id='btnAddStep' class='imagebutton new'/>
								<input type='button' value='Remove' id='btnRemoveStep' class='imagebutton delete'/>
								<input type='button' value='Refresh' id='btnRefreshSteps' class='imagebutton refresh'/>
								<p id='stepMessage'></p>
								<div id='user-message-steps'></div>
							</div>
							<div id='dataGrid'>
									<table id='test-steps-table' class='display sortable-table tjn-default-data-table'>
										<thead>
											<tr>
												<th class='tiny text-center nosort'>#</th>
												<th class='table-row-selector nosort'><input type='checkbox'  id='chk_all_steps' class='tbl-select-all-rows'/></th>
												<th class='nosort'>Test Step ID</th>
												<th class='nosort'>Summary</th>
												<th class='nosort'>Application</th>
												<th class='nosort'>API/Function</th>
												 <!-- Commented  By Priyanka starts -->
												<%-- <c:choose>
						                      	  <c:when test="${testcase.mode eq 'GUI'}">
														<th class='nosort'>Function</th>
												  </c:when>
												<c:otherwise>
													<th class='nosort'>API</th>
												</c:otherwise>
												</c:choose> --%>
												<!-- Commented  By Priyanka ends -->
												<th class='nosort'>Mode</th>
												<th class='nosort'>Rules</th>
											</tr>
										</thead>
										<tbody id='tblNaviflowModules_body'>
											<c:forEach items="${teststep }" var="teststep" >
											<tr data-record-id='${teststep.recordId}' data-sequence='${teststep.sequence}' id='${teststep.recordId}' data-rules='${teststep.customRules}'>
												<td class='tiny sequence text-center'>${teststep.sequence }</td>
												<td class='table-row-selector'><input type='checkbox' data-record-id='${teststep.recordId}'/></td>
												<td ><a href='TestStepServlet?t=view&key=${fn:escapeXml(teststep.recordId)}&callback=testcase' title='${fn:escapeXml(teststep.id) }'>${fn:escapeXml(teststep.id) }</a></td>
												<td ><a href='TestStepServlet?t=view&key=${teststep.recordId}&callback=testcase' title='${teststep.shortDescription }'>${teststep.shortDescription }</a></td>
												<td>${fn:escapeXml(teststep.appName) }</td>
												<td>${teststep.moduleCode }</td>
												<td>${teststep.txnMode }</td>
												<td><c:out value="${not empty teststep.customRules ?'Yes':'No'}"/></td>
											</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							
							
						</div>
						<div id='test-details-tab' class='tab-section grid_15'>
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label for='txtCreatedBy'>Created By</label></div>
								<div class='grid_4'>
									<input type='text' id='txtCreatedBy' name='tcCreatedBy' class='stdTextBox'  title='Created By' value='${testcase.tcCreatedBy }' disabled='disabled'/>
								</div>
							</div>
							
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label for='txtCreatedOn'>Created On</label></div>
								<div class='grid_4'>
									<input type='text' value='${testcase.tcCreatedOn }' id='txtCreatedOn' name='tcCreatedOn' title='Created On' mandatory= 'no' disabled='true' class='stdTextBox' />
								</div>		
							</div>
							<!-- Added by Preeti for TENJINCG-972 starts -->
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label for='txtUpdatedBy'>Last Updated By</label></div>
								<div class='grid_4'>
									<input type='text' id='txtUpdatedBy' name='tcUpdatedBy' class='stdTextBox'  title='Updated By' value='${testcase.auditRecord.auditId eq 0 ? "N/A" : testcase.auditRecord.lastUpdatedBy }' disabled='disabled'/>
								</div>
							</div>
							
							<div class='fieldSection grid_7'>
								<div class='clear'></div>
								<div class='grid_2'><label for='txtUpdatedOn'>Last Updated On</label></div>
								<div class='grid_4'>
									<input type='text' value='${testcase.auditRecord.auditId eq 0 ? "N/A" : testcase.auditRecord.lastUpdatedOn }' id='txtUpdatedOn' name='tcUpdatedOn' title='Updated On' mandatory= 'no' disabled='true' class='stdTextBox' />
								</div>	
							</div>
						 
						</div>
					
				</fieldset>
				</div>
		</div>
	</form>
	<div class='modalmask'></div>
        <div class='subframe' style='left:3%'>
            <iframe frameborder="2" scrolling="auto"  marginwidth="5" marginheight="5" style='height:500px;width:900px;' seamless="seamless" id='prj-sframe' src=''></iframe>
        </div>
        <div class='subframe' id='syncTestCases' style='position:absolute;top:100px;left:250px;'>
                
                <div class='subframe_title'><p>Please Confirm...</p></div>
                <div class='subframe_content'>
                    <div class='highlight-icon-holder'>
                        <img src='images/warning_confirm.png' alt='Please Confirm' />
                    </div>
                    <div class='subframe_single_message'>
                    <p>Synchronization will make you lose all the changes made to test case. Do you still want to proceed?</p>
                    </div>
                </div>
            <div class='subframe_actions'>
                <input type='button' id='btnCOk' value='Proceed anyway' class='imagebutton ok'/>
                <input type='button' id='btnTCancel' value='Do not Proceed' class='imagebutton cancel'/> 
            </div>
        </div>
		<div class='subframe' id='historyCases'>
			<iframe frameborder="2" scrolling="no" marginwidth="5"
				marginheight="5" style='height: 350px; width: 600px; overflow: hidden;'
				seamless="seamless" id='history-sframe' src=''>
			</iframe>
		</div>
</body>
</html>