

/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestDataPathHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 30-Nov-2017		       Leelaprasad		   		Newly added for TENJINCG-539
* 12-02-2018               Padmavathi              for TENJINCG-545
* 19-02-2018               Padmavathi              for TENJINCG-545
*/
package com.ycs.tenjin.handler;


import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutsHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.project.ProjectTestDataPath;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Utilities;

public class TestDataPathHandler {

	private static final Logger logger = LoggerFactory
			.getLogger(TestDataPathHandler.class);
	private String mode=null;
	private int applicationId=0;
	/*added by Padmavathi for TENJINCG-545 starts*/
	private String applicationName=null;
	/*added by Padmavathi for TENJINCG-545 ends*/
	public void updateTestDataPath(ProjectTestDataPath testDataPath) throws TenjinServletException, DatabaseException{
		
		//Validate the testDataPath
		this.validateTestDataPathObject(testDataPath);;
		/*Changed by leelaprasad for testdata path TENJINCG-549 starts
		Changed by leelaprasad for testdata path TENJINCG-549 starts*/
		if(mode==null){
			testDataPath.setAppId(applicationId);
			new ProjectHelper().persistTestDataPathForApplication(testDataPath);
			Aut aut =new AutHandler().getApplication(applicationId);
			aut.setTestDataPath(testDataPath.getTestDataPath());
			new ProjectHelper().updateAutUrl(aut, testDataPath.getProjectId());
		
		}else if( mode.equalsIgnoreCase("Function")){
			/*added by Padmavathi for TENJINCG-545 starts*/
			testDataPath.setAppId(applicationId);
			testDataPath.setAppName(applicationName);
			/*added by Padmavathi for TENJINCG-545 ends*/
			new ProjectHelper().persistTestDataPathForFunction(testDataPath);
		}/*	Added by Padmavathi for TENJINCG-545 starts*/
		else if(mode.equalsIgnoreCase("Group")){
			testDataPath.setAppId(applicationId);
			testDataPath.setAppName(applicationName);
			new ProjectHelper().persistTestDataPathForGroup(testDataPath);
		}
		/*	Added by Padmavathi for TENJINCG-545 ends*/
		
		//Do the update
	}
	
	private void validateTestDataPathObject(ProjectTestDataPath testDataPath) throws TenjinServletException, DatabaseException{
		//1. Validate the application
		int appId=testDataPath.getAppId();
		String appName = Utilities.trim(testDataPath.getAppName());
		String funcCode=Utilities.trim(testDataPath.getFuncCode());
		/*	Added by Padmavathi for TENJINCG-545 starts*/
		String groupName=Utilities.trim(testDataPath.getGroupName());
		/*	Added by Padmavathi for TENJINCG-545 ends*/
		 String testDataPathVal=null;
			if(testDataPath.getTestDataPath()!=null){
				testDataPathVal=Utilities.trim(testDataPath.getTestDataPath());
			}
		Aut aut = null;
		
		if(appName.length() > 0) {
			aut = new AutsHelper().hydrateProjectAut(testDataPath.getProjectId(), appName);
			if(aut == null) {
				throw new TenjinServletException("This application does not exist or is not mapped to this project. Please review your request and try again");
				
			}
			
			if(appId > 0 && appId != aut.getId()) {
				throw new TenjinServletException("This application Id and application name doesn't match. Please review your request and try again");
			}
			applicationId=aut.getId();
			/*added by Padmavathi for TENJINCG-545 starts*/
			applicationName=aut.getName();
			/*added by Padmavathi for TENJINCG-545 ends*/
		}
		
		if(appId > 0) {
			aut = new AutsHelper().hydrateProjectAut(testDataPath.getProjectId(), appId);
			if(aut == null) {
				throw new TenjinServletException("This application does not exist or is not mapped to this project. Please review your request and try again");
			}
			applicationId=aut.getId();
			/*added by Padmavathi for TENJINCG-545 starts*/
			applicationName=aut.getName();
			/*added by Padmavathi for TENJINCG-545 ends*/
		}
		/*	Added by Padmavathi for TENJINCG-545 starts*/
		if(funcCode.length()>0&&groupName.length()>0){
			throw new TenjinServletException("Please specify either funcCode or groupName.");
		}
		/*	Added by Padmavathi for TENJINCG-545 ends*/
		//3. Validate Function Code (if passed)
		if(funcCode.length()>0){
			/*	Modified by Padmavathi to handle exception starts*/
			try{
				new ModuleHandler().getModule(applicationId, funcCode);
			
				//if(module == null) {
				}catch(RecordNotFoundException re){
					Api api=new ApiHelper().hydrateApi(applicationId, funcCode);
					if(api==null){
					throw new TenjinServletException("This function or api does not exist under this application. Please review your request and try again");
					}
				}
			/*	Modified by Padmavathi to handle exception ends*/
			mode="Function";
		}
		/*	Added by Padmavathi for TENJINCG-545 starts*/
		if(groupName.length()>0){
			try {
				if(new ModulesHelper().hydrateGroupInfo(applicationId, groupName)==null){
					throw new TenjinServletException("This group does not exist under this application. Please review your request and try again");
				}
			} catch (SQLException e) {
				throw new DatabaseException("An internal error occurred. Please contact Tenjin Support.");
			}
			mode="Group";
		}
		/*	Added by Padmavathi for TENJINCG-545 ends*/
		if(testDataPath.getTestDataPath()==null || !(testDataPathVal.length()>0)){
			throw new TenjinServletException("Testdatapath is mandatory. Please review your request and try again");
		}
		
		
	}
	/*	Added by Padmavathi for TENJINCG-540 starts*/
	public ArrayList<ProjectTestDataPath> getProjectTestData(ProjectTestDataPath testDataPath) throws  DatabaseException, TenjinServletException, DuplicateRecordException{
		ArrayList<ProjectTestDataPath>	testDataPaths=new ArrayList<>();
		if(testDataPath.getAppId()==0 && !(Utilities.trim(testDataPath.getFuncCode()).length()>0)&&!(Utilities.trim(testDataPath.getGroupName()).length()>0)){
				testDataPaths=new ProjectHelper().hydrateTestDataPathsForProject(testDataPath.getProjectId());
		}
		else{
			 this.validateProjectTestDataPath(testDataPath);
			 if(testDataPath.getAppId()!=0&&!(Utilities.trim(testDataPath.getFuncCode()).length()>0)&&!(Utilities.trim(testDataPath.getGroupName()).length()>0)){
		     	testDataPaths=new ProjectHelper().fetchAppTestDataPath(testDataPath.getAppId(),testDataPath.getProjectId());
	          }
			else if(Utilities.trim(testDataPath.getFuncCode()).length()>0){
				
				testDataPath= new ProjectHelper().hydrateTestDataPathForFunction(testDataPath);
				testDataPaths.add(testDataPath);
				}
			 /*	Added by Padmavathi for TENJINCG-545 starts*/
			else if(Utilities.trim(testDataPath.getGroupName()).length()>0){
				testDataPaths=new ProjectHelper().hydrateTestDataPathForFunctionGroup(testDataPath);
			}
				
			 /*	Added by Padmavathi for TENJINCG-545 ends*/
		}
		return testDataPaths;
		
	}

	public void validateProjectTestDataPath(ProjectTestDataPath testDataPath) throws DatabaseException, TenjinServletException, DuplicateRecordException{
		String funcCode=Utilities.trim(testDataPath.getFuncCode());
		/*	Added by Padmavathi for TENJINCG-545 starts*/
		String groupName=Utilities.trim(testDataPath.getGroupName());
		/*	Added by Padmavathi for TENJINCG-545 ends*/
		 if(funcCode.length()>0&&testDataPath.getAppId()==0){
			   throw new TenjinServletException("AppId[appId] is mandatory");
		   }
		   Aut aut=new AutsHelper().hydrateProjectAut(testDataPath.getProjectId(), testDataPath.getAppId());
		   if(aut==null){
			   throw new TenjinServletException("This application does not exist or is not mapped to this project. Please review your request and try again");
		   }
		   testDataPath.setAppName(aut.getName());
		   /*	Added by Padmavathi for TENJINCG-545 starts*/
		   if(funcCode.length()>0&&groupName.length()>0){
				throw new DuplicateRecordException("Please specify either funcCode or groupName.");
			}
			/*	Added by Padmavathi for TENJINCG-545 ends*/
			if(funcCode.length()>0){
				try{
				new ModuleHandler().getModule(testDataPath.getAppId(), funcCode);
				}catch(RecordNotFoundException re){
					Api api=new ApiHelper().hydrateApi(testDataPath.getAppId(), funcCode);
					if(api==null){
					throw new TenjinServletException("This function or api does not exist under this application. Please review your request and try again");
					}
				}
			}
			/*	Added by Padmavathi for TENJINCG-545 starts*/
			if(groupName.length()>0){
				try {
					if(new ModulesHelper().hydrateGroupInfo(testDataPath.getAppId(), groupName)==null){
						throw new TenjinServletException("This group does not exist under this application. Please review your request and try again");
					}
				} catch (SQLException e) {
					logger.info("error occured while fecthing group");
				}
			
			}
			/*	Added by Padmavathi for TENJINCG-545 ends*/
		}
	/*	Added by Padmavathi for TENJINCG-540 ends*/
	/*Changed by leelaprasad for testdata path TENJINCG-549 starts*/
	public String prepareTestDataPath(String path) throws TenjinServletException{
		if(path==null){
			throw new TenjinServletException("Testdatapath is mandatory. Please review your request and try again");
		}
		String testDataPath=path;
		/*	Modified by Padmavathi for TENJINCG-545 starts*/
		char c= testDataPath.charAt(0);
		if(c>='a'&&c<='z'||c>='A'&&c<='Z'){
			testDataPath=path;
		}
		
		else{
			testDataPath=path.substring(1,path.length());
			testDataPath="\\\\"+testDataPath;
		}
		
		return testDataPath;
		/*	Modified by Padmavathi for TENJINCG-545 ends*/
	}
	/*Changed by leelaprasad for testdata path TENJINCG-549 starts*/
}
