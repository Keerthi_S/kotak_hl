/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AutHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION 
 *18-09-2017			Sameer					Added Newly  
 */

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Adapter;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class AdapterHelper {
	private static final Logger logger = LoggerFactory
			.getLogger(AdapterHelper.class);

	public List<String> getAvailableAdapters() throws DatabaseException {
		List<String> adapters = new ArrayList<String>();

		List<Adapter> listAdapters = this.hydrateAdapters();

		for (Adapter adapter : listAdapters) {
			adapters.add(adapter.getName());
		}

		return adapters;
	}

	public List<Adapter> hydrateAdapters() throws DatabaseException {
		long oMillis = System.currentTimeMillis();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		List<Adapter> listAdapters = new ArrayList<Adapter>();
		Adapter adapter = null;
		try {
			String query = "SELECT  A.NAME , A.OR_TOOL ,A.APPLICATION_TYPE from TJN_ADAPTER A ";

			pst = conn.prepareStatement(query);
			long startMillis = System.currentTimeMillis();
		    rs = pst.executeQuery();
			long endMillis = System.currentTimeMillis();
			logger.debug("HYDR_TJN_ADAPTER_TIMER >> Execute Query 1 >> "
					+ Utilities.calculateElapsedTime(startMillis, endMillis));
			startMillis = System.currentTimeMillis();
			while (rs.next()) {
				adapter = new Adapter();
				adapter.setName(rs.getString("NAME"));
				adapter.setOrTool(rs.getInt("OR_TOOL"));
				adapter.setApplicationType(rs.getInt("APPLICATION_TYPE"));
				listAdapters.add(adapter);
			}
			endMillis = System.currentTimeMillis();
			logger.debug("HYDR_TJN_ADAPTER_TIMER >> Process Query 1 >> "
					+ Utilities.calculateElapsedTime(startMillis, endMillis));
			

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		long oEMillis = System.currentTimeMillis();
		logger.debug("HYDR_TJN_ADAPTER_TIMER >> hydrateAut complete >> "
				+ Utilities.calculateElapsedTime(oMillis, oEMillis));
		return listAdapters;
	}

}
