<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  client_new.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 

-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/cssreset-min.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/ifr_main.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/style.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/tabs.css' />
		<link rel="Stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/buttons.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/css/960_16_col.css' />
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src='${pageContext.request.contextPath}/js/data-grids.js'></script>
		
		<script>
		$(document).ready(function(){

			mandatoryFields('clientForm');
			$('#port').val('');
			$('.enableDeviceFarm').hide();
			var checkDeviceFarm=document.getElementById('txtCheck');
			if (!checkDeviceFarm.checked){
				$('#devFarmCheck').val('N');
		    }
			var hostName = $('#hostName').val();
			$(document).on('click', '#txtCheck', function() {
				$('#txtCheck').val(this.checked);
				if (this.checked) {
					$('#txtCheck').val('Y');
					$('#devFarmCheck').val('Y');
					$('#hostName').val('000.000.000.000');
					$('#port').val('0000');
					$('#osType').val('Device Farm');
					$('.enableDeviceFarm').show();
					$('#deviceFarmCheck').hide();
					
				} else {
					$('#hostName').val(hostName);
					$('#port').val('');
					$('#osType').val('');
					$('#txtCheck').val('N');
					$('#devFarmCheck').val('N');
					$('.enableDeviceFarm').hide();
					$('#deviceFarmCheck').show();
				}
			});
		});
			$(document).on('click', '#btnBack', function() {
				var confirmResult = confirm(getLocalizedMessage('confirm.cancel.operation',''));
				if(confirmResult) {
					window.location.href='ClientServlet';
				}else{
					return false;
				}
			});
			
			
		</script>
		
	</head>
	<body>
	<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
		<div class='title'>
			<p>Client Details</p>
		</div>
		
		<input type='hidden' id='callback' value='${callback }' />
		<form action='ClientServlet' method='POST' id='clientForm' name='clientForm'>
		<input type = "hidden" id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
			<div class='toolbar'>
				<input type='submit' id='btnSave' value='Save' class='imagebutton save' />
				<input type='button' id='btnBack' value='Cancel' class='imagebutton cancel' />
			</div>
			
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${fn:escapeXml(screenState.message) }' />
			
			<div id='user-message'></div>
			
			<div class='form container_16'>
				<fieldset>
					<legend>Basic Information</legend>
					<div class='fieldSection grid_7'>
							<div class='grid_2'><label for='clientName'>Name</label></div>
							  <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<div class='grid_4'><input type='text' class='stdTextBox' value='${fn:escapeXml(client.name) }' id='clientName' name='name' mandatory='yes' maxlength="15" placeholder ="MaxLength 15"/></div>
						      <!-- Modified by Priyanka for TENJINCG-1233 ends -->
						<div id='deviceFarmCheck'>
							<div class='clear'></div>
							
							<div class='grid_2'><label for='hostName'>Host Name</label></div>
							  <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<div class='grid_4'><input type='text' class='stdTextBox' value='${fn:escapeXml(client.hostName) eq "0:0:0:0:0:0:0:1" ? "Localhost" : fn:escapeXml(client.hostName) }' id='hostName' name='hostName' mandatory='yes' maxlength="15" placeholder ="MaxLength 15"/></div>
							    <!-- Modified by Priyanka for TENJINCG-1233 ends -->
							<div class='clear'></div>
							
							<div class='grid_2'><label for='port'>Port</label></div>
							  <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<div class='grid_4'><input type='text' class='stdTextBox' value='${fn:escapeXml(client.port) }' id='port' name='port' mandatory='yes' maxlength ="5" placeholder ="MaxLength 5"/></div>
							  <!-- Modified by Priyanka for TENJINCG-1233 ends -->
							<div class='clear'></div>
							
							<div class='grid_2'><label for='osType'>OS Type</label></div>
							<div class='grid_4'><input type='text' class='stdTextBox' value='${fn:escapeXml(client.osType) }' id='osType' name='osType' mandatory='yes' maxlength ="50"/></div>
						</div>
					</div>
				</fieldset>
			<fieldset>
				<legend>Device Farm Information</legend>
				<div class='fieldSection grid_13'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='txtCheck'>Device Farm</label>
					</div>
					<div class='grid_4'>
					        <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<input type='checkbox' id='txtCheck' name='txtCheck'
							title='Select CheckBox' value='${fn:escapeXml(client.deviceFarmCheck)}' style='vertical-align: bottom;margin-top:6px;'/> <input
							type='hidden' id='devFarmCheck' name='devFarmCheck' value='' maxlength="5" />
					        <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>
				</div>
				<div class='fieldSection grid_7 enableDeviceFarm'>
					<div class='clear'></div>
					<div class='grid_2'>
						<label for='devFarmUsrName'>Login Username</label>
					</div>
					<div class='grid_4'>
					     <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						<input type='text' class='stdTextBox'
							value='${fn:escapeXml(client.deviceFarmUsrName)}' id='devFarmUsrName'
							name='devFarmUsrName' maxlength='50' placeholder="MaxLength 50" />
						<!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='devFarmPwd'>Login Password</label>
					</div>
					<div class='grid_4'>
					        <!-- Modified by Priyanka for TENJINCG-1233 starts -->
							<input type='password' class='stdTextBox'
							value='${fn:escapeXml(client.deviceFarmPwd)}' id='devFarmPwd' name='devFarmPwd' autocomplete="new-password" maxlength="400"/>
					        <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>

					<div class='clear'></div>
					<div class='grid_2'>
						<label for='devFarmKey'>Key</label>
					</div>
					<div class='grid_4'>
					    <!-- Modified by Priyanka for TENJINCG-1233 starts -->
						    <input type='password' class='stdTextBox'
							value='${fn:escapeXml(client.deviceFarmKey)}' id='devFarmKey' name='devFarmKey' maxlength="400"/>
					    <!-- Modified by Priyanka for TENJINCG-1233 ends -->
					</div>
				</div>
			</fieldset>
		</div>
		</form>
	</body>
</html>