
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RunHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              	DESCRIPTION
 * 03-12-2019          Roshni Das          	Newly Added For TENJINCG-1168
 * 02-06-2021          Paneendra                   TENJINCG-1267
 ***/
package com.ycs.tenjin.handler;

import java.sql.Connection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.TestCaseHelper;
import com.ycs.tenjin.db.TestSetHelper;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.util.Constants;

public class RunHandler {

	private static Logger logger = LoggerFactory
			.getLogger(RunHandler.class);
	private Connection conn;
	
	public RunHandler(){
		try {
			this.conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch (DatabaseException e) {
			
			logger.error("Error ", e);
		}
	}
	
	public TestCase hydrateTestCaseForStep(int projectId, int testStepRecordId) throws DatabaseException{
		
		return new TestCaseHelper().hydrateTestCaseForStep(projectId,testStepRecordId,this.conn);
		
	}

	public List<RegisteredClient> hydrateAllClients() throws DatabaseException {
		
		return new ClientHelper().hydrateAllClients(this.conn);
	}

	public TestCase hydrateTestCase(int id, int tcRecId) throws DatabaseException {
		
		return new TestCaseHelper().hydrateTestCase(id,tcRecId,this.conn);
	}

	public TestSet hydrateTestSetBasicDetails(int tsRecId, int projId) throws DatabaseException {
		
		return new TestSetHelper().hydrateTestSetBasicDetails(tsRecId,projId,this.conn);
	}
	
	/*Added by paneendra for  TENJINCG-1267 starts*/
	 public void CloseConnection(){
			DatabaseHelper.close(conn);
	}
	 /*Added by paneendra for  TENJINCG-1267 ends*/
	
}
