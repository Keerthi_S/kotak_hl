/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MsgFormatSelectorTag.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 
 */

package com.ycs.tenjin.core.tags;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.MessageValidate;
import com.ycs.tenjin.handler.MessageValidateHandler;
import com.ycs.tenjin.util.Utilities;

public class MsgFormatSelectorTag extends SimpleTagSupport {
	private static final Logger logger = LoggerFactory.getLogger(MsgFormatSelectorTag.class);
	
	private String applicationSelector;
	private String cssClass;
	private String id;
	private String name;
	private String defaultValue;
	private int applicationId;
	private String mandatory;
	private String title;
	
	
	public String getMandatory() {
		return mandatory;
	}
	public void setMandatory(String mandatory) {
		this.mandatory = mandatory;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int appId) {
		this.applicationId = appId;
	}
	
	public String getApplicationSelector() {
		return applicationSelector;
	}
	public void setApplicationSelector(String applicationSelector) {
		this.applicationSelector = applicationSelector;
	}
	
	public String getCssClass() {
		return cssClass;
	}
	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	@Override
	public void doTag() throws JspException, IOException {
		String finalHtml = "<select id='" + this.id +"' applicationId='"+ this.applicationId+"' name='"+ this.name+ "' mandatory='"+this.mandatory+"' title='"+this.title+"' class='" + this.cssClass +" functionSelector' data-application-selector='" + this.applicationSelector + "'>";
		finalHtml += "<option value='-1'>-- Select One --</option>";
		if(this.applicationId > 0) {
			List<String> avoidDup=new ArrayList<String>();
			try {
				List<MessageValidate> messages = new MessageValidateHandler().getMessageFormat(this.applicationId);
				for(MessageValidate message : messages) {
					if(avoidDup!=null && avoidDup.size()>0 && avoidDup.contains(message.getMessageFormat())){
						continue;
					}
					if(Utilities.trim(message.getMessageFormat()).equalsIgnoreCase(this.defaultValue)) {
						avoidDup.add(message.getMessageFormat());
						finalHtml += "<option value='" + message.getMessageFormat() + "' selected>" + Utilities.escapeXml(message.getMessageFormat()) +  "</option>";
					}else {
						avoidDup.add(message.getMessageFormat());
						finalHtml += "<option value='" + message.getMessageFormat() + "' >" + Utilities.escapeXml(message.getMessageFormat()) +  "</option>";
					}
				}
			} catch (Exception e) {
				logger.error("ERROR getting function groups for application {}", this.applicationId, e);
			}
		}
		finalHtml += "</select>";
		
		getJspContext().getOut().write(finalHtml);
	}
}
