/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ifr_main.css


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 31-Aug-2016			LATIEF					Req#TJN_23_12
24-Jul-2017		Sriram						TENJINCG-284
28-Jul-2017		Gangadhar Badagi			TENJINCG-315
19-Oct-2017		Sriram						TENJINCG-396
*/

@CHARSET "ISO-8859-1";
@font-face {
	font-family: 'Open Sans';
	src: url('fonts/Open_Sans/OpenSans-Regular-webfont.eot');
	src: url('fonts/Open_Sans/OpenSans-Regular-webfont.eot?#iefix') format('embedded-opentype'),
       url('fonts/Open_Sans/OpenSans-Regular-webfont.woff') format('woff'),
       url('fonts/Open_Sans/OpenSans-Regular-webfont.ttf') format('truetype'),
       url('fonts/Open_Sans/OpenSans-Regular-webfont.svg#open_sansregular') format('svg');
	font-weight:normal;
	font-style:normal;
}

@font-face {
	font-family: 'Open Sans';
	src: url('fonts/Open_Sans/OpenSans-Italic-webfont.eot');
	src: url('fonts/Open_Sans/OpenSans-Italic-webfont.eot?#iefix') format('embedded-opentype'),
       url('fonts/Open_Sans/OpenSans-Italic-webfont.woff') format('woff'),
       url('fonts/Open_Sans/OpenSans-Italic-webfont.ttf') format('truetype'),
       url('fonts/Open_Sans/OpenSans-Italic-webfont.svg#open_sansregular') format('svg');
	font-weight:normal;
	font-style:italic;
}

@font-face {
	font-family: 'Open Sans';
	src: url('fonts/Open_Sans/OpenSans-Bold-webfont.eot');
	src: url('fonts/Open_Sans/OpenSans-Bold-webfont.eot?#iefix') format('embedded-opentype'),
       url('fonts/Open_Sans/OpenSans-Bold-webfont.woff') format('woff'),
       url('fonts/Open_Sans/OpenSans-Bold-webfont.ttf') format('truetype'),
       url('fonts/Open_Sans/OpenSans-Bold-webfont.svg#open_sansregular') format('svg');
	font-weight:bold;
	font-style:normal;
}

@font-face {
	font-family: 'Open Sans';
	src: url('fonts/Open_Sans/OpenSans-BoldItalic-webfont.eot');
	src: url('fonts/Open_Sans/OpenSans-BoldItalic-webfont.eot?#iefix') format('embedded-opentype'),
       url('fonts/Open_Sans/OpenSans-BoldItalic-webfont.woff') format('woff'),
       url('fonts/Open_Sans/OpenSans-BoldItalic-webfont.ttf') format('truetype'),
       url('fonts/Open_Sans/OpenSans-BoldItalic-webfont.svg#open_sansregular') format('svg');
	font-weight:bold;
	font-style:italic;
}

body
{
	background:url('../images/back.jpg');
	font-family:'Open Sans';
	height:100%;
	font-size:12px;
}

.title{
	height:20px;
	background:url(../images/bg.gif);
	color:white;
	font-family:'Open Sans';
	padding:6px;
	font-weight:bold;
}

/*Added by Sriram to display current Project Information on Title bar*/
.title > p{
	float:left;
}

.title > #prj-info{
	display:inline-block;
	float:right;
}

/*Added by Sriram to display current Project Information on Title bar ends*/

.toolbar
{
	display: inline-block;
	width: 100%;
	background-color: #cccccc;
	padding-top: 5px;
	padding-bottom: 5px;
	font-size:1.2em;
}

/* Added by Sriram for TENJINCG-284 */
.toolbar-right
{
	display: inline;
    font-weight: bold;
    float: right;
    font-size: 0.9em;
    line-height: 25px;
    margin-right: 10px;
}
/* Added by Sriram for TENJINCG-284 ends*/

.fieldSection
{
	margin-left:3px;
	margin-top:10px;
}

.fieldSection div
{
	margin-bottom:5px;
}

.form h2
{
	font-weight:bold;
	font-size:1.4em;
	color:rgb(16,115,151);
	padding:10px;
}

/*Added by Sriram for Results Screen overhaul*/
.form h3
{
	font-weight:bold;
	font-size:1.1em;
	color:rgb(16,115,151);
	padding:10px;
}



/*Added by Sriram for Results screen overhaul ends (29-09-2015)*/


.form fieldset
{
	padding-left:20px;
	border:0;
	border-top: 1px solid black;
	margin-bottom:5px;
}

.form fieldset > legend
{
	font-weight:bold;
}

.subframe
{
	position: absolute;
	width: 650px;
	/*Commented by Sriram as it was left as it was*/
	/* border: 1px solid black; */
	top: 25px;
	left:10%;
	display: block;
	margin: 0 auto;	
	z-index: 9999;
	display:none;
}

.subframe > iframe
{
	width:650px;
	height:600px;
	overflow:hidden;
	border:0;

}

.modalmask
{
	position: fixed;
	top: 0px;
	right: 0px;
	bottom: 0px;
	left: 0px;
	opacity: 0.7;
	z-index: 9998;
	cursor: pointer;
	background-color: rgb(0, 0, 0);
	width:100%;
	display:none;
}

#tsMapTreeContainer
{
	display: block;
	background-color: white;
	min-height: 350px;
	max-height:350px;
	overflow:auto;
	border-radius: 4px;
	margin-top: 10px;
	border: 1px solid rgb(226, 226, 226);
	box-shadow: 0 0 5px rgb(197, 193, 193);
}

#tsMapedTcContainer{
	display: block;
	background-color: white;
	min-height: 350px;
	max-height:350px;
	overflow:auto;
	border-radius: 4px;
	margin-top: 10px;
	border: 1px solid rgb(226, 226, 226);
	box-shadow: 0 0 5px rgb(197, 193, 193);	
}

.mappedtc:hover
{
	
	background-color: #D4E3F7;
	border: 1px solid #C5DAF7;
	box-shadow: 0 0 5px #D4E3F7;
	
}

.mappedtc
{
	background-repeat: no-repeat;
	padding-left: 30px;
	display: block;
	padding-top: 6px;
	background-position-y: 50%;
	padding-bottom: 6px;
	margin-left: 10px;
	margin-top: 5px;
	background-color: #F9EDED;
	border: 1px solid #FADADA;
	background-position-x: 2%;
	min-width: 75px;
	max-width: 250px;
	border-radius: 5px;
	cursor: pointer;
}

.selectedToRemove
{
	background-color: #D4E3F7;
	border: 1px solid #C5DAF7;
}

.res-val-detail {
width: 250px;
padding: 10px;
border: 1px solid rgb(253, 186, 186);
border-radius: 4px;
background-color: rgb(253, 207, 207);
box-shadow: 0 0 5px rgb(252, 132, 132);
text-shadow: 0 0 1px;
margin-left:2px;
margin-right:5px;
float:left;
margin-top:2px;
margin-bottom:5px;
}

.res-val-detail > a {
float: right;
text-decoration: none;
text-shadow: none;
}

a.pagination{
	display: inline-block;
  width: 15px;
  height: 15px;
  background-color: #6b84f1;
  text-align: center;
  text-decoration: none;
  color: #fff;
  border: 1px solid #31459e;
  padding: 3px;
  font-weight: bold;
}

#curpage{
	width:25px;
}

#ttd-options-wrapper
{
	padding:20px;
	/*Commented by Sriram as it was left as it was*/
	/* border:0.5px solid black; */ 
	display:table;
}

#ttd-options-layout tr
{
	display:table;
	min-height:75px;
}

#ttd-options-layout tr > td.selector
{
	width:30px;
	border:1px solid #ccc;
	text-align:center;
}

#ttd-options-layout tr > td.ttd-option-block
{
	width:500px;
	border:1px solid #ccc;
}

.ttd-option-title
{
	font-weight:bold;
	padding:5px;
	margin-bottom:3px;
}

.ttd-option-desc
{
	padding-left:5px;
}



input#mandatorySelector {
  width: 13px;
  height: 13px;
  padding: 0;
  margin: 0;
  vertical-align: middle;
  position: relative;
  top: -1px;
}

.checklist {
   /*  border: 1px solid #ccc; */
    list-style: none;
    margin-left:5px;
}


.checklist, .checklist li { margin: 0; padding: 0; }

.checklist label {
    display: block;
    padding-left: 25px;
    text-indent: -25px;
}

.checklist img
{
	padding-right:5px;
	top:3px;
	position:relative;
}

.checklist label:hover { background: #777; color: #fff; }

/*  changed by latief for requirement# TJN_23_12  */
.tomap
{
	background-repeat: no-repeat;
	padding-left: 30px;
	display: block;
	padding-top: 6px;
	background-position-y: 50%;
	padding-bottom: 6px;
	margin-left: 10px;
	margin-top: 5px;
	background-color: #F9EDED;
	border: 1px solid #FADADA;
	background-position-x: 2%;
	min-width: 75px;
	max-width: 250px;
	border-radius: 5px;
	cursor: pointer;
	
}
/*  changed by latief for requirement# TJN_23_12 ENDS*/






/*  Removed by Gangadhar Badagi for TENJINCG-315 starts*/
/* .modal-open {
  overflow: hidden;
}
.modal {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1040;
  display: none;
  overflow: hidden;
  -webkit-overflow-scrolling: touch;
  outline: 0;
}
.modal.fade .modal-dialog {
  -webkit-transition: -webkit-transform .3s ease-out;
       -o-transition:      -o-transform .3s ease-out;
          transition:         transform .3s ease-out;
  -webkit-transform: translate(0, -25%);
      -ms-transform: translate(0, -25%);
       -o-transform: translate(0, -25%);
          transform: translate(0, -25%);
}
.modal.in .modal-dialog {
  -webkit-transform: translate(0, 0);
      -ms-transform: translate(0, 0);
       -o-transform: translate(0, 0);
          transform: translate(0, 0);
}
.modal-open .modal {
  overflow-x: hidden;
  overflow-y: auto;
}
.modal-dialog {
  position: relative;
  width: auto;
  margin: 2px;
}
.modal-content {
  position: relative;
  background-color: #fff;
  -webkit-background-clip: padding-box;
          background-clip: padding-box;
  border: 1px solid #999;
  border: 1px solid rgba(0, 0, 0, .2);
  border-radius: 4px;
  outline: 0;
  -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, .5);
          box-shadow: 0 3px 9px rgba(0, 0, 0, .5);
}
.modal-backdrop {
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  background-color: #000;
}
.modal-backdrop.fade {
  filter: alpha(opacity=0);
  opacity: 0;
}
.modal-backdrop.in {
  filter: alpha(opacity=50);
  opacity: .5;
}
.modal-header {
  min-height: 16.42857143px;
  padding: px;
  border-bottom: 1px solid #e5e5e5;
}
.modal-header .close {
  margin-top: -2px;
}
.modal-title {
  margin: 0;
  line-height: 1.42857143;
}
.modal-body {
  position: relative;
  padding: 10px;
}
.modal-footer {
  padding: 2px;
  text-align: right;
  border-top: 1px solid #e5e5e5;
}
.modal-footer .btn + .btn {
  margin-bottom: 0;
  margin-left: 5px;
}
.modal-footer .btn-group .btn + .btn {
  margin-left: -1px;
}
.modal-footer .btn-block + .btn-block {
  margin-left: 0;
}
.modal-scrollbar-measure {
  position: absolute;
  top: -9999px;
  width: 60px;
  height: 60px;
  overflow: scroll;
}

@media (min-width: 768px) {
  .modal-dialog {
    width: 700px;
    margin: 30px auto;
  }
  .modal-content {
    -webkit-box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
            box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
  }
  .modal-sm {
    width: 3500px;
  }
}
@media (min-width: 992px) {
  .modal-lg {
    width: 900px;
  }
}
.modal-footer:before,
.modal-footer:after {
  display: table;
  content: " ";
}

 */

/*  Removed by Gangadhar Badagi for TENJINCG-315 ends*/



.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    margin-left: 10px;
    padding: 10px;
    border: 1px solid #888;
    width: 90%;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 25px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

/* TENJINCG-314 */
#functions_export_block
{
	min-height:200px;
	background-color:#fff;
}

#importMetadataModal
{
	min-height: 250px;background-color: #fff;
}

#import_instructions
{
	color:red;
}

#import_instructions strong
{
	font-weight:bold;
}
/* TENJINCG-396 */
#dataGrid
{
	padding-left:15px;
	padding-right:15px;
}

.fieldSection label
{
	display: block;
    text-align: right;
}
/* TENJINCG-396 Ends */