/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: PcloudyDeviceHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2018-19 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
#301,�Trinity Woods�, 872/D, 
Sir CV Raman Road, Indiranagar, 
Bengaluru, Karnataka 560038
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 25-01-2019			Sahana 					Newly Added for pCloudy
 */
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.device.RegisteredDevice;
import com.ycs.tenjin.util.Constants;

public class PcloudyDeviceHelper {
	private static final Logger logger = LoggerFactory.getLogger(PcloudyDeviceHelper.class);

	public boolean checkDeviceExist(int deviceId) throws DatabaseException {
		boolean deviceFound = false;
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("SELECT * FROM PCLOUDYDEVICES WHERE DEVICE_ID =?");
				){
			pst.setInt(1, deviceId);
			try (ResultSet rs = pst.executeQuery()){
				if(rs.next()){
					deviceFound=true;
				}
			}
		} catch(SQLException e) {
			logger.error("Could not fetch pCloudy Device records");
			throw new DatabaseException("Could not fetch pCloudy Device records",e);
		}
		return deviceFound;
	}

	public RegisteredDevice hydrateDevice(int deviceId) throws DatabaseException {
		RegisteredDevice rd = null;
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("SELECT * FROM PCLOUDYDEVICES WHERE DEVICE_ID =?");
				){
			pst.setInt(1,deviceId );
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					rd = new RegisteredDevice();
					rd.setDeviceId(rs.getString("DEVICE_ID"));
					rd.setDeviceName(rs.getString("MANUFACTURER")+" "+rs.getString("DISPLAY_NAME"));
					rd.setPlatform(rs.getString("PLATFORM"));
					rd.setPlatformVersion(rs.getString("VERSION"));
					rd.setDeviceRecId(rs.getString("DEVICE_ID"));
					rd.setDeviceType("pCloudy");
				}
			}
		} catch(SQLException e) {
			logger.error("Could not fetch device records");
			throw new DatabaseException("Could not fetch device records",e);
		}
		return rd;
	}

	public void persistPcloudyDevice(JSONObject device) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("INSERT INTO PCLOUDYDEVICES (DEVICE_ID,FULL_NAME,MODEL,CPU,DISPLAY_NAME,PLATFORM,VERSION,MANUFACTURER,DPI,RAM,RESOLUTION,DISPLAY_AREA,AVAILABLE,NETWORK,MOBILE_NUMBER) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				){
			pst.setInt(1, Integer.parseInt(device.getString("id")));
			pst.setString(2, device.getString("full_name"));
			pst.setString(3, device.getString("model"));
			pst.setString(4, device.getString("cpu"));
			pst.setString(5, device.getString("display_name"));
			pst.setString(6, device.getString("platform"));
			pst.setString(7, device.getString("version"));
			pst.setString(8, device.getString("manufacturer"));
			pst.setString(9, device.getString("dpi"));
			pst.setInt(10, Integer.parseInt(device.getString("ram")));
			pst.setString(11, device.getString("resolution"));
			pst.setString(12, device.getString("display_area"));
			pst.setString(13, device.getString("available"));
			pst.setString(14, device.getString("Network"));
			pst.setString(15, device.getString("Mobile Number"));
			pst.execute();
		}catch(Exception e) {
			logger.error("Could not insert pCloudy device");
			throw new DatabaseException("Could not insert pCloudy device",e);
		}
	}
}
