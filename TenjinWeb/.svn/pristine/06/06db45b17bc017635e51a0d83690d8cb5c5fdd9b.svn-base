/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ModuleHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              	DESCRIPTION
 * 24-Oct-2017           Sriram Sridharan          	Newly Added For TENJINCG-399
 * 03-11-2017            Padmavathi            		TENJINCG-400
 * 15-06-2018            Padmavathi            		T251IT-30
 * 21-02-2019			 Ashiki						TJN252-84
 * 08-03-2019			 Sahana					    pCloudy
 * 27-08-2019			 Ashiki	 					TENJINCG-1105
 * 09-10-2019			 Pushpalatha				TJN252-7
 * 05-02-2020			 Roshni					TENJINCG-1168
 * 09-10-2019			 Pushpalatha				TJN252-7
 * 02-06-2021            paneendra                TENJINCG-1267
 */

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.pojo.aut.ExtractorResultBean;
import com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

/**
 * @author Sriram
 *
 */
public class ModuleHelper {
	private static final Logger logger = LoggerFactory.getLogger(ModuleHelper.class);

	public Module hydrateModule(int appId, String moduleCode) throws DatabaseException {
		Connection conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		try {
			Module module = this.hydrateModule(conn, appId, moduleCode);
			return module;
		}finally {
			/*DatabaseHelper.closeConnection(conn);*/
			DatabaseHelper.close(conn);
		}
	}
	
	public Module hydrateModule(Connection conn, int appId, String moduleCode) throws DatabaseException {
		Module module = null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {
			/*Modified by Padmavathi for T251IT-30 starts*/
			pst = conn.prepareStatement("SELECT * FROM MASMODULE WHERE APP_ID=? AND Lower(MODULE_cODE)=?");
			pst.setInt(1, appId);
			pst.setString(2, moduleCode.toLowerCase());
			/*Modified by Padmavathi for T251IT-30 ends*/
			rs = pst.executeQuery();
			
			while(rs.next()) {
				module = this.mapFields(rs);
				module.setLastSuccessfulLearningResult(this.getLastSuccessfullLearningResult(conn, appId, module.getCode()));
			}
			
			
		} catch (SQLException e) {
			logger.error("ERROR fetching module: application {}, Function Code {}", appId, moduleCode, e);
			throw new DatabaseException("Could not fetch module due to an internal error");
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			
		}
		
		return module;
	}
	
	public List<Module> hydrateModules(Connection conn, int appId, String groupName) throws DatabaseException {
		List<Module> modules = new ArrayList<Module>();
		PreparedStatement pst=null;
		ResultSet rs=null;
		String query = "SELECT * FROM MASMODULE WHERE APP_ID=?"; 
		if(Utilities.trim(groupName).length() > 0 && !Utilities.trim(groupName).equalsIgnoreCase("all")) {
			query += " AND GROUP_NAME=?";
		}
		
		/*Changed By Ashiki for TJN252-84 starts*/
		query += " ORDER BY UPPER(MODULE_CODE)";
		/*Changed By Ashiki for TJN252-84 ends*/
		
		try {
			pst = conn.prepareStatement(query);
			pst.setInt(1,appId);
			if(Utilities.trim(groupName).length() > 0 && !Utilities.trim(groupName).equalsIgnoreCase("all")) {
				pst.setString(2,  Utilities.trim(groupName));
			}
			rs = pst.executeQuery();
			
			while(rs.next()) {
				Module module = this.mapFields(rs);
				module.setLastSuccessfulLearningResult(this.getLastSuccessfullLearningResult(conn, appId, module.getCode()));
				modules.add(module);
			}
			
		} catch (SQLException e) {
			logger.error("ERROR fetching modules for application {}, Group {}", appId, groupName, e);
			throw new DatabaseException("Could not fetch modules due to an internal error");
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			
		}
		
		return modules;
	}
	
	private LearnerResultBean getLastSuccessfullLearningResult(Connection conn, int appId, String moduleCode) throws DatabaseException {
		LearnerResultBean result = null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {/*Changed by Ashiki for TENJINCG--1105 starts*/
			pst = conn.prepareStatement("SELECT LRNR_END_TIME,LRNR_ID,LRNR_STATUS,LRNR_RUN_ID,FIELDS_LEARNT_COUNT from LRNR_AUDIT_TRAIL WHERE LRNR_STATUS IN(?, ?) AND LRNR_FUNC_CODE=? AND LRNR_APP_ID=? ORDER BY LRNR_ID DESC");
			/*Changed by Ashiki for TENJINCG--1105 starts*/
			pst.setString(1, "COMPLETE");
			pst.setString(2, "IMPORTED");
			pst.setString(3, moduleCode);
			pst.setInt(4, appId);
			rs = pst.executeQuery();
			while(rs.next()) {
				result = new LearnerResultBean();
				result.setId(rs.getInt("LRNR_ID"));
				result.setEndTimestamp(rs.getTimestamp("LRNR_END_TIME"));
				result.setLearnStatus(rs.getString("LRNR_STATUS"));
				/*Added by Ashiki for TENJINCG--1105 starts*/
				result.setLastLearntFieldCount(rs.getInt("FIELDS_LEARNT_COUNT"));
				/*Added by Ashiki for TENJINCG--1105 ends*/
				break;
			}
			logger.info("Modules of last successful learning history  {} ",result);
		
		} catch (SQLException e) {
			logger.error("ERROR getting last successful learning history for function {} in app {}", moduleCode, appId);
			throw new DatabaseException("Could not find last successful learning history for function");
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			
		}
		
		return result;
		
	}
	
	public void persistModule(Module module, int appId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		try {
			logger.debug("Persisting module {}, App {}", module.getCode(), appId);
			this.persistModule(conn, module, appId);
			logger.debug("Done");
		} catch (DatabaseException e) {
			throw e;
		}finally {
				DatabaseHelper.close(conn);
		}
	}
	
	public void updateModule(Module module, int appId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		try {
			logger.debug("Updating module {}, app {}", module.getCode(), appId);
			this.updateModule(conn, module, appId);
			logger.debug("Done");
		}catch(DatabaseException e) {
			throw e;
		}finally {
			DatabaseHelper.close(conn);
	    }
	}
	
	private void updateModule(Connection conn, Module module, int appId) throws DatabaseException {
		PreparedStatement pst = null;
		
		try {
			pst = conn.prepareStatement("UPDATE MASMODULE SET MODULE_NAME=?, MODULE_MENU_CONTAINER=?, GROUP_NAME=?, APP_DATE_FORMAT=? WHERE MODULE_CODE=? AND APP_ID=?");
			pst.setString(1, module.getName());
			pst.setString(2, module.getMenuContainer());
			pst.setString(3, module.getGroup());
			pst.setString(4, module.getDateFormat());
			pst.setString(5, module.getCode());
			pst.setInt(6, appId);
			pst.executeUpdate();
		} catch (SQLException e) {
			logger.error("ERROR updating module", e);
			throw new DatabaseException("Could not update module", e);
		} finally {
			
			DatabaseHelper.close(pst);
		}
	}
	
	private void persistModule(Connection conn, Module module, int appId) throws DatabaseException {
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement("INSERT INTO MASMODULE (APP_ID,MODULE_CODE,MODULE_NAME,MODULE_MENU_CONTAINER,GROUP_NAME,APP_DATE_FORMAT) VALUES (?,?,?,?,?,?)");
			pst.setInt(1, appId);
			pst.setString(2, module.getCode());
			pst.setString(3, module.getName());
			pst.setString(4, module.getMenuContainer());
			pst.setString(5, module.getGroup());
			pst.setString(6, module.getDateFormat());
			pst.execute();
		} catch (SQLException e) {
			logger.error("ERROR persisting module", e);
			throw new DatabaseException("Could not create module", e);
		} finally {
			
		DatabaseHelper.close(pst);
	}
	}
	
	private LearnerResultBean mapLearnerResultBeanFields(ResultSet rs) throws SQLException {
		LearnerResultBean lrnrBean=new LearnerResultBean();
		lrnrBean.setId(rs.getInt("LRNR_RUN_ID"));
		lrnrBean.setStartTimestamp(rs.getTimestamp("LRNR_START_TIME"));
		lrnrBean.setEndTimestamp(rs.getTimestamp("LRNR_END_TIME"));
		lrnrBean.setLearnStatus(rs.getString("LRNR_STATUS"));
		lrnrBean.setMessage(rs.getString("LRNR_MESSAGE"));
		lrnrBean.setUserID(rs.getString("LRNR_USER_ID"));
		/*Added by Ashiki for TENJINCG-1105 starts*/
		lrnrBean.setLastLearntFieldCount(rs.getInt("FIELDS_LEARNT_COUNT"));
		/*Added by Ashiki for TENJINCG-1105 ends*/
		if(lrnrBean.getStartTimestamp() != null && lrnrBean.getEndTimestamp() != null){
			long startMillis =lrnrBean.getStartTimestamp().getTime();
			long endMillis =lrnrBean.getEndTimestamp().getTime();

			long millis = endMillis - startMillis;

			String eTime = String.format(
					"%02d:%02d:%02d",
					TimeUnit.MILLISECONDS.toHours(millis),
					TimeUnit.MILLISECONDS.toMinutes(millis)
					- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
							.toHours(millis)),
							TimeUnit.MILLISECONDS.toSeconds(millis)
							- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
									.toMinutes(millis)));
			lrnrBean.setElapsedTime(eTime);
		}
		/*Added by Sahana for pCloudy: starts*/
		lrnrBean.setLearnerIpAddr(rs.getString("LRNR_IP_ADDRESS"));
		lrnrBean.setDeviceFarmFlag(rs.getString("RUN_DEVICE_FARM"));
		/*Added by Sahana for pCloudy: ends*/
		return lrnrBean;
	}
	
	private ExtractorResultBean mapExtractorResultBeanFields(ResultSet rs) throws SQLException {
		ExtractorResultBean extrBean = new ExtractorResultBean();
		
		extrBean.setRunId(rs.getInt("EXTR_RUN_ID"));
		extrBean.setStartTime(rs.getTimestamp("EXTR_START_TIME"));
		extrBean.setEndTime(rs.getTimestamp("EXTR_END_TIME"));
		extrBean.setStatus(rs.getString("EXTR_STATUS"));
		extrBean.setMessage(rs.getString("EXTR_MESSAGE"));
		extrBean.setUserId(rs.getString("EXTR_USER_ID"));
		if(extrBean.getStartTime() != null && extrBean.getEndTime() != null){
			long startMillis =extrBean.getStartTime().getTime();
			long endMillis =extrBean.getEndTime().getTime();

			long millis = endMillis - startMillis;

			String eTime = String.format(
					"%02d:%02d:%02d",
					TimeUnit.MILLISECONDS.toHours(millis),
					TimeUnit.MILLISECONDS.toMinutes(millis)
					- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
							.toHours(millis)),
							TimeUnit.MILLISECONDS.toSeconds(millis)
							- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
									.toMinutes(millis)));
			extrBean.setElapsedTime(eTime);
		}
		
		return extrBean;
	}
	
	
	private Module mapFields(ResultSet rs) throws SQLException {
		try {
			Module module = new Module();
			module.setCode(rs.getString("MODULE_CODE"));
			module.setName(rs.getString("MODULE_NAME"));
			module.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
			module.setGroup(rs.getString("GROUP_NAME"));
			module.setDateFormat(rs.getString("APP_DATE_FORMAT"));
			
			return module;
		} catch (SQLException e) {
			logger.error("ERROR creating Module from ResultSet", e);
			throw e;
		}
	}
	
	
	public void deleteModules(int appId, String[] moduleCodes) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		try {
			this.deleteModules(conn, appId, moduleCodes);
		} catch (Exception e) {
			throw e;
		} finally {
			DatabaseHelper.close(conn);
		}
	}
	
	private void deleteModules(Connection conn, int appId, String[] moduleCodes) throws DatabaseException {
		PreparedStatement pst =null;
		/*modified by shruthi for sql injection starts*/
		for(String str : moduleCodes) {
		try {
			
			logger.debug("Setting auto-commit to false");
			
			pst = conn.prepareStatement("DELETE FROM MASMODULE WHERE APP_ID=? AND MODULE_CODE=?");
			pst.setInt(1, appId);
			pst.setString(2, str);
			pst.execute();
			DatabaseHelper.commit(conn);
		} catch (SQLException e) {
			logger.error("ERROR while deleting multiple modules", e);
			DatabaseHelper.rollback(conn);			
			throw new DatabaseException("Could not delete modules", e);
		} finally {
			DatabaseHelper.close(pst);
		}
		}
		/*modified by shruthi for sql injection ends*/
	}
	
	
	public boolean isModuleMappedToTestStep(Connection conn, int appId, String moduleCode)  {
		PreparedStatement pst=null;
		ResultSet rs=null;
		int mappedStepCount = 0;
		try {
			pst = conn.prepareStatement("SELECT COUNT(*) as MAPPED_STEPS_COUNT FROM TESTSTEPS WHERE APP_ID=? AND FUNC_CODE=?");
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			rs = pst.executeQuery();
			while(rs.next()) {
				mappedStepCount = rs.getInt("MAPPED_STEPS_COUNT");
			}
			
		
		} catch (SQLException e) {
			logger.error("ERROR checking if module is mapped to test step", e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
		if(mappedStepCount > 0) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean isModuleCurrentlyBeingLearnt(Connection conn, int appId, String moduleCode) {
		PreparedStatement pst=null;
		ResultSet rs=null;
		boolean currentlyBeingLearnt = false;
		try {
			pst = conn.prepareStatement("SELECT * FROM LRNR_AUDIT_TRAIL WHERE LRNR_APP_ID=? AND LRNR_FUNC_CODE=? AND LRNR_STATUS IN (?,?)");
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, BridgeProcess.QUEUED);
			pst.setString(4, "LEARNING");
			rs = pst.executeQuery();
			while(rs.next()) {
				currentlyBeingLearnt = true;
				break;
			}
			
		
		} catch (SQLException e) {
			logger.error("ERROR checking if module is currently being learnt.", e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
		return currentlyBeingLearnt;
	}
	
	public List<LearnerResultBean> hydrateLearningHistory(int appId, String functionCode) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		List<LearnerResultBean> results = new ArrayList<LearnerResultBean>();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			/*Changed by Sahana for pCloudy: starts*/
			pst = conn.prepareStatement("Select A.*,B.RUN_DEVICE_FARM from LRNR_AUDIT_TRAIL A,MASTESTRUNS B"
					+ " where lrnr_app_id=? and lrnr_func_code=? and "
					+ "A.LRNR_RUN_ID=B.RUN_ID order by A.LRNR_RUN_ID desc");
			/*Changed by Sahana for pCloudy: ends*/
			pst.setInt(1, appId);
			pst.setString(2, functionCode);
			
			rs = pst.executeQuery();
			
			while(rs.next()) {
				LearnerResultBean lBean = this.mapLearnerResultBeanFields(rs);
				if(lBean != null && !lBean.getLearnStatus().equalsIgnoreCase("QUEUED")) {
					results.add(lBean);
				}
			}
			
			
		} catch (SQLException e) {
			logger.error("ERROR fetching learning history", e);
			throw new DatabaseException("Could not fetch learning history", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return results;
	}
	
	public List<ExtractorResultBean> hydrateExtractionHistory(int appId, String functionCode) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		List<ExtractorResultBean> results = new ArrayList<ExtractorResultBean> ();
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			pst = conn.prepareStatement("Select * from extr_audit_trail where extr_app_id=? and extr_func_code=? order by EXTR_ID desc");
			pst.setInt(1, appId);
			pst.setString(2, functionCode);
			rs = pst.executeQuery();
			
			while(rs.next()) {
				ExtractorResultBean result = this.mapExtractorResultBeanFields(rs);
				if(Utilities.trim(result.getStatus()).equalsIgnoreCase(BridgeProcess.QUEUED)) {
					continue;
				}else{
					results.add(result);
				}
			}
			
		
		} catch (SQLException e) {
			logger.error("ERROR fetching extraction history for app {}, function {}", appId, functionCode, e);
			throw new DatabaseException("Could not fetch extraction history", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return results;
	}
	
	
	
	/*Added by Padmavathi for TENJINCG-400 starts*/
	public ArrayList<Module> hydrateModules(int appId) throws DatabaseException, SQLException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		ResultSet rs=null;
		ArrayList<Module> modules = new ArrayList<Module>();
		try {
		/*modified by Priyanka for VAPT Helper fix starts*/
			pst = conn.prepareStatement( "SELECT * FROM MASMODULE WHERE APP_ID=?");
			pst.setInt(1, appId);
			/*modified by Priyanka for VAPT Helper fix ends*/
		    rs = pst.executeQuery();
			while(rs.next()) {
				Module module = this.mapFields(rs);
				modules.add(module);
			}
			
			
		} catch (SQLException e) {
			logger.error("ERROR fetching module",e);
			throw new DatabaseException("Could not fetch modules due to an internal error");
		}finally{			
	    DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);}
		
		return modules;
	
	}
	/*Added by Padmavathi for TENJINCG-400 ends*/
	
	/*Added by Pushpalatha for TJN252-7 starts*/
	/* modified by Roshni for TENJINCG-1168 starts */
	/*public void validateFunc(List<String> funcs,int appId) throws DatabaseException, RequestValidationException {*/
	public void validateFunc(List<String> funcs,int appId,Connection conn) throws DatabaseException, RequestValidationException {
		/*modified by shruthi for sql injection starts*/
		
		for(String str : funcs) {
		try(
				PreparedStatement pst=conn.prepareStatement( "SELECT * FROM LRNR_AUDIT_TRAIL WHERE LRNR_APP_ID=? AND LRNR_STATUS IN ('QUEUED','LEARNING') AND LRNR_FUNC_CODE=?");)
		{
			pst.setInt(1, appId);
			pst.setString(2, str);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					throw new RequestValidationException("The selected Function(s) is already Learning, please review your selection and try again");
				}
			}
		}
		
		
		
		catch (SQLException e) {
			logger.error("ERROR fetching module",e);
			throw new DatabaseException("Could not fetch modules due to an internal error");
		}
		}
		/*modified by shruthi for sql injection ends*/
	}
	/*Added by Pushpalatha for TJN252-7 ends*/
}
