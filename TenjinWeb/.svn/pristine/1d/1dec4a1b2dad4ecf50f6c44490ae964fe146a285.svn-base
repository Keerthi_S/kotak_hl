 /***
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestRunServlet.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 */

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 03-11-2016            Leelaprasad             Defect #571
* 20-Jan-2017			Manish					TENJINCG-8
  16-06-2017			Sriram					TENJINCG-187
  28-July-2017			Gangadhar Badagi		TENJINCG-315
* 01-Sep-2017		    Manish					T25IT-413
* 17-04-2018			Preeti					TENJINCG-624
* 08-05-2018			Preeti					TENJINCG-625
* 26-06-2018            Padmavathi              T251IT-53
* 29-06-2018            Padmavathi              T251IT-68
* 24-10-2018			Sriram					TENJINCG-866
*/
package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.BridgeProcessor;
import com.ycs.tenjin.bridge.pojo.ExecutorProgress;
import com.ycs.tenjin.bridge.pojo.TaskManifest;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.CoreDefectHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.DefectHelper;
import com.ycs.tenjin.db.DeviceHelper;
import com.ycs.tenjin.db.ExecutionHelper;
import com.ycs.tenjin.db.IterationHelper;
import com.ycs.tenjin.db.PaginatedRecords;
import com.ycs.tenjin.db.ResultsHelper;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.defect.Defect;
import com.ycs.tenjin.device.RegisteredDevice;
import com.ycs.tenjin.handler.TestRunHandler;
import com.ycs.tenjin.pcloudy.Pcloudy;
import com.ycs.tenjin.pcloudy.PcloudyProcessor;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.run.TestRunProgress;
import com.ycs.tenjin.run.TestRunSearchRequest;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
 
@WebServlet("/TestRunServlet")
public class TestRunServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = org.slf4j.LoggerFactory
			.getLogger(TestRunServlet.class);

	 
	/*changed by manish for requirement# TJN_17  ENDS */ 
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String txn = request.getParameter("param");
		if(Utilities.trim(txn).equalsIgnoreCase("")) {
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User currentUser=tjnSession.getUser();
			Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
			map.put("user", currentUser);
			request.getSession().setAttribute("SCR_MAP", map);
			/*	Added by Pushpalatha for TENJINCG-1223 starts*/
			String status=request.getParameter("status");
			String message=request.getParameter("message");
			if(status!=null || message!=null){
				SessionUtils.setScreenState(status,message, request);
				SessionUtils.loadScreenStateToRequest(request);
			}
			/*	Added by Pushpalatha for TENJINCG-1223 ends*/
			request.getRequestDispatcher("v2/project_runs.jsp").forward(request, response);
		}else if(Utilities.trim(txn).equalsIgnoreCase("search")) {
			String start = request.getParameter("start");
			String length = request.getParameter("length");
			String orderColumn = Utilities.trim(request.getParameter("order[0][column]"));
			String orderDirection = Utilities.trim(request.getParameter("order[0][dir]"));
			String sortColumn = "";
			if(orderColumn.length() > 0) {
				sortColumn = Utilities.trim(request.getParameter("columns[" + orderColumn + "][data]"));
			}
			
			int pageNumber = 1;
			int maxRecords = 10;
			int draw=0;
			try {
				maxRecords = Integer.parseInt(length);
				pageNumber = (Integer.parseInt(start) / maxRecords) + 1;
			} catch (NumberFormatException e) {
				logger.warn("Invalid number passed for offset and/or maxrecords", e);
			}
			
			try {
				draw = Integer.parseInt(Utilities.trim(request.getParameter("draw")));
			} catch (NumberFormatException e1) {
				logger.warn("Invalid or no draw parameter passed");
			}
			
			
			TestRunSearchRequest searchRequest = TestRunSearchRequest.buildFromRequest(request);
			
			PaginatedRecords<TestRun> records = new PaginatedRecords<>();
			if(searchRequest != null) {
				searchRequest.setProjectId(SessionUtils.getTenjinSession(request) != null ? SessionUtils.getTenjinSession(request).getProject().getId() : 0);
				TestRunHandler handler = new TestRunHandler();
				try {
					records = handler.searchForRuns(searchRequest, maxRecords, pageNumber, sortColumn, orderDirection);
				} catch (DatabaseException e) {
					records.setError(e.getMessage());
				} finally {
					records.setDraw(draw);
					response.getWriter().write(new GsonBuilder().create().toJson(records));
				}
			}else {
				records.setError("Could not perform search due to an internal error.");
				records.setDraw(draw);
				response.getWriter().write(new GsonBuilder().create().toJson(records));
			}
		}
		
		else if (txn.equalsIgnoreCase("hydrate_run_defects")) {
			/* changed by manish for req TENJINCG-8 on 20-Jan-2017 starts */
			String tcRecId = request.getParameter("tcRecId");
			/* changed by manish for req TENJINCG-8 on 20-Jan-2017 ends */
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			String runId = request.getParameter("runId");
			CoreDefectHelper cHelper = new CoreDefectHelper();
			ResultsHelper runHelper = new ResultsHelper();
			List<Defect> defects = new ArrayList<Defect>();
			Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
			boolean allOk = true;
			String message = "";
			TestRun run = null;
			if(map != null){
				run = (TestRun) map.get("RUN");
				if(run == null){
					try {
						run = runHelper.hydrateRun(Integer.parseInt(runId), tjnSession.getProject().getId());
						map.put("RUN", run);
					} catch (NumberFormatException e) {
						logger.error("Invalid Run ID [{}]", runId);
						allOk = false;
						message = "Invalid Run ID [" + runId + "]. Please contact Tenjin Support.";
					} catch (DatabaseException e) {
						message = e.getMessage();
						allOk = false;
					}
				}
			}
			
			
			try {
				defects = cHelper.hydrateDefects(Integer.parseInt(runId));
				
				List<Integer> distinctAppIds = new ArrayList<Integer>();
				if(defects != null){
					for(Defect defect:defects){
						if(!distinctAppIds.contains(defect.getAppId())){
							distinctAppIds.add(defect.getAppId());
						}
					}
				}
				
				Map<Integer, List<String>> sevMap = new DefectHelper().getSeverityDefinitions(tjnSession.getProject().getId(), distinctAppIds);
				map.put("SEV_MAP", sevMap);
				map.put("DEFECTS", defects);
				
				
			} catch (DatabaseException e) {
				logger.error("ERROR --> " + e.getMessage());
				message = e.getMessage();
				allOk = false;
				
			} finally {
				if(allOk){
					map.put("STATUS", "SUCCESS");
				}else{
					map.put("STATUS", "ERROR");
					map.put("MESSAGE", message);
				}
				
				request.getSession().removeAttribute("SCR_MAP");
				request.getSession().setAttribute("SCR_MAP", map);
			}
			
			String callback = request.getParameter("callback");
			if(com.ycs.tenjin.util.Utilities.trim(callback).equalsIgnoreCase("")){
				callback="";
			}
			/* changed by manish for req TENJINCG-8 on 20-Jan-2017 starts */
			response.sendRedirect("rundefectlist.jsp?callback=" + callback+"&tcRecId="+tcRecId) ;
			/* changed by manish for req TENJINCG-8 on 20-Jan-2017 ends */
		} 

		/*added by manish for display run defects on 21-Nov-16 ends*/
		
		/*Added by Sriram for TENJINCG-187 */
		else if(txn.equalsIgnoreCase("abort")){
			String rId = request.getParameter("run");
			/*Added by Preeti for TENJINCG-624 starts*/
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			User user = tjnSession.getUser();
			String userId = user.getId();
			/*Added by Preeti for TENJINCG-624 ends*/
			int projectId = tjnSession.getProject().getId();
			JsonObject json = new JsonObject();
			try {
			/*Added by Padmavathi for T251IT-68 starts*/
				ExecutionHelper exeHelper= new ExecutionHelper();
			/*Added by Padmavathi for T251IT-68 ends*/	
				logger.info("Setting abort flag in run cache to true for Run ID [{}]", rId);
				ExecutorProgress eProgress = (ExecutorProgress)CacheUtils.getObjectFromRunCache(Integer.parseInt(rId));
				if(eProgress != null) {
					/*Added by Padmavathi for T251IT-68 starts*/
					TestRun testRun=exeHelper.fetchRunStatus(Integer.parseInt(rId));
					if(testRun!=null && !testRun.getStatus().equalsIgnoreCase("Complete")){
						/*Added by Padmavathi for T251IT-68 ends*/
						eProgress.setRunAborted(true);
						CacheUtils.putObjectInRunCache(Integer.parseInt(rId), eProgress);
						logger.info("Done");
						logger.info("Updating run status");
						new RunHelper().informAbort(Integer.parseInt(rId));
						/*Added by Padmavathi for T251IT-53 starts*/
						eProgress.setRunAborteduserId(userId);
						/*Added by Padmavathi for T251IT-53 ends*/
						json.addProperty("status", "success");
					}
				}else{
					/*Modified by Preeti for TENJINCG-624 starts*/
					logger.info("Abortig run, when thread is not found");
					new IterationHelper().abortRun(Integer.parseInt(rId),userId);
					/*Added by Preeti for TENJINCG-625 starts*/
					logger.info("Creating thread to send mail");
					TaskManifest manifest = new TaskManifest();
					manifest.setRunId(Integer.parseInt(rId));
					manifest.setProjectId(projectId);
					manifest.setTaskType("Mail");
					BridgeProcessor handler = new BridgeProcessor(manifest);
					handler.createTask1();
					handler.runTask();
					logger.info("Done");
					/*Added by Preeti for TENJINCG-625 ends*/
					json.addProperty("status", "success");
					/*Modified by Preeti for TENJINCG-624 ends*/
				}
				
				
			} catch (NumberFormatException e) {
				logger.error("Invalid Run Id {}", rId);
				json.addProperty("status", "error");
				json.addProperty("message", "Invalid Run ID.");
			} catch (DatabaseException e) {
				logger.error(e.getMessage());
				json.addProperty("status", "error");
				json.addProperty("message", e.getMessage());
			} catch (TenjinConfigurationException e) {
				logger.error(e.getMessage());
				json.addProperty("status", "error");
				json.addProperty("message", e.getMessage());
			} finally {
				response.getWriter().write(json.toString());
			}
		}
		/*Added by Sriram for TENJINCG-187 */
		//TENJINCG-1196
		else if(txn.equalsIgnoreCase("run_progress")) {
			String dispatchTo = "runprogress.jsp";
			String errorPage = "error.jsp";
			try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ)) {
				int runId = Integer.parseInt(request.getParameter("run"));
				TestRun testRun = new ResultsHelper().hydrateBasicRunInformation(conn, runId);
				request.setAttribute("run", testRun);
				String referer = request.getHeader("referer");
				logger.info("Referer for this request is {}", referer);
				TestRunProgress progress = new TestRunHandler().getRunProgress(runId);
				request.setAttribute("testCaseSummary", progress.getTestCaseExecutionProgress().getTestCases());
				request.setAttribute("currentStep", progress.getTestCaseExecutionProgress().getCurrentTestStep());
				request.setAttribute("currentTestCase", progress.getTestCaseExecutionProgress().getCurrentTestCase());
				request.setAttribute("runStartTimestamp", progress.getStartTimestamp());
				
				getManualFieldInputDetails(runId, request);
				getDeviceInformation(testRun, request);
				
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run ID --> {}", request.getParameter("run"));
				dispatchTo = errorPage;
			} catch (DatabaseException e) {
				logger.error(e.getMessage());
				dispatchTo = errorPage;
			} catch (Exception e) {
				logger.error("An unexpected error occurred while fetching run details", e);
				dispatchTo = errorPage;
			} finally {
				request.getRequestDispatcher(dispatchTo).forward(request, response);
			}
		}
		//TENJINCG-1196 ends

	}
	
	private void getManualFieldInputDetails(int runId, HttpServletRequest request) {
		TestObject output=null;
		String manualdata="";
		try {
			CacheManager cm = CacheManager.getInstance();				

			//3. Get a cache called "cacheStore"
			Cache cache = cm.getCache("cacheStore");						

			Element ele = cache.get(runId);
			output=(TestObject) ele.getObjectValue();
			if(output.getData().equalsIgnoreCase("false"))
			{
				manualdata=	output.getData();						
			}						
			request.setAttribute("pauseOrContinue",manualdata);
			request.setAttribute("manualField", output);
		}catch(Exception e){		
			logger.error("Error getting manual field input data", e);
		}
	}
	
	private void getDeviceInformation(TestRun run, HttpServletRequest request) {
		if(run == null) return;
		
		if(run.getDeviceRecId() >= 0) {			
			RegisteredDevice device = null;
			try {
				if(run.getDeviceFarmFlag()!=null && run.getDeviceFarmFlag().equalsIgnoreCase("Y")){
					Pcloudy objPcloudy=new PcloudyProcessor().getPcloudyCredentials(run.getMachine_ip());
					device=new PcloudyProcessor(objPcloudy).deviceById(run.getDeviceRecId());
				}else{
					device=new DeviceHelper().hydrateDevice(run.getDeviceRecId());	
				}
				
				request.setAttribute("device", device);
			} catch (DatabaseException e) {
				logger.error("Error getting device details", e);
			}
		}
	}
	 
}
/*changed by manish for requirement# TJN_17  ENDS */ 

