/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:   ProjectDashBoardServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 19-02-2019			Padmavathi		        Newly added for TENJINCG-924
 * 21-02-2019           Padmavathi              TENJINCG-924
 * 03-05-2019			Preeti					TENJINCG-1019
 
 */
package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ProjectDashboardHelper;
import com.ycs.tenjin.handler.ProjectDashboardHandler;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.util.Constants;

/**
 * Servlet implementation class ProjectDashBoard
 */
@WebServlet("/ProjectDashBoardServlet")
public class ProjectDashBoardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProjectDashBoardServlet() {
		super();
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ProjectDashboardHelper.class);
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String txn = request.getParameter("t");
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		ProjectDashboardHandler dashboardHandler = new ProjectDashboardHandler();
		ProjectHandler projectHandler=new ProjectHandler();
		if (txn.equalsIgnoreCase("project_dashboard")) {

			String prefrence = request.getParameter("prefrence");
			String updateProjectLog = request.getParameter("updateProjectLog");
			int prjId=0;
			try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ)){
				prjId=Integer.parseInt(request.getParameter("paramval"));
				Project project = dashboardHandler.hydrateProjectForDashboard(prjId, conn);

				if (project != null && updateProjectLog != null && updateProjectLog.equalsIgnoreCase("true")) {
					String userId = tjnSession.getUser().getId();
					int preferenceProjectId = projectHandler.hydrateUserProjectPreference(userId);
					if (preferenceProjectId != 0 && preferenceProjectId != project.getId()) {
						if (prefrence != null && prefrence.equalsIgnoreCase("Y"))
							projectHandler.updateUserProjectPrefrence(prjId, userId);

					}
					if (tjnSession.getProject() != null) {
						int oldProjectId = tjnSession.getProject().getId();
						if (oldProjectId != project.getId()) {
							// end old project session for user
							projectHandler.endProjectSession(oldProjectId, userId);
							// start new project session for user
							projectHandler.startProjectSession(project.getId(), userId);
							tjnSession.setProject(project);
						}
					} else {
						projectHandler.startProjectSession(project.getId(), userId);
					}

				}
				/*Modified by Preeti for TENJINCG-1019 starts*/
				
				String userId = tjnSession.getUser().getId();
				String roleInProject = tjnSession.getUser().getRoleInCurrentProject();
				Map<String, Integer> entityCount = dashboardHandler.hydrateEntityCount(project.getId(), conn);
				request.setAttribute("entityCount", entityCount);
				if(roleInProject.equalsIgnoreCase("Project Administrator")){
					Map<TestSet,ArrayList<Integer>> ts = dashboardHandler.hydrateTestSetForProject(project.getId(), conn);
					TestRun run=dashboardHandler.hydrateLastRunBasicDetails(conn,prjId);
					Map<TestSet,ArrayList<Integer>> tsForDefects = dashboardHandler.hydrateTestSetForProjectDefects(project.getId(), conn);
					Map<String,Map<String,Integer>> defects=dashboardHandler.hydrateLatestDefectsForProjects(conn, prjId);
					request.setAttribute("run", run);
					request.setAttribute("testSets", ts);
					request.setAttribute("testSetsForDefect", tsForDefects);
					request.setAttribute("defects", defects);
				}else{
					Map<TestSet,ArrayList<Integer>> ts = dashboardHandler.hydrateTestSetForProjectForUser(project.getId(),userId, conn);
					TestRun run=dashboardHandler.hydrateLastRunBasicDetailsForUser(conn,userId,prjId);
					Map<TestSet,ArrayList<Integer>> tsForDefects = dashboardHandler.hydrateTestSetForProjectDefectsForUser(project.getId(),userId, conn);
					Map<String,Map<String,Integer>> defects=dashboardHandler.hydrateLatestDefectsForProjectsForUser(conn,userId, prjId);
					request.setAttribute("run", run);
					request.setAttribute("testSets", ts);
					request.setAttribute("testSetsForDefect", tsForDefects);
					request.setAttribute("defects", defects);
				}
				request.setAttribute("project", project);
				/*Modified by Preeti for TENJINCG-1019 ends*/
				tjnSession.setProject(project);
				request.getRequestDispatcher("v2/project_dashboard.jsp").forward(request, response);

			} catch (NumberFormatException e) {
				
				logger.error("Error ", e);
				logger.error(e.getMessage());
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
				logger.error(e.getMessage());
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (SQLException e1) {
				
				logger.error("Error ", e1);
				logger.error(e1.getMessage());
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} 

		}else if(txn.equalsIgnoreCase("generate_Report")){
			String param=request.getParameter("param");
			/*Added by Preeti for TENJINCG-1019 starts*/
			String tsRecId = request.getParameter("tsRecId");
			/*Added by Preeti for TENJINCG-1019 ends*/
			JSONObject responseJson = new JSONObject();
			try {
				if(param!=null && param.equalsIgnoreCase("weekly")){
					JSONObject weeklyReportJson = new JSONObject();
					/*Modified by Preeti for TENJINCG-1019 starts*/
					String roleInProject = tjnSession.getUser().getRoleInCurrentProject();
					if(roleInProject.equalsIgnoreCase("Project Administrator")){
						if(Integer.parseInt(tsRecId)==0)
							weeklyReportJson = dashboardHandler.generateweeklyReportForTCs(tjnSession.getProject().getId());
						else
							weeklyReportJson = dashboardHandler.generateweeklyReportForTCs(tjnSession.getProject().getId(),Integer.parseInt(tsRecId));
						}
					else{
						if(Integer.parseInt(tsRecId)==0)
							weeklyReportJson = dashboardHandler.generateweeklyReportForTCs(tjnSession.getProject().getId(),tjnSession.getUser().getId());
						else
							weeklyReportJson = dashboardHandler.generateweeklyReportForTCs(tjnSession.getProject().getId(),Integer.parseInt(tsRecId),tjnSession.getUser().getId());
						}
					/*Modified by Preeti for TENJINCG-1019 ends*/
					responseJson.put("STATUS", "SUCCESS");
					responseJson.put("WEEKLY_REPORT_JSON", weeklyReportJson);
				}else{
					JSONObject dayReportJson = new JSONObject();
					/*Modified by Preeti for TENJINCG-1019 starts*/
					String roleInProject = tjnSession.getUser().getRoleInCurrentProject();
					if(roleInProject.equalsIgnoreCase("Project Administrator")){
					if(Integer.parseInt(tsRecId)==0)
						dayReportJson = dashboardHandler.generateDayReportForTCs(tjnSession.getProject().getId());
					else
						dayReportJson = dashboardHandler.generateDayReportForTCs(tjnSession.getProject().getId(),Integer.parseInt(tsRecId));
					}
					else{
						if(Integer.parseInt(tsRecId)==0)
							dayReportJson = dashboardHandler.generateDayReportForTCs(tjnSession.getProject().getId(),tjnSession.getUser().getId());
						else
							dayReportJson = dashboardHandler.generateDayReportForTCs(tjnSession.getProject().getId(),Integer.parseInt(tsRecId),tjnSession.getUser().getId());
						}
					/*Modified by Preeti for TENJINCG-1019 ends*/
					responseJson.put("STATUS", "SUCCESS");
					responseJson.put("DAY_REPORT_JSON", dayReportJson);
				}
				
				
			} catch (NumberFormatException | DatabaseException | ParseException | JSONException e) {
				try {
					responseJson.put("STATUS", "ERROR");
					responseJson.put("MESSAGE", "An Internal Error occurred. Please contact your System Administrator");
				} catch (JSONException e1) {}
			}
		response.getWriter().write(responseJson.toString());
		}
		/*Added by Preeti for TENJINCG-1019 starts*/
		else if(txn.equalsIgnoreCase("run_details")){
			JSONObject responseJson = new JSONObject();
			int runId = Integer.parseInt(request.getParameter("param"));
			try {
				JSONObject runJson = new JSONObject();
				runJson = dashboardHandler.getRunDetails(runId);
				responseJson.put("STATUS", "SUCCESS");
				responseJson.put("RUN_JSON", runJson);
			}
			catch (NumberFormatException | DatabaseException | ParseException | JSONException e) {
				try {
					responseJson.put("STATUS", "ERROR");
					responseJson.put("MESSAGE", "An Internal Error occurred. Please contact your System Administrator");
				} catch (JSONException e1) {}
			}
			response.getWriter().write(responseJson.toString());
		}
		else if(txn.equalsIgnoreCase("defect_details")){
			JSONObject responseJson = new JSONObject();
			int runId = Integer.parseInt(request.getParameter("param"));
			try {
				JSONObject defectJson = new JSONObject();
				defectJson = dashboardHandler.getDefectDetails(runId);
				responseJson.put("STATUS", "SUCCESS");
				responseJson.put("DEFECT_JSON", defectJson);
			}
			catch (NumberFormatException | DatabaseException | ParseException | JSONException e) {
				try {
					responseJson.put("STATUS", "ERROR");
					responseJson.put("MESSAGE", "An Internal Error occurred. Please contact your System Administrator");
				} catch (JSONException e1) {}
			}
			response.getWriter().write(responseJson.toString());
		}
		/*Added by Preeti for TENJINCG-1019 ends*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		doGet(request, response);
	}

}
