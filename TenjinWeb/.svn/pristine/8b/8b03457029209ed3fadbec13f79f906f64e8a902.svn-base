/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AjaxResponse.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 24-Oct-2017           Sriram Sridharan          Newly Added For TENJINCG-396
 * 14-03-2018            Padmavathi                TENJINCG-612
 * 22-03-2018            Padmavathi                TENJINCG-612
 * 14-06-2018            Padmavathi                for T251IT-14
 * 05-02-2020			 Roshni					   TENJINCG-1168
 * 15-06-2021			 Ashiki					   TENJINCG-1275
 * 
 */

package com.ycs.tenjin.servlet.ajax;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.core.ajax.AjaxResponse;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.ApiHandler;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.handler.MessageValidateHandler;
import com.ycs.tenjin.handler.ModuleHandler;
import com.ycs.tenjin.rest.JsonSerializationStrategy;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class AutFunctionAjaxServlet
 */
@WebServlet("/AutFunctionAjaxServlet")
public class AutFunctionAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(AutFunctionAjaxServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AutFunctionAjaxServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String entity = Utilities.trim(request.getParameter("entity"));
		if(entity.equalsIgnoreCase("function_group")) {
			String aId = request.getParameter("a");
			Gson gson = new GsonBuilder().setExclusionStrategies(new JsonSerializationStrategy()).create();
			try {
				List<String> groups = new AutHandler().getFunctionGroups(Integer.parseInt(aId));
				response.getWriter().write(gson.toJson(new AjaxResponse("success", "", groups)));
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid Application ID {}", aId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			} catch (DatabaseException e) {
				logger.error(e.getMessage(),e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.db.error"), null)));
			}
		}
		/*Modified by Padmavathi for TENJINCG-612 starts*/
		else if(entity.equalsIgnoreCase("function")) {
			String aId = request.getParameter("a");
			Gson gson = new GsonBuilder().setExclusionStrategies(new JsonSerializationStrategy()).create();
			try {
				List<Module> modules = new ModuleHandler().getModules(Integer.parseInt(aId));
				response.getWriter().write(gson.toJson(new AjaxResponse("success", "", modules)));
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid Application ID {}", aId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			} catch (DatabaseException e) {
				logger.error(e.getMessage(),e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.db.error"), null)));
			}
		}else if(entity.equalsIgnoreCase("api")) {
			String aId = request.getParameter("a");
			Gson gson = new GsonBuilder().setExclusionStrategies(new JsonSerializationStrategy()).create();
			try {
			/* modified by Roshni for TENJINCG-1168 starts */
				/*List<Api> apis = new ApiHelper().hydrateAllApi(aId);*/
				List<Api> apis = new ApiHandler().hydrateAllApi(aId);
				/* modified by Roshni for TENJINCG-1168 ends */
				response.getWriter().write(gson.toJson(new AjaxResponse("success", "", apis)));
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid Application ID {}", aId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			} catch (DatabaseException e) {
				logger.error(e.getMessage(),e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.db.error"), null)));
			}
		}
		else if(entity.equalsIgnoreCase("apiOperation")) {
			String aId = request.getParameter("a");
			String apiCode= request.getParameter("apiCode");
			Gson gson = new GsonBuilder().setExclusionStrategies(new JsonSerializationStrategy()).create();
			try {
				List<ApiOperation> apiOperations = new ApiHelper().fetchApiOperations(aId,apiCode);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", "", apiOperations)));
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid Application ID {}", aId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			}
		}
		else if(entity.equalsIgnoreCase("operation")) {
			String aId = request.getParameter("a");
			Gson gson = new GsonBuilder().setExclusionStrategies(new JsonSerializationStrategy()).create();
			try {
				Aut aut = new AutHandler().getApplication(Integer.parseInt(aId));
				String [] operations=null;
				if(aut.getOperation()!=null){
					 operations= aut.getOperation().split(",");
				}
				response.getWriter().write(gson.toJson(new AjaxResponse("success", "", operations)));
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid Application ID {}", aId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			} catch (DatabaseException e) {
				logger.error("ERROR - Invalid Application ID {}", aId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			}
		}
		else if(entity.equalsIgnoreCase("autLoginTypes")) {
			String aId = request.getParameter("a");
			
			Gson gson = new GsonBuilder().setExclusionStrategies(new JsonSerializationStrategy()).create();
			try {
				Aut aut = new AutHandler().getApplication(Integer.parseInt(aId));
				String [] autLoginTypes=null;
			/*	Modified by Padmavathi for T251IT-14 starts*/
				/*if(aut.getOperation()!=null){*/
				if(aut.getLoginUserType()!=null){
			/*	Modified by Padmavathi for T251IT-14 ends*/
					autLoginTypes= aut.getLoginUserType().split(",");
				}
				response.getWriter().write(gson.toJson(new AjaxResponse("success", "", autLoginTypes)));
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid Application ID {}", aId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			} catch (DatabaseException e) {
				logger.error("ERROR - Invalid Application ID {}", aId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			}
		}
		else if(entity.equalsIgnoreCase("apiResponsertype")) {
			String aId = request.getParameter("a");
			String apiCode= request.getParameter("apiCode");
			String operation= request.getParameter("operation");
			Gson gson = new GsonBuilder().setExclusionStrategies(new JsonSerializationStrategy()).create();
			try {
				String responseType = new ApiHelper().fetchAPIResponseTypes(Integer.parseInt(aId),apiCode,operation);
				if(responseType!=null&&!responseType.equals("0")){
					String [] responseTypeArray= responseType.split(",");
					response.getWriter().write(gson.toJson(new AjaxResponse("success", "", responseTypeArray)));
				}else{
					response.getWriter().write(gson.toJson(new AjaxResponse("success", "", "")));
				}
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid Application ID {}", aId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			} catch (DatabaseException e) {
				logger.error("ERROR - Invalid Application ID {}", aId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			}
		}
		
		/*Added by Ashiki for TENJINCG-1275 starts*/
		else if(entity.equalsIgnoreCase("fetch_Message_Name")) {
			String appId = request.getParameter("a");
			String msgFormat= request.getParameter("msgFormat");
			Gson gson = new GsonBuilder().setExclusionStrategies(new JsonSerializationStrategy()).create();
			try {
				String responseType = new MessageValidateHandler().fetchMessageName(Integer.parseInt(appId),msgFormat);
				
				if(responseType!=null&&!responseType.equals("0")){
					String [] responseTypeArray= responseType.split(",");
					response.getWriter().write(gson.toJson(new AjaxResponse("success", "", responseTypeArray)));
				}else{
					response.getWriter().write(gson.toJson(new AjaxResponse("success", "", "")));
				}
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid Application ID {}", appId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			} catch (DatabaseException e) {
				logger.error("ERROR - Invalid Application ID {}", appId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			}
		}
		
		else if(entity.equalsIgnoreCase("messageFormat")) {

			String appId = request.getParameter("a");
			Gson gson = new GsonBuilder().setExclusionStrategies(new JsonSerializationStrategy()).create();
			try {
				String msgFormat = new MessageValidateHandler().fetchMessageFormat(Integer.parseInt(appId));
				if(msgFormat!=null&&!msgFormat.equals("0")){
					String [] msgFormatArray= msgFormat.split(",");
					response.getWriter().write(gson.toJson(new AjaxResponse("success", "", msgFormatArray)));
				}else{
					response.getWriter().write(gson.toJson(new AjaxResponse("success", "", "")));
				}
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid Application ID {}", appId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			} catch (DatabaseException e) {
				logger.error("ERROR - Invalid Application ID {}", appId, e);
				response.getWriter().write(gson.toJson(new AjaxResponse("success", SessionUtils.loadMessage(request, "generic.error"), null)));
			}
		
		}
		/*Added by Ashiki for TENJINCG-1275 ends*/
	}
	/*Modified by Padmavathi for TENJINCG-612 ends*/
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
