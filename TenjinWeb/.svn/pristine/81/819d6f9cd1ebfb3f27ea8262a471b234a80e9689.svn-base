/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RemoteApiService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd. 
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 15-sep-2016			Sameer Gupta			new file added
 */

package com.ycs.tenjin.rest.remoteapi;

import java.io.File;
import java.util.Date;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.remoteapiserver.RemoteApiServerCache;
import com.ycs.tenjin.bridge.remoteapiserver.RemoteApiServerRequest;
import com.ycs.tenjin.bridge.utils.AssistedLearningUtils;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.rest.RestUtils;

@Path("/remoteapi")
public class RemoteApiService {

	private static final Logger logger = LoggerFactory
			.getLogger(RemoteApiService.class);

	@POST
	@Path("/checkRequest")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	@PermitAll
	public Response checkRequest(String clientIp) {
		Response response = null;
		try {
			String json = RemoteApiServerCache.getRequest(clientIp);
			if (json == null) {
				json = "";
			} else {
				Gson gson = new Gson();
				RemoteApiServerRequest objRemoteApiServerRequest = gson
						.fromJson(json, RemoteApiServerRequest.class);

				if (!objRemoteApiServerRequest.getRequestAccepted()) {
					objRemoteApiServerRequest.setRequestAccepted(true);

					String value = gson.toJson(objRemoteApiServerRequest,
							objRemoteApiServerRequest.getClass());

					RemoteApiServerCache.putRequest(clientIp, value);
				}

			}

			response = RestUtils.buildResponse(Status.CREATED, json, null);
		} catch (Exception e) {
			response = RestUtils
					.buildResponse(
							Status.INTERNAL_SERVER_ERROR,
							"An internal error occurred. Please contact Tenjin Administrator",
							null);
		}
		return response;
	}

	@POST
	@Path("/sendResponse")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	@PermitAll
	public Response sendResponse(String responseMessage) {
		logger.info("Request to persisting Execution Data");
		Response response = null;
		try {

			JsonElement jsonElementMessage = new JsonParser()
					.parse(responseMessage);
			JsonObject jsonObjectMessage = jsonElementMessage.getAsJsonObject();

			String clientIp = jsonObjectMessage.get("clientIp").getAsString();

			RemoteApiServerCache.putResponse(clientIp, responseMessage);

			response = RestUtils.buildResponse(Status.CREATED, "", null);
		} catch (Exception e) {
			response = RestUtils
					.buildResponse(
							Status.INTERNAL_SERVER_ERROR,
							"An internal error occurred. Please contact Tenjin Administrator",
							null);
		}
		System.out.println(response);
		return response;
	}

	@POST
	@Path("/clearCache")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	@PermitAll
	public Response clearCache(String clientIp) {
		logger.info("Request to persisting Execution Data");
		Response response = null;
		try {

			JsonObject responseMessage = new JsonObject();
			responseMessage.addProperty("clientIp", clientIp);
			responseMessage.addProperty("exceptionType", "RemoteException");
			responseMessage.addProperty("exceptionMessage",
					"Client was restarted.");
			RemoteApiServerCache.putResponse(clientIp,
					responseMessage.toString());

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				logger.info("Thread sleep issue while processing request [{}]",
						clientIp);
			}

			RemoteApiServerCache.removeRequest(clientIp);

			response = RestUtils.buildResponse(Status.CREATED, "", null);
		} catch (Exception e) {
			response = RestUtils
					.buildResponse(
							Status.INTERNAL_SERVER_ERROR,
							"An internal error occurred. Please contact Tenjin Administrator",
							null);
		}
		System.out.println(response);
		return response;
	}

	@POST
	@Path("/exportZip")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes(MediaType.TEXT_PLAIN)
	@PermitAll
	public Response download(String clientIp) {

		ResponseBuilder response = null;

		String filename = "adp";
		/*
		 * CHanges done by sharath for Burgan handling space characters while
		 * reading adp.zip file for Tenjin client on 14/10/2018 starts
		 */
		String filepath = this.getClass().getClassLoader()
				.getResource("adp.zip").getFile().replace("%20", " ");
		File file = new File(filepath);
		/*
		 * CHanges done by sharath for Burgan handling space characters while
		 * reading adp.zip file for Tenjin client on 14/10/2018 Ends
		 */

		response = Response.ok(file).type(MediaType.APPLICATION_OCTET_STREAM);
		response.header("Content-Disposition", "attachment; filename=\""
				+ filename + " - " + new Date().toString() + ".zip\"");

		return response.build();
	}

	@POST
	@Path("/getAssistedLearningData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	@PermitAll
	public String getAssistedLearningData(String appNameAndFunctionCode) {

		String[] arr = appNameAndFunctionCode.split("@@");
		String functionCode = arr[1];
		String assistedLearningString = "";

		try {
			String appName = arr[0].trim();
			Aut aut = new AutHandler().getApplication(appName);

			assistedLearningString = AssistedLearningUtils
					.getAssistedLearningDataXMLFormat(aut, functionCode);

		} catch (Exception e) {
		}
		return assistedLearningString;
	}

}
