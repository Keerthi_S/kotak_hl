<!--

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  testset_list.jsp


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
 19-11-2020             Priyanka                TENJINCG-1231  
  -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://tenjin.yethi.com/jsp/tags" prefix="tjn" %>
<%@page import="java.util.UUID" %>
<%@page import="com.google.common.cache.Cache" %>
<%@page import="com.google.common.cache.CacheBuilder" %>
<%@page import="java.util.concurrent.TimeUnit" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TestSet List</title>
		<link rel='stylesheet' href='css/cssreset-min.css' />
		<link rel='stylesheet' href='css/ifr_main.css' />
		<link rel='stylesheet' href='css/style.css' />
		<link rel='stylesheet' href='css/tabs.css' />
		<link rel="Stylesheet" href="css/jquery.dataTables.css" />
		<link rel='stylesheet' href='css/buttons.css' />
		<link rel='stylesheet' href='css/960_16_col.css' />
		<script type='text/javascript' src='js/jquery-3.6.0.min.js'></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type='text/javascript' src='js/pages/tjnmaster.js'></script>
		<script type='text/javascript' src=' js/bootstrap.min.js'></script>
		<script type='text/javascript' src='js/pages/v2/testset_list.js'></script>
		<script type='text/javascript' src="js/jquery-ui.js" ></script>	
</head>
<body>
<!-- added by shruthi for CSRF token starts -->
<%
    Cache<String, Boolean> csrfTokenCache = null;
    csrfTokenCache = CacheBuilder.newBuilder().maximumSize(5000).expireAfterWrite(300, TimeUnit.SECONDS).build();
    session.setAttribute("csrfTokenCache", csrfTokenCache);
    UUID uuid = UUID.randomUUID();    String csrftoken = uuid.toString();csrfTokenCache.put(csrftoken, Boolean.TRUE);
    session.setAttribute ("csrftoken_session", csrftoken);
    %>
<!-- added by shruthi for CSRF token ends -->
	<div class='title'>
			
			<p>All Test Sets</p>
 
			
		</div>
		<form name='main_form' action='TestCaseServlet' method='POST'>
			<div class='toolbar'>
			    <!-- added by shruthi for CSRF token starts -->
		        <input type = "hidden"  id="csrftoken_form" name ="csrftoken_form" value = <%= csrftoken %> />
		        <!-- added by shruthi for CSRF token ends -->
				<input type='button' value='New' id='btnNew' class='imagebutton new'/>
				<input type='button' value='Remove' id='btnDelete' class='imagebutton delete'/>
				<input type='button' value='Upload' id='btnUpload' class='imagebutton upload'/>
				<input type='button' value='Download' id='btnDownload' class='imagebutton download'/>
				<input type='button' value='Refresh' id='btnRefreshSets' class='imagebutton refresh' />
				<img src='images/inprogress.gif'
				id='img_download_loader' style='display: none;' />
			</div>
			<input type='hidden' id='tjn-status' value='${screenState.status }' />
			<input type='hidden' id='tjn-message' value='${screenState.message }' />
			<input type='hidden' id='domain' name='domain' value='${project.domain}'/>
			<input type='hidden' id='projectName' name='projectName' value='${project.name }'/>
			<input type='hidden' id='pid' name='pid' value='${project.id }'/>
			<input type='hidden' id='user' name='user' value='${user }'/>
			<input type='hidden' id='downloadPath' name='downloadPath' value='${fn:escapeXml(downloadPath) }'/>
			<!-- Added by Priyanka for TENJINCG-1231 starts*/ --> 
			<input type='hidden' name='edate' id='endDate' value='${edate }'/>
			<!-- Added by Priyanka for TENJINCG-1231 ends*/ --> 
			<div id='user-message'></div>
			<div class='form container_16'>

				<fieldset>
					<legend>All Test Sets</legend>
				</fieldset>
				<div id='dataGrid'>
					 
					<table id='testset-table' class='display'>
							<thead>
								<tr>
									<th class='table-row-selector nosort'><input type='checkbox'  id='tbl-select-all-rows-tc' class='tbl-select-all-rows'/></th>
									 
									<th>Name</th>
									<th>Type</th>
									<th>Created On</th>	
									<th>Created By</th>
									<th class="text-center">Last Run Id</th>
									<th>Last Run Status</th>
									
								</tr>
							</thead>
						 
						</table>
				</div>
			</div>
		</form>
		<div class='modalmask'></div>
		
		<div class='subframe' id='uploadExcel' style='position:absolute;top:100px;left:250px;'>
			<iframe src='' scrolling="no" seamless="seamless"></iframe> 
		</div>
</body>
</html>