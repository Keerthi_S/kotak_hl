/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  InitialLaunchServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 12-10-2018        Leelaprasad P         newly added for TENJINCG-872 
* 19-11-2018		Prem					TENJINCG-838
* 24-06-2019        Padmavathi              for license
* 28-08-2019			Prem			      TENJINCG-1099
*/


package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.LicenseExpiredException;
import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.handler.LicenseHandler;
import com.ycs.tenjin.license.License;

/**
 * Servlet implementation class InitialLaunchServlet
 */
/*@WebServlet("/InitialLaunchServlet")*/
public class InitialLaunchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = LoggerFactory
			.getLogger(InitialLaunchServlet.class);
	
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InitialLaunchServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LicenseHandler licenseHandler=new LicenseHandler();
		/*Added by prem for TENJINCG-1099 starts*/
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		/*Added by prem for TENJINCG-1099 starts*/
		try {
			try{
			License  license = (License) CacheUtils.getObjectFromRunCache("licensedetails");
			
			if(license==null){
				// list contains license key and renewal license key (if license is renewal before license expires)  
				List<String> licenseKeys=licenseHandler.getLicenseKey();
				if(licenseKeys ==null || licenseKeys.size()==0){
					String systemId=licenseHandler.generateSystemId(); 
					request.setAttribute("systemId", systemId);
					request.getRequestDispatcher("license_installation.jsp").forward(request, response);
				}else{
					license=licenseHandler.validateLicenseKey(licenseKeys.get(0),false);
					licenseHandler.putLicenseInCache(license);
					if(licenseKeys.size()>1 && licenseKeys.get(1)!=null){
						licenseHandler.putRenewalLicenseInCache(licenseHandler.getLicense(licenseKeys.get(1)));
					}
					request.getRequestDispatcher("login.jsp").forward(request, response);
				}
			}else{
				licenseHandler.licenseActiveValidation(license, false);
				/*Added by prem for TENJINCG-1099 starts*/
				if(tjnSession.isUrlSession()){
					request.getRequestDispatcher("UserLandingServlet?key=landing").forward(request, response);
				}else
					/*Added by prem for TENJINCG-1099 Ends*/
				{
				request.getRequestDispatcher("login.jsp").forward(request, response);
				}
				}
			}catch (TenjinServletException |LicenseExpiredException|LicenseInactive e ) {
				request.setAttribute("systemId",licenseHandler.generateSystemId());
				request.setAttribute("status", "error");
				request.setAttribute("message", e.getMessage());
				request.getRequestDispatcher("license_installation.jsp").forward(request, response);
			} 
		} 
		catch (NullPointerException e) {
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}catch (Exception e) {
			request.setAttribute("systemId", "N-A");
			request.setAttribute("status", "error");
			request.setAttribute("message", "Internal error occured.Please contact Tenjin Support.");
			request.getRequestDispatcher("license_installation.jsp").forward(request, response);
			logger.error("Error ", e);
		}
		
		/*Added by Prem for TENJINCG-837 */
	
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
	