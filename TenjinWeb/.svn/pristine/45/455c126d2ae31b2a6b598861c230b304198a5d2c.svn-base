/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AuthenticationServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
12-10-2018        Leelaprasad P          TENJINCG-872-re written 
19-10-2018           Leelaprasad             TENJINCG-829
19-11-2018			Prem						TENJINCG-837
26-11-2018			Prem						TENJINCG-837
03-12-2018			Prem		     for defect  TJNUN262-36 
14-12-2018			Prem				     TJNUN262-57
06-05-2019			Roshni					TENJINCG-1036
07-05-2019			Pushpalatha				TENJINCG-1043
24-06-2019          Padmavathi              for license 
28-08-2019			Prem			      TENJINCG-1099
03-04-2020			Shruthi				  TENJINCG-1112
12-06-2020			Ruksar					v210Reg-15
 */

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.DuplicateUserSessionException;
import com.ycs.tenjin.LicenseExpiredException;
import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.MaxActiveUsersException;
import com.ycs.tenjin.TenjinAuthenticationException;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.UserBlockedException;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.UserHelperNew;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.handler.LicenseHandler;
import com.ycs.tenjin.handler.TenjinSessionHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class AuthenticationServlet
 */
// @WebServlet("/AuthenticationServlet")
public class AuthenticationServlet extends HttpServlet {
	private static Logger logger = LoggerFactory
			.getLogger(AuthenticationServlet.class);
	private static final long serialVersionUID = 1L;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AuthenticationServlet() {
		super();
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		String task = request.getParameter("t");
		
		/* Added by Roshni for TENJINCG-1036 starts */
			if (task.equalsIgnoreCase("auth_error_reset_pwd") || task.equalsIgnoreCase("auth_error")) {
				String userid=(String)request.getSession().getAttribute("userid");
				User user=null;
				try {
					user=new UserHandler().getUser(userid);
				} catch (RecordNotFoundException e) {
					
					logger.error("Error ", e);
				} catch (DatabaseException e) {
					
					logger.error("Error ", e);
				}
				request.getSession().setAttribute("user", user);
				request.getRequestDispatcher("login.jsp?param=resetpwd")
				.forward(request, response);
			}
		/* Added by Roshni for TENJINCG-1036 ends */
			/*modified by paneendra for VAPT fix ends*/
		/*Added by Pushpalatha for TENJINCG-1043 starts*/
			else if (task.equalsIgnoreCase("resetPassword")) {

				String question=request.getParameter("question");
				String answer=request.getParameter("answer");
				String userId=(String) request.getSession().getAttribute("userId");
				Map<String,String> map=(Map<String, String>) request.getSession().getAttribute("securityQueAndAns");
				if(Utilities.trim(answer).equals("")){
					SessionUtils.setScreenState("error", "Please provide answer", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("login", "fail");
					request.getSession().setAttribute("securityQueAndAns", map);
					request.setAttribute("securityQueAndAns",map);
					SessionUtils.loadScreenStateToRequest(request);
					request.getRequestDispatcher("login.jsp").forward(
							request, response);
				}

				boolean securityMatch=false;
				for (Map.Entry<String, String> entry : map.entrySet()) {
					if(Utilities.trim(question).equals(entry.getKey()) && Utilities.trim(answer).equals(entry.getValue())){
						securityMatch=true;

						User user;
						try {
							user = new UserHandler().getUser(userId);
							/*changed by shruthi for Tenj210-118 starts*/
							response.sendRedirect("TenjinPasswordResetServlet?param=resetreq&usermail="+user.getEmail()+"&userid="+user.getId()+"&username="+user.getFirstName());
							/*changed by shruthi for Tenj210-118 ends*/
						} catch (RecordNotFoundException e) {
							
							logger.error("Error ", e);
						} catch (DatabaseException e) {
							
							logger.error("Error ", e);
						}

					}
				}

				if(!securityMatch){
					try {
						new UserHelperNew().blockUser(userId);
					} catch (DatabaseException e) {
						
						logger.error("Error ", e);
					}
					SessionUtils.setScreenState("error", "user.blocked",
							new String[] { userId }, request);
					response.sendRedirect("AuthenticationServlet?t=auth_error");
				}

			} 
		/*Added by Pushpalatha for TENJINCG-1043 ends*/

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		logger.info("Received new authentication request");
		LicenseHandler licenseHandler = new LicenseHandler();
		String userName = Utilities.trim(request.getParameter("txtLoginName"));
		String password = request.getParameter("txtPassword");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		request.changeSessionId();
		if (ipAddress == null)
			ipAddress = request.getRemoteAddr();

		User user = null;
		Map<String, Object> map = new HashMap<String, Object>();
		TenjinSessionHandler sessionHandler = new TenjinSessionHandler();

		/*Added by Prem for TENJINCG-837 */
		
		try{
			/*Modified by Padmavathi for license starts*/
			/*user = sessionHandler.authenticate(userName, password, ipAddress);*/
			logger.info("authenticate method in TenjinSessionHandler is getting called");
			user = sessionHandler.authenticate(userName, password, ipAddress,request);
			logger.info("Authentication done succesfully");
			/*Modified by Padmavathi for license ends*/
			if (user != null) {
				TenjinSession session = new TenjinSession();
				session.setTenjinSessionId(user.getTenjinSessionId());
				session.setUser(user);
				/*Added by prem for TENJINCG-1099 starts*/
				session.setUrlSession(true);
				/*Added by prem for TENJINCG-1099 Ends*/
				request.getSession().setAttribute("TJN_SESSION", session);
				/*Added by ruksar for v210Reg-15 Starts*/
				UserHelperNew userhelper = new UserHelperNew();
				int pwdHistCount = userhelper.hydrateUserPassword(user.getId());
				if(pwdHistCount<1)  
					/*Added by ruksar for v210Reg-15 Ends*/{
					logger.debug("{} is logging in for the first time");
					request.getSession().setAttribute("SCR_MAP", map);
					response.sendRedirect("UserPasswordServlet?operation=firstLogin&userid="
							+ user.getId());
				} else {
					logger.info("sending control to userlandingservlet..");
					response.sendRedirect("UserLandingServlet?key=login");
				}
			} 
		} catch (DatabaseException e) {
			SessionUtils.setScreenState("error", "generic.db.error", request);
			response.sendRedirect("AuthenticationServlet?t=auth_error");
		}

		/* Added by Roshni for TENJINCG-1036 starts */
		catch(TenjinAuthenticationException e){
			request.getSession().setAttribute("userid", userName);
			/*Added by Shruthi for TENJINCG-1112 starts*/
			SessionUtils.setScreenState("error", "auth.failed", request);
			/*Added by Shruthi for TENJINCG-1112 ends*/
			response.sendRedirect("AuthenticationServlet?t=auth_error_reset_pwd");
		}
		/* Added by Roshni for TENJINCG-1036 ends */
		catch (TenjinServletException e) {
			SessionUtils.setScreenState("error", "auth.failed", request);
			response.sendRedirect("AuthenticationServlet?t=auth_error");
		} catch (Exception e) {
			try{


				if (e instanceof DuplicateUserSessionException) {
					request.setAttribute("userid", userName);
					request.setAttribute("ipAddress",
							((DuplicateUserSessionException) e).getTerminal());
					request.getRequestDispatcher("authenticateError.jsp").forward(
							request, response);
				} else if (e instanceof UserBlockedException) {
					/*Modified by Pushpalatha for TENJINCG-1043 starts*/
					Map<String,String> securityQueAndAns=null;
					try {
						securityQueAndAns=new UserHandler().hydrateSecurityQuestionForUser(userName);
						if(securityQueAndAns.size()<1){
							new UserHelperNew().blockUser(userName);
							SessionUtils.setScreenState("error", "user.blocked",
									new String[] { userName }, request);
							response.sendRedirect("AuthenticationServlet?t=auth_error");
						}else{
							request.setAttribute("login", "fail");
							request.getSession().setAttribute("securityQueAndAns", securityQueAndAns);
							request.getSession().setAttribute("userId", userName);
							request.setAttribute("securityQueAndAns",securityQueAndAns);
							SessionUtils.loadScreenStateToRequest(request);
							request.getRequestDispatcher("login.jsp").forward(
									request, response);
						}
					} catch (SQLException e1) {
						logger.error("Error ", e1);
					} catch (DatabaseException e1) {
						logger.error("Error ", e1);
					}

					/*Modified by Pushpalatha for TENJINCG-1043 starts*/
				} 
				/* Uncommented by Padmavathi for license starts */
				/*Added by Prem for TENJINCG-837 */
				else if(e instanceof MaxActiveUsersException) {
					SessionUtils.setScreenState("error", "auth.MaxUsers", request);
					response.sendRedirect("AuthenticationServlet?t=auth_error");
				}
				else if(e instanceof LicenseExpiredException) {
					request.setAttribute("systemId",licenseHandler.generateSystemId());
					request.setAttribute("status", "error");
					request.setAttribute("message", e.getMessage());
					request.getRequestDispatcher("license_installation.jsp").forward(request, response);
				}
				else if(e instanceof LicenseInactive) {
					
					request.setAttribute("systemId",licenseHandler.generateSystemId());
					request.setAttribute("status", "error");
					request.setAttribute("message", e.getMessage());
					request.getRequestDispatcher("license_installation.jsp").forward(request, response);
				}else if(e instanceof TenjinConfigurationException) {
					
					request.setAttribute("status", "error");
					request.setAttribute("systemId",licenseHandler.generateSystemId());
					request.setAttribute("message", e.getMessage());
					request.getRequestDispatcher("license_installation.jsp").forward(request, response);
				}

				else {
					SessionUtils.setScreenState("error", "generic.error", request);
					response.sendRedirect("AuthenticationServlet?t=auth_error");
				}
			}catch(Exception e1){
				request.setAttribute("status", "error");
				request.setAttribute("message", "An internal error occurred. Please contact Tenjin Support.");
				request.getRequestDispatcher("license_installation.jsp").forward(request, response);
			}
		}
		/* Uncommented by Padmavathi for license ends */

	}

}
