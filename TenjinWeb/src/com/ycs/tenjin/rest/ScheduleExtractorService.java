
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ScheduleExtractorService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 13-07-2017		    Gangadhar Badagi		newly added
 * 20-07-2017		    padmavathi 		        For Defect 299 
 * 17-Aug-2017          Gangadhar Badagi        T25IT-93
 */


package com.ycs.tenjin.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.constants.BrowserType;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.SchedulerHelper;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.run.ExtractorGateway;
import com.ycs.tenjin.scheduler.SchMapping;
import com.ycs.tenjin.scheduler.Scheduler;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

@Path("/extractor")
public class ScheduleExtractorService {
	
	private static final Logger logger = LoggerFactory.getLogger(ScheduleExtractorService.class);
	@Context
	ContainerRequestContext requestContext;

	@Context
	SecurityContext securityContext;
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/schedules")
	public Response getSchedulesForExtract()
	{
			ArrayList<Scheduler> schedulerHelper;
			try {
				String action="Extract";
				/*Changed by Gangadhar Badagi for TENJINCG-308 starts*/
				schedulerHelper = new SchedulerHelper().hydrateAllSchedules(action,0);
				/*Changed by Gangadhar Badagi for TENJINCG-308 ends*/
				logger.info("{} schedules found in all", schedulerHelper.size());
				Response response = RestUtils.buildResponse(Status.OK, "", schedulerHelper);
				return response;
			} catch (DatabaseException e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/schedules/{id}")
	public Response getSchedulesForExtract(@PathParam("id") int id)
	{
			Scheduler scheduler;
			try {
				scheduler = new SchedulerHelper().hydrateSchedule(String.valueOf(id));
				if(scheduler==null || scheduler.getAction()==null || !scheduler.getAction().equals("Extract")){
					return RestUtils.buildResponse(Status.NOT_FOUND, "Scheduler id '"+id+"' does not exist ", null);
				}
				Response response = RestUtils.buildResponse(Status.OK, "", scheduler);
				return response;
			} catch (DatabaseException e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			}
	}
	
	@Path("/schedules")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response scheduleTask(String jsonString)
	{
		Scheduler scheduler=new Gson().fromJson(jsonString, Scheduler.class);
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		TenjinSession tjnSession=new TenjinSession();
		tjnSession.setUser(user);
		
		List<String> listOne =new ArrayList<String>() ;
		List<String> listTwo =new ArrayList<String>();
		Map<String,String> funcMap=null;
		scheduler.setCreated_by(user.getId());
		Calendar calendar1 = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String currentDate = dateFormat.format(calendar1.getTime());
		scheduler.setCreated_on(currentDate);
	
		int tsetId=0;
				try{
						new ScheduleValidator().validateMandatoryFields(scheduler);
						
						SchedulerHelper helper = new SchedulerHelper();
						scheduler.setProjectId(0);
						int appId=0;
						int checkSch_task=0;
						
						RegisteredClient client=new ClientHelper().hydrateClient(scheduler.getReg_client());
						if(client==null)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "Client '"+scheduler.getReg_client()+"' does not exist", null);
						if(Utilities.trim(scheduler.getAutLoginTYpe()).equalsIgnoreCase("") && scheduler.getAutLoginTYpe()!="Maker" && scheduler.getAutLoginTYpe()!="Checker")
							return RestUtils.buildResponse(Status.BAD_REQUEST, "autLoginType ["+scheduler.getAutLoginTYpe()+"] not found. Enter 'Maker' or 'Checker'", null);
						if(Utilities.trim(scheduler.getBrowserType()).equalsIgnoreCase("")){
							scheduler.setBrowserType("APPDEFAULT");
						}
						if(!scheduler.getAction().equalsIgnoreCase("Extract"))
							return RestUtils.buildResponse(Status.BAD_REQUEST, "action ["+scheduler.getAction()+"] not found. Enter 'Extract' ", null);
						scheduler.setAction(scheduler.getAction().substring(0,1).toUpperCase()+scheduler.getAction().substring(1));
						if(!scheduler.getBrowserType().equalsIgnoreCase("APPDEFAULT") && !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.CHROME) && !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.FIREFOX)&& !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.IE))
							return RestUtils.buildResponse(Status.BAD_REQUEST, "browserType ["+scheduler.getBrowserType()+"] not found. Enter 'APPDEFAULT' or '"+BrowserType.CHROME+"' or '"+BrowserType.FIREFOX+"' or '"+BrowserType.IE+"'", null);
						
							
						Aut aut=new AutHelper().hydrateAut(scheduler.getAppName());
						if(aut==null)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "App Name '"+scheduler.getAppName()+"' does not exist", null);
							appId=aut.getId();
							funcMap=new HashMap<String,String>();
						for (String functionCode : scheduler.getFunctions()) {
							listOne.add(functionCode);
							
						}
						for (ModuleBean functionCode : aut.getFunctions()) {
							listTwo.add(functionCode.getModuleCode());
						}
						listOne.removeAll(listTwo);
						if(listOne.size()>0)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "Functions '"+listOne+"'not found", null);
						
						Iterator<String> functionCodes = scheduler.getFunctions().iterator();
						Iterator<String> testDataPaths = scheduler.getTestDataPath().iterator();

						while (functionCodes.hasNext() || testDataPaths.hasNext()) 
							funcMap.put(functionCodes.next(), testDataPaths.next());
						
						checkSch_task=helper.checkSch_Task(scheduler,tsetId);
						
						Date crntDate = null;
						Date restDate = null;
						try {
							crntDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
							        .parse(currentDate);
							restDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
							        .parse(scheduler.getSch_date());
						} catch (ParseException e) {
							return RestUtils.buildResponse(Status.NOT_FOUND, "Please enter sch_date ["+scheduler.getSch_date()+"] in 'dd-MMM-yyyy' format(ex:"+currentDate+") and there should not be any space in between", null);
						}
						
						if(restDate.compareTo(crntDate) < 0)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "sch_date ["+scheduler.getSch_date()+"]should not be previous date", null);
						if(new ScheduleValidator().timeCheck(calendar1.getTime(), scheduler.getSch_time())){
							return RestUtils.buildResponse(Status.BAD_REQUEST, "sch_time ["+scheduler.getSch_time()+"] should not be previous time", null);
						}
						if(checkSch_task==0){
							
							ExtractorGateway gateway=new ExtractorGateway(appId,funcMap,tjnSession.getUser().getId());
							gateway.setClientName(scheduler.getReg_client());
							gateway.setAutLoginType(scheduler.getAutLoginTYpe());
							gateway.performAllValidations();
							logger.info("Initializing Extracter...");
							gateway.createRun();
							scheduler.setRun_id(gateway.getTestRun().getId());
						scheduler.setSchRecur("N");
						scheduler.setStatus("Scheduled");
						scheduler = new SchedulerHelper().persistSchedule(scheduler);
						logger.info("{} creating schedule", scheduler);
						Response response = RestUtils.buildResponse(Status.OK, "Task is scheduled successfully",scheduler.getSchedule_Id());
						return response;
				}
						else{
							return RestUtils.buildResponse(Status.FOUND, "Schedule Already exists",null);
						}
				}
				catch(TenjinServletException e)
				{
					return RestUtils.buildResponse(Status.FORBIDDEN,e.getMessage(), null);
					}
			catch (DatabaseException e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			} 
				
			
   
	}
	
	
	@Path("/schedules/{taskid}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateSchedule(@PathParam("taskid")int sch_id,String jsonString)
	{
		Scheduler scheduler=new Gson().fromJson(jsonString, Scheduler.class);
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		TenjinSession tjnSession=new TenjinSession();
		tjnSession.setUser(user);
		
		List<String> listOne =new ArrayList<String>() ;
		List<String> listTwo =new ArrayList<String>();
		Map<String,String> funcMap=null;
		List<String> functionList=new ArrayList<String>();
		List<String> testDataPathsList=new ArrayList<String>();
		
		scheduler.setCreated_by(user.getId());
		Calendar calendar1 = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String currentDate = dateFormat.format(calendar1.getTime());
		scheduler.setCreated_on(currentDate);
		
		int tsetId=0;
		int appId=0;
		
				try{
					SchedulerHelper helper = new SchedulerHelper();
					Scheduler sch=helper.hydrateSchedule(String.valueOf(sch_id));
					if(sch==null || sch.getAction()==null || !sch.getAction().equals("Extract") )
						return RestUtils.buildResponse(Status.NOT_FOUND, "Scheduler id '"+sch_id+"' does not exist ", null);
					ArrayList<SchMapping> scheduleList = new ArrayList<SchMapping>();
                
                   
                 
					scheduleList =helper.hydrateScheduleMap1(sch);
					if(!Utilities.trim(scheduler.getAction()).equalsIgnoreCase(""))
					{
						return RestUtils.buildResponse(Status.BAD_REQUEST, "Can't update the action", null);
					}
					
					if(scheduler.getAction()==null){
						scheduler.setAction(sch.getAction());
					}
					if(Utilities.trim(scheduler.getBrowserType()).equalsIgnoreCase("")){
						scheduler.setBrowserType(sch.getBrowserType());
					}
					
					funcMap=new HashMap<String,String>();
					if(scheduleList != null && scheduleList.size() > 0) {
						if(scheduler.getFunctions()==null || scheduler.getFunctions().isEmpty()){
						for (SchMapping schMapping : scheduleList) {
							functionList.add(schMapping.getFunc_Code());
						}
						scheduler.setFunctions(functionList);
						}
						if(scheduler.getTestDataPath()==null || scheduler.getTestDataPath().isEmpty()){
							for (SchMapping schMapping : scheduleList) {
								testDataPathsList.add(schMapping.getExt_file_path());
							}
							scheduler.setTestDataPath(testDataPathsList);
							}
						if(scheduler.getAppName()==null || scheduler.getAppName()==""){
						 appId = Integer.parseInt(scheduleList.get(0).getApp_id());
						Aut aut= new AutHelper().hydrateAut(appId);
						scheduler.setAppName(aut.getName());
						
						}
					}
					
					scheduler=new ScheduleValidator().fieldsValidation(scheduler, sch_id);
					scheduler.setProjectId(0);
					int checkSch_task=0;
					RegisteredClient client=new ClientHelper().hydrateClient(scheduler.getReg_client());
					if(client==null)
						return RestUtils.buildResponse(Status.BAD_REQUEST, "Client '"+scheduler.getReg_client()+"' does not exist", null);
					if(!scheduler.getAction().equalsIgnoreCase("Extract") )
						return RestUtils.buildResponse(Status.BAD_REQUEST, "action ["+scheduler.getAction()+"] not found. Enter 'Extract' ", null);
					scheduler.setAction(scheduler.getAction().substring(0,1).toUpperCase()+scheduler.getAction().substring(1));
					if(!scheduler.getBrowserType().equalsIgnoreCase("APPDEFAULT") && !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.CHROME) && !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.FIREFOX)&& !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.IE))
						return RestUtils.buildResponse(Status.BAD_REQUEST, "browserType ["+scheduler.getBrowserType()+"] not found. Enter 'APPDEFAULT' or '"+BrowserType.CHROME+"' or '"+BrowserType.FIREFOX+"' or '"+BrowserType.IE+"'", null);
				   if(!scheduler.getAutLoginTYpe().equals("Maker") && !scheduler.getAutLoginTYpe().equals("Checker"))
							return RestUtils.buildResponse(Status.BAD_REQUEST, "autLoginType ["+scheduler.getAutLoginTYpe()+"] not found. Enter 'Maker' or 'Checker'", null);
					Aut aut=new AutHelper().hydrateAut(scheduler.getAppName());
					if(aut==null)
						return RestUtils.buildResponse(Status.BAD_REQUEST, "App Name '"+scheduler.getAppName()+"' does not exist", null);
					   appId=aut.getId();
					for (String functionCode : scheduler.getFunctions()) {
						listOne.add(functionCode);
					}
					for (ModuleBean functionCode : aut.getFunctions()) {
						listTwo.add(functionCode.getModuleCode());
					}
					listOne.removeAll(listTwo);
					if(listOne.size()>0)
						return RestUtils.buildResponse(Status.BAD_REQUEST, "Functions '"+listOne+"'not found", null);

					Iterator<String> functionCodes = scheduler.getFunctions().iterator();
					Iterator<String> testDataPaths = scheduler.getTestDataPath().iterator();

					while (functionCodes.hasNext() || testDataPaths.hasNext())
						funcMap.put(functionCodes.next(), testDataPaths.next());
					
					checkSch_task=helper.checkSch_Task(scheduler,tsetId);
					
						
						if(scheduler.getStatus()=="" || !Utilities.trim(scheduler.getStatus()).equalsIgnoreCase("")){
							return RestUtils.buildResponse(Status.NOT_FOUND, "You can't update schedule status manually ", null);
						}
						Date crntDate = null;
						Date restDate = null;
						try {
							crntDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
							        .parse(currentDate);
							restDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
							        .parse(scheduler.getSch_date());
						} catch (ParseException e) {
							return RestUtils.buildResponse(Status.NOT_FOUND, "Please enter sch_date ["+scheduler.getSch_date()+"] in 'dd-MMM-yyyy' format(ex:"+currentDate+") and there should not be any space in between", null);
						}
						
						if(restDate.compareTo(crntDate) < 0)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "sch_date ["+scheduler.getSch_date()+"]should not be previous date", null);
						if(new ScheduleValidator().timeCheck(calendar1.getTime(), scheduler.getSch_time())){
							return RestUtils.buildResponse(Status.BAD_REQUEST, "sch_time ["+scheduler.getSch_time()+"] should not be previous time", null);
						}
						if(checkSch_task==0){
							ExtractorGateway gateway=new ExtractorGateway(appId,funcMap,tjnSession.getUser().getId());
							gateway.setClientName(scheduler.getReg_client());
							gateway.setAutLoginType(scheduler.getAutLoginTYpe());
							gateway.performAllValidations();
							logger.info("Initializing Extracter...");
							gateway.createRun();
							scheduler.setRun_id(gateway.getTestRun().getId());
							
							scheduler.setSchRecur("N");
							scheduler.setStatus("Scheduled");
						    scheduler.setSchedule_Id(sch_id);
						 new SchedulerHelper().updateSchedule(scheduler);
						 
						 /*Added by Gangadhar BAdagi for T25IT-93 starts*/
						 Scheduler updatedScheduler=helper.hydrateSchedule(String.valueOf(scheduler.getSchedule_Id()));
						 /*Added by Gangadhar BAdagi for T25IT-93 ends*/
						return RestUtils.buildResponse(Status.OK, "Schedule Updated Successfully",updatedScheduler);
						}
				else{
					return RestUtils.buildResponse(Status.FOUND, "Schedule Already exists",null);
				}
				}
				
				catch(TenjinServletException e)
				{
					return RestUtils.buildResponse(Status.FORBIDDEN,e.getMessage(), null);
					}
			catch (DatabaseException   e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			}
				
	}
	@Path("/schedules/{taskid}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response cancelTask(@PathParam("taskid")int taskId)
	{
		Scheduler sch;
		try {
			sch = new SchedulerHelper().hydrateSchedule(String.valueOf(taskId));
			if(sch==null || sch.getAction()==null || !sch.getAction().equals("Extract"))
				return RestUtils.buildResponse(Status.NOT_FOUND, "Scheduler id '"+taskId+"' does not exist ", null);
			if(!sch.getStatus().equalsIgnoreCase("Scheduled"))
				return RestUtils.buildResponse(Status.BAD_REQUEST, "We can't cancel the task because status is '"+sch.getStatus()+"'", null);
			sch=new SchedulerHelper().hydrateSchedule(String.valueOf(taskId));
			sch.setStatus("Cancelled");
			new SchedulerHelper().updateSchedule(sch);
			return RestUtils.buildResponse(Status.OK, "Schedule Cancelled Successfully",null);
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
	}
	
	
}
