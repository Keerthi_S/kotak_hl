/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestSetService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 10-Dec-2016         Gangadhar Badagi         Newly Added For TJN_243_02 
* 10-Dec-2016         Abhilsh          Newly Added For TJN_243_02
* 13-06-2017			Sriram					TENJINCG-189
* 20-Jun-2017		Sriram Sridharan		Changes for TENJINCG-235
* 17-Aug-2017			Gangadhar Badagi		T25IT-93
* 18-Aug-2017		  Sriram Sridharan		   Entire file re-designed for T25IT-197,T25IT-198,T25IT-201,T25IT-208,T25IT-209,T25IT-211
  19-Aug-2017         Padmavathi                  T25IT-204
  15-Nov-2017         Padmavathi                Added @PathParam in method signature    
  15-Nov-2017			Gangadhar Badagi		For rerun a test	
 * 16-Nov-2017			Manish					TENJINCG-452
 * 16-Nov-2017			Manish					TENJINCG-463
 * 16-Nov-2017			Manish					TENJINCG-450
 * 16-Nov-2017			Manish					TENJINCG-453
 * 23-Nov-2017          Leelaprasad             TENJINCG-520
 * 08-Dec-2017          Padmavathi              Changed Status 
 * 23-Apr-2018          Padmavathi              for TENJINCG-636 
 * 19-06-2018           Padmavathi              T251IT-83
 * 19-02-2019			Preeti					TENJINCG-969
 * 29-03-2019			Preeti					TENJINCG-1003
  
*/


package com.ycs.tenjin.rest;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.CoreDefectHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ResultsHelper;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.db.TestSetHelper;
import com.ycs.tenjin.defect.Defect;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.handler.TestSetHandler;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.User;


@Path("/domains")
public class TestSetService {
	
	private static final Logger logger = LoggerFactory.getLogger(TestSetService.class);
	TestSetHandler testSetHandler =new TestSetHandler();
	
	@Context
	ContainerRequestContext requestContext;
	
	@Context
	SecurityContext securityContext;
	
	private static final String INTERNAL_SERVER_ERROR_MSG = "An internal error occurred. Please contact Tenjin Support.";
	
	@Path("/{dName}/projects/{pName}/testsets")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	  public Response createNewTestSet(TestSet testSet,@PathParam("dName") String domainName,@PathParam("pName") String projectName) {
		if(testSet == null) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, "Request data is empty. Please review your request and try again.", null);
		}
		 logger.info("Recieved request to create test set");
		Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		User user =(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		testSet.setCreatedBy(user.getId()); 
		testSet.setProject(project.getId());
		try {
			testSetHandler.persistTestSet(testSet, project.getId());
			logger.info("testset created successfully.");
			return RestUtils.buildResponse(Status.CREATED, "testset.create.success", null,testSet);
		} catch (DatabaseException e) {
			logger.error("ERROR creating test set", e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,e.getMessage(),null, null);
		} catch (DuplicateRecordException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),e.getTargetFields(),null);
		} catch (RequestValidationException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),e.getTargetFields(),null);
		}
	}
	
	@Path("/{dName}/projects/{pName}/testsets/bulk")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createNewTestSets(@PathParam("dName") String domainName,@PathParam("pName") String projectName,List<TestSet> testSets) {
		if(testSets == null || testSets.size() < 1) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, "Request data is empty. Please review your request and try again.", null);
		}
		 logger.info("Recieved request to create bulk test set");
		User user =(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		
		List<Object> messages = new ArrayList<Object> ();
		for(TestSet testSet : testSets) {
			testSet.setCreatedBy(user.getId());
			testSet.setProject(project.getId());
			
			try {
				this.testSetHandler.persistTestSet(testSet, project.getId());
				messages.add("TestSet '"+testSet.getName()+"' created successfully!");
				logger.info("TestSet '"+testSet.getName()+"' created successfully!");
				messages.add(testSet);
				
			}catch (DatabaseException e) {
				messages.add("TestSet '"+testSet.getName()+"' -- "+e.getMessage());
			}  catch (DuplicateRecordException e) {
				messages.add("TestSet '"+testSet.getName()+"' -- "+e.getMessage());
			}
			catch (RequestValidationException e) {
				messages.add("TestSet '"+testSet.getName()+"' -- "+e.getMessage()+Arrays.toString(e.getTargetFields()));
			} 
			catch (RecordNotFoundException e) {
				messages.add("TestSet '"+testSet.getName()+"' -- "+e.getMessage());
			}
		}
		
		return RestUtils.buildResponse(Status.OK, "Bulk operation is complete. Please review the response body for status of Individual test sets.", messages);
	}
	
	@Path("/{dName}/projects/{pName}/testsets/{testSetRecordId}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTestSet(TestSet testSet,@PathParam("dName") String domainName,@PathParam("pName") String projectName, @PathParam("testSetRecordId") int testSetRecordId) {
		if(testSet == null) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, "Request data is empty. Please review your request and try again.", null);
		}
		 logger.info("Recieved request to update test set");
		Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		testSet.setId(testSetRecordId);
		testSet.setProject(project.getId());
		/*Added by Preeti for TENJINCG-969 starts*/
		AuditRecord audit = new AuditRecord();
		audit.setEntityRecordId(testSet.getId());
		audit.setLastUpdatedBy(user.getId());
		audit.setEntityType("testset");
		testSet.setAuditRecord(audit);
		/*Added by Preeti for TENJINCG-969 ends*/
		try {
			this.testSetHandler.updateTestSet(testSet);
			logger.info("testset updated successfully.");
			return RestUtils.buildResponse(Status.OK, "testSet.update.success",null,this.testSetHandler.hydrateTestSet(project.getId(),testSetRecordId));
		}  catch (DatabaseException e) {
			logger.error("ERROR updating test set", e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR_MSG, null);
		} catch (RequestValidationException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),e.getTargetFields(),null);
		}catch (RecordNotFoundException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(),null,null);
		}
	}
	
	@Path("/{dName}/projects/{pName}/testsets/bulk")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTestSets(@PathParam("dName") String domainName,@PathParam("pName") String projectName,List<TestSet> testSets) {
		if(testSets == null || testSets.size() < 1) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, "Request data is empty. Please review your request and try again.", null);
		}
		logger.info("Recieved request to update bulk test set");
		Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		List<Object> messages = new ArrayList<Object> ();
		for(TestSet testSet : testSets) {
			int tsRecId = testSet.getId();
			if(tsRecId == 0) {
				messages.add("Test Set 0 could not be updated: Test Set ID [id] is not passed.");
				continue;
			}
			try {
				testSet.setProject(project.getId());
				/*Added by Preeti for TENJINCG-969 starts*/
				AuditRecord audit = new AuditRecord();
				audit.setEntityRecordId(testSet.getId());
				audit.setLastUpdatedBy(user.getId());
				audit.setEntityType("testset");
				testSet.setAuditRecord(audit);
				/*Added by Preeti for TENJINCG-969 ends*/
				this.testSetHandler.updateTestSet(testSet);
				logger.info("TestSet '"+tsRecId+"' Updated successfully!");
				messages.add("TestSet '"+tsRecId+"' Updated successfully!");
				messages.add(this.testSetHandler.hydrateTestSet(project.getId(),tsRecId));
			}catch (DatabaseException e) {
				messages.add("Test Set [" + tsRecId + "] could not be updated: " + INTERNAL_SERVER_ERROR_MSG);
			} catch (DuplicateRecordException e) {
				messages.add("TestSet '"+tsRecId+"' -- "+e.getMessage()+Arrays.toString(e.getTargetFields()));
			}
			catch (RequestValidationException e) {
				messages.add("TestSet '"+tsRecId+"' -- "+e.getMessage()+Arrays.toString(e.getTargetFields()));
			} 
			catch (RecordNotFoundException e) {
				messages.add("TestSet '"+tsRecId+"' -- "+e.getMessage());
			}
		}
		
		return RestUtils.buildResponse(Status.OK, "Bulk operation is complete. Please review the response body for status of Individual test sets.", messages);
	}
	
	@GET
	@Path("/{dName}/projects/{pName}/testsets")
	@Produces(MediaType.APPLICATION_JSON)
	  public Response getAllTestSetsForProject(@PathParam("dName") String domainName,@PathParam("pName") String projectName) {
		Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		 logger.info("Recieved request to hydrate all test sets");
		try {
			 new TestSetHandler().getAllTestSets(project.getId());
			 logger.info("All testsets fetched successfully.");
			return RestUtils.buildResponse(Status.OK, "test.sets.fetched", null, new TestSetHandler().getAllTestSets(project.getId()));
		} catch (DatabaseException e) {
			logger.error("ERROR fetching all test sets", e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,e.getMessage(),null, null);
		}
	}
	
	@Path("/{dName}/projects/{pName}/testsets/{testSetRecordId}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeTestSet(@PathParam("dName") String domainName,@PathParam("pName") String projectName,@PathParam("testSetRecordId") int testSetRecordId) {
		Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		 logger.info("Recieved request to delete test set");
		try {
			/*Modified by Preeti for TENJINCG-1003 starts*/
			/*this.testSetHandler.deleteTestSet(new String[] {String.valueOf(testSetRecordId)}, project.getId());*/
			this.testSetHandler.deleteTestSet(new String[] {String.valueOf(testSetRecordId)}, project.getId(), user.getId());
			/*Modified by Preeti for TENJINCG-1003 ends*/
			 logger.info("testset deleted successfully.");
			return RestUtils.buildResponse(Status.OK, "testSet.delete.success",null,null);
		} catch (DatabaseException e) {
			logger.error("ERROR deleting test set", e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR_MSG, null);
		} catch (RequestValidationException e) {
			logger.error(e.getMessage() );
			return RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(),null,null);
		} catch (ResourceConflictException e) {
			logger.error(e.getMessage() );
			return RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(),null,null);
		}catch (RecordNotFoundException e) {
			logger.error(e.getMessage());
			return RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(),null,null);
		}
	}
	
	@GET
	@Path("/{dName}/projects/{pName}/testsets/{testSetRecordId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTestSet(@PathParam("dName") String domainName,@PathParam("pName") String projectName,@PathParam("testSetRecordId") int testSetRecordId) {
		 logger.info("Recieved request to hydrate test set");
		Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		try {
			return RestUtils.buildResponse(Status.OK, "test.set.fetched",null,new TestSetHandler().hydrateTestSet(project.getId(),testSetRecordId));
		}catch (RecordNotFoundException e) {
			logger.error("testset not found"+testSetRecordId,e );
			return RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(),null,null);
		}catch (DatabaseException e) {
			logger.error("ERROR fetching test set", e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(),null, null);
		}
	}
	
	@GET
	@Path("/{dName}/projects/{pName}/testsets/{testSetRecordId}/testcases")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTestCasesInTestSet(@PathParam("dName") String domainName,@PathParam("pName") String projectName, @PathParam("testSetRecordId") int testSetRecordId) {
		Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		logger.info("Recieved request to hydrate testcases  for test set");
		try {
			TestSet testSet =new TestSetHandler().hydrateTestSet(project.getId(),testSetRecordId);
			 logger.info(" testcases for testset fetched successfully.");
			if(testSet.getTests().size()==1){
				return RestUtils.buildResponse(Status.OK,"testcase.mapped.to.testset", testSet.getTests().size(), testSet.getTests());
			}else{
				return RestUtils.buildResponse(Status.OK, "testcases.mapped.to.testset", testSet.getTests().size(),testSet.getTests());
			}
		}catch (RecordNotFoundException e) {
			logger.error("testset not found"+testSetRecordId,e );
			return RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(),null,null);
		}catch (DatabaseException e) {
			logger.error("ERROR fetching test set", e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(),null, null);
		}
	}
	
	@GET
	@Path("/{dName}/projects/{pName}/testsets/{testSetRecordId}/runs")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRunsForTestSet(@PathParam("dName") String domainName,@PathParam("pName") String projectName,@PathParam("testSetRecordId") int testSetRecordId) {
		Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		logger.info("Recieved request to hydrate run for test set");
		try {
			List<TestRun> runs = new TestSetHandler().hydrateTestSetExecutionHistory(project.getId(), testSetRecordId);
			 logger.info("Runs for testsets fetched successfully.");
			return RestUtils.buildResponse(Status.OK, runs.size() + " runs found for this Test Set", runs);
		}catch (RecordNotFoundException e) {
			logger.error("testset not found"+testSetRecordId,e );
			return RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(),null,null);
		} catch (DatabaseException e) {
			logger.error("ERROR fetchhing runs for test set.", e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR_MSG, null);
		}
	}
	
	@POST
	@Path("/{dName}/projects/{pName}/testsets/{testSetRecordId}/testcases")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addTestCasesToTestSet(@PathParam("dName") String domainName,@PathParam("pName") String projectName,@PathParam("testSetRecordId") int testSetRecordId, List<TestCase> testCases) {
		String tcRecIdsToBeMapped = "";
		TestSetHandler testsetHandler=new TestSetHandler();
		Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		List<String> messages = new ArrayList<String> ();
		
		messages.add("Test Case mapping completed successfully");
		Map<Integer, TestCase> mappedTestCases = new HashMap<Integer,TestCase>();
		try {
			TestSet ts=testsetHandler.hydrateTestSet(project.getId(),testSetRecordId);
			ArrayList<TestCase> mappedTC=ts.getTests();
			for(TestCase tc:mappedTC){
				tcRecIdsToBeMapped=tcRecIdsToBeMapped+tc.getTcRecId()+",";
			}
			
			for(TestCase tc:testCases){
				TestCase testcase=testsetHandler.hydrateTestCase(tc.getTcRecId(),project.getId());
				if(testcase==null){
					messages.add("Could not map Test Case with record Id '" + tc.getTcRecId() + "' because it does not exist in this project");
				}else{
				tcRecIdsToBeMapped=tcRecIdsToBeMapped+tc.getTcRecId()+",";
				}
			}
			testsetHandler.persistTestSetMap(testSetRecordId, tcRecIdsToBeMapped, project.getId(), true);
			
			mappedTC=ts.getTests();
			for(TestCase tc:mappedTC){
				mappedTestCases.put(tc.getTcRecId(),tc);
			}
			
			return RestUtils.buildResponse(Status.OK, messages.toString(), new TestSetHelper().hydrateTestSet(testSetRecordId, project.getId()));
		}catch(RecordNotFoundException e){
			logger.error("testset not found"+testSetRecordId,e );
			return RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(),null,null);
		}
		
		catch (DatabaseException e) {
			logger.error("ERROR mapping test cases to set.", e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR_MSG, null);
		} 
			
	}
	
    @POST
    @Path("/{dName}/projects/{pName}/testsets/{testSetIdOrName}/execute")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response execute(@PathParam("dName") String domainName, @PathParam("pName") String projectName, @PathParam("testSetIdOrName") String testSetIdOrName, ExecutorInput input) {
   	 
   	 User user = (User) requestContext.getProperty(RestContextProperties.TJN_USER);
   	 Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
   	 
   	 logger.info("Recieved request to execute test set");
   	 input.setDomainName(domainName);
   	 input.setProjectName(projectName);
   	 try {
   		 int tsRecId = Integer.parseInt(testSetIdOrName);
   		 input.setTestSetRecordId(tsRecId);
   	 }catch(NumberFormatException e) {
   		 logger.info("Test set name is passed --> [{}]", testSetIdOrName);
   		 input.setTestSetName(testSetIdOrName);
   	 }
   	  RestExecutionHandler handler = new RestExecutionHandler(input, user, project);
   	 try {
			handler.validateTestSetExecutionInput();
		} catch (RestRequestException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		} catch (TenjinServletException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		} /*Added by padmavathi for T251IT-83 starts*/
   	 	catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
   	/*Added by padmavathi for T251IT-83 ends*/
   	 
   	 try {
			int runId = handler.startRun();
			return RestUtils.buildResponse(Status.OK, "Execution initiated successfully!", runId);
		} catch (TenjinServletException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
    }
    
    @POST
	@Path("/{dName}/projects/{pName}/runs/{runId}/rerun")
	@Produces(MediaType.APPLICATION_JSON)
	public Response reRun(@PathParam("dName") String domainName, @PathParam("pName") String projectName,@PathParam("runId") int runId,ExecutorInput executorInput){
		
		 User user = (User) requestContext.getProperty(RestContextProperties.TJN_USER);
	   	 Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		try {
			TestRun testRun=new RunHelper().hydrateRunForProject(runId, project.getId());
			if(testRun==null){
				return RestUtils.buildResponse(Status.BAD_REQUEST, "Run Id"+runId+" not found", null);
			}
			if(testRun.getChildRunIds()!=null){
				return RestUtils.buildResponse(Status.BAD_REQUEST, "Can not re-run a test with [" + runId + "] as it has child run id/s.", null);
			}
			if(testRun.getStatus().equalsIgnoreCase("Pass")){
				return RestUtils.buildResponse(Status.BAD_REQUEST, "Can not re-run a passed test.", null);
			}
			executorInput.setDomainName(domainName);
			executorInput.setProjectName(projectName);
			TestSet testSet=new ResultsHelper().hydrateTestSetForRerun(testRun.getProjectId(), testRun.getTestSetRecordId(), testRun.getParentRunId(), "");
			executorInput.setTestSetName(testSet.getName());
			executorInput.setTestSetRecordId(testSet.getId());
			RestExecutionHandler handler = new RestExecutionHandler(executorInput, user, project);
			handler.validateTestSetExecutionInput();
			int reRunId = handler.startRun();
			return RestUtils.buildResponse(Status.OK, "Execution initiated successfully!", reRunId);
		} catch (RestRequestException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		} catch (TenjinServletException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,e.getMessage(), null);
		}
	}
    
    
    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{dName}/projects/{pName}/testsets/{testSetRecordId}/defects")
	public Response getDefectsForTestSet(@PathParam("dName") String domainName,@PathParam("pName") String projectName,@PathParam("testSetRecordId") int testSetRecordId) {
		logger.debug("Request to get defects for test set");
		logger.info("Getting  defects for Test Set");
		
		try {
			List<Defect> setDefects = new ArrayList<Defect>();
			
			setDefects = new CoreDefectHelper().hydrateDefectsForTestSet(testSetRecordId);
			
			if(setDefects != null && setDefects.size() > 0) {
				return RestUtils.buildResponse(Status.OK, setDefects.size() + " defects found for this test set", setDefects);
			}else {
			
				return RestUtils.buildResponse(Status.OK, "No defects were found for this test set",null);
			}
		} catch(DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Support",null);
		}
	}
    
}
