/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectTestDataPathResponse.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 29-Nov-2017		       Sriram		   		Newly added for TENJINCG-539
*/


package com.ycs.tenjin.rest;

import com.ycs.tenjin.project.ProjectTestDataPath;

public class ProjectTestDataPathResponse extends ProjectTestDataPath {
	private String status;
	private String message;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public static ProjectTestDataPathResponse create(ProjectTestDataPath path, String status, String message) {
		ProjectTestDataPathResponse p = new ProjectTestDataPathResponse();
		
		p.setAppId(path.getAppId());
		p.setAppName(path.getAppName());
		p.setFuncCode(path.getFuncCode());
		p.setGroupName(path.getGroupName());
		p.setTestDataPath(path.getTestDataPath());
		p.setProjectId(path.getProjectId());
		p.setStatus(status);
		p.setMessage(message);
		return p;
	}
}
