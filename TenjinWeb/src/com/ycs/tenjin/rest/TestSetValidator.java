
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestSetValidator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 10-Dec-2016         Gangadhar Badagi         Newly Added For TJN_243_02 
* 18-Aug-2017		  Sriram Sridharan		   Entire file re-designed for T25IT-197,T25IT-198,T25IT-201,T25IT-208,T25IT-209,T25IT-211 
*/


package com.ycs.tenjin.rest;


import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.TestSetHelper;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Utilities;

public class TestSetValidator {
	
	private static final Logger logger = LoggerFactory.getLogger(TestSetValidator.class);
	private static final List<String> PRIORITIES = Arrays.asList("Low", "Medium", "High");
	private static final List<String> TEST_SET_TYPES = Arrays.asList("Functional");
	private static final List<String> MODES = Arrays.asList("GUI", "API");
		
	
	
	public void validateAndCreateTestSet(TestSet testSet, Project project) throws TenjinServletException, DatabaseException  {
		logger.info("Validating Test Set...");
		this.validateTestSet(project, testSet, true);
		logger.info("Done");
		
		testSet.setProject(project.getId());
		testSet.setRecordType("TS");
		logger.info("Creating test set");
		TestSetHelper helper = new TestSetHelper();
		helper.persistTestSet(testSet, project.getId());
		logger.info("Done");
	}
	
	public TestSet validateAndUpdateTestSet(int testSetRecordId, TestSet newTestSet, Project project) throws TenjinServletException, DatabaseException{
		TestSetHelper helper = new TestSetHelper();
		
		TestSet mergedTestSet = helper.hydrateTestSet(testSetRecordId, project.getId());
		if(mergedTestSet == null) {
			logger.error("ERROR - Test Set {} does not exist for project {}", testSetRecordId, project.getName() + " (" + project.getDomain() + ")");
			throw new TenjinServletException("Test Set [" + testSetRecordId + "] does not exist for this project."); 
		}
		String oMode = mergedTestSet.getMode();
		String oName = mergedTestSet.getName();
		mergedTestSet.merge(newTestSet);
		
		if(!oMode.equalsIgnoreCase(mergedTestSet.getMode())) {
			logger.error("ERROR - Invalid field update of Mode");
			throw new TenjinServletException("The value of [mode] cannot be changed after a Test Set is created.");
		}
		boolean duplicateCheckRequired = false;
		if(!oName.equalsIgnoreCase(mergedTestSet.getName())) {
			duplicateCheckRequired = true;
		}
		
		logger.info("Validating test set for update...");
		this.validateTestSet(project, mergedTestSet, duplicateCheckRequired);
		logger.info("Done");
		
		logger.info("Updating test set...");
		helper.savetestsetdetails(testSetRecordId, mergedTestSet.getName(), mergedTestSet.getDescription(), mergedTestSet.getType(), mergedTestSet.getPriority(), mergedTestSet.getMode());
		logger.info("Done");
	
		return helper.hydrateTestSet(testSetRecordId, project.getId());
		
		
	}
	
	private void validateTestSet(Project project, TestSet testSet, boolean duplicateCheckRequired) throws TenjinServletException, DatabaseException {
		if(testSet == null) {
			throw new TenjinServletException("Request data is empty. Please review your request and try again.");
		}
		
		if(Utilities.trim(testSet.getName()).length() < 1) {
			throw new TenjinServletException("Test Set Name [name] is mandatory.");
		}
		
		this.validateMode(testSet.getMode());
		this.validatePriority(testSet.getPriority());
		this.validateType(testSet.getType());
		
		if(duplicateCheckRequired) {
			TestSetHelper helper= new TestSetHelper();
			if(helper.hydrateTestSet(testSet.getName(), project.getId()) != null) {
				throw new TenjinServletException("Test Set [" + testSet.getName() + "] already exists for this project.");
			}
		}
	}
	
	private void validateMode(String mode) throws TenjinServletException {
		if(!MODES.contains(mode)) {
			throw new TenjinServletException("Invalid value for [mode]. Valid values are " + MODES.toString());
		}
	}
	
	private void validateType(String type) throws TenjinServletException {
		if(!TEST_SET_TYPES.contains(type)) {
			throw new TenjinServletException("Invalid value for [type]. Valid values are " + TEST_SET_TYPES.toString());
		}
	}
	
	private void validatePriority(String priority) throws TenjinServletException {
		if(!PRIORITIES.contains(priority)) {
			throw new TenjinServletException("Invalid value for [priority]. Valid values are " + PRIORITIES.toString());
		}
	}
}
