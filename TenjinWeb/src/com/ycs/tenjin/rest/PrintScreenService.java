/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  PrintScreenService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.R

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 26-09-2018           Leelaprasad             new file added for TENJINCG-739
 */


package com.ycs.tenjin.rest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.PrintScreenObject;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.util.Utilities;

@PermitAll
@Path("/printscreen")
public class PrintScreenService {
	private static final Logger logger = LoggerFactory.getLogger(PrintScreenService.class);
	@Context
	ContainerRequestContext requestContext;

	@Context
	SecurityContext securityContext;

	@GET
	@PermitAll
	@Path("pollstatus/{clientIp}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPrintScreenPollStatus(@PathParam("clientIp") String clientIp){
		try {
			PrintScreenObject prntScreenObject = (PrintScreenObject)CacheUtils.getObjectFromRunCache("screenshot"+clientIp);
			Map<String, Object> map = new HashMap<String, Object>();
			Response response = null;
			
				map.put("pollstatus", prntScreenObject.isScreenshotInProgress());
				response = RestUtils.buildResponse(Status.OK, "", map);
			
			return response;
		} catch (Exception e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
	}
	
	@POST
	@PermitAll
	@Path("scrnshot")
	public Response getScreenshotAsFile(String data){
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			JSONObject jsonObject = new JSONObject(data);
			String clientIp = (String)jsonObject.get("clientIp");
			String data1 = (String)jsonObject.get("data");
			/*File scrnshot = File.createTempFile("Screenshot" + Utilities.getRawTimeStamp(),".jpg");*/
			File scrnshot = null;
			String directories = System.getProperty("java.io.tmpdir")+File.separator+"TenjinScreen"+File.separator+"Screenshot" + Utilities.getRawTimeStamp()+".jpg";
			scrnshot = new File(directories);
			if(scrnshot.getParentFile().mkdirs()) {
			if(scrnshot.createNewFile()) { 
			 try {
			        // Converting a Base64 String into Image byte array
				 FileOutputStream imageOutFile = new FileOutputStream(scrnshot);
			        byte[] imageByteArray = Base64.decodeBase64(data1);
			        imageOutFile.write(imageByteArray);
			        imageOutFile.close();
			    } catch (FileNotFoundException e) {
			        System.out.println("Image not found" + e);
			    } catch (IOException ioe) {
			        System.out.println("Exception while reading the Image " + ioe);
			    }
			 } else {
	                logger.error("File is not created ");
	            }
			}else {
				logger.error("couldnt create a directory ");
			}
			PrintScreenObject prntScreenObject = (PrintScreenObject)CacheUtils.getObjectFromRunCache("screenshot"+clientIp);
			prntScreenObject.setScreenshotInProgress(false);
			prntScreenObject.setScreenshot(scrnshot);
			CacheUtils.putObjectInRunCache(clientIp, prntScreenObject);
			map.put("pollstatus", prntScreenObject.isScreenshotInProgress());
			Response response = RestUtils.buildResponse(Status.OK, "",map);
			return response;
		} catch (Exception e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
	}
	
	@GET
	@PermitAll
	@Path("/stoppoll/{clientIp}")
	public Response stopPolling(@PathParam("clientIp") String clientIp){
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			CacheUtils.removeObjectInRunCache(clientIp);
			Response response = RestUtils.buildResponse(Status.OK, "", map.put("pollstatus", "Client is removed from Cache."));
			return response;
		} catch (Exception e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
	}
	
}
