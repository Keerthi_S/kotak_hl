/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ModuleValidator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 
 * 17-Dec-2016			Gangadhar Badagi		Added to validate function fields
 * 17-08-2017           Padmavathi              T25IT-183
 * 18-08-2017           Padmavathi              T25IT-177
 * 19-08-2017           Padmavathi              T25IT-247
 * 28-08-2017           Gangadhar Badagi        T25IT-333
 * 11-10-2017           Gangadhar Badagi        TENJINCG-356
 * 15-11-2017           Padmavathi              for updating group name
 * 
 */


package com.ycs.tenjin.rest;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Utilities;

public class ModuleValidator {
	
	private static final Logger logger = LoggerFactory.getLogger(ModuleValidator.class);
	
	/**********Added by Gangadhar Badagi to validate mandate fields starts*****/
	public void validateMandateFlds(ModuleBean module) throws TenjinServletException{
		if(Utilities.trim(module.getApplicationName()).equalsIgnoreCase("")){
			logger.error("Application  not found");
			throw new TenjinServletException("Application Name [applicationName] is mandatory");
		}
		if(Utilities.trim(module.getModuleName()).equalsIgnoreCase("")){
			logger.error("Function Name not found");
			throw new TenjinServletException("Function Name[moduleName] is mandatory");
		}
		
		if(Utilities.trim(module.getModuleCode()).equalsIgnoreCase("")){
			logger.error("Function Code not found");
			throw new TenjinServletException("Function Code [moduleCode] is mandatory");
		}
		
		if(Utilities.trim(module.getMenuContainer()).equalsIgnoreCase("")){
			logger.error("Menu container not found");
			throw new TenjinServletException("Menu container [menuContainer] is mandatory");
		}
		//Fix for T25IT-247
		else if(Utilities.trim(module.getMenuContainer()).length() > 200) {
			throw new TenjinServletException("Value for [menuContainer] is too long. Please specify a value within 200 characters.");
		}//Fix for T25IT-247 ends
		
	}
	/**********Added by Gangadhar Badagi to validate mandate fields ends*****/
	/**********Added by Padmavathi for T25IT-183 starts*****/
	public void validateSize(ModuleBean module) throws TenjinServletException{
		if(Utilities.trim(module.getModuleName()).length()>200){
			logger.error("Function Name too large");
			throw new TenjinServletException("Function Name [moduleName] should be less than or equal to 200 words.");
		}
		
		if(Utilities.trim(module.getModuleCode()).length()>100){
			logger.error("Function Code too large");
			throw new TenjinServletException("Function Code [moduleCode]  should be less than or equal to 100 words");
		}
		
		if(Utilities.trim(module.getMenuContainer()).length()>200){
			logger.error("Menu container too large");
			throw new TenjinServletException("Menu container [menuContainer]  should be less than or equal to 200 words");
		}
		
	}
	/**********Added by Padmavathi for T25IT-183 ends*****/
	/**********Added by Padmavathi for T25IT-177 starts*****/
	// changed jsonarray to list by manish
	/*public JSONArray validateMandateFldsBulk(ModuleBean module) {*/
	public List<String> validateMandateFldsBulk(ModuleBean module) {
		/*JSONArray jarray=new JSONArray();*/
		List<String> messages = new ArrayList<String>();
		if(Utilities.trim(module.getModuleName()).equalsIgnoreCase("")){
			logger.error("Function Name not found");
			/*jarray.put("Function Name[moduleName] is mandatory");*/
			messages.add("Function Name[moduleName] is mandatory");
		}
		
		if(Utilities.trim(module.getModuleCode()).equalsIgnoreCase("")){
			logger.error("Function Code not found");
			/*jarray.put("Function Code [moduleCode] is mandatory");*/
			messages.add("Function Code [moduleCode] is mandatory");
		}
		
		if(Utilities.trim(module.getMenuContainer()).equalsIgnoreCase("")){
			logger.error("Menu container not found");
			/*jarray.put("Menu container [menuContainer] is mandatory");*/
			messages.add("Menu container [menuContainer] is mandatory");
		}
		/*return jarray;*/
		return messages;
		
	}
	/**********Added by Padmavathi for T25IT-177 ends
	 * @return 
	 * @throws SQLException 
	 * @throws DatabaseException *****/
	
	/*Added by Gangadhar Badagi for T25IT-333 starts */
	public Module validateFileds(int appId,ModuleBean module) throws DatabaseException, SQLException{
		Module m=new Module();
		ModuleBean mb=new ModulesHelper().hydrateModule(appId, module.getModuleCode());
		if(Utilities.trim(module.getModuleName()).equalsIgnoreCase("")){
			m.setName(mb.getModuleName());
		}
		else{
			m.setName(module.getModuleName());
		}
		if(Utilities.trim(module.getMenuContainer()).equalsIgnoreCase("")){
			m.setMenuContainer(mb.getMenuContainer());
		}
		else{
			m.setMenuContainer(module.getMenuContainer());
		}
		/*Added by Padmavathi for updating group name starts*/
		if(Utilities.trim(module.getGroupName()).equalsIgnoreCase("")){
			m.setGroup(mb.getGroupName());
		}
		else{
			m.setGroup(module.getGroupName());
		}
		/*Added by Padmavathi for updating group name ends*/
		
		/*Changed by Gangadhar Badagi to update the space in dateformat field TENJINCG-356 starts*/
		
		if(module.getDateFormat()==null)
		{
			m.setDateFormat(mb.getDateFormat());
		}
		/*Changed by Gangadhar Badagi to update the space in dateformat field TENJINCG-356 ends */
		else{
			m.setDateFormat(module.getDateFormat());
		}
		/*Changed by Gangadhar Badagi to update the space in WSURL field for TENJINCG-356 starts*/
		
		if(module.getWSURL()==null){
			m.setWsurl(mb.getWSURL());
		}
		/*Changed by Gangadhar Badagi to update the space in WSURL field for TENJINCG-356 ends*/
		else{
			m.setWsurl(module.getWSURL());
		}
		/*Changed by Gangadhar Badagi to update the space in WSOP field TENJINCG-356 starts*/
		
		if(module.getWSOp()==null){
			m.setWsop(mb.getWSOp());
		}
		/*Changed by Gangadhar Badagi to update the space in WSOP field TENJINCG-356 ends*/
		else{
			m.setWsop(module.getWSOp());
		}
		m.setAut(appId);
		m.setPreLearnString(null);
		m.setCode(module.getModuleCode());
		return m;
		
	}
	/*Added by Gangadhar Badagi for T25IT-333 ends */
	// changed jsonarray to list by manish
	/*Added by Gangadhar Badagi for TENJINCG-356 starts */
	public List<String> validateBulkFunctionsSize(ModuleBean module){
		/*JSONArray jarray=new JSONArray();*/
		List<String> messages = new ArrayList<String>();
		
		if(Utilities.trim(module.getModuleName()).length()>200){
			logger.error("Function Name too large");
			messages.add(module.getModuleName()+" should be less than or equal to 200 words. ");
		}
		
		if(Utilities.trim(module.getModuleCode()).length()>100){
			logger.error("Function Code too large");
			messages.add(module.getModuleCode()+" should be less than or equal to 100 words. ");
		}
		
		if(Utilities.trim(module.getMenuContainer()).length()>200){
			logger.error("Menu container too large");
			messages.add(module.getMenuContainer()+" should be less than or equal to 200 words. ");
		}
		
		if(module.getAut()!=0){
			messages.add(" Can't update aut. ");
		}
		/*return jarray;*/
		return messages;
	}
	/*Added by Gangadhar Badagi for TENJINCG-356 ends */
}
