/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UserService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 11-nov-2016			prafful tiwari			new file added
 * 05-Dec-2016			Sriram					TJN_243_02
 * 05-Dec-2016			Gangadhar				Removed Empty path annotations
 * 17-Dec-2016          Leelaprasad             Defect fix#TEN-98,TEN-99,TEN-100
 * 02-Aug-2017			Gangadhar Badagi		TENJINCG-183
   08-Aug-2017			Manish					T25IT-19
   08-Aug-2017			Manish					T25IT-20
   08-Aug-2017			Pushpalatha				Defect T25IT-14
  08-Aug-2017			Pushpalatha				Defect T25IT-38
   09-Aug-2017			Manish					T25IT-28
   10-Aug-2017			Gangadhar Badagi		T25IT-88
   17-Aug-2017			Gangadhar Badagi		T25IT-93
   23-Aug-2017			Roshni					User role Validation
   24-Aug-2017			Roshni					T25IT-298
   29-Aug-2017			Roshni					T25IT-298
* 10-Nov-2017			Manish					TENJINCG-428
* 10-Nov-2017			Manish					user enable/disable service
* 23-Nov-2017			Gangadhar Badagi        Added to validate tenjin user credentials
* 27-11-2017            Gangadhar Badagi        Added to change status
* 27-11-2017            Padmavathi              TENJINCG-534
  27-11-2017            Padmavathi              TENJINCG-535 
  28-Nov-2017           Manish		            TENJINCG-536 
  28-11-2017            Padmavathi              TENJINCG-538
  29-Nov-2017			Manish					rearranged code(removed UserValidator code and used UserHandler)
  24-04-2018			Preeti					Added catch block for getUser(RecordNotFoundException)
  18-05-2018			Pushpalatha				TENJINCG-648
  29-04-2019			Pushpalatha				TENJINCG-1035
  24-06-2019            Padmavathi              for license
* 
 */



package com.ycs.tenjin.rest;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ycs.tenjin.DuplicateUserSessionException;
import com.ycs.tenjin.MaxActiveUsersException;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.UserHelper;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.User;

@Path("/users")
public class UserService { 	 
	
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	@Context
	ContainerRequestContext requestContext;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUsers(){
		try {
			List<User> users = new UserHandler().getAllUsers();
			return RestUtils.buildResponse(Status.OK, "Users fetched successfully.", users);
		} catch (DatabaseException e) {
			logger.error("Could not fetch all users",e );
			return RestUtils.buildResponse(Status.NOT_FOUND, "An internal error occurred, Please contact Tenjin supprt.", null);
		}
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@PathParam("id") String id){
		logger.info("Request to Get User with ID [{}]", id);
		Response response = null;
			User user;
			try {
				user = new UserHandler().getUser(id);
				response = RestUtils.buildResponse(Status.OK, "User details fetched successfully.", user);
			} 
			catch (RecordNotFoundException e) {
				response = RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
			}
			catch (DatabaseException e) {
				response = RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
			}
		return response;
	}
	
	
	@PUT
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateUser(String jsonString, @PathParam("id") String id){
		logger.info("Request to update user");
		logger.info("Input JSON --> {}", jsonString);
		logger.info("ID --> {}", id);
	
		User loggedInUser=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		if(loggedInUser.getRoles().equalsIgnoreCase("Tester"))
				{
			return RestUtils.buildResponse(Status.UNAUTHORIZED, loggedInUser.getRoles()+" cannot update the details", null);
				}
		try{
			User objUser = new Gson().fromJson(jsonString, User.class);
			objUser.setId(id);				
			/*Changed by Pushpalatha for TENJINCG-648 starts*/
			new UserHandler().update(objUser,loggedInUser.getId());
			/*Changed by Pushpalatha for TENJINCG-648 ends*/
			return RestUtils.buildResponse(Status.OK, "User Updated Successfully", new UserHandler().getUser(id));
		} catch (RecordNotFoundException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
		}catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
		catch (Exception e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createUser(String jsonString){
		logger.info("Request to post user");						
		logger.info("Input JSON --> {}", jsonString);
		/*Added by Pushpa for TENJINCG-1035 starts*/
		User loggedInUser=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		/*Added by Pushpa for TENJINCG-1035 ends*/
		User user = new Gson().fromJson(jsonString, User.class);
		try {
			/*Modified by Pushpa for TENJINCG-1035 starts*/
			new UserHandler().persist(user,loggedInUser.getId());
			/*Modified by Pushpa for TENJINCG-1035 ends*/
			return RestUtils.buildResponse(Status.CREATED, "User Created Successfully", new UserHandler().getUser(user.getId()));
		}catch(RecordNotFoundException e){
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}catch(RequestValidationException e){
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
		} catch (TenjinServletException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, "An Internal error occured, Please contact Tenjin support", null);
		} /*Added by Padmavathi for license starts*/
		catch (MaxActiveUsersException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
		}
		/*Added by Padmavathi for license ends*/
	}
	
	
	@Path("/bulk")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response bulkCreateUsers(String jsonString){
		logger.info("Request to post user");						
		logger.info("Input JSON --> {}", jsonString);
		
		Type listType = new TypeToken<ArrayList<User>>(){}.getType();
		List<User> users = new Gson().fromJson(jsonString, listType);
		List<String> messagesList = new ArrayList<String>();
		List<User> createdUserList = new ArrayList<User>();
		/*Added by Pushpa for TENJINCG-1035 starts*/
		User loggedInUser=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		/*Added by Pushpa for TENJINCG-1035 ends*/
		for(User user:users){
			try {
				/*Modified by Pushpa for TENJINCG-1035 starts*/
				new UserHandler().persist(user,loggedInUser.getId());
				/*Modified by Pushpa for TENJINCG-1035 ends*/
				messagesList.add("User '"+user.getId()+"' created successfully!");
				createdUserList.add(new UserHandler().getUser(user.getId()));	
			}catch (RecordNotFoundException e) {
				messagesList.add("User '"+user.getId()+"' -- "+e.getMessage());
			}catch (DatabaseException e) {
				messagesList.add("User '"+user.getId()+"' -- "+e.getMessage());
			} catch (RequestValidationException e) {
				messagesList.add("User '"+user.getId()+"' -- "+e.getMessage());
			} catch (TenjinServletException e) {
				logger.error("User '"+user.getId()+"' -- "+e.getMessage());
			} /*Added by Padmavathi for license starts*/
			catch (MaxActiveUsersException e) {
				messagesList.add("User '"+user.getId()+"' -- "+e.getMessage());
			}
			/*Added by Padmavathi for license ends*/
			
		}
		return RestUtils.buildResponse(Status.OK, messagesList.toString(), createdUserList);
		
	}


	@PUT
	@Path("/{id}/{enableStatus}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response changeUserStatus(@PathParam("id") String updateUserId, @PathParam("enableStatus") String enableStatus){
		logger.info("Request to enable/disable user");
		logger.info("ID --> {}", updateUserId);

		User loggedInUser=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		try{
			User user = new UserHandler().getUser(updateUserId);
			// tester cannot update the details
			if(loggedInUser.getRoles().equalsIgnoreCase("Tester")){
				return RestUtils.buildResponse(Status.UNAUTHORIZED, loggedInUser.getRoles()+" cannot update the details", null);
			}
			new UserHandler().changeUserStatus(user, enableStatus, loggedInUser.getId());
			return RestUtils.buildResponse(Status.OK, "User Updated Successfully", new UserHandler().getUser(user.getId()));
		}
		catch (RecordNotFoundException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
		}
		catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
		}catch (ResourceConflictException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
		}
	}
	
	/*Added by Gangadhar Badagi to validate tenjin user credentials starts*/
	@POST
	@Path("/validateuser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response valdateTenjinUserCredentials( String jsonString) {
		
		User user = new Gson().fromJson(jsonString, User.class);
		try{
			/* Modified by Prem for VAPT Passwrod Encryption change starts*/
			//String hashedpassword = Hashing.sha256().hashString(user.getPassword() , StandardCharsets.UTF_8).toString();
			user = new UserHelper().authenticateRest(user.getId(), user.getPassword());
	//	user = new UserHelper().authenticateRest(user.getId(), user.getPassword());
		/* Modified by Prem for VAPT Passwrod Encryption change Ends*/
		return RestUtils.buildResponse(Status.OK, "User credentials are validated successfully!.", null);
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
		}
		catch (DuplicateUserSessionException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
		}
		
		
	}	
}
