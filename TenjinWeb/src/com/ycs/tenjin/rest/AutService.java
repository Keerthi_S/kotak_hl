/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AutService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 11-nov-2016			nagababu				new file added
 * 05-Dec-2016          Gangadhar Badagi        Removed empty path annoatation
 * 21-Dec-2016        	Abhilash                Defect #TEN-144
 * 17-Feb-2017         	Gangadhar Badagi        TENJINCG-126
 * 17-Aug-2017         	Gangadhar Badagi       	T25IT-93
 * 18-Aug-2017			Manish					T25IT-166
 * 22-Sep-2017			sameer gupta			For new adapter spec
 * 17-Nov-2017        	Gangadhar Badagi        TENJINCG-473
 * 20-Nov-2017			Preeti Singh			TENJINCG-483
 *  01-12-2017			Gangadhar Badagi		TENJINCG-546
 * 01-12-2017			Gangadhar Badagi		TENJINCG-547
 * 01-03-2018           Padmavathi              TENJINCG-554
 * 17-12-2018           Padmavathi              TJN252-36
 *  */

package com.ycs.tenjin.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.AutAuditRecord;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.user.User;

@Path("/auts")
public class AutService {
	private static final Logger logger = LoggerFactory
			.getLogger(UserService.class);

	@Context
	ContainerRequestContext requestContext;
	/****** Removed empty path annotation *****/
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response getAllAuts() {
		try {
			return RestUtils.buildResponse(Status.OK,
					"Auts fetched successfully", new AutHandler().getAllApplications());
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAut(@PathParam("id") String id) {

		try {
			return RestUtils.buildResponse(Status.OK,"Aut fetched successfully!.", new AutHandler().getApplication(Integer.parseInt(id)));
		    } catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(),null);
		    }
		catch (RecordNotFoundException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(),null);
		    }
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createAut(String jsonString) {
		logger.info("Input JSON --> {}", jsonString);

		Aut aut = new Gson().fromJson(jsonString, Aut.class);
		try {
			new AutHandler().persist(aut);
			return RestUtils.buildResponse(Status.CREATED,
					"Aut Created Successfully",
					new AutHandler().getApplication(aut.getId()));
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(),
					null);
		} catch (DuplicateRecordException e) {
			return RestUtils.buildResponse(Status.CONFLICT, e.getMessage(),e.getTargetFields(),
					null);
		} catch (RequestValidationException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),e.getTargetFields(),
					null);
		}

	}
	
	
	@PUT
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateAut(String jsonString, @PathParam("id") int id) {
		logger.info("Input JSON --> {}", jsonString);
		logger.info("ID --> {}", id);

		Aut aut = new Gson().fromJson(jsonString, Aut.class);
		/*Added by Padmavathi for Regression Pack fix starts*/
		AutAuditRecord audit=new AutAuditRecord();
		audit.setEntityRecordId(aut.getId());
		User loggedInUser = (User) requestContext.getProperty(RestContextProperties.TJN_USER);
		audit.setLastUpdatedBy(loggedInUser.getId());
		audit.setEntityType("application");
		aut.setAutAuditRecord(audit);
		/*Added by Padmavathi for Regression Pack fix ends*/
		try {
			new AutHandler().update(aut, id);
			return RestUtils.buildResponse(Status.OK,
					"Aut Updated Successfully",
					new AutHandler().getApplication(id));
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),
					null);
		} catch (RequestValidationException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),e.getTargetFields(),
					null);
		}
		/*Added by Gangadhar Badagi for TENJINCG-546 starts*/
		catch (RecordNotFoundException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),null);
		}
		/*Added by Gangadhar Badagi for TENJINCG-546 ends*/ 
		/*Added by Padmavathi for TJN252-36 starts*/
		catch (ResourceConflictException e) {
			return RestUtils.buildResponse(Status.CONFLICT, e.getMessage(),null);
		}
		/*Added by Padmavathi for TJN252-36 ends*/
	}
	@Path("/{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteAut(@PathParam("id") int id) {

		try {
			new AutHandler().deleteAut(String.valueOf(id));
			return RestUtils.buildResponse(Status.OK, "Aut deleted Successfully",null );
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
		} catch (ResourceConflictException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), e.getTargetFields(),null);
		}
		/*Added by Gangadhar Badagi for TENJINCG-547 starts*/
		catch (RecordNotFoundException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),null);
		}
		/*Added by Gangadhar Badagi for TENJINCG-547 ends*/
	}
	
	
	
}
