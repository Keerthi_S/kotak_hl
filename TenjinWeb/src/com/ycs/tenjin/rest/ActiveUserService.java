/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ActiveUserService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 11-nov-2016	        prafful tiwari	        new file added
   05-Dec-2016          Gangadhar Badagi        Removed empty path annoatation
 */

package com.ycs.tenjin.rest;

import java.util.ArrayList;
import java.util.ListIterator;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.UserHelper;
import com.ycs.tenjin.user.User;

@Path("/activeusers")
public class ActiveUserService {

	private static final Logger logger = LoggerFactory
			.getLogger(UserService.class);

	@Path("/{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response clearUser(@PathParam("id") String id) {
		logger.info("Request to clear the user");

		try {
			
			ArrayList<User> currentUser = new UserHelper()
			.hydrateCurrentUsers();
			int flag=0;
			ListIterator<User> listIterator = currentUser.listIterator();
			User temp = null;
			while (listIterator.hasNext()) {
				temp = listIterator.next();
				if (id.equals(temp.getId())) {
					flag = 1;
					new UserHelper().clearUserSession(id);
					break;
				}
			}
			
			if(flag==1)
			return RestUtils.buildResponse(Status.OK,
					"User cleared Successfully", null);
			
			return RestUtils.buildResponse(Status.OK,
					"Users not available ", null);
		} catch (DatabaseException e) {
			
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,
					e.getMessage(), null);
		}
	}



	/******Removed empty path annotation*****/
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response clearMultipleUser(String jsonString) {
		logger.info("Request to clear multiple  users");
		logger.info("Input JSON --> {}", jsonString);

		JSONObject json = null;

		try {
			json = new JSONObject(jsonString);
			System.out.println(json.get("id").toString());
			JSONArray jsonarr = json.getJSONArray("id");
			String available = "";
			ArrayList<User> currentUser = new UserHelper()
					.hydrateCurrentUsers();

			for (int i = 0; i < jsonarr.length(); i++) {
				String username = jsonarr.getString(i);
				int flag = 0;
				ListIterator<User> listIterator = currentUser.listIterator();
				User temp = null;
				while (listIterator.hasNext()) {
					temp = listIterator.next();
					if (username.equals(temp.getId())) {
						flag = 1;
						new UserHelper().clearUserSession(username);
						break;
					}
				}

				if (flag == 1)
					available += temp.getId() + " ";
			}

			if (!available.equals(""))
				return RestUtils.buildResponse(Status.OK, available
						+ " cleared Successfully ", null);

			return RestUtils.buildResponse(Status.OK,
					"Users not available ", null);
		} catch (JSONException e) {
			logger.error("Error ", e);
			
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,
					e.getMessage(), null);
		}

		catch (DatabaseException e) {
			
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,
					e.getMessage(), null);
		}
	}
	
	
	/******Removed empty path annotation*****/
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllActiveUsers() {
		logger.info("Request to get all active users");

		try {
			
			ArrayList<User> currentUser = new UserHelper()
			.hydrateCurrentUsers();
			ListIterator<User> listIterator = currentUser.listIterator();
			ArrayList<User> user = new ArrayList<User>();
			while (listIterator.hasNext()) 
				user.add(listIterator.next());
				
			if(user.size()>0)
			return RestUtils.buildResponse(Status.OK,
					"All user fetched Successfully", user);
			else
				return RestUtils.buildResponse(Status.NOT_FOUND,
						"Users are not available", "");
			
		} catch (DatabaseException e) {
			
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,
					e.getMessage(), null);
		}
	}
	
	
	
}
