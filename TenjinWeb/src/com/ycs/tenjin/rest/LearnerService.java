/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LearnerService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 11-nov-2016			nagababu				new file added
 * 16-Nov-2017          Gangadhar Badagi        TENJINCG-456
 *  17-Nov-2017          Gangadhar Badagi        TENJINCG-461
 * 17-Nov-2017          Padmavathi               TENJINCG-460,462
 * 24-06-2019           Padmavathi              for license
 */


package com.ycs.tenjin.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.MaxActiveUsersException;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiLearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.handler.ModuleHandler;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.User;

/*Changed by Gangadhar Badagi to remove Path starts*/
/*@Path("/learner")*/
@Path("/applications")
/*Changed by Gangadhar Badagi to remove Path ends*/
public class LearnerService {
	private static final Logger logger = LoggerFactory.getLogger(LearnerService.class);
	
	@Context
	ContainerRequestContext requestContext;

	@Context
	SecurityContext securityContext;
	
	//Web Service to begin the Run for learning
	@GET
	@Path("/{runId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLearningInformation(@PathParam("runId") int runId){
		logger.info("Request to get learner run Information");
		Response response=null;
		JSONObject json=new JSONObject();
		try {
			json= new RunHelper().getLearningInformationforRun(runId);
			response=Response.status(Status.OK).entity(json.toString()).build();
		} catch (DatabaseException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		return response;
	}
	
	@PUT
	@Path("/{runId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response beginRun(@PathParam("runId") int runId){
		logger.info("Request to put updating run");
		Response response=null;
		try {
			 new RunHelper().beginLearningRun(runId, false);
			 response=RestUtils.buildResponse(Status.CREATED, "Run updated Successfully", null);			
		} catch (DatabaseException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		return response;
	}
	
	@PUT
	@Path("/endRun/{runId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response endRun(@PathParam("runId") int runId){
		logger.info("Request to put updating run");
		Response response=null;
		try {
			 new RunHelper().endLearningRun(runId);
			 response=RestUtils.buildResponse(Status.CREATED, "Run updated Successfully", null);			
		} catch (DatabaseException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		return response;
	}
	
	@POST
	/*Changed by Padmavathi for TENJINCG-460,462 starts*/
	//@Path("/functions")
	@Path("/{applicationId}/functions/learn")
	/*Changed by Padmavathi for TENJINCG-460,462 ends*/
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
		/*Changed by Padmavathi for TENJINCG-460,462 starts*/
	//public Response learnFunctions(LearnerInput learnerInput) {
	public Response learnFunctions(LearnerInput learnerInput,@PathParam("applicationId") int appId) {
	/*Changed by Padmavathi for TENJINCG-460,462 ends*/
		logger.info("Got request to learn functions");
		learnerInput.setApplicationId(appId);
		/*Changed by Padmavathi for TENJINCG-460,462 ends*/
		User user = (User) requestContext.getProperty(RestContextProperties.TJN_USER);
		
		if(user == null) {
			logger.error("USER is null in request context");
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "User not found in session. Your session may have timed out. Please login again", null);
		}
		
		RestLearningHandler handler= new RestLearningHandler(learnerInput, user.getId());
		
		try {
			handler.validateGuiLearning();
			int runId = handler.startRun();
			
			return RestUtils.buildResponse(Status.OK, "Learning initiated successfully!", runId);
		} catch (RestRequestException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
		} catch (TenjinServletException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
		/* Uncommented by Padmavathi for license starts */ 
	/*	Added by Prem for validating adapter license start*/
		catch (MaxActiveUsersException | LicenseInactive e) {
			return RestUtils.buildResponse(Status.UNAUTHORIZED, e.getMessage(), null);
		/*Added by Prem for validating adapter license start*/
		}/* Uncommented by Padmavathi for license ends */
		
	}
	
	@POST
	/*Changed by Padmavathi for TENJINCG-460,462 starts*/
	//@Path("/apis")
	@Path("/{applicationId}/apis/learn")
	/*Changed by Padmavathi for TENJINCG-460,462 ends*/
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	/*Changed by Padmavathi for TENJINCG-460,462 starts*/
	/*public Response learnApis(LearnerInput learnerInput) {*/
	public Response learnApis(LearnerInput learnerInput,@PathParam("applicationId") int appId) {
		
		logger.info("Got request to learn functions");
		learnerInput.setApplicationId(appId);
		/*Changed by Padmavathi for TENJINCG-460,462 ends*/
		User user = (User) requestContext.getProperty(RestContextProperties.TJN_USER);
	
		if(user == null) {
			logger.error("USER is null in request context");
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "User not found in session. Your session may have timed out. Please login again", null);
		}
		
		RestLearningHandler handler= new RestLearningHandler(learnerInput, user.getId());
		
		try {
			handler.validateApiLearning();
			int runId = handler.startRun();
			
			return RestUtils.buildResponse(Status.OK, "Learning initiated successfully!", runId);
		} catch (RestRequestException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
		} catch (TenjinServletException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		} 
		/*Added by Prem for validating adapter license start*/
		/* Uncommented by Padmavathi for license starts */
		catch (MaxActiveUsersException | LicenseInactive e) {
			return RestUtils.buildResponse(Status.UNAUTHORIZED, e.getMessage(), null);
		/*Added by Prem for validating adapter license start*/
		}/* Uncommented by Padmavathi for license ends */
		
	}
	/*Added by Gangadhar Badagi for TENJINCG-456 starts*/
	@GET
	@Path("/{applicationId}/apis/{apicode}/learningHistory")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getApiLearningHistory(@PathParam("apicode") String apiCode,@PathParam("applicationId") int appId) {

		ArrayList<ApiLearnerResultBean> lrnr_runs=null;
		
		try {
			new AutHelper().hydrateAut(appId);
			Api api=new ApiHelper().hydrateApi(appId, apiCode);
			if(api==null){
				return RestUtils.buildResponse(Status.NOT_FOUND, "Invalid apiCode ["+apiCode+"]", null);
			}
			lrnr_runs=new ApiHelper().hydrateLearningHistory(apiCode, appId);
			return RestUtils.buildResponse(Status.OK, "Api learning history fetched successfully!.", lrnr_runs);
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
		}		
		
	}
	/*Added by Gangadhar Badagi for TENJINCG-456 ends*/
	/*Added by Gangadhar Badagi for TENJINCG-461 starts*/
	@GET
	@Path("/{applicationId}/functions/{modulecode}/learningHistory")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFunctionLearningHistory(@PathParam("modulecode") String moduleCode,@PathParam("applicationId") int appId) {

		List<LearnerResultBean> lrnr_runs=null;
		ModuleHandler mHandler = new ModuleHandler();
		try {
			new AutHelper().hydrateAut(appId);
			mHandler.getModule(appId, moduleCode);
			lrnr_runs = mHandler.getLearningHistory(appId, moduleCode);
			return RestUtils.buildResponse(Status.OK, "Function learning history fetched successfully!.", lrnr_runs);
		} 
		catch(RecordNotFoundException re){
			return RestUtils.buildResponse(Status.NOT_FOUND,"Invalid moduleCode ["+moduleCode+"]" , null);
		}
		catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
		}		
		
	}
	/*Added by Gangadhar Badagi for TENJINCG-461 ends*/
	
}
