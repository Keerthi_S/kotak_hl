/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutorService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 11-nov-2016			nagababu				new file added
 * 12-Jan-2017          Parveen                 REQ #TENJINCG-28 (Running Test Set through REST)
 * 12-Jan-2017          Parveen                 REQ #TENJINCG-29 (Fetching RunId information through REST)
 * 20-Jun-2017			Sriram Sridharan		Changes for TENJINCG-235
 * 16-11-2017			Preeti					TENJINCG-454
 * 05-01-2018	Parveen					TENJINCG-561
 */

package com.ycs.tenjin.rest;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ExecutionHelper;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.PdfHandler;

/*Modified by Preeti for TENJINCG-454 starts*/
@Path("/runs")
/*Modified by Preeti for TENJINCG-454 ends*/
public class ExecutorService {

	//Added by Parveen for the REQ #TENJINCG-28 starts
	@Context
	ContainerRequestContext requestContext;

	@Context
	SecurityContext securityContext;
	//Added by Parveen for the REQ #TENJINCG-28 ends

	private static final Logger logger = LoggerFactory.getLogger(ExecutorService.class);
	@PUT
	@Path("/{runId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response beginRun(@PathParam("runId") int runId){
		logger.info("Request to put updating run");
		Response response=null;
		try {
			new RunHelper().beginRun(runId);
			response=RestUtils.buildResponse(Status.CREATED, "Run updated Successfully", null);			
		} catch (DatabaseException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		return response;
	}

	@PUT
	@Path("/endRun/{runId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response endRun(@PathParam("runId") int runId){
		logger.info("Request to put updating run");
		Response response=null;
		try {
			new RunHelper().endRun(runId,"Complete");
			response=RestUtils.buildResponse(Status.CREATED, "Run updated Successfully", null);			
		} catch (DatabaseException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		return response;
	}
	

	//Added by Parveen for the REQ #TENJINCG-29 starts
	@GET
	@Path("/{runId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRunDetails(@PathParam("runId") int runId){
		logger.info("Request to Get Run Details");
		Response response=null;
		TestRun testRun=null;
		Connection conn =null;
		try {
			conn =DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			testRun=new ExecutionHelper().hydrateOngoingTestRun(conn,runId);
			if(testRun!=null)
				response = RestUtils.buildResponse(Status.OK,
						"Run Details fetched Successfully for RunId "+runId,testRun);
			else{
				logger.error("This RunId ",runId," dose not exists ");
				response = RestUtils.buildResponse(Status.NOT_FOUND,
						runId+"-RunId does not exists", null);
			}

		} catch (DatabaseException e) {
			logger.error("Error occured while connecting to database  during hydrating Testrun details ", e);
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage()+" of runId "+runId, null);
		}catch(Exception e){
			logger.error("An internal error occurred", e);
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		finally{
			try{
				conn.close();
			}catch(Exception e){
				logger.error("An  error occurred while closing the database connection ", e);
			}
		}
		return response;
	}
	//Added by Parveen for the REQ #TENJINCG-29 ends
	
	
	/*Added by parveen for the requirement TENJINCG-561 starts*/
	@POST
	@Path("/{runId}/runReport")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getRunReport(@PathParam("runId") int runId,@Context HttpServletRequest request,String jsonString){
		logger.info("Request for Getting the Run Report");
		Response response =null;
		TestRun testRun=null;
		try {
			
			User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
			testRun=new RunHelper().hydrateRun(runId);
			if(testRun==null){
				logger.error("This RunId ",runId," dose not exists ");
			    response = RestUtils.buildResponse(Status.NOT_FOUND,
					runId+"-RunId does not exists", null);		
			}
			else{
				
				if(testRun.getProjectId()==0){
					logger.error("This Run Id  ",runId,"dont have the report ");
				    response = RestUtils.buildResponse(Status.NOT_FOUND,
						runId+"-RunId is for "+testRun.getTaskType()+" so cant able to download the Report", null);		
				}
				else{
					JSONObject pathObject = new JSONObject(jsonString);
					String folderpath=pathObject.getString("reportPath");
					new ExecutorValidator().checkForRunReportFolder(folderpath);
					

					PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), user.getFullName());
					String fileName = pdfHandler.generatePdfReportForRun(runId,testRun.getProjectId(),folderpath);
					response=RestUtils.buildResponse(Status.CREATED, "Run Report for the RunId "+runId+" in the following Path "+folderpath+" with file name "+fileName, null);		
				}
				
			}
			
			
				
		} catch (DatabaseException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		return response;
	}
	/*Added by parveen for the requirement TENJINCG-561 starts*/
}
