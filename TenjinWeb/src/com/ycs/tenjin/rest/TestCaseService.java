/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCaseService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 19-03-2018			Pushpalatha				Newly Added for TENJINCG-611
 * 19-02-2019			Preeti					TENJINCG-969
 
 * 
 */


package com.ycs.tenjin.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.db.CoreDefectHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.db.TestCaseHelper;
import com.ycs.tenjin.defect.Defect;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.TestCaseHandler;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

@Path("/domains")
public class TestCaseService {

	private static final Logger logger = LoggerFactory.getLogger(TestCaseService.class);
	private String STORAGE="Local";
	/************
	 * Req No. TJN_243_02 - Rest Service PRJ_TC_01 - Get Test Case Details - Sriram
	 */

	@Context
	ContainerRequestContext requestContext;

	@Context
	SecurityContext securityContext;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{dName}/projects/{pName}/testcases")
	public Response getAllTestCases(@PathParam("dName") String domainName, @PathParam("pName") String projectName){
		
		logger.debug("entered getAllTestCases({}, {})", domainName, projectName);
		logger.info("Getting all Test Cases in Domain [{}], Project [{}]", domainName, projectName);
		try {
			Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
			logger.info("Getting all tests under project");
			List<TestCase> testCases = new TestCaseHandler().getAllTestCases(project.getId());
			logger.info("{} test cases found in all", testCases.size());
			Response response = RestUtils.buildResponse(Status.OK, "TestCase details are fetched successfully.", testCases);
			return response;
		} catch (DatabaseException e) {
			logger.error("Could not fetch all the test cases",e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		} 
		
	}

	
	@Path("/{dName}/projects/{pName}/testcases")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createNewTestCase(String jsonString,@PathParam("dName") String domainName,@PathParam("pName") String projectName)
	{
		logger.info("Request to create New Test case");						
		logger.info("Creating New Test Case in Domain [{}], Project [{}]", domainName, projectName);
		TestCase testCase=new Gson().fromJson(jsonString, TestCase.class);
		Project project=(Project)requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);

		testCase.setProjectId(project.getId());
		testCase.setRecordType("TC");
		testCase.setTcFolderId(-1);
		testCase.setTcCreatedBy(user.getId());
		testCase.setStorage(STORAGE);
		TestCaseHandler handler=new TestCaseHandler();
		try {
			handler.persistTestCase(testCase);
			logger.info("Testcase created Successfully");
			return RestUtils.buildResponse(Status.CREATED, "Test Case Created Successfully", testCase);
		}catch(DuplicateRecordException e){
			return RestUtils.buildResponse(Status.FOUND, e.getMessage(), null);
		}catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(),null);
		} catch (RequestValidationException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(),e.getTargetFields(),null);
		}
		
	}



	@Path("/{dName}/projects/{pName}/testcases/{tcRecId}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTestCase(@PathParam("tcRecId")int tcRecId,String jsonString,@PathParam("dName") String domainName,@PathParam("pName") String projectName)
	{
		logger.info("Request to update Test case");						
		logger.info("Updating Test Case in Domain [{}], Project [{}]", domainName, projectName);
		TestCase testCase=new Gson().fromJson(jsonString, TestCase.class);
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		TestCaseHandler handler=new TestCaseHandler();
		try {
			Project project=(Project)requestContext.getProperty(RestContextProperties.TJN_PROJECT);
			TestCase tc=handler.hydrateTestCase(project.getId(), tcRecId);
			testCase.setTcRecId(tcRecId);
			testCase.setProjectId(project.getId());
			/*Added by Preeti for TENJINCG-969 starts*/
			AuditRecord audit = new AuditRecord();
			audit.setEntityRecordId(testCase.getTcRecId());
			audit.setLastUpdatedBy(user.getId());
			audit.setEntityType("testcase");
			testCase.setAuditRecord(audit);
			/*Added by Preeti for TENJINCG-969 ends*/
			handler.updateTestCase(testCase);
			tc=handler.hydrateTestCase(project.getId(), tcRecId);
			return RestUtils.buildResponse(Status.OK, "TestCase Updated Successfully",tc);
		}catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),null);
		} catch (RequestValidationException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),e.getTargetFields(),null);
		}catch (RecordNotFoundException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),null,null);
		}
	}

	@Path("/{dName}/projects/{pName}/testcases/{tcRecId}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteTestCase(@PathParam("tcRecId")int tcRecId,@PathParam("dName") String domainName,@PathParam("pName") String projectName)
	{
		logger.info("Request to delete Test case");						
		logger.info("Deleting Test Case in Domain [{}], Project [{}]", domainName, projectName);
		try {
			Project project=(Project)requestContext.getProperty(RestContextProperties.TJN_PROJECT);
			
			TestCaseHandler handler=new TestCaseHandler();
			handler.deleteTestCase(project.getId(),tcRecId);
			
			return RestUtils.buildResponse(Status.OK, "TestCase Deleted Successfully",null);
			
		} catch(RecordNotFoundException e){
			return RestUtils.buildResponse(Status.NOT_FOUND, "test case rec id "+tcRecId+" does not exist ", null);
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
			
		} catch (RequestValidationException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
			
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{dName}/projects/{pName}/testcases/{recordId}")
	public Response getTestCase(@PathParam("recordId") int recordId,@PathParam("dName") String domainName, @PathParam("pName") String projectName){
		logger.debug("Request to get test case details");
		logger.info("Getting  Test Case details in Domain [{}], Project [{}]", domainName, projectName);
		TestCaseHandler handler=new TestCaseHandler();
		try {
			Project project=(Project)requestContext.getProperty(RestContextProperties.TJN_PROJECT);
			TestCase testCases = handler.hydrateTestCase(project.getId(), recordId);
			logger.info("Getting  test case details under project");
			Response response = RestUtils.buildResponse(Status.OK, "TestCase details are fetched successfully.", testCases);
			return response;
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		} catch (RecordNotFoundException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),null,null);
		}
	}

	
	
	@Path("/{dName}/projects/{pName}/testcases/bulk")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createTestCases(String jsonString,@PathParam("dName") String domainName,@PathParam("pName") String projectName) throws TenjinServletException, RequestValidationException
	{
		logger.info("Request to create New Test case");						
		logger.info("Creating Test Cases in Domain [{}], Project [{}]", domainName, projectName);
		List<String> messages=new ArrayList<String>();
		
		List<TestCase> createdTestCases = new ArrayList<TestCase>();
		TestCaseHandler handler=new TestCaseHandler();
		try {			
			JSONArray jsonArray=new JSONArray(jsonString);
			for (int i = 0; i < jsonArray.length(); i++) {

				TestCase testCase=new Gson().fromJson(jsonArray.get(i).toString(), TestCase.class);

				Project project=(Project)requestContext.getProperty(RestContextProperties.TJN_PROJECT);
				User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);

				testCase.setProjectId(project.getId());
				testCase.setRecordType("TC");
				testCase.setTcFolderId(-1);
				testCase.setTcCreatedBy(user.getId());
				testCase.setStorage(STORAGE);
				try {
					handler.persistTestCase(testCase);
					messages.add(" Test Case '"+testCase.getTcId()+"' created successfully");
					createdTestCases.add(testCase);
				} catch (DatabaseException e) {
					messages.add(e.getMessage());
					logger.error("Error ", e);
				}catch (RequestValidationException e) {
					messages.add(Arrays.toString(e.getTargetFields())+" Not Found");
					
				}
			}
			
			return RestUtils.buildResponse(Status.CREATED, messages.toString(), createdTestCases);
		} 
		
		catch (JSONException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
	}



	@Path("/{dName}/projects/{pName}/testcases/bulk")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTestCases(String jsonString,@PathParam("dName") String domainName,@PathParam("pName") String projectName)
	{
		logger.info("Request to update Test case");						
		logger.info("Updating Test Case in Domain [{}], Project [{}]", domainName, projectName);
		List<String> errorMessage=new ArrayList<String>();
		List<String> successMessage=new ArrayList<String>();
		List<TestCase> updatedTestCases=new ArrayList<TestCase>();
		
		List<String> messages = new ArrayList<String>();
		
		try {
			JSONArray jsonArray=new JSONArray(jsonString);
			for (int i = 0; i <jsonArray.length(); i++) {

				TestCase testCase=new Gson().fromJson(jsonArray.getString(i), TestCase.class);
				Project project=(Project)requestContext.getProperty(RestContextProperties.TJN_PROJECT);
				User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
				TestCaseHandler handler=new TestCaseHandler();
				try {
					TestCase tc=handler.hydrateTestCase(project.getId(), testCase.getTcRecId());
					testCase.setTcRecId(testCase.getTcRecId());
					testCase.setProjectId(project.getId());
					/*Added by Preeti for TENJINCG-969 starts*/
					AuditRecord audit = new AuditRecord();
					audit.setEntityRecordId(testCase.getTcRecId());
					audit.setLastUpdatedBy(user.getId());
					audit.setEntityType("testcase");
					testCase.setAuditRecord(audit);
					/*Added by Preeti for TENJINCG-969 ends*/
					handler.updateTestCase(testCase);
					successMessage.add(" Test Case '"+testCase.getTcRecId()+"' Updated Successfully ");
					TestCase updatedTestCase=handler.hydrateTestCase(project.getId(), testCase.getTcRecId());
					updatedTestCases.add(updatedTestCase);
				} catch (DatabaseException e) {
					errorMessage.add(e.getMessage());
					
				}catch(RecordNotFoundException e){
					errorMessage.add(e.getMessage());
				}
				catch (RequestValidationException e) {
					errorMessage.add("Can't update"+Arrays.toString(e.getTargetFields()));
				}
			 }
			
			
			if(successMessage.size()>0){
				for (int i = 0; i < successMessage.size(); i++) {
					messages.add(successMessage.get(i));
				}
				
			}
			if(errorMessage.size()>0){
				for (int i = 0; i < errorMessage.size(); i++) {

					messages.add(errorMessage.get(i));
				}
			}
			
			return RestUtils.buildResponse(Status.OK, messages.toString(), updatedTestCases);
			
		}
		catch (JSONException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
	}

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{dName}/projects/{pName}/testcases/{recordId}/runs")
	public Response getRunsForTestCase(@PathParam("recordId") int recordId,@PathParam("dName") String domainName, @PathParam("pName") String projectName){
		logger.debug("Request to get runs for test case");
		logger.info("Getting  runs for Test Case in Domain [{}], Project [{}]", domainName, projectName);
		try {
			Project project=(Project)requestContext.getProperty(RestContextProperties.TJN_PROJECT);
			TestCase ts = new TestCaseHelper().hydrateTestCase(project.getId(), recordId);
			if(ts==null){
				return RestUtils.buildResponse(Status.NOT_FOUND, " test case rec id '"+recordId+"' does not exist ", null);
			}
			else{
				logger.info("Getting  test case details under project");
				List<TestRun> runs=new RunHelper().getAllRunsForTestCase(recordId);
				
				if(runs !=null && runs.size()>0){
					return RestUtils.buildResponse(Status.OK, "Runs hydrated successfully for Test Case - "+ts.getTcId(), runs);
				}else{
					return RestUtils.buildResponse(Status.OK, "No runs available for Test Case "+ts.getTcId(), null);
				}
			}
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		} 
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{dName}/projects/{pName}/testcases/{recordId}/defects")
	public Response getDefectsForTestCase(@PathParam("dName") String domainName, @PathParam("pName") String projectName,@PathParam("recordId") int recordId) {
		logger.debug("Request to get defects for test case");
		logger.info("Getting  defects for Test Case");
		
		try {
			List<Defect> defects = new CoreDefectHelper().hydrateDefectsForTestCase(recordId);
			if(defects != null && defects.size() > 0) {
				return RestUtils.buildResponse(Status.OK, defects.size() + " defects found for this test case", defects);
			}else {
				return RestUtils.buildResponse(Status.NOT_FOUND, "No defects were found for this test case",null);
			}
		} catch(DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Support.",null);
		}
	}
	
	@POST
    @Path("/{dName}/projects/{pName}/testcases/{testCaseId}/execute")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public Response executeTestCase(@PathParam("dName") String domainName, @PathParam("pName") String projectName, @PathParam("testCaseId") String testCaseId, TestCaseExecutorInput input) {
		User user = (User) requestContext.getProperty(RestContextProperties.TJN_USER);
		Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);

		logger.info("Recieved request to execute test case");

		input.setTestCaseId(testCaseId);
		RestExecutionHandler handler = new RestExecutionHandler(input, user, project);
		try {
			handler.validateTestCaseExecutionInput();
		} catch (RestRequestException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		} catch (TenjinServletException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}


		try {
			int runId = handler.startRun();
			return RestUtils.buildResponse(Status.OK, "Execution initiated successfully!", runId);
		} catch (TenjinServletException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}

	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{dName}/projects/{pName}/testcases/search")
	public Response queryTestCase(@PathParam("dName") String domainName, @PathParam("pName") String projectName,
			@QueryParam("tcId") String tcId,
			@QueryParam("tcName") String tcName,@QueryParam("tcType") String type,@QueryParam("mode") String tcMode){

		if(Utilities.trim(tcId).equalsIgnoreCase("") && Utilities.trim(tcName).equalsIgnoreCase("") && 
				Utilities.trim(type).equalsIgnoreCase("") && Utilities.trim(tcMode).equalsIgnoreCase("")){
			return RestUtils.buildResponse(Status.BAD_REQUEST, "Please provide search creteria.", null);
		}
		
		TestCase testCase=new TestCase();
		testCase.setTcId(tcId);
		testCase.setTcType(type);
		testCase.setTcName(tcName);
		testCase.setMode(tcMode);
		try {
			Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
			ArrayList<TestCase> listTestCases=new TestCaseValidator().queryTestcase(testCase, String.valueOf(project.getId()));
			if(listTestCases.size()>0){
			return RestUtils.buildResponse(Status.OK, "Fetched '"+listTestCases.size()+"' rows successfully!.", listTestCases);
			}else{
				return RestUtils.buildResponse(Status.NOT_FOUND, "No records found.", null);
			}
		} catch (Exception e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		} 
	}
	
}

