/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Regclientvalidator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 11-nov-2016			nagababu				new file added
 * 17-Dec-2016          Leelaprasad              Defect fix#TEN-98,TEN-99,TEN-100
 * 17-Aug-2017			Sriram Sridharan		Fix for T25IT-131
 * 17-Aug-2017			Sriram Sridharan		Fix for T25IT-114
 * 17-Aug-2017			Sriram Sridharan		Fix for T25IT-125
 * 19-Aug-2017			Sriram Sridharan		Fix for T25IT-119
 */

package com.ycs.tenjin.rest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class RegclientValidator {

	private static final Logger logger = LoggerFactory.getLogger(ProjectHelper.class);

	public boolean regclientAvailability(String id){
		Connection conn=null;
		try {
			 conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			Statement st = conn.createStatement();
			ResultSet rs=st.executeQuery("SELECT rc_name FROM MASREGCLIENTS");
			while(rs.next()){
				if(rs.getString(1).equals(id))
					return true;
			}
			
			
		} catch (DatabaseException e) {
			
			logger.error("exception occured while fetching Client");
		}
		catch(SQLException e){
			logger.error("exception occured while fetching Client");
		}
		finally{
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("Error ", e);
				}
			}
		}
		
	return false;	
	}
	
	
	
	public void retreive(String jsonString, String name) {

		ClientHelper obj = new ClientHelper();
		try {

			RegisteredClient regclient = new RegisteredClient();
			regclient = obj.hydrateClient(name);

			String rcname = regclient.getName();
			String host = regclient.getHostName();
			String port = regclient.getPort();

			try {
				JSONObject myJsonObj = new JSONObject(jsonString);

				try {
					String jname = myJsonObj.getString("name");
					regclient.setName(jname);
				} catch (Exception e) {
					regclient.setName(rcname);
				}

				try {
					String jhost = myJsonObj.getString("hostName");
					regclient.setHostName(jhost);
				} catch (Exception e) {
					regclient.setHostName(host);
				}

				try {
					String jport = myJsonObj.getString("port");
					regclient.setPort(jport);
				} catch (Exception e) {
					regclient.setPort(port);
				}

				new ClientHelper().updateClient(regclient, name);

			} catch (JSONException e) {
				
				logger.error("exception occured while fetching regclient");
			}

		} catch (Exception e) {
			logger.error("exception occured while fetching regclient");
		}
	}
	/*Changed by leelaprasad for the requirement of Defect fix#TEN-98,TEN-99,TEN-100 on 17-12-2016 starts*/
	public boolean validateCreateDetails(JSONObject json) throws TenjinServletException {
	String msg=null;
		try {
		
				if(Utilities.trim(json.getString("port")).equalsIgnoreCase("")){
					logger.debug("port is mandatory");
					msg="port is mandatory";
					throw new TenjinServletException(msg);
				}
				
				//Fix for T25IT-131
				else if(!Utilities.trim(json.getString("port")).equalsIgnoreCase("")){
					String portValue = Utilities.trim(json.getString("port"));
					try {
						Integer.parseInt(portValue);
					} catch (NumberFormatException e) {
						logger.error("port number specified is not a valid number", portValue);
						throw new TenjinServletException("Invalid value specified for [port]. Only numeric values allowed.") ;
					}
				}
				//Fix for T25IT-131 ends
				if(Utilities.trim(json.getString("name")).equalsIgnoreCase("")){
					logger.debug("name is mandatory");
					msg="name is mandatory";
					throw new TenjinServletException(msg);
				}
					
					if(Utilities.trim(json.getString("hostName")).equalsIgnoreCase("")){
						logger.debug("hostName is mandatory");
						msg="hostName is mandatory";
						throw new TenjinServletException(msg);
					}
				//Fix for T25IT-114
					else if(!Utilities.trim(json.getString("hostName")).equalsIgnoreCase("")) {
						String hostNameWithoutDots = Utilities.trim(json.getString("hostName").replace(".", ""));
						hostNameWithoutDots = Utilities.trim(hostNameWithoutDots).replace("_", "");
						hostNameWithoutDots = Utilities.trim(hostNameWithoutDots).replace(" ", "");
						
						if(hostNameWithoutDots.length() < 1) {
							logger.error("Host Name {} is invalid", json.getString("hostName"));
							throw new TenjinServletException("The value for [hostName] is invalid. Please verify your input and try again.");
						}
					}
				//Fix for T25IT-114 ends
		} catch (JSONException e) {
			
			//Fix for T25IT-125
			/*throw new TenjinServletException(e.getMessage());*/
			throw new TenjinServletException("An internal error occurred. Please contact Tenjin Support.");
			//Fix for T25IT-125 ends
			//logger.error("Error ", e);
		}
		return false;
		
	}
	
	/*Changed by leelaprasad for the requirement of Defect fix#TEN-98,TEN-99,TEN-100 on 17-12-2016 ends*/
	
	//Fix for T25IT-125
	public boolean validateUpdateDetails(JSONObject json) throws TenjinServletException {
		String clientName = "";
		String host = "";
		String port = "";
		
		try {
			clientName = json.getString("name");
		} catch (JSONException e) {
			logger.debug("name is not passed");
		}
		
		try {
			host = json.getString("hostName");
		} catch (JSONException e) {
			logger.debug("hostName is not passed");
		}
		
		try {
			port = json.getString("port");
		} catch (JSONException e) {
			logger.debug("port is not passed");
		}
		
		
		if(Utilities.trim(port).length() > 0) {
			try {
				Integer.parseInt(port);
			}catch(NumberFormatException e) {
				throw new TenjinServletException("Invalid value specified for [port]. Only numeric values allowed.");
			}
		}
		
		if(Utilities.trim(host).length() > 0) {
			String hostNameWithoutDots = Utilities.trim(host.replace(".", ""));
			hostNameWithoutDots = Utilities.trim(hostNameWithoutDots).replace("_", "");
			hostNameWithoutDots = Utilities.trim(hostNameWithoutDots).replace(" ", "");
			
			if(hostNameWithoutDots.length() < 1) {
				logger.error("Host Name {} is invalid", host);
				throw new TenjinServletException("The value for [hostName] is invalid. Please verify your input and try again.");
			}
		}
		
		if(clientName.length() > 15) {
			throw new TenjinServletException("The value for [name] can only be upto a maximum of 15 characters.");
		}
		
		if(host.length() > 15) {
			throw new TenjinServletException("The value for [hostName] can only be upto a maximum of 15 characters.");
		}
		
		return true;
		
	}
	//Fix for T25IT-125 ends
	
	//Fix for T25IT-119
	public RegisteredClient validateAndUpdateClient(RegisteredClient newClient, String oldClientName) throws TenjinServletException, DatabaseException {
		
		ClientHelper helper = new ClientHelper();
		
		RegisteredClient oClient = helper.hydrateClient(oldClientName);
		if(oClient == null) {
			throw new TenjinServletException("Client [" + oldClientName + "] does not exist.");
		}
		oClient.merge(newClient);
		if(!Utilities.trim(oClient.getName()).equalsIgnoreCase(oldClientName)) {
			if(newClient.getName().length() > 15) {
				throw new TenjinServletException("The value for [name] can only be upto a maximum of 15 characters.");
			}
			if(helper.hydrateClient(Utilities.trim(newClient.getName())) != null) {
				throw new TenjinServletException("Client [" + newClient.getName() + "] already exists.");
			}
		}
		
		String host = oClient.getHostName();
		if(Utilities.trim(host).length() > 0) {
			String hostNameWithoutDots = Utilities.trim(host.replace(".", ""));
			hostNameWithoutDots = Utilities.trim(hostNameWithoutDots).replace("_", "");
			hostNameWithoutDots = Utilities.trim(hostNameWithoutDots).replace(" ", "");
			
			if(hostNameWithoutDots.length() < 1) {
				logger.error("Host Name {} is invalid", host);
				throw new TenjinServletException("The value for [hostName] is invalid. Please verify your input and try again.");
			}
		}
		if(host.length() > 15) {
			throw new TenjinServletException("The value for [hostName] can only be upto a maximum of 15 characters.");
		}
		
		String port = oClient.getPort();
		if(Utilities.trim(port).length() > 0) {
			try {
				Integer.parseInt(port);
			}catch(NumberFormatException e) {
				throw new TenjinServletException("Invalid value specified for [port]. Only numeric values allowed.");
			}
		}
		
		
		helper.updateClient(oClient, oldClientName);
		
		return oClient;
	}
}
