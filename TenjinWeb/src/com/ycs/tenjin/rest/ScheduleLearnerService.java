

/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ScheduleLearnerService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 13-07-2017		    Gangadhar Badagi		newly added
 * 18-07-2017		    Gangadhar Badagi		added for Learn API
 * 20-07-2017		    padmavathi 		        For Defect 299 
 * 21-07-2017		    Gangadhar Badagi		TENJINCG-306
 * 21-07-2017		    Gangadhar Badagi		Changed for getting previous value
 * 21-07-2017		    Gangadhar Badagi		TENJINCG-308
 * 10-Aug-2017          Gangadhar Badagi        T25IT-90
 * 17-Aug-2017          Gangadhar Badagi        T25IT-93
 * 12-04-2018			Preeti					TENJINCG-104
 * 02-01-2019			Prem					For validating adapter license
 */


package com.ycs.tenjin.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.constants.BrowserType;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.SchedulerHelper;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.run.LearnerGateway;
import com.ycs.tenjin.scheduler.SchMapping;
import com.ycs.tenjin.scheduler.Scheduler;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

@Path("/learner")
public class ScheduleLearnerService {
	
	private static final Logger logger = LoggerFactory.getLogger(ScheduleLearnerService.class);
	@Context
	ContainerRequestContext requestContext;

	@Context
	SecurityContext securityContext;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/schedules")
	public Response getSchedulesForLearn()
	{
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
			ArrayList<Scheduler> schedulerHelper;
			try {
				/*Added by Preeti for TENJINCG-104 starts*/
				if(user.getRoles().equalsIgnoreCase("tester"))
				{
					return RestUtils.buildResponse(Status.BAD_REQUEST,"User with role tester can not get schedule task list for learning", null);
				}
				/*Added by Preeti for TENJINCG-104 ends*/
				String action="Learn";
				/*Changed by Gangadhar Badagi for TENJINCG-308 starts*/
				schedulerHelper = new SchedulerHelper().hydrateAllSchedules(action,0);
				/*Changed by Gangadhar Badagi for TENJINCG-308 ends*/
				logger.info("{} schedules found in all", schedulerHelper.size());
				Response response = RestUtils.buildResponse(Status.OK, "", schedulerHelper);
				return response;
			} catch (DatabaseException e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/schedules/{id}")
	public Response getSchedulesForLearn(@PathParam("id") int id)
	{
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
			Scheduler scheduler;
			try {
				/*Added by Preeti for TENJINCG-104 starts*/
				if(user.getRoles().equalsIgnoreCase("tester"))
				{
					return RestUtils.buildResponse(Status.BAD_REQUEST,"User with role tester can not get schedule task list for learning", null);
				}
				/*Added by Preeti for TENJINCG-104 ends*/
				scheduler = new SchedulerHelper().hydrateSchedule(String.valueOf(id));
				/*Changed by Gangadhar Badagi for TENJINCG-306 starts*/
				if(scheduler==null || scheduler.getAction()==null || (!scheduler.getAction().equals("Learn") && !scheduler.getAction().equals("LearnAPI"))){
					/*Changed by Gangadhar Badagi for TENJINCG-306 ends*/
					return RestUtils.buildResponse(Status.NOT_FOUND, "Scheduler id '"+id+"' does not exist ", null);
				}
				Response response = RestUtils.buildResponse(Status.OK, "", scheduler);
				return response;
			} catch (DatabaseException e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			}
	}
	
	@Path("/schedules")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response scheduleTask(String jsonString)
	{
		Scheduler scheduler=new Gson().fromJson(jsonString, Scheduler.class);
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		
		/*Added by Gangadhar Badagi for Learn API starts*/
		TenjinSession tjnSession=new TenjinSession();
		tjnSession.setUser(user);
		/*Added by Gangadhar Badagi for Learn API ends*/
		List<String> listOne =new ArrayList<String>() ;
		List<String> listTwo =new ArrayList<String>();
		
		scheduler.setCreated_by(user.getId());
		Calendar calendar1 = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String currentDate = dateFormat.format(calendar1.getTime());
		scheduler.setCreated_on(currentDate);
	
		int tsetId=0;
				try{
					/*Added by Preeti for TENJINCG-104 starts*/
					if(user.getRoles().equalsIgnoreCase("tester"))
					{
						return RestUtils.buildResponse(Status.BAD_REQUEST,"User with role tester can not schedule task for learning", null);
					}
					/*Added by Preeti for TENJINCG-104 ends*/
						new ScheduleValidator().validateMandatoryFields(scheduler);
						
						SchedulerHelper helper = new SchedulerHelper();
						scheduler.setProjectId(0);
						int appId=0;
						int checkSch_task=0;
						
					/*Added by Gangadhar Badagi for Learn API starts*/
						if(scheduler.getAction().equalsIgnoreCase("Learn")){
						/*Added by Gangadhar Badagi for Learn API ends*/
						RegisteredClient client=new ClientHelper().hydrateClient(scheduler.getReg_client());
						if(client==null)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "Client '"+scheduler.getReg_client()+"' does not exist", null);
						if(!scheduler.getAutLoginTYpe().equals("Maker") && !scheduler.getAutLoginTYpe().equals("Checker"))
							return RestUtils.buildResponse(Status.BAD_REQUEST, "autLoginType ["+scheduler.getAutLoginTYpe()+"] not found. Enter 'Maker' or 'Checker'", null);
						if(Utilities.trim(scheduler.getBrowserType()).equalsIgnoreCase("")){
							scheduler.setBrowserType("APPDEFAULT");
						}
						if(!scheduler.getBrowserType().equalsIgnoreCase("APPDEFAULT") && !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.CHROME) && !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.FIREFOX)&& !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.IE))
							return RestUtils.buildResponse(Status.BAD_REQUEST, "browserType ["+scheduler.getBrowserType()+"] not found. Enter 'APPDEFAULT' or '"+BrowserType.CHROME+"' or '"+BrowserType.FIREFOX+"' or '"+BrowserType.IE+"'", null);
						}
						/*Added by Gangadhar Badagi for Learn API starts*/
						if(!scheduler.getAction().equalsIgnoreCase("Learn") && !scheduler.getAction().equalsIgnoreCase("LearnAPI"))
							return RestUtils.buildResponse(Status.BAD_REQUEST, "action ["+scheduler.getAction()+"] not found. Enter 'Learn' or 'LearnAPI'", null);
						if(scheduler.getAction().equalsIgnoreCase("Learn")){
						scheduler.setAction(scheduler.getAction().substring(0,1).toUpperCase()+scheduler.getAction().substring(1));
						}
						/*Added by Gangadhar Badagi for Learn API starts*/
						if(scheduler.getAction().equalsIgnoreCase("LearnAPI")){
							if(Utilities.trim(scheduler.getApiLearnType()).equalsIgnoreCase("")){
								scheduler.setApiLearnType("URL");
							}	
							if(!scheduler.getApiLearnType().equalsIgnoreCase("URL") && !scheduler.getApiLearnType().equalsIgnoreCase("REQUEST_XML"))
								return RestUtils.buildResponse(Status.BAD_REQUEST, "apiLearnType ["+scheduler.getApiLearnType()+"] not found. Enter 'URL' or 'REQUEST_XML' ", null);
							String firstLetterCap=scheduler.getAction().substring(0,1).toUpperCase()+scheduler.getAction().substring(1,5);
							String action=firstLetterCap+scheduler.getAction().substring(5,8).toUpperCase();
						   scheduler.setAction(action);
						   /*Changed by Gangadhar Badagi for T25IT-90 starts*/
						   scheduler.setReg_client("NA");
						   /*Changed by Gangadhar Badagi for T25IT-90 ends*/
						}
						/*Added by Gangadhar Badagi for Learn API ends*/
						
						
						Aut aut=new AutHelper().hydrateAut(scheduler.getAppName());
						if(aut==null)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "App Name '"+scheduler.getAppName()+"' does not exist", null);
							appId=aut.getId();
							
						if(scheduler.getAction().equalsIgnoreCase("Learn"))	{
						for (String functionCode : scheduler.getFunctions()) {
							listOne.add(functionCode);
							
						}
						for (ModuleBean functionCode : aut.getFunctions()) {
							listTwo.add(functionCode.getModuleCode());
						}
						listOne.removeAll(listTwo);
						if(listOne.size()>0)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "Functions '"+listOne+"'not found", null);
						}
						/*Added by Gangadhar Badagi for Learn API starts*/
						if(scheduler.getAction().equalsIgnoreCase("LearnAPI"))	{
							ApiHelper apiHelper=new ApiHelper();
							Api apiCode=apiHelper.hydrateApi(appId,scheduler.getApiCode());
							 List<ApiOperation> opreations = apiHelper.fetchApiOperations(String.valueOf(appId), scheduler.getApiCode());
							if(apiCode==null){
								return RestUtils.buildResponse(Status.BAD_REQUEST, "Api Code '"+scheduler.getApiCode()+"'not found", null);
							}
							for (String functionCode : scheduler.getFunctions()) {
								listOne.add(functionCode);
							}
							for (ApiOperation api : opreations) {
								listTwo.add(api.getName());
							}
							listOne.removeAll(listTwo);
							if(listOne.size()>0)
								return RestUtils.buildResponse(Status.BAD_REQUEST, "Functions '"+listOne+"'not found", null);
							}
						/*Added by Gangadhar Badagi for Learn API ends*/
						checkSch_task=helper.checkSch_Task(scheduler,tsetId);
						
						Date crntDate = null;
						Date restDate = null;
						try {
							crntDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
							        .parse(currentDate);
							restDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
							        .parse(scheduler.getSch_date());
						} catch (ParseException e) {
							return RestUtils.buildResponse(Status.NOT_FOUND, "Please enter sch_date ["+scheduler.getSch_date()+"] in 'dd-MMM-yyyy' format(ex:"+currentDate+") and there should not be any space in between", null);
						}
						
						if(restDate.compareTo(crntDate) < 0)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "sch_date ["+scheduler.getSch_date()+"]should not be previous date", null);
						if(new ScheduleValidator().timeCheck(calendar1.getTime(), scheduler.getSch_time())){
							return RestUtils.buildResponse(Status.BAD_REQUEST, "sch_time ["+scheduler.getSch_time()+"] should not be previous time", null);
						}
						if(checkSch_task==0){
							if(scheduler.getAction().equalsIgnoreCase("Learn"))
							{
						LearnerGateway gateway = new LearnerGateway(appId, scheduler.getFunctions(), user.getId());
						gateway.setAutLoginType(scheduler.getAutLoginTYpe());
						gateway.setBrowserSelection(scheduler.getBrowserType());
						gateway.setRegClientName(scheduler.getReg_client());
						gateway.performAllValidations();
						
						gateway.createRun();
						scheduler.setRun_id(gateway.getTestRun().getId());
						}
							/*Added by Gangadhar Badagi for Learn API starts*/
						if(scheduler.getAction().equalsIgnoreCase("Learnapi")){
								List<ApiOperation> operationsToLearn = new ArrayList<ApiOperation>();
								for(String op:scheduler.getFunctions()) {
									ApiOperation operation = new ApiOperation();
									operation.setName(op);
									operationsToLearn.add(operation);
								}
								LearnerGateway gateway = new LearnerGateway(appId, scheduler.getApiCode(), operationsToLearn, tjnSession.getUser().getId());
								gateway.performAllValidationsForApiLearning();
								logger.info("Setting learn type to {}", scheduler.getApiLearnType());
								gateway.setApiLearnType(scheduler.getApiLearnType());
								logger.info("Initializing Learner...");
								gateway.createRun();
								scheduler.setRun_id(gateway.getTestRun().getId());
								/*Removed by Gangadhar Badagi for T25IT-90 starts*/
								/*scheduler.setReg_client("-1");*/
								/*Removed by Gangadhar Badagi for T25IT-90 ends*/
							}
						/*Added by Gangadhar Badagi for Learn API ends*/
						scheduler.setSchRecur("N");
						scheduler.setStatus("Scheduled");
						scheduler = new SchedulerHelper().persistSchedule(scheduler);
						logger.info("{} creating schedule", scheduler);
						Response response = RestUtils.buildResponse(Status.OK, "Task is scheduled successfully",scheduler.getSchedule_Id());
						
						return response;
				}
						else{
							return RestUtils.buildResponse(Status.FOUND, "Schedule Already exists",null);
						}
				}
				catch(TenjinServletException e)
				{
					return RestUtils.buildResponse(Status.FORBIDDEN,e.getMessage(), null);
					}
			catch (DatabaseException e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			}
				
   
	}
	
	@Path("/schedules/{taskid}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateSchedule(@PathParam("taskid")int sch_id,String jsonString)
	{
		Scheduler scheduler=new Gson().fromJson(jsonString, Scheduler.class);
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		/*Added by Gangadhar Badagi for Learn API starts*/
		TenjinSession tjnSession=new TenjinSession();
		tjnSession.setUser(user);
		/*Added by Gangadhar Badagi for Learn API ends*/
		List<String> listOne =new ArrayList<String>() ;
		List<String> listTwo =new ArrayList<String>();
		
		scheduler.setCreated_by(user.getId());
		Calendar calendar1 = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String currentDate = dateFormat.format(calendar1.getTime());
		scheduler.setCreated_on(currentDate);
		
		int tsetId=0;
		int appId=0;
		
				try{
					/*Added by Preeti for TENJINCG-104 starts*/
					if(user.getRoles().equalsIgnoreCase("tester"))
					{
						return RestUtils.buildResponse(Status.BAD_REQUEST,"User with role tester can not schedule task for learning", null);
					}
					/*Added by Preeti for TENJINCG-104 ends*/
					SchedulerHelper helper = new SchedulerHelper();
					Scheduler sch=helper.hydrateSchedule(String.valueOf(sch_id));
					if(sch==null || sch.getAction()==null || (!sch.getAction().equals("Learn") && !sch.getAction().equals("LearnAPI")))
						return RestUtils.buildResponse(Status.NOT_FOUND, "Scheduler id '"+sch_id+"' does not exist ", null);
					ArrayList<SchMapping> scheduleList = new ArrayList<SchMapping>();
                
                   List<String> functionList=new ArrayList<String>();
                 
					scheduleList =helper.hydrateScheduleMap1(sch);
					if(!Utilities.trim(scheduler.getAction()).equalsIgnoreCase("") || scheduler.getAction()=="")
					{
						return RestUtils.buildResponse(Status.BAD_REQUEST, "Can't update the action", null);
					}
					if(scheduler.getAction()==null){
						scheduler.setAction(sch.getAction());
					}
					if(scheduler.getAction().equalsIgnoreCase("Learn")){
					if(Utilities.trim(scheduler.getBrowserType()).equalsIgnoreCase(""))
					{
						scheduler.setBrowserType(sch.getBrowserType());
					}
					}
					/*Added by Gangadhar Badagi for Learn API starts*/
					if(scheduler.getAction().equalsIgnoreCase("Learnapi")){
						if(Utilities.trim(scheduler.getApiCode()).equalsIgnoreCase("")){
						scheduler.setApiCode(sch.getApiCode());
						}
						if(Utilities.trim(scheduler.getApiLearnType()).equalsIgnoreCase("")){
							scheduler.setApiLearnType(sch.getApiLearnType());
							}
						/*Changed by Gangadhar Badagi for T25IT-90 starts*/
						   scheduler.setReg_client("NA");
						   /*Changed by Gangadhar Badagi for T25IT-90 ends*/
					}
					/*Added by Gangadhar Badagi for Learn API ends*/
					if(scheduleList != null && scheduleList.size() > 0) {
						if(scheduler.getFunctions()==null || scheduler.getFunctions().isEmpty()){
							
						for (SchMapping schMapping : scheduleList) {
							if(scheduler.getAction().equalsIgnoreCase("Learn")){
							functionList.add(schMapping.getFunc_Code());
						}
							/*Added by Gangadhar Badagi for Learn API starts*/
							if(scheduler.getAction().equalsIgnoreCase("Learnapi")){
								functionList.add(schMapping.getApiOperation());
							}
							/*Added by Gangadhar Badagi for Learn API ends*/
						scheduler.setFunctions(functionList);
						}
						if(scheduler.getAppName()==null || scheduler.getAppName()==""){
						 appId = Integer.parseInt(scheduleList.get(0).getApp_id());
						 /*changed by Gangadhar Badagi for getting the previous value starts*/
						 Aut app=new AutHelper().hydrateAut(appId);
						 scheduler.setAppName(app.getName());
						 /*changed by Gangadhar Badagi for getting the previous value ends*/
						}
						/*changed by Gangadhar Badagi for getting the previous value starts*/
						if(scheduler.getAction().equalsIgnoreCase("Learnapi")){
						if(scheduler.getApiCode()==null || scheduler.getApiCode()==""){
							scheduler.setApiCode(scheduleList.get(0).getFunc_Code());
						}
						}
						/*changed by Gangadhar Badagi for getting the previous value ends*/
					}}
					scheduler=new ScheduleValidator().fieldsValidation(scheduler, sch_id);
					scheduler.setProjectId(0);
					int checkSch_task=0;
					
					if(scheduler.getAction().equalsIgnoreCase("Learn")){
					RegisteredClient client=new ClientHelper().hydrateClient(scheduler.getReg_client());
					if(client==null)
						return RestUtils.buildResponse(Status.NOT_FOUND, "Client '"+scheduler.getReg_client()+"' does not exist", null);
					if(!scheduler.getBrowserType().equalsIgnoreCase("APPDEFAULT") && !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.CHROME) && !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.FIREFOX)&& !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.IE))
						return RestUtils.buildResponse(Status.NOT_FOUND, "browserType ["+scheduler.getBrowserType()+"] not found. Enter 'APPDEFAULT' or '"+BrowserType.CHROME+"' or '"+BrowserType.FIREFOX+"' or '"+BrowserType.IE+"'", null);
				   if(!scheduler.getAutLoginTYpe().equals("Maker") && !scheduler.getAutLoginTYpe().equals("Checker"))
							return RestUtils.buildResponse(Status.NOT_FOUND, "autLoginType ["+scheduler.getAutLoginTYpe()+"] not found. Enter 'Maker' or 'Checker'", null);
					}
					/*Added by Gangadhar Badagi for Learn API starts*/
					if(scheduler.getAction().equalsIgnoreCase("Learnapi")){
						if(!scheduler.getApiLearnType().equalsIgnoreCase("URL") && !scheduler.getApiLearnType().equalsIgnoreCase("REQUEST_XML"))
							return RestUtils.buildResponse(Status.BAD_REQUEST, "apiLearnType ["+scheduler.getApiLearnType()+"] not found. Enter 'URL' or 'REQUEST_XML' ", null);
					}
					/*Added by Gangadhar Badagi for Learn API ends*/
					Aut aut=new AutHelper().hydrateAut(scheduler.getAppName());
					if(aut==null)
						return RestUtils.buildResponse(Status.NOT_FOUND, "App Name '"+scheduler.getAppName()+"' does not exist", null);
					appId=aut.getId();
					if(scheduler.getAction().equalsIgnoreCase("Learn")){
							for (String functionCode : scheduler.getFunctions()) {
								listOne.add(functionCode);
							}
							for (ModuleBean functionCode : aut.getFunctions()) {
								listTwo.add(functionCode.getModuleCode());
							}
				     	listOne.removeAll(listTwo);
							if(listOne.size()>0)
								return RestUtils.buildResponse(Status.NOT_FOUND, "Functions '"+listOne+"'not found", null);
				     }
					/*Added by Gangadhar Badagi for Learn API starts*/
					if(scheduler.getAction().equalsIgnoreCase("LearnAPI"))	{
						ApiHelper apiHelper=new ApiHelper();
						Api apiCode=apiHelper.hydrateApi(appId,scheduler.getApiCode());
						 List<ApiOperation> opreations = apiHelper.fetchApiOperations(String.valueOf(appId), scheduler.getApiCode());
						if(apiCode==null){
							return RestUtils.buildResponse(Status.BAD_REQUEST, "Api Code '"+scheduler.getApiCode()+"'not found", null);
						}
							for (String functionCode : scheduler.getFunctions()) {
								listOne.add(functionCode);
							}
							for (ApiOperation api : opreations) {
								listTwo.add(api.getName());
							}
							listOne.removeAll(listTwo);
							if(listOne.size()>0)
								return RestUtils.buildResponse(Status.BAD_REQUEST, "Functions '"+listOne+"'not found", null);
						}
					/*Added by Gangadhar Badagi for Learn API ends*/
					checkSch_task=helper.checkSch_Task(scheduler,tsetId);
					
						
						if(scheduler.getStatus()=="" || !Utilities.trim(scheduler.getStatus()).equalsIgnoreCase("")){
							return RestUtils.buildResponse(Status.NOT_FOUND, "You can't update schedule status manually ", null);
						}	
						Date crntDate = null;
						Date restDate = null;
						try {
							crntDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
							        .parse(currentDate);
							restDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
							        .parse(scheduler.getSch_date());
						} catch (ParseException e) {
							return RestUtils.buildResponse(Status.NOT_FOUND, "Please enter sch_date ["+scheduler.getSch_date()+"] in 'dd-MMM-yyyy' format(ex:"+currentDate+") and there should not be any space in between", null);
						}
						
						if(restDate.compareTo(crntDate) < 0)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "sch_date ["+scheduler.getSch_date()+"]should not be previous date", null);
						if(new ScheduleValidator().timeCheck(calendar1.getTime(), scheduler.getSch_time())){
							return RestUtils.buildResponse(Status.BAD_REQUEST, "sch_time ["+scheduler.getSch_time()+"] should not be previous time", null);
						}
						if(checkSch_task==0){
						if(scheduler.getAction().equalsIgnoreCase("Learn")){
						LearnerGateway gateway = new LearnerGateway(appId, scheduler.getFunctions(), user.getId());
						gateway.setAutLoginType(scheduler.getAutLoginTYpe());
						gateway.setBrowserSelection(scheduler.getBrowserType());
						gateway.performAllValidations();
						
						gateway.createRun();
						scheduler.setRun_id(gateway.getTestRun().getId());
						
						scheduler.setSchedule_Id(sch_id);
						}
						/*Added by Gangadhar Badagi for Learn API starts*/
						if(scheduler.getAction().equalsIgnoreCase("Learnapi")){
							List<ApiOperation> operationsToLearn = new ArrayList<ApiOperation>();
							for(String op:scheduler.getFunctions()) {
								ApiOperation operation = new ApiOperation();
								operation.setName(op);
								operationsToLearn.add(operation);
							}
							LearnerGateway gateway = new LearnerGateway(appId, scheduler.getApiCode(), operationsToLearn, tjnSession.getUser().getId());
							gateway.performAllValidationsForApiLearning();
							logger.info("Setting learn type to {}", scheduler.getApiLearnType());
							gateway.setApiLearnType(scheduler.getApiLearnType());
							logger.info("Initializing Learner...");
							gateway.createRun();
							scheduler.setRun_id(gateway.getTestRun().getId());
							scheduler.setReg_client(sch.getReg_client());
							scheduler.setSchedule_Id(sch_id);
						}
						/*Added by Gangadhar Badagi for Learn API ends*/
						scheduler.setStatus("Scheduled");
						scheduler.setSchRecur("N");
						 new SchedulerHelper().updateSchedule(scheduler);
						 /*Added by Gangadhar BAdagi for T25IT-93 starts*/
						 Scheduler updatedScheduler=helper.hydrateSchedule(String.valueOf(scheduler.getSchedule_Id()));
						 /*Added by Gangadhar BAdagi for T25IT-93 ends*/
						return RestUtils.buildResponse(Status.OK, "Schedule Updated Successfully",updatedScheduler);
						}
				else{
					return RestUtils.buildResponse(Status.FOUND, "Schedule Already exists",null);
				}
				}
				
				catch(TenjinServletException e)
				{
					return RestUtils.buildResponse(Status.FORBIDDEN,e.getMessage(), null);
					}
			catch (DatabaseException   e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			} 
					}
	@Path("/schedules/{taskid}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response cancelTask(@PathParam("taskid")int taskId)
	{
		Scheduler sch;
		try {
			sch = new SchedulerHelper().hydrateSchedule(String.valueOf(taskId));
			if(sch==null || sch.getAction()==null || !sch.getAction().equals("Learn"))
				return RestUtils.buildResponse(Status.NOT_FOUND, "Scheduler id '"+taskId+"' does not exist ", null);
			if(!sch.getStatus().equalsIgnoreCase("Scheduled"))
				return RestUtils.buildResponse(Status.NOT_FOUND, "We can't cancel the task because status is '"+sch.getStatus()+"'", null);
			sch=new SchedulerHelper().hydrateSchedule(String.valueOf(taskId));
			sch.setStatus("Cancelled");
			new SchedulerHelper().updateSchedule(sch);
			return RestUtils.buildResponse(Status.OK, "Schedule Cancelled Successfully",null);
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
	}
}
