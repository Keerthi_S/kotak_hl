/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AutValidator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 22-nov-2016		prafful tiwari			new file added
 * 16-Dec-2016           Abhilash K N              Req Defect Fix #TEN-98,99,100.
* 18-Aug-2017			Manish						T25IT-166
  22-Sep-2017			sameer gupta			For new adapter spec
* 20-11-2017			Padmavathi		        TENJINCG-482

 */
package com.ycs.tenjin.rest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.util.Constants;

public class AutValidator {
	private static final Logger logger = LoggerFactory.getLogger(ProjectHelper.class);

	public boolean autAvailability(String id){
		Connection conn=null;
		try {
			 conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			Statement st = conn.createStatement();
			ResultSet rs=st.executeQuery("SELECT app_id FROM MASAPPLICATION");
			while(rs.next()){
				if(rs.getString(1).equals(id))
					return true;
			}
			
			
		} catch (DatabaseException e) {
			
			logger.error("exception occured while fetching aut");
		}
		catch(SQLException e){
			logger.error("exception occured while fetching aut");
		}
		finally{
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("Error ", e);
				}
			}
		}
		
	return false;	
	}
	
}
