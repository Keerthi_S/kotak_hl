/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 26-08-2017			Roshni					
 * 15-Nov-2017			Manish					TENJINCG-440		
 * 15-Nov-2017			Manish					TENJINCG-442		
 * 16-Nov-2017			Manish					TENJINCG-455(changed responses-passed project object instead of string)				
 * 17-Nov-2017			Manish					TENJINCG-469	
 * 23-Nov-2017          Leelaprasad             update Project Response status
 * 23-11-2017           Padmavathi              for TENJINCG-528
 * 18-01-2018           Padmavathi              for TENJINCG-576
 * 19-01-2018           Padmavathi              for TENJINCG-576
 * 02-05-2018			Preeti					TENJINCG-656
 * 03-05-2018			Preeti					TENJINCG-656
 *
 */
package com.ycs.tenjin.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DomainHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.db.ProjectHelperNew;
import com.ycs.tenjin.db.UserHelper;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.ProjectMailHandler;
import com.ycs.tenjin.project.Domain;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.ProjectMail;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;



@Path("/domains")
public class ProjectService {

	private static final Logger logger = LoggerFactory
			.getLogger(ProjectService.class);

	@Context
	ContainerRequestContext requestContext;

	@Context
	SecurityContext securityContext;

	

	@Path("/{dName}/projects")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createProject(@PathParam("dName") String dName, String jsonString) {

		logger.info("Request to create the project");

		try {
			User loggedInUser = (User) requestContext.getProperty(RestContextProperties.TJN_USER);
			if (loggedInUser.getRoles().equalsIgnoreCase("Tester")) {
				return RestUtils.buildResponse(Status.UNAUTHORIZED, loggedInUser.getRoles() + " cannot create project.",
						null);
			}
			Domain domain = new DomainHelper().fetchDomain(dName);
			if (domain == null) {
				return RestUtils.buildResponse(Status.NOT_FOUND, "Domain '" + dName + "' does not exist", null);
			}
			if (jsonString == null) {
				return RestUtils.buildResponse(Status.BAD_REQUEST,
						"Request data is empty. Please review your request and try again.", null);
			}
			/*modified by shruthi for TCGST-56 and TCGST-57 starts*/
			Project prj=null;
			try {
			 prj = new Gson().fromJson(jsonString, Project.class);
			}
			catch(Exception e)
			{
				  return RestUtils.buildResponse(Status.NOT_FOUND, "Date should be in formate "+ "YYYY-MM-DD", null);
			}
			/*modified by shruthi for TCGST-56 and TCGST-57 ends*/
			prj.setDomain(dName);
			new ProjectValidator().validateProjectAndDomain(prj);
			/*Changed by Padmavathi for TENJINCG-528 starts*/
			
			if (new ProjectValidator().projectAvailability(Utilities.trim(prj.getName()), dName)) {
				return RestUtils.buildResponse(Status.FOUND, "Project already exist ", null);
			}
			prj.setName(Utilities.trim(prj.getName()));
			/*Changed by Padmavathi for TENJINCG-528 ends*/
			User user = (User) requestContext.getProperty(RestContextProperties.TJN_USER);

			prj.setOwner(user.getId().trim());
			prj.setCreatedDate(new Timestamp(new Date().getTime()));
			
			/*added by shruthi for TCGST-56 and TCGST-57 starts*/
			if(prj.getStartDate()==null)
				return RestUtils.buildResponse(Status.NOT_FOUND, "Start Date is mandatory", null);
			if(prj.getEndDate()==null)
				return RestUtils.buildResponse(Status.NOT_FOUND, "End Date mandatory", null);
		
			if(prj.getStartDate().after(prj.getEndDate()))
				return RestUtils.buildResponse(Status.NOT_FOUND, "End Date should be greater than Start Date", null);
			/*added by shruthi for TCGST-56 and TCGST-57 ends*/
		
			
			if (prj.getType() == null)
				return RestUtils.buildResponse(Status.NOT_FOUND, "Type is mandatory", null);
			if (Utilities.trim(prj.getType()).equalsIgnoreCase("")
					|| !prj.getType().equals("Public") && !prj.getType().equals("Private")) {
				return RestUtils.buildResponse(Status.NOT_FOUND, "Type '" + prj.getType() + "' is not found", null);

			} else {
				/*Modified by Padmavathi for Regression Pack starts*/
				new ProjectHelperNew().persistProject(prj);	 
				/*Modified by Padmavathi for Regression Pack ends*/
				Project exmProject=new ProjectHelper().hydrateProject(prj.getDomain(), prj.getName());
				/*return RestUtils.buildResponse(Status.CREATED, "Project created Successfully", null);*/
               return RestUtils.buildResponse(Status.CREATED, "Project created Successfully", exmProject);

			}

		}

		catch (DatabaseException e) {
			
			logger.error("Error ", e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		} catch (TenjinServletException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
	}

	@Path("/{dName}/projects/{pName}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProject(@PathParam("dName") String dName, @PathParam("pName") String pName,
			String jsonString) {
		logger.info("Request to update the project");
		logger.info("Input JSON --> {}", pName);

		try {
			Domain domain = new DomainHelper().fetchDomain(dName);
			ProjectMail prjMail = new ProjectMail();
			if (domain == null) {
				return RestUtils.buildResponse(Status.NOT_FOUND, "Domain '" + dName + "' does not exist", null);
			}
			if (jsonString == null) {
				return RestUtils.buildResponse(Status.BAD_REQUEST,
						"Request data is empty. Please review your request and try again.", null);
			}
			Project prj = new Gson().fromJson(jsonString, Project.class);
			int id = new ProjectHelper().getProjectId(pName, dName);

			if (id == 0) {
				return RestUtils.buildResponse(Status.NOT_FOUND, "Project does not exist ", null);
			}
			prj.setId(id);

			boolean exist = new ProjectHelper().checkProjectExist(dName, prj.getName(), prj.getId());
			if (exist == true)
				return RestUtils.buildResponse(Status.CONFLICT, "Project Name is already exists ", null);
			if (prj.getScreenshotoption() != null) {
				if (Integer.parseInt(prj.getScreenshotoption()) > 3) {
					return RestUtils.buildResponse(Status.NOT_FOUND, "Screenshot option does not exist ", null);
				}
			}
			if (prj.getType() != null) {
				if (!prj.getType().equals("Private") && !prj.getType().equals("Public"))
					return RestUtils.buildResponse(Status.NOT_FOUND, "Project Type does not exist ", null);
			}
			if (prj.getOwner() != null) {
				return RestUtils.buildResponse(Status.CONFLICT, "Project Owner name cannot be changed ", null);
			}
			if (prj.getDomain() != null)
				return RestUtils.buildResponse(Status.CONFLICT, "Project domain cannot be changed ", null);
			
			/*Added by Preeti for TENJINCG-656 starts*/
			prjMail = new ProjectMailHandler().hydrateProjectMail(id);
			if(prj.getProjectMail() !=null)
			{
				prjMail.setPrjId(id);
				prjMail.setPrjMailNotification(prj.getProjectMail().getPrjMailNotification());
				prjMail.setPrjMailSubject(prj.getProjectMail().getPrjMailSubject());
				prjMail = new ProjectMailHandler().updateProjectForMail(prjMail);
			}
			prj.setProjectMail(prjMail);
			/*Added by Preeti for TENJINCG-656 ends*/
			prj = new ProjectValidator().updateFields(prj);
			prj = new ProjectHelper().updateProject(prj);
			/*Changed by Leelaprasad for the requirement of update Project Response status starts*/
			return RestUtils.buildResponse(Status.OK, "Project updated Successfully", prj);
			/*Changed by Leelaprasad for the requirement of update Project Response status ends*/

		}

		catch (DatabaseException e) {
			
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		} catch (RequestValidationException e) {
			
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null,null);
		}
	}

	@Path("/{dName}/projects/{pName}/users")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addUserToProject(@PathParam("dName") String dName, @PathParam("pName") String pName,
			String jsonString) {

		ArrayList<String> message = new ArrayList<>();

		try {
			User loggedInUser = (User) requestContext.getProperty(RestContextProperties.TJN_USER);

			if (loggedInUser.getRoles().equalsIgnoreCase("Tester")) {
				return RestUtils.buildResponse(Status.UNAUTHORIZED, loggedInUser.getRoles() + " cannot add user", null);
			}
			Domain domain = new DomainHelper().fetchDomain(dName);
			if (domain == null) {
				return RestUtils.buildResponse(Status.NOT_FOUND, "Domain '" + dName + "' does not exist", null);
			}
			int projectId = new ProjectHelper().getProjectId(pName, dName);
			if (projectId == 0)
				return RestUtils.buildResponse(Status.NOT_FOUND, " Project id '" + projectId + "' not found ", null);

			if (jsonString == null) {
				return RestUtils.buildResponse(Status.BAD_REQUEST,
						"Request data is empty. Please review your request and try again.", null);
			}

			ArrayList<User> users = new UserHelper().hydrateUsersNotInProject(projectId);
			if (users != null) {

				ArrayList<User> projectUsers = new UserHelper().hydrateCurrentUsersInProject(projectId);

				User user = new Gson().fromJson(jsonString, User.class);
				if (user.getRoleInCurrentProject() == null)

					return RestUtils.buildResponse(Status.NOT_FOUND, " Role is mandatory ", null);
				else {
					/*Modified by padmavathi for removing user from APIS*/
					if (Utilities.trim(user.getRoleInCurrentProject()).equalsIgnoreCase("") || !(Utilities
							.trim(user.getRoleInCurrentProject()).equalsIgnoreCase("Test Engineer"))
							&& !(Utilities.trim(user.getRoleInCurrentProject()).equalsIgnoreCase("Project Administrator"))) {
						return RestUtils.buildResponse(Status.NOT_FOUND, " Given role is not valid ", null);
					}
					/*Modified by padmavathi for removing user from APIS ends*/
				}
				

				for (int k = 0; k < projectUsers.size(); k++) {
					if (projectUsers.get(k).getId().equalsIgnoreCase(user.getId())) {
						return RestUtils.buildResponse(Status.FOUND,
								" user '" + user.getId() + "' is already mapped to project '"+pName+"'", null);
					}
				}

				for (int k = 0; k < users.size(); k++) {
					if (users.get(k).getId().equals(user.getId())) {
						new ProjectHelper().mapUsersToProject(projectId, user.getId());
						return RestUtils.buildResponse(Status.OK,
								"User '" + user.getId() + "' mapped to project '"+pName+"' successfully", new ProjectHelper().hydrateProject(projectId).getUsers());
					}

				}
					
				return RestUtils.buildResponse(Status.NOT_FOUND,
							"User '" + user.getId() + "' does not exist.", null);

			} else {
				return RestUtils.buildResponse(Status.NOT_FOUND,
						"All existing users are already mapped to project.", null);
			}
			/*changed by manish for bug# TENJINCG-440 on 15-Nov-2017 ends*/

		} catch (DatabaseException e) {
			message.add("Couldn't map user to project.");
		} catch (Exception e) {
			logger.error("JSONException occurred", e);
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
		}
		return RestUtils.buildResponse(Status.OK,
				"An Internal error occured, Please contact Tenjin support", null);
	}	
	

	@Path("/{dName}/projects/{pName}/auts")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addAutToProject(@PathParam("dName") String dName, @PathParam("pName") String pName,
			String jsonString) {
		logger.info("Request to create Aut to the project");
		logger.info("Input JSON --> {}", pName);

		ArrayList<String> message = new ArrayList<>();

		try {
			Domain domain = new DomainHelper().fetchDomain(dName);
			if (domain == null)
				return RestUtils.buildResponse(Status.NOT_FOUND, "Domain '" + dName + "' does not exist", null);
			int id = new ProjectHelper().getProjectId(pName, dName);
			if (id == 0)
				return RestUtils.buildResponse(Status.NOT_FOUND, " Project id '" + id + "' not found ", null);
			if (jsonString == null) {
				return RestUtils.buildResponse(Status.BAD_REQUEST,
						"Request data is empty. Please review your request and try again.", null);
			}
			String userList = "";

			ArrayList<Aut> auts = new ProjectHelper().hydrateUnmappedAuts(id);
			if (auts != null) {
				ArrayList<Aut> autProjects = new ProjectHelper().hydrateProjectAuts(id);
				Aut app = new Gson().fromJson(jsonString, Aut.class);
				int appid = app.getId();
				Aut aut=null;
				try{aut=new AutHelper().hydrateAut(appid);}
				catch(Exception e){}
				if (aut!=null) {
					boolean exists = false;
					for (int k = 0; k < autProjects.size(); k++) {
						if (autProjects.get(k).getId() == appid) {
							exists = true;
							break;
						}
					}

					if (exists == true) {
						message.add("Aut '" + aut.getName() + "' is already mapped");
					} else {

						userList = String.valueOf(appid) + ";";
						new ProjectHelper().mapAutsToProject(id, userList);
						message.add("Aut '" + aut.getName() + "' is mapped to project");
					}

				} else {
					message.add("Aut '" + app.getId() + "' does not exists");
				}
			} else {
				return RestUtils.buildResponse(Status.NOT_FOUND,
						"Project id '" + id + "' does not have auts or already all auts are added", new ProjectHelper().hydrateProject(id).getAuts());
			}
			
			return RestUtils.buildResponse(Status.OK,
					message.toString(), new ProjectHelper().hydrateProject(id).getAuts());

		} catch (Exception e) {
			logger.error("JSONException occurred", e);
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
		}
		
	}

	@Path("/{dName}/projects/{pName}/users")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsersForProject(@PathParam("dName") String dName, @PathParam("pName") String pName) {
		logger.info("Request to get all the users");
		Response response = null;
		try {
			Domain domain = new DomainHelper().fetchDomain(dName);
			if (domain == null)
				return RestUtils.buildResponse(Status.NOT_FOUND, "Domain '" + dName + "' does not exist", null);
			int id = new ProjectHelper().getProjectId(pName, dName);
			if (id == 0)
				return RestUtils.buildResponse(Status.NOT_FOUND, "Project not available ", null);

			response = RestUtils.buildResponse(Status.OK, "Users fetched Successfully ",
					new ProjectHelper().hydrateProjectUsers(id));

		} catch (DatabaseException e) {
			
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
		return response;
	}

	@Path("/{dName}/projects/{pName}/auts")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAutsForProject(@PathParam("dName") String dName, @PathParam("pName") String pName) {
		logger.info("Request to get all applications");
		Response response = null;
		List<Aut> mappedAuts = null;
		try {
			Domain domain = new DomainHelper().fetchDomain(dName);
			if (domain == null)
				return RestUtils.buildResponse(Status.NOT_FOUND, "Domain '" + dName + "' does not exist", null);
			int id = new ProjectHelper().getProjectId(pName, dName);
			if (id == 0)
				return RestUtils.buildResponse(Status.NOT_FOUND, "Project not available ", null);
			
			mappedAuts = new ProjectHelper().hydrateProjectAuts(id);
			if(mappedAuts!=null && mappedAuts.size()>0){
				response = RestUtils.buildResponse(Status.OK, " "+mappedAuts.size()+" Applications are fetched Successfully ",mappedAuts);
			}else{
				response = RestUtils.buildResponse(Status.NOT_FOUND, "No applications are mapped to this project",null);
			}
			

		} catch (DatabaseException e) {
			
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
		return response;
	}

	@Path("/{dName}/projects/{pName}/users/{userId}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeUserProject(@PathParam("dName") String dName, @PathParam("pName") String pName,
			@PathParam("userId") String userId) {
		logger.info("Request to unmap user from project");
		try {
			User loggedInUser = (User) requestContext.getProperty(RestContextProperties.TJN_USER);

			if (loggedInUser.getRoles().equalsIgnoreCase("Tester")) {
				return RestUtils.buildResponse(Status.UNAUTHORIZED, loggedInUser.getRoles() + " cannot unmap user",
						null);
			}
			Domain domain = new DomainHelper().fetchDomain(dName);
			if (domain == null)
				return RestUtils.buildResponse(Status.NOT_FOUND, "Domain '" + dName + "' does not exist", null);

			int id = new ProjectHelper().getProjectId(pName, dName);
			if (id == 0)
				return RestUtils.buildResponse(Status.NOT_FOUND, "Project not available ", null);
			User user = (User) requestContext.getProperty(RestContextProperties.TJN_USER);
			if (!new UserValidator().userAvailability(userId))
				return RestUtils.buildResponse(Status.NOT_FOUND, "User does not exist ",  new ProjectHelper().hydrateProject(id).getUsers());
			boolean check = new UserHelper().CheckUserAssociateToProj(id, userId);

			if (check == false) {
				return RestUtils.buildResponse(Status.CONFLICT, userId + " is not mapped with this project",  new ProjectHelper().hydrateProject(id).getUsers());
			} else {
				if (userId.equalsIgnoreCase(user.getId())) {
					return RestUtils.buildResponse(Status.CONFLICT, userId + "You cannot unmap your self from project.",
							null);
				}
				boolean checkAdmin = new ProjectHelper().checkAdminOfProject(id, userId);
				if (checkAdmin == true) {
					int found = new ProjectHelper().countAdminOfProject(id);
					if (found > 1) {
						new ProjectHelper().unmapUsersFromProject(id, userId);
						return RestUtils.buildResponse(Status.OK, userId + " is unmapped Successfully",  new ProjectHelper().hydrateProject(id).getUsers());
					} else
						return RestUtils.buildResponse(Status.CONFLICT, pName + " needs atleast one administrator",
								null);
				} else {
					new ProjectHelper().unmapUsersFromProject(id, userId);
					return RestUtils.buildResponse(Status.OK, userId + " is unmapped Successfully", new ProjectHelper().hydrateProject(id).getUsers());
				}
			}

		} catch (DatabaseException e) {
			
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
	}

	
	@Path("/{dName}/projects/{pName}/auts/{autId}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeAutProject(@PathParam("dName") String dName, @PathParam("pName") String pName,
			@PathParam("autId") String autId) {
		logger.info("Request to unmap application from project");
		try {
			Domain domain = new DomainHelper().fetchDomain(dName);
			if (domain == null)
				return RestUtils.buildResponse(Status.NOT_FOUND, "Domain '" + dName + "' does not exist", null);

			int id = new ProjectHelper().getProjectId(pName, dName);
			if (id == 0)
				return RestUtils.buildResponse(Status.NOT_FOUND, "Project does not exist ", null);

			String autName = new AutHelper().getAutName(Integer.parseInt(autId));
			if (autName.equals(""))
				return RestUtils.buildResponse(Status.CONFLICT, "Application doesn't exist", null);
			else {
				boolean check = new AutHelper().CheckAutAssociateToProj(Integer.parseInt(autId), id);
				if (check == false) {
					return RestUtils.buildResponse(Status.CONFLICT, "Application is not associated with this project.",
							new ProjectHelper().hydrateProject(id));
				} else {
					new ProjectHelper().unmapAutsFromProject(id, autId);
					return RestUtils.buildResponse(Status.OK, "Application is unmapped Successfully", new ProjectHelper().hydrateProject(id).getAuts());
				}
			}

		} catch (DatabaseException e) {
			

			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "Application is not unmapped", null);
		}
	}
	

	@Path("/{dName}/projects/{pName}/auts/bulk")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addAllAutToProject(@PathParam("dName") String dName, @PathParam("pName") String pName,
			String jsonString) {
		ArrayList<String> message = new ArrayList<>();
		logger.info("Request to add Aut to the project");
		logger.info("Input JSON --> {}", pName);

		try {
			Domain domain = new DomainHelper().fetchDomain(dName);
			if (domain == null) {
				return RestUtils.buildResponse(Status.NOT_FOUND, "Domain '" + dName + "' does not exist", null);
			}
			int id = new ProjectHelper().getProjectId(pName, dName);
			if (id == 0)
				return RestUtils.buildResponse(Status.NOT_FOUND, "Project not available ", null);

			if (jsonString == null) {
				return RestUtils.buildResponse(Status.BAD_REQUEST,
						"Request data is empty. Please review your request and try again.", null);
			}

			JSONArray jsonArray = new JSONArray(jsonString);

			String userList = "";
			ArrayList<Aut> unmappedaut = new ProjectHelper().hydrateUnmappedAuts(id);
			if (unmappedaut != null) {
				ArrayList<Aut> projectAuts = new ProjectHelper().hydrateProjectAuts(id);
				for (int i = 0; i < jsonArray.length(); i++) {
					Aut app = new Gson().fromJson(jsonArray.getString(i), Aut.class);
					int appid = app.getId();
					Aut aut=null;
					try{ aut= new AutHelper().hydrateAut(appid);}
					catch(Exception e){}
					boolean exists = false;
					if (aut != null && !(projectAuts.isEmpty())) {
						for (int k = 0; k < projectAuts.size(); k++) {

							if (projectAuts.get(k).getId() == appid) {
								exists = true;
							}

						}
						if (exists == true) {
							message.add("Aut " + aut.getName() + "' is already mapped to project");
						} else {
							userList = String.valueOf(appid);
							new ProjectHelper().mapAutsToProject(id, userList);
							message.add("Aut '" + aut.getName() + "' is mapped to project");
						}

					} else if (aut != null) {
						userList = String.valueOf(appid);
						new ProjectHelper().mapAutsToProject(id, userList);
						message.add("Aut '" + aut.getName() + "' is mapped to project");
					} else {
						message.add("Aut '" + app.getId() + "' doesn't exist.");
					}
				}
			} else{
				return RestUtils.buildResponse(Status.NOT_FOUND,
						"Either all auts are mapped with this project or there is no aut to map. ", null);
			}
			return RestUtils.buildResponse(Status.OK,message.toString(),new ProjectHelper().hydrateProject(id).getAuts());

		} catch (Exception e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
		}

		
	}

	@Path("/{dName}/projects/{pName}/users/bulk")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addAllUserToProject(@PathParam("dName") String dName, @PathParam("pName") String pName,
			String jsonString) {
		ArrayList<String> messages = new ArrayList<>();
		logger.info("Request to add user to the project");
		logger.info("Input JSON --> {}", pName);

		try {
			User loggedInUser = (User) requestContext.getProperty(RestContextProperties.TJN_USER);

			if (loggedInUser.getRoles().equalsIgnoreCase("Tester")) {
				return RestUtils.buildResponse(Status.UNAUTHORIZED, loggedInUser.getRoles() + " cannot add users",
						null);
			}
			Domain domain = new DomainHelper().fetchDomain(dName);
			if (domain == null) {
				return RestUtils.buildResponse(Status.NOT_FOUND, "Domain '" + dName + "' does not exist", null);
			}
			int projectId = new ProjectHelper().getProjectId(pName, dName);
			if (projectId == 0)
				return RestUtils.buildResponse(Status.NOT_FOUND, "Project not available ", null);

			if (jsonString == null) {
				return RestUtils.buildResponse(Status.BAD_REQUEST,
						"Request data is empty. Please review your request and try again.", null);
			}

			JSONArray jsonArray = new JSONArray(jsonString);
			String userList = "";
			ArrayList<User> unmappedUsers = new UserHelper().hydrateUsersNotInProject(projectId);

			if (unmappedUsers != null) {
				ArrayList<User> projectUsers = new ProjectHelper().hydrateProjectUsers(projectId);
				for (int i = 0; i < jsonArray.length(); i++) {
					User user = new Gson().fromJson(jsonArray.getString(i), User.class);
					boolean exists = false;
					User exmUser = new UserHelper().hydrateUser(user.getId());
					if (exmUser != null) {
						if (user.getRoleInCurrentProject() == null) {
							/*changed by manish for bug# TENJINCG-442 on 15-Nov-2017 starts*/
							/*message.add("'roleInCurrentProject' is mandatory for project user " + user.getId());*/
							return RestUtils.buildResponse(Status.NOT_FOUND, "Role is mandatory for user '"+user.getId()+"'", null);
							/*changed by manish for bug# TENJINCG-442 on 15-Nov-2017 ends*/
						} else {
							if (Utilities.trim(user.getRoleInCurrentProject()).equalsIgnoreCase("")
									|| !(Utilities.trim(user.getRoleInCurrentProject()).equalsIgnoreCase("Tester"))
											&& !(Utilities.trim(user.getRoleInCurrentProject())
													.equalsIgnoreCase("Administrator"))) {
								/*changed by manish for bug# TENJINCG-442 on 15-Nov-2017 starts*/
								/*message.add("Given role for " + user.getId() + " is not valid");*/
								return RestUtils.buildResponse(Status.NOT_FOUND, "Given role for " + user.getId() + " is not valid", null);
								/*changed by manish for bug# TENJINCG-442 on 15-Nov-2017 ends*/

							} else {
								if (!(projectUsers.isEmpty())) {
									for (int k = 0; k < projectUsers.size(); k++) {

										if (projectUsers.get(k).getId().equalsIgnoreCase(user.getId())) {
											exists = true;
										}
									}
									if (exists == true) {
										messages.add("User '" + user.getId() + "' is already mapped to project");
									} else {

										userList = user.getId();
										new ProjectHelper().mapUsersToProject(projectId, userList);
										messages.add("User '" + user.getId() + "' mapped to project");
									}
								}

								else {
									userList = user.getId();
									new ProjectHelper().mapUsersToProject(projectId, userList);
									messages.add("User '" + user.getId() + "' mapped to project");
								}
							}
						}
					}else {
						messages.add("User '" + user.getId() + "' doesn't exist.");
					}
				}
			} else {
				/*changed by manish for bug# TENJINCG-442 on 15-Nov-2017 starts*/
				/*return RestUtils.buildResponse(Status.NOT_FOUND,
						"Either all users are mapped with this project or there is no user to map. ", null);*/
				return RestUtils.buildResponse(Status.NOT_FOUND,
						"Either all users are mapped with this project or there is no user to map.", null);
				/*changed by manish for bug# TENJINCG-442 on 15-Nov-2017 ends*/
			}
			
			return RestUtils.buildResponse(Status.OK,
					messages.toString(), new ProjectHelper().hydrateProject(projectId).getUsers());

		} catch (Exception e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
		}
	}
}
