/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GroupService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 *01-Mar-2018			Pushpalatha				Removed comments and added new method for TENJINCG-603
 *19-06-2018			Preeti					T251IT-79
 */

package com.ycs.tenjin.rest;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.aut.Group;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.GroupHandler;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

@Path("/auts")
public class GroupService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Path("/{id}/groups/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllGroups(@PathParam("id") String appId) {
		
		logger.info("Request to get all Groups");
		Connection conn = null;
		Response response = null;
		boolean flag = false;
		Group group = null;
		ArrayList<Group> groupId = new ArrayList<Group>();
		GroupHandler handler=new GroupHandler();
		
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			Aut aut = handler.hydrateAut(Integer.parseInt(appId));
			if(aut==null){
				 return RestUtils.buildResponse(Status.NOT_FOUND, "Aut does not exist.", null);
			}
			
			ArrayList<Group> currentGroup = new AutHelper().hydrateCurrentGroups(conn);
			
			for (int i = 0; i < currentGroup.size(); i++) {
				if (appId.equalsIgnoreCase("" + currentGroup.get(i).getAut())) {
					groupId.add(currentGroup.get(i));
					flag = true;
				}
			}
			
			if (flag == true) {
				response = RestUtils.buildResponse(Status.OK,
						"Groups fetched Successfully ", groupId);
			} else {
				response = RestUtils.buildResponse(Status.NOT_FOUND,
						"Groups not available", group);
			}
		} catch (DatabaseException e) {
			logger.error(e.getMessage());
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,e.getMessage(), null);
		}
		return response;
	}

	@Path("/{id}/groups/{groupName}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroup(@PathParam("id") String appId,
			@PathParam("groupName") String groupName) {
		logger.info("Request to get Group");

		

		Response response = null;
		boolean flag = false;
		Group group = null;
		GroupHandler handler=new GroupHandler();
		try {
			
			Aut aut = handler.hydrateAut(Integer.parseInt(appId));
		
			 if(aut==null){
				 return RestUtils.buildResponse(Status.NOT_FOUND, "Aut does not exist.", null);
			 }
			
			 group=handler.hydrateGroup(Integer.parseInt(appId), groupName);
			
			if(groupName.equalsIgnoreCase(group.getGroupName())){
				flag=true;
			}
			if (flag == true) {
				response = RestUtils.buildResponse(Status.OK,
						"Group fetched Successfully ", group);
			} else {
				response = RestUtils.buildResponse(Status.NOT_FOUND,
						"Group not available", group);
			}
		} catch (DatabaseException e) {
			logger.error(e.getMessage());
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,
					e.getMessage(), null);
		} catch(RecordNotFoundException rne){
			return RestUtils.buildResponse(Status.NOT_FOUND,
					"Group not available", group);
			
		}
		return response;
	}

	
	@Path("/{id}/groups")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createGroups(String jsonString, @PathParam("id") String appId) {
		logger.info("Request to create Groups");
		
		Response response = null;
		Group group = new Group();
		GroupHandler handler=new GroupHandler();
		try {
			 group=new Gson().fromJson(jsonString, Group.class);
			 Aut aut = handler.hydrateAut(Integer.parseInt(appId));
			 if(aut==null){
				 return RestUtils.buildResponse(Status.NOT_FOUND, "Aut does not exist.", null);
			 }
			 new GroupValidator().validateMandateFields(group);
			
			 group.setAut(Integer.parseInt(appId));
			 group.setAppName(aut.getName());
			 
			if(new GroupValidator().groupAvailability(appId,group.getGroupName()))
				return RestUtils.buildResponse(Status.FOUND, "Group already exist ", null);
			handler.persistGroup(group);

			response = RestUtils.buildResponse(Status.CREATED,"Group created Successfully ", group);
			
		} catch (DatabaseException e) {
			logger.error(e.getMessage());
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,
					e.getMessage(), null);
		}catch (TenjinServletException e) {
			logger.error(e.getMessage());
			return RestUtils.buildResponse(Status.NOT_FOUND,
					e.getMessage(), null);
		} catch (RequestValidationException e) {
			
			return RestUtils.buildResponse(Status.NOT_FOUND,
					e.getMessage(), null);
		}
		return response;
	}

	

	@Path("/{id}/groups/{groupName}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteGroup(@PathParam("id") int appId,
			@PathParam("groupName") String groupName) {
		logger.info("Request to delete Groups");
		GroupHandler handler=new GroupHandler();
		Response response = null;
		try {
			
			if(!new GroupValidator().groupAvailability(appId+"",groupName))
				return RestUtils.buildResponse(Status.NOT_FOUND, "Group does not exist ", null);

			
			String[] record = new String[1];
			record[0] = String.valueOf(appId) + "|" + groupName;
			handler.deleteGroup(record);
			response = RestUtils.buildResponse(Status.OK,
					"Group deleted Successfully ", null);
		} catch (DatabaseException e) {
			
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,
					e.getMessage(), null);
		} catch (Exception e) {
			
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,
					e.getMessage(), null);
		}
		return response;
	}

	@Path("/{id}/groups/{groupName}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateGroups(@PathParam("id") String appId, String jsonString,
			@PathParam("groupName") String groupName) {
		logger.info("Request to update Groups");
		
		Response response = null;
		Group group = new Group();
		GroupHandler handler=new GroupHandler();
		try {
			group=new Gson().fromJson(jsonString, Group.class);
			
			 Aut aut =handler.hydrateAut(Integer.parseInt(appId));
			 if(aut==null){
				 return RestUtils.buildResponse(Status.NOT_FOUND, "Aut does not exist.", null);
			 }
			
			if(!new GroupValidator().groupAvailability(appId,groupName))
				return RestUtils.buildResponse(Status.NOT_FOUND, "Group does not exist.", null);
			
			if(!Utilities.trim(group.getGroupName()).equalsIgnoreCase("")){
				return RestUtils.buildResponse(Status.BAD_REQUEST, "Cannot update group name.", null);
			}
			
			if(!Utilities.trim(group.getAppName()).equalsIgnoreCase("")){
				return RestUtils.buildResponse(Status.BAD_REQUEST, "Cannot update aut name.", null);
			}
			
			if(group.getAut()!=0 && group.getAut()!=Integer.parseInt(appId)){
				return RestUtils.buildResponse(Status.BAD_REQUEST, "Cannot update aut Id.", null);
			}
			
			group.getAut();
			

			group.setAut(Integer.parseInt(appId));
			group.setGroupName(groupName);
			group.setAppName(aut.getName());
			if(Utilities.trim(group.getGroupDesc()).equalsIgnoreCase("")){
				 String groupDes=new GroupValidator().getGroupDesc(appId, groupName);
				group.setGroupDesc(groupDes);
			}
			
			handler.updateGroup(group);

			response = RestUtils.buildResponse(Status.OK,
					"Group updated Successfully ", group);

		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,
					e.getMessage(), null);
		} 
		/*Added by Preeti for T251IT-79 starts*/
		catch (RequestValidationException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST,
					e.getMessage(), null);
		}
		/*Added by Preeti for T251IT-79 ends*/
		return response;
	}
	
	
	@Path("/{id}/groups/{groupName}/functions")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupFunctions(@PathParam("id") String appId, @PathParam("groupName") String groupName) {
		logger.info("Request to get all functions for a group");
		GroupHandler handler=new GroupHandler();
		List<Module> groupModules = new ArrayList<Module>();
		 Aut aut;
		try {
			aut = handler.hydrateAut(Integer.parseInt(appId));
			 if(aut==null){
				 return RestUtils.buildResponse(Status.NOT_FOUND, "Aut does not exist.", null);
			 }
			 
			 if(!new GroupValidator().groupAvailability(appId,groupName))
					return RestUtils.buildResponse(Status.NOT_FOUND, "Group does not exist.", null);
			 
			 groupModules =handler.hydrateModulesInGroup(Integer.parseInt(appId), groupName);
			 if(groupModules.size()>0)
				 return RestUtils.buildResponse(Status.OK, "Functions for group '"+groupName+"' fetched successfully", groupModules);
			 else
				 return RestUtils.buildResponse(Status.NOT_FOUND, "No functions available for group '"+groupName+"'", null);
			 
		} catch (NumberFormatException e) {
			logger.error(e.getMessage());
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An Internal error occured, Please contact Tenjin support.", null);
		} catch (DatabaseException e) {
			logger.error(e.getMessage());
			return RestUtils.buildResponse(Status.OK, e.getMessage(), null);
		}
		
	}
	
	
	
	@Path("/{id}/groups/{groupName}/functions")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response mapFunctionsToGroup(String jsonString, @PathParam("id") String appId, @PathParam("groupName") String groupName) {
		logger.info("Request to create functions in a group");

		ModuleBean module = null;
		List<String> messages=new ArrayList<String>();
		List<ModuleBean> listRetModules = new ArrayList<ModuleBean>();
		GroupHandler handler=new GroupHandler();
		
		try {
			
			Aut aut = handler.hydrateAut(Integer.parseInt(appId));
			 if(aut==null){
				 return RestUtils.buildResponse(Status.NOT_FOUND, "Aut does not exist.", null);
			 }
			
			if(!new GroupValidator().groupAvailability(appId,groupName))
				return RestUtils.buildResponse(Status.NOT_FOUND, "Group does not exist.", null);
			
			JSONArray jarray=new JSONArray(jsonString);
			for(int i=0;i<jarray.length();i++){
				module = new Gson().fromJson(jarray.getString(i), ModuleBean.class);
				module.setGroupName(groupName);
				ModuleBean existModule = handler.hydrateModule(Integer.parseInt(appId), module.getModuleCode());
				if(existModule==null){
					messages.add("Function '"+module.getModuleCode()+"' does not exist");
				}else{
					ModuleBean tempModule = new ModuleBean();
					tempModule = handler.hydrateModule(Integer.parseInt(appId), module.getModuleCode());
					if(tempModule.getGroupName()!=null && tempModule.getGroupName().equalsIgnoreCase(groupName)){
						messages.add("Function '"+module.getModuleCode()+"' already mapped");
					}else{
						handler.mapSingleFunction(Integer.parseInt(appId), groupName, module.getModuleCode());
						messages.add("Function '"+module.getModuleCode()+"' mapped successfully");
						listRetModules.add(tempModule);
					}
				}
			}
			
		}catch (JSONException e) {
			logger.error(e.getMessage());
		}catch (NumberFormatException e) {
			logger.error(e.getMessage());
		} catch (DatabaseException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return RestUtils.buildResponse(Status.OK, messages.toString(), listRetModules);
	}
	
	@Path("/{id}/groups/{groupName}/apis")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupApis(@PathParam("id") String appId, @PathParam("groupName") String groupName) {
		logger.info("Request to get all functions for a group");
		GroupHandler handler=new GroupHandler();
		List<Api> groupApis = new ArrayList<Api>();
		 Aut aut;
		try {
			aut = handler.hydrateAut(Integer.parseInt(appId));
			 if(aut==null){
				 return RestUtils.buildResponse(Status.NOT_FOUND, "Aut does not exist.", null);
			 }
			 
			 if(!new GroupValidator().groupAvailability(appId,groupName))
					return RestUtils.buildResponse(Status.NOT_FOUND, "Group does not exist.", null);
			 
			 groupApis =handler.hydrateApi(Integer.parseInt(appId), groupName);
			 if(groupApis.size()>0)
				 return RestUtils.buildResponse(Status.OK, "API's for group '"+groupName+"' fetched successfully", groupApis);
			 else
				 return RestUtils.buildResponse(Status.NOT_FOUND, "No API's available for group '"+groupName+"'", null);
			 
		} catch (NumberFormatException e) {
			logger.error(e.getMessage());
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An Internal error occured, Please contact Tenjin support.", null);
		} catch (DatabaseException e) {
			logger.error(e.getMessage());
			return RestUtils.buildResponse(Status.OK, e.getMessage(), null);
		} catch (SQLException e) {
			
			return RestUtils.buildResponse(Status.OK, e.getMessage(), null);
		}
		
	}
	
	@Path("/{id}/groups/{groupName}/apis")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response mapApisToGroup(String jsonString, @PathParam("id") String appId, @PathParam("groupName") String groupName) {
		logger.info("Request to create apis in a group");

		Api api=null;
		List<String> messages=new ArrayList<String>();
		List<Api> listRetApis = new ArrayList<Api>();
		GroupHandler handler=new GroupHandler();
		
		try {
			
			Aut aut = handler.hydrateAut(Integer.parseInt(appId));
			 if(aut==null){
				 return RestUtils.buildResponse(Status.NOT_FOUND, "Aut does not exist.", null);
			 }
			
			if(!new GroupValidator().groupAvailability(appId,groupName))
				return RestUtils.buildResponse(Status.NOT_FOUND, "Group does not exist.", null);
			
			JSONArray jarray=new JSONArray(jsonString);
			for(int i=0;i<jarray.length();i++){
				api = new Gson().fromJson(jarray.getString(i), Api.class);
				api.setGroup(groupName);
				Api existApi = handler.hydrateApi1(Integer.parseInt(appId), api.getCode());
				if(existApi==null){
					messages.add("Api '"+api.getCode()+"' does not exist");
				}else{
					Api tempApi = new Api();
					tempApi = handler.hydrateApi1(Integer.parseInt(appId), api.getCode());
					if(tempApi.getGroup()!=null && tempApi.getGroup().equalsIgnoreCase(groupName)){
						messages.add("Api '"+api.getCode()+"' already mapped");
					}else{
						handler.mapSingleApi(Integer.parseInt(appId), groupName, api.getCode());
						messages.add("Api '"+api.getCode()+"' mapped successfully");
						listRetApis.add(tempApi);
					}
				}
			}
			
		}catch (JSONException e) {
			logger.error(e.getMessage());
		}catch (NumberFormatException e) {
			logger.error(e.getMessage());
		} catch (DatabaseException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return RestUtils.buildResponse(Status.OK, messages.toString(), listRetApis);
	}
	
	
}
