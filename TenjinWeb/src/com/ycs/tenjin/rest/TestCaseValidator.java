/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCaseValidator .java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 10-Dec-2016           Gangadhar Badagi          Newly Added For TJN_243_02 
 * 22-Dec-2016           Gangadhar Badagi          Changed to validate type and priority in test case DEFECT #ten-159
 * 22-Dec-2016           Gangadhar Badagi          Changed to validate mandate fiellds in test case DEFECT #ten-159
 * 09-08-2017            Gangadhar Badagi            T25IT-65
 * 21-Nov-2017		    Gangadhar Badagi		    TENJINCG-471
 */




package com.ycs.tenjin.rest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.FilterCriteria;
import com.ycs.tenjin.db.TestCaseHelper;
import com.ycs.tenjin.db.TestCasePaginator;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class TestCaseValidator {

	private static final Logger logger = LoggerFactory.getLogger(TestCaseValidator.class);

	String jsonString;
	public TestCaseValidator(String jsonString) {
		this.jsonString=jsonString;
	}
	public TestCaseValidator()
	{

	}

	public boolean testCaseAvailability(TestCase testCase) throws DatabaseException
	{
		Connection conn=null;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			TestCaseHelper tHelper=new TestCaseHelper();
			/*Changed by Gangadhar Badagi for T25IT-65 starts*/
			boolean exist=tHelper.doesTestCaseExist(conn, testCase.getProjectId(),String.valueOf(testCase.getTcId().trim()));
			/*boolean exist=tHelper.doesTestCaseExist(conn, testCase.getProjectId(),String.valueOf(testCase.getTcId().trim()));*/
			/*Changed by Gangadhar Badagi for T25IT-65 ends*/
			return exist;
		} catch (DatabaseException e) {
			logger.error("exception occured while fetching testcase");
		}	
		return false;	
	}

	public boolean typeValidator(String type){
		/********Changed by Gangadhar Badagi to validate type starts*********/
		//	if(type.equals(null) ||type.equals("Acceptance") || type.equals("Accessibility") 
		if(type.equals("Acceptance") || type.equals("Accessibility") 
				|| type.equals("Compatibility")|| type.equals("Regression")|| 
				type.equals("Functional")|| type.equals("Smoke")|| 
				type.equals("Usability")|| type.equals("Other")){
			return true;	
		}
		return false;
	}
	/********Changed by Gangadhar Badagi to validate type ends*********/

	public boolean priorityValidator(String priority){
		/********Changed by Gangadhar Badagi to validate priority starts*********/
		//	if(priority.equals(null) || priority.equals("Low") || priority.equals("Medium") || priority.equals("High"))
		if(priority.equals("Low") || priority.equals("Medium") || priority.equals("High"))
		{
			return true;
		} 
		return false;
	}
	/********Changed by Gangadhar Badagi to validate priority ends*********/

	public void deleteAllTestCases(int projectId) throws DatabaseException
	{
		Connection conn=null;
		PreparedStatement pst=null;
		conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		try {
			pst = conn.prepareStatement("DELETE FROM TESTCASES WHERE TC_PRJ_ID = ?");
			pst.setInt(1, projectId);
			pst.execute();
			pst.close();
		} catch (SQLException e) {
		}
		finally{
			try{
				conn.close();
			}catch(Exception e){
			}
		}


	}
	public TestCase fieldsValidation(TestCase testCase,int pId,int recordId) throws DatabaseException{
		if(testCase.getTcType()==null){
			TestCase t=new TestCaseHelper().hydrateTestCase(pId, recordId);
			String type=t.getTcType();
			testCase.setTcType(type);
		}
		if(testCase.getTcPriority()==null){
			TestCase t=new TestCaseHelper().hydrateTestCase(pId, recordId);
			String priority=t.getTcPriority();
			testCase.setTcPriority(priority);
		}
		if(testCase.getTcDesc()==null){
			TestCase t=new TestCaseHelper().hydrateTestCase(pId, recordId);
			String desc=t.getTcDesc();
			testCase.setTcDesc(desc);
		}
		if(testCase.getTcId()==null){
			TestCase t=new TestCaseHelper().hydrateTestCase(pId, recordId);
			String tcId=t.getTcId();
			testCase.setTcId(tcId);
		}
		if(testCase.getTcName()==null){
			TestCase t=new TestCaseHelper().hydrateTestCase(pId, recordId);
			String tcName=t.getTcName();
			testCase.setTcName(tcName);
		}
		return testCase;
	}

	public void validateMandatoryFields(TestCase testCase){

		if(Utilities.trim(testCase.getTcId()).equalsIgnoreCase(""))
		{
			logger.error(" Test Case id [tcId] is mandatory ");
			try {
				if(testCase.getTcId()==null){
					throw new TenjinServletException("Test Case id [tcId] is mandatory");
				}
			} catch (TenjinServletException e) {

			}
		}
		if(Utilities.trim(testCase.getTcName()).equalsIgnoreCase(""))
		{
			logger.error(" Test Case name [tcName] is mandatory ");
			try {
				if(testCase.getTcName()==null){
					throw new TenjinServletException("Test Case name [tcName] is mandatory");
				}
			} catch (TenjinServletException e) {

			}
		}

	}
	/********Added by Gangadhar Badagi to validate mandate fields starts*********/
	public boolean validateMandateFields(TestCase testCase){
		//VAPT null chekc fix
		if(testCase.getTcId()!=null && testCase.getTcId()!=""){
			return true;
		}
		//VAPT null chekc fix
		if(testCase.getTcName()!=null && testCase.getTcName()!=""){
			return true;
		}

		return false;
	}
	/********Changed by Gangadhar Badagi to validate mandate fields ends
	 * @return 
	 * @throws DatabaseException *********/
	/*Added by Gangadhar Badagi for TENJINCG-471 starts*/
	public ArrayList<TestCase> queryTestcase(TestCase testCase,String projectId) throws DatabaseException{
		List<FilterCriteria> criteria = new ArrayList<FilterCriteria>();
		
		if(!Utilities.trim(testCase.getTcId()).equalsIgnoreCase("")){
			if(testCase.getTcId().contains("%")){
				criteria.add(new FilterCriteria("LOWER(TC_ID)", testCase.getTcId().toLowerCase() , FilterCriteria.LIKE));
			}else{
				criteria.add(new FilterCriteria("LOWER(TC_ID)", testCase.getTcId().toLowerCase() , FilterCriteria.MATCH));
			}
		}
		if(!Utilities.trim(testCase.getTcName()).equalsIgnoreCase("")){
			/*if(testCase.getTcName().contains("%")){
				criteria.add(new FilterCriteria(new String[]{"LOWER(TC_NAME)","LOWER(TC_ID)","LOWER(TC_DESC)"}, testCase.getTcName().toLowerCase() , FilterCriteria.PATTERN_MATCHES_ANY_FIELD_VALUE));
			}else{
				criteria.add(new FilterCriteria("LOWER(TC_NAME)", testCase.getTcName().toLowerCase() , FilterCriteria.EQUALS));
			}*/
			criteria.add(new FilterCriteria(new String[]{"LOWER(TC_NAME)","LOWER(TC_ID)","LOWER(TC_DESC)"}, testCase.getTcName().toLowerCase() , FilterCriteria.PATTERN_MATCHES_ANY_FIELD_VALUE));
		}
		if(!Utilities.trim(testCase.getTcType()).equalsIgnoreCase("")){
			criteria.add(new FilterCriteria("TC_TYPE", testCase.getTcType(), FilterCriteria.MATCH));
		}
		if(!Utilities.trim(testCase.getMode()).equalsIgnoreCase("")){
			criteria.add(new FilterCriteria("TC_EXEC_MODE",testCase.getMode(),FilterCriteria.MATCH));
		}
		if(!Utilities.trim(projectId).equalsIgnoreCase("")){
			criteria.add(new FilterCriteria("tc_prj_id", projectId, FilterCriteria.MATCH));
		}
		TestCasePaginator p = new TestCasePaginator(10, criteria);
		p.setTcIdFilter(testCase.getTcId());
		p.setTcNameFilter(testCase.getTcName());
		p.setTcTypeFilter(testCase.getTcType());
		p.setTcModeFilter(testCase.getMode());
		p.setTcProjectId(projectId);
		ArrayList<TestCase> listTestCases=p.queryTestCase();
		return listTestCases;
	}
	/*Added by Gangadhar Badagi for TENJINCG-471 ends*/
}

