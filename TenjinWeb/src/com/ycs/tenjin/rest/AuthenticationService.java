/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AuthenticationService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 11-nov-2016			nagababu				new file added
* 20-Jun-2017			Sriram					TENJINCG-226
* 10-Nov-2017           Padmavathi      	    To check the loggedin user
*/

package com.ycs.tenjin.rest;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.DuplicateUserSessionException;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.UserHelper;
import com.ycs.tenjin.user.User;


@Path("/v1/authentication")
public class AuthenticationService {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthenticationService.class);
	
	@PermitAll
	@POST
	@Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
	public Response authenticate(@FormParam("username") String username, @FormParam("password") String password) {
		
		try{
			/* Modified by Prem for VAPT Passwrod Encryption change starts*/
			//String hashedpassword = Hashing.sha256().hashString(password , StandardCharsets.UTF_8).toString();
			User user = new UserHelper().authenticateRest(username, password);
			/* Modified by Prem for VAPT Passwrod Encryption change Ends*/
			logger.info("User {} authenticated");
			
			/* Changed by Sriram for TENJINCG-226 */
			/*String token = issueToken(username);*/
			String token = issueToken(username, password);
			/* Changed by Sriram for TENJINCG-226 ends*/
			logger.info("Generated Token is {}", token);
			
			return RestUtils.buildResponse(Status.OK, "", token);
		} catch(DatabaseException e){
			return RestUtils.buildResponse(Status.UNAUTHORIZED, e.getMessage(), null);
			
		}
		/*Added by Padmavathi for To check the loggedin user starts*/
		catch (DuplicateUserSessionException e) {
			return RestUtils.buildResponse(Status.UNAUTHORIZED, e.getMessage(), null);
		}
		
		/*Added by Padmavathi for To check the loggedin user ends*/
		
	}
	
	
	
	/* Added by Sriram for TENJINCG-226 */
	private static String issueToken(String username, String password) {
		byte[] encoded = Base64.encodeBase64((username + ":" + password).getBytes());
		return new String(encoded);
	}
	/* Added by Sriram for TENJINCG-226 ends*/
}
