/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GroupValidator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 22-nov-2016			prafful tiwari			new file added
* 17-Dec-2016		    Gangadhar Badagi		Added for validating mandate fields and updating Description for groups
* 19-Aug-2016		    Manish					groups service
*/

package com.ycs.tenjin.rest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.aut.Group;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class GroupValidator {
	private static final Logger logger = LoggerFactory.getLogger(ProjectHelper.class);

	/*added by manish for function group service on 19-Aug-2017 starts*/
	public boolean groupAvailability(String appId,String name){
		Connection conn=null;
		/*modified by paneendra for sql injection starts*/
		PreparedStatement pst = null;
		ResultSet rs=null;
		try {
			 conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			
			
			pst=conn.prepareStatement("select * from TJN_AUT_GROUPS WHERE APP_ID=? AND GROUP_NAME=?");
			pst.setString(1, appId);
			pst.setString(2, name);
			rs=pst.executeQuery();
			/*modified by paneendra for sql injection ends*/
			while(rs.next()){
					return true;
			}
		} catch (DatabaseException e) {
			logger.error("exception occured while group user");
		}
		catch(SQLException e){
			logger.error("exception occured while group user");
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error(e.getMessage());
				}
			}
					}
		
	return false;	
	}
	/*added by manish for function group service on 19-Aug-2017 ends*/
	
	/*Added by Gangadhar Badagi for defect #TEN-98,99,100:starts*/
	public String getGroupDesc(String id,String name){
		Connection conn=null;
		try {
			 conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			Statement st = conn.createStatement();
			ResultSet rs=st.executeQuery("select GROUP_DESC from TJN_AUT_GROUPS");
			while(rs.next()){
				String groupDesc=rs.getString("GROUP_DESC");
				return groupDesc;
			}
			
			
		} catch (DatabaseException e) {
			
			logger.error("exception occured while group user");
		}
		catch(SQLException e){
			logger.error("exception occured while group user");
		}
		finally{
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("Error ", e);
				}
			}
		}
		return null;
	}
	
	/*Added by Gangadhar Badagi for defect #TEN-98,99,100:ends*/
	
	
	/*Added by Gangadhar Badagi for defect #TEN-98,99,100:starts*/
	public void validateMandateFields(Group group) throws TenjinServletException{
		
		if(Utilities.trim(group.getGroupName()).equalsIgnoreCase("")){
			logger.error("Group Name  not found");
			throw new TenjinServletException("Group Name is mandatory");
		}
		
		
		
	}
	/*Added by Gangadhar Badagi for defect #TEN-98,99,100:ends*/
}
