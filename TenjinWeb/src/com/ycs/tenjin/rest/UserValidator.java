/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UserValidator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
*11-nov-2016			prafful tiwari			new file added
*17-Dec-2016          	Leelaprasad             Defect fix#TEN-98,TEN-99,TEN-100
*19-Dec-2016          	Abhilash K N            Defect fix#TEN-108
 08-Aug-2017			Manish					T25IT-20
 08-Aug-2017		  	Pushpalatha			   	Defect T25IT-14	
 08-Aug-2017		  	Pushpalatha				Defect T25IT-38
 23-Aug-2017			Roshni					User role Validation
* 10-Nov-2017			Manish					TENJINCG-428
* 10-Nov-2017			Manish					user enable/disable service
* 27-11-2017            Padmavathi              TENJINCG-535,538
* 29-Jan-2018			Preeti					TENJINCG-507
*/
package com.ycs.tenjin.rest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class UserValidator{
	private static final Logger logger = LoggerFactory.getLogger(ProjectHelper.class);

	String jsonString;
	
	
	public UserValidator(String jsonString){
		this.jsonString=jsonString;
	}
	public UserValidator(){
	}
	
	
	public boolean userAvailability(String id){
		Connection conn=null;
		try {
			 conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			Statement st = conn.createStatement();
			ResultSet rs=st.executeQuery("select user_id from MASUSER");
			while(rs.next()){
				if(rs.getString(1).equals(id))
					return true;
			}
			
			
		} catch (DatabaseException e) {
			
			logger.error("exception occured while fetching user");
		}
		catch(SQLException e){
			logger.error("exception occured while fetching user");
		}
		finally{
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("Error ", e);
				}
			}
		}
		
	return false;	
	}
	/*Changed by leelaprasad for the requirement of Defect fix#TEN-98,TEN-99,TEN-100 on 17-12-2016 starts*/
	public boolean validateCreateDetails(JSONObject json) throws TenjinServletException {
	String msg=null;
		try {
		
				if(Utilities.trim(json.getString("id")).equalsIgnoreCase("")){
					logger.debug("id is mandatory");
					msg="Id is mandatory";
					throw new TenjinServletException(msg);
				}
				
				else if(Utilities.trim(json.getString("firstName")).equalsIgnoreCase("")){
					logger.debug("first name is mandatory");
					msg="first name is mandatory";
					throw new TenjinServletException(msg);
				}
					
					else if(Utilities.trim(json.getString("roles")).equalsIgnoreCase("")){
						logger.debug("role is mandatory");
						msg="role is mandatory";
						throw new TenjinServletException(msg);
					}
			
		} catch (JSONException e) {
			
			throw new TenjinServletException(e.getMessage());
			//logger.error("Error ", e);
		}
		return false;
		
	}
	
	/*Changed by leelaprasad for the requirement of Defect fix#TEN-98,TEN-99,TEN-100 on 17-12-2016 ends*/
	
	
}
