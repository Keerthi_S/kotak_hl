/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AutUserValidator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 11-nov-2016			prafful tiwari			new file added
 *17-08-2017			Manish					T25IT-169
*/


package com.ycs.tenjin.rest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

public class AutUserValidator {
	private static final Logger logger = LoggerFactory.getLogger(ProjectHelper.class);

	public Aut updateAutUser(Aut aut, String id) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst =null;
		try {
			/*modified by paneendra for sql injection starts*/
			
			
			pst=conn.prepareStatement("UPDATE USRAUTCREDENTIALS SET  APP_LOGIN_NAME=?,APP_PASSWORD=?,APP_TXN_PWD=? WHERE USER_ID=? and APP_ID=? and APP_USER_TYPE=?");
			
			
			pst.setString(1, aut.getLoginName());
			pst.setString(2, aut.getPassword());
			pst.setString(3, aut.getTxnPassword());
			pst.setString(4, id);
			pst.setInt(5, aut.getId());
			pst.setString(6, aut.getLoginUserType());
			/*modified by paneendra for sql injection ends*/
			try {
				pst.executeUpdate();
			} catch (Exception e) {
				logger.error("Error ", e);
			}
			return aut;
		} catch (Exception e) {
			logger.error("Error ", e);
			throw new DatabaseException("Could not update record", e);
		} finally {
			DatabaseHelper.close(pst);
			try {
				conn.close();
			} catch (Exception e) {
			}
		}
	}
	
	public void deleteAutUser(String userid) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		/*modified by paneendra for sql injection starts*/
		PreparedStatement pst =null;
		try {
			
			
			pst=conn.prepareStatement("delete from USRAUTCREDENTIALS WHERE USER_ID=?");
			pst.setString(1, userid);
			int i=pst.executeUpdate();
			/*modified by paneendra for sql injection ends*/
			System.out.println(i);
		} catch (Exception e) {
			throw new DatabaseException("Could not delete record", e);
		} finally {
			DatabaseHelper.close(pst);
			try {
				conn.close();
			} catch (Exception e) {
			}
		}
	}

	
	public void deleteAutUnderSpecificUser(String userid,String appid) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		/*modified by paneendra for sql injection starts*/
		PreparedStatement pst =null;
		try {
			
			
			pst=conn.prepareStatement("delete from USRAUTCREDENTIALS WHERE USER_ID=? and APP_ID=?");
			pst.setString(1, userid);
			pst.setString(2, appid);
			int i=pst.executeUpdate();
			/*modified by paneendra for sql injection ends*/
			System.out.println(i);
		} catch (Exception e) {
			throw new DatabaseException("Could not delete record", e);
		} finally {
			DatabaseHelper.close(pst);
			try {
				conn.close();
			} catch (Exception e) {
			}
		}
	}
	/*changed by manish for bug T25IT-169 on 17-Aug-2017 starts*/
	public boolean autuserAvailability(String userId,String appId){
		Connection conn=null;
		try {
			 conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			Statement st = conn.createStatement();
			ResultSet rs=st.executeQuery("select * from USRAUTCREDENTIALS");
			while(rs.next()){
				if(rs.getString("USER_ID").equals(userId)&&rs.getString("APP_ID").equals(appId))
					return true;
			}
			
			/*changed by manish for bug T25IT-169 on 17-Aug-2017 ends*/
		} catch (DatabaseException e) {
			
			logger.error("exception occured while fetching autuser");
		}
		catch(SQLException e){
			logger.error("exception occured while fetching autuser");
		}
		finally{
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("Error ", e);
				}
			}
		}
		
	return false;	
	}
	/*changed by manish for bug T25IT-169 on 17-Aug-2017 starts*/
	public boolean autuserAvailability(String userId, int appId, String type){
		Connection conn=null;
		try {
			 conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement pstmt = conn.prepareStatement("select * from USRAUTCREDENTIALS where USER_ID=? AND APP_ID=? AND APP_USER_TYPE=?");
			pstmt.setString(1, userId);
			pstmt.setString(3, type);
			pstmt.setInt(2, appId);
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()){
					return true;
			}
			
			
		} catch (DatabaseException e) {
			
			logger.error("exception occured while fetching autuser");
		}
		catch(SQLException e){
			logger.error("exception occured while fetching autuser");
		}
		finally{
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("Error ", e);
				}
			}
		}
		
	return false;	
	}
	
	public Aut fieldsValidation(Aut existedAut,Aut enteredAut) throws DatabaseException{
		try {
			if(!Utilities.trim(enteredAut.getLoginName()).equalsIgnoreCase("")){
				existedAut.setLoginName(enteredAut.getLoginName());
			}
			if(!Utilities.trim(enteredAut.getPassword()).equalsIgnoreCase("")){
				/*Modified by paneendra for VAPT FIX starts*/
				/*existedAut.setPassword(new Crypto().encrypt(enteredAut.getPassword()));*/
				existedAut.setPassword(new CryptoUtilities().encrypt(enteredAut.getPassword()));
				/*Modified by paneendra for VAPT FIX ends*/
			}
			if(!Utilities.trim(enteredAut.getLoginUserType()).equalsIgnoreCase("")){
				existedAut.setLoginUserType(enteredAut.getLoginUserType());
			}
			if(!Utilities.trim(enteredAut.getTxnPassword()).equalsIgnoreCase("")){
				existedAut.setTxnPassword(enteredAut.getTxnPassword());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		} 
		
		return existedAut;
	}
	/*changed by manish for bug T25IT-169 on 17-Aug-2017 ends*/
}
