/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ScheduleValidator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 05-07-2017			Gangadhar Badagi		Newly for TENJINCG-263
 * 18-07-2017		    Gangadhar Badagi		added for Learn API
 * 21-07-2017		    Gangadhar Badagi		TENJINCG-292
 */

package com.ycs.tenjin.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.SchedulerHelper;
import com.ycs.tenjin.db.TestCaseHelper;
import com.ycs.tenjin.db.TestSetHelper;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.scheduler.Scheduler;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Utilities;

public class ScheduleValidator {
	
	
	public void validateMandatoryFields(Scheduler scheduler) throws TenjinServletException{

		if(Utilities.trim(scheduler.getTaskName()).equalsIgnoreCase(""))
		{
			throw new TenjinServletException("Scheduler TaskName [taskName] is mandatory");
		}

		if(Utilities.trim(scheduler.getSch_date()).equalsIgnoreCase(""))
		{
			throw new TenjinServletException("Scheduler Date [sch_date] is mandatory");
		}

		if(Utilities.trim(scheduler.getSch_time()).equalsIgnoreCase(""))
		{
			throw new TenjinServletException("Scheduler Time [sch_time] is mandatory");
		}

		if(Utilities.trim(scheduler.getAction()).equalsIgnoreCase(""))
		{
			throw new TenjinServletException("Scheduler Action [action] is mandatory");
			
		}
		if(!scheduler.getAction().trim().equalsIgnoreCase("LearnAPI")){
		if(Utilities.trim(scheduler.getReg_client()).equalsIgnoreCase(""))
		{
			throw new TenjinServletException("Client [reg_client] is mandatory");
		}
		if(!scheduler.getAction().trim().equalsIgnoreCase("Execute")){
		if(Utilities.trim(scheduler.getAutLoginTYpe()).equalsIgnoreCase(""))
		{
			throw new TenjinServletException("Aut User Type [autLoginType] is mandatory");
			
		}
		}
		}
        if(scheduler.getAction().trim().equalsIgnoreCase("LearnAPI")){
         if(Utilities.trim(scheduler.getApiCode()).equalsIgnoreCase(""))
    		{
    			throw new TenjinServletException("APi Code [apiCode] is mandatory");
    		}
		}
		if(!scheduler.getAction().trim().equalsIgnoreCase("Execute")){
		
		if(Utilities.trim(scheduler.getAppName()).equalsIgnoreCase(""))
		{
			throw new TenjinServletException("App Name [appName] is mandatory");
		}
		if(scheduler.getFunctions()==null || scheduler.getFunctions().isEmpty())
		{
			throw new TenjinServletException("Functions [functions]  mandatory");
		}
		}
		if(scheduler.getAction().trim().equalsIgnoreCase("Extract")){
		if(scheduler.getTestDataPath()==null || scheduler.getTestDataPath().isEmpty())
		{
			throw new TenjinServletException("TestDataPath [testDataPath]  mandatory");
		}
		}
		if(scheduler.getAction().trim().equalsIgnoreCase("Execute")){
		if(scheduler.getScreenShotOption()!=0 && scheduler.getScreenShotOption()!=1 && scheduler.getScreenShotOption()!=2 && scheduler.getScreenShotOption()!=3)
		{
			throw new TenjinServletException("ScreenShot Option [screenShotOption] '"+scheduler.getScreenShotOption()+"' not a valid,Please enter 0,1,2 or 3");
			
		}
		
		}
	

	}
	
	
	
	public Scheduler fieldsValidation(Scheduler scheduler,int sch_id) throws DatabaseException, TenjinServletException{
		if(scheduler.getTaskName()==null){
			Scheduler sch=new SchedulerHelper().hydrateSchedule(String.valueOf(sch_id));
			scheduler.setTaskName(sch.getTaskName());
		}
		else if(scheduler.getTaskName().trim().equalsIgnoreCase("")){
			throw new TenjinServletException("Scheduler TaskName [taskname] can't be empty");
		}
		if(scheduler.getSch_date()==null){
			Scheduler sch=new SchedulerHelper().hydrateSchedule(String.valueOf(sch_id));
			scheduler.setSch_date(sch.getSch_date());
		}
		else if(scheduler.getSch_date().trim().equalsIgnoreCase("")){
			throw new TenjinServletException("Scheduler date [sch_date] can't be empty");
		}
		if(scheduler.getSch_time()==null){
			Scheduler sch=new SchedulerHelper().hydrateSchedule(String.valueOf(sch_id));
			scheduler.setSch_time(sch.getSch_time());
		}
		else if(scheduler.getSch_time().trim().equalsIgnoreCase("")){
			throw new TenjinServletException("Scheduler time [sch_time] can't be empty");
		}
		
		if(scheduler.getAction()==null){
			Scheduler sch=new SchedulerHelper().hydrateSchedule(String.valueOf(sch_id));
			scheduler.setAction(sch.getAction());
		}
		else if(scheduler.getAction().trim().equalsIgnoreCase("")){
			throw new TenjinServletException("Scheduler Action [action] can't be empty");
		}
		
	if(!scheduler.getAction().trim().equalsIgnoreCase("LearnAPI")){
		
		if(!scheduler.getAction().trim().equalsIgnoreCase("Execute")){
		if(scheduler.getAutLoginTYpe()==null){
			Scheduler sch=new SchedulerHelper().hydrateSchedule(String.valueOf(sch_id));
			scheduler.setAutLoginTYpe(sch.getAutLoginTYpe());
		}
		else if(scheduler.getAutLoginTYpe().trim().equalsIgnoreCase("")){
			throw new TenjinServletException("Scheduler Aut Login Type [autLoginType] can't be empty");
		}
		}
		if(scheduler.getReg_client()==null){
			Scheduler sch=new SchedulerHelper().hydrateSchedule(String.valueOf(sch_id));
			scheduler.setReg_client(sch.getReg_client());
		}
		else if(scheduler.getReg_client().trim().equalsIgnoreCase("")){
			throw new TenjinServletException("Scheduler Client [reg_client] can't be empty");
		}
	}
	/*Added by Gangadhar Badagi for Learn API starts*/
	if(scheduler.getAction().trim().equalsIgnoreCase("Learnapi")){
		if(scheduler.getApiCode()==null){
			Scheduler sch=new SchedulerHelper().hydrateSchedule(String.valueOf(sch_id));
			scheduler.setApiCode(sch.getApiCode());;
		}
		else if(scheduler.getApiCode().trim().equalsIgnoreCase("")){
			throw new TenjinServletException("Scheduler Api Code [apiCode] can't be empty");
		}
	}
	/*Added by Gangadhar Badagi for Learn API ends*/
	if(scheduler.getAction().trim().equalsIgnoreCase("Execute")){
		if(scheduler.getScreenShotOption()!=0 && scheduler.getScreenShotOption()!=1 && scheduler.getScreenShotOption()!=2 && scheduler.getScreenShotOption()!=3)
		{
			throw new TenjinServletException("ScreenShot Option [screenShotOption] '"+scheduler.getScreenShotOption()+"' not a valid,Please enter 0,1,2 or 3");
			
		}
	}
		
		return scheduler;
	}
	
	
  public boolean timeCheck(Date todayDate,String sch_time){
	  String currentHour=new SimpleDateFormat("HH").format(todayDate);
	  String currentMinute=new SimpleDateFormat("mm").format(todayDate);
	  Date userDate =null;
	  try {
		userDate = new SimpleDateFormat("HH:mm").parse(sch_time);
	} catch (ParseException e) {
	
	}
	  String usertHour=new SimpleDateFormat("HH").format(userDate);
	  String userMinute=new SimpleDateFormat("mm").format(userDate);
	  if(Integer.parseInt(usertHour)<Integer.parseInt(currentHour))
	  {
		  return true;
	  }
	  if(Integer.parseInt(usertHour)==Integer.parseInt(currentHour))
	  {
		  if(Integer.parseInt(userMinute)<Integer.parseInt(currentMinute))
		  {
			  return true;
		  }
	  }
	  
		return false;
  }
  
  /*Changed by Gangadhar Badagi for TENJINCG-292 starts*/
  public int createTestSet(int testCaseRecId,int prjId,String userId) throws DatabaseException{
	
		
		ArrayList<TestCase> tcs =null;
  
			TestSet t = new TestSet();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_hhmmss");
			/*Changed by leelaprasad for TJNUN262-53 starts*/
			/*t.setName("AdHoc_Test" + "_" + sdf.format(new Date()));*/
			t.setName("Test" + "_" + sdf.format(new Date()));
			/*Changed by leelaprasad for TJNUN262-53 ends*/TestCase testCase = new TestCaseHelper().hydrateTestCase(prjId,testCaseRecId);
			t.setMode(testCase.getMode());
			ArrayList<TestCase> tests = new ArrayList<TestCase>();
			tests.add(testCase);
			t.setTests(tests);
			t.setDescription("Auto generated test set");
			t.setType("Functional");
			t.setRecordType("TSA");
			t.setParent(Integer.parseInt("1"));
			t.setPriority("Medium");
			t.setCreatedBy(userId);
			t = new TestSetHelper().persistTestSet(t, prjId);
			
			tcs=t.getTests();
			String tcList = "";
			int counter=1;
			if(tcs != null){
				for(TestCase tc:tcs){
					if(counter < tcs.size()){
						tcList = tcList + tc.getTcRecId() + ",";
					}else{
						tcList = tcList + tc.getTcRecId();
					}
				}
			}
			new TestSetHelper().persistTestSetMap(t.getId(), tcList, prjId, true);
			return t.getId();
			
  }
  /*Changed by Gangadhar Badagi for TENJINCG-292 ends*/
}

  

