/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ModuleService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 11-nov-2016			nagababu				new file added
 * 17-Dec-2016			Gangadhar Badagi		Added to create and update functions
 * 21-Dec-2016			Gangadhar Badagi		Fix#TEN-147
 * 18-08-2017           Padmavathi              T25IT-177
 * 19-08-2017           Sriram                  T25IT-251
 * 26-08-2017           Gangadhar Badagi        T25IT-293
 * 28-08-2017           Gangadhar Badagi        T25IT-333
 * 11-10-2017           Gangadhar Badagi        TENJINCG-356
 * 15-11-2017           Padmavathi              TENJINCG-449
 * 21-Nov-2017          Gangadhar Badagi        TENJINCG-490
 * 23-Nov-2017          Leelaprasad             TENJINCG-521
 * 28-Nov-2017          Manish		            TENJINCG-537
 * 08-Feb-2018          Padmavathi              TENJINCG-601
 */

package com.ycs.tenjin.rest;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.LearningHelper;
import com.ycs.tenjin.db.ModuleHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Utilities;

@Path("/auts")
public class ModuleService {
	
	private static final Logger logger = LoggerFactory.getLogger(ModuleService.class);
	
	//Changed by Gangadhar For Defect TEN-98, TEN-99, TEN-100 strats
	//@Path("/functions/")
	@Path("/{id}/functions/")
	//Changed by Gangadhar For Defect TEN-98, TEN-99, TEN-100 ends
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createFunction(@PathParam("id") int autId,String jsonString){
	/*	Changed by padmavathi for T25IT-174 starts*/
		logger.info("Request to post creating Function");
		logger.info("Input JSON --> {}", jsonString);
		Response response=null;
		ModuleBean module = new Gson().fromJson(jsonString, ModuleBean.class);
		try {
			Aut id=new AutHelper().hydrateAut(autId);
			if(id!=null){
			module.setApplicationName(id.getName());
			new ModuleValidator().validateMandateFlds(module);
			module.setAut(id.getId());
			/*Added by Padmavathi for TENJINCG-601 starts*/
			if(new ModuleHelper().hydrateModule(id.getId(),Utilities.trim(module.getModuleCode()))!=null){
				response = RestUtils.buildResponse(Status.BAD_REQUEST,"Module code '"+module.getModuleCode()+"' already exist.", null);
			}else if(new ApiHelper().hydrateApi(id.getId(),Utilities.trim(module.getModuleCode()))!=null){
				response = RestUtils.buildResponse(Status.CONFLICT,"Module Code '"+ module.getModuleCode()+"' is already being used by API under Application Id '"+ autId+"'.", null);
			}else{
				/*Added by Padmavathi for TENJINCG-601 ends*/
				new ModulesHelper().persistModule(module);
				ModuleBean module1=new ModulesHelper().hydrateModule(id.getId(), module.getModuleCode());
				response=RestUtils.buildResponse(Status.CREATED, "Module Created Successfully", module1);
			}
			}
			else{
				response = RestUtils.buildResponse(Status.NOT_FOUND,"Applicaiton Id'"+autId+"' does not exist", null);
			}
		
		}catch (DatabaseException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,"Module code '"+module.getModuleCode()+"' already exist.", null);
		} catch (SQLException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}
		catch (TenjinServletException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}
		catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		return response;
	}
	
	//View Function (Under AUT)
	@GET
	@Path("/{id}/functions/{funcCode}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewFunction(@PathParam("id") int appId,@PathParam("funcCode") String functionCode){
		logger.info("Request to Get Functions with appId and function Code [{}]", appId,functionCode);

		Response response = null;
		try {
			ModuleBean mBean =null;
			mBean = new ModulesHelper().hydrateModule(appId, functionCode);
			if(mBean !=null)
				response = RestUtils.buildResponse(Status.OK, "Module  "+functionCode +" fetched for  application Id "+appId,mBean);
			else
				response = RestUtils.buildResponse(Status.NOT_FOUND, "Module does not exist for the application Id "+appId, null);
		} catch (DatabaseException e) {
			
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		} catch (SQLException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		return response;
	}
	//View all Functions (Under AUT)
	@GET
	@Path("/{id}/functions/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewAllFunctionsAUT(@PathParam("id") int appId){
		logger.info("Request to Get all function Codes [{}]", appId);

		Response response = null;
		try {
			ArrayList<ModuleBean> mBean = new ModulesHelper().hydrateModules(appId);
		System.out.println();
			if(mBean.size()!=0)
				response = RestUtils.buildResponse(Status.OK, " Module fetched for all application Id "+appId,mBean);
			else
				response = RestUtils.buildResponse(Status.NOT_FOUND, "Modules does not exist for the application Id "+appId, null);
		} catch (DatabaseException e) {
			
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		} catch (SQLException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		return response;
	}
	
	@GET
	@Path("/functions/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewAllFunctions(){
		logger.info("Request to Get all function Codes [{}]","");

		Response response = null;
		try {
			ArrayList<ModuleBean> mBean = new ModulesHelper().hydrateModulesAcrossAUTS();
			if(mBean !=null)
				response = RestUtils.buildResponse(Status.OK, "Modules fetched for all application",mBean);
			else
				response = RestUtils.buildResponse(Status.NOT_FOUND, "Modules does not exist for any application", null);
		} catch (DatabaseException e) {
			
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		} catch (SQLException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		return response;
	}
	
	
	@Path("/{id}/functions/{funcCode}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON) 
	public Response deleteFunction(@PathParam("id") int appId,@PathParam("funcCode") String funcCode){
	logger.info("Request to dlete a function [{}]", funcCode);

	Response response = null;
	try {
		boolean flag=false;
		ArrayList<ModuleBean> mBean = new ModulesHelper().hydrateModules(appId);
		for(int i=0;i<mBean.size();i++){
			if(mBean.get(i).getModuleCode().equalsIgnoreCase(funcCode)){
				flag=true;
				break;
			}
		}
		
		//For T25IT-251
		if(flag) {
			LearningHelper learningHelper = new LearningHelper();
			/*Changed by Gangadhar Badagi for T25IT-293 starts*/
			/*if(learningHelper.isFunctionLearningInProgress(appId, funcCode)) */
			/*Changed by Gangadhar Badagi for T25IT-293 ends*/
			if(!learningHelper.isFunctionLearningInProgress(appId, funcCode)) {
				flag = true;
			}else {
				flag = false;
				response = RestUtils.buildResponse(Status.CONFLICT, "Cannot delete this function as it is either currently being learnt or is queued for learning.", null);
			}
		}
		//For T25IT-251 ends
		
		if(flag==true){
			AutHelper aut=new AutHelper();
			aut.clearModule(Integer.toString(appId), funcCode);
			/*Changed by Leelaprasad for TENJINCG-491 starts
			return RestUtils.buildResponse(Status.OK, "Module deleted Successfully", null);
			/*Changed by Leelaprasad for TENJINCG-491 starts*/
		}
		else{
			//For T25IT-251
			if(response == null)
				response = RestUtils.buildResponse(Status.NOT_FOUND, "Modules does not exist for the application "+appId, null);
			//For T25IT-251 ends
		}
	} catch (DatabaseException e) {
		
		response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
	} catch (SQLException e) {
		response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
	}catch(Exception e){
		response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
	}
	return response;
  }
	
	
	@Path("/{id}/functions/{funcCode}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateFunction(String jsonString, @PathParam("id") int appId,@PathParam("funcCode") String funcCode){
		logger.info("Request to update function under AUT");
		logger.info("Input JSON --> {}", jsonString);
		logger.info("ID --> {}", appId);
		Response response=null;
		try{
			ModuleBean module = new Gson().fromJson(jsonString, ModuleBean.class);
		//	new ModuleValidator().fieldsNotToUpdate(module);
		//	if(module.getApplicationName().trim()==null && module.getApplicationName().trim()=="")
			
			
			/**********Added by Gangadhar Badagi to validate mandate fields starts*****/
			/**********Changed by Padmavathi for T25IT-183 starts*****/
			/*if(Utilities.trim(module.getApplicationName()).equalsIgnoreCase(""))*/
				/**********Added by Gangadhar Badagi to validate mandate fields ends*****/
			if(Utilities.trim(module.getModuleCode()).length()>0){
				logger.error("We can't update Application");
				throw new TenjinServletException("We can't update function code [moduleCode] for this Application Id "+appId);
			}
			new ModuleValidator().validateSize(module);
			/**********Changed by Padmavathi for T25IT-183 ends*****/
			boolean flag=false;
			/**********Changed by Padmavathi for T25IT-183 starts*****/
			Aut id=new AutHelper().hydrateAut(appId);
			if(id!=null){
		   /**********Changed by Padmavathi for T25IT-183 ends*****/
			ArrayList<ModuleBean> mBean = new ModulesHelper().hydrateModules(appId);
			
			for(int i=0;i<mBean.size();i++){
				if(mBean.get(i).getModuleCode().equalsIgnoreCase(funcCode)){
					flag=true;
					break;
				}
			}
			
			if(flag==true){
				module.setModuleCode(funcCode);
				ModulesHelper mh=new ModulesHelper();
				/*Changed by Gangadhar Badagi for T25IT-333 starts */
				Module m=new ModuleValidator().validateFileds(appId,module);
				mh.updateModule(m);
				ModuleBean mb=mh.hydrateModule(appId, funcCode);
				/*Changed by Gangadhar Badagi for TENJINCG-490 starts*/
				/*return RestUtils.buildResponse(Status.CREATED, "Module updated Successfully", mb);*/
				return RestUtils.buildResponse(Status.OK, "Module updated Successfully", mb);
				/*Changed by Gangadhar Badagi for TENJINCG-490 ends*/
			}
			/**********Changed by Padmavathi for T25IT-183 starts*****/
			else{
				response = RestUtils.buildResponse(Status.NOT_FOUND, "Module does not exist for Function code [moduleCode] "+funcCode, null);	
			}
			/**********Changed by Padmavathi for T25IT-183 ends*****/
			}
			else{
				response = RestUtils.buildResponse(Status.NOT_FOUND, "Module does not exist for the application Id "+appId, null);	
			}
			
		} catch (DatabaseException e) {
			
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}catch (TenjinServletException e) {
			response = RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(), null);
		}catch (SQLException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
	return response;
	}
	
	/*Added by Gangadhar Badagi for TENJINCG-356 starts */
	@Path("/{id}/functions")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateBulkFunctions(@PathParam("id") int autId,String jsonString) {
		logger.info("Request to update bulk Functions");
		logger.info("Input JSON --> {}", jsonString);
		Response response=null;
		List<ModuleBean> bulkResponse=new ArrayList<ModuleBean>();
		/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 starts*/
		/*JSONArray jarray=new JSONArray();*/
		List<String> messages = new ArrayList<String>();
		/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 ends*/
		ModuleBean module=null;
		
		try {
			Aut id1=new AutHelper().hydrateAut(autId);
			if(id1!=null){
			JSONArray jsonArray=new JSONArray(jsonString);
			for (int i = 0; i < jsonArray.length(); i++) {
            module = new Gson().fromJson(jsonArray.get(i).toString(), ModuleBean.class);
			if(Utilities.trim(module.getModuleCode()).equalsIgnoreCase("")){
				/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 starts*/
				/*jarray.put("Function code [moduleCode] is needed to update.");*/
				messages.add("Function code [moduleCode] is needed to update.");
				/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 ends*/
			}
			else{
				/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 starts*/
				/*JSONArray mandateJsonArray=new ModuleValidator().validateMandateFldsBulk(module);*/
				List<String> mandateJsonArray=new ModuleValidator().validateMandateFldsBulk(module);
				if(mandateJsonArray.size()==0){
			List<String> jsonArrayFldsSize=new ModuleValidator().validateBulkFunctionsSize(module);
			/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 ends*/
			if(jsonArrayFldsSize.size()==0){
			boolean flag=false;
			Aut id=new AutHelper().hydrateAut(autId);
			if(id!=null){
			ArrayList<ModuleBean> mBean = new ModulesHelper().hydrateModules(autId);
			for(int j=0;j<mBean.size();j++){
				if(mBean.get(j).getModuleCode().equalsIgnoreCase(module.getModuleCode())){
					flag=true;
					break;
				}
			}
			
			if(flag==true){
				module.setModuleCode(module.getModuleCode());
				ModulesHelper mh=new ModulesHelper();
				Module m=new ModuleValidator().validateFileds(autId,module);
				mh.updateModule(m);
				ModuleBean mb=mh.hydrateModule(autId, module.getModuleCode());
				/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 starts*/
				messages.add(" Module '"+mb.getModuleCode()+"' updated Successfully ");
				/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 ends*/
				bulkResponse.add(mb);
				
			}
			else{
				/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 starts*/
				messages.add("Module does not exist for Function code [moduleCode] "+ module.getModuleCode());
				/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 ends*/
			}
			}
			else{
				/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 starts*/
				messages.add("Module does not exist for the application Id "+autId);
				/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 ends*/
			}
			}else{
				/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 starts*/
				messages.addAll(mandateJsonArray);
				/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 ends*/
			}
				}else{
					/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 starts*/
					messages.addAll(mandateJsonArray);
					/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 ends*/
				}
			
			}
			}
			/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 starts*/
			/*return RestUtils.buildResponse(Status.OK, jarray.toString(), bulkResponse);*/
			return RestUtils.buildResponse(Status.OK, messages.toString(), bulkResponse);
			/*changed by manish for bug# TENJINCG-537 on 28-Nov-2017 ends*/
			}
			else{
				return RestUtils.buildResponse(Status.NOT_FOUND,"AppId '"+autId+"' does not exist.", null);
			}
		} 
		catch (DatabaseException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,"AppId '"+autId+"' does not exist.", null);
		} catch (SQLException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}
		catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		return response;
	}
	
	/*Added by Gangadhar Badagi for TENJINCG-356 ends */
	
	
	@Path("/{id}/functions/search")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchFunction(@PathParam("id") int appId,@QueryParam("key") String key,@QueryParam("keyValue") String value){
		logger.info("Request to post search Function");
		logger.info("Input JSON --> {}", appId);
		Response response=null;
		try {
			ArrayList<Aut> auts=new AutHelper().hydrateAllAut();
			boolean flag=false;
			for(int i=0;i<auts.size();i++){
				if(auts.get(i).getId()==appId){
					flag=true;
					break;
				}
			}
			if(flag==true){
				ArrayList<ModuleBean> mBean = new ModulesHelper().hydrateModules(appId);
				ArrayList<ModuleBean> modules=new ArrayList<ModuleBean>();
				for(int i=0;i<mBean.size();i++){
					if(key.equalsIgnoreCase("moduleCode") && value.equalsIgnoreCase(mBean.get(i).getModuleCode())){
						modules.add(mBean.get(i));
					}
					else if(key.equalsIgnoreCase("moduleName") && value.equalsIgnoreCase(mBean.get(i).getModuleName())){
						modules.add(mBean.get(i));
					}
				}
				if(modules!=null && modules.size()>0){
					response = RestUtils.buildResponse(Status.OK, "",modules);
				}
				else{
					response = RestUtils.buildResponse(Status.NOT_FOUND,"Functions does exists", null);
				}
			}
			else{
				response = RestUtils.buildResponse(Status.NOT_FOUND,"Applicaiton does not exist to to search functions", null);
			}
		} catch (DatabaseException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		} catch (SQLException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		return response;
	}
	/**********Added by Padmavathi for T25IT-177 starts*****/
	@Path("/{id}/functions/bulk")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createFunctionBulk(@PathParam("id") int autId,String jsonString) {
		logger.info("Request to post creating bulk Functions");
		logger.info("Input JSON --> {}", jsonString);
		List<ModuleBean> bulkResponse=new ArrayList<ModuleBean>();
		Response response=null;
		/*JSONArray jarray=new JSONArray();*/
		List<String> messages = new ArrayList<String>();
		ModuleBean module=null;
		
		try {		
			Aut id=new AutHelper().hydrateAut(autId);
			if(id==null){
				return RestUtils.buildResponse(Status.NOT_FOUND,"Applicaiton id '"+autId+"' does not exist.", null);
			}else{
			JSONArray jsonArray=new JSONArray(jsonString);
			for (int i = 0; i < jsonArray.length(); i++) {

				 module = new Gson().fromJson(jsonArray.get(i).toString(), ModuleBean.class);
				ModuleBean moduleBean=new ModulesHelper().hydrateModule(autId, module.getModuleCode());
				if(moduleBean!=null){
					/*jarray.put("Module code '"+module.getModuleCode()+"' already exist .");*/
					messages.add("Module code '"+module.getModuleCode()+"' already exist .");
				}/*Added by Padmavathi for TENJINCG-601 starts*/
				else if(new ApiHelper().hydrateApi(id.getId(),Utilities.trim(module.getModuleCode()))!=null){
					messages.add("Module Code '"+ module.getModuleCode()+"' is already being used by API under Application Id '"+ autId+"'.");
				}
				/*Added by Padmavathi for TENJINCG-601 ends*/
				else{
					 /*JSONArray mandateJsonArray=new ModuleValidator().validateMandateFldsBulk(module);*/
					List<String> mandateJsonArray=new ModuleValidator().validateMandateFldsBulk(module);
					/*if(mandateJsonArray.length()==0){*/
					if(mandateJsonArray.size()==0){
					module.setApplicationName(id.getName());
					/*commented by padmavathi for TENJINCG-449 starts*/
					/*module.setTxnMode("G");*/
					/*commented by padmavathi for TENJINCG-449 ends*/
					module.setAut(id.getId());
					new ModulesHelper().persistModule(module);
						ModuleBean module1=new ModulesHelper().hydrateModule(id.getId(), module.getModuleCode());
						/*jarray.put(" Module created successfully. ");*/
						messages.add("Module '"+module.getModuleCode()+"' created successfully.");
						bulkResponse.add(module1);
					}
					else{
						/*jarray.put(mandateJsonArray);*/
						messages.addAll( mandateJsonArray);
					}
						
				}
				
			}
			/*return RestUtils.buildResponse(Status.OK, jarray.toString(), bulkResponse);*/
			return RestUtils.buildResponse(Status.OK, messages.toString(), bulkResponse);
			}
		} 
		catch (DatabaseException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,"Module code '"+module.getModuleCode()+"' already exist.", null);
		} catch (SQLException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
		}
		catch(Exception e){
			response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
		return response;
	}
	/**********Added by Padmavathi for T25IT-177 ends*****/
}
