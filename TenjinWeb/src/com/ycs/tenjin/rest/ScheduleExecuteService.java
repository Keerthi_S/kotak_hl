/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ScheduleExecuteService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */
/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 14-07-2017		    Padmavathi		        Newly for TENJINCG-263
 * 20-07-2017		    padmavathi 		        For Defect 299 
 * 21-07-2017		    Gangadhar Badagi 		TENJINCG-292 
 * 21-07-2017		    Gangadhar Badagi 		TENJINCG-307
 * 21-07-2017		    Gangadhar Badagi		TENJINCG-308
 * 17-Aug-2017          Gangadhar Badagi        T25IT-93
 * 18-Aug-2017          Gangadhar Badagi        Added to set type
 * 24-04-2020			Ashiki					2.9 Adapter build
 */


package com.ycs.tenjin.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.constants.BrowserType;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.SchedulerHelper;
import com.ycs.tenjin.db.TestCaseHelper;
import com.ycs.tenjin.db.TestSetHelper;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.run.ExecutorGateway;
import com.ycs.tenjin.scheduler.SchMapping;
import com.ycs.tenjin.scheduler.Scheduler;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

@Path("/domains")
public class ScheduleExecuteService {
	

	private static final Logger logger = LoggerFactory.getLogger(ScheduleExecuteService.class);
	@Context
	ContainerRequestContext requestContext;

	@Context
	SecurityContext securityContext;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{dName}/projects/{pName}/schedules")
	public Response getSchedules(@PathParam("dName") String domainName, @PathParam("pName") String projectName)
	{
		String action="Execute";
		/*Changed by Gangadhar Badagi for TENJINCG-308 starts*/
		Project project=(Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		/*Changed by Gangadhar Badagi for TENJINCG-308 ends*/
			ArrayList<Scheduler> schedule=null;
			try {
				/*Changed by Gangadhar Badagi for TENJINCG-308 starts*/
				/*schedule = new SchedulerHelper().hydrateAllSchedules(action);*/
				schedule = new SchedulerHelper().hydrateAllSchedules(action,project.getId());
				/*Changed by Gangadhar Badagi for TENJINCG-308 ends*/
				logger.info("{} schedules found in all", schedule.size());
				Response response = RestUtils.buildResponse(Status.OK, "", schedule);
				return response;
			} catch (DatabaseException e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			}
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{dName}/projects/{pName}/schedules/{id}")
	public Response getSchedules(@PathParam("dName") String domainName, @PathParam("pName") String projectName,@PathParam("id") int id)
	{
		
		Scheduler schedule=null;
		try {
			schedule = new SchedulerHelper().hydrateSchedule(String.valueOf(id));
			if(schedule==null || schedule.getAction()==null || !schedule.getAction().equals("Execute")){
				return RestUtils.buildResponse(Status.BAD_REQUEST, "Scheduler id '"+id+"' does not exist. ", null);
			}
			Response response = RestUtils.buildResponse(Status.OK, "", schedule);
			return response;
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
	
	}
	
	@Path("/{dName}/projects/{pName}/schedules")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response scheduleTask(String jsonString,@PathParam("dName") String domainName, @PathParam("pName") String projectName)
	{
		Scheduler scheduler=new Gson().fromJson(jsonString, Scheduler.class);
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		Project project=(Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		TenjinSession tjnSession = new TenjinSession();
		tjnSession.setProject(project);
		tjnSession.setUser(user);
		scheduler.setCreated_by(user.getId());
		Calendar calendar1 = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String currentDate = dateFormat.format(calendar1.getTime());
		scheduler.setCreated_on(currentDate);
		scheduler.setStatus("Scheduled");
		/*Added by Ashiki for 2.9 Adapter starts*/
		scheduler.setAction("Execute");
		/*Added by Ashiki for 2.9 Adapter end*/
		int tsetId=0;
				try{
					new ScheduleValidator().validateMandatoryFields(scheduler);
						
						SchedulerHelper helper = new SchedulerHelper();
						if(scheduler.getAction().trim().equalsIgnoreCase("Execute")){
							scheduler.setProjectId(project.getId());
							scheduler.setScreenShotOption(scheduler.getScreenShotOption());
							/*Changed by Gangadhar Badagi for TENJINCG-292 starts*/
							if(scheduler.getType().equalsIgnoreCase("TestCase")){
								if(Utilities.trim(scheduler.getTestCaseName()).equalsIgnoreCase(""))
								{
									return RestUtils.buildResponse(Status.BAD_REQUEST, "TestCase Name [testCaseName] is mandatory ", null);
								}
							TestCase testCase=new TestCaseHelper().hydrateTestCaseByName(tjnSession.getProject().getId(), scheduler.getTestCaseName());
							if(testCase==null)
							{
								return RestUtils.buildResponse(Status.BAD_REQUEST, "TestCase Name is '"+scheduler.getTestCaseName()+"' does not exist", null);
							}
							tsetId=new ScheduleValidator().createTestSet(testCase.getTcRecId(), project.getId(), user.getId());
							
							}
							/*Changed by Gangadhar Badagi for TENJINCG-292 ends*/
						}
						String task=scheduler.getAction();
						if(Utilities.trim(scheduler.getBrowserType()).equalsIgnoreCase(""))
						{
							scheduler.setBrowserType("APPDEFAULT");
						}
						
						int checkSch_task=0;
						
						RegisteredClient client=new ClientHelper().hydrateClient(scheduler.getReg_client());
						if(client==null)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "Client '"+scheduler.getReg_client()+"' does not exist", null);
						/*if( !scheduler.getAction().equalsIgnoreCase("Execute"))
							return RestUtils.buildResponse(Status.BAD_REQUEST, "action ["+scheduler.getAction()+"] not found. Enter 'Execute'", null);*/
						if(!scheduler.getBrowserType().trim().equalsIgnoreCase("APPDEFAULT") && !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.CHROME) && !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.FIREFOX)&& !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.IE))
							return RestUtils.buildResponse(Status.BAD_REQUEST, "Browser ["+scheduler.getBrowserType()+"] not found.", null);
						if(!scheduler.getAction().trim().equalsIgnoreCase("Execute"))
							return RestUtils.buildResponse(Status.BAD_REQUEST, "action ["+scheduler.getAction()+"] not found. Enter 'Execute' ", null);
						scheduler.setAction(scheduler.getAction().substring(0,1).toUpperCase()+scheduler.getAction().substring(1));
			        	
						/*Changed by Gangadhar Badagi for TENJINCG-292 starts*/
						/*TestSet testset=new TestSetHelper().hydrateTestSet(scheduler.getTestSetName(), project.getId());
						if(testset==null)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "TestSet Name '"+scheduler.getTestSetName()+"' does not exist", null);
						tsetId=testset.getId();*/
						if(scheduler.getType().equalsIgnoreCase("TestSet")){
			        		if(Utilities.trim(scheduler.getTestSetName()).equalsIgnoreCase(""))
			        		{
			        			throw new TenjinServletException("TestSet Name [testSetName] is mandatory");
			        		}
						TestSet testset=new TestSetHelper().hydrateTestSet(scheduler.getTestSetName(), project.getId());
						if(testset==null)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "TestSet Name '"+scheduler.getTestSetName()+"' does not exist", null);
						tsetId=testset.getId();
			        	}
						/*Changed by Gangadhar Badagi for TENJINCG-292 ends*/
						ArrayList<TestCase> testCases=new TestSetHelper().hydrateMappedTCs(tsetId, project.getId());
						if(testCases.size()==0)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "No  TestCases are mapped to testset '"+tsetId+"'", null);
						
							checkSch_task=helper.checkSch_Task(scheduler,tsetId);
						
						
						Date crntDate = null;
						Date restDate = null;
						try {
							crntDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
							        .parse(currentDate);
							restDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
							        .parse(scheduler.getSch_date());
						} catch (ParseException e) {
							return RestUtils.buildResponse(Status.NOT_FOUND, "Please enter sch_date ["+scheduler.getSch_date()+"]in 'dd-MMM-yyyy' format(ex:"+currentDate+") and there should not be any space in between", null);
						}
						
						if(restDate.compareTo(crntDate) < 0)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "sch_date ["+scheduler.getSch_date()+"]should not be previous date", null);
						/*Modified by Ashiki for 2.9 Adapter starts*/
						else if(restDate.compareTo(crntDate) == 0) {
							boolean timeCheck=new ScheduleValidator().timeCheck(calendar1.getTime(), scheduler.getSch_time());
							if(timeCheck)
								return RestUtils.buildResponse(Status.BAD_REQUEST, "sch_time ["+scheduler.getSch_time()+"] should not be previous time", null);
						}
						
						/*Modified by Ashiki for 2.9 Adapter ends*/
						if(checkSch_task==0){
					 if(task.equalsIgnoreCase("Execute")){
							String oem="";
							try {
								logger.info("Getting Automation Engine configuration");
								oem = TenjinConfiguration.getProperty("OEM_TOOL");
							} catch (TenjinConfigurationException e) {
								logger.error("CONFIG ERROR --> {}", e.getMessage());
								logger.info("OEM will be defaulted to Selenium WebDriver");
							}
							try {
								client = new ClientHelper().hydrateClient(scheduler.getReg_client());
								if(client != null){
									logger.info("Client Info --> Host [{}], Port [{}]", client.getHostName(), client.getPort());
								}
							} catch (Exception e) {
								logger.error("ERROR validating client [{}]", scheduler.getReg_client(), e);
							}
							ExecutorGateway gateway=new ExecutorGateway(tjnSession, tsetId, scheduler.getBrowserType(), client.getHostName(), Integer.parseInt(client.getPort()),oem, 0, scheduler.getScreenShotOption(), "N");
							gateway.performAllValidations();
							gateway.createRun();
							scheduler.setRun_id(gateway.getTaskManifest().getRunId());
						}
						scheduler.setSchRecur("N");
						scheduler = new SchedulerHelper().persistSchedule(scheduler);
						logger.info("{} creating schedule", scheduler);
						Response response = RestUtils.buildResponse(Status.OK, "Task is scheduled successfully",scheduler.getSchedule_Id());
						return response;
				}
						else{
							return RestUtils.buildResponse(Status.FOUND, "Schedule Already exists",null);
						}
				}
				catch(TenjinServletException e)
				{
					return RestUtils.buildResponse(Status.FORBIDDEN,e.getMessage(), null);
					}
			catch (DatabaseException e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			}
			
   
	}

	@Path("/{dName}/projects/{pName}/schedules/{taskid}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateSchedule(@PathParam("taskid")int sch_id,String jsonString,@PathParam("dName") String domainName, @PathParam("pName") String projectName)
	{
		Scheduler scheduler=new Gson().fromJson(jsonString, Scheduler.class);
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		Project project=(Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		TenjinSession tjnSession = new TenjinSession();
		tjnSession.setProject(project);
		tjnSession.setUser(user);
		scheduler.setCreated_by(user.getId());
		Calendar calendar1 = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String currentDate = dateFormat.format(calendar1.getTime());
		scheduler.setCreated_on(currentDate);
		scheduler.setStatus("Scheduled");
		scheduler.setSchRecur("N");
		int tsetId=0;
	
				try{
					SchedulerHelper helper = new SchedulerHelper();
					Scheduler sch=helper.hydrateSchedule(String.valueOf(sch_id));
					ArrayList<SchMapping> scheduleList = new ArrayList<SchMapping>();
                    scheduleList =helper.hydrateScheduleMap1(sch);
					if(!Utilities.trim(scheduler.getAction()).equalsIgnoreCase(""))
					{
						return RestUtils.buildResponse(Status.BAD_REQUEST, "Can't update the action", null);
					}
					
					if(scheduler.getAction()==null){
						scheduler.setAction(sch.getAction());
					}
					if(scheduleList != null && scheduleList.size() > 0) {
						tsetId=scheduleList.get(0).getTestStepId();
						TestSet testset=new TestSetHelper().hydrateTestSet(tsetId, project.getId());
						if(scheduler.getAction().trim().equalsIgnoreCase("Execute") ){
							/*Changed by Gangadhar Badagi for TENJINCG-292 starts*/
							if(Utilities.trim(scheduler.getType()).equalsIgnoreCase("") && (Utilities.trim(scheduler.getTestCaseName()).equalsIgnoreCase("") || Utilities.trim(scheduler.getTestSetName()).equalsIgnoreCase(""))){
								/*Changed by Gangadhar Badagi for TENJINCG-292 ends*/
							if(Utilities.trim(scheduler.getTestSetName()).equalsIgnoreCase("")){
								scheduler.setTestSetName(testset.getName());
								/*Added by Gangadhar Badagi to set type starts*/
								if(testset.getRecordType().equalsIgnoreCase("TSA")){
									scheduler.setType("TestCase");
								}
								else{
									scheduler.setType("TestSet");
								}
								/*Added by Gangadhar Badagi to set type ends*/
							}
							/*Changed by Gangadhar Badagi for TENJINCG-292 starts*/
							}
							
							else{
								/*Changed by Gangadhar Badagi for TENJINCG-307 starts*/
								if(Utilities.trim(scheduler.getType()).equalsIgnoreCase("")){
									/*Changed by Gangadhar Badagi for TENJINCG-307 ends*/
								if(scheduler.getTestCaseName()!=null){
								return RestUtils.buildResponse(Status.BAD_REQUEST, "Can't upadate TestCase Name'"+scheduler.getTestCaseName()+"' without type ", null);
								}
								if(scheduler.getTestSetName()!=null){
									return RestUtils.buildResponse(Status.BAD_REQUEST, "Can't upadate TestSet Name'"+scheduler.getTestSetName()+"' without type ", null);
									}
								}
							}
							/*Changed by Gangadhar Badagi for TENJINCG-292 ends*/
						}
					}
					if(sch==null || sch.getAction()==null || !sch.getAction().equals("Execute"))
						return RestUtils.buildResponse(Status.BAD_REQUEST, "Scheduler id '"+sch_id+"' does not exist ", null);
					scheduler=new ScheduleValidator().fieldsValidation(scheduler, sch_id);
					if(scheduler.getAction().trim().equalsIgnoreCase("Execute")){
						scheduler.setProjectId(project.getId());
						scheduler.setScreenShotOption(scheduler.getScreenShotOption());
					}
					if(Utilities.trim(scheduler.getBrowserType()).equalsIgnoreCase(""))
					{
						scheduler.setBrowserType(sch.getBrowserType());
					}
					String task=scheduler.getAction();
					int checkSch_task=0;
					RegisteredClient client=new ClientHelper().hydrateClient(scheduler.getReg_client());
					if(client==null)
						return RestUtils.buildResponse(Status.BAD_REQUEST, "Client '"+scheduler.getReg_client()+"' does not exist", null);
					if(!scheduler.getAction().equalsIgnoreCase("Execute"))
						return RestUtils.buildResponse(Status.BAD_REQUEST, "action ["+scheduler.getAction()+"] not found. Enter  'Execute'", null);
					if(!scheduler.getBrowserType().equalsIgnoreCase("APPDEFAULT") && !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.CHROME) && !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.FIREFOX)&& !scheduler.getBrowserType().equalsIgnoreCase(BrowserType.IE))
						return RestUtils.buildResponse(Status.BAD_REQUEST, "browserType ["+scheduler.getBrowserType()+"] not found. Enter 'APPDEFAULT' or '"+BrowserType.CHROME+"' or '"+BrowserType.FIREFOX+"' or '"+BrowserType.IE+"'", null);
					
				
					if(scheduler.getAction().trim().equalsIgnoreCase("Execute")){
						/*Changed by Gangadhar Badagi for TENJINCG-292 starts*/
						if(Utilities.trim(scheduler.getType()).equalsIgnoreCase("TestSet")){
							if(Utilities.trim(scheduler.getTestSetName()).equalsIgnoreCase("")){
								return RestUtils.buildResponse(Status.BAD_REQUEST, "TestSet Name [testSetName] can't be empty", null);
							}
							/*Changed by Gangadhar Badagi for TENJINCG-292 ends*/
						TestSet testset=new TestSetHelper().hydrateTestSet(scheduler.getTestSetName(), project.getId());
						if(testset==null)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "TestSet Name '"+scheduler.getTestSetName()+"' does not exist", null);
						tsetId=testset.getId();
						}
						/*Changed by Gangadhar Badagi for TENJINCG-292 starts*/
						if(Utilities.trim(scheduler.getType()).equalsIgnoreCase("TestCase")){
							if(Utilities.trim(scheduler.getTestCaseName()).equalsIgnoreCase("")){
								return RestUtils.buildResponse(Status.BAD_REQUEST, "TestCase Name [testCaseName] can't be empty", null);
							}
							TestCase testCase=new TestCaseHelper().hydrateTestCaseByName(tjnSession.getProject().getId(), scheduler.getTestCaseName());
							if(testCase==null)
								return RestUtils.buildResponse(Status.BAD_REQUEST, "TestCase Name '"+scheduler.getTestCaseName()+"' does not exist", null);
							tsetId=new ScheduleValidator().createTestSet(testCase.getTcRecId(), tjnSession.getProject().getId(), tjnSession.getUser().getId());
							}
						/*Changed by Gangadhar Badagi for TENJINCG-292 ends*/
						
						/*Modified by Ashiki for 2.9 Adapter build starts*/
						String oRegClient=sch.getReg_client();
						if(!scheduler.getSch_date().equals(sch.getSch_date())){
							checkSch_task=helper.checkSch_Task(scheduler,tsetId);
						}else if(!scheduler.getSch_time().equals(sch.getSch_time())){
							checkSch_task=helper.checkSch_Task(scheduler,tsetId);
						}else if(!oRegClient.equalsIgnoreCase(sch.getReg_client())){
							checkSch_task=helper.checkSch_Task(scheduler,tsetId);
						}
						/*Modified by Ashiki for 2.9 Adapter build ends*/
					}
						Date crntDate = null;
						Date restDate = null;
						try {
							crntDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
							        .parse(currentDate);
							restDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
							        .parse(scheduler.getSch_date());
						} catch (ParseException e) {
							return RestUtils.buildResponse(Status.BAD_REQUEST, "Please enter sch_date ["+scheduler.getSch_date()+"]in 'dd-MMM-yyyy' format(ex:"+currentDate+") and there should not be any space in between", null);
						}
						
						/*Modified by Ashiki for 2.9 Adapter build starts*/
						if(restDate.compareTo(crntDate) < 0)
							return RestUtils.buildResponse(Status.BAD_REQUEST, "sch_date ["+scheduler.getSch_date()+"]should not be previous date", null);
						else if(restDate.compareTo(crntDate) == 0) {
							boolean timeCheck=new ScheduleValidator().timeCheck(calendar1.getTime(), scheduler.getSch_time());
							if(timeCheck)
								return RestUtils.buildResponse(Status.BAD_REQUEST, "sch_time ["+scheduler.getSch_time()+"] should not be previous time", null);
						}
						/*Modified by Ashiki for 2.9 Adapter build end*/
						
						/*if(new ScheduleValidator().timeCheck(calendar1.getTime(), scheduler.getSch_time())){
							return RestUtils.buildResponse(Status.BAD_REQUEST, "sch_time ["+scheduler.getSch_time()+"] should not be previous time", null);
						}*/
						if(checkSch_task==0){
						 if(task.equalsIgnoreCase("Execute")){
							String oem="";
							try {
								logger.info("Getting Automation Engine configuration");
								oem = TenjinConfiguration.getProperty("OEM_TOOL");
							} catch (TenjinConfigurationException e) {
								logger.error("CONFIG ERROR --> {}", e.getMessage());
								logger.info("OEM will be defaulted to Selenium WebDriver");
							}
							try {
								client = new ClientHelper().hydrateClient(scheduler.getReg_client());
								if(client != null){
									logger.info("Client Info --> Host [{}], Port [{}]", client.getHostName(), client.getPort());
								}
							} catch (Exception e) {
								logger.error("ERROR validating client [{}]", scheduler.getReg_client(), e);
							}
							ExecutorGateway gateway=new ExecutorGateway(tjnSession, tsetId, scheduler.getBrowserType(), client.getHostName(), Integer.parseInt(client.getPort()),oem, 0, scheduler.getScreenShotOption(), "N");
							gateway.performAllValidations();
							gateway.createRun();
							scheduler.setRun_id(gateway.getTaskManifest().getRunId());
						
						}
						scheduler.setSchedule_Id(sch_id);
						 new SchedulerHelper().updateSchedule(scheduler);
						 /*Added by Gangadhar BAdagi for T25IT-93 starts*/
						 Scheduler updatedScheduler=helper.hydrateSchedule(String.valueOf(scheduler.getSchedule_Id()));
						 /*Added by Gangadhar BAdagi for T25IT-93 ends*/
						return RestUtils.buildResponse(Status.OK, "Schedule Updated Successfully",updatedScheduler);
				}
				else{
					return RestUtils.buildResponse(Status.FOUND, "Schedule Already exists",null);
				}
				}
				
				catch(TenjinServletException e)
				{
					return RestUtils.buildResponse(Status.FORBIDDEN,e.getMessage(), null);
					}
			catch (DatabaseException   e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			}
	}
	
	@Path("/{dName}/projects/{pName}/schedules/{taskid}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response cancelTask(@PathParam("taskid")int taskId,@PathParam("dName") String domainName, @PathParam("pName") String projectName)
	{
		Project project=(Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		Scheduler sch;
		try {
			sch = new SchedulerHelper().hydrateSchedule(String.valueOf(taskId));
			if(sch==null || sch.getAction()==null || !sch.getAction().equals("Execute"))
				return RestUtils.buildResponse(Status.NOT_FOUND, "Scheduler id '"+taskId+"' does not exist ", null);
			if(!sch.getStatus().equalsIgnoreCase("Scheduled"))
				return RestUtils.buildResponse(Status.NOT_FOUND, "We can't cancel the task because status is '"+sch.getStatus()+"'", null);
			sch=new SchedulerHelper().hydrateSchedule(String.valueOf(taskId));
			if(sch.getAction().trim().equalsIgnoreCase("Execute"))
			{
				sch.setProjectId(project.getId());
			}
			sch.setStatus("Cancelled");
			new SchedulerHelper().updateSchedule(sch);
			return RestUtils.buildResponse(Status.OK, "Schedule Cancelled Successfully",null);
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Administrator", null);
		}
	}
}
