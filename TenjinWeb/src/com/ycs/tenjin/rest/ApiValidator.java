/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApiValidator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
17-08-2017			Sriram Sridharan			T25IT-187
17-08-2017			Sriram Sridharan			T25IT-188
17-08-2017			Sriram Sridharan			T25IT-175
22-Sep-2017			sameer gupta			For new adapter spec
21-11-2017			Gangadhar Badagi			TENJINCG-492
21-11-2017			Gangadhar Badagi			TENJINCG-496
23-11-2017			Gangadhar Badagi			TENJINCG-522
*/

package com.ycs.tenjin.rest;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.api.ApiApplicationFactory;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Utilities;

public class ApiValidator {
	
	private static final Logger logger = LoggerFactory.getLogger(ApiValidator.class);
	
	public void validateApiForUpdate(Api api) throws TenjinServletException, BridgeException, DatabaseException, DuplicateRecordException {
		this.validateApi(api, false);
	}
	
	public void validateApiForCreate(Api api) throws TenjinServletException, BridgeException, DatabaseException, DuplicateRecordException {
		this.validateApi(api, true);
	}
	
	/*Added by Gangadhar Badagi for TENJINCG-492 starts*/
	public void validateApiManadateFields(Api api) throws TenjinServletException {
		this.validateApiManadatoryFields(api);
	}
	/*Added by Gangadhar Badagi for TENJINCG-492 ends*/
	public void validateApiOperationForCreate(int appId, String apiCode, ApiOperation operation) throws TenjinServletException, DatabaseException {
		this.validateApiOperation(appId, apiCode, operation, true);
	}
	//For T25IT-188
	public void validateApiOperationForUpdate(int appId, String apiCode, ApiOperation operation, String oldOperationName) throws TenjinServletException, DatabaseException {
		if(!oldOperationName.equalsIgnoreCase(operation.getName())) {
			this.validateApiOperation(appId, apiCode, operation, true);
		}else {
			this.validateApiOperation(appId, apiCode, operation, false);
		}
	}//For T25IT-188 ends
	
	private void validateApi(Api api, boolean duplicateCheck) throws TenjinServletException, BridgeException, DatabaseException, DuplicateRecordException{
		
		logger.debug("entered validateApi(Api api)");
		
		if(api == null) {
			logger.error("API object is null");
			throw new TenjinServletException("Could not validate API Information due to an internal error. Please contact Tenjin Support.");
		}
		
		logger.debug("Getting all availabe API Adapters");
		List<String> availableAdapters = null;
		
		availableAdapters = ApiApplicationFactory.getAvailableAPIAdapters();
	
		
		if(duplicateCheck) {
			logger.debug("Validating duplicates");
			ApiHelper helper = new ApiHelper();
			ModulesHelper mHelper = new ModulesHelper();
			
			try {
				if(helper.hydrateApi(api.getApplicationId(), api.getCode()) != null) {
					logger.error("API with code [{}] already exists under application [{}]", api.getCode(), api.getApplicationId());
					/*Changed by Gangadhar Badagi for TENJINCG-522 starts*/
					throw new DuplicateRecordException("API with code " + api.getCode() + " is already present under this application. Please use a unique API Code.");
					/*Changed by Gangadhar Badagi for TENJINCG-522 ends*/
				}
				
				if(mHelper.hydrateModule(api.getApplicationId(), api.getCode()) != null){
					logger.error("API Cannot be created as a Function with code [{}] already exists under application [{}]", api.getCode(), api.getApplicationId());
					/*Changed by Gangadhar Badagi for TENJINCG-522 starts*/
					throw new DuplicateRecordException("The API Code you specified (" + api.getCode() + ") is already being used by a GUI Function under the same application. Please use a different API Code and try again.");
					/*Changed by Gangadhar Badagi for TENJINCG-522 ends*/
				}
			} catch (SQLException e) {
				
				logger.error("ERROR validating API duplicacy", e);
				throw new TenjinServletException("Could not validate API Information due to an internal error. Please contact Tenjin Support.");
			}
		}
		
		logger.debug("Validating API Type");
		if(!availableAdapters.contains(api.getType())){
			logger.error("API Type {} is not present in the list of available adapters", api.getType());
			throw new TenjinServletException("Invalid value ["+ api.getType() +"] specified for [type].");
		}
		
		logger.debug("Validating URL");
		if(Utilities.trim(api.getUrl()).length() < 1) {
			logger.error("ERROR no URL specified. Entered value is {}", api.getUrl());
			throw new TenjinServletException("Field [url] is mandatory.");
		}
		
		/* For T25IT-175 */
		logger.debug("Validating Name");
		if(Utilities.trim(api.getName()).length() < 1) {
			throw new TenjinServletException("Field [name] is mandatory.");
		}
		/* For T25IT-175 ends*/
		/*Added by Gangadhar Badagi for TENJINCG-496 starts*/
		logger.debug("Validating ApiCode");
		if(Utilities.trim(api.getCode()).length() < 1) {
			throw new TenjinServletException("Field [code] is mandatory.");
		}
		/*Added by Gangadhar Badagi for TENJINCG-496 ends*/
		logger.info("API Validation successful!");
		
	}
	/* Fix for T25IT-187 */
	private void validateApiOperation(int appId, String apiCode, ApiOperation operation, boolean duplicateCheck) throws TenjinServletException, DatabaseException {
		if(operation == null) {
			logger.error("Operation is null");
			throw new TenjinServletException("Request data is empty. Please verify your request and try again.");
		}
		ApiHelper helper = new ApiHelper();
		
		Api api = helper.hydrateApi(appId, apiCode);
		if(api != null) {
			if(api.getType().toLowerCase().startsWith("soap")) {
				throw new TenjinServletException("Cannot add operations to SOAP API.");
			}
		}else {
			throw new TenjinServletException("The API [" + apiCode +  "] does not exist for the specified application.");
		}
		
		if(duplicateCheck) {
			if(helper.hydrateApiOperation(appId, apiCode, operation.getName()) != null) {
				logger.error("An operation with name {} already exists for this API", operation.getName());
				throw new TenjinServletException("An operation with name [" + operation.getName() + "] already exisits for API [" + apiCode + "]");
			}
		}
		
		if(Utilities.trim(operation.getName()).length() < 1) {
			throw new TenjinServletException("Field [name] is mandatory for Operation");
		}
		
		if(Utilities.trim(operation.getMethod()).length() < 1) {
			throw new TenjinServletException("Field [method] is mandatory for Operation");
		}
	}
	/* Fix for T25IT-187 */
	
	/*Added by Gangadhar Badagi for TENJINCG-492 starts*/
	private void validateApiManadatoryFields(Api api) throws TenjinServletException{
		
		if(Utilities.trim(api.getName()).equalsIgnoreCase("")) {
			throw new TenjinServletException("Field [name] is mandatory for api creation.");
		}
		
		if(Utilities.trim(api.getCode()).equalsIgnoreCase("")) {
			throw new TenjinServletException("Field [code] is mandatory for api creation");
		}
		if(Utilities.trim(api.getType()).equalsIgnoreCase("")) {
			throw new TenjinServletException("Field [type] is mandatory for api creation.");
		}
		
		if(Utilities.trim(api.getUrl()).equalsIgnoreCase("")) {
			throw new TenjinServletException("Field [url] is mandatory for api creation");
		}
	}
	/*Added by Gangadhar Badagi for TENJINCG-492 ends*/
}	
