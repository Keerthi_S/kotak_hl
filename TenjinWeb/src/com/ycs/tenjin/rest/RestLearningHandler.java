/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SeleniumTaskHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 23-06-2017		Sriram Sridharan			For TENJINCG-254
 * 19-Aug-2017      Padmavathi                  T25IT-258
22-Sep-2017			sameer gupta			For new adapter spec
02-01-2019			Prem					For validating adapter license
24-06-2019          Padmavathi              for license
 */

package com.ycs.tenjin.rest;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.MaxActiveUsersException;
import com.ycs.tenjin.bridge.BridgeProcessor;
import com.ycs.tenjin.bridge.oem.api.generic.ApiLearnType;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.run.LearnerGateway;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Utilities;

public class RestLearningHandler {
	private static final Logger logger = LoggerFactory.getLogger(RestLearningHandler.class);
	
	private LearnerInput learnerInput;
	private String userId;
	private LearnerGateway gateway;

	public RestLearningHandler(LearnerInput learnerInput, String userId) {
		this.learnerInput = learnerInput;
		this.userId = userId;
	}
	
	public void validateApiLearning() throws RestRequestException, TenjinServletException, MaxActiveUsersException, LicenseInactive {
		logger.info("Validating learner input for API Learning");
		
		int appId = this.performAmbiguityCheck();
		
		List<ApiOperation> operations = new ArrayList<ApiOperation>();
		if(!Utilities.trim(this.learnerInput.getOperations()).equalsIgnoreCase("all")){
			String[] split = this.learnerInput.getOperations().split(",");
			for(String op : split) {
				ApiOperation operation = new ApiOperation();
				operation.setName(op);
				operations.add(operation);
			}
		}else{
			try {
				Api api = new ApiHelper().hydrateApi(appId, this.learnerInput.getApiCode());
				if(api == null) {
					throw new RestRequestException("API [" + this.learnerInput.getApiCode() + "] was not found. Please review your input and try again.");
				}
				operations = api.getOperations();
			} catch (DatabaseException e) {
				
				logger.error("ERROR getting operations for API Code [{}]", this.learnerInput.getApiCode(), e);
			}
		}
		
		
		this.gateway = new LearnerGateway(appId, this.learnerInput.getApiCode(), operations, this.userId);
		
		/* Changed by Sriram for TENJINCG-254 */
		/*this.gateway.setApiLearnType(this.learnerInput.getApiLearnType());*/
		if(Utilities.trim(this.learnerInput.getApiLearnType()).equalsIgnoreCase("MESSAGE")) {
			this.gateway.setApiLearnType(ApiLearnType.REQUEST_XML.toString());
		}else{
			this.gateway.setApiLearnType(this.learnerInput.getApiLearnType());
		}
		/* Changed by Sriram for TENJINCG-254 ends*/
		this.gateway.performAllValidationsForApiLearning();
		/*Added by Padmavathi for license starts*/
		this.gateway.validateAdapterLicense();
		/*Added by Padmavathi for license ends*/
		logger.info("Learner Validation Complete");
		
	}
	
	public void validateGuiLearning() throws RestRequestException, TenjinServletException, MaxActiveUsersException, LicenseInactive {
		logger.info("Validating learner input for GUI Learning");
		
		int appId = this.performAmbiguityCheck();
		List<String> functionCodes = this.convertToList(this.learnerInput.getFunctionCodes());
		if(functionCodes.size() < 1) {
			logger.error("ERROR - No function codes specified.");
			throw new RestRequestException("functionCodes not specified. Please specify list of function codes separated by comma. Example: FUNC01,FUNC02,FUNC03");
		}
		
		this.gateway = new LearnerGateway(appId, functionCodes,this.userId);
		this.gateway.setAutLoginType(this.learnerInput.getAutUserType());
		if(Utilities.trim(this.learnerInput.getBrowserType()).equalsIgnoreCase(""))
		{
			this.gateway.setBrowserSelection("APPDEFAULT");
		}else{
			this.gateway.setBrowserSelection(this.learnerInput.getBrowserType());
			}
		this.gateway.setRegClientName(this.learnerInput.getClientName());
		
		this.gateway.performAllValidations();
		/*Added by Padmavathi for license starts*/
		this.gateway.validateAdapterLicense();
		/*Added by Padmavathi for license ends*/
		logger.info("Learner Validation Complete");
		
	}
	
	public int startRun() throws TenjinServletException {
		logger.info("Creating Learning Run...");
		this.gateway.createRun();
		
		int runId = this.gateway.getTestRun().getId();
		logger.info("Run created with ID [{}]", runId);
		
		logger.info("Creating extended task");
		BridgeProcessor handler = new BridgeProcessor(this.gateway.getManifest());
		handler.createTask();
		
		logger.info("Done. Starting task");
		handler.runTask();
		
		logger.info("Learning run [{}] initiated successfully!", runId);
		
		return runId;
		
	}
	
	private List<String> convertToList(String codes) {
		if(Utilities.trim(codes).length() > 0) {
			String[] split = codes.split(",");
			List<String> list = new ArrayList<String>();
			for(String code : split) {
				list.add(code);
			}
			
			return list;
		}else{
			return new ArrayList<String>();
		}
	}
	
	private int performAmbiguityCheck() throws RestRequestException {
		logger.info("Performing ambiguity check on Application information");
		
		if(this.learnerInput.getApplicationId() > 0 && Utilities.trim(this.learnerInput.getApplicationName()).length() > 0) {
			try {
				Aut aut = new AutHelper().hydrateAut(this.learnerInput.getApplicationName());
				if(aut == null) {
					throw new RestRequestException("Application with name [" + this.learnerInput.getApplicationName() + "] was not found.");
				}
				if(aut.getId() == this.learnerInput.getApplicationId()) {
					return aut.getId();
				}else{
					logger.error("ERROR - Ambiguity check failed. Both Application name and ID are passed, and they represent different applications.");
					throw new RestRequestException("Request is ambiguous. Both applicationId and applicationName are present in the request and they represent different applications.");
				}
			} catch (DatabaseException e) {
				
				logger.error("Ambiguity check failed. Both App ID and name are sent in the request, and both seem to represent different applications");
				throw new RestRequestException("Request is ambiguous. Both applicationId and applicationName are present in the request and they represent different applications.");
			}
		}else if(this.learnerInput.getApplicationId() > 0) {
			return this.learnerInput.getApplicationId();
		}else if(Utilities.trim(this.learnerInput.getApplicationName()).length() > 0) {
			try {
				Aut aut = new AutHelper().hydrateAut(this.learnerInput.getApplicationName());
				if(aut == null) {
					throw new RestRequestException("Application with name [" + this.learnerInput.getApplicationName() + "] was not found.");
				}
				return aut.getId();
			} catch (DatabaseException e) {
				
				logger.error("ERROR finding application with name {}", this.learnerInput.getApplicationName(), e);
				throw new RestRequestException("Application with name [" + this.learnerInput.getApplicationName() + "] was not found.");
			}
			
		}else{
			throw new RestRequestException("Please specify either applicationId or applicationName.");
		}
		
		
		
	}
	
}
