/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCaseService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 05-Dec-2016           Sriram Sridharan          Newly Added For TJN_243_02 
* 19-Aug-2017			Roshni						T25IT-152
* 10-Nov-2017           Gangadhar Badagi         To check the loggedin user
* 10-Nov-2017           Padmavathi      	    To check the loggedin user
* 01-Mar-2017			Padmavathi				TENJINCG-554
* 24-06-2019            Padmavathi              for license	
*/

package com.ycs.tenjin.rest.filter;

import java.io.IOException;
import java.lang.reflect.Method;
import java.security.Principal;
import java.text.ParseException;
import java.util.List;
import java.util.StringTokenizer;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.glassfish.jersey.internal.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.DuplicateUserSessionException;
import com.ycs.tenjin.LicenseExpiredException;
import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.MaxActiveUsersException;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DomainHelper;
import com.ycs.tenjin.db.UserHelper;
import com.ycs.tenjin.handler.LicenseHandler;
import com.ycs.tenjin.license.License;
import com.ycs.tenjin.project.Domain;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.rest.ProjectValidator;
import com.ycs.tenjin.rest.RestUtils;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

public class RestAccessFilter implements ContainerRequestFilter {

	@Context
	private ResourceInfo resourceInfo;
	private static final String AUTHORIZATION_PROPERTY = "Authorization";
    private static final String AUTHENTICATION_SCHEME = "Basic";
    private static final Response ACCESS_DENIED = RestUtils.buildResponse(Status.UNAUTHORIZED, "You are unauthorized to access this resource.", null);
	private static final Response ACCESS_FORBIDDEN =RestUtils.buildResponse(Status.FORBIDDEN, "Access to this resource is forbidden.", null);
	
	private static final Logger logger = LoggerFactory.getLogger(RestAccessFilter.class);
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		
		Method method = resourceInfo.getResourceMethod();
		
		if(!method.isAnnotationPresent(PermitAll.class)){
			if(method.isAnnotationPresent(DenyAll.class))
			{
				requestContext.abortWith(ACCESS_FORBIDDEN);
				return;
			}
			
			 //Get request headers
            final MultivaluedMap<String, String> headers = requestContext.getHeaders();
              
            //Fetch authorization header
            final List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);
              
            //If no authorization information present; block access
            if(authorization == null || authorization.isEmpty())
            {
                requestContext.abortWith(ACCESS_DENIED);
                return;
            }
            try{
          //Get encoded username and password
            final String encodedUserPassword = authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ", "");
              
            //Decode username and password
            String usernameAndPassword = new String(Base64.decode(encodedUserPassword.getBytes()));;
  
            //Split username and password tokens
            final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
            final String username = tokenizer.nextToken();
            final String password = tokenizer.nextToken();
              
            //Verifying Username and password
            /*System.out.println(username);
            System.out.println(password);*/
            User user=null;
            /*Added by Padmavathi for license starts*/
            try {
            	LicenseHandler licenseHandler=new LicenseHandler();
    			License license = (License) CacheUtils.getObjectFromRunCache("licensedetails");
    			if(license==null){
    				List<String> licenseKeys=licenseHandler.getLicenseKey();
    				if(licenseKeys ==null || licenseKeys.size()==0){
    					throw new LicenseExpiredException("You need a Tenjin License to perform this operation. Please contact Tenjin Support.");
    				}
    				license=licenseHandler.validateLicenseKey(licenseKeys.get(0),false);
    				licenseHandler.putLicenseInCache(license);
    				if(licenseKeys.size()>1 && licenseKeys.get(1)!=null){
						licenseHandler.putRenewalLicenseInCache(licenseHandler.getLicense(licenseKeys.get(1)));
					}
    			}else{
    				licenseHandler.licenseActiveValidation(license, false);
    			}
    			/*Added by Padmavathi for license ends*/
    			/*added by shruthi for VAPT encryption issue starts*/
   			    // String hashedpassword = Hashing.sha256().hashString(password , StandardCharsets.UTF_8).toString();
				user = new UserHelper().authenticateRest(username, password);
				 /*added by shruthi for VAPT encryption issue ends*/
				if(license.getType().getName().equalsIgnoreCase("Floating")&& user.getMaxNumberUsers()>=license.getCoreLimit()) {
    				logger.error("More number of Active users than permited...");
    				throw new MaxActiveUsersException("You have reached the maximum limit for your License.");
    			}
				logger.info("User [{}] authenticated", user.getId());
				logger.info("User Roles--> " + user.getRoles());
				
				MultivaluedMap<String, String> pathParams = requestContext.getUriInfo().getPathParameters();
				String domainName = pathParams.getFirst("dName");
				String projectName = pathParams.getFirst("pName");
				
				/*******
				 * Added by Sriram for TJN_243_02
				 */
				Project project = null;
				if(Utilities.trim(domainName).equalsIgnoreCase("") && Utilities.trim(projectName).equalsIgnoreCase("")){
					
				}else{
					Domain domain = new DomainHelper().fetchDomain(domainName);
					if (domain == null) {
						requestContext.abortWith(RestUtils.buildResponse(Status.NOT_FOUND, "Domain '" + domainName + "' does not exist", null));
					}
					else{
			/*	Modified for T25IT-152 starts*/
					if(projectName != null) {
						try {
							/*changed by manish starts*/
							/*project = new ProjectValidator().validateProject(domainName, projectName, user.getId());*/
							project = new ProjectValidator().validateProject(domainName, projectName);
							/*changed by manish ends*/
						} catch (TenjinServletException e) {
							
							requestContext.abortWith(RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null));
						}
					}
			/*		Modified for T25IT-152 ends*/
				}
            }
				
				requestContext.setProperty(RestContextProperties.TJN_USER, user);
				requestContext.setProperty(RestContextProperties.TJN_PROJECT, project);
				
				requestContext.setSecurityContext(new SecurityContext() {
					
					@Override
					public boolean isUserInRole(String arg0) {
						
						return false;
					}
					
					@Override
					public boolean isSecure() {
						
						return false;
					}
					
					@Override
					public Principal getUserPrincipal() {
						
						return new Principal() {

				            @Override
				            public String getName() {
				                return username;
				            }
				        };
					}
					
					@Override
					public String getAuthenticationScheme() {
						
						return null;
					}
					
					
				});
				
				/*******
				 * Added by Sriram for TJN_243_02 ends
				 */
			} catch (DatabaseException e) {
				
				logger.error("Authentication Failed --> " + e.getMessage());
				requestContext.abortWith(RestUtils.buildResponse(Status.UNAUTHORIZED, e.getMessage(), null));
                return;
			} 
            /*Added by Padmavathi for To check the loggedin user starts*/
            catch (DuplicateUserSessionException e1) {
				requestContext.abortWith(RestUtils.buildResponse(Status.UNAUTHORIZED, e1.getMessage(), null));
				return;
			}
            /*Added by Padmavathi for To check the loggedin user ends*/
              /*Added by Prem for TJN262R2-46 stars*/
            /* Uncommented by Padmavathi for license starts */
            catch (MaxActiveUsersException e) {
            	requestContext.abortWith(RestUtils.buildResponse(Status.FORBIDDEN, e.getMessage(), null));
                return;
    		}
    		catch (LicenseExpiredException e) {
    			requestContext.abortWith(RestUtils.buildResponse(Status.UNAUTHORIZED, e.getMessage(), null));
                return;
    		}
            /*Added by Prem for TJN262R2-46 end*/ 
            catch (TenjinServletException e) {
            	requestContext.abortWith(RestUtils.buildResponse(Status.UNAUTHORIZED, e.getMessage(), null));
                return;
			} catch (LicenseInactive e) {
				requestContext.abortWith(RestUtils.buildResponse(Status.UNAUTHORIZED, e.getMessage(), null));
                return;
			} catch (ParseException e) {
				requestContext.abortWith(RestUtils.buildResponse(Status.UNAUTHORIZED, e.getMessage(), null));
                return;
			} catch (TenjinConfigurationException e1) {
				
				logger.error("Error ", e1);
			}
            /* Uncommented by Padmavathi for license ends */
            
            
		}/*added by padmavathi for TENJINCG-554 starts*/
            catch(java.util.NoSuchElementException e){
			requestContext.abortWith(RestUtils.buildResponse(Status.UNAUTHORIZED, "Both username and password are mandatory.", null));
		}
            /*added by padmavathi for TENJINCG-554 starts*/
		}
	}

}
