/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestStepValidator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                  CHANGED BY              DESCRIPTION
* 07-Dec-2016           Abhilash K N          	Newly Added For TJN_243_02 
* 15-Nov-2017           Padmavathi            	For TENJINCG-441,445
* 17-11-2017			Padmavathi		        TENJINCG-470
* 21-Nov-2017			Manish		          	TENJINCG-493
* 30-01-2018			Preeti					TENJINCG-503
* 31-01-2018			Preeti					TENJINCG-503
*/
package com.ycs.tenjin.rest;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ModuleHelper;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Utilities;


public class TestStepValidator {

	private static final  Logger logger=LoggerFactory.getLogger(TestStepValidator.class);
	
	public void validateTestStep(TestStep step) throws TenjinServletException{
		
		//Mandatory Fields 
		//ID, Short Description, Data ID, Application Name (or) ID, Function Code, Operation, Type
		
		if(Utilities.trim(step.getId()).equalsIgnoreCase("")){
			logger.error("Step ID not found");
			throw new TenjinServletException("Step ID [id] is mandatory");
		}
		
		if(Utilities.trim(step.getShortDescription()).equalsIgnoreCase("")){
			throw new TenjinServletException("Short Description [shortDescription] is mandatory");
		}
		
		if(Utilities.trim(step.getDataId()).equalsIgnoreCase("")){
			throw new TenjinServletException("Data ID [dataId] is mandatory");
		}

		if(step.getAppId() == 0 && Utilities.trim(step.getAppName()).equalsIgnoreCase("")){
			throw new TenjinServletException("Either Application Name [appName] or Application ID [appId] is mandatory");

		}
	
		if(!step.getTxnMode().equalsIgnoreCase("API")){
			if(Utilities.trim(step.getModuleCode()).equalsIgnoreCase("")){
				throw new TenjinServletException("Function Code [moduleCode] is mandatory");
			}
		}
			
		else{
			
			if(Utilities.trim(step.getModuleCode()).equalsIgnoreCase("")){
			 throw new TenjinServletException("API Code [moduleCode] is mandatory");
			}
			/* Modified by Preeti for TENJINCG-503 ends */
		}
		
		if((step.getRowsToExecute())<=0){
			throw new TenjinServletException("Rows to execute [rowsToExecute] is mandatory");
		}
		/*	Changed by Padmavathi for TENJINCG-470 ends*/
		
		if(Utilities.trim(step.getOperation()).equalsIgnoreCase("")){
			throw new TenjinServletException("Operation [operation] is mandatory");
		}
		
		/*Added by Pushpalatha for Teststep validation starts*/
		if(Utilities.trim(step.getAutLoginType()).equalsIgnoreCase("")){
			throw new TenjinServletException("AutLoginType [autLoginType] is mandatory");
		}
		else if(!Utilities.trim(step.getAutLoginType()).equalsIgnoreCase("Maker")){
				if(!Utilities.trim(step.getAutLoginType()).equalsIgnoreCase("Checker")){
					throw new TenjinServletException("AutLoginType [autLoginType] should be either 'Maker' or 'Checker' ");
				}
		}
		
		if(Utilities.trim(step.getType()).equalsIgnoreCase("")){
			throw new TenjinServletException("Type [type] is mandatory");
		}
		/*Added by Pushpalatha for Teststep validation ends*/
		
		
		logger.debug("Mandatory fields --> Success");
		
		//Field Validations
		logger.debug("Validating application");
	
	
		
		logger.debug("validating function");
		logger.debug("validating operation");
		logger.debug("validating AUt type");
		/*List<Aut> autLst;
		ArrayList<Module> moduleFunctions;*/
		/*	Changed by Padmavathi for TENJINCG-470 starts*/
		int appId=step.getAppId();
		try {
			Aut aut=new AutHelper().hydrateAut(appId);
			if(aut==null){
				throw new TenjinServletException("Could not validate Application [" + step.getAppName() + "]");
			}
			
			else if(!Utilities.trim(step.getTxnMode()).equalsIgnoreCase("API")){
				this.validateGuiTestStep(aut,step);
			}
			else{
				this.validateApiTestStep(appId,step);
				}
				this.validateAutLoginType(aut, step);
			} catch (DatabaseException e) {
				throw new TenjinServletException(e.getMessage());
			}
		
	}
	/*Added by Padmavathi for TENJINCG-470 starts*/
   public void validateAutLoginType(Aut aut,TestStep step ) throws DatabaseException, TenjinServletException{
		String [] userType=aut.getLoginUserType().split(",");
	   int userFlag=0;
		for(String user:userType){
			if(user.equalsIgnoreCase(Utilities.trim(step.getAutLoginType()))){
				
			}
			else
			{
				userFlag++;
	    		if(userFlag==userType.length){
	    			throw new TenjinServletException("Could not validate autLoginType [" + step.getAutLoginType()+ "]"); 
	    		}
	    		else{continue;}
			}
			break;
		}
	}
   public void validateApiTestStep(int appId,TestStep step) throws DatabaseException, TenjinServletException{
   		/* Modified by Preeti for TENJINCG-503 starts */
	  	Api api  =new ApiHelper().hydrateApi(appId, step.getModuleCode());
		if(api==null){
			/*throw new TenjinServletException("Could not validate Apicode [" + step.getApiCode() + "]");*/ 
			throw new TenjinServletException("Could not validate Apicode [" + step.getModuleCode() + "]"); 
		}
		else{
			ApiOperation apiOperation=	new ApiHelper().hydrateApiOperation(appId,step.getModuleCode(),step.getOperation());
			/* Modified by Preeti for TENJINCG-503 ends */
			if(apiOperation==null){
				throw new TenjinServletException("Could not validate Opeartion [" + step.getOperation() + "]");
			}
		}
   }
   public void validateGuiTestStep(Aut aut,TestStep step) throws DatabaseException, TenjinServletException{
		Module module=new ModuleHelper().hydrateModule(aut.getId(), step.getModuleCode());
		if(module==null){
			throw new TenjinServletException("Could not validate function [" + step.getModuleCode() + "]"); 
		}
		String []operations=aut.getOperation().split(",");
		int opFlag=0;
		for(String operation:operations){
			if(operation.equalsIgnoreCase(Utilities.trim(step.getOperation()))){
				}
			else{
				opFlag++;
				if(opFlag==operations.length){
					throw new TenjinServletException("Could not validate Operation [" + step.getOperation()+ "]"); 
				}
				else{
					continue;
				}
			}
			break;
		}
   }
   /*Added by Padmavathi for TENJINCG-470 ends*/
	//Validation to Update a new Test Step
	public TestStep validateUpdateTestStep(TestStep ts, String jsonString,
			int recordId) throws JSONException, TenjinServletException {
		ts.setTestCaseRecordId(recordId);
		
		JSONObject json=new JSONObject(jsonString);
		
		try{
			json.getString("dataId");
			ts.setDataId(json.getString("dataId"));
		}
		catch(JSONException e){}
		/*Changed by Padmavathi for TENJINCG-441,445 starts*/
		
		try{
			String appId=json.getString("appId");
			if(appId!=null)
			ts.setAppId(Integer.parseInt(appId));
		}
		/*Changed by Padmavathi for TENJINCG-441,445 ends*/
		catch(JSONException e){}
		/*	Changed by Padmavathi for TENJINCG-470 starts*/
		try{
			ts.setTxnMode(json.getString("txnMode"));
		}
		catch(JSONException e){}
		
		try{
			json.getString("type");
			ts.setType(json.getString("type"));
		}
		catch(JSONException e){}
		/*	Changed by Padmavathi for TENJINCG-470 ends*/
		try{
			json.getString("description");
			ts.setDescription(json.getString("description"));
		}
		catch(JSONException e){}
		try{
			json.getString("expectedResult");
			ts.setExpectedResult(json.getString("expectedResult"));
		}
		catch(JSONException e){}
		try{
			json.getString("id");
			ts.setId(json.getString("id"));
			ts.setDataId(json.getString("dataId"));
		}
		catch(JSONException e){}
		try{
			json.getString("moduleCode");
			ts.setModuleCode(json.getString("moduleCode"));
		}
		catch(JSONException e){}
		try{
			json.getString("operation");
			ts.setOperation(json.getString("operation"));
		}
		catch(JSONException e){}
		try{
			json.getString("autLoginType");
			ts.setAutLoginType(json.getString("autLoginType"));
		}
		catch(JSONException e){}
		try{
			json.getString("rowsToExecute");
			ts.setRowsToExecute(Integer.parseInt(json.getString("rowsToExecute")));
		}
		catch(JSONException e){}
		try{
			json.getString("shortDescription");
			ts.setShortDescription(json.getString("shortDescription"));
		}
		catch(JSONException e){}
		/*added by manish for bug# TENJINCG-493 on 21-Nov-2017 starts*/
		try{
			json.getString("validationType");
			ts.setValidationType(json.getString("validationType"));
		}
		catch(JSONException e){}
		/*added by manish for bug# TENJINCG-493 on 21-Nov-2017 ends*/

		return ts;
		
	}
}
