/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AdapterService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 18-Aug-2017			manish					new file added
 * 22-Sep-2017			sameer gupta			For new adapter spec			
 * 17-Nov-2017			Manish					TENJINCG-476
 * 01-Mar-2017			Padmavathi				TENJINCG-554		
 */

package com.ycs.tenjin.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Adapter;
import com.ycs.tenjin.db.AdapterHelper;
import com.ycs.tenjin.db.DatabaseException;

@Path("/adapters")
public class AdapterService {

	private static final Logger logger = LoggerFactory
			.getLogger(AdapterService.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response getAllAdapters() {
		Response response =null;
		try {
			/*******************************************
			/* Changed by Sameer starts New Adapter Spec */
			
			/*changed by manish for bug# TENJINCG-476 on 17-Nov-2017 starts*/
			
			List<Adapter> adapters = new AdapterHelper().hydrateAdapters();
			/*changed by manish for bug# TENJINCG-476 on 17-Nov-2017 ends*/

			
			/*******************************************
			/* Changed by Sameer starts New Adapter Spec */
			/*changed by manish for bug# TENJINCG-476 on 17-Nov-2017 starts*/
			if(adapters!=null&&adapters.size()>0){
				response = RestUtils.buildResponse(Status.OK, adapters.size()+" adapters fetched successfully", adapters);
			}else{
				response = RestUtils.buildResponse(Status.NOT_FOUND, "No adapters found", null);
			}
			/*changed by manish for bug# TENJINCG-476 on 17-Nov-2017 ends*/

		}  catch (DatabaseException e) {
			logger.error(e.getMessage());
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "Could not fetch adapters.", null);
		}
		return response;
	}
	
}
