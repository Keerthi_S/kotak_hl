/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutionResultUploadService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 11-nov-2016			sameer gupta			new file added 
 * 08-11-2018			Ashiki					TENJINCG-902
 * 11-01-2019          	Padmavathi              for TJN252-74 
 * 18-01-2018           Padmavathi             	TJN252-74
 * 28-03-2019			Ashiki					TJN252-96
 */

package com.ycs.tenjin.rest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.xmlbeans.impl.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.RuntimeScreenshot;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.IterationHelper;
import com.ycs.tenjin.db.RunDefectHelper;
import com.ycs.tenjin.util.Utilities;

@Path("/executionResult")
public class ExecutionResultUploadService {

	private static final Logger logger = LoggerFactory
			.getLogger(ExecutionResultUploadService.class);

	@POST
	@Path("/getJson")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	@PermitAll
	public Response getJson(String iterationJsonKey) {
		Response response = null;
		try {

			String json = (String) CacheUtils
					.getObjectFromRunCache(iterationJsonKey);
			if (json == null) {
				json = "";
			} else {
				CacheUtils.removeObjectInRunCache(iterationJsonKey);
			} 
			
			response = Response.status(Status.CREATED).entity(json).build();
		} catch (Exception e) {
			response = RestUtils
					.buildResponse(
							Status.INTERNAL_SERVER_ERROR,
							"An internal error occurred. Please contact Tenjin Administrator",
							null);
		}
		return response;
	}

	@POST
	@Path("/persistJson")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	@PermitAll
	public Response persistExecutionData(String xmlString) {
		logger.info("Request to persisting Execution Data");
		Response response = null;
		try {
			this.persistExecutionIteration(xmlString);
			response = RestUtils.buildResponse(Status.CREATED,
					"persisted Successfully", null);
		} catch (DatabaseException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,
					e.getMessage(), null);
		} catch (Exception e) {
			response = RestUtils
					.buildResponse(
							Status.INTERNAL_SERVER_ERROR,
							"An internal error occurred. Please contact Tenjin Administrator",
							null);
		}
		System.out.println(response);
		return response;
	}

	private void persistExecutionIteration(String xmlString)
			throws IOException, ParserConfigurationException, SAXException,
			DatabaseException {
		/*modified by paneendra for VAPT FIX  starts*/
		File file = null;
		String directories = System.getProperty("java.io.tmpdir")+File.separator+"TenjinResult"+File.separator+"import" + Utilities.getRawTimeStamp()+".xml";
		file = new File(directories);
		file.getParentFile().mkdirs();
		if(file.createNewFile()) {
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			bw.write(xmlString);
			bw.close();
            }
            else {
                logger.error("File is not created ");
            }/*modified by paneendra for VAPT FIX  ends*/
		List<RuntimeFieldValue> values = new ArrayList<RuntimeFieldValue>();
		List<ValidationResult> results = new ArrayList<ValidationResult>();
		List<RuntimeScreenshot> shots = new ArrayList<RuntimeScreenshot>();


		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		/*Added by paneendra for VAPT FIX starts*/
		DocumentBuilderFactory.newInstance("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl", ClassLoader.getSystemClassLoader());
		dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		//dbFactory.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, "true");
		dbFactory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
		/*Added by paneendra for VAPT FIX ends*/
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);

		doc.getDocumentElement().normalize();

		System.out.println("Root element :"
				+ doc.getDocumentElement().getNodeName());

		NodeList nList = doc.getElementsByTagName("iterationStatus");

		System.out.println("----------------------------");
		String runId = null;
		String tdUid = null;
		int iteration = 0;
		int testStepRecordId = 0;
		String tdGid = null;
		String message = null;
		String finalResult = null;

		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node nNode = nList.item(temp);

			System.out.println("\nCurrent Element :" + nNode.getNodeName());

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) nNode;
				NodeList childs = eElement.getChildNodes();
				for (int tmp = 0; tmp < eElement.getChildNodes().getLength(); tmp++) {
					Node tempNode = childs.item(tmp);
					if (tempNode.getChildNodes().getLength() == 1) {
						if (tempNode.getNodeName().equalsIgnoreCase("runId")) {
							runId = tempNode.getTextContent();
						} else if (tempNode.getNodeName().equalsIgnoreCase(
								"tdUid")) {
							tdUid = tempNode.getTextContent();
						} else if (tempNode.getNodeName().equalsIgnoreCase(
								"iteration")) {
							iteration = Integer.parseInt(tempNode
									.getTextContent());
						} else if (tempNode.getNodeName().equalsIgnoreCase(
								"testStepRecordId")) {
							testStepRecordId = Integer.parseInt(tempNode
									.getTextContent());
						} else if (tempNode.getNodeName().equalsIgnoreCase(
								"tdGid")) {
							tdGid = tempNode.getTextContent();
						} else if (tempNode.getNodeName().equalsIgnoreCase(
								"message")) {
							message = tempNode.getTextContent();
						} else if (tempNode.getNodeName().equalsIgnoreCase(
								"result")) {
							finalResult = tempNode.getTextContent();
						}
					}
					if (tempNode.getNodeName().contains("runtimeFieldValues")) {
						if (tempNode.getChildNodes() != null) {
							NodeList runtimeFields = tempNode.getChildNodes();
							if (runtimeFields.getLength() > 0) {
								for (int k = 0; k < runtimeFields.getLength(); k++) {
									if (runtimeFields.item(k).getNodeType() == Node.ELEMENT_NODE) {
										Element runFields = (Element) runtimeFields
												.item(k);
										if (runFields.getNodeName().contains(
												"runtimeFieldValue")) {
											RuntimeFieldValue value = new RuntimeFieldValue();
											value.setLocationName(runFields
													.getElementsByTagName(
															"location").item(0)
													.getTextContent());
											value.setField(runFields
													.getElementsByTagName(
															"field").item(0)
													.getTextContent());
											value.setValue(runFields
													.getElementsByTagName(
															"value").item(0)
													.getTextContent());
											value.setRunId(Integer
													.parseInt(runId));
											value.setTdUid(tdUid);
											value.setTdGid(tdGid);
											value.setIteration(iteration);
											value.setTestStepRecordId(testStepRecordId);
											value.setDetailRecordNo(1);
											values.add(value);
										}
									}
								}
							}
						}
					} else if (tempNode.getNodeName().contains(
							"validationResults")) {
						if (tempNode.getChildNodes() != null) {
							NodeList validateFields = tempNode.getChildNodes();
							if (validateFields.getLength() > 0) {
								for (int k = 0; k < validateFields.getLength(); k++) {
									if (validateFields.item(k).getNodeType() == Node.ELEMENT_NODE) {
										Element validateField = (Element) validateFields
												.item(k);
										if (validateField.getNodeName()
												.contains("validationResult")) {
											ValidationResult result = new ValidationResult();
											result.setActualValue(validateField
													.getElementsByTagName(
															"actualValue")
													.item(0).getTextContent());
											result.setExpectedValue(validateField
													.getElementsByTagName(
															"expectedValue")
													.item(0).getTextContent());
											result.setStatus(validateField
													.getElementsByTagName(
															"status").item(0)
													.getTextContent());
											result.setDetailRecordNo(validateField
													.getElementsByTagName(
															"detailRecordNo")
													.item(0).getTextContent());
											result.setRunId(Integer
													.parseInt(runId));
											result.setIteration(iteration);
											result.setTestStepRecordId(testStepRecordId);
											result.setPage(validateField
													.getElementsByTagName(
															"page").item(0)
													.getTextContent());
											result.setField(validateField
													.getElementsByTagName(
															"field").item(0)
													.getTextContent());
											results.add(result);
										}
									}
								}
							}
						}
					} else if (tempNode.getNodeName().contains(
							"runtimeScreenshots")) {
						if (tempNode.getChildNodes() != null) {
							NodeList runtimeScreenshots = tempNode
									.getChildNodes();
							if (runtimeScreenshots.getLength() > 0) {
							
								for (int k = 0; k < runtimeScreenshots
										.getLength(); k++) {
									/*File outputfile = null;*/
									if (runtimeScreenshots.item(k)
											.getNodeType() == Node.ELEMENT_NODE) {
										Element runtimeScreenshot = (Element) runtimeScreenshots
												.item(k);
										if (runtimeScreenshot.getNodeName()
												.contains("runtimeScreenshot")) {
											RuntimeScreenshot shot = new RuntimeScreenshot();
											shot.setDescription(runtimeScreenshot
													.getElementsByTagName(
															"description")
													.item(0).getTextContent());
											shot.setType(runtimeScreenshot
													.getElementsByTagName(
															"type").item(0)
													.getTextContent());

											// DateFormat formatter;
											
											shot.setSequence(Integer
													.parseInt(runtimeScreenshot
															.getElementsByTagName(
																	"sequence")
															.item(0)
															.getTextContent()));
											String timestamp=runtimeScreenshot
													.getElementsByTagName(
															"timestamp")
													.item(0)
													.getTextContent();
											
											Date date=null;
											try {
												date = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(timestamp);
											} catch (ParseException e) {
												
												logger.error("Error ", e);
											} 
											Timestamp ts = new Timestamp(date.getTime());
											shot.setTimestamp(ts);
											byte a[] = Base64
													.decode(runtimeScreenshot
															.getElementsByTagName(
																	"screenshotByteArray")
															.item(0)
															.getTextContent()
															.getBytes());
											shot.setScreenshotByteArray(a);
											shot.setRunId(Integer
													.parseInt(runId));
											shot.setTdGid(tdGid);
											shot.setTdUid(tdUid);
											shot.setTestStepRecordId(testStepRecordId);
											shot.setIteration(iteration);
											shots.add(shot);
										}
									}
								}
							}
						}
					}
				}

			}
		}
		IterationStatus status2 = new IterationStatus();
		// status2.setTdGid(TDGID);
		status2.setTdUid(tdUid);
		status2.setRunId(Integer.parseInt(runId));
		status2.setValidationResults(results);
		status2.setRuntimeFieldValues(values);
		status2.setRuntimeScreenshots(shots);
		status2.setResult(finalResult);
		status2.setMessage(message);

		

		String iterationKey = runId + "IterationStatus_FOR_EXTRACTION"; 
		String operationKey = runId + "OPERATION_FOR_EXECUTION";
		String operation = null;
		try {
			operation = (String)CacheUtils.getObjectFromRunCache(operationKey);
		} catch (TenjinConfigurationException e) { 
			logger.error("Error ", e);
		}
		
		if("extraction".equalsIgnoreCase(operation)){ 
			CacheUtils.putObjectInRunCache(iterationKey, status2);
		} else {

			IterationHelper helper = new IterationHelper();

			// TENJINCG-730 - By Sameer - Start
			Date endTime = new Date();
			helper.updateRunStatus(status2.getRunId(), testStepRecordId,
					iteration, status2.getResult(), status2.getMessage(),
					endTime);
			// TENJINCG-730 - By Sameer - End

			logger.info("Persisting validation resulsts...");
			helper.persistValidationResults(status2.getValidationResults(),
					status2.getRunId(), testStepRecordId, iteration);
			
			/*Added Ashiki for TJN252-96 starts*/
			List<StepIterationResult> validationFailures = new RunDefectHelper()
					.getTransactionsWithValidationFailures(Integer.parseInt(runId));
			if (validationFailures != null && validationFailures.size() > 0) {
				for (StepIterationResult r : validationFailures) {
					
					if (testStepRecordId == r.getTsRecId()
							&& status2.getResult().equalsIgnoreCase("S")
							&& r.getIterationNo() == iteration) {
						
						status2.setResult("F");
						status2.setMessage("Validation Failed");
						break;
					}
				}
			}
			
			endTime = new Date();
			helper.updateRunStatus(Integer.parseInt(runId), testStepRecordId, iteration,status2.getResult(), status2.getMessage(), endTime);
			/*Added Ashiki for TJN252-96 ends*/
			
			logger.info("Persisting Run-Time Values...");
			helper.persistRuntimeFieldValues(status2.getRuntimeFieldValues(),
					status2.getRunId(), testStepRecordId, iteration);

			logger.info("Persisting Run-Time Screenshots...");
			/* Modified by Padmavathi for TJN252-74 starts */
			/*
			 * helper.persistRuntimeScreenshots(status2.getRuntimeScreenshots(),
			 * status2.getRunId(), testStepRecordId, iteration);
			 */
			helper.persistRuntimeScreenshots(status2.getRuntimeScreenshots(),
					status2.getRunId(), testStepRecordId, iteration, true);
			/* Modified by Padmavathi for TJN252-74 ends */
		}
		
	}

	

}
