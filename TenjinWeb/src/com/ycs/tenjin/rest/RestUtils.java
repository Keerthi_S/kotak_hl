/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RestUtils.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 11-nov-2016			nagababu				new file added
 */

package com.ycs.tenjin.rest;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ycs.tenjin.util.Utilities;

public class RestUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(RestUtils.class);
	
	public static Response buildResponse(int status, String message, Object object){
		
		try {
			JSONObject json = new JSONObject();
			json.put("status", status);
			json.put("message", message);
			
			Gson gson = new Gson();
			String oJson = gson.toJson(object);
			json.put("data", oJson);
			
			return Response.status(status).entity(json.toString()).build();
		} catch (JSONException e) {
			
			logger.error("JSONException caught while building response", e);
			String ret = "{status:500, message:Internal Server Error. Please Contact Tenjin Support.}";
			return Response.status(500).entity(ret).build();
		}
		
	}
	
	
	public static Response buildResponse(Response.Status status, String message, Object object){
		
		try {
			JSONObject json = new JSONObject();
			json.put("status", status);
			json.put("message", message);
			
			Gson gson = new GsonBuilder().setExclusionStrategies(new JsonSerializationStrategy()).create();
			if (object != null) {
				
				if(object instanceof String) {
					json.put("data", (String) object);
				}else if(object instanceof Integer) {
					json.put("data", (Integer) object);
				}else{
					String oJson = gson.toJson(object);
					try {
						JSONObject sjson = new JSONObject(oJson);
						json.put("data", sjson);
					} catch (Exception e) {
						
						JSONArray jArray = new JSONArray(oJson);
						json.put("data", jArray);
					}
				}
			}
			return Response.status(status).entity(json.toString()).build();
		} catch (JSONException e) {
			
			logger.error("JSONException caught while building response", e);
			String ret = "{status:500, message:Internal Server Error. Please Contact Tenjin Support.}";
			return Response.status(500).entity(ret).build();
		}
		
	}
	
	public static Response buildResponse(Response.Status status, String messageKey, Object[] parameters, Object object) {
		Locale locale = new Locale("en", "US");
		return buildResponse(status, loadMessage(locale, messageKey, parameters), object);
	}
	
	public static Response buildResponse(Response.Status status, String messageKey, Object parameter, Object object) {
		Locale locale = new Locale("en", "US");
		Object[] parameters = {parameter};
		return buildResponse(status, loadMessage(locale, messageKey, parameters), object);
	}
	
	public static String loadMessage(String messageKey, Object[] parameters) {
		return loadMessage(new Locale("en", "US"), messageKey, parameters);
	}
	public static String loadMessage(Locale locale, String messageKey, Object[] parameters) {
		
		logger.info("Loading message for {}", locale.toString());
		ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.tjn_messages", locale);
		try {
			String messageFromBundle = resourceBundle.getString(messageKey);
			return Utilities.trim(messageFromBundle).length() > 0 ? MessageFormat.format(messageFromBundle, parameters) : messageKey;
		} catch (Exception e) {
			logger.error("ERROR getting value for Message Key {} from Message bundle", messageKey, e);
			return messageKey;
		}
	}
}
