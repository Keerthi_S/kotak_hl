/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApiService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 17-Aug-2017			Sriram Sridharan		T25IT-186
* 17-Aug-2017			Sriram Sridharan		T25IT-184
* 17-Aug-2017			Sriram Sridharan		T25IT-187
* 17-Aug-2017			Sriram Sridharan		T25IT-188
* 17-Aug-2017			Sriram Sridharan		T25IT-181
* 17-Aug-2017			Sriram Sridharan		T25IT-178
* 17-Aug-2017			Sriram Sridharan		T25IT-179
* 15-Nov-2017			Gangadhar Badagi		TENJINCG-439
* 21-11-2017			Gangadhar Badagi		TENJINCG-492
* 21-11-2017			Gangadhar Badagi		TENJINCG-496
* 21-11-2017			Gangadhar Badagi		TENJINCG-497
* 22-Nov-2017			Manish					TENJINCG-504
* 23-Nov-2017			Sriram Sridharan		TENJINCG-515
* 23-11-2017			Gangadhar Badagi		TENJINCG-522
* 27-11-2017            Gangadhar Badagi        Added to change status
*/

package com.ycs.tenjin.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Representation;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Utilities;

@Path("/auts/{applicationId}/apis")
public class ApiService {
	
	private static final Logger logger = LoggerFactory.getLogger(ApiService.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllApisForApplication(@PathParam("applicationId") int id) {
		
		logger.info("Getting all APIs under application [{}]", id);
		try {
			/*Added by Gangadhar Badagi for TENJINCG-497 starts*/
			new AutHelper().hydrateAut(id);
			/*Added by Gangadhar Badagi for TENJINCG-497 ends*/
			List<Api> apis = new ApiHelper().hydrateAllApiWithDetails(id);
			logger.info("{} APIs fetched", apis.size());
			return RestUtils.buildResponse(Status.OK, apis.size() + " APIs fetched.", apis);
		} catch (DatabaseException e) {
			
			logger.error("ERROR fetching APIs", e);
			/*Changed by Gangadhar Badagi for generic error message starts*/
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
			/*Changed by Gangadhar Badagi for generic error message ends*/
		}
		
		
	}
	
	@GET
	@Path("/{apiCode}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getApiForApplication(@PathParam("applicationId") int appId, @PathParam("apiCode") String apiCode) {
		
		logger.info("Getting API [{}] under application [{}]", appId, apiCode);
		try {
			/*Added by Gangadhar Badagi for TENJINCG-497 starts*/
			new AutHelper().hydrateAut(appId);
			/*Added by Gangadhar Badagi for TENJINCG-497 ends*/
			Api api = new ApiHelper().hydrateApi(appId, apiCode);
			if(api == null) {
				logger.error("API [{}] was not found under application [{}]", apiCode, appId);
				return RestUtils.buildResponse(Status.NOT_FOUND, "API [" + apiCode + "] was not found.", null);
			}
			logger.info("API Information fetched successfully.");
			return RestUtils.buildResponse(Status.OK, "Successfully fetched API information", api);
		} catch (DatabaseException e) {
			
			logger.error("ERROR fetching API", e);
			/*Changed by Gangadhar Badagi for generic error message starts*/
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
			/*Changed by Gangadhar Badagi for generic error message ends*/
		}
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createApi(@PathParam("applicationId") int appId, Api api) {
		logger.info("Creating API [{}]", api.getCode());
		
		ApiValidator validator = new ApiValidator();
		ApiHelper apiHelper = new ApiHelper();
		try {
			/*Added by Gangadhar Badagi for TENJINCG-492 starts*/
			new AutHelper().hydrateAut(appId);
			/*Added by Gangadhar Badagi for TENJINCG-492 ends*/
			/* Fix for T25IT-178 */
			api.setApplicationId(appId);
			/* Fix for T25IT-178 ends*/
			/*Added by Gangadhar Badagi for TENJINCG-492 starts*/
			validator.validateApiManadateFields(api);
		    /*Added by Gangadhar Badagi for TENJINCG-492 ends*/
			validator.validateApiForCreate(api);
			apiHelper.persistApi(appId, api);
			
			logger.info("API created successfully");
			return RestUtils.buildResponse(Status.CREATED, "API " + api.getCode() + " created successfully.", api);
		} catch (TenjinServletException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
		} catch (BridgeException e) {
			logger.error("ERROR fetching API", e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Support", null);
		} catch (DatabaseException e) {
			logger.error("ERROR fetching API", e);
			/*Changed by Gangadhar Badagi for TENJINCG-492 starts*/
			return RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
			/*Changed by Gangadhar Badagi for TENJINCG-492 ends*/
		} 
		/*Changed by Gangadhar Badagi for TENJINCG-522 starts*/
		catch (DuplicateRecordException e) {
			return RestUtils.buildResponse(Status.CONFLICT, e.getMessage(), null);
		}
		/*Changed by Gangadhar Badagi for TENJINCG-522 ends*/
	}
	
	
	@PUT
	@Path("/{apiCode}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateApi(@PathParam("applicationId") int appId, @PathParam("apiCode") String apiCode, Api api) {
		logger.info("Updating API [{}]", apiCode);
		
		try{
			
			/*Added by Gangadhar Badagi for TENJINCG-497 starts*/
			new AutHelper().hydrateAut(appId);
			/*Added by Gangadhar Badagi for TENJINCG-497 ends*/
			/*Changed by manish to restrict updation of apicode and api type*/
			if(!Utilities.trim(api.getCode()).equalsIgnoreCase("")){
				return RestUtils.buildResponse(Status.BAD_REQUEST, "Cannot update API code.", null);
			}
			
			if(!Utilities.trim(api.getType()).equalsIgnoreCase("")){
				return RestUtils.buildResponse(Status.BAD_REQUEST, "Cannot update API type.", null);
			}
			/*Changed by manish to restrict updation of apicode and api type*/
			Api oApi = new ApiHelper().hydrateApi(appId, apiCode);
			oApi.merge(api);
			new ApiValidator().validateApiForUpdate(oApi);
			new ApiHelper().updateApi(oApi);
			
			Api updatedApi = new ApiHelper().hydrateApi(appId, apiCode);
			return RestUtils.buildResponse(Status.OK, "API Updated successfully", updatedApi);
		}catch (DatabaseException e) {
			
			logger.error("ERROR fetching API", e);
			/*Changed by Gangadhar Badagi for generic error message starts*/
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
			/*Changed by Gangadhar Badagi for generic error message ends*/
		} catch (TenjinServletException e) {
			
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
		} catch (BridgeException e) {
			
			logger.error("ERROR fetching API", e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Support", null);
			
		} /*Changed by Gangadhar Badagi for TENJINCG-522 starts*/
		catch (DuplicateRecordException e) {
			return RestUtils.buildResponse(Status.CONFLICT, e.getMessage(), null);
		}
		/*Changed by Gangadhar Badagi for TENJINCG-522 ends*/
	}
	
	/* Fix for T25IT-184 */
	
	
	@GET
	@Path("/{apiCode}/operations")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOperationsForApi(@PathParam("applicationId") int appId, @PathParam("apiCode") String apiCode) {
		logger.info("Getting operations for API [{}] for Application [{}]", apiCode, appId);
		
		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 starts*/
		try{
			new AutHelper().hydrateAut(appId);
		}catch(DatabaseException e){
			return RestUtils.buildResponse(Status.NOT_FOUND, "Aut with id '"+appId+"' does not exist", null);
		}
		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 ends*/
		
		
		try {
			Api api = new ApiHelper().hydrateApi(appId, apiCode);
			if(api != null) {
				if(api.getOperations() != null && api.getOperations().size() > 0) {
					return RestUtils.buildResponse(Status.OK, api.getOperations().size() + " operations found.", api.getOperations());
				}else {
					return RestUtils.buildResponse(Status.NOT_FOUND, "No operations were found for this API.", null);
				}
			}else {
				return RestUtils.buildResponse(Status.NOT_FOUND, "The specified API was not found.", null);
			}
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "Could not fetch operations due to an internal error. Please contact Tenjin Support.",null);
		}
	}
	/* Fix for T25IT-184 ends*/
	
	/* Fix for T25IT-186 */
	@GET
	@Path("/{apiCode}/operations/{operationName}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOperation(@PathParam("applicationId") int appId, @PathParam("apiCode") String apiCode, @PathParam("operationName") String operationName) {
		logger.info("Getting operation [{}] for API [{}] for Application [{}]", operationName, apiCode, appId);
		
		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 starts*/
		try{
			new AutHelper().hydrateAut(appId);
		}catch(DatabaseException e){
			return RestUtils.buildResponse(Status.NOT_FOUND, "Aut with id '"+appId+"' does not exist", null);
		}
		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 ends*/
		
		try {
			Api api = new ApiHelper().hydrateApi(appId, apiCode);
			if(api != null) {
				if(api.getOperations() != null && api.getOperations().size() > 0) {
					/*return RestUtils.buildResponse(Status.OK, api.getOperations().size() + " operations found.", api.getOperations());*/
					boolean opFound = false;
					ApiOperation targetOp = null;
					for(ApiOperation op : api.getOperations()) {
						if(Utilities.trim(operationName).equalsIgnoreCase(op.getName())) {
							opFound = true;
							targetOp = op;
						}
					}
					
					if(!opFound) {
						return RestUtils.buildResponse(Status.NOT_FOUND, "Operation does not exist.", null);
					}else {
						return RestUtils.buildResponse(Status.OK, "Operation fetched successfully!", targetOp);
					}
					
				}else {
					logger.error("No Operations found for this API");
					return RestUtils.buildResponse(Status.NOT_FOUND, "Operation does not exist.", null);
				}
			}else {
				return RestUtils.buildResponse(Status.NOT_FOUND, "The specified API was not found.", null);
			}
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "Could not fetch operations due to an internal error. Please contact Tenjin Support.",null);
		}
	}
	/* Fix for T25IT-186 ends*/
	/* Fix for T25IT-187*/
	@POST
	@Path("/{apiCode}/operations")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createApiOperation(@PathParam("applicationId") int appId, @PathParam("apiCode") String apiCode, ApiOperation operation) {
		
		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 starts*/
		try{
			new AutHelper().hydrateAut(appId);
		}catch(DatabaseException e){
			return RestUtils.buildResponse(Status.NOT_FOUND, "Aut with id '"+appId+"' does not exist", null);
		}
		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 ends*/
		
		ApiValidator validator = new ApiValidator();
		/*Added by Gangadhar Badagi for TENJINCG-439 starts*/
		ArrayList<Representation> r=new ArrayList<Representation>();
		Representation rr=new Representation();
		rr.setMediaType("application/json");
		r.add(rr);
		/*Added by Gangadhar Badagi for TENJINCG-439 ends*/
		try {
			validator.validateApiOperationForCreate(appId, apiCode, operation);
		} catch (TenjinServletException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
		} catch(DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Support.", null);
		}
		
		try {
			/*Added by Gangadhar Badagi for TENJINCG-439 starts*/
			if(operation.getListRequestRepresentation()==null){
			operation.setListRequestRepresentation(r);
			}
			if(operation.getListResponseRepresentation()==null){
				operation.setListResponseRepresentation(r);
				}
			/*Added by Gangadhar Badagi for TENJINCG-439 ends*/
			new ApiHelper().persistApiOperationInDetail(operation, apiCode, appId);
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Support.", null);
		}
		
		try {
			ApiOperation op = new ApiHelper().hydrateApiOperation(appId, apiCode, operation.getName());
			return RestUtils.buildResponse(Status.CREATED, "Operation created successfully!", op);
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.CREATED, "Operation created successfully, but Tenjin was unable to fetch its details due to an internal error.", null);
		}
		
		
	}
	
	@POST
	@Path("/{apiCode}/operations/bulk")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createApiOperations(@PathParam("applicationId") int appId, @PathParam("apiCode") String apiCode, List<ApiOperation> operations) {
		
		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 starts*/
		try{
			new AutHelper().hydrateAut(appId);
		}catch(DatabaseException e){
			return RestUtils.buildResponse(Status.NOT_FOUND, "Aut with id '"+appId+"' does not exist", null);
		}
		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 ends*/
		
		
		ApiValidator validator = new ApiValidator();
		/*Added by Gangadhar Badagi for TENJINCG-439 starts*/
		ArrayList<Representation> r=new ArrayList<Representation>();
		Representation rr=new Representation();
		rr.setMediaType("application/json");
		r.add(rr);
		/*Added by Gangadhar Badagi for TENJINCG-439 ends*/
		List<Object> messages = new ArrayList<Object>();
		if(operations != null) {
			for(ApiOperation operation : operations) {
				String opName = operation.getName() != null ? operation.getName() : "[NO NAME SPECIFIED]";
				try {
					validator.validateApiOperationForCreate(appId, apiCode, operation);
				} catch (TenjinServletException e) {
					messages.add("Failed creating operation [" + opName + "]: " + e.getMessage());
					continue;
				} catch (DatabaseException e) {
					messages.add("Failed creating operation [" + opName + "]: An internal error occurred. Please contact Tenjin Support.");
					continue;
				}
				
				try {
					/*Added by Gangadhar Badagi for TENJINCG-439 starts*/
					if(operation.getListRequestRepresentation()==null){
					operation.setListRequestRepresentation(r);
					}
					if(operation.getListResponseRepresentation()==null){
						operation.setListResponseRepresentation(r);
						}
					/*Added by Gangadhar Badagi for TENJINCG-439 ends*/
					new ApiHelper().persistApiOperationInDetail(operation, apiCode, appId);
				} catch (DatabaseException e) {
					messages.add("Failed creating operation [" + opName + "]: An internal error occurred. Please contact Tenjin Support.");
					continue;
				}
				
				try {
					ApiOperation op = new ApiHelper().hydrateApiOperation(appId, apiCode, operation.getName());
					messages.add(op);
				} catch (DatabaseException e) {
					messages.add("Operation [" + opName + "] was created successfully, but Tenjin could not fetch its details due to an internal error.");
					continue;
				}
			}
		}
		/*Changed by Gangadhar Badagi to change status starts*/
		/*return RestUtils.buildResponse(Status.OK, "Bulk operation completed. Please see detailed messages for each operation.", messages);*/
		return RestUtils.buildResponse(Status.CREATED, "Bulk operation completed. Please see detailed messages for each operation.", messages);
		/*Changed by Gangadhar Badagi to change status ends*/
	}
	/* Fix for T25IT-187 ends*/
	
	/* Fix for T25IT-188 */
	@PUT
	@Path("/{apiCode}/operations/{operationName}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateOperation(@PathParam("applicationId") int appId, @PathParam("apiCode") String apiCode, @PathParam("operationName") String operationName, ApiOperation operation) {

		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 starts*/
		try{
			new AutHelper().hydrateAut(appId);
		}catch(DatabaseException e){
			return RestUtils.buildResponse(Status.NOT_FOUND, "Aut with id '"+appId+"' does not exist", null);
		}
		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 ends*/
		
		
		ApiHelper helper = new ApiHelper();
		try {
			if(operation.getMethod()!=null){
				return RestUtils.buildResponse(Status.BAD_REQUEST, "Cannot update operation method", null);
			}
			
			
			ApiOperation mergedOperation = helper.hydrateApiOperation(appId, apiCode, operationName);
			mergedOperation.merge(operation);
			
			ApiValidator validator = new ApiValidator();
			try {
				validator.validateApiOperationForUpdate(appId, apiCode, mergedOperation, operationName);
			} catch (TenjinServletException e) {
				return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
			}
			
			helper.updateApiOperation(mergedOperation, appId, apiCode, operationName);
			
			//TENJINCG-515
			logger.info("Updating Representations");
			helper.updateApiRepresentations(appId, apiCode, operationName, mergedOperation.getListRequestRepresentation(), mergedOperation.getListResponseRepresentation());
			
			logger.info("Updating Resource Parameters");
			helper.updateApiOperationResourceParameters(appId, apiCode, operationName, mergedOperation.getResourceParameters());
			
			logger.info("Updation successfull");//TENJINCG-515 ends
			
			ApiOperation newOp = helper.hydrateApiOperation(appId, apiCode, mergedOperation.getName());
			return RestUtils.buildResponse(Status.OK, "Operation updated successfully", newOp);
			
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Support.", null);
		}
		
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{apiCode}/operations/{operationName}")
	public Response removeOperation(@PathParam("applicationId") int appId, @PathParam("apiCode") String apiCode, @PathParam("operationName") String operationName) {
		
		
		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 starts*/
		try{
			new AutHelper().hydrateAut(appId);
		}catch(DatabaseException e){
			return RestUtils.buildResponse(Status.NOT_FOUND, "Aut with id '"+appId+"' does not exist", null);
		}
		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 ends*/
		
		ApiHelper helper = new ApiHelper();
		
		try {
			ApiOperation op = helper.hydrateApiOperation(appId, apiCode, operationName);
			if(op == null) {
				return RestUtils.buildResponse(Status.NOT_FOUND, "Operation [" + operationName + "] was not found for API [" + apiCode + "]", null);
			}
			
			helper.clearApiOperationInDetail(appId, apiCode, operationName);
			
			return RestUtils.buildResponse(Status.OK, "Operation removed successfully.", null);
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Support.", null);
		}
		
	}
	/* Fix for T25IT-188 ends*/
	
	/* Fix for T25IT-181 */
	@DELETE
	@Path("/{apiCode}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeApi(@PathParam("applicationId") int appId, @PathParam("apiCode") String apiCode) {
		logger.info("Updating API [{}]", apiCode);
		
		try{
			/*Added by Gangadhar Badagi for TENJINCG-497 starts*/
			new AutHelper().hydrateAut(appId);
			/*Added by Gangadhar Badagi for TENJINCG-497 ends*/
			Api oApi = new ApiHelper().hydrateApi(appId, apiCode);
			
			if(oApi == null) {
				return RestUtils.buildResponse(Status.NOT_FOUND, "The API [" + apiCode + "] does not exist for the specified application.", null);
			}
			
			ApiHelper helper = new ApiHelper();
			helper.clearApi(appId, apiCode);
			
			return RestUtils.buildResponse(Status.OK, "API deleted successfully.", null);
		}catch (DatabaseException e) {
			logger.error("ERROR fetching API", e);
			/*Changed by Gangadhar Badagi for generic error message starts*/
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null);
			/*Changed by Gangadhar Badagi for generic error message ends*/
		} 
	}
	/* Fix for T25IT-181 ends*/
	
	/* Fix for T25IT-179*/
	@POST
	@Path("/bulk")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createApis(@PathParam("applicationId") int appId, List<Api> apis) {
		
		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 starts*/
		try{
			new AutHelper().hydrateAut(appId);
		}catch(DatabaseException e){
			return RestUtils.buildResponse(Status.NOT_FOUND, "Aut with id '"+appId+"' does not exist", null);
		}
		/*added by manish for bug# TENJINCG-504 on 22-Nov-2017 ends*/
		
		
		ApiValidator validator = new ApiValidator();
		ApiHelper apiHelper = new ApiHelper();
		
		if(apis == null) {
			
		}
		/*Added by Gangadhar Badagi for TENJINCG-496 starts*/
		try {
			new AutHelper().hydrateAut(appId);
		} catch (DatabaseException e1) {
			return RestUtils.buildResponse(Status.NOT_FOUND,e1.getMessage(), null);
		}
		/*Added by Gangadhar Badagi for TENJINCG-496 ends*/
		List<Object> messages = new ArrayList<Object> ();
		
		for(Api api : apis) {
			String apiCode = Utilities.trim(api.getCode()).length() > 0 ? api.getCode() : "NO_CODE_SPECIFIED";
			try {
				/* Fix for T25IT-178 */
				api.setApplicationId(appId);
				/* Fix for T25IT-178 ends*/
				validator.validateApiForCreate(api);
				apiHelper.persistApi(appId, api);
				
				logger.info("API created successfully");
				messages.add(api);
			} catch (TenjinServletException e) {
				messages.add("Error creating API [" + apiCode + "]: " + e.getMessage());
			} catch (BridgeException e) {
				logger.error("ERROR fetching API", e);
				messages.add("Error creating API [" + apiCode + "]: " + "An internal error occurred. Please contact Tenjin Support.");
			} catch (DatabaseException e) {
				logger.error("ERROR fetching API", e);
				messages.add("Error creating API [" + apiCode + "]: " + "An internal error occurred. Please contact Tenjin Support.");
			} /*Changed by Gangadhar Badagi for TENJINCG-522 starts*/
			catch (DuplicateRecordException e) {
				/*Changed by Padmavathi for adding error message instead of returing starts*/
				messages.add(e.getMessage());
				/*Changed by Padmavathi for adding error message instead of returing ends*/
			}
			/*Changed by Gangadhar Badagi for TENJINCG-522 ends*/
		}
		
		return RestUtils.buildResponse(Status.OK, "Bulk Addition of APIs is complete. Please refer to the response data for individual results", messages);
		
	}
	/* Fix for T25IT-179 ends*/
}
