/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCaseService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 07-Dec-2016           Abhilash K N          Newly Added For TJN_243_02 
* 10-08-2017		    Gangadhar Badagi		    T25IT-115
* 11-08-2017			Pushpalatha			  	T25IT-130
* 16-08-2017			Pushpalatha			  	T25IT-134
* 16-08-2017			Pushpalatha			  	T25IT-165
* 17-08-2017		    Gangadhar Badagi		T25IT-93
* 18-08-2017		    Gangadhar Badagi		T25IT-196
* 18-08-2017			Roshni					T25IT-216
* 18-08-2017			Sriram					T25IT-197
* 18-08-2017		    Gangadhar Badagi		T25IT-194
* 19-08-2017			Pushpalatha				T25IT-190
* 29-08-2017			Padmavathi    			T25IT-337
* 17-10-2017			Padmavathi    			for checking txnMode  as null or not
* 17-11-2017			Sriram Sridharan		TENJINCG-459
* 20-11-2017			Padmavathi		        TENJINCG-470
* 22-Nov-2017			Gangadhar Badagi		TENJINCG-510
* 16-03-2018            Padmavathi              TENJINCG-612
* 19-02-2019			Preeti					TENJINCG-969
* 29-03-2019			Preeti					TENJINCG-1003
*/

package com.ycs.tenjin.rest;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.TestCaseHandler;
import com.ycs.tenjin.handler.TestStepHandler;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.User;
@Path("/domains")
public class TestStepService {
	
	private static final Logger logger = LoggerFactory.getLogger(TestStepService.class);
	TestStepHandler stepHandler=new TestStepHandler();
	
	@Context
	ContainerRequestContext requestContext;
	
	@Context 
	SecurityContext securityContext;
	
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		@Path("/{dName}/projects/{pName}/testcases/{recordId}/steps")	
		public Response getAllTestSteps(@PathParam("dName") String domainName, @PathParam("pName") String projectName , @PathParam("recordId") int tcRecordId){
			Project project=(Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
			try {
				TestCase testCase=	new TestCaseHandler().hydrateTestCase(project.getId(), tcRecordId);
				logger.info("TestSteps fetched successfully.");
				return RestUtils.buildResponse(Status.OK,"test.steps.fetched",testCase.getTcSteps().size(),testCase.getTcSteps());
			}catch (RecordNotFoundException e) {
				logger.error("testcase not found"+tcRecordId,e );
				return RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(),null,null);
			}catch (DatabaseException e) {
				logger.error("Could not fetch all TestSteps",e );
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null,null);
			} 
		}
	
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		@Path("/{dName}/projects/{pName}/testcases/{recordId}/steps/{sRecordId}")	
		public Response getSingleTestStep(@PathParam("dName") String domainName, @PathParam("pName") String projectName , @PathParam("recordId") int tcRecordId, @PathParam("sRecordId") int stepRecordId){
			Project project=(Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
			TestStep testStep=null;
			try {
				logger.info("Request to Get TestStep with ID [{}]", stepRecordId);
				testStep=stepHandler.getTestStep(tcRecordId, stepRecordId, project.getId());
				logger.info("TestStep fetched successfully.");
				return RestUtils.buildResponse(Status.OK,"test.step.fetched",null,testStep);
			}catch (RecordNotFoundException e) {
             logger.info("test step not found"+stepRecordId);
				return RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(),null,null);
		    }catch (DatabaseException e) {
		    	logger.error(e.getMessage());
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null,null);
			} 
		}

		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		@Path("/{dName}/projects/{pName}/testcases/{recordId}/steps")	
		public Response createTestStep(@PathParam("dName") String domainName, @PathParam("pName") String projectName, @PathParam("recordId") int tcRecordId, String jsonString) 
		{
			logger.info("Request to create teststep");						
			logger.info("Input JSON --> {}", jsonString);
			
			Project project=(Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
			TestStep testStep = new Gson().fromJson(jsonString, TestStep.class);
			testStep.setTestCaseRecordId(tcRecordId);
			try {
				stepHandler.persistTestStep(testStep,project.getId());
				logger.info("TestStep Created Successfully.");
				return RestUtils.buildResponse(Status.CREATED,"teststep.create.success",null,stepHandler.getTestStep(testStep.getRecordId()));
			} catch (DatabaseException e) {
				logger.info("error creating test step",e.getMessage());
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null,null);
			} catch (DuplicateRecordException e) {
				logger.error(e.getMessage(),e);
				return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),e.getTargetFields(),null);
			} catch (RequestValidationException e) {
				logger.error(e.getMessage(),e);
				return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),e.getTargetFields(),null);
			}
			catch (RecordNotFoundException e) {
				logger.error(e.getMessage(),e);
				return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),null,null);
			}
		}
		
		@PUT
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		@Path("/{dName}/projects/{pName}/testcases/{recordId}/steps/{id}")
		public Response updateTestStep(@PathParam("dName") String domainName, @PathParam("pName") String projectName, @PathParam("recordId") int tcRecId, @PathParam("id") int stepRecId, String jsonString) {
			logger.info("Request to update test step");
			logger.info("Input JSON --> {}", jsonString);
			logger.info("ID --> {}", stepRecId);
			User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
			Project project=(Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
			TestStep testStep = new Gson().fromJson(jsonString, TestStep.class);
			testStep.setRecordId(stepRecId);
			testStep.setTestCaseRecordId(tcRecId);
			/*Added by Preeti for TENJINCG-969 starts*/
			AuditRecord audit = new AuditRecord();
			audit.setEntityRecordId(testStep.getRecordId());
			audit.setLastUpdatedBy(user.getId());
			audit.setEntityType("teststep");
			testStep.setAuditRecord(audit);
			/*Added by Preeti for TENJINCG-969 ends*/
			try {
				stepHandler.update(testStep,project.getId());
				logger.info("TestStep updated Successfully.");
				return RestUtils.buildResponse(Status.OK,"Teststep.update.success",null,stepHandler.getTestStep(stepRecId));
			} catch (DatabaseException e) {
				logger.error(e.getMessage(),e);
				return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),null,null);
			}catch (DuplicateRecordException e) {
				logger.error(e.getMessage(),e);
				return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),e.getTargetFields(),null);
			} catch (RequestValidationException e) {
				logger.error(e.getMessage(),e);
				return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),e.getTargetFields(),null);
			}catch (RecordNotFoundException e) {
				logger.error(e.getMessage(),e);
				return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),null,null);
			}
			
		
		}
	
		@Path("/{dName}/projects/{pName}/testcases/{recordId}/steps/bulk")
		@PUT
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		
		public Response updateBulkTestStep(@PathParam("dName") String domainName, @PathParam("pName") String projectName, @PathParam("recordId") int tcRecordId, String jsonString){
			logger.info("Request to update bulk Teststeps");						
			logger.info("Input JSON --> {}", jsonString); 
			
			Project project=(Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
			User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
			Type listType = new TypeToken<ArrayList<TestStep>>(){}.getType();
			List<TestStep> testSteps = new Gson().fromJson(jsonString, listType);
			List<String> messagesList = new ArrayList<String>();
			List<TestStep> updatedTestStepList = new ArrayList<TestStep>();
			for(TestStep testStep:testSteps){
				try {
					testStep.setTestCaseRecordId(tcRecordId);
					/*Added by Preeti for TENJINCG-969 starts*/
					AuditRecord audit = new AuditRecord();
					audit.setEntityRecordId(testStep.getRecordId());
					audit.setLastUpdatedBy(user.getId());
					audit.setEntityType("teststep");
					testStep.setAuditRecord(audit);
					/*Added by Preeti for TENJINCG-969 ends*/
					this.stepHandler.update(testStep,project.getId());
					testStep=this.stepHandler.getTestStep(testStep.getRecordId());
					messagesList.add("TestStep '"+testStep.getId()+"' updated successfully!");
					updatedTestStepList.add(testStep);	
				}catch (DatabaseException e) {
					messagesList.add("TestStep '"+testStep.getId()+"' -- "+e.getMessage());
				}  catch (DuplicateRecordException e) {
					messagesList.add(e.getMessage());
				}
				catch (RequestValidationException e) {
					messagesList.add(e.getMessage()+Arrays.toString(e.getTargetFields()));
				} 
				catch (RecordNotFoundException e) {
					messagesList.add("TestStep '"+testStep.getId()+"' -- "+e.getMessage());
				}
			}
			return RestUtils.buildResponse(Status.OK, messagesList.toString(), updatedTestStepList);
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		@Path("/{dName}/projects/{pName}/testcases/{recordId}/steps/bulk")	
		public Response createBulkTestStep(@PathParam("dName") String domainName, @PathParam("pName") String projectName, @PathParam("recordId") int tcRecordId, String jsonString) throws TenjinServletException
		{
			logger.info("Request to Create bulk Teststeps");						
			logger.info("Input JSON --> {}", jsonString); 
			
			Project project=(Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
			Type listType = new TypeToken<ArrayList<TestStep>>(){}.getType();
			List<TestStep> testSteps = new Gson().fromJson(jsonString, listType);
			List<String> messagesList = new ArrayList<String>();
			List<TestStep> createdTestStepList = new ArrayList<TestStep>();
			for(TestStep testStep:testSteps){
				try {
					testStep.setTestCaseRecordId(tcRecordId);
					this.stepHandler.persistTestStep(testStep,project.getId());
					messagesList.add("TestStep '"+testStep.getId()+"' created successfully!");
					createdTestStepList.add(this.stepHandler.getTestStep(testStep.getRecordId()));	
				}catch (DatabaseException e) {
					messagesList.add("TestStep '"+testStep.getId()+"' -- "+e.getMessage());
				}  catch (DuplicateRecordException e) {
					messagesList.add(e.getMessage());
				}
				catch (RequestValidationException e) {
					messagesList.add("TestStep '"+testStep.getId()+"' -- "+e.getMessage()+Arrays.toString(e.getTargetFields()));
				} 
				catch (RecordNotFoundException e) {
					messagesList.add("TestStep '"+testStep.getId()+"' -- "+e.getMessage());
				}
			}
			return RestUtils.buildResponse(Status.OK, messagesList.toString(), createdTestStepList);
		}
		
		@DELETE
		@Produces(MediaType.APPLICATION_JSON)
		@Path("/{dName}/projects/{pName}/testcases/{recordId}/steps/{id}")
		public Response deleteTestStep(@PathParam("dName") String domainName, @PathParam("pName") String projectName, @PathParam("recordId") int tcRecordId, @PathParam("id") int stepRecId){
			Project project=(Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
			User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
			logger.info("request to delete teststep");
			try {
				/*Modified by Preeti for TENJINCG-1003 starts*/
				/*this.stepHandler.deleteTestStep(new String[] {String.valueOf(stepRecId)},tcRecordId,project.getId());*/
				this.stepHandler.deleteTestStep(new String[] {String.valueOf(stepRecId)},tcRecordId,project.getId(),user.getId());
				/*Modified by Preeti for TENJINCG-1003 ends*/
				logger.info("Test step deleted Successfully");
				return RestUtils.buildResponse(Status.OK, "testStep.delete.success",null,null);
			} catch (DatabaseException e) {
				logger.info(e.getMessage());
				return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null,null);
			}catch (RecordNotFoundException e) {
				logger.info(e.getMessage());
				return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null,null);
			}catch (RequestValidationException e) {
				logger.info(e.getMessage());
				return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),null,null);
			}
			
		
		}
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		@Path("/{dName}/projects/{pName}/testcases/{recordId}/steps/{sRecordId}/runs")
		public Response getRunsForTeststep(@PathParam("dName") String domainName, @PathParam("pName") String projectName , @PathParam("recordId") int tcRecordId, @PathParam("sRecordId") int tStepRecId){
			
			//Fix for T25IT-197
			try {
				TestStep step = this.stepHandler.getTestStep(tStepRecId);
				if(step == null) {
					return RestUtils.buildResponse(Status.NOT_FOUND, "Test step does not exist in this project", null);
				}
				
				List<TestRun> runs = new RunHelper().getAllRunsForTestStep(tStepRecId);
				if(runs == null || runs.size() < 1) {
					return RestUtils.buildResponse(Status.BAD_REQUEST, "This test step has not been executed yet.", null);
				}else {
					return RestUtils.buildResponse(Status.OK, runs.size() + " runs found for this test step", runs);
				}
			} catch (DatabaseException e) {
				logger.error("ERROR fetching runs for test step", e);
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "An internal error occurred. Please contact Tenjin Support.", null);
			}
		}
		
		// TENJINCG-459
		@POST
	    @Path("/{dName}/projects/{pName}/testcases/{testCaseId}/steps/{testStepId}/execute")
	    @Produces(MediaType.APPLICATION_JSON)
	    @Consumes(MediaType.APPLICATION_JSON)
		public Response executeTestStep(@PathParam("dName") String domainName, @PathParam("pName") String projectName, @PathParam("testCaseId") String testCaseId, @PathParam("testStepId") String testStepId, TestStepExecutorInput input) {
			User user = (User) requestContext.getProperty(RestContextProperties.TJN_USER);
			Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);

			logger.info("Recieved request to execute test step");
			
			input.setTestCaseId(testCaseId);
			input.setTestStepId(testStepId);
			RestExecutionHandler handler = new RestExecutionHandler(input, user, project);
			try {
				handler.validateTestStepExecutionInput();
			} catch (RestRequestException e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			} catch (TenjinServletException e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			}


			try {
				int runId = handler.startRun();
				return RestUtils.buildResponse(Status.OK, "Execution initiated successfully!", runId);
			} catch (TenjinServletException e) {
				return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
			}
		}
}	