/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RestExecutionHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 20-Jun-2017		Sriram Sridharan  			New File added for TENJINCG-235
 * 18-Aug-2017      Padmavathi                  T25IT-204
   17-Nov-2017			Sriram Sridharan			TENJINCG-458
* 17-11-2017			Sriram Sridharan		TENJINCG-459
* 03-05-2018			Pushpalatha				TENJINCG-350
* 19-06-2018            Padmavathi              T251IT-83
* 21-12-2018            Padmavathi              TJN262R2-14
* 24-06-2019            Padmavathi              for license
 *
 */

package com.ycs.tenjin.rest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.BridgeProcessor;
import com.ycs.tenjin.bridge.pojo.TaskManifest;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.TestCaseHelper;
import com.ycs.tenjin.db.TestSetHelper;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.ExecutorGateway;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

public class RestExecutionHandler {
	private static final Logger logger = LoggerFactory.getLogger(RestExecutionHandler.class);
	
	private ExecutorInput executorInput;
	private String userId;
	private Project project;
	private ExecutorGateway gateway;
	private RegisteredClient client;
	private User user;
	/*Added by padmavathi for T251IT-83(for differentiating type of test) starts*/
	private String testType;
	/*Added by padmavathi for T251IT-83(for differentiating type of test) ends*/
	public RestExecutionHandler(ExecutorInput executorInput, User user, Project project) {
		this.executorInput = executorInput;
		this.userId = user.getId();
		this.user = user;
		this.project = project;
	}

	public void validateTestSetExecutionInput() throws RestRequestException, TenjinServletException, DatabaseException {
		logger.info("Validating execution input for Test Set Execution");
		
		/*this.validateProject();*/
		logger.info("Validating Test Set Information");
		if(this.executorInput.getTestSetRecordId() < 1) {
			try {
				TestSet testSet = new TestSetHelper().hydrateTestSet(this.executorInput.getTestSetName(), this.project.getId());
				if(testSet == null) {
					throw new RestRequestException("Invalid Test Set ["+this.executorInput.getTestSetName()+"]. Please review your input and try again.");
				}
				/*Added by padmavathi for T251IT-83 starts*/
				this.testType=testSet.getMode();
				/*Added by padmavathi for T251IT-83 ends*/
				this.executorInput.setTestSetRecordId(testSet.getId());
			} catch (DatabaseException e) {
				
				logger.error("ERROR validating test set name", e);
				throw new TenjinServletException("Could not validate Test Set [" + this.executorInput.getTestSetName() + "]. Please contact Tenjin Support.");
			}
		}else{
			TestSet testSet=new TestSetHelper().hydrateTestSet(this.executorInput.getTestSetRecordId(), this.project.getId());
			/*Added by padmavathi for T251IT-83 starts*/
			this.testType=testSet.getMode();
			/*Added by padmavathi for T251IT-83 ends*/
		}
		
		this.validateClient();
		this.initializeGateway();
	}
	
	public int startRun() throws TenjinServletException {
		logger.info("Creating Run");
		this.gateway.createRun();
		
		logger.info("Run created with ID [{}]", this.gateway.getRun().getId());
		
		logger.info("Creating extended task");
		TaskManifest manifest = this.gateway.getTaskManifest();
		BridgeProcessor adapterHandler = new BridgeProcessor(manifest);
		adapterHandler.createTask();
		
		logger.info("Running task");
		adapterHandler.runTask();
		
		return this.gateway.getRun().getId();
	}
	
	private void initializeGateway() throws TenjinServletException {
		String targetIp = "";
		int port = 0;
		
		if(this.client != null) {
			targetIp = this.client.getHostName();
			port = Integer.parseInt(this.client.getPort());
		}
		
		logger.info("Initializing gateway");
		TenjinSession tjnSession = new TenjinSession();
		tjnSession.setProject(this.project);
		tjnSession.setUser(this.user);
		/*Added by Padmavathi for TJN262R2-14 starts*/
		int screenShotOption=0;
		try{
			screenShotOption=Integer.parseInt(this.project.getScreenshotoption());
		}catch(NumberFormatException e){
			logger.error(e.getMessage());
		}
		/*Added by Padmavathi for TJN262R2-14 ends*/
		try {
			/*Modified by Padmavathi for TJN262R2-14 starts*/
			this.gateway = new ExecutorGateway(tjnSession, this.executorInput.getTestSetRecordId(), this.executorInput.getBrowserType(), targetIp, port, TenjinConfiguration.getProperty("OEM_TOOL"), 0, screenShotOption, "N");
			/*Modified by Padmavathi for TJN262R2-14 ends*/
		} catch (TenjinConfigurationException e) {
			
			throw new TenjinServletException(e.getMessage());
		} 
		
		logger.info("Performing all validations");
		this.gateway.performAllValidations();
		/*Added by Padmavathi for license starts*/
		Set<String> al =null;
		String msg = "";
		try {
			Map<String, String> adapterLicenseMap = gateway.validateAdapterCredentials(al);
			if (adapterLicenseMap != null && adapterLicenseMap.size() > 0) {
				msg = "";
				for (String key : adapterLicenseMap.keySet()) {
					msg = msg + key + " - " + adapterLicenseMap.get(key) + "\n";
				}
				logger.error("Adapter license Validation failed\n [{}]", msg);
				throw new TenjinServletException(msg);
			}
		} catch (TenjinConfigurationException e) {
			throw new TenjinServletException(e.getMessage());
		}
		/*Added by Padmavathi for license ends*/
	}
	
	private void validateClient() throws RestRequestException, TenjinServletException {
		logger.info("Validating client information");
		/*Added by padmavathi for T251IT-83 starts*/
		/*Added by padmavathi for T251IT-83 ends*/
		if(Utilities.trim(this.executorInput.getClientName()).length() > 0) {
			try {
				this.client = new ClientHelper().hydrateClient(this.executorInput.getClientName());
				if(this.client == null) {
					throw new RestRequestException("Client [" + this.executorInput.getClientName() + "] was not found. Please review your input and try again.");
				}
			} catch (DatabaseException e) {
				
				logger.error("ERROR - Could not validate client information", e);
				throw new TenjinServletException("Could not validate client [" + this.executorInput.getClientName() + "]. Please contact Tenjin Support.");
			}
		}
		/*Added by Padmavathi for T25IT-204 starts*/
		
	}
	
	// TENJINCG-458
	TestCaseExecutorInput tcExecutorInput = null;
	public RestExecutionHandler(TestCaseExecutorInput executorInput, User user, Project project) {
		this.tcExecutorInput = executorInput;
		this.userId = user.getId();
		this.user = user;
		this.project = project;
	}
	
	
	public void validateTestCaseExecutionInput() throws RestRequestException, TenjinServletException {
		logger.info("Validating Test Case [{}] for execution", this.tcExecutorInput.getTestCaseId());
		
		boolean searchByRecordId = false;
		int testCaseRecordId = 0;
		try {
			testCaseRecordId = Integer.parseInt(this.tcExecutorInput.getTestCaseId());
			searchByRecordId = true;
		} catch(NumberFormatException e) {
			logger.warn("Test Case Record ID is not passed.");
		}
		
		TestCase targetTestCase = null;
		
		try {
			if(searchByRecordId) {
				logger.info("Searching test case with record ID {}", testCaseRecordId);
				targetTestCase = new TestCaseHelper().hydrateTestCase(this.project.getId(), testCaseRecordId);
				}else{
				logger.info("Searching test case with ID {}", this.tcExecutorInput.getTestCaseId());
				targetTestCase = new TestCaseHelper().hydrateTestCase(this.project.getId(), this.tcExecutorInput.getTestCaseId());
			
			}
		} catch (DatabaseException e) {
			logger.error("ERROR loading Test Case details: ", e);
			throw new TenjinServletException("Could not load test case information due to an internal error. Please contact Tenjin Support.");
		}
		
		if(targetTestCase == null) {
			/*Changed by Pushpalatha for TENJINCG-350 starts*/
			throw new RestRequestException("Test Case [" + this.tcExecutorInput.getTestCaseId() + "] was not found in this project. Please review your request and try again.");
			/*Changed by Pushpalatha for TENJINCG-350 ends*/
		}
		/*Added by padmavathi for T251IT-83 starts*/
		this.testType=targetTestCase.getMode();
		/*Added by padmavathi for T251IT-83 ends*/
		logger.info("Creating Ad-Hoc Test Set");
		TestSet set = this.createAdHocTestSetForTestCase(targetTestCase);
		
		this.executorInput = new ExecutorInput();
		this.executorInput.setTestSetRecordId(set.getId());
		this.executorInput.setClientName(this.tcExecutorInput.getClientName());
		this.executorInput.setBrowserType(this.tcExecutorInput.getBrowserType());
		
		logger.info("Performing necessary validations");
		this.validateClient();
		this.initializeGateway();
	}
	
	
	private TestSet createAdHocTestSetForTestCase(TestCase testCase) throws TenjinServletException {
		TestSet t = new TestSet();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_hhmmss");
		
		/*Changed by leelaprasad for TJNUN262-53 starts*/
		/*t.setName("AdHoc_Test" + "_" + sdf.format(new Date()));*/
		t.setName("Test" + "_" + sdf.format(new Date()));
		/*Changed by leelaprasad for TJNUN262-53 ends*/
		t.setMode(testCase.getMode());
		ArrayList<TestCase> tests = new ArrayList<TestCase>();
		tests.add(testCase);
		t.setTests(tests);
		t.setDescription("Auto generated test set");
		t.setType("Functional");
		t.setRecordType("TSA");
		t.setParent(Integer.parseInt("1"));
		t.setPriority("Medium");
		t.setCreatedBy(this.user.getId());
		
		try {
			new TestSetHelper().persistTestSet(t, this.project.getId());
			
			new TestSetHelper().persistTestSetMap(t.getId(), String.valueOf(testCase.getTcRecId()), this.project.getId(), true);
			
		} catch (DatabaseException e) {
			logger.error("ERROR creating ad-hoc test set", e);
			throw new TenjinServletException("Could not initialize execution due to an internal error", e);
		}
		return t;
	}
	
	// TENJINCG-458 ends
	
	// TENJINCG-459
	TestStepExecutorInput stepExecutorInput = null;
	public RestExecutionHandler(TestStepExecutorInput executorInput, User user, Project project) {
		this.stepExecutorInput = executorInput;
		this.userId = user.getId();
		this.user = user;
		this.project = project;
	}
	public void validateTestStepExecutionInput() throws RestRequestException, TenjinServletException {

		logger.info("Validating Test Case [{}] for execution", this.stepExecutorInput.getTestCaseId());
		
		boolean searchByRecordId = false;
		int testCaseRecordId = 0;
		try {
			testCaseRecordId = Integer.parseInt(this.stepExecutorInput.getTestCaseId());
			searchByRecordId = true;
		} catch(NumberFormatException e) {
			logger.warn("Test Case Record ID is not passed.");
		}
		
		TestCase targetTestCase = null;
		
		try {
			if(searchByRecordId) {
				logger.info("Searching test case with record ID {}", testCaseRecordId);
				targetTestCase = new TestCaseHelper().hydrateTestCase(this.project.getId(), testCaseRecordId);
			}else{
				logger.info("Searching test case with ID {}", this.stepExecutorInput.getTestCaseId());
				targetTestCase = new TestCaseHelper().hydrateTestCase(this.project.getId(), this.stepExecutorInput.getTestCaseId());
			}
		} catch (DatabaseException e) {
			logger.error("ERROR loading Test Case details: ", e);
			throw new TenjinServletException("Could not load test case information due to an internal error. Please contact Tenjin Support.");
		}
		if(targetTestCase == null) {
			throw new RestRequestException("Test Case [" + this.stepExecutorInput.getTestCaseId() + "] was not found in this project. Please review your request and try again.");
		}
		/*Added by padmavathi for T251IT-83 starts*/
		this.testType=targetTestCase.getMode();
		/*Added by padmavathi for T251IT-83 ends*/
		logger.info("Validating Test Step [{}] for execution", this.stepExecutorInput.getTestStepId());
		int stepRecordId = 0;
		boolean stepRecordIdPassed = false;
		try {
			stepRecordId = Integer.parseInt(this.stepExecutorInput.getTestStepId());
			stepRecordIdPassed = true;
		}catch(NumberFormatException e) {
			logger.warn("Step record ID is not passed");
		}
		
		TestStep step = null;
		
		try {
			if(stepRecordIdPassed) {
				step = new TestCaseHelper().hydrateTestStepDetailsForTestCase(this.project.getId(), stepRecordId, targetTestCase.getTcRecId());
			}else{
				step = new TestCaseHelper().hydrateTestStepDetailsForTestCase(this.project.getId(), this.stepExecutorInput.getTestStepId(), targetTestCase.getTcRecId());
			}
		} catch (DatabaseException e) {
			logger.error("ERROR validating test step", e);
			throw new TenjinServletException("Could not validate step [" + this.stepExecutorInput.getTestStepId() + "] due to an internal error. Please contact Tenjin Support.");
		}
		
		if(step ==  null) {
			throw new RestRequestException("Test Step [" + this.stepExecutorInput.getTestStepId() + "] was not found under test case [" + this.stepExecutorInput.getTestCaseId() + "] under domain ["+ this.project.getDomain() +"], project ["+this.project.getName()+"]. Please review your request and try again.");
		}
		
		logger.info("Creating Ad-Hoc Test Set");
		TestSet set = this.createAdHocTestSetForTestCase(targetTestCase);
		
		this.executorInput = new ExecutorInput();
		this.executorInput.setTestSetRecordId(set.getId());
		this.executorInput.setClientName(this.stepExecutorInput.getClientName());
		this.executorInput.setBrowserType(this.stepExecutorInput.getBrowserType());
		
		logger.info("Performing necessary validations");
		this.validateClient();
		this.initializeGateway(step);
	
	}
	
	private void initializeGateway(TestStep stepToExecute) throws TenjinServletException {
		this.initializeGateway();
		List<TestStep> stepsToExecute = new ArrayList<TestStep>();
		stepsToExecute.add(stepToExecute);
		this.gateway.setStepsToExecute(stepsToExecute);
	}
	// TENJINCG-459 ends
	
}
