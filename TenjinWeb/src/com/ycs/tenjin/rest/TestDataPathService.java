

/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectTestDataPathResponse.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 30-Nov-2017		       Leelaprasad		   		Newly added for TENJINCG-539
* 12-02-2018           Padmavathi              for TENJINCG-545
* 19-02-2018           Padmavathi              for TENJINCG-545
*/
package com.ycs.tenjin.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.handler.TestDataPathHandler;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.ProjectTestDataPath;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.servlet.TenjinServletException;



@Path("/domains")
public class TestDataPathService {

	private static final Logger logger = LoggerFactory.getLogger(TestDataPathService.class);
	/*	Added by Padmavathi for TENJINCG-540 starts*/
	TestDataPathHandler handler=new TestDataPathHandler();
	/*	Added by Padmavathi for TENJINCG-540 ends*/
	@Context
	ContainerRequestContext requestContext;
	
	@Context
	SecurityContext securityContext;
	@Path("/{dName}/projects/{pName}/testdatapaths")
	/*	Added by Padmavathi for TENJINCG-540 starts*/
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	/*Modified by Padmavathi for adding @QueryParam for group starts */
	public Response getProjectTestData(@PathParam("dName") String domainName,@PathParam("pName") String projectName,
			@QueryParam("appId") int appId,@QueryParam("funcCode") String funcCode,@QueryParam("groupName") String groupName ){
	/*Modified by Padmavathi for adding @QueryParam for group ends */
		Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		List<ProjectTestDataPathResponse> projectTestDataPathResponses = new ArrayList<ProjectTestDataPathResponse> ();
		String status=null;
		String message=null;
		ProjectTestDataPath projectTestDataPath=new ProjectTestDataPath();
		projectTestDataPath.setProjectId(project.getId());
		projectTestDataPath.setAppId(appId);
		projectTestDataPath.setFuncCode(funcCode);
		/*	Added by Padmavathi for TENJINCG-545 starts*/
		projectTestDataPath.setGroupName(groupName);
		/*	Added by Padmavathi for TENJINCG-545 ends*/
		
		try {
			List<ProjectTestDataPath> projectTestDataPaths=this.handler.getProjectTestData(projectTestDataPath);
			if(projectTestDataPaths!=null&&projectTestDataPaths.size()>0){
				for(ProjectTestDataPath testDataPath:projectTestDataPaths){
					ProjectTestDataPathResponse path=ProjectTestDataPathResponse.create(testDataPath, status, message);
					projectTestDataPathResponses.add(path);
				}
			}
			else{
				return RestUtils.buildResponse(Status.NOT_FOUND, "records not found",null);
			}
		
		} catch (TenjinServletException e) {
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(),null);
		} 
		catch(RecordNotFoundException e){
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(),null);
		}
		catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(),null);
		} /*	Added by Padmavathi for TENJINCG-545 starts*/
		catch (DuplicateRecordException e) {
			return RestUtils.buildResponse(Status.CONFLICT, e.getMessage(),null);
		} 
		/*	Added by Padmavathi for TENJINCG-545 ends*/
		return RestUtils.buildResponse(Status.OK, "TestDataPath fetched successfully.", projectTestDataPathResponses);
		
	}
	/*	Added by Padmavathi for TENJINCG-540 ends*/
	@Path("/{dName}/projects/{pName}/testdatapaths")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response testDataPaths(@PathParam("dName") String dname,@PathParam("pName") String pname,List<ProjectTestDataPath> testDataPaths){
		
		if(testDataPaths == null || testDataPaths.size() < 1) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, "Request data is empty. Please review your request and try again.", null);
		}
		Project project = (Project) requestContext.getProperty(RestContextProperties.TJN_PROJECT);
		logger.info("Request to update testdatapath");	
	
		
		List<ProjectTestDataPathResponse> projectTestDataPathResponses = new ArrayList<ProjectTestDataPathResponse> ();
		
		for(ProjectTestDataPath testDataPath : testDataPaths) {
			try {
				
				
			/*Changed by leelaprasad for testdata path TENJINCG-549 starts*/
			String exmTestDataPath=handler.prepareTestDataPath(testDataPath.getTestDataPath());
			testDataPath.setTestDataPath(exmTestDataPath);
			/*Changed by leelaprasad for testdata path TENJINCG-549 starts*/
			
				testDataPath.setProjectId(project.getId());
				handler.updateTestDataPath(testDataPath);
				testDataPath.setTestDataPath(testDataPath.getTestDataPath().replaceAll("\\\\\\\\", Matcher.quoteReplacement("\\")));
				projectTestDataPathResponses.add(ProjectTestDataPathResponse.create(testDataPath, "OK", "Test Data Path updated successfully."));
			} catch (TenjinServletException e) {
				projectTestDataPathResponses.add(ProjectTestDataPathResponse.create(testDataPath, "ERROR", e.getMessage()));
			} catch (DatabaseException e) {
				projectTestDataPathResponses.add(ProjectTestDataPathResponse.create(testDataPath, "ERROR", "An internal error occurred. Please contact Tenjin Support."));
			}catch(RecordNotFoundException re){
				projectTestDataPathResponses.add(ProjectTestDataPathResponse.create(testDataPath, "ERROR", "function does not exist under this application"));
			}
		}
		return RestUtils.buildResponse(Status.OK, "Bulk operation is complete. Please review the response body for status of Individual testdatapaths.", projectTestDataPathResponses);
	}
	
}
