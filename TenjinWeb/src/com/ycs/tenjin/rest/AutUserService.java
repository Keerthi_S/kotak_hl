/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AutUserService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                	CHANGED BY             		DESCRIPTION
* 19-11-2018			Ashiki						Re-written for TENJINCG-846
* 20-11-2018            Padmavathi                  for regression pack issues
* 
*/


package com.ycs.tenjin.rest;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.bridge.pojo.aut.ApplicationUser;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.ApplicationUserHandler;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.user.User;



@Path("/auts/{id}/credentials")
public class AutUserService {	
	private static final Logger logger = LoggerFactory
			.getLogger(UserService.class);
	
	@Context
	ContainerRequestContext requestContext;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllAutUnderUser(@PathParam("id") String appId) {
		logger.info("Request to get credentials for Application-"+appId);
		
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		Response response = null;
		
		try {
		ArrayList<ApplicationUser> applicationUser =  new ApplicationUserHandler().getAllApplicationUser(user.getId(),Integer.parseInt(appId));
		
		if(applicationUser.size()>0)
			
		response = RestUtils.buildResponse(Status.OK,
						"Application user fetched Successfully for user-"+user.getId(), applicationUser);
		else
			response = RestUtils.buildResponse(Status.NOT_FOUND,
					"Application user not available for user-"+user.getId(), applicationUser);
	
		} catch (DatabaseException e) {
			
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,
					e.getMessage(), null);
		}
		return response;
	}
	
	@Path("/{type}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllAutUnderUserType(@PathParam("id") String appId, @PathParam("type") String type) {
		logger.info("Request to get credentials for Application-"+appId);
		
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		Response response = null;
		 
		try {
			ApplicationUser applicationUser =  new ApplicationUserHandler().getApplicationUser(user.getId(),Integer.parseInt(appId),type);
			
		if(applicationUser!=null)
			
		response = RestUtils.buildResponse(Status.OK,
						"Application User credentials fetched Successfully for user-"+user.getId(), applicationUser);
		else
			response = RestUtils.buildResponse(Status.NOT_FOUND,
					"Application User credentials not available for user-"+user.getId()+"("+type+")", applicationUser);
	
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,
					e.getMessage(), null);
		}
		return response;
	}
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createAutUser( String jsonString, @PathParam("id") String appId) {
		logger.info("Request to create Application user");
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		ApplicationUser applicationUser = new Gson().fromJson(jsonString, ApplicationUser.class);
		applicationUser.setUserId(user.getId());
		applicationUser.setAppId(Integer.parseInt(appId));
		ApplicationUserHandler handler=new ApplicationUserHandler();
		
		try {
			handler.persistApplicationUser(applicationUser);
			logger.info("Application User created Successfully");
			return RestUtils.buildResponse(Status.CREATED, "Application User Created Successfully", applicationUser);
		}
			catch (DuplicateRecordException e) {
				logger.error(e.getMessage());
				/*Changed by padmavathi to show proper status(if aut user is already exits showing status conflict instead of found*/
				//return RestUtils.buildResponse(Status.FOUND,e.getMessage(), null);
				return RestUtils.buildResponse(Status.CONFLICT, e.getMessage(), e.getTargetFields(), null);
				/*Changed by padmavathi to show proper status ends*/
				
			}catch (DatabaseException e) {
				logger.error(e.getMessage());
				return RestUtils.buildResponse(Status.NOT_FOUND,e.getMessage(), null);
			
			}catch (RequestValidationException e) {
				return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(),e.getTargetFields(),null);
			}
	}
	
	
	@Path("/{type}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateAutUser(String jsonString,@PathParam("id") String appId,@PathParam("type") String type) {
		
		logger.info("Request to update Application user");
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		ApplicationUser userEnteredAut = new Gson().fromJson(jsonString, ApplicationUser.class);
		/*Added by Padmavathi to set login user type in appliction user*/
		userEnteredAut.setLoginUserType(type);
		/*Added by Padmavathi ends*/
		ApplicationUserHandler handler=new ApplicationUserHandler();
		try {
			ApplicationUser applicationuser = handler.updateApplicationUserService(userEnteredAut,Integer.parseInt(appId), user.getId(),type);
			return RestUtils.buildResponse(Status.OK, "Application user updated Sucessfully",applicationuser);
		} catch (RequestValidationException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),e.getTargetFields(),null);
		} catch (DatabaseException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),null);
		}catch (RecordNotFoundException e) {
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(),null);
		}
		
	}
	
	@Path("/{type}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteAutUnderSpecificUser(@PathParam("id") String appId, @PathParam("type") String type) {
		logger.info("Request to delete Application user ("+type+") for Application "+appId);
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		ApplicationUserHandler handler=new ApplicationUserHandler();
		try {
		handler.deleteApplicationUser(Integer.parseInt(appId), user.getId(),type);
		return RestUtils.buildResponse(Status.OK, "Application User Deleted Successfully",null);
		} 
		catch (RecordNotFoundException e){
		return RestUtils.buildResponse(Status.BAD_REQUEST,e.getMessage(), null);
		}
		catch (DatabaseException e) {
		return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,e.getMessage(), null);
		}
		
	
}
}