/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Regclientservice.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY             	 	DESCRIPTION
 * 7-nov-2016			latief						Added by latief
   05-Dec-2016          Gangadhar Badagi        	Removed empty path annoatation
   17-Dec-2016          Leelaprasad            		Defect fix#TEN-98,TEN-99,TEN-100
   10-Aug-2017			Manish						T25IT-95
   11-Aug-2017          Leelaprasad                 T25IT-112
   17-Aug-2017        	Gangadhar Badagi            T25IT-93
   17-Aug-2017         	Sriram Sridharan           	T25IT-127
 * 19-Aug-2017			Sriram Sridharan			Fix for T25IT-119
 * 11-10-2017           Gangadhar Badagi        	TENJINCG-368
 * 19-Oct-2017			Sriram				   		TENJINCG-397 (Entire file changed)
 * 17-11-2017			Preeti						TENJINCG-474
 * 20-11-2017			Preeti						TENJINCG-480
 * 20-11-2017			Padmavathi					TENJINCG-489'
 * 23-Nov-2017        	Gangadhar Badagi            TENJINCG-522
 * 23-Nov-2017        	Gangadhar Badagi            TENJINCG-522(Revert back the changes) 
 */

package com.ycs.tenjin.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.handler.ClientHandler;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

@Path("/clients")
public class Regclientservice{
	
	@Context
	ContainerRequestContext requestContext;

	@Context
	SecurityContext securityContext;
	
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	private ClientHandler clientHandler = new ClientHandler();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllClients() {
		try {
			List<RegisteredClient> clients = this.clientHandler.getAllClients();
			if(clients != null && clients.size() > 0) {
				return RestUtils.buildResponse(Status.OK, "client.list.success", clients.size(), clients);
			}else {
				return RestUtils.buildResponse(Status.NOT_FOUND, "client.list.not.found", null);
			}
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "generic.db.error", null, null);
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{clientName}")
	public Response getClientByName(@PathParam("clientName") String clientIdentifier) {
		
		try {
			RegisteredClient client = null;
			if(Utilities.isNumeric(clientIdentifier)) {
				client = this.clientHandler.getClient(Integer.valueOf(clientIdentifier));
			}else{
				client = this.clientHandler.getClient(clientIdentifier);
			}
			return RestUtils.buildResponse(Status.OK, "client.found", null, client);
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "generic.db.error", null, null);
		} catch(RecordNotFoundException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null, null);
		}
	}
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createClient(RegisteredClient client) {
		User user = (User) requestContext.getProperty(RestContextProperties.TJN_USER);
		try {
			this.clientHandler.persist(client, user.getId());
			logger.info("Registered a new client with ID {}", client.getRecordId());
			RegisteredClient newClient =  this.clientHandler.getClient(client.getRecordId());
			return RestUtils.buildResponse(Status.CREATED, "client.create.success", newClient.getRecordId(), newClient);
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "generic.db.error", null, null);
		} catch (DuplicateRecordException e) {
			logger.error(e.getMessage(),e);
			/*Changed by Gangadhar Badagi for TENJINCG-522(Revert back the changes) starts*/
			/*Changed by Gangadhar Badagi for TENJINCG-522 starts*/
			/*return RestUtils.buildResponse(Status.CONFLICT, e.getMessage(), e.getTargetFields(), null);*/
			/*return RestUtils.buildResponse(Status.FOUND, e.getMessage(), e.getTargetFields(), null);*/
			/*Changed by Gangadhar Badagi for TENJINCG-522 ends*/
			return RestUtils.buildResponse(Status.CONFLICT, e.getMessage(), e.getTargetFields(), null);
			/*Changed by Gangadhar Badagi for TENJINCG-522(Revert back the changes)  ends*/
		}catch (RequestValidationException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), e.getTargetFields(), null);
		} catch (ResourceConflictException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.CONFLICT, e.getMessage(), e.getTargetFields(), null);
		} catch (TenjinConfigurationException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null);
		}
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{recordId}")
	public Response updateClient(RegisteredClient client, @PathParam("recordId") int recordId) {
		
		try {
			client.setRecordId(recordId);
			this.clientHandler.update(client);
			RegisteredClient newClient =  this.clientHandler.getClient(client.getRecordId());
			return RestUtils.buildResponse(Status.OK, "client.update.success", newClient.getRecordId(), newClient);
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "generic.db.error", null, null);
		} catch (DuplicateRecordException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.CONFLICT, e.getMessage(), e.getTargetFields(), null);
		}catch (RequestValidationException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), e.getTargetFields(), null);
		}
        /*Added by Padmavathi for TENJINCG-489 starts*/
		catch (RecordNotFoundException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.BAD_REQUEST, e.getMessage(), null,null);
		}
		   /*Added by Padmavathi for TENJINCG-489 ends*/
		
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{recordId}")
	public Response deleteClient(@PathParam("recordId") int recordId) {
		try {
			this.clientHandler.deleteClient(recordId);
			logger.info("Client with record ID {} deleted ", recordId);
			/*Modified by Preeti for TENJINCG-474 starts*/
			return RestUtils.buildResponse(Status.OK, "client.delete.success",recordId, null);
			/*Modified by Preeti for TENJINCG-474 ends*/
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR, "generic.db.error", null, null);
		} catch (ResourceConflictException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.CONFLICT, e.getMessage(), null, null);
		} 
		/*Added by Preeti for TENJINCG-480 starts*/
		catch(RecordNotFoundException e) {
			logger.error(e.getMessage(),e);
			return RestUtils.buildResponse(Status.NOT_FOUND, e.getMessage(), null, null);
		}
		/*Added by Preeti for TENJINCG-480 ends*/
	}
	
	
}
