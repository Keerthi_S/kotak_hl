/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectValidator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 22-nov-2016			prafful tiwari			new file added
 * 05-Dec-2016			Sriram					Added validateProject() method for TJN_243_02
 * 07-Dec-2016			Sriram					Added validateProject(domainName, projectName, userName) method for TJN_243_02
 * 19-Aug-2017			Roshni					T25IT-156,157,158
 * 18-01-2018           Padmavathi              for TENJINCG-576
 * 19-01-2018           Padmavathi              for TENJINCG-576
 * 16-03-2018		  	Preeti				   	TENJINCG-615
 * 23-04-2018           Padmavathi              for displaying proper error message	
 * 02-05-2018			Preeti					TENJINCG-656
 */
package com.ycs.tenjin.rest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class ProjectValidator {
	private static final Logger logger = LoggerFactory.getLogger(ProjectHelper.class);

	
	/* Modified by Roshni for T25IT-156,157,158 starts */
	/*******Added by Gangadhar Badagi to update project starts**************/
	public Project updateFields(Project project) throws DatabaseException{
		Project prj=new ProjectHelper().hydrateProject(project.getId());
		
		/*if(Utilities.trim(project.getName().trim()).equalsIgnoreCase(""))*/
		if(project.getName()==null||Utilities.trim(project.getName().trim()).equalsIgnoreCase("")){
			 project.setName(prj.getName());
		}
		if(project.getDescription()==null||Utilities.trim(project.getDescription().trim()).equalsIgnoreCase("")){
			if(prj.getDescription()==null||Utilities.trim(prj.getDescription().trim()).equalsIgnoreCase(""))
			{
			 project.setDescription("");
			}
			else
				 project.setDescription(prj.getDescription().trim());
		}
		if(project.getType()==null||Utilities.trim(project.getType().trim()).equalsIgnoreCase("")){
			project.setType(prj.getType().trim());
		}
		if(project.getScreenshotoption()==null||Utilities.trim(project.getScreenshotoption().trim()).equalsIgnoreCase("")){
			project.setScreenshotoption(prj.getScreenshotoption().trim());
		}
		
		return project;
	}
	/*******Added by Gangadhar Badagi to update project ends*******/
		/* Modified by Roshni for T25IT-156,157,158 ends */
	
	/*******Added by Gangadhar Badagi to validate project starts
	 * @throws DatabaseException ******/
	public boolean projectAvailability(String prjName,String domain) throws DatabaseException {
		Connection conn=null;
		/*modified by paneendra for sql injection starts*/
		PreparedStatement pst = null;
		ResultSet rs=null;
		try {
			 conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			
			 pst=conn.prepareStatement("select PRJ_NAME from TJN_PROJECTS where PRJ_DOMAIN=?");
				pst.setString(1, domain);
				rs=pst.executeQuery();
				/*modified by paneendra for sql injection ends*/
			while(rs.next()){
				String projectName=rs.getString("PRJ_NAME");
				if(projectName.trim().equals(prjName)){
					return true;
				}
			}
		}
		catch(SQLException e){
			logger.error("exception occured while fetching project");
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("Error ", e);
				}
			}
		}
		
	return false;	
	}
/****	Added by Gangadhar Badagi to validate project ends******/

	/*******Added by Gangadhar Badagi to validate domain starts
	 * @throws TenjinServletException ******/
	
	
	public void validateProjectAndDomain(Project project) throws TenjinServletException{
		
		
		if(Utilities.trim(project.getName()).equalsIgnoreCase("")){
			logger.error("Project name is mandatory");
			throw new TenjinServletException("Project name is mandatory");
		}
		if(Utilities.trim(project.getDomain()).equalsIgnoreCase("")){
			logger.error("Domain is mandatory");
			throw new TenjinServletException("Domain [domain] is mandatory");
			}
	}
	//Added by Gangadhar Badagi to validate domain ends
	/***********
	 * Added by Sriram for TJN_243_02
	 */
	public Project validateProject(String domainName, String projectName) throws TenjinServletException, DatabaseException{
		
		logger.debug("entered validateProject({}, {})", domainName, projectName);
		
		Project project = new ProjectHelper().hydrateProject(domainName, projectName);
		if(project == null){
			/*Changed by Padmavathi for displaying proper error message starts*/
			/*throw new TenjinServletException("Invalid Project Description");*/
			throw new TenjinServletException("Invalid Project Name");
			/*Changed by Padmavathi for displaying proper error message ends*/
		} else if(!Utilities.trim(project.getState()).equalsIgnoreCase("A")){
			logger.error("Project [{}] is not active", projectName);
			throw new TenjinServletException("Project " + projectName + " under domain " + domainName + " is not Active.");
		}
		
		return project;
		
	}
	
	
	
	
	
	/***********
	 * Added by Sriram for TJN_243_02 ends
	 */
	
}
