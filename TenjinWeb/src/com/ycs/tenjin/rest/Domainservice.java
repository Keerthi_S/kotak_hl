/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Domainservice.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 11-nov-2016		nagababu		new file added
   05-Dec-2016          Gangadhar Badagi        Removed empty path annoatation
   17-Dec-2016          Gangadhar Badagi        Added user for creating domain
   02-Aug-2017          Gangadhar Badagi        Added to correct the method name
   02-Aug-2017          Gangadhar Badagi        TENJINCG-182
   16-08-2017           Padmavathi              T25IT-133
   18-08-2017			Pushpalatha				T25IT-219
   01-03-2017			Padmavathi				TENJINCG-554
  
 */

package com.ycs.tenjin.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DomainHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.project.Domain;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.rest.filter.RestContextProperties;
import com.ycs.tenjin.user.User;

@Path("domains")
public class Domainservice

{
	private static final Logger logger = LoggerFactory
			.getLogger(Domainservice.class);
	/*****Added to get user for creating domain starts****/
	@Context
	ContainerRequestContext requestContext;
	
	@Context
	SecurityContext securityContext;
	/*****Added to get user for creating domain ends****/

/******Removed empty path annotation*****/
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	/* @PermitAll */
	/*Changed by Gangadhar Badagi to correct the method name starts*/
	/*public Response getAllDomsins() {*/
	public Response getAllDomains() {
		/*Changed by Gangadhar Badagi to correct the method name ends*/
		try {
			ArrayList<Domain> domains = new DomainHelper().fetchAllDomains();

			Response response = RestUtils.buildResponse(Status.OK, "All domain fetched successfully", domains);

			return response;

		} catch (DatabaseException e) {
			logger.error("Could not fetch all domains", e);
			Response response = RestUtils.buildResponse(Status.INTERNAL_SERVER_ERROR,
					"An internal error occurred.", null);
			return response;
		}
	}

	
/******Removed empty path annotation*****/
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @RolesAllowed("Administrator")
	public Response createdomain( String jsonString) {
		logger.info("Request to post user");
		logger.info("Input JSON --> {}", jsonString);

		Domain domains = new Gson().fromJson(jsonString, Domain.class);
		/*****Added to get user  for creating domain starts ********/
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		/*****Added to get user  for creating domain ends********/
		domains.setCreatedDate(new Timestamp(new Date().getTime()));
		try {
			JSONObject json1=new JSONObject(jsonString);
			
			Domain domains1 = new DomainHelper().fetchDomain(json1.get("name").toString());
			if(domains1!=null)
				return RestUtils.buildResponse(Status.CREATED,
						"domain already exists", null);
			/*****Added to set user  for creating domain starts********/
			domains.setOwner(user.getId());
			/*****Added to set user  for creating domain ends********/
			new ProjectHelper().persistDomain(domains);
			Domain exmDomain = new DomainHelper().fetchDomain(domains.getName());
/*			return RestUtils.buildResponse(Status.CREATED,
					"domain Created Successfully", null);*/
			return RestUtils.buildResponse(Status.CREATED,
					"domain Created Successfully", exmDomain);
		} catch (DatabaseException e) {
			

			return RestUtils
					.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}
		catch(JSONException e){
			return RestUtils
					.buildResponse(Status.INTERNAL_SERVER_ERROR, e.getMessage(), null);
		}

	}
	

	@GET
	@Path("/{domainname}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response singledomain(@PathParam("domainname") String domainname) {
		logger.info("Request to Get domainname", domainname);
		/*Added by Pushpalatha for defect T25IT-219 starts*/
		User user=(User) requestContext.getProperty(RestContextProperties.TJN_USER);
		

		Response response = null;
		if(user.getRoles().equalsIgnoreCase("Tester")){
			return RestUtils.buildResponse(Status.NOT_FOUND, "User is not allowed to get Domains and Projects details", null);
		}
		/*Added by Pushpalatha for defect T25IT-219 ends*/
		try {
			Domain domains = new DomainHelper().fetchDomain(domainname);
			if (domains != null)
				response = RestUtils.buildResponse(Status.OK, "Domain "+domainname+" fetched successfully", domains);
			else
				response = RestUtils.buildResponse(Status.NOT_FOUND,
						"domain does not exist", null);
		} catch (DatabaseException e) {
			
			response = RestUtils
					.buildResponse(
							Status.INTERNAL_SERVER_ERROR,
							"An internal error occurred. Please contact Tenjin Administrator",
							null);
		}
		return response;
	}

	@GET
	@Path("/projectname/{domainname}")
	@Produces(MediaType.APPLICATION_JSON)
	/*commented by padmavathi for TENJINCG-554 starts*/
	/*@PermitAll*/
	/*commented by padmavathi for TENJINCG-554 ends*/
	public Response allDomainProjects(@PathParam("domainname") String domainname) {
		logger.info("Request to Get projectName", domainname);
		
		

		Response response = null;
		
		try {

			ArrayList<Project> projects = new DomainHelper()
					.getProjectsForDomain(domainname);
			if (projects.size() > 0)
				response = RestUtils.buildResponse(Status.OK, "Domain "+domainname+" fetched successfully", projects);
			else
				response = RestUtils.buildResponse(Status.NOT_FOUND,
						"project does not exist", null);
		} catch (DatabaseException e) {
			
			response = RestUtils
					.buildResponse(
							Status.INTERNAL_SERVER_ERROR,
							"An internal error occurred. Please contact Tenjin Administrator",
							null);
		}
		return response;
	}
	
	/*Added by Gangadhar Badagi for TENJINCG-182 starts*/
	@GET
	@Path("/{domainname}/projects/{projectname}")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getProjectDetailsForDomain(@PathParam("domainname") String domainname,@PathParam("projectname") String projectname) {
		logger.info("Request to Get projectName", domainname);

		Response response = null;
		try {

			Domain domain = new DomainHelper().fetchDomain(domainname);
			if (domain == null) {
				return RestUtils.buildResponse(Status.NOT_FOUND, "Domain '" + domainname + "' does not exist", null);
			}
			Project prj=new DomainHelper().fetchProjectDetailsForDomain(domainname, projectname);
			if (prj!=null)
				response = RestUtils.buildResponse(Status.OK, "Project '"+projectname+"' details fetched successfully", prj);
			else
				response = RestUtils.buildResponse(Status.NOT_FOUND,
						"project '"+projectname+"'does not exist", null);
		} catch (DatabaseException e) {
			
			response = RestUtils
					.buildResponse(
							Status.INTERNAL_SERVER_ERROR,
							"An internal error occurred. Please contact Tenjin Administrator",
							null);
		}
		return response;
	}
	/*Added by Gangadhar Badagi for TENJINCG-182 ends*/
	/*Added by Padmavathi for T25IT-133 starts*/
	@GET
	@Path("/{domainname}/projects")
	@Produces(MediaType.APPLICATION_JSON)
	/*commented by padmavathi for TENJINCG-554 starts*/
	/*@PermitAll*/
	/*commented by padmavathi for TENJINCG-554 ends*/
	/*public Response getProjectDetailsForDomain(@PathParam("domainname") String domainname) {*/
	public Response getAllProjectsForDomain(@PathParam("domainname") String domainname) {
		logger.info("Request to Get projects ", domainname);

		Response response = null;
		try {
			Domain domain=new DomainHelper().fetchDomain(domainname);
			if(domain==null){
				return RestUtils.buildResponse(Status.NOT_FOUND,"Domain '"+domainname+"'does not exist", null);
			}
			
            domain.getProjects();
			if ( domain.getProjects()!=null && domain.getProjects().size()>0)
				response = RestUtils.buildResponse(Status.OK, "Project(s) details fetched successfully", domain.getProjects());
			else
				response = RestUtils.buildResponse(Status.NOT_FOUND,
						"Domain '"+domainname+"'does not have projects", null);
		} catch (DatabaseException e) {
			
			response = RestUtils
					.buildResponse(
							Status.INTERNAL_SERVER_ERROR,
							"An internal error occurred. Please contact Tenjin Administrator",
							null);
		}
		return response;
	}
	/*Added by Padmavathi for T25IT-133 ends*/
	
}
