package com.ycs.tenjin.rest;


public class RestRequestException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RestRequestException(String message) {
		super(message);
	}
	
	public RestRequestException(String message, Throwable cause) {
		super(message, cause);
	}
}
