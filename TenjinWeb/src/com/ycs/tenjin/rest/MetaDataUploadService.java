/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutorService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 11-nov-2016			sameer gupta			new file added 
 */

package com.ycs.tenjin.rest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.util.MetaDataUtils;
import com.ycs.tenjin.util.Utilities;

@Path("/metadata")
public class MetaDataUploadService {
	private static final Logger logger = LoggerFactory
			.getLogger(MetaDataUploadService.class);

	@POST
	@Path("/{runId}")
	@Produces(MediaType.APPLICATION_JSON)
	/* @Consumes(MediaType.APPLICATION_XML) */
	@Consumes(MediaType.TEXT_PLAIN)
	@PermitAll
	public Response persistLearningData(String xmlString,
			@PathParam("runId") int runId) {
		logger.info("Request to persisting Learning Data");
		Response response = null;
		try {
			TestRun t = new RunHelper().hydrateRun(runId);
			new MetaDataUploadService().persistMetada(xmlString, t.getUser(),
					"COMPLETE");
			response = RestUtils.buildResponse(Status.CREATED,
					"persisted Successfully", null);
		} catch (DatabaseException e) {
			response = RestUtils.buildResponse(Status.NOT_FOUND,
					e.getMessage(), null);
		} catch (Exception e) {
			response = RestUtils
					.buildResponse(
							Status.INTERNAL_SERVER_ERROR,
							"An internal error occurred. Please contact Tenjin Administrator",
							null);
		}
		return response;
	}

	public void persistMetada(String xmlString, String userId, String status)
			throws Exception {
		/*modified by paneendra for VAPT FIX  starts*/
		/*File file = File.createTempFile("import", ".xml");*/
		File file = null;
		String directories = System.getProperty("java.io.tmpdir")+File.separator+"TenjinMeta"+File.separator+"import" + Utilities.getRawTimeStamp()+".xml";
		file = new File(directories);
		file.getParentFile().mkdirs();
		if(file.createNewFile()) {
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			bw.write(xmlString);
			bw.close();
            }
            else {
                logger.error("File is not created ");
            }/*modified by paneendra for VAPT FIX  ends*/

		MetaDataUtils util = new MetaDataUtils();
		util.persistData(file, userId, status);

	}
}
