/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestSetHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 10-04-2018		    Pushpalatha			   	Newly Added
* 17-04-2018			Preeti					TENJINCG-638 
* 28-May-2018	  		Pushpa					TENJINCG-386
* 14-06-2018			Preeti					T251IT-22 
* 18-06-2018			Preeti					T251IT-75
* 27-09-2018		    Pushpa					TENJINCG-740
* 08-10-2018            Padmavathi              TENJINCG-840 
* 23-10-2018			Preeti					TENJINCG-848
* 04-12-2018            Leelaprasad             TJNUN262-66
* 19-02-2019			Preeti					TENJINCG-969
* 29-03-2019			Preeti					TENJINCG-1003
* 27-05-2019			Prem					V2.8-69
* 03-06-2019            Leelaprasad             TENJINCG-1068,TENJINCG-1069
* 19-09-2019			Preeti					TENJINCG-1068,1069
* 10-10-2019            Padmavathi              TJN27-47
* 11-05-2020			Ashiki					TJN27-81
*/

package com.ycs.tenjin.handler;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.FilterCriteria;
import com.ycs.tenjin.db.PaginatedRecords;
import com.ycs.tenjin.db.TestSetHelperNew;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.util.Utilities;



public class TestSetHandler {
	private static final Logger logger = LoggerFactory.getLogger(TestSetHandler.class);
	private TestSetHelperNew testSetHelperNew=new TestSetHelperNew();
	private AuditHandler auditHandler = new AuditHandler();
	private ProjectHandler projectHandler=new ProjectHandler();
	
	/*Added by Padmavathi   TJN27-47  starts*/
	public PaginatedRecords<TestSet> getAllTestSets(int projectId,String searchValue, int maxRecords, int pageNumber, String sortField, String sortDirection) throws DatabaseException {
		List<FilterCriteria> criteria = new ArrayList<FilterCriteria>();
		
		if(searchValue != null && !searchValue.equalsIgnoreCase("")){
			criteria.add(new FilterCriteria(new String[]{"LOWER(TS_NAME)","LOWER(TS_TYPE)","LOWER(TS_CREATED_ON)","LOWER(TS_CREATED_BY)","LOWER(TS_EXEC_MODE)"}, searchValue.toLowerCase() , FilterCriteria.PATTERN_MATCHES_ANY_FIELD_VALUE));
		}
		String prjId = Integer.toString(projectId);
		if(prjId != null && !prjId.equalsIgnoreCase("")){
			criteria.add(new FilterCriteria("TS_PRJ_ID", prjId, FilterCriteria.MATCH));
		}
		criteria.add(new FilterCriteria("TS_REC_TYPE", "TS", FilterCriteria.MATCH));
		
		return this.testSetHelperNew.hydrateAllTestSets(projectId,criteria, maxRecords, pageNumber, sortField, sortDirection);
	}
	/*Added by Padmavathi   TJN27-47  ends*/
	
	public List<TestSet> getAllTestSets(int projectId) throws DatabaseException {
		List<TestSet> testset=this.testSetHelperNew.hydrateAllTestSets(projectId);
		return testset;
	}

	public TestSet hydrateTestSet(int projectId,int testSetRecordId) throws DatabaseException{
		
		TestSet testSet= this.testSetHelperNew.hydrateTestSet(projectId, testSetRecordId);
		if(testSet==null){
			throw new RecordNotFoundException("test.set.not.found"); 
		}
		return testSet;
	}
	
	/*Added by Preeti for TENJINCG-638 starts*/
	public List<TestRun> hydrateTestSetExecutionHistory(int projectId, int tsRecId) throws DatabaseException {
		try{
			this.hydrateTestSet(projectId, tsRecId);
		}catch(RecordNotFoundException e){
			throw e;
		}
		List<TestRun> runs=this.testSetHelperNew.hydrateTestSetExecutionHistory(projectId, tsRecId);
		if(runs==null){
			throw new RecordNotFoundException("testsethistory.not.found");
		}
		return runs;
	}
	/*Added by Preeti for TENJINCG-638 ends*/
	/*Modified by Preeti for TENJINCG-1003 starts*/
	public void deleteTestSet(String[] testSetRecIds, int projectId, String userId)throws DatabaseException,RequestValidationException, ResourceConflictException {
	/*Modified by Preeti for TENJINCG-1003 ends*/
	 ArrayList<TestCase> mappedTCs=new ArrayList<TestCase>();
	 try{
		 for(int i=0;i<testSetRecIds.length;i++){
			 String tsId=testSetRecIds[i];
			 this.hydrateTestSet(projectId, Integer.parseInt(tsId));
			 mappedTCs = this.testSetHelperNew.hydrateMappedTCs(Integer.parseInt(tsId), projectId);
			 if(mappedTCs.size()>0)
				 break;
		 }
	 }catch(RecordNotFoundException e){
			logger.error(e.getMessage());
			throw e;
		}
	 
	 if(mappedTCs.size()>0){
		
			throw new ResourceConflictException("Test cases are mapped to test set ,Please unmap them to delete a Test Set");
	 }
	/*Added by Preeti for TENJINCG-1003 starts*/
	 String prjUserRole = "";
	 for(int i=0;i<testSetRecIds.length;i++){
		 int tsId=Integer.parseInt(testSetRecIds[i]);
		 prjUserRole = this.projectHandler.hydrateCurrentUserRoleForProject(userId, projectId);
		 if(prjUserRole.equalsIgnoreCase("Test Engineer")){
			 TestSet ts = this.hydrateTestSetBasicDetails(projectId, tsId);
			 if(!ts.getCreatedBy().equalsIgnoreCase(userId))
				throw new RequestValidationException("Could not delete Test Set(s) , user has no access to delete one or more selected Test Set(s).");
		 }
	 }
	 /*Added by Preeti for TENJINCG-1003 ends*/
	 String checkTS=this.testSetHelperNew.delete_testsets(testSetRecIds);
	 if(checkTS.contains("Execution")){
		 throw new RequestValidationException(checkTS);
	 }
		
	}

	public void updateTestSet(TestSet testset)throws DatabaseException,RequestValidationException {
		TestSet testsetToUpdate=null;
		try{
			testsetToUpdate=this.hydrateTestSet(testset.getProject(),testset.getId());
		}catch(RecordNotFoundException e){
			throw e;
		}
		/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		/*Modified by Preeti for TENJINCG-1068,1069 starts*/
		TestSet testSetOld=new TestSet();
		testSetOld.merge(testsetToUpdate);
		TestSet testSetNew=new TestSet();
		testSetNew.merge(testset);
		/*Modified by Preeti for TENJINCG-1068,1069 ends*/
		/*Added by Leelaprasad for TENJINCG-1068,1069 ends*/
		String oName=Utilities.trim(testsetToUpdate.getName());
		
		testsetToUpdate.merge(testset);
		if(Utilities.trim(testsetToUpdate.getName()).equalsIgnoreCase(oName)){
			this.validateTestSetMandatoryFields(testsetToUpdate,false);
		}else{
			this.validateTestSetMandatoryFields(testsetToUpdate,true);
		}
		
		if(testset.getId()!=0&&testsetToUpdate.getId()!=( testset.getId() )){
			logger.error("ERROR - Illegal update. Testset Id. Old value [{}], New Value [{}]",testsetToUpdate.getId(), testset.getId());
			throw new RequestValidationException("illegal.field.update", "Id");
		}if((Utilities.trim(testset.getCreatedBy()).length())>1&&!Utilities.trim(testsetToUpdate.getCreatedBy()).equalsIgnoreCase(Utilities.trim(testset.getCreatedBy()))){
			logger.error("ERROR - Illegal update. Testset Created By. Old value [{}], New Value [{}]",testsetToUpdate.getCreatedBy(), testset.getCreatedBy());
			throw new RequestValidationException("illegal.field.update", "Created By");
		}if((testset.getCreatedOn())!=null&&!testsetToUpdate.getCreatedOn().equals( testset.getCreatedOn())){
			logger.error("ERROR - Illegal update. Testset Created on. Old value [{}], New Value [{}]",testsetToUpdate.getCreatedOn(), testset.getCreatedOn());
			throw new RequestValidationException("illegal.field.update", "Created On");
		}
		 
		this.testSetHelperNew.updateTestSet(testsetToUpdate);
		/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		 ObjectMapper mapper = new ObjectMapper();
			try {
				String newDataJson = mapper.writeValueAsString(testSetNew);
				String oldDataJson = mapper.writeValueAsString(testSetOld);
				testset.getAuditRecord().setNewData(newDataJson);
				testset.getAuditRecord().setOldData(oldDataJson);
			} catch (JsonProcessingException e) {
				logger.error("Error while persisting audit record");
				logger.error("Error ", e);
			}
			/*Added by Leelaprasad for TENJINCG-1068,1069 ends*/
		/*Added by Preeti for TENJINCG-969 starts*/
		this.auditHandler.persistAuditRecord(testset.getAuditRecord());
		/*Added by Preeti for TENJINCG-969 ends*/
	}

	public void persistTestSet(TestSet testset, int projectId)throws DatabaseException,RequestValidationException,DuplicateRecordException {
		
		logger.info("Validating TestSet information");
		testset.setRecordType("TS");
		this.validateTestSetMandatoryFields(testset,true);
		logger.info("Done");
		
		logger.info("Creating TestSet");
		this.testSetHelperNew.persistTestSet(testset,projectId);
		logger.info("Done");
	}

	private void validateTestSetMandatoryFields(TestSet testset, boolean b) throws RequestValidationException, DatabaseException {
		
		if(Utilities.trim(testset.getName()).length() < 1) {
			logger.error("ERROR - test set name  is blank");
			/*Changed by Leelaprasad for the Defect TJNUN262-66 starts*/
			
			/*throw new RequestValidationException("field.mandatory", "name");*/
			throw new RequestValidationException("field.mandatory", "Test set name");
			/*Changed by Leelaprasad for the Defect TJNUN262-66 ends*/
		}
		/*Modified by Preeti for T251IT-75 starts*/
		if(Utilities.trim(testset.getName()).length() > 100) {
			throw new RequestValidationException("field.length", new String[] {"Test set name","100"});
		}
		/*Modified by Preeti for T251IT-75 ends*/
		/*Added by Preeti for T251IT-22 starts*/
		if(Utilities.trim(testset.getDescription()).length() > 3000) {
			throw new RequestValidationException("field.length", new String[] {"Test set description","3000"});
		}
		/*Added by Preeti for T251IT-22 ends*/
		
		if(Utilities.trim(testset.getType()).length()<1){
			testset.setType("Functional");
			
		}else if(!Utilities.trim(testset.getType()).equals("Functional")){
			throw new RequestValidationException("test.set.type");
		}
		if(Utilities.trim(testset.getPriority()).length()<1){
			testset.setPriority("Low");
		}else if(!Utilities.trim(testset.getPriority()).equals("Low")&&!Utilities.trim(testset.getPriority()).equals("Medium")&&!Utilities.trim(testset.getPriority()).equals("High")){
			throw new RequestValidationException("test.set.priority");
		}
		if(b){
			if(this.testSetHelperNew.hydrateTestSet(testset.getName(), testset.getProject())!=null){
				throw new DuplicateRecordException("test.set.duplicate",testset.getName());
			}
		}
		
		
	}

	public int getStepCount(List<TestCase> testcase) {
		
		int stepCount=0;
		for(TestCase tc:testcase){
			stepCount=stepCount+tc.getTcSteps().size();
		}
		return stepCount;
	}
	/*Added by Pushpalatha for TENJINCG-637 starts*/
	public ArrayList<TestCase> getUnmappedTc(int tsRecID, int prjId, String mode) throws DatabaseException, RequestValidationException {
		ArrayList<TestCase> unMappedTcs=this.testSetHelperNew.hydrateUnMappedTCs(tsRecID,prjId,mode);
		if(unMappedTcs == null || unMappedTcs.size() <= 0 ){
			/*throw new RequestValidationException("No Test Cases were available to Map.");*/
			logger.info("No Test Cases were available to Map.");
		}
		return unMappedTcs;
	}
	
	public ArrayList<TestCase> getPriorityTc(int tsRecID, int prjId, String mode,String priority) throws DatabaseException, RequestValidationException {
		ArrayList<TestCase> unMappedTcs=this.testSetHelperNew.hydrateUnMappedTCs(tsRecID,prjId,mode);
		if(priority.equals("-1")){
			return unMappedTcs;
		}
		ArrayList<TestCase> unMappedPriorityTcs=new ArrayList<TestCase>();
		for(TestCase tc:unMappedTcs){
			if(tc.getTcPriority().equals(priority)){
				unMappedPriorityTcs.add(tc);
			}
		
		}
		if(unMappedTcs == null || unMappedTcs.size() <= 0 ){
			throw new RequestValidationException("No Test Cases were available to Map.");
		}
		return unMappedPriorityTcs;
	}
	/*Added by Pushpalatha for TENJINCG-637 ends*/

	public void persistTestSetunMap(int tsID, String unMappedTc, int prjId) throws DatabaseException {
		this.testSetHelperNew.persistTestSetunMap(tsID, unMappedTc, prjId);
		
	}

	public void persistTestSetMap(int tsId, String mappedTc, int prjId, boolean b) throws DatabaseException {
		
		this.testSetHelperNew.persistTestSetMap(tsId, mappedTc, prjId, true);
		
	}
	
	/*Added by Preeti for TENJINCG-848 starts*/
	public TestSet hydrateTestSetBasicDetails(int projectId,int testSetRecordId) throws DatabaseException{
		
		TestSet testSet= this.testSetHelperNew.hydrateTestSetBasicDetails(projectId, testSetRecordId);
		if(testSet==null){
			throw new RecordNotFoundException("test.set.not.found"); 
		}
		return testSet;
	}
	public void copyTestSetMap(int newtsId,int oldtsId, int prjId) throws DatabaseException, RequestValidationException {
		ArrayList<Integer> mappedTcsId = this.hydrateMappedTestCasesId(prjId, oldtsId);
		if(mappedTcsId != null && mappedTcsId.size()>0)
			this.testSetHelperNew.persistTestSetMap(newtsId, mappedTcsId, prjId);
		
	}
	public ArrayList<Integer> hydrateMappedTestCasesId(int prjId, int oldtsId) throws DatabaseException, RequestValidationException {
			return this.testSetHelperNew.hydrateMappedTCsId(oldtsId, prjId);
		
	}
	/*Added by Preeti for TENJINCG-848 ends*/
	
	public TestCase hydrateTestCase(int tcRecId, int prjId) throws DatabaseException {
		TestCase testcase=this.testSetHelperNew.hydrateTestCase(prjId, tcRecId);
		return testcase;
	}
/*Added by pushpalatha for TENJINCG-386 starts */
	public Boolean validateMobile(TestSet ts) throws DatabaseException {
		Boolean mobFlag=false;
		for(TestCase testCase:ts.getTests()){
			for(TestStep testStep:testCase.getTcSteps()){
				int appId=testStep.getAppId();
				int appType=new AutHandler().getAutType(appId);
				/*Changed by Padmavathi for TENJINCG-840 starts*/
				/*if(appType==2 || appType==3 || appType==4){*/
				if(appType==3 || appType==4){
				/*Changed by Padmavathi for TENJINCG-840 ends*/
					mobFlag=true;
				    break;
				}
			}
		}
		
		return mobFlag;
	}
/*Added by pushpalatha for TENJINCG-386 ends */

	/*Added by Pushpa for TENJINCG-740 Starts*/
	public ArrayList<TestSet> hydrateTestSets(int projectId, String[] tsetIds) throws DatabaseException {
		
		ArrayList<TestSet> testsetList=this.testSetHelperNew.hydrateTestSets(projectId,tsetIds);

		return testsetList;
	}
	/*Added by Pushpa for TENJINCG-740 ends*/

	
	/*Added by Prem for TJN27-197 & TJN27-203 Starts */
	public List<TestSet> hydrateAllTestSetsForReport(int projectId) throws DatabaseException {
		List<TestSet> testset=this.testSetHelperNew.hydrateAllTestSetsForReport(projectId);
		return testset;
	}
	/*Added by Prem for TJN27-197 & TJN27-203 Ends */
	
}
