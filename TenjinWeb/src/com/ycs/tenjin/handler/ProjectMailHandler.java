/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectMailHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 * DATE                 CHANGED BY              DESCRIPTION
 * 02-05-2018			Preeti					Newly added for TENJINCG-656
 * 03-05-2018            Leelaprasad              TENJINCG-639
 * 10-05-2018			Pushpalatha				TENJINCG-629
 * 03-05-2019            Padmavathi              TENJINCG-1048&1049&1050

*/

package com.ycs.tenjin.handler;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ProjectMailHelper;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.mail.TenjinJavaxMail;
import com.ycs.tenjin.project.ProjectMail;
import com.ycs.tenjin.project.ProjectMailPreference;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class ProjectMailHandler {
	private static final Logger logger = LoggerFactory.getLogger(ProjectMailHandler.class);
	private ProjectMailHelper projectMailHelper = new ProjectMailHelper();
	
	public ProjectMail hydrateProjectMail(int projectId) throws DatabaseException {
		ProjectMail projectMail = projectMailHelper.hydrateProjectMail(projectId); 
		return projectMail;
	}

	public ProjectMail updateProjectForMail(ProjectMail projectMail) throws DatabaseException, RequestValidationException {
		
		if (!(Utilities.trim(projectMail.getPrjMailNotification()).length()<1)) {
			if (!Utilities.trim(projectMail.getPrjMailNotification()).equalsIgnoreCase("Y") && !Utilities.trim(projectMail.getPrjMailNotification()).equalsIgnoreCase("N")){
				logger.error("ERROR - Project mail notification is not valid");
				throw new RequestValidationException("project.mail.notification");
			}else{
				projectMail.setPrjMailNotification(Utilities.trim(projectMail.getPrjMailNotification()).toUpperCase());
			}
		}
		if (Utilities.trim(projectMail.getPrjMailNotification()).equalsIgnoreCase(""))
			{
				logger.error("ERROR - Project mail notification is blank");
				throw new RequestValidationException("project.mail.notification");
			}
		ProjectMail projectMailUpdate = projectMailHelper.updateProjectForMail(projectMail);
		return projectMailUpdate;
	}
	/*Added by Leelaprasad for TENJINCG-639 starts*/
	public void sendRunReportManual(int runId, int prjId, String userList) throws TenjinConfigurationException{
		new TenjinJavaxMail().buildMailInformationForManualSentMail(runId, prjId, userList);
		
		
	}
	/*Added by Leelaprasad for TENJINCG-639 ends*/
	/*Added by Pushpalatha for TENJINCG-629 starts*/
	public LinkedHashMap<String, String> hydrateEscalationRules() throws DatabaseException {
		
		return this.projectMailHelper.getEscalationRules();
	}
	/*Added by Pushpalatha for TENJINCG-629 starts*/
	
	/*Added by Padmavathi for TENJINCG-1048&1049&1050 starts*/
	public void persistProjectMailPreference(int prjId,String action,String prjUsers) throws DatabaseException{
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);){
			
				if(this.projectMailHelper.doesProjectMailPreferenceExits(conn, prjId, action)){
					this.projectMailHelper.updateProjectMailPreference(conn,prjId, action, prjUsers);
				}
				else{
					this.projectMailHelper.persistProjectMailPreference(conn,prjId, action, prjUsers);
				}
			}catch(Exception e){
				throw new DatabaseException("Could not insert record",e);
			}
		
	}
	
	public String getPrjMailPreferenceUsers(int prjId,String action) throws DatabaseException{
		return this.projectMailHelper.hydrateProjectMailPreferenceUsers( prjId, action);
	}
	public List<ProjectMailPreference> getProjectMailPreference(Connection conn,int prjId) throws DatabaseException{
		return this.projectMailHelper.hydrateProjectMailPreference(conn,prjId);
	}
	
	public void updateProjectUserMailPreference(int prjId,String actions,String userId) throws DatabaseException{
		
		Map<String,String> preferenceMapToUpdate= new HashMap<String,String>();
		Map<String,String> preferenceMapToPersist= new HashMap<String,String>();
		List<String> userActionsToUpdate = Arrays.asList(actions.split(","));
		List<String> userList =null;
		List<String> oUserAction=new ArrayList<String>();
		String actionsToDelete="";
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);){
			//getting all configured mail preferences for project
			List<ProjectMailPreference> preferenceList= this.getProjectMailPreference(conn, prjId);
			for(ProjectMailPreference preference:preferenceList){
				oUserAction.add(preference.getAction());
				if(preference.getUsers()!=null){
					userList = Arrays.asList(preference.getUsers().split(","));
				}
				//comparing user selected action with configured action,if action is already present and 
				//if user is not there in action adding user to that mail preference
				if(!userActionsToUpdate.get(0).equals("")&&userActionsToUpdate.contains(preference.getAction())){
					if(userList!=null){
						if(!userList.contains(userId))
							preferenceMapToUpdate.put(preference.getAction(), preference.getUsers()+","+userId);
					}else{
						preferenceMapToUpdate.put(preference.getAction(), userId);
					}
				}else{
					//if action is not contains in already configured preference ,removing current user from that preference
					String prjUser="";
					if(userList!=null){
						for(String user:userList){
							if(!user.equalsIgnoreCase(userId)){
								prjUser=prjUser+user+",";
							}
						}
						if(prjUser.length()>0)
							preferenceMapToUpdate.put(preference.getAction(), prjUser.substring(0, prjUser.length()-1));
						else
							actionsToDelete=actionsToDelete+"'"+preference.getAction()+"',";
					}
				}
			}
			//checking and persisting new configured actions to mail preference
			if(!oUserAction.equals(userActionsToUpdate)){
				for(String userActionToUpdate:userActionsToUpdate){
					if(!userActionsToUpdate.get(0).equals("")&&!oUserAction.contains(userActionToUpdate)){
						preferenceMapToPersist.put(userActionToUpdate,userId);
					}
				}
			}
			//conn.setAutoCommit(false);
			if(preferenceMapToUpdate.size()>0){
				this.projectMailHelper.updateProjectMailPreference(conn, prjId, preferenceMapToUpdate);
			}
			if(preferenceMapToPersist.size()>0){
				this.projectMailHelper.persistProjectMailPreference(conn, prjId, preferenceMapToPersist);
			}
			if(actionsToDelete.length()>0){
				this.projectMailHelper.deleteProjectMailPreference(conn, prjId, actionsToDelete.substring(0, actionsToDelete.length()-1));
			}
			//conn.commit();
		}catch(Exception e){
			logger.error(e.getMessage());
			logger.error("Error ", e);
			throw new DatabaseException("Could not insert record",e);
		}
	
		
	}
	 public List<String> getProjectMailPreferenceForUsers(int prjId,String user) throws DatabaseException{
		List<String> actions=new ArrayList<String>();
		 try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);){
				List<ProjectMailPreference> preferenceList= this.getProjectMailPreference(conn, prjId);
				for(ProjectMailPreference preference:preferenceList){
					if(preference.getUsers()!=null){
						List<String> userList = Arrays.asList(preference.getUsers().split(","));
						if(userList.contains(user)){
							actions.add(preference.getAction());
						}
					}
				}
			}catch(Exception e){
				throw new DatabaseException("Could not get record",e);
			}
		return actions;
		 
	 }
	 
	 public boolean isUserOptOutFromMail(int prjId,String action,String userId) {
		 try {
			String users=this.getPrjMailPreferenceUsers(prjId, action);
			 if(users==null){
				 return true;
			 }
			List<String> userList = Arrays.asList(users.split(","));
			if(userList.contains(userId)){
				return false;
			}
		} catch (DatabaseException e) {
			logger.error(e.getMessage());
		}
		return true;
		 
	 }
	 /*Added by Padmavathi for TENJINCG-1048&1049&1050 ends*/
}
