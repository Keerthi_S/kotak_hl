/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApplicationUserHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 27-10-2018			Ashiki			Newly added for TENJINCG-846
* 22-03-2019			Preeti					TENJINCG-1018
* */
package com.ycs.tenjin.handler;

import java.net.ConnectException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.ApplicationUser;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.ApplicationUserHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;
public class ApplicationUserHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(GroupHandler.class);
	ApplicationUserHelper applicationUserHelper= new ApplicationUserHelper();
	
	public ArrayList<ApplicationUser>getAllApplicationUsers(String UserId) throws DatabaseException {
		logger.info("Fetching all Application Users");
		ArrayList<ApplicationUser> applicationUsers=this.applicationUserHelper.hydrateAllApplicationUsers(UserId);
		logger.info("Done");
		return applicationUsers;
	}
	
	public ArrayList<ApplicationUser> getAllApplicationUser(String userId,int appId) throws DatabaseException {
		logger.info("Fetching all Application Users");
		ArrayList<ApplicationUser> applicationUsers=this.applicationUserHelper.hydrateAllApplicationUsers(userId, appId);
		logger.info("Done");
		return applicationUsers;
	}
	
	public ApplicationUser getApplicationUser(String userId,int appId, String type) throws DatabaseException {
		logger.info("Fetching all Application Users");
		ApplicationUser applicationUserExists=this.applicationUserHelper.hydrateAllApplicationUsers(appId,userId,type);
		logger.info("Done");
		return applicationUserExists;
	}
	
	public void persistApplicationUser(ApplicationUser applicationUser) throws RequestValidationException, DatabaseException {
		logger.info("Validating Application User information");
		this.validateApplicationUserDetails(applicationUser);
		
		try {
			/*Modified by paneendra for VAPT fix starts*/
			//applicationUser.setPassword(new Crypto().encrypt(applicationUser.getPassword()));
			  applicationUser.setPassword(new CryptoUtilities().encrypt(applicationUser.getPassword()));
			//applicationUser.setTxnPassword(new Crypto().encrypt(applicationUser.getTxnPassword()));
			  applicationUser.setTxnPassword(new CryptoUtilities().encrypt(applicationUser.getTxnPassword()));
			  /*Modified by paneendra for VAPT fix ends*/
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		logger.info("Done");
		
		
		
		logger.info("Creating Application User");
		this.applicationUserHelper.persistApplicationUser(applicationUser);
		logger.info("Done");
	}


	public ApplicationUser updateApplicationUser(ApplicationUser applicationUser,String userId) throws RequestValidationException, DatabaseException {
		
		logger.info("Request to update Application User information"); 
		this.validateApplicationUserDetails(applicationUser);
		
		ApplicationUser existingApplicationUser=this.applicationUserHelper.hydrateApplicationUser(applicationUser.getUacId());
		try{
		if(!(existingApplicationUser.getPassword().equalsIgnoreCase(applicationUser.getPassword())))
		{
			/*Modified by paneendra for VAPT fix starts*/
			//applicationUser.setPassword(new Crypto().encrypt(applicationUser.getPassword()));
			 applicationUser.setPassword(new CryptoUtilities().encrypt(applicationUser.getPassword()));
			 /*Modified by paneendra for VAPT fix ends*/
		}
		if(!(existingApplicationUser.getTxnPassword().equalsIgnoreCase(applicationUser.getTxnPassword()))){
			/*Modified by paneendra for VAPT fix starts*/
			//applicationUser.setTxnPassword(new Crypto().encrypt(applicationUser.getTxnPassword()));
			 applicationUser.setTxnPassword(new CryptoUtilities().encrypt(applicationUser.getTxnPassword()));
			 /*Modified by paneendra for VAPT fix ends*/
		}
		// Code added for OAuth 2.0 requirement - Sunitha  TENJINCG-1018 Starts
		if(applicationUser.getLoginUserType().equalsIgnoreCase(existingApplicationUser.getLoginUserType())){
			applicationUser.setAccessToken(existingApplicationUser.getAccessToken());
			applicationUser.setRefreshToken(existingApplicationUser.getRefreshToken());
			applicationUser.setTokenGenTime(existingApplicationUser.getTokenGenTime());
			applicationUser.setTokenValidity(existingApplicationUser.getTokenValidity());
		}
		// Code added for OAuth 2.0 requirement - Sunitha  TENJINCG-1018 Ends
		}
		 catch (Exception e) {
				logger.error(e.getMessage()); 
			}
		logger.info("Done");
		
		logger.info("Updating Application User");
		ApplicationUser updatedUser = this.applicationUserHelper.update(applicationUser,userId);
		logger.info("Done");
		return updatedUser;
	}
	
	private void validateApplicationUserDetails(ApplicationUser applicationUser) throws RequestValidationException, DatabaseException {
		logger.info("Validating information");
		if(Utilities.trim(applicationUser.getLoginName()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "Name");
		}
		if(Utilities.trim(applicationUser.getLoginName()).length() > 50) {
			throw new RequestValidationException("field.length", new String[] {"Name","50"});
		}
		if(Utilities.trim(applicationUser.getPassword()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "Password");
		}
		if(Utilities.trim(applicationUser.getLoginUserType()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "User Type");
		}
		ApplicationUser applicationUserExists=this.applicationUserHelper.hydrateAllApplicationUsers(applicationUser.getAppId(),applicationUser.getUserId(),applicationUser.getLoginUserType());
		if(applicationUserExists!=null && applicationUserExists.getUacId()!=applicationUser.getUacId()){
			throw new DuplicateRecordException("applicationUserName.already.exist",applicationUser.getLoginUserType());
		}
	}
	

		public ApplicationUser getApplicationUser(int uacId) throws DatabaseException {
			logger.info("Fetching Application User");
			ApplicationUser applicationUser=this.applicationUserHelper.hydrateApplicationUser(uacId);
			logger.info("Done");
			return applicationUser;
		}

		public void deleteApplicationUser(String[] uacIds) throws DatabaseException {
			logger.info("Request to Delete Application User");
			this.applicationUserHelper.deleteApplicationUser(uacIds);
			logger.info("Done");
		}
		
		public void deleteApplicationUser(int appId,String userId,String type) throws DatabaseException {
			logger.info("Request to Delete Application User");
			ApplicationUser applicationUserExists=this.applicationUserHelper.hydrateAllApplicationUsers(appId,userId,type);
			if(applicationUserExists==null){
				throw new RecordNotFoundException("applicationUser.not.found");
			}
			this.applicationUserHelper.deleteApplicationUser(appId,userId,type);
			logger.info("Done");
		}

		public ApplicationUser updateApplicationUserService(ApplicationUser userEnteredAut, int appId, String userId,String type) throws RequestValidationException, DatabaseException {
			boolean typeExist=false;
			logger.info("Request to update Application User information service"); 
			String userType =this.applicationUserHelper.hydrateAllApplicationUserType(appId);
			if(!Utilities.trim(userType).equalsIgnoreCase("")){
				String[] userTypeSplit = userType.split(",");
				for(String uType:userTypeSplit){
					if(uType.equalsIgnoreCase(userEnteredAut.getLoginUserType())){
						typeExist = true;
					}
				}
			}
			if(typeExist){
				ApplicationUser existingUser=this.applicationUserHelper.hydrateAllApplicationUsers(appId,userId,type);
				if(existingUser==null){
					throw new RecordNotFoundException("Could not fetch record for requested Login Type");
				}
				existingUser.merge(userEnteredAut);
				ApplicationUser appUser= this.updateApplicationUser(existingUser, userId);
				return appUser;
			}else{
				throw new RequestValidationException("Invalid Login Type");
			}
			
	
		}
		
		/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Starts*/
		public ApplicationUser getAccessTokenRecord(int uacId) throws DatabaseException {
			ApplicationUser user = this.applicationUserHelper.hydrateAccessTokenRecord(uacId);
			try {
				String password = user.getPassword();
				/*Modified by paneendra for VAPT FIX starts*/
				/*String dPass = new Crypto().decrypt(password.split("!#!")[1],
						decodeHex(password.split("!#!")[0].toCharArray()));*/
				String dPass =new CryptoUtilities().decrypt(password);
				/*Modified by paneendra for VAPT FIX ends*/
				user.setPassword(dPass);
			} catch (Exception e) {
				logger.debug("Error while decrypting the password.");
			}
			return user;
		}
		
		
		public void persistAccessToken(ApplicationUser record, boolean flag, String grantType) throws DatabaseException, RequestValidationException{
			this.validateTokenDetail(record, flag, grantType);
			this.applicationUserHelper.persistAccessToken(record);
		}
		
		private void validateTokenDetail(ApplicationUser appUser, boolean flag, String grantType) throws RequestValidationException, DatabaseException{
			
			if(Utilities.trim(appUser.getAccessToken()).length() < 1) {
				throw new RequestValidationException("field.mandatory", "Access Token");
			}
			
			if(appUser.getTokenGenTime().toString().length()<1){
				throw new RequestValidationException("field.mandatory", "Token Generation Time");
			}
			
			if(flag){
				Date date = new Date();
				if(!(appUser.getTokenGenTime().after(date) || appUser.getTokenGenTime().equals(date))){
					throw new RequestValidationException("accesstoken.failure.prev.date", "Token Generation Time");
				}
			}
			
			if(Utilities.trim(appUser.getTokenValidity()).length() < 1) {
				throw new RequestValidationException("field.mandatory", "Token Validity");
			}
			
			if(!Utilities.isNumeric(appUser.getTokenValidity())) {
				throw new RequestValidationException("field.numeric", "Token Validity");
			}
			
			if(Utilities.trim(appUser.getRefreshToken()).length() < 1 && !(grantType.equalsIgnoreCase("implicit") || grantType.equalsIgnoreCase("client_credentials")) ) {
				throw new RequestValidationException("field.mandatory", "Refresh Token");
			}
		}
		
		
		
		public JSONObject sendAuthRequest(String url, Aut autDetails) throws RequestValidationException{
			try {
				String clientCredentials = autDetails.getClientId()+":"+autDetails.getClientSecret();
				String encodedCredentials = new String(Base64.encodeBase64(clientCredentials.getBytes()));
				Client client = ClientBuilder.newClient();
				WebTarget target = client.target(url);
				Response response1 = target.request().accept(MediaType.APPLICATION_JSON).header("Authorization", "Basic "+encodedCredentials).post(null, Response.class);
				String tokenDetails = (String)response1.readEntity(String.class);
				logger.debug("Response from Auth Server --> {}.", tokenDetails);
				JSONObject json = new JSONObject(tokenDetails);
				json.put("resp_status", response1.getStatus());
				json.put("generation_date", new Timestamp(new Date().getTime()));
				return json;
			} catch (Exception e) {
				logger.debug("Error while sending the request to Authorization server.");
				if(e.getCause() instanceof  ConnectException){
					throw new RequestValidationException("Could not connect to access to Auth server.");
				}
				throw new RequestValidationException("Could not get Access Token.");
			}
		}
		
		public ApplicationUser mapJSONtoOAuthDetails(ApplicationUser refreshedTknDetails, JSONObject tokenJson) {
			try {
				refreshedTknDetails.setAccessToken(tokenJson.getString("access_token"));
				refreshedTknDetails.setTokenValidity(tokenJson.getString("expires_in"));
				refreshedTknDetails.setTokenGenTime((Timestamp)tokenJson.get("generation_date"));
				refreshedTknDetails.setRefreshToken(tokenJson.getString("refresh_token"));
			} catch (JSONException e) {
				logger.debug("Error occured while mapping the json attributes to Object.");
			}
			return refreshedTknDetails;
		}

		public boolean checkAuthResponse(JSONObject tokenJson) {
			try {
				if(tokenJson.getInt("resp_status") == 200)
					return true;
				else
					return false;
			} catch (JSONException e) {}
			return false;
		}

		public JSONObject processRequest(String url, Aut autDetails, ApplicationUser user, boolean doValidateDate, String grantType) throws Exception {
			JSONObject tokenJson = null;
			try {
				tokenJson = sendAuthRequest(url, autDetails);
				user = mapJSONtoOAuthDetails(user, tokenJson);
				persistAccessToken(user, doValidateDate, grantType);
			} catch (Exception e) {
				logger.debug("Error while sending the request. Error Message : {}.", e.getMessage());
				throw e;
			}
			return tokenJson;
		}
		/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Ends*/
}
		


