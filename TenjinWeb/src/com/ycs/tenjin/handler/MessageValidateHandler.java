/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MessaegValidateHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 27-10-2018			Ashiki			Newly added for TENJINCG-1275
* */
package com.ycs.tenjin.handler;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.MessageValidate;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.MessageValidateHelper;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.util.Utilities;

public class MessageValidateHandler {
	
	private static Logger logger = LoggerFactory
			.getLogger(MessageValidateHandler.class);
	
	public List<MessageValidate> hydrateAllMsgs(String appId) {
		List<MessageValidate> messageType=null;
		try {
			messageType = new MessageValidateHelper().getMessageFormat(Integer.parseInt(appId));
		} catch (DatabaseException e) {
			logger.error("Error ", e);
		}
		return messageType;
	}
	public List<MessageValidate> hydrateAllMessageFormat() throws DatabaseException {
		return  new MessageValidateHelper().hydrateAllMessage();
	}
	
	public void persistMessage(MessageValidate message) throws DatabaseException, RequestValidationException {
		this.validateMessage(message);
		new MessageValidateHelper().persistMessage(message);
		
	}
	
	public MessageValidate hydrateXML(int parseInt) throws DatabaseException {
		return new MessageValidateHelper().hydrateXML(parseInt);
	}
	
	private void validateMessage(MessageValidate message) throws DatabaseException, RequestValidationException {
		
		if(Utilities.trim(message.getMessageFormat()).length() < 1) {
			throw new RequestValidationException("field.length", new String[] {"Format","50"});
		}
		if(Utilities.trim(message.getMessageFormat()).length() > 50) {
			throw new RequestValidationException("field.length", new String[] {"Format","50"});
		}
		if(Utilities.trim(message.getMessageName()).length() < 1) {
			throw new RequestValidationException("field.length", new String[] {"Name","50"});
		}
		if(Utilities.trim(message.getMessageName()).length() > 50) {
			throw new RequestValidationException("field.length", new String[] {"Name","50"});
		}
		
		boolean messageExists=new MessageValidateHelper().hydrateAllMessage(message.getMessageName());
		if(messageExists==true){
			throw new DuplicateRecordException("messageName.already.exist",message.getMessageName());
		}
	}
	public Map<String, String> hydrateFieldLabels(String messageType, String subType) throws DatabaseException {
		return new MessageValidateHelper().hydrateFieldLabels(messageType,subType);
	}
	public List<MessageValidate> getMessageFormat(int applicationId) throws DatabaseException {
		return new MessageValidateHelper().getMessageFormat(applicationId);
	}
	public String fetchMessageFormat(int appId) throws DatabaseException {
		return new MessageValidateHelper().fetchMessageType(appId);
	}
	public String fetchMessageName(int appId, String msgFormat) throws DatabaseException {
		return new MessageValidateHelper().hydrateMessageName(appId,msgFormat);
	}
	public void deleteMessage(String values) throws DatabaseException {
		new MessageValidateHelper().deleteMessage(values);
		
	}
	public List<MessageValidate> fetchMessageNames(int appId, String msgFormat) throws DatabaseException {
		
		return new MessageValidateHelper().hydrateMessageNames(appId,msgFormat);
	}
	
	
}
