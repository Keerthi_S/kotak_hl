/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LicenseHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 	CHANGED BY              DESCRIPTION
 * 20-06-2019            Padmavathi              Added for License TENJINCG-1081
 * 30-06-2021            Paneendra               VAPT[changed encryption & decryption]
 * 09-07-2021            Paneendra               VAPT[changed encryption & decryption from RSA]
 * */
package com.ycs.tenjin.handler;


import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.RSAPrivateKeySpec;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.LicenseExpiredException;
import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.LicenseHelper;
import com.ycs.tenjin.db.TenjinSessionHelper;
import com.ycs.tenjin.db.UserHelperNew;
import com.ycs.tenjin.license.License;
import com.ycs.tenjin.license.LicenseComponent;
import com.ycs.tenjin.mail.EmailHandler;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.CryptoUtilities;

public class LicenseHandler {

	private static final Logger logger = LoggerFactory.getLogger(LicenseHandler.class);
	

	public List<String> getLicenseKey() throws DatabaseException{
		return new LicenseHelper().hydrateLicenseKey();
	}
	public void persistLicenseKey(String licenseKey) throws DatabaseException{
		new LicenseHelper().persistLicenseKey(licenseKey);
	}
	public void updateLicenseKeyFromRenewalLicenseKey() throws DatabaseException{
		new LicenseHelper().updateLicenseKeyFromRenewalLicenseKey();
	}
	public void persistRenewalLicenseKey(String licenseKey) throws DatabaseException{
		new LicenseHelper().persistRenewalLicenseKey(licenseKey);
	}
	public void updateLicenseKey(String licenseKey) throws DatabaseException{
		new LicenseHelper().updateLicenseKey(licenseKey);
	}
	@SuppressWarnings("static-access")
	public String generateSystemId() throws TenjinServletException, TenjinConfigurationException{

		// encrypting system id
		/*modified by pannendra for systemidencrypt starts*/
		String systemId= new CryptoUtilities().encrypt(this.getSystemId());
		/*modified by pannendra for systemidencrypt ends*/
		logger.debug("generated System id : "+systemId);
		return systemId;

	}
	public String getHostName(String osName){
		String hostName="";
		if (osName.toLowerCase().contains("win")) {
			hostName=System.getenv().get("COMPUTERNAME"); 
		} else if (osName.toLowerCase().contains("nix") || osName.toLowerCase().contains("nux") || osName.toLowerCase().contains("mac os x")) {
			hostName=System.getenv().get("HOSTNAME");
		}
		return hostName;
	}
	public String getSystemId() throws TenjinServletException{

		logger.info("generating system id...");
		//sb contains mac ,host name,Os type and Os version separated by Semicolon(;) 
		StringBuilder sb = new StringBuilder();
		sb.append(mac());
		sb.append(";");
		String os = System.getProperty("os.name");
		sb.append(getHostName(os));
		sb.append(";");
		sb.append(os);
		sb.append(";");
		sb.append(System.getProperty("os.version"));

		return sb.toString();

	}
	
	
	public void putLicenseInCache(License license) throws TenjinServletException  {

		try {
			CacheUtils.initializeRunCache();
			CacheUtils.putObjectInRunCache("licensedetails",license);
		} catch (TenjinConfigurationException e) {
			logger.error(e.getMessage(), e);
			throw new TenjinServletException("Internal error occured.Please contact Tenjin Support.");
		}

	}
	public void putRenewalLicenseInCache(License license) throws TenjinConfigurationException{
		CacheUtils.initializeRunCache();
		CacheUtils.putObjectInRunCache("renewal_licensedetails",license);
	}
	public void upadateLicenseInCache(License license){
		CacheUtils.putObjectInRunCache("licensedetails",license);
	}
	public void licenseActiveValidation(License license,boolean isRenewal) throws LicenseExpiredException, LicenseInactive, ParseException, DatabaseException {
		this.licenseActiveValidation(license, isRenewal,null);
	}
	public void licenseActiveValidation(License license,boolean isRenewal,HttpServletRequest request) throws LicenseExpiredException, LicenseInactive, ParseException, DatabaseException {
		Date expireDate = null;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(license.getEndDate());
		if(license.getGracePeriod()!=null){
			calendar.add(Calendar.DATE, license.getGracePeriod()); 
		}
		Date today = new Date();
		expireDate = calendar.getTime();
		logger.debug("Checking for expire date" + expireDate);
		if (today.after(expireDate)) {
			logger.warn("License is exipered ... ");
			boolean islicenseActive=false;
			if(!isRenewal){
				logger.debug("checking for  renewal license...");
				islicenseActive=renewalLicenseActiveValidation();
			}
			if(!islicenseActive){
				new LicenseHelper().deleteLicenseKey();
				CacheUtils.removeObjectInRunCache("licensedetails");
				throw new LicenseExpiredException("Your Tenjin License has been Expired.Please contact Tenjin Support.");
			}
		}
		if(!isRenewal){
			logger.debug("checking for renewal");
			calendar.setTime(license.getStartDate());
			if(calendar.getTime().after(today)) {
				logger.error("Your Tenjin License key is Inactive, please contact Tenjin support team.");
				throw new LicenseInactive("Your Tenjin License has not been activated yet. Please contact Tenjin Support.");
			}
			if(request!=null){
				this.validateLicenseNotification(request, license);
			}
		}
	}public void licenseComponentActiveValidation(LicenseComponent licenseComponent) throws  LicenseInactive {
		Calendar calendar = Calendar.getInstance();
		Date today = new Date();
		calendar.setTime(licenseComponent.getStartDate());
		if(calendar.getTime().after(today)) {
			logger.error("Your Tenjin License component is Inactive, please contact Tenjin support team.");
			throw new LicenseInactive("Your license for ["+licenseComponent.getProductComponent().getName()+"] is not active. Please contact Tenjin Support");
		}
	}
	public void validateLicenseNotification(HttpServletRequest request,License license){
		try {
			License renewalLicense=(License) CacheUtils.getObjectFromRunCache("renewal_licensedetails");
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			if(renewalLicense==null){
				Map<String, Object> map = new HashMap<String, Object>();
				Date endDate = null;
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(license.getEndDate());
				endDate = calendar.getTime();
				Date today = new Date();
				long days = TimeUnit.DAYS.convert(endDate.getTime() - today.getTime(), TimeUnit.MILLISECONDS);
				logger.debug(" Days remaining " + days);
				if((days<=30) && !(days<=0)) {
					//						SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
					map.put("MESSAGE", "Your License will expire on "+ formatter.format(endDate) + ". Please renew your license.");
					request.getSession().setAttribute("LICENSE_NOTIFICATION", map);
					if(new TenjinSessionHelper().getUserSessionCount()==0){
						this.sendLicenseNotificationMail(license, "LicenseWillExpire");
					}
				}else if (days==0){
					/* Modified by Padmavathi start */
					if(license.getGracePeriod()!=null){
						/* Modified by Prem start */
						map.put("MESSAGE", "Your License has been expired. You have " + license.getGracePeriod() + " day(s) to renew your license.");
						/* Modified by Prem End */
					}else{
						map.put("MESSAGE", "Your License is going to expire today at "+formatter.format(endDate)+". You have " + 0 + " day(s) to renew.");
					}
					request.getSession().setAttribute("LICENSE_NOTIFICATION", map);
					if(new TenjinSessionHelper().getUserSessionCount()==0){
						this.sendLicenseNotificationMail(license, "LicenseExpired");
					}
				}else if(days<0){
					if(license.getGracePeriod()!=null){
						calendar.add(Calendar.DATE, license.getGracePeriod());
						Date expireDate = calendar.getTime();
						map.put("MESSAGE", "Your License has been expired. Your grace period will expire on " +formatter.format(expireDate)+"." );
						request.getSession().setAttribute("LICENSE_NOTIFICATION", map);
						if(new TenjinSessionHelper().getUserSessionCount()==0){
							this.sendLicenseNotificationMail(license, "LicenseExpired");
						}
					}
				}
				/* Modified by Padmavathi ends */
			}
		} catch (TenjinConfigurationException | DatabaseException e) {
			logger.error(e.getMessage(), e);
		}
	}
	public boolean renewalLicenseActiveValidation() throws LicenseExpiredException, LicenseInactive, ParseException, DatabaseException {

		try {
			License	license = (License) CacheUtils.getObjectFromRunCache("renewal_licensedetails");
			if(license==null){
				logger.info("license is not renewed");
				return false;
			}else{
				Date endDate = null;
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(license.getEndDate());
				if(license.getGracePeriod()!=null){
					calendar.add(Calendar.DATE, license.getGracePeriod());
				}
				endDate = calendar.getTime();
				Date today = new Date();
				if (today.after(endDate)) {
					logger.info(" renewed License is exipered ... ");
					new LicenseHelper().deleteLicenseKey();
					CacheUtils.removeObjectInRunCache("renewal_licensedetails");
					throw new LicenseExpiredException("Your Tenjin License has been Expired, Please contact Tenjin support team.");
				}
				calendar.setTime(license.getStartDate());
				if(calendar.getTime().after(today)) {
					logger.error("Your Tenjin License has not been activated yet. Please contact Tenjin Support.");
					throw new LicenseInactive("Your Tenjin License has not been activated yet. Please contact Tenjin Support.");
				}
				//updating license renewal key to license key and removing renewal key from cache
				new LicenseHandler().updateLicenseKeyFromRenewalLicenseKey();
				upadateLicenseInCache(license);
				CacheUtils.removeObjectInRunCache("renewal_licensedetails");
				return true;
			}
		} catch (TenjinConfigurationException e) {
			logger.error(e.getMessage(), e);
			return false;
		}		
	}

	public License validateLicenseKey(String licenseKey,boolean isRenewal) throws TenjinServletException, LicenseExpiredException, LicenseInactive, ParseException, DatabaseException{
		License license=this.getLicense(licenseKey);
		/*
		 * String hostName= getHostName(System.getProperty("os.name"));
		 * if(!mac().equals(license.getHost().getKey())||
		 * !hostName.equals(license.getHost().getHostName())){ throw new
		 * TenjinServletException("Invalid License key. Please contact Tenjin Support."
		 * ); } this.licenseActiveValidation(license,isRenewal);
		 */
		logger.info("validating license key is done");
		return license;	
	}
	public License getLicense(String licenseKey) throws TenjinServletException{
		String decryptedKey = LicenseHandler.decrypt(licenseKey);
		String[] keys=decryptedKey.split(";");
		Gson gson = new Gson();
		License license=gson.fromJson(keys[0], License.class);
		if(keys.length>1&& keys[1]!=null){
			String licSupportMails="";
			String[] mailIds=keys[1].split("\\|");
			if(mailIds.length>1){
				for(String mailId:mailIds) {
					licSupportMails += mailId+",";
				} 
				licSupportMails=licSupportMails.substring(0,licSupportMails.length()-1);
			}else{
				licSupportMails=keys[1];
			}
			license.setLicSupportMails(licSupportMails);
		}
		return license;
	}
	public void sendLicenseNotificationMail(License license,String content){
		try {
			String recipients= new UserHelperNew().hydrateUserMailIds(1);
			new EmailHandler().sendLicenseNotificationMail(license, content,recipients);
		} catch (SQLException | DatabaseException e) {
			logger.error(e.getMessage(), e);
		}
	}
	public static String mac() throws TenjinServletException {
		StringBuilder sb = new StringBuilder();
		try {
			InetAddress ip = InetAddress.getLocalHost();
			ip.getHostName();
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			byte[] mac = network.getHardwareAddress();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "" : ""));
			}
		} catch (UnknownHostException | SocketException e) {
			logger.error(e.getMessage(), e);
			throw new TenjinServletException("Internal error occured.Please contact Tenjin Support.");
		} 
		return sb.toString();
	}


	
      /*Added by paneendra for VAPT fix [license decrypt] starts*/
	public static Key readPrivateKey() throws IOException {
		File file = new File(new LicenseHandler().getClass().getClassLoader().getResource("resources/private_license2048.key").getFile().replaceAll("%20", " "));
		Key key = null;
		InputStream inputStream = new FileInputStream(file.getPath());
		ObjectInputStream objectInputStream = new ObjectInputStream(new BufferedInputStream(inputStream));
		try {
			BigInteger modulus = (BigInteger) objectInputStream.readObject();
			BigInteger exponent = (BigInteger) objectInputStream.readObject();
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			key = keyFactory.generatePrivate(new RSAPrivateKeySpec(modulus, exponent));

		} catch (Exception e) {
			logger.error("Error loading private key", e);
		} finally {
			objectInputStream.close();
		}
		return key;
	}
	 

	public static String decrypt(String encryptedText) {
		try {
			Key privateKey = readPrivateKey();
			Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-512ANDMGF1PADDING");
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			int MAX_ENCRYPT_BLOCK =256; 
			return new String(cipherFinal(cipher, Base64.decodeBase64(encryptedText), MAX_ENCRYPT_BLOCK));
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | IOException e) {
			logger.error("Could not decrypt plain text", e);
		}
		return encryptedText;		
	}

	public static byte[] cipherFinal(Cipher cipher, byte[] srcBytes, int segmentSize)
			throws IllegalBlockSizeException, BadPaddingException,IOException { 
		if (segmentSize <= 0)
			throw new
			RuntimeException("The segment size must be greater than 0");
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		int inputLen =srcBytes.length; int offSet = 0; byte[] cache; int i = 0;


		while (inputLen -offSet > 0) {
			if (inputLen - offSet > segmentSize) { 
				cache =
						cipher.doFinal(srcBytes, offSet, segmentSize); 
			} else { cache =
					cipher.doFinal(srcBytes, offSet, inputLen - offSet);
			} out.write(cache, 0,
					cache.length); i++;
					offSet = i * segmentSize;
		} 
		byte[] data =out.toByteArray();
		out.close(); 
		return data;
	} /*Added by paneendra for VAPT fix [license decrypt] ends*/



}
