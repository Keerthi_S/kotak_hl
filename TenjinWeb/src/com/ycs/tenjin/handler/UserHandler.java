/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UserHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              	DESCRIPTION
* 30-10-2017			Roshni Das				Newly added for TENJINCG-398
* 10-Nov-2017			Manish					TENJINCG-428
* 14-Nov-2017			Sriram					Added by Sriram to Set User Enabled = Y by default at the time of new user creation ends
  29-Nov-2017			Manish					added new methods to validate user role and change user status(enable/disable)
  13-Dec-2017           Padmavathi              for checking user roles with String
  08-01-2018            Padmavathi              for TENJINCG-578
  29-Jan-2018			Preeti					TENJINCG-507
  01-03-2018            Padmavathi              for TENJINCG-591
  24-04-2018			Preeti					For authentication message change
  18-05-2018			Pushpalatha				TENJINCG-648
  13-06-2018            Padmavathi              for T251IT-8
  19-10-2018           	Leelaprasad             TENJINCG-829
  27-02-2019			Preeti					TENJINCG-984
  28-03-2019			Ashiki					Tenjinch-1023
  23-04-2019			Pushpa					TENJINCG-1044,1041
  29-04-2019			Pushpalatha				TENJINCG-1037
  29-04-2019			Pushpalatha				TENJINCG-1035
  03-05-2019			Pushpalatha				TENJINCG-1042
  07-05-2019			Pushpalatha				TENJINCG-1043
  13-05-2019            Padmavathi              To fix password reset issue
  06-06-2019			Ashiki					V2.8-5 
  07-06-2019			Pushpalatha				TENJINCG-1067,1070
  21-05-2019			Ashiki					TJN27-1
  24-06-2019            Padmavathi              for license
  28-11-2019            Leelaprasad             TENJINCG-1166  
  03-12-2019			Pushpalatha				TENJINCG-1170
  05-02-2020			Roshni					TENJINCG-1168
  18-05-2020			Ashiki					Tenj210-9
  02-06-2020			Ruksar					Tenj210-89
* */

package com.ycs.tenjin.handler;


import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.naming.NameNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ycs.tenjin.DuplicateUserSessionException;
import com.ycs.tenjin.MaxActiveUsersException;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.UserHelperNew;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.license.License;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.FailedLoginAttempts;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class UserHandler {

	private static final Logger logger = LoggerFactory.getLogger(UserHandler.class);

	UserHelperNew userHelper = new UserHelperNew();

	public User getUser(String userId) throws RecordNotFoundException, DatabaseException {
		User user = this.userHelper.hydrateUser(userId);
		if (user == null) {
			/*Modified by Preeti for authentication message change starts*/
			throw new RecordNotFoundException("user is not found");
			/*Modified by Preeti for authentication message change ends*/
		} else {
			return user;
		}
	}
	/*Added by Preeti for TENJINCG-984 starts*/
	public Map<String, String> getCurrentTaskForUser(List<String> usersId) throws SQLException, DatabaseException{
		Map<String, String> taskType = this.userHelper.getCurrentTaskForUser(usersId);
		return taskType;
	}
	/*Added by Preeti for TENJINCG-984 ends*/
	public List<User> getAllUsers() throws DatabaseException {
		return this.userHelper.hydrateAllUsers();
	}
	/*Modified by Pushpa for TENJINCG-1035 starts*/
	public void persist(User user,String currentUser ) throws DatabaseException, RequestValidationException, TenjinServletException, MaxActiveUsersException {
	/*Modified by Pushpa for TENJINCG-1035 ends*/
		logger.info("Validating user information");
		/*Added by Sriram to Set User Enabled = Y by default at the time of new user creation */
		/*Modified by Preeti for TENJINCG-507 starts*/
		/*Added by Padmavathi for license starts*/
		try {
			License license=(License) CacheUtils.getObjectFromRunCache("licensedetails");
			if(license.getType().getName().equalsIgnoreCase("Named")&& this.userHelper.hydrateUsersCount()>=license.getCoreLimit()){
				throw new MaxActiveUsersException("You have reached the maximum limit for your License.");
			}
		} catch (TenjinConfigurationException e1) {
			logger.error("Cannot get license from cache.");
			throw new TenjinServletException("Can not get license details from cache.");
		}
		/*Added by Padmavathi for license ends*/
		user.setEnabledStatus("Y");
		/*Modified by Preeti for TENJINCG-507 ends*/
		/*Added by Sriram to Set User Enabled = Y by default at the time of new user creation ends*/
		this.validateUserDetails(user, true);
		/*Added by Padmavathi for T251IT-8 starts*/
		/*Added by Padmavathi for T251IT-8 ends*/
		
		logger.info("Done");
		try {
			user = this.validateUserRole(user);
		
			user.setPassword((user.getPassword()));
			
		}catch (NameNotFoundException e) {
			throw new RequestValidationException(e.getMessage());
		} 
		/*Modified by Pushpalatha for TENJINCG-1042 ends*/
		logger.info("Registering User");
		/*Modified by Pushpa for TENJINCG-1035 starts*/
		this.userHelper.persistNewUser(user,currentUser);
		/*Modified by Pushpa for TENJINCG-1035 ends*/
	/*Changed by leelaprasad for TENJINCG-829 starts*/
		/*Modified by Ashiki for TJN27-1 starts*/
		this.userHelper.persistUserPreference(Utilities.trim(user.getId()));
		/*Modified by Ashiki for TJN27-1 starts*/
		/*Changed by leelaprasad for TENJINCG-829 ends*/
		logger.info("Done");
	}

	private void validateUserDetails(User user, boolean duplicateCheckRequired)
			throws RequestValidationException, DatabaseException, DuplicateRecordException {
		if (Utilities.trim(user.getId()).length() < 1) {
			throw new RequestValidationException("Id cannot be blank", "Id");
		}
		
		if (Utilities.trim(user.getFirstName()).length() < 1) {
			throw new RequestValidationException("First Name cannot be blank", "First Name");
		}
		 /*Changed by Leelaprasad for TENJINCG-1166 starts*/
		if (Utilities.trim(user.getFirstName()).length() > 15) {
			throw new RequestValidationException("First Name size cannot exceed 15 ", "First Name");
 		}
          if (Utilities.trim(user.getLastName()).length() > 15) {
        	  throw new RequestValidationException("last Name size cannot exceed 15", "Last Name");
		}
          /*Changed by Leelaprasad for TENJINCG-1166 ends*/
		if (Utilities.trim(user.getRoles()).length() < 1) {
			throw new RequestValidationException("Role cannot be blank", "Role");
		}
		/*added by Padmavathi for TENJINCG-578 starts*/
		if (Utilities.trim(user.getEmail()).length() < 1) {
			throw new RequestValidationException("Email cannot be blank", "Email");
		}
		/*added by Padmavathi for TENJINCG-578 ends*/
		
		/*added by manish for bug #TENJINCG-428 on 10-Nov-2017 starts*/
		/*Modified by Preeti for TENJINCG-507 starts*/
		
		if (Utilities.trim(user.getEnabledStatus()).length() < 1) {
			throw new RequestValidationException("User Enable cannot be blank", "User Enable");
		}
		/*Modified by Preeti for TENJINCG-507 ends*/
		/*added by manish for bug #TENJINCG-428 on 10-Nov-2017 starts*/
		
		if (duplicateCheckRequired) {
			
			if (this.userHelper.hydrateUser(Utilities.trim(user.getId())) != null) {
				throw new DuplicateRecordException("user.already.exists", user.getId());
			}
		}

	}

	public void update(User user,String loggedUser) throws DatabaseException, RequestValidationException {
		logger.info("Request to update user");
		User userdetails = this.getUser(user.getId());
		
		if (userdetails == null) {
			logger.error(
					"You are trying to update a user with user id {} which does not exist in the system. Could have been deleted",
					user.getId());
			throw new RecordNotFoundException("The record you are searching for is no longer available.");
		}
		/*added by Padmavathi for TENJINCG-591 starts*/
		user.setEnabledStatus(userdetails.getEnabledStatus());
		/*added by Padmavathi for TENJINCG-591 ends*/
		
		/*Added by Ashiki for TENJINCG-1023 starts*/
		User oldUser=new User();
		oldUser.merge(userdetails);
		oldUser.setFullName(userdetails.getFirstName()+" "+userdetails.getLastName());
		/*Added by Ashiki for TENJINCG-1023 ends*/
		
		
// added by manish to shift code from validator to handler
		/*Added by Pushpa for TENJINCG-648 starts*/
		String userRole="";
		if(userdetails.getRoles().equalsIgnoreCase("Site Administrator"))
		{
			userRole="1";
		}
		if(userdetails.getRoles().equalsIgnoreCase("Tester")){
			userRole="3";
		}
		
		try {
			if(user.getRoles()!=null){
			if(user.getId().equals(loggedUser) && !user.getRoles().equalsIgnoreCase(userRole)){
				throw new RequestValidationException("User cannot update his/her own role");
			}
			/*Added by Pushpalatha for TENJINCG-648 ends*/
			}
			if(user.getRoles()!=null){
				user = this.validateUserRole(user);
			}else{
				user.setRoles(userdetails.getRoles());
				user = this.validateUserRole(user );
			}
			
			
			if(user.getPassword()!=null){
				/*Modified by Pushpa for TENJINCG-1041 starts*/
				this.validatePassword(user, "Changed Password");
				/*Modified by Pushpa for TENJINCG-1041 ends*/
				/*modified by shruthi for VAPT Bcryption fixes starts*/
				String Password =user.getPassword();
				user.setPassword(Password);
			}
		} catch (NameNotFoundException e) {
			throw new RequestValidationException("Not a valid user role.");
		}
		/*modified by shruthi for VAPT Bcryption fixes ends*/
// added by manish to shift code from validator to handler ends
		logger.info("Updating user");
		userdetails.merge(user);
		this.validateUserDetails(userdetails, false);
		this.userHelper.updateUser(userdetails);
		/*Added by Ashiki for TENJINCG-1023 starts*/
		User newuser=userdetails;
		newuser.setFullName(newuser.getFirstName()+" "+newuser.getLastName());
		persitAudit(newuser,oldUser,user.getId(),loggedUser);
		/*Added by Ashiki for TENJINCG-1023 ends*/
	}

	
	/*Added by Ashiki for TENJINCG-1023 starts*/
	public void persitAudit(User newUser,User oldUser,String userId,String loggedUser) throws DatabaseException {
		ObjectMapper mapper = new ObjectMapper();
		AuditHandler auditHandler = new AuditHandler();
		AuditRecord record=new AuditRecord();
		User userData=new User();
		try {
			
			String newDataJson = mapper.writeValueAsString(newUser);
			String oldDataJson = mapper.writeValueAsString(oldUser);
			record.setOldData(oldDataJson);
			record.setNewData(newDataJson);
			record.setEntityRecordId(0);
			record.setEntityType("user");
			record.setId(userId);
			record.setLastUpdatedBy(loggedUser);
			userData.setAuditRecord(record);
			
			auditHandler.persistAuditRecord(userData.getAuditRecord());
		} catch (JsonProcessingException e) {
			
			logger.error("Error ", e);
		}
	}
	/*Added by Ashiki for TENJINCG-1023 ends*/
	
	
	public void unblockUser(String id, String cUser) throws DatabaseException, ResourceConflictException {
		
		
		
		User user = this.getUser(id);
		
		if(Utilities.trim(user.getEnabledStatus()).equalsIgnoreCase("y")) {
			throw new ResourceConflictException("user.already.enabled", user.getId());
		}
		/*Modified by Preeti for TENJINCG-507 ends*/
		if(Utilities.trim(cUser).equalsIgnoreCase(Utilities.trim(id))){
			throw new ResourceConflictException("user.self.status.change.error");
		}
		
		this.userHelper.unblockUser(id);
		/*Added by Ashiki for TENJINCG-1023 starts*/
		User unblockedUser = this.getUser(id);
		this.persitAudit(unblockedUser, user, id, cUser);
		/*Added by Ashiki for TENJINCG-1023 ends*/
		this.userHelper.clearFailedLoginAttemptRecords(id);

	}

	public void blockUser(String id, String cUser) throws DatabaseException, ResourceConflictException {
		
		
		User user = this.getUser(id);
		
		/*Modified by Preeti for TENJINCG-507 starts*/
		
		if(Utilities.trim(user.getEnabledStatus()).equalsIgnoreCase("N")) {
			throw new ResourceConflictException("user.already.disabled", user.getId());
		}
		/*Modified by Preeti for TENJINCG-507 ends*/
		if(Utilities.trim(cUser).equalsIgnoreCase(Utilities.trim(id))){
			throw new ResourceConflictException("user.self.status.change.error");
		}
		
		this.userHelper.blockUser(id);
		/*Added by Ashiki for TENJINCG-1023 starts*/
		User blockedUser = this.getUser(id);
		this.persitAudit(blockedUser, user, id, cUser);
		/*Added by Ashiki for TENJINCG-1023 ends*/
		
	}
	/*Modified by Pushpa for TENJINCG-1035 starts*/
	/* modified by Roshni for TENJINCG-1168 starts */
	
	public void validateChangePassword(String currentPassword, String newPassword, String confirmPassword, String userId,
			String loggedInUserId,String changeReason,Connection conn) throws RequestValidationException, DatabaseException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
	/* modified by Roshni for TENJINCG-1168 ends */
	/*Modified by Pushpa for TENJINCG-1035 ends*/
		/*Modified by Pushpa for TENJINCG-1041 starts*/
		User user=new UserHelperNew().hydrateUser(conn,userId);
		user.setPassword(newPassword);
	
		if (loggedInUserId.equalsIgnoreCase(userId)) {
			try {
				/*Modified by Pushpa for TENJINCG-1041 starts*/
				user.setPassword(currentPassword);
				boolean passwordOk = new UserHelperNew().isUserPasswordCorrect(loggedInUserId, currentPassword,conn);
				/* modified by Roshni for TENJINCG-1168 ends */
				if (!passwordOk) {
					logger.error("User Current Password validation failed");
					throw new RequestValidationException("user.current.password.false");

					}
				} catch (DatabaseException e) {
				logger.error("User Current Password validation failed");
				throw new RequestValidationException("user.current.password.false");
			}
		}
		
		if (!newPassword.equals(confirmPassword)) {
			String[] var={"New","Confirm"};
			logger.error("User New Password validation failed");
			throw new RequestValidationException("password.mismatch.fail",var);

		}
		if (currentPassword != null && newPassword != null && (currentPassword.equalsIgnoreCase(newPassword))) {
			logger.error("Password is same as old Password Please Try New Password");
			throw new RequestValidationException("password.found.current");
		}
		
		String encryptNewPassword=(newPassword);
		
		if (new UserHelperNew().histPasswordCheck(conn,userId, encryptNewPassword, currentPassword,loggedInUserId,changeReason)) {
		
		} else {
			logger.error("password is available in history");
			throw new RequestValidationException("password.found.history");

		}
		if (newPassword != null && confirmPassword != null && (newPassword.equalsIgnoreCase(confirmPassword))) {

		} else {
			String[] var={"New","Confirm"};
			logger.error("User New Password validation failed");
			throw new RequestValidationException("password.mismatch.fail",var);
			
		}
		
	}
	/*Modified by Pushpa for TENJINCG-1041 starts*/
	public void validatePassword(User user,String passwordName) throws RequestValidationException{
	/*Modified by Pushpa for TENJINCG-1041 ends*/
		/*Modified by pushpa for TENJINCG-1044 starts*/
		String password=user.getPassword();
		char[] passwordArray=password.toCharArray();
		boolean containsUpper=false;
		boolean containsLower=false;

		if(password.length()<1){
			logger.error(passwordName+" field doesn't contain any value");
			throw new RequestValidationException("password is mandatory", passwordName);
		}
		if(Utilities.trim(password).contains(" ")){
			logger.error(passwordName+" should not contain space");
			throw new RequestValidationException("password should not contain space", passwordName);
		}
		if(password.length()<8){
			logger.error(passwordName+" length is less than 8");
			/*Modified by Ashiki for V2.8-5 starts*/
			/*throw new RequestValidationException("password.length.fail", passwordName);*/
			throw new RequestValidationException("password.rules.fail", passwordName);
			/*Modified by Ashiki for V2.8-5 ends*/
		}
		/*Added by Pushpa for TENJINCG-1041 starts*/
		if(password.toUpperCase().contains(user.getId().toUpperCase())){
			logger.error(passwordName+" should not contain User ID");
			throw new RequestValidationException("Password should not contain User ID", passwordName);
		}
		if(password.toUpperCase().contains(user.getFirstName().toUpperCase())){
			logger.error(passwordName+" should not contain User's First Name");
			throw new RequestValidationException("Password should not contain User First Name", passwordName);
		}
		
		/*Modified by Padmavathi to fix password reset issue starts*/
		/*if( !user.getLastName().equalsIgnoreCase("") && password.toUpperCase().contains(user.getLastName().toUpperCase())){*/
		if(user.getLastName()!=null && !user.getLastName().equalsIgnoreCase("") && password.toUpperCase().contains(user.getLastName().toUpperCase())){	
			/*Modified by Padmavathi to fix password reset issue ends*/	
			logger.error(passwordName+" should not contain User's Last Name");
			throw new RequestValidationException("Password should not contain User Last Name", passwordName);
		}
		/*Added by Pushpa for TENJINCG-1041 ends*/
		for(char c:passwordArray)
		{
			if(Character.isUpperCase(c))
			{
				containsUpper=true;
			}
			if(Character.isLowerCase(c)){
				containsLower=true;
			}
			if(containsUpper && containsLower){
				break;
			}
		}
		if(!containsUpper){
			logger.error(passwordName+"should contain atleast one uppercase alphabetic characters");
			/*Modified by Ashiki for V2.8-5 starts*/
			/*throw new RequestValidationException("password.uppercase.fail", passwordName);*/
			throw new RequestValidationException("password.rules.fail", passwordName);
			/*Modified by Ashiki for V2.8-5 ends*/
		}
		if(!containsLower){
			logger.error(passwordName+"should contains atleast one lowercase alphabetic characters");
			/*Modified by Ashiki for V2.8-5 starts*/
			/*throw new RequestValidationException("password.lowercase.fail", passwordName);*/
			throw new RequestValidationException("password.rules.fail", passwordName);
			/*Modified by Ashiki for V2.8-5 ends*/
		}
		//To check Whether password contains number or not
		if(!password.matches(".*\\d.*")){
			logger.error(passwordName+"should have at least one numerical character");
			/*Modified by Ashiki for V2.8-5 starts*/
			/*throw new RequestValidationException("password.number.fail", passwordName);*/
			throw new RequestValidationException("password.rules.fail", passwordName);
			/*Modified by Ashiki for V2.8-5 ends*/
		}
		
		Pattern p = Pattern.compile("[^A-Za-z0-9]");
	    Matcher m = p.matcher(password);
	    boolean b = m.find();
	    if(!b){
	    	logger.error(passwordName+"should have at least one special character");
	    	/*Modified by Ashiki for V2.8-5 starts*/
			/*throw new RequestValidationException("password.splChar.fail", passwordName);*/
	    	throw new RequestValidationException("password.rules.fail", passwordName);
	    	/*Modified by Ashiki for V2.8-5 ends*/
			
	    }
		/*Modified by Pushpa for TENJINCG-1044 ends*/
	}
	/*Modified by Pushpa for TENJINCG-1035 starts*/
	public void updateUserPassword(String currentPassword, String newPassword, String confirmPassword, String userId,
			String loggedInUserId,String changeReason) throws DatabaseException, RequestValidationException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
	/*Modified by Pushpa for TENJINCG-1035 ends*/
		logger.info("Validating user password information");
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);){
			/*Added By Ashiki for TENJINCG-1023 starts*/
			User oldUser = new UserHelperNew().hydrateUser(conn,userId);
			/*Added By Ashiki for TENJINCG-1023 ends*/
			/*Modified by Pushpa for TENJINCG-1035 starts*/
			this.validateChangePassword(currentPassword, newPassword, confirmPassword, userId, loggedInUserId,changeReason,conn);
			/*Modified by Pushpa for TENJINCG-1035 ends*/
			logger.info("Done");
			logger.info("persisting password");
			this.userHelper.updatePassword(userId, newPassword,oldUser);
			/*Added By Ashiki for TENJINCG-1023 starts*/
			User newUser = new UserHelperNew().hydrateUser(conn,userId);
			this.persitAudit(newUser, oldUser, userId, loggedInUserId);
			/*Added By Ashiki for TENJINCG-1023 ends*/
			logger.info("Done");
			
		} /*Modified by Ashiki for Tenj210-9 starts*/
		catch (SQLException e) {
			
			logger.error("Error ", e);
		}
		/*Modified by Ashiki for Tenj210-9 ends*/
		

	}
	public List<User> getActiveUsers(String loggedUser) throws DatabaseException {
		return this.userHelper.hydrateAllActiveUsers(loggedUser);
	}

	public void clearUsers(String[] uArray) throws DatabaseException {
		this.userHelper.clearUser(uArray);
	}
	
	public User authenticate(String userId, String password, String ipAddress) throws DatabaseException,TenjinServletException, DuplicateUserSessionException{
		User user=null;
		int count=0;
		boolean status = false;
		List<FailedLoginAttempts> failedAttempts = null;
		
		boolean auth=false;
		boolean userLocked=false;
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		FailedLoginAttempts attempt = new FailedLoginAttempts();
		
		user=this.getUser(userId);
		
		attempt.setTimeStamp(timeStamp);
		/*modified by shruthi for VAPT Bcryption fixes starts*/
		auth=TenjinSessionHandler.checkPassword(password ,user.getPassword());
		/*modified by shruthi for VAPT Bcryption fixes ends*/
		count=this.userHelper.noOfFailedAttempts(userId);
		status = this.userHelper.userBlockedStatus(userId);
		if (status) {
			throw new DatabaseException(userId+ " is blocked, Please contact administrator");
		}else if(auth==false && count >= 2){
			this.userHelper.blockUser(userId);
			this.userHelper.insertFailedLoginRecords(userId, ipAddress);
			throw new DatabaseException(userId+ " is blocked, Please contact administrator");
		}else if(auth==false && count < 2){
			this.userHelper.insertFailedLoginRecords(userId, ipAddress);
			throw new DatabaseException("Authentication Failed");
		}else if(auth==true && count>2){
			this.userHelper.blockUser(userId);
			this.userHelper.insertFailedLoginRecords(userId, ipAddress);
			throw new DatabaseException(userId+ " is blocked, Please contact administrator");
		}
		else {
			failedAttempts = this.userHelper.hydrateFailedLoginRecords(userId);
			user.setFailedLoginAttempts(failedAttempts);
			logger.debug("Authenticate user {} --> SUCCESS", user.getId());
			this.userHelper.clearFailedLoginAttemptRecords(userId);
		}
		userLocked=this.userHelper.checkLoggedInUser(userId);
		if(userLocked){
			logger.error("User {} is already logged in",userId);
			throw new DuplicateUserSessionException("User is already logged in");
		}
		else{
			this.userHelper.persistCurrentUser(userId,ipAddress);
			String lastLogin=this.userHelper.getuserLastLogIn(userId);
			user.setLastLogin(lastLogin);
			int sessionId=this.userHelper.getSessionId();
			user.setTenjinSessionId(sessionId);
			this.userHelper.persistUserSession(sessionId,userId);
		}
		return user;
	}


	public boolean checkUserPasswordStatus(String userid) throws DatabaseException {
		return this.userHelper.checkUserPasswordStatus(userid);
	}
	
	// added by manish to shift code from validator to handler
	
	public User validateUserRole(User user) throws NameNotFoundException{
		Map<String,String> roles = this.hydrateUserRoles();
		if(!roles.containsKey(user.getRoles())){
			/*	Added by Padmavathi for checking user roles with String starts*/
			if(!roles.containsValue(user.getRoles())){
				/*	Added by Padmavathi for checking user roles with String ends*/
			throw new NameNotFoundException("Not a valid user role.");
			}
		}else{
			user.setRoles(String.valueOf(roles.get(user.getRoles())));
			
		}
		return user;
	}
	
	public Map<String, String> hydrateUserRoles(){
		try {
			return this.userHelper.hydrateUserRole();
		} catch (DatabaseException e) {
			logger.error(e.getMessage());
			return null;
		}
		
	}
	
	public void changeUserStatus(User user , String status, String cUserId) throws ResourceConflictException, DatabaseException{
		if(status.equalsIgnoreCase("enable")){
			new UserHandler().unblockUser(user.getId(), cUserId);
		}else if(status.equalsIgnoreCase("disable")){
			new UserHandler().blockUser(user.getId(), cUserId);
		}else{
			throw new ResourceConflictException("'"+status+"' not a valid status value.");
		}		
	}
	/*Added by Pushpa for TENJINCG-1037 starts*/
	public void updatePasswordSecurity(Map<String,String> usersecurity,String loggedInUserId) throws SQLException, DatabaseException {
		
		new UserHelperNew().updatePasswordSecurity(usersecurity,loggedInUserId);
	}
	/*Added by Pushpa for TENJINCG-1037 ends*/
	
	
	/*Added by Pushpalatha for TENJINCG-1043 starts*/
	public Map<String, String> hydrateSecurityQuestionForUser(String userId) throws SQLException, DatabaseException {
		Map<String,String> securityQueAndAns=this.userHelper.hydrateSecurityQuestionForUser(userId);
		return securityQueAndAns;
	}
	
	/*Added by Puspalatha for TENJINCG-1043 ends*/
	/*Added by Pushpa for TENJINCG-1067,1070 starts*/
	public static List<String> getDaysBetweenDates(Date startdate, Date enddate) throws ParseException
	{
		logger.info("fetching days between dates...");
	    List<String> dates = new ArrayList<String>();
	    Calendar calendar = new GregorianCalendar();
	    calendar.setTime(startdate);
	    int i=0;
	    while (calendar.getTime().before(enddate))
	    {
	    	logger.info("day"+i+" "+calendar.getTime());
	        Date result = calendar.getTime();
	        String pattern = "dd-MM-yy";
	        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

	        String c = simpleDateFormat.format(result);
	        logger.info("formated date="+c);
	        dates.add(c);
	        calendar.add(Calendar.DATE, 1);
	        
	    }
	    return dates;
	}
	public ArrayList<HashMap<String, Object>> getUserActivityReport(String userForReport,String startDate,String endDate) throws SQLException, DatabaseException, ParseException {
		
		 Date date1=new SimpleDateFormat("dd-MMM-yyyy").parse(startDate); 
		 Date date2=new SimpleDateFormat("dd-MMM-yyyy").parse(endDate);  
		List<String> dates=this.getDaysBetweenDates(date1, date2);
		logger.info("size od date list="+dates.size());
		
	 	ArrayList<HashMap<String, Object>> list=new UserHelperNew().getUserActivityReport(userForReport,startDate,endDate,dates);
		return list;
	}
	/*Added by Pushpa for TENJINCG-1067,1070 starts*/
	
	/*Added by Pushpa for TENJINCG-1170 starts*/
	public void validateUserUpdate(String userId, User cUser) throws RequestValidationException {
		
		if(!(cUser.getId().equalsIgnoreCase(userId))&&cUser.getRoles().equalsIgnoreCase("Tenjin User")){
			throw new RequestValidationException("Tenjin user can not view other user profile");
		}
		
	}
	/*Added by Pushpa for TENJINCG-1170 ends*/
	/*added by shruthi for VAPT fixes starts*/
	public String hydrateCount(String userid) throws SQLException, DatabaseException {
		return this.userHelper.hydrateCount(userid);
	}
	public void updateCount(String userid, String count) throws DatabaseException {
		this.userHelper.updateCount(userid , count);
	}
	
	/*added by shruthi for VAPT fixes ends*/

}
