/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UnstructuredDataHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 08-04-2020			Ashiki			Newly added for TENJINCG-1204
* */
package com.ycs.tenjin.handler;


import java.util.ArrayList;

import com.ycs.tenjin.ValidationDetails;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.UnstructuredDataHelper;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.util.Utilities;

public class UnstructuredDataHandler {

	UnstructuredDataHelper helper = new UnstructuredDataHelper();
	public void persistUnstructuredData(ValidationDetails validateDetails) throws DatabaseException,RequestValidationException {
		this.validateUnstructuredData(validateDetails);
		helper.persistUnstructuredData(validateDetails);
	}

	private void validateUnstructuredData(ValidationDetails validateDetails) throws RequestValidationException {
		if(Utilities.trim(validateDetails.getValidationName()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "Validation Type");
		}
		if(Utilities.trim(validateDetails.getSearchText()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "Search Text");
		}

		if(Utilities.trim(validateDetails.getValidationName()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "Validation Type");
		}
		if(Utilities.trim(validateDetails.getSearchText()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "Search text");
		}
		if(Utilities.trim(validateDetails.getFileType()).equalsIgnoreCase("excel")&&Utilities.trim(validateDetails.getSheetName()).length()<1) {
			throw new RequestValidationException("field.mandatory", new String[] {" Sheet Name"});
		}
		if(Utilities.trim(validateDetails.getValidationName()).equalsIgnoreCase("Text validation") ) {
			if(Utilities.trim(validateDetails.getCellReference()).length()<1)
			throw new RequestValidationException("field.mandatory", "Cell Reference ");
		}
		if(Utilities.trim(validateDetails.getValidationName()).equalsIgnoreCase("Text occurrence validation") ||Utilities.trim(validateDetails.getValidationName()).equalsIgnoreCase("Text occurrence validation in cell range")||Utilities.trim(validateDetails.getValidationName()).equalsIgnoreCase("Text occurrence validation in page") ) {
			if(validateDetails.getExpectedOccurance()<1)
			throw new RequestValidationException("field.mandatory", "Expected Occurance ");
		}
		if(Utilities.trim(validateDetails.getValidationName()).equalsIgnoreCase("Text occurrence validation in cell range") && Utilities.trim(validateDetails.getCellRange()).length()<1) {
			throw new RequestValidationException("field.mandatory", "Cell Range ");
		}
		if(Utilities.trim(validateDetails.getValidationName()).equalsIgnoreCase("Text occurrence validation in page")||Utilities.trim(validateDetails.getValidationName()).equalsIgnoreCase("Text occurrence in page") ) {
			if(validateDetails.getPageNumber()<1)
			throw new RequestValidationException("field.mandatory", "Page Number ");
		}
	
		
	}

	public  ArrayList<ValidationDetails>  hydrateUnstructuredData(int stepRecordId) throws DatabaseException{
		return helper.hydrateUnstructuredData(stepRecordId);		
	}

	public ValidationDetails getValidationdetailsById(int recId) throws DatabaseException {
		return helper.hydrateValidation(recId);
	}

	public void deleteUnstructuredData(String[] selectedRecIds) throws DatabaseException {
		helper.deleteUnstructuredData(selectedRecIds);		
	}

	public void updateUnstructuredData(ValidationDetails validateDetails, int recId) throws RequestValidationException, DatabaseException {
		this.validateUnstructuredData(validateDetails);
		helper.updateUnstructuredData(validateDetails,recId);
		
	}


}
