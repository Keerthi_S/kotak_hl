/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AuditHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 18-02-2019			Preeti					TENJINCG-969
* 19-09-2019			Preeti					TENJINCG-1068,1069
*/
package com.ycs.tenjin.handler;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.AuditHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.project.AuditRecord;

public class AuditHandler {
	private static final Logger logger = LoggerFactory.getLogger(AuditHandler.class);
	private AuditHelper helper = new AuditHelper();
	public void persistAuditRecord(AuditRecord record) throws DatabaseException{
		logger.info("persisting audit record");
		helper.persistAuditRecord(record);
	}
	/*Added by Preeti for TENJINCG-1068,1069 starts*/
	public ArrayList<AuditRecord> getAuditRecord(int entityRecId, String entityType, String fromDate, String toDate) throws DatabaseException{
		logger.info("Hydrating audit record");
		return helper.hydrateAuditRecord(entityRecId, entityType, fromDate, toDate);
	}
	/*Added by Preeti for TENJINCG-1068,1069 ends*/
}
