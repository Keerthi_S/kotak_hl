/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 15-10-2018		   	Pushpalatha			  	Newly Added 
* 17-10-2018        	Leelaprasad           	TENJINCG-827
* 19-10-2018           	Leelaprasad             TENJINCG-829
* 19-10-2018           	Sriram             		TENJINCG-873
* 02-11-2018			Sriram					TENJINCG-896
* 08-11-2018         	Leelaprasad           	TENJINCG-874
* 28-11-2018			Ramya					TJNUN262-5
* 18-02-2019			Preeti					TENJINCG-969
* 29-03-2019			Preeti					TENJINCG-1003
* 11-04-2019           Padmavathi               TNJN27-84
* 03-06-2019            Leelaprasad             TENJINCG-1068,TENJINCG-1069
* 19-09-2019			Preeti					TENJINCG-1068,1069
* 14-05-2021            Paneendra               Tenj212-8
*/
package com.ycs.tenjin.handler;


import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ProjectAutHelper;
import com.ycs.tenjin.db.ProjectDTTHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.db.ProjectHelperNew;
import com.ycs.tenjin.db.ProjectTestDataPathHelper;
import com.ycs.tenjin.db.ProjectTestMangerHelper;
import com.ycs.tenjin.db.ProjectUserHelper;
import com.ycs.tenjin.defect.DefectManagementInstance;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.project.Domain;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.ProjectTestDataPath;
import com.ycs.tenjin.testmanager.TestManagerInstance;
import com.ycs.tenjin.user.User;

public class ProjectHandler {
	private static final Logger logger = LoggerFactory.getLogger(Project.class);
	
	private ProjectHelperNew projectHelper=new ProjectHelperNew();
	private AuditHandler auditHandler = new AuditHandler();
	public Project persistProject(Project project) throws DatabaseException, RequestValidationException {
		
		
		if(project.getName().length()>60){
			throw new RequestValidationException("field.length", new String[] {"Name","60"});
		}
		if(project.getDescription()!=null && project.getDescription().length()>1000){
			throw new RequestValidationException("field.length", new String[] {"Description","1000"});
		}
		Project prj=this.projectHelper.hydrateProject(project.getDomain(), project.getName());
		if(prj!=null){
			throw new DuplicateRecordException("project.already.exists", project.getName());
		}
		/*added by shruthi for Tenj212-3 starts*/
		if(project.getRepoType().equalsIgnoreCase("AWS S3"))
		{
			prj=this.projectHelper.persistProject(project);
		}
		else
		{
		File file = new File(project.getRepoRoot());
		if(!file.exists()){
			if(!file.mkdirs()){
				throw new RequestValidationException("Please enter valid path" );
			}
		}
		
		if(file.isAbsolute()) {
			prj=this.projectHelper.persistProject(project);
		}
		else{
			throw new RequestValidationException("Please enter valid path" );
		}
		}
		/*added by shruthi for Tenj212-3 ends*/
		return prj;
	}
	/*Added by Preeti for TENJINCG-1003 starts*/
	public String hydrateCurrentUserRoleForProject(String userId, int currentProjectid) throws DatabaseException {
		String prjUserRole = this.projectHelper.hydrateCurrentUserRoleForProject(userId, currentProjectid);
		return prjUserRole;
	}
	/*Added by Preeti for TENJINCG-1003 ends*/
	public Project hydrateProject(int projectId) throws DatabaseException {
		
		Project project=this.projectHelper.hydrateProject(projectId);
		return project;
	}

	public void changeProjectState(int prjId,String currentstate){
		String state=null;
		if(currentstate.equalsIgnoreCase("A")){
			state="I";
		}else if (currentstate.equalsIgnoreCase("I")){
			state="A";
		}
		
		this.projectHelper.modifyProjectState(prjId,state);
	}
	public void updateProject(Project project) throws DatabaseException{
		Project proejctToUpdate=this.hydrateProject(project.getId());
		/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		/*Modified by Preeti for TENJINCG-1068,1069 starts*/
		Project projectOld= new Project();
		projectOld.merge(proejctToUpdate);
		Project projectNew=new Project();
		projectNew.merge(project);
		/*Modified by Preeti for TENJINCG-1068,1069 ends*/
		/*Added by Leelaprasad for TENJINCG-1068,1069 ends*/
		proejctToUpdate.merge(project);
		this.projectHelper.updateProject(proejctToUpdate);
		/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		 ObjectMapper mapper = new ObjectMapper();
			try {
				/*Added by Preeti for TENJINCG-1068,1069 starts*/
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				mapper.setDateFormat(df);
				/*Added by Preeti for TENJINCG-1068,1069 ends*/
				String newDataJson = mapper.writeValueAsString(projectNew);
				String oldDataJson = mapper.writeValueAsString(projectOld);
				project.getAuditRecord().setNewData(newDataJson);
				project.getAuditRecord().setOldData(oldDataJson);
			} catch (JsonProcessingException e) {
				logger.error("Error while persisting audit record");
				logger.error("Error ", e);
			}
			/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		/*Added by Preeti for TENJINCG-969 starts*/
		this.auditHandler.persistAuditRecord(project.getAuditRecord());
		/*Added by Preeti for TENJINCG-969 ends*/
	}
	public ArrayList<User> hydrateUsersNotInProject(int projectId) throws DatabaseException {
		
		ArrayList<User> users=new ProjectUserHelper().hydrateUsersNotInProject(projectId);
		return users;
	}

	public void mapUsersToProject(int projectId, String userList,String projectType) throws DatabaseException,RequestValidationException {
		
		new ProjectUserHelper().mapUsersToProject(projectId,userList,projectType);
	}

	public void unmapUsersFromProject(int projectId, String userList) throws DatabaseException {
		
		new ProjectUserHelper().unmapUsersFromProject(projectId,userList);
	}


	public ArrayList<Aut> hydrateUnmappedAuts(int projectId) throws DatabaseException {
		
		 int i=projectId;
		ArrayList<Aut> auts=new ProjectAutHelper().hydrateUnmappedAuts(i);
		return auts;
	}


	public ArrayList<Aut> hydratePrjectAuts(int projectId) throws DatabaseException {
		
		ArrayList<Aut> auts=new ProjectAutHelper().hydratePrjectAuts(projectId);
		return auts;
	}


	public void mapAutsToProject(int projectId, String auList) throws DatabaseException {
		
		new ProjectAutHelper().mapAutsToProject(projectId,auList);
	}


	public void unmapAutsFromProject(int projectId, String autList) throws DatabaseException ,RequestValidationException, SQLException{
		
		new ProjectAutHelper().unmapAutsFromProject(projectId,autList);
	}


	public void updateAutUrl(ArrayList<Aut> autList, int projectId) throws DatabaseException {
		
		new ProjectAutHelper().updateAutUrl(autList,projectId);
	}
	
	/*changed by Leelaprasad for TENJINCG-827 starts*/
	public void startProjectSession(int projectId,String userId) throws DatabaseException{
		this.projectHelper.persistProjectSession(projectId, userId);
	}
	public void endProjectSession(int projectId,String userId) throws DatabaseException{
		this.projectHelper.updateProjectSession(projectId, userId);
	}
	/*changed by Leelaprasad for TENJINCG-827 ends*/
	/*Changed by leelaprasad for TENJINCG-829 starts*/
	public void updateUserProjectPrefrence(int projectId,String userId) {
		this.projectHelper.updateUserProjectPreference(projectId, userId);
	}
	public int hydrateUserProjectPreference(String userId) {
		return this.projectHelper.hydrateUserProjectPreference(userId);
	}
	public ArrayList<Project> hydrateAllProjectsForUser(String userId) throws DatabaseException{
		return this.projectHelper.hydrateAllProjectsForUser(userId);
		
	}
	/*Changed by leelaprasad for TENJINCG-829 ends*/
	
	//SRIRAM for TENJINCG-873
	public List<User> getProjectUsers(int projectId) throws DatabaseException {
		return this.projectHelper.hydrateProjectUsers(projectId);
	}
	//SRIRAM for TENJINCG-873 ends
/*Added by Preeti for TENJINCG-850 starts*/
	public ArrayList<Project> hydrateProjectsForCurrentUser(String userId, int currentProjectid) throws DatabaseException {
		ArrayList<Project> projects = this.projectHelper.hydrateProjectsForCurrentUser(userId, currentProjectid);
		return projects;
	}
	
	public ArrayList<Integer> hydrateProjectAutIds(Connection conn, int projectId) throws DatabaseException{
		ArrayList<Integer> projectAppIds = this.projectHelper.hydrateProjectAutIds(conn, projectId);
		return projectAppIds;
	}
	
	/*Added by Preeti for TENJINCG-850 ends*/
	public ArrayList<ProjectTestDataPath> hydrateProjectTDP(int projectId, String applicationId, String groupName) throws DatabaseException {
			
		int appId;
		 try {
			 appId=Integer.parseInt(applicationId);
	        } catch (NumberFormatException e) {
	        	appId=0;
	        }
		ArrayList<ProjectTestDataPath> projectTDP=new ArrayList<ProjectTestDataPath>(); 
		projectTDP=new ProjectTestDataPathHelper().hydrateTDP(projectId,appId,groupName);
		
		return projectTDP;
	}


	
	//TENJINCG-875 (Sriram)
	public List<Domain> getProjectTree() throws DatabaseException {
		//TENJINCG-896 (Sriram)
		return this.projectHelper.hydrateAllProjectsGroupedByDomain();
		//TENJINCG-896 (Sriram) ends
	}
	
	public List<Domain> getProjectTree(String owner) throws DatabaseException {
		//TENJINCG-896 (Sriram)
		return this.projectHelper.hydrateAllProjectsGroupedByDomain(owner);
		//TENJINCG-896 (Sriram) ends
	}
	
		/*Added by Padmavathi for TENJINCG-824 starts*/
		public boolean checkProjectAccessForuser(String userId,int projectId) throws DatabaseException{
			return this.projectHelper.checkProjectAccessForUser(userId, projectId);
		}
		/*Added by Padmavathi for TENJINCG-824 ends*/

		
/*Added by Leelaprasa dfor TENJINCG-874 starts*/
		
		ProjectTestMangerHelper prjoectTestmangerHelper = new ProjectTestMangerHelper();

		public void validateInstanceCredintials(String tjnUserId,
				String instanceName) throws DatabaseException {
			
			this.prjoectTestmangerHelper.validateInstanceCredintials(tjnUserId,
					instanceName);
		}

		public void updateProjectTestManagerDetails(Project project)
				throws DatabaseException {
			
			this.prjoectTestmangerHelper.updateProjectTestManagerDetails(project);
		}

		public void updateFilterValues(Project project) throws DatabaseException {
			
			this.prjoectTestmangerHelper.updateFilterValues(project);
		}

		public TestManagerInstance hydrateTMCredentialsForProject(
				String etmInstance, String tjnUserId, String tjnPrjId)
				throws DatabaseException {
			
			return this.prjoectTestmangerHelper.hydrateTMCredentialsForProject(
					etmInstance, tjnUserId, tjnPrjId);
		}

		public Project hydrateProjectETMDetails(String tjnPrjId)
				throws DatabaseException {
			
			return this.prjoectTestmangerHelper.hydrateProjectETMDetails(tjnPrjId);
		}
		/*Added by Leelaprasa dfor TENJINCG-874 ends*/


		public ArrayList<Aut> hydrateProjectDTT(int projectId) throws SQLException, DatabaseException {
			
			ProjectDTTHelper projectDTTHelper=new ProjectDTTHelper();
			ArrayList<Aut> auts=projectDTTHelper.hydrateProjectDTT(projectId);
			return auts;
		}


	

		public List<DefectManagementInstance> fetchDefectManagerInstance() throws DatabaseException {
			
			List<DefectManagementInstance> dmInstance=new ProjectDTTHelper().fetchDefectManagerInstance();
			return dmInstance;
		}

		/*Added by Ramya for TJNUN262-5 Starts*/
		public User hydrateCurrentUserForProject(String id, int projectId) throws DatabaseException {
			
			User user=new ProjectHelperNew().hydrateCurrentUserForProject(id,projectId);
			return user;
		}
		/*Added by Ramya for TJNUN262-5 Ends*/
		
		/*Added by Padmavathi for TNJN27-84 starts*/
		public void validateProjectUserRoles(int projectId,int prjAdminCount) throws DatabaseException, RequestValidationException{

			
			List<User> users=new LinkedList<>();
			int count=0;
			users = this.getProjectUsers(projectId);
			for(User user:users){
				if(user.getRoleInCurrentProject().equals("Project Administrator")){
					count++;
				}
			}
			if((count-prjAdminCount)<2){
				throw new RequestValidationException("Public project should have minimum two Project Administrator.");
			}
		}
		/*Added by Padmavathi for TNJN27-84 ends*/
		public void persistProject(String accessKey, String accessSecKey) throws DatabaseException {
			ProjectHelper projectHelper = new ProjectHelper();
		  projectHelper.persistProject(accessKey,accessSecKey);
			return ;
		}
		public Project hydrateAwsDeatils() {
			Project project =new Project();
			ProjectHelper projectHelper = new ProjectHelper();
			project = projectHelper.hydrateAwsDeatils();
			return project;
		}
		
		
		
}
