/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TenjinMailHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 02-May-2019        	 Roshni          Newly Added For TENJINCG-1046
 * 06-May-2019			 Roshni					TENJINCG-1036
 */


package com.ycs.tenjin.handler;

import java.sql.Timestamp;
import java.util.Date;

import org.json.JSONObject;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.mail.TenjinJavaxMail;
import com.ycs.tenjin.project.TenjinMailContent;
import com.ycs.tenjin.user.User;

public class TenjinMailHandler {
	public void sendEmailNotification(JSONObject mailDetails) throws TenjinConfigurationException{
		new TenjinJavaxMail().buildTenjinMailInformation(mailDetails);;	
	}
	
	public JSONObject getMailContent(String entityId,String entityName,String entityType,String userName,String operation,Timestamp dateTime,String userEmailId,String parentId){
		TenjinMailContent mailContent=new TenjinMailContent();
		mailContent.setEntityType(entityType);
		mailContent.setEntityId(entityId);
		mailContent.setEntityName(entityName);
		mailContent.setUserName(userName);
		mailContent.setUserEmailId(userEmailId);
		mailContent.setOperation(operation);	
		mailContent.setDateTime(new Timestamp(new Date().getTime()));
		mailContent.setParentId(parentId);
		JSONObject mailDetails=new JSONObject(mailContent);
		return mailDetails;
	}
	/* Added by Roshni for TENJINCG-1036 starts */
	public void sendPasswordResetMail(JSONObject mailDetails) throws TenjinConfigurationException{
		new TenjinJavaxMail().buildTenjinPwdResetMailInformation(mailDetails);	
	}
	public JSONObject getPasswordResetContent(User user, String url){
		TenjinMailContent mailContent=new TenjinMailContent();
		mailContent.setEntityId(user.getId());
		mailContent.setEntityName(url);
		mailContent.setUserEmailId(user.getEmail());
		mailContent.setUserName(user.getFirstName());
		JSONObject mailDetails=new JSONObject(mailContent);
		return mailDetails;
	}
	/* Added by Roshni for TENJINCG-1036 ends */
}
