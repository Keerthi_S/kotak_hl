/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestRunHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright &copy 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 22-10-2018			Sriram					Newly added for TENJINCG-866
 * 13-03-2020			Sriram					TENJINCG-1196 
 */

package com.ycs.tenjin.handler;

import java.sql.Connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.PaginatedRecords;
import com.ycs.tenjin.db.TestRunHelper;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.run.TestRunProgress;
import com.ycs.tenjin.run.TestRunSearchRequest;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;

public class TestRunHandler {
	private static final Logger logger = LoggerFactory.getLogger(TestRunHandler.class);
	
	private TestRunHelper testRunHelper = new TestRunHelper();
	
	public PaginatedRecords<TestRun> searchForRuns(TestRunSearchRequest request, int maxRecords, int pageNumber, String sortColumn, String sortDirection) throws DatabaseException {
		logger.info("Searching for Runs");
		PaginatedRecords<TestRun> records = this.testRunHelper.searchRuns(request, maxRecords, pageNumber, sortColumn, sortDirection);
		logger.info("{} runs found in this draw", records.getCurrentSize());
		return records;
	}
	// TENJINCG-1196 (Sriram)
	public TestRunProgress getRunProgress(int runId) throws TenjinServletException{
		TestRunProgress progress = new TestRunProgress();
		TestRunHelper helper = new TestRunHelper();
		
		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ)) {
			progress = helper.hydrateTestCaseExecutionSummary(conn, runId);
			
			TestStep currentStep = helper.hydrateCurrentlyExecutedStep(conn, runId);
			progress.getTestCaseExecutionProgress().setCurrentTestStep(currentStep);
		}catch(Exception e) {
			throw new TenjinServletException("Could not fetch execution progress", e);
		}
		
		return progress;
	}
	// TENJINCG-1196 (Sriram) ends
}
