/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AjaxResponse.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                  CHANGED BY              DESCRIPTION
 * 24-Oct-2017           Sriram Sridharan        Newly Added For TENJINCG-399
 * 13-11-2017			 Preeti					 TENJINCG-411
 * 04-12-2017            Padmavathi              TCGST-9 
 * 07-Dec-2017           Manish                  to make menu mandatory field
 * 08-Feb-2018           Padmavathi              TENJINCG-601
* 18-06-2018			 Preeti					 T251IT-56
* 18-06-2018             Padmavathi              T251IT-61
* 21-06-2018			 Ashiki					 T251IT-96
* 23-11-2018             Padmavathi              TENJINCG-903
* 09-10-2019			 Pushpalatha			 TJN252-7
* 05-02-2020			 Roshni					TENJINCG-1168
* 31-03-2021             Paneendra              TENJINCG-1267
 */

package com.ycs.tenjin.handler;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ExtractorResultBean;
import com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ModuleHelper;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class ModuleHandler {
	private static final Logger logger = LoggerFactory.getLogger(ModuleHandler.class);
	private ModuleHelper moduleHelper = new ModuleHelper();
	
	public List<Module> getModules(int appId, String groupName) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if(Utilities.trim(groupName).length() > 1) {
		/* modified by Roshni for TENJINCG-1168 starts */
			return this.moduleHelper.hydrateModules(conn,appId, groupName);
		}else {
			return this.moduleHelper.hydrateModules(conn,appId, "ALL");
		/* modified by Roshni for TENJINCG-1168 ends */
		}
	}
	
	public List<Module> getModules(int appId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		/* modified by Roshni for TENJINCG-1168 starts */
		return this.moduleHelper.hydrateModules(conn,appId, "ALL");
		/* modified by Roshni for TENJINCG-1168 starts */
	}
	
	public Module getModule(int appId, String moduleCode) throws DatabaseException {
		Module module = this.moduleHelper.hydrateModule(appId, moduleCode);
		if(module == null) {
			logger.error("ERROR function [{}] does not exist for app id {}", moduleCode, appId);
			throw new RecordNotFoundException();
		}
		
		return module;
	}
	
	public void persist(Module module, int appId) throws DatabaseException, RequestValidationException {
		/*Modified by Padmavathi for T251IT-61 starts*/
		this.validateModule(module, appId, true,"persist");
		/*Modified by Padmavathi for T251IT-61 ends*/
		logger.info("Function validation completed.");
	
		this.moduleHelper.persistModule(module, appId);
		logger.info("Function {} created successfully under application {}", module.getCode(), appId);
	}
	
	public void update(String moduleCode, Module module, int appId) throws DatabaseException, RequestValidationException {
		
		Aut aut = new AutHandler().getApplication(appId);
		if(aut == null) {
			logger.error("ERROR - Application {} does not exist.");
			throw new RecordNotFoundException();
		}
		
		if(!moduleCode.equalsIgnoreCase(module.getCode())) {
			logger.error("ERROR - Illegal update. Module Code cannot be udpated. Old value [{}], New Value [{}]", moduleCode, module.getCode());
			throw new RequestValidationException("illegal.field.update", "Function Code");
		}
		
		Module moduleToUpdate = this.getModule(appId, moduleCode);
		if(moduleToUpdate == null ) {
			logger.error("ERROR - Module {} is undefined for application {}", module.getCode(), appId);
			throw new RecordNotFoundException();
		}
		
		moduleToUpdate.merge(module);
		/*Modified by Padmavathi for T251IT-61 starts*/
		this.validateModule(moduleToUpdate, appId, false,"update");
		/*Modified by Padmavathi for T251IT-61 ends*/
		logger.info("Function validation completed.");
		
		this.moduleHelper.updateModule(moduleToUpdate, appId);
		logger.info("Function {} under application {} updated successfully", moduleCode, appId);
		
		
	}
	/*Modified by Padmavathi for T251IT-61 starts*/
	/*private void validateModule(Module module, int appId, boolean duplicateCheckRequired) throws RequestValidationException, DatabaseException {*/
	private void validateModule(Module module, int appId, boolean duplicateCheckRequired,String checkReq) throws RequestValidationException, DatabaseException {
		/*Modified by Padmavathi for T251IT-61 ends*/
		logger.info("Validating function information");
		
		logger.debug("Validating aut");
		Aut aut = new AutHandler().getApplication(appId);
		if(aut == null) {
			logger.error("ERROR - Application not found with ID {}", appId);
			throw new RecordNotFoundException();
		}
		
		if(Utilities.trim(module.getCode()).length() < 1) {
			logger.error("ERROR - Function code is blank");
			throw new RequestValidationException("field.mandatory", "Function Code");
		}
		/* 	Added by Preeti for TENJINCG-411 starts */
		if((!Utilities.trim(module.getCode()).matches("\\s*\\S+\\s*"))){
			logger.error("ERROR - Function code can not contain space.");
			throw new RequestValidationException("space.not.allowed","Function Code");
		}
		/* 	Added by Preeti for TENJINCG-411 ends */
		if(Utilities.trim(module.getName()).length() < 1) {
			logger.error("ERROR - Function name is blank");
			throw new RequestValidationException("field.mandatory", "Function Name");
		}
		
		if(Utilities.trim(module.getMenuContainer()).length() < 1) {
			/*logger.error("ERROR - Function menu is blank");*/
			/*changed by manish to make menu mandatory field starts*/
			logger.warn("WARNING - Menu is blank. Defaulting it to {}", module.getCode());
			throw new RequestValidationException("field.mandatory", "Menu");
			/*changed by manish to make menu mandatory field ends*/
		}
		/*Modified by Padmavathi for T251IT-61 starts*/
		if(Utilities.trim(module.getDateFormat()).length()<1 && checkReq.equalsIgnoreCase("persist")) {
			logger.warn("Date format not specified. Defaulting it from Application - {}", aut.getDateFormat());
			module.setDateFormat(aut.getDateFormat());
		}
		/*Modified by Padmavathi for T251IT-61 ends*/
		
		/*Added by Ashiki for T251IT-96 starts*/
       if(Utilities.trim(module.getDateFormat()).length()>50){
			throw new RequestValidationException("field.length", new String[] {"Date Format","50"});

       }
       /*Added by Ashiki for T251IT-96 ends*/
	
	   /*Added by Padmavathi for TCGST-9 starts*/
       
    
       /*Modified by Ashiki for T251IT-96 starts*/
		if(Utilities.trim(module.getDateFormat())!=null){
		/* Modified by Ashiki for T251IT-96 ends*/

			String dateFormat=Utilities.trim(module.getDateFormat());
			try{
				SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
				sdf.format(new Date());
				module.setDateFormat( dateFormat);
			}catch(Exception e){
				logger.error("ERROR - invalid dateformat module.getDateFormat()");
				throw new RequestValidationException("aut.dateformat");
			}
		}
		/*Added by Padmavathi for TCGST-9 ends*/
		
		if(Utilities.trim(module.getGroup()).length() < 1) {
			logger.warn("WARNING - Group is blank. Defaulting it to Ungrouped");
			module.setGroup("Ungrouped");
		}
		/*Added by Preeti for T251IT-56 starts*/
		//Modified by Keerthi for TJN27-183 starts
		if(Utilities.trim(module.getCode()).length() > 50) {
			throw new RequestValidationException("field.length", new String[] {"Function code","50"});
		//Modified by Keerthi for TJN27-183 Ends
		}
		
		if(Utilities.trim(module.getName()).length() > 200) {
			throw new RequestValidationException("field.length", new String[] {"Function name","200"});
		}
		
		if(Utilities.trim(module.getMenuContainer()).length() > 200) {
			throw new RequestValidationException("field.length", new String[] {"Menu","200"});
		}
		
		if(Utilities.trim(module.getGroup()).length() > 100) {
			throw new RequestValidationException("field.length", new String[] {"Group","100"});
		}
		/*Added by Preeti for T251IT-56 ends*/
		
		
		
		if(duplicateCheckRequired) {
			if(this.moduleHelper.hydrateModule(appId, module.getCode()) != null) {
				logger.error("ERROR - Module {} already exists for application {}", module.getCode(), appId);
				throw new DuplicateRecordException("function.already.exists", new String[] {module.getCode(), aut.getName()});
			}
			/*Added by Padmavathi for TENJINCG-601 starts*/
			 if(new ApiHelper().hydrateApi(appId,module.getCode())!=null){
				logger.error("ERROR - API {} already exists for application {}", module.getCode(), appId);
				throw new DuplicateRecordException("module.api.already.exists", new String[] {module.getCode(), aut.getName()});
			}	
			/*Added by Padmavathi for TENJINCG-601 ends*/
		}
	}
	
	
	public void deleteModules(int appId, String[] moduleCodes) throws DatabaseException, ResourceConflictException {
		
		
		
		logger.debug("Getting database connection");
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		logger.debug("Done");
		
		for(String moduleCode : moduleCodes) {
			logger.debug("Validating module {} for deletion", moduleCode);
			try {
				
				if(this.moduleHelper.hydrateModule(conn, appId, moduleCode) == null) {
					logger.warn("WARNING - Module supplied for deletion {} does not exist", moduleCode);
					continue;
				}
				
				if(this.moduleHelper.isModuleCurrentlyBeingLearnt(conn, appId, moduleCode)) {
					logger.error("ERROR deleting module - Module {} is either being learnt currently or queued for learning.");
					throw new ResourceConflictException("function.delete.learning.conflict", moduleCode);
				}
				
				if(this.moduleHelper.isModuleMappedToTestStep(conn, appId, moduleCode)) {
					logger.error("ERROR deleting module - Module {} is mapped to one or more test steps");
					throw new ResourceConflictException("function.delete.step.conflict", moduleCode);
				}
			} catch (ResourceConflictException e) {
				throw e;
			}/*Added by paneendra for TENJINCG-1267 starts*/
			finally {
                DatabaseHelper.close(conn);
			}
			/*Added by paneendra for TENJINCG-1267 ends*/
		}
		
		logger.info("All validations complete. Safe to delete modules {}", (Object[]) moduleCodes);
		this.moduleHelper.deleteModules(appId, moduleCodes);
		logger.info("Successfully deleted modules {} from app {}", moduleCodes, appId);
	}
	
	public List<LearnerResultBean> getLearningHistory(int appId, String functionCode) throws DatabaseException{
		logger.debug("Getting Learning History for App {}, Function {}", appId, functionCode);
		return this.moduleHelper.hydrateLearningHistory(appId, functionCode);
	}
	
	public List<ExtractorResultBean> getExtractionHistory(int appId, String functionCode) throws DatabaseException {
		logger.debug("Getting extraction history for App {}, function {}", appId, functionCode);
		return this.moduleHelper.hydrateExtractionHistory(appId, functionCode);
	}

	/*Added by Pushpalatha for TJN252-7 starts*/
	/* modified by Roshni for TENJINCG-1168 starts */
	public void validateFunc(List<String> funcs, Integer appId,Connection conn) throws DatabaseException, RequestValidationException {
		/*this.moduleHelper.validateFunc(funcs, appId);*/
		this.moduleHelper.validateFunc(funcs, appId,conn);
	/* modified by Roshni for TENJINCG-1168 ends */
	}
	/*Added by Pushpalatha for TJN252-7 ends*/
	
}
