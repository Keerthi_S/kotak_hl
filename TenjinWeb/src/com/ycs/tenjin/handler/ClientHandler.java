/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ClientHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                  CHANGED BY              DESCRIPTION
* 19-Oct-2017		    Sriram		   			Newly added for TENJINCG-397
* 08-11-2017			Preeti Singh			TENJINCG-410
* 15-11-2017			Preeti Singh			TENJINCG-410
* \20-11-2017			Preeti					TENJINCG-480
* 28-06-2018            Padmavathi              T251IT-160
* 28-02-2019            Leelaprasad P           TENJINCG-983
* 08-03-2019			Sahana					pCloudy
* 22-05-2020			Ashiki					Tenj210-36
*/


package com.ycs.tenjin.handler;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

public class ClientHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(ClientHandler.class);
	
	private ClientHelper clientHelper = new ClientHelper();
	
	public List<RegisteredClient> getAllClients() throws DatabaseException {
		return this.clientHelper.hydrateAllClients();
	}
	
	public RegisteredClient getClient(String clientName) throws DatabaseException {
		RegisteredClient client = this.clientHelper.hydrateClient(clientName);
		if(client == null) {
			throw new RecordNotFoundException("client.not.found");
		}else {
			return client;
		}
	}
	
	public RegisteredClient getClient(int recordId) throws DatabaseException {
		RegisteredClient client = this.clientHelper.hydrateClient(recordId);
		if(client == null) {
			throw new RecordNotFoundException("client.not.found");
		}else {
			return client;
		}
	}
	
	public void persist(RegisteredClient client, String createdBy) throws DatabaseException, RequestValidationException, ResourceConflictException, TenjinConfigurationException {
		logger.info("Validating client information");
		this.validateClientDetails(client, true);
		
		/*Added by Padmavathi for T251IT-160 starts*/
		if(this.clientHelper.hydrateClientByHostName(Utilities.trim(client.getHostName())) != null) {
			throw new DuplicateRecordException("client.already.exists.hostname", client.getHostName());
		}
		/*Added by Padmavathi for T251IT-160 ends*/
		logger.info("Done");
		client.setCreatedOn(new Date());
		
		/*Added by Preeti for TENJINCG-410 starts*/
		/*Added by Preeti for TENJINCG-410 ends*/
		logger.info("Registering client");
		/*added by Sahana for pCloudy: starts*/
		try {
			if(client.getDeviceFarmPwd()!=null && !client.getDeviceFarmPwd().isEmpty())
				/*Modified by paneendra for VAPT FIX starts*/
				/*client.setDeviceFarmPwd(new Crypto().encrypt(client.getDeviceFarmPwd()));*/
			      client.setDeviceFarmPwd(new CryptoUtilities().encrypt(client.getDeviceFarmPwd()));
			/*Modified by paneendra for VAPT FIX ends*/
			if(client.getDeviceFarmKey()!=null && !client.getDeviceFarmKey().isEmpty())
				/*Modified by paneendra for VAPT FIX starts*/
				/*client.setDeviceFarmKey(new Crypto().encrypt(client.getDeviceFarmKey()));*/
			    client.setDeviceFarmKey(new CryptoUtilities().encrypt(client.getDeviceFarmKey()));
			/*Modified by paneendra for VAPT FIX ends*/
		} catch (Exception e) {
			logger.error(e.getMessage());
		} 
		/*added by Sahana for pCloudy: Ends*/
		this.clientHelper.persistRegClient(client);
		logger.info("Done");
	}
	
	public void update(RegisteredClient client) throws DatabaseException, RequestValidationException {
		logger.info("Request to update client");
		RegisteredClient mergedClient = this.getClient(client.getRecordId());
		if(mergedClient == null) {
			logger.error("You are trying to update a client with record id {} which does not exist in the system. Could have been deleted", client.getRecordId());
			throw new RecordNotFoundException("The record you are searching for is no longer available.");
		}
		
		String oClientName = Utilities.trim(mergedClient.getName());
		/*Added by Padmavathi for T251IT-160 starts*/
		String oHostName=Utilities.trim(mergedClient.getHostName());
		/*Added by Padmavathi for T251IT-160 ends*/
		mergedClient.merge(client);
		
		logger.info("Validating information");
		if(!oClientName.equalsIgnoreCase(Utilities.trim(mergedClient.getName()))) {
			this.validateClientDetails(mergedClient, true);
		}else {
			this.validateClientDetails(mergedClient, false);
		}
		/*Added by Padmavathi for T251IT-160 starts*/
		if(!oHostName.equalsIgnoreCase(Utilities.trim(mergedClient.getHostName()))) {
			if(this.clientHelper.hydrateClientByHostName(Utilities.trim(client.getHostName())) != null) {
				throw new DuplicateRecordException("client.already.exists.hostname", client.getHostName());
			}
		}
		/*Added by Padmavathi for T251IT-160 ends*/
		logger.info("Done");
		/*added by Sahana for pCloudy: starts*/
		try {
			if(client.getDeviceFarmPwd()!=null && !client.getDeviceFarmPwd().isEmpty())
				/*Modified by paneendra for VAPT FIX starts*/
				/*mergedClient.setDeviceFarmPwd(new Crypto().encrypt(client.getDeviceFarmPwd()));*/
			    mergedClient.setDeviceFarmPwd(new CryptoUtilities().encrypt(client.getDeviceFarmPwd()));
			   /*Modified by paneendra for VAPT FIX ends*/
			if(client.getDeviceFarmKey()!=null && !client.getDeviceFarmKey().isEmpty())
				/*Modified by paneendra for VAPT FIX starts*/
				/*mergedClient.setDeviceFarmKey(new Crypto().encrypt(client.getDeviceFarmKey()));*/
			    mergedClient.setDeviceFarmKey(new CryptoUtilities().encrypt(client.getDeviceFarmKey()));
			   /*Modified by paneendra for VAPT FIX ends*/
		} catch (Exception e) {
			logger.error(e.getMessage());
		} 
		/*added by Sahana for pCloudy: ends*/
		logger.info("Updating client");
		this.clientHelper.updateClient(mergedClient, client.getRecordId());
		
	}
	
	public void deleteClient(int clientRecordId) throws DatabaseException, ResourceConflictException {
		/*Added by Preeti for TENJINCG-480 starts*/
		RegisteredClient client = this.clientHelper.hydrateClient(clientRecordId);
		if(client == null) {
			throw new RecordNotFoundException("client.not.found");
		}
		/*Added by Preeti for TENJINCG-480 ends*/
		if(this.clientHelper.isClientBusy(clientRecordId)) {
			throw new ResourceConflictException("client.delete.error.in.use");
		}else {
			this.clientHelper.removeClient(clientRecordId);
		}
	}
	
	public void deleteClients(int[] clientRecordIds) throws DatabaseException, ResourceConflictException {
		boolean oneClient = false;
		if(clientRecordIds.length < 2) {
			oneClient = true;
		}
		for(int clientRecordId : clientRecordIds) {
			if(this.clientHelper.isClientBusy(clientRecordId)) {
				if(oneClient) {
					throw new ResourceConflictException("client.delete.error.in.use");
				}else {
					throw new ResourceConflictException("client.delete.error.multiple.in.use");
				}
			}
		}
		
		this.clientHelper.removeClient(clientRecordIds);
		
	}
	
	private void validateClientDetails(RegisteredClient client, boolean duplicateCheckRequired) throws RequestValidationException, DatabaseException, DuplicateRecordException {
		
		if(Utilities.trim(client.getName()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "Name");
		}
		/*Modified by Preeti for TENJINCG-410 starts*/
		if(Utilities.trim(client.getName()).length() > 15) {
			throw new RequestValidationException("field.length", new String[] {"Name","15"});
		}
		
		if(Utilities.trim(client.getHostName()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "Host Name");
		}
		
		if(Utilities.trim(client.getHostName()).length() > 15) {
			throw new RequestValidationException("field.length", new String[] {"Host Name","15"});
		}
		
		if(Utilities.trim(client.getPort()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "Port");
		}
		
		if(Utilities.trim(client.getPort()).length() > 5) {
			throw new RequestValidationException("field.length", new String[] {"Port","5"});
		}
		/*Modified by Preeti for TENJINCG-410 ends*/
		try {
			Integer.parseInt(Utilities.trim(client.getPort()));
		}catch(NumberFormatException e) {
			throw new RequestValidationException("field.value.invalid", new String[] {client.getPort(), "Port"});
		}
		/*Changed by Leelaprasad for TENJINCG-983 starts*/
		if(Utilities.trim(client.getOsType()).length() > 50) {
			throw new RequestValidationException("field.length", new String[] {"OS Type","50"});
		}
		if(Utilities.trim(client.getOsType()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "OS Type");
		}
		/*Changed by Leelaprasad for TENJINCG-983 ends*/
		if(duplicateCheckRequired) {
			if(this.clientHelper.hydrateClient(Utilities.trim(client.getName())) != null) {
				throw new DuplicateRecordException("client.already.exists", client.getName());
			} 
		}
		
	}
	
}

