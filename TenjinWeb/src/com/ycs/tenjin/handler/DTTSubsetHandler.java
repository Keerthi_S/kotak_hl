/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DTTSubsetHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*25-09-2019				Ashiki				Newly added by TENJINCG-1101
*05-02-2020				Roshni					TENJINCG-1168
*11-12-2020				Pushpalatha				TENJINCG-1221

			
*/package com.ycs.tenjin.handler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.db.DTTSubsetHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.DefectHelper;
import com.ycs.tenjin.defect.DTTSubset;
import com.ycs.tenjin.defect.DefectFactory;
import com.ycs.tenjin.defect.DefectManagementInstance;
import com.ycs.tenjin.defect.DefectManager;
import com.ycs.tenjin.defect.EDMException;
import com.ycs.tenjin.defect.UnreachableEDMException;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

public class DTTSubsetHandler {
	
	private static Logger logger = LoggerFactory
			.getLogger(DTTSubsetHandler.class);

	private DTTSubsetHelper subsetHelper = new DTTSubsetHelper();
	
	public List<DTTSubset> getAllDTTSubsets() throws DatabaseException {
		return this.subsetHelper.hydrateAllDTTSubsets();
	}
	
	public List<String> fetchAllInstances() throws DatabaseException{
		List<String> instances = new ArrayList<String>();
		instances =  this.subsetHelper.fetchAllInstances();
		return instances;
		
	}
	public DefectManagementInstance getDefectInstance(String instance) throws DatabaseException{
		DefectManagementInstance defectInstance= new DefectManagementInstance();
		defectInstance=new DefectHelper().hydrateDefects(instance);
		return defectInstance;
	}
	
	/* modified by Roshni for TENJINCG-1168 starts */
	/*public DTTSubset persistDefectManagementProjects(DTTSubset persistSubset) throws DatabaseException, RequestValidationException{*/
	public DTTSubset persistDefectManagementProjects(Connection conn,DTTSubset persistSubset) throws DatabaseException, RequestValidationException{
		this.validateSubset(conn,persistSubset);
		return this.subsetHelper.persistDefectManagementProjects(conn,persistSubset);
		/* modified by Roshni for TENJINCG-1168 ends */
	}
	/* modified by Roshni for TENJINCG-1168 starts */
	private void validateSubset(Connection conn,DTTSubset persistSubset) throws RequestValidationException, DatabaseException {
		/* modified by Roshni for TENJINCG-1168 ends */
		if(Utilities.trim(persistSubset.getSubSetName()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "Name");
		}
		if(Utilities.trim(persistSubset.getSubSetName()).length() > 15) {
			throw new RequestValidationException("field.length", new String[] {"Name","15"});
		}
		/* modified by Roshni for TENJINCG-1168 starts */
		if(this.subsetHelper.subsetExist(conn,persistSubset)) 
		/* modified by Roshni for TENJINCG-1168 ends */{
			throw new DuplicateRecordException("subsetName.already.exist",persistSubset.getSubSetName());
		}
	}

	public void deleteSubset(String[] recId) throws DatabaseException {
		this.subsetHelper.deleteSubset(recId);
	}

	public DTTSubset getDTTSubset(int recId) throws DatabaseException {
		return this.subsetHelper.hydrateDTTSubset(recId);
	}
	
	
	public Map<String, String> getProjects(String instanceName,DTTSubset subset) throws DatabaseException{
		
		JSONArray dttProjectsArray = new JSONArray();
		DefectManagementInstance defectManagement = new DefectManagementInstance();
			
			defectManagement = new DTTSubsetHandler().getDefectInstance(instanceName);
			
			String url = defectManagement.getURL();
			String username = defectManagement.getAdminId();
			String encPwd = defectManagement.getPassword();
			/*Added by Pushpalatha for TENJINCG-1221 starts*/
			String gitOrg=defectManagement.getOrganization();
			/*Added by Pushpalatha for TENJINCG-1221 ends*/
			String password;
			DefectManager dm = null;
			Map<String, String> map = new HashMap<String, String>();
			try {
				/*Modified by paneendra for VAPT FIX starts*/
				/*password = new Crypto().decrypt(encPwd.split("!#!")[1],decodeHex(encPwd.split("!#!")[0].toCharArray()));*/
				password = new CryptoUtilities().decrypt(encPwd);
				/*Modified by paneendra for VAPT FIX ends*/
				/*Modified by Pushpalatha for TENJINCG-1221 starts*/
				if(gitOrg==null){
					dm = DefectFactory.getDefectTools(defectManagement.getTool(), url, username, password);
				}else{
					dm = DefectFactory.getDefectTools(defectManagement.getTool(), url, username, password,gitOrg);
				}
				/*Modified by Pushpalatha for TENJINCG-1221 ends*/
				if(dm.isUrlValid()){
					dm.login(); 
					dttProjectsArray = dm.getAllProjects();
					dm.logout();
					
				}
				if(!(subset.getPrjId()<1)) {
					map=this.compareProjects(dttProjectsArray,subset);
					
				}
			} catch (EDMException e) {
				
				logger.error("Error ", e);
			} catch (UnreachableEDMException e) {
				
				logger.error("Error ", e);
			} catch (TenjinConfigurationException e) {
				
				logger.error("Error ", e);
			}
			return map;
			
		}

	private Map<String, String> compareProjects(JSONArray dttProjectsArray,DTTSubset subset) throws NumberFormatException, DatabaseException {
		 Map<String,String> map=new HashMap<String,String>();    
		String[] saveProjects=subset.getProjects().split(",");
		String projectNew[] = new String[dttProjectsArray.length()]; 
		for (int i=0;i<dttProjectsArray.length();i++) {
			try {
				String pName=dttProjectsArray.getJSONObject(i).getString("pname");
				String prjId=dttProjectsArray.getJSONObject(i).getString("pid");
				String project=pName+":"+prjId;
				projectNew[i]=project;
				
			} catch (JSONException e) {
				logger.error("Error ", e);
			}
		}
	        
		for(int i=0;i<projectNew.length;i++) {
			int flag=0;
			for (int j=0;j<saveProjects.length;j++) {
				if(saveProjects[j].equalsIgnoreCase(projectNew[i]) ) {
					flag++;
					map.put(saveProjects[j],"Y");
					break;
				}
			}
			if(flag==0) {
			map.put(projectNew[i],"N");
			}
		}
		return map;
	}


	public DTTSubset updateProjects(DTTSubset upadateSubset,int recId, String task ) throws DatabaseException, RequestValidationException {
		DTTSubset subset=this.subsetHelper.hydrateDTTSubset(recId);
		String projects=subset.getProjects();
		
			String[] savedProject = projects.split(",");
			String[] newProject = upadateSubset.getProjects().split(",");
			Set<String> linkedHashSet = new LinkedHashSet<>();
			String mergedProject="";
			if(task.equalsIgnoreCase("add")) {
			for(String newPrj:newProject) {
				if(linkedHashSet.add(newPrj)) {
					mergedProject=mergedProject+newPrj+",";
				}
			}
			for (String savedPrj:savedProject) {
				if(linkedHashSet.add(savedPrj)) {
					mergedProject=mergedProject+savedPrj+",";
				}
				
				}
			}else {
				
				for(String newPrj:newProject) {
					linkedHashSet.add(newPrj);
				}
				
				for(String str:savedProject) {
					if(linkedHashSet.add(str)) {
						mergedProject=mergedProject+str+",";
					}
				}
			}
			
			String projectString="";
			if(!mergedProject.equalsIgnoreCase("")) {
				projectString=mergedProject.substring(0,mergedProject.length()-1);
			}
			
	        upadateSubset.setProjects(projectString);
		return this.subsetHelper.updateDefectManagementProjects(upadateSubset);
	}

	public boolean checkSubsetMapped(String[] subsetName) throws DatabaseException {
		return this.subsetHelper.mappedSubset(subsetName);
		
	}
/* added by Roshni for TENJINCG-1168 ends */
	public DTTSubset persistDttSubSet(String instanceName, DTTSubset persistSubset) throws DatabaseException, RequestValidationException {
		
		try(Connection conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);){
			DefectManagementInstance defectManagement=this.getDefectInstance(instanceName);
			persistSubset.setInstance(defectManagement.getTool());
			persistSubset=this.persistDefectManagementProjects(conn,persistSubset);
		} catch (SQLException e) {
			
			logger.error("Error ", e);
		}
		return persistSubset;
	}
	/* added by Roshni for TENJINCG-1168 ends */
	
}
