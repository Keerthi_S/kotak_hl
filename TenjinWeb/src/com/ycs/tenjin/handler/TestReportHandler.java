/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestReportHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 *
 * DATE              	CHANGED BY              DESCRIPTION
 * 23-05-2019			Preeti					TENJINCG-1058,1059,1060,1061,1062
 * 31-05-2019			Pushpa					TENJINCG-1073,1064
 * 19-08-2019			Preeti					TENJINCG-1065,1072
 * 21-08-2019			Preeti					TENJINCG-1066,1071
 * 19-09-2019			Preeti					TENJINCG-1068,1069
 * 03-02-2021			Paneendra	            Tenj210-158 
 * 23-02-2021			Ashiki					TJN27-201
 * 15-03-2021			Ashiki					TENJINCG-1266
 * 30-03-2021			Ashiki					TJN27-216
 */

package com.ycs.tenjin.handler;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.TestReportHelper;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.project.TestStep;

public class TestReportHandler {
	private static final Logger logger = LoggerFactory.getLogger(TestReportHandler.class);
	TestReportHelper helper = new TestReportHelper();
	public void validateReportdata(HashMap<String,String> map) throws RequestValidationException{
		for (Map.Entry<String,String> entry : map.entrySet()) {
		   if(entry.getValue().equalsIgnoreCase("")){
		    	throw new RequestValidationException("field.mandatory",entry.getKey());
		    }
		}
	}
	public ArrayList<Integer> getRunsForTestSet(int tsId, int projectId) throws DatabaseException {
		ArrayList<Integer> runIds = this.helper.getRunsForTestSet(tsId, projectId);
		return runIds;
	}
	

	public ArrayList<HashMap<String,Object>> getTestSetRunHistoryData(int projectId, int tsRecId, int fromRunId, int toRunId,String fromDate, String toDate) throws DatabaseException {
		logger.info("hydrating test set run history");
		ArrayList<HashMap<String,Object>> data = helper.getTestSetRunHistoryData(projectId, tsRecId, fromRunId, toRunId, fromDate, toDate);
		return data;
	}
	public ArrayList<HashMap<String,Object>> getTestSetRunHistoryDataForRun(int projectId, int tsRecId, int fromRunId, int toRunId) throws DatabaseException {
		logger.info("hydrating test set run history");
		ArrayList<HashMap<String,Object>> data = helper.getTestSetRunHistoryDataForRun(projectId, tsRecId, fromRunId, toRunId);
		return data;
	}
	public ArrayList<HashMap<String,Object>> getTestSetRunHistoryDataForDate(int projectId, int tsRecId,String fromDate, String toDate) throws DatabaseException {
		logger.info("hydrating test set run history");
		ArrayList<HashMap<String,Object>> data = helper.getTestSetRunHistoryDataForDate(projectId, tsRecId, fromDate, toDate);
		return data;
	}
	public ArrayList<HashMap<String,Object>> getTestSetExecutionDetailData(int projectId, int tsRecId,String execDate) throws DatabaseException {
		logger.info("hydrating test set execution detail");
		ArrayList<HashMap<String,Object>> data = helper.getTestSetExecutionDetailData(projectId, tsRecId, execDate);
		return data;
	}
	public ArrayList<HashMap<String,Object>> getTestSetExecutionSummaryData(int projectId,String fromDate, String toDate, String userId) throws DatabaseException {
		logger.info("hydrating test set execution summary");
		ArrayList<HashMap<String,Object>> data = helper.getTestSetExecutionSummaryData(projectId, fromDate,toDate,userId);
		return data;
	}
	public ArrayList<HashMap<String,Object>> getTestSetExecutionSummaryData(int projectId,String fromDate, String toDate) throws DatabaseException {
		logger.info("hydrating test set execution summary");
		ArrayList<HashMap<String,Object>> data = helper.getTestSetExecutionSummaryData(projectId, fromDate,toDate);
		return data;
	}
	public ArrayList<HashMap<String,Object>> getTestCaseExecutionDetailData(int projectId,int tcRecId,String execDate, String userId) throws DatabaseException {
		logger.info("hydrating test case execution detail");
		ArrayList<HashMap<String,Object>> data = helper.getTestCaseExecutionDetailData(projectId, tcRecId, execDate, userId);
		return data;
	}
	public ArrayList<HashMap<String,Object>> getTestCaseExecutionDetailData(int projectId,int tcRecId,String execDate) throws DatabaseException {
		logger.info("hydrating test case execution detail");
		ArrayList<HashMap<String,Object>> data = helper.getTestCaseExecutionDetailData(projectId, tcRecId, execDate);
		return data;
	}
	/*Modified by Ashiki for TJN27-216 starts*/
	public ArrayList<HashMap<String,Object>> getTestCaseExecutionHistoryData(int projectId,String fromDate, String toDate, String userId, int tcRecId) throws DatabaseException {
		logger.info("hydrating test case execution history");
		ArrayList<HashMap<String,Object>> data = helper.getTestCaseExecutionHistoryData(projectId, fromDate,toDate,userId,tcRecId);
		return data;
	/*Modified by Ashiki for TJN27-216 ends*/
	}
	/*Modified by Ashiki for TJN27-216 starts*/
	public ArrayList<HashMap<String,Object>> getTestCaseExecutionHistoryData(int projectId,String fromDate, String toDate,int tcRecId) throws DatabaseException {
		logger.info("hydrating test case execution history");
		ArrayList<HashMap<String,Object>> data = helper.getTestCaseExecutionHistoryData(projectId, fromDate, toDate, tcRecId);
		/*Modified by Ashiki for TJN27-216 ends*/	
		return data;
	}
	/*Modified by Ashiki for TJN27-201 starts*/
	public ArrayList<HashMap<String,Object>> getApplicationWiseExecutionData(int appId,String fromDate, String toDate, int prjId) throws DatabaseException {
		logger.info("hydrating application wise execution");
		ArrayList<HashMap<String,Object>> data = helper.getApplicationWiseExecutionData(appId, fromDate,toDate,prjId);
		/*Modified by Ashiki for TJN27-201 ends*/
		return data;
	}
	/*Added by Pushpa for TENJINCG-1073 and TENJINCG-1064 starts*/
	public ArrayList<HashMap<String,Object>> getTesterWiseTestSetExecutionSummary(int projectId,String fromDate, String toDate, String userId) throws DatabaseException {
		logger.info("hydrating tester wise test set execution summary");
		ArrayList<HashMap<String,Object>> data = helper.getTesterWiseTestSetExecutionSummary(projectId, fromDate,toDate,userId);
		return data;
	}
	/*Added by Pushpa for TENJINCG-1074  And TENJINCG-1063 ends*/
	/*Added by Preeti for TENJINCG-1065,1072 starts*/
	public ArrayList<HashMap<String,Object>> getSeverityWiseDefectData(int projectId,int tsRecId, int runFrom, int runTo) throws DatabaseException {
		logger.info("hydrating severity wise defect");
		ArrayList<HashMap<String,Object>> data = helper.getSeverityWiseDefectsData(projectId, tsRecId, runFrom, runTo);
		return data;
	}
	/*Added by Preeti for TENJINCG-1065,1072 ends*/
	/*Added by Preeti for TENJINCG-1066,1071 starts*/
	public ArrayList<HashMap<String,Object>> getSchedulerData(int projectId, String taskType, String taskStatus, String fromDate, String toDate, String schType) throws DatabaseException {
		logger.info("hydrating scheduler data");
		ArrayList<HashMap<String,Object>> data;
		if(schType.equalsIgnoreCase("recurrence"))
			data = helper.getSchedulerDataForRecurrence(projectId, taskType, taskStatus, fromDate, toDate);
		else
			data = helper.getSchedulerData(projectId, taskType, taskStatus, fromDate, toDate);
		return data;
	}
	public ArrayList<HashMap<String,Object>> getSchedulerData(int projectId, String taskType, String taskStatus, String fromDate, String toDate, String userId, String schType) throws DatabaseException {
		logger.info("hydrating scheduler data");
		ArrayList<HashMap<String,Object>> data;
		if(schType.equalsIgnoreCase("recurrence"))
			data = helper.getSchedulerDataForRecurrence(projectId, taskType, taskStatus, fromDate, toDate, userId);
		else
			data = helper.getSchedulerData(projectId, taskType, taskStatus, fromDate, toDate, userId);
		return data;
	}
	/*Added by Preeti for TENJINCG-1066,1071 ends*/
	/*Added by Preeti for TENJINCG-1068,1069 starts*/
	public String compareObjects(Object oldObject, Object newObject) {
		BeanMap map = new BeanMap(oldObject);
		PropertyUtilsBean propUtils = new PropertyUtilsBean();
		JSONArray jArray = new JSONArray();
		try {
			for (Object propNameObject : map.keySet()) {
				String propertyName = (String) propNameObject;
				String propertyType = propUtils.getPropertyType(oldObject, propertyName).getName();
				if (!propertyType.equalsIgnoreCase("java.util.List")
							&& !propertyType.equalsIgnoreCase("java.lang.class")
							&& !propertyType.equalsIgnoreCase("java.util.map")) {
							if (propertyType.toLowerCase().startsWith("com.ycs.")) {
								// This is a Model
								String oldValue = getNameValueFromModel(propUtils.getProperty(oldObject, propertyName));
								String newValue = getNameValueFromModel(propUtils.getProperty(newObject, propertyName));
								
									//removed below condition because if old value is null in history it was not coming
									//if (!Objects.equals(oldValue, newValue) && !oldValue.equalsIgnoreCase("")) {
									if (!Objects.equals(oldValue, newValue)) {
										JSONObject json = new JSONObject();
										json.put("property", propertyName);
										json.put("oldValue", oldValue);
										json.put("newValue", newValue);
										jArray.put(json);
									}
								}else {
								// This could be a native Java Type.
								Object propValue1 = propUtils.getProperty(oldObject, propertyName);
								Object propValue2 = propUtils.getProperty(newObject, propertyName);
								if (propValue1 == null || propValue1.equals("-1")) {
									propValue1 = "";
								}

								if (propValue2 == null || propValue2.equals("-1")) {
									propValue2 = "";
								}
								if (!Objects.equals(propValue1, propValue2)) {
									
										// Property is changed.
										JSONObject json = new JSONObject();
										json.put("property", propertyName);
										
										json.put("oldValue",propValue1);
										json.put("newValue",propValue2);
										jArray.put(json);
									}
								}
							
					}
						else if (propertyType.equalsIgnoreCase("java.util.List")) {
						Object oldList = propUtils.getProperty(oldObject, propertyName);
						Object newList = propUtils.getProperty(newObject, propertyName);
						String newString = "";
						String oldString = "";
						if (oldList != null || newList != null) {

							if (!Objects.equals(oldList, newList)) {
								@SuppressWarnings("unchecked")
								List<Object> oldValueObj = (List<Object>) oldList;

								if (oldValueObj!=null && !oldValueObj.isEmpty()) { 

									for (Object object1 : oldValueObj) {
										if (!(oldString.equalsIgnoreCase(""))) {
											oldString = oldString + ",";
										} else {
											oldString.trim();
										}

										oldString = oldString + getNameValueFromModel(object1);

									}
								}
								@SuppressWarnings("unchecked")
								List<Object> newValueObj = (List<Object>) newList;

								if (newValueObj!=null && !newValueObj.isEmpty()) {
  
									for (Object object2 : newValueObj) {
										if (!(newString.equalsIgnoreCase(""))) {
											newString = newString + ",";
										} else {
											newString.trim();
										}
										newString = newString + getNameValueFromModel(object2);

									}
								}

							}
							if (oldString.equalsIgnoreCase(newString)) {

							} else {
									JSONObject json = new JSONObject();
									json.put("property", propertyName);
									json.put("oldValue", oldString);
									json.put("newValue", newString);
									jArray.put(json);
								
							}
						}

					}
			}
		} catch (Exception e) {
			logger.error("ERROR - Could not compare objects due to an unexpected error", e);
		}
		logger.info("Difference: [{}]", jArray.toString());
		return jArray.toString();
	}
	public static String getNameValueFromModel(Object modelObject) {

		if (modelObject == null) {
			return "";
		}

		PropertyUtilsBean propUtils = new PropertyUtilsBean();
		try {
			Object nameObj = propUtils.getProperty(modelObject, "name");
			if (nameObj != null) {
				return nameObj.toString();
			} else {
				logger.error("ERROR - 'name' property not found for type [{}]", modelObject.getClass().getName());
				return "";
			}
		} catch (Exception e) {
			logger.error("ERROR - Could not get value of 'name' property of type [{}]");
			return "";
		}

	}
	public ArrayList<HashMap<String,Object>> jsonToMap(String s) throws JSONException {
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
        JSONArray jsonArray = new JSONArray(s);
        for (int i = 0; i < jsonArray.length(); i++) {  
        	HashMap<String, Object> map = new LinkedHashMap<String, Object>();
            String property = jsonArray.getJSONObject(i).getString("property");
            String oldValue = jsonArray.getJSONObject(i).getString("oldValue");
            String newValue = jsonArray.getJSONObject(i).getString("newValue");
            map.put("property", property);
            map.put("oldValue", oldValue);
            map.put("newValue", newValue);
            data.add(map); 
        }
		return data;
    }
	public ArrayList<HashMap<String,Object>> getData(ArrayList<AuditRecord> records, String entityType) throws JSONException, JsonParseException, JsonMappingException, IOException {
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		for(AuditRecord record : records){
			String differData="";
			Gson g = new Gson();
			if(entityType.equalsIgnoreCase("teststep")){
				TestStep oldObject = g.fromJson(record.getOldData(), TestStep.class);
				TestStep newObject = g.fromJson(record.getNewData(), TestStep.class);
				differData = compareObjects(oldObject, newObject);
			}else if(entityType.equalsIgnoreCase("testcase")){
				TestCase oldObject = g.fromJson(record.getOldData(), TestCase.class);
				TestCase newObject = g.fromJson(record.getNewData(), TestCase.class);
				differData = compareObjects(oldObject, newObject);
			}else if(entityType.equalsIgnoreCase("testset")){
				TestSet oldObject = g.fromJson(record.getOldData(), TestSet.class);
				TestSet newObject = g.fromJson(record.getNewData(), TestSet.class);
				differData = compareObjects(oldObject, newObject);
			}else if(entityType.equalsIgnoreCase("project")){
				Project oldObject = g.fromJson(record.getOldData(), Project.class);
				Project newObject = g.fromJson(record.getNewData(), Project.class);
				differData = compareObjects(oldObject, newObject);
			}
			JSONArray jsonArray = new JSONArray(differData);
	        for (int i = 0; i < jsonArray.length(); i++) {  
	        	HashMap<String, Object> map = new LinkedHashMap<String, Object>();
	            String field = jsonArray.getJSONObject(i).getString("property");
	            String oldValue = jsonArray.getJSONObject(i).getString("oldValue");
	            String newValue = jsonArray.getJSONObject(i).getString("newValue");
	            map.put("Date", record.getLastUpdatedOn());
	            map.put("User", record.getLastUpdatedBy());
	            map.put("Field", field);
	            map.put("Old Value", oldValue);
	            map.put("New Value", newValue);
	            data.add(map); 
	        }
		}
		return data;
	}
	/*Added by Preeti for TENJINCG-1068,1069 ends*/
	
	
	/*Added by Paneendra for  Tenj210-158 starts*/
	public ArrayList<Timestamp> getDatesForTestSet(int tsId, int projectId) throws DatabaseException {
		logger.info("hydrating testset dates ");
		ArrayList<Timestamp> runDates = this.helper.getDatesForTestSet(tsId, projectId);
		return runDates;
	}
	
	public ArrayList<Timestamp> getDatesForTestCase(int tsId, int projectId)  throws DatabaseException{
		logger.info("hydrating testcase dates");
		ArrayList<Timestamp> runDates = this.helper.getDatesForTestCase(tsId, projectId);
		return runDates;
	}
	
	/*Added by Paneendra for  Tenj210-158 ends*/
	
	/*Added By Ashiki for TENJINCG-1266 starts*/
	public ArrayList<HashMap<String, Object>> getTestSetDetailData(int projectId,
			String fromDate, String toDate) throws DatabaseException {
		return helper.getTestSetDetailData(projectId,fromDate, toDate);
		
	}
	/*Added By Ashiki for TENJINCG-1266 ends*/
}


