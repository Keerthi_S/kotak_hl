/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DependencyRulesHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 09-05-2018           Padmavathi              TENJINCG-645 & TENJINCG-646 
* 14-05-2018           Padmavathi              TENJINCG-645 & TENJINCG-646 
* 16-05-2018           Padmavathi              TENJINCG-645 & TENJINCG-646 
* 07-06-2018           Padmavathi              Added methods for after deleting step, updating dependent step Id. 
* 24-10-2018			Preeti					TENJINCG-850
* 08-01-2019           Padmavathi              TJN252-67
* 11-03-2019           Padmavathi              TENJINCG-997
*/

package com.ycs.tenjin.handler;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Map;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DependencyRulesHelper;
import com.ycs.tenjin.project.DependencyRules;
import com.ycs.tenjin.project.TestStep;



public class DependencyRulesHandler {
	
	
	public void UpdateDependencyRules(int tcRecId,String type,String execStatus) throws DatabaseException{
		 DependencyRulesHelper hepler=new DependencyRulesHelper();
		ArrayList<TestStep> steps=new TestStepHandler().hydrateSteps(tcRecId);
		DependencyRules rules=	new DependencyRules();
		rules.setDependencyType(type);
		rules.setExecStatus(execStatus);
		rules.setTcRecId(tcRecId);
		/*Modified by Padmavathi for TJN27-33 starts*/
		if(steps.size()>1){
			hepler.deleteDependencyRulesForTestCase(tcRecId);
			hepler.persistDependencyRules(rules,steps);
		}
		/*Modified by Padmavathi for TJN27-33 ends*/
		
	}
	/*Modified by Padmavathi for TJN27-33 starts*/
	public void UpdateDependencyRules(int tcRecId,String type,String execStatus,Connection conn, Connection appConn) throws DatabaseException{
		 DependencyRulesHelper hepler=new DependencyRulesHelper();
		ArrayList<TestStep> steps=new TestStepHandler().hydrateSteps(tcRecId,conn,appConn);
		DependencyRules rules=	new DependencyRules();
		rules.setDependencyType(type);
		rules.setExecStatus(execStatus);
		rules.setTcRecId(tcRecId);
		if(steps.size()>1){
			hepler.deleteDependencyRulesForTestCase(tcRecId);
			hepler.persistDependencyRules(rules,steps);
		}
	}
	/*Modified by Padmavathi for TJN27-33 ends*/
	public void deleteDependencyRules(int tcRecId) throws DatabaseException{
		new DependencyRulesHelper().deleteDependencyRulesAtTestCaseLevel(tcRecId);
	}
	/*Modified by Padmavathi for TENJINCG-997 starts*/
	/*public void persistDependencyRules(int stepRecId,String type,String execStatus) throws DatabaseException{*/
	public void persistDependencyRules(TestStep step,String type,String execStatus) throws DatabaseException{
			TestStepHandler stepHandler=new TestStepHandler();
			
			int dependentStepRecId =stepHandler.getStepRecId(step.getSequence()-1,step.getTestCaseRecordId());
			
			DependencyRules rules=	new DependencyRules();
			rules.setDependencyType(type);
			rules.setExecStatus(execStatus);
			rules.setTcRecId(step.getTestCaseRecordId());
			rules.setStepRecId(step.getRecordId());
			/*Modified by Padmavathi for TENJINCG-997 ends*/
			rules.setDependentStepRecId(dependentStepRecId);
			
			new DependencyRulesHelper().persistDependencyRules(rules);
		
	}
	/*Added by Preeti for TENJINCG-850 starts*/
	public void persistDependencyRules(Connection conn,TestStep step,String type,String execStatus) throws DatabaseException{
		
		TestStepHandler stepHandler=new TestStepHandler();
		int dependentStepRecId =stepHandler.getStepRecId(conn,step.getSequence()-1,step.getTestCaseRecordId());
		
		DependencyRules rules=	new DependencyRules();
		rules.setDependencyType(type);
		rules.setExecStatus(execStatus);
		rules.setTcRecId(step.getTestCaseRecordId());
		rules.setStepRecId(step.getRecordId());
		rules.setDependentStepRecId(dependentStepRecId);
		
		new DependencyRulesHelper().persistDependencyRules(conn, rules);
	
	}
	/*Added by Preeti for TENJINCG-850 ends*/	
	
	/*Modified by Padmavathi for TJN252-67 Starts*/
	/*public void updateDependencyRulesForStep(int stepRecId,String execStatus) throws DatabaseException{*/
	public void updateDependencyRulesForStep(TestStep step,String execStatus) throws DatabaseException{
		TestStepHandler stepHandler=new TestStepHandler();
		
		//TestStep step=stepHandler.getTestStep(stepRecId);
		int dependentStepRecId =stepHandler.getStepRecId(step.getSequence()-1,step.getTestCaseRecordId());
		
		DependencyRules rules=	new DependencyRules();
		
		rules.setDependencyType("TestStep");
		rules.setExecStatus(execStatus);
		rules.setTcRecId(step.getTestCaseRecordId());
		rules.setStepRecId(step.getRecordId());
		rules.setDependentStepRecId(dependentStepRecId);
		
		this.deleteDependencyRulesforTestStep(step.getRecordId());
		new DependencyRulesHelper().persistDependencyRules(rules);
		
	}
	/*Modified by Padmavathi for TJN252-67 ends*/
	/*Modified by Padmavathi for TENJINCG-997 starts*/
	/*public void reOrderStepRule(int testCaseRecordId,Map<Integer,Integer> oldSequenceMap,Map<Integer,Integer> newSequenceMap) throws DatabaseException{*/
	public void reOrderStepRule(int testCaseRecordId,Map<Integer,Integer> oldSequenceMap,Map<Integer,Integer> newSequenceMap,int deleteSteprules, Map<Integer,Integer> dependencyIdsMap) throws DatabaseException{
		for(Integer stepRecordId : newSequenceMap.keySet()) {
			int sequenceValue = newSequenceMap.get(stepRecordId);
			int OldsequenceValue=oldSequenceMap.get(stepRecordId);
			if(sequenceValue==1||deleteSteprules!=0){
				this.deleteDependencyRulesforTestStep(stepRecordId);
				continue;
			} 
			if(OldsequenceValue==1){
				continue;
			}
				/*int dependentRecId=new TestStepHelper().hydrateTestStepRecId(sequenceValue-1, testCaseRecordId);*/
				this.updateDependentStepRecId(stepRecordId, dependencyIdsMap.get(stepRecordId));
		}
		/*Modified by Padmavathi for TENJINCG-997 ends*/
	}
	public DependencyRules getDependencyRules(int stepRecId) throws DatabaseException{
		return new DependencyRulesHelper().hydrateDependencyRules(stepRecId);
	}
	public int getStepRecIdBydependentId(int DependentStepId) throws DatabaseException{
		return new DependencyRulesHelper().hydrateStepRecIdByDependentStepId(DependentStepId);
	}
	public String checkDependencyRules(int tcRecId) throws DatabaseException{
		return new DependencyRulesHelper().checkDependencyRules(tcRecId);
	}
	public void deleteDependentTestStepRecIdRules(int stepRecId) throws DatabaseException{
		new DependencyRulesHelper().deleteDependentTestStepRecIdRules(stepRecId);
	}
	
	public void updateDependencyType(int tcRecId) throws DatabaseException{
		new DependencyRulesHelper().updateDependencyType(tcRecId);
	}
	public void deleteDependencyRulesforTestStep(int stepRecId,int tcRecId) throws DatabaseException{
		new DependencyRulesHelper().deleteDependencyRulesForTestStep(stepRecId);
		 
	 }
	 public void deleteDependencyRulesforTestStep(int stepRecId) throws DatabaseException{
		 new DependencyRulesHelper().deleteDependencyRulesForTestStep(stepRecId);
	 }
	 public void updateDependentStepRecId(int stepRecId,int dependentStepRecId) throws DatabaseException{
		 new DependencyRulesHelper().updateDependentStepRecId(stepRecId, dependentStepRecId);
	 }
}
