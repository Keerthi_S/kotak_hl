/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCaseHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                	CHANGED BY              DESCRIPTION
* 08-03-2018		  	Pushpalatha			  	Newly Added 
* 20-03-2018			Preeti					TENJINCG-611(Test case execution history)
* 09-05-2018            Padmavathi              TENJINCG-646
* 04-06-2018			Pushpalatha				TENJINCG-679
* 19-06-2018            Padmavathi              T251IT-65
* 27-06-2018			Preeti					T251IT-149
* 28-06-2018			Preeti					T251IT-165
* 10-09-2018			Sriram					TENJINCG-733
* 18-09-2018			Sriram					TENJINCG-735
* 28-09-2018			Leelaprasad			    TENJINCG-738
* 12-10-2018            Padmavathi              TENJINCG-847
* 24-10-2018            Padmavathi              TENJINCG-849
* 23-10-2018			Preeti					TENJINCG-850
* 25-10-2018            Padmavathi              TENJINCG-849
* 02-11-2018			Ashiki					TENJINCG-895
* 02-11-2018			Pushpa					TENJINCG-897
* 04-12-2018            Padmavathi              TJNUN262-3
* 22-01-2019			Ashiki					TJN252-45
* 13-02-2019			Preeti					TENJINCG-970
* 18-02-2019			Ashiki					TJN252-60
* 19-02-2019			Preeti					TENJINCG-969
* 06-03-2019			Pushpa					TENJINCG-978,979
* 11-03-2019            Padmavathi              TENJINCG-997
* 29-03-2019			Preeti					TENJINCG-1003
* 03-06-2019            Leelaprasad P           TENJINCG-1068,TENJINCG-1069
* 19-09-2019			Preeti					TENJINCG-1068,1069
* 22-05-2020			Ashiki					TENJINCG-1214,TENJINCG-1215
* 29-05-2020			Ashiki					Tenj210-12
* 05-06-2020			Ashiki					Tenj210-108
*/

package com.ycs.tenjin.handler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.DependencyRulesHelper;
import com.ycs.tenjin.db.FilterCriteria;
import com.ycs.tenjin.db.PaginatedRecords;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.db.TestCaseHelperNew;
import com.ycs.tenjin.db.TestCasePaginator;
import com.ycs.tenjin.db.TestStepHelper;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.testmanager.TestManagerInstance;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.ExcelException;
import com.ycs.tenjin.util.ExcelHandler;
import com.ycs.tenjin.util.Utilities;

public class TestCaseHandler {
	private static final Logger logger = LoggerFactory.getLogger(TestCaseHandler.class);
	private TestCaseHelperNew testCaseHelperNew=new TestCaseHelperNew();
	private TestStepHandler testStepHandler=new TestStepHandler();
	private ProjectHandler projectHandler=new ProjectHandler();
	private AuditHandler auditHandler = new AuditHandler();
	public int persistTestCase(TestCase test) throws DatabaseException, RequestValidationException {
		
		this.validateTestcase(test,true);
		this.validateTcType(test.getTcType());

		return this.testCaseHelperNew.persistTestCaseTuned(test);
		
	}

	private void validateTestcase(TestCase test, boolean duplicateCheckRequired) throws RequestValidationException, DatabaseException,DuplicateRecordException {
		logger.info("Validating TestCase information");
		
		/*Commented by Ashiki for TENJINCG-895 starts*/
		boolean tcNameExists=false;
		/*Commented by Ashiki for TENJINCG-895 ends*/
		
		if(Utilities.trim(test.getTcId()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "Test Case Id");
		}
		
		if(Utilities.trim(test.getTcId()).contains(" ")) {
			throw new RequestValidationException("field.space","Test Case Id");
		}
		
		if(Utilities.trim(test.getTcName()).length() < 1) {
			throw new RequestValidationException("field.mandatory", "Test Case Name");
		}
		
		/*Commented by Ashiki for TENJINCG-895 starts*/
		tcNameExists=this.testCaseHelperNew.doesTestCaseNameExist(test.getProjectId(), test.getTcName());
		if(tcNameExists){
			logger.error("ERROR - TestCase Name already exists ", test.getTcName());
			throw new DuplicateRecordException("testcase.name.already.exists",test.getTcName());
		}
		
		
		
		if(Utilities.trim(test.getTcType()).length()<1){
			throw new RequestValidationException("field.mandatory", "Test Case Type");
			
		}
		
		
		
		if(Utilities.trim(test.getTcPriority()).length()<1){
			throw new RequestValidationException("field.mandatory", "Test Case Priority");
		}
		
		if(!Utilities.trim(test.getTcPriority()).equals("Low")&&!Utilities.trim(test.getTcPriority()).equals("Medium")&&!Utilities.trim(test.getTcPriority()).equals("High")){
			throw new RequestValidationException("field.mandatory", "Test Case Priority value should be one from these:Low,Medium,High ");
		}
		/*Added by Padmavathi for T251IT-65 starts*/
		if(Utilities.trim(test.getTcDesc()).length()>1 && Utilities.trim(test.getTcDesc()).length()>3000){
			throw new RequestValidationException("field.length", new String[] {"Description","3000"});
		}
		/*Added by Padmavathi for T251IT-65 ends*/
		/*if(Utilities.trim(test.getStorage()).length()<1)*/
		
		if(duplicateCheckRequired) {
			boolean exists=this.testCaseHelperNew.testCaseAvailability(test);
			if(exists) {
				logger.error("ERROR - TestCase already exists ", test.getTcRecId());
				throw new DuplicateRecordException("testcase.already.exists",test.getTcId());
			}
		}
		
		
	}

	private void validateTcType(String testcaseType) throws RequestValidationException {
		Set<String> tcType=new HashSet<String>();
		
		tcType.add("Acceptance");
		tcType.add("Accessibility");
		tcType.add("Compatibility");
		tcType.add("Regression");
		tcType.add("Functional");
		tcType.add("Smoke");
		tcType.add("Usability");
		tcType.add("Other");
		if(!tcType.contains(testcaseType)){
			throw new RequestValidationException("field.value.invalid",new String[]{testcaseType,"TcType"});
		}
		
	}

	public List<TestCase> getAllTestCases(int projectId) throws DatabaseException {
		//Changed for TENJINCG-733 - Sriram
		List<TestCase> testcase=this.testCaseHelperNew.hydrateAllTestCasesForList(projectId);
		//Changed for TENJINCG-733 - Sriram ends
		return testcase;
	}
	//TENJINCG-735 Sriram
	public PaginatedRecords<TestCase> getAllTestCases(int projectId, int maxRecords, int pageNumber) throws DatabaseException {
		/*Modified by Ashiki for TENJINCG-1215 starts*/
		return this.searchTestCases(projectId, null, null, null, null, maxRecords, pageNumber,null);
		/*Modified by Ashiki for TENJINCG-1215 ends*/
	}
	
	public PaginatedRecords<TestCase> getAllTestCases(int projectId, int maxRecords, int pageNumber, String sortColumn, String sortDirection, String label) throws DatabaseException {
		/*Modified by Ashiki for TENJINCG-1215 starts*/
		return this.searchTestCases(projectId, null, null, null, null, maxRecords, pageNumber, sortColumn, sortDirection, label);
		/*Modified by Ashiki for TENJINCG-1215 ends*/
	}
	//TENJINCG-735 - Sriram ends

	public TestCase hydrateTestCase(int projectId, int tcRecId) throws DatabaseException,RecordNotFoundException {
		/*Changed by Padmavathi for TENJINCG-847(to check whether to hydrate  test case with steps or with out steps starts*/
		TestCase testcase=this.testCaseHelperNew.hydrateTestCaseBasicDetails(projectId, tcRecId,true);
		/*Changed by Padmavathi for TENJINCG-847(to check whether to hydrate  test case with steps or with out steps ends*/
		if(testcase==null){
			throw new RecordNotFoundException("testcase.not.found");
		}
		return testcase;
	}
	
	/*Added by Preeti for TENJINCG-611(Test case execution history) starts*/
	public List<TestRun> hydrateTestCaseExecutionHistory(int projectId, int tcRecId) throws DatabaseException {
		List<TestRun> runs=this.testCaseHelperNew.hydrateTestCaseExecutionHistory(projectId, tcRecId);
		if(runs==null){
			throw new RecordNotFoundException("testcasehistory.not.found");
		}
		return runs;
	}
	/*Added by Preeti for TENJINCG-611(Test case execution history) ends*/
	
	public String updateTestCase(TestCase testcase) throws DatabaseException,RequestValidationException {
		/*Added by Pushpalatha for TENJINCG-679 starts*/
		if(testcase.getTcName().equals("")){
			throw new RequestValidationException("field.empty","Testcase Name");
		}
		/*Added by Pushpalatha for TENJINCG-679 ends*/
		
		/*Added by Padmavathi for T251IT-65 starts*/
		if( Utilities.trim(testcase.getTcName()).length()>200){
				throw new RequestValidationException("field.length", new String[] {"Name","200"});
			}
		if(Utilities.trim(testcase.getTcDesc()).length()>1 && Utilities.trim(testcase.getTcDesc()).length()>3000){
			throw new RequestValidationException("field.length", new String[] {"Description","3000"});
		}
		/*Added by Padmavathi for T251IT-65 ends*/
		TestCase testcaseToUpdate=this.hydrateTestCase(testcase.getProjectId(), testcase.getTcRecId());
		/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		/*Modified by Preeti for TENJINCG-1068,1069 starts*/
		/*TestCase testCaseOld=testcaseToUpdate;*/
		TestCase testCaseOld = new TestCase();
		testCaseOld.merge(testcaseToUpdate);
		/*TestCase testCaseNew=testcase;*/
		TestCase testCaseNew = new TestCase();
		testCaseNew.merge(testcase);
		testCaseNew.setTcId(testcaseToUpdate.getTcId());
		testCaseNew.setTcCreatedBy(testcaseToUpdate.getTcCreatedBy());
		/*Modified by Preeti for TENJINCG-1068,1069 ends*/
		/*Added by Leelaprasad for TENJINCG-1068,1069 ends*/
		/*Commented by Ashiki for TENJINCG-895 starts*/
		if(!testcase.getTcName().equals(testcaseToUpdate.getTcName())){
			 boolean tcNameExists=this.testCaseHelperNew.doesTestCaseNameExist(testcase.getProjectId(), testcase.getTcName());
			if(tcNameExists){
				logger.error("ERROR -Can't Update Testcase, TestCase Name already exists ",testcase.getTcName());
				throw new DuplicateRecordException("testcase.name.already.exists",testcase.getTcName());
			}
		}
		/*Commented by Ashiki for TENJINCG-895 ends*/
		/*Added by Padmavathi for TENJINCG-997 starts*/
		String OPresetRules=testcaseToUpdate.getTcPresetRules();
		/*Added by Padmavathi for TENJINCG-997 ends*/
		if(!Utilities.trim(testcase.getTcId()).equalsIgnoreCase("")){
			throw new RequestValidationException("illegal.field.update", "Testcase ID");
		}
		
		if(!Utilities.trim(testcase.getMode()).equalsIgnoreCase("")){
			throw new RequestValidationException("illegal.field.update", "TestCase Mode");
		}
		
		if(!Utilities.trim(testcase.getTcCreatedBy()).equalsIgnoreCase("")){
			throw new RequestValidationException("illegal.field.update", "TestCase Created By");
		}
		
		this.validateTcType(testcase.getTcType());
		
		if(!Utilities.trim(testcase.getTcPriority()).equals("Low")&&!Utilities.trim(testcase.getTcPriority()).equals("Medium")&&!Utilities.trim(testcase.getTcPriority()).equals("High")){
			throw new RequestValidationException("Test Case Priority value should be one from these:Low,Medium,High ");
		}
		
		testcaseToUpdate.merge(testcase);
		String tc=this.testCaseHelperNew.updateTestCase(testcaseToUpdate);
		/*Added by Pushpalatha for TENJINCG-679 starts*/
		if(tc.equalsIgnoreCase("Executing")){
			throw new RequestValidationException("Can not update testcase while it is executing");
		}
		/*Added by Pushpalatha for TENJINCG-679 ends*/
		
		/*Modified by Padmavathi for TENJINCG-997 starts*/
		/*Added By Padmavathi for TENJINCG-646 starts*/
		
		if(!Utilities.trim(testcaseToUpdate.getTcPresetRules()).equalsIgnoreCase("-1")){
			new DependencyRulesHandler().UpdateDependencyRules(testcaseToUpdate.getTcRecId(), "TestCase", testcaseToUpdate.getTcPresetRules());
		}else if(OPresetRules!=null && Utilities.trim(testcaseToUpdate.getTcPresetRules()).equalsIgnoreCase("-1")) {
			new DependencyRulesHandler().deleteDependencyRules(testcaseToUpdate.getTcRecId());
		}
		/*Added By Padmavathi for TENJINCG-646 ends*/
		/*Modified by Padmavathi for TENJINCG-997 ends*/
		/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		    ObjectMapper mapper = new ObjectMapper();
			try {
				String newDataJson = mapper.writeValueAsString(testCaseNew);
				String oldDataJson = mapper.writeValueAsString(testCaseOld);
				testcase.getAuditRecord().setNewData(newDataJson);
				testcase.getAuditRecord().setOldData(oldDataJson);
			} catch (JsonProcessingException e) {
				logger.error("Error while persisting audit record");
				logger.error("Error ", e);
			}
			/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		/*Added by Preeti for TENJINCG-969 starts*/
		this.auditHandler.persistAuditRecord(testcase.getAuditRecord());
		/*Added by Preeti for TENJINCG-969 ends*/
		return tc;
	}
	
	

	public List<TestStep> hydrateStepsForTestCase(Connection conn, Connection conn2, int tcRecId) throws DatabaseException {
		TestStepHelper stepHelper=new TestStepHelper();
		/*Modified by Padmavathi for TENJINCG-997 starts*/
		return stepHelper.hydrateStepsForTestCase(conn, conn2, tcRecId,true);
		/*Modified by Padmavathi for TENJINCG-997 ends*/
	}
	/*Added by Pushpa for TENJINCG-897 starts*/
	/*Modified by Preeti for TENJINCG-1003 starts*/
	public void deleteTestCase(String selectedTestCases, String userId, int projectId) throws DatabaseException, SQLException, RequestValidationException{
	/*Modified by Preeti for TENJINCG-1003 ends*/
		int i=0;
		/*Modified by Padmavathi for TJNUN262-3 starts*/
		String [] Tcs=selectedTestCases.split(",");
		int[] testCases=new int[Tcs.length];
		/*Modified by Padmavathi for TJNUN262-3 ends*/
		for(String tcRecId:Tcs){
			testCases[i]=Integer.parseInt(tcRecId);
			i++;
		}
		/*Added by Preeti for TENJINCG-1003 starts*/
		String prjUserRole = "";
		for(int tcRecId:testCases) {
			prjUserRole = this.projectHandler.hydrateCurrentUserRoleForProject(userId, projectId);
			if(prjUserRole.equalsIgnoreCase("Test Engineer")){
				TestCase testcase = this.hydrateTestCaseBasicDetails(projectId, tcRecId);
				if(!testcase.getTcCreatedBy().equalsIgnoreCase(userId))
					throw new RequestValidationException("Could not delete Test Case(s) , user has no access to delete one or more selected Test Case(s).");
			}
		}
		/*Added by Preeti for TENJINCG-1003 ends*/
		/*Added by Ashiki for TJN252-45 starts*/
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);){
		//conn.setAutoCommit(false);
		boolean deleteStatus=new TestStepHelper().hydrateStepsForTestCaseDelete(testCases,conn);
	
		if(deleteStatus){
			for(int tcRecId:testCases) {
				new DependencyRulesHelper().deleteDependencyRulesForTestCase(tcRecId,conn);
				new TestStepHelper().deleteTestStep(tcRecId, conn);
			}
			
			this.testCaseHelperNew.deleteTestCase(testCases,conn);
			//conn.commit();
			
		}else{
			throw new RequestValidationException("Could not delete testcase(s) ,the selected Test Case contains Test Steps which are executed");
		}
		}catch (DatabaseException e) {
			
		}
		/*Added by Ashiki for TJN252-45 ends*/
	}
	

	public List<TestCase> searchTestcase(String tcId, String tcName, String tcType, String tcMode, String prjId) throws DatabaseException {
		List<FilterCriteria> criteria = new ArrayList<FilterCriteria>();
		if(tcId != null && !tcId.equalsIgnoreCase("")){
			if(tcId.contains("%")){
				
				criteria.add(new FilterCriteria("LOWER(TC_ID)", tcId.toLowerCase() , FilterCriteria.LIKE));
			}else{
				criteria.add(new FilterCriteria("LOWER(TC_ID)", tcId.toLowerCase() , FilterCriteria.MATCH));
			}
		}

		if(tcName != null && !tcName.equalsIgnoreCase("")){
			criteria.add(new FilterCriteria(new String[]{"LOWER(TC_NAME)","LOWER(TC_ID)","LOWER(TC_DESC)"}, tcName.toLowerCase() , FilterCriteria.PATTERN_MATCHES_ANY_FIELD_VALUE));
		}

		if(tcType != null && !tcType.equalsIgnoreCase("")){
			criteria.add(new FilterCriteria("TC_TYPE", tcType, FilterCriteria.MATCH));
		}
		
		if(tcMode != null && !tcMode.equals("")){
			criteria.add(new FilterCriteria("TC_EXEC_MODE",tcMode,FilterCriteria.MATCH));
		}
		if(prjId != null && !prjId.equalsIgnoreCase("")){
			criteria.add(new FilterCriteria("tc_prj_id", prjId, FilterCriteria.MATCH));
		}
		TestCasePaginator p = new TestCasePaginator(10, criteria);
		/*Modified by Preeti for T251IT-149 starts*/
		logger.info("Initializing Test Case Paginator");
		p.init();
		/*Modified By Preeti for T251IT-165 starts*/
		TestCasePaginator p1;
		if(p.getNumberOfPages()>1)
			p1 = new TestCasePaginator(p.getNumberOfPages()*10, criteria);
		else
			p1 = new TestCasePaginator(10, criteria);
		/*Modified By Preeti for T251IT-165 ends*/
		p1.setTcIdFilter(tcId);
		p1.setTcNameFilter(tcName);
		p1.setTcTypeFilter(tcType);
		p1.setTcModeFilter(tcMode);
		p1.setTcProjectId(prjId);
		logger.info("Initializing Test Case Paginator");
		p1.init();
		if(p1.getNumberOfPages() >= 1){
			logger.info("Fetching test cases from page {}", p1.getCurrentPage());
			p1.fetchNextPage();
		}
		List<TestCase> testcase=p1.getTestCases();
		/*Modified by Preeti for T251IT-149 ends*/
		return testcase;
	}

	public void deleteTestCase(int projectId, int tcRecId) throws RecordNotFoundException, DatabaseException, RequestValidationException {
		TestCase tc=this.hydrateTestCase(projectId, tcRecId);
		if(tc.getTcSteps().size()>0){
			 throw new RequestValidationException(" test case can not be deleted because it has steps");
		}
		this.testCaseHelperNew.deleteTestCase(tcRecId);
		
	}

	public Project hydrateProject(int projectId) throws DatabaseException {
		ProjectHelper phelper = new ProjectHelper();
		Project prj = phelper.hydrateProject(projectId);
		return prj;
	}

	public void hydrateFilters(Project prj) throws DatabaseException {
		this.testCaseHelperNew.hydrateFilters(prj);
		
	}

	public TestManagerInstance hydrateTMCredentials(String etmInstance, String userId, int projectId) throws DatabaseException {
		TestManagerInstance tmInstance=this.testCaseHelperNew.hydrateTMCredentials(etmInstance, userId, projectId);
		return tmInstance;
	}

	public boolean checkTestCase(String tcId, int id) throws DatabaseException {
		boolean tcCheck=this.testCaseHelperNew.checkTestCase(tcId, id);
		/*Modified by Ashiki for Tenj210-108 starts*/
		return tcCheck;
		/*Modified by Ashiki for Tenj210-108 ends*/
	}

	public boolean checkImportedTestStep(TestStep tjnStep, int tcRecId) throws DatabaseException {
		
		return this.testCaseHelperNew.checkImportedTestStep(tjnStep, tcRecId);
		
	}
	//TENJINCG-735 - Sriram
	public PaginatedRecords<TestCase> searchTestCases(int projectId, String tcId, String tcName, String tcType, String tcMode, int maxRecords, int pageNumber,String label) throws DatabaseException {
		return this.searchTestCases(projectId, tcId, tcName, tcType, tcMode, maxRecords, pageNumber, "tcRecId", "asc",label);
	}
	
	/*Modified by Ashiki for TENJINCG-1215 starts*/
	public PaginatedRecords<TestCase> searchTestCases(int projectId, String tcId, String tcName, String tcType, String tcMode, int maxRecords, int pageNumber, String sortField, String sortDirection, String label) throws DatabaseException {
	/*Modified by Ashiki for TENJINCG-1215 starts*/
		List<FilterCriteria> criteria = new ArrayList<FilterCriteria>();
		if(tcId != null && !tcId.equalsIgnoreCase("")){
			if(tcId.contains("%")){
				
				criteria.add(new FilterCriteria("LOWER(TC_ID)", tcId.toLowerCase() , FilterCriteria.LIKE));
			}else{
				criteria.add(new FilterCriteria("LOWER(TC_ID)", tcId.toLowerCase() , FilterCriteria.MATCH));
			}
		}

		if(tcName != null && !tcName.equalsIgnoreCase("")){
			criteria.add(new FilterCriteria(new String[]{"LOWER(TC_NAME)","LOWER(TC_ID)","LOWER(TC_DESC)"}, tcName.toLowerCase() , FilterCriteria.PATTERN_MATCHES_ANY_FIELD_VALUE));
		}

		if(tcType != null && !tcType.equalsIgnoreCase("")){
			criteria.add(new FilterCriteria("TC_TYPE", tcType, FilterCriteria.MATCH));
		}
		
		if(tcMode != null && !tcMode.equals("")){
			criteria.add(new FilterCriteria("TC_EXEC_MODE",tcMode,FilterCriteria.MATCH));
		}
		
		/*Added by Ashiki for TENJINCG-1215 starts*/
		if(label != null && !label.equalsIgnoreCase("")){
			criteria.add(new FilterCriteria("LOWER(TC_TAG)", label.toLowerCase() , FilterCriteria.MATCH));
		}
		/*Added by Ashiki for TENJINCG-1215 starts*/
		
		String prjId = Integer.toString(projectId);
		if(prjId != null && !prjId.equalsIgnoreCase("")){
			criteria.add(new FilterCriteria("tc_prj_id", prjId, FilterCriteria.MATCH));
		}
		
		PaginatedRecords<TestCase> testCases = this.testCaseHelperNew.hydrateAllTestCases(projectId, criteria, maxRecords, pageNumber, sortField, sortDirection);
		
		return testCases;
	}
	//TENJINCG-735 - Sriram Ends
	/*Changed by Leelaprasad for the Requirement TENJINCG-738 starts*/
	public JSONArray fetchTestCaseSuggestion(String query, int projectId) throws DatabaseException {
		
		return new TestCaseHelperNew().fetchTestCaseIds(query, projectId);
	}
	/*Changed by Leelaprasad for the Requirement TENJINCG-738 ends*/
	
	/*Added by Padmavathi for TENJINCG-847 starts*/
	public List<TestCase> getAllTestCases(int projectId,String mode) throws DatabaseException {
		
		List<TestCase> testcase=this.testCaseHelperNew.hydrateAllTestCases(projectId,mode);
		
		return testcase;
	}
	
public List<TestCase> getAllTestCases1(int projectId) throws DatabaseException {
		
		List<TestCase> testcase=this.testCaseHelperNew.hydrateAllTestCasesByProject(projectId);
		
		return testcase;
	}
	
	public TestCase hydrateTestCaseBasicDetails(int projectId, int tcRecId) throws DatabaseException{
		
		TestCase tc=this.testCaseHelperNew.hydrateTestCaseBasicDetails(projectId, tcRecId,false);
		
		return tc;
	}
	/*Added by Padmavathi for TENJINCG-847 ends*/
	
	/*Added by Padmavathi for TENJINCG-849 starts*/
	public void copyTestStepsforCase(int targetTcRecId,TestCase tc,int sourceTcRecId ) throws DatabaseException{
		ArrayList<TestStep> testSteps=null;
		Connection conn =null;
		try{
				conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				if(conn==null){
					throw new DatabaseException("could not connect to DataBase");
				}
				//conn.setAutoCommit(false);
				testSteps=new TestStepHandler().hydrateSteps(conn, sourceTcRecId);
				new TestStepHandler().copyTestStepsForCase(conn, tc,sourceTcRecId,testSteps, targetTcRecId);
				//conn.commit();
		 	
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				logger.error("Error ", e1);
			}
			logger.error("Error ", e);
			throw new DatabaseException("could not copy testcase");
		}finally{
			DatabaseHelper.close(conn);
		}
	}
	public void updatePresetRule(Connection conn,int tcRecId,String presetRule) throws DatabaseException{
		this.testCaseHelperNew.updatePresetRule(conn, tcRecId, presetRule);
	}
	/*Added by Padmavathi for TENJINCG-849 ends*/
	
	/*Added by Preeti for TENJINCG-850 starts*/
	public void AppValidationForImport(Connection conn, String[] selectedTestCase, int projectId) throws DatabaseException, RequestValidationException{
		HashMap<Integer, String> importTestsApp = this.testStepHandler.getAppIdForSteps(conn, selectedTestCase);
		List<Integer> currentprojectAppIds = this.projectHandler.hydrateProjectAutIds(conn, projectId);
		for(Integer appId : importTestsApp.keySet()){
			if(!currentprojectAppIds.contains(appId)){
				throw new RequestValidationException("Application "+importTestsApp.get(appId)+" is not mapped to current Project. Please map the application and try again.");
			}
		}
	}
	public List<TestCase> getAllTestCaseForImport(Connection conn, String[] selectedTestCaseIds) throws DatabaseException{
		List<TestCase> testCases = this.testCaseHelperNew.hydrateAllTestCasesForImport(conn, selectedTestCaseIds);
		return testCases;
	}
	/*Added by Preeti for TENJINCG-969 starts*/
	public String updateTestCase(Connection conn, TestCase testCase) throws DatabaseException{
		String checkExecution=this.testCaseHelperNew.updateTestCase(conn, testCase);
		
		TestCase testcaseToUpdate = this.testCaseHelperNew.hydrateTestCase(conn, testCase.getProjectId(), testCase.getTcId());
		TestCase testCaseOld=testcaseToUpdate;
		/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		TestCase testCaseNew=testCase;
		/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		if(!checkExecution.equalsIgnoreCase("Executing"))
		{
		/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
			 ObjectMapper mapper = new ObjectMapper();
				try {
					String newDataJson = mapper.writeValueAsString(testCaseNew);
					String oldDataJson = mapper.writeValueAsString(testCaseOld);
					testCase.getAuditRecord().setNewData(newDataJson);
					testCase.getAuditRecord().setOldData(oldDataJson);
				} catch (JsonProcessingException e) {
					logger.error("Error while persisting audit record");
					logger.error("Error ", e);
				}
				/*Added by Leelaprasad for TENJINCG-1068,1069 ends*/
				this.auditHandler.persistAuditRecord(testCase.getAuditRecord());
		}
		return checkExecution;
	}
	/*Added by Preeti for TENJINCG-969 ends*/
	/*Modified by Preeti for TENJINCG-970 starts*/
	public void importTestCasesForProject(Connection conn, String[] selectedTestCaseIds, String userId, int projectId, String folderPath) throws DatabaseException, RequestValidationException, ExcelException{
		List<TestCase> sourceTestCases = this.getAllTestCaseForImport(conn, selectedTestCaseIds);
		
		int tcRecId = 0;
		int sourceTcRecId = 0;
		Map<TestCase, String> testCaseMsg = new LinkedHashMap<TestCase, String>();
		Map<TestStep, String> testStepMsg = new LinkedHashMap<TestStep, String>();
		List<Integer> currentprojectAppIds = this.projectHandler.hydrateProjectAutIds(conn, projectId);
		ArrayList<TestStep> newSteps = new ArrayList<TestStep>();
		for(TestCase testCase : sourceTestCases){
			sourceTcRecId = testCase.getTcRecId();
			TestCase destTestcase = this.testCaseHelperNew.hydrateTestCase(conn, projectId, testCase.getTcId());
			if(destTestcase!=null){
					testCase.setTcRecId(destTestcase.getTcRecId());
					testCase.setTcPresetRules(destTestcase.getTcPresetRules());
					/*Added by Preeti for TENJINCG-969 starts*/
					AuditRecord audit = new AuditRecord();
					audit.setEntityRecordId(testCase.getTcRecId());
					audit.setLastUpdatedBy(userId);
					audit.setEntityType("testcase");
					testCase.setAuditRecord(audit);
					/*Added by Preeti for TENJINCG-969 ends*/
					String caseExecuting = this.updateTestCase(conn, testCase);
					if(caseExecuting.equalsIgnoreCase("Executing"))
						testCaseMsg.put(testCase, "Cannot update test case while it is executing.");
					else
						testCaseMsg.put(testCase, "Test case updated successfully.");
					ArrayList<TestStep> tsteps=new TestStepHandler().hydrateSteps(conn, sourceTcRecId);
					
					if(tsteps.size()>0){
						for(TestStep testStep : tsteps){
							TestStep destTeststep = this.testStepHandler.getTestStep(conn,destTestcase.getTcRecId(),testStep.getId());
							testStep.setTestCaseRecordId(destTestcase.getTcRecId());
							testStep.setTestCaseName(destTestcase.getTcName());
							if(destTeststep!=null){
								if(currentprojectAppIds.contains(testStep.getAppId())){
								testStep.setRecordId(destTeststep.getRecordId());
								testStep.setCustomRules(destTeststep.getCustomRules());
								/*Added by Preeti for TENJINCG-969 starts*/
								AuditRecord auditStep = new AuditRecord();
								auditStep.setEntityRecordId(testStep.getRecordId());
								auditStep.setLastUpdatedBy(userId);
								auditStep.setEntityType("teststep");
								testStep.setAuditRecord(auditStep);
								/*Added by Preeti for TENJINCG-969 ends*/
								String stepExecuting = this.testStepHandler.update(conn,testStep);
								if(stepExecuting.equalsIgnoreCase("Executing"))
									testStepMsg.put(testStep, "Cannot update test step while it is executing.");
								else
									testStepMsg.put(testStep, "Test step updated successfully.");
							}
							else
								testStepMsg.put(testStep, "Cannot update test step as application "+testStep.getAppName()+" is not mapped to current project.");
							}
							else{
								if(currentprojectAppIds.contains(testStep.getAppId())){
									newSteps.add(testStep);
									testStepMsg.put(testStep, "Test step created successfully.");
								}
								else
									testStepMsg.put(testStep, "Cannot create test step as application "+testStep.getAppName()+" is not mapped to current project.");
							}
						
						}
						if(newSteps.size()>0){
							this.testStepHandler.copyTestStepsForCase(conn, testCase, sourceTcRecId, newSteps, testCase.getTcRecId());
						}
					}
			}
			else{
				testCase.setProjectId(projectId);
				testCase.setTcCreatedBy(userId);
				tcRecId = this.testCaseHelperNew.persistTestCase(conn, testCase);
				testCaseMsg.put(testCase, "Test case created successfully.");
				ArrayList<TestStep> tsteps=new TestStepHandler().hydrateSteps(conn, sourceTcRecId);
				if(tsteps.size()>0){
					for(TestStep step : tsteps){
						step.setTestCaseRecordId(tcRecId);
						step.setTestCaseName(testCase.getTcName());
						if(currentprojectAppIds.contains(step.getAppId())){
							newSteps.add(step);
							testStepMsg.put(step, "Test step created successfully.");
						}
						else
							testStepMsg.put(step, "Cannot create test step as application "+step.getAppName()+" is not mapped to current project.");
					}
					if(newSteps.size()>0)
						this.testStepHandler.copyTestStepsForCase(conn, testCase, sourceTcRecId, newSteps, tcRecId);
				}
			}
		}
		ExcelHandler e=new ExcelHandler();
		e.processCaseImportFromOtherProjectSheet(testCaseMsg, testStepMsg, folderPath, "TestCases-output.xlsx");
	}
	/*Modified by Preeti for TENJINCG-970 ends*/
	/*Added by Preeti for TENJINCG-850 ends*/
	
	/*Added By Ashiki for TJN252-60 starts*/
	public Map<String, String> hydrateMappedAttribute(int projectId) throws DatabaseException {
		Map<String, String>  tmInstances =testCaseHelperNew.hydrateMappedAttribute(projectId);
		return tmInstances;
	}
	/*Added By Ashiki for TJN252-60 ends*/
/*Added by Pushpa for TENJINCG-978,979 starts*/
	public LinkedHashMap<Integer,String> hydrateMappedTestCases(int projectId, int testSetRecordId) throws DatabaseException {
		
		LinkedHashMap<Integer,String> tcSeq=new TestCaseHelperNew().hydrateMappedTestCases(projectId,testSetRecordId);
		return tcSeq;
	}
	/*Added by Pushpa for TENJINCG-978,979 ends*/
	
	/*Added by Ashiki for Tenj210-108 starts*/
	public void updateImportedTestCase(TestCase tjnTc, int prjId) throws DatabaseException {
		this.testCaseHelperNew.updateImportedTestcase(tjnTc,prjId);
		
	}
	/*Added by Ashiki for Tenj210-108 ends*/

/*Added by Ashiki for TENJINCG_1215 starts*/
	public JSONArray fetchTestCaseLabelSuggestion(String query, int projectId) throws DatabaseException {
		return new TestCaseHelperNew().fetchTestCaseLabels(query, projectId);
	}
	/*Added by Ashiki for TENJINCG_1215 end*/

/*Added by ashiki for TJN27-180 by starts*/
public void validateReportdata(HashMap<String,String> map) throws RequestValidationException{
	for (Map.Entry<String,String> entry : map.entrySet()) {
	   if(entry.getValue().equalsIgnoreCase("")){
	    	throw new RequestValidationException("field.mandatory",entry.getKey());
	    }
	}
}
/*Added by ashiki for TJN27-180 by end*/
}
