/**Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: TestStepHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

*//*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 * 05-03-2018           Padmavathi              Newly added for TENJINCG-612
 * 20-03-2018           Padmavathi              for TENJINCG-612
 * 26-03-2018			Preeti                  Modified for creation of API Test step
 * 09-05-2018           Padmavathi              TENJINCG-645
 * 14-05-2018           Padmavathi              TENJINCG-645
 * 18-05-2018           Padmavathi              TENJINCG-669
 * 01-06-2018           Padmavathi              Removed duplicate validations
 * 07-06-2018           Padmavathi              After deleting step, updating dependent step Id.
 * 19-06-2018			Preeti					T251IT-67
 * 10-07-2018           Leelaprasad             TENJINCG-623
 * 08-10-2018			Pushpalatha				TENJINCG-844
 * 12-10-2018           Padmavathi              TENJINCG-847
 * 24-10-2018           Padmavathi              TENJINCG-847
 * 07-12-2018			Pushpa					TENJINCG-910
 * 08-01-2019           Padmavathi              TJN252-67
 * 25-01-2019           Padmavathi              for TPT-17
 * 01-02-2019           Padmavathi              TJN252-77
 * 13-02-2019			Preeti					TENJINCG-970
 * 19-02-2019			Preeti					TENJINCG-969
 * 11-03-2019           Padmavathi              TENJINCG-997
 * 29-03-2019			Preeti					TENJINCG-1003
 * 03-04-2019           Padmavathi              TNJN27-52
 * 04-04-2019           Padmavathi              TNJN27-45
 * 10-04-2019			Prem					TNJN27-27
 * 11-04-2019           Padmavathi              TNJN27-10
 * 19-09-2019			Preeti					TENJINCG-1068,1069
 * 05-06-2020			Ashiki					Tenj210-108
*/


package com.ycs.tenjin.handler;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiLearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutsHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ModuleHelper;
import com.ycs.tenjin.db.TestStepHelper;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.project.DependencyRules;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.util.Utilities;

public class TestStepHandler {
	private static final Logger logger = LoggerFactory.getLogger(TestStepHandler.class);
	private TestStepHelper helper=new TestStepHelper();
	private ApiHelper apiHelper = new ApiHelper();
	private AuditHandler auditHandler = new AuditHandler();
	private ProjectHandler projectHandler=new ProjectHandler();
	public int persistTestStep(TestStep testStep,int projectId) throws RequestValidationException, DatabaseException {
		
		TestCase testCase=null;
		try{
			/*Modified by Padmavathi for TENJINCG-997 starts*/
			testCase= new TestCaseHandler().hydrateTestCaseBasicDetails(projectId, testStep.getTestCaseRecordId());
			/*Modified by Padmavathi for TENJINCG-997 ends*/
		}catch(RecordNotFoundException e){
			throw e;
		}
		
		/*Added by Ashiki for Tenj210-108 starts*/
		if(testCase.getStorage().equalsIgnoreCase("Remote")) {
			this.validateTestStepMandatoryFields(testStep,false);
		}else {
			this.validateTestStepMandatoryFields(testStep,true);
		}
		/*Added by Ashiki for Tenj210-108 ends*/
		
		if(Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("GUI")&&testStep.getValidationType().equalsIgnoreCase("UI")){
			if(testStep.getRowsToExecute()> 0&&testStep.getRowsToExecute()!=1) {
				logger.error("RowsToExecute cannot be inserted for validation type 'UI'.");
						throw new RequestValidationException("rowstoexecute.not.insert.for.val.type.ui");
					}
				}
		logger.info("TestStep mandatory validations completed");
		
		Aut aut=this.validateAut(testStep, projectId);
		
		if(Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("GUI")){
			this.validateGuiTestStep(aut,testStep);
		}else if(Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("API")){
			this.validateApiTestStep(testStep);
		}
		/*Added by Ashiki for TENJINCG-1275 starts*/
		if(!Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("MSG")){
			this.validateAutLoginType(aut, testStep);
			}
		/*Added by Ashiki for TENJINCG-1275 ends*/
		logger.info("All validations are completed successfully");
		/*Modified by Padmavathi for TENJINCG-997 starts*/
		testStep=this.helper.persistTestStep(testStep);
		logger.info("TestStep  created successfully"+testStep.getId());
		/*Added by Padmavathi for TENJINCG-645 starts*/
		 /*Changed by Padmavathi for TENJINCG-847 starts*/
		if(testStep.getSequence()!=1 && testCase.getTcPresetRules()!=null&& !testCase.getTcPresetRules().equalsIgnoreCase("-1"))
			/*Modified by Padmavathi for TENJINCG-997 ends*/
		new DependencyRulesHandler().persistDependencyRules(testStep, "TestCase", testCase.getTcPresetRules());
		/*Changed by Padmavathi for TENJINCG-847 ends*/
		/*Added by Padmavathi for TENJINCG-645 ends*/
		return testStep.getRecordId();
	}
	
	

	/*Added by Preeti for TENJINCG-970 starts*/
	public TestStep persistTestStep(Connection conn, TestStep testStep) throws RequestValidationException, DatabaseException {
		TestStep step = this.helper.persistTestStep(conn,testStep);
		return step;
	}
	
	public String update(Connection conn,TestStep testStep) throws DatabaseException, RequestValidationException{
		String checkExecution=helper.updateTestStep(conn,testStep);
		/*Added by Preeti for TENJINCG-969 starts*/
		/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		TestStep testStepToUpdate=this.getTestStep(conn, testStep.getTestCaseRecordId(), testStep.getId());
		TestStep testStepOld=testStepToUpdate;
		TestStep testStepNew=testStep;
		/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		if(!checkExecution.equalsIgnoreCase("Executing"))
		{
		/*Added by Leelaprasad for TENJINCG-1068,1069 ends*/
			 ObjectMapper mapper = new ObjectMapper();
				try {
					String newDataJson = mapper.writeValueAsString(testStepNew);
					String oldDataJson = mapper.writeValueAsString(testStepOld);
					testStep.getAuditRecord().setNewData(newDataJson);
					testStep.getAuditRecord().setOldData(oldDataJson);
				} catch (JsonProcessingException e) {
					logger.error("Error while persisting audit record");
					logger.error("Error ", e);
				}
				/*Added by Leelaprasad for TENJINCG-1068,1069 ends*/
			this.auditHandler.persistAuditRecord(testStep.getAuditRecord());
		}
		/*Added by Preeti for TENJINCG-969 ends*/
		return checkExecution;
	}
		
	public TestStep getTestStep(Connection conn, int tcRecId, String stepId) throws DatabaseException{
		return helper.hydrateTestStepWithDependencyRules(conn, tcRecId, stepId);
	}
	/*Added by Preeti for TENJINCG-970 ends*/
	public void update(TestStep testStep,int projectId) throws DatabaseException, RequestValidationException{
		TestStep testStepToUpdate=null;
		TestCase tc=null;
		/*Changed by leelaprasad for the defect TENJINCG-623 starts*/
		/*Changed by leelaprasad for the defect TENJINCG-623 ends*/
		try{
			/*Changed by Padmavathi to hydrate test case details with out steps starts */
		    tc= new TestCaseHandler().hydrateTestCaseBasicDetails(projectId, testStep.getTestCaseRecordId());
		    /*Changed by Padmavathi to hydrate test case details with out steps ends*/
			testStepToUpdate=this.getTestStep(testStep.getRecordId());
			/*Changed by leelaprasad for the defect TENJINCG-623 starts*/
			/*Changed by leelaprasad for the defect TENJINCG-623 ends*/
			testStep.setTestCaseName(testStepToUpdate.getTestCaseName());
			/*Added by Preeti for TENJINCG-1068,1069 starts*/
			/*Added by Preeti for TENJINCG-1068,1069 ends*/
		}catch(RecordNotFoundException e){
			throw e;
		}
		/*Added by Ashiki for TENJINCG-1275 starts*/
		if(!Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("MSG")) {
		if(!Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("")){
			throw new RequestValidationException("illegal.field.update", "Mode");
		}
		}
		/*Added by Ashiki for TENJINCG-1275 ends*/
		if(Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("GUI")&&!Utilities.trim(testStep.getValidationType()).equalsIgnoreCase("")&&testStep.getValidationType().equalsIgnoreCase("UI")){
			if(testStep.getRowsToExecute()> 0&&testStep.getRowsToExecute()!=1) {
				logger.error("RowsToExecute cannot be updated for validation type 'UI'.");
				throw new RequestValidationException("rowstoexecute.not.update.for.val.type.ui");
			}
		}
		/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		/*Modified by Preeti for TENJINCG-1068,1069 starts*/
		TestStep testStepOld=new TestStep();
		 testStepOld.merge(testStepToUpdate);
		 
		/*TestStep testStepNew=testStep;*/
		 TestStep testStepNew=new TestStep();
		 testStepNew.merge(testStep);
		 if(testStepNew.getRowsToExecute()==0)
			 testStepNew.setRowsToExecute(1);
		 testStepNew.setAppName(helper.getappname(testStepNew.getAppId()));
		/*Modified by Preeti for TENJINCG-1068,1069 ends*/
		/*Added by Leelaprasad for TENJINCG-1068,1069 ends*/
		/*Added by Padmavathi for TENJINCG-997 starts*/
		String oDependencyRule=testStepToUpdate.getCustomRules();
		/*Added by Padmavathi for TENJINCG-997 ends*/
		String oName = Utilities.trim(testStepToUpdate.getId());
		testStepToUpdate.merge(testStep);
		/*Changed by leelaprasad for the defect TENJINCG-623 starts*/
		/*Changed by leelaprasad for the defect TENJINCG-623 ends*/
		if(oName.equalsIgnoreCase(Utilities.trim(testStepToUpdate.getId()))) {
			this.validateTestStepMandatoryFields(testStepToUpdate, false);
		}else {
			this.validateTestStepMandatoryFields(testStepToUpdate, true);
		}
		logger.info("TestStep mandatory validations completed");
		Aut aut=this.validateAut(testStepToUpdate, projectId);
		
		if(Utilities.trim(testStepToUpdate.getTxnMode()).equalsIgnoreCase("GUI")){
			this.validateGuiTestStep(aut,testStepToUpdate);
			this.validateAutLoginType(aut, testStepToUpdate);
		}else{
			this.validateApiTestStep(testStepToUpdate);
			this.validateAutLoginType(aut, testStepToUpdate);
		}
		
		
		logger.info("All validations are completed successfully");
		String checkExecution=helper.updateTestStep(testStepToUpdate);
		
		if(checkExecution.equalsIgnoreCase("Executing")){
			logger.error("test step is executing"+testStepToUpdate.getId());
		throw new RequestValidationException("test.step.not.update.execute");
		}
		/*Modified by Padmavathi for TENJINCG-997 starts*/
		/*Added by Padmavathi for TENJINCG-645 starts*/
		if(testStep.getCustomRules()!=null&&!testStepToUpdate.getCustomRules().equalsIgnoreCase("-1")){
			/*Modified by Padmavathi for TJN252-67 Starts*/
			new DependencyRulesHandler().updateDependencyRulesForStep(testStepToUpdate,testStepToUpdate.getCustomRules());
			/*Modified by Padmavathi for TJN252-67 ends*/
			if(!testStepToUpdate.getCustomRules().equalsIgnoreCase(tc.getTcPresetRules())){
				new DependencyRulesHandler().updateDependencyType(testStepToUpdate.getTestCaseRecordId());
			}
			
		}else if(testStepToUpdate.getSequence()!=1&& oDependencyRule!=null && testStepToUpdate.getCustomRules().equalsIgnoreCase("-1")){
			new DependencyRulesHandler().deleteDependencyRulesforTestStep(testStep.getRecordId(),testStepToUpdate.getTestCaseRecordId());
			new DependencyRulesHandler().updateDependencyType(testStepToUpdate.getTestCaseRecordId());
		}
		/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
		 ObjectMapper mapper = new ObjectMapper();
			try {
				String newDataJson = mapper.writeValueAsString(testStepNew);
				String oldDataJson = mapper.writeValueAsString(testStepOld);
				testStep.getAuditRecord().setNewData(newDataJson);
				testStep.getAuditRecord().setOldData(oldDataJson);
			} catch (JsonProcessingException e) {
				logger.error("Error while persisting audit record");
				logger.error("Error ", e);
			}
			/*Added by Leelaprasad for TENJINCG-1068,1069 ends*/
		/*Added by Padmavathi for TENJINCG-645 ends*/
		/*Modified by Padmavathi for TENJINCG-997 ends*/
			/*Added by Preeti for TENJINCG-969 starts*/
			this.auditHandler.persistAuditRecord(testStep.getAuditRecord());
			/*Added by Preeti for TENJINCG-969 ends*/
		
	}
	
	public TestStep getTestStep(int tStepRecId) throws DatabaseException{
		TestStep testStep= helper.hydrateTestStep(tStepRecId);
			if(testStep==null){
				logger.error("test step not found"+tStepRecId);
				throw new RecordNotFoundException("teststep.not.found");
			}
			return testStep;
	}
	
	public TestStep getTestStep(int tcRecId,int tStepRecId,int projectId) throws DatabaseException{
		TestStep testStep=null;
		try{
			/*Changed by Padmavathi to hydrate test case details with out steps starts */
			new TestCaseHandler().hydrateTestCaseBasicDetails(projectId, tcRecId);
			/*Changed by Padmavathi to hydrate test case details with out steps ends */
			testStep=helper.hydrateTestStep(tcRecId, tStepRecId);
			if(testStep==null){
				logger.error("test step not found"+tStepRecId);
				throw new RecordNotFoundException("teststep.not.found");
			}
		}catch(RecordNotFoundException e){
			logger.error(e.getMessage());
			throw e;
		}
		return testStep;
		
	}
	
	public void reOrderTestCases(int testCaseRecordId,Map<Integer, Integer> sequenceMap) throws DatabaseException{
		helper.reOrderTestCases(testCaseRecordId, sequenceMap);
	}
	
	public ArrayList<ApiLearnerResultBean> getLearningHistoryforApi(int appId, String apiCode) throws DatabaseException{
		ArrayList<ApiLearnerResultBean> lrns=new ApiHelper().hydrateLearningHistory(apiCode,appId);
		return lrns;
	}
	/*Modified by Preeti for TENJINCG-1003 starts*/
	public void deleteTestStep(String[] tStepRecIds,int tcRecId,int prjId, String userId) throws DatabaseException, RequestValidationException {
	/*Modified by Preeti for TENJINCG-1003 ends*/	
		try{
			/*Changed by Padmavathi to hydrate test case details with out steps starts */
			new TestCaseHandler().hydrateTestCaseBasicDetails(prjId, tcRecId);
			/*Changed by Padmavathi to hydrate test case details with out steps ends */
			/*Modified by Padmavathi for TJN252-77 Starts*/
			boolean deletion_status=true;
			boolean isRulesPresent=false;
			DependencyRulesHandler depHandler=new DependencyRulesHandler();
			String dependencyRule=new DependencyRulesHandler().checkDependencyRules(tcRecId);
			String prjUserRole = "";
			for(String tStepRecId:tStepRecIds){
				int stepRecId=Integer.parseInt(tStepRecId);
				TestStep step=this.getTestStep(stepRecId);
				/*Added by Preeti for TENJINCG-1003 starts*/
				prjUserRole = this.projectHandler.hydrateCurrentUserRoleForProject(userId, prjId);
				if(prjUserRole.equalsIgnoreCase("Test Engineer")){
					if(!step.getCreatedBy().equalsIgnoreCase(userId))
						throw new RequestValidationException("Could not delete Test Step(s) , user has no access to delete one or more selected Test Step(s).");
				}
				/*Added by Preeti for TENJINCG-1003 ends*/
				deletion_status=this.helper.deleteTestStep(stepRecId,tcRecId);
				if(!deletion_status){
					logger.info("test step is executing..."+stepRecId);
					throw new RequestValidationException("Test.step.not.delete");
				}
				/*Added by Padmavathi for TENJINCG-645 starts*/
				if(dependencyRule!=null ){
					isRulesPresent=true;
					if(dependencyRule.equalsIgnoreCase("TestStep")||step.getSequence()==1){
						depHandler.deleteDependentTestStepRecIdRules(stepRecId);
					}else {
						//hydrating dependency rules to get dependent step recId
						DependencyRules rules=depHandler.getDependencyRules(stepRecId);
						int dependentStepId=stepRecId;
						//hydrating stepRecordId,which is dependent on passed dependentId
						int stepRecordId=depHandler.getStepRecIdBydependentId(dependentStepId);
						depHandler.deleteDependencyRulesforTestStep(stepRecId, tcRecId);
						//After deleting step updating dependent recId
						depHandler.updateDependentStepRecId(stepRecordId,rules.getDependentStepRecId());
					}
				}
			}
			//After deleting dependecy rules for steps if preset rules are not there,updating preset rules to null 
				if(isRulesPresent){
					String dependencyRules=new DependencyRulesHandler().checkDependencyRules(tcRecId);
					if(dependencyRules==null ||dependencyRules.equalsIgnoreCase("TestStep")){
						depHandler.updateDependencyType(tcRecId);
					}
				}
			/*Added by Padmavathi for TENJINCG-645 ends*/
				
				logger.info("Successfully deleted TestSteps.");
				
			/*	Modified by Padmavathi for TENJINCG-669 ends*/
		}catch(RecordNotFoundException e){
			logger.error(e.getMessage());
			throw e;
		}
		
	}
	public Aut validateAut(TestStep step,int projectId) throws DatabaseException, RequestValidationException{
        Aut aut = null;
		logger.info("validating aut"+step.getAppId());
		/*Added by Padmavathi for TNJN27-10 starts*/
		boolean checkWithAutId=true;
		/*Added by Padmavathi for TNJN27-10 ends*/
		if(step.getAppName()!=null) {
			logger.info("validating aut"+step.getAppName());
			aut = new AutsHelper().hydrateProjectAut(projectId, step.getAppName());
			if(aut == null) {
				throw new RequestValidationException("aut.not.exist.or.not.mapped.project");
				
			}
			if(step.getAppId() > 0&& step.getAppId()!=aut.getId()) {
				throw new RequestValidationException("autname.autid.not.match");
			}
			step.setAppId(aut.getId());
			/*Added by Padmavathi for TNJN27-10 starts*/
			checkWithAutId=false;
			/*Added by Padmavathi for TNJN27-10 ends*/
		}
		/*Modified by Padmavathi for TNJN27-10 starts*/
		if(step.getAppId() > 0 && checkWithAutId) {
		/*Modified by Padmavathi for TNJN27-10 ends*/
			logger.info("validating aut"+step.getAppId());
			aut = new AutsHelper().hydrateProjectAut(projectId, step.getAppId());
			if(aut == null) {
				throw new RequestValidationException("aut.not.exist.or.not.mapped.project");
			}
		}
		return aut;
	}
	 
	public void validateAutLoginType(Aut aut,TestStep step ) throws DatabaseException, RequestValidationException{
			String [] userType=aut.getLoginUserType().split(",");
		   int userFlag=0;
			for(String user:userType){
				if(!Utilities.trim(user).equalsIgnoreCase(Utilities.trim(step.getAutLoginType()))){
					userFlag++;
		    		if(userFlag==userType.length){
		    			logger.error("could not validate autLoginType"+step.getAutLoginType());
		    			throw new RequestValidationException("Could.not.validate.autLoginType",step.getAutLoginType()); 
		    		}
		    		else{continue;}
				}
				break;
			}
		}
	
	  public void validateApiTestStep(TestStep step) throws DatabaseException, RequestValidationException{
		  	Api api  =new ApiHelper().hydrateApi(step.getAppId(), step.getModuleCode());
			if(api==null){ 
				logger.error("could not validate apicode"+step.getModuleCode());
				throw new RequestValidationException("Could.not.validate.Apicode",step.getModuleCode() ); 
			}
			else{
				ApiOperation apiOperation=	new ApiHelper().hydrateApiOperation(step.getAppId(),step.getModuleCode(),step.getOperation());
				if(apiOperation==null){
					logger.error("could not validate Opeartion"+step.getOperation());
					throw new RequestValidationException("Could.not.validate.Opeartion",  step.getOperation());
				}
				String responses=apiHelper.fetchAPIResponseTypes(step.getAppId(),step.getModuleCode(), step.getOperation());
				/*Modified by Preeti for creation of API Test step starts*/
				if(responses!=null && step.getResponseType()!=0){
				/*Modified by Preeti for creation of API Test step ends*/
				String []	responseTypes=responses.split(",");
				int opFlag=0;
					for(String responseType:responseTypes){
						if(Integer.parseInt(responseType)!=0){
							if(Integer.parseInt(responseType)!=step.getResponseType()){
								opFlag++;
								if(opFlag==responseTypes.length){
									logger.error("could not validate responsetype"+responseType);
									throw new RequestValidationException("Could.not.validate.responseType",  responseType); 
								}
								else{
									continue;
								}
							}
							break;
						}
					}
				}
			}
	   }

	  public void validateGuiTestStep(Aut aut,TestStep step) throws DatabaseException, RequestValidationException{
			Module module=new ModuleHelper().hydrateModule(aut.getId(), step.getModuleCode());
			if(module==null){
				logger.error("could not validate function"+step.getModuleCode());
				throw new RequestValidationException("Could.not.validate.function", step.getModuleCode()); 
			}
			String []operations=aut.getOperation().split(",");
			int opFlag=0;
			for(String operation:operations){
				if(!operation.equalsIgnoreCase(Utilities.trim(step.getOperation()))){
					opFlag++;
					if(opFlag==operations.length){
						logger.error("could not validate Opeartion"+step.getOperation());
						throw new RequestValidationException("Could.not.validate.Opeartion",  step.getOperation()); 
					}
					else{
						continue;
					}
				}
				break;
			}
	   }
	
	  
	private void validateTestStepMandatoryFields(TestStep testStep,boolean duplicateCheckRequired) throws RequestValidationException, DatabaseException {
		
		if(Utilities.trim(testStep.getId()).length() < 1) {
			logger.error("ERROR - Step ID  is blank");
			throw new RequestValidationException("field.mandatory", "Step ID");
		}
		if(Utilities.trim(testStep.getId()).length() > 30) {
			throw new RequestValidationException("field.length", new String[] {"Step ID","30"});
		}
		
		if(Utilities.trim(testStep.getShortDescription()).length() < 1) {
			logger.error("ERROR - Short Description is blank");
			/*<!-- Modified by Prem for TNJN27-27 Start -->*/
			throw new RequestValidationException("field.mandatory", "Summary");
					/*<!-- Modified by Prem for TNJN27-27 Ends -->*/
		}
		if(Utilities.trim(testStep.getShortDescription()).length() > 100) {
			/*<!-- Modified by Prem for TNJN27-27 Starts -->*/
			throw new RequestValidationException("field.length", new String[] {"Summary","100"});
			/*<!-- Modified by Prem for TNJN27-27 Ends -->*/
		}
		/*Added by Preeti for T251IT-67 starts*/
		if(Utilities.trim(testStep.getDescription()).length() > 4000) {
			throw new RequestValidationException("field.length", new String[] {"Description","4000"});
		}
		
		if(Utilities.trim(testStep.getExpectedResult()).length() > 1000) {
			throw new RequestValidationException("field.length", new String[] {"Expected result","1000"});
		}
		/*Added by Preeti for T251IT-67 ends*/
		if(testStep.getAppId()<=0) {
			if(Utilities.trim(testStep.getAppName()).length() < 1){
				logger.error("ERROR - Application is blank");
				throw new RequestValidationException("field.mandatory", "Application");
			}
		}
		/*Modified by Padmavathi for TNJN27-52 starts*/
		if(Utilities.trim(testStep.getModuleCode()).equals("-1") || Utilities.trim(testStep.getModuleCode()).length() < 1 ) {
			/*Modified by Padmavathi for TNJN27-52 ends*/
			if(Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("GUI")){
				logger.error("ERROR - Function Code is blank");
			throw new RequestValidationException("field.mandatory", "Function Code ");
			}else if(Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("MSG")) {
				logger.error("ERROR - Format is blank");
				throw new RequestValidationException("field.mandatory", "Format ");
			}
			else{
				logger.error("ERROR - API Code  is blank");
				throw new RequestValidationException("field.mandatory", "API Code");
			}
		}
		if(Utilities.trim(testStep.getModuleCode()).length() >50) {
			if(Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("GUI")){
				throw new RequestValidationException("field.length", new String[] {"Function Code ","50"});
			}else{
				throw new RequestValidationException("field.length", new String[] {"API Code ","50"});
			}
		}
		/*Modified by Padmavathi for TNJN27-52 starts*/
		/*Modified by Ashiki for TENJINCG-1275 starts*/
		if(!Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("MSG")) {
			if(Utilities.trim(testStep.getOperation()).equals("-1") ||Utilities.trim(testStep.getOperation()).length() < 1 ) {
				/*Modified by Padmavathi for TNJN27-52 ends*/
				logger.error("ERROR -Operation is blank");
				throw new RequestValidationException("field.mandatory", "Operation");
			}
			if(Utilities.trim(testStep.getOperation()).length() >100) {
				throw new RequestValidationException("field.length", new String[] {"Operation","100"});
			}
			if(Utilities.trim(testStep.getType()).length() < 1) {
				logger.error("ERROR - Type  is blank");
				throw new RequestValidationException("field.mandatory", "Type");
			}else if(!Utilities.trim(testStep.getType()).equalsIgnoreCase("Positive")&&!Utilities.trim(testStep.getType()).equalsIgnoreCase("Negative")){
				throw new RequestValidationException("Type ["+testStep.getType()+"] not found, Please enter value either 'Positive' or 'Negative'.");
			}
		}
		if(Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("MSG")) {
			if(Utilities.trim(testStep.getActualResult()).equals("-1")) {
				throw new RequestValidationException("field.mandatory", "Message");
			}
			if(Utilities.trim(testStep.getFileName()).length()<1) {
				throw new RequestValidationException("field.mandatory", "File Name");
			}
			if(Utilities.trim(testStep.getFilePath()).length()<1) {
				throw new RequestValidationException("field.mandatory", "File Path");
			}
		}
		/*Modified by Ashiki for TENJINCG-1275 ends*/
		/*Modified by Padmavathi for TNJN27-52 starts*/
		if(!Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("MSG")){
		if(Utilities.trim(testStep.getAutLoginType()).equals("-1") || Utilities.trim(testStep.getAutLoginType()).length() < 1 ) {
			/*Modified by Padmavathi for TNJN27-52 ends*/
			logger.error("ERROR - AUT Login Type  is blank");
			throw new RequestValidationException("field.mandatory", "AUT Login Type");
		}
		if(Utilities.trim(testStep.getAutLoginType()).length() >30) {
			throw new RequestValidationException("field.length", new String[] {"AUT Login Type","30"});
		}
		}
		if(Utilities.trim(testStep.getTxnMode()).length() < 1) {
			logger.error("ERROR - Mode  is blank");
			throw new RequestValidationException("field.mandatory", "Mode");
		}
		if(Utilities.trim(testStep.getTxnMode()).length() > 3) {
			throw new RequestValidationException("field.length", new String[] {"Mode","3"});
		}
		if(!Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("MSG")) {
		if(Utilities.trim(testStep.getType()).length() < 1) {
			logger.error("ERROR - Type  is blank");
			throw new RequestValidationException("field.mandatory", "Type");
		}else if(!Utilities.trim(testStep.getType()).equalsIgnoreCase("Positive")&&!Utilities.trim(testStep.getType()).equalsIgnoreCase("Negative")){
			throw new RequestValidationException("Type ["+testStep.getType()+"] not found, Please enter value either 'Positive' or 'Negative'.");
		}
		}
		
		if(Utilities.trim(testStep.getDataId()).length() < 1) {
			logger.error("ERROR - Data ID  is blank");
			throw new RequestValidationException("field.mandatory", "Data ID");
		}
		if(Utilities.trim(testStep.getDataId()).length() > 30) {
			
			throw new RequestValidationException("field.length", new String[] {"Data ID","30"});
		}
		
		if(Utilities.trim(testStep.getValidationType()).length() < 1) {
			logger.error("ERROR - validation type  is blank setting to default functional");
			testStep.setValidationType("Functional");
		}else if(!testStep.getValidationType().trim().equalsIgnoreCase("Functional") && !testStep.getValidationType().trim().equalsIgnoreCase("UI")){
				throw new RequestValidationException("Validation Type ["+testStep.getValidationType()+"] not found, Please enter value either 'Functional' or 'UI'.");
		}
		
		if(Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("API")){
			if(!Utilities.trim(testStep.getValidationType()).equalsIgnoreCase("")&&!testStep.getValidationType().equalsIgnoreCase("Functional"))
			{
				throw new RequestValidationException("The parameter validationType cannot be used for Txnmode API.");
			}
		}
		/*Modified by Padmavathi TNJN27-45  starts*/
			if(testStep.getRowsToExecute()<= 0) {
				testStep.setRowsToExecute(1);
			}
			else if(testStep.getRowsToExecute() > 9999) {
			throw new RequestValidationException("field.length", new String[] {"Rows To Execute","4"});
		}	
			/*Modified by Padmavathi TNJN27-45  ends*/
			
		if(duplicateCheckRequired) {
			if(helper.hydrateTestStep(testStep.getTestCaseRecordId(), testStep.getId()) != null) {
				logger.error("ERROR - TestStep already exists ", testStep.getId());
				throw new DuplicateRecordException("teststep.already.exists",testStep.getId());
			}
		}
		
	}
	
	/*Added by Pushpalatha for TENJINCG-611 starts*/
	public ArrayList<TestStep> hydrateStepsForTestCase(Connection projConn, Connection appConn, int tcRecId) throws DatabaseException {
		/*Modified by Padmavathi for TENJINCG-997 starts*/
		ArrayList<TestStep> steps=helper.hydrateStepsForTestCase(projConn, appConn, tcRecId,true);
		/*Modified by Padmavathi for TENJINCG-997 ends*/
		return steps;
	}
	/*Added by Pushpalatha for TENJINCG-611 ends*/
	
	
	
	/*Added by Padmavathi for TENJINCG-645 starts*/
	public ArrayList<TestStep> hydrateSteps (int tcRecId) throws DatabaseException {
		ArrayList<TestStep> steps=helper.hydrateTestSteps(tcRecId); 
		return steps;
	}
	/*Modified by Padmavathi for TJN27-33 starts*/
	public ArrayList<TestStep> hydrateSteps (int tcRecId,Connection conn, Connection appConn) throws DatabaseException {
		ArrayList<TestStep> steps=helper.hydrateStepsForTestCase(conn, appConn, tcRecId,false); 
		return steps;
	}
	/*Modified by Padmavathi for TJN27-33 ends*/
	public int getStepRecId(int sequence,int tcRecId) throws DatabaseException{
		return this.helper.hydrateTestStepRecId(sequence,tcRecId);
	}
	/*Added by Padmavathi for TENJINCG-645 ends*/
	
	/*Added by  Preeti for  TENJINCG-850 starts*/
	public ArrayList<TestStep> hydrateSteps (Connection conn, int tcRecId) throws DatabaseException {
		ArrayList<TestStep> steps=helper.hydrateStepsForTestCase(conn, tcRecId); 
		return steps;
	}
	public int getStepRecId(Connection conn, int sequence,int tcRecId) throws DatabaseException{
		return this.helper.hydrateTestStepRecId(conn,sequence,tcRecId);
	}
	/*Added by  Preeti for  TENJINCG-850 ends*/
	
	/*Added by Pushpa for TENJINCG-844 starts*/
	public ArrayList<TestStep> hydrateStepsForTcTs(Connection projConn, Connection appConn, int tcRecId) throws DatabaseException {
		ArrayList<TestStep> steps=helper.hydrateStepsForTcTs(projConn, appConn, tcRecId);
		return steps;
	}
	/*Added by Pushpa for TENJINCG-844 starts*/
	
	public void copyManualFieldMap(int sourceTcRecId,String sourceStepId, TestStep step) throws DatabaseException{
		helper.copyManualFieldMap(sourceTcRecId,sourceStepId, step);
	}
	//*Added by  Preeti for  TENJINCG-850 starts*/
	public void copyManualFieldMap(Connection conn, int sourceTcRecId,String sourceStepId, TestStep step) throws DatabaseException{
		helper.copyManualFieldMap(conn,sourceTcRecId,sourceStepId, step);
	}
	public HashMap<Integer, String> getAppIdForSteps(Connection conn, String[] selectedTestCase) throws DatabaseException{
		HashMap<Integer, String> listApp = this.helper.getAppIdForSteps(conn, selectedTestCase);
		return listApp;
	}
	/*Added by  Preeti for  TENJINCG-850 ends*/
	
	/*Added by Padmavathi and Preeti for TENJINCG-849 and TENJINCG-850 starts*/
	public void copyTestStepsForCase(Connection conn,TestCase testCase,int sourceTcRecId, ArrayList<TestStep> testSteps, int tcRecId) throws DatabaseException{
		int stepsCount=testSteps.size();
		if(stepsCount>1 && testCase.getTcPresetRules()!=null && !testCase.getTcPresetRules().equalsIgnoreCase("")){ 
			new TestCaseHandler().updatePresetRule(conn,tcRecId, testCase.getTcPresetRules());
		}
		for(TestStep step : testSteps){
			int sourceStepRecId = step.getRecordId();
			step.setTestCaseRecordId(tcRecId);
			/*Modified by padmavathi for TPT-17 starts*/
			TestStep testStep=this.helper.persistTestStep(conn, step);
			step.setRecordId(testStep.getRecordId());
			/*Modified by padmavathi for TPT-17 ends*/
			logger.info("TestStep  created successfully"+step.getId());
			
			if(stepsCount>1 && step.getSequence()!=1&& testCase.getTcPresetRules()!=null && !testCase.getTcPresetRules().equalsIgnoreCase("")){ 
				new DependencyRulesHandler().persistDependencyRules(conn, step, "TestCase", testCase.getTcPresetRules());
			}
			else if (step.getCustomRules() !=null && !step.getCustomRules().equals("")){
				new DependencyRulesHandler().persistDependencyRules(conn, step, "TestStep", step.getCustomRules());
			}
			
			if(step.getTxnMode().equalsIgnoreCase("GUI")){
				this.copyManualFieldMap(conn,sourceTcRecId,step.getId(), step);
				if(step.getValidationType() != null && step.getValidationType().equalsIgnoreCase("UI")){
				/*Modified by padmavathi for TPT-17 starts*/
					new UIValidationHandler().copyUIValidationSteps(conn, testStep.getRecordId(),sourceStepRecId);
				/*Modified by padmavathi for TPT-17 ends*/
				}
				
			}
		}
	}
	/*Added by Padmavathi and Preeti for TENJINCG-849 and TENJINCG-850 ends*/
	
	/*Added by Padmavathi for TENJINCG-849 starts*/
	public int getStepsCountForCase(int tcRecId) throws DatabaseException{
		return helper.getStepsCountForCase(tcRecId);
	}
	/*Added by Padmavathi for TENJINCG-849 ends*/
	/*Added by Pushpa for TENJINCG-910 starts*/
	public String getAppDefaultBrowser(int appId) throws SQLException, DatabaseException {
		
		String browser=this.helper.getAppDefaultBrowser(appId);
		return browser;
	}
	/*Added by Pushpa for TENJINCG-910 ends*/

	/*Added by Ashiki for Tenj210-108 starts*/
	public void importTestStep(TestStep testStep, int projectId, Aut aut) throws RequestValidationException, DatabaseException {
			if(helper.hydrateTestStep(testStep.getTestCaseRecordId(), testStep.getId()) != null) {
				this.validateTestStepMandatoryFields(testStep,false);
				if(Utilities.trim(testStep.getTxnMode()).equalsIgnoreCase("GUI")){
					this.validateGuiTestStep(aut,testStep);
				}else{
					this.validateApiTestStep(testStep);
				}
				
				this.validateAutLoginType(aut, testStep);
				logger.info("All validations are completed successfully");
				helper.updateImportedTeststep(testStep);
				
			}else {
				this.persistTestStep(testStep, projectId);
			}
		
		
	}
	/*Added by Ashiki for Tenj210-108 end*/
}