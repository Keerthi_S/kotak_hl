
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:   ProjectDashBoardHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 19-02-2019			Padmavathi		        Newly added for TENJINCG-924
 * 21-02-2019           Padmavathi              TENJINCG-924
 * 03-05-2019			Preeti					TENJINCG-1019
 
 */
package com.ycs.tenjin.handler;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

import org.json.JSONObject;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ProjectDashboardHelper;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.run.TestRun;

public class ProjectDashboardHandler {
	
	ProjectDashboardHelper dashboardHelper= new ProjectDashboardHelper();
	
	public Project hydrateProjectForDashboard(int projectId,Connection conn) throws DatabaseException {
		
		return this.dashboardHelper.hydrateProjectForDashboard(projectId,conn);
	}
	
	public TestRun hydrateLastRunBasicDetails(Connection conn, int prjId) throws DatabaseException {
		return this.dashboardHelper.hydrateLastRunBasicDetails(conn, prjId);
	}
	
	public  JSONObject generateDayReportForTCs(int projectId) throws DatabaseException, ParseException {
		return this.dashboardHelper.generateDayReportForTCs(projectId);
	}
	
	
	
	public  JSONObject generateDayReportForTCs(int projectId,String userId) throws DatabaseException, ParseException {
		return this.dashboardHelper.generateDayReportForTCs(projectId,userId);
	}
	public  JSONObject generateDayReportForTCs(int projectId, int tsRecId) throws DatabaseException, ParseException {
		return this.dashboardHelper.generateDayReportForTCs(projectId,tsRecId);
	}
	public  JSONObject generateDayReportForTCs(int projectId, int tsRecId, String userId) throws DatabaseException, ParseException {
		return this.dashboardHelper.generateDayReportForTCs(projectId,tsRecId,userId);
	}
	public  JSONObject generateweeklyReportForTCs(int projectId) throws DatabaseException, ParseException {
		return this.dashboardHelper.generateWeeklyReportForTCs(projectId);
	}
	public  JSONObject generateweeklyReportForTCs(int projectId,String userId) throws DatabaseException, ParseException {
		return this.dashboardHelper.generateWeeklyReportForTCs(projectId,userId);
	}
	public  JSONObject generateweeklyReportForTCs(int projectId,int tsRecId) throws DatabaseException, ParseException {
		return this.dashboardHelper.generateWeeklyReportForTCs(projectId,tsRecId);
	}
	public  JSONObject generateweeklyReportForTCs(int projectId,int tsRecId,String userId) throws DatabaseException, ParseException {
		return this.dashboardHelper.generateWeeklyReportForTCs(projectId,tsRecId,userId);
	}
	public Map<String, Integer> hydrateEntityCount(int projectId, Connection conn) throws DatabaseException, SQLException {
		return this.dashboardHelper.hydrateEntityCount(projectId, conn);
	}
	public Map<TestSet,ArrayList<Integer>> hydrateTestSetForProject(int projectId, Connection conn) throws DatabaseException, SQLException {
		return this.dashboardHelper.hydrateTestSetForProject(projectId, conn);
	}
	public Map<TestSet,ArrayList<Integer>> hydrateTestSetForProjectDefects(int projectId, Connection conn) throws DatabaseException, SQLException {
		return this.dashboardHelper.hydrateTestSetForProjectDefects(projectId, conn);
	}
	public  JSONObject getRunDetails(int runId) throws DatabaseException, ParseException {
		return this.dashboardHelper.hydrateRunDetails(runId);
	}
	public  JSONObject getDefectDetails(int runId) throws DatabaseException, ParseException {
		return this.dashboardHelper.hydrateDefectDetailsForRun(runId);
	}
	public Map<String,Map<String,Integer>> hydrateLatestDefectsForProjects(Connection conn, int prjId) throws DatabaseException, SQLException {
		return this.dashboardHelper.hydrateLatestDefectsForProjects(conn, prjId);
	}
	public TestRun hydrateLastRunBasicDetailsForUser(Connection conn,String userId, int prjId) throws DatabaseException {
		return this.dashboardHelper.hydrateLastRunBasicDetailsForUser(conn,userId, prjId);
	}
	public Map<TestSet,ArrayList<Integer>> hydrateTestSetForProjectForUser(int projectId, String userId, Connection conn) throws DatabaseException, SQLException {
		return this.dashboardHelper.hydrateTestSetForProjectForUser(projectId, userId, conn);
	}
	public Map<TestSet,ArrayList<Integer>> hydrateTestSetForProjectDefectsForUser(int projectId, String userId, Connection conn) throws DatabaseException, SQLException {
		return this.dashboardHelper.hydrateTestSetForProjectDefectsForUser(projectId,userId, conn);
	}
	public Map<String,Map<String,Integer>> hydrateLatestDefectsForProjectsForUser(Connection conn, String userId, int prjId) throws DatabaseException, SQLException {
		return this.dashboardHelper.hydrateLatestDefectsForProjectsForUser(conn,userId, prjId);
	}
	/*Added by Preeti for TENJINCG-1019 ends*/
}
