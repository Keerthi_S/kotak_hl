/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UIValidationHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 14-03-2018		    Padmavathi 		        Newly added for TENJINCG-613
 * 12-10-2018           Padmavathi              TENJINCG-847
 * 24-10-2018           Padmavathi              TENJINCG-847
 * 24-10-2018			Preeti					TENJINCG-850
 */


package com.ycs.tenjin.handler;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.project.UIValidation;
import com.ycs.tenjin.bridge.pojo.project.UIValidationType;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.LearningHelper;
import com.ycs.tenjin.db.UIValidationHelper;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.servlet.TenjinServletException;

public class UIValidationHandler {

	private static final Logger logger = LoggerFactory.getLogger(UIValidationHandler.class);
	UIValidationHelper helper = new UIValidationHelper();
	
	public List<UIValidation> getUIValidationsForStep(int tStepRecId) throws DatabaseException{
		List<UIValidation> vals = this.helper.hydrateUIValidationsForStep(tStepRecId);
		return vals;
	}
	/*Added by Preeti for TENJINCG-850 starts*/
	public List<UIValidation> getUIValidationsForStep(Connection conn, int tStepRecId) throws DatabaseException{
		List<UIValidation> vals = this.helper.hydrateUIValidationsForStep(conn, tStepRecId);
		return vals;
	}
	/*Added by Preeti for TENJINCG-850 ends*/
	public UIValidation getUIValidationStep(int uiValRecId) throws DatabaseException{
		UIValidation val= new UIValidationHelper().hydrateUIValidationStep(uiValRecId);
		if(val==null){
			logger.error("ui validation step not found");
			 throw new RecordNotFoundException("ui.val.step.not.found");
		}
		return val;
	}
	
	public List<UIValidationType> getAllUIValidationTypes() throws DatabaseException{
		List<UIValidationType> valTypes = new UIValidationHelper().hydrateAllUIValidationTypes();
		return valTypes;
	}
	
	public int createUIValidationRecord(HttpServletRequest request) throws  TenjinServletException {
		try {
			UIValidation val = new UIValidation();

			val.setStepRecordId(Integer.parseInt(request.getParameter("testStepRecordId"))); 
			val.setPageArea(request.getParameter("pageArea"));

			String valType = request.getParameter("validationType");
			UIValidationType type = this.helper.hydrateUIValidationType(Integer.parseInt(valType));

			if(type == null) {
				logger.error("Could not create validation step. The Validation type you have selected is no longer valid. Please try again.");
				throw new TenjinServletException("Could not create validation step. The Validation type you have selected is no longer valid. Please try again.");
			}else {
				val.setType(type);
			}

			val.setPreSteps(request.getParameter("preStepsJson"));
			val.setSteps(request.getParameter("valStepsJson"));

			helper.persistUIValidation(val);
			return val.getRecordId();
		} catch (NumberFormatException e) {
			logger.error("Invalid Number", e);
			throw new TenjinServletException("Could not create record due to an internal error. Please contact Tenjin Support.");
		} catch (DatabaseException e) {
			throw new TenjinServletException(e.getMessage());
		} catch (Exception e) {
			logger.error("Uncaught exception while creating UI validation record.", e);
			throw new TenjinServletException("Could not create record due to an internal error. Please contact Tenjin Support.");
		}
	}
	
	public void updateValidationRecord(HttpServletRequest request) throws TenjinServletException {
		String vRecId = request.getParameter("recordId");
		String preJsonString = request.getParameter("preStepsJson");
		String valStepsJsonString = request.getParameter("valStepsJson");
		
		try {
			this.helper.updateUIValidationStep(Integer.parseInt(vRecId), preJsonString, valStepsJsonString);
		} catch (NumberFormatException e) {
			logger.error("ERROR while updating UI validation record", e);
			throw new TenjinServletException("Could not update record due to an internal error. Please contact Tenjin Support.");
		} catch (DatabaseException e) {
			
			throw new TenjinServletException(e.getMessage());
		}
		
	}
	
	public void deleteUIValidationStep(String[] uiRecIds) throws DatabaseException {
		this.helper.deleteUIValidationStep(uiRecIds);
		logger.info("UI validation are deleted successfully.");
	}   
	 
	public List<TestObject> hydrateTestObjectsOnPage(String moduleCode, int appId, String pageAreaName) throws DatabaseException{
		
		List<TestObject> fields = new LearningHelper().hydrateTestObjectsOnPage(moduleCode, appId, pageAreaName);
		return fields;
	}
	
	public List<Location> hydrateMetadata(int appId, String functionCode, boolean mandatoryOnly, boolean editableOnly) throws DatabaseException{
		
		List<Location> metadata = new LearningHelper().hydrateMetadata(appId, functionCode, false, false);
		return metadata;
	}
	
	/*Added by Padmavathi for TENJINCG-847 Starts*/
	public void copyUIValidationSteps(int targetStepRecId,int sourceStepRecId) throws DatabaseException{
		
		List<UIValidation> uiValSteps= this.getUIValidationsForStep(sourceStepRecId);
			if(uiValSteps.size()>0)
				helper.persistUIValidationSteps(uiValSteps,targetStepRecId);
		}
	/*Added by Padmavathi for TENJINCG-847 ends*/
	/*Added by Preeti for TENJINCG-850 starts*/
	public void copyUIValidationSteps(Connection conn, int copyStepRecId,int sourceStepRecId) throws DatabaseException{
		
		List<UIValidation> uiValSteps= this.getUIValidationsForStep(conn, sourceStepRecId);
			if(uiValSteps.size()>0)
				helper.persistUIValidationSteps(conn,uiValSteps,copyStepRecId);
		}
	/*Added by Preeti for TENJINCG-850 ends*/
}
