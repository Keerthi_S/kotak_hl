/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ResultsHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright &copy 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 22-10-2018			Sriram					Newly added for TENJINCG-876
 * 16-09-2020 			Pushpa					TENJINCG-1223
 * 25-09-2020            shruthi                  TENJINCG-1224
 */

package com.ycs.tenjin.handler;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.pojo.project.UIValidation;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.RuntimeScreenshot;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.bridge.pojo.run.UIValidationResult;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ResultsHelper;
import com.ycs.tenjin.db.UIValidationHelper;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.mail.TenjinReflection;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.FileCompressor;
import com.ycs.tenjin.util.HatsConstants;
import com.ycs.tenjin.util.MessageUtil;
import com.ycs.tenjin.util.Utilities;

public class ResultsHandler {
	private static final Logger logger = LoggerFactory.getLogger(ResultsHandler.class);
	
	private ResultsHelper resultHelper = new ResultsHelper();
	
	public TestRun fetchRunSummary(int runId) throws DatabaseException, RecordNotFoundException {	
		return this.fetchRunSummary(runId, true);
	}
	
	public TestRun fetchRunSummary(int runId, boolean includeTestCaseSummary) throws DatabaseException, RecordNotFoundException {
		TestRun run = this.resultHelper.hydrateRunSummary(runId, includeTestCaseSummary);
		if(run == null) {
			logger.error("Error - Run {} was not found", runId);
			throw new RecordNotFoundException();
		}
		return run;
	}
	
	public TestRun fetchRunSummary(int runId, boolean includeTestCaseSummary, boolean includeTestStepSummary) throws DatabaseException, RecordNotFoundException {
		TestRun run = null;
		
		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ)) {
			run = this.resultHelper.hydrateRunSummary(conn, runId, includeTestCaseSummary);
			if(run == null) {
				logger.error("Error - Run {} was not found", runId);
				throw new RecordNotFoundException();
			}
			
			if(includeTestCaseSummary && includeTestStepSummary) {
				ArrayList<TestCase> testCases = new ArrayList<>();
				for(TestCase tc : run.getTestSet().getTests()) {
					tc = this.resultHelper.hydrateTestCaseResultSummary(conn, runId, tc.getTcRecId());
					testCases.add(tc);
				}
				
				run.getTestSet().setTests(testCases);
			}
			
		} catch (SQLException e) {
			logger.error("Error connecting to the database", e);
			throw new DatabaseException("Could not connect to the database");
		}
		
		return run;
	}
	
	
	public List<RuntimeFieldValue> fetchRuntimeFieldValues(int runId, int testStepRecordId, String tduid) throws DatabaseException {
		return this.resultHelper.hydrateRunTimeValues(runId, testStepRecordId, tduid);
	}
	
	public StepIterationResult fetchValidationResults(int runId, int testStepRecordId, int iteration) throws DatabaseException {
		List<ValidationResult> results = this.resultHelper.hydrateValidationResults(runId, testStepRecordId, iteration);
		StepIterationResult result = new StepIterationResult();
		result.setValidationResults(results);
		String validationStatus = "";
		if(results != null) {
			for(ValidationResult r : results) {
				if(r.getStatus().equalsIgnoreCase("error")) {
					validationStatus = HatsConstants.ACTION_RESULT_ERROR;
				}else if(validationStatus.length() < 1) {
					if(r.getStatus().equalsIgnoreCase("fail")) {
						validationStatus = HatsConstants.ACTION_RESULT_FAILURE;
					}
				}
			}
		}
		
		if(validationStatus.length() < 1) {
			validationStatus = HatsConstants.ACTION_RESULT_SUCCESS;
		}
		
		result.setValidationStatus(validationStatus);
		return result;
	}
	
	public Map<RuntimeScreenshot, String> getScreenshots(int runId, int testStepRecordId, int iteration, String tdUid) throws DatabaseException, TenjinServletException {
		LinkedHashSet<RuntimeScreenshot> screenshotArray = new LinkedHashSet<>();
		String classPath = "";
		Map<RuntimeScreenshot, String> imageMap = new LinkedHashMap<RuntimeScreenshot, String> ();
		try {
			URL resource = getClass().getResource("/");
			classPath = resource.getPath();
		} catch (Exception e) {
			logger.error("Error - Could not get context path", e);
			throw new TenjinServletException("generic.error");
		}
		
		String screenshotDumpPath = "screenshots" + File.separator + runId + File.separator + tdUid;
		/*Modified by Prem for TJN210-1 starts to remove the space in the folder structure  */
		screenshotDumpPath = classPath.replaceAll("WEB-INF/classes/", "").replace("%20", " ") + File.separator + screenshotDumpPath;
		/*Modified by Prem for TJN210-1 Ends*/
		
		
		
		logger.info("Loading screenshots from {}", screenshotDumpPath);
		
		File file = new File(screenshotDumpPath);
		if(!file.exists()) {
			logger.info("No screenshots are cached. Fetching screenshots from the server");
			screenshotArray = this.resultHelper.getScreenshot(runId, testStepRecordId, iteration);
			
			file.mkdirs();
			try {
				for(RuntimeScreenshot screenshot : screenshotArray) {
					String imageFileName = testStepRecordId + "_" + screenshot.getTdUid() + "_" + String.format("%03d", screenshot.getSequence()) + ".png";
					BufferedImage bImg = ImageIO.read(new ByteArrayInputStream(screenshot.getScreenshotByteArray()));
					ImageIO.write(bImg, "png", new File(screenshotDumpPath + File.separator + imageFileName));
				}
			} catch (IOException e) {
				logger.error("Error writing screenshots to disk", e);
			}
		}
		
		File[] listOfFiles = file.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			RuntimeScreenshot screenshot = new RuntimeScreenshot();
			  if (listOfFiles[i].isFile()) {
				  String name[] = listOfFiles[i].getName().split("_");
				  screenshot.setSequence(Integer.parseInt(name[name.length-1].replace(".png", "")));
				  imageMap.put(screenshot, "screenshots/" + runId + File.separator + tdUid + File.separator +  listOfFiles[i].getName());
			 }
		}
				
		return imageMap;
	}
	
	public Map<String, String> getApiRequest(int runId, int testStepRecordId, String tdUid) throws DatabaseException {
		String message = this.resultHelper.hydrateAPIRequestResponse(runId, testStepRecordId, tdUid, "request");
		Map<String, String> fileMap = new HashMap<>();
		if(Utilities.isJson(message)) {
			fileMap.put("extn","json");
			fileMap.put("contentType", "application/json");
		}else if(Utilities.isXML(message)) {
			fileMap.put("extn", "xml");
			fileMap.put("contentType", "application/xml");
		}else {
			fileMap.put("extn", "txt");
			fileMap.put("contentType", "text/plain");
		}
		
		fileMap.put("name", runId + "_" + tdUid + "_request");
		fileMap.put("content", message);
		
		return fileMap;
	}
	
	public Map<String, String> getApiResponse(int runId, int testStepRecordId, String tdUid) throws DatabaseException {
		String message = this.resultHelper.hydrateAPIRequestResponse(runId, testStepRecordId, tdUid, "response");
		Map<String, String> fileMap = new HashMap<>();
		if(Utilities.isJson(message)) {
			fileMap.put("extn","json");
		}else if(Utilities.isXML(message)) {
			fileMap.put("extn", "xml");
		}else {
			fileMap.put("extn", "txt");
		}
		
		fileMap.put("name", runId + "_" + tdUid + "_response");
		fileMap.put("content", message);
		
		return fileMap;
	}
	
	public String generateAllTransactionsMessageDownload(int runId) throws DatabaseException, TenjinServletException {
		List<StepIterationResult> results = this.resultHelper.hydrateAllTransactionResults(runId);
		
		String contextRoot = this.getContextRoot();
		String downloadsFolder = contextRoot + File.separator + "Downloads";
		
		Utilities.checkForDownloadsFolder(contextRoot);
		
		String runFolder = downloadsFolder + File.separator + runId + "_messages";
		File file = new File(runFolder);
		if(!file.exists()) {
			file.mkdirs();
		}
		
		if(results != null) {
			for(StepIterationResult result : results) {
				String wsRequest = result.getWsReqMessage();
				String wsResponse = result.getWsResMessage();
				String tdUid = result.getTduid();
				int iNo = result.getIterationNo();
				String folderPath = runFolder + File.separator + tdUid;
				File f = new File(folderPath);
				if(f.exists()) {
					f.delete();
				}
				
				f.mkdir();
				
				if(Utilities.trim(wsRequest).length() > 1) {
					String extn = "txt";
					if(Utilities.isJson(wsRequest)) {
						extn= "json";
					}else if(Utilities.isXML(wsRequest)) {
						extn = "xml";
					}
					logger.debug("Downloading request for {}", tdUid);;
					try {
						OutputStream os = new FileOutputStream(new File(folderPath + "/Txn_" + iNo + "_" + tdUid + "_Request." + extn));
						os.write(wsRequest.getBytes());
						os.close();
					} catch (IOException e) {
						logger.warn("Could not write request for TDUID {} to file", tdUid, e);
						continue;
					}
				}
				
				if(Utilities.trim(wsResponse).length() > 1) {
					String extn = "txt";
					if(Utilities.isJson(wsResponse)) {
						extn= "json";
					}else if(Utilities.isXML(wsResponse)) {
						extn = "xml";
					}
					logger.debug("Downloading request for {}", tdUid);;
					try {
						OutputStream os = new FileOutputStream(new File(folderPath + "/Txn_" + iNo + "_" + tdUid + "_Response." + extn));
						os.write(wsResponse.getBytes());
						os.close();
					} catch (IOException e) {
						logger.warn("Could not write response for TDUID {} to file", tdUid, e);
						continue;
					}
				}
				
			}
			
			String zipFileName = "API_Messages_" + runId + ".zip";
			FileCompressor compressor = new FileCompressor(runFolder, contextRoot + File.separator + "Downloads" + File.separator + zipFileName);
			try {
				compressor.compress();
			} catch (Exception e) {
				logger.error("ERROR compressing files", e);
				throw new TenjinServletException("Could not generate download. Please contact Tenjin Support.");
			}
			
			return zipFileName;
		}else {
			return null;
		}
	}
	
	public Map<String, Map<String, List<UIValidation>>> getUIValidationResults(int runId, int testStepRecordId) throws DatabaseException {
		Map<String, Map<String, List<UIValidation>>> map = new UIValidationHelper().hydrateUIValidationsForStepWithResults(testStepRecordId, runId, "type");
		
		for(String type : map.keySet()) {
			String messageKeyPrefix = type.replace(" ", "_");
			for(String page : map.get(type).keySet()) {
				for(UIValidation val : map.get(type).get(page)) {
					if(val.getResults() != null) {
						for(UIValidationResult result : val.getResults()) {
							String expMessageKey = messageKeyPrefix;
							String actMessageKey = messageKeyPrefix;
							if(Utilities.trim(result.getExpectedValue()).equalsIgnoreCase("true") || Utilities.trim(result.getExpectedValue()).equalsIgnoreCase("false")) {
								 /*Fix for T25IT-123 */
								expMessageKey += "." + Utilities.trim(result.getExpectedValue()) + ".expected";
								actMessageKey += "." + Utilities.trim(result.getActualValue()) + ".actual";
								expMessageKey += "." + Utilities.trim(result.getExpectedValue()).toLowerCase() + ".expected";
								actMessageKey += "." + Utilities.trim(result.getActualValue()).toLowerCase() + ".actual";
								 /*Fix for T25IT-123 ends*/
							}else {
								expMessageKey += ".expected";
								actMessageKey += ".actual";
							}
							
							try {
								 /*Fix for T25IT-123 */
								result.setExpectedValue(MessageUtil.getMessage(expMessageKey, result.getExpectedValue()));
								result.setActualValue(MessageUtil.getMessage(actMessageKey, result.getActualValue()));
								
								String expMsg = MessageUtil.getMessage(expMessageKey, result.getExpectedValue());
								String actMsg = MessageUtil.getMessage(actMessageKey, result.getActualValue());
								result.setExpectedValue(Utilities.trim(expMsg).length() > 0 ? Utilities.trim(expMsg) : result.getExpectedValue());
								result.setActualValue(Utilities.trim(actMsg).length()> 0 ? Utilities.trim(actMsg) : result.getActualValue());
								 /*Fix for T25IT-123 ends*/
							} catch (TenjinConfigurationException e) {
								
								logger.warn("Could not get message from configuration", e);
							}
						}
					}
					
				}
			}
		}
		
		return map;
	}
	
	private String getContextRoot() {
		String classPath="";
		try {
			URL resource = getClass().getResource("/");
			classPath = resource.getPath();
			return classPath.replaceAll("WEB-INF/classes/", "");
		} catch (Exception e) {
			logger.error("Error - Could not get context path", e);
			return "";
		}
	}
/*	Added by Pushpalatha for TENJINCG-1106 starts*/
	public TestRun getRunJson(int runId) throws DatabaseException, SQLException {
		
		String runData=this.resultHelper.getRunJson(runId);
		if(runData==null){
			int projectId=new ResultsHelper().getPrjId(runId);
			TenjinReflection tenjinReflection=new TenjinReflection();
			runData=tenjinReflection.hydrateRunForAudit(runId,projectId);
			tenjinReflection.persisteExecAudit(runId,projectId);
		}
		TestRun run = new TestRun();
		Gson gson = new Gson();
		run = gson.fromJson(runData, TestRun.class);
		/*Added by Pushpalatha for TENJINCG-1227 starts*/
		Map<String,List<Integer>> m=new ResultsHelper().getParentRunIds(run);
		List<Integer> parentRuns=m.get("parentRuns");
		if(parentRuns!=null && parentRuns.size() > 0) {
			 Collections.reverse(parentRuns);
			 run.setAllParentRunIds(parentRuns);
			 run.setPureRunId(parentRuns.get(parentRuns.size()-1));
		 }
		
		
		run.setReRunIds(m.get("childRuns"));
		/*Added by Pushpalatha for TENJINCG-1227 ends*/
		return run;
	}

	public TestCase getTestCaseResultSummary(int runId, int testCaseRecordId) throws DatabaseException, SQLException {
		
		TestRun run=this.getRunJson(runId);
		TestSet testSet=run.getTestSet();
		ArrayList<TestCase> testCases=testSet.getTests();
		TestCase tc=new TestCase();
		for(TestCase t:testCases){
			if(t.getTcRecId()==testCaseRecordId){
				 return t;
				
			}
		}
		
		
		return tc;
	}
	/*Added by Pushpalatha for TENJINCG-1106 ends*/
	/*Added by Pushpalatha for TENJINCG-1223 starts*/
	public void deleteTestRuns(ArrayList<Integer> runs) throws DatabaseException, SQLException {
		
		new ResultsHelper().removeTestRuns(runs);
		
	}
	
	public ArrayList<Integer> getChildRuns(ArrayList<Integer> runs) throws DatabaseException, SQLException {
		
		return new ResultsHelper().getChildRuns(runs);
	}
	/*Added by Pushpalatha for TENJINCG-1223 ends*/
	/*added by shruthi for TENJINCG-1224 starts*/
	public void deleteDefects(ArrayList<Integer> runs) throws DatabaseException, SQLException {
		
		new ResultsHelper().removeDefects(runs);
		
	}
	/*added by shruthi for TENJINCG-1224 ends*/
}
