/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AutHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 24-Oct-2017           Sriram Sridharan          Newly Added For TENJINCG-399 and TENJINCG-400
 * 03-11-2017            Padmavathi                 TENJINCG-400
 * 16-Nov-2017			Gangadhar Badagi		To avoid deletion of Application when it contains Apis
 * 24-Nov-2017			Sahana					Updated appType to integer,UI changes
 * 01-12-2017			Gangadhar Badagi		TENJINCG-546
 * 01-12-2017			Gangadhar Badagi		TENJINCG-547
 * 01-03-2018           Padmavathi              TENJINCG-402
 * 16-03-2018			Preeti					TENJINCG-73
 * 26-03-2018			Pushpalatha 			TENJINCG-611
 * 14-06-2018			Preeti					T251IT-19
 * 21-06-2018			Ashiki					T251IT-96
 * 28-06-2018			Preeti					T251IT-161
 * 15-11-2018			Ashiki					TENJINCG-898
 * 26-Nov 2018			Shivam	Sharma			TENJINCG-904
 * 17-12-2018           Padmavathi              TJN252-36
 * 18-12-2018           Padmavathi              Added try catch 
 * 10-01-2019			Preeti					TJN252-43
 * 11-01-2019           Padmavathi              TJN252-43
 * 25-02-2019			Ashiki					TENJINCG-954
 * 22-03-2019			Preeti					TENJINCG-1018
 * 27-03-2019			Ashiki					TENJINCG-1004
 * 24-04-2019			Roshni					TENJINCG-1038
 * 03-07-2019			Ashiki					TJN27-23
 * 24-10-2019			Roshni					TENJINCG-1119
 * 03-12-2019			Roshni					TENJINCG-1168
 * 01-06-2020			Prem					Tenj210-51
 */

package com.ycs.tenjin.handler;


import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.ApplicationUser;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.AdapterHelper;
import com.ycs.tenjin.db.ApplicationUserHelper;
import com.ycs.tenjin.db.AutsHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ModuleHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

public class AutHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(AutHandler.class);
	private AutsHelper autHelper=new AutsHelper();
	/* Added by Roshni for TENJINCG-1038 starts */
	private AutAuditHandler auditHandler = new AutAuditHandler();
	/* Added by Roshni for TENJINCG-1038 ends */
	
	public List<Aut> getAllApplications() throws DatabaseException {
		return new AutsHelper().hydrateAllAut();
	}
	public List<String> getAllAdapters() throws DatabaseException {
		AdapterHelper adapterHelper = new AdapterHelper();
		List<String> adapters=null;
		try {
			 adapters = adapterHelper.getAvailableAdapters();
		} catch (DatabaseException e) {
			logger.error("ERROR fetching adapters ", e);
			throw new DatabaseException("generic.db.error", e);
	
		}
		return adapters;
	}
	
	public List<String> getFunctionGroups(int appId) throws DatabaseException {
		try {
			// To fix the issue in Function List 
			/*List<String> functionGroups = new AutHelper().fetch_all_groups(appId);*/
			List<String> functionGroups = new ModulesHelper().fetch_all_groups(appId);
			if(functionGroups == null) {
				functionGroups = new ArrayList<String>();
			}
			
			if(functionGroups.size() < 1) {
				functionGroups.add("Ungrouped");
			}
			
			return functionGroups;
		} catch (SQLException e) {
			logger.error("ERROR fetching group names for applciation ", e);
			throw new DatabaseException("generic.db.error", e);
		}
	}
	
	public Aut getApplication(int appId) throws DatabaseException {
		/* modified by Roshni for TENJINCG-1168 starts */
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		Aut aut =this. autHelper.hydrateAut(conn,appId);
		/* modified by Roshni for TENJINCG-1168 ends */
		if(aut == null) {
			throw new RecordNotFoundException("record.not.found");
		}
		return aut;
	}
	public Aut getApplication(int appId,Connection conn) throws DatabaseException {
		
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		Aut aut =this. autHelper.hydrateAut(conn,appId);
		if(aut == null) {
			throw new RecordNotFoundException("record.not.found");
		}
		return aut;
	}
	public Aut getApplication(String appName) throws DatabaseException {
		Aut aut =this. autHelper.hydrateAut(appName);
		if(aut == null) {
			throw new RecordNotFoundException("record.not.found");
		}
		return aut;
	}
	public void persist(Aut aut) throws DatabaseException, RequestValidationException {
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);){
			this.validateAut(conn,aut,true);
			logger.info("Aut validation completed.");
			/*Added by Preeti for TENJINCG-73 starts*/
			if(aut.getPauseTime()==null)
				aut.setPauseTime(0);
			/*Added by Preeti for TENJINCG-73 ends*/
			/*Added by Roshni for TENJINCG-1119 starts*/
			if(aut.getTemplatePwd()!=null&&!aut.getTemplatePwd().equalsIgnoreCase("")){
				try{
					/*Modified by paneendra for VAPT FIX starts*/
					/*aut.setTemplatePwd(new Crypto().encrypt(aut.getTemplatePwd()));*/
					aut.setTemplatePwd(new CryptoUtilities().encrypt(aut.getTemplatePwd()));
					/*Modified by paneendra for VAPT FIX ends*/
				}
				catch(Exception e){
					logger.error("coudnt encrypt the password");
				}
			}
			/*Added by Roshni for TENJINCG-1119 ends*/
			this.autHelper.persistAut(conn,aut);
			logger.info("Aut {} created successfully" ,aut.getName());
		}catch(Exception e){
			logger.error("Coudnt persist AUT");
			/*Added by Prem for Tenj210-51 start*/
			throw new RequestValidationException(e.getMessage(),((RequestValidationException) e).getTargetFields());
			/*Added by Prem for Tenj210-51 End*/
			
		}
		
		
	}
	/*Modified by Padmavathi for TJN252-36 starts*/
	public void update(Aut aut,int appId) throws DatabaseException, RequestValidationException, ResourceConflictException {
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);){
			Aut autToUpdate = this.autHelper.hydrateAut(conn, appId);
			if(autToUpdate == null) {
				logger.error("ERROR - Application {} does not exist.");
				throw new RecordNotFoundException("aut.not.found");
			}
			/*Added by Padmavathi for TJN252-36 starts*/
			String oUserTypes=autToUpdate.getLoginUserType();
			String oOperations=autToUpdate.getOperation();
			/*Added by Padmavathi for TJN252-36 ends*/
			
			String oName = Utilities.trim(autToUpdate.getName());
			/*Changed by Gangadhar Badagi for TENJINCG-546 starts*/
			if(aut.getId()>0){
			if(appId!=(aut.getId())) {
				logger.error("ERROR - Illegal update. appid. Old value [{}], New Value [{}]", appId, aut.getId());
				throw new RequestValidationException("illegal.field.update", "AppId");
			}
			}
			/*Changed by Gangadhar Badagi for TENJINCG-546 ends*/
			
			/*Changed by Gangadhar Badagi for TENJINCG-546 starts*/
			if(aut.getApplicationType()!=null){
				/*Changed by Gangadhar Badagi for TENJINCG-546 ends*/
			if(! autToUpdate.getApplicationType() .equals ( aut.getApplicationType() )){
				logger.error("ERROR - Illegal update. ApplicationType. Old value [{}], New Value [{}]", autToUpdate.getApplicationType(), aut.getApplicationType());
				throw new RequestValidationException("illegal.field.update", "Application Type");
			}
			}
			/*Added by Ashiki for TJN27-23 starts*/  
			if(oUserTypes!=aut.getLoginUserType()){

			String[] userTypeNew = aut.getLoginUserType().split(",");
			String[] userTypeOld = oUserTypes.split(",");
			
			 ArrayList<String> oldUserTypes = new ArrayList<String>();

	         for(int i = 0; i < userTypeOld.length; i++)
	             {
	                 if(!Arrays.asList(userTypeNew).contains(userTypeOld[i]))
	                	 oldUserTypes.add(userTypeOld[i]);
	             }	
	         if(oldUserTypes!=null) {
	        	 ApplicationUser userType=null;
	        	 for(String loginType:oldUserTypes) {
	        	  userType=new ApplicationUserHelper().hydrateApplicationUserType(appId,loginType);

	          	 if(userType!=null) {
	        		   throw new RequestValidationException("Application user(s) has been created for some of the AUT user type(s), Please remove respective Application user(s) before updating AUT user types for application.");
	        	   }
	         }
	         }
			
			}
			/*Added by Ashiki for TJN27-23 ends*/ 		
			autToUpdate.merge(aut);
			/*changed by padmavathi for TENJINCG-402 starts*/
			if(!oName.equalsIgnoreCase(Utilities.trim(autToUpdate.getName()))) {
				/*changed by padmavathi for TENJINCG-402 ends*/
				this.validateAut(conn,autToUpdate, true);
			}else {
				this.validateAut(conn,autToUpdate, false);
			}
			
			try{
				this.isOperationMappedToTestStep(aut,oOperations,conn);
				this.isAutUserMappedToTestStep(aut, oUserTypes, conn);
			} catch (ResourceConflictException e) {
				throw e;
			}
			/*Added by Padmavathi for TJN252-36 ends*/
			
			logger.info("AUT validation completed.");
			this.autHelper.updateAut(autToUpdate,appId);
			logger.info("AUT {} updated successfully", appId);
			/* Added by Roshni for TENJINCG-1038 starts */
			this.auditHandler.persistAuditRecord(aut.getAutAuditRecord());
			logger.info("AUT Audit Record {} inserted successfully", appId);
			/* Added by Roshni for TENJINCG-1038 ends */
		}catch(Exception e){
			logger.error("coudnt update the aut");
		}
		/*Modified by Padmavathi for TJN252-36 ends*/
		
		
	}
	

		private void validateAut(Connection conn,Aut aut, boolean duplicateCheckRequired) throws RequestValidationException, DatabaseException {
			logger.info("Validating aut information");
			
			if(Utilities.trim(aut.getName()).length() < 1) {
				logger.error("ERROR - aut name is blank");
				throw new RequestValidationException("field.mandatory", "Application Name");
			}
			/*Added by Preeti for T251IT-19 starts*/
			if(Utilities.trim(aut.getName()).length() > 100) {
				logger.error("ERROR - aut name is long");
				throw new RequestValidationException("field.length", new String[] {"Application Name","100"});
			}
			/*Added by Preeti for T251IT-19 ends*/
			if(aut.getApplicationType() == null ){
				logger.error("ERROR - aut type is blank");
				throw new RequestValidationException("field.mandatory", "Application Type");
			}
			/*Added by sahana for UI Changes:starts*/
			if( aut.getApplicationType() .equals (1)|| aut.getApplicationType() .equals (2)){
				/*Added by sahana for UI Changes:ends*/
				if(Utilities.trim(aut.getURL()).length() < 1) {
				logger.error("ERROR - URL  is blank");
				throw new RequestValidationException("field.mandatory", "AUT URI");
				}
				/*Added by sahana for UI Changes:starts*/
				if(aut.getApplicationType().equals (1) && Utilities.trim(aut.getBrowser()).length() < 1) {
					/*Added by sahana for UI Changes:ends*/
					logger.error("ERROR - Browser is blank");
					throw new RequestValidationException("field.mandatory", "Browser");
				}
				/*Added by Preeti for T251IT-161 starts*/
				if(Utilities.trim(aut.getURL()).length() > 200) {
					logger.error("ERROR - aut URI is long");
					throw new RequestValidationException("field.length", new String[] {"Application URI","200"});
				}
				/*Added by Preeti for T251IT-161 ends*/
			}
			if( aut.getApplicationType().equals(3) ){
				if(Utilities.trim(aut.getApplicationPackage()).length() < 1) {
					logger.error("ERROR - Application Package  is blank");
					throw new RequestValidationException("field.mandatory", "Package");
					}
				if(Utilities.trim(aut.getApplicationActivity()).length() < 1) {
					logger.error("ERROR - Application Activity  is blank");
					throw new RequestValidationException("field.mandatory", "Activity");
					}
			}
			if( aut.getApplicationType().equals(4) ){
				if(Utilities.trim(aut.getApplicationPackage()).length() < 1) {
					logger.error("ERROR - ApplicationPackage  is blank");
					throw new RequestValidationException("field.mandatory", "Bundle Id");
					}
			}
			
			if(Utilities.trim(aut.getAdapterPackage()).length() < 1) {
				logger.error("ERROR - AdapterPackage  is blank");
				throw new RequestValidationException("field.mandatory", "Adapter Package");
			}
			String dateFormat=Utilities.trim(aut.getDateFormat());
			
			/*Added by Ashiki for T251IT-96 starts*/
			if(dateFormat.length()>50){
				
				throw new RequestValidationException("field.length", new String[] {"Date Format","50"});
			}
			/*Added by Ashiki for T251IT-96 ends*/
			
			try{
			if(dateFormat!=null){
				SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
				sdf.format(new Date());
				aut.setDateFormat( dateFormat);
				}
			}catch(Exception e){
				logger.error("ERROR - invalid dateformat aut.getDateFormat()");
				throw new RequestValidationException("aut.dateformat");
			}
			
			if(duplicateCheckRequired) {
				if(autHelper.hydrateAut(conn,aut.getName()) != null){
					logger.error("ERROR - Application already exists ", aut.getName());
					throw new DuplicateRecordException("aut.already.exists",aut.getName());
				}
			}
			if(!aut.getApplicationType().equals (1))
			{
				
				if(aut.getApplicationType().equals (3) ){
					aut.setApplicationActivity(aut.getApplicationActivity());
				}
				else{
					aut.setApplicationActivity("N-A");
				}
			}
			else
				aut.setPlatform("Web");
			/*Added by Preeti for TENJINCG-73 starts*/
			String pauseLocation = Utilities.trim(aut.getPauseLocation());
			if(pauseLocation != null && !pauseLocation.equalsIgnoreCase("")){
				if(pauseLocation.equalsIgnoreCase("before login") || pauseLocation.equalsIgnoreCase("after login")){
				}
				else{
					logger.error("ERROR - invalid pause location");
					throw new RequestValidationException("aut.pauseLocation");
				}
			}
			if(aut.getPauseTime() != null){
				if(aut.getPauseTime()<0)
				{
					logger.error("ERROR - invalid pause Time");
					throw new RequestValidationException("aut.pauseTime.negative");
				}
			}
			/*Added By Ashiki for TENJINCG-954 starts*/
			if(Utilities.trim(aut.getOperation()).length() < 1) {
				logger.error("ERROR - Operation is blank");
				throw new RequestValidationException("field.mandatory", "AUT operations");
			}
			else if(Utilities.trim(aut.getOperation()).length() > 500) {
				logger.error("ERROR - aut operation is long");
				throw new RequestValidationException("field.length", new String[] {"Application Operations","500"});
			}
			else{
				boolean duplicateAut=this.checkDuplicateValue(Utilities.trim(aut.getOperation()));
				if(duplicateAut){
					throw new DuplicateRecordException("aut.duplicate.operators");
				}
				
			}
			
			if(Utilities.trim(aut.getLoginUserType()).length() < 1) {
				logger.error("ERROR - LoginUserType is blank");
				throw new RequestValidationException("field.mandatory", "AUT user types");
			}
			else if(Utilities.trim(aut.getLoginUserType()).length() > 500) {
				logger.error("ERROR - aut user type is long");
				throw new RequestValidationException("field.length", new String[] {"Application User Types","500"});
			}
		
			else{
				boolean duplicateAut=this.checkDuplicateValue(Utilities.trim(aut.getLoginUserType()));
				if(duplicateAut){
					throw new DuplicateRecordException("aut.duplicate.usertypes");
				}
			}
			if(!Utilities.trim(aut.getLoginUserType()).equalsIgnoreCase("")){
				String[] userTypeSplit = aut.getLoginUserType().split(",");
				for(String uType:userTypeSplit){
					// Modified by Priyanka for KTKCBS-7 starts
					if(uType.length()<=500){
				   // Modified by Priyanka for KTKCBS-7 endsS
						continue;
					}else{
						throw new RequestValidationException("aut.usertype.length", uType);
					}
				}
			}
			/*Added By Ashiki for TENJINCG-954 ends*/
		}

		public void deleteAut(String appId) throws DatabaseException, ResourceConflictException {
			
			/*Added by Gangadhar Badagi for TENJINCG-547 starts*/
			Aut aut = this.getApplication(Integer.parseInt(appId));
			if(aut == null) {
				logger.error("ERROR - Application {} does not exist.");
				throw new RecordNotFoundException("aut.not.found");
			}
			/*Added by Gangadhar Badagi for TENJINCG-547 ends*/
			try{
				ModuleHelper module=new ModuleHelper();
				int id=	Integer.parseInt(appId);
				if(this.autHelper.CheckAutAssociateToProj( id)) {
					logger.error("ERROR deleting AUT");
					throw new ResourceConflictException("aut.delete.project.function" );
				}
				
				/*Changed by Gangadhar Badagi for TENJINCG-547 starts*/
				
				if(module.hydrateModules( id)!=null && module.hydrateModules( id).size()>0) {
					logger.error("ERROR deleting AUT");
					throw new ResourceConflictException( "aut.delete.function" );
				}
				
				
				if(new ModulesHelper().fetch_all_groups( id)!=null && new ModulesHelper().fetch_all_groups( id).size()>0){
					logger.error("ERROR deleting AUT");
					throw new ResourceConflictException( "aut.delete.group" );
				}
				/*Changed by Gangadhar Badagi for TENJINCG-547 ends*/
			}catch (ResourceConflictException e) {
				throw e;
			}catch( SQLException e){
				logger.error("ERROR deleting AUT");
				throw new DatabaseException("aut.delete");
			}
			logger.info("All validations complete. Safe to delete Aut {}", appId);
			this.autHelper.clearAut(appId);
			logger.info("Successfully deleted AUT" ,appId);
		}
		public void deleteAuts(String[] appIds) throws DatabaseException, ResourceConflictException {
			ModuleHelper module=new ModuleHelper();
			ApiHandler api=new ApiHandler();
			
			for(String appId: appIds) {
			int id=	Integer.parseInt(appId);
				logger.debug("Validating AUt {} for deletion", appId);
				try {
					
					if(this.autHelper.CheckAutAssociateToProj(id)) {
						logger.error("ERROR deleting AUT");
						throw new ResourceConflictException("aut.delete.project.function" );
					}
					
					if(module.hydrateModules(id).size()!=0) {
						logger.error("ERROR deleting AUT");
						throw new ResourceConflictException( "aut.delete.function" );
					}
					/*Added by Gangadhar Badagi to avoid deletion of Application when it contains Apis starts*/
					if(api.hydrateAllApi(appId).size()!=0) {
						logger.error("ERROR deleting AUT");
						throw new ResourceConflictException( "aut.delete.api" );
					}
					/*Added by Gangadhar Badagi to avoid deletion of Application when it contains Apis ends*/
					if(new ModulesHelper().fetch_all_groups(id).size()!=0) {
						logger.error("ERROR deleting AUT");
						throw new ResourceConflictException( "aut.delete.group" );
					}
				} catch (ResourceConflictException e) {
					throw e;
				}
				catch(SQLException e){
					logger.error("ERROR deleting AUT");
					throw new ResourceConflictException( "aut.delete" );
				}
			}
			logger.info("All validations complete. Safe to delete Auts");
			
			this.autHelper.clearAllApp(appIds);
			logger.info("Successfully deleted AUT");
		}
		public boolean checkDuplicateValue(String listOfName)
		{
			String[] name=listOfName.split(",");
			int size = name.length;
			boolean duplicate=false;

			for (int i = 0; i < size; i++) 
			{

				for (int j = i + 1; j < size; j++) 
				{


					if (name[i].equalsIgnoreCase(name[j]))
					{
						duplicate=true;
						return duplicate;
					}
				}
			}
			return duplicate;
		}
		/*Added by Pushpalatha for TENJINCG-611 starts*/
		public int hydrateAppId(int id, String appName) throws DatabaseException {
			int appId=new AutsHelper().hydrateAppId(id, appName);
			return appId;
		}
		/*Added by Pushpalatha for TENJINCG-611 ends*/
		public int getAutType(int appId) throws DatabaseException {
			
			int appType=new AutsHelper().getAutType(appId);
			return appType;
		}
		//added by shivam sharma for  TENJINCG-904 starts
		public void updateAppFileName(String appFileName,int appId) throws DatabaseException {
			this.autHelper.updateAppFileName(appFileName, appId);
		}
		//added by shivam sharma for  TENJINCG-904 ends
		
		
		/*Added by Ashiki TENJINCG-1004 starts*/
		public String downloadApkFile(int appId) throws DatabaseException {
			return this.autHelper.downloadApkFile(appId);
		}
		/*Added by Ashiki TENJINCG-1004 ends*/
		
		/*Added by Padmavathi for TJN252-36 starts*/
		public void isOperationMappedToTestStep(Aut autToUpdate,String oOperations,Connection conn) throws ResourceConflictException{
			try{
				if(oOperations!=null && autToUpdate.getOperation()!=null){
					String []operationsToUpdate=autToUpdate.getOperation().split(",");
					String []operations=oOperations.split(",");
					ArrayList<String> operationsToCheck = new ArrayList<String>();
					for(int i = 0; i < operations.length; i++) {
						//comparing both old operations and operations to update
						if(!Arrays.asList(operationsToUpdate).contains(operations[i]))
							//if old operations is not there in updating operations to  then adding to operationsTodelete
							operationsToCheck.add(operations[i]);
					}
					if(operationsToCheck.size()>0){
						if(this.autHelper.isOperationMappedToTestStep(conn, autToUpdate.getId(), operationsToCheck)) {
							logger.error("ERROR updating aut - operation {} is mapped to one or more test steps");
							throw new ResourceConflictException("aut.operation.step.conflict");
						}
					}
				}
			}catch(NullPointerException e){
				logger.error("Error ", e);
			}
		}
		public void isAutUserMappedToTestStep(Aut autToUpdate,String oUserTypes,Connection conn) throws ResourceConflictException{
			try{
				if(oUserTypes!=null && autToUpdate.getLoginUserType()!=null){
				String []autUsersTypesToUpdate=autToUpdate.getLoginUserType().split(",");
				String []autUsersTypes=oUserTypes.split(",");
			
				ArrayList<String> userTypesToCheck = new ArrayList<String>();
		
				for(int i = 0; i < autUsersTypes.length; i++) {
					//comparing both old user Types and user types to update
					if(!Arrays.asList(autUsersTypesToUpdate).contains(autUsersTypes[i]))
						//if old user types is not there in updating user types to  then adding to userTypesTodelete
						userTypesToCheck.add(autUsersTypes[i]);
				}
				if(userTypesToCheck.size()>0){
					if(this.autHelper.isAutUserMappedToTestStep(conn, autToUpdate.getId(), userTypesToCheck)) {
						logger.error("ERROR updating aut  - aut user {} is mapped to one or more test steps");
						throw new ResourceConflictException("aut.user.types.step.conflict");
					}
				}
			 }
			}catch(NullPointerException e){
				logger.error("Error ", e);
			}
		}
		/*Added by Padmavathi for TJN252-36 ends*/
		/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Starts*/
		
		public Aut getOAuthRecord(int appId) throws DatabaseException {
			return this.autHelper.hydrateOAuthRecord(appId);
		}
		public void persistOAuthRecord(Aut record) throws DatabaseException, RequestValidationException{
			this.validateOAuthDetail(record);
			this.autHelper.persistOAuthRecord(record);
		}
		
		public boolean oAuthRecordExists(int appId) throws DatabaseException {
			return this.autHelper.OAuthRecordExists(appId);
		}
		
		private void validateOAuthDetail(Aut oAuth) throws RequestValidationException, DatabaseException{
			
			if(Utilities.trim(oAuth.getGrantType()).length() < 1) {
				throw new RequestValidationException("field.mandatory", "Grant Type");
			}
			
			if(((!oAuth.getGrantType().equalsIgnoreCase("password")) && (!oAuth.getGrantType().equalsIgnoreCase("client_credentials"))) && Utilities.trim(oAuth.getCallBackUrl()).length() < 1) {
				throw new RequestValidationException("field.mandatory", "Callback URL");
			}
			
			if(Utilities.trim(oAuth.getClientId()).length() < 1) {
				throw new RequestValidationException("field.mandatory", "Client ID");
			}
			
			if((!oAuth.getGrantType().equalsIgnoreCase("implicit")) && Utilities.trim(oAuth.getClientSecret()).length() < 1) {
				throw new RequestValidationException("field.mandatory", "Client Secret");
			}
			
			if(((!oAuth.getGrantType().equalsIgnoreCase("password")) && (!oAuth.getGrantType().equalsIgnoreCase("client_credentials"))) && Utilities.trim(oAuth.getAuthUrl()).length() < 1) {
				throw new RequestValidationException("field.mandatory", "Auth URL");
			}
			
			if((!oAuth.getGrantType().equalsIgnoreCase("implicit")) && Utilities.trim(oAuth.getAccessTokenUrl()).length() < 1) {
				throw new RequestValidationException("field.mandatory", "Access Token URL");
			}
		}
		/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Ends*/
 }
