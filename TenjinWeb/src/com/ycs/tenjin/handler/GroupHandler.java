/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GroupHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 19-02-2018		   Pushpalatha			   Newly Added 
* 19-06-2018			Preeti					T251IT-79
* 03-07-2018           Padmavathi               T251IT-172
* 03-12-2019			Roshni					TENJINCG-1168
*/


package com.ycs.tenjin.handler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.aut.Group;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.GroupHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.util.Utilities;

public class GroupHandler {
	private static final Logger logger = LoggerFactory.getLogger(GroupHandler.class);
	private GroupHelper groupHelper=new GroupHelper();
	public List<Group> getAllGroups() throws DatabaseException {
		return this.groupHelper.hydrateAllGroups();
	}
	
	public void deleteGroup(String[] groups) throws DatabaseException{
		this.groupHelper.clearGroup(groups);
	}
	
	public Group persistGroup(Group group) throws DatabaseException, RequestValidationException{
		logger.info("Validating group information");
		this.validateGroupDetails(group, true);	
		/*Added by Padmavathi for T251IT-172 starts*/
		if(this.groupHelper.hydrateGroup(group.getAut(), group.getGroupName())!=null){
			throw new DuplicateRecordException("Group.already.exist", group.getGroupName());
		}
		/*Added by Padmavathi for T251IT-172 ends*/
		logger.info("Done");
		return this.groupHelper.persistGroup(group);
	}

	private void validateGroupDetails(Group group, boolean b) throws DatabaseException, RequestValidationException {
		
		if(Utilities.trim(group.getGroupName()).length()<1){
			throw new RequestValidationException("field.mandatory", "Group Name");
		}
		if(Utilities.trim(group.getGroupName()).length()>105){
			throw new RequestValidationException("groupName.length","Group Name");
		}
		/*Added by Preeti for T251IT-79 starts*/
		if(Utilities.trim(group.getGroupDesc()).length()>1000){
			throw new RequestValidationException("field.length", new String[] {"Group Description","1000"});
		}
		/*Added by Preeti for T251IT-79 ends*/
	}

	public Group hydrateGroup(int appId,String groupName) throws DatabaseException {
		
		Group group=this.groupHelper.hydrateGroup(appId,groupName);
		if(group==null){
			throw new RecordNotFoundException("group.not.found");
		}
		return group;
		
	}

	public ArrayList<ModuleBean> hydrateModules(int appId, String groupName) throws DatabaseException, SQLException {
		ArrayList<ModuleBean> mappedMod=this.groupHelper.hydrateModules(appId,groupName);
		return mappedMod;
	}

	public void unMapFunction(int appid, String groupName, String[] record) throws DatabaseException {
		this.groupHelper.unMapFunction(appid,groupName,record);
		
	}

	public void mapFunction(int appid, String groupName, String[] record) throws DatabaseException {
		this.groupHelper.mapFunction(appid,groupName,record);
		
	}

	public ArrayList<Api> hydrateApi(int appId, String groupName) throws DatabaseException, SQLException {
		ArrayList<Api> mappedApi=this.groupHelper.hydrateApi(appId,groupName);
		return mappedApi;
	}

	public void mapApi(int appId, String groupName, String[] record) throws DatabaseException {
		this.groupHelper.mapApi(appId,groupName,record);
		
	}
	public void unMapApi(int appId, String groupName, String[] record) throws DatabaseException {
		this.groupHelper.unMapApi(appId,groupName,record);
		
	}

	public Aut hydrateAut(int appId) throws DatabaseException {
		/* modified by Roshni for TENJINCG-1168 starts */
		/*Aut aut=new AutsHelper().hydrateAut(appId);*/
		Aut aut=new AutHandler().getApplication(appId);
		/* modified by Roshni for TENJINCG-1168 ends */
		return aut;
	}
	
	/*Modified by Preeti for T251IT-79 starts*/
	public void updateGroup(Group group) throws DatabaseException, RequestValidationException{
		logger.info("Validating group information");
		this.validateGroupDetails(group, true);	
		logger.info("Done");
		this.groupHelper.updateGroup(group);
	}
	/*Modified by Preeti for T251IT-79 ends*/
	
	public List<Module> hydrateModulesInGroup(int appId,String groupName) throws DatabaseException{
		List<Module> module=this.groupHelper.hydrateModulesInGroup(appId, groupName);
		return module;
	}

	public ModuleBean hydrateModule(int appId, String moduleCode) throws DatabaseException, SQLException {
		ModuleBean moduleBean=new ModulesHelper().hydrateModule(appId,moduleCode);
		return moduleBean;
	}
	
	public boolean mapSingleFunction(int appId, String groupName,String moduleCode) throws DatabaseException{
		return new ModulesHelper().mapSingleFunction(appId,groupName,moduleCode);
		
	}
	
	public Api hydrateApi1(int appId, String moduleCode) throws DatabaseException, SQLException {
		Api api=new ApiHelper().hydrateApi(appId,moduleCode);
		return api;
	}
	
	public boolean mapSingleApi(int appId, String groupName,String moduleCode) throws DatabaseException{
		return new ApiHelper().mapSingleApi(appId,groupName,moduleCode);
		
	}
	
}
