
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TenjinSessionHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 12-10-2018             Leelaprasad P          newly added for TENJINCG-872
* 19-11-2018			Prem					 TENJINCG-843
* 26-11-2018			Prem					 TENJINCG-843
* 28-11-2018			Prem					 TENJINCG-843
* 10-12-2018			Prem				 	TJNUN262-108
* 13-12-2018            Padmavathi              TJNUN262-79
* 17-12-2018			Prem					 for java 7 encryption and decryption
* 06-05-2019			Roshni					TENJINCG-1036
* 24-06-2019            Padmavathi              for license
* */
package com.ycs.tenjin.handler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCrypt;

import com.ycs.tenjin.DuplicateUserSessionException;
import com.ycs.tenjin.LicenseExpiredException;
import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.MaxActiveUsersException;
import com.ycs.tenjin.TenjinAuthenticationException;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.UserBlockedException;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.TenjinSessionHelper;
import com.ycs.tenjin.license.License;
import com.ycs.tenjin.security.Crypto;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

public class TenjinSessionHandler {
	
	TenjinSessionHelper sessionHelper = new TenjinSessionHelper();
	private static final Logger logger = LoggerFactory.getLogger(TenjinSessionHandler.class);
	public void clearUsers(String[] uArray) throws DatabaseException {
		this.sessionHelper.clearUser(uArray);
	}
	@SuppressWarnings("static-access")
	public User authenticate(String userId, String password, String terminal,HttpServletRequest request) throws DatabaseException,TenjinServletException, DuplicateUserSessionException, MaxActiveUsersException, LicenseExpiredException, LicenseInactive, ParseException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, TenjinAuthenticationException, TenjinConfigurationException {
		User user = this.sessionHelper.hydrateUserToAuthenticate(userId);

		
		if(user == null) {
			throw new TenjinServletException("Authentication Failed");
		}
		if(!this.authenticatePassword(password, user)) {
			logger.error("Authentication Failed for user [{}]", userId);
			
			this.sessionHelper.persistFailedLoginAttempt(user.getId(), terminal);
			
			if(user.getFailedLoginAttemptsCount() >= 2) {
				
				logger.info("User [{}] is blocked due to too many failed login attempts", user.getId());
				throw new UserBlockedException(user.getId()); 
			}
			/* Added by Roshni for TENJINCG-1036 starts */
			else if(user.getFailedLoginAttemptsCount() >= 0 && user.getFailedLoginAttemptsCount() <= 1){
				throw new TenjinAuthenticationException("Reset password");
			}
			/* Added by Roshni for TENJINCG-1036 ends */
			throw new TenjinServletException("Authentication Failed");
		}

		/*replaced by shruthi for TCGST-55 starts*/
		/*Modified by Padmavathi for license starts*/
		if(user.isLoggedIn()) {
			logger.error("Authentication error - User [{}] is already logged in from [{}]", user.getId(), user.getCurrentTerminal());
			throw new DuplicateUserSessionException(user.getCurrentTerminal());
		}
	 		   License	license = (License) CacheUtils.getObjectFromRunCache("licensedetails");
	 		   logger.info("licenseActiveValidation is getting called");
	 		   new LicenseHandler().licenseActiveValidation(license, false,request);
	 		   logger.info("Successfully validation done for license activation");
	 		  logger.info("Checking for license type ");
	 		   if(license.getType().getName().equalsIgnoreCase("Floating") && user.getMaxNumberUsers()>=license.getCoreLimit()) {
	 			   logger.error("More number of Active users than permited...");
					throw new MaxActiveUsersException("Number of users exceeded,Please contact Tenjin support.");
				}
			 
		/*Modified by Padmavathi for license ends*/
	   /*replaced by shruthi for TCGST-55 ends*/
		if(!"Y".equalsIgnoreCase(user.getEnabledStatus())) {
			logger.error("Authentication error - User [{}] is blocked", userId);
			throw new UserBlockedException(user.getId()); 
		}
				
		
		user.setCurrentTerminal(terminal);
		/*Added by Padmavathi for TJNUN262-79 starts*/
		String rawTimestamp = Utilities.getRawTimeStamp();
		String sessionKey=this.buildSessionKey(rawTimestamp,terminal,user.getId());
		user.setSessionKey(sessionKey);
		/*Added by Padmavathi for TJNUN262-79 ends*/
		logger.info("calling finalize authentication");
		this.sessionHelper.finalizeAuthentication(user);
		logger.info("User [{}] authenticated successfully.", user.getId());
		return user;
	}
	/*Added by Padmavathi for TJNUN262-79 starts*/
private String buildSessionKey(String rawTimestamp, String terminal, String userId) throws TenjinServletException {
	         String sessionKeyNormal=terminal+rawTimestamp;
	         String sessionKeyFinal="";
	         try {
				sessionKeyFinal=new Crypto().encryptOneWay(sessionKeyNormal);
			} catch (NoSuchAlgorithmException e) {
				logger.error(e.getMessage());
				throw new TenjinServletException("unable to persist user session please contact tenjin administrator");
			} catch (UnsupportedEncodingException e) {
				logger.error(e.getMessage());
				throw new TenjinServletException("unable to persist user session please contact tenjin administrator");
			
			}
		return sessionKeyFinal;
	}
/*Added by Padmavathi for TJNUN262-79 ends*/
private boolean authenticatePassword(String password, User user) throws TenjinServletException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
		
		boolean auth=false;
		String dPass="";
		try {
			/*Modified by Pushpalatha for TENJINCG-1042 starts*/
			/*Modified by Prem for VAPT issue fix starts*/
			dPass = user.getPassword();
			/*Modified by Prem for VAPT issue fix Ends*/
			/*Modified by Pushpalatha for TENJINCG-1042 ends*/
		} catch (NumberFormatException e) {
			logger.error("Internal ERROR occured");
			throw new TenjinServletException("An internal error occurred. Please contact your Tenjin Support.");
		}
		/*Modified by Pushpalatha for TENJINCG-1042 starts*/
		if(checkPassword(password ,dPass)){
			auth=true;
		}
		/*Modified by Pushpalatha for TENJINCG-1042 ends*/
		return auth;
	}
	/*added by shruthi for VAPT Bcryption fixes ends*/
	public static boolean checkPassword(String password_plaintext, String stored_hash) {
		boolean password_verified = false;
		if(null == stored_hash || !stored_hash.startsWith("$2a$"))
			throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");
		password_verified = BCrypt.checkpw(password_plaintext, stored_hash);
		return(password_verified);
	}
	/*added by shruthi for VAPT Bcryption fixes ends*/
	


}
