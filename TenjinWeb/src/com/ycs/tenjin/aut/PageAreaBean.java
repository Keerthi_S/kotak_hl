/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  PageAreaBean.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

/*17-Nov-2014 G2WMAP-New Screen (Sneha):new POJO class for PageArea*/
package com.ycs.tenjin.aut;

public class PageAreaBean {
	int appId;
	String paFuncCode;
	String paName;
	String paParent;
	String paWayIn;
	String paWayOut;
	int paSeqNo;
	String txnMode;
	String paType;
	String paSource;
	String paPath;
	String paLabel;

	public void setPaSource(String paSource) {
		this.paSource = paSource;
	}

	public String getPaSource() {
		return this.paSource;
	}

	public void setPath(String paPath) {
		this.paPath = paPath;
	}

	public String getpaPath() {
		return this.paPath;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}

	public int getAppId() {
		return this.appId;
	}

	public void setPaFuncCode(String paFuncCode) {
		this.paFuncCode = paFuncCode;
	}

	public String getPaFuncCode() {
		return this.paFuncCode;
	}

	public void setPaName(String paName) {
		this.paName = paName;
	}

	public String getPaName() {
		return this.paName;
	}

	public void setPaParent(String paParent) {
		this.paParent = paParent;
	}

	public String getPaParent() {
		return this.paParent;
	}

	public void setPaWayIn(String paWayIn) {
		this.paWayIn = paWayIn;
	}

	public String getPaWayIn() {
		return this.paWayIn;
	}

	public void setPaWayOut(String paWayOut) {
		this.paWayOut = paWayOut;
	}

	public String getPaWayOut() {
		return this.paWayOut;
	}

	public void setPaSeqNo(int paSeqNo) {
		this.paSeqNo = paSeqNo;
	}

	public int getPaSeqNo() {
		return this.paSeqNo;
	}

	public void setTxnMode(String txnMode) {
		this.txnMode = txnMode;
	}

	public String getTxnMode() {
		return this.txnMode;
	}

	public void setPaType(String paType) {
		this.paType = paType;
	}

	public String getPaType() {
		return this.paType;
	}

	public void setPaLabel(String paLabel) {
		this.paLabel = paLabel;
	}

	public String getPaLabel() {
		return this.paLabel;
	}
}
