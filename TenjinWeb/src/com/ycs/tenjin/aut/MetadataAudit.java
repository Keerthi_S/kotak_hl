package com.ycs.tenjin.aut;

public class MetadataAudit {
private String user;
private String type;
private String date;
private String status;

public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getUser() {
	return user;
}
public void setUser(String user) {
	this.user = user;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}
}
