/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestStep.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                	CHANGED BY              DESCRIPTION
* 18-Nov-2016			Sriram Sridharan		Defect#789 
* 11-Jan-2017			Manish					TENJINCG-33
* 24-Jul-2017			Sriram					TENJINCG-284
* 27-Jul-2017			Sriram Sridharan		TENJINCG-311
* 19-08-2017         	Padmavathi              T25IT-172
* 30-01-2018			Preeti					TENJINCG-503
* 14-03-2018        	Padmavathi              TENJINCG-612
* 20-03-2018        	Padmavathi              TENJINCG-612
* 09-05-2018            Padmavathi              TENJINCG-645
* 15-05-2018			Padmavathi		        TENJINCG-647 
* 18-05-2018			Padmavathi		        TENJINCG-647 
* 26-09-2018            Padmavathi              TENJINCG-741
* 20-12-2018            Padmavathi              TENJINCG-911
* 04-01-2019            Padmavathi              TJN252-62 
* 18-02-2019			Preeti					TENJINCG-969
* 15-06-2021			Ashiki					TENJINCG-1275
*/

package com.ycs.tenjin.project;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.ycs.tenjin.ValidationDetails;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.HighLevelStepResult;
import com.ycs.tenjin.bridge.pojo.run.ResultValidation;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.rest.SkipSerialization;
import com.ycs.tenjin.util.Utilities;


public class TestStep {
	private int recordId;
	private int appId;
	private int testCaseRecordId;
	private HighLevelStepResult highLevelResult;
	private String shortDescription;
	private String dataId;
	private String type;
	private String description;
	private String expectedResult;
	private String actualResult;
	private String moduleCode;
	private String transactionContext;
	private String executionContext;
	private String txnMode;	/*20-Oct-2014 WS*/
	private HighLevelStepResult resultSummary;
	/* Added by Padmavathi for TENJINCG-645 & TENJINCG-647 Starts*/
	private String customRules;
	private int dependentStepRecId;
	private String latestDependentStepStatus;
	private Map<String,String> mediaType;
	/* Added by Padmavathi for TENJINCG-645 &TENJINCG-647 ends*/
	@SkipSerialization
	private ArrayList<StepIterationResult> detailedResults;
	/*Added By padmavathi for T25IT-172-starts*/
	@SkipSerialization
	/*Added By padmavathi for T25IT-172-ends*/
	private ArrayList<ResultValidation> validations;
	private String id;
	private String appName;
	private String parentTestCaseId;
	private int uploadRow;
	private String testCaseName;
	private int rowsToExecute;
	
	private String tstepStorage;
	/*Added by Preeti for TENJINCG-969 starts*/
	private String createdBy;
	private Timestamp createdOn;
	private AuditRecord auditRecord;
	/*Added by Ashiki for TENJINCG-1275 starts*/
	private String fileName;
	private String filePath;
	/*Added by Ashiki for TENJINCG-1275 ends*/
	/*Added by Prem for TENJINCG-1181 start*/
	private ArrayList<ValidationDetails> validationdetails;
	
	
	public ArrayList<ValidationDetails> getValidationdetails() {
		return validationdetails;
	}
	public void setValidationdetails(ArrayList<ValidationDetails> validationdetails) {
		this.validationdetails = validationdetails;
	}
	/*Added by Prem for TENJINCG-1181 ends*/
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public AuditRecord getAuditRecord() {
		return auditRecord;
	}
	public void setAuditRecord(AuditRecord auditRecord) {
		this.auditRecord = auditRecord;
	}
	/*Added by Preeti for TENJINCG-969 ends*/
	/*Added by Padmavathi for TENJINCG-911 starts*/
	private String rerunStep;
	
	public String getRerunStep() {
		return rerunStep;
	}
	public void setRerunStep(String rerunStep) {
		this.rerunStep = rerunStep;
	}
	/*Added by Padmavathi for TENJINCG-911 ends*/
	public String getTstepStorage() {
		return tstepStorage;
	}
	public void setTstepStorage(String tstepStorage) {
		this.tstepStorage = tstepStorage;
	}
	private int responseType;
	

	public int getResponseType() {
		return responseType;
	}
	public void setResponseType(int responseType) {
		this.responseType = responseType;
	}
	public String getTestCaseName() {
		return testCaseName;
	}
	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}
	public int getRowsToExecute() {
		return rowsToExecute;
	}
	public void setRowsToExecute(int rowsToExecute) {
		this.rowsToExecute = rowsToExecute;
	}
	public int getUploadRow() {
		return uploadRow;
	}
	public void setUploadRow(int uploadRow) {
		this.uploadRow = uploadRow;
	}
	public String getParentTestCaseId() {
		return parentTestCaseId;
	}
	public void setParentTestCaseId(String parentTestCaseId) {
		this.parentTestCaseId = parentTestCaseId;
	}
	private int totalTransactions;
	private int totalExecutedTxns;
	private int totalPassedTxns;
	private int totalFailedTxns;
	private int totalErroredTxns;
	private String status;
	
	
	
	public int getTotalTransactions() {
		return totalTransactions;
	}
	/*	Added by Padmavathi for TENJINCG-647 (Dependency Rules) starts*/
	public int getDependentStepRecId() {
		return dependentStepRecId;
	}
	public void setDependentStepRecId(int dependentStepRecId) {
		this.dependentStepRecId = dependentStepRecId;
	}
	public String getLatestDependentStepStatus() {
		return latestDependentStepStatus;
	}
	public void setLatestDependentStepStatus(String latestDependentStepStatus) {
		this.latestDependentStepStatus = latestDependentStepStatus;
	}
	/*	Added by Padmavathi for TENJINCG-647 (Dependency Rules) ends*/
	public void setTotalTransactions(int totalTransactions) {
		this.totalTransactions = totalTransactions;
	}
	public int getTotalExecutedTxns() {
		return totalExecutedTxns;
	}
	public void setTotalExecutedTxns(int totalExecutedTxns) {
		this.totalExecutedTxns = totalExecutedTxns;
	}
	public int getTotalPassedTxns() {
		return totalPassedTxns;
	}
	public void setTotalPassedTxns(int totalPassedTxns) {
		this.totalPassedTxns = totalPassedTxns;
	}
	public int getTotalFailedTxns() {
		return totalFailedTxns;
	}
	public void setTotalFailedTxns(int totalFailedTxns) {
		this.totalFailedTxns = totalFailedTxns;
	}
	public int getTotalErroredTxns() {
		return totalErroredTxns;
	}
	public void setTotalErroredTxns(int totalErroredTxns) {
		this.totalErroredTxns = totalErroredTxns;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	private String autLoginType;
	public String getAutLoginType() {
		return autLoginType;
	}
	public void setAutLoginType(String autLoginType) {
		this.autLoginType = autLoginType;
	}
	private String operation;
	
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getTxnMode() {
		return txnMode;
	}
	public void setTxnMode(String m) {
		this.txnMode = m == null ? "GUI" : m;
	}
	
	
	public ArrayList<ResultValidation> getValidations() {
		return validations;
	}
	public void setValidations(ArrayList<ResultValidation> validations) {
		this.validations = validations;
	}
	public HighLevelStepResult getResultSummary() {
		return resultSummary;
	}
	public void setResultSummary(HighLevelStepResult resultSummary) {
		this.resultSummary = resultSummary;
	}
	public ArrayList<StepIterationResult> getDetailedResults() {
		return detailedResults;
	}
	public void setDetailedResults(ArrayList<StepIterationResult> detailedResults) {
		this.detailedResults = detailedResults;
	}
	public int getRecordId() {
		return recordId;
	}
	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	public int getTestCaseRecordId() {
		return testCaseRecordId;
	}
	public void setTestCaseRecordId(int testCaseRecordId) {
		this.testCaseRecordId = testCaseRecordId;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getDataId() {
		return dataId;
	}
	public void setDataId(String dataId) {
		this.dataId = dataId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getExpectedResult() {
		return expectedResult;
	}
	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}
	public String getActualResult() {
		return actualResult;
	}
	public void setActualResult(String actualResult) {
		this.actualResult = actualResult;
	}
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getTransactionContext() {
		return transactionContext;
	}
	public void setTransactionContext(String transactionContext) {
		this.transactionContext = transactionContext;
	}
	public String getExecutionContext() {
		return executionContext;
	}
	public void setExecutionContext(String executionContext) {
		this.executionContext = executionContext;
	}
	public HighLevelStepResult getHighLevelResult() {
		return highLevelResult;
	}
	public void setHighLevelResult(HighLevelStepResult highLevelResult) {
		this.highLevelResult = highLevelResult;
	}
	
	private String moduleName;
	
	
	
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public ExecutionStep getExecutionStep(){
		
		ExecutionStep step = new ExecutionStep();
		step.setRecordId(this.recordId);
		step.setAppId(this.getAppId());
		step.setAppName(this.appName);
		step.setFunctionCode(this.moduleCode);
		step.setTdGid(this.dataId);
		step.setTransactionLimit(this.totalTransactions);
		step.setOperation(this.operation);
		step.setId(this.id);
		step.setAutLoginType(this.autLoginType);
		step.setType(this.type);
		step.setExpectedResult(this.expectedResult);
		step.setValidationType(this.validationType);
		step.setResponseType(this.responseType);
		/*Added by Prem for TENJINCG-1181 Start*/
		step.setValidationdetails(this.validationdetails);
		/*Added by Prem for TENJINCG-1181 ends*/
		
		/***
         * Changed for TENJINCG-1175 (TENJINCG-1178) Avinash
         */
        step.setTxnMode(this.txnMode);
        /***
         * Changed for TENJINCG-1175 (TENJINCG-1178) Avinash ends
         */
         /*Added by Ashiki for TENJINCG-1275 starts*/
        step.setFileName(this.fileName);
        /*Added by Ashiki for TENJINCG-1275 ends*/
		return step;
	}
	/* Added by Padmavathi for TENJINCG-645 Starts*/
	public String getCustomRules() {
		return customRules;
	}
	public void setCustomRules(String customRules) {
		this.customRules = customRules;
	}
	/* Added by Padmavathi for TENJINCG-645 ends*/
	private int sequence;

	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	
	private String validationType;

	public String getValidationType() {
		return validationType;
	}
	public void setValidationType(String validationType) {
		this.validationType = validationType;
	}
	
	/*Added by Padmavathi for TENJINCG-612 starts*/
	public void merge(TestStep testStep){
		if(testStep.getAppId()==0&&testStep.getAppName()==null){
			this.appId=testStep.getAppId()!=0?testStep.getAppId():this.appId;
		}else{
			this.appId=testStep.getAppId();
			this.appName=null;
		}
		if(testStep.getAppName()!=null){
			this.appName=testStep.getAppName()!=null?Utilities.trim(testStep.getAppName()): this.appName;
		}
		this.id=testStep.getId()!=null?Utilities.trim(testStep.getId()):this.id;
		this.validationType=testStep.getValidationType()!=null?Utilities.trim(testStep.getValidationType()):this.validationType;
		this.shortDescription=testStep.getShortDescription()!=null?Utilities.trim(testStep.getShortDescription()):this.shortDescription;
		this.moduleCode=testStep.getModuleCode()!=null?Utilities.trim(testStep.getModuleCode()):this.moduleCode;
		this.operation=testStep.getOperation()!=null?Utilities.trim(testStep.getOperation()):this.operation;
		this.autLoginType=testStep.getAutLoginType()!=null?Utilities.trim(testStep.getAutLoginType()):this.autLoginType;
		this.responseType=testStep.getResponseType()!=0?testStep.getResponseType():this.responseType;
		this.type=testStep.getType()!=null?Utilities.trim(testStep.getType()):this.type;
		this.dataId=testStep.getDataId()!=null?Utilities.trim(testStep.getDataId()):this.dataId;
		this.rowsToExecute=testStep.getRowsToExecute()!=0?testStep.getRowsToExecute():this.rowsToExecute;
		this.description=testStep.getDescription()!=null?Utilities.trim(testStep.getDescription()):this.description;
		this.expectedResult=testStep.getExpectedResult()!=null?testStep.getExpectedResult():this.expectedResult;
		/* Added by Padmavathi for TENJINCG-645 Starts*/
		this.customRules=testStep.getCustomRules()!=null?testStep.getCustomRules():this.customRules;
		/* Added by Padmavathi for TENJINCG-645 ends*/
		/*Added by Padmavathi for TENJINCG-911 starts*/
		this.rerunStep=testStep.getRerunStep()!=null?testStep.getRerunStep():this.rerunStep;
		/*Added by Padmavathi for TENJINCG-911 ends*/
		/*Added by Ashiki for TENJINCG-1275 starts*/
		this.fileName=testStep.getFileName()!=null?testStep.getFileName():this.fileName;
		this.filePath=testStep.getFilePath()!=null?testStep.getFilePath():this.filePath;
		/*Added by Ashiki for TENJINCG-1275 ends*/
	}
	/*Added by Padmavathi for TENJINCG-612 ends*/
	
	/*Added by Padmavathi for TENJINCG-647 starts(for sorting sequence number)*/
	 public static Comparator<TestStep> sequenceNo = new Comparator<TestStep>() {

			public int compare(TestStep step, TestStep step1) {

			   int seq1 = step.getSequence();
			   int seq2 = step1.getSequence();
			   return seq1-seq2;

		   }};
	 /*Added by Padmavathi for TENJINCG-647 ends*/
		   
	/*Added By Padmavathi for TENJINCG-741 starts*/
	public void merge(TestStep testStep,boolean uploadingFromExcel){
		
		this.appId=testStep.getAppId()==0?this.appId:testStep.getAppId();
		this.appName=(testStep.getAppName()==null||testStep.getAppName().equals(""))? this.appName: Utilities.trim(testStep.getAppName());
		this.id=(testStep.getId()==null||testStep.getId().equals(""))? this.id: Utilities.trim(testStep.getId());
		this.validationType=(testStep.getValidationType()==null||testStep.getValidationType().equals(""))? this.validationType: Utilities.trim(testStep.getValidationType());
		this.shortDescription=(testStep.getShortDescription()==null||testStep.getShortDescription().equals(""))? this.shortDescription: Utilities.trim(testStep.getShortDescription());
		this.moduleCode=(testStep.getModuleCode()==null||testStep.getModuleCode().equals(""))? this.moduleCode: Utilities.trim(testStep.getModuleCode());
		this.operation=(testStep.getOperation()==null||testStep.getOperation().equals(""))? this.operation: Utilities.trim(testStep.getOperation());
		this.autLoginType=(testStep.getAutLoginType()==null||testStep.getAutLoginType().equals(""))? this.autLoginType: Utilities.trim(testStep.getAutLoginType());
		this.type=(testStep.getType()==null||testStep.getType().equals(""))? this.type: Utilities.trim(testStep.getType());
		this.dataId=(testStep.getDataId()==null||testStep.getDataId().equals(""))? this.dataId: Utilities.trim(testStep.getDataId());
		this.description=(testStep.getDescription()==null||testStep.getDescription().equals(""))? this.description: Utilities.trim(testStep.getDescription());
		this.expectedResult=(testStep.getExpectedResult()==null||testStep.getExpectedResult().equals(""))? this.expectedResult: Utilities.trim(testStep.getExpectedResult());
		this.customRules=(testStep.getCustomRules()==null||testStep.getCustomRules().equals(""))? this.customRules: Utilities.trim(testStep.getCustomRules());
		this.responseType=testStep.getResponseType()==0?this.responseType:testStep.getResponseType();
		this.rowsToExecute=testStep.getRowsToExecute()==0?this.rowsToExecute:testStep.getRowsToExecute();
		/*Added by Padmavathi for TJN252-62 starts*/
		this.rerunStep=(testStep.getRerunStep()==null||testStep.getRerunStep().equals(""))?this.rerunStep:testStep.getRerunStep();
		/*Added by Padmavathi for TJN252-62 ends*/
		/*Added by Ashiki for TENJINCG-1275 starts*/
		this.fileName=(testStep.getFileName()==null||testStep.getFileName().equals(""))? this.fileName: Utilities.trim(testStep.getFileName());
		/*Added by Ashiki for TENJINCG-1275 ends*/
	}
	/*Added By Padmavathi for TENJINCG-741 ends*/
	/*Added by Ashiki for TENJINCG-1275 starts*/
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	/*Added by Ashiki for TENJINCG-1275 ends*/
	public Map<String, String> getMediaType() {
		return mediaType;
	}
	public void setMediaType(Map<String, String> mediaType) {
		this.mediaType = mediaType;
	}
	

}
