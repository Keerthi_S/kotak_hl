/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestSet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 13-04-2018			   Pushpalatha G		   TenjinCG-636
 27-09-2018			   Pushpa				   TENJINCG-740
 18-02-2019            Padmavathi              TENJINCG-973   
*18-02-2019				Preeti					TENJINCG-969
*/

package com.ycs.tenjin.project;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.ycs.tenjin.util.Utilities;

public class TestSet {
	private int id;
	private String type;
	private String name;
	private int parent;
	
	private Timestamp createdOn;
	private String createdBy;
	private Date modifiedOn;
	private String modifiedBy;
	private String status;
	private String recordType;
	private String priority;
	private ArrayList<TestCase> tests;
	private String reviewed;
	private String description;
	private int project;
	/*Added by Preeti for TENJINCG-969 starts*/
	private AuditRecord auditRecord;
	public AuditRecord getAuditRecord() {
		return auditRecord;
	}
	public void setAuditRecord(AuditRecord auditRecord) {
		this.auditRecord = auditRecord;
	}
	/*Added by Preeti for TENJINCG-969 ends*/
	/*Added by Padmavathi for TENJINCG-973 starts*/
	private int lastRunId;
	private String lastRunStatus;
	
	public int getLastRunId() {
		return lastRunId;
	}
	public void setLastRunId(int lastRunId) {
		this.lastRunId = lastRunId;
	}
	public String getLastRunStatus() {
		return lastRunStatus;
	}
	public void setLastRunStatus(String lastRunStatus) {
		this.lastRunStatus = lastRunStatus;
	}
	/*Added by Padmavathi for TENJINCG-973 ends*/

	private String mode;
	/*Added by Pushpa for TENJINCG-740 starts*/
	private int uploadRow;
	
	public int getUploadRow() {
		return uploadRow;
	}
	public void setUploadRow(int uploadRow) {
		this.uploadRow = uploadRow;
	}
	/*Added by Pushpa for TENJINCG-740 Ends*/
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	
	public int getProject() {
		return project;
	}
	public void setProject(int project) {
		this.project = project;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getReviewed() {
		return reviewed;
	}
	public void setReviewed(String reviewed) {
		this.reviewed = reviewed;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		
		this.name = Utilities.trim(name);
		
	}
	public int getParent() {
		return parent;
	}
	public void setParent(int parent) {
		this.parent = parent;
	}
	
	
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList<TestCase> getTests() {
		return tests;
	}
	public void setTests(ArrayList<TestCase> tests) {
		this.tests = tests;
	}
	
	
	public void merge(TestSet newTestSet) {
		this.name =newTestSet.getName()!=null ? Utilities.trim(newTestSet.getName()) : this.name;
		this.description = newTestSet.getDescription()!=null? Utilities.trim(newTestSet.getDescription()) : this.description;
		this.priority = newTestSet.getPriority()!=null ? Utilities.trim(newTestSet.getPriority()) : this.priority;
		this.type = newTestSet.getType()!=null? Utilities.trim(newTestSet.getType()) : this.type;
		
	}
	
}
