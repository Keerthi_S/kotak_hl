/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCase.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 09-05-2018           Padmavathi              TENJINCG-646
* 27-09-2018           Padmavathi              TENJINCG-741 
* 27-09-2018 		   Pushpa				   TENJINCG-740
* 18-02-2019			Preeti					TENJINCG-969
* 28-02-2019           Padmavathi              TENJINCG-942
* 
*/

package com.ycs.tenjin.project;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.ycs.tenjin.bridge.pojo.run.TestCaseResult;
import com.ycs.tenjin.util.Utilities;


public class TestCase {

	int tcRecId;
	String tcId;
	int tcFolderId;
	String tcFolderName;
	String tcName;
	String tcDesc;
	int tcTsetId;
	String tcType;
	String tcPriority;
	String tcReviewed;
	String tcStatus;
	/*Added by Poojalakshmi for TJN27-94 starts */
	String tcAppName;
	/*Added by Poojalakshmi for TJN27-94 ends */
	
	Timestamp tcCreatedOn;
	/*Added by Poojalakshmi for TJN27-94 ends */
	public String getTcAppName() {
		return tcAppName;
	}
	public void setTcAppName(String tcAppName) {
		this.tcAppName = tcAppName;
	}
	/*Added by Poojalakshmi for TJN27-94 ends */
	Date tcModifiedOn;
	String tcCreatedBy;
	String tcModifiedBy;
	int projectId;
	String recordType;
	private ArrayList<TestStep> tcSteps;
	private TestCaseResult result;
	private int uploadRow;
	private Collection<TestStep> stepCollection;
	/*Added By Padmavathi for TENJINCG-646 starts*/
	private String tcPresetRules;
	/*Added By Padmavathi for TENJINCG-646 ends*/
	private String storage;
	/*Added by Pushpa for TENJINCG-740 Starts*/
	private String tcTsetMode;
	/*Added by Ashiki for TENJINCG-1214 starts*/
	private String label;
	/*Added by Ashiki for TENJINCG-1214 ends*/
	public String getTcTsetMode() {
		return tcTsetMode;
	}
	public void setTcTsetMode(String tcTsetMode) {
		this.tcTsetMode = tcTsetMode;
	}
	/*Added by Pushpa for TENJINCG-740 Ends*/
	/*Added by Preeti for TENJINCG-969 starts*/
	private AuditRecord auditRecord;
	public AuditRecord getAuditRecord() {
		return auditRecord;
	}
	public void setAuditRecord(AuditRecord auditRecord) {
		this.auditRecord = auditRecord;
	}
	/*Added by Preeti for TENJINCG-969 ends*/
	
	/*Added By Padmavathi for TENJINCG-942 starts*/
	private int appId;
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	/*Added By Padmavathi for TENJINCG-942 ends*/
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	private String mode;
	
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public Collection<TestStep> getStepCollection() {
		return stepCollection;
	}
	public void setStepCollection(Collection<TestStep> stepCollection) {
		this.stepCollection = stepCollection;
	}
	public int getUploadRow() {
		return uploadRow;
	}
	public void setUploadRow(int uploadRow) {
		this.uploadRow = uploadRow;
	}
	
	private int totalSteps;
	private int totalPassedSteps;
	private int totalFailedSteps;
	private int totalExecutedSteps;
	private int totalErrorredSteps;
	
	
	
	
	public int getTotalSteps() {
		return totalSteps;
	}
	public void setTotalSteps(int totalSteps) {
		this.totalSteps = totalSteps;
	}
	public int getTotalPassedSteps() {
		return totalPassedSteps;
	}
	public void setTotalPassedSteps(int totalPassedSteps) {
		this.totalPassedSteps = totalPassedSteps;
	}
	public int getTotalFailedSteps() {
		return totalFailedSteps;
	}
	public void setTotalFailedSteps(int totalFailedSteps) {
		this.totalFailedSteps = totalFailedSteps;
	}
	public int getTotalExecutedSteps() {
		return totalExecutedSteps;
	}
	public void setTotalExecutedSteps(int totalExecutedSteps) {
		this.totalExecutedSteps = totalExecutedSteps;
	}
	public int getTotalErrorredSteps() {
		return totalErrorredSteps;
	}
	public void setTotalErrorredSteps(int totalErrorredSteps) {
		this.totalErrorredSteps = totalErrorredSteps;
	}
	
	public TestCaseResult getResult() {
		return result;
	}
	public void setResult(TestCaseResult result) {
		this.result = result;
	}
	public ArrayList<TestStep> getTcSteps() {
		return tcSteps;
	}
	public void setTcSteps(ArrayList<TestStep> tcSteps) {
		this.tcSteps = tcSteps;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getTcCreatedBy() {
		return tcCreatedBy;
	}
	public void setTcCreatedBy(String tcCreatedBy) {
		this.tcCreatedBy = tcCreatedBy;
	}
	public String getTcModifiedBy() {
		return tcModifiedBy;
	}
	public void setTcModifiedBy(String tcModifiedBy) {
		this.tcModifiedBy = tcModifiedBy;
	}
	public int getTcRecId() {
		return tcRecId;
	}
	public void setTcRecId(int tcRecId) {
		this.tcRecId = tcRecId;
	}
	public String getTcId() {
		return tcId;
	}
	public void setTcId(String tcId) {
		this.tcId = tcId;
	}

	public int getTcFolderId() {
		return tcFolderId;
	}
	public void setTcFolderId(int tcFolderId) {
		this.tcFolderId = tcFolderId;
	}
	public String getTcFolderName() {
		return tcFolderName;
	}
	public void setTcFolderName(String tcFolderName) {
		this.tcFolderName = tcFolderName;
	}

	public String getTcName() {
		return tcName;
	}
	public void setTcName(String tcName) {
		this.tcName = tcName;
	}
	public String getTcDesc() {
		return tcDesc;
	}
	public void setTcDesc(String tcDesc) {
		this.tcDesc = tcDesc;
	}
	public int getTcTsetId() {
		return tcTsetId;
	}
	public void setTcTsetId(int tcTsetId) {
		this.tcTsetId = tcTsetId;
	}
	public String getTcType() {
		return tcType;
	}
	public void setTcType(String tcType) {
		this.tcType = tcType;
	}
	public String getTcPriority() {
		return tcPriority;
	}
	public void setTcPriority(String tcPriority) {
		this.tcPriority = tcPriority;
	}
		/*Added By Padmavathi for TENJINCG-646 starts*/
	public String getTcPresetRules() {
		return tcPresetRules;
	}
	public void setTcPresetRules(String tcPresetRules) {
		this.tcPresetRules = tcPresetRules;
	}
		/*Added By Padmavathi for TENJINCG-646 ends*/
	public String getTcReviewed() {
		return tcReviewed;
	}
	public void setTcReviewed(String tcReviewed) {
		this.tcReviewed = tcReviewed;
	}
	public String getTcStatus() {
		return tcStatus;
	}
	public void setTcStatus(String tcStatus) {
		this.tcStatus = tcStatus;
	}
	
	
	public Timestamp getTcCreatedOn() {
		return tcCreatedOn;
	}
	public void setTcCreatedOn(Timestamp tcCreatedOn) {
		this.tcCreatedOn = tcCreatedOn;
	}
	
	public Date getTcModifiedOn() {
		return tcModifiedOn;
	}
	public void setTcModifiedOn(Date tcModifiedOn) {
		this.tcModifiedOn = tcModifiedOn;
	}
	
	private String tcTestSetName;

	public String getTcTestSetName() {
		return tcTestSetName;
	}
	public void setTcTestSetName(String tcTestSetName) {
		this.tcTestSetName = tcTestSetName;
	}
	
	
	public void merge(TestCase testcase) {
		this.tcId=testcase.getTcId()!=null?Utilities.trim(testcase.getTcId()):this.tcId;
		this.tcName=testcase.getTcName()!=null?Utilities.trim(testcase.getTcName()):this.tcName;
		this.tcDesc=testcase.getTcDesc()!=null?Utilities.trim(testcase.getTcDesc()):this.tcDesc;
		this.tcType=testcase.getTcType()!=null?Utilities.trim(testcase.getTcType()):this.tcType;
		this.tcPriority=testcase.getTcPriority()!=null?Utilities.trim(testcase.getTcPriority()):this.tcPriority;
		this.tcCreatedBy=testcase.getTcCreatedBy()!=null?Utilities.trim(testcase.getTcCreatedBy()):this.tcCreatedBy;
		/*Added By Padmavathi for TENJINCG-646 starts*/
		this.tcPresetRules=testcase.getTcPresetRules()!=null?Utilities.trim(testcase.getTcPresetRules()):this.tcPresetRules;
		/*Added By Padmavathi for TENJINCG-646 ends*/
		this.appId=testcase.getAppId()==0?this.appId:testcase.getAppId();
		/*Added by Ashiki for TENJINCG-1214 starts*/
		this.label=testcase.getLabel()!=null?Utilities.trim(testcase.getLabel()):this.label; 
		/*Added by Ashiki for TENJINCG-1214 ends*/
	}
	/*Added By Padmavathi for TENJINCG-741 starts*/
	public void merge(TestCase tc, boolean uploadingFromExcel) {
		this.tcId=(tc.getTcId()==null ||tc.getTcId().equals(""))?this.tcId:Utilities.trim(tc.getTcId());
		this.tcName=(tc.getTcName()==null ||tc.getTcName().equals(""))?this.tcName:Utilities.trim(tc.getTcName());
		this.tcDesc=(tc.getTcDesc()==null ||tc.getTcDesc().equals(""))?this.tcDesc:Utilities.trim(tc.getTcDesc());
		this.tcType=(tc.getTcType()==null ||tc.getTcType().equals(""))?this.tcType:Utilities.trim(tc.getTcType());
		this.tcPriority=(tc.getTcPriority()==null ||tc.getTcPriority().equals(""))?this.tcPriority:Utilities.trim(tc.getTcPriority());
		this.tcCreatedBy=(tc.getTcCreatedBy()==null ||tc.getTcCreatedBy().equals(""))?this.tcCreatedBy:Utilities.trim(tc.getTcCreatedBy());
		this.tcPresetRules=(tc.getTcPresetRules()==null ||tc.getTcPresetRules().equals(""))?this.tcPresetRules:Utilities.trim(tc.getTcPresetRules());
		
	}
	/*Added By Padmavathi for TENJINCG-741 ends*/
	/*Added by Ashiki for TENJINCG-1214 starts*/
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	/*Added by Ashiki for TENJINCG-1214 ends*/
}
