/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DependencyRules.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 09-05-2018           Padmavathi              TENJINCG-645 & TENJINCG-646 
* 
* 
*/

package com.ycs.tenjin.project;

public class DependencyRules {
	
	private int ruleId;
	private int tcRecId;
	private int stepRecId;
	private int dependentStepRecId;
	private String execStatus;
	private String dependencyType;
	
	public int getRuleId() {
		return ruleId;
	}
	public void setRuleId(int ruleId) {
		this.ruleId = ruleId;
	}
	public int getTcRecId() {
		return tcRecId;
	}
	public void setTcRecId(int tcRecId) {
		this.tcRecId = tcRecId;
	}
	public int getStepRecId() {
		return stepRecId;
	}
	public void setStepRecId(int stepRecId) {
		this.stepRecId = stepRecId;
	}
	public int getDependentStepRecId() {
		return dependentStepRecId;
	}
	public void setDependentStepRecId(int dependentStepRecId) {
		this.dependentStepRecId = dependentStepRecId;
	}
	public String getExecStatus() {
		return execStatus;
	}
	public void setExecStatus(String execStatus) {
		this.execStatus = execStatus;
	}
	public String getDependencyType() {
		return dependencyType;
	}
	public void setDependencyType(String dependencyType) {
		this.dependencyType = dependencyType;
	}
	
}
