/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Project.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                	CHANGED BY              DESCRIPTION
* 27-09-16				manish					requirement TJN_24_06
* 30-jan-17				parveen					requirement TJNJINCG_55
* 30-jan-17				manish					requirement TJNJINCG_57,58,59
* 18-01-2018            Padmavathi              for TENJINCG-576
* 19-01-2018            Padmavathi              for TENJINCG-576
* 13-03-2018			Preeti					TENJINCG-615
* 02-05-2018			Preeti					TENJINCG-656
* 18-02-2019			Preeti					TENJINCG-969
* 28-02-2019			Pushpa					TENJINCG-980,981
* 15-03-2019			Ashiki				    TENJINCG-986
* 26-03-2019			Roshni					TJN252-54
* 03-06-2019            Leelaprasad             TENJINCG-1068,TENJINCG-1069
* 13-03-2020			Ashiki					TENJINCG-1123
* 11-12-2020			Pushpalatha				TENJINCG-1221
* 05-02-2021            Shruthi A N             TENJINCG-1255 
*/

package com.ycs.tenjin.project;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.testmanager.TestManagerInstance;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

public class Project {
	
	private int id;
	private String name;
	private String type;
	private String domain;
	private String dbServer;
	private ArrayList<User> users;
	private ArrayList<Aut> auts;
	private String owner;
	private Timestamp createdDate;
	private String description;
	private String screenshotoption;
	private String State;
	
	/*Added by Pushpa for TENJINCG-980,981 starts*/
	private Timestamp startDate;
	private Timestamp endDate;
	private String projectOwnerEmail;
	/*Added by Pushpalatha for TENJINCG-1221 starts*/
	private String dttInstance;
	/*added by shruthi for TENJINCG-1255 starts*/
	private String repoRoot;
	private String repoType;
	/*added by shruthi for TENJINCG-1255 ends*/
	
	public String getDttInstance() {
		return dttInstance;
	}
	public void setDttInstance(String dttInstance) {
		this.dttInstance = dttInstance;
	}
	private String repoOwner;
	
	
	public String getRepoOwner() {
		return repoOwner;
	}
	public void setRepoOwner(String repoOwner) {
		this.repoOwner = repoOwner;
	}
	/*Added by Pushpalatha for TENJINCG-1221 ends*/
	public Timestamp getStartDate() {
		return startDate;
	}
	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	public Timestamp getEndDate() {
		return endDate;
	}
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	/*Added by Pushpa for TENJINCG-980,981 ends*/
	private List<String> dttSeverities;
	private List<String> dttPriorities;
	
	
	private String dttEnable;
	private String dttKey;
	/*Modified by Preeti for TENJINCG-656 starts*/
	private ProjectMail projectMail;
	
	
	/*changed by manish for req TENJINCG-59 on 31-Jan-2017 starts*/
	private String tcFilter;
	private String tcFilterValue;
	private String tsFilter;
	private String tsFilterValue;
	private String etmInstance;
	private List<TestManagerInstance> tmInstances;
	private String createdBy;
	/*Added by Preeti for TENJINCG-969 starts*/
	private AuditRecord auditRecord;
	public AuditRecord getAuditRecord() {
		return auditRecord;
	}
	public void setAuditRecord(AuditRecord auditRecord) {
		this.auditRecord = auditRecord;
	}
	/*Added by Preeti for TENJINCG-969 ends*/
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public List<TestManagerInstance> getTmInstances() {
		return tmInstances;
	}
	public void setTmInstances(List<TestManagerInstance> tmInstances) {
		this.tmInstances = tmInstances;
	}
	public String getEtmInstance() {
		return etmInstance;
	}
	public void setEtmInstance(String etmInstance) {
		this.etmInstance = etmInstance;
	}
	public String getTcFilter() {
		return tcFilter;
	}
	public void setTcFilter(String tcFilter) {
		this.tcFilter = tcFilter;
	}
	public String getTcFilterValue() {
		return tcFilterValue;
	}
	public void setTcFilterValue(String tcFilterValue) {
		this.tcFilterValue = tcFilterValue;
	}
	public String getTsFilter() {
		return tsFilter;
	}
	public void setTsFilter(String tsFilter) {
		this.tsFilter = tsFilter;
	}
	public String getTsFilterValue() {
		return tsFilterValue;
	}
	public void setTsFilterValue(String tsFilterValue) {
		this.tsFilterValue = tsFilterValue;
	}
	
	/*changed by manish for req TENJINCG-59 on 31-Jan-2017 ends*/
	
	//added by parveen for the requirment #TENJINCG-55 starts
	private String etmTool;
	private String etmProject;
	private String etmEnable;


	public String getEtmTool() {
		return etmTool;
	}
	
	public void setEtmTool(String etmTool) {
		this.etmTool = etmTool;
	}
	public String getEtmProject() {
		return etmProject;
	}
	public void setEtmProject(String etmProject) {
		this.etmProject = etmProject;
	}
	public String getEtmEnable() {
		return etmEnable;
	}
	public void setEtmEnable(String etmEnable) {
		this.etmEnable = etmEnable;
	}
	//added by parveen for the requirment #TENJINCG-55 ends
	public String getDttEnable() {
		return dttEnable;
	}
	public void setDttEnable(String dttEnable) {
		this.dttEnable = dttEnable;
	}
	public ProjectMail getProjectMail() {
		return projectMail;
	}
	public void setProjectMail(ProjectMail projectMail) {
		this.projectMail = projectMail;
	}
	public String getDttKey() {
		return dttKey;
	}
	public void setDttKey(String dttKey) {
		this.dttKey = dttKey;
	}
	public List<String> getDttSeverities() {
		return dttSeverities;
	}
	public void setDttSeverities(List<String> dttSeverities) {
		this.dttSeverities = dttSeverities;
	}
	public List<String> getDttPriorities() {
		return dttPriorities;
	}
	public void setDttPriorities(List<String> dttPriorities) {
		this.dttPriorities = dttPriorities;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		this.State = state;
	}
	public String getScreenshotoption() {
		return screenshotoption;
	}
	public void setScreenshotoption(String screenshotoption) {
		this.screenshotoption = screenshotoption;
	}

	/******************************************************8
	 * Change by Sriram to add test data path attribute for project
	 */
	private ArrayList<ProjectTestDataPath> testDataPaths;
	
	
	public ArrayList<ProjectTestDataPath> getTestDataPaths() {
		return testDataPaths;
	}
	public void setTestDataPaths(ArrayList<ProjectTestDataPath> testDataPaths) {
		this.testDataPaths = testDataPaths;
	}
	/******************************************************8
	 * Change by Sriram to add test data path attribute for project
	 */
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the createdDate
	 */
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}
	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}
	/**
	 * @return the users
	 */
	public ArrayList<User> getUsers() {
		return users;
	}
	/**
	 * @param users the users to set
	 */
	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}
	/**
	 * @return the auts
	 */
	public ArrayList<Aut> getAuts() {
		return auts;
	}
	/**
	 * @param auts the auts to set
	 */
	public void setAuts(ArrayList<Aut> auts) {
		this.auts = auts;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}
	/**
	 * @param domain the domain to set
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}
	/**
	 * @return the dbServer
	 */
	public String getDbServer() {
		return dbServer;
	}
	/**
	 * @param dbServer the dbServer to set
	 */
	public void setDbServer(String dbServer) {
		this.dbServer = dbServer;
	}
	/*added by Roshni */
	private String tcDisplayValue;
	public String getTcDisplayValue() {
		return tcDisplayValue;
	}
	public void setTcDisplayValue(String tcDisplayValue) {
		this.tcDisplayValue = tcDisplayValue;
	}
	public String getTsDisplayValue() {
		return tsDisplayValue;
	}
	public void setTsDisplayValue(String tsDisplayValue) {
		this.tsDisplayValue = tsDisplayValue;
	}
	private String tsDisplayValue;
	
	public boolean hasAdminPrivileges(String userId){
		//StringBuilder sBuilder = new StringBuilder();
		boolean ret = false;
		
		if(this.users != null && this.users.size() > 0){
			for(User user:this.users){
				if(user.getId().equalsIgnoreCase(userId) && (user.getRoleInCurrentProject().equalsIgnoreCase("Owner") || user.getRoleInCurrentProject().equalsIgnoreCase("Project Manager")||  user.getRoleInCurrentProject().equalsIgnoreCase("Administrator"))){
					ret= true;
					break;
				}
			}
		}
		
		return ret;
	}
	
	/******************************
	 *  added by manish for requirement TJN_24_06 on 27-09-16
	 * @param tool
	 * @return
	 * @throws DatabaseException
	 */
	
	private String tool;
	private String defProject;
	
	
	
	public String getTool() {
		return tool;
	}
	public void setTool(String tool) {
		this.tool = tool;
	}
	public String getDefProject() {
		return defProject;
	}
	public void setDefProject(String defProject) {
		this.defProject = defProject;
	}
	
	/******************************
	 *  added by manish for requirement TJN_24_06 on 27-09-16 ends
	 * @param tool
	 * @return
	 * @throws DatabaseException
	 */
	
	/******************************
	 *  added by manish for requirement TJN_24_06 on 27-09-16
	 * @param tool
	 * @return
	 * @throws DatabaseException
	 */
	
	private String instance;
	//private String defProject;
	
	
	
	public String getInstance() {
		return instance;
	}
	public void setInstance(String instance) {
		this.instance = instance;
	}
	
	/******************************
	 *  added by manish for requirement TJN_24_06 on 27-09-16 ends
	 * @param tool
	 * @return
	 * @throws DatabaseException
	 */
	
	/* Added By Ashiki TENJINCG-986 starts */
	private String subSet;
	
	
	public String getSubSet() {
		return subSet;
	}
	public void setSubSet(String subSet) {
		this.subSet = subSet;
	} 
	
	/* Added By Ashiki TENJINCG-986 ends */
	public void merge(Project project){
		this.name = project.getName()!=null ? Utilities.trim(project.getName()): this.name;
		this.type = project.getType()!=null ? Utilities.trim(project.getType()): this.type;
		this.description = project.getDescription()!=null ? Utilities.trim(project.getDescription()): this.description;
		this.screenshotoption = project.getScreenshotoption()!=null ? Utilities.trim(project.getScreenshotoption()): this.screenshotoption;
		this.projectOwnerEmail=project.getProjectOwnerEmail()!=null? Utilities.trim(project.getProjectOwnerEmail()):this.projectOwnerEmail;
		this.endDate=project.getEndDate()!=null?project.getEndDate():this.endDate;
		
	}
	public String getProjectOwnerEmail() {
		return projectOwnerEmail;
	}
	public void setProjectOwnerEmail(String projectOwnerEmail) {
		this.projectOwnerEmail = projectOwnerEmail;
	}
	
	/*Added by Ashiki for TENJINCG-1123 starts*/
	private String userRole;
	
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	/*Added by Ashiki for TENJINCG-1123 ends*/
	/*added by shruthi for TENJINCG-1255 starts*/
	public String getRepoType() {
		return repoType;
	}
	public void setRepoType(String repoType) {
		this.repoType = repoType;
	}
	public String getRepoRoot() {
		return repoRoot;
	}
	public void setRepoRoot(String repoRoot) {
		this.repoRoot = repoRoot;
	}
	/*added by shruthi for TENJINCG-1255 ends*/
	
	private String accessKey;
	
	private String AccessSecKey;

	public String getAccessKey() {
		return accessKey;
	}
	public String setAccessKey(String accessKey) {
		return this.accessKey = accessKey;
	}
	public String getAccessSecKey() {
		return AccessSecKey;
	}
	public String setAccessSecKey(String accessSecKey) {
		return AccessSecKey = accessSecKey;
	}
	
	
	
}
