/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectMail.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 * DATE                 CHANGED BY              DESCRIPTION
 * 02-05-2018			Preeti					Newly added for TENJINCG-656
 * 10-05-2018			Pushpalatha				TENJINCG-629
 * 12-03-2020			Lokanath				TENJINCG-1185
 * 13-03-2020           Jagadish                TENJINCG-1184
*/

package com.ycs.tenjin.project;

import java.sql.Timestamp;

public class ProjectMail {
	private int prjId;
	private String prjMailNotification;
	private String prjMailSubject;
	private String prjMailEscalation;
	private String prjConsolidateMail;
	private Timestamp prjConsolidateMailTime;
	/*Changed by Ashiki for TENJINCG-1051 starts*/
	/*private int mailEscalationRuleId;*/
	private String mailEscalationRuleId;
	/*Changed by Ashiki for TENJINCG-1051 ends*/
	private String prjEscalationMails;
	/*Added by Pushpalatha for TENJINCG-629 Strats*/
	private String prjEscalationRuleDesc;
	/*Added by lokanath for TENJINCG-1185 starts*/
	private String prjAddAdditionalMails;
	/*Added by Jagadish for TENJINCG-1184 Strats*/
    public String getPrjMailtype() {
		return prjMailtype;
	}
	public void setPrjMailtype(String prjMailtype) {
		this.prjMailtype = prjMailtype;
	}
	private String prjMailtype;
	/*Added by Jagadish for TENJINCG-1184 ends*/
	public String getPrjAddAdditionalMails() {
		return prjAddAdditionalMails;
	}
	public void setPrjAddAdditionalMails(String prjAddAdditionalMails) {
		this.prjAddAdditionalMails = prjAddAdditionalMails;
	}
	/*Added by lokanath for TENJINCG-1185 ends*/
	public String getPrjEscalationRuleDesc() {
		return prjEscalationRuleDesc;
	}
	public void setPrjEscalationRuleDesc(String prjEscalationRuleDesc) {
		this.prjEscalationRuleDesc = prjEscalationRuleDesc;
	}
	/*Added by Pushpalatha for TENJINCG-629 Ends*/
	public int getPrjId() {
		return prjId;
	}
	public void setPrjId(int prjId) {
		this.prjId = prjId;
	}
	public String getPrjMailNotification() {
		return prjMailNotification;
	}
	public void setPrjMailNotification(String prjMailNotification) {
		this.prjMailNotification = prjMailNotification;
	}
	public String getPrjMailSubject() {
		return prjMailSubject;
	}
	public void setPrjMailSubject(String prjMailSubject) {
		this.prjMailSubject = prjMailSubject;
	}
	public String getPrjMailEscalation() {
		return prjMailEscalation;
	}
	public void setPrjMailEscalation(String prjMailEscalation) {
		this.prjMailEscalation = prjMailEscalation;
	}
	public String getPrjConsolidateMail() {
		return prjConsolidateMail;
	}
	public void setPrjConsolidateMail(String prjConsolidateMail) {
		this.prjConsolidateMail = prjConsolidateMail;
	}
	public Timestamp getPrjConsolidateMailTime() {
		return prjConsolidateMailTime;
	}
	public void setPrjConsolidateMailTime(Timestamp prjConsolidateMailTime) {
		this.prjConsolidateMailTime = prjConsolidateMailTime;
	}
	public String getMailEscalationRuleId() {
		return mailEscalationRuleId;
	}
	public void setMailEscalationRuleId(String mailEscalationRuleId) {
		this.mailEscalationRuleId = mailEscalationRuleId;
	}
	public String getPrjEscalationMails() {
		return prjEscalationMails;
	}
	public void setPrjEscalationMails(String prjEscalationMails) {
		this.prjEscalationMails = prjEscalationMails;
	}
	
	
}
