/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutionScheme.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

package com.ycs.tenjin.project;

import java.sql.Timestamp;
import java.util.ArrayList;

import com.ycs.tenjin.bridge.pojo.aut.TestObject;
public class ExecutionScheme {
	private int id;
	private int appId;
	private String functionCode;
	private String name;
	private String description;
	private String basePageArea;
	private ArrayList<TestObject> preActions;
	private ArrayList<TestObject> actions;
	private ArrayList<TestObject> postActions;
	private Timestamp createdTimeStamp;
	private String createdBy;
	private boolean enabled;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	public String getFunctionCode() {
		return functionCode;
	}
	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBasePageArea() {
		return basePageArea;
	}
	public void setBasePageArea(String basePageArea) {
		this.basePageArea = basePageArea;
	}
	public ArrayList<TestObject> getPreActions() {
		return preActions;
	}
	public void setPreActions(ArrayList<TestObject> preActions) {
		this.preActions = preActions;
	}
	public ArrayList<TestObject> getActions() {
		return actions;
	}
	public void setActions(ArrayList<TestObject> actions) {
		this.actions = actions;
	}
	public ArrayList<TestObject> getPostActions() {
		return postActions;
	}
	public void setPostActions(ArrayList<TestObject> postActions) {
		this.postActions = postActions;
	}
	public Timestamp getCreatedTimeStamp() {
		return createdTimeStamp;
	}
	public void setCreatedTimeStamp(Timestamp createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	
	
	
}
