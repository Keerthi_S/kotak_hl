/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AutHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION

 *30-07-2018			Preeti					Closed connections
 *26-Nov 2018			Shivam	Sharma			TENJINCG-904
 *22-03-2019			Preeti					TENJINCG-1018
 *15-04-2019			Ashiki	 				TJN252-97
 *24-04-2019			Roshni					TENJINCG-1038
 *05-02-2020			Roshni					TENJINCG-1168
 *31-03-2021            Paneendra               TENJINCG-1267
 */

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.aut.Group;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;


public class AutHelper {
	private static final Logger logger = LoggerFactory.getLogger(AutHelper.class);

	
	/*modified by shruthi for VAPT helper fixes starts*/
	public Aut hydrateAut(int appId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		Aut aut = null;
		try {
		
			pst = conn.prepareStatement("SELECT * FROM MASAPPLICATION M left outer join TJN_ADAPTER A  on M.APP_ADAPTER_PACKAGE = A.NAME WHERE APP_ID =?");
			pst.setInt(1, appId);
			rs = pst.executeQuery();

			while (rs.next()) {
				aut = new Aut();
				aut.setId(rs.getInt("APP_ID"));
				aut.setName(rs.getString("APP_NAME"));
				aut.setURL(rs.getString("APP_URL"));
				aut.setLoginReqd(rs.getString("APP_LOGIN_REQD"));
				aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
				aut.setPassword(rs.getString("APP_PASSWORD"));
				/* changed by sahana for TJN_23_09 requirement on 22-7-2016-->Starts */
				aut.setBrowser(rs.getString("APP_DEF_BROWSER"));
				/* changed by sahana for TJN_23_09 requirement on 25-7-2016-->Ends */
				/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 starts */
				aut.setDateFormat(rs.getString("APP_DATE_FORMAT"));
				/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 ends */
				/********************************************************
				 * Added by Sriram for Req#TJN_23_17 18-Aug-2016
				 */
				aut.setAdapterPackage(rs.getString("APP_ADAPTER_PACKAGE"));
				/*********************************************************
				 * Added by Sriram for Req#TJN_23_17 18-Aug-2016 ends
				 */
				/* Added by Parveen for defect #665 starts */
				aut.setOperation(rs.getString("APP_OPERATIONS"));
				/* Added by Parveen for defect #665 ends */

				/* Added by Parveen for Enhancement#665 on 24-11-2016 starts */

				aut.setLoginUserType(rs.getString("APP_USERTYPES"));
				/* Added by Parveen for Enhancement#665 on 24-11-2016 ends */

				// Added by Sriram for API Base uRL
				aut.setApiBaseUrl(rs.getString("API_BASE_URL"));
				// Added by Sriram for API Base uRL ends

				// Added by Sameer for new Adapter Spec
				aut.setAdapterPackageOrTool(rs.getInt("OR_TOOL"));
				aut.setApplicationType(rs.getInt("APPLICATION_TYPE"));
				// Added by Sameer for new Adapter Spec

				/* Added by sahana for Req#TENJINCG-375 starts */
				aut.setApplicationType(rs.getInt("APP_TYPE"));
				aut.setPlatform(rs.getString("APP_PLATFORM"));
				aut.setPlatformVersion(rs.getString("APP_PLATFORM_VERSION"));
				aut.setApplicationPackage(rs.getString("APP_PACKAGE"));
				aut.setApplicationActivity(rs.getString("APP_ACTIVITY"));
				/* Added by sahana for Req#TENJINCG-375 ends */

				// TENJINCG-415
				aut.setApiAuthenticationType(rs.getString("API_AUTH_TYPE"));
				/* Added by Preeti for TENJINCG-73 starts */
				aut.setPauseLocation(rs.getString("APP_PAUSE_LOCATION"));
				aut.setPauseTime(rs.getInt("APP_PAUSE_TIME"));
				/* Added by Preeti for TENJINCG-73 ends */
				// TENJINCG-415 ends
			}

			if (aut != null) {
				ArrayList<ModuleBean> modules = new ModulesHelper().hydrateModules(aut.getId());
				aut.setFunctions(modules);
			} else {
				throw new DatabaseException("Aut does not exist");
			}

			
		}
		 catch (SQLException e) {
			
			logger.error(e.getMessage(), e);
		}
		/* changed by manish for bug T25IT-220 ends */
		finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return aut;
	}
	/*modified by shruthi for VAPT helper fixes ends*/
	
	/*modified by shruthi for VAPT helper fixes starts*/
	public Aut hydrateAut(Connection conn, int appId) throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		Aut aut = null;
		try {
			pst = conn.prepareStatement("SELECT * FROM MASAPPLICATION WHERE APP_ID =?");
			pst.setInt(1, appId);
			rs = pst.executeQuery();
			while (rs.next()) {
				aut = new Aut();
				aut.setId(rs.getInt("APP_ID"));
				aut.setName(rs.getString("APP_NAME"));
				aut.setURL(rs.getString("APP_URL"));
				aut.setLoginReqd(rs.getString("APP_LOGIN_REQD"));
				aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
				aut.setPassword(rs.getString("APP_PASSWORD"));
				/* changed by sahana for TJN_23_09 requirement on 22-7-2016-->Starts */
				aut.setBrowser(rs.getString("APP_DEF_BROWSER"));
				/* changed by sahana for TJN_23_09 requirement on 25-7-2016-->Ends */
				/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 starts */
				aut.setDateFormat(rs.getString("APP_DATE_FORMAT"));
				/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 ends */
			}

			
			 /* added by Roshni for TENJINCG-1168 starts */
			if (aut != null) {
				ArrayList<ModuleBean> modules = new ModulesHelper().hydrateModules(aut.getId());
				aut.setFunctions(modules);
			} else {
				throw new DatabaseException("Aut does not exist");
			}
			/* added by Roshni for TENJINCG-1168 ends */
			
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return aut;
	}
	/*modified by shruthi for VAPT helper fixes ends*/


	public String getAutName(Connection conn, int appId) throws DatabaseException {
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		String appName = "";
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("SELECT APP_NAME FROM MASAPPLICATION WHERE APP_ID = ?");
			pst.setInt(1, appId);
			rs = pst.executeQuery();
			while (rs.next()) {
				appName = rs.getString("APP_NAME");
			}

			return appName;
		} catch (SQLException e) {
			logger.error("Exception in getAutName(Connection conn, int appId)");
			logger.error("Could not get name for Application with ID " + appId, e);
			return null;
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
	}

	public int getAutId(Connection conn, String appName) throws DatabaseException {
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		int appId = 0;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("SELECT APP_ID FROM MASAPPLICATION WHERE APP_NAME = ?");
			pst.setString(1, appName);
			rs = pst.executeQuery();
			while (rs.next()) {
				appId = rs.getInt("APP_ID");
			}

			return appId;
		} catch (SQLException e) {
			logger.error("Exception in getAutName(Connection conn, int appId)");
			logger.error("Could not get name for Application with ID " + appId, e);
			return 0;
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
	}
	
	/*modified by shruthi for VAPT helper fixes starts*/
	public Aut hydrateAut(String appName) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		/*Statement st = null;*/
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		Aut aut = null;
		try {
			
			pst = conn.prepareStatement("SELECT * FROM MASAPPLICATION WHERE APP_NAME =?");
			pst.setString(1, appName);
			rs = pst.executeQuery();
			while (rs.next()) {
				aut = new Aut();
				aut.setId(rs.getInt("APP_ID"));
				aut.setName(rs.getString("APP_NAME"));
				aut.setURL(rs.getString("APP_URL"));
				aut.setLoginReqd(rs.getString("APP_LOGIN_REQD"));
				aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
				aut.setPassword(rs.getString("APP_PASSWORD"));
				/* changed by sahana for TJN_23_09 requirement on 22-7-2016-->Starts */
				aut.setBrowser(rs.getString("APP_DEF_BROWSER"));
				/* changed by sahana for TJN_23_09 requirement on 25-7-2016-->Ends */
				/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 starts */
				aut.setDateFormat(rs.getString("APP_DATE_FORMAT"));
				/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 ends */
			}
			/* Added by Gangadhar Badagi to get List of functions for TENJINCG-263 starts */
			if (aut != null) {
				ArrayList<ModuleBean> modules = new ModulesHelper().hydrateModules(aut.getId());
				aut.setFunctions(modules);
			}
			/* Added by Gangadhar Badagi to get List of functions for TENJINCG-263 ends */
			
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return aut;
	}
	/*modified by shruthi for VAPT helper fixes ends*/

	public ArrayList<Aut> hydrateAllAut() throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<Aut> autList = null;

		try {
			pst = conn.prepareStatement("SELECT * FROM MASAPPLICATION");
			rs = pst.executeQuery();
			autList = new ArrayList<Aut>();
			while (rs.next()) {
				Aut aut = new Aut();
				aut.setId(rs.getInt("APP_ID"));
				aut.setName(Utilities.escapeXml(rs.getString("APP_NAME")));
				aut.setURL(rs.getString("APP_URL"));
				aut.setLoginReqd(rs.getString("APP_LOGIN_REQD"));
				aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
				aut.setPassword(rs.getString("APP_PASSWORD"));
				/* changed by sahana for TJN_23_09 requirement on 22-7-2016-->Starts */
				aut.setBrowser(rs.getString("APP_DEF_BROWSER"));
				/* changed by sahana for TJN_23_09 requirement on 25-7-2016-->Ends */
				/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 starts */
				// aut.setDateFormat(rs.getString("APP_DATE_FORMAT"));
				/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 ends */

				/* Added by sahana for Req#TENJINCG-375 starts */
				aut.setApplicationType(rs.getInt("APP_TYPE"));
				aut.setPlatform(rs.getString("APP_PLATFORM"));
				aut.setPlatformVersion(rs.getString("APP_PLATFORM_VERSION"));
				aut.setApplicationPackage(rs.getString("APP_PACKAGE"));
				aut.setApplicationActivity(rs.getString("APP_ACTIVITY"));
				/* Added by sahana for Req#TENJINCG-375 ends */
				autList.add(aut);
			}

			/*
			 * rs.close(); pst.close();
			 */

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch Applications Under Test", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return autList;
	}

	public ArrayList<Aut> hydrateAllAut(Connection conn) throws DatabaseException {


		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<Aut> autList = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("SELECT * FROM MASAPPLICATION");
			rs = pst.executeQuery();
			autList = new ArrayList<Aut>();
			while (rs.next()) {
				Aut aut = new Aut();
				aut.setId(rs.getInt("APP_ID"));
				aut.setName(rs.getString("APP_NAME"));
				aut.setURL(rs.getString("APP_URL"));
				aut.setLoginReqd(rs.getString("APP_LOGIN_REQD"));
				aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
				aut.setPassword(rs.getString("APP_PASSWORD"));
				/* changed by sahana for TJN_23_09 requirement on 22-7-2016-->Starts */
				aut.setBrowser(rs.getString("APP_DEF_BROWSER"));
				/* changed by sahana for TJN_23_09 requirement on 25-7-2016-->Ends */
				/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 starts */
				// aut.setDateFormat(rs.getString("APP_DATE_FORMAT"));
				/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 ends */

				/* Added for TENJINCG-314 */
				aut.setOperation(rs.getString("APP_OPERATIONS"));
				aut.setLoginUserType(rs.getString("APP_USERTYPES"));
				aut.setApiBaseUrl(rs.getString("API_BASE_URL"));
				aut.setDateFormat(rs.getString("APP_DATE_FORMAT"));
				aut.setAdapterPackage(rs.getString("APP_ADAPTER_PACKAGE"));
				/* Added for TENJINCG-314 ends */
				autList.add(aut);
			}

			
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch Applications Under Test", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return autList;
	}

	/* changed by anudeep on 10092015 for learned functions dropdown Starts */
	public ArrayList<Module> hydrateFunctions(int appid) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<Module> autfuncList = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			/*modified by shruthi for VAPT Helper fix starts*/
			pst = conn.prepareStatement("SELECT * FROM MASMODULE WHERE APP_ID=?");
			pst.setInt(1, appid);
			/*modified by shruthi for VAPT Helper fix ends*/
			rs = pst.executeQuery();
			autfuncList = new ArrayList<Module>();
			while (rs.next()) {

				Module mod = new Module();
				mod.setAut(rs.getInt("APP_ID"));
				mod.setCode(rs.getString("MODULE_CODE"));
				mod.setName(rs.getString("MODULE_NAME"));
				mod.setWsurl(rs.getString("WS_URL"));
				mod.setWsop(rs.getString("WS_OPERATION"));
				mod.setName(rs.getString("MODULE_NAME"));
				autfuncList.add(mod);
			}

			

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch Applications Functions", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return autfuncList;

	}

	public void clearModule(String appid, String moduleCode) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		boolean learnt = this.functionLearningStatus(Integer.parseInt(appid), moduleCode, conn);
		if (learnt) {
			
			DatabaseHelper.close(conn);
			throw new DatabaseException("Could not delete function [" + moduleCode + "] as it has Metadata.");
		}
		try {
			pst = conn.prepareStatement("delete from masmodule where module_code=? and app_id=?");
			pst.setString(1, moduleCode);
			pst.setString(2, appid);
			pst.execute();

		} catch (Exception e) {
			throw new DatabaseException("Could not delete record", e);
		} finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	public boolean functionLearningStatus(int appId, String functionCode, Connection conn) throws DatabaseException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean learnt = false;
		try {
			pst = conn.prepareStatement(
					"SELECT * FROM LRNR_AUDIT_TRAIL WHERE LRNR_APP_ID=? AND LRNR_FUNC_CODE=? AND LRNR_STATUS=?");
			pst.setInt(1, appId);
			pst.setString(2, functionCode);
			pst.setString(3, "COMPLETE");

			rs = pst.executeQuery();
			while (rs.next()) {
				learnt = true;
				break;
			}


			return learnt;
		} catch (Exception e) {
			logger.error("ERROR checking if function [{}] is already learnt", functionCode, e);
			return false;
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
	}

	public ArrayList<Aut> hydrateAllUserAut(String tjnUserId, int appId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<Aut> userCredentials = new ArrayList<Aut>();
		try {
			pst = conn.prepareStatement(
					"SELECT  A.*,B.APP_NAME AS APP_NAME FROM USRAUTCREDENTIALS A, MASAPPLICATION B WHERE A.APP_ID = B.APP_ID AND LOWER(A.USER_ID) = LOWER(?) AND A.APP_ID =?");
			pst.setInt(2, appId);
			pst.setString(1, tjnUserId);

			rs = pst.executeQuery();

			while (rs.next()) {
				Aut aut = new Aut();
				aut.setUserId(rs.getString("USER_ID"));
				aut.setId(rs.getInt("APP_ID"));
				aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
				aut.setPassword(rs.getString("APP_PASSWORD"));
				aut.setLoginUserType(rs.getString("APP_USER_TYPE"));
				aut.setName(rs.getString("APP_NAME"));
				/* changed by sahana for TJN_23_13 requirement on 26-7-2016 -->Starts */
				aut.setTxnPassword(rs.getString("APP_TXN_PWD"));
				/* changed by sahana for TJN_23_13 requirement on 27-7-2016 -->Ends */
				userCredentials.add(aut);
			}

			
		} catch (SQLException e) {
			
			logger.error("Could not fetch Credentials for Application - " + appId, e);
			throw new DatabaseException("Could not fetch Credentials for the selected Application" + appId, e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return userCredentials;
	}

	public ArrayList<Aut> hydrateAllUserAut(Connection conn, String tjnUserId, int appId, int prjId)
			throws DatabaseException {

		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<Aut> userCredentials = new ArrayList<Aut>();
		try {
			pst = conn.prepareStatement(
					"SELECT  A.*,B.APP_NAME,B.APP_DEF_BROWSER,B.APP_ADAPTER_PACKAGE,B.APP_URL,B.API_AUTH_TYPE,B.API_BASE_URL,B.APP_PACKAGE,B.APP_ACTIVITY,B.APP_PAUSE_TIME,B.APP_PAUSE_LOCATION, B.APP_FILE_NAME, C.OR_TOOL,C.APPLICATION_TYPE,B.OAUTH_AUTH_URL, B.OAUTH_ACCESS_TOKEN_URL, B.OAUTH_CLIENT_ID, B.OAUTH_CLIENT_SECRET, B.OAUTH_ADDAUTHDATA_TO, B.OAUTH_ACCESS_TOKEN_URL, A.OAUTH_ACCESS_TOKEN, A.OAUTH_REFRESH_TOKEN, A.OAUTH_TOKENGEN_TIMESTAMP, A.OAUTH_TOKEN_VALIDITY, A.UAC_ID,D.AUT_URL, B.OAUTH_GRANT_TYPE FROM USRAUTCREDENTIALS A, TJN_PRJ_AUTS D, MASAPPLICATION B  left outer join TJN_ADAPTER C  on B.APP_ADAPTER_PACKAGE = C.NAME WHERE A.APP_ID = B.APP_ID AND LOWER(A.USER_ID) = LOWER(?) AND A.APP_ID =? AND D.PRJ_ID=? AND B.APP_ID=D.APP_ID");

			pst.setInt(2, appId);
			pst.setString(1, tjnUserId);
			/* Added by Ashiki for TJN252-97 starts */
			pst.setInt(3, prjId);
			/* Added by Ashiki for TJN252-97 ends */
			rs = pst.executeQuery();

			while (rs.next()) {
				Aut aut = new Aut();
				aut.setUserId(rs.getString("USER_ID"));
				aut.setId(rs.getInt("APP_ID"));
				aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
				aut.setPassword(rs.getString("APP_PASSWORD"));
				aut.setLoginUserType(rs.getString("APP_USER_TYPE"));
				aut.setName(rs.getString("APP_NAME"));
				/* changed by sahana for TJN_23_13 requirement on 26-7-2016 -->Starts */
				aut.setTxnPassword(rs.getString("APP_TXN_PWD"));
				aut.setAdapterPackage(rs.getString("APP_ADAPTER_PACKAGE"));
				aut.setBrowser(rs.getString("APP_DEF_BROWSER"));
				/* Added by Ashiki for TJN252-97 starts */
				/* aut.setURL(rs.getString("APP_URL")); */
				if (!rs.getString("AUT_URL").equalsIgnoreCase("")) {
					aut.setURL(rs.getString("AUT_URL"));
				} else {
					aut.setURL(rs.getString("APP_URL"));
				}
				/* Added by Ashiki for TJN252-97 ends */

				/* changed by sahana for TJN_23_13 requirement on 27-7-2016 -->Ends */

				// Added by sriram to include API Base URL (26th May 2017)
				aut.setApiBaseUrl(rs.getString("API_BASE_URL"));

				// Added by Sameer for new Adapter Spec
				aut.setAdapterPackageOrTool(rs.getInt("OR_TOOL"));
				aut.setApplicationType(rs.getInt("APPLICATION_TYPE"));
				aut.setApplicationPackage(rs.getString("APP_PACKAGE"));
				aut.setApplicationActivity(rs.getString("APP_ACTIVITY"));
				// Added by Sameer for new Adapter Spec

				// TENJINCG-415
				aut.setApiAuthenticationType(rs.getString("API_AUTH_TYPE"));
				/* Added by Preeti for TENJINCG-73 starts */
				aut.setPauseLocation(rs.getString("APP_PAUSE_LOCATION"));
				aut.setPauseTime(rs.getInt("APP_PAUSE_TIME"));
				/* Added by Preeti for TENJINCG-73 ends */
				// TENJINCG-415 ends
				// added by shivam sharma for TENJINCG-904 starts
				aut.setAppFileName(rs.getString("APP_FILE_NAME"));
				// added by shivam sharma for TENJINCG-904 ends
				// Code changes for OAuth 2.0 requirement TENJINCG-1018 by Avinash - Starts
				aut.setAccessTokenUrl(rs.getString("OAUTH_ACCESS_TOKEN_URL"));
				aut.setAuthUrl(rs.getString("OAUTH_AUTH_URL"));
				aut.setClientId(rs.getString("OAUTH_CLIENT_ID"));
				aut.setClientSecret(rs.getString("OAUTH_CLIENT_SECRET"));
				aut.setAddAuthDataTo(rs.getString("OAUTH_ADDAUTHDATA_TO"));
				aut.setAccessToken(rs.getString("OAUTH_ACCESS_TOKEN"));
				aut.setRefreshToken(rs.getString("OAUTH_REFRESH_TOKEN"));
				aut.setTokenValidity(rs.getString("OAUTH_TOKEN_VALIDITY"));
				aut.setTokenGenerationTime(rs.getTimestamp("OAUTH_TOKENGEN_TIMESTAMP"));
				aut.setGrantType(rs.getString("OAUTH_GRANT_TYPE"));
				aut.setUacId(rs.getInt("UAC_ID"));
				// Code changes for OAuth 2.0 requirement TENJINCG-1018 by Avinash - Ends
				userCredentials.add(aut);
			}

			
		} catch (SQLException e) {
			
			logger.error("Could not fetch Credentials for Application - " + appId, e);
			throw new DatabaseException("Could not fetch Credentials for the selected Application" + appId, e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return userCredentials;
	}

	public ArrayList<Aut> hydrateAllUserAutNotForUser(String tjnUserId, int appId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<Aut> userCredentials = new ArrayList<Aut>();
		try {
			pst = conn.prepareStatement(
					"SELECT  A.*,B.APP_NAME AS APP_NAME FROM USRAUTCREDENTIALS A, MASAPPLICATION B WHERE A.APP_ID = B.APP_ID AND LOWER(A.USER_ID) != LOWER(?) AND A.APP_ID =?");
			pst.setInt(2, appId);
			pst.setString(1, tjnUserId);

			rs = pst.executeQuery();

			while (rs.next()) {
				Aut aut = new Aut();
				aut.setUserId(rs.getString("USER_ID"));
				aut.setId(rs.getInt("APP_ID"));
				aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
				aut.setPassword(rs.getString("APP_PASSWORD"));
				aut.setLoginUserType(rs.getString("APP_USER_TYPE"));
				aut.setName(rs.getString("APP_NAME"));
				/* changed by sahana for TJN_23_13 requirement on 26-7-2016 -->Starts */
				aut.setTxnPassword(rs.getString("APP_TXN_PWD"));
				/* changed by sahana for TJN_23_13 requirement on 27-7-2016 -->Ends */
				userCredentials.add(aut);
			}

			
		} catch (SQLException e) {
			
			logger.error("Could not fetch Credentials for Application - " + appId, e);
			throw new DatabaseException("Could not fetch Credentials for the selected Application" + appId, e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return userCredentials;
	}

	/* 19-03-2015 Updated Code for Tenjin Maker and Chekcer for the User:Starts */
	public ArrayList<String> hydrateAllAutLoginTypes(String user, int appid) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		ArrayList<String> str = new ArrayList<String>();
		try {
			/*modified by shruthi for VAPT Helper fix starts*/
			pst = conn.prepareStatement("SELECT distinct(APP_USER_TYPE) FROM USRAUTCREDENTIALS where USER_ID=? and APP_ID=?");
			pst.setString(1, user);
			pst.setInt(2, appid);
			/*modified by shruthi for VAPT Helper fix ends*/
			rs = pst.executeQuery();
			while (rs.next()) {
				str.add(rs.getString("APP_USER_TYPE"));
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch Applications AUT Login Type", e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return str;
	}
	/* added by Roshni for TENJINCG-1168 starts */
	public ArrayList<String> hydrateAllAutLoginTypes(String user, int appid,Connection conn) throws DatabaseException {

		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		ArrayList<String> str = new ArrayList<String>();
		try {
			/*modified by shruthi for VAPT Helper fix starts*/
			pst = conn.prepareStatement("SELECT distinct(APP_USER_TYPE) FROM USRAUTCREDENTIALS where USER_ID=? and APP_ID=?");
			pst.setString(1, user);
			pst.setInt(2,appid);
			/*modified by shruthi for VAPT Helper fix ends*/
			rs = pst.executeQuery();
			// userAutTypes = new ArrayList<String>();
			while (rs.next()) {
				str.add(rs.getString("APP_USER_TYPE"));
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch Applications AUT Login Type", e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return str;
	}
	/* added by Roshni for TENJINCG-1168 ends */
	// Added by Sriram to update AUT Base URL
	// TENJINCG-415
	public ArrayList<Group> hydrateCurrentGroups(Connection conn) throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<Group> grpList = null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {
			pst = conn.prepareStatement("SELECT * FROM TJN_AUT_GROUPS");
			rs = pst.executeQuery();
			grpList = new ArrayList<Group>();
			while (rs.next()) {
				Group grp = new Group();
				grp.setAut(rs.getInt("APP_ID"));
				grp.setAppName(rs.getString("APP_NAME"));
				grp.setGroupName(rs.getString("GROUP_NAME"));
				grp.setGroupDesc(rs.getString("GROUP_DESC"));
				grpList.add(grp);
			}

			

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch Groups under Applications Under Test", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return grpList;
	}
	public void updateAutApiProperties(int appId, String basePath, String authenticationType) throws DatabaseException {
		// TENJINCG-415 ends
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement("UPDATE MASAPPLICATION SET API_BASE_URL=?,API_AUTH_TYPE=? WHERE APP_ID=?");
			pst.setString(1, basePath);
			// TENJINCG-415
			pst.setInt(3, appId);
			pst.setString(2, authenticationType);
			// TENJINCG-415 ends

			pst.executeUpdate();

		} catch (SQLException e) {
			logger.error("ERROR updating API Base URL", e);
			throw new DatabaseException(
					"Could not update details due to an internal error. Please contact Tenjin Support");
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	// Added by Sriram to update AUT Base URL ends

	/* For TENJINCG-314 */
	public ArrayList<Aut> hydrateAllAut(Connection conn, int[] appIds) throws DatabaseException {

		// Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		ResultSet rs = null;
		/*modified by shruthi for sql injection starts*/
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<Aut> autList = null;
  for(int str : appIds) {
		try {
			pst = conn.prepareStatement("SELECT * FROM MASAPPLICATION WHERE APP_ID=?");
			pst.setInt(1, str);
			rs = pst.executeQuery();
			autList = new ArrayList<Aut>();
			while (rs.next()) {
				Aut aut = new Aut();
				aut.setId(rs.getInt("APP_ID"));
				aut.setName(rs.getString("APP_NAME"));
				aut.setURL(rs.getString("APP_URL"));
				aut.setLoginReqd(rs.getString("APP_LOGIN_REQD"));
				aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
				aut.setPassword(rs.getString("APP_PASSWORD"));
				/* changed by sahana for TJN_23_09 requirement on 22-7-2016-->Starts */
				aut.setBrowser(rs.getString("APP_DEF_BROWSER"));
				/* changed by sahana for TJN_23_09 requirement on 25-7-2016-->Ends */
				/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 starts */
				// aut.setDateFormat(rs.getString("APP_DATE_FORMAT"));
				/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 ends */

				/* Added for TENJINCG-314 */
				aut.setOperation(rs.getString("APP_OPERATIONS"));
				aut.setLoginUserType(rs.getString("APP_USERTYPES"));
				aut.setApiBaseUrl(rs.getString("API_BASE_URL"));
				aut.setDateFormat(rs.getString("APP_DATE_FORMAT"));
				aut.setAdapterPackage(rs.getString("APP_ADAPTER_PACKAGE"));
				/* Added for TENJINCG-314 ends */
				autList.add(aut);
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch Applications Under Test", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
  }
  /*modified by shruthi for sql injection ends*/
		return autList;
	}
	/* For TENJINCG-314 ends */

	// Added by Roshni for T25IT-156,157,158 starts
	public String getAutName(int appId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		String appName = "";
		try {
			pst = conn.prepareStatement("SELECT APP_NAME FROM MASAPPLICATION WHERE APP_ID = ?");
			pst.setInt(1, appId);
			rs = pst.executeQuery();
			while (rs.next()) {
				appName = rs.getString("APP_NAME");
			}

			return appName;
		} catch (SQLException e) {
			logger.error("Exception in getAutName(Connection conn, int appId)");
			logger.error("Could not get name for Application with ID " + appId, e);
			return null;
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	/*modified by shruthi for VAPT helper fixes starts*/
	public Boolean CheckAutAssociateToProj(int appid, int projId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean flag = false;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try {
	
			pst = conn.prepareStatement("select * from tjn_prj_auts where app_id=? and prj_id=?");
			pst.setInt(1, appid);
			pst.setInt(2, projId);
			rs = pst.executeQuery();

			if (rs.next()) {
				flag = true;

			} else {
				flag = false;
			}
		} catch (Exception e) {

		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return flag;
		

	}
	/*modified by shruthi for VAPT helper fixes ends*/
	// Added by Roshni for T25IT-156,157,158 starts

	/*modified by shruthi for VAPT helper fixes starts*/
	// Added by manish to fetch only app id based on app name and project id starts
	public int hydrateAppId(int projId, String appName) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		/*Statement st = null;*/
		PreparedStatement pst=null;
		ResultSet rs = null;
		int appId = 0;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			
			pst = conn.prepareStatement("select APP_ID from tjn_prj_auts where app_id in(select app_id from masapplication where app_name =?) and prj_id=?");
			pst.setString(1, appName);
			pst.setInt(2, projId);
			rs = pst.executeQuery();
			while (rs.next()) {
				appId = rs.getInt("APP_ID");
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new DatabaseException("An Internal error occured, Please contact Tenjin support");
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return appId;

	}
	/*modified by shruthi for VAPT helper fixes ends*/
	/* Added by Roshni for TenjinCG-1038 starts */
	public String hydrateTemplatePassword(int appid) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			pst = conn.prepareStatement("SELECT TEMPLATE_PASSWORD FROM MASAPPLICATION where APP_ID=?");
			pst.setInt(1, appid);
			rs = pst.executeQuery();
			if (rs.next()) {
				if (rs.getString("TEMPLATE_PASSWORD") == null) {
					return "";
				} else {
					return rs.getString("TEMPLATE_PASSWORD");
				}
			}
		} catch (Exception e) {

		}
		/*Added by paneendra for TENJINCG-1267 starts*/
		 finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		/*Added by paneendra for TENJINCG-1267 ends*/ 
		return null;
	}
	/* Added by Roshni for TenjinCG-1038 ends */
	public boolean isFunctionLearnt(Connection conn, String modCode, int appId) throws DatabaseException{
		boolean learnt = false;

		PreparedStatement pst = null;
		ResultSet rs = null;

		try{
			pst = conn.prepareStatement("SELECT COUNT(*) AS PA_COUNT FROM AUT_FUNC_PAGEAREAS WHERE PA_FUNC_CODE = ? AND PA_APP = ?");
			pst.setInt(2, appId);
			pst.setString(1, modCode);
			rs = pst.executeQuery();

			while(rs.next()){
				if(rs.getInt("PA_COUNT") > 0){
					learnt = true;
				}
				break;
			}
		}catch(Exception e){
			logger.error("ERROR checking if function {} was learnt",modCode,e);
			throw new DatabaseException("Could not check learning status for function " + modCode + " due to an internal error");
		}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return learnt;
	}
}
