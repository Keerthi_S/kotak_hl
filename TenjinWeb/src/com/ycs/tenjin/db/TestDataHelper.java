/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestDataHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 *
 * DATE              	CHANGED BY              DESCRIPTION
 * 03-02-2021			Paneendra	            Tenj210-158 
 */

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestDataPath;
import com.ycs.tenjin.util.Constants;


public class TestDataHelper {
	private static final Logger logger = LoggerFactory.getLogger(TestDataHelper.class);




	public Project hydrateTestDataPath(int projectId) throws DatabaseException {

		Project project=null;
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

				PreparedStatement pst = conn.prepareStatement("SELECT PRJ_NAME,TEST_DATA_REPO_TYPE,REPO_ROOT FROM TJN_PROJECTS WHERE PRJ_ID=? ");
				){
			pst.setInt(1, projectId);

			try(ResultSet rs = pst.executeQuery()){
				while(rs.next()){

					project=new Project();
					project.setName(rs.getString("PRJ_NAME"));
					project.setRepoRoot(rs.getString("REPO_ROOT"));
					project.setRepoType(rs.getString("TEST_DATA_REPO_TYPE"));
				}
			}
		}
		catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records ," +projectId);
		}

		return project; 
	}

	public void persistTestData(TestDataPath testdatapath) throws DatabaseException {


		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst =null;
		try {                                   

			pst = conn.prepareStatement("INSERT INTO TJN_PRJ_TESTDATA (PRJ_ID,APP_ID,FUNC_CODE,FILE_NAME,UPLOADED_BY,UPLOADED_ON) VALUES (?,?,?,?,?,?)");
			pst.setInt(1,testdatapath.getProjectId());
			pst.setInt(2,testdatapath.getAppId());
			pst.setString(3,testdatapath.getFunction());
			pst.setString(4,testdatapath.getFileName());
			pst.setString(5,testdatapath.getUser());
			pst.setTimestamp(6,testdatapath.getUploadedOn());

			pst.execute();


		}
		catch (SQLException e) {
			logger.error("Could not persist record");
			throw new DatabaseException("Could not persist record",e);
		}
		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);

	}


	public List<TestDataPath> hydrateAllTestData(int projectId,String userId) throws DatabaseException {

		ArrayList<TestDataPath> testDataPath=new ArrayList<TestDataPath>();
		TestDataPath tstdataPath;
		ResultSet rs=null;
		PreparedStatement pst=null;

		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

				){

			pst = conn.prepareStatement("SELECT APP_ID,FUNC_CODE,FILE_NAME FROM TJN_PRJ_TESTDATA WHERE PRJ_ID=? AND UPLOADED_BY=?");		
			pst.setInt(1, projectId);
			pst.setString(2, userId);

			rs = pst.executeQuery();

			while(rs.next()){

				tstdataPath=new TestDataPath();
				tstdataPath.setAppId(rs.getInt("APP_ID"));
				String appName=new AutHelper().getAutName(rs.getInt("APP_ID"));
				tstdataPath.setAppName(appName);
				tstdataPath.setFunction(rs.getString("FUNC_CODE"));
				tstdataPath.setFileName(rs.getString("FILE_NAME"));
				testDataPath.add(tstdataPath);

			}

		}
		catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records ," +projectId);
		}
		DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);


		return testDataPath; 
	}
	
	public List<TestDataPath> hydrateAllTestData(int projectId,String userId, String appId) throws DatabaseException {

		ArrayList<TestDataPath> testDataPath=new ArrayList<TestDataPath>();
		TestDataPath tstdataPath;
		ResultSet rs=null;
		PreparedStatement pst=null;

		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

				){

			pst = conn.prepareStatement("SELECT APP_ID,FUNC_CODE,FILE_NAME FROM TJN_PRJ_TESTDATA WHERE PRJ_ID=? AND UPLOADED_BY=? AND APP_ID=?");		
			pst.setInt(1, projectId);
			pst.setString(2, userId);
			pst.setString(3, appId);

			rs = pst.executeQuery();

			while(rs.next()){

				tstdataPath=new TestDataPath();
				tstdataPath.setAppId(Integer.parseInt(appId));
				String appName=new AutHelper().getAutName(Integer.parseInt(appId));
				tstdataPath.setAppName(appName);
				tstdataPath.setFunction(rs.getString("FUNC_CODE"));
				tstdataPath.setFileName(rs.getString("FILE_NAME"));
				testDataPath.add(tstdataPath);

			}

		}
		catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records ," +projectId);
		}
		DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);


		return testDataPath; 
	}
	
	
	 /*Modified by Priyanka  during LTI Testing for TDP validation starts */
	public TestDataPath hydrateAllTestDataPaths(int projectId,String funcode, String userId) throws DatabaseException {
		/*Modified by Priyanka  during LTI Testing for TDP validation ends */


		TestDataPath tstdataPath = null;
		ResultSet rs=null;
		PreparedStatement pst=null;

		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

				){
		/* modified by shruthi for 2.12 Test data path fix starts*/
			pst = conn.prepareStatement("SELECT APP_ID,FUNC_CODE,UPLOADED_BY FROM TJN_PRJ_TESTDATA WHERE PRJ_ID=? AND FUNC_CODE=? AND UPLOADED_BY=?");		
			pst.setInt(1, projectId);
			pst.setString(2, funcode);
			pst.setString(3, userId);
		 /* modified by shruthi for 2.12 Test data path fix ends*/
			rs = pst.executeQuery();

			while(rs.next()){

				tstdataPath=new TestDataPath();
				tstdataPath.setAppId(rs.getInt("APP_ID"));
				String appName=new AutHelper().getAutName(rs.getInt("APP_ID"));
				String prjName=new ProjectHelper().hydrateProjectName(projectId);
				tstdataPath.setAppName(appName);
				tstdataPath.setProjectName(prjName);
				tstdataPath.setFunction(rs.getString("FUNC_CODE"));
				tstdataPath.setUser(rs.getString("UPLOADED_BY"));


			}

		}
		catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records ," +projectId);
		}
		DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);


		return tstdataPath; 
	}



	public void updateTestData(TestDataPath testdatapath) throws DatabaseException {


		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst =null;
		try {                                   

			pst = conn.prepareStatement("UPDATE  TJN_PRJ_TESTDATA SET FILE_NAME=?,UPLOADED_ON=? WHERE PRJ_ID=? AND APP_ID=? AND FUNC_CODE=? AND UPLOADED_BY=? ");
			pst.setString(1,testdatapath.getFileName());
			pst.setTimestamp(2,testdatapath.getUploadedOn());
			pst.setInt(3,testdatapath.getProjectId());
			pst.setInt(4,testdatapath.getAppId());
			pst.setString(5,testdatapath.getFunction());
			pst.setString(6,testdatapath.getUser());

			pst.execute();


		}
		catch (SQLException e) {
			logger.error("Could not update record");
			throw new DatabaseException("Could not persist record",e);
		}
		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);

	}


	public boolean hydrateTestDataFileName(TestDataPath testdatapath) throws DatabaseException {

		boolean recordExist=false;
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst =null;
		ResultSet rs=null;
		try {                                   

			pst = conn.prepareStatement("SELECT FILE_NAME FROM TJN_PRJ_TESTDATA WHERE PRJ_ID=? AND APP_ID=? AND FUNC_CODE=? AND UPLOADED_BY=?");

			pst.setInt(1,testdatapath.getProjectId());
			pst.setInt(2,testdatapath.getAppId());
			pst.setString(3,testdatapath.getFunction());
			pst.setString(4,testdatapath.getUser());


			rs = pst.executeQuery();

			while(rs.next()) {
				recordExist=true;
				break;
			}

		}catch (SQLException e) {
			logger.error("Could not fetch record");
			throw new DatabaseException("Could not fetch record",e);
		}
		DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);

		return recordExist;

	}

	public List<TestDataPath> hydrateTestData(int projectId) throws DatabaseException {

		ArrayList<TestDataPath> testDataPath=new ArrayList<TestDataPath>();
		TestDataPath tstdataPath;
		ResultSet rs=null;
		PreparedStatement pst=null;

		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

				){

			pst = conn.prepareStatement("SELECT APP_ID,FILE_NAME,FUNC_CODE FROM TJN_PRJ_TESTDATA WHERE PRJ_ID=?");		
			pst.setInt(1, projectId);
			rs = pst.executeQuery();

			while(rs.next()){

				tstdataPath=new TestDataPath();
				int appId=rs.getInt("APP_ID");
				String appName=new AutHelper().getAutName(appId);
				tstdataPath.setAppId(rs.getInt("APP_ID"));
				tstdataPath.setAppName(appName);
				tstdataPath.setFunction(rs.getString("FUNC_CODE"));
				tstdataPath.setFileName(rs.getString("FILE_NAME"));
				testDataPath.add(tstdataPath);

			}

		}
		catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records ," +projectId);
		}
		DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);


		return testDataPath; 
	}

}