/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  CustomBasicDataSourceFactory.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION 
 *21-01-2019			Roshni					Added Newly 
 *23-01-2019			Roshni					TJN252-78 
 */

package com.ycs.tenjin.db.factory;


import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.Name;

import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import org.apache.tomcat.dbcp.dbcp.BasicDataSourceFactory;

import com.ycs.tenjin.util.CryptoUtilities;

public class CustomBasicDataSourceFactory extends BasicDataSourceFactory {
	@Override
	public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable environment) throws Exception {
		Object o = super.getObjectInstance(obj, name, nameCtx, environment);
		if (o != null) {
			BasicDataSource ds = (BasicDataSource) o;
			if (ds.getPassword() != null && ds.getPassword().length() > 0) {
				/*String decryptedPwd = new Crypto().decrypt(ds.getPassword().split("!#!")[1],
				decodeHex(ds.getPassword().split("!#!")[0].toCharArray()));*/
		String decryptedPwd = new CryptoUtilities().decrypt(ds.getPassword());
		/*String decryptedUname = new Crypto().decrypt(ds.getUsername().split("!#!")[1],
				decodeHex(ds.getUsername().split("!#!")[0].toCharArray()));*/
		String decryptedUname = new CryptoUtilities().decrypt(ds.getUsername());
		/*String decryptedURL = new Crypto().decrypt(ds.getUrl().split("!#!")[1],
				decodeHex(ds.getUrl().split("!#!")[0].toCharArray()));*/
		String decryptedURL = new CryptoUtilities().decrypt(ds.getUrl());
		/*Modified by paneendra for VAPT FIX ends*/
				ds.setUrl(decryptedURL);
				ds.setUsername(decryptedUname);
				ds.setPassword(decryptedPwd);
			}
			return ds;
		} else {
			return null;
		}

	}

}
