/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCaseHelperNew.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 	CHANGED BY              DESCRIPTION
 * 19-02-2018		   	Pushpalatha			  	Newly Added 
 * 20-03-2018			Preeti					TENJINCG-611(Test case execution history)
 * 09-05-2018            Padmavathi              TENJINCG-646 & TENJINCG-645
 * 10-05-2018			Preeti					TENJINCG-665
 * 10-09-2018			Sriram					TENJINCG-733
 * 18-09-2018			Sriram					TENJINCG-735
 * 27-09-2018            Padmavathi              TENJINCG-741
 * 28-09-2018			Leelaprasad			    TENJINCG-738
 * 01-10-2018			Pushpalatha		    	TENJINCG-821
 * 08-10-2018			Pushpalatha				TENJINCG-844
 * 12-10-2018            Padmavathi              TENJINCG-847
 * 24-10-2018            Padmavathi              TENJINCG-849
 * 24-10-2018			Preeti					TENJINCG-850
 * 25-10-2018			Preeti					TENJINCG-850
 * 02-11-2018			Ashiki					TENJINCG-895
 * 02-11-2018			Pushpa					TENJINCG-897
 * 28-11-2018			Preeti					TJNUN262-7
 * 20-12-2018			Pushpa					TJN262R2-49
 * 24-12-2018			Preeti					TJN262R2-24
 * 22-01-2019			Ashiki					TJN252-45
 * 13-02-2019			Preeti					TENJINCG-970
 * 19-02-2019			Ashiki					TJN252560
 * 19-02-2019			Preeti					TENJINCG-969
 * 28-02-2019            Padmavathi              TENJINCG-942
 * 06-03-2019			Pushpalatha				TENJINCG-978
 * 13-01-2020			Prem					scripting fix
 * 05-06-2020			Ashiki					Tenj210-108
 * 18-05-2020			Ashiki					TENJINCG_1215,TENJINCG_1214
 */
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.handler.TestStepHandler;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.testmanager.TestManagerInstance;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class TestCaseHelperNew {

	private static final Logger logger = LoggerFactory.getLogger(TestCaseHelperNew.class);

	//TENJINCG-735 - Sriram
	private final Map<String, String> fieldToColumnMap = new TreeMap<>();
	public TestCaseHelperNew() {
		this.fieldToColumnMap.put("tcRecId", "TC_REC_ID");
		this.fieldToColumnMap.put("tcId", "TC_ID");
		this.fieldToColumnMap.put("tcName", "TC_NAME");
		this.fieldToColumnMap.put("tcCreatedOn", "TC_CREATED_ON");
		this.fieldToColumnMap.put("mode", "TC_EXEC_MODE");
		this.fieldToColumnMap.put("storage", "TC_STORAGE");
		this.fieldToColumnMap.put("tcType", "TC_TYPE");
	}
	//TENJINCG-735 - Sriram ends

	/*Modified by Preeti for TENJINCG-850 starts*/
	/*Changed by Padmavathi for TENJINCG-849 Starts*/
	public int persistTestCaseTuned(TestCase t) throws DatabaseException{
		/*	Changed by Padmavathi for TENJINCG-849 ends*/
		int tcRecId = 0;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);){
			tcRecId = this.persistTestCase(conn, t);
		}catch(Exception e){
			logger.warn("Could not persist test case", e);
		}
		return tcRecId;
	}
	/*Modified by Preeti for TENJINCG-850 ends*/

	public boolean doesTestCaseExist(Connection conn, int projectId, String testCaseId) throws DatabaseException{
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		boolean exists = false;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst = conn.prepareStatement("SELECT COUNT(*) AS CNT FROM TESTCASES WHERE TC_PRJ_ID = ? AND TC_ID = ?");
			pst.setInt(1, projectId);
			pst.setString(2, testCaseId);
			rs = pst.executeQuery();
			while(rs.next()){
				if(rs.getInt("CNT") > 0){
					exists = true;
				}
			}
		}catch(Exception e){
			logger.error("Could not check if test case " + testCaseId + " already exists",e);
			throw new DatabaseException("Could not check if this test case exists");
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		return exists;
	}

	/*Commented by Ashiki for TENJINCG-895 starts*/
	public boolean doesTestCaseNameExist(int projectId, String testCaseName) throws DatabaseException{

		Connection conn= DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		boolean exists = false;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst = conn.prepareStatement("SELECT COUNT(*) AS CNT FROM TESTCASES WHERE TC_PRJ_ID = ? AND TC_NAME = ?");
			pst.setInt(1, projectId);
			pst.setString(2, testCaseName);
			rs = pst.executeQuery();
			while(rs.next()){
				if(rs.getInt("CNT") > 0){
					exists = true;
				}
			}
		}catch(Exception e){
			logger.error("Could not check if test case " + testCaseName + " already exists",e);
			throw new DatabaseException("Could not check if this test case exists");
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		return exists;
	}
	 
	/*Commented by Ashiki for TENJINCG-895 ends*/
	/*modified by shruthi for VAPT helper fixes starts*/
	//Added for TENJINCG-733 - Sriram
	public ArrayList<TestCase> hydrateAllTestCasesForList(int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs =null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		ArrayList<TestCase> testCases = new ArrayList<TestCase>();
		try{
			pst = conn.prepareStatement("SELECT * FROM TESTCASES WHERE TC_PRJ_ID =? ORDER BY TC_REC_ID");
			pst.setInt(1, projectId);
			rs = pst.executeQuery();
			while(rs.next()){
				TestCase tc=	this.mapFields(rs);
				testCases.add(tc);
			}

		}catch(Exception e){

		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}/*modified by shruthi for VAPT helper fixes ends*/

		return testCases;
	}//Added for TENJINCG-733 - Sriram ends

	public ArrayList<TestCase> hydrateAllTestCases(int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		ResultSet rs =null;
		PreparedStatement pst = null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		ArrayList<TestCase> testCases = new ArrayList<TestCase>();
		try{
			
			/*modified by paneendra for sql injection starts*/		
			pst = conn.prepareStatement("SELECT * FROM TESTCASES WHERE TC_PRJ_ID = ? ORDER BY TC_REC_ID");
			pst.setInt(1, projectId);
			rs=pst.executeQuery();
			/*modified by paneendra for sql injection ends*/
			while(rs.next()){
				TestCase tc=	this.mapFields(rs);
				ArrayList<TestStep> steps = new TestStepHandler().hydrateStepsForTestCase(conn, conn, tc.getTcRecId());
				tc.setTcSteps(steps);
				testCases.add(tc);
			}

		}catch(Exception e){

		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return testCases;
	}

	public ArrayList<TestStep> hydrateStepsForTestCase(Connection conn, Connection appConn, int testCaseRecordId) throws DatabaseException{
		PreparedStatement pst = null;
		ResultSet rs = null;

		ArrayList<TestStep> steps =null;

		try{
			pst = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID = ? ORDER BY TSTEP_REC_ID");
			pst.setInt(1, testCaseRecordId);
			rs = pst.executeQuery();

			steps = new ArrayList<TestStep>();

			Map<Integer, String> autMap = new HashMap<Integer, String>();

			while(rs.next()){

				TestStep t = new TestStep();
				t.setRecordId(rs.getInt("TSTEP_REC_ID"));
				t.setId(rs.getString("TSTEP_ID"));
				t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
				String appname= autMap.get(t.getAppId());
				if(appname != null && !appname.equalsIgnoreCase("")){
					t.setAppName(appname);
				}else{
					appname = getappname(appConn, rs.getInt("APP_ID"));
					t.setAppName(appname);
				}
				t.setModuleCode(rs.getString("FUNC_CODE"));
				t.setType(rs.getString("TSTEP_TYPE"));
				t.setDataId(rs.getString("TSTEP_DATA_ID"));
				t.setDescription(rs.getString("TSTEP_DESC"));
				t.setExpectedResult(rs.getString("TSTEP_EXP_RESULT"));
				t.setOperation(rs.getString("TSTEP_OPERATION"));
				t.setAutLoginType(rs.getString("TSTEP_AUT_LOGIN_TYPE"));
				t.setRowsToExecute(rs.getInt("TSTEP_ROWS_TO_EXEC"));
				t.setTstepStorage(rs.getString("TSTEP_STORAGE"));
				String mode=rs.getString("TXNMODE");
				if(mode.equalsIgnoreCase("GUI"))
				{
					mode="GUI";
				}
				else
				{
					mode="API";
					t.setResponseType((rs.getInt("TSTEP_API_RESP_TYPE")));
				}
				t.setTxnMode(mode);
				steps.add(t);
			}

			if(steps == null || steps.size() == 0){
				System.err.println();
			}
		}catch(Exception e){
			logger.error("ERROR fetching steps under test case",e);
			throw new DatabaseException("Could not fetch steps under this test case due to an internal error",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return steps;
	}

	public static String getappname(Connection conn, int appid) throws DatabaseException{
		String appname="";
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst = conn.prepareStatement("SELECT app_name FROM masapplication WHERE app_id = ?");
			pst.setInt(1, appid);
			rs = pst.executeQuery();

			if(rs.next()){
				appname = rs.getString("app_name");
			}

		}catch(Exception e){
			logger.error("Could not fetch app name for ID {}", appid, e);
			throw new DatabaseException(e.getMessage(),e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return appname;
	}
	/*Added by Preeti for TENJINCG-611(Test case execution history) starts*/
	public List<TestRun> hydrateTestCaseExecutionHistory(int prjId, int tcRecId) throws DatabaseException{

		List<TestRun> summary = new ArrayList<TestRun>();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			/*Modified by Preeti for TENJINCG-665 starts*/
			pst = conn.prepareStatement("SELECT DISTINCT A.RUN_START_TIME,A.RUN_END_TIME,A.RUN_USER,"
					+" C.RUN_STATUS,C.TOTAL_TCS,C.TOTAL_EXE_TCS,C.TOTAL_PASSED_TCS,C.TOTAL_FAILED_TCS,C.TOTAL_ERROR_TCS,"
					+" B.TC_NAME,B.TC_REC_ID,B.TC_ID,"
					+" A.RUN_ID"
					+" FROM MASTESTRUNS A, TESTCASES B, V_RUNRESULTS C, V_TSTEP_EXEC_HISTORY_2 D"
					+" WHERE D.tc_rec_id=B.TC_REC_ID"
					+" AND A.RUN_ID=C.RUN_ID"
					+" AND A.RUN_ID=D.RUN_ID"
					+" AND B.tc_rec_id=? and A.run_prj_id=? order by A.RUN_ID DESC");
			/*Modified by Preeti for TENJINCG-665 ends*/
			pst.setInt(1, tcRecId);
			pst.setInt(2, prjId);
			rs = pst.executeQuery();
			while(rs.next()){
				TestRun run = new TestRun();
				run.setId(rs.getInt("RUN_ID"));
				run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
				run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
				run.setUser(rs.getString("RUN_USER"));
				run.setTcRecId(rs.getString("TC_REC_ID"));
				run.setStatus(rs.getString("RUN_STATUS"));
				if(run.getStartTimeStamp() != null && run.getEndTimeStamp() != null){
					long startMillis = run.getStartTimeStamp().getTime();
					long endMillis = run.getEndTimeStamp().getTime();

					long millis = endMillis - startMillis;

					String eTime = String.format(
							"%02d:%02d:%02d",
							TimeUnit.MILLISECONDS.toHours(millis),
							TimeUnit.MILLISECONDS.toMinutes(millis)
							- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
									.toHours(millis)),
							TimeUnit.MILLISECONDS.toSeconds(millis)
							- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
									.toMinutes(millis)));
					run.setElapsedTime(eTime);
				}

				run.setTotalTests(rs.getInt("TOTAL_TCS"));
				run.setExecutedTests(rs.getInt("TOTAL_EXE_TCS"));
				run.setPassedTests(rs.getInt("TOTAL_PASSED_TCS"));
				run.setFailedTests(rs.getInt("TOTAL_FAILED_TCS"));
				run.setErroredTests(rs.getInt("TOTAL_ERROR_TCS"));
				TestCase t = new TestCase();
				t.setTcId(rs.getString("TC_ID"));
				/*changed by leelaprasad for the SQL server migration starts*/
				/*t.setTcRecId(Integer.parseInt(run.getTcRecId()));*/
				t.setTcRecId(tcRecId);
				/*changed by leelaprasad for the SQL server migration ends*/
				t.setTcName(rs.getString("TC_NAME"));
				run.setTestCase(t);
				summary.add(run);
			}
		} catch (SQLException e) {
			throw new DatabaseException("Could not fetch execution history for test case " + tcRecId,e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return summary;

	}
	/*Added by Preeti for TENJINCG-611(Test case execution history) ends*/

	/*Changed by Padmavathi for TENJINCG-847( to check whether to hydrate  test case with steps or not starts*/
	/*	public TestCase hydrateTestCaseBasicDetails(int projectId, int recordId) throws DatabaseException{*/
	public TestCase hydrateTestCaseBasicDetails(int projectId, int recordId,boolean withSteps) throws DatabaseException{
		/*Changed by Padmavathi for TENJINCG-847( to check whether to hydrate  test case with steps or not ends*/
		Connection projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		Connection appConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		Statement st1 = null;
		ResultSet rs =null;
		ResultSet rs1 =null;
		if(projConn == null || appConn ==null){
			throw new DatabaseException("Invalid or no database connection");
		}
		TestCase tc = null;
		try{
			/*Modified by Preeti for TENJINCG-969 starts*/
			pst = projConn.prepareStatement("SELECT * FROM TESTCASES T LEFT JOIN (SELECT * FROM TJN_AUDIT WHERE AUDIT_ID=(SELECT MAX(AUDIT_ID) FROM TJN_AUDIT WHERE ENTITY_TYPE=? AND ENTITY_RECORD_ID = ?)) A ON A.ENTITY_RECORD_ID = T.TC_REC_ID WHERE T.TC_REC_ID = ? AND T.TC_PRJ_ID = ? ");
			pst.setString(1, "testcase");
			pst.setInt(2,recordId);
			pst.setInt(3,recordId);
			pst.setInt(4, projectId);
			rs = pst.executeQuery();

			while(rs.next()){
				tc=this.mapFieldsWithAudit(rs);
			}
			/*Modified by Preeti for TENJINCG-969 ends*/
			/*Changed by Padmavathi for TENJINCG-847 starts*/
			/*Changed by Padmavathi for TENJINCG-847 ends*/
			if(tc != null && withSteps ){
				ArrayList<TestStep> tsteps=new TestStepHandler().hydrateStepsForTestCase(projConn, appConn, tc.getTcRecId());

				tc.setTcSteps(tsteps);
				/*Added By Padmavathi for TENJINCG-646 &TENJINCG-645 starts*/
				tc.setTotalSteps(tsteps.size());
				/*Added By Padmavathi for TENJINCG-646 &TENJINCG-645 ends*/
			}
		}catch(SQLException e){
			logger.error("Error in hydrateTestCaseBasicDetails(Connection projConn, Connection appConn, int projectId, int recordId)");
			logger.error("Could not fetch details for test case with Record ID " + recordId,e);
			throw new DatabaseException("Could not fetch Test Case Details",e);
		}finally{
			DatabaseHelper.close(rs1);		
			DatabaseHelper.close(st1);
			DatabaseHelper.close(rs);		
			DatabaseHelper.close(pst);
			DatabaseHelper.close(projConn);		
			DatabaseHelper.close(appConn);
		}

		return tc;
	}


	@SuppressWarnings("finally")
	public String updateTestCase(TestCase testcase) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst =null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		boolean abortUpdate = false;
		try{
			abortUpdate = this.testCaseConcurrencyCheck(conn, testcase.getTcRecId());
		}catch(Exception e){
			throw new DatabaseException("Test Case concurrency check failed");
		}

		if(abortUpdate){
			DatabaseHelper.close(conn);
			return "Executing";
		}

		try{
			/*Modified By Padmavathi for TENJINCG-646 &TENJINCG-645 starts*/
			/*Modified By Padmavathi for TENJINCG-942 starts*/
			/*Modified by Ashiki for TENJINCG-1214 starts*/
			pst = conn.prepareStatement("UPDATE TESTCASES SET tc_id = ?, tc_name = ?, tc_priority = ?, tc_type =?,tc_desc = ?,TC_EXEC_MODE = ?,TC_PRESET_RULES=? ,TC_APP_ID=?, TC_TAG=? where tc_rec_id = ?" );
			/*Modified by Ashiki for TENJINCG-1214 ends*/
			/*Modified By Padmavathi for TENJINCG-942 starts*/
			/*Modified By Padmavathi for TENJINCG-646 &TENJINCG-645 ends*/
			pst.setString(1, testcase.getTcId());
			pst.setString(2, testcase.getTcName());
			pst.setString(3, testcase.getTcPriority());
			pst.setString(4, testcase.getTcType());
			pst.setString(5, testcase.getTcDesc());

			pst.setString(6, testcase.getMode());
			/*Added By Padmavathi for TENJINCG-646 &TENJINCG-645 starts*/
			pst.setString(7, testcase.getTcPresetRules());
			/*Added By Padmavathi for TENJINCG-646 &TENJINCG-645 starts*/

			/*Added By Padmavathi for TENJINCG-942 starts*/
			pst.setInt(8, testcase.getAppId());
			/*Added By Padmavathi for TENJINCG-942 ends*/
			/*Added by Ashiki for TENJINCG_1214 starts*/
			pst.setString(9, testcase.getLabel());
			/*Added by Ashiki for TENJINCG_1214 end*/
			pst.setInt(10, testcase.getTcRecId());

			pst.execute();
		}catch(Exception e){
			throw new DatabaseException("Could not update test case due to an internal error",e);
		} finally{

			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
			return "Not Executing";
		}

	}

	/*Added by Preeti for TENJINCG-970 starts*/
	public String updateTestCase(Connection conn,TestCase testcase) throws DatabaseException{

		boolean abortUpdate = false;
		try{
			abortUpdate = this.testCaseConcurrencyCheck(conn, testcase.getTcRecId());
		}catch(Exception e){
			throw new DatabaseException("Test Case concurrency check failed");
		}

		if(abortUpdate){
			DatabaseHelper.close(conn);
			return "Executing";
		}

		try(PreparedStatement pst = conn.prepareStatement("UPDATE TESTCASES SET tc_id = ?, tc_name = ?, tc_priority = ?, tc_type =?,tc_desc = ?,TC_EXEC_MODE = ? where tc_rec_id = ?" );

				){
			pst.setString(1, testcase.getTcId());
			pst.setString(2, testcase.getTcName());
			pst.setString(3, testcase.getTcPriority());
			pst.setString(4, testcase.getTcType());
			pst.setString(5, testcase.getTcDesc());
			pst.setString(6, testcase.getMode());
			pst.setInt(7, testcase.getTcRecId());
			pst.execute();
		}catch(Exception e){
			throw new DatabaseException("Could not update test case due to an internal error",e);
		} 
		return "Not Executing";
	}
	/*Added by Preeti for TENJINCG-970 ends*/

	public boolean testCaseConcurrencyCheck(Connection conn, int testCaseRecordId) throws DatabaseException{
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		boolean isTestCaseExecuting = false;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst = conn.prepareStatement("SELECT A.RUN_ID FROM MASTESTRUNS A, TESTSETMAP B WHERE A.RUN_TS_REC_ID = B.TSM_TS_REC_ID AND B.TSM_TC_REC_ID = ? AND A.RUN_STATUS = ?");
			pst.setInt(1, testCaseRecordId);
			pst.setString(2, "Executing");

			rs = pst.executeQuery();

			while(rs.next()){
				isTestCaseExecuting = true;
				break;
			}
		}catch(Exception e){
			logger.error("ERROR performing Test Case Concurrency Check",e);
			throw new DatabaseException("Could not check test case concurrency due to an internal error. Please contact your system administrator");
		}finally{	
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		logger.info("Test Case Concurrency Check --> {}", isTestCaseExecuting);
		return isTestCaseExecuting;
	}

	
	public void deleteTestCase(int testCaseRecordId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst1 =null;
		PreparedStatement pst2 =null;
		PreparedStatement pst3 =null;
		PreparedStatement pst =null;
		ResultSet rs = null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}


		try{
			pst1 = conn.prepareStatement("DELETE FROM RUNRESULTS_TS WHERE TC_REC_ID = ?");
			pst1.setInt(1, testCaseRecordId);
			pst1.execute();

		}catch(SQLException e){
			logger.error("ERROR clearing results for test case " + testCaseRecordId + " in RUNRESULTS_TS",e);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				
				logger.warn("ROLLBACK ERROR after deleting data in RUNRESULTS_TS",e1);
			}
			throw new DatabaseException("Could not delete test case due to an internal error");
		}finally{
			DatabaseHelper.close(pst1);
		}

		try{
			pst2 = conn.prepareStatement("DELETE FROM RUNRESULTS_TC WHERE TC_REC_ID = ?");
			pst2.setInt(1, testCaseRecordId);
			pst2.execute();

		}catch(SQLException e){
			logger.error("ERROR clearing results for test case " + testCaseRecordId + " in RUNRESULTS_TC",e);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				
				logger.warn("ROLLBACK ERROR after deleting data in RUNRESULTS_TC",e1);
			}
			throw new DatabaseException("Could not delete test case due to an internal error");
		}finally{
			DatabaseHelper.close(pst2);
		}

		try{	
			pst3 = conn.prepareStatement("DELETE FROM TESTSETMAP WHERE TSM_TC_REC_ID = ?");
			pst3.setInt(1, testCaseRecordId);
			pst3.execute();
		}catch(SQLException e){
			logger.error("ERROR clearing results for test case " + testCaseRecordId + " in TESTSETMAP",e);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				
				logger.warn("ROLLBACK ERROR after deleting data in TESTSETMAP",e1);
			}
			throw new DatabaseException("Could not delete test case due to an internal error");
		}finally{
			DatabaseHelper.close(pst3);
		}

		try{
			pst = conn.prepareStatement("SELECT TSTEP_REC_ID FROM TESTSTEPS WHERE TC_REC_ID = ?");
			pst.setInt(1, testCaseRecordId);

			rs = pst.executeQuery();
			while(rs.next()){
				int stepRecId = rs.getInt("TSTEP_REC_ID");
				TestStep t = new TestStep();
				t.setRecordId(stepRecId);
				this.deleteTestStep(conn, t);
			}

		}catch(SQLException e){
			logger.error("ERROR fetching test steps to delete",e);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				
				logger.warn("ROLLBACK ERROR after error in fetching test steps to delete",e1);
			}
			throw new DatabaseException("Could not delete test case due to an internal error");
		}catch(DatabaseException e){
			logger.error("ERROR deleting test case --> "  + e.getMessage(),e);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				
				logger.warn("ROLLBACK ERROR",e1);
			}
			throw new DatabaseException("Could not delete test case due to an internal error");
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		try{
			pst = conn.prepareStatement("DELETE FROM TESTCASES WHERE TC_REC_ID = ?");
			pst.setInt(1, testCaseRecordId);
			pst.execute();
			pst.close();
		}catch(SQLException e){
			logger.error("ERROR clearing data for test case " + testCaseRecordId + " in TESTCASES",e);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				
				logger.warn("ROLLBACK ERROR deleting record in TESTCASES",e1);
			}
			throw new DatabaseException("Could not delete test case due to an internal error");
		}finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}



		/*try{
			conn.commit();
		}catch(Exception e){
			logger.error("ERROR COMMITING DELETE in deleteTestCase()",e);
			DatabaseHelper.close(conn);
			throw new DatabaseException("Could not delete test case due to an internal error");
		}
		finally{
			DatabaseHelper.close(conn);
		}*/


	}

	public void deleteTestStep(Connection conn, TestStep step) throws DatabaseException{


		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst1=null, pst2=null, pst3=null, pst4=null, pst5=null;
		try{

			pst1 = conn.prepareStatement("DELETE FROM RESVALRESULTS WHERE TSTEP_REC_ID = ?");
			pst1.setInt(1, step.getRecordId());

			pst2 = conn.prepareStatement("DELETE FROM RESVALMAP WHERE TSTEP_REC_ID = ?");
			pst2.setInt(1, step.getRecordId());

			pst3 = conn.prepareStatement("DELETE FROM RUNRESULT_TS_TXN WHERE TSTEP_REC_ID = ?");
			pst3.setInt(1, step.getRecordId());

			pst4 = conn.prepareStatement("DELETE FROM RUNRESULTS_TS WHERE TSTEP_REC_ID = ?");
			pst4.setInt(1, step.getRecordId());

			pst5 = conn.prepareStatement("DELETE FROM TESTSTEPS WHERE TSTEP_REC_ID = ?");
			pst5.setInt(1, step.getRecordId());


			pst1.execute();
			pst2.execute();
			pst3.execute();
			pst4.execute();
			pst5.execute();

			pst1.close();
			pst2.close();
			pst3.close();
			pst4.close();
			pst5.close();
		}catch(Exception e){
			throw new DatabaseException("Could not remove Test Step",e);
		}finally{

			DatabaseHelper.close(pst5);		
			DatabaseHelper.close(pst4);		
			DatabaseHelper.close(pst3);		
			DatabaseHelper.close(pst2);		
			DatabaseHelper.close(pst1);
		}

	}


	public boolean testCaseAvailability(TestCase testCase) throws DatabaseException
	{
		Connection conn=null;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

			boolean exist=this.doesTestCaseExist(conn, testCase.getProjectId(),String.valueOf(testCase.getTcId().trim()));

			return exist;
		} catch (DatabaseException e) {
			logger.error("exception occured while fetching testcase");
		}/*Added by paneendra for TENJINCG-1267 starts*/
		finally {

			DatabaseHelper.closeConnection(conn);
		}
		/*Added by paneendra for  TENJINCG-1267 ends*/
		return false;	
	}

	public TestManagerInstance hydrateTMCredentials(String instance, String userId, int projectId) throws DatabaseException{
		TestManagerInstance tmInstance = new TestManagerInstance();
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("SELECT A.TJN_USER_ID,A.TJN_INSTANCE_NAME,A.USERNAME,A.PASSWORD,"
					+" B.TM_REC_ID,B.TM_TOOL,B.TM_URL,C.TM_PROJECT FROM"
					+" TJN_TM_USER_MAPPING A,"
					+" TJN_TM_TOOL_MASTER B, TJN_PRJ_TM_LINKAGE C WHERE"
					+" A.TJN_INSTANCE_NAME=B.TM_INSTANCE_NAME AND"
					+" C.TM_INSTANCE_NAME=B.TM_INSTANCE_NAME AND"
					+" A.TJN_USER_ID=? AND"
					+" B.TM_INSTANCE_NAME=? AND C.PRJ_ID=?");
			pst.setString(1, userId);
			pst.setString(2, instance);
			pst.setInt(3, projectId);
			rs = pst.executeQuery();
			while(rs.next()){
				tmInstance.setURL(rs.getString("TM_URL"));
				tmInstance.setPassword(rs.getString("PASSWORD"));
				tmInstance.setTjnUsername(rs.getString("TJN_USER_ID"));
				tmInstance.setTool(rs.getString("TM_TOOL"));
				tmInstance.setUsername(rs.getString("USERNAME"));
				tmInstance.setTmProjectDomain(rs.getString("TM_PROJECT"));
			}
		} catch (SQLException e) {
			logger.error("Could not hydrate TM instance's credentials {}",instance,e);
		}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return tmInstance;
	}

	public void hydrateFilters(Project project) throws DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn
					.prepareStatement("SELECT * FROM TJN_PRJ_TM_LINKAGE WHERE PRJ_ID=?");
			pst.setInt(1, project.getId());
			rs = pst.executeQuery();
			while(rs.next()){
				project.setEtmTool(rs.getString("TM_INSTANCE_NAME"));
				project.setEtmProject(rs.getString("TM_PROJECT"));
				project.setEtmEnable("TM_ENABLED");
				project.setTcFilter(rs.getString("TC_FILTER"));
				project.setTcFilterValue(rs.getString("TC_FILTER_VALUE"));
				project.setTsFilter(rs.getString("TS_FILTER"));
				project.setTsFilterValue(rs.getString("TS_FILTER_VALUE"));
			}
		} catch (SQLException e) {
			logger.error("could not fetch filters for project {}",project.getId(),e);
		}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	public boolean checkTestCase(String tcId, int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		boolean exist=false;
		ResultSet rs = null;
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement("SELECT * FROM TESTCASES WHERE TC_ID=? AND TC_PRJ_ID=?");
			pst.setString(1, tcId);
			pst.setInt(2, projectId);
			rs = pst.executeQuery();
			while(rs.next()){
				exist=true;
			}
		} catch (SQLException e) {
			logger.error("error while checking testcase already imported or not",e);
		}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}


		return exist;
	}

	public boolean checkImportedTestStep(TestStep step, int tcRecId) throws DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		boolean exist=false;
		ResultSet rs = null;
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TSTEP_ID=? AND TC_REC_ID=?");
			pst.setString(1, step.getId());
			pst.setInt(2, tcRecId);
			rs = pst.executeQuery();
			while(rs.next()){
				exist=true;
			}
		} catch (SQLException e) {
			logger.error("error while checking testcase already imported or not",e);
		}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return exist;
	}

	private TestCase mapFields(ResultSet rs) throws SQLException {

		try {
			TestCase testcase=new TestCase();

			testcase.setTcId(Utilities.escapeXml(rs.getString("tc_id")));
			testcase.setTcFolderId(rs.getInt("TC_FOLDER_ID"));

			testcase.setTcName(Utilities.escapeXml(rs.getString("tc_name")));
			testcase.setTcDesc(rs.getString("tc_desc"));
			testcase.setTcRecId(rs.getInt("TC_REC_ID"));

			testcase.setTcType(rs.getString("tc_type"));
			testcase.setTcPriority(rs.getString("TC_PRIORITY"));
			testcase.setTcReviewed(rs.getString("TC_REVIEWED"));
			testcase.setTcStatus(rs.getString("tc_status"));
			testcase.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
			testcase.setTcModifiedOn(rs.getTimestamp("tc_modified_on"));
			testcase.setTcCreatedBy(rs.getString("tc_created_by"));
			testcase.setTcModifiedBy(rs.getString("tc_modified_by"));
			testcase.setProjectId(rs.getInt("TC_PRJ_ID"));
			testcase.setRecordType(rs.getString("TC_REC_TYPE"));
			testcase.setStorage(rs.getString("TC_STORAGE"));
			testcase.setMode(rs.getString("TC_EXEC_MODE"));
			/*Added By Padmavathi for TENJINCG-646 &TENJINCG-645 starts*/
			testcase.setTcPresetRules(rs.getString("TC_PRESET_RULES"));
			/*Added By Padmavathi for TENJINCG-646 &TENJINCG-645 ends*/
			/*Added by Ashiki for TENJINCG-1215 starts*/
			testcase.setLabel(rs.getString("TC_TAG"));
			/*Added by Ashiki for TENJINCG-1215 ends*/
			return testcase;
		} catch (SQLException e) {
			logger.error("ERROR creating AUT from ResultSet", e);
			throw e;
		}

	}
	/*Added by Preeti for TENJINCG-969 starts*/
	private TestCase mapFieldsWithAudit(ResultSet rs) throws SQLException {

		try {
			TestCase testcase=new TestCase();

			testcase.setTcId(rs.getString("tc_id"));
			testcase.setTcFolderId(rs.getInt("TC_FOLDER_ID"));

			testcase.setTcName(rs.getString("tc_name"));
			testcase.setTcDesc(rs.getString("tc_desc"));
			testcase.setTcRecId(rs.getInt("TC_REC_ID"));

			testcase.setTcType(rs.getString("tc_type"));
			testcase.setTcPriority(rs.getString("TC_PRIORITY"));
			testcase.setTcReviewed(rs.getString("TC_REVIEWED"));
			testcase.setTcStatus(rs.getString("tc_status"));
			testcase.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
			testcase.setTcModifiedOn(rs.getTimestamp("tc_modified_on"));
			testcase.setTcCreatedBy(rs.getString("tc_created_by"));
			testcase.setTcModifiedBy(rs.getString("tc_modified_by"));
			testcase.setProjectId(rs.getInt("TC_PRJ_ID"));
			testcase.setRecordType(rs.getString("TC_REC_TYPE"));
			testcase.setStorage(rs.getString("TC_STORAGE"));
			testcase.setMode(rs.getString("TC_EXEC_MODE"));
			/*Added By Padmavathi for TENJINCG-646 &TENJINCG-645 starts*/
			testcase.setTcPresetRules(rs.getString("TC_PRESET_RULES"));
			/*Added By Padmavathi for TENJINCG-646 &TENJINCG-645 ends*/
			/*Added By Padmavathi for TENJINCG-942 starts*/
			testcase.setAppId(rs.getInt("TC_APP_ID"));
			/*Added By Padmavathi for TENJINCG-942 ends*/
			AuditRecord record = new AuditRecord();
			record.setAuditId(rs.getInt("AUDIT_ID"));
			record.setEntityRecordId(rs.getInt("ENTITY_RECORD_ID"));
			record.setEntityType(rs.getString("ENTITY_TYPE"));
			record.setLastUpdatedBy(rs.getString("LAST_MODIFIED_BY"));
			record.setLastUpdatedOn(rs.getTimestamp("LAST_MODIFIED_ON"));
			testcase.setAuditRecord(record);
			/*Added by Ashiki for TENJINCG-1214 starts*/
			testcase.setLabel(rs.getString("TC_TAG"));
			/*Added by Ashiki for TENJINCG-1214 ends*/
			return testcase;
		} catch (SQLException e) {
			logger.error("ERROR creating AUT from ResultSet", e);
			throw e;
		}

	}
	/*Added by Preeti for TENJINCG-969 ends*/
	//TENJINCG-735 - Sriram
	public PaginatedRecords<TestCase> hydrateAllTestCases(int projectId, List<FilterCriteria> filterCriteria, int maxRecords, int pageNumber, String sortColumn, String sortDirection) throws DatabaseException {
		Paginator paginator = null;

		try {
			paginator = new Paginator(maxRecords, pageNumber);
			List<TestCase> testCases = new LinkedList<>();


			String sCol = Utilities.trim(sortColumn).length() > 0 ? this.fieldToColumnMap.get(sortColumn) : "TC_REC_ID";
			String sDir = Utilities.trim(sortDirection).length() > 0 ? Utilities.trim(sortDirection) : "asc";
			if(sCol == null || sCol.length() < 1) {
				logger.warn("Sort Column {} is invalid. Default sorting will be applied", sortColumn);
				sCol = "TC_REC_ID";
			}
			String baseQuery = this.getQuery(new String[] {"*"}, filterCriteria);
			//Apply sort
			baseQuery += " ORDER BY " + sCol + " " + sDir;
			/*Modified by Pushpa for TJN262R2-49 starts*/
			paginator.executeQuery("testcase",baseQuery);
			/*Modified by Pushpa for TJN262R2-49 ends*/
			int totalRecords = 0;
			while(paginator.getResultSet().next()) {
				TestCase tc = this.mapFields(paginator.getResultSet());
				testCases.add(tc);
				try {
					totalRecords = paginator.getResultSet().getInt("P_REC_COUNT");
				}catch(Exception e) {

				}
			}

			PaginatedRecords<TestCase> records = new PaginatedRecords<>();
			records.setPageNumber(pageNumber);
			records.setRecords(testCases);
			records.setTotalSize(totalRecords);
			records.setCurrentSize(maxRecords);
			return records;
		} catch(Exception e) {
			logger.error("ERROR occurred while fetching test cases under project", e);
			throw new DatabaseException("Could not fetch test cases due to an internal error.", e);
		} finally {
			if(paginator != null) {
				try {
					paginator.close();
				} catch (Exception e) {
					logger.warn("ERROR while closing paginator. This could potentially be a connection leak", e);
				}
			}

		}
	}

	private String getQuery(String[] fieldsToFetch, List<FilterCriteria> criteria) {

		String query = "";

		String cols = "";
		if(fieldsToFetch != null && fieldsToFetch.length > 0){
			for(String field:fieldsToFetch){
				if(cols.length() < 1){
					cols += field;
				}else{
					cols += ", " + field;
				}
			}
		}else{
			cols = "*";
		}

		query = "SELECT " + cols + " FROM TESTCASES";

		String filterList = "";
		FilterCriteria orderBy = null;
		//FilterCriteria groupBy = null;
		if(criteria != null && criteria.size() > 0){
			for(FilterCriteria criterion:criteria){
				if(criterion.getCondition().equalsIgnoreCase(FilterCriteria.ORDER_BY)){
					orderBy = criterion;
					continue;
				}
				if(criterion.toString() != null){
					if(filterList.length() < 1){
						filterList += criterion.toString();
					}else{
						filterList += " AND " + criterion.toString();
					}
				}
			}
		}

		if(filterList.length() > 0){

			query = query + " WHERE " + filterList;

		}

		if(orderBy != null && orderBy.toString() != null){
			query = query + " " + orderBy.toString();
		}

		logger.debug("Generated Query --> {}", query);
		return query;

	}

	//TENJINCG-735 - Sriram ends

	/*Added by Padmavathi for TENJINCG-741 starts*/
	public TestCase hydrateTestCase(Connection conn,int projectId,String tcId) throws DatabaseException{

		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		TestCase tc = null;
		try(PreparedStatement pst = conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_ID = ? AND TC_PRJ_ID = ?")){
			pst.setString(1,tcId);
			pst.setInt(2, projectId);
			try(ResultSet rs=pst.executeQuery()){
				while(rs.next()){
					tc=this.mapFields(rs);
				}
			}
		}catch(SQLException e){
			logger.error("Could not fetch details for test case with Record ID " + tcId,e);
			throw new DatabaseException("Could not fetch Test Case Details",e);
		}

		return tc;
	}
	/*Added by Padmavathi for TENJINCG-741 ends*/

	/*Changed by Leelaprasad for the Requirement TENJINCG-738 starts*/
	public JSONArray fetchTestCaseIds(String query, int projectId)
			throws DatabaseException {
		JSONArray tcIds = new JSONArray();
		JSONArray sortedJsonArray = new JSONArray();
		int count = 0;
		/*Added by Preeti for TJNUN262-7 starts*/
		if(query.contains("!"))
			query=query.replace("!", "!!");
		if(query.contains("_"))
			query=query.replace("_", "!_");
		if(query.contains("%"))
			query=query.replace("%", "!%");	
		/*Added by Preeti for TJNUN262-7 ends*/
		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				/*Modified by Preeti for TJNUN262-7 starts*/
				PreparedStatement pst = conn.prepareStatement(
						"SELECT TC_ID FROM TESTCASES WHERE TC_PRJ_ID=? AND (UPPER(TC_ID) LIKE ? ESCAPE '!')",
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY);) {
			pst.setInt(1, projectId);
			pst.setString(2, "%" + query.toUpperCase() + "%");
			/*Modified by Preeti for TJNUN262-7 ends*/
			try (ResultSet rs = pst.executeQuery();) {
				if (rs.last()) {
					count = rs.getRow();
					rs.beforeFirst();
				}
				if (count > 0) {

					while (rs.next()) {
						tcIds.put(rs.getString("TC_ID"));

					}
					/*Added by Preeti for TJN262R2-24 starts*/
					List<String> jsonValues = new ArrayList<String>();
					for (int i = 0; i < tcIds.length(); i++)
						jsonValues.add(tcIds.getString(i));
					Collections.sort(jsonValues);
					sortedJsonArray = new JSONArray(jsonValues);
					/*Added by Preeti for TJN262R2-24 ends*/
				}
			} catch (Exception e) {
				logger.error("unable to fetch test cae ids for suggestion box");

			}
		} catch (Exception e) {
			logger.error("Could not connect to data base due to internal error");
			throw new DatabaseException("Could not fetch Test Case Details", e);
		}
		/*Modified by Preeti for TJN262R2-24 starts*/
		return sortedJsonArray;
		/*Modified by Preeti for TJN262R2-24 ends*/
	}

	public static String toCamelCase(String value) {
		StringBuilder sb = new StringBuilder();

		final char delimChar = '_';
		boolean lower = false;
		for (int charInd = 0; charInd < value.length(); ++charInd) {
			final char valueChar = value.charAt(charInd);
			if (valueChar == delimChar) {
				lower = false;
			} else if (lower) {
				sb.append(Character.toLowerCase(valueChar));
			} else {
				sb.append(Character.toUpperCase(valueChar));
				lower = true;
			}
		}

		return sb.toString();
	}
	/*Changed by Leelaprasad for the Requirement TENJINCG-738 ends*/

	/*Added by Pushpa for TENJINCG-821 starts*/
	public TestCase hydrateTestCaseByName(Connection conn,int projectId,String tcName) throws DatabaseException{

		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		TestCase tc = null;
		try(PreparedStatement pst = conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_NAME = ? AND TC_PRJ_ID = ?")){
			pst.setString(1,tcName);
			pst.setInt(2, projectId);
			try(ResultSet rs=pst.executeQuery()){
				while(rs.next()){
					tc=this.mapFields(rs);
				}
			}
		}catch(SQLException e){
			logger.error("Could not fetch details for test case with Record ID " + tcName,e);
			throw new DatabaseException("Could not fetch Test Case Details",e);
		}

		return tc;
	}
	/*Added by Pushpa for TENJINCG-821 ends*/

	/*Added by Pushpa for TENJINCG-844 starts*/
	public TestCase hydrateTestCasesForSet(int projectId, int recordId) throws DatabaseException{

		TestCase tc = null;
		try(Connection projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				Connection appConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = projConn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_REC_ID = ? AND TC_PRJ_ID = ?");	
				){

			pst.setInt(1,recordId);
			pst.setInt(2, projectId);
			try(ResultSet rs = pst.executeQuery();){
				while(rs.next()){
					tc=new TestCase();
					tc.setTcId(Utilities.escapeXml(rs.getString("tc_id")));
					tc.setTcName(Utilities.escapeXml(rs.getString("tc_name")));
					tc.setTcRecId(rs.getInt("TC_REC_ID"));
				}
				if(tc != null){
					ArrayList<TestStep> tsteps=new TestStepHandler().hydrateStepsForTcTs(projConn, appConn, tc.getTcRecId());
					tc.setTcSteps(tsteps);
					tc.setTotalSteps(tsteps.size());

				}
			}
		}catch(SQLException e){
			logger.error("Error in hydrateTestCaseForSet(Connection projConn, Connection appConn, int projectId, int recordId)");
			logger.error("Could not fetch details for test case with Record ID " + recordId,e);
			throw new DatabaseException("Could not fetch Test Case Details",e);
		}
		return tc;
	}
	/*Added by Pushpa for TENJINCG-844 ends*/

	/*Added by Padmavathi for TENJINCG-847 starts*/
	public ArrayList<TestCase>  hydrateAllTestCases(int projectId,String mode) throws DatabaseException{

		TestCase tc = null;
		ArrayList<TestCase> testCases = new ArrayList<TestCase>();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_PRJ_ID = ? AND TC_EXEC_MODE = ?")){
			pst.setInt(1, projectId);
			pst.setString(2,mode);
			try(ResultSet rs=pst.executeQuery()){
				while(rs.next()){
					tc=this.mapFields(rs);
					testCases.add(tc);
				}
			}
		}catch(SQLException e){
			logger.error("Could not fetch test cases with project ID " + projectId,e);
			throw new DatabaseException("Could not fetch test cases",e);
		}

		return testCases;
	}


	public ArrayList<TestCase>  hydrateAllTestCasesByProject(int projectId) throws DatabaseException{

		TestCase tc = null;
		ArrayList<TestCase> testCases = new ArrayList<TestCase>();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_PRJ_ID = ? ")){
			pst.setInt(1, projectId);

			try(ResultSet rs=pst.executeQuery()){
				while(rs.next()){
					tc=this.mapFields(rs);
					testCases.add(tc);
				}
			}
		}catch(SQLException e){
			logger.error("Could not fetch test cases with project ID " + projectId,e);
			throw new DatabaseException("Could not fetch test cases",e);
		}

		return testCases;
	}
	/*Added by Padmavathi for TENJINCG-847 ends*/

	/*Added by Padmavathi for TENJINCG-849 starts*/
	public void updatePresetRule(Connection conn,int tcRecId,String presetRule) throws DatabaseException{
		try(PreparedStatement ps=conn.prepareStatement("UPDATE TESTCASES SET TC_PRESET_RULES=? WHERE TC_REC_ID=?");){
			ps.setString(1, presetRule);
			ps.setInt(2, tcRecId);
			ps.execute();
		}catch(SQLException e){
			logger.error("Could not update preset rule for test case",e);
			throw new DatabaseException("Could not update preset rule for test case",e);
		}
	}
	/*Added by Padmavathi for TENJINCG-849 ends*/
	/*Added by Preeti for TENJINCG-850 starts*/
	public ArrayList<TestCase> hydrateAllTestCasesForImport(Connection conn, String[] selectedTestCaseIds) throws DatabaseException{

		TestCase tc = null;
		ArrayList<TestCase> testCases = new ArrayList<TestCase>();
		String tcIds = "";
		for(int i=0; i <selectedTestCaseIds.length; i++) {
			tcIds += selectedTestCaseIds[i]; 
			if(i < selectedTestCaseIds.length-1) {
				tcIds += ",";
			}
		}
		/*modified by paneendra for sql injection starts*/
		
		for(String selectedtcId:selectedTestCaseIds) {
			try(
					PreparedStatement pst = conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_REC_ID = ?");
				){
				pst.setString(1, selectedtcId);
			
			try(ResultSet rs=pst.executeQuery()){
				while(rs.next()){
					tc=this.mapFields(rs);
					testCases.add(tc);		
				}
			
			}
		}catch(SQLException e){
			logger.error("Could not fetch test cases",e);
			throw new DatabaseException("Could not fetch test cases",e);
		}
		}/*modified by paneendra for sql injection ends*/
		return testCases;
	}
	public int persistTestCase(Connection conn, TestCase tc) throws DatabaseException{
		try(
				/*Modified by Preeti for TENJINCG-969 starts*/
				/*PreparedStatement pst = conn.prepareStatement("SELECT coalesce(MAX(TC_REC_ID),0) AS TC_rEC_ID FROM TESTCASES");*/
				/*Modified By Padmavathi for TENJINCG-942 starts*/
				/*PreparedStatement pst1 = conn.prepareStatement("INSERT INTO TESTCASES (TC_REC_ID,TC_REC_TYPE,TC_ID,TC_FOLDER_ID,TC_PRJ_ID,TC_NAME,TC_TYPE,TC_PRIORITY,TC_CREATED_ON,TC_DESC,TC_CREATED_BY,TC_STATUS,TC_STORAGE,TC_EXEC_MODE) " + 
						" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");*/
				/*Modified by Ashiki for TENJINCG-1214 starts*/
				PreparedStatement pst1 = conn.prepareStatement("INSERT INTO TESTCASES (TC_REC_ID,TC_REC_TYPE,TC_ID,TC_FOLDER_ID,TC_PRJ_ID,TC_NAME,TC_TYPE,TC_PRIORITY,TC_CREATED_ON,TC_DESC,TC_CREATED_BY,TC_STATUS,TC_STORAGE,TC_EXEC_MODE,TC_APP_ID,TC_TAG) " + 
						" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				/*Modified by Ashiki for TENJINCG-1214 ends*/
				/*Modified By Padmavathi for TENJINCG-942 ends*/
				){
			tc.setTcRecId(DatabaseHelper.getGlobalSequenceNumber(conn));
			/*Modified by Preeti for TENJINCG-969 ends*/
			pst1.setInt(1, tc.getTcRecId());
			pst1.setString(2, tc.getRecordType());
			pst1.setString(3, tc.getTcId());
			pst1.setInt(4, tc.getTcFolderId());
			pst1.setInt(5, tc.getProjectId());
			pst1.setString(6, tc.getTcName());
			pst1.setString(7, tc.getTcType());
			pst1.setString(8, tc.getTcPriority());
			Timestamp timeStamp = new Timestamp(new Date().getTime());
			tc.setTcCreatedOn(timeStamp);
			pst1.setTimestamp(9, timeStamp);
			pst1.setString(10, tc.getTcDesc());
			pst1.setString(11, tc.getTcCreatedBy());
			pst1.setString(12, "Not Executed");
			pst1.setString(13, tc.getStorage());
			pst1.setString(14, tc.getMode());
			/*Added By Padmavathi for TENJINCG-942 starts*/
			pst1.setInt(15, tc.getAppId());
			/*Added By Padmavathi for TENJINCG-942 ends*/
			/*Added by Ashiki for TENJINCG-1215 starts*/
			pst1.setString(16, tc.getLabel());
			/*Added by Ashiki for TENJINCG-1215 ends*/
			pst1.execute();
			/*}*/
		} catch (SQLException e) {
			logger.error("Could not persist test cases",e);
			throw new DatabaseException("Could not persist test cases",e);
		}
		return tc.getTcRecId();
	}
	/*Added by Preeti for TENJINCG-850 ends*/
	/*Added by Ashiki for TJN252-45 starts*/
	public void deleteTestCase(int[] testCases,Connection conn) throws SQLException, DatabaseException {
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		for(int tcRecId:testCases){
			try(
					PreparedStatement pst=conn.prepareStatement("DELETE FROM RUNRESULTS_TS WHERE TC_REC_ID = ?");
					PreparedStatement pst1=conn.prepareStatement("DELETE FROM RUNRESULTS_TC WHERE TC_REC_ID = ?");
					PreparedStatement pst2=conn.prepareStatement("DELETE FROM TESTSETMAP WHERE TSM_TC_REC_ID = ?");
					PreparedStatement pst3=conn.prepareStatement("DELETE FROM TESTCASES WHERE TC_REC_ID = ?")){
				pst.setInt(1,tcRecId);
				pst1.setInt(1, tcRecId);
				pst2.setInt(1, tcRecId);
				pst3.setInt(1, tcRecId);
				pst.execute();
				pst1.execute();
				pst2.execute();
				pst3.execute();
			}
		}
	}
	/*Added by Ashiki for TJN252-45 ends*/
	/*Added by Pushpa for TENJINCG-897 starts*/
	public void deleteTestCase1(int[] testCases) throws SQLException, DatabaseException {
		for(int tcRecId:testCases){
			try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					PreparedStatement pst=conn.prepareStatement("DELETE FROM RUNRESULTS_TS WHERE TC_REC_ID = ?");
					PreparedStatement pst1=conn.prepareStatement("DELETE FROM RUNRESULTS_TC WHERE TC_REC_ID = ?");
					PreparedStatement pst2=conn.prepareStatement("DELETE FROM TESTSETMAP WHERE TSM_TC_REC_ID = ?");
					PreparedStatement pst3=conn.prepareStatement("DELETE FROM TESTCASES WHERE TC_REC_ID = ?");	){
				pst.setInt(1,tcRecId);
				pst1.setInt(1, tcRecId);
				pst2.setInt(1, tcRecId);
				pst3.setInt(1, tcRecId);
				pst.execute();
				pst1.execute();
				pst2.execute();
				pst3.execute();
			}
		}
	}
	/*Added by Pushpa for TENJINCG-897 ends*/

	/*Added by Ashiki for TJN252-60 starts*/
	public Map<String, String>  hydrateMappedAttribute(int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		Map<String, String>  mapAttributes = new TreeMap<String, String> ();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("SELECT * FROM TJN_PRJ_TM_ATTRIBUTES");
			rs = pst.executeQuery();
			while(rs.next()){
				mapAttributes.put(rs.getString("TJN_FIELD_NAME"), rs.getString("TM_FIELD_NAME"));
			}



		} catch (SQLException e) {
			logger.error("Error during fetching mapped data for a tenjin field");
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return mapAttributes;
	}
	/*Added by Ashiki for TJN252-60 ends*/
	/*Added by Pushpalatha for TENJINCG-978 starts*/
	public LinkedHashMap<Integer,String> hydrateMappedTestCases(int projectId, int testSetRecordId) throws DatabaseException {
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		LinkedHashMap<Integer,String> tcList=new LinkedHashMap<Integer,String>();
		try {
			pst = conn.prepareStatement("SELECT A.*,B.* FROM TESTSETMAP A,TESTCASES B WHERE A.TSM_PRJ_ID=? AND A.TSM_TS_REC_ID=? AND A.TSM_TC_REC_ID=B.TC_REC_ID ORDER BY A.TSM_TC_SEQ");
			pst.setInt(1, projectId);
			pst.setInt(2, testSetRecordId);
			rs = pst.executeQuery();
			while(rs.next()){

				tcList.put(rs.getInt("TSM_TC_SEQ"),rs.getString("TC_ID"));
			}



		} catch (SQLException e) {
			logger.error("Error during fetching TCSEQUENCES");
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return tcList;
	}
	/*Added by Pushpalatha for TENJINCG-978 end*/

	/*Added by Ashiki for TENJINCG_1215 starts*/
	public JSONArray fetchTestCaseLabels(String query, int projectId) throws DatabaseException {

		JSONArray tcIds = new JSONArray();
		JSONArray sortedJsonArray = new JSONArray();
		int count = 0;
		if(query.contains("!"))
			query=query.replace("!", "!!");
		if(query.contains("_"))
			query=query.replace("_", "!_");
		if(query.contains("%"))
			query=query.replace("%", "!%");	
		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement(
						"SELECT TC_TAG FROM TESTCASES WHERE TC_PRJ_ID=? AND (UPPER(TC_TAG) LIKE ? ESCAPE '!')",
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY);) {
			pst.setInt(1, projectId);
			pst.setString(2, "%" + query.toUpperCase() + "%");
			try (ResultSet rs = pst.executeQuery();) {
				if (rs.last()) {
					count = rs.getRow();
					rs.beforeFirst();
				}
				if (count > 0) {

					while (rs.next()) {
						tcIds.put(rs.getString("TC_TAG"));

					}
					List<String> jsonValues = new ArrayList<String>();
					for (int i = 0; i < tcIds.length(); i++)
						jsonValues.add(tcIds.getString(i));
					Collections.sort(jsonValues);
					sortedJsonArray = new JSONArray(jsonValues);
				}
			} catch (Exception e) {
				logger.error("unable to fetch test case labels for suggestion box");

			}
		} catch (Exception e) {
			logger.error("Could not connect to data base due to internal error");
			throw new DatabaseException("Could not fetch Test Case Label Details", e);
		}
		return sortedJsonArray;

	}
	/*Added by Ashiki for TENJINCG_1215 end*/



	/*Added by Ashiki for Tenj210-108 starts*/
	public void updateImportedTestcase(TestCase testcase,int prjId) throws DatabaseException {
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("UPDATE TESTCASES SET  tc_name = ?, tc_priority = ?, tc_type =?,tc_desc = ?,TC_EXEC_MODE = ?,TC_PRESET_RULES=? ,TC_APP_ID=? where tc_id = ? and TC_PRJ_ID=?");)
		{
			pst.setString(1, testcase.getTcName());
			pst.setString(2, testcase.getTcPriority());
			pst.setString(3, testcase.getTcType());
			pst.setString(4, testcase.getTcDesc());
			pst.setString(5, testcase.getMode());
			pst.setString(6, testcase.getTcPresetRules());
			pst.setInt(7, testcase.getAppId());
			pst.setString(8, testcase.getTcId());
			pst.setInt(9, prjId);
			pst.execute();
		}catch (Exception e) {
			logger.error("Error ", e);
			throw new DatabaseException("Could not update Application User because of an internal error ");
		}
	}
	/*Added by Ashiki for Tenj210-108 ends*/


}
