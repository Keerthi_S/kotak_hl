/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MetaDataHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright  2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              	DESCRIPTION
* 
* 02-12-2016           Parveen                	REQ #TJN_243_04 Changing insert query
* 30-01-2017           Jyoti Ranjan            	REQ #TENJINCG-65,67.
* 19-Aug-2017          Gangadhar Badagi        	T25IT-227
* 20-09-2017		   Roshni				   	Tenjincg-351
* 23-04-2018           Padmavathi              	TENJINCG-617
* 29-06-2018		   Sriram Sridharan			T251IT-101
* 02-11-2018		   Sriram SRidharan			TENJINCG-894
* 13-11-2018           Padmavathi               TENJINCG-899
* 28-12-2018		   LeelaPrasad			   	TJN252-41
* 02-05-2019		   Jyoti					for import metadata issue
*/


package com.ycs.tenjin.db;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Multimap;
import com.ycs.tenjin.aut.MetadataAudit;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class MetaDataHelper {

	public int currentFunctionCount = 0;

	public int getCurrentFunctionCount() {
		return currentFunctionCount;
	}

	private static final Logger logger = LoggerFactory.getLogger(MetaDataHelper.class);
	
	
	/********************************************
	 * Added by Prafful - Req#TJN_24_09
	 */
	public  String getMetadataJSON(int appId, String funcCode) throws DatabaseException, SQLException, JSONException{
		String jsonString = null;
		

		Connection conn =null;
		PreparedStatement pst=null;
		PreparedStatement pst1=null;
		ResultSet rs=null;
		ResultSet rs1=null;
		try{
		 conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
	
		JSONArray allPages = new JSONArray();
		String firstQuery = "SELECT * FROM AUT_FUNC_PAGEAREAS WHERE PA_APP = ? AND PA_FUNC_CODE=? ORDER BY PA_SEQ_NO";
		 pst=conn.prepareStatement(firstQuery);
		pst.setInt(1,appId);
		pst.setString(2,funcCode);
		rs= pst.executeQuery();
		while(rs.next())
		{
			JSONObject json = new JSONObject();
			json.put("id", rs.getString("PA_SEQ_NO"));
		    json.put("text", rs.getString("PA_NAME")); 	
		    JSONObject liAttr = new JSONObject();

		    liAttr.put("node_type","page");
		    liAttr.put("ismrb", rs.getString("PA_IS_MRB"));
		    liAttr.put("type", rs.getString("PA_TYPE"));
		    /*Jyoti : mandatory icon display start TENJINCG-65*/
		    json.put("icon", "jstree/page.png");
		    /*Jyoti : mandatory icon display end TENJINCG-65*/
		    json.put("li_attr", liAttr);
		    /*Modified by Padmavathi for TENJINCG-899 starts*/
		    String secondQuery ="SELECT A.*, B.FLD_USER_REMARKS FROM AUT_FUNC_FIELDS A LEFT JOIN AUT_FUNC_FIELD_INFO B ON "
				+ "A.FLD_APP = B.APP_ID AND A.FLD_FUNC_CODE=B.FUNC_CODE AND A.FLD_PAGE_AREA = B.PA_NAME AND A.FLD_UNAME = B.FLD_UNAME " + 
				"WHERE A.FLD_APP=? and a.fld_func_Code=? and a.fld_page_area=? order by fld_seq_no";
		    /*Modified by Padmavathi for TENJINCG-899 ends*/
			pst1=conn.prepareStatement(secondQuery);
			pst1.setInt(1,appId);
			pst1.setString(2,funcCode);
			pst1.setString(3,rs.getString("PA_NAME"));
			rs1= pst1.executeQuery();	
			JSONArray children = new JSONArray();
			
			while(rs1.next())
			{		
				 JSONObject json1 = new JSONObject();
				 /*Changed by Gangadhar Badagi for T25IT-227 starts*/
				 /*json1.put("id", rs1.getString("FLD_UID"));*/
				 json1.put("id", "Field_" + rs1.getString("FLD_SEQ_NO"));
				 /*Changed by Gangadhar Badagi for T25IT-227 ends*/
				 /*Jyoti : mandatory icon display start. TENJINCG-67*/
				    //json1.put("text", rs1.getString("FLD_UNAME"));
				    if(rs1.getString("MANDATORY").equalsIgnoreCase("Yes")){
				    	 json1.put("text", rs1.getString("FLD_UNAME")+"<font color='red'><b>*</b></font>");
				    }else{
				    	json1.put("text", rs1.getString("FLD_UNAME"));
				    }
				    /*Jyoti: mandatory icon display end. TENJINCG-67*/
				 JSONObject liAttr1 = new JSONObject();
				 	liAttr1.put("node_type","field");  
				 	liAttr1.put("uname", rs1.getString("FLD_UNAME"));
				    liAttr1.put("type", rs1.getString("FLD_UID_TYPE"));
				    liAttr1.put("mandatory", rs1.getString("MANDATORY"));
				    liAttr1.put("default_option", rs1.getString("FLD_DEFAULT_OPTIONS"));
				    liAttr1.put("object_class", rs1.getString("FLD_TYPE"));
				    /*Added by Padmavathi for TENJINCG-899 starts*/
				    liAttr1.put("userRemarks", rs1.getString("FLD_USER_REMARKS"));
				    /*Added by Padmavathi for TENJINCG-899 ends*/
				    liAttr1.put("text", json.get("text"));
				    liAttr1.put("type1", rs.getString("PA_IS_MRB"));
				    liAttr1.put("ismrb", rs.getString("PA_TYPE"));
				    
				    json1.put("li_attr", liAttr1);
				    
				    /*Jyoti: Fields type icon display Start. TENJINCG-65*/
				    /*Added by Roshni for Tenjincg-351 starts*/
				    if(rs1.getString("FLD_TYPE").equalsIgnoreCase("JSON Element")){
				    	json1.put("icon", "jstree/textfield.png");
				    }/*Added by Roshni for Tenjincg-351 ends*/
				    else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("TEXTBOX")){
				    	json1.put("icon", "jstree/textfield.png");
				    }else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("RADIO")){
				    	json1.put("icon", "jstree/radio.png");
				    }else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("CHECKBOX")){
				    	json1.put("icon", "jstree/checkbox.png");
				    }else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("BUTTON")){
				    	json1.put("icon", "jstree/button.png");
				    }else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("Table Column Selector")){
				    	json1.put("icon", "jstree/tablecolumnselector.png");
				    }else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("Table Column")
				    		|| rs1.getString("FLD_TYPE").equalsIgnoreCase("TABLE_COLUMN")){
				    	json1.put("icon", "jstree/column.png");
				    }else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("LIST")){
				    	json1.put("icon", "jstree/list.png");
				    }else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("TEXTAREA")){
				    	json1.put("icon", "jstree/textarea.png");
				    }else{
				    	json1.put("icon", "jstree/optional.png");
				    }
				    /*Jyoti: Fields type icon display end. TENJINCG-65*/
				
				children.put(json1);
			}
		    json.put("children",children);
		    allPages.put(json);
		}

	    
		jsonString = allPages.toString();
		return jsonString;
		}
		
		finally{
			
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
			}
		
	}
	
	//Added for T251IT-101 - Sriram
	public String getMetadataJSON(int appId, String apiCode, String operation) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		
		PreparedStatement pst = null;
		ResultSet rs = null;
	    PreparedStatement pst1 = null;
	    ResultSet rs1 = null;
		
		//Create JSON Arrays for REQUEST, RESPONSE and Special Pages (Resource Parameters, etc.)
		JSONArray requestPagesArray = new JSONArray();
		Map<String, JSONArray> responseJsonArrayMap = new HashMap<>(); //We're using a MAP here because we don't know how many different kinds of response codes we have for this API at the moment.
		JSONArray specialPagesArray = new JSONArray();
		
		try {
			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_PAGEAREAS WHERE PA_APP = ? AND PA_FUNC_CODE=? AND PA_API_OPERATION=? ORDER BY PA_SEQ_NO");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);
			pst.setString(3, operation);
			rs = pst.executeQuery();
			
			while(rs.next()) {
				//Loop through each page area, and create a JSON object for it.
				JSONObject json = new JSONObject();
				
			    json.put("text", rs.getString("PA_NAME")); 	
			    JSONObject liAttr = new JSONObject();

			    liAttr.put("node_type","page");
			    liAttr.put("ismrb", rs.getString("PA_IS_MRB"));
			    liAttr.put("type", rs.getString("PA_TYPE"));
			    json.put("icon", "jstree/page.png");
			    
			    
			    String entityType = rs.getString("PA_API_ENTITY_TYPE");
			    String responseCode = rs.getString("PA_API_RESP_TYPE");
			    String secondQuery ="select * from aut_func_fields where fld_app =? and fld_func_code=? and fld_page_area = ? and fld_api_operation=? and fld_api_entity_type=? and fld_api_resp_type=? order by fld_seq_no";
			    String fldEntityType = entityType;
			    if(Utilities.trim(rs.getString("PA_NAME")).equalsIgnoreCase("resource parameters")) {
			    	fldEntityType = entityType + "-RESOURCE-PARAMS";
			    }
			    liAttr.put("entityType", entityType);
			    liAttr.put("responseCode", responseCode);
			    json.put("li_attr", liAttr);
				pst1=conn.prepareStatement(secondQuery);
				pst1.setInt(1,appId);
				pst1.setString(2,apiCode);
				pst1.setString(3,rs.getString("PA_NAME"));
				pst1.setString(4, operation);
				pst1.setString(5, entityType);
				pst1.setString(6, responseCode);
				rs1= pst1.executeQuery();	
				JSONArray children = new JSONArray();
				
				while(rs1.next())
				{		
					JSONObject json1 = new JSONObject();
					//json1.put("id", "Field_" + rs1.getString("FLD_SEQ_NO"));
					//ID for all field nodes will be Field_<ENTITY_TYPE>_<RESP_STATUS_CODE>_<PA_SEQ_NO>_<FLD_SEQ_NO>
					json1.put("id", "Field_" + fldEntityType + "_" + responseCode + "_" + rs.getString("PA_SEQ_NO") + "_" + rs1.getString("FLD_SEQ_NO"));
					if(rs1.getString("MANDATORY").equalsIgnoreCase("Yes")){
						json1.put("text", rs1.getString("FLD_UNAME")+"<font color='red'><b>*</b></font>");
					}else{
						json1.put("text", rs1.getString("FLD_UNAME"));
					}
					JSONObject liAttr1 = new JSONObject();
					liAttr1.put("node_type","field");  
					liAttr1.put("uname", rs1.getString("FLD_UNAME"));
					liAttr1.put("type", rs1.getString("FLD_UID_TYPE"));
					liAttr1.put("mandatory", rs1.getString("MANDATORY"));
					liAttr1.put("default_option", rs1.getString("FLD_DEFAULT_OPTIONS"));
					liAttr1.put("object_class", rs1.getString("FLD_TYPE"));
					liAttr1.put("sequence_no", rs1.getString("FLD_SEQ_NO"));
					liAttr1.put("text", json.get("text"));
					liAttr1.put("type1", rs.getString("PA_IS_MRB"));
					liAttr1.put("ismrb", rs.getString("PA_TYPE"));

					json1.put("li_attr", liAttr1);
					if(rs1.getString("FLD_TYPE").equalsIgnoreCase("JSON Element")){
						json1.put("icon", "jstree/textfield.png");
					}
					else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("TEXTBOX")){
						json1.put("icon", "jstree/textfield.png");
					}else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("RADIO")){
						json1.put("icon", "jstree/radio.png");
					}else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("CHECKBOX")){
						json1.put("icon", "jstree/checkbox.png");
					}else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("BUTTON")){
						json1.put("icon", "jstree/button.png");
					}else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("Table Column Selector")){
						json1.put("icon", "jstree/tablecolumnselector.png");
					}else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("Table Column")
							|| rs1.getString("FLD_TYPE").equalsIgnoreCase("TABLE_COLUMN")){
						json1.put("icon", "jstree/column.png");
					}else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("LIST")){
						json1.put("icon", "jstree/list.png");
					}else if(rs1.getString("FLD_TYPE").equalsIgnoreCase("TEXTAREA")){
						json1.put("icon", "jstree/textarea.png");
					}else{
						json1.put("icon", "jstree/optional.png");
					}

					children.put(json1);
				}
				
				rs1.close();pst1.close();
			    json.put("children",children);
			    String pageClassifier = entityType;
			  //ID for all page nodes will be page_<ENTITY_TYPE>_<RESP_STATUS_CODE>_<PAGE_SEQ>
			    if(Utilities.trim(entityType).equalsIgnoreCase("request")) {
			    	if(Utilities.trim(rs.getString("PA_NAME")).equalsIgnoreCase("Resource Parameters")) {
			    		//THis is a special page area
			    		json.put("id", "page_REQUEST-RESOURCE-PARAMS"  + "_" + responseCode + "_" + rs.getString("PA_SEQ_NO"));
			    		json.getJSONObject("li_attr").put("group", "request_special");
			    		specialPagesArray.put(json);
			    	}else {
			    		//This is a request page area
			    		json.put("id", "page_" + pageClassifier + "_" + responseCode + "_" + rs.getString("PA_SEQ_NO"));
			    		json.getJSONObject("li_attr").put("group", "request");
			    		requestPagesArray.put(json);
			    	}
			    }else {
			    	//If we have already come across this status code, then put the page area json to the arraylist having the response code as key. Else, create a new list, and put the josn object in that list.
			    	if(responseJsonArrayMap.get(responseCode) == null) {
			    		responseJsonArrayMap.put(responseCode, new JSONArray());
			    	}
			    	json.put("id", "page_" + pageClassifier + "_" + responseCode + "_" + rs.getString("PA_SEQ_NO"));
			    	json.getJSONObject("li_attr").put("group", "response");
			    	responseJsonArrayMap.get(responseCode).put(json);
			    }
			    
			    
			    
			   
			}
		} catch (SQLException e) {
			logger.error("ERROR fetching API metadata", e);
			throw new DatabaseException("Could not fetch metadata due to an internal error. Please contact Tenjin Support.");
		} catch (JSONException e) {
			logger.error("ERROR fetching API metadata", e);
			throw new DatabaseException("Could not fetch metadata due to an internal error. Please contact Tenjin Support.");
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(conn);
		}
		
		//Now we'll build our final JSON for jsontree
		JSONArray allPages = new JSONArray();	
		
		try {
			//First, we add all the special pages to the tree under a parent node called "Request Parameters"
			JSONObject reqParamsObj = new JSONObject();
			reqParamsObj.put("id", "pages_req_params");
			reqParamsObj.put("text", "Request Parameters");
			JSONObject reqParamsAttr = new JSONObject();
			reqParamsAttr.put("node_type", "page_group");
			reqParamsAttr.put("type", "req_special");
			reqParamsObj.put("li_attr", reqParamsAttr);
			reqParamsObj.put("children", specialPagesArray);
			allPages.put(reqParamsObj);
			
			
			//Next, we create a parent node called "Request Body" and put all request page areas under it.
			JSONObject request = new JSONObject();
			request.put("id", "pages_request");
			request.put("text", "Request Body");
			JSONObject requestAttr = new JSONObject();
			requestAttr.put("type", "request");
			requestAttr.put("node_type", "page_group");
			request.put("li_attr", requestAttr);
			request.put("children", requestPagesArray);
			allPages.put(request);
			
			//Lastly, we create a parent node, one for each response code, and put all corresponding pages into it.
			for(String responseCode : responseJsonArrayMap.keySet()) {
				JSONObject response = new JSONObject();
				response.put("id", "pages_response");
				response.put("text", "Response[" + responseCode + "] - Body");
				JSONObject responseAttr = new JSONObject();
				responseAttr.put("node_type", "page_group");
				responseAttr.put("type", "response");
				response.put("li_attr", responseAttr);
				response.put("children", responseJsonArrayMap.get(responseCode));
				allPages.put(response);
			}
		} catch(JSONException e) {
			logger.error("ERROR - an unexpected error occurred while building tree.", e);
			throw new DatabaseException("Could not fetch metadata due to an internal error. Please contact Tenjin Support.");
		}
		
		return allPages.toString();
	}
	
	
	public ArrayList<Location> getFunctionPageAreas(int appId, String moduleCode)throws DatabaseException, SQLException, IntrospectionException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return null;
		}

	public ArrayList<TestObject> getFunctionFields(int appId,String moduleCode, String locationName) throws DatabaseException,SQLException, IntrospectionException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return null;
		}

	public ArrayList<Module> getAllModules(int appId)throws DatabaseException, SQLException, IntrospectionException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return null;
		}

	public ArrayList<Module> getSetOfModules(int appId, String[] moduleArray)throws DatabaseException, SQLException, IntrospectionException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return null;
		}

	public ArrayList<Aut> getAllAuts() throws DatabaseException, SQLException, IntrospectionException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return null;
		}

	public ArrayList<Aut> getSetOfAuts(int[] appIds) throws DatabaseException,SQLException, IntrospectionException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return null;}

	public Multimap<String, JSONObject> persistCompleteAut(JSONObject autJson,boolean importFlag,String status,String learnMode) throws DatabaseException, JSONException, IllegalArgumentException, SecurityException, IntrospectionException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return null;
		}

	

	public Multimap<String, JSONObject> persistSetOfModules(List<String> funcList, JSONObject autJson, boolean importFlag,String status,String learnMode)throws DatabaseException, JSONException, IllegalArgumentException, SecurityException, IntrospectionException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return null;
		}

	public int getFunctionCountofAllAuts() throws Exception {
		int count = 0;
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pstmt =null;
		PreparedStatement pstmt1 =null;
		ResultSet rs=null;
		ResultSet rs1 =null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			pstmt = conn.prepareCall("SELECT APP_ID FROM MASAPPLICATION");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				pstmt1 = conn.prepareCall("SELECT COUNT(*) as FUNC_COUNT FROM MASMODULE WHERE APP_ID = ?");
				pstmt1.setInt(1, rs.getInt("APP_ID"));
				rs1 = pstmt1.executeQuery();
				while (rs1.next()) {
					count += rs1.getInt("FUNC_COUNT");
				}
			}
			
		} catch (Exception e) {
			throw new DatabaseException("Error getting the function count: "+e.getMessage(), e);
		} finally {
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pstmt1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);
			
		}
		return count;
	}

	public int getFunctionCountofPartialAuts(int[] appIds) throws Exception {
		return currentFunctionCount;}

	

	public ArrayList<MetadataAudit> getAuditDetails(String type)throws Exception {
		ArrayList<MetadataAudit> audits = new ArrayList<MetadataAudit>();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try {
			pstmt = conn.prepareCall("SELECT * FROM TJN_AUDIT_METADATA WHERE AUDIT_ACTION = ?");
			pstmt.setString(1, type);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				MetadataAudit audit = new MetadataAudit();
				audit.setUser(rs.getString("AUDIT_USER_ID"));
				if (rs.getTimestamp("AUDIT_TIMESTAMP") != null) {
					String timeStamp = new SimpleDateFormat().format(rs.getTimestamp("AUDIT_TIMESTAMP"));
					audit.setDate(timeStamp);
				} else {
					audit.setDate(null);
				}
				audit.setType(rs.getString("AUDIT_ACTION"));
				audit.setStatus(rs.getString("STATUS"));
				audits.add(audit);
			}
		} catch (Exception e) {
			throw new DatabaseException("Error while persisting audit details: "+e.getMessage(),e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);
		}
		return audits;
	}

	public void persistExportAudit(MetadataAudit audit)throws DatabaseException, SQLException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pstmt =null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try {
			/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/

			pstmt = conn.prepareCall("INSERT INTO TJN_AUDIT_METADATA (AUDIT_TIMESTAMP,AUDIT_USER_ID,AUDIT_ACTION,STATUS) VALUES(?,?,?,?)");
			/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/


			pstmt.setTimestamp(1, new Timestamp(new Date().getTime()));
			pstmt.setString(2, audit.getUser());
			pstmt.setString(3, audit.getType());
			pstmt.setString(4, audit.getStatus());
			pstmt.execute();
		} catch (Exception e) {
			throw new DatabaseException("Error while persisting audit details: "+e.getMessage(),e);
		} finally {
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);
		}
	}

	public void overrideExistingFunctions(List<String> functions,String appName, JSONObject autJson,String status,String learnMode) throws DatabaseException,JSONException  {
		}

	
	public String getFunctionStatus(String functionCode, int appId) throws DatabaseException{
		String status = "";
		String query =  "select lrnr_status from lrnr_audit_trail where "
				+ "lrnr_id = (select max(la.lrnr_id) from lrnr_audit_trail  la where la.lrnr_func_code = ? and la.lrnr_app_id = ?)";
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try {
			pst = conn.prepareCall(query);
			pst.setString(1, functionCode);
			pst.setInt(2, appId);
			rs = pst.executeQuery();
			while(rs.next()){
				status = rs.getString("lrnr_status");
			}
		} catch (SQLException e) {
			
			logger.error(e.getMessage(), e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		if(status.equalsIgnoreCase("complete") || status.equalsIgnoreCase("imported")){
			status = "Y";
		}else{
			status = "N";
		}
		return status;
	}
	
	
	
	
	
	//TENJINCG-894 (Sriram)
	public TestObject hydrateTestObject(int appId, String functionCode, String pageAreaName, String fieldUniqueName) throws DatabaseException {
		TestObject t = null;
		
		String query = "SELECT A.*, B.FLD_USER_REMARKS FROM AUT_FUNC_FIELDS A LEFT JOIN AUT_FUNC_FIELD_INFO B ON "
				+ "A.FLD_APP = B.APP_ID AND A.FLD_FUNC_CODE=B.FUNC_CODE AND A.FLD_PAGE_AREA = B.PA_NAME AND A.FLD_UNAME = B.FLD_UNAME " + 
				"WHERE A.FLD_APP=? and a.fld_func_Code=? and a.fld_page_area=? and a.fld_uname=?";
		
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement(query);
				
			) {
			
			
			pst.setInt(1, appId);
			pst.setString(2, functionCode);
			pst.setString(3, pageAreaName);
			pst.setString(4, fieldUniqueName);
			
			try (ResultSet rs = pst.executeQuery()) {
				if(rs.next()) {
					t = new TestObject();
					t.setLabel(rs.getString("FLD_LABEL"));
					t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
					t.setUniqueId(rs.getString("FLD_UID"));
					t.setObjectClass(rs.getString("FLD_TYPE"));
					t.setLocation(rs.getString("FLD_PAGE_AREA"));
					t.setViewmode(rs.getString("VIEWMODE"));
					t.setMandatory(rs.getString("MANDATORY"));
					t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
					t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
					t.setGroup(rs.getString("FLD_GROUP"));
					t.setIsMultiRecord(rs.getString("FLD_IS_MULTI_REC"));
					t.setName(rs.getString("FLD_UNAME"));
					t.setSequence(rs.getInt("FLD_SEQ_NO"));
					t.setDefaultOptions(rs.getString("FLD_DEFAULT_OPTIONS"));
					t.setUserRemarks(rs.getString("FLD_USER_REMARKS"));
				}
			}
			
		} catch (SQLException e) {
			logger.error("Error while fetching Test object information", e);
			throw new DatabaseException("Could not fetch test object information", e);
		}
		
		return t;
	}
	
	public void updateFieldUserInfo(TestObject testObject, int appId, String functionCode) throws DatabaseException {
		String query = "UPDATE AUT_FUNC_FIELD_INFO SET FLD_USER_REMARKS=? WHERE APP_ID=? AND FUNC_CODE=? AND PA_NAME=? AND FLD_UNAME=?";
		String insQuery = "INSERT INTO AUT_FUNC_FIELD_INFO (APP_ID, FUNC_CODE, PA_NAME, FLD_UNAME, FLD_USER_REMARKS) VALUES (?,?,?,?,?)";
		
		try(
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP); 
				
			) {
			
			int updateCount = 0;
			try(PreparedStatement pst = conn.prepareStatement(query)) {
			
				pst.setString(1, Utilities.trim(testObject.getUserRemarks()));
				pst.setInt(2, appId);
				pst.setString(3, functionCode);
				pst.setString(4, testObject.getLocation());
				pst.setString(5, testObject.getName());
				updateCount = pst.executeUpdate();
			}
			
			if(updateCount < 1) {
				logger.debug("No existing record found for user info for this field. Creating a new one");
				try (PreparedStatement pst = conn.prepareStatement(insQuery)) {
					pst.setInt(1, appId);
					pst.setString(2, functionCode);
					pst.setString(3, testObject.getLocation());
					pst.setString(4, testObject.getName());
					pst.setString(5, testObject.getUserRemarks());
					pst.execute();
				}
			}
			
		} catch (SQLException e) {
			logger.error("Error updating user info for field",e );
			throw new DatabaseException("Could not update user information due to an internal error", e);
		}
	}
	//TENJINCG-894 (Sriram) ends
	
}