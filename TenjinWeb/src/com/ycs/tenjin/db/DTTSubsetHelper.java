/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DTTSubsetHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*25-09-2019				Ashiki				Newly added by TENJINCG-1101
*05-02-2020				Roshni					TENJINCG-1168

			
*/package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.defect.DTTSubset;
import com.ycs.tenjin.util.Constants;

public class DTTSubsetHelper {
	private static final Logger logger = LoggerFactory.getLogger(DTTSubsetHelper.class);
	
	public List<DTTSubset> hydrateAllDTTSubsets() throws DatabaseException {
		ArrayList<DTTSubset> subSetList = new ArrayList<DTTSubset>();
		DTTSubset subset = null;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT * from TJN_DTT_PROJECTS");)
		{
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					subset=buildSubsetObject(rs);
					subSetList.add(subset);
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch DTT Subset List",e);
			throw new DatabaseException("Could not fetch DTT Subset List due to an internal error",e);
		}
		return subSetList;
	}
	/* modified by Roshni for TENJINCG-1168 starts */
	/*public DTTSubset persistDefectManagementProjects(DTTSubset subset) throws DatabaseException {*/
	public DTTSubset persistDefectManagementProjects(Connection conn,DTTSubset subset) throws DatabaseException {

		/*try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("INSERT INTO TJN_DTT_PROJECTS (PRJ_ID,PROJECT_SET_NAME,INSTANCE,PROJECTS,INSTANCE_NAME,USER_ID)  VALUES(?,?,?,?,?,?)");)
		{*/
	/* modified by Roshni for TENJINCG-1168 ends */
		try(PreparedStatement pst=conn.prepareStatement("INSERT INTO TJN_DTT_PROJECTS (PRJ_ID,PROJECT_SET_NAME,INSTANCE,PROJECTS,INSTANCE_NAME,USER_ID)  VALUES(?,?,?,?,?,?)");)
		{
			int prjId=DatabaseHelper.getGlobalSequenceNumber(conn);
			pst.setInt(1, prjId);
			pst.setString(2,subset.getSubSetName());
			pst.setString(3, subset.getInstance());
			pst.setString(4,subset.getProjects() );
			pst.setString(5,subset.getInstanceName());
			pst.setString(6,subset.getUserId());
			pst.execute();
			subset.setPrjId(prjId);
		}catch(DatabaseException e){
			logger.error("ERROR Defect Managements Projects", e);
			throw e;
		}catch(Exception e){
			logger.error("Could not persist DTT Subset ",e);
			throw new DatabaseException("Could not insert Defect Managements Projects due to an internal error",e);
		}
		return subset;
	
	}
	

	public void deleteSubset(String[] recIds) throws DatabaseException {
		/*modified by shruthi for sql injection starts*/
		
		for(String str : recIds) {
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("DELETE TJN_DTT_PROJECTS WHERE PRJ_ID=?");)
		{
			pst.setString(1, str);
			pst.executeUpdate();
		} catch (SQLException e) {
			logger.error("ERROR removing Subset",e );
			throw new DatabaseException("Could not delete Subset due to an internal error.", e);
		}
		}
		/*modified by shruthi for sql injection ends*/
	}
	
	public DTTSubset hydrateDTTSubset(int recId) throws DatabaseException {

		DTTSubset subset = null;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT * from TJN_DTT_PROJECTS WHERE PRJ_ID=?");)
		{
			pst.setInt(1, recId);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					subset=buildSubsetObject(rs);
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch DTT Subset",e);
			throw new DatabaseException("Could not fetch DTT Subset due to an internal error",e);
		}
		return subset;
	
	}
	
	private DTTSubset buildSubsetObject(ResultSet rs) throws SQLException {
		DTTSubset subset=new DTTSubset() ;
		subset.setPrjId(rs.getInt("PRJ_ID"));
		subset.setSubSetName(rs.getString("PROJECT_SET_NAME"));
		subset.setInstance(rs.getString("INSTANCE"));
		subset.setProjects(rs.getString("PROJECTS"));
		subset.setInstanceName(rs.getString("INSTANCE_NAME"));
		return subset;
	}

	public DTTSubset updateDefectManagementProjects(DTTSubset upadateSubset) throws DatabaseException {


		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("UPDATE TJN_DTT_PROJECTS SET PROJECT_SET_NAME=?,PROJECTS=?,USER_ID= ? WHERE PRJ_ID=?");)
		{
			
			pst.setString(1,upadateSubset.getSubSetName());
			pst.setString(2,upadateSubset.getProjects() );
			pst.setString(3,upadateSubset.getUserId());
			pst.setInt(4,upadateSubset.getPrjId());
			pst.execute();
		}catch(DatabaseException e){
			logger.error("ERROR Defect Managements Projects", e);
			throw e;
		}catch(Exception e){
			logger.error("Could not update Defect Managements Project user",e);
			throw new DatabaseException("Could not update Defect Managements Projects due to an internal error",e);
		}
		return upadateSubset;
	}

	public boolean mappedSubset(String[] subsetName) throws DatabaseException {
		boolean mappedSubset=false;
		/*modified by shruthi for sql injection starts*/
		
		for(String str : subsetName) {
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT * FROM TJN_PRJ_AUTS WHERE PROJECT_SET_NAME=?");)
		{
			pst.setString(1, str);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					mappedSubset=true;
				}
			}
			
		} catch (SQLException e) {
			logger.error("ERROR mapping Subset",e );
			throw new DatabaseException("Could not map Subset due to an internal error.", e);
		}
		}
		/*modified by shruthi for sql injection ends*/
		return mappedSubset;
	}

	public List<String> fetchAllInstances() throws DatabaseException {

		List<String> instances = new ArrayList<String>();
		String instance = "";
		String tool="alm";
		/*modified by shruthi for sql injection starts*/
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT INSTANCE_NAME FROM tjn_def_tool_master WHERE TOOL_NAME != ?");)
		{
			pst.setString(1, tool);
		/*modified by shruthi for sql injection ends*/
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					instance = rs.getString("INSTANCE_NAME");
					instances.add(instance);
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch DTT instance",e);
			throw new DatabaseException("Could not fetch DTT instance due to an internal error",e);
		}
		return instances;
	
	
	}
	/* modified by Roshni for TENJINCG-1168 starts */
	/*public boolean subsetExist(DTTSubset persistSubset) throws DatabaseException {*/
	public boolean subsetExist(Connection conn,DTTSubset persistSubset) throws DatabaseException {

		boolean instance = false;
		
		try(PreparedStatement pst=conn.prepareStatement("SELECT * from TJN_DTT_PROJECTS WHERE PROJECT_SET_NAME=?");)
		{
			pst.setString(1, persistSubset.getSubSetName());
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					instance=true;
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch DTT instance",e);
			throw new DatabaseException("Could not fetch DTT instance due to an internal error",e);
		}
		return instance;
	
	
	
	}

}
