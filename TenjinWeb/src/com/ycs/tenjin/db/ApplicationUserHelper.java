/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApplicationUserHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              	DESCRIPTION
* 27-10-2018			Ashiki					Newly added for TENJINCG-846
* 17-12-2018            Leelaprasad       		for TJN262R2-15,16
* 22-03-2019			Preeti					TENJINCG-1018
* 22-04-2019            Padmavathi              changed done for SQLSever DB
* 03-07-2019			Ashiki					TJN27-23
* 04-07-2019			Ashiki					TV2.8R2-19
* 22-06-2021            Paneendra               VAPT FIX
* */

package com.ycs.tenjin.db;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.ApplicationUser;
import com.ycs.tenjin.util.Constants;

public class ApplicationUserHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(ApplicationUserHelper.class);
	
	
	public ArrayList<ApplicationUser> hydrateAllApplicationUsers(String UserId) throws DatabaseException{
		
		ArrayList<ApplicationUser> applicationUsers = new ArrayList<ApplicationUser>();
		ApplicationUser applicationUser = null;
		/*modified by shruthi for VAPT Helper fix starts*/
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT A.*,B.APP_NAME AS APP_NAME FROM USRAUTCREDENTIALS A, MASAPPLICATION B WHERE A.APP_ID = B.APP_ID AND A.USER_ID =?");)
		{
			pst.setString(1,UserId);
			/*modified by shruthi for VAPT Helper fix ends*/
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					applicationUser=buildApplicationUserObject(rs);
					applicationUser.setAppName(rs.getString("APP_NAME"));
					applicationUsers.add(applicationUser);
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch selected application user",e);
			throw new DatabaseException("Could not fetch selected application user due to an internal error",e);
		}
		return applicationUsers;
	}

	public ArrayList<ApplicationUser> hydrateAllApplicationUsers(String userId,int appId) throws DatabaseException {
		ArrayList<ApplicationUser> applicationUsers = new ArrayList<ApplicationUser>();
		ApplicationUser applicationUser =null;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT  A.*,B.APP_NAME AS APP_NAME FROM USRAUTCREDENTIALS A, MASAPPLICATION B WHERE A.APP_ID = B.APP_ID AND LOWER(A.USER_ID) = LOWER(?) AND A.APP_ID =?");)
		{
			pst.setString(1, userId);
			pst.setInt(2, appId);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					applicationUser=buildApplicationUserObject(rs);
					applicationUser.setAppName(rs.getString("APP_NAME"));
					applicationUsers.add(applicationUser);
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch selected application user",e);
			throw new DatabaseException("Could not fetch selected application user due to an internal error",e);
		}
		return applicationUsers;
	
	}
	public String hydrateAllApplicationUserType(int appId) throws DatabaseException {
		String loginTypes="";
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT * FROM MASAPPLICATION WHERE APP_ID = ?");)
		{
			
			pst.setInt(1, appId);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					loginTypes=rs.getString("APP_USERTYPES");
					
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch selected application user",e);
			throw new DatabaseException("Could not fetch selected application user due to an internal error",e);
		}
		return loginTypes;
	
	}
	public ApplicationUser hydrateAllApplicationUsers(int appId,String userId,String loginType) throws DatabaseException{
		ApplicationUser applicationUser=null;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT * FROM USRAUTCREDENTIALS WHERE APP_ID =? AND USER_ID = ? and APP_USER_TYPE =?");)
		{
			pst.setInt(1, appId);
			pst.setString(2, userId);
			pst.setString(3,loginType);
			try(ResultSet rs= pst.executeQuery();){
				
				while (rs.next()) {
					applicationUser = buildApplicationUserObject(rs);
				}
				
			}
		}
		catch(Exception e){
			
			logger.error("Could not fetch selected application user",e);
			throw new DatabaseException("Could not fetch selected application user due to an internal error",e);
		}
		return applicationUser;
}


	public void persistApplicationUser(ApplicationUser applicationUser) throws DatabaseException {
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("INSERT INTO USRAUTCREDENTIALS (UAC_ID,USER_ID,APP_ID,APP_LOGIN_NAME,APP_PASSWORD,APP_USER_TYPE,APP_TXN_PWD)  VALUES(?,?,?,?,?,?,?)");)
		{
			int uacId=DatabaseHelper.getGlobalSequenceNumber(conn);
			pst.setInt(1, uacId);
			pst.setString(2,applicationUser.getUserId() );
			pst.setInt(3,applicationUser.getAppId());
			pst.setString(4, applicationUser.getLoginName());
			pst.setString(5, applicationUser.getPassword());
			pst.setString(6, applicationUser.getLoginUserType());
			pst.setString(7, applicationUser.getTxnPassword());
			/*Changed by Leelaprasad for the TJN26R2-15,TJN26R2-16 starts*/
			/*pst.executeQuery();*/
			pst.execute();
			/*Changed by Leelaprasad for the TJN26R2-15,TJN26R2-16 ends*/
			applicationUser.setUacId(uacId);
		}catch(DatabaseException e){
			logger.error("ERROR persisting Application User", e);
			throw e;
		}catch(Exception e){
			logger.error("Could not Create application user",e);
			throw new DatabaseException("Could not insert application user details due to an internal error",e);
		}
	}


	public ApplicationUser hydrateApplicationUser(int uacId) throws DatabaseException {
		ApplicationUser applicationUser = new ApplicationUser();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT * FROM USRAUTCREDENTIALS WHERE UAC_ID=?");)
		{
			pst.setInt(1, uacId);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					applicationUser = buildApplicationUserObject(rs);
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch selected application user",e);
			throw new DatabaseException("Could not fetch Application User  due to an internal error",e);
		}
		return applicationUser;
	}
	
	

	public ApplicationUser update(ApplicationUser applicationUser,String userId) throws DatabaseException {
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				// Code added for OAuth 2.0 requirement TENJINCG-1018- Sunitha  Starts
				PreparedStatement pst=conn.prepareStatement("UPDATE USRAUTCREDENTIALS SET USER_ID=?,APP_ID=?, APP_LOGIN_NAME=?,APP_PASSWORD=?, APP_USER_TYPE=?, APP_TXN_PWD=?, OAUTH_ACCESS_TOKEN=?, OAUTH_REFRESH_TOKEN=?, OAUTH_TOKENGEN_TIMESTAMP=?, OAUTH_TOKEN_VALIDITY=? WHERE UAC_ID=?");)
				//Code added for OAuth 2.0 requirement TENJINCG-1018- Sunitha  Ends
			{
			pst.setString(1, userId);
			pst.setInt(2,applicationUser.getAppId());
			pst.setString(3, applicationUser.getLoginName());
			pst.setString(4, applicationUser.getPassword());
			pst.setString(5, applicationUser.getLoginUserType());
			pst.setString(6, applicationUser.getTxnPassword());
			// Code added for OAuth 2.0 requirement TENJINCG-1018- Sunitha  Starts
			pst.setString(7, applicationUser.getAccessToken());
			pst.setString(8, applicationUser.getRefreshToken());
			pst.setTimestamp(9, applicationUser.getTokenGenTime());
			pst.setString(10, applicationUser.getTokenValidity());
			pst.setInt(11, applicationUser.getUacId());
			//Code added for OAuth 2.0 requirement TENJINCG-1018- Sunitha  Ends
			/*Modified by Padmavathi for SQLSever DB starts*/
			/*pst.executeQuery();*/
			pst.execute();
			/*Modified by Padmavathi for SQLSever DB ends*/
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new DatabaseException("Could not update Application User because of an internal error ");
		}
		return applicationUser;
	}


	

	/*modified by shruthi for sql injection starts*/
	public void deleteApplicationUser(String[] uacIds) throws DatabaseException {
		
		for(String str : uacIds) {
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("DELETE USRAUTCREDENTIALS WHERE UAC_ID=?");)
		{
			/*Changed by Ashiki for TV2.8R2-19 starts*/
			pst.setString(1, str);
			pst.executeUpdate();
			/*Changed by Ashiki for TV2.8R2-19 ends*/
		} catch (SQLException e) {
			logger.error("ERROR removing Application User",e );
			throw new DatabaseException("Could not delete Application user due to an internal error.", e);
		}
		}
	}
	/*modified by shruthi for sql injection ends*/
	

	public void deleteApplicationUser(int appId, String userId,String type) throws DatabaseException {
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("DELETE USRAUTCREDENTIALS  WHERE APP_ID =? AND USER_ID = ? and APP_USER_TYPE =?");)
		{	
			pst.setInt(1, appId);
			pst.setString(2, userId);
			pst.setString(3, type);
			/*Changed by Ashiki for TV2.8R2-19 starts*/
			/*pst.executeQuery();*/
			pst.executeUpdate();
			/*Changed by Ashiki for TV2.8R2-19 ends*/
		} catch (SQLException e) {
			logger.error("ERROR removing Application User",e );
			throw new DatabaseException("Could not delete Application User due to an internal error.", e);
		}
		
	}

	private ApplicationUser buildApplicationUserObject(ResultSet rs) throws SQLException{
		ApplicationUser applicationUser=new ApplicationUser();
		applicationUser.setUserId(rs.getString("USER_ID"));
		applicationUser.setAppId(rs.getInt("APP_ID"));
		applicationUser.setLoginName(rs.getString("APP_LOGIN_NAME"));
		applicationUser.setPassword(rs.getString("APP_PASSWORD"));
		applicationUser.setLoginUserType(rs.getString("APP_USER_TYPE"));
		applicationUser.setTxnPassword(rs.getString("APP_TXN_PWD"));
		applicationUser.setUacId(rs.getInt("UAC_ID"));
		//Code changes by Sunitha for OAuth 2.0 requirement TENJINCG-1018- Starts
		applicationUser.setAccessToken(rs.getString("OAUTH_ACCESS_TOKEN"));
		applicationUser.setRefreshToken(rs.getString("OAUTH_REFRESH_TOKEN"));
		applicationUser.setTokenValidity(rs.getString("OAUTH_TOKEN_VALIDITY"));
		applicationUser.setTokenGenTime(rs.getTimestamp("OAUTH_TOKENGEN_TIMESTAMP"));
		//Code changes by Sunitha for OAuth 2.0 requirement TENJINCG-1018- Ends
		return applicationUser;
		
	}

	/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Starts*/
	public ApplicationUser hydrateAccessTokenRecord(int uacId) throws DatabaseException {
		ApplicationUser record = new ApplicationUser();
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("SELECT APP_LOGIN_NAME, APP_PASSWORD, APP_USER_TYPE, OAUTH_ACCESS_TOKEN, OAUTH_REFRESH_TOKEN, OAUTH_TOKEN_VALIDITY, OAUTH_TOKENGEN_TIMESTAMP FROM USRAUTCREDENTIALS WHERE UAC_ID=?");
			){
			
			pst.setInt(1, uacId);
			
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					record = this.buildAccessTokenObject(rs);
				}
			}
			
		} catch(SQLException e) {
			logger.error("Could not fetch access token record");
			throw new DatabaseException("Could not fetch access token record",e);
		}
		record.setUacId(uacId);
		return record;
			
	}
	
	public ApplicationUser persistAccessToken(ApplicationUser record) throws DatabaseException {
		
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
					PreparedStatement pst = conn.prepareStatement("SELECT OAUTH_ACCESS_TOKEN, OAUTH_REFRESH_TOKEN, OAUTH_TOKENGEN_TIMESTAMP, APP_ID, USER_ID, UAC_ID FROM USRAUTCREDENTIALS WHERE UAC_ID = ?");
					PreparedStatement pst1 = conn.prepareStatement("UPDATE USRAUTCREDENTIALS SET OAUTH_ACCESS_TOKEN=?, OAUTH_REFRESH_TOKEN=?, OAUTH_TOKENGEN_TIMESTAMP=?, OAUTH_TOKEN_VALIDITY=? WHERE UAC_ID=?");
				){
				
				pst.setInt(1, record.getUacId());
				boolean isNewToken = false;
				try (ResultSet rs = pst.executeQuery()){
					while(rs.next()){
						String accessToken = rs.getString("OAUTH_ACCESS_TOKEN");
						if(rs.getString("OAUTH_ACCESS_TOKEN") == null || accessToken.isEmpty()){
							isNewToken = true;
						}
					}
				}
				
				if(!isNewToken){
					persistoAuthAudit(pst);
				}
			
				pst1.setString(1, record.getAccessToken());
				pst1.setString(2, record.getRefreshToken());
				pst1.setTimestamp(3, record.getTokenGenTime());
				pst1.setString(4, record.getTokenValidity());
				pst1.setInt(5, record.getUacId());
				pst1.execute();
		}catch(SQLException e) {
			logger.error("Could not update AUT user record for access token");
			throw new DatabaseException("Could not update AUT user record for access token",e);
		}
		return record;
	}
	
	
	
	private ApplicationUser buildAccessTokenObject(ResultSet rs) throws SQLException {
		ApplicationUser record = new ApplicationUser();
		record.setAccessToken(rs.getString("OAUTH_ACCESS_TOKEN"));
		record.setRefreshToken(rs.getString("OAUTH_REFRESH_TOKEN"));
		record.setTokenValidity(rs.getString("OAUTH_TOKEN_VALIDITY"));
		record.setTokenGenTime(rs.getTimestamp("OAUTH_TOKENGEN_TIMESTAMP"));
		record.setLoginName(rs.getString("APP_LOGIN_NAME"));
		record.setPassword(rs.getString("APP_PASSWORD"));
		record.setLoginUserType(rs.getString("APP_USER_TYPE"));
		return record;
		
	}
	
	public void persistoAuthAudit(PreparedStatement pst) throws DatabaseException {
		try(
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst2 = conn.prepareStatement("INSERT INTO TJN_OAUTH_AUDIT_TRAIL (UTKN_ID, ACCESS_TOKEN, TKN_GENERATED_BY, REFRESH_TOKEN, APP_ID, GENERATE_TIME, UPDATE_TIME, UAC_ID) VALUES (?,?,?,?,?,?,?,?)");
				PreparedStatement pst3 = conn.prepareStatement("SELECT coalesce(MAX(UTKN_ID),0) AS UTKN_ID FROM TJN_OAUTH_AUDIT_TRAIL");
			){
						
			//GEtting the max UTKN_ID
			int tknId = 0;
			try (ResultSet rs3 = pst3.executeQuery()){
				while(rs3.next()){
					tknId = rs3.getInt("UTKN_ID");
				}
			}
			++tknId;
			//Inserting the record
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					pst2.setInt(1, tknId);
					pst2.setString(2, rs.getString("OAUTH_ACCESS_TOKEN"));
					pst2.setString(3, rs.getString("USER_ID"));
					pst2.setString(4, rs.getString("OAUTH_REFRESH_TOKEN"));
					pst2.setInt(5, rs.getInt("APP_ID"));
					pst2.setTimestamp(6, rs.getTimestamp("OAUTH_TOKENGEN_TIMESTAMP"));
					pst2.setTimestamp(7, new Timestamp(new Date().getTime()));
					pst2.setInt(8, rs.getInt("UAC_ID"));
					pst2.execute();
				}
			}
		}catch(SQLException e) {
			logger.error("Could not insert OAUTH_AUDIT user record for access token");
			throw new DatabaseException("Could not insert OAUTH_AUDIT user record for access token",e);
		}
	}
	/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Ends*/
	
	/*Added by Ashiki for TJN27-23 starts*/  
	public ApplicationUser hydrateApplicationUserType(int appId,String loginType) throws DatabaseException {
				ApplicationUser applicationUser = null;
				try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
						PreparedStatement pst=conn.prepareStatement("SELECT * FROM USRAUTCREDENTIALS WHERE APP_ID = ? and APP_USER_TYPE=?");)
				{
					pst.setInt(1, appId);
					pst.setString(2, loginType);
					try(ResultSet rs= pst.executeQuery();){
						while(rs.next()){
							applicationUser=buildApplicationUserObject(rs);
						}
					}
				}
				catch(Exception e){
					logger.error("Could not fetch selected application user",e);
					throw new DatabaseException("Could not fetch selected application user due to an internal error",e);
				}
				return applicationUser;

	}
	/*Added by Ashiki for TJN27-23 ends*/  
}
