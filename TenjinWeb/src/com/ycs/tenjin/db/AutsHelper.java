/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AutsHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

  *//******************************************
  * CHANGE HISTORY
  * ==============
  *
  * DATE                 CHANGED BY              DESCRIPTION
  * 26-Oct-2017         Padmavathi              Newly Added For TENJINCG-400
  * 16-03-2018			Preeti					TENJINCG-73
  * 26-03-2018			Pushpalatha				TENJINCG-611
  * 26-Nov 2018			Shivam	Sharma			TENJINCG-904
  * 17-12-2018          Padmavathi              TJN252-36
  * 25-02-2019			Ashiki					TENJINCG-954
  * 22-03-2019			Preeti					TENJINCG-1018
  * 27-03-2019			Ashiki					TENJINCG-1004
  * 11-04-2019          Padmavathi              TNJN27-10
	25-04-2019			Roshni					TENJINCG-1038
	24-10-2019			Roshni					TENJINCG-1119
	03-12-2019			Roshni					TENJINCG-1168
  */

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

public class AutsHelper {

	private static final Logger logger = LoggerFactory.getLogger(AutsHelper.class);

	public void updateAut(Aut aut,int appId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst=null;
		PreparedStatement pst1=null;
		try {
			logger.debug("Updating Aut {} ", aut.getId());
			/*Modified by Preeti for TENJINCG-73 starts*/
			/*pst = conn.prepareStatement("UPDATE MASAPPLICATION SET APP_URL = ?,APP_NAME=?,APP_LOGIN_NAME = ?,APP_PASSWORD = ?,APP_DEF_BROWSER=?, APP_ADAPTER_PACKAGE=?,APP_OPERATIONS=?,APP_USERTYPES=? ,APP_DATE_FORMAT=?,API_BASE_URL=?,APP_PLATFORM_VERSION=?,APP_PACKAGE=?,APP_ACTIVITY=? where APP_ID = ?");*/
			/*pst = conn.prepareStatement("UPDATE MASAPPLICATION SET APP_URL = ?,APP_NAME=?,APP_LOGIN_NAME = ?,APP_PASSWORD = ?,APP_DEF_BROWSER=?, APP_ADAPTER_PACKAGE=?,APP_OPERATIONS=?,APP_USERTYPES=? ,APP_DATE_FORMAT=?,API_BASE_URL=?,APP_PLATFORM_VERSION=?,APP_PACKAGE=?,APP_ACTIVITY=?,APP_PAUSE_TIME=?,APP_PAUSE_LOCATION=? where APP_ID = ?");*/
			/*Modified by Preeti for TENJINCG-73 ends*/
			/*Modified by Roshni for TENJINCG-1038 starts*/
			pst = conn.prepareStatement("UPDATE MASAPPLICATION SET APP_URL = ?,APP_NAME=?,APP_LOGIN_NAME = ?,APP_PASSWORD = ?,APP_DEF_BROWSER=?, APP_ADAPTER_PACKAGE=?,APP_OPERATIONS=?,APP_USERTYPES=? ,APP_DATE_FORMAT=?,API_BASE_URL=?,APP_PLATFORM_VERSION=?,APP_PACKAGE=?,APP_ACTIVITY=?,APP_PAUSE_TIME=?,APP_PAUSE_LOCATION=?,TEMPLATE_PASSWORD=? where APP_ID = ?");
			/*Modified by Roshni for TENJINCG-1038 ends*/
			pst.setString(1, aut.getURL());
			pst.setString(2, aut.getName());
			pst.setString(3, "");
			pst.setString(4, "");
			pst.setString(5, aut.getBrowser());
			pst.setString(6, aut.getAdapterPackage());
			pst.setString(7, aut.getOperation());
			pst.setString(8, aut.getLoginUserType());
			pst.setString(9, aut.getDateFormat());
			pst.setString(10, aut.getApiBaseUrl());
			pst.setString(11, aut.getPlatformVersion());
			pst.setString(12, aut.getApplicationPackage());
			pst.setString(13, aut.getApplicationActivity());
			/*Added by Preeti for TENJINCG-73 starts*/
			pst.setInt(14, aut.getPauseTime());
			pst.setString(15, aut.getPauseLocation());
			/*Added by Preeti for TENJINCG-73 ends*/
			/*pst.setInt(16, appId);*/
			/*changed by Roshni for TENJINCG-1038 starts*/
			if(aut.getTemplatePwd()!=null&&!aut.getTemplatePwd().equalsIgnoreCase(""))
				/*Modified by paneendra for VAPT FIX starts*/
				/*pst.setString(16, new Crypto().encrypt(aut.getTemplatePwd()));*/
			    pst.setString(16, new CryptoUtilities().encrypt(aut.getTemplatePwd()));
			    /*Modified by paneendra for VAPT FIX ends*/
			else
				pst.setString(16, "");
			pst.setInt(17, appId);
			/*changed by Roshni for TENJINCG-1038 ends*/
			pst.execute();
			if(aut.getDateFormat()!=null){
				pst1=conn.prepareStatement("UPDATE MASMODULE SET APP_DATE_FORMAT=? WHERE APP_ID = ? ");
				pst1.setString(1, aut.getDateFormat());
				pst1.setInt(2, aut.getId());
				pst1.execute();
			}

		} catch (Exception e) {
			logger.debug("error updating AUT",e);
			throw new DatabaseException("Could not update AUT",e);
		}finally{
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	/*modified by shruthi for VAPT helper fixes starts*/
	public Aut hydrateAut(Connection conn, int appId) throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		Aut aut = null;
		/*Statement st=null;*/
		PreparedStatement pst = null;
		ResultSet rs=null;

		try {
			logger.debug("hydrating AUT" +appId);
			/*st = conn.createStatement();
			rs = st.executeQuery("SELECT * FROM MASAPPLICATION M left outer join TJN_ADAPTER A  on M.APP_ADAPTER_PACKAGE = A.NAME WHERE APP_ID = "+ appId);*/
			pst = conn.prepareStatement("SELECT * FROM MASAPPLICATION M left outer join TJN_ADAPTER A  on M.APP_ADAPTER_PACKAGE = A.NAME WHERE APP_ID =?");
			pst.setInt(1, appId);
			rs = pst.executeQuery();
			while (rs.next()) {
				aut=this.mapFields(rs); 
				aut.setAdapterPackageOrTool(rs.getInt("OR_TOOL")); 
			}
			if(aut!=null){
				/* modified by Roshni for TENJINCG-1168 starts */
				ArrayList<ModuleBean> modules = new ModulesHelper().hydrateModules(conn,aut.getId());
				/* modified by Roshni for TENJINCG-1168 ends */
				aut.setFunctions(modules);
				logger.debug("done");
			}
		} catch (Exception e) {
			logger.debug("error fecthing AUTS",e);
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		return aut;
	}
	/*modified by shruthi for VAPT helper fixes ends*/
	public ArrayList<Aut> hydrateAllAut() throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			ArrayList<Aut> autList = this.hydrateAllAut(conn);
			return autList;
		}finally {
			DatabaseHelper.close(conn);
		}
	}
	public ArrayList<Aut> hydrateAllAut(Connection conn) throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<Aut> autList = null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {
			logger.debug("hydrating AUTs");
			pst = conn.prepareStatement("SELECT * FROM MASAPPLICATION");
			rs = pst.executeQuery();
			autList = new ArrayList<Aut>();
			while (rs.next()) {
				Aut aut=this.mapFields(rs);
				autList.add(aut);
			}
			logger.debug("done");
		} catch (Exception e) {
			logger.debug("error fecthing AUTS",e);
			throw new DatabaseException("Could not fetch Applications Under Test", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);

		}

		return autList;
	}
	/*public Aut persistAut(Aut aut) throws DatabaseException {*/
	public Aut persistAut(Connection conn,Aut aut) throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		PreparedStatement pst=null;
		try {
			logger.debug("persisting AUT "+aut.getId());
			/*Modified by Preeti for TENJINCG-73 starts*/
			/*pst = conn.prepareStatement("INSERT INTO MASAPPLICATION (APP_ID,APP_NAME,APP_URL,APP_LOGIN_REQD,APP_LOGIN_NAME,APP_PASSWORD,APP_DEF_BROWSER,APP_ADAPTER_PACKAGE,APP_OPERATIONS,APP_USERTYPES,API_BASE_URL,APP_DATE_FORMAT,APP_PLATFORM,APP_PLATFORM_VERSION,APP_PACKAGE,APP_ACTIVITY,APP_TYPE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");*/
			/*pst = conn.prepareStatement("INSERT INTO MASAPPLICATION (APP_ID,APP_NAME,APP_URL,APP_LOGIN_REQD,APP_LOGIN_NAME,APP_PASSWORD,APP_DEF_BROWSER,APP_ADAPTER_PACKAGE,APP_OPERATIONS,APP_USERTYPES,API_BASE_URL,APP_DATE_FORMAT,APP_PLATFORM,APP_PLATFORM_VERSION,APP_PACKAGE,APP_ACTIVITY,APP_TYPE,APP_PAUSE_TIME,APP_PAUSE_LOCATION) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");*/
			/*Modified by Preeti for TENJINCG-73 ends*/
			/*Modified by Roshni for TENJINCG-1038 starts*/
			pst = conn.prepareStatement("INSERT INTO MASAPPLICATION (APP_ID,APP_NAME,APP_URL,APP_LOGIN_REQD,APP_LOGIN_NAME,APP_PASSWORD,APP_DEF_BROWSER,APP_ADAPTER_PACKAGE,APP_OPERATIONS,APP_USERTYPES,API_BASE_URL,APP_DATE_FORMAT,APP_PLATFORM,APP_PLATFORM_VERSION,APP_PACKAGE,APP_ACTIVITY,APP_TYPE,APP_PAUSE_TIME,APP_PAUSE_LOCATION,TEMPLATE_PASSWORD) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			/*Modified by Roshni for TENJINCG-1038 ends*/
			int appId = DatabaseHelper.getGlobalSequenceNumber(conn);
			pst.setInt(1, appId);
			pst.setString(2, aut.getName());
			pst.setString(3, aut.getURL());
			pst.setString(4, "YES");
			pst.setString(5, "");
			pst.setString(6, "");
			pst.setString(7, aut.getBrowser());
			pst.setString(8, aut.getAdapterPackage());
			/*Modified By Ashiki for TENJINCG-954 starts*/
			/*pst.setString(9, "");*/
			pst.setString(9, aut.getOperation());
			/*pst.setString(10, "Maker,Checker");*/
			pst.setString(10, aut.getLoginUserType());
			/*Modified By Ashiki for TENJINCG-954 ends*/
			if(Utilities.trim(aut.getApiBaseUrl()).length() <1) {
				pst.setString(11, aut.getURL());
			}else{
				pst.setString(11, aut.getApiBaseUrl());
			}
			pst.setString(12, aut.getDateFormat());
			pst.setString(13, aut.getPlatform());
			pst.setString(14,aut.getPlatformVersion());
			pst.setString(15, aut.getApplicationPackage());
			pst.setString(16, aut.getApplicationActivity());
			pst.setInt(17, aut.getApplicationType());
			/*Added by Preeti for TENJINCG-73 starts*/
			pst.setInt(18, aut.getPauseTime());
			pst.setString(19, aut.getPauseLocation());
			/*Added by Preeti for TENJINCG-73 ends*/
			/*Added by Roshni for TENJINCG-1038 starts*/
			/*Modified by Roshni for TENJINCG-1119 starts*/
			/*pst.setString(20, (new Crypto().encrypt(aut.getTemplatePwd())));*/
			pst.setString(20, aut.getTemplatePwd());
			/*Added by Roshni for TENJINCG-1038 ends*/
			/*Modified by Roshni for TENJINCG-1119 ends*/

			pst.executeUpdate();
			aut.setId(appId);
			logger.debug("done");
		} catch (Exception e) {
			logger.error("Could not create record due to "+e.getMessage());
			throw new DatabaseException("Could not create record", e);
		} finally {
			
			DatabaseHelper.close(pst);
		}

		return aut;
	}
	public void clearAut(String appid) throws DatabaseException {

		try {
			this.clearAllApp(new String[] {appid});
		} catch (Exception e) {
			throw e;
		}

	}
	/*modified by shruthi for sql injection starts*/
	public void clearAllApp(String[] appid) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		
		for(String str : appid) {
		try {

			pst = conn.prepareStatement("DELETE FROM MASAPPLICATION WHERE APP_ID=?");
			pst.setString(1, str);
			pst.execute();
			DatabaseHelper.commit(conn);
			
		} catch (SQLException e) {
			logger.error("ERROR while deleting multiple auts", e);
			DatabaseHelper.rollback(conn);			
			throw new DatabaseException("Could not delete auts", e);
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		}
		/*modified by shruthi for sql injection ends*/
	}
	/*modified by shruthi for VAPT helper fixes starts*/
	public Boolean CheckAutAssociateToProj(int appid) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		boolean flag=false;
		PreparedStatement pst = null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try{
	
			pst = conn.prepareStatement("select * from tjn_prj_auts where app_id=?");
			pst.setInt(1, appid);
			rs = pst.executeQuery();
			if(rs.next()){
				flag=true;

			}
			else{
				flag=false;
			}
		}
		catch(Exception e){

		}
		finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return flag;


	}
	/*modified by shruthi for VAPT helper fixes ends*/
	/*modified by shruthi for VAPT helper fixes starts*/
	public Aut hydrateAut(String appName) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		Aut aut = null;
		try {
			
			pst = conn.prepareStatement("SELECT * FROM MASAPPLICATION WHERE APP_NAME=?");
			pst.setString(1, appName);
			rs = pst.executeQuery();
			while(rs.next()){
				aut=this.mapFields(rs);
			}
			
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return aut;
	}
	/*modified by shruthi for VAPT helper fixes ends*/
	/*modified by shruthi for VAPT helper fixes starts*/
	/* added by Roshni for TENJINCG-1168 starts */
	public Aut hydrateAut(Connection conn,String appName) throws DatabaseException {
		PreparedStatement pst = null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		Aut aut = null;
		try {
			pst = conn.prepareStatement("SELECT * FROM MASAPPLICATION WHERE APP_NAME =?");
			pst.setString(1, appName);
			rs = pst.executeQuery();
			while(rs.next()){
				aut=this.mapFields(rs);
			}
			
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return aut;
	}
	/* added by Roshni for TENJINCG-1168 ends */
	/*modified by shruthi for VAPT helper fixes ends*/
	private Aut mapFields(ResultSet rs) throws SQLException {
		try {
			Aut aut=new Aut();
			aut.setId(rs.getInt("APP_ID"));
			aut.setName(rs.getString("APP_NAME"));
			aut.setURL(rs.getString("APP_URL"));
			aut.setLoginReqd(rs.getString("APP_LOGIN_REQD"));
			aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
			aut.setPassword(rs.getString("APP_PASSWORD"));
			aut.setBrowser(rs.getString("APP_DEF_BROWSER"));
			aut.setAdapterPackage(rs.getString("APP_ADAPTER_PACKAGE"));
			aut.setOperation(rs.getString("APP_OPERATIONS"));
			aut.setLoginUserType(rs.getString("APP_USERTYPES"));
			aut.setApiBaseUrl(rs.getString("API_BASE_URL"));
			aut.setDateFormat(rs.getString("APP_DATE_FORMAT"));
			aut.setPlatform(rs.getString("APP_PLATFORM"));
			aut.setPlatformVersion(rs.getString("APP_PLATFORM_VERSION"));
			aut.setApplicationPackage(rs.getString("APP_PACKAGE"));
			aut.setApplicationActivity(rs.getString("APP_ACTIVITY"));
			aut.setApplicationType(rs.getInt("APP_TYPE")); 
			/*Added by Preeti for TENJINCG-73 starts*/
			aut.setPauseTime(rs.getInt("APP_PAUSE_TIME"));
			aut.setPauseLocation(rs.getString("APP_PAUSE_LOCATION"));
			/*Added by Preeti for TENJINCG-73 ends*/
			//added by shivam sharma for  TENJINCG-904 starts
			aut.setAppFileName(rs.getString("APP_FILE_NAME")); 
			//added by shivam sharma for  TENJINCG-904 ends
			/* Added by Roshni for TENJINCG-1038 starts */
			try{
				if(rs.getString("TEMPLATE_PASSWORD")!=null&&!rs.getString("TEMPLATE_PASSWORD").equalsIgnoreCase("")){
					/*Modified by paneendra for VAPT FIX starts*/
					/*String pwd=new Crypto().decrypt(rs.getString("TEMPLATE_PASSWORD").split("!#!")[1],decodeHex(rs.getString("TEMPLATE_PASSWORD").split("!#!")[0].toCharArray()));*/
					String pwd =new CryptoUtilities().decrypt(rs.getString("TEMPLATE_PASSWORD"));
					/*Modified by paneendra for VAPT FIX ends*/
				aut.setTemplatePwd(pwd);
				}
				else{
					aut.setTemplatePwd("");
				}
			}
			catch(Exception e){
				
			}
			/* Added by Roshni for TENJINCG-1038 starts */

			//Changes for OAuth Implementation TENJINCG-1018- Avinash - starts
			buildOAuthObject(rs, aut);
			//Changes for OAuth Implementation TENJINCG-1018- Avinash - Ends
			
			return aut;
		} catch (SQLException e) {
			logger.error("ERROR creating AUT from ResultSet", e);
			throw e;
		}

	}

	public Aut hydrateProjectAut(int projectId, int appId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		Aut aut = null;
		PreparedStatement pst =null;
		ResultSet rs=null;
		try {
			pst = conn.prepareStatement("SELECT * FROM MASAPPLICATION A, TJN_PRJ_AUTS B WHERE A.APP_ID = B.APP_ID AND B.PRJ_ID=? AND B.APP_ID=?");
			pst.setInt(1, projectId);
			pst.setInt(2, appId);

			rs = pst.executeQuery();
			while(rs.next()) {
				aut = this.mapFields(rs);
			}

		} catch (SQLException e) {
			logger.error("ERROR hydrating project AUT", e);
			throw new DatabaseException("Could not fetch Project AUT",e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);

		}

		return aut;

	}

	public Aut hydrateProjectAut(int projectId, String appName) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		Aut aut = null;
		PreparedStatement pst =null;
		ResultSet rs=null;
		try {
			pst = conn.prepareStatement("SELECT * FROM MASAPPLICATION WHERE APP_NAME = ? AND APP_ID IN (SELECT APP_ID FROM TJN_PRJ_AUTS WHERE PRJ_ID=?)");
			pst.setInt(2, projectId);
			pst.setString(1, Utilities.trim(appName));

			rs = pst.executeQuery();
			while(rs.next()) {
				aut = this.mapFields(rs);
			}

		} catch (SQLException e) {
			logger.error("ERROR hydrating project AUT", e);
			throw new DatabaseException("Could not fetch Project AUT",e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return aut;

	}
	public ArrayList<Aut> hydrateProjectAuts(int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		ArrayList<Aut> auts = new ArrayList<Aut>();

		try{
			pst = conn.prepareStatement("Select * from TJN_PRJ_AUTS where PRJ_ID = ?");
			pst.setInt(1, projectId);

			rs = pst.executeQuery();

			while(rs.next()){
				int appId = rs.getInt("APP_ID");
				try{
					/* modified by Roshni for TENJINCG-1168 starts */
					/*Aut aut = this.hydrateAut(appId);*/
					Aut aut = this.hydrateAut(conn,appId);
					/* modified by Roshni for TENJINCG-1168 ends */
					auts.add(aut);
				}catch(Exception e){
					logger.error("Could not fetch details of AUT " + appId);
				}
			}

			pst.close();
			rs.close();
		}catch(Exception e){
			throw new DatabaseException("Could not fetch AUTs for Project",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return auts;
	}
	/*Added by Preeti for TENJINCG-73 starts*/
	public Aut hydrateAutForDriverLaunch(Connection conn,String appName, String userId, String loginType) throws DatabaseException {
		long oMillis = System.currentTimeMillis();
		logger.debug("Entered hydrateAut(" + appName + ", " + userId + ", " + loginType + ")");
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst=null;
		ResultSet rs=null;
		Aut aut = null;
		try {
			//String query = "SELECT A.APP_ADAPTER_PACKAGE, A.APP_DEF_BROWSER, A.APP_ID,A.APP_NAME,A.APP_URL,A.APP_LOGIN_REQD, A.APP_PACKAGE, A.APP_ACTIVITY,A.APP_PAUSE_TIME, A.APP_PAUSE_LOCATION, B.APP_LOGIN_NAME,B.APP_PASSWORD,B.APP_USER_TYPE,B.APP_TXN_PWD, C.OR_TOOL, C.APPLICATION_TYPE from MASAPPLICATION A left outer join TJN_ADAPTER C on A.APP_ADAPTER_PACKAGE = C.NAME , USRAUTCREDENTIALS B WHERE B.APP_ID = A.APP_ID AND A.APP_NAME = ? AND B.USER_ID =? and B.APP_USER_TYPE =?";
			//added by shivam sharma for  TENJINCG-904 starts
			String query = "SELECT A.APP_ADAPTER_PACKAGE, A.APP_DEF_BROWSER, A.APP_ID,A.APP_NAME,A.APP_URL,A.APP_LOGIN_REQD, A.APP_PACKAGE, A.APP_ACTIVITY,A.APP_PAUSE_TIME, A.APP_PAUSE_LOCATION,A.APP_FILE_NAME, B.APP_LOGIN_NAME,B.APP_PASSWORD,B.APP_USER_TYPE,B.APP_TXN_PWD, C.OR_TOOL, C.APPLICATION_TYPE from MASAPPLICATION A left outer join TJN_ADAPTER C on A.APP_ADAPTER_PACKAGE = C.NAME , USRAUTCREDENTIALS B WHERE B.APP_ID = A.APP_ID AND A.APP_NAME = ? AND B.USER_ID =? and B.APP_USER_TYPE =?";
			//added by shivam sharma for  TENJINCG-904 ends
			boolean recFound = false;
			pst = conn.prepareStatement(query);
			pst.setString(1, appName);
			pst.setString(2, userId);
			pst.setString(3,loginType);
			long startMillis = System.currentTimeMillis();
			rs = pst.executeQuery();
			long endMillis = System.currentTimeMillis();
			logger.debug("HYDR_AUT_TIMER >> Execute Query 1 >> " + Utilities.calculateElapsedTime(startMillis, endMillis));
			startMillis = System.currentTimeMillis();
			while (rs.next()) {
				recFound = true;
				aut = new Aut();
				aut.setId(rs.getInt("APP_ID"));
				aut.setName(rs.getString("APP_NAME"));
				aut.setURL(rs.getString("APP_URL"));
				aut.setLoginReqd(rs.getString("APP_LOGIN_REQD"));
				aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
				aut.setPassword(rs.getString("APP_PASSWORD"));
				aut.setTxnPassword(rs.getString("APP_TXN_PWD"));
				aut.setAdapterPackage(rs.getString("APP_ADAPTER_PACKAGE"));
				aut.setBrowser(rs.getString("APP_DEF_BROWSER"));
				aut.setAdapterPackageOrTool(rs.getInt("OR_TOOL"));
				aut.setApplicationType(rs.getInt("APPLICATION_TYPE"));
				aut.setApplicationPackage(rs.getString("APP_PACKAGE"));
				aut.setApplicationActivity(rs.getString("APP_ACTIVITY"));
				aut.setPauseTime(rs.getInt("APP_PAUSE_TIME"));
				aut.setPauseLocation(rs.getString("APP_PAUSE_LOCATION"));
				//added by shivam sharma for  TENJINCG-904 starts
				aut.setAppFileName(rs.getString("APP_FILE_NAME")); 
				//added by shivam sharma for  TENJINCG-904 ends
			}
			endMillis = System.currentTimeMillis();
			logger.debug("HYDR_AUT_TIMER >> Process Query 1 >> " + Utilities.calculateElapsedTime(startMillis, endMillis));
			rs.close();
			pst.close();
			if(!recFound){
				//pst = conn.prepareStatement("SELECT A.APP_ID,A.APP_NAME,A.APP_URL,A.APP_LOGIN_REQD,B.APP_LOGIN_NAME,B.APP_PASSWORD,B.APP_USER_TYPE,B.APP_TXN_PWD from MASAPPLICATION A, USRAUTCREDENTIALS B WHERE B.APP_ID = A.APP_ID AND A.APP_NAME = ? and B.APP_LOGIN_NAME =?");
				//added by shivam sharma for  TENJINCG-904 starts
				pst = conn.prepareStatement("SELECT A.APP_ID,A.APP_NAME,A.APP_URL,A.APP_LOGIN_REQD,A.APP_FILE_NAME,B.APP_LOGIN_NAME,B.APP_PASSWORD,B.APP_USER_TYPE,B.APP_TXN_PWD from MASAPPLICATION A, USRAUTCREDENTIALS B WHERE B.APP_ID = A.APP_ID AND A.APP_NAME = ? and B.APP_LOGIN_NAME =?");
				//added by shivam sharma for  TENJINCG-904 ends
				pst.setString(1, appName);
				pst.setString(2,loginType);
				startMillis = System.currentTimeMillis();
				rs = pst.executeQuery();
				endMillis = System.currentTimeMillis();
				logger.debug("HYDR_AUT_TIMER >> Execute Query 2 >> " + Utilities.calculateElapsedTime(startMillis, endMillis));
				startMillis = System.currentTimeMillis();
				while (rs.next()) {
					recFound = true;
					aut = new Aut();
					aut.setId(rs.getInt("APP_ID"));
					aut.setName(rs.getString("APP_NAME"));
					aut.setURL(rs.getString("APP_URL"));
					aut.setLoginReqd(rs.getString("APP_LOGIN_REQD"));
					aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
					aut.setPassword(rs.getString("APP_PASSWORD"));
					aut.setTxnPassword(rs.getString("APP_TXN_PWD"));
					//added by shivam sharma for  TENJINCG-904 starts
					aut.setAppFileName(rs.getString("APP_FILE_NAME"));
					//added by shivam sharma for  TENJINCG-904 ends
				}
				endMillis = System.currentTimeMillis();
				logger.debug("HYDR_AUT_TIMER >> Process Query 2 >> " + Utilities.calculateElapsedTime(startMillis, endMillis));
				rs.close();
				pst.close();
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		long oEMillis = System.currentTimeMillis();
		logger.debug("HYDR_AUT_TIMER >> hydrateAut complete >> " + Utilities.calculateElapsedTime(oMillis, oEMillis));
		return aut;
	}
	/*Added by Preeti for TENJINCG-73 ends*/
	
	/*modified by shruthi for VAPT helper fixes starts*/
	/*Added by Pushpalatha for TENJINCG-611 starts*/
	public int hydrateAppId(int projId, String appName) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		/*Statement st=null;*/
		PreparedStatement pst = null;
		ResultSet rs=null;
		int appId = 0;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			/*st = conn.createStatement();
			rs=st.executeQuery("select APP_ID from tjn_prj_auts where app_id in(select app_id from masapplication where app_name = '"+appName+"') and prj_id='"+projId+"'");*/
			pst = conn.prepareStatement("select APP_ID from tjn_prj_auts where app_id in(select app_id from masapplication where app_name =? ) and prj_id=?");
			pst.setString(1,appName);
			pst.setInt(2, projId);
			rs = pst.executeQuery();

			while(rs.next()){
				appId = rs.getInt("APP_ID");
			}
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new DatabaseException("An Internal error occured, Please contact Tenjin support");
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return appId;

	}
	/*Added by Pushpalatha for TENJINCG-611 ends*/
	/*modified by shruthi for VAPT helper fixes ends*/

	//added by pushpa for Mobility:Starts
	public int getAutType(int appId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		int appType=0;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try{
			pst = conn.prepareStatement("SELECT APP_TYPE FROM MASAPPLICATION WHERE APP_ID = ?");
			pst.setInt(1,appId);
			rs = pst.executeQuery();
			while(rs.next()){
				appType = rs.getInt("APP_TYPE");
			}

			return appType;
		}catch(SQLException e){
			logger.error("Exception in getAutName(Connection conn, int appId)");
			logger.error("Could not get name for Application with ID " + appId,e);
			return appType;
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	//added by Pushpa for Mobility:ends
	/* added by Roshni for TENJINCG-1168 starts */
	public int getAutType(int appId,Connection conn) throws DatabaseException{
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		int appType=0;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try{
			pst = conn.prepareStatement("SELECT APP_TYPE FROM MASAPPLICATION WHERE APP_ID = ?");
			pst.setInt(1,appId);
			rs = pst.executeQuery();
			while(rs.next()){
				appType = rs.getInt("APP_TYPE");
			}

			return appType;
		}catch(SQLException e){
			logger.error("Exception in getAutName(Connection conn, int appId)");
			logger.error("Could not get name for Application with ID " + appId,e);
			return appType;
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
	}
	/* added by Roshni for TENJINCG-1168 ends */
	//added by shivam sharma for  TENJINCG-904 starts
	public void updateAppFileName(String appFileName,int appId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst=null;
		try {
			logger.debug("Updating appFileName {} ", appId);
			pst = conn.prepareStatement("UPDATE MASAPPLICATION SET APP_FILE_NAME=? where APP_ID = ?");
			pst.setString(1, appFileName);
			pst.setInt(2, appId);
			pst.execute();


		} catch (Exception e) {
			logger.debug("error updating AUT",e);
			throw new DatabaseException("Could not update appFileName",e);
		}finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	//added by shivam sharma for  TENJINCG-904 ends
	
	/*Added by Ashiki TENJINCG-1004 starts*/
	public String downloadApkFile(int appId) throws DatabaseException {
		
		String file="";
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT APP_FILE_NAME FROM MASAPPLICATION where APP_ID =?");)
		{
			logger.debug("Fetching file name  ", appId);
			pst.setInt(1, appId);
				try(ResultSet rs= pst.executeQuery();){
					
					while (rs.next()) {
						file=rs.getString("APP_FILE_NAME");
					}
					
				}
			

		} catch (Exception e) {
			logger.debug("error Fetchinfg File name",e);
			throw new DatabaseException("Could not Download File",e);
		}
		return file;
	
	}
	/*Added by Ashiki TENJINCG-1004 ends*/
	
	/*Added by Padmavathi for TJN252-36 starts*/
	public boolean isOperationMappedToTestStep( Connection conn,int appId,ArrayList<String> operations){
		/*modified by shruthi for sql injection starts*/
		
		int mappedStepCount = 0;
		for(String str : operations ) {
		try (PreparedStatement pst=conn.prepareStatement("SELECT COUNT(*) as MAPPED_STEPS_COUNT FROM TESTSTEPS WHERE APP_ID=? AND TSTEP_OPERATION=?")){
			pst.setInt(1, appId);
			pst.setString(2, str);
			try(ResultSet rs = pst.executeQuery()){
				while(rs.next()) {
					mappedStepCount = rs.getInt("MAPPED_STEPS_COUNT");
				}
			}
		} catch (SQLException e) {
			logger.error("ERROR checking if module is operation to test step", e);
		}
		}
		/*modified by shruthi for sql injection ends*/
		if(mappedStepCount > 0) {
			return true;
		}else {
			return false;
		}
	}
		
	public boolean isAutUserMappedToTestStep( Connection conn,int appId,ArrayList<String> userTypes){
		/*modified by shruthi for sql injection starts*/

		int mappedStepCount = 0;
		for(String str : userTypes) {
		try (PreparedStatement pst=conn.prepareStatement("SELECT COUNT(*) as MAPPED_STEPS_COUNT FROM TESTSTEPS WHERE APP_ID=? AND TSTEP_AUT_LOGIN_TYPE=?")){
			pst.setInt(1, appId);
			pst.setString(2, str);
			try(ResultSet rs = pst.executeQuery()){
				while(rs.next()) {
					mappedStepCount = rs.getInt("MAPPED_STEPS_COUNT");
				}
			}
		} catch (SQLException e) {
			logger.error("ERROR checking if aut user is mapped to test step", e);
		}
		}
		/*modified by shruthi for sql injection ends*/
		if(mappedStepCount > 0) {
			return true;
		}else {
			return false;
		}
	}
	/*Added by Padmavathi for TJN252-36 ends*/
	
	/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Starts*/
	public Aut hydrateOAuthRecord(int appId) throws DatabaseException {
		Aut record = new Aut();
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("SELECT OAUTH_GRANT_TYPE, OAUTH_CALLBACK_URL, OAUTH_AUTH_URL, OAUTH_ACCESS_TOKEN_URL, OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET, OAUTH_ADDAUTHDATA_TO FROM MASAPPLICATION WHERE APP_ID=?");
			){
			
			pst.setInt(1, appId);
			
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					record = this.buildOAuthObject(rs, record);
				}
			}
			
		} catch(SQLException e) {
			logger.error("Could not fetch oAuth Details record");
			throw new DatabaseException("Could not fetch oAuth Details record",e);
		}
		record.setId(appId);
		return record;
			
	}
	
	public Aut persistOAuthRecord(Aut record) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("UPDATE MASAPPLICATION SET OAUTH_GRANT_TYPE=?, OAUTH_CALLBACK_URL=?, OAUTH_AUTH_URL=?, OAUTH_ACCESS_TOKEN_URL=?, OAUTH_CLIENT_ID=?, OAUTH_CLIENT_SECRET=?, OAUTH_ADDAUTHDATA_TO=? WHERE APP_ID=?");
			){
				pst.setString(1, record.getGrantType());
				pst.setString(2, record.getCallBackUrl());
				pst.setString(3, record.getAuthUrl());
				pst.setString(4, record.getAccessTokenUrl());
				pst.setString(5, record.getClientId());
				pst.setString(6, record.getClientSecret());
				pst.setString(7, record.getAddAuthDataTo());
				pst.setInt(8,record.getId());
				pst.execute();
		}catch(SQLException e) {
			logger.error("Could not insert oAuth detail record");
			throw new DatabaseException("Could not insert oAuth detail record",e);
		}
		return record;
	}

	public boolean OAuthRecordExists(int appId) throws DatabaseException {
		boolean exists = false;
		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("SELECT OAUTH_CLIENT_ID FROM MASAPPLICATION WHERE APP_ID=?");) {
			pst.setInt(1, appId);
			try (ResultSet rs = pst.executeQuery()) {
				while (rs.next()) {
					if(!Utilities.trim(rs.getString("OAUTH_CLIENT_ID")).isEmpty()){
						exists = true;
					}
				}
			}

		} catch (SQLException e) {
			logger.error("Could not fetch oAuth detail record");
			throw new DatabaseException("Could not fetch oAuth detail record", e);
		}
		return exists;

	}

	private Aut buildOAuthObject(ResultSet rs, Aut record) throws SQLException {
		record.setGrantType(rs.getString("OAUTH_GRANT_TYPE"));
		record.setCallBackUrl(rs.getString("OAUTH_CALLBACK_URL"));
		record.setAuthUrl(rs.getString("OAUTH_AUTH_URL"));
		record.setAccessTokenUrl(rs.getString("OAUTH_ACCESS_TOKEN_URL"));
		record.setClientId(rs.getString("OAUTH_CLIENT_ID"));
		record.setClientSecret(rs.getString("OAUTH_CLIENT_SECRET"));
		record.setAddAuthDataTo(rs.getString("OAUTH_ADDAUTHDATA_TO"));
		return record;

	}

   /* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Ends*/
}
