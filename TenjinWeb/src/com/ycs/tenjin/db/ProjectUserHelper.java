/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectUserHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 15-10-2018		   	Pushpalatha			  	Newly Added 
* 29-11-2018			Ashiki					TJNUN262-25
* 22-05-2019			Prem					V2.8-38
*/

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Constants;

public class ProjectUserHelper {
	
	private static Logger logger = LoggerFactory
			.getLogger(ProjectUserHelper.class);

		

	public ArrayList<User> hydrateUsersNotInProject(int projectId) throws DatabaseException {

		ArrayList<User> users = new ArrayList<User>();

		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
				PreparedStatement pst = conn.prepareStatement("SELECT * FROM MASUSER ORDER BY USER_ID");
				ResultSet rs = pst.executeQuery();) {

			while (rs.next()) {
				User user = new User();
				user.setId(rs.getString("USER_ID"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setLastName(rs.getString("LAST_NAME"));
				user.setPassword(rs.getString("PASS_WORD"));
				user.setPrimaryPhone(rs.getString("PHONE_1"));
				user.setSecondaryPhone(rs.getString("PHONE_2"));
				user.setEmail(rs.getString("EMAIL_ID"));

				if (user.getLastName() != null && !user.getLastName().equalsIgnoreCase("")) {
					user.setFullName(user.getFirstName() + " " + user.getLastName());
				} else {
					user.setFullName(user.getFirstName());
				}
				try (PreparedStatement oPst = conn.prepareStatement(
						"SELECT COUNT(USER_ID) AS CNT FROM TJN_PRJ_USERS WHERE PRJ_ID = ? AND USER_ID = ?");) {

					oPst.setInt(1, projectId);
					oPst.setString(2, user.getId());
					try (ResultSet oRs = oPst.executeQuery();) {
						int pCnt = 0;
						while (oRs.next()) {
							pCnt = oRs.getInt("CNT");
						}
						if (pCnt < 1) {
							users.add(user);
						}
					}
				}

			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch unmapped users", e);
		}
		return users;
	}

	public void mapUsersToProject(int projectId, String userList,String projectType) throws DatabaseException, RequestValidationException {
		if(projectType.equalsIgnoreCase("Public")){
			boolean validateUser=this.validateuser(projectId,userList);
			if(!validateUser){
				/*Modified by Prem for V2.8-38 start*/
				throw new RequestValidationException("Public project should have minimum two Project Administrator");
				/*Modified by Prem for V2.8-38 Ends*/
			}
		}
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			){
			String[] arr = userList.split(",");
			
			for(String userDetail:arr){
				try{
				/*changed by Ashiki for TJNUN262-25 starts*/
				String[] u = userDetail.split(":");
				/*changed by Ashiki for TJNUN262-25 ends*/
				String userId = u[0];
				String role = "";
				if(u.length > 1){
					role = u[1];
				}else{
					role = "Tester";
				}

				User user = new UserHandler().getUser(userId);
				if(user != null){
					try(PreparedStatement pst = conn.prepareStatement("INSERT INTO TJN_PRJ_USERS (PRJ_ID,USER_ID,USER_ROLE) VALUES (?,?,?)");){
					
					pst.setInt(1, projectId);
					pst.setString(2, user.getId());
					pst.setString(3, role);
					pst.execute();
					
					}
				}
			}catch(Exception e){
				logger.error("Error ", e);
			}
			}

		}catch(Exception e){

		}
	}
	
	private boolean validateuser(int projectId, String userList) throws DatabaseException {
		
		List<User> users=new LinkedList<>();
		int count=0;
		boolean flag=false;
		users = new ProjectHandler().getProjectUsers(projectId);
		for(User user:users){
			if(user.getRoleInCurrentProject().equals("Project Administrator")){
				count++;
				if(count>=2){
					flag=true;
					break;
				}
			}
		}
		if(!flag){
			String[] arr = userList.split(",");
			int admincount=0;
			for(String userDetail:arr){
				String[] u = userDetail.split(":");
				if( u[1].equalsIgnoreCase("Project Administrator")){
					admincount++;
					if(admincount>=1){
						flag=true;
						break;
					}
				}
			}
		}
		return flag;
	}

	

	public void unmapUsersFromProject(int projectId, String userList) throws DatabaseException {
		
		String[] users = userList.split(",");
		ArrayList<String> admins = new ArrayList<String>();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement pst = conn.prepareStatement("SELECT USER_ID FROM TJN_PRJ_USERS WHERE PRJ_ID = ? AND USER_ROLE IN (?,?)");){
			pst.setInt(1, projectId);
			pst.setString(2, "Project Administrator");
			pst.setString(3, "Owner");
			try(ResultSet rs = pst.executeQuery();){
				while(rs.next()){
					admins.add(rs.getString("USER_ID"));
				}
			}
			boolean allAdminsMarked = false;
			int foundCount = 0;
			if (admins != null) {
				for (String admin : admins) {
					for (String u : users) {
						if (admin.equalsIgnoreCase(u)) {
							foundCount++;
							break;
						}
					}
				}

				if (foundCount >= admins.size()) {
					allAdminsMarked = true;
				}
			}

			if(allAdminsMarked){
				throw new DatabaseException("Project needs at least one Administrator");
			}

			for(String u:users){
				try(PreparedStatement pst1 = conn.prepareStatement("DELETE FROM TJN_PRJ_USERS WHERE PRJ_ID = ? AND USER_ID = ?");){
				
				pst1.setInt(1, projectId);
				pst1.setString(2, u);
				pst1.execute();
				}catch(Exception e){
					throw new DatabaseException(e.getMessage(),e);
				}
			}
		}catch(Exception e){
			throw new DatabaseException(e.getMessage(),e);
		}
	}

}
