/***

 Yethi Consulting Private Ltd. CONFIDENTIAL


 Name of this file:  ProjectTestMangerHelper.java


 Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


 Copyright � 2016-17 by Yethi Consulting Private Ltd.


 This source file is part of the TENJIN Software Product and System 
 and is copyrighted by Yethi Consulting Private Ltd.

 All rights reserved.  No part of this work may be reproduced, copied, 
 duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
 system, transmitted in any form or by any means, electronic, 
 mechanical, photographic, graphic, optic recording or otherwise, translated 
 in any language or computer language, sold, rented, leased without the prior 
 written permission of Yethi Consulting Services Private Ltd.

 Notice: All information and source code contained in this file is, and remains 
 the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
 The intellectual and technical concepts contained herein are proprietary to Yethi 
 Consulting Services Private Ltd., and its suppliers and may be covered under patents 
 and patents in process and are protected by trade secret or copyright laws. Dissemination 
 of this information or reproduction of this material is strictly forbidden unless prior 
 written permission is obtained from Yethi Consulting Services Private Ltd. 


 Yethi Consulting Private Ltd.
 # 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
 HAL 3rd Stage, Bangalore - 560 075,
 Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 08-11-2018           Leelaprasad             newly added for TENJINCG-874
 * 26-03-2019			Roshni					TJN252-54
 */
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.testmanager.TestManagerInstance;
import com.ycs.tenjin.util.Constants;

public class ProjectTestMangerHelper {

	private static final Logger logger = LoggerFactory
			.getLogger(ProjectTestMangerHelper.class);

	public void validateInstanceCredintials(String userId, String instance)
			throws DatabaseException {

		try (Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn
						.prepareStatement("SELECT * FROM TJN_TM_USER_MAPPING where TJN_USER_ID=? and TJN_INSTANCE_NAME=?");) {
			int name = 0;

			pst.setString(1, userId);
			pst.setString(2, instance);

			try (ResultSet rs = pst.executeQuery();) {
				while (rs.next()) {
					name++;
				}
				if (name == 0) {
					logger.info(instance + " does not  have  credentials");
					throw new DatabaseException(instance
							+ " does not  have  credentials");
				}
			}

		} catch (SQLException e) {
			logger.error("Could not hydrate hydrate TMCredentials ForProject ",
					instance, e);

		}

	}

	public void updateProjectTestManagerDetails(Project project)
			throws DatabaseException {

		try (Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn
						.prepareStatement("UPDATE TJN_PRJ_TM_LINKAGE SET  TM_INSTANCE_NAME = ?, TM_PROJECT = ?,TM_ENABLED=? WHERE PRJ_ID = ?");) {

			pst.setString(1, project.getEtmInstance());
			pst.setString(2, project.getEtmProject());
			pst.setString(3, project.getEtmEnable());
			pst.setInt(4, project.getId());
			pst.execute();

		} catch (Exception e) {
			logger.error("Could not update external project Instance ", e);
			throw new DatabaseException(
					"Could not update external project Instance", e);
		}

	}

	public void updateFilterValues(Project project) throws DatabaseException {

		try (Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn
						.prepareStatement("UPDATE TJN_PRJ_TM_LINKAGE SET  TC_FILTER = ?, TC_FILTER_VALUE = ?,TS_FILTER=? ,TS_FILTER_VALUE=?,TC_DISPLAY_VALUE=?,TS_DISPLAY_VALUE=? WHERE PRJ_ID = ?");) {

			pst.setString(1, project.getTcFilter());
			pst.setString(2, project.getTcFilterValue());
			pst.setString(3, project.getTsFilter());
			pst.setString(4, project.getTsFilterValue());
			/*Added By Roshni */
			pst.setString(5, project.getTcDisplayValue());
			pst.setString(6, project.getTsDisplayValue());
			pst.setInt(7, project.getId());
			
			pst.execute();

		} catch (Exception e) {
			throw new DatabaseException(
					"Could not update external project Instance", e);
		}

	}

	public TestManagerInstance hydrateTMCredentialsForProject(String instance,
			String userId, String tjnPrjId) throws DatabaseException {
		TestManagerInstance tmInstance = new TestManagerInstance();

		int checkResult = 0;
		try (Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn
						.prepareStatement("SELECT A.TJN_USER_ID,A.TJN_INSTANCE_NAME,A.USERNAME,A.PASSWORD,"
								+ " B.TM_REC_ID,B.TM_TOOL,B.TM_URL,C.TM_PROJECT FROM"
								+ " TJN_TM_USER_MAPPING A,"
								+ " TJN_TM_TOOL_MASTER B, TJN_PRJ_TM_LINKAGE C WHERE"
								+ " A.TJN_INSTANCE_NAME=B.TM_INSTANCE_NAME AND"
								+ " C.TM_INSTANCE_NAME=B.TM_INSTANCE_NAME AND"
								+ " A.TJN_USER_ID=? AND"
								+ " B.TM_INSTANCE_NAME=? AND C.PRJ_ID=?");) {
			pst.setString(1, userId);
			pst.setString(2, instance);
			pst.setString(3, tjnPrjId);
			try (ResultSet rs = pst.executeQuery();) {

				while (rs.next()) {
					checkResult = 1;
					tmInstance.setURL(rs.getString("TM_URL"));
					tmInstance.setPassword(rs.getString("PASSWORD"));
					tmInstance.setTjnUsername(rs.getString("TJN_USER_ID"));
					tmInstance.setTool(rs.getString("TM_TOOL"));
					tmInstance.setUsername(rs.getString("USERNAME"));
					tmInstance.setTmProjectDomain(rs.getString("TM_PROJECT"));
				}
			}
			if (checkResult == 0) {
				logger.info(instance + " does not  have  credentials");
				throw new DatabaseException(instance
						+ " does not  have  credentials");
			}

		} catch (SQLException e) {
			logger.error("Could not hydrate hydrate TMCredentials ForProject ",
					instance, e);

		}
		return tmInstance;
	}

	public Project hydrateProjectETMDetails(String tjnPrjId)
			throws DatabaseException {

		Project project = new Project();
		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement("SELECT A.*,B.PRJ_NAME FROM TJN_PROJECTS B  left join TJN_PRJ_TM_LINKAGE A on A.PRJ_ID = B.PRJ_ID  WHERE A.PRJ_ID = B.PRJ_ID AND B.PRJ_ID=?");) {
			
			pst.setInt(1, Integer.parseInt(tjnPrjId));
			project.setId(Integer.parseInt(tjnPrjId));
			try (ResultSet rs = pst.executeQuery();) {
				while (rs.next()) {
					project.setEtmInstance(rs.getString("TM_INSTANCE_NAME"));
					project.setEtmProject(rs.getString("TM_PROJECT"));
					project.setEtmEnable(rs.getString("TM_ENABLED"));
					project.setTcFilter(rs.getString("TC_FILTER"));
					project.setTcFilterValue(rs.getString("TC_FILTER_VALUE"));
					project.setTsFilter(rs.getString("TS_FILTER"));
					project.setTsFilterValue(rs.getString("TS_FILTER_VALUE"));
					project.setName(rs.getString("PRJ_NAME"));
					/* Added By Roshni */
					project.setTcDisplayValue(rs.getString("TC_DISPLAY_VALUE"));
					project.setTsDisplayValue(rs.getString("TS_DISPLAY_VALUE"));
				}
			}
		} catch (Exception e) {

		}
		return project;
	}
}
