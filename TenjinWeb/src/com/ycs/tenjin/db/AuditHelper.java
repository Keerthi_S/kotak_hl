/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AuditHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 18-02-2019			Preeti					TENJINCG-969
* 28-03-2019			Ashiki					TENJINCG-1023
* 03-06-2019            Leelaprasad P           TENJINCG-1068,TENJINCG-1069
* 19-09-2019			Preeti					TENJINCG-1068,1069
*/

package com.ycs.tenjin.db;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.util.Constants;

public class AuditHelper {
	private static final Logger logger = LoggerFactory.getLogger(AuditHelper.class);
	public void persistAuditRecord(AuditRecord record) throws DatabaseException {
		
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				/*Changed by Ashiki for TENJINCG-1023 starts*/
				PreparedStatement pst = conn.prepareStatement("INSERT INTO TJN_AUDIT (AUDIT_ID,ENTITY_RECORD_ID,ENTITY_TYPE,LAST_MODIFIED_BY,LAST_MODIFIED_ON,OLD_DATA,NEW_DATA,ID) VALUES(?,?,?,?,?,?,?,?)");
				){/*Changed by Ashiki for TENJINCG-1023 ends*/
				pst.setInt(1, DatabaseHelper.getGlobalSequenceNumber(conn));
				pst.setInt(2, record.getEntityRecordId());
				pst.setString(3, record.getEntityType());
				pst.setString(4, record.getLastUpdatedBy());
				pst.setTimestamp(5, new Timestamp(new Date().getTime()));
				/*Changed by Ashiki for TENJINCG-1023 starts*/
				/*Added by Leelaprasad for TENJINCG-1068,1069 starts*/
				pst.setString(6, record.getOldData());
				pst.setString(7, record.getNewData());
				Clob clobOld = pst.getConnection().createClob();
				clobOld.setString(1, record.getOldData());
				pst.setClob(6, clobOld);
				Clob clobNew = pst.getConnection().createClob();
				clobNew.setString(1, record.getNewData());
				pst.setClob(7, clobNew);
				/*Added by Leelaprasad for TENJINCG-1068,1069 ends*/
				
				pst.setString(8, record.getId());
				/*Changed by Ashiki for TENJINCG-1023 starts*/
				pst.execute();
		}catch(SQLException e) {
			logger.error("Could not insert audit record");
			throw new DatabaseException("Could not insert audit record",e);
		}
	}
	/*Added by Preeti for TENJINCG-1068,1069 starts*/
	public ArrayList<AuditRecord> hydrateAuditRecord(int entityRecId, String entityType, String fromDate, String toDate) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				){
					return this.hydrateAuditRecord(conn, entityRecId, entityType, fromDate, toDate);
		}catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	}
	public ArrayList<AuditRecord> hydrateAuditRecord(Connection conn, int entityRecId, String entityType, String fromDate, String toDate) throws DatabaseException, SQLException {
		ArrayList<AuditRecord> results = new ArrayList<AuditRecord>();
		String query="";
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query = "SELECT * FROM TJN_AUDIT WHERE ENTITY_RECORD_ID=? AND ENTITY_TYPE=? AND "
					+ "LAST_MODIFIED_ON>=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND "
					+ "LAST_MODIFIED_ON <=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') ORDER BY AUDIT_ID DESC ";
		}else{
			query = "SELECT * FROM TJN_AUDIT WHERE ENTITY_RECORD_ID=? AND ENTITY_TYPE=? AND "
					+ "LAST_MODIFIED_ON >= CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND "
					+ "LAST_MODIFIED_ON < CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 ORDER BY AUDIT_ID DESC ";
		}
		try (
				PreparedStatement pst = conn.prepareStatement(query);
			){
			
			pst.setInt(1, entityRecId);
			pst.setString(2, entityType);
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(3, fromDate+" 00:00:00");
				pst.setString(4, toDate+" 23:59:59");
			}else{
				pst.setString(3, fromDate);
				pst.setString(4, toDate);
			}
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					AuditRecord record = this.buildAuditRecordObject(rs);
					results.add(record);
				}
			}
			
		} catch(SQLException e) {
			logger.error("Could not fetch assisted learning records");
			throw new DatabaseException("Could not fetch assisted learning records",e);
		}
		return results;
			
	}
	private AuditRecord buildAuditRecordObject(ResultSet rs) throws SQLException {
		AuditRecord record = new AuditRecord();
		record.setAuditId(rs.getInt("AUDIT_ID"));
		record.setEntityRecordId(rs.getInt("ENTITY_RECORD_ID"));
		record.setEntityType(rs.getString("ENTITY_TYPE"));
		record.setOldData(rs.getString("OLD_DATA"));
		record.setNewData(rs.getString("NEW_DATA"));
		record.setLastUpdatedBy(rs.getString("LAST_MODIFIED_BY"));
		record.setLastUpdatedOn(rs.getTimestamp("LAST_MODIFIED_ON"));
		return record;
		
	}
	/*Added by Preeti for TENJINCG-1068,1069 ends*/
}
