
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TenjinSessionHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 12-10-2018            Leelaprasad P          	newly added for TENJINCG-872
* 13-12-2018            Padmavathi              TJNUN262-79
* 19-02-2019			Preeti					TENJINCG-928
*  24-06-2019           Padmavathi              for license
* */
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.user.FailedLoginAttempts;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Constants;

public class TenjinSessionHelper {

	private static final Logger logger = LoggerFactory.getLogger(TenjinSessionHelper.class);
	public void clearUser(String[] uArray) throws DatabaseException {
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try{
		for(String u:uArray){
			try {
			
				pst = conn
					.prepareStatement("UPDATE USERSESSIONS SET US_SESSION_STATE = ?,US_LOGOUT_TIMESTAMP=? WHERE US_USER_ID = ?");
				pst.setString(1, "I");
				pst.setTimestamp(2, new Timestamp(new Date().getTime()));
				pst.setString(3, u);
				pst.executeUpdate();
			
			} catch (SQLException e) {
				logger.error("ERROR clearning Tenjin user session for user {}",u);
				logger.error(e.getMessage(), e);
				throw new DatabaseException("Could not clear user session because of an internal error");
			}finally{
				DatabaseHelper.close(pst);
				
			}			
			try {
				 pst = conn
						.prepareStatement("DELETE FROM TJN_CURRENT_USERS WHERE USER_ID = ?");
				pst.setString(1, u);
				pst.execute();

				pst.close();
				logger.debug("User {} unlocked", u);
			} catch (Exception e) {
				logger.error("ERROR clearning Tenjin user session for user {}",
						u);
				logger.error(e.getMessage(), e);
				throw new DatabaseException(
						"Could not clear user session because of an internal error");
			}finally{
				DatabaseHelper.close(pst);
				
			}
		}
		}
		
		finally{
		DatabaseHelper.close(conn);
		}
		
	}
	public User hydrateUserToAuthenticate(String userId) throws DatabaseException {
		
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
				PreparedStatement pst = this.prepareStatementForUserAuthentication(conn, userId);
				ResultSet rs = pst.executeQuery();
			) {
			
			User user = null;
			while(rs.next()) {
				user = this.buildUserObject(rs);
				break;
			}
			
			if(user != null && !user.isLoggedIn() && user.getFailedLoginAttemptsCount() > 0) {
				//Get detailed failed login attempts info only if all other authentication parameters succeed.
				user.setFailedLoginAttempts(this.hydrateFailedLoginAttempts(conn, userId));
			}else if(user != null && user.isLoggedIn()) {
				this.hydrateLoggedInUserInfo(conn, user);
			}
			
			return user;
		} catch(SQLException e) {
			logger.error("ERROR hydrating user [{}]", userId, e);
			throw new DatabaseException("An internal error occurred. Please contact Tenjin Support.");
		}
	
	}
	private List<FailedLoginAttempts> hydrateFailedLoginAttempts(Connection conn, String userId)  {
		List<FailedLoginAttempts> attempts = new ArrayList<FailedLoginAttempts>();
		
		try (
				PreparedStatement pst = this.buildPreparedStatementForFailedLoginAttempts(conn, userId);
				ResultSet rs = pst.executeQuery();
			) {
				while(rs.next()) {
					attempts.add(this.buildFailedLoginAttemptObject(rs));
				}
		} catch(Exception e) {
			logger.error("ERROR hydrating failed login attempts", e);
		}
		
		return attempts;
	}
	private PreparedStatement buildPreparedStatementForFailedLoginAttempts(Connection conn, String userId) throws SQLException {
		PreparedStatement pst = conn.prepareStatement("SELECT TERMINAL,TIME_STAMP FROM FAILED_LOGIN_ATTEMPTS WHERE USER_ID=? ORDER BY TIME_STAMP DESC");
		pst.setString(1, userId);
		return pst;
	}
	/*Modified by Prem for TENJINCG-843*/
	private PreparedStatement prepareStatementForUserAuthentication(Connection conn, String userId) throws SQLException {
		String query = "SELECT A.*, B.GROUP_ID,C.GROUP_DESC,D.LAST_LOGIN_TIMESTAMP,E.FAILED_LOGIN_ATTEMPTS,F.ACTIVE_SESSION_COUNT,G.ACTIVE_USER_COUNT FROM "
				+ "MASUSER A,"
				+ "USERROLES B,"
				+ "MASUSERGROUPS C,"
				+ "(SELECT MAX(US_LOGIN_TIMESTAMP) AS LAST_LOGIN_TIMESTAMP FROM USERSESSIONS WHERE US_USER_ID=?) D,"
				+ "(SELECT COUNT(TIME_STAMP) AS FAILED_LOGIN_ATTEMPTS FROM FAILED_LOGIN_ATTEMPTS WHERE USER_ID=?) E, "
				+ "(SELECT COUNT(USER_ID) AS ACTIVE_SESSION_COUNT FROM TJN_CURRENT_USERS WHERE USER_ID=?) F,"
				+ "(SELECT COUNT(USER_ID) AS ACTIVE_USER_COUNT FROM TJN_CURRENT_USERS ) G WHERE "
				+ "C.GROUP_ID=B.GROUP_ID AND "
				+ "B.USER_ID=A.USER_ID AND "
				+ "A.USER_ID=?";
		PreparedStatement pst = conn.prepareStatement(query);
		for(int i=1; i <=4; i++) {
			pst.setString(i, userId);
		}
		
		return pst;
	}
	private FailedLoginAttempts buildFailedLoginAttemptObject(ResultSet rs) throws SQLException {
		FailedLoginAttempts attempt = new FailedLoginAttempts();
		attempt.setIpAddress(rs.getString("TERMINAL"));
		attempt.setTimeStamp(rs.getTimestamp("TIME_STAMP"));
		
		return attempt;
	}
	private User buildUserObject(ResultSet rs) throws SQLException{
		User user = new User();
		
		String fullName = "";
		if (rs.getString("LAST_NAME") == null || rs.getString("LAST_NAME").equals("")) {
			fullName = fullName + rs.getString("FIRST_NAME") + "";
		} else {
			fullName = fullName + rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME");
		}
		user.setFirstName(rs.getString("FIRST_NAME"));
		user.setLastName(rs.getString("LAST_NAME"));
		user.setId(rs.getString("USER_ID"));
		user.setPassword(rs.getString("PASS_WORD"));
		user.setFullName(fullName);
		user.setEmail(rs.getString("EMAIL_ID"));
		user.setRoles(rs.getString("GROUP_DESC"));
		user.setPrimaryPhone(rs.getString("PHONE_1"));
		user.setSecondaryPhone(rs.getString("PHONE_2"));
		user.setEnabledStatus(rs.getString("USER_ENABLED"));
		/*Uncommented  by Padmavathi for license starts*/
		user.setMaxNumberUsers(rs.getInt("ACTIVE_USER_COUNT"));
		/*Uncommented  by Padmavathi for license starts*/
		/*Modified by Preeti for TENJINCG-928 starts*/
		/*SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy hh:mm:ss");*/
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		/*Modified by Preeti for TENJINCG-928 ends*/
		try {
			user.setLastLogin(sdf.format(rs.getTimestamp("LAST_LOGIN_TIMESTAMP")));
		} catch (Exception e) {
			logger.warn("ERROR - Could not get Last Login Timestamp for user [{}]", user.getId(), e);
		}
		
		user.setFailedLoginAttemptsCount(rs.getInt("FAILED_LOGIN_ATTEMPTS"));
		if(rs.getInt("ACTIVE_SESSION_COUNT") < 1) {
			user.setLoggedIn(false);
		}else {
			user.setLoggedIn(true);
		}
		
		return user;
		
	}
	private void hydrateLoggedInUserInfo(Connection conn, User user) throws DatabaseException {
		try (
				PreparedStatement pst = this.buildPreparedStatementForCurrentUser(conn, user.getId());
				ResultSet rs = pst.executeQuery();
			) {
			
			while(rs.next()) {
				this.buildCurrentUserObject(rs, user);
				break;
			}
			
		}catch(Exception e) {
			logger.error("ERROR hydrating active user information for user [{}]", user.getId(), e);
		}
	}
	private void buildCurrentUserObject(ResultSet rs, User user) throws SQLException {
		user.setCurrentTerminal(rs.getString("TERMINAL"));
		/*Modified by Preeti for TENJINCG-928 starts*/
		/*SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy hh:mm:ss");*/
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		/*Modified by Preeti for TENJINCG-928 ends*/
		try {
			user.setLastLogin(sdf.format(rs.getTimestamp("LOGIN_TIME")));
		} catch (Exception e) {
			logger.warn("ERROR - Could not get Last Login Timestamp for user [{}]", user.getId(), e);
		}
	}
	private PreparedStatement buildPreparedStatementForCurrentUser(Connection conn, String userId) throws SQLException {
		PreparedStatement pst = conn.prepareStatement("SELECT TERMINAL,LOGIN_TIME FROM TJN_CURRENT_USERS WHERE USER_ID=?");
		pst.setString(1, userId);
		return pst;
	}
	public void persistFailedLoginAttempt(String userId, String terminal) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
				PreparedStatement pst = this.buildPreparedStatementForFailedAttemptsInsert(conn, userId, terminal);
			) {
			
			pst.execute();
			
		} catch(Exception e) {
			logger.error("ERROR - Could not insert failed login attempt for user [{}]", userId, e);
		}
	}
	private PreparedStatement buildPreparedStatementForFailedAttemptsInsert(Connection conn, String userId, String terminal) throws SQLException {
		PreparedStatement pst = conn.prepareStatement("INSERT INTO FAILED_LOGIN_ATTEMPTS  (USER_ID,TERMINAL,TIME_STAMP) values(?,?,?)");
		pst.setString(1, userId);
		pst.setString(2, terminal);
		pst.setTimestamp(3, new Timestamp(new Date().getTime()));
		return pst;
		
	}
	public void finalizeAuthentication(User user) throws DatabaseException {
		
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
				
				PreparedStatement userSessionPst = this.buildPreparedStatementForUserSessionInsert(conn, DatabaseHelper.getGlobalSequenceNumber(conn), user.getId(), "A",user.getSessionKey());
				PreparedStatement currentUserPst = this.buildPreparedStatementForCurrentUserInsert(conn, user.getId(), user.getCurrentTerminal(),user.getSessionKey());
			
				PreparedStatement clearInvalidAttemptsPst = this.buildPreparedStatementToClearInvalidAttempts(conn, user.getId());
			) {
			
			try {
				userSessionPst.execute();
				currentUserPst.execute();
				clearInvalidAttemptsPst.execute();
			} catch (Exception e) {
				logger.error("ERROR - An internal error occurred. Login process could not be completed for user [{}]", user.getId(), e);
				conn.rollback();
				throw new DatabaseException("An internal error occurred.", e);
			}
			
			//conn.commit();
			
		} catch(Exception e) {
			throw new DatabaseException("An internal error occurred.", e);
		}
		
	}
	
	private PreparedStatement buildPreparedStatementForCurrentUserInsert(Connection conn, String userId, String terminal,String sessionKey) throws SQLException {
		PreparedStatement pst = conn.prepareStatement("INSERT INTO TJN_CURRENT_USERS (USER_ID,TERMINAL,LOGIN_TIME,TJN_SESSION_ID) VALUES (?,?,?,?)");
		pst.setString(1, userId);
		pst.setString(2, terminal);
		pst.setTimestamp(3, new Timestamp(new Date().getTime()));
		pst.setString(4,sessionKey);
		return pst;
	}
	
	private PreparedStatement buildPreparedStatementForUserSessionInsert(Connection conn, int sessionId, String userId, String sessionState,String sessionKey) throws SQLException {
		PreparedStatement pst  = conn.prepareStatement("INSERT INTO USERSESSIONS (US_ID,US_USER_ID,US_LOGIN_TIMESTAMP,US_SESSION_STATE,TJN_SESSION_ID) VALUES(?,?,?,?,?)");
		pst.setInt(1, sessionId);
		pst.setString(2, userId);
		pst.setTimestamp(3, new Timestamp(new Date().getTime()));
		pst.setString(4, "A");
		pst.setString(5,sessionKey);
		return pst;
	}
	
	private PreparedStatement buildPreparedStatementToClearInvalidAttempts(Connection conn, String userId) throws SQLException {
		PreparedStatement pst = conn.prepareStatement("DELETE FROM FAILED_LOGIN_ATTEMPTS WHERE USER_ID=?");
		pst.setString(1, userId);
		return pst;
	}
	/*Modified by Padmavathi for TJNUN262-79 ends*/
	/*Added by Padmavathi for license starts*/
	public int getUserSessionCount() throws DatabaseException{
		int count=0;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT COUNT(US_USER_ID) AS USER_SESSION_COUNT FROM USERSESSIONS WHERE TRUNC(US_LOGIN_TIMESTAMP)=TRUNC(sysdate)");
				ResultSet rs=pst.executeQuery();){
				while(rs.next()){
					count=rs.getInt("USER_SESSION_COUNT");
				}
				
			}catch(SQLException e){
				logger.error("Could not get license key", e.getMessage());
				throw new DatabaseException("Could not fetch License key. Please contact Tenjin Support.");
			}
		return count;
	}
	/*Added by Padmavathi for license ends*/
	
	/*Added by Pushpa for desjardins VAPT fix starts*/
	public void removeSessionForUser(String userid) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pstmt = null;
		try {
			pstmt = conn
					.prepareStatement("DELETE FROM USERSESSIONS WHERE US_USER_ID=?");
			pstmt.setString(1, userid);
			pstmt.executeQuery();
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} finally {
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);
		}
		
	
	}

	/*Added by Pushpa for desjardins VAPT fix ends*/
}
