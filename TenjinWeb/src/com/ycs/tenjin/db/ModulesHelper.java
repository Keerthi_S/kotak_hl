/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ModulesHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 2-Nov-16			Sriram					Fix by Sriram for Defect#669 and 550 (2-Nov-2016)
* 02-12-2016            Parveen               REQ #TJN_243_04  Changing insert query
* 05-12-2016           Leelaprasad              Req#TJN_243_09
* 19-12-2016           Leelaprasad              DEFECT FIX#TEN-95
* 09-01-2017           Leelaprasad              Req#TENJINCG-6
* 02-02-2017           Leelaprasad              TENJINCG-94
* 20-06-2017           Padmavathi               TENJINCG-195
* 01-08-2016           Leelaprasad             TENJINCG-265
* 17-08-2017           Padmavathi              T25IT-174
* 24-08-2017           Manish	               T25IT-295
* 08-11-2017           Padmavathi              TENJINCG-421
* 15-11-2017           Padmavathi              TENJINCG-448
* 02-02-2018		   Pushpalatha				TENJINCG-568
* 19-02-2018           Padmavathi              for TENJINCG-545
* 30-07-2018			Preeti					Closed connections
* 22-12-2018		   Ashiki				   TJN262R2-63
* 12-04-2019		   Pushpalatha			   TENJINCG-1030
* 05-02-2020			Roshni					TENJINCG-1168
*/

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.aut.Group;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class ModulesHelper {
	private static final Logger logger = LoggerFactory.getLogger(ModulesHelper.class);	
	
	
	public void updateModule(Module t) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		 PreparedStatement pst = null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		try{
			 pst = conn.prepareStatement("UPDATE MASMODULE SET module_name = ?, ws_url = ?,ws_operation =?, module_menu_container=?,app_date_format=?,GROUP_NAME=? where module_code = ?" );
			 pst.setString(7, t.getCode());
			 pst.setString(1, t.getName());
			 pst.setString(2, t.getWsurl());
			 pst.setString(3, t.getWsop());
			 pst.setString(4, t.getMenuContainer());
			 /*Changed by Padmavathi for Req# Tenjincg-195 21-06-2017 starts*/ 
			 pst.setString(5, t.getDateFormat());
			 pst.setString(6, t.getGroup());
			 /*Changed by Padmavathi for updating group name ends*/ 
			 pst.executeUpdate();
			 pst.close();
			 
			 pst = conn.prepareStatement("DELETE FROM LRNR_FUNC_ACTIONS WHERE FUNC_CODE =?");
			 pst.setString(1, t.getCode());
			 pst.execute();
			 pst.close();
			 
			 pst = conn.prepareStatement("INSERT INTO LRNR_FUNC_ACTIONS (APP_ID,FUNC_CODE,LRNR_ACTION) VALUES (?,?,?)");
			 pst.setInt(1, 0);
			 pst.setString(2, t.getCode());
			 pst.setString(3, t.getPreLearnString());
			 pst.executeUpdate();
			 pst.close();
			 
		}catch(Exception e){
			throw new DatabaseException("Could not update Module due to an internal error",e);
		}finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
			
		}
	}
	public void persistModule(ModuleBean bean) throws DatabaseException, SQLException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		
		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}
		
		try{
			pst = conn.prepareStatement("INSERT INTO MASMODULE (MODULE_CODE,MODULE_NAME,APP_ID,MODULE_MENU_CONTAINER,GROUP_NAME,APP_DATE_FORMAT ) VALUES(?,?,?,?,?,?)");//changed app_code to app_id
			pst.setString(1, bean.getModuleCode());
			pst.setString(2, bean.getModuleName());
			pst.setInt(3, bean.getAut());
			pst.setString(4, bean.getMenuContainer());
			if(bean.getGroupName()!=null)
				pst.setString(5, bean.getGroupName());
			else
				pst.setString(5, "Ungrouped");
			pst.setString(6,bean.getDateFormat());
			pst.execute();
			
		}catch(Exception e){
			throw new DatabaseException("Could not create module",e);
		}
		finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
    
	
	public ArrayList<ModuleBean> hydrateModules(String application) throws DatabaseException, SQLException{
		ArrayList<ModuleBean> oModules = new ArrayList<ModuleBean>();
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		ResultSet rs=null;
		if(conn != null){
				
			try{
				/*modified by paneendra for sql injection starts*/
				pst = conn.prepareStatement("Select MasModule.Module_Code, MasModule.Module_Name,MasModule.Module_Menu_Container, MasModule.app_id,MasModule.APP_DATE_FORMAT From MasModule Inner join MasApplication On MasApplication.App_Id = MasModule.App_Id Where MasApplication.App_Name = ?");
				pst.setString(1,application);
				rs=pst.executeQuery();
				/*modified by paneendra for sql injection ends*/
				while(rs.next()){
					ModuleBean mBean = new ModuleBean();
					mBean.setApplicationName(application);
					mBean.setModuleCode(rs.getString("Module_Code"));
					mBean.setModuleName(rs.getString("Module_Name"));
					mBean.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
					mBean.setAut(rs.getInt("APP_ID"));//changed app_code to app_id
					/*Added by Pushpa for TENJINCG-1030 starts*/
					mBean.setDateFormat(rs.getString("APP_DATE_FORMAT"));
					/*Added by Pushpa for TENJINCG-1030 ends*/
					oModules.add(mBean);
				}
				return oModules;
			}
			catch(Exception e){
				logger.error("Could not hydrate modules for application " + application,e);
				throw new DatabaseException("Could not hydrate modules for application " + application,e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}
		else{
			throw new DatabaseException("Invalid database connection");
		}
	}
	
	/**********************************************************************
	 * Method depreceated by Sriram for Req#TJN_24_05
	 * 
	 */
		
	/*Added by Pushpalatha for Req#TENJINCG-568 starts*/

	public ArrayList<Api> hydrateApi(int application,String group_name) throws DatabaseException, SQLException{
		ArrayList<Api> oApi = new ArrayList<Api>();
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		ResultSet rs=null;
		long startMillis = System.currentTimeMillis();
		if(conn != null){
			
			try{
				if(group_name.equalsIgnoreCase(""))
					group_name="NONE";
				/*modified by paneendra for sql injection starts*/
				pst = conn.prepareStatement("Select * From TJN_AUT_APIS WHERE TJN_AUT_APIS.App_Id = ? and TJN_AUT_APIS.group_name= ? ORDER BY TJN_AUT_APIS.API_Code");
				pst.setInt(1,application);
				pst.setString(2,group_name);
				rs = pst.executeQuery();	
				/*modified by paneendra for sql injection ends*/
				while(rs.next()){
					Api api=new Api();
					api.setApplicationId(application);
					api.setCode(rs.getString("API_CODE"));
					api.setName(rs.getString("API_NAME"));
					api.setType(rs.getString("API_TYPE"));
					api.setUrl(rs.getString("API_URL"));
					api.setGroup(rs.getString("GROUP_NAME"));
					
					
					
					
					oApi.add(api);
				}
				
				
				return oApi;
			}
			catch(Exception e){
				logger.error("Could not hydrate modules for application " + application,e);
				long endMillis = System.currentTimeMillis();
				logger.debug("hydrateModules() completed in " + Utilities.calculateElapsedTime(startMillis, endMillis));
				throw new DatabaseException("Could not hydrate modules for application " + application,e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}
		else{
			long endMillis = System.currentTimeMillis();
			logger.debug("hydrateModules() completed in " + Utilities.calculateElapsedTime(startMillis, endMillis));
			throw new DatabaseException("Invalid database connection");
		}
	}
	/*Added by Pushpalatha for Req#TENJINCG-568 ends*/
	public List<Module> hydrateModulesInGroup(int application, String groupName) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		PreparedStatement pst = null;
		PreparedStatement pst2 = null;
		ResultSet rs = null;
		ResultSet rs2 =null;
		
		List<Module> functions = new ArrayList<Module>();
		
		try {
			String query = null;
			if(groupName == null || Utilities.trim(groupName).equalsIgnoreCase("") || groupName.equalsIgnoreCase("All")){
				
					
				query = "SELECT MODULE_CODE, MODULE_NAME, MODULE_MENU_CONTAINER FROM MASMODULE WHERE APP_ID=? ORDER BY MODULE_CODE";
				
				pst = conn.prepareStatement(query);
				pst.setInt(1, application);
			}else{
					
				query = "SELECT MODULE_CODE, MODULE_NAME, MODULE_MENU_CONTAINER FROM MASMODULE WHERE APP_ID=? and group_name=? ORDER BY MODULE_CODE";
				
				pst = conn.prepareStatement(query);
				pst.setInt(1, application);
				pst.setString(2, groupName);
				
			}
			
			rs = pst.executeQuery();
			
			while(rs.next()){
				Module module = new Module();
				module.setCode(rs.getString("MODULE_CODE"));
				module.setName(rs.getString("MODULE_NAME"));
				module.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
				
				/*Changed  by manish for the Defect T25IT-295 on 24-Aug-2017 starts*/
				module.setAut(application);
			
				pst2 = conn.prepareStatement("SELECT LRNR_END_TIME,LRNR_ID,LRNR_STATUS,LRNR_RUN_ID from LRNR_AUDIT_TRAIL WHERE LRNR_STATUS IN(?, ?) AND LRNR_FUNC_CODE=? AND LRNR_APP_ID=? ORDER BY LRNR_ID DESC");
				pst2.setString(1, "COMPLETE");
				pst2.setString(2, "IMPORTED");
				pst2.setString(3, module.getCode());
				pst2.setInt(4, application);
				/*Changed by Leelaprasad for the requirement TENJINCG-94 ends*/
				
				 rs2 = pst2.executeQuery();
				while(rs2.next()){
					LearnerResultBean result = new LearnerResultBean();
					result.setId(rs2.getInt("LRNR_ID"));
					result.setEndTimestamp(rs2.getTimestamp("LRNR_END_TIME"));
					result.setLearnStatus(rs2.getString("LRNR_STATUS"));
					module.setLastSuccessfulLearningResult(result);
					break;
				}
				
			functions.add(module);
			}
			
		} catch (SQLException e) {
			
			logger.error("ERROR fetching functions information", e);
			throw new DatabaseException("Could not fetch functions due to an internal error");
		} finally{
			
			DatabaseHelper.close(rs2);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
			
			
		}
		
		
		
		return functions;
	}
    
	public ArrayList<ModuleBean> hydrateModules(int application) throws DatabaseException, SQLException{
		ArrayList<ModuleBean> oModules = new ArrayList<ModuleBean>();
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		long startMillis = System.currentTimeMillis();
		if(conn != null){
			PreparedStatement pst = null;
			ResultSet rs=null;
			
			try{
				/*modified by paneendra for sql injection starts*/
				pst = conn.prepareStatement("Select MasModule.Module_Code, MasModule.Module_Name,MasModule.Module_Menu_Container, MasModule.ws_url, MasModule.ws_operation,MasModule.END_TimESTAMP,MasModule.APP_DATE_FORMAT,MasModule.GROUP_NAME From MasModule WHERE MasModule.App_Id = ? ORDER BY MasModule.Module_Code");
				pst.setInt(1, application);
				rs=pst.executeQuery();
				/*modified by paneendra for sql injection starts*/
				while(rs.next()){
					ModuleBean mBean = new ModuleBean();
					mBean.setAut(application);
					mBean.setModuleCode(rs.getString("Module_Code"));
					mBean.setModuleName(rs.getString("Module_Name"));
					mBean.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
					/*added by padmavathi for  TENJINCG-545 starts*/
					mBean.setGroupName(rs.getString("GROUP_NAME"));
					/*added by padmavathi for  TENJINCG-545 ends*/
					/*20-Oct-2014 WS: Starts*/
					mBean.setWSURL(rs.getString("WS_URL"));
					mBean.setWSOp(rs.getString("WS_OPERATION"));
					mBean.setDateFormat(rs.getString("APP_DATE_FORMAT"));
					
						if(rs.getTimestamp("END_TIMESTAMP") != null){
						TenjinConfiguration.getProperty("DATE_FORMAT");
						String endTs = new SimpleDateFormat().format(rs.getTimestamp("END_TIMESTAMP"));
						mBean.setLastLearntTimestamp(endTs);
					}else{
						mBean.setLastLearntTimestamp(null);
					}
					oModules.add(mBean);
				}
				
				long endMillis = System.currentTimeMillis();
				logger.debug(oModules.size() + " functions fetched in " + Utilities.calculateElapsedTime(startMillis, endMillis));
				return oModules;
			}
			catch(Exception e){
				logger.error("Could not hydrate modules for application " + application,e);
				long endMillis = System.currentTimeMillis();
				logger.debug("hydrateModules() completed in " + Utilities.calculateElapsedTime(startMillis, endMillis));
				throw new DatabaseException("Could not hydrate modules for application " + application,e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}
		else{
			long endMillis = System.currentTimeMillis();
			logger.debug("hydrateModules() completed in " + Utilities.calculateElapsedTime(startMillis, endMillis));
			throw new DatabaseException("Invalid database connection");
			
		}
	}
	
	public ArrayList<ModuleBean> hydrateModules(Connection conn,int application) throws DatabaseException, SQLException{
		ArrayList<ModuleBean> oModules = new ArrayList<ModuleBean>();
		
		
		long startMillis = System.currentTimeMillis();
		if(conn != null){
			PreparedStatement pst=null;
			ResultSet rs=null;
			
			try{
				/*modified by paneendra for sql injection starts*/
				pst=conn.prepareStatement("Select MasModule.Module_Code, MasModule.Module_Name,MasModule.Module_Menu_Container, MasModule.ws_url, MasModule.ws_operation,MasModule.END_TimESTAMP,MasModule.GROUP_NAME From MasModule WHERE MasModule.App_Id = ? ORDER BY MasModule.Module_Code");
				pst.setInt(1,application);
				rs=pst.executeQuery();
				/*modified by paneendra for sql injection ends*/
			
				while(rs.next()){
					ModuleBean mBean = new ModuleBean();
					mBean.setAut(application);
					mBean.setModuleCode(rs.getString("Module_Code"));
					mBean.setModuleName(rs.getString("Module_Name"));
					mBean.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
					/*20-Oct-2014 WS: Starts*/
					mBean.setWSURL(rs.getString("WS_URL"));
					mBean.setWSOp(rs.getString("WS_OPERATION"));
					//Change by nagareddy for test data path
					mBean.setGroupName(rs.getString("GROUP_NAME"));
					/************************************************************
					 * Change by Sriram for Tenjin v2.1 (Change data time of END_TIMESTAMP in MASMODULE from VARCHAR2 to DATE
					 */
					if(rs.getTimestamp("END_TIMESTAMP") != null){
						TenjinConfiguration.getProperty("DATE_FORMAT");
						String endTs = new SimpleDateFormat().format(rs.getTimestamp("END_TIMESTAMP"));
						mBean.setLastLearntTimestamp(endTs);
					}else{
						mBean.setLastLearntTimestamp(null);
					}
					/************************************************************
					 * Change by Sriram for Tenjin v2.1 (Change data time of END_TIMESTAMP in MASMODULE from VARCHAR2 to DATE ends
					 */
					/*20-Oct-2014 WS: Ends*/
					oModules.add(mBean);
				}
				
				long endMillis = System.currentTimeMillis();
				logger.debug(oModules.size() + " functions fetched in " + Utilities.calculateElapsedTime(startMillis, endMillis));
				return oModules;
			}
			catch(Exception e){
				logger.error("Could not hydrate modules for application " + application,e);
				long endMillis = System.currentTimeMillis();
				logger.debug("hydrateModules() completed in " + Utilities.calculateElapsedTime(startMillis, endMillis));
				throw new DatabaseException("Could not hydrate modules for application " + application,e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}
		}
		else{
			long endMillis = System.currentTimeMillis();
			logger.debug("hydrateModules() completed in " + Utilities.calculateElapsedTime(startMillis, endMillis));
			throw new DatabaseException("Invalid database connection");
		}
	}
	
	public ModuleBean hydrateModule(int application, String moduleCode) throws DatabaseException, SQLException{
		ArrayList<ModuleBean> oModules = new ArrayList<ModuleBean>();
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		long startMillis = System.currentTimeMillis();
		ModuleBean mBean = null;
		if(conn != null){
			
			ResultSet rs=null;
			PreparedStatement pst=null;
			try{
				/*modified by paneendra for sql injection starts*/
					pst=conn.prepareStatement("Select MasModule.GROUP_NAME,MasModule.Module_Code, MasModule.Module_Name,MasModule.Module_Menu_Container, MasModule.ws_url, MasModule.ws_operation, MasModule.END_TimESTAMP, MasModule.WS_END_TimESTAMP,MasModule.APP_DATE_FORMAT From MasModule WHERE MasModule.App_Id = ? and Masmodule.Module_Code = ?");
					pst.setInt(1, application);
					pst.setString(2, moduleCode);
					rs=pst.executeQuery();
					/*modified by paneendra for sql injection ends*/
				while(rs.next()){
					mBean = new ModuleBean();
					mBean.setAut(application);
					mBean.setModuleCode(rs.getString("Module_Code"));
					mBean.setModuleName(rs.getString("Module_Name"));
					mBean.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
					/*20-Oct-2014 WS: Starts*/
					mBean.setWSURL(rs.getString("WS_URL"));
					mBean.setWSOp(rs.getString("WS_OPERATION"));
					/*20-Oct-2014 WS: Ends*/
					/*02-Nov-2014 Adding Last Learnt Timestamp: Starts*/
					mBean.setLastLearntTimestamp(rs.getString("END_TIMESTAMP"));
					mBean.setWSLastLearntTimestamp(rs.getString("WS_END_TIMESTAMP"));
					/*02-Nov-2014 Adding Last Learnt Timestamp: Ends*/
					/*Added by padmavathi for T25IT-174 starts*/
					mBean.setDateFormat(rs.getString("APP_DATE_FORMAT"));
					/*Added by padmavathi for T25IT-174 ends*/
					mBean.setGroupName(rs.getString("GROUP_NAME"));
					oModules.add(mBean);
				}
				
				long endMillis = System.currentTimeMillis();
				logger.debug("TIMER >> hydrateModule(int application, String moduleCode) >> " + Utilities.calculateElapsedTime(startMillis, endMillis));
				return mBean;
			}
			catch(Exception e){
				logger.error("Could not hydrate module " + moduleCode + " for application " + application,e);
				long endMillis = System.currentTimeMillis();
				logger.debug("TIMER >> hydrateModule(int application, String moduleCode) >> " + Utilities.calculateElapsedTime(startMillis, endMillis));
				throw new DatabaseException("Could not hydrate module " + moduleCode + " for application " + application,e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}
		else{
			long endMillis = System.currentTimeMillis();
			logger.debug("TIMER >> hydrateModule(int application, String moduleCode) >> " + Utilities.calculateElapsedTime(startMillis, endMillis));
			throw new DatabaseException("Invalid database connection");
		}
	}
	public ArrayList<String> fetch_all_groups(int appid) throws DatabaseException, SQLException{
	
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		if(conn != null){
			ResultSet rs=null;
			PreparedStatement pst=null;
			ArrayList<String> list=new ArrayList<String>();
			try{
				/*modified by paneendra for sql injection starts*/
				
				pst=conn.prepareStatement("Select distinct group_name From tjn_aut_groups where app_id = ?");
				pst.setInt(1,appid);
				rs=pst.executeQuery();
				/*modified by paneendra for sql injection ends*/
				while(rs.next()){
					list.add(rs.getString("Group_Name"));
				}
				
				return list;
			}
			catch(Exception e){
				logger.error("Could not hydrate group names for application " + appid,e);
				throw new DatabaseException("Could not hydrate group names for application " + appid,e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}
		else{
			throw new DatabaseException("Invalid database connection");
		}
	}
	
	
	public Module hydrateModuleInfo(int application, String moduleCode) throws DatabaseException{
		new ArrayList<ModuleBean>();
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		Module mBean = null;
		if(conn != null){
			
			PreparedStatement pst=null;
			ResultSet rs=null;
			
			try{
				/*modified by paneendra for sql injection starts*/
				
				pst=conn.prepareStatement("SELECT * FROM MASMODULE A LEFT JOIN LRNR_FUNC_ACTIONS B ON (A.MODULE_CODE = B.FUNC_CODE) WHERE A.MODULE_CODE = ? AND A.APP_ID = ?");
				pst.setString(1,moduleCode);
				pst.setInt(2,application);
				rs=pst.executeQuery();
				
				/*modified by paneendra for sql injection ends*/
				while(rs.next()){
					mBean = new Module();
					mBean.setAut(application);
					mBean.setCode(rs.getString("Module_Code"));
					mBean.setName(rs.getString("Module_Name"));
					mBean.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
					/*20-Oct-2014 WS: Starts*/
					mBean.setWsurl(rs.getString("WS_URL"));
					mBean.setWsop(rs.getString("WS_OPERATION"));
					mBean.setPreLearnString(rs.getString("LRNR_ACTION"));
						mBean.setDateFormat(rs.getString("APP_DATE_FORMAT"));
					/*Changed by Padmavathi for Req# Tenjincg-195 21-06-2017 ends*/
				}
				
				return mBean;
			}
			catch(Exception e){
				logger.error("Could not hydrate module " + moduleCode + " for application " + application,e);
				throw new DatabaseException("Could not hydrate module " + moduleCode + " for application " + application,e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}
		else{
			throw new DatabaseException("Invalid database connection");
		}
	}
	
	/* added by Roshni for TENJINCG-1168 starts */
	public Module hydrateModuleInfo(int application, String moduleCode, Connection conn) throws DatabaseException{
		new ArrayList<ModuleBean>();
		
		Module mBean = null;
		if(conn != null){
			
			PreparedStatement pst=null;
			ResultSet rs=null;
			
			try{
				
				/*modified by paneendra for sql injection starts*/
				
				
				pst=conn.prepareStatement("SELECT * FROM MASMODULE A LEFT JOIN LRNR_FUNC_ACTIONS B ON (A.MODULE_CODE = B.FUNC_CODE) WHERE A.MODULE_CODE = ? AND A.APP_ID = ?");
				pst.setString(1,moduleCode);
				pst.setInt(2,application);
				rs=pst.executeQuery();
				/*modified by paneendra for sql injection ends*/
				while(rs.next()){
					mBean = new Module();
					mBean.setAut(application);
					mBean.setCode(rs.getString("Module_Code"));
					mBean.setName(rs.getString("Module_Name"));
					mBean.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
					/*20-Oct-2014 WS: Starts*/
					mBean.setWsurl(rs.getString("WS_URL"));
					mBean.setWsop(rs.getString("WS_OPERATION"));
					mBean.setPreLearnString(rs.getString("LRNR_ACTION"));
						mBean.setDateFormat(rs.getString("APP_DATE_FORMAT"));
					/*Changed by Padmavathi for Req# Tenjincg-195 21-06-2017 ends*/
				}
				
				return mBean;
			}
			catch(Exception e){
				logger.error("Could not hydrate module " + moduleCode + " for application " + application,e);
				throw new DatabaseException("Could not hydrate module " + moduleCode + " for application " + application,e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}
		}
		else{
			throw new DatabaseException("Invalid database connection");
		}
	}
    /* added by Roshni for TENJINCG-1168 ends */
    
	public Group hydrateGroupInfo(int appId, String groupName) throws DatabaseException, SQLException{
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		Group grp = null;
		if(conn != null){

			ResultSet rs=null;
			PreparedStatement pst=null;
			try{
				/*modified by paneendra for sql injection starts*/
				pst=conn.prepareStatement("SELECT * FROM TJN_AUT_GROUPS  WHERE APP_ID =? AND GROUP_NAME =?");
				pst.setInt(1,appId);
				pst.setString(2,groupName);
				rs=pst.executeQuery();
				/*modified by paneendra for sql injection ends*/
				while(rs.next()){
					grp = new Group();
					grp.setAut(appId);
					grp.setAppName(rs.getString("APP_NAME"));					
					grp.setGroupName(rs.getString("GROUP_NAME"));
					grp.setGroupDesc(rs.getString("GROUP_DESC"));
				}
				
				return grp;
			}
			catch(Exception e){
				logger.error("Could not hydrate group " + groupName + " for application " + appId,e);
				throw new DatabaseException("Could not hydrate group " + groupName + " for application " + appId,e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}
		else{
			throw new DatabaseException("Invalid database connection");
		}
	}
	
	/*modified by shruthi for sql injection starts*/
	public Group persistGroup(Group g)throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst1 = null;
		ResultSet rs1 = null;
		PreparedStatement pst = null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		try{	

			
			pst1 = conn.prepareStatement("SELECT 1 FROM TJN_AUT_GROUPS  WHERE APP_ID =? AND GROUP_NAME =?");
			pst1.setLong(1, g.getAut());
			pst1.setString(2, g.getGroupName());
			rs1=pst1.executeQuery();
			if(rs1.next())
			{
				g=null;
			}
			else
			{
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/
				pst = conn.prepareStatement("INSERT INTO TJN_AUT_GROUPS (APP_ID,APP_NAME,GROUP_NAME,GROUP_DESC) VALUES (?,?,?,?)");
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 ends*/
				pst.setInt(1, g.getAut());
				pst.setString(2, g.getAppName());
				pst.setString(3, g.getGroupName());
				pst.setString(4, g.getGroupDesc());
				
				pst.execute();

			}
		}catch(Exception e){
			logger.error("Could not insert AUT Group record",e);
			throw new DatabaseException("Could not insert record",e);
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(conn);
		}
		
		return g;
	}/*modified by shruthi for sql injection ends*/
	
		
		
		
	
	
	public ArrayList<ModuleBean> hydrateModulesAcrossAUTS() throws DatabaseException, SQLException{
		ArrayList<ModuleBean> oModules = new ArrayList<ModuleBean>();
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		long startMillis = System.currentTimeMillis();
		if(conn != null){
			Statement st=null;
			ResultSet rs=null;
			
			try{

				st = conn.createStatement();
				rs = st.executeQuery("Select MasModule.Module_Code, MasModule.Module_Name,MasModule.Module_Menu_Container, MasModule.ws_url,"
						+ "MasModule.ws_operation,MasModule.END_TimESTAMP,MasModule.app_id,MasModule.APP_DATE_FORMAT From MasModule ORDER BY MasModule.app_id,MasModule.Module_Code");
					while(rs.next()){
					ModuleBean mBean = new ModuleBean();
					mBean.setAut(rs.getInt("app_id"));
					mBean.setModuleCode(rs.getString("Module_Code"));
					mBean.setModuleName(rs.getString("Module_Name"));
					mBean.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
					/*20-Oct-2014 WS: Starts*/
					mBean.setWSURL(rs.getString("WS_URL"));
					mBean.setWSOp(rs.getString("WS_OPERATION"));
					mBean.setDateFormat(rs.getString("APP_DATE_FORMAT"));
					
					if(rs.getTimestamp("END_TIMESTAMP") != null){
						TenjinConfiguration.getProperty("DATE_FORMAT");
						String endTs = new SimpleDateFormat().format(rs.getTimestamp("END_TIMESTAMP"));
						mBean.setLastLearntTimestamp(endTs);
					}else{
						mBean.setLastLearntTimestamp(null);
					}
					oModules.add(mBean);
				}
				
				long endMillis = System.currentTimeMillis();
				logger.debug(oModules.size() + " functions fetched in " + Utilities.calculateElapsedTime(startMillis, endMillis));
				return oModules;
			}
			catch(Exception e){
				logger.error("Could not hydrate modules for all applications",e);
				long endMillis = System.currentTimeMillis();
				logger.debug("hydrateModules() completed in " + Utilities.calculateElapsedTime(startMillis, endMillis));
				throw new DatabaseException("Could not hydrate modules for all applications",e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(st);
				DatabaseHelper.close(conn);
			}
		}
		else{
			long endMillis = System.currentTimeMillis();
			logger.debug("hydrateModules() completed in " + Utilities.calculateElapsedTime(startMillis, endMillis));
			throw new DatabaseException("Invalid database connection");
		}
	}
	

	
	
	/*Changed by leelaprasad for the Requirement of Defect Fix #TEN-95 on 19-12-2016 starts*/
	public ArrayList<LearnerResultBean> hydrateSuccessfulLearningHistory(String functionCode, int appID) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);	
		PreparedStatement pst =null;
		ResultSet rs =null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
		ArrayList<LearnerResultBean> lrnrRuns = null;

		try{
			pst = conn.prepareStatement("Select * from lrnr_audit_trail where lrnr_app_id=? AND lrnr_func_code=? AND lrnr_status IN('COMPLETE','IMPORTED')");
			pst.setInt(1, appID);
			pst.setString(2, functionCode);

			rs = pst.executeQuery();
			lrnrRuns=new ArrayList<LearnerResultBean>();
			while(rs.next()){
				LearnerResultBean lrnrBean=new LearnerResultBean();
				lrnrBean.setId(rs.getInt("LRNR_RUN_ID"));
				lrnrBean.setStartTimestamp(rs.getTimestamp("LRNR_START_TIME"));
				lrnrBean.setEndTimestamp(rs.getTimestamp("LRNR_END_TIME"));
				lrnrBean.setLearnStatus(rs.getString("LRNR_STATUS"));
				lrnrBean.setMessage(rs.getString("LRNR_MESSAGE"));
				lrnrBean.setUserID(rs.getString("LRNR_USER_ID"));
				if(lrnrBean.getStartTimestamp() != null && lrnrBean.getEndTimestamp() != null){
					long startMillis =lrnrBean.getStartTimestamp().getTime();
					long endMillis =lrnrBean.getEndTimestamp().getTime();

					long millis = endMillis - startMillis;

					String eTime = String.format(
							"%02d:%02d:%02d",
							TimeUnit.MILLISECONDS.toHours(millis),
							TimeUnit.MILLISECONDS.toMinutes(millis)
							- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
									.toHours(millis)),
									TimeUnit.MILLISECONDS.toSeconds(millis)
									- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
											.toMinutes(millis)));
					lrnrBean.setElapsedTime(eTime);
				}
				lrnrRuns.add(lrnrBean);
			}


		} catch (SQLException e) {

			throw new DatabaseException("Could not fetch learning history for test function " + functionCode,e);
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return lrnrRuns;
}
	/*Changed by leelaprasad for the Requirement of Defect Fix #TEN-95 on 19-12-2016 ends*/

	/****************************************************************************************
	 * Method Added by Leelaprasad for Requirement#TENJINCG-6 ON 09-01-2017 starts
	 */
	
	
	 /*Changed by Leelaprasad for the requirement TENJINCG-265 starts*/
	/****************************************************************************************
	 * Method Added by Leelaprasad for Requirement#TENJINCG-265 ON 09-01-2017 starts
	 */

	public Map<String, List<String>> fetchModulePageField(int appId,
			String moduleCode) {	
			    Connection conn=null;
				try {
					conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				} catch (DatabaseException e1) {
					
					logger.error("Error ", e1);
				}
				
				if(conn == null){
					try {
						throw new DatabaseException("Invalid or no database connection");
					} catch (DatabaseException e) {
						
						logger.error("Error ", e);
					}
				}
				
				List<String> pageAreas=null;
				try {
					pageAreas = this.fetchPageAreas(conn, appId, moduleCode);
				} catch (DatabaseException e1) {
					
					logger.error("Error ", e1);
				}
				Map<String,List<String>> sheetData=new  LinkedHashMap<String,List<String>>();
				
				try{
				if(pageAreas.size()>0){
					
					ResultSet rs=null;PreparedStatement pst=null;
					List<String> pageAreafields=null;
					for (String page : pageAreas) {
				
				try{	
					pst = conn.prepareStatement("SELECT FLD_UNAME,FLD_SEQ_NO FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE=? AND FLD_PAGE_AREA=? AND FLD_TYPE NOT IN ('LINK','BUTTON','IMG') ORDER BY FLD_SEQ_NO");
					pst.setInt(1, appId);
					pst.setString(2, moduleCode);
					pst.setString(3, page.toString());				
					 rs = pst.executeQuery();
					 pageAreafields=new ArrayList<String>();
					 while(rs.next()){
						pageAreafields.add(rs.getString("FLD_UNAME")); 
					}
				}catch(Exception e){
					throw new DatabaseException(e.getMessage(),e);
				}finally{
					if(rs!=null){			
					try{				
							rs.close();					
					}catch(Exception e){
						
					}
				}
					if(pst!=null){			
						try{				
								pst.close();					
						}catch(Exception e){
							
						}
					}
				}
				sheetData.put(page.toString(), pageAreafields);
				
					}//end of for
					
				}//end of if
				}catch(Exception e){
					
				}finally{
					if(conn!=null){
						try {
							conn.close();
						} catch (SQLException e) {
							
							logger.error("Error ", e);
						}
					}
				}
				return sheetData;
		}
	public List<String> fetchPageAreas(Connection conn ,int appId,String moduleCode) throws DatabaseException{    
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		ResultSet rs =null;PreparedStatement pst=null;
		List<String> pageAreas=new ArrayList<String>();
		try{				
			 pst = conn.prepareStatement("SELECT PA_NAME FROM AUT_FUNC_PAGEAREAS WHERE PA_APP=? AND PA_FUNC_CODE=? ORDER BY PA_SEQ_NO");
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			 rs = pst.executeQuery();
			
			 while(rs.next()){
				pageAreas.add(rs.getString("PA_NAME"));	 
			}
		}catch(Exception e){
			throw new DatabaseException(e.getMessage(),e);
		}
		finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		return pageAreas;
		
	}
	

	public List<String> fetchWaitTimeMap(int appId, String moduleCode,String pageArea) throws DatabaseException {
		
		List<String> manualFields=new ArrayList<String>();
		

	    Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		ResultSet rs =null;PreparedStatement pst=null;
		int count=0;
		try{				
			/*Changed by Ashiki for TJN262R2-63 starts*/
			pst = conn.prepareStatement("SELECT FIELD_LABEL,WAIT_TIME_SEC FROM WAIT_TIME_FIELD_MAP WHERE APP_ID=? AND FUNCTION_CODE=? AND PAGE_AREA=?", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			/*Changed by Ashiki for TJN262R2-63 ends*/
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, pageArea);
			rs = pst.executeQuery();
			 if(rs.last())
				{
					count=rs.getRow();
					rs.beforeFirst();
				}
				if(count>0){
				
			while(rs.next())
			{
				String fieldLabel=rs.getString("FIELD_LABEL");
				String waitTime=rs.getString("WAIT_TIME_SEC");
				manualFields.add(fieldLabel+"<>"+waitTime);
			}
				}
		}catch(Exception e){
			throw new DatabaseException(e.getMessage(),e);
		}
		finally{
				DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return manualFields;
	}
	public void insertOrUpdateWaitTimeField(int appId, String moduleCode,
			 JSONArray manualObj) throws JSONException, DatabaseException {
		
		 Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			
			if(conn == null){
				throw new DatabaseException("Invalid or no database connection");
			}
		try{	
			for (int i = 0; i < manualObj.length(); i++) {  // **line 2**
				JSONObject childJSONObject = manualObj.getJSONObject(i);
				String srcSheet = childJSONObject.getString("srcSheet");
				String srcField  = childJSONObject.getString("srcField");
				String srcWaitTime  = childJSONObject.getString("srcWaitTime");
				boolean status=this.checkWaitTimeField(conn,appId,moduleCode,srcSheet,srcField);
				if(!status){
					//////insert/////////
					this.insertWaitTimeField(conn,appId,moduleCode,srcSheet,srcField,srcWaitTime);
		    	 
				}
				else{
					///////update///////
					this.updateWaitTimeField(conn,appId,moduleCode,srcSheet,srcField,srcWaitTime);
				}
		     
			}
		}
		catch(Exception e){
			logger.debug(e.getMessage());
		}
		finally{
			DatabaseHelper.close(conn);
		}
		
	}

	private boolean checkWaitTimeField(Connection conn,int appId, String moduleCode,String srcSheet,String srcField) throws DatabaseException {    
		
		boolean status=false;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		ResultSet rs =null;PreparedStatement pst=null;
		
		try{				
			pst = conn.prepareStatement("SELECT count(*) FROM WAIT_TIME_FIELD_MAP WHERE APP_ID=? AND FUNCTION_CODE=? AND PAGE_AREA=? AND FIELD_LABEL=?");
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, srcSheet);
			pst.setString(4, srcField);
			
			
			 rs = pst.executeQuery();	
			rs.next();
		int	result=rs.getInt(1);
		if(result>0){
			status=true;
		}
		}catch(Exception e){
			throw new DatabaseException(e.getMessage(),e);
		}
		finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			
		
		}
		return status;
		
	}
	private void insertWaitTimeField(Connection conn, int appId,
			String moduleCode, String srcSheet, String srcField,String srcWaitTime) throws DatabaseException {  	
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst=null;
		
		try{			
			pst = conn.prepareStatement("INSERT INTO WAIT_TIME_FIELD_MAP (APP_ID,FUNCTION_CODE,PAGE_AREA,FIELD_LABEL,WAIT_TIME_SEC)  VALUES (?,?,?,?,?)");
			
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, srcSheet);	
			pst.setString(4, srcField);
			pst.setInt(5, Integer.parseInt(srcWaitTime));
			
			pst.execute();
		}catch(Exception e){
			throw new DatabaseException(e.getMessage(),e);
		}
		finally{			
			
		DatabaseHelper.close(pst);
		}
		
		
	}
	private void updateWaitTimeField(Connection conn, int appId,
			String moduleCode, String srcSheet, String srcField,String srcWaitTime) throws DatabaseException {  	
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst=null;
		
		try{			
			pst = conn.prepareStatement("UPDATE WAIT_TIME_FIELD_MAP SET WAIT_TIME_SEC=? WHERE APP_ID=? and FUNCTION_CODE=?  and PAGE_AREA=? and FIELD_LABEL=? ");
			
			pst.setInt(1, Integer.parseInt(srcWaitTime));
			pst.setInt(2, appId);
			pst.setString(3, moduleCode);
			pst.setString(4, srcSheet);	
			pst.setString(5, srcField);	
			
			
			pst.execute();
		}catch(Exception e){
			throw new DatabaseException(e.getMessage(),e);
		}
		finally{			
				DatabaseHelper.close(pst);
		
		}
		
		
	}
	public void unMapSingleWaitTimeField(int appId, String moduleCode, String unMapSheet,
			String unMapField) throws DatabaseException {
		 Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			
			if(conn == null){
				throw new DatabaseException("Invalid or no database connection");
			}
			
			PreparedStatement pst=null;
			
			try{				
				
				pst = conn.prepareStatement("DELETE FROM WAIT_TIME_FIELD_MAP WHERE APP_ID=? AND FUNCTION_CODE=?  AND PAGE_AREA=? AND FIELD_LABEL=?");
				pst.setInt(1, appId);
				pst.setString(2, moduleCode);
				pst.setString(3, unMapSheet);	
				pst.setString(4, unMapField);	
				pst.execute();
			}catch(Exception e){
				
			}
			finally{
			
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		
	}
	public void unMapAllWaitTimeField(int appId, String moduleCode) throws DatabaseException {
		
		 Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			
			if(conn == null){
				throw new DatabaseException("Invalid or no database connection");
			}
			
			PreparedStatement pst=null;
			
			try{				
				
				pst = conn.prepareStatement("DELETE FROM WAIT_TIME_FIELD_MAP WHERE APP_ID=? AND FUNCTION_CODE=?");
				
				pst.setInt(1, appId);
				pst.setString(2, moduleCode);
				pst.execute();
			}catch(Exception e){
				
			}
			finally{
				
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
	}

	/*Changed by Leelaprasad for the requirement TENJINCG-265 ends*/
	
	/* Added for TENJINCG-314 */
	public List<Module> hydrateModulesForApplication(int appId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Module> modules = new ArrayList<Module>();
		try {
			pst = conn.prepareStatement("SELECT A.*, B.APP_NAME FROM MASMODULE A, MASAPPLICATION B WHERE B.APP_ID = A.APP_ID AND A.APP_ID=? ORDER BY A.MODULE_CODE");
			pst.setInt(1, appId);
			rs = pst.executeQuery();
			
			while(rs.next()) {
				Module mBean = new Module();
				mBean.setAut(appId);
				mBean.setCode(rs.getString("Module_Code"));
				mBean.setName(rs.getString("Module_Name"));
				mBean.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
				mBean.setDateFormat(rs.getString("APP_DATE_FORMAT"));
				mBean.setAppName(rs.getString("APP_NAME"));
				modules.add(mBean);
			}
			
			
		} catch (SQLException e) {
			
			logger.error("ERROR hydrating Functions for app", e);
			throw new DatabaseException("Could not fetch functions. Please contact Tenjin Support.");
		} finally {
			
				DatabaseHelper.close(rs);
					DatabaseHelper.close(pst);
					DatabaseHelper.close(conn);
			
		}
		
		return modules;
	}
	
	public List<Module> hydrateAllModules(Connection conn) throws DatabaseException {
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Module> modules = new ArrayList<Module>();
		try {
			pst = conn.prepareStatement("SELECT A.*, B.APP_NAME FROM MASMODULE A, MASAPPLICATION B WHERE B.APP_ID = A.APP_ID ORDER BY B.APP_NAME, A.MODULE_CODE");
			rs = pst.executeQuery();
			
			while(rs.next()) {
				Module mBean = new Module();
				mBean.setAut(rs.getInt("APP_ID"));
				mBean.setCode(rs.getString("Module_Code"));
				mBean.setName(rs.getString("Module_Name"));
				mBean.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
				mBean.setDateFormat(rs.getString("APP_DATE_FORMAT"));
				mBean.setAppName(rs.getString("APP_NAME"));
				modules.add(mBean);
			}
			
			
		} catch (SQLException e) {
			
			logger.error("ERROR hydrating Functions for app", e);
			throw new DatabaseException("Could not fetch functions. Please contact Tenjin Support.");
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
		return modules;
	}
	
	
	
	public List<Module> hydrateModulesForApplication(Connection conn, int appId, String[] functionCodes) throws DatabaseException {
		/*modified by shruthi for sql injection starts*/
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Module> modules = new ArrayList<Module>();
		for(String str:functionCodes ) {
		try {
			pst = conn.prepareStatement("SELECT A.*, B.APP_NAME FROM MASMODULE A, MASAPPLICATION B WHERE B.APP_ID = A.APP_ID AND A.APP_ID=? AND A.MODULE_CODE=? ORDER BY A.MODULE_CODE");
			pst.setString(1, str);
			pst.setInt(2, appId);
			rs = pst.executeQuery();
			
			while(rs.next()) {
				Module mBean = new Module();
				mBean.setAut(appId);
				mBean.setCode(rs.getString("Module_Code"));
				mBean.setName(rs.getString("Module_Name"));
				mBean.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
				mBean.setDateFormat(rs.getString("APP_DATE_FORMAT"));
				mBean.setAppName(rs.getString("APP_NAME"));
				modules.add(mBean);
			}
			
	
		} catch (SQLException e) {
			
			logger.error("ERROR hydrating Functions for app", e);
			throw new DatabaseException("Could not fetch functions. Please contact Tenjin Support.");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}}
		/*modified by shruthi for sql injection ends*/
		return modules;
	}
	
	/* Added for TENJINCG-314 ends*/
	
	/*added by manish for function group service on 19-Aug-2017 starts*/
	public boolean mapSingleFunction(int appId,String groupName, String functionCode) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		boolean mapped = true;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		try{
			
		 		 pst = conn.prepareStatement("UPDATE masmodule SET group_name = ? where app_id = ? and module_code = ?" );
				 pst.setString(1, groupName);
				 pst.setInt(2, appId);
				 pst.setString(3, functionCode);
				 pst.executeUpdate();
			 
		}catch(Exception e){
			mapped =false;
			throw new DatabaseException("Could not update group due to an internal error",e);
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return mapped;
	}
	
	
	
}
