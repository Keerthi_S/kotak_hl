/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RunHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 23-Sep-2016           Sriram Sridharan          Newly Added For 
* 27-Sep-2016			Sriram					Added by Sriram for Req#TJN_23_18
* 10-DEC-2016           Gangadhar Badagi        New method added to get runs for test case 
* 17-01-2017            Leelaprasad             TENJINCG-26
* 16-06-2017			Sriram					TENJINCG-187
* 03-07-2017			Roshni					TENJINCG-259
* 18-08-2017			Sriram					T25IT-197
* 18-08-2017			Manish					T25IT-105
* 15-Nov-2017			Gangadhar Badagi		For rerun a test
* 21-11-2017			Preeti					TENJINCG-484
*  22-Dec-2017			Sahana					Mobility
*  30-07-2018			Preeti					Closed connections
*  26-09-2018           Leelaprasad             TENJINCG-815
*  24-10-2018           Ramya                   TENJINCG-832
*  25-10-2018           Ramya                   TENJINCG-832
*  07-10-2018			Pushpa					TENJINCG-909
*  08-03-2019			Sahana					pCloudy
*  24-04-2019			Prem					TNJNR2-6
*  22-05-2019			Ashiki					TJN252-1
*  20-06-2019			Ashiki					V2.8-87
*  22-06-2019			Preeti					V2.8-161
*  08-04-2020			Lokanath				TENJINCG-1193
*  18-11-2020			Ashiki					TENJINCG-1213
*  30-03-2021           Paneendra                TENJINCG-1267
*/

package com.ycs.tenjin.db;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.pojo.TaskManifest;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiLearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ExtractionRecord;
import com.ycs.tenjin.bridge.pojo.aut.ExtractorResultBean;
import com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.handler.TestStepHandler;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.rest.JsonSerializationStrategy;
import com.ycs.tenjin.run.ExecutionSettings;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class RunHelper {
	private static final Logger logger = LoggerFactory.getLogger(RunHelper.class);
	/****************************
	 * Added by Sriram for Req#TJN_23_18
	 */
	
	public void beginRun(int runId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		
		try{
			pst = conn.prepareStatement("UPDATE MASTESTRUNS SET RUN_STATUS=? WHERE RUN_ID=?");
			pst.setInt(2, runId);
			pst.setString(1, BridgeProcess.IN_PROGRESS);
			pst.execute();
		} catch(SQLException e){
			logger.error("ERROR updating Run start for run [{}]", runId, e);
		} finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	public void endRun(int runId, String status) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		ResultSet rs=null;
		
		try{
			pst = conn.prepareStatement("UPDATE MASTESTRUNS SET RUN_STATUS=?,RUN_END_TIME=? WHERE RUN_ID=?");
			pst.setInt(3, runId);
			pst.setString(1, status);
			pst.setTimestamp(2, new Timestamp(new Date().getTime()));
			pst.execute();
			
			//This is for Checking the Execution Status Completed for Scheduler
			pst=conn.prepareStatement("SELECT SCH_ID FROM TJN_SCHEDULER WHERE RUN_ID=? AND STATUS='Executing'");
			pst.setInt(1, runId);
			rs=pst.executeQuery();
			int schId=0;
			if(rs.next()){
				schId=rs.getInt("SCH_ID");
			}
			rs.close();
			pst.close();
			if(schId!=0)
			{
				new SchedulerHelper().updateScheduleStatus("Completed",schId);
			}
		} catch(SQLException e){
			logger.error("ERROR updating Run End for run [{}]", runId, e);
		} finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	
	public JSONObject getLearningInformationforRun(int runId) throws Exception{
		JSONObject finalObject = new JSONObject();
		Connection conn =null;
		PreparedStatement pst=null;
		PreparedStatement pst1=null;
		PreparedStatement pst2=null;
		ResultSet rs=null;
		ResultSet rs1=null;
		ResultSet rs2=null;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			pst=conn.prepareStatement("SELECT M.*,R.RUN_USER FROM MASAPPLICATION M, MASTESTRUNS R WHERE M.APP_ID=R.RUN_APP_ID AND R.RUN_ID=?");
			pst.setInt(1, runId);
			rs=pst.executeQuery();
			Aut aut=null;
			String runUser=null;
			while(rs.next()){
				aut=new Aut();
				aut.setId(rs.getInt("APP_ID"));
				aut.setName(rs.getString("APP_NAME"));
				aut.setURL(rs.getString("APP_URL"));
				aut.setLoginReqd(rs.getString("APP_LOGIN_REQD"));
				aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
				aut.setPassword(rs.getString("APP_PASSWORD"));
				aut.setBrowser(rs.getString("APP_DEF_BROWSER"));
				aut.setAdapterPackage(rs.getString("APP_ADAPTER_PACKAGE"));
				runUser=rs.getString("RUN_USER");
			}
			
			String autLoginType=null;
			JSONArray functions=new JSONArray();
			if(aut!=null){
				pst1=conn.prepareStatement("SELECT M.*,L.LRNR_AUT_USER_TYPE FROM MASMODULE M, LRNR_AUDIT_TRAIL L WHERE L.LRNR_APP_ID=M.APP_ID AND M.module_code = L.LRNR_FUNC_CODE AND M.APP_ID=? AND L.LRNR_RUN_ID=?");
				pst1.setInt(1, aut.getId());
				pst1.setInt(2,runId);
				rs1=pst1.executeQuery();
				int counter=0;
				while(rs1.next()){
					ModuleBean mBean=new ModuleBean();
					mBean.setAut(rs1.getInt("APP_ID"));
					mBean.setModuleCode(rs1.getString("MODULE_CODE"));
					mBean.setModuleName(rs1.getString("MODULE_NAME"));
					mBean.setMenuContainer(rs1.getString("MODULE_MENU_CONTAINER"));
					mBean.setGroupName(rs1.getString("GROUP_NAME"));
					if(counter==0)
						autLoginType=rs1.getString("LRNR_AUT_USER_TYPE");
					counter++;

					Gson gson = new GsonBuilder().setExclusionStrategies(new JsonSerializationStrategy()).create();
					String oJson = gson.toJson(mBean);
					JSONObject func = new JSONObject(oJson);
					functions.put(func);
				}
				

				pst2=conn.prepareStatement("SELECT APP_LOGIN_NAME,APP_PASSWORD FROM USRAUTCREDENTIALS WHERE APP_ID=? AND APP_USER_TYPE=? AND USER_ID=?");
				pst2.setInt(1, aut.getId());
				pst2.setString(2, autLoginType);
				pst2.setString(3, runUser);
				rs2=pst2.executeQuery();
				while(rs2.next()){
					aut.setLoginName(rs2.getString("APP_LOGIN_NAME"));
					aut.setPassword(rs2.getString("APP_PASSWORD"));
				}
				
			}			
			finalObject.put("RUN_ID", runId);
			if(aut!=null)
				finalObject.put("Browser", aut.getBrowser());
			else
				finalObject.put("Browser", "");

			finalObject.put("AutLoginType", autLoginType);
			JSONObject sjson=null;
			if(aut!=null){
				Gson gson = new GsonBuilder().setExclusionStrategies(new JsonSerializationStrategy()).create();
				String oJson = gson.toJson(aut);
				sjson = new JSONObject(oJson);
				finalObject.put("Aut", sjson);
			}
			else{
				finalObject.put("Aut", "");
			}
			if(functions!=null)
				finalObject.put("Function", functions);
		} catch (SQLException e) {
			
			throw new DatabaseException("Could not get run information. Please contact Tenjin Support.");
		}catch (DatabaseException e) {
			
			throw new DatabaseException("Could not get run information. Please contact Tenjin Support.");
		}catch(Exception e){
			throw e;
		}finally{
			DatabaseHelper.close(rs2);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return finalObject;
	}
	
	public void beginLearningRun(int runId, boolean updateStartTime) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		PreparedStatement pst = null;
		
		try {
			String updateQuery = "";
			if(updateStartTime){
				updateQuery = "Update MasTestRuns Set run_status=?, run_start_time=? where run_id=?";
				pst = conn.prepareStatement(updateQuery);
				pst.setString(1, BridgeProcess.IN_PROGRESS);
				pst.setTimestamp(2, new Timestamp(new Date().getTime()));
				pst.setInt(3, runId);
			}else{
				updateQuery = "Update MasTestRuns Set run_status=? where run_id=?";
				pst = conn.prepareStatement(updateQuery);
				pst.setString(1, BridgeProcess.IN_PROGRESS);
				pst.setInt(2, runId);
			}
			
			pst.executeUpdate();
			
		} catch (SQLException e) {
			
			logger.error("ERROR updating Run Start", e);
		} finally{
		
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
	
	public void endLearningRun(int runId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		PreparedStatement pst = null;
		ResultSet rs=null;
		try {
			String updateQuery = "";
			
			updateQuery = "Update MasTestRuns Set run_status=?, run_end_time=? where run_id=?";
			pst = conn.prepareStatement(updateQuery);
			pst.setString(1, BridgeProcess.COMPLETE);
			pst.setTimestamp(2, new Timestamp(new Date().getTime()));
			pst.setInt(3, runId);
			
			pst.executeUpdate();
			
			pst=conn.prepareStatement("SELECT SCH_ID FROM TJN_SCHEDULER WHERE RUN_ID=? AND STATUS='Executing'");
			pst.setInt(1, runId);
			rs=pst.executeQuery();
			int schId=0;
			if(rs.next()){
				schId=rs.getInt("SCH_ID");
			}
			rs.close();
			if(schId!=0)
			{
				new SchedulerHelper().updateScheduleStatus("Completed",schId);
			}
			pst.close();
		} catch (SQLException e) {
			
			logger.error("ERROR updating Run End", e);
		} finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
	
	public void persistNewRun(Connection conn, TestRun run, TaskManifest manifest) throws DatabaseException{
		if(conn == null){
			logger.error("Database connection is null");
			throw new DatabaseException("Invalid Database Connection");
		}

		/* Changed by Sriram for TENJINCG-152 - API Testing Enablement*/
		/*if(manifest != null && manifest.getTaskType().equalsIgnoreCase(AdapterTask.EXECUTE)){*/
		if(manifest != null && 
			(manifest.getTaskType().equalsIgnoreCase(BridgeProcess.EXECUTE) || manifest.getTaskType().equalsIgnoreCase(BridgeProcess.APIEXECUTE))){
			/* Changed by Sriram for TENJINCG-152 - API Testing Enablement ends*/
			
			logger.info("Persisting [{}] Run", manifest.getTaskType());
			this.persistNewRun(conn, run, manifest.getTdUidMap());
		}else{
			logger.info("Persisting [{}] Run", manifest.getTaskType());
			/*Modified by Ashiki for TENJINCG-1213 starts*/
			this.persistNewRun(conn, run, manifest.getAut(), manifest.getFunctions(), manifest.getTaskType(), manifest.getApi(),manifest.getApis());
			/*Added by Ashiki for TENJINCG-1213 ends*/
		}
		
	}
	
	public void persistNewRun(TestRun run, TaskManifest manifest) throws DatabaseException{
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		try{
		if(conn == null){
			logger.error("Database connection is null");
			throw new DatabaseException("Invalid Database Connection");
		}
		
		if(manifest != null && manifest.getTaskType().equalsIgnoreCase(BridgeProcess.EXECUTE)){
			logger.info("Persisting Execution Run");
			this.persistNewRun(conn, run, manifest.getTdUidMap());
		}else if(manifest != null && manifest.getTaskType().equalsIgnoreCase(BridgeProcess.LEARN)){
			logger.info("Persisting Learning Run");
			/*Modified by Ashiki for TENJINCG-1213 starts*/
			this.persistNewRun(conn, run, manifest.getAut(), manifest.getFunctions(), manifest.getTaskType(), manifest.getApi(),manifest.getApis());
			/*Modified by Ashiki for TENJINCG-1213 ends*/
		}
		}
		
		finally{
			DatabaseHelper.close(conn);
		}
		
	}
	
	/***************
	 *  Newly Added For Data Extraction in Tenjin 2.4.1 - Sriram
	 * @param apis 
	 */
	/*private void persistNewRun(Connection conn, TestRun run, Aut aut, List<Module> functions, String adapterTask) throws DatabaseException{*/
	/*Modified by Ashiki for TENJINCG-1213 starts*/
	private void persistNewRun(Connection conn, TestRun run, Aut aut, List<Module> functions, String adapterTask, Api api, List<Api> apis) throws DatabaseException{
	/*Added by Ashiki for TENJINCG-1213 ends*/
		if(conn == null){
			logger.error("Database connection is null");
			throw new DatabaseException("Invalid Database Connection");
		}
		
		logger.info("Entered persistNewRun(Connection conn, TestRun run, Aut aut, List<Module> functions, String adapterTask, Api api) ");
		
		int runId = 0;
		
		
		
		logger.info("Getting new  run ID");
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst = conn.prepareStatement("SELECT coalesce(MAX(RUN_ID),0) AS RUN_ID FROM MASTESTRUNS");
			/*02-Mar-2015 R2.1 Database agnostic Changes: Ends*/
			rs = pst.executeQuery();

			while(rs.next()){
				runId = rs.getInt("RUN_ID");
			}
			rs.close();
			pst.close();
			runId++;
		}catch(SQLException e){
			logger.error("ERROR generating new Run ID",e);
			DatabaseHelper.rollback(conn);	
			throw new DatabaseException("Could not create new run due to an internal error");
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
		logger.info("New Run ID would be [{}]", runId);
		logger.info("Inserting into MASTESTRUNS");
		/*added by Sahana for Mobility,pCloudy: starts*/
		
		String insQuery = "INSERT INTO MASTESTRUNS ("
				+ "RUN_ID,"
				+ "RUN_TASK_TYPE, "
				+ "RUN_START_TIME, "
				+ "RUN_USER, "
				+ "RUN_STATUS,"
				+ "RUN_MACHINE_IP,"
				+ "RUN_CL_PORT, "
				+ "RUN_BROWSER_TYPE,"
				+ "RUN_APP_ID,"
				+ "RUN_DEVICE_REC_ID,"
				+ "RUN_DEVICE_FARM"
				+ ") values (?,?,?,?,?,?,?,?,?,?,?)";
		
		/*added by Sahana for Mobility,pCloudy: ends*/
		
		try{
			pst = conn.prepareStatement(insQuery);
			
			pst.setInt(1, runId);
			pst.setString(2, adapterTask);
			pst.setTimestamp(3, run.getStartTimeStamp());
			pst.setString(4, run.getUser());
			pst.setString(5, BridgeProcess.NOT_STARTED);
			pst.setString(6, run.getMachine_ip());
			pst.setInt(7, run.getTargetPort());
			pst.setString(8, run.getBrowser_type());
			pst.setInt(9, run.getAppId());
			/*added by Sahana for Mobility, pCloudy: starts*/
			pst.setInt(10, run.getDeviceRecId());
			pst.setString(11, run.getDeviceFarmFlag());
			/*added by Sahana for Mobility, pCloudy: ends*/
			
			pst.execute();
			run.setId(runId);
		}catch (SQLException e){
			logger.error("ERROR persisting Master Run info", e);
			DatabaseHelper.rollback(conn);	
			throw new DatabaseException("Could not create Run due to an internal error. Please contact Tenjin Support.");
		} finally{
			
			DatabaseHelper.close(pst);
			
		}
		logger.info("Inserting AUDIT  TRAIL");		
		try {
			if(BridgeProcess.LEARN.equalsIgnoreCase(adapterTask)){
				this.persistNewLearningRun(conn, run,aut,functions);
			}else if(BridgeProcess.EXTRACT.equalsIgnoreCase(adapterTask)){
				this.persistNewExtractionRun(conn, run, aut, functions);
			}
			
			/* Added by Sriram for TENJINCG-152 - API Testing Enablement */
			else if(BridgeProcess.APILEARN.equalsIgnoreCase(adapterTask)) {
				this.persistNewApiLearningRun(conn, run, aut, api);
			/*Added by Ashiki for TENJINCG-1213 starts*/
			}else if(BridgeProcess.APICODELEARN.equalsIgnoreCase(adapterTask)) {
				this.persistNewApiLearningRun(conn, run, aut, apis);
			}/*Added by Ashiki for TENJINCG-1213 ends*/
		} catch (DatabaseException e) {
			DatabaseHelper.rollback(conn);	
			logger.error("ERROR rolling back transaction. This may cause unwanted errors", e);
			throw e;
		} catch(Exception e){
			logger.error("ERROR occurred while persisting [{}] Run", adapterTask, e);
			DatabaseHelper.rollback(conn);	
			throw new DatabaseException("Could not create run due to an internal error. Please contact Tenjin Support.");
		}
		
		/*try{
			logger.info("Committing Run..");
				conn.commit();
		}catch(Exception e){
			logger.error("Could not commit run",e );
			throw new DatabaseException("Could not create run due to an internal error. Please contact Tenjin Support.");
		}*/
	}
	
	/*Added by Ashiki for TENJINCG-1213 starts*/
	private void persistNewApiLearningRun(Connection conn, TestRun run, Aut aut, List<Api> apis) throws DatabaseException {
		try {
			LearningHelper lHelper = new LearningHelper();
			lHelper.insertApiLearningAuditTrail(conn, aut.getId(), apis, run.getId(), run.getUser(), BridgeProcess.QUEUED);
		} catch (DatabaseException e) {
			logger.error("ERROR entering audit informatio for learning run [{}]", run.getId(), e);

			try{
				conn.rollback();
			}catch(SQLException e1){
				logger.error("ERROR rolling back transaction. This may cause unwanted errors", e1);
			}
			
			throw e;
		} 
		
			
	}
	/*Added by Ashiki for TENJINCG-1213 ends*/
	private void persistNewExtractionRun(Connection conn, TestRun run, Aut aut, List<Module> functions) throws DatabaseException{
		try{
			new ExtractionHelper().insertExtractorAuditTrailMaster(conn, run.getId(), aut.getId(), functions, run.getUser());
		} catch(DatabaseException e){
			try{
				conn.rollback();
			}catch(SQLException e1){
				logger.error("ERROR rolling back transaction. This may cause unwanted errors", e1);
			}
			throw e;
		} 
	}
	
	/***************
	 *  Newly Added For Data Extraction in Tenjin 2.4.1 - Sriram ends
	 */
	
	private void persistNewLearningRun(Connection conn, TestRun run, Aut aut, List<Module> functions) throws DatabaseException{
		
		
		try {
			LearningHelper lHelper = new LearningHelper();
			lHelper.insertlearnaudittrail(conn, run.getUser(), run.getMachine_ip(), aut.getId(), functions, BridgeProcess.QUEUED, run.getId());
			
			
		} catch (DatabaseException e) {
			
			logger.error("ERROR entering audit informatio for learning run [{}]", run.getId(), e);

			try{
				conn.rollback();
			}catch(SQLException e1){
				logger.error("ERROR rolling back transaction. This may cause unwanted errors", e1);
			}
			
			throw e;
		}  finally{

		}
		
	}
	/****************************
	 * Added by Sriram for Req#TJN_23_18 ends
	 */
	
	/* Added by Sriram for TENJINCG-152 - API Testing Enablement */
	private void persistNewApiLearningRun(Connection conn, TestRun run, Aut aut, Api api) throws DatabaseException{
		
		
		try {
			LearningHelper lHelper = new LearningHelper();
			lHelper.insertApiLearningAuditTrail(conn, aut.getId(), api, run.getId(), run.getUser(), BridgeProcess.QUEUED);
			
			
		} catch (DatabaseException e) {
			
			logger.error("ERROR entering audit informatio for learning run [{}]", run.getId(), e);

			try{
				conn.rollback();
			}catch(SQLException e1){
				logger.error("ERROR rolling back transaction. This may cause unwanted errors", e1);
			}
			
			throw e;
		}  
		
	}
	/* Added by Sriram for TENJINCG-152 - API Testing Enablement ends*/
	/*changed by Ramya for TENJINCG-832 starts*/
	 /* public void persistNewRun(Connection conn, TestRun run, Map<Integer, List<String>> tdUidMap) throws DatabaseException{*/
	public int persistNewRun(Connection conn, TestRun run, Map<Integer, List<String>> tdUidMap) throws DatabaseException{
		/*changed by Ramya for TENJINCG-832 ends*/
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
		int runId = 0;
		
		
		PreparedStatement pst =null;
		ResultSet rs=null;
		try{
			 pst = conn.prepareStatement("SELECT coalesce(MAX(RUN_ID),0) AS RUN_ID FROM MASTESTRUNS");
			/*02-Mar-2015 R2.1 Database agnostic Changes: Ends*/
			 rs = pst.executeQuery();

			while(rs.next()){
				runId = rs.getInt("RUN_ID");
			}
			rs.close();
			pst.close();
			runId++;
		}catch(Exception e){
			logger.error("ERROR generating new Run ID",e);
			throw new DatabaseException("Could not create new run due to an internal error");
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
		run.setId(runId);
		
		try{
			
			pst = conn.prepareStatement("INSERT INTO MASTESTRUNS (RUN_ID, RUN_PRJ_ID, RUN_TS_REC_ID, RUN_START_TIME, RUN_USER, RUN_STATUS,RUN_MACHINE_IP,RUN_CL_PORT,RUN_BROWSER_TYPE,TOTAL_TCS,RUN_PRJ_NAME,RUN_DOMAIN_NAME,RUN_TASK_TYPE,PARENT_RUN_ID,RUN_DEVICE_REC_ID,RUN_DEVICE_FARM) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			/*Modified by Preeti for TENJINCG-484 ends*/
			pst.setInt(1,runId);
			pst.setInt(2, run.getProjectId());
			pst.setInt(3, run.getTestSet().getId());
			pst.setTimestamp(4, run.getStartTimeStamp());/* Fix for timestamp Changing the type- Arvind */			
			pst.setString(5, run.getUser());
			pst.setString(6,run.getStatus());
			pst.setString(7, run.getMachine_ip());
			/*Added by Preeti for TENJINCG-484 starts*/
			pst.setInt(8, run.getTargetPort());
			/*Added by Preeti for TENJINCG-484 ends*/
			pst.setString(9, run.getBrowser_type());
			pst.setInt(10, run.getTestSet().getTests().size());
			pst.setString(11, run.getProjectName());
			pst.setString(12, run.getDomainName());
			//changed by NagaBabu for the execution starts
			pst.setString(13, run.getTaskType());
			//changed by NagaBabu for the execution ends
			/* Addded by Roshni for TENJINCG-259 */
			pst.setInt(14, run.getParentRunId());
			/*added by Sahana for Mobility: starts*/
			pst.setInt(15, run.getDeviceRecId());
			/*added by Sahana for Mobility: ends*/
			/*added by Sahana for pCloudy: starts*/
			pst.setString(16, run.getDeviceFarmFlag());
			/*added by Sahana for pCloudy: ends*/
			pst.execute();
			pst.close();
		}catch(Exception e){
			logger.error("ERROR persisting new run",e);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				throw new DatabaseException("Could not create new run due to an internal error");
			}
			
		}
		finally{
			DatabaseHelper.close(pst);
		}
		
		try{
			for(TestCase t:run.getTestSet().getTests()){
				for(TestStep s:t.getTcSteps()){
					List<String> tduids = tdUidMap.get(s.getRecordId());
					for(int i=1;i<=s.getTotalTransactions();i++){
						String tduid = tduids.get(i-1);
						this.insertTestStepIterationStatus(conn, "N", run.getId(), s.getRecordId(), i, tduid);
					}
				}
			}
		}catch(Exception e){
			logger.error("ERROR updating status of test cases to Not Started",e);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				
				logger.error("Error ", e1);
			}
			
			throw new DatabaseException("Could not create new run due to an internal error");
		}
		
		/*try{
			conn.commit();
		}catch(Exception e){
			
			try {
				conn.rollback();
			} catch (SQLException e1) {
				logger.error("ERROR committing new Run {}", run.getId(),e);
			}
			throw new DatabaseException("Could not create new run due to an internal error");
		}*/
		/*Added by Ramya for TENGINCG-832 starts*/
		return runId;
		/*Added by Ramya for TENGINCG-832 ends*/ 
	}
	
	
	
	
	public void insertTestStepIterationStatus(Connection conn, String status, int runId, int tsRecId, int iterationNumber, String tduid) throws DatabaseException{
		
		PreparedStatement pst = null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
		
		try{
			pst = conn.prepareStatement("INSERT INTO RUNRESULT_TS_TXN (RUN_ID,TXN_NO,TSTEP_REC_ID,TSTEP_RESULT,TSTEP_TDUID) VALUES(?,?,?,?,?)");
			pst.setInt(1, runId);
			pst.setInt(2, iterationNumber);
			pst.setInt(3, tsRecId);
			pst.setString(4, status);
			pst.setString(5, tduid);
			pst.execute();
		}catch(Exception e){
			logger.error("ERROR inserting record in RUNRESULT_TS_TXN for Run ID {}, Test STep Record ID {}, Iteration Number {}", runId, tsRecId,iterationNumber,e);
			throw new DatabaseException("Could not update Test case status");
		}finally{
			DatabaseHelper.close(pst);
		}
	}
	/*Added by Ramya for TENJINCG-832 starts*/
	/*Modified by padmavathi for schedule apis*/ 
	public void insertExecutionSettings(int runId,TestSet ts,String client,String browserType,int screenshotOption,String type)throws SQLException, DatabaseException
	{
		/*Modified by padmavathi for schedule apis ends*/
		try(Connection conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			/*Modified by Pushpa for TENJINCG-909 starts*/
			PreparedStatement pst=conn.prepareStatement("INSERT INTO TJN_EXEC_SETTINGS (RUN_ID,CLIENT,BROWSER,SCREENSHOT_OPTION) VALUES(?,?,?,?)");){
			String scrnShotOption="";
			
			if(screenshotOption==0)
			{
				scrnShotOption="Never Capture";
			}
			else if(screenshotOption==1)
			{
				scrnShotOption="Capture Only for Failures / Errors";
			}
			else if(screenshotOption==2)
			{
				scrnShotOption="Capture For All Messages";
			}
			else if(screenshotOption==3)
			{
				scrnShotOption="Capture At the End of Each Page";
			}
			pst.setInt(1, runId);
			pst.setString(2, client);
			
			/*Modified by Preeti for V2.8-161 starts*/
			Set<String> browserList = new LinkedHashSet<String>();
			if(browserType!=null && browserType.equalsIgnoreCase("APPDEFAULT")){
				if(ts != null && ts.getTests().size() > 0){
					for(TestCase t:ts.getTests()){
						int stepCount = 0;
						for(TestStep step:t.getTcSteps()){
							stepCount++;
							
									browserType=new TestStepHandler().getAppDefaultBrowser(step.getAppId());
								browserList.add(browserType);
						}
					}
				}
				String browser="";
				for(String browserName:browserList){
					browser=browser + browserName + ",";
				}
				pst.setString(3, browser.substring(0,browser.length()-1));
			}
			else
				pst.setString(3, browserType);
			/*Modified by Preeti for V2.8-161 ends*/
			pst.setString(4, scrnShotOption);
			pst.execute();
			/*Modified by Pushpa for TENJINCG-909 ends*/
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new DatabaseException("Could not insert");
		}
		
	}
    /*Modified by Ramya for TENJINCG-832 starts
     * public ExecutionSettings fetchExecutionSettings() throws DatabaseException*/
	public ExecutionSettings fetchExecutionSettings(int runId) throws DatabaseException
	/*Modified by Ramya for TENJINCG-832 ends*/
	{

		ExecutionSettings exeSettings=new ExecutionSettings();
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement  pst=conn.prepareStatement("SELECT * FROM TJN_EXEC_SETTINGS WHERE RUN_ID=?");){
			pst.setInt(1, runId);
		
			try(ResultSet rs=pst.executeQuery();){
				while(rs.next()){
				exeSettings.setRunId(rs.getInt("RUN_ID"));
				exeSettings.setClient(rs.getString("CLIENT"));
				exeSettings.setBrowser(rs.getString("BROWSER"));
				exeSettings.setScrnShotOption(rs.getString("SCREENSHOT_OPTION"));
				}
		}} catch (Exception e) {
			
			logger.error(e.getMessage());
			throw new DatabaseException("Could not fetch the details from TJN_EXEC_SETTINGS");
		
		}
		return exeSettings;
	}
	/*Added by Ramya for TENJINCG-832 ends*/
	public int getNewRunIdForLearning() throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		int newRunId = 0;
		
		try{
			pst = conn.prepareStatement("SELECT coalesce(MAX(LRNR_RUN_ID),0) AS RUN_ID FROM LRNR_AUDIT_TRAIL");
			rs = pst.executeQuery();
			while(rs.next()){
				newRunId = rs.getInt("RUN_ID") + 1;
			}
			logger.debug("Inserting temp record to avoid concurrency issues");
			pst = conn.prepareStatement("INSERT INTO LRNR_AUDIT_TRAIL (LRNR_USER_ID,LRNR_IP_ADDRESS,LRNR_APP_ID,LRNR_FUNC_CODE,LRNR_START_TIME,LRNR_RUN_ID) VALUES (?,?,?,?,?,?)");
			pst.setString(1, "TEMP");
			pst.setString(2, "TEMP");
			pst.setInt(3, 0);
			pst.setString(4, "TEMP");
			pst.setTimestamp(5, new Timestamp(new Date().getTime()));
			pst.setInt(6, newRunId);
			pst.execute();
			
		}catch(SQLException e){
			logger.error("Could not create new Learning Run", e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return newRunId;
	}
	
	public TestRun hydrateLearningRun(int runId) throws DatabaseException{
		TestRun run = null;
		
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			pst = conn.prepareStatement("SELECT A.*, B.MODULE_NAME,B.GROUP_NAME,B.MODULE_MENU_CONTAINER FROM LRNR_AUDIT_TRAIL A, MASMODULE B WHERE B.APP_ID=A.LRNR_APP_ID AND B.MODULE_CODE=A.LRNR_FUNC_CODE AND A.LRNR_RUN_ID=? ORDER BY A.LRNR_ID");
			pst.setInt(1, runId);
			
			rs = pst.executeQuery();
			List<Module> functions = new ArrayList<Module>();
			Module module = null;
			LearnerResultBean result = null;
			int total = 0;
			int executed = 0;
			int passed = 0;
			int error = 0;
			while(rs.next()){
				module = new Module();
				result = new LearnerResultBean();
				
				result.setStartTimestamp(rs.getTimestamp("LRNR_START_TIME"));
				result.setEndTimestamp(rs.getTimestamp("LRNR_END_TIME"));
				result.setLearnStatus(rs.getString("LRNR_STATUS"));
				result.setMessage(rs.getString("LRNR_MESSAGE"));
				module.setLastSuccessfulLearningResult(result);
				module.setCode(rs.getString("LRNR_FUNC_CODE"));
				module.setName(rs.getString("MODULE_NAME"));
				module.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
				module.setGroup(rs.getString("GROUP_NAME"));
				functions.add(module);
				total++;
				
				if(result.getLearnStatus().equalsIgnoreCase("complete")){
					passed++;
					executed++;
				}else if(result.getLearnStatus().equalsIgnoreCase("aborted") || result.getLearnStatus().equalsIgnoreCase("error")){
					error++;
					executed++;
				}
			}
			
			pst.close();
			rs.close();
			
			run = new TestRun();
			run.setId(runId);
			run.setFunctions(functions);
			run.setTotalTests(total);
			run.setPassedTests(passed);
			run.setErroredTests(error);
			run.setExecutedTests(executed);
		} catch (SQLException e) {
			
			logger.error("ERROR Occurred while fetching details of Learning Run [{}]", runId, e);
			throw new DatabaseException("Could not fetch learning status due to an unexpected error. Please contact Tenjin Support.");
		} finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return run;
	}
	
	
	
	
	
	
	/******************************
	 * Added by Sriram for Req#TJN_23_18
	 */
	public TestRun hydrateRun(Connection conn, int runId) throws DatabaseException{
		
		if(conn == null){
			logger.error("DB Connection is null");
			throw new DatabaseException("Invalid or no Database Connection");
		}
		
		TestRun run = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		logger.info("Getting Basic Run Information for ID [{}]", runId);
		try {
			pst = conn.prepareStatement("SELECT * FROM MASTESTRUNS WHERE RUN_ID =?");
			pst.setInt(1, runId);
			rs = pst.executeQuery();
			
			while(rs.next()){
				run = new TestRun();
				run.setId(rs.getInt("RUN_ID"));
				run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
				run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
				run.setUser(rs.getString("RUN_USER"));
				run.setMachine_ip(rs.getString("RUN_MACHINE_IP"));
				run.setBrowser_type(rs.getString("RUN_BROWSER_TYPE"));
				run.setProjectName(rs.getString("RUN_PRJ_NAME"));
				run.setDomainName(rs.getString("RUN_DOMAIN_NAME"));
				run.setTargetPort(rs.getInt("RUN_CL_PORT"));
				run.setTaskType(rs.getString("RUN_TASK_TYPE"));
				run.setProjectId(rs.getInt("RUN_PRJ_ID"));
				run.setAppId(rs.getInt("RUN_APP_ID"));
				int tsId = rs.getInt("RUN_TS_REC_ID");
				run.setTestSetRecordId(tsId);
				/*Added by sahana for pCloudy: Starts*/
				/*Modified by Prem for TNJNR2-6 : Starts*/
				/*if(rs.getInt("RUN_DEVICE_REC_ID")>0 && rs.getString("RUN_DEVICE_FARM")!=null && rs.getString("RUN_DEVICE_FARM").equalsIgnoreCase("Y"))*/
				if(rs.getInt("RUN_DEVICE_REC_ID")>0 && rs.getString("RUN_DEVICE_FARM")!=null)
				/*Modified by Prem for TNJNR2-6 : Ends*/
				{
					run.setDeviceRecId(rs.getInt("RUN_DEVICE_REC_ID"));
					run.setDeviceFarmFlag(rs.getString("RUN_DEVICE_FARM"));
				}
				/*Added by sahana for pCloudy: Ends*/
			}
		} catch (SQLException e) {
			
			logger.error("ERROR Getting basic information of run [{}] from Master Table", runId, e);
			throw new DatabaseException("Could not fetch Run Information. Please contact Tenjin Support.");
		} finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
		if(run == null){
			logger.error("Run for ID [{}] is null", runId);
			throw new DatabaseException("Run ID [" + runId + "] is invalid.");
		}
		
		
		if(Utilities.trim(run.getTaskType()).equalsIgnoreCase(BridgeProcess.LEARN)){
			this.hydrateRunDataForLearning(conn, run);
		}else if(Utilities.trim(run.getTaskType()).equalsIgnoreCase(BridgeProcess.EXECUTE)){
			this.hydrateRunDataForExecution(conn, run);
		}else if(Utilities.trim(run.getTaskType()).equalsIgnoreCase(BridgeProcess.EXTRACT)){
			this.hydrateRunDataForExtraction(conn, run);
		}
		/******************************
		 * Added by Sriram for TENJINCG-169 (Schedule API Learning)
		 */
		else if(Utilities.trim(run.getTaskType()).equalsIgnoreCase(BridgeProcess.APILEARN)){
			this.hydrateRunDataForApiLearning(conn, run);
		}
		/******************************
		 * Added by Sriram for TENJINCG-169 (Schedule API Learning) ends
		 */
		
		return run;
	}
	
	public TestRun hydrateRun(int runId) throws DatabaseException{
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if(conn == null){
			logger.error("DB Connection is null");
			throw new DatabaseException("Invalid or no Database Connection");
		}
		
		TestRun run = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		logger.info("Getting Basic Run Information for ID [{}]", runId);
		try {
			pst = conn.prepareStatement("SELECT * FROM MASTESTRUNS WHERE RUN_ID =?");
			pst.setInt(1, runId);
			rs = pst.executeQuery();
			
			while(rs.next()){
				run = new TestRun();
				run.setId(rs.getInt("RUN_ID"));
				run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
				run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
				run.setUser(rs.getString("RUN_USER"));
				run.setMachine_ip(rs.getString("RUN_MACHINE_IP"));
				run.setBrowser_type(rs.getString("RUN_BROWSER_TYPE"));
				run.setProjectName(rs.getString("RUN_PRJ_NAME"));
				run.setDomainName(rs.getString("RUN_DOMAIN_NAME"));
				run.setTargetPort(rs.getInt("RUN_CL_PORT"));
				run.setTaskType(rs.getString("RUN_TASK_TYPE"));
				run.setProjectId(rs.getInt("RUN_PRJ_ID"));
				run.setAppId(rs.getInt("RUN_APP_ID"));
				run.setCompletionStatus(rs.getString("RUN_STATUS"));
				int tsId = rs.getInt("RUN_TS_REC_ID");
				run.setTestSetRecordId(tsId);
			}
		} catch (SQLException e) {
			
			logger.error("ERROR Getting basic information of run [{}] from Master Table", runId, e);
			try{
				conn.close();
			}catch(Exception ignore){}
		
			throw new DatabaseException("Could not fetch Run Information. Please contact Tenjin Support.");
		} finally{
			
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			
		}
		
		if(run == null){
			logger.error("Run for ID [{}] is null", runId);
			throw new DatabaseException("Run ID [" + runId + "] is invalid.");
		}
		
		try{
		if(Utilities.trim(run.getTaskType()).equalsIgnoreCase(BridgeProcess.LEARN)){
			this.hydrateRunDataForLearning(conn, run);
		}else if(Utilities.trim(run.getTaskType()).equalsIgnoreCase(BridgeProcess.EXECUTE)){
			this.hydrateRunDataForExecution(conn, run);
		}else if(Utilities.trim(run.getTaskType()).equalsIgnoreCase(BridgeProcess.EXTRACT)){
			this.hydrateRunDataForExtraction(conn, run);
		}/******************************
		 * Added by Sriram for TENJINCG-169 (Schedule API Learning)
		 */
		else if(Utilities.trim(run.getTaskType()).equalsIgnoreCase(BridgeProcess.APILEARN)){
			this.hydrateRunDataForApiLearning(conn, run);
			/*Added by Ashiki for TENJINCG-1213 starts*/
		}else if(Utilities.trim(run.getTaskType()).equalsIgnoreCase(BridgeProcess.APICODELEARN)) {
			this.hydrateRunDataForApiCodeLearning(conn, run);
		}
		/*Added by Ashiki for TENJINCG-1213 ends*/
		/******************************
		 * Added by Sriram for TENJINCG-169 (Schedule API Learning) ends
		 */
		
		}finally{
		DatabaseHelper.close(conn);
		}
		return run;
	}
	
	/*Added by Ashiki for TENJINCG-1213 starts*/
	 private void hydrateRunDataForApiCodeLearning(Connection conn, TestRun run) throws DatabaseException {

			if(conn == null){
				logger.error("DB Connection is null");
				throw new DatabaseException("Invalid or no Database Connection");
			}
			
			PreparedStatement pst = null;
			ResultSet rs = null;
			
			try {
				pst = conn.prepareStatement("SELECT A.*, B.API_CODE,B.API_NAME,B.API_TYPE,B.API_URL FROM LRNR_AUDIT_TRAIL_API A, TJN_AUT_APIS B WHERE B.APP_ID=A.LRNR_APP_ID AND B.API_CODE=A.LRNR_API_CODE AND A.LRNR_RUN_ID=? ORDER BY A.LRNR_ID");
				pst.setInt(1, run.getId());
				
				rs = pst.executeQuery();
				List<ApiOperation> operations = new ArrayList<ApiOperation>();
				List<Api> apis = new ArrayList<Api>();
				Api api = new Api();
				ApiLearnerResultBean result = null;
				int total = 0;
				int executed = 0;
				int passed = 0;
				int error = 0;
				while(rs.next()){
					api = new Api();
					result = new ApiLearnerResultBean();
					api.setCode(rs.getString("API_CODE"));
					api.setName(rs.getString("API_NAME"));
					api.setType(rs.getString("API_TYPE"));
					api.setUrl(rs.getString("API_URL"));
					result.setStartTimestamp(rs.getTimestamp("LRNR_START_TIME"));
					result.setEndTimestamp(rs.getTimestamp("LRNR_END_TIME"));
					result.setStatus(rs.getString("LRNR_STATUS"));
					result.setMessage(rs.getString("LRNR_MESSAGE"));
					result.setId(rs.getInt("LRNR_ID"));
					ApiOperation op = new ApiOperation();
					op.setName(rs.getString("LRNR_OPERATION"));
					op.setLastSuccessfulLearningResult(result);
					op.setApiCode(rs.getString("API_CODE"));
					operations.add(op);
					total++;
					
					if(result.getStatus().equalsIgnoreCase("complete")){
						passed++;
						executed++;
					}else if(result.getStatus().equalsIgnoreCase("aborted") || result.getStatus().equalsIgnoreCase("error")){
						error++;
						executed++;
					}
					api.setOperations(operations);
					apis.add(api);
				}
				
				run.setApis(apis);
				run.setApiOperations(operations);
				run.setTotalTests(total);
				run.setPassedTests(passed);
				run.setErroredTests(error);
				run.setExecutedTests(executed);
			} catch (SQLException e) {
				
				logger.error("ERROR Occurred while fetching details of API Learning Run [{}]", run.getId(), e);
				throw new DatabaseException("Could not fetch API learning status due to an unexpected error. Please contact Tenjin Support.");
			} finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}
	}
	/*Added by Ashiki for TENJINCG-1213 ends*/
/*Added by Gangadhar Badagi for rerun a test starts*/
public TestRun hydrateRunForProject(int runId,int projectId) throws DatabaseException{
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if(conn == null){
			logger.error("DB Connection is null");
			throw new DatabaseException("Invalid or no Database Connection");
		}
		
		TestRun run = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		logger.info("Getting Basic Run Information for ID [{}]", runId);
		try {
			pst = conn.prepareStatement("SELECT A.*, B.RUN_STATUS as vRun_Status FROM MASTESTRUNS A, v_runresults B WHERE A.RUN_ID=B.RUN_ID AND A.RUN_ID=? AND A.RUN_PRJ_ID=?");
			
			pst.setInt(1, runId);
			pst.setInt(2, projectId);
			rs = pst.executeQuery();
			
			while(rs.next()){
				run = new TestRun();
				run.setId(rs.getInt("RUN_ID"));
				run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
				run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
				run.setUser(rs.getString("RUN_USER"));
				run.setMachine_ip(rs.getString("RUN_MACHINE_IP"));
				run.setBrowser_type(rs.getString("RUN_BROWSER_TYPE"));
				run.setProjectName(rs.getString("RUN_PRJ_NAME"));
				run.setDomainName(rs.getString("RUN_DOMAIN_NAME"));
				run.setTargetPort(rs.getInt("RUN_CL_PORT"));
				run.setTaskType(rs.getString("RUN_TASK_TYPE"));
				run.setProjectId(rs.getInt("RUN_PRJ_ID"));
				run.setAppId(rs.getInt("RUN_APP_ID"));
				run.setCompletionStatus(rs.getString("RUN_STATUS"));
				run.setStatus(rs.getString("vRun_Status"));
				int tsId = rs.getInt("RUN_TS_REC_ID");
				run.setTestSetRecordId(tsId);
			}
		} catch (SQLException e) {
			logger.error("ERROR Getting basic information of run [{}] from Master Table", runId, e);
			
			DatabaseHelper.close(conn);
			throw new DatabaseException("Could not fetch Run Information. Please contact Tenjin Support.");
		} finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			
		}
		
		if(run == null){
			logger.error("Run for ID [{}] is null", runId);
			throw new DatabaseException("Run ID [" + runId + "] is invalid.");
		}
		try{
		if(Utilities.trim(run.getTaskType()).equalsIgnoreCase(BridgeProcess.LEARN)){
			this.hydrateRunDataForLearning(conn, run);
		}else if(Utilities.trim(run.getTaskType()).equalsIgnoreCase(BridgeProcess.EXECUTE)){
			this.hydrateRunDataForExecution(conn, run);
		}else if(Utilities.trim(run.getTaskType()).equalsIgnoreCase(BridgeProcess.EXTRACT)){
			this.hydrateRunDataForExtraction(conn, run);
		}/******************************
		 * Added by Sriram for TENJINCG-169 (Schedule API Learning)
		 */
		else if(Utilities.trim(run.getTaskType()).equalsIgnoreCase(BridgeProcess.APILEARN)){
			this.hydrateRunDataForApiLearning(conn, run);
		}
		/******************************
		 * Added by Sriram for TENJINCG-169 (Schedule API Learning) ends
		 */
		
		}finally{
		DatabaseHelper.close(conn);
		}
		return run;
	}
/*Added by Gangadhar Badagi for rerun a test ends*/
	
	
	private void hydrateRunDataForExtraction(Connection conn, TestRun run) throws DatabaseException{
		if(conn == null){
			logger.error("DB Connection is null");
			throw new DatabaseException("Invalid or no Database Connection");
		}
		
		PreparedStatement pst = null;
		PreparedStatement dPst = null;
		ResultSet rs = null;
		ResultSet dRs = null;
		
		try {
			pst = conn.prepareStatement("SELECT A.*, B.MODULE_NAME,B.GROUP_NAME,B.MODULE_MENU_CONTAINER FROM EXTR_AUDIT_TRAIL A, MASMODULE B WHERE B.APP_ID=A.EXTR_APP_ID AND B.MODULE_CODE=A.EXTR_FUNC_CODE AND A.EXTR_RUN_ID=? ORDER BY A.EXTR_ID");
			pst.setInt(1, run.getId());
			
			rs = pst.executeQuery();
			List<Module> functions = new ArrayList<Module>();
			Module module = null;
			ExtractorResultBean result = null;
			List<ExtractionRecord> records = null;
			int total = 0;
			int executed = 0;
			int passed = 0;
			int error = 0;
			while(rs.next()){
				module = new Module();
				result = new ExtractorResultBean();
				module.setAutUserType(rs.getString("EXTR_AUT_USER_TYPE"));
				result.setStartTime(rs.getTimestamp("EXTR_START_TIME"));
				result.setEndTime(rs.getTimestamp("EXTR_END_TIME"));
				result.setStatus(rs.getString("EXTR_STATUS"));
				result.setMessage(rs.getString("EXTR_MESSAGE"));
				result.setId(rs.getInt("EXTR_ID"));
//				/module.setLastSuccessfulExtractionStatus(result);
				module.setExtractorInputFile(rs.getString("EXTR_INP_FILE_PATH"));
				module.setCode(rs.getString("EXTR_FUNC_CODE"));
				module.setName(rs.getString("MODULE_NAME"));
				module.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
				module.setGroup(rs.getString("GROUP_NAME"));
				total++;
				
				if(result.getStatus().equalsIgnoreCase(BridgeProcess.COMPLETE)){
					passed++;
					executed++;
				}else if(result.getStatus().equalsIgnoreCase("aborted") || result.getStatus().equalsIgnoreCase("error")){
					error++;
					executed++;
				}
				
				dPst = conn.prepareStatement("SELECT * FROM EXTR_AUDIT_TRAIL_DETAIL WHERE EXTR_RUN_ID=? AND EXTR_FUNC_CODE=? ORDER BY EXTR_SS_ROW_NUM");
				dPst.setInt(1, run.getId());
				dPst.setString(2, module.getCode());
				dRs = dPst.executeQuery();
				records = new ArrayList<ExtractionRecord>();
				while(dRs.next()){
					ExtractionRecord record = new ExtractionRecord();
					record.setTdUid(dRs.getString("ExTR_TDUID"));
					record.setTdGid(dRs.getString("EXTR_TDGID"));
					record.setResult(dRs.getString("EXTR_RESULT"));
					record.setMessage(dRs.getString("EXTR_MESSAGE"));
					Map<String, String> qMap = new HashMap<String, String>();
					String q = dRs.getString("EXTR_QUERY_FIELDS");
					if(!Utilities.trim(q).equalsIgnoreCase("")){
						String[] qSplit = q.split(";;;");
						for(String s:qSplit){
							String[] sSplit = s.split("\\[");
							try {
								String val = sSplit[1].replace("]", "");
								qMap.put(sSplit[0], val);
							} catch (ArrayIndexOutOfBoundsException e) {
								
								logger.warn(e.getMessage(),e);
								logger.warn("Could not resolve query fields [{}]", s);
							}
						}
					}
					record.setQueryFields(qMap);
					Clob clob = dRs.getClob("EXTR_DATA");
					try {
						record.setData(clob.getSubString(1, (int) clob.length()));
					} catch (NullPointerException e) {
						
						record.setData("");
					}
					record.setSpreadsheetRowNum(dRs.getInt("EXTR_SS_ROW_NUM"));
					records.add(record);
				}
				
			
				
				result.setRecords(records);
				module.setLastSuccessfulExtractionStatus(result);
				functions.add(module);
			}
			
			run.setFunctions(functions);
			run.setTotalTests(total);
			run.setPassedTests(passed);
			run.setErroredTests(error);
			run.setExecutedTests(executed);
			
			
		} catch (SQLException e) {
			
			logger.error("ERROR Occurred while fetching details of Extraction Run [{}]", run.getId(), e);
			throw new DatabaseException("Could not fetch extraction status due to an unexpected error. Please contact Tenjin Support.");
		} finally{
			
			DatabaseHelper.close(dRs);
			DatabaseHelper.close(dPst);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
		
		
	}
	
	private void hydrateRunDataForLearning(Connection conn, TestRun run) throws DatabaseException{
		if(conn == null){
			logger.error("DB Connection is null");
			throw new DatabaseException("Invalid or no Database Connection");
		}
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			pst = conn.prepareStatement("SELECT A.*, B.MODULE_NAME,B.GROUP_NAME,B.MODULE_MENU_CONTAINER FROM LRNR_AUDIT_TRAIL A, MASMODULE B WHERE B.APP_ID=A.LRNR_APP_ID AND B.MODULE_CODE=A.LRNR_FUNC_CODE AND A.LRNR_RUN_ID=? ORDER BY A.LRNR_ID");
			pst.setInt(1, run.getId());
			
			rs = pst.executeQuery();
			List<Module> functions = new ArrayList<Module>();
			Module module = null;
			LearnerResultBean result = null;
			int total = 0;
			int executed = 0;
			int passed = 0;
			int error = 0;
			while(rs.next()){
				module = new Module();
				result = new LearnerResultBean();
				module.setAutUserType(rs.getString("LRNR_AUT_USER_TYPE"));
				result.setStartTimestamp(rs.getTimestamp("LRNR_START_TIME"));
				result.setEndTimestamp(rs.getTimestamp("LRNR_END_TIME"));
				result.setLearnStatus(rs.getString("LRNR_STATUS"));
				result.setMessage(rs.getString("LRNR_MESSAGE"));
				result.setId(rs.getInt("LRNR_ID"));
				module.setLastSuccessfulLearningResult(result);
				module.setCode(rs.getString("LRNR_FUNC_CODE"));
				module.setName(rs.getString("MODULE_NAME"));
				module.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
				module.setGroup(rs.getString("GROUP_NAME"));
				functions.add(module);
				total++;
				
				if(result.getLearnStatus().equalsIgnoreCase("complete")){
					passed++;
					executed++;
				}else if(result.getLearnStatus().equalsIgnoreCase("aborted") || result.getLearnStatus().equalsIgnoreCase("error")){
					error++;
					executed++;
				}
			}
			
			
			run.setFunctions(functions);
			run.setTotalTests(total);
			run.setPassedTests(passed);
			run.setErroredTests(error);
			run.setExecutedTests(executed);
		} catch (SQLException e) {
			
			logger.error("ERROR Occurred while fetching details of Learning Run [{}]", run.getId(), e);
			throw new DatabaseException("Could not fetch learning status due to an unexpected error. Please contact Tenjin Support.");
		} finally{
		
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
		
		
	}
	
	/******************************
	 * Added by Sriram for TENJINCG-169 (Schedule API Learning)
	 */
	private void hydrateRunDataForApiLearning(Connection conn, TestRun run) throws DatabaseException{
		if(conn == null){
			logger.error("DB Connection is null");
			throw new DatabaseException("Invalid or no Database Connection");
		}
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			pst = conn.prepareStatement("SELECT A.*, B.API_CODE,B.API_NAME,B.API_TYPE,B.API_URL FROM LRNR_AUDIT_TRAIL_API A, TJN_AUT_APIS B WHERE B.APP_ID=A.LRNR_APP_ID AND B.API_CODE=A.LRNR_API_CODE AND A.LRNR_RUN_ID=? ORDER BY A.LRNR_ID");
			pst.setInt(1, run.getId());
			
			rs = pst.executeQuery();
			List<ApiOperation> operations = new ArrayList<ApiOperation>();
			Api api = new Api();
			ApiLearnerResultBean result = null;
			int total = 0;
			int executed = 0;
			int passed = 0;
			int error = 0;
			while(rs.next()){
				api = new Api();
				result = new ApiLearnerResultBean();
				api.setCode(rs.getString("API_CODE"));
				api.setName(rs.getString("API_NAME"));
				api.setType(rs.getString("API_TYPE"));
				api.setUrl(rs.getString("API_URL"));
				result.setStartTimestamp(rs.getTimestamp("LRNR_START_TIME"));
				result.setEndTimestamp(rs.getTimestamp("LRNR_END_TIME"));
				result.setStatus(rs.getString("LRNR_STATUS"));
				result.setMessage(rs.getString("LRNR_MESSAGE"));
				result.setId(rs.getInt("LRNR_ID"));
				ApiOperation op = new ApiOperation();
				op.setName(rs.getString("LRNR_OPERATION"));
				op.setLastSuccessfulLearningResult(result);
				
				operations.add(op);
				total++;
				
				if(result.getStatus().equalsIgnoreCase("complete")){
					passed++;
					executed++;
				}else if(result.getStatus().equalsIgnoreCase("aborted") || result.getStatus().equalsIgnoreCase("error")){
					error++;
					executed++;
				}
			}
			
		
	
			run.setApi(api);
			run.setApiOperations(operations);
			run.setTotalTests(total);
			run.setPassedTests(passed);
			run.setErroredTests(error);
			run.setExecutedTests(executed);
		} catch (SQLException e) {
			
			logger.error("ERROR Occurred while fetching details of API Learning Run [{}]", run.getId(), e);
			throw new DatabaseException("Could not fetch API learning status due to an unexpected error. Please contact Tenjin Support.");
		} finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
		
		
	}
	/******************************
	 * Added by Sriram for TENJINCG-169 (Schedule API Learning) ends
	 */
	
	private void hydrateRunDataForExecution(Connection conn, TestRun run) throws DatabaseException{
		if(conn == null){
			logger.error("DB Connection is null");
			throw new DatabaseException("Invalid or no Database Connection");
		}
	}
	
	public void updateRunInfo(Connection conn, TestRun run, Map<Integer, List<String>> tdUidMap) throws DatabaseException{
		if(conn == null){
			logger.error("DB Connection is null");
			throw new DatabaseException("Invalid or no Database Connection");
		}
		
		logger.debug("Updating Run information");
		this.updateRunInfo(conn, run.getId(), run.getStartTimeStamp().getTime(), run.getBrowser_type(), run.getMachine_ip(), run.getTargetPort(), run.getUser());
		
		logger.debug("Clearing existing transaction information");
		this.clearTransactionData(conn, run.getId());
		
		logger.debug("Inserting new transaction information");
		try{
			for(TestCase t:run.getTestSet().getTests()){
				for(TestStep s:t.getTcSteps()){
					List<String> tduids = tdUidMap.get(s.getRecordId());
					for(int i=1;i<=s.getTotalTransactions();i++){
						String tduid = tduids.get(i-1);
						this.insertTestStepIterationStatus(conn, "N", run.getId(), s.getRecordId(), i, tduid);
					}
				}
			}
		}catch(Exception e){
			logger.error("ERROR updating status of test cases to Not Started",e);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				
				logger.error("Error ", e1);
			}
			
			throw new DatabaseException("Could not create new run due to an internal error");
		}
	}
	
	private void clearTransactionData(Connection conn, int runId) throws DatabaseException{
		
		if(conn == null){
			logger.error("DB Connection is null");
			throw new DatabaseException("Invalid or no Database Connection");
		}
		
		PreparedStatement pst = null;
		
		try {
			pst = conn.prepareStatement("Delete from RunResult_ts_txn where run_id=?");
			pst.setInt(1, runId);
			pst.execute();
		} catch (SQLException e) {
			
			logger.error("ERROR clearing run transaction info", e);
			throw new DatabaseException("Could not clear run transaction information");
		} finally{
			
		DatabaseHelper.close(pst);
		}
	}
	
	public void updateRunInfo(Connection conn, int runId, long startTime, String browser, String targetIp, int targetPort, String user) throws DatabaseException{
		if(conn == null){
			logger.error("DB Connection is null");
			throw new DatabaseException("Invalid or no Database Connection");
		}
		
		PreparedStatement pst = null;
		
		try{
			pst = conn.prepareStatement("UPDATE MASTESTRUNS SET "
					+ "RUN_START_TIME=?, "
					+ "RUN_BROWSER_TYPE=?, "
					+ "RUN_MACHINE_IP=?, "
					+ "RUN_CL_PORT=?, "
					+ "RUN_USER=? WHERE "
					+ "RUN_ID=?");
			
			pst.setTimestamp(1, new Timestamp(startTime));
			pst.setString(2, browser);
			pst.setString(3, targetIp);
			pst.setInt(4, targetPort);
			pst.setString(5, user);
			pst.setInt(6, runId);
			
			pst.executeUpdate();
			
		} catch(SQLException e){
			logger.error("ERROR updating run information", e);
			throw new DatabaseException("Could not update run information. Please contact Tenjin Support.");
		} finally{
			
			DatabaseHelper.close(pst);
		}
	}
	/******************************
	 * Added by Sriram for Req#TJN_23_18 ends
	 */
	


 /******** added by Gangadhar Badagi to get runs for test case starts ***************/
public List<TestRun> getAllRunsForTestCase(int testCaseRecordId) throws DatabaseException{
           
            Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
            PreparedStatement pst =null;
    		ResultSet rs=null;
            ResultsHelper helper = new ResultsHelper();
            List<TestRun> runs = new ArrayList<TestRun>();
            try {
                  pst = conn.prepareStatement("SELECT RUN_ID,RUN_PRJ_ID FROM MASTESTRUNS WHERE RUN_ID IN (SELECT RUN_ID FROM V_RUNRESULTS_TC WHERE TC_REC_ID = ?) ORDER BY RUN_ID");
                  pst.setInt(1, testCaseRecordId);
                 
                  rs = pst.executeQuery();
                 
                  while(rs.next()){
                        int runId = rs.getInt("RUN_ID");
                        int projectId = rs.getInt("RUN_PRJ_ID");
                        /*changed by manish for bug T25IT-105 on 18-Aug-2017 starts*/
                        TestRun run = helper.hydrateRun(runId, projectId);
                        /*changed by manish for bug T25IT-105 on 18-Aug-2017 ends*/
                        runs.add(run);
                       
                  }
                
            } catch (SQLException e) {
                  logger.error("ERROR while fetching runs");
                  throw new DatabaseException("Could not fetch runs for test case "+ testCaseRecordId + " due to an internal error. Please contact Tenjin Support");
            }finally{
            	DatabaseHelper.close(rs);
    			DatabaseHelper.close(pst);
    			DatabaseHelper.close(conn);
            }
           
            return runs;
      }
	
/******** added by Gangadhar Badagi to get runs for test case ends ***************/

/** 
 * **********Method Added by Leelaprasad for the Requirement TENJINCG-26 starts
 * @throws DatabaseException **
 * 
 */


/** 
 * **********Method Added by Leelaprasad for the Requirement TENJINCG-26 ends
 * @throws DatabaseException **
 * 
 */

/*Added by Sriram for TENJINCG-187 */
	public void informAbort(int runId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		
		try {
			pst = conn.prepareStatement("UPDATE MASTESTRUNS SET RUN_STATUS=? WHERE RUN_ID=?");
			pst.setString(1, BridgeProcess.ABORTING);
			pst.setInt(2, runId);
			
			pst.executeUpdate();
		} catch (SQLException e) {
			
			logger.error("Error setting run status to aborted", e);
			throw new DatabaseException("Could not abort run. Please contact Tenjin Support.");
		} finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
	}
	/*Added by Sriram for TENJINCG-187 ends*/
	/*Added by Lokanath for TENJINCG-1193 starts*/
	public void updateLearnAbort(int runId, String userName) throws DatabaseException{
		Connection conn =null;
		
		PreparedStatement pst = null;
		
		try {
			 conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			pst = conn.prepareStatement("UPDATE MASTESTRUNS SET RUN_STATUS=? WHERE RUN_ID=?");
			pst.setString(1, BridgeProcess.ABORTED);
			pst.setInt(2, runId);
			pst.executeUpdate();
		} catch (SQLException |DatabaseException e) {
			
			logger.error("Error setting run status to aborted", e);
			throw new DatabaseException("Could not abort run. Please contact Tenjin Support.");
		} 
		//Updating Learner Audit Trail
		try{
			pst = conn.prepareStatement("UPDATE LRNR_AUDIT_TRAIL SET LRNR_STATUS =?,LRNR_END_TIME=?,LRNR_MESSAGE=? ,LRNR_START_TIME =? WHERE LRNR_RUN_ID =?");
			pst.setInt(5, runId);
			pst.setTimestamp(4, new Timestamp(new Date().getTime()));
			pst.setString(1, BridgeProcess.ABORTED);
			pst.setString(3, "Aborted by "+userName);
			pst.setTimestamp(2, new Timestamp(new Date().getTime()));
			pst.execute();
			}catch(SQLException e){
			}
	        finally{
				DatabaseHelper.close(pst);
				/*Added by paneendra for TENJINCG-1267 starts*/
				DatabaseHelper.close(conn);
				/*Added by paneendra for TENJINCG-1267 ends*/
			}
	}
	/*Added by Lokanath for TENJINCG-1193 ends*/
	//Fix for T25IT-197
	public List<TestRun> getAllRunsForTestStep(int testStepRecordId) throws DatabaseException{
        
        Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
        PreparedStatement pst =null;
		ResultSet rs=null;
        ResultsHelper helper = new ResultsHelper();
        List<TestRun> runs = new ArrayList<TestRun>();
        try {
              pst = conn.prepareStatement("SELECT RUN_ID,RUN_PRJ_ID FROM MASTESTRUNS WHERE RUN_ID IN (SELECT RUN_ID FROM V_RUNRESULTS_Ts WHERE TSTEP_REC_ID = ?) ORDER BY RUN_ID DESC");
              pst.setInt(1, testStepRecordId);
             
              rs = pst.executeQuery();
             
              while(rs.next()){
                    int runId = rs.getInt("RUN_ID");
                    int projectId = rs.getInt("RUN_PRJ_ID");
                   
                    TestRun run = helper.hydrateRun(conn, runId, projectId);
                    runs.add(run);
                   
              }
            
        } catch (SQLException e) {
              logger.error("ERROR while fetching runs");
              throw new DatabaseException("Could not fetch runs for test step "+ testStepRecordId + " due to an internal error. Please contact Tenjin Support");
        }finally{
        	DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
        }
       
        return runs;
  }
	//Fix for T25IT-197 ends
	
	/*Changed by Leelaprasad for the requirement TENJINCG-815 starts*/
	public void abortAllEndLessRuns() throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs=null;
		ResultSet rs1=null;
		ResultSet rs2=null;
		try {/*Changed by Ashiki for V2.8-87 starts*/
			pst = conn.prepareStatement("SELECT RUN_ID, RUN_TASK_TYPE FROM MASTESTRUNS WHERE RUN_STATUS=? ");
			/*Changed by Ashiki for V2.8-87 ends*/
			pst.setString(1, "Executing");
			 rs = pst.executeQuery();
			while (rs.next()) {
				int runId = rs.getInt("RUN_ID");
				/*Added by Ashiki for V2.8-87 starts*/
				String runTaskType = rs.getString("RUN_TASK_TYPE");
				/*Added by Ashiki for V2.8-87 starts*/
				try {
					pst = conn
							.prepareStatement("UPDATE MASTESTRUNS SET RUN_STATUS=? ,RUN_END_TIME=? WHERE RUN_ID=?");
					pst.setString(1, "Aborted");
					pst.setTimestamp(2, new Timestamp(new Date().getTime()));
					pst.setInt(3, runId);

					pst.executeUpdate();
				} catch (SQLException se) {
					logger.error("Unable to abort end less runs");

				} finally {
					DatabaseHelper.close(pst);
				}/*Added by Ashiki for V2.8-87 starts*/
				if(runTaskType.equalsIgnoreCase("Execute")) {
					/*Added by Ashiki for V2.8-87 ends*/
					try {
						logger.debug("Updating abort status for individual transactions");
						/*Changed by Ashiki for TJN252-1 starts*/
						/*pst = conn.prepareStatement("UPDATE RUNRESULT_TS_TXN SET TSTEP_RESULT=?, TSTEP_MESSAGE=? WHERE RUN_ID=?");*/
						pst = conn.prepareStatement("UPDATE RUNRESULT_TS_TXN SET TSTEP_RESULT=?, TSTEP_MESSAGE=? WHERE RUN_ID=? AND TSTEP_RESULT NOT IN('E','F','S')");
						/*Changed by Ashiki for TJN252-1 ends*/
						pst.setString(1, "E");
						pst.setString(2, "Execution aborted by System");
						pst.setInt(3, runId);

						pst.executeUpdate();
					} catch (SQLException se) {
						logger.error("Unable to abort end less runs");
					} finally {
						DatabaseHelper.close(pst);
					}

					logger.debug("Updating abort status for the run");
				}/*Added by Ashiki for V2.8-87 starts*/
				else if(runTaskType.equalsIgnoreCase("Learn")){
					try {
					logger.debug("Updating abort status for individual transactions");
					pst = conn.prepareStatement("UPDATE LRNR_AUDIT_TRAIL SET LRNR_STATUS=?,LRNR_MESSAGE=? WHERE LRNR_RUN_ID=?");
					pst.setString(1, "Aborted");
					pst.setString(2, "Learning aborted by System");
					pst.setInt(3, runId);
					pst.executeUpdate();
					} catch (SQLException se) {
						logger.error("Unable to abort end less runs");
					} finally {
						DatabaseHelper.close(pst);
					}

					logger.debug("Updating abort status for the run");
				}else if(runTaskType.equalsIgnoreCase("Extract")) {
					

					try {
					logger.debug("Updating abort status for individual transactions");
					pst = conn.prepareStatement("UPDATE EXTR_AUDIT_TRAIL SET EXTR_STATUS= ?,EXTR_MESSAGE=? WHERE EXTR_RUN_ID=?");
					pst.setString(1, "Aborted");
					pst.setString(2, "Extraction aborted by System");
					pst.setInt(3, runId);
					pst.executeUpdate();
					} catch (SQLException se) {
						logger.error("Unable to abort end less runs");
					} finally {
						DatabaseHelper.close(pst);
					}

					logger.debug("Updating abort status for the run");
				
				}
				/*Added by Ashiki for V2.8-87 ends*/
			}

		} catch (SQLException se) {
			logger.error("Unable to abort end less runs");

		} 
		/*added by shruthi for TJN27-108 starts*/
		try{
			pst1 = conn.prepareStatement("SELECT LRNR_ID,LRNR_RUN_ID FROM LRNR_AUDIT_TRAIL WHERE LRNR_STATUS=?");

			pst1.setString(1,"QUEUED");
			/*pst1.setString(2,"LEARNING");*/
			 rs1 = pst1.executeQuery();
			while (rs1.next()) {
				int lnrId = rs1.getInt("LRNR_ID");
				int lrnrrunID= rs1.getInt("LRNR_RUN_ID");
				pst = conn
						.prepareStatement("SELECT * FROM TJN_SCHEDULER WHERE RUN_ID=?");
				pst.setInt(1, lrnrrunID);
				 rs2 = pst.executeQuery();
				Boolean abritFlag = true;
				while (rs2.next()){
					abritFlag = false;
					break;
				}
				if(abritFlag){
				try {
					pst1 = conn
							.prepareStatement("UPDATE LRNR_AUDIT_TRAIL SET LRNR_STATUS=? ,LRNR_END_TIME=? WHERE LRNR_ID=?");
					pst1.setString(1, "ABORTED");
					pst1.setTimestamp(2, new Timestamp(new Date().getTime()));
					pst1.setInt(3, lnrId);

					pst1.executeUpdate();
				} catch (SQLException se) {
					logger.error("Unable to abort end less runs");

				} }
			}}
		catch (SQLException se) {
			logger.error("Unable to abort end less learning funcns");

		} 
		finally {
			DatabaseHelper.close(rs2);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(conn);
		}
		/*added by shruthi for TJN27-108 ends*/
		}

	
	/*Changed by Leelaprasad for the requirement TENJINCG-815 ends*/
}
