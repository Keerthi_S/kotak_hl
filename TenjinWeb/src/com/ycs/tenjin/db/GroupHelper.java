/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GroupHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 19-02-2018		   Pushpalatha			   Newly Added 
 * 03-07-2018           Padmavathi              T251IT-172(for avoiding multiple validation)
 * 30-07-2018			Preeti					Closed connections
 */

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.aut.Group;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class GroupHelper {
	private static final Logger logger = LoggerFactory.getLogger(ModulesHelper.class);	
	public List<Group> hydrateAllGroups() throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<Group> grpList = null;
		try {
			pst = conn.prepareStatement("SELECT * FROM TJN_AUT_GROUPS");
			rs = pst.executeQuery();
			grpList = new ArrayList<Group>();
			while (rs.next()) {
				Group grp = new Group();
				grp.setAut(rs.getInt("APP_ID"));
				grp.setAppName(rs.getString("APP_NAME"));
				grp.setGroupName(rs.getString("GROUP_NAME"));
				grp.setGroupDesc(rs.getString("GROUP_DESC"));
				if(!grp.getGroupName().equalsIgnoreCase("Ungrouped")){
					grpList.add(grp);
				}
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch Groups under Applications Under Test", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			/*Added by paneendra for TENJINCG-1267 starts*/
			DatabaseHelper.close(conn);
			/*Added by paneendra for TENJINCG-1267 ends*/
		}

		return grpList;
	}
	/*modified by shruthi for sql injection starts*/
	public void clearGroup(String[] rec) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		try{
			for( int i = 0 ; i < rec.length; i++ )
			{
				try{
					String autId = rec[i].substring(0, rec[i].indexOf("|"));
					String groupName = rec[i].substring(rec[i].indexOf("|")+1, rec[i].length());
					
					pst = conn.prepareStatement("DELETE FROM tjn_aut_groups WHERE app_id =? and group_name =?");
					pst.setString(1, autId);
					pst.setString(2, groupName);
					pst.executeUpdate();
					pst.close();
					pst = conn.prepareStatement("UPDATE MASMODULE SET GROUP_NAME='Ungrouped' where GROUP_Name=?");
					
					pst.setString(1, groupName);
					pst.executeUpdate();
					pst.close();
					pst = conn.prepareStatement("UPDATE TJN_AUT_APIS SET GROUP_NAME='Ungrouped' where GROUP_NAME=?");
					
					pst.setString(1, groupName);
					pst.executeUpdate();
					pst.close();
				}catch(Exception e){
					throw new DatabaseException("Could not delete group due to an internal error",e);
				}finally{
					DatabaseHelper.close(pst);
				}
			}


		}catch(Exception e){
			throw new DatabaseException("Could not delete group due to an internal error",e);
		}finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	/*modified by shruthi for sql injection ends*/
	public Group persistGroup(Group group) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		try{
			
			pst = conn.prepareStatement("INSERT INTO TJN_AUT_GROUPS (APP_ID,APP_NAME,GROUP_NAME,GROUP_DESC) VALUES (?,?,?,?)");

			pst.setInt(1, group.getAut());
			pst.setString(2, group.getAppName());
			pst.setString(3, group.getGroupName());
			pst.setString(4, group.getGroupDesc());

			pst.execute();

		}catch(Exception e){
			logger.error("Could not insert AUT Group record",e);
			throw new DatabaseException("Could not insert record",e);
		}finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return group;
	}

	public Group hydrateGroup(int appId, String groupName) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		Group grp = null;
		if(conn != null){
			
			PreparedStatement pst = null;
			ResultSet rs=null;

			try{
				
				pst = conn.prepareStatement("SELECT * FROM TJN_AUT_GROUPS  WHERE APP_ID =? AND GROUP_NAME =?");
				pst.setInt(1, appId);
				pst.setString(2, groupName);
				rs = pst.executeQuery();
				while(rs.next()){
					grp = new Group();
					grp.setAut(appId);
					grp.setAppName(rs.getString("APP_NAME"));					
					grp.setGroupName(rs.getString("GROUP_NAME"));
					grp.setGroupDesc(rs.getString("GROUP_DESC"));
				}

				return grp;
			}
			catch(Exception e){
				logger.error("Could not hydrate group " + groupName + " for application " + appId,e);
				throw new DatabaseException("Could not hydrate group " + groupName + " for application " + appId,e);
			}
			finally{

				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}
		else{
			throw new DatabaseException("Invalid database connection");
		}
	}




	public ArrayList<ModuleBean> hydrateModules(int application,String group_name) throws DatabaseException, SQLException{
		ArrayList<ModuleBean> oModules = new ArrayList<ModuleBean>();


		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		/*Modified by Priyanka for VAPT Helper fix starts*/
		PreparedStatement pst = null;
		/*Modified by Priyanka for VAPT Helper fix ends*/
		ResultSet rs=null;
		long startMillis = System.currentTimeMillis();
		if(conn != null){
			try{
				if(group_name.equalsIgnoreCase(""))
					group_name="NONE";
				/*Modified by Priyanka for VAPT Helper fix starts*/

				pst = conn.prepareStatement("Select MasModule.Module_Code, MasModule.Module_Name,MasModule.Module_Menu_Container, MasModule.ws_url, MasModule.ws_operation,MasModule.END_TimESTAMP From MasModule WHERE MasModule.App_Id =? and MasModule.group_name=? ORDER BY MasModule.Module_Code");
				pst.setInt(1, application);
				pst.setString(2, group_name);
				rs = pst.executeQuery();
				/*Modified by Priyanka for VAPT Helper fix ends*/
				while(rs.next()){
					ModuleBean mBean = new ModuleBean();
					mBean.setAut(application);
					mBean.setModuleCode(rs.getString("Module_Code"));
					mBean.setModuleName(rs.getString("Module_Name"));
					mBean.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
					mBean.setWSURL(rs.getString("WS_URL"));
					mBean.setWSOp(rs.getString("WS_OPERATION"));

					if(rs.getTimestamp("END_TIMESTAMP") != null){
						String endTs = new SimpleDateFormat().format(rs.getTimestamp("END_TIMESTAMP"));
						mBean.setLastLearntTimestamp(endTs);
					}else{
						mBean.setLastLearntTimestamp(null);
					}

					oModules.add(mBean);
				}
				long endMillis = System.currentTimeMillis();
				logger.debug(oModules.size() + " functions fetched in " + Utilities.calculateElapsedTime(startMillis, endMillis));
				return oModules;
			}
			catch(Exception e){
				logger.error("Could not hydrate modules for application " + application,e);
				long endMillis = System.currentTimeMillis();
				logger.debug("hydrateModules() completed in " + Utilities.calculateElapsedTime(startMillis, endMillis));
				throw new DatabaseException("Could not hydrate modules for application " + application,e);
			}
			finally{

				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}
		else{

			long endMillis = System.currentTimeMillis();
			logger.debug("hydrateModules() completed in " + Utilities.calculateElapsedTime(startMillis, endMillis));
			throw new DatabaseException("Invalid database connection");
		}
	}

	public void unMapFunction(int appId,String groupName, String[] rec) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		try{

			for( int i = 0 ; i < rec.length; i++ )
			{
				try{
					pst = conn.prepareStatement("UPDATE masmodule SET group_name = ? where app_id = ? and module_code = ?" );
					pst.setString(1, groupName);
					pst.setInt(2, appId);
					pst.setString(3, rec[i]);
					pst.executeUpdate();
					pst.close();
				}catch(Exception e){
					throw new DatabaseException("Could not update group due to an internal error",e);
				}finally{
					DatabaseHelper.close(pst);
				}
			}

		}catch(Exception e){
			throw new DatabaseException("Could not update group due to an internal error",e);
		}finally{

			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	public void mapFunction(int appId,String groupName, String[] rec) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		try{

			for( int i = 0 ; i < rec.length; i++ )
			{
				try{
					pst = conn.prepareStatement("UPDATE masmodule SET group_name = ? where app_id = ? and module_code = ?" );
					pst.setString(1, groupName);
					pst.setInt(2, appId);
					pst.setString(3, rec[i]);
					pst.executeUpdate();
					pst.close();
				}catch(Exception e){
					throw new DatabaseException("Could not update group due to an internal error",e);
				}finally{
					DatabaseHelper.close(pst);
				}
			}

		}catch(Exception e){
			throw new DatabaseException("Could not update group due to an internal error",e);
		}finally{

			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}



	public ArrayList<Api> hydrateApi(int application,String group_name) throws DatabaseException, SQLException{
		ArrayList<Api> oApi = new ArrayList<Api>();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		/*Modified by Priyanka for VAPT Helper fix starts*/
		PreparedStatement pst = null;
		/*Modified by Priyanka for VAPT Helper fix ends*/
		ResultSet rs=null;
		long startMillis = System.currentTimeMillis();
		if(conn != null){

			try{
				if(group_name.equalsIgnoreCase(""))
					group_name="NONE";
				/*Modified by Priyanka for VAPT Helper fix starts*/

				pst = conn.prepareStatement("Select * From TJN_AUT_APIS WHERE TJN_AUT_APIS.App_Id =? and TJN_AUT_APIS.group_name=? ORDER BY TJN_AUT_APIS.API_Code");
				pst.setInt(1, application);
				pst.setString(2, group_name);
				rs=pst.executeQuery();
				/*Modified by Priyanka for VAPT Helper fix ends*/
				while(rs.next()){
					Api api=new Api();
					api.setApplicationId(application);
					api.setCode(rs.getString("API_CODE"));
					api.setName(rs.getString("API_NAME"));
					api.setType(rs.getString("API_TYPE"));
					api.setUrl(rs.getString("API_URL"));
					api.setGroup(rs.getString("GROUP_NAME"));

					oApi.add(api);
				}


				return oApi;
			}
			catch(Exception e){
				logger.error("Could not hydrate modules for application " + application,e);
				long endMillis = System.currentTimeMillis();
				logger.debug("hydrateModules() completed in " + Utilities.calculateElapsedTime(startMillis, endMillis));
				throw new DatabaseException("Could not hydrate modules for application " + application,e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}
		else{

			long endMillis = System.currentTimeMillis();
			logger.debug("hydrateModules() completed in " + Utilities.calculateElapsedTime(startMillis, endMillis));
			throw new DatabaseException("Invalid database connection");
		}
	}

	public void unMapApi(int appId,String groupName, String[] rec) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		try{

			for( int i = 0 ; i < rec.length; i++ )
			{
				try{
					pst = conn.prepareStatement("UPDATE TJN_AUT_APIS SET group_name = ? where app_id = ? and api_code = ?" );
					pst.setString(1, groupName);
					pst.setInt(2, appId);
					pst.setString(3, rec[i]);
					pst.executeUpdate();
					pst.close();
				}catch(Exception e){
					throw new DatabaseException("Could not update group due to an internal error",e);
				}finally{

					DatabaseHelper.close(pst);
				}
			}

		}catch(Exception e){
			throw new DatabaseException("Could not update group due to an internal error",e);
		}finally{

			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	public void mapApi(int appId,String groupName, String[] rec) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		try{

			for( int i = 0 ; i < rec.length; i++ )
			{
				try{
					pst = conn.prepareStatement("UPDATE TJN_AUT_APIS SET group_name = ? where app_id = ? and api_code = ?" );
					pst.setString(1, groupName);
					pst.setInt(2, appId);
					pst.setString(3, rec[i]);
					pst.executeUpdate();
					pst.close();
				}catch(Exception e){
					throw new DatabaseException("Could not update group due to an internal error",e);
				}finally{
					DatabaseHelper.close(pst);
				}
			}

		}catch(Exception e){
			throw new DatabaseException("Could not update group due to an internal error",e);
		}finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	public void updateGroup(Group g) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		try{
			pst = conn.prepareStatement("UPDATE tjn_aut_groups SET group_desc = ? where app_id = ? and group_name = ?" );
			pst.setString(1, g.getGroupDesc());
			pst.setInt(2, g.getAut());
			pst.setString(3, g.getGroupName());
			pst.executeUpdate();
			pst.close();

		}catch(Exception e){
			throw new DatabaseException("Could not update group due to an internal error",e);
		}finally{

			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}


	public List<Module> hydrateModulesInGroup(int application, String groupName) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		PreparedStatement pst = null;
		PreparedStatement pst2 = null;
		ResultSet rs = null;
		ResultSet rs2 =null;

		List<Module> functions = new ArrayList<Module>();

		try {
			String query = null;
			if(groupName == null || Utilities.trim(groupName).equalsIgnoreCase("") || groupName.equalsIgnoreCase("All")){

				query = "SELECT MODULE_CODE, MODULE_NAME, MODULE_MENU_CONTAINER FROM MASMODULE WHERE APP_ID=? ORDER BY MODULE_CODE";

				pst = conn.prepareStatement(query);
				pst.setInt(1, application);
			}else{
				query = "SELECT MODULE_CODE, MODULE_NAME, MODULE_MENU_CONTAINER FROM MASMODULE WHERE APP_ID=? and group_name=? ORDER BY MODULE_CODE";

				pst = conn.prepareStatement(query);
				pst.setInt(1, application);
				pst.setString(2, groupName);
			}

			rs = pst.executeQuery();

			while(rs.next()){
				Module module = new Module();
				module.setCode(rs.getString("MODULE_CODE"));
				module.setName(rs.getString("MODULE_NAME"));
				module.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));

				module.setAut(application);

				pst2 = conn.prepareStatement("SELECT LRNR_END_TIME,LRNR_ID,LRNR_STATUS,LRNR_RUN_ID from LRNR_AUDIT_TRAIL WHERE LRNR_STATUS IN(?, ?) AND LRNR_FUNC_CODE=? AND LRNR_APP_ID=? ORDER BY LRNR_ID DESC");
				pst2.setString(1, "COMPLETE");
				pst2.setString(2, "IMPORTED");
				pst2.setString(3, module.getCode());
				pst2.setInt(4, application);

				rs2 = pst2.executeQuery();
				while(rs2.next()){
					LearnerResultBean result = new LearnerResultBean();
					result.setId(rs2.getInt("LRNR_ID"));
					result.setEndTimestamp(rs2.getTimestamp("LRNR_END_TIME"));
					result.setLearnStatus(rs2.getString("LRNR_STATUS"));
					module.setLastSuccessfulLearningResult(result);
					break;
				}
				functions.add(module);
			}

		} catch (SQLException e) {

			logger.error("ERROR fetching functions information", e);
			throw new DatabaseException("Could not fetch functions due to an internal error");
		} finally{

			DatabaseHelper.close(rs2);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);

		}
		return functions;
	}

}




