/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ResultsHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright &copy 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 10-Nov-16			Sriram					Fix for defect#816
 * 02-12-2016           Parveen                 REQ #TJN_243_04 Changing insert query
 * 12-01-2017			Manish					TENJINCG-8
 * 12-01-2017			Manish					TENJINCG-135
 * 15-05-2017			Sriram					TENJINCG-172
 * 19-06-2017			Roshni					TENJINCG-198
 * 03-07-2017			Roshni					TENJINCG-259
 * 27-Jul-2017			Sriram Sridharan		TENJINCG-312
 * 03-08-2017           Padmavathi              Tenjincg-331
 * 04-08-2017           Manish               	TENJINCG-330
 * 16-Aug-2017			Manish					T25IT-105
 * 21-11-2017			Preeti					TENJINCG-484
 * 17-01-2018			Preeti					TENJINCG-574
 * 30-01-2018			Preeti					TENJINCG-503
 * 02-02-2018			Preeti					TENJINCG-101
 * 03-02-2018			Preeti					TENJINCG-101
 * 20-04-2018			Preeti					TENJINCG-616
 * 18-05-2018			Padmavathi		        for TENJINCG-647 
 * 22-Dec-2017			Sahana					Mobility	
 * 30-07-2018			Preeti					Closed connections
 * 24-08-2018           Padmavathi              for TENJINCG-726
 * 29-08-2018           Padmavathi              to display testcases in order  in test selection page 
 * 30-08-2018			Preeti					TENJINCG-729
 * 28-09-2018			Ashiki					TENJINCG-746
 * 11-10-2018			Sriram					TENJINCG-871
 * 22-10-2018			Sriram					TENJINCG-870 and TENJINCG-876
 * 30-10-2018			Sriram					TENJINCG-886
 * 20-12-2018           Padmavathi              TENJINCG-911
 * 07-02-2019           Leelaprasad             TJN252-85
 * 25-02-2019			Ashiki					TENJINCG-985
 * 22-03-2019			Preeti					TENJINCG-1018
 * 08-03-2019			Sahana					pCloudy
 * 11-06-2019			Pushpa					TJN27-8
 * 13-06-2019			Pushpa					TJN27-13
 * 05-02-2020			Roshni					TENJINCG-1168
 * 13-03-2020			Sriram					TENJINCG-1196
 * 06-06-2020			Ashiki					Tenj210-76
 * 16-09-2020 			Pushpa					TENJINCG-1223
 * 16-09-2020           shruthi                 TENJINCG-1226
 * 25-09-2020           shruthi                 TENJINCG-1224
 * 28-09-2020			Ashiki					TENJINCG-1211
 * 18-11-2020			Pushpa					TENJINCG-1228
   10-12-2020           Shruthi                 TENJINCG-1238
 * 11-12-2020			Pushpa					TENJINCG-1221
 * 08-11-2021			Ashiki					Tenj212-19
 */

package com.ycs.tenjin.db;

import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.pojo.run.ResValConfig;
import com.ycs.tenjin.bridge.pojo.run.ResultValidation;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.RuntimeScreenshot;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.defect.Defect;
import com.ycs.tenjin.handler.ResultsHandler;
import com.ycs.tenjin.mail.TenjinReflection;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.HatsConstants;
import com.ycs.tenjin.util.Utilities;

/*//import ch.qos.logback.classic.Logger;*/

/*Change History
02-Mar-2015 R2.1 Database agnostic Changes: Babu: NVL Not Supported in SQL Server,use 'coalesce'
 */
public class ResultsHelper {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ResultsHelper.class);

	public void persistResultValidationMapping(int rvid, int stepRecId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);


		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		PreparedStatement pst = null;

		try{
			/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/
			pst = conn.prepareStatement("INSERT INTO RESVALMAP (TSTEP_REC_ID,RES_VAL_ID) VALUES(?,?)");
			/*Changed by  Parveen for changing insert query REQ #TJN_243_04 ends*/
			pst.setInt(1, stepRecId);
			pst.setInt(2, rvid);

			pst.execute();
		}catch(Exception e){
			throw new DatabaseException("Could not insert Result Validation for step",e);
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}

	


	public void persistResultValidationMapping(Connection conn, int rvid, int stepRecId) throws DatabaseException{


		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
		PreparedStatement pst = null;
		try{
			/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/
			pst = conn.prepareStatement("INSERT INTO RESVALMAP (TSTEP_REC_ID,RES_VAL_ID) VALUES(?,?)");
			/*Changed by  Parveen for changing insert query REQ #TJN_243_04 ends*/
			pst.setInt(1, stepRecId);
			pst.setInt(2, rvid);

			pst.execute();
		}catch(Exception e){
			throw new DatabaseException("Could not insert Result Validation for step",e);
		}finally{
			
			DatabaseHelper.close(pst);
		}

	}

	public void removeResultValidationMapping(int rvid, int stepRecId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst =null;

		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		try{
			pst = conn.prepareStatement("DELETE FROM RESVALMAP WHERE RES_VAL_ID = ? AND TSTEP_REC_ID = ?");
			pst.setInt(1, rvid);
			pst.setInt(2, stepRecId);

			pst.execute();
		}catch(Exception e){
			throw new DatabaseException("Could not remove Result Validation for step",e);
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}

	/* For Issue TJN_22_21 PDF Generation is time consuming
	 * Using one Connection and passing it to the subsequent method calls
	 */

	

	




	

	public ResultValidation hydrateResultValidation(int valId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		ResultValidation val = null;
		PreparedStatement pst0 =null;
		ResultSet rs=null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		try{
			pst0 = conn.prepareStatement("SELECT A.*, B.APP_NAME FROM MASRESVALDEF A, MASAPPLICATION B WHERE B.APP_ID = A.RVD_APP_ID AND A.RVD_ID = ?");
			pst0.setInt(1, valId);
			rs = pst0.executeQuery();
			int rId = 0;
			while(rs.next()){
				val = new ResultValidation();
				val.setId(rs.getInt("RVD_ID"));
				val.setName(rs.getString("RVD_NAME"));
				val.setDescription(rs.getString("RVD_DESC"));
				val.setType(rs.getString("RVD_TYPE"));
				val.setApplicationName(rs.getString("APP_NAME"));
				val.setApplication(rs.getInt("RVD_APP_ID"));
			}

			rId++;


			if(val != null){
				ArrayList<ResValConfig> config = this.hydrateResValConfiguration(valId);
				val.setConfiguration(config);
			}



		}catch(Exception e){
			logger.error("Could not fetch Result Validation Definition details",e);
			throw new DatabaseException("Could not fetch Result Validation Definition details",e);
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst0);
			DatabaseHelper.close(conn);
		}

		return val;



	}

	public ResultValidation hydrateResultValidationForRun(Connection conn, int valId) throws DatabaseException{
		ResultValidation val = null;
		PreparedStatement pst0 =null;
		ResultSet rs=null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		try{
			pst0 = conn.prepareStatement("SELECT A.*, B.APP_NAME FROM MASRESVALDEF A, MASAPPLICATION B WHERE B.APP_ID = A.RVD_APP_ID AND A.RVD_ID = ?");
			pst0.setInt(1, valId);
			rs = pst0.executeQuery();
			int rId = 0;
			while(rs.next()){
				val = new ResultValidation();
				val.setId(rs.getInt("RVD_ID"));
				val.setName(rs.getString("RVD_NAME"));
				val.setDescription(rs.getString("RVD_DESC"));
				val.setType(rs.getString("RVD_TYPE"));
				val.setApplicationName(rs.getString("APP_NAME"));
				val.setApplication(rs.getInt("RVD_APP_ID"));
			}

			rId++;


			if(val != null){
				ArrayList<ResValConfig> config = this.hydrateResValConfigurationForRun(conn,valId);
				val.setConfiguration(config);
			}



		}catch(Exception e){
			logger.error("Could not fetch Result Validation Definition details",e);
			throw new DatabaseException("Could not fetch Result Validation Definition details",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst0);
		}

		return val;



	}



	public ArrayList<ResValConfig> hydrateResValConfiguration(int rvId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		ResultSet rs=null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<ResValConfig> config = null;

		try{
			pst = conn.prepareStatement("SELECT * FROM RESVALCONFIG WHERE RES_VAL_ID = ? ORDER BY RES_VAL_SEQ");
			pst.setInt(1, rvId);

			rs = pst.executeQuery();
			config = new ArrayList<ResValConfig>();
			while(rs.next()){
				ResValConfig c = new ResValConfig();
				c.setResValId(rs.getInt("RES_VAL_ID"));
				c.setSequence(rs.getInt("RES_VAL_SEQ"));
				c.setType(rs.getString("RES_VAL_TYPE"));
				c.setModCode(rs.getString("RES_VAL_FUNC"));
				c.setField(rs.getString("RES_VAL_FLD"));
				c.setPage(rs.getString("RES_VAL_PA"));
				config.add(c);
			}	
		}catch(Exception e){
			throw new DatabaseException("Could not get configuration for RVD " + rvId,e);
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return config;
	}

	public ArrayList<ResValConfig> hydrateResValConfigurationForRun(Connection conn, int rvId) throws DatabaseException{
		PreparedStatement pst =null;
		ResultSet rs=null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<ResValConfig> config = null;

		try{
			pst = conn.prepareStatement("SELECT * FROM RESVALCONFIG WHERE RES_VAL_ID = ? ORDER BY RES_VAL_SEQ");
			pst.setInt(1, rvId);

			rs = pst.executeQuery();
			config = new ArrayList<ResValConfig>();
			while(rs.next()){
				ResValConfig c = new ResValConfig();
				c.setResValId(rs.getInt("RES_VAL_ID"));
				c.setSequence(rs.getInt("RES_VAL_SEQ"));
				c.setType(rs.getString("RES_VAL_TYPE"));
				c.setModCode(rs.getString("RES_VAL_FUNC"));
				c.setField(rs.getString("RES_VAL_FLD"));
				c.setPage(rs.getString("RES_VAL_PA"));
				config.add(c);
			}	
		}catch(Exception e){
			throw new DatabaseException("Could not get configuration for RVD " + rvId,e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return config;
	}

	

	public ResultValidation hydrateResultValidation(String name) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst0 =null;
		ResultSet rs=null;
		ResultValidation val = null;

		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		try{
			pst0 = conn.prepareStatement("SELECT A.*, B.APP_NAME FROM MASRESVALDEF A, MASAPPLICATION B WHERE B.APP_ID = A.RVD_APP_ID AND A.RVD_NAME = ?");
			pst0.setString(1, name);
			rs = pst0.executeQuery();
			int rId = 0;
			while(rs.next()){
				val = new ResultValidation();
				val.setId(rs.getInt("RVD_ID"));
				val.setName(rs.getString("RVD_NAME"));
				val.setDescription(rs.getString("RVD_DESC"));
				val.setType(rs.getString("RVD_TYPE"));
				val.setApplicationName(rs.getString("APP_NAME"));
			}

			rId++;




		}catch(Exception e){
			logger.error("Could not fetch Result Validation Definition details",e);
			throw new DatabaseException("Could not fetch Result Validation Definition details",e);
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst0);
			DatabaseHelper.close(conn);
		}

		return val;



	}

	public ArrayList<ValidationResult> hydrateValidationResult(int resValId, int runId, int testStepRecordId, int iteration) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst =null;
		ResultSet rs=null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<ValidationResult> results = null;
		try{
			pst = conn.prepareStatement("SELECT * FROM RESVALRESULTS WHERE RUN_ID = ? AND RES_VAL_ID =  ? AND TSTEP_REC_ID = ? AND ITERATION =?");
			pst.setInt(1, runId);
			pst.setInt(2, resValId);
			pst.setInt(3, testStepRecordId);
			pst.setInt(4, iteration);
			rs = pst.executeQuery();
			results = new ArrayList<ValidationResult>();
			while(rs.next()){
				ValidationResult res = new ValidationResult();
				res.setRunId(rs.getInt("RUN_ID"));
				res.setResValId(rs.getInt("RES_VAL_ID"));
				res.setTestStepRecordId(rs.getInt("TSTEP_REC_ID"));
				res.setPage(rs.getString("RES_VAL_PAGE"));
				res.setField(rs.getString("RES_VAL_FIELD"));
				res.setExpectedValue(rs.getString("RES_VAL_EXP_VAL"));
				res.setActualValue(rs.getString("RES_VAL_ACT_VAL"));
				res.setStatus(rs.getString("RES_VAL_STATUS"));
				res.setIteration(rs.getInt("ITERATION"));
				results.add(res);
			}
		}catch(Exception e){
			throw new DatabaseException("Could not get validation results for Res Val ID " + resValId,e);
		}finally{
		
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);


		}

		return results;
	}

	

	

	public ResultValidation hydrateResultValidation(Connection conn, int valId) throws DatabaseException{
		ResultValidation val = null;
		PreparedStatement pst0 =null;
		ResultSet rs=null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		try{
			pst0 = conn.prepareStatement("SELECT A.*, B.APP_NAME FROM MASRESVALDEF A, MASAPPLICATION B WHERE B.APP_ID = A.RVD_APP_ID AND A.RVD_ID = ?");
			pst0.setInt(1, valId);
			rs = pst0.executeQuery();
			int rId = 0;
			while(rs.next()){
				val = new ResultValidation();
				val.setId(rs.getInt("RVD_ID"));
				val.setName(rs.getString("RVD_NAME"));
				val.setDescription(rs.getString("RVD_DESC"));
				val.setType(rs.getString("RVD_TYPE"));
				val.setApplicationName(rs.getString("APP_NAME"));
				/*******************
				 * Fix for Bug 621 in Tenjin Demo Pack by Sriram Sridharan
				 */

				val.setApplication(rs.getInt("rvd_app_id"));
				/*******************
				 * Fix for Bug 621 in Tenjin Demo Pack by Sriram Sridharan Ends
				 */
			}

			rId++;

		}catch(Exception e){
			logger.error("Could not fetch Result Validation Definition details",e);
			throw new DatabaseException("Could not fetch Result Validation Definition details",e);
		}finally{
		
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst0);
		}

		return val;



	}

	public ResultValidation persistResultValidationDefinition(ResultValidation validation) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst0 =null;
		PreparedStatement pst =null;
		ResultSet rs=null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		try{
			/*02-Mar-2015 R2.1 Database agnostic Changes: Starts*/
			pst0 = conn.prepareStatement("SELECT coalesce(MAX(RVD_ID),0) AS RVD_ID FROM MASRESVALDEF");
			/*02-Mar-2015 R2.1 Database agnostic Changes: Ends*/
			rs = pst0.executeQuery();
			int rId = 0;
			while(rs.next()){
				rId = rs.getInt("RVD_ID");
			}

			rId++;


			/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/
			pst = conn.prepareStatement("INSERT INTO MASRESVALDEF (RVD_ID,RVD_NAME,RVD_APP_ID,RVD_DESC,RVD_TYPE) VALUES (?,?,?,?,?)");
			/*Changed by  Parveen for changing insert query REQ #TJN_243_04 ends*/
			pst.setInt(1, rId);
			pst.setString(2, validation.getName());
			pst.setInt(3, validation.getApplication());
			pst.setString(4, validation.getDescription());
			pst.setString(5, validation.getType());
			pst.execute();

			try{
				validation = this.hydrateResultValidation(conn, rId);
			}catch(Exception e){
				validation.setId(rId);

			}

		}catch(Exception e){
			logger.error("Could not insert row for Result Validation Definition",e);
			throw new DatabaseException("Could not insert row for Result Validation Definition",e);
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(pst0);
			DatabaseHelper.close(conn);
			
		}

		return validation;



	}
	





	


	

	

	
	

	public TestRun hydrateRun(Connection conn,int runId, int projectId) throws DatabaseException{
		PreparedStatement pst = null;
		ResultSet rs =null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		TestRun run = null;

		//Hydrate Run information from MastestRuns
		try {
			pst = conn.prepareStatement("SELECT * FROM MASTESTRUNS WHERE RUN_ID = ? AND RUN_PRJ_ID = ?");
			pst.setInt(1, runId);
			pst.setInt(2, projectId);

			rs = pst.executeQuery();

			while(rs.next()){
				run = new TestRun();
				run.setId(rs.getInt("RUN_ID"));
				run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
				run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
				run.setUser(rs.getString("RUN_USER"));
				int tsId = rs.getInt("RUN_TS_REC_ID");
				run.setTestSetRecordId(tsId);
				run.setStatus(rs.getString("RUN_STATUS"));
				run.setMachine_ip(rs.getString("RUN_MACHINE_IP"));
				run.setBrowser_type(rs.getString("RUN_BROWSER_TYPE"));
				if(run.getStartTimeStamp() != null && run.getEndTimeStamp() != null){
					long startMillis = run.getStartTimeStamp().getTime();
					long endMillis = run.getEndTimeStamp().getTime();

					long millis = endMillis - startMillis;

					String eTime = String.format(
							"%02d:%02d:%02d",
							TimeUnit.MILLISECONDS.toHours(millis),
							TimeUnit.MILLISECONDS.toMinutes(millis)
							- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
									.toHours(millis)),
							TimeUnit.MILLISECONDS.toSeconds(millis)
							- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
									.toMinutes(millis)));
					run.setElapsedTime(eTime);
				}
				/*****************************************************
				 * Change by Sriram for Performance Tuning of Execution History
				 */
				run.setTotalTests(rs.getInt("TOTAL_TCS"));
				run.setExecutedTests(rs.getInt("TOTAL_EXE_TCS"));
				run.setPassedTests(rs.getInt("TOTAL_PASSED_TCS"));
				run.setFailedTests(rs.getInt("TOTAL_FAILED_TCS"));
				run.setErroredTests(rs.getInt("TOTAL_ERROR_TCS"));
				/*****************************************************
				 * Change by Sriram for Performance Tuning of Execution History ends
				 */
				

			}

		} catch (SQLException e) {
			
			logger.error("Hydrate run from MASTESTRUNS --> Failed",e);
			throw new DatabaseException("Could not fetch run information",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		if(run == null){
			throw new DatabaseException("Could not fetch run information");
		}

		//Fetch Test Set details from TESTSETS and TESTSETMAP
		TestSet ts = null;
		try {
			/*changed by manish for bug T25IT-105 on 16-Aug-2017 starts*/
			/*PreparedStatement pst = conn.prepareStatement("SELECT * FROM TESTSETS A LEFT join TESTSETMAP B on (A.TS_REC_ID = B.TSM_TS_REC_ID) WHERE A.TS_REC_ID = ?  AND A.TS_REC_TYPE = ? order by b.tsm_tc_seq");*/
			pst = conn.prepareStatement("SELECT * FROM TESTSETS A LEFT join TESTSETMAP B on (A.TS_REC_ID = B.TSM_TS_REC_ID) WHERE A.TS_REC_ID = ?  AND A.TS_REC_TYPE IN(?,?) order by b.tsm_tc_seq");
			
			pst.setInt(1, run.getTestSetRecordId());
			pst.setString(2, "TS");
			pst.setString(3, "TSA");
			/*changed by manish for bug T25IT-105 on 16-Aug-2017 ends*/
			//pst.setInt(2, projectId);
			rs = pst.executeQuery();
			ArrayList<TestCase> tests = new ArrayList<TestCase>();
			while(rs.next()){
				ts = new TestSet();
				ts.setName(rs.getString("TS_NAME"));
				ts.setRecordType(rs.getString("TS_REC_TYPE"));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_TYPE"));
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				//ts.setStatus(rs.getString("TS_STATUS"));
				ts.setDescription(rs.getString("TS_DESC"));
				
				/*Added by Ramya for TENJINCG-832 starts*/
				ts.setMode(rs.getString("TS_EXEC_MODE"));
				/*Added by Ramya for TENJINCG-832 ends*/
				
				int tcRecId = rs.getInt("TSM_TC_REC_ID");
				PreparedStatement tPst=null;
				ResultSet tRs=null;
				//Get the details of testcase along with results summary from TESTCASES and RUNRESULTS_TC
				try{
					
					tPst = conn.prepareStatement("SELECT A.*, B.TC_TOTAL_STEPS, B.TC_TOTAL_EXEC, B.TC_TOTAL_PASS, B.TC_TOTAL_FAIL, B.TC_TOTAL_ERROR, B.TC_STATUS AS TC_STAT FROM TESTCASES A, RUNRESULTS_TC B WHERE B.TC_REC_ID = A.TC_REC_ID AND A.TC_REC_ID = ? AND B.RUN_ID = ?");
					tPst.setInt(1, tcRecId);
					tPst.setInt(2, runId);
					
					tRs = tPst.executeQuery();

					while(tRs.next()){
						TestCase tc = new TestCase();
						tc.setTcId(tRs.getString("tc_id"));
						tc.setTcName(tRs.getString("tc_name"));
						tc.setTcType(tRs.getString("tc_type"));
						//tc.setTcStatus(tRs.getString("tc_status"));
						tc.setTcCreatedBy(tRs.getString("tc_created_by"));
						/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
						/*tc.setTcCreatedOn(tRs.getDate("tc_created_on"));*/
						tc.setTcCreatedOn(tRs.getTimestamp("tc_created_on"));
						/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
						tc.setTcModifiedBy(tRs.getString("tc_modified_by"));
						tc.setTcModifiedOn(tRs.getDate("tc_modified_on"));
						tc.setTcDesc(tRs.getString("tc_desc"));
						tc.setTcRecId(tRs.getInt("TC_REC_ID"));
						tc.setTcFolderId(tRs.getInt("TC_FOLDER_ID"));
						tc.setProjectId(tRs.getInt("TC_PRJ_ID"));
						tc.setRecordType(tRs.getString("TC_REC_TYPE"));
						tc.setTcReviewed(tRs.getString("TC_REVIEWED"));
						tc.setTcPriority(tRs.getString("TC_PRIORITY"));
						tc.setTcRecId(tRs.getInt("TC_REC_ID"));
						tc.setTotalSteps(tRs.getInt("TC_TOTAL_STEPS"));
						tc.setTotalExecutedSteps(tRs.getInt("TC_TOTAL_EXEC"));
						tc.setTotalPassedSteps(tRs.getInt("TC_TOTAL_PASS"));
						tc.setTotalFailedSteps(tRs.getInt("TC_TOTAL_FAIL"));
						tc.setTotalErrorredSteps(tRs.getInt("TC_TOTAL_ERROR"));
						tc.setTcStatus(tRs.getString("TC_STAT"));
						tests.add(tc);
					}
				}catch(SQLException e){
					logger.error("Fetch Test Case Details from TESTCASES and RUNRESULTS_TC --> Failed",e);
					continue;
				}finally{
					DatabaseHelper.close(tRs);
					DatabaseHelper.close(tPst);
				}

			}

			ts.setTests(tests);
		} catch (SQLException e) {
			
			logger.error("Fetch Test Set details from TESTSETS and TESTSETMAP --> Failed",e);
			throw new DatabaseException("Could not fetch details of the test set in the current run",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		ArrayList<TestCase> newTCs = new ArrayList<TestCase>();
		PreparedStatement p =null;
		ResultSet rs2=null;
		//For each test case in the test set, get all steps along with step results
		for(TestCase tc:ts.getTests()){
			ArrayList<TestStep> steps = null;
			try {
				/*changed by manish for defect TENJINCG-135 on 23-02-2017 starts*/
				pst = conn.prepareStatement("SELECT A.*, B.RUN_ID,B.TSTEP_TOTAL_TXN, B.TSTEP_TOTAL_EXEC, B.TSTEP_TOTAL_PASS, B.TSTEP_TOTAL_FAIL, B.TSTEP_TOTAL_ERROR,B.TSTEP_STATUS AS TSTEP_STAT FROM TESTSTEPS A, RUNRESULTS_TS B WHERE B.TSTEP_REC_ID = A.TSTEP_REC_ID AND A.TC_REC_ID = ? AND B.RUN_ID = ? ORDER BY A.TSTEP_REC_ID");
				/*changed by manish for defect TENJINCG-135 on 23-02-2017 ends*/
				pst.setInt(1, tc.getTcRecId());
				pst.setInt(2, runId);
				rs = pst.executeQuery();
				steps = new ArrayList<TestStep>();
				while(rs.next()){
					try{
					TestStep t = new TestStep();
					t.setRecordId(rs.getInt("tstep_rec_id"));
					t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
					t.setDataId(rs.getString("TSTEP_DATA_ID"));
					t.setType(rs.getString("TSTEP_TYPE"));
					t.setDescription(rs.getString("TSTEP_DESC"));
					t.setExpectedResult(rs.getString("TSTEP_EXP_RESULT"));
					t.setAppId(rs.getInt("APP_ID"));
					t.setModuleCode(rs.getString("FUNC_CODE"));
					t.setId(rs.getString("TSTEP_ID"));
					t.setTestCaseRecordId(rs.getInt("TC_REC_ID"));
					t.setOperation(rs.getString("TSTEP_OPERATION"));
					t.setAutLoginType(rs.getString("TSTEP_AUT_LOGIN_TYPE"));
					t.setTotalTransactions(rs.getInt("TSTEP_TOTAL_TXN"));
					t.setTotalExecutedTxns(rs.getInt("TSTEP_TOTAL_EXEC"));
					t.setTotalPassedTxns(rs.getInt("TSTEP_TOTAL_PASS"));
					t.setTotalFailedTxns(rs.getInt("TSTEP_TOTAL_FAIL"));
					t.setTotalErroredTxns(rs.getInt("TSTEP_TOTAL_ERROR"));
					t.setStatus(rs.getString("TSTEP_STAT"));
					//Get all Result Validations for this step
					p = conn.prepareStatement("SELECT * FROM RESVALMAP WHERE TSTEP_REC_ID = ?");
					p.setInt(1, t.getRecordId());

					ArrayList<ResultValidation> vals = new ArrayList<ResultValidation>();
					ArrayList<ResultValidation> newVals = new ArrayList<ResultValidation>();
					rs2 = p.executeQuery();
					while(rs2.next()){
						int r = rs2.getInt("RES_VAL_ID");
						ResultValidation val = this.hydrateResultValidation(conn,r);
						vals.add(val);
					}

					//Get Detailed step results from RUNRESULT_TS_TXN
					ArrayList<StepIterationResult> results= new ArrayList<StepIterationResult>();
					PreparedStatement rPst =null;
					ResultSet oRs =null;
					try{
						rPst = conn.prepareStatement("SELECT * FROM RUNRESULT_TS_TXN WHERE TSTEP_REC_ID = ? AND RUN_ID = ? ORDER BY TXN_NO");
						rPst.setInt(1, t.getRecordId());
						rPst.setInt(2, runId);
						oRs = rPst.executeQuery();
						while(oRs.next()){
							StepIterationResult r = new StepIterationResult();
							r.setIterationNo(oRs.getInt("TXN_NO"));
							r.setResult(oRs.getString("TSTEP_RESULT"));
							r.setMessage(oRs.getString("TSTEP_MESSAGE"));
							Blob b = oRs.getBlob("TSTEP_SCRSHOT");
							if(b != null){
								byte barr[] = b.getBytes(1,(int)b.length());
								r.setScreenshotByteArray(barr);
							}else{
								r.setScreenshotByteArray(null);
							}
							PreparedStatement vPst =null;
							ResultSet vRs =null;
							//Get Result Validation Results
							for(ResultValidation val:vals){
								try{
									vPst = conn.prepareStatement("SELECT * FROM RESVALRESULTS WHERE RUN_ID = ? AND RES_VAL_ID =  ? AND TSTEP_REC_ID = ? AND ITERATION =?");
									vPst.setInt(1, runId);
									vPst.setInt(2, val.getId());
									vPst.setInt(3, t.getRecordId());
									vPst.setInt(4, r.getIterationNo());
									vRs = vPst.executeQuery();
									ArrayList<ValidationResult> valRess = new ArrayList<ValidationResult>();
									while(vRs.next()){
										ValidationResult res = new ValidationResult();
										res.setRunId(vRs.getInt("RUN_ID"));
										res.setResValId(vRs.getInt("RES_VAL_ID"));
										res.setTestStepRecordId(vRs.getInt("TSTEP_REC_ID"));
										res.setPage(vRs.getString("RES_VAL_PAGE"));
										res.setField(vRs.getString("RES_VAL_FIELD"));
										res.setExpectedValue(vRs.getString("RES_VAL_EXP_VAL"));
										res.setActualValue(vRs.getString("RES_VAL_ACT_VAL"));
										res.setStatus(vRs.getString("RES_VAL_STATUS"));
										res.setIteration(vRs.getInt("ITERATION"));
										res.setDetailRecordNo(vRs.getString("RES_VAL_DETAIL_ID"));
										valRess.add(res);
									}

									val.setResults(valRess);
									newVals.add(val);
								}catch(SQLException e){
									logger.error("Get Result Validation Results for Step (record id - " + t.getRecordId() + ") from RESVALRESULTS --> Failed",e);
									continue;
								}finally{
									DatabaseHelper.close(vRs);
									DatabaseHelper.close(vPst);
								}
							}

							r.setValidations(newVals);
							results.add(r);
						}
					}catch(SQLException e){
						logger.error("Get Detailed Step result from RUNRESULT_TS_TXN --> Failed",e);
						continue;
					}finally{
						DatabaseHelper.close(oRs);
						DatabaseHelper.close(rPst);
					}

					t.setDetailedResults(results);
					steps.add(t);
				}finally{
						DatabaseHelper.close(rs2);
						DatabaseHelper.close(p);
					}
				}
			} catch (SQLException e) {
				
				logger.error("Fetch steps for Test Case " + tc.getTcId() + " from TESTSTEPS and RUNRESULTS_TS --> Failed",e);
				throw new DatabaseException("Could not get Test Run Info",e);
			}finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}

			tc.setTcSteps(steps);
			newTCs.add(tc);
		}

		ts.setTests(null);
		ts.setTests(newTCs);

		run.setTestSet(null);
		run.setTestSet(ts);

		return run;

	}
	/* For Issue TJN_22_21 PDF Generation is time consuming
	 * Using one Connection and passing it to the subsequent method calls
	 */		
	/*************************************************************
	 * Method changed by Sriram for Run Results Summary (Tenjin v2.1)
	 * @throws DatabaseException 
	 */

	
/*	paste it in iteration helper*/
	public TestRun hydrateRun(int runId, int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		TestRun run = null;

		//Hydrate Run information from MastestRuns
		PreparedStatement pst =null;
		PreparedStatement pstmt=null;
		ResultSet rset=null;
		ResultSet rs =null;
		try {
			/* Modified by Roshni for TENJINCG-259 */
			/*Modified by Preeti for TENJINCG-484 starts*/
			/*changed by sahana for Mobility, pCloudy: Starts*/
			pst = conn.prepareStatement("SELECT A.RUN_ID, A.RUN_TASK_TYPE, A.RUN_STATUS AS RUN_COMPLETION_STATUS, A.RUN_START_TIME,A.RUN_END_TIME,A.RUN_USER,A.RUN_TS_REC_ID,A.RUN_MACHINE_IP,A.RUN_CL_PORT,A.RUN_BROWSER_TYPE,A.RUN_PRJ_NAME,A.PARENT_RUN_ID,A.RUN_DOMAIN_NAME,A.RUN_DEVICE_REC_ID, A.RUN_DEVICE_FARM,B.TOTAL_TCS,B.TOTAL_EXE_TCS,B.TOTAL_PASSED_TCS,B.TOTAL_FAILED_TCS,B.TOTAL_ERROR_TCS,B.RUN_STATUS FROM MASTESTRUNS A, V_RUNRESULTS B WHERE B.RUN_ID = A.RUN_ID AND A.RUN_ID = ? AND A.RUN_PRJ_ID=?");
			/*changed by sahana for Mobility, pCloudy: ends*/
			
			/*Modified by Preeti for TENJINCG-484 ends*/
			pst.setInt(1, runId);
			pst.setInt(2, projectId);

			rs = pst.executeQuery();

			while(rs.next()){
				run = new TestRun();
				run.setId(rs.getInt("RUN_ID"));
				run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
				run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
				run.setUser(rs.getString("RUN_USER"));
				int tsId = rs.getInt("RUN_TS_REC_ID");
				run.setTestSetRecordId(tsId);
				run.setStatus(rs.getString("RUN_STATUS"));
				run.setMachine_ip(rs.getString("RUN_MACHINE_IP"));
				/*Added by Preeti for TENJINCG-484 starts*/
				run.setTargetPort(rs.getInt("RUN_CL_PORT"));
				/*Added by Preeti for TENJINCG-484 ends*/
				run.setBrowser_type(rs.getString("RUN_BROWSER_TYPE"));
				run.setTaskType(rs.getString("RUN_TASK_TYPE"));
				if(run.getStartTimeStamp() != null && run.getEndTimeStamp() != null){
					long startMillis = run.getStartTimeStamp().getTime();
					long endMillis = run.getEndTimeStamp().getTime();

					long millis = endMillis - startMillis;

					String eTime = String.format(
							"%02d:%02d:%02d",
							TimeUnit.MILLISECONDS.toHours(millis),
							TimeUnit.MILLISECONDS.toMinutes(millis)
							- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
									.toHours(millis)),
							TimeUnit.MILLISECONDS.toSeconds(millis)
							- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
									.toMinutes(millis)));
					run.setElapsedTime(eTime);
				}
				/*****************************************************
				 * Change by Sriram for Performance Tuning of Execution History
				 */
				run.setTotalTests(rs.getInt("TOTAL_TCS"));
				run.setExecutedTests(rs.getInt("TOTAL_EXE_TCS"));
				run.setPassedTests(rs.getInt("TOTAL_PASSED_TCS"));
				run.setFailedTests(rs.getInt("TOTAL_FAILED_TCS"));
				run.setErroredTests(rs.getInt("TOTAL_ERROR_TCS"));
				/*changed by sahana for Mobility: Starts*/
				run.setDeviceRecId(rs.getInt("RUN_DEVICE_REC_ID"));
				/*changed by sahana for Mobility: ends*/
				/*Added by sahana for pCloudy: Starts*/
				run.setDeviceFarmFlag(rs.getString("RUN_DEVICE_FARM"));
				/*Added by sahana for pCloudy: Ends*/
				
				/*****************************************************
				 * Change by Sriram for Performance Tuning of Execution History ends
				 */
				run.setDomainName(rs.getString("RUN_DOMAIN_NAME"));
				run.setProjectName(rs.getString("RUN_PRJ_NAME"));
				/* Added by Roshni for TENJINCG-259 */
				run.setParentRunId(rs.getInt("PARENT_RUN_ID"));
				/*************************************************************************************
				 * Added by Sriram for Requirement TJN_22_07 (Screentshots for information/error message encountered during execution)
				 * AND
				 * Requirement TJN_22_10 (Multiple Outputs should be captured which can be used as an input to the next test step)
				 * 25-09-2015 Starts
				 */
				
				/*************************************************************************************
				 * Added by Sriram for Requirement TJN_22_07 (Screentshots for information/error message encountered during execution)
				 * AND
				 * Requirement TJN_22_10 (Multiple Outputs should be captured which can be used as an input to the next test step)
				 * 25-09-2015 ends
				 */
				/*added by manish for bug T25IT-105 on 18-Aug-2017 starts*/
				run.setProjectId(projectId);
				/*added by manish for bug T25IT-105 on 18-Aug-2017 ends*/

			}
			/* Added by Roshni for TENJINCG-259 starts*/
			int tcRecId=0;
			/*modified by shruthi for sql injection starts*/
			pstmt=conn.prepareStatement("SELECT TC_REC_ID FROM V_RUNRESULTS_TC WHERE RUN_ID=?");
			pstmt.setInt(1, runId);
			/*modified by shruthi for sql injection ends*/
			rset=pstmt.executeQuery();
			if(rset.next())
				tcRecId=rset.getInt("TC_REC_ID");
			 /* added by shruthi for TENJINCG-1238 starts*/	
			try
			{
			run.setTcRecId(String.valueOf(tcRecId));
			}
			  catch(NullPointerException e) 
	        { 
	            System.out.print("Caught NullPointerException"); 
	        } 
			 /* added by shruthi for TENJINCG-1238 ends*/	
			/* Added by Roshni for TENJINCG-259 ends*/
			
		
		} catch (SQLException e) {
			
			logger.error("Hydrate run from MASTESTRUNS --> Failed",e);
			throw new DatabaseException("Could not fetch run information",e);
		}finally{
			DatabaseHelper.close(rset);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		if(run == null){
			throw new DatabaseException("Could not fetch run information");
		}
			/* Added by Roshni for TENJINCG-259 starts*/
			run.setPureRunId(this.getParent(runId));
			run.setReRunIds(this.getChildRunIDs(run.getPureRunId()));			
			run.setAllParentRunIds(this.getAllParentIds());
			/* Added by Roshni for TENJINCG-259 ends*/
		//Fetch Test Set details from TESTSETS and TESTSETMAP
		TestSet ts = null;
		try {
			//Fix for Defect#TEN-23 -Sriram
			pst = conn.prepareStatement("SELECT * FROM TESTSETS A LEFT join TESTSETMAP B on (A.TS_REC_ID = B.TSM_TS_REC_ID) WHERE A.TS_REC_ID = ?  AND A.TS_REC_TYPE IN (?,?) order by b.tsm_tc_seq");
			pst.setInt(1, run.getTestSetRecordId());
			pst.setString(2, "TS");
			pst.setString(3, "TSA");//Fix for Defect#TEN-23 -Sriram ends
			rs = pst.executeQuery();
			ArrayList<TestCase> tests = new ArrayList<TestCase>();
			while(rs.next()){
				ts = new TestSet();
				ts.setName(rs.getString("TS_NAME"));
				ts.setRecordType(rs.getString("TS_REC_TYPE"));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_TYPE"));
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				//ts.setStatus(rs.getString("TS_STATUS"));
				ts.setDescription(rs.getString("TS_DESC"));

				/*added by manish for bug T25IT-105 on 18-Aug-2017 starts*/
				ts.setProject(projectId);
				/*added by manish for bug T25IT-105 on 18-Aug-2017 ends*/
				
				/*Added by Ramya for TENJINCG-832 starts*/
				ts.setMode(rs.getString("TS_EXEC_MODE"));
				/*Added by Ramya for TENJINCG-832 ends*/
				
				int tcRecId = rs.getInt("TSM_TC_REC_ID");
				//Get the details of testcase along with results summary from TESTCASES and RUNRESULTS_TC
				PreparedStatement tPst =null;
				ResultSet tRs =null;
				try{
					tPst = conn.prepareStatement("SELECT A.*, B.TC_TOTAL_STEPS, B.TC_TOTAL_EXEC, B.TC_TOTAL_PASS, B.TC_TOTAL_FAIL, B.TC_TOTAL_ERROR, B.TC_STATUS AS TC_STAT FROM TESTCASES A, V_RUNRESULTS_TC B WHERE B.TC_REC_ID = A.TC_REC_ID AND A.TC_REC_ID = ? AND B.RUN_ID = ?");
					tPst.setInt(1, tcRecId);
					tPst.setInt(2, runId);
					tRs = tPst.executeQuery();

					while(tRs.next()){
						TestCase tc = new TestCase();
						tc.setTcId(tRs.getString("tc_id"));
						tc.setTcName(tRs.getString("tc_name"));
						tc.setTcType(tRs.getString("tc_type"));
						//tc.setTcStatus(tRs.getString("tc_status"));
						tc.setTcCreatedBy(tRs.getString("tc_created_by"));
						/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
						/*tc.setTcCreatedOn(tRs.getDate("tc_created_on"));*/
						tc.setTcCreatedOn(tRs.getTimestamp("tc_created_on"));
						/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
						tc.setTcModifiedBy(tRs.getString("tc_modified_by"));
						tc.setTcModifiedOn(tRs.getDate("tc_modified_on"));
						tc.setTcDesc(tRs.getString("tc_desc"));
						tc.setTcRecId(tRs.getInt("TC_REC_ID"));
						tc.setTcFolderId(tRs.getInt("TC_FOLDER_ID"));
						tc.setProjectId(tRs.getInt("TC_PRJ_ID"));
						tc.setRecordType(tRs.getString("TC_REC_TYPE"));
						tc.setTcReviewed(tRs.getString("TC_REVIEWED"));
						tc.setTcPriority(tRs.getString("TC_PRIORITY"));
						tc.setTcRecId(tRs.getInt("TC_REC_ID"));
						tc.setTotalSteps(tRs.getInt("TC_TOTAL_STEPS"));
						tc.setTotalExecutedSteps(tRs.getInt("TC_TOTAL_EXEC"));
						tc.setTotalPassedSteps(tRs.getInt("TC_TOTAL_PASS"));
						tc.setTotalFailedSteps(tRs.getInt("TC_TOTAL_FAIL"));
						tc.setTotalErrorredSteps(tRs.getInt("TC_TOTAL_ERROR"));
						tc.setTcStatus(tRs.getString("TC_STAT"));
						tests.add(tc);
					}
				}catch(SQLException e){
					logger.error("Fetch Test Case Details from TESTCASES and RUNRESULTS_TC --> Failed",e);
					continue;
				}finally{
					DatabaseHelper.close(tRs);
					DatabaseHelper.close(tPst);
				}

			}

			ts.setTests(tests);
		} catch (SQLException e) {
			
			logger.error("Fetch Test Set details from TESTSETS and TESTSETMAP --> Failed",e);
			throw new DatabaseException("Could not fetch details of the test set in the current run",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		ArrayList<TestCase> newTCs = new ArrayList<TestCase>();

		//For each test case in the test set, get all steps along with step results
		for(TestCase tc:ts.getTests()){
			ArrayList<TestStep> steps = null;
			try {
				/*changed by manish for defect TENJINCG-135 on 23-02-2017 starts*/
				/*PreparedStatement pst = conn.prepareStatement("SELECT A.*, B.RUN_ID,B.TSTEP_TOTAL_TXN, B.TSTEP_TOTAL_EXEC, B.TSTEP_TOTAL_PASS, B.TSTEP_TOTAL_FAIL, B.TSTEP_TOTAL_ERROR,B.TSTEP_STATUS AS TSTEP_STAT FROM TESTSTEPS A, V_RUNRESULTS_TS B WHERE B.TSTEP_REC_ID = A.TSTEP_REC_ID AND A.TC_REC_ID = ? AND B.RUN_ID = ?");*/
				/*PreparedStatement pst = conn.prepareStatement("SELECT A.*, C.APP_NAME, B.RUN_ID,B.TSTEP_TOTAL_TXN, B.TSTEP_TOTAL_EXEC, B.TSTEP_TOTAL_PASS, B.TSTEP_TOTAL_FAIL, B.TSTEP_TOTAL_ERROR,B.TSTEP_STATUS AS TSTEP_STAT FROM TESTSTEPS A, V_RUNRESULTS_TS B, MASAPPLICATION C WHERE B.TSTEP_REC_ID = A.TSTEP_REC_ID AND C.APP_ID = A.APP_ID AND A.TC_REC_ID = ? AND B.RUN_ID = ? ORDER BY A.TSTEP_REC_ID");*/
				/*changed by manish for defect TENJINCG-135 on 23-02-2017 ends*/
				/*changed by Roshni for TENJINCG-198 starts */
				pst = conn.prepareStatement("SELECT A.*, C.APP_NAME, B.RUN_ID,B.TSTEP_TOTAL_TXN, B.TSTEP_TOTAL_EXEC, B.TSTEP_TOTAL_PASS, B.TSTEP_TOTAL_FAIL, B.TSTEP_TOTAL_ERROR,B.TSTEP_STATUS AS TSTEP_STAT FROM TESTSTEPS A, V_RUNRESULTS_TS B, MASAPPLICATION C WHERE B.TSTEP_REC_ID = A.TSTEP_REC_ID AND C.APP_ID = A.APP_ID AND A.TC_REC_ID = ? AND B.RUN_ID = ? ORDER BY A.TSTEP_EXEC_SEQ");
				/*changed by Roshni for TENJINCG-198 ends */
				pst.setInt(1, tc.getTcRecId());
				pst.setInt(2, runId);
				rs = pst.executeQuery();
				steps = new ArrayList<TestStep>();
				while(rs.next()){
					TestStep t = new TestStep();
					t.setRecordId(rs.getInt("tstep_rec_id"));
					t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
					t.setDataId(rs.getString("TSTEP_DATA_ID"));
					t.setType(rs.getString("TSTEP_TYPE"));
					t.setDescription(rs.getString("TSTEP_DESC"));
					t.setExpectedResult(rs.getString("TSTEP_EXP_RESULT"));
					t.setAppId(rs.getInt("APP_ID"));
					t.setModuleCode(rs.getString("FUNC_CODE"));
					t.setId(rs.getString("TSTEP_ID"));
					t.setTestCaseRecordId(rs.getInt("TC_REC_ID"));
					t.setOperation(rs.getString("TSTEP_OPERATION"));
					t.setAutLoginType(rs.getString("TSTEP_AUT_LOGIN_TYPE"));
					t.setTotalTransactions(rs.getInt("TSTEP_TOTAL_TXN"));
					t.setTotalExecutedTxns(rs.getInt("TSTEP_TOTAL_EXEC"));
					t.setTotalPassedTxns(rs.getInt("TSTEP_TOTAL_PASS"));
					t.setTotalFailedTxns(rs.getInt("TSTEP_TOTAL_FAIL"));
					t.setTotalErroredTxns(rs.getInt("TSTEP_TOTAL_ERROR"));
					t.setStatus(rs.getString("TSTEP_STAT"));
					t.setTxnMode(rs.getString("TXNMODE"));
					/*Commented by Preeti for TENJINCG-503 starts*/
					/*t.setApiCode(rs.getString("API_CODE"));*/
					/*Commented by Preeti for TENJINCG-503 ends*/
					t.setAppName(rs.getString("APP_NAME"));
					/* For TENJINCG-312 */
					t.setValidationType(rs.getString("TSTEP_VAL_TYPE"));
					/* For TENJINCG-312 ends*/
					
					PreparedStatement rmPst = null;
					ResultSet rmRs = null;
					try {
						rmPst = conn.prepareStatement("SELECT MEDIA_TYPE,REPRESENTATION_TYPE FROM TJN_AUT_API_OP_REPRESENTATIONS WHERE API_CODE=? AND APP_ID=?  AND API_OPERATION=? AND REPRESENTATION_TYPE NOT IN('HEADER')");
						rmPst.setString(1, t.getModuleCode());
						rmPst.setInt(2, t.getAppId());
						rmPst.setString(3, t.getOperation());
						rmRs = rmPst.executeQuery();
						Map<String, String> map= new HashMap<String, String>();
						while (rmRs.next()) {
				        map.put(rmRs.getString("REPRESENTATION_TYPE"), rmRs.getString("MEDIA_TYPE"));
						}
						t.setMediaType(map);
					} catch (Exception e) {
						
						logger.error(e.getMessage());
					}finally{
						DatabaseHelper.close(rmRs);
						DatabaseHelper.close(rmPst);
					}
					//Get Detailed step results from RUNRESULT_TS_TXN
					ArrayList<StepIterationResult> results= new ArrayList<StepIterationResult>();
					PreparedStatement rPst =null;
					ResultSet oRs =null;

					try{
						rPst = conn.prepareStatement("SELECT * FROM RUNRESULT_TS_TXN WHERE TSTEP_REC_ID = ? AND RUN_ID = ? ORDER BY TXN_NO");
						rPst.setInt(1, t.getRecordId());
						rPst.setInt(2, runId);
						oRs = rPst.executeQuery();
						while(oRs.next()){
							StepIterationResult r = new StepIterationResult();
							r.setIterationNo(oRs.getInt("TXN_NO"));
							/*************************************************************************************
							 * Added by Sriram for Requirement TJN_22_07 (Screentshots for information/error message encountered during execution)
							 * AND
							 * Requirement TJN_22_10 (Multiple Outputs should be captured which can be used as an input to the next test step)
							 * 25-09-2015 Starts
							 */


							ArrayList<RuntimeScreenshot> runtimeScreenshots = new ArrayList<RuntimeScreenshot>();
							ArrayList<RuntimeFieldValue> runtimeValues = new ArrayList<RuntimeFieldValue>();
							String tdUid = oRs.getString("TSTEP_TDUID");
							r.setTduid(tdUid);
							/*************************************************************************************
							 * Added by Sriram for Requirement TJN_22_07 (Screentshots for information/error message encountered during execution)
							 * AND
							 * Requirement TJN_22_10 (Multiple Outputs should be captured which can be used as an input to the next test step)
							 * 25-09-2015 ends
							 */
							r.setResult(oRs.getString("TSTEP_RESULT"));
							r.setMessage(oRs.getString("TSTEP_MESSAGE"));
							//Code changes for OAuth 2.0 requirement TENJINCG-1018- Avinash - Starts
							r.setApiAuthType(oRs.getString("API_AUTH_TYPE"));
							r.setApiAccessToken(oRs.getString("API_ACCESS_TOKEN"));
							  /* added by shruthi for TENJINCG-1238 starts*/
							r.setWsReqMessage(oRs.getString("TSTEP_WS_REQ_MSG"));
							r.setWsResMessage(oRs.getString("TSTEP_WS_RES_MSG"));
							  /* added by shruthi for TENJINCG-1238 ends*/
							
							//Code changes for OAuth 2.0 requirement TENJINCG-1018- Avinash - Ends
							Blob b = oRs.getBlob("TSTEP_SCRSHOT");
							if(b != null){
								byte barr[] = b.getBytes(1,(int)b.length());
								r.setScreenshotByteArray(barr);
							}else{
								r.setScreenshotByteArray(null);
							}
							/*Added by Preeti for TENJINCG-101 starts*/
							r.setStartTimeStamp(oRs.getTimestamp("EXEC_START_TIME"));
							r.setEndTimeStamp(oRs.getTimestamp("EXEC_END_TIME"));
							if(r.getEndTimeStamp() != null && r.getStartTimeStamp() != null){
								String elapsedTime= Utilities.calculateElapsedTime(r.getStartTimeStamp().getTime(), r.getEndTimeStamp().getTime());
								r.setElapsedTime(elapsedTime);
							}
							else
								r.setElapsedTime("00:00:00");
							/*Added by Preeti for TENJINCG-101 ends*/
							PreparedStatement vPst = null;
							ResultSet vRs = null;
							try{
								vPst = conn.prepareStatement("SELECT * FROM RESVALRESULTS WHERE RUN_ID = ? AND RES_VAL_ID =  ? AND TSTEP_REC_ID = ? AND ITERATION =?");
								vPst.setInt(1, runId);
								vPst.setInt(2, 0);
								vPst.setInt(3, t.getRecordId());
								vPst.setInt(4, r.getIterationNo());
								vRs = vPst.executeQuery();
								ArrayList<ValidationResult> valRess = new ArrayList<ValidationResult>();
								while(vRs.next()){
									ValidationResult res = new ValidationResult();
									res.setRunId(vRs.getInt("RUN_ID"));
									res.setResValId(vRs.getInt("RES_VAL_ID"));
									res.setTestStepRecordId(vRs.getInt("TSTEP_REC_ID"));
									res.setPage(vRs.getString("RES_VAL_PAGE"));
									res.setField(vRs.getString("RES_VAL_FIELD"));
									res.setExpectedValue(vRs.getString("RES_VAL_EXP_VAL"));
									res.setActualValue(vRs.getString("RES_VAL_ACT_VAL"));
									res.setStatus(vRs.getString("RES_VAL_STATUS"));
									res.setIteration(vRs.getInt("ITERATION"));
									res.setDetailRecordNo(vRs.getString("RES_VAL_DETAIL_ID"));
									valRess.add(res);
								}
								r.setValidationResults(valRess);
							}catch(Exception e){
								logger.error("Get Result Validation Results for Step (record id - " + t.getRecordId() + ") from RESVALRESULTS --> Failed",e);
								continue;
							}finally{
								DatabaseHelper.close(vRs);
								DatabaseHelper.close(vPst);
							}

							/*************************************************************************************
							 * Added by Sriram for Requirement TJN_22_07 (Screentshots for information/error message encountered during execution)
							 * AND
							 * Requirement TJN_22_10 (Multiple Outputs should be captured which can be used as an input to the next test step)
							 * 25-09-2015 Starts
							 */
							PreparedStatement sPst=null;
							ResultSet sRs=null;;

							try {
								sPst = conn.prepareStatement("SELECT * FROM TJN_EXEC_SCRNSHOT WHERE RUN_ID = ? AND TSTEP_REC_ID = ? AND TDUID=? ORDER BY SEQ_NO");
								sPst.setInt(1, runId);
								sPst.setInt(2, t.getRecordId());
								sPst.setString(3, tdUid);
								sRs = sPst.executeQuery();
								while (sRs.next()) {
									RuntimeScreenshot s = new RuntimeScreenshot();
									s.setRunId(runId);
									s.setTdGid(sRs.getString("TDGID"));
									s.setTdUid(sRs.getString("TDUID"));
									s.setTestStepRecordId(sRs.getInt("TSTEP_REC_ID"));
									s.setIteration(sRs.getInt("ITERATION_NO"));
									s.setSequence(sRs.getInt("SEQ_NO"));
									s.setType(sRs.getString("TYPE_SCRNSHOT"));
									s.setTimestamp(sRs.getTimestamp("SCR_TIMESTAMP"));
									s.setDescription(sRs.getString("SCR_DESC"));
									//Fix by Sriram for Defect#816
									/*Blob b1 = oRs.getBlob("TSTEP_SCRSHOT");*/
									Blob b1 = sRs.getBlob("SCRNSHOT");
									//Fix by Sriram for Defect#816 ends
									if (b1 != null) {
										byte barr[] = b1.getBytes(1, (int) b1.length());
										s.setScreenshotByteArray(barr);
									} else {
										s.setScreenshotByteArray(null);
									}

									runtimeScreenshots.add(s);
								}
								r.setRuntimeSnapshots(runtimeScreenshots);
							
							} catch (Exception e) {
								
								logger.error(e.getMessage());
							}finally{
								DatabaseHelper.close(sRs);
								DatabaseHelper.close(sPst);
							}
							try {
								sPst = conn.prepareStatement(
										"SELECT * FROM TJN_EXEC_OUTPUT WHERE RUN_ID = ? AND TSTEP_REC_ID = ? AND TDUID = ?");
								sPst.setInt(1, runId);
								sPst.setInt(2, t.getRecordId());
								sPst.setString(3, tdUid);
								sRs = sPst.executeQuery();
								while (sRs.next()) {
									RuntimeFieldValue s = new RuntimeFieldValue();
									s.setRunId(runId);
									s.setTdGid(sRs.getString("TDGID"));
									s.setTdUid(sRs.getString("TDUID"));
									s.setTestStepRecordId(sRs.getInt("TSTEP_REC_ID"));
									s.setDetailRecordNo(sRs.getInt("DETAIL_NO"));
									s.setField(sRs.getString("FIELD_NAME"));
									s.setValue(sRs.getString("FIELD_VALUE"));
									runtimeValues.add(s);
								} 
								
							} catch (Exception e) {
								
								logger.error(e.getMessage());
							}finally{
								DatabaseHelper.close(sRs);
								DatabaseHelper.close(sPst);
							}
							

							r.setRuntimeFieldValues(runtimeValues);
							/*************************************************************************************
							 * Added by Sriram for Requirement TJN_22_07 (Screentshots for information/error message encountered during execution)
							 * AND
							 * Requirement TJN_22_10 (Multiple Outputs should be captured which can be used as an input to the next test step)
							 * 25-09-2015 Starts
							 */

							/************************************************
							 * Sriram for TJN_24_11
							 */
							
							
							results.add(r);
						}
					}catch(SQLException e){
						logger.error("Get Detailed Step result from RUNRESULT_TS_TXN --> Failed",e);
						continue;
					}finally{
						DatabaseHelper.close(oRs);
						DatabaseHelper.close(rPst);
					}

					t.setDetailedResults(results);
					steps.add(t);
				}
			} catch (SQLException e) {
				
				logger.error("Fetch steps for Test Case " + tc.getTcId() + " from TESTSTEPS and RUNRESULTS_TS --> Failed",e);
				throw new DatabaseException("Could not get Test Run Info",e);
			}finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}

			tc.setTcSteps(steps);
			newTCs.add(tc);
		}

		ts.setTests(null);
		ts.setTests(newTCs);

		run.setTestSet(null);
		run.setTestSet(ts);

		
		DatabaseHelper.close(conn);
		return run;

	}



	/*Added by Preeti for TENJINCG-729 starts*/
	public LinkedHashSet<RuntimeScreenshot> getScreenshot(int runId, int testStepRecordId, int iteration) throws DatabaseException{

		
		//Optimized for TENJINCG-876 (Sriram)
		LinkedHashSet<RuntimeScreenshot> screenshotArray =new LinkedHashSet<RuntimeScreenshot>();
		
		String query = "SELECT * FROM TJN_EXEC_SCRNSHOT WHERE RUN_ID = ? AND TSTEP_REC_ID = ? and ITERATION_no=? order by SEQ_NO";
		
		try(
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement(query);
			) {
			
			pst.setInt(1, runId);
			pst.setInt(2, testStepRecordId);
			pst.setInt(3, iteration);
			byte[] barr = null;
			try(ResultSet rs = pst.executeQuery()) {
				while(rs.next()){
					
					RuntimeScreenshot screenshot=new RuntimeScreenshot();
					Blob b1 = rs.getBlob("SCRNSHOT");
					if(b1 != null){
						barr = b1.getBytes(1,(int)b1.length());
						screenshot.setScreenshotByteArray(barr);
					}
					screenshot.setIteration(rs.getInt("ITERATION_NO"));
					screenshot.setSequence(rs.getInt("SEQ_NO"));
					screenshot.setRunId(rs.getInt("RUN_ID"));
					screenshot.setTdUid(rs.getString("TDUID"));
					screenshot.setDescription(rs.getString("SCR_DESC"));
			
					screenshotArray.add(screenshot);
				}
			}
			
		} catch (SQLException e) {
			logger.error("ERROR getting screenshot for run {}, step record id {}, iteration {}", runId, testStepRecordId, iteration,e);
			throw new DatabaseException("Could not get screenshots for this run due to an internal error.", e);
		}
		
		return screenshotArray;
		//Optimized for TENJINCG-876 (Sriram) ends
	}
	/*Added by Preeti for TENJINCG-729 ends*/	
	
	/**
	 *Changes done By Parveen and LeelaPrasad For the Requirement TJN_23_11 ends on  27/09/2016-
	 */	

	

	

	

	

	

	

	public Project fetchDttInstancePrj(int prjId, int appId)
			throws DatabaseException {
		Project project = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		try {
			/*modified by shruthi for VAPT helper fixes starts*/
			pstmt = conn.prepareStatement("select * from tjn_prj_auts where prj_id=? and app_id=?");
			pstmt.setInt(1, prjId);
			pstmt.setInt(2, appId);
			/*modified by shruthi for VAPT helper fixes ends*/
			rs = pstmt.executeQuery();
			while (rs.next()) {
				project = new Project();
				project.setInstance(/* this.fetchInstanceForTool( */rs
						.getString("PRJ_DEF_MANAGER")/* ) */);
				project.setDefProject(rs.getString("PRJ_DM_PROJECT"));
				/*Added by Pushpa for TENJINCG-1221 starts*/
				project.setSubSet(rs.getString("PROJECT_SET_NAME"));
				/*Added by Pushpa for TENJINCG-1221 ends*/
				project.setDttSeverities(this.fetchSeverities(conn, project.getInstance()));
				project.setDttPriorities(this.fetchPriorities(conn, project.getInstance()));
			}
		} catch (Exception e) {
			logger.error("ERROR fetching Defet Management instannce details for project {}, app {}", prjId, appId);
			/*changed by Ashiki for TENJINCG-985 starts*/
			throw new DatabaseException("Could not fetch Defcet Mangement Instance Information. Please contact Tenjin Support");
			/*changed by Ashiki for TENJINCG-985 ends*/
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
		}
		/*Added by Pushpa for TENJINCG-1221 starts*/
		String projectDM=""; 
		try {
			pstmt = conn.prepareStatement("select * from TJN_DTT_PROJECTS where PROJECT_SET_NAME=?");
			pstmt.setString(1,project.getSubSet());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				projectDM=rs.getString("PROJECTS");
				project.setDttInstance(rs.getString("INSTANCE"));
			}
			String[] prjList=projectDM.split(",");
			for(String str:prjList){
			 String[] temp = str.split(":");
				if(temp[0].equalsIgnoreCase(project.getDefProject())){
					try{
					project.setRepoOwner(temp[2]);
					}catch(Exception e){
						continue;
					}
					break;
				}
					
			}
			
		} catch (Exception e) {
			logger.error("ERROR fetching Defet Management instannce details for project {}, app {}", prjId, appId);
			
			throw new DatabaseException("Could not fetch Information. Please contact Tenjin Support");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);
		}
		/*Added by Pushpa for TENJINCG-1221 ends*/
		
		return project;
	}

	

	public List<Defect> fetchSeverities(String instance)
			throws DatabaseException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Defect> sev = new ArrayList<Defect>();
		conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		try {
			/*modified by shruthi for VAPT helper fixes starts*/
			pstmt = conn
					.prepareStatement("select distinct * from tjn_dtt_properties where instance_name=? and property_name='Severity'");
			pstmt.setString(1, instance);
			/*modified by shruthi for VAPT helper fixes ends*/
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Defect severity = new Defect();
				severity.setSeverity(rs.getString("PROPERTY_VALUE"));
				sev.add(severity);
			}
		} catch (SQLException e) {

		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);
		}
		return sev;
	}


	public List<String> fetchSeverities(Connection conn, String instance) throws DatabaseException{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<String> sev = new ArrayList<String>();
		try {
			/*modified by shruthi for VAPT helper fixes starts*/
			pstmt = conn
					.prepareStatement("select distinct * from tjn_dtt_properties where instance_name=? and property_name='Severity' order by PROPERTY_SCORE desc");
			pstmt.setString(1, instance);
			/*modified by shruthi for VAPT helper fixes ends*/
			rs = pstmt.executeQuery();
			while (rs.next()) {
				sev.add(rs.getString("PROPERTY_VALUE"));
			}

			
		} catch (SQLException e) {
			logger.error("ERROR fetching severities for instance [{}]", instance, e);
			throw new DatabaseException("Could not fetch Severities for [" + instance + "]");
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
		}

		return sev;
	}

	public List<String> fetchPriorities(Connection conn, String instance) throws DatabaseException{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<String> sev = new ArrayList<String>();
		try {
			/*modified by shruthi for VAPT helper fixes starts*/
			pstmt = conn
					.prepareStatement("select distinct * from tjn_dtt_properties where instance_name=? and property_name='Priority' order by PROPERTY_SCORE desc");
			pstmt.setString(1, instance);
			/*modified by shruthi for VAPT helper fixes ends*/
			rs = pstmt.executeQuery();
			while (rs.next()) {
				sev.add(rs.getString("PROPERTY_VALUE"));
			}

		} catch (SQLException e) {
			logger.error("ERROR fetching severities for instance [{}]", instance, e);
			throw new DatabaseException("Could not fetch Severities for [" + instance + "]");
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
		}

		return sev;
	}

	public List<Defect> fetchPriorities(String instance)
			throws DatabaseException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Defect> sev = new ArrayList<Defect>();
		conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		try {
			/*modified by shruthi for VAPT helper fixes starts*/
			pstmt = conn
					.prepareStatement("select distinct * from tjn_dtt_properties where instance_name=? and property_name='Priority'");
			pstmt.setString(1, instance);
			/*modified by shruthi for VAPT helper fixes ends*/
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Defect severity = new Defect();
				severity.setPriority(rs.getString("PROPERTY_VALUE"));
				sev.add(severity);
			}
		} catch (SQLException e) {

			logger.error("Error ", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);
		}
		return sev;
	}

	

	public List<TestStep> fetchApplicationFunctions(int appId)
			throws DatabaseException {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<TestStep> functions = new ArrayList<TestStep>();
		conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		try {
			/*modified by shruthi for VAPT helper fixes starts*/
			pstmt = conn
					.prepareStatement("select * from MASMODULE where app_id=?");
			pstmt.setInt(1,appId);
			/*modified by shruthi for VAPT helper fixes ends*/
		    rs = pstmt.executeQuery();
			while (rs.next()) {
				TestStep tstep = new TestStep();
				tstep.setId(rs.getString(2));
				functions.add(tstep);
			}
		} catch (SQLException e) {

		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);
		}
		return functions;

	}

	

	/****************************************************
	 * Manish - TO show auto-generated defects - (21-11-2016) starts ends
	 */

	/***************
	 * Sriram - To get only basic Run Information  for all runs in a project
	 */

	public List<TestRun> hydrateBasicRunInformation(int projectId, String taskType) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		PreparedStatement pst = null;
		ResultSet rs = null;

		List<TestRun> runs = new ArrayList<TestRun>();

		try {
			if(BridgeProcess.EXECUTE .equalsIgnoreCase(taskType) || BridgeProcess.APIEXECUTE.equalsIgnoreCase(taskType)){
				pst = conn.prepareStatement("select A.*, B.TS_NAME FROM MASTESTRUNS A, TESTSETS B WHERE B.TS_REC_ID =A.RUN_TS_REC_ID AND A.RUN_PRJ_ID=? AND A.RUN_TASK_TYPE IN (?,?) ORDER BY A.RUN_ID DESC");
				pst.setString(2, BridgeProcess.APIEXECUTE);
				pst.setString(3, BridgeProcess.EXECUTE);
			}else{
				pst = conn.prepareStatement("select A.*, B.TS_NAME FROM MASTESTRUNS A, TESTSETS B WHERE B.TS_REC_ID =A.RUN_TS_REC_ID AND A.RUN_PRJ_ID=? AND A.RUN_TASK_TYPE=? ORDER BY A.RUN_ID DESC");
				pst.setString(2, taskType);
			}
			pst.setInt(1, projectId);

			rs = pst.executeQuery();

			while(rs.next()){
				TestRun run =new TestRun();
				run.setId(rs.getInt("RUN_ID"));
				run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
				run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
				run.setUser(rs.getString("RUN_USER"));
				run.setTestSetRecordId(rs.getInt("RUN_TS_REC_ID"));
				run.setStatus(rs.getString("RUN_STATUS"));
				TestSet set = new TestSet();
				set.setId(run.getTestSetRecordId());
				set.setName(rs.getString("TS_NAME"));
				run.setTestSet(set);
				runs.add(run);

			}

			
		} catch (SQLException e) {
			
			logger.error("ERROR hydrating all runs in project", e);
			throw new DatabaseException("Could not fetch available Test Runs. Please contact Tenjin Support.");
		} finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return runs;
	}

	


	
	public String hydrateAPIRequestResponse(int runId, int testStepRecordId, String tduid, String entityType) throws DatabaseException {
		String query = "SELECT ";
		if("request".equalsIgnoreCase(entityType)) {
			query += "TSTEP_WS_REQ_MSG ";
		}else{
			query += "TSTEP_WS_RES_MSG ";
		}

		query+= "FROM RUNRESULT_TS_TXN WHERE RUN_ID=? AND TSTEP_REC_ID=? AND TSTEP_TDUID=?";
		String retValue = "";
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement(query)
			) {
			pst.setInt(1, runId);
			pst.setInt(2, testStepRecordId);
			pst.setString(3, tduid);
			
			try (ResultSet rs = pst.executeQuery()) {
				rs.next();
				retValue = rs.getString(1);
			}
		} catch(SQLException e) {
			logger.error("ERROR getting API {} for run {}, test step {} and tduid {}", entityType, runId, testStepRecordId, tduid,e );
		}
		
		
		return retValue;
	}//Method Optimized for TENJINCG-876 (Sriram) ends 

	/* Added by Sriram for TENJINCG-172 */
	public List<StepIterationResult> hydrateAllTransactionResults(int runId) throws DatabaseException {
		List<StepIterationResult> results = new ArrayList<StepIterationResult> ();
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			pst = conn.prepareStatement("SELECT * from RUNRESULT_TS_TXN WHERE RUN_ID=? ORDER BY TXN_NO");
			pst.setInt(1, runId);
			rs = pst.executeQuery();
			
			while(rs.next()) {
				StepIterationResult r = new StepIterationResult();
				r.setIterationNo(rs.getInt("TXN_NO"));
				
				String tdUid = rs.getString("TSTEP_TDUID");
				r.setTduid(tdUid);
				r.setResult(rs.getString("TSTEP_RESULT"));
				r.setMessage(rs.getString("TSTEP_MESSAGE"));
				Blob b = rs.getBlob("TSTEP_SCRSHOT");
				if(b != null){
					byte barr[] = b.getBytes(1,(int)b.length());
					r.setScreenshotByteArray(barr);
				}else{
					r.setScreenshotByteArray(null);
				}
				 /*Added by Padmavathi for TENJINCG-726 starts*/
				r.setStartTimeStamp(rs.getTimestamp("EXEC_START_TIME"));
				r.setEndTimeStamp(rs.getTimestamp("EXEC_END_TIME"));
				if(r.getEndTimeStamp() != null && r.getStartTimeStamp() != null){
					String elapsedTime= Utilities.calculateElapsedTime(r.getStartTimeStamp().getTime(), r.getEndTimeStamp().getTime());
					r.setElapsedTime(elapsedTime);
				}
				 /*Added by Padmavathi for TENJINCG-726 ends*/
				r.setWsReqMessage(rs.getString("TSTEP_WS_REQ_MSG"));
				r.setWsResMessage(rs.getString("TSTEP_WS_RES_MSG"));
				results.add(r);
				
			}
			
			
		} catch(Exception e) {
			logger.error("ERROR while fetching transaction results for run {}", runId, e);
			throw new DatabaseException("Could not fetch results. Please contact Tenjin Support.");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return results;

	}
	/* Added by Sriram for TENJINCG-172 ends*/
	
	
	/*Added by Padmavathi for TENJINCG-647 starts*/
	public Map<String, Object> hydrateTestSetDetailsForRerun(int projectId, int testSetRecordId, int parentRunId, String status) throws DatabaseException {
		
	Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		TestSet ts = null;
		PreparedStatement pst =null;
		ResultSet rs=null;
		/*Modified by Padmavathi to display testcases in order  in test selection page starts*/
		PreparedStatement pst2 =null;
		ResultSet rTc = null;
			/*Modified by Padmavathi to display testcases in order  in test selection page ends*/
		//Map that contains all the failed steps. Key is teh Test Case Record ID, and value is the test set record ID.
		Multimap<Integer, Integer> reRunStepMap = ArrayListMultimap.create();
		Map<Integer, String> statusMap = new HashMap<Integer,String>(); 
		//This map contains test set record ID as key and its status as its value (for display purposes).
		
		//This map contains test set and passed step status map.
		Map<String, Object> map = new HashMap<String, Object>();
		//This map contains step record ID as key and its status as its value.
		Map<Integer, String> passedStepMap = new HashMap<Integer,String>();
		try {
			
			//First get all the failed or error steps for the run based on the status filter, and add them to reRunStepMap.
			String query = "";
				query = "SELECT * FROM V_RUNRESULTS_TS WHERE RUN_ID=?";
			
			pst = conn.prepareStatement(query);
			pst.setInt(1, parentRunId);
			/*pst.setString(2, status);*/
			
			rs = pst.executeQuery();
			while(rs.next()) {
				int tcRecId = rs.getInt("TC_REC_ID");
				int tStepRecId1 = rs.getInt("TSTEP_REC_ID");
				Integer tStepRecId=new Integer(tStepRecId1);
				String stepStatus=rs.getString("TSTEP_STATUS");
				
				if(stepStatus.equalsIgnoreCase("Pass"))
					passedStepMap.put(tStepRecId,stepStatus);
					statusMap.put(tStepRecId, stepStatus);
					reRunStepMap.put(tcRecId, tStepRecId);
			}
				
		
			ArrayList<TestCase> testCasesToRun = new ArrayList<TestCase>();
			
			//Now iterate through the map
			TestCaseHelper tcHelper = new TestCaseHelper();
			/*Modified by Padmavathi to display testcases in order  in test selection page starts*/
			
			pst2 = conn.prepareStatement("SELECT TSM_TC_REC_ID FROM TESTSETMAP WHERE TSM_PRJ_ID = ? AND TSM_TS_REC_ID = ? ORDER BY TSM_TC_SEQ");
			pst2.setInt(1, projectId);
			pst2.setInt(2,testSetRecordId);
			rTc = pst2.executeQuery();
			while(rTc.next()){
				int tcId = rTc.getInt("TSM_TC_REC_ID");
					if(reRunStepMap.containsKey(tcId)){
				/* modified by Roshni for TENJINCG-1168 starts */		
						
						TestCase tc = tcHelper.hydrateTestCaseBasicDetails(conn, projectId, tcId,passedStepMap);
				/* modified by Roshni for TENJINCG-1168 ends */
					//The hydrated TC will have all the test steps. Now we have to remove the steps that are not to be executed in the re-run
				Collection<Integer> stepRecordIds = reRunStepMap.get(tcId);
				/*Modified by Padmavathi to display testcases in order  in test selection page ends*/
				Iterator<Integer> stepRecordIdIter = stepRecordIds.iterator();
				ArrayList<TestStep> stepsToRun = new ArrayList<TestStep>();
				/*Added by Ashiki for TENJINCG-746 Starts*/
				List<String> statuses=new ArrayList<String>();
				/*Added by Ashiki for TENJINCG-746 Ends*/
				while(stepRecordIdIter.hasNext()) {
					int stepRecordId = stepRecordIdIter.next();
					
					//Get the TestStep object from the hydrated test case and add it to stepsToRun
					for(TestStep step : tc.getTcSteps()) {
						if(stepRecordId == step.getRecordId()) {
							step.setStatus(statusMap.get(step.getRecordId()));
							step.setLatestDependentStepStatus(passedStepMap.get(step.getDependentStepRecId()));
							stepsToRun.add(step);
							/*Added by Ashiki for TENJINCG-746 Starts*/
							statuses.add(step.getStatus());
							/*Added by Ashiki for TENJINCG-746 Ends*/
							break;
						}
					}
				}
				//Once done, replace the hydrated test case's steps to the stepsToRun
				tc.setTcSteps(null);
				tc.setTcSteps(stepsToRun);
				/*Added by Ashiki for TENJINCG-746 Starts*/
				tc.setTcStatus(new IterationHelper().processExecStepStatus(statuses));
				/*Added by Ashiki for TENJINCG-746 Ends*/
				//Add teh test case to testCasesToRun
				testCasesToRun.add(tc);
				/*Added by Padmavathi to display testcases in order  in test selection page  starts*/
					}
						/*Added by Padmavathi to display testcases in order  in test selection page  ends*/
				}
					
				/*Added by Padmavathi to display testcases in order  in test selection page  starts*/
				/*}*/
				/*Added by Padmavathi to display testcases in order  in test selection page  ends*/
			
			//Now, if testCasesToRun is not empty, then hydrate basic details about the test set
			pst = conn.prepareStatement("SELECT * FROM TESTSETS WHERE TS_REC_ID = ? AND TS_PRJ_ID = ?");
			pst.setInt(1, testSetRecordId);
			pst.setInt(2, projectId);
			
			rs = pst.executeQuery();
			
			while(rs.next()) {
				ts = new TestSet();
				ts.setName(rs.getString("TS_NAME"));
				ts.setRecordType(rs.getString("TS_REC_TYPE"));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_TYPE"));
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				ts.setStatus(rs.getString("TS_STATUS"));
				ts.setDescription(rs.getString("TS_DESC"));
				ts.setMode(rs.getString("TS_EXEC_MODE"));
			}
			
			if(ts != null) {
				//set the prepared testCasesToRun to the test set.
				ts.setTests(testCasesToRun);
			}
		}
		 catch (SQLException e) {
				
				logger.error("ERROR while fetching test set for re-run. Please contact Tenjin support.", e);
				throw new DatabaseException("Could not fetch information on failed tests due to an internal error. Please contact Tenjin Support.");
			}finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(rTc);
				DatabaseHelper.close(pst2);
				DatabaseHelper.close(conn);
			}
			map.put("TestSet", ts);
			map.put("PassedStepMap",passedStepMap);
			return map;
		
		
		
		
	}
	/*Added by Padmavathi for TENJINCG-647 starts*/
	public TestSet hydrateTestSetForRerun(int projectId, int testSetRecordId, int parentRunId, String status) throws DatabaseException {
		
Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		TestSet ts = null;
		PreparedStatement pst =null;
		ResultSet rs=null;
		//Map that contains all the failed steps. Key is teh Test Case Record ID, and value is the test set record ID.
		Multimap<Integer, Integer> reRunStepMap = ArrayListMultimap.create();
		Map<Integer, String> statusMap = new HashMap<Integer,String>(); 
		//This map contains test set record ID as key and its status as its value (for display purposes).
		try {
			
			//First get all the failed or error steps for the run based on the status filter, and add them to reRunStepMap.
			String query = "";
			if(Utilities.trim(status).length() > 0) {
				query = "SELECT * FROM V_RUNRESULTS_TS WHERE RUN_ID=? AND TSTEP_STATUS=?";
			}else{
				query = "SELECT * FROM V_RUNRESULTS_TS WHERE RUN_ID=? AND TSTEP_STATUS NOT IN (?)";
				status = "Pass";
			}
			pst = conn.prepareStatement(query);
			pst.setInt(1, parentRunId);
			pst.setString(2, status);
			
			rs = pst.executeQuery();
			while(rs.next()) {
				int tcRecId = rs.getInt("TC_REC_ID");
				int tStepRecId1 = rs.getInt("TSTEP_REC_ID");
				Integer tStepRecId=new Integer(tStepRecId1);
				statusMap.put(tStepRecId, rs.getString("TSTEP_STATUS"));
				reRunStepMap.put(tcRecId, tStepRecId);
				
			}
			
			
			
			ArrayList<TestCase> testCasesToRun = new ArrayList<TestCase>();
			
			//Now iterate through the map
			TestCaseHelper tcHelper = new TestCaseHelper();
			for(int tcRecId : reRunStepMap.keySet()) {
				//Hydrate the test case basic info
			
				TestCase tc = tcHelper.hydrateTestCaseBasicDetails(conn, projectId, tcRecId,null);
				/* modified by Roshni for TENJINCG-1168 ends */
				//The hydrated TC will have all the test steps. Now we have to remove the steps that are not to be executed in the re-run
				Collection<Integer> stepRecordIds = reRunStepMap.get(tcRecId);
				Iterator<Integer> stepRecordIdIter = stepRecordIds.iterator();
				ArrayList<TestStep> stepsToRun = new ArrayList<TestStep>();
				/*Added by Ashiki for TENJINCG-746 Starts*/
				List<String> statuses=new ArrayList<String>();
				/*Added by Ashiki for TENJINCG-746 Ends*/
				while(stepRecordIdIter.hasNext()) {
					int stepRecordId = stepRecordIdIter.next();
					
					//Get the TestStep object from the hydrated test case and add it to stepsToRun
					for(TestStep step : tc.getTcSteps()) {
						if(stepRecordId == step.getRecordId()) {
							step.setStatus(statusMap.get(step.getRecordId()));
							stepsToRun.add(step);
							/*Added by Ashiki for TENJINCG-746 Starts*/
							statuses.add(step.getStatus());
							/*Added by Ashiki for TENJINCG-746 Ends*/
							break;
						}
					}
				}
				//Once done, replace the hydrated test case's steps to the stepsToRun
				tc.setTcSteps(null);
				tc.setTcSteps(stepsToRun);
				/*Added by Ashiki for TENJINCG-746 Starts*/
				tc.setTcStatus(new IterationHelper().processExecStepStatus(statuses));
				/*Added by Ashiki for TENJINCG-746 Ends*/
				
				//Add teh test case to testCasesToRun
				testCasesToRun.add(tc);
			}
			
			//Now, if testCasesToRun is not empty, then hydrate basic details about the test set
			pst = conn.prepareStatement("SELECT * FROM TESTSETS WHERE TS_REC_ID = ? AND TS_PRJ_ID = ?");
			pst.setInt(1, testSetRecordId);
			pst.setInt(2, projectId);
			
			rs = pst.executeQuery();
			
			while(rs.next()) {
				ts = new TestSet();
				ts.setName(rs.getString("TS_NAME"));
				ts.setRecordType(rs.getString("TS_REC_TYPE"));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_TYPE"));
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				ts.setStatus(rs.getString("TS_STATUS"));
				ts.setDescription(rs.getString("TS_DESC"));
				ts.setMode(rs.getString("TS_EXEC_MODE"));
			}
			
			if(ts != null) {
				//set the prepared testCasesToRun to the test set.
				ts.setTests(testCasesToRun);
			}
		}
		 catch (SQLException e) {
				
				logger.error("ERROR while fetching test set for re-run. Please contact Tenjin support.", e);
				throw new DatabaseException("Could not fetch information on failed tests due to an internal error. Please contact Tenjin Support.");
			}finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
			
			return ts;
		
		
		
		
	}
	
	/*Added by Preeti for TENJINCG-574 starts*/
	public TestSet hydrateTestSetDetailsForRerun(int projectId, int testSetRecordId) throws DatabaseException {
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				
				TestSet ts = null;
				PreparedStatement pst =null;
				ResultSet rs=null;
				try {
					pst = conn.prepareStatement("SELECT * FROM TESTSETS WHERE TS_REC_ID = ? AND TS_PRJ_ID = ?");
					pst.setInt(1, testSetRecordId);
					pst.setInt(2, projectId);
					rs = pst.executeQuery();
					
					while(rs.next()) {
						ts = new TestSet();
						ts.setName(rs.getString("TS_NAME"));
						ts.setRecordType(rs.getString("TS_REC_TYPE"));
						ts.setId(rs.getInt("TS_REC_ID"));
						ts.setType(rs.getString("TS_TYPE"));
						ts.setParent(rs.getInt("TS_FOLDER_ID"));
						ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
						ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
						ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
						ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
						ts.setStatus(rs.getString("TS_STATUS"));
						ts.setDescription(rs.getString("TS_DESC"));
						ts.setMode(rs.getString("TS_EXEC_MODE"));
					}
				}
				 catch (SQLException e) {
						
						logger.error("ERROR while fetching test set for re-run. Please contact Tenjin support.", e);
						throw new DatabaseException("Could not fetch information on failed tests due to an internal error. Please contact Tenjin Support.");
					}finally{
						DatabaseHelper.close(rs);
						DatabaseHelper.close(pst);
						DatabaseHelper.close(conn);
					}
					
					return ts;
				
			}
	/*Added by Preeti for TENJINCG-574 ends*/
	
	 public int getParent(int runId)throws DatabaseException
		{
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			int parentId=0;
		
			PreparedStatement pst=null;
			ResultSet rs1=null;
			try {
				do{
					/*modified by shruthi for sql injection starts*/
					String query = "SELECT PARENT_RUN_ID FROM MASTESTRUNS WHERE RUN_ID=?";
					pst = conn.prepareStatement(query);
					pst.setInt(1, runId);
					/*modified by shruthi for sql injection ends*/
					rs1=pst.executeQuery();
					if(rs1.next())
					{
						parentId=rs1.getInt("PARENT_RUN_ID");
							 
					}
					
					
					if(parentId>0)
						runId=parentId;
					
				}
				while(parentId>0);
			
			   }catch(SQLException e){
				
				throw new DatabaseException("Could not merge failures. Please contact Tenjin support.");
			}finally{
				
				DatabaseHelper.close(rs1);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
						return runId;
		}
		public List<Integer> getChildRunIDs(int runId)throws DatabaseException
		{
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			int parentId=0;
			
			PreparedStatement pst=null;
			ResultSet rs1=null;
			List<Integer> childList=new ArrayList<Integer>();
		    try {
				boolean flag=true;
				while(flag!=false)
				{
					/*modified by shruthi for sql injection starts*/
					/*Modified by Ashiki for Tenj212-19 start*/
					//String query = "SELECT RUN_ID FROM MASTESTRUNS WHERE PARENT_RUN_ID=?";
					pst = conn.prepareStatement("SELECT RUN_ID FROM MASTESTRUNS WHERE PARENT_RUN_ID=?");
					pst.setInt(1, runId);
					/*modified by shruthi for sql injection ends*/
					/*Modified by Ashiki for Tenj212-19 ends*/
					rs1=pst.executeQuery();
					if(rs1.next())
					{
							 parentId=rs1.getInt("RUN_ID");
							 childList.add(parentId);
							 runId=parentId;
							 }	
					else
						flag=false;
			
				}
				
			}catch(SQLException e){
				
				throw new DatabaseException("Could not merge failures. Please contact Tenjin support.");
			}finally{
				DatabaseHelper.close(rs1);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
			
			return childList;
		}
		 public  List<Integer> getAllParentIds( )throws DatabaseException
			{
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				int parentId=0;
			
				PreparedStatement pst=null;
				ResultSet rs1=null;
				List<Integer> list=new ArrayList<Integer>();
				try { 
					
					String query = "SELECT PARENT_RUN_ID FROM MASTESTRUNS WHERE PARENT_RUN_ID IS NOT NULL";
						pst = conn.prepareStatement(query);
						rs1=pst.executeQuery();
						while(rs1.next())
						{
							parentId=rs1.getInt("PARENT_RUN_ID");
							list.add(parentId);
								 
						}
						
						
						
						}catch(SQLException e){
					
					throw new DatabaseException("Could not merge failures. Please contact Tenjin support.");
				}finally{
					DatabaseHelper.close(rs1);
					DatabaseHelper.close(pst);
					DatabaseHelper.close(conn);
				}
							return list;
			}
			/*Added by Roshni for TENJINCG-259 ends*/
		 
		 /*Added by Padmavathi for TENJINCG-726 starts*/
			
		 //Method changed by Sriram for TENJINCG-1196
		 public TestRun hydrateBasicRunInformation(int runId) throws DatabaseException {
			 try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ)) {
				 return hydrateBasicRunInformation(conn, runId);
			 } catch(SQLException e) {
				 throw new DatabaseException("Could not connect to database", e);
			 }
		 }
		 public TestRun hydrateBasicRunInformation(Connection conn, int runId) throws DatabaseException{
			 
			 	try (
			 			PreparedStatement pst = conn.prepareStatement("Select A.*, B.TS_NAME from MasTestRuns A, TestSets B where b.TS_REC_ID=a.RUN_TS_REC_ID and A.RUN_ID=?");
			 		){
			 		
			 		pst.setInt(1, runId);
			 		
			 		TestRun run = null;
			 		try(ResultSet rs = pst.executeQuery()) {
			 			while(rs.next()) {
			 				run = new TestRun();
				 			run.setId(rs.getInt("RUN_ID"));
				 			run.setCompletionStatus(rs.getString("RUN_STATUS"));
				 			run.setStatus(run.getCompletionStatus());
				 			run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
				 			run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
				 			run.setUser(rs.getString("RUN_USER"));
				 			run.setMachine_ip(rs.getString("RUN_MACHINE_IP"));
				 			run.setTargetPort(rs.getInt("RUN_CL_PORT"));
				 			run.setBrowser_type(rs.getString("RUN_BROWSER_TYPE"));
				 			run.setDeviceRecId(rs.getInt("RUN_DEVICE_REC_ID"));
				 			run.setDeviceFarmFlag(rs.getString("RUN_DEVICE_FARM"));
				 			run.setTestSetRecordId(rs.getInt("RUN_TS_REC_ID"));
				 			run.setTestSetName(rs.getString("TS_NAME"));
			 			}
			 		}
			 		
			 		return run;
			 		
			 	} catch (Exception e) {
			 		logger.error("Error fetching basic run information", e);
			 		throw new DatabaseException("Could not fetch run information", e);
			 	}
			}
		//Method changed by Sriram for TENJINCG-1196 ends
		 public String getElapsedTimeForOveralRun(int runId) throws DatabaseException{
			 /*added by shruthi for TENJINCG-1226 starts*/
			 List<String> elapsedTimeList = new ArrayList<String>();
			 ResultsHandler handler = new ResultsHandler();
			 TestRun run = null;
			 run = handler.fetchRunSummary(runId);
			 List<Integer> parentRunIDs = run.getAllParentRunIds();
			 if(parentRunIDs!=null) {
			 if(parentRunIDs.size()>0) {
			 for(Integer parentRunID:parentRunIDs) {
			            TestRun parentRunSummary = handler.fetchRunSummary(parentRunID);
			            if(parentRunSummary.getElapsedTime()==null) {
			            	String ElapsedTime ="00:00:00";

			            	elapsedTimeList.add(ElapsedTime);
			            	}else {
			            	elapsedTimeList.add(parentRunSummary.getElapsedTime());
			            	}
			 		}
			 elapsedTimeList.add(run.getElapsedTime());
			 }
			 }
			 else {
			 List<StepIterationResult> stepResults=this.hydrateAllTransactionResults(runId);
		 		for(StepIterationResult stepResult:stepResults){
		 			if(stepResult.getElapsedTime()!=null)
		 				elapsedTimeList.add(stepResult.getElapsedTime());
		 			
		 		}
	}
			 return Utilities.addElapsedTimes(elapsedTimeList);
}
		/* added by shruthi for TENJINCG-1226 ends*/
		 /*Added by Padmavathi for TENJINCG-726 ends*/
		 
		 
		 //TENJINCG-870 (Sriram)
		 public TestRun hydrateRunSummary(int runId, boolean includeTestCaseSummary) throws DatabaseException {
			 try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ)) {
				 return this.hydrateRunSummary(conn, runId, includeTestCaseSummary);
			 } catch (SQLException e) {
				throw new DatabaseException("Could not connect to database", e);
			}
		 }
		 
		 //TENJINCG-1196 (Sriram)
		 
		 //TENJINCG-1196 (Sriram) ends
		 
		 public TestRun hydrateRunSummary(Connection conn, int runId, boolean includeTestCaseSummary) throws DatabaseException {
			 TestRun run = null;
			 
			 //Fetch basic run details
			 /*Modified by sahana for pCloudy: Starts*/
			 /*String query = "SELECT A.RUN_ID, A.RUN_PRJ_ID, A.RUN_TASK_TYPE, A.RUN_STATUS AS RUN_COMPLETION_STATUS, A.RUN_START_TIME,A.RUN_END_TIME,A.RUN_USER,"
				 		+ "A.RUN_TS_REC_ID,A.RUN_MACHINE_IP,A.RUN_CL_PORT,A.RUN_BROWSER_TYPE,A.RUN_PRJ_NAME,A.PARENT_RUN_ID,A.RUN_DOMAIN_NAME,A.RUN_DEVICE_REC_ID, "
				 		+ "B.TOTAL_TCS,B.TOTAL_EXE_TCS,B.TOTAL_PASSED_TCS,B.TOTAL_FAILED_TCS,B.TOTAL_ERROR_TCS,B.RUN_STATUS,C.TS_NAME FROM "
				 		+ "MASTESTRUNS A, V_RUNRESULTS B, TESTSETS C WHERE C.TS_REC_ID=A.RUN_TS_REC_ID AND B.RUN_ID = A.RUN_ID AND A.RUN_ID = ?";
			  */
			 String query = "SELECT A.RUN_ID, A.RUN_PRJ_ID, A.RUN_TASK_TYPE, A.RUN_STATUS AS RUN_COMPLETION_STATUS, A.RUN_START_TIME,A.RUN_END_TIME,A.RUN_USER,"
					 + "A.RUN_TS_REC_ID,A.RUN_MACHINE_IP,A.RUN_CL_PORT,A.RUN_BROWSER_TYPE,A.RUN_PRJ_NAME,A.PARENT_RUN_ID,A.RUN_DOMAIN_NAME,A.RUN_DEVICE_REC_ID,A.RUN_DEVICE_FARM, "
					 + "B.TOTAL_TCS,B.TOTAL_EXE_TCS,B.TOTAL_PASSED_TCS,B.TOTAL_FAILED_TCS,B.TOTAL_ERROR_TCS,B.RUN_STATUS,C.TS_NAME FROM "
					 + "MASTESTRUNS A, V_RUNRESULTS B, TESTSETS C WHERE C.TS_REC_ID=A.RUN_TS_REC_ID AND B.RUN_ID = A.RUN_ID AND A.RUN_ID = ?";
			 /*Modified by sahana for pCloudy: Ends*/
			 try {
				 
				 try(PreparedStatement pst = conn.prepareStatement(query)){
					 pst.setInt(1, runId);

					 try (ResultSet rs =pst.executeQuery()) {
						 while(rs.next()) {
							 run = this.buildTestRunObject(rs);
						 }
					 }
				 }
				 
				 String parentRunsQuery = "WITH rec_table (PARENT_RUN_ID, RUN_ID) AS ( SELECT e.parent_run_id, e.run_id FROM MASTESTRUNS e WHERE run_id = ? UNION ALL SELECT e.parent_run_id, e.run_id FROM mastestruns e INNER JOIN rec_table d ON e.run_id = d.parent_run_id ) SELECT run_id FROM rec_table";
				 String childRunsQuery = "WITH rec_table (PARENT_RUN_ID, RUN_ID) AS ( SELECT e.parent_run_id, e.run_id FROM MASTESTRUNS e WHERE parent_run_id = ? UNION ALL SELECT e.parent_run_id, e.run_id FROM mastestruns e INNER JOIN rec_table d ON e.parent_run_id = d.run_id ) SELECT run_id FROM rec_table";
				 
				 List<Integer> parentRuns = new LinkedList<>();
				 List<Integer> childRuns = new LinkedList<>();
				 
				 if(run != null && run.getParentRunId() > 0) {
					
					 
					 //Get all parent runs
					 try(PreparedStatement pst = conn.prepareStatement(parentRunsQuery)) {
						 pst.setInt(1, runId);
						 
						 try (ResultSet rs =pst.executeQuery()) {
							 while(rs.next()) {
								 int r = rs.getInt("RUN_ID");
								 if(r == runId) continue;
								 parentRuns.add(r);
							 }
						 }
					 }
					 
					 if(parentRuns.size() > 0) {
						 Collections.reverse(parentRuns);
						 run.setAllParentRunIds(parentRuns);
						 run.setPureRunId(parentRuns.get(parentRuns.size()-1));
					 }
					 
					 
					 
				 }
				 
				//Get all child runs
				 try(PreparedStatement pst = conn.prepareStatement(childRunsQuery)) {
					 pst.setInt(1, runId);
					 
					 try (ResultSet rs =pst.executeQuery()) {
						 while(rs.next()) {
							 int r = rs.getInt("RUN_ID");
							 if(r == runId) continue;
							 childRuns.add(r);
						 }
					 }
				 }
				 
				 run.setReRunIds(childRuns);
				 
				 //Stub the test set information
				 TestSet testSet = new TestSet();
				 testSet.setId(run.getTestSetRecordId());
				 testSet.setName(run.getTestSetName());
				 testSet.setTests(new ArrayList<TestCase>());
				 run.setTestSet(testSet);
				 if(includeTestCaseSummary) {
					//Fetch test case summary
					/* Added by Pushpa for TJN27-8 starts*/
					 //String testCaseSummaryQuery = "SELECT A.*, B.TC_TOTAL_STEPS, B.TC_TOTAL_EXEC, B.TC_TOTAL_PASS, B.TC_TOTAL_FAIL, B.TC_TOTAL_ERROR, B.TC_STATUS AS TC_STAT FROM TESTCASES A, V_RUNRESULTS_TC B WHERE B.TC_REC_ID = A.TC_REC_ID AND B.RUN_ID = ?";
					 String testCaseSummaryQuery = "SELECT C.TSM_TC_SEQ, A.*, B.TC_TOTAL_STEPS, B.TC_TOTAL_EXEC, B.TC_TOTAL_PASS, B.TC_TOTAL_FAIL, B.TC_TOTAL_ERROR, B.TC_STATUS AS TC_STAT "
						 		+ "FROM TESTCASES A, V_RUNRESULTS_TC B,TESTSETMAP C WHERE B.TC_REC_ID = A.TC_REC_ID AND A.TC_REC_ID = C.TSM_TC_REC_ID AND B.RUN_ID = ? AND C.TSM_TS_REC_ID=? ORDER BY  C.TSM_TC_SEQ";
					 /*Added by Pushpa for TJN27-8 ends*/
					 try(PreparedStatement pst = conn.prepareStatement(testCaseSummaryQuery)) {
						 pst.setInt(1, runId);
						 /*Added by Pushpa for TJN27-8 starts*/
						 pst.setInt(2,run.getTestSetRecordId());
						 /*Added by Pushpa for TJN27-8 ends*/
						 try(ResultSet rs = pst.executeQuery()) {
							 while(rs.next()) {
								 TestCase tc = new TestCase();
								 tc.setTcId(rs.getString("tc_id"));
								 tc.setTcName(rs.getString("tc_name"));
								 tc.setTcType(rs.getString("tc_type"));
								 tc.setTcCreatedBy(rs.getString("tc_created_by"));
								 tc.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
								 tc.setTcModifiedBy(rs.getString("tc_modified_by"));
								 tc.setTcModifiedOn(rs.getDate("tc_modified_on"));
								 tc.setTcDesc(rs.getString("tc_desc"));
								 tc.setTcRecId(rs.getInt("TC_REC_ID"));
								 tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
								 tc.setProjectId(rs.getInt("TC_PRJ_ID"));
								 tc.setRecordType(rs.getString("TC_REC_TYPE"));
								 tc.setTcReviewed(rs.getString("TC_REVIEWED"));
								 tc.setTcPriority(rs.getString("TC_PRIORITY"));
								 tc.setTcRecId(rs.getInt("TC_REC_ID"));
								 tc.setTotalSteps(rs.getInt("TC_TOTAL_STEPS"));
								 tc.setTotalExecutedSteps(rs.getInt("TC_TOTAL_EXEC"));
								 tc.setTotalPassedSteps(rs.getInt("TC_TOTAL_PASS"));
								 tc.setTotalFailedSteps(rs.getInt("TC_TOTAL_FAIL"));
								 tc.setTotalErrorredSteps(rs.getInt("TC_TOTAL_ERROR"));
								 tc.setTcStatus(rs.getString("TC_STAT"));
								 run.getTestSet().getTests().add(tc);
							 }
						 }
					 }
				 }
				 
				 
			 } catch (SQLException e) {
				logger.error("ERROR fetching run summary", e);
				throw new DatabaseException("An internal error occurred while fetching run summary");
			}
			 
			 return run;
		 }
		 
		 public TestCase hydrateTestCaseResultSummary(int runId, int testCaseRecordId) throws DatabaseException {
			 try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ)) {
				 return this.hydrateTestCaseResultSummary(conn, runId, testCaseRecordId);
			 } catch (SQLException e) {
				 throw new DatabaseException("Could not connect to database", e);
			 }
		 }
		 
		 public TestCase hydrateTestCaseResultSummary(Connection conn, int runId, int testCaseRecordId) throws DatabaseException {
			 /*Changed by Leelaprasad for the requirement of T252-85 starts*/
	
			 // Change for TENJINCG-1175 (TENJINCG-1194) - Sriram
			 String query = "SELECT C.TSTEP_EXEC_SEQ,A.*, e.TSTEP_RESULT,E.TSTEP_MESSAGE,E.EXEC_START_TIME,E.EXEC_END_TIME, B.TSTEP_STATUS, B.TSTEP_TOTAL_TXN AS TSTEP_TOTAL_TRANS, B.TSTEP_TOTAL_EXEC as TSTEP_TOTAL_TRANS_EXEC,B.TSTEP_TOTAL_PASS AS TSTEP_TOTAL_TRANS_PASS,B.TSTEP_TOTAL_FAIL AS TSTEP_TOTAL_TRANS_FAIL,B.TSTEP_TOTAL_ERROR AS TSTEP_TOTAL_TRANS_ERROR,B.TSTEP_TOTAL_EXECUTING AS TSTEP_TOTAL_TRANS_EXECUTING,B.TSTEP_TOTAL_NOT_STARTED AS TSTEP_TOTAL_TRANS_NOT_STARTED,B.TC_REC_ID,C.TSTEP_SHT_DESC,C.TSTEP_OPERATION, C.TXNMODE, C.TSTEP_VAL_TYPE,C.TSTEP_ID,D.TC_NAME,D.TC_ID,e.TSTEP_WS_RES_MSG,TSTEP_WS_REQ_MSG FROM"
				 		+ " V_RUNRESULT_TS_TXN A, V_RUNRESULTS_TS B, TESTSTEPS C, TESTCASES D, RUNRESULT_TS_TXN E WHERE"
				 		+ " E.RUN_ID=A.RUN_ID AND"
				 		+ " E.TSTEP_REC_ID=A.TSTEP_REC_ID AND"
				 		+ " D.TC_REC_ID=B.TC_REC_ID AND"
				 		+ " C.TSTEP_REC_ID=B.TSTEP_REC_ID AND"
				 		+ " B.RUN_ID=A.RUN_ID AND"
				 		+ " A.TSTEP_REC_ID=B.TSTEP_REC_ID AND"
				 		+ " A.TXN_NO=E.TXN_NO AND "
				 		+ " B.TC_REC_ID=? AND"
				 		+ " A.RUN_ID=? ORDER BY  C.TSTEP_EXEC_SEQ";
			 
			 /*Changed by Leelaprasad for the requirement of T252-85 ends*/
			 
			 String tcSummaryQuery = "SELECT A.*, B.TC_ID, B.TC_NAME FROM TESTCASES B, V_RUNRESULTS_TC A WHERE B.TC_REC_ID=A.TC_rEC_ID AND A.TC_REC_ID=? AND A.RUN_ID=?";
			 
			 TestCase tc = null;
			 /*Modified by Pushpa for TJN27-13 starts*/
			 /*Map<Integer, TestStep> stepMap = new TreeMap<>();*/
			 LinkedHashMap<Integer, TestStep> stepMap = new LinkedHashMap<>();
			 /*Modified by Pushpa for TJN27-13 ends*/
			 
			 try {
				 
				 
				 try(PreparedStatement pst = conn.prepareStatement(tcSummaryQuery)) {
					 pst.setInt(1, testCaseRecordId);
					 pst.setInt(2, runId);

					 try (ResultSet rs = pst.executeQuery()) {
						 rs.next();
						 tc = new TestCase();
						 tc.setTcId(rs.getString("tc_id"));
						 tc.setTcName(rs.getString("tc_name"));
						 tc.setTcRecId(rs.getInt("TC_REC_ID"));
						 tc.setTotalSteps(rs.getInt("TC_TOTAL_STEPS"));
						 tc.setTotalExecutedSteps(rs.getInt("TC_TOTAL_EXEC"));
						 tc.setTotalPassedSteps(rs.getInt("TC_TOTAL_PASS"));
						 tc.setTotalFailedSteps(rs.getInt("TC_TOTAL_FAIL"));
						 tc.setTotalErrorredSteps(rs.getInt("TC_TOTAL_ERROR"));
						 tc.setTcStatus(rs.getString("TC_STATUS"));
					 }

				 }
				 
				 
				 if(tc != null) {
					 try(PreparedStatement pst = conn.prepareStatement(query)) {
						 pst.setInt(1, testCaseRecordId);
						 pst.setInt(2, runId);
						 
						 try (ResultSet rs = pst.executeQuery()) {
							 while(rs.next()) {

								 
								 if(tc == null) {
									 tc = new TestCase();
									 tc.setTcRecId(testCaseRecordId);
									 tc.setTcId(rs.getString("TC_ID"));
									 tc.setTcName(rs.getString("TC_NAME"));
								 }
								 
								 TestStep step = stepMap.get(rs.getInt("TSTEP_REC_ID"));
								 
								 if(step == null) {
									 step = new TestStep();
									 step.setRecordId(rs.getInt("TSTEP_REC_ID"));
									 step.setId(rs.getString("TSTEP_ID"));
									 step.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
									 step.setStatus(rs.getString("TSTEP_STATUS"));
									 step.setOperation(rs.getString("TSTEP_OPERATION"));
									 step.setValidationType(rs.getString("TSTEP_VAL_TYPE"));
									 step.setTotalTransactions(rs.getInt("TSTEP_TOTAL_TRANS"));
									 step.setTotalPassedTxns(rs.getInt("TSTEP_TOTAL_TRANS_PASS"));
									 step.setTotalFailedTxns(rs.getInt("TSTEP_TOTAL_TRANS_FAIL"));
									 step.setTotalErroredTxns(rs.getInt("TSTEP_TOTAL_TRANS_ERROR"));
									 step.setTotalExecutedTxns(step.getTotalPassedTxns() + step.getTotalFailedTxns() + step.getTotalErroredTxns());
									 step.setTxnMode(rs.getString("TXNMODE"));
									 stepMap.put(step.getRecordId(), step);
								 }
								 
								 step = stepMap.get(rs.getInt("TSTEP_REC_ID"));
								 StepIterationResult result = this.buildIterationResultSummaryObject(rs);
								 
								 if(step.getDetailedResults() != null) {
									 step.getDetailedResults().add(result);
								 }else {
									 ArrayList<StepIterationResult> results = new ArrayList<>();
									 results.add(result);
									 step.setDetailedResults(results);
								 }
								 
								 
							 
							 }
						 }
					 }
				 }
				 
			 } catch(SQLException e) {
				 logger.error("An error occurred while fetching test case summary", e);
				 throw new DatabaseException("Could not fetch test case summary due to an internal error", e);
			 }
			 
		
			 
			 if(tc != null && stepMap != null && stepMap.keySet().size() > 0) {
				 ArrayList<TestStep> steps = new ArrayList<>();
				 for(Entry<Integer, TestStep> entry : stepMap.entrySet()) {
					 steps.add(entry.getValue());
				 }
				 
				 tc.setTcSteps(steps);
			 }
			 			 
			 return tc;
			 
		 }
		 
		 public List<RuntimeFieldValue> hydrateRunTimeValues(int runId, int testStepRecordId, String tdUid) throws DatabaseException {
			 List<RuntimeFieldValue> values = new LinkedList<>();
			 
			 try (
					 Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					 PreparedStatement pst = conn.prepareStatement("SELECT * FROM TJN_EXEC_OUTPUT WHERE RUN_ID=? AND TSTEP_REC_ID=? AND TDUID=? ORDER BY FIELD_NAME");
				) {
				 
				 pst.setInt(1, runId);
				 pst.setInt(2, testStepRecordId);
				 pst.setString(3, tdUid);
				 
				 try (ResultSet rs = pst.executeQuery()) {
					 while(rs.next()) {
						 values.add(this.buildRuntimeFieldValueObject(rs));
					 }
				 }
				 
			 } catch (SQLException e) {
				logger.error("Error fetching runtime values", e);
				throw new DatabaseException("Could not fetch runtime values", e);
			}
			 
			 return values;
		 }
		 
		 public List<ValidationResult> hydrateValidationResults(int runId, int testStepRecordId, int iteration) throws DatabaseException {
			 List<ValidationResult> results = new LinkedList<>();
			 
			 try(
					 Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					 PreparedStatement pst = conn.prepareStatement("SELECT * FROM RESVALRESULTS WHERE RUN_ID=? AND TSTEP_REC_ID=? AND ITERATION=?");
				) {
				 pst.setInt(1, runId);
				 pst.setInt(2, testStepRecordId);
				 pst.setInt(3, iteration);
				 
				 try(ResultSet rs = pst.executeQuery()) {
					 while(rs.next()) {
						 results.add(this.buildValidationResultObject(rs));
					 }
				 }
			 } catch (SQLException e) {
				 logger.error("Error fetching validation results", e);
				 throw new DatabaseException("Could not fetch validation results", e);
			 }
			 
			 return results;
		 }
		 
		 private ValidationResult buildValidationResultObject(ResultSet vRs) throws SQLException {
			 	ValidationResult res = new ValidationResult();
				res.setRunId(vRs.getInt("RUN_ID"));
				res.setResValId(vRs.getInt("RES_VAL_ID"));
				res.setTestStepRecordId(vRs.getInt("TSTEP_REC_ID"));
				res.setPage(vRs.getString("RES_VAL_PAGE"));
				res.setField(vRs.getString("RES_VAL_FIELD"));
				res.setExpectedValue(vRs.getString("RES_VAL_EXP_VAL"));
				res.setActualValue(vRs.getString("RES_VAL_ACT_VAL"));
				res.setStatus(vRs.getString("RES_VAL_STATUS"));
				res.setIteration(vRs.getInt("ITERATION"));
				res.setDetailRecordNo(vRs.getString("RES_VAL_DETAIL_ID"));
				return res;
		 }
		 
		 private RuntimeFieldValue buildRuntimeFieldValueObject(ResultSet rs ) throws SQLException {
			 RuntimeFieldValue value = new RuntimeFieldValue();
			 value.setDetailRecordNo(rs.getInt("DETAIL_NO"));
			 value.setField(rs.getString("FIELD_NAME"));
			 value.setValue(rs.getString("FIELD_VALUE"));
			 value.setIteration(rs.getInt("ITERATION_NO"));
			 value.setRunId(rs.getInt("RUN_ID"));
			 value.setTdGid(rs.getString("TDGID"));
			 value.setTdUid(rs.getString("TDUID"));
			 value.setTestStepRecordId(rs.getInt("TSTEP_REC_ID"));
			 return value;
		 }
		 
		 private StepIterationResult buildIterationResultSummaryObject(ResultSet rs) throws SQLException {
			 StepIterationResult r = new StepIterationResult();
			 
			 r.setRunId(rs.getInt("RUN_ID"));
			 r.setIterationNo(rs.getInt("TXN_NO"));
			 /*Added by khalid for TENJINCG-1194 starts*/
			 r.setTsRecId(rs.getInt("TSTEP_REC_ID"));
			 /*Added by khalid for TENJINCG-1194 ends*/
			 r.setDataId(rs.getString("TSTEP_TDUID"));
			 r.setTotalScreenshots(rs.getInt("TSTEP_TOTAL_SCRNSHOT"));
			 r.setTotalRuntimeValues(rs.getInt("TSTEP_TOTAL_OUTPUT"));
			 r.setTotalResultValidations(rs.getInt("TSTEP_TOTAL_VALS"));
			 r.setTotalResultValidationsPassed(rs.getInt("TSTEP_TOTAL_PASS"));
			 r.setTotalResultValidationsFailed(rs.getInt("TSTEP_TOTAL_FAIL"));
			 r.setTotalResultValidationsErrored(rs.getInt("TSTEP_TOTAL_ERROR"));
			 r.setResult(rs.getString("TSTEP_RESULT"));
			 r.setMessage(rs.getString("TSTEP_MESSAGE"));
			 if(rs.getTimestamp("EXEC_START_TIME") != null) {
				 r.setStartTimeStamp(rs.getTimestamp("EXEC_START_TIME"));
				 r.setExecutionStartTime(new Date(r.getStartTimeStamp().getTime()));
			 }
			 
			 if(rs.getTimestamp("EXEC_END_TIME") != null) {
				 r.setEndTimeStamp(rs.getTimestamp("EXEC_END_TIME"));
				 r.setExecutionEndTime(new Date(r.getEndTimeStamp().getTime()));
			 }
			 
			 
			 //Ascertain validation status
			 if(r.getTotalResultValidationsErrored() > 0) {
				 r.setValidationStatus(HatsConstants.ACTION_RESULT_ERROR);
			 }else if(r.getTotalResultValidationsFailed() > 0) {
				 r.setValidationStatus(HatsConstants.ACTION_RESULT_FAILURE);
			 }else if(r.getTotalResultValidationsPassed() > 0) {
				 r.setValidationStatus(HatsConstants.ACTION_RESULT_SUCCESS);
			 }else {
				 r.setValidationStatus("N-A");
			 }
			 
			 
			 //Ascertain step elapsed time
			 if(r.getExecutionStartTime() != null && r.getExecutionEndTime() != null) {
				 String elapsedTime = Utilities.calculateElapsedTime(r.getExecutionStartTime().getTime(), r.getExecutionEndTime().getTime());
				 r.setElapsedTime(elapsedTime);
			 }
			 
			 //TENJINCG-866 (Sriram)
			 try {
				 r.setWsReqMessage(rs.getString("TSTEP_WS_REQ_MSG"));	
				 r.setWsResMessage(rs.getString("TSTEP_WS_RES_MSG"));
			 } catch(Exception e) {
				 logger.warn("Could not get TSTEP_WS_REQ_MSG and TSTEP_WS_RES_MSG. Probably not included in query");
			 } //TENJINCG-866 (Sriram) ends
			 return r;
		 }
		 
		 private TestRun buildTestRunObject(ResultSet rs) throws SQLException {
			 TestRun run = new TestRun();
			 run.setId(rs.getInt("RUN_ID"));
			 run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
			 run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
			 run.setUser(rs.getString("RUN_USER"));
			 int tsId = rs.getInt("RUN_TS_REC_ID");
			 run.setTestSetRecordId(tsId);
			 run.setStatus(rs.getString("RUN_STATUS"));
			 run.setMachine_ip(rs.getString("RUN_MACHINE_IP"));
			 run.setTargetPort(rs.getInt("RUN_CL_PORT"));
			 run.setBrowser_type(rs.getString("RUN_BROWSER_TYPE"));
			 run.setTaskType(rs.getString("RUN_TASK_TYPE"));
			 if(run.getStartTimeStamp() != null && run.getEndTimeStamp() != null){
				 long startMillis = run.getStartTimeStamp().getTime();
				 long endMillis = run.getEndTimeStamp().getTime();

				 long millis = endMillis - startMillis;

				 String eTime = String.format(
						 "%02d:%02d:%02d",
						 TimeUnit.MILLISECONDS.toHours(millis),
						 TimeUnit.MILLISECONDS.toMinutes(millis)
						 - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
								 .toHours(millis)),
						 TimeUnit.MILLISECONDS.toSeconds(millis)
						 - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
								 .toMinutes(millis)));
				 run.setElapsedTime(eTime);
			 }
			 run.setTotalTests(rs.getInt("TOTAL_TCS"));
			 run.setExecutedTests(rs.getInt("TOTAL_EXE_TCS"));
			 run.setPassedTests(rs.getInt("TOTAL_PASSED_TCS"));
			 run.setFailedTests(rs.getInt("TOTAL_FAILED_TCS"));
			 run.setErroredTests(rs.getInt("TOTAL_ERROR_TCS"));
			 run.setDeviceRecId(rs.getInt("RUN_DEVICE_REC_ID"));
			 run.setDomainName(rs.getString("RUN_DOMAIN_NAME"));
			 run.setProjectName(rs.getString("RUN_PRJ_NAME"));
			 run.setParentRunId(rs.getInt("PARENT_RUN_ID"));
			 run.setProjectId(rs.getInt("RUN_PRJ_ID"));
			 /*Added by sahana for pCloudy: Starts*/
			 run.setDeviceFarmFlag(rs.getString("RUN_DEVICE_FARM"));
			 /*Added by sahana for pCloudy: Starts*/
			 try {
				 run.setTestSetName(rs.getString("TS_NAME"));
			 }catch(Exception e) {
				 logger.warn("TS_REC_ID is not part of the result set. This field will be skipped.");
			 }
			 
			 return run;
		 }
		//TENJINCG-870 (Sriram) ends

		public List<RuntimeFieldValue> hydrateRunTimeValues(int runId) throws DatabaseException {
			 List<RuntimeFieldValue> values = new LinkedList<>();
			 
			 try (
					 Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					/* Commented by Ashiki for Tenj210-76 starts*/
					 PreparedStatement pst = conn.prepareStatement("select * from TJN_EXEC_OUTPUT where RUN_ID=? order by TDGID");
					 /* Commented by Ashiki for Tenj210-76 end*/
				) {
				 
				 pst.setInt(1, runId);
				 try (ResultSet rs = pst.executeQuery()) {
					 while(rs.next()) {
						 values.add(this.buildRuntimeFieldValueObject(rs));
					 }
				 }
				 
			 } catch (SQLException e) {
				logger.error("Error fetching runtime values", e);
				throw new DatabaseException("Could not fetch runtime values", e);
			}
			 
			 return values;
		}
		
		

		public String getRunJson(int runId) throws DatabaseException {
			
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement pst = null;
			ResultSet rs=null;
			String runJson="";
			try {
				
				/*modified by shruthi for VAPT helper fixes starts*/
				pst = conn.prepareStatement("SELECT RUN_JSON FROM MASTESTRUNS  WHERE RUN_ID =?");
				pst.setInt(1, runId);
				/*modified by shruthi for VAPT helper fixes ends*/
				 rs=pst.executeQuery();
				if(rs.next()){
					runJson=rs.getString("RUN_JSON");
				}
				
			}catch (Exception e) {
				logger.error("Error ", e);
				throw new DatabaseException("Could not fetch run json", e);
			} finally {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
			return runJson; 
		}
		
		public int getPrjId(int runId) throws DatabaseException {
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			int prjId=0;
		
			PreparedStatement pst = null;
			ResultSet rs1 = null;
			try {
				 /*modified by shruthi for sql injection starts*/
				String query = "SELECT RUN_PRJ_ID FROM MASTESTRUNS WHERE RUN_ID=?";
				pst = conn.prepareStatement(query);
				pst.setInt(1, runId);
				/*modified by shruthi for sql injection ends*/
					rs1=pst.executeQuery();
					if(rs1.next())
					{
						prjId=rs1.getInt("RUN_PRJ_ID");
							 
					}
				
			   }catch(SQLException e){
				
				throw new DatabaseException("Could not merge failures. Please contact Tenjin support.");
			}finally{
				
				DatabaseHelper.close(rs1);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
			return prjId;
		}
		/*Added by Pushpalatha for TENJINCG-1106 ends*/
		
		/*Added by Pushpalatha for TENJINCG-1227 starts*/
		public Map<String,List<Integer>> getParentRunIds(TestRun run) throws DatabaseException, SQLException {
			
			 Map<String,List<Integer>> map=new HashMap<String,List<Integer>>();
			 
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			 String parentRunsQuery = "WITH rec_table (PARENT_RUN_ID, RUN_ID) AS ( SELECT e.parent_run_id, e.run_id FROM MASTESTRUNS e WHERE run_id = ? UNION ALL SELECT e.parent_run_id, e.run_id FROM mastestruns e INNER JOIN rec_table d ON e.run_id = d.parent_run_id ) SELECT run_id FROM rec_table";
			 String childRunsQuery = "WITH rec_table (PARENT_RUN_ID, RUN_ID) AS ( SELECT e.parent_run_id, e.run_id FROM MASTESTRUNS e WHERE parent_run_id = ? UNION ALL SELECT e.parent_run_id, e.run_id FROM mastestruns e INNER JOIN rec_table d ON e.parent_run_id = d.run_id ) SELECT run_id FROM rec_table";
			 
			 List<Integer> parentRuns = new LinkedList<>();
			 List<Integer> childRuns = new LinkedList<>();
			 
			 if(run != null && run.getParentRunId() > 0) {
				 try(PreparedStatement pst = conn.prepareStatement(parentRunsQuery)) {
					 pst.setInt(1, run.getId());
					 
					 try (ResultSet rs =pst.executeQuery()) {
						 while(rs.next()) {
							 int r = rs.getInt("RUN_ID");
							 if(r == run.getId()) continue;
							 parentRuns.add(r);
						 }
					 }
				 }
				 map.put("parentRuns",parentRuns);
			 }
			 
			//Get all child runs
			 try(PreparedStatement pst = conn.prepareStatement(childRunsQuery)) {
				 pst.setInt(1, run.getId());
				 
				 try (ResultSet rs =pst.executeQuery()) {
					 while(rs.next()) {
						 int r = rs.getInt("RUN_ID");
						 if(r == run.getId()) continue;
						 childRuns.add(r);
					 }
				 }
			 }
			 map.put("childRuns",childRuns);
			return map;
		}
		/*Added by Pushpalatha for TENJINCG-1227 ends*/
		
		/*Added by Pushpalatha for TENJINCG-1226 starts*/
		@SuppressWarnings("resource")
		public void removeTestRuns(ArrayList<Integer> runs) throws DatabaseException, SQLException {
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement pst = null;
			for(Integer runId:runs){
				try {
					/*modified by paneendra for sql injection starts*/
						pst = conn.prepareStatement("DELETE FROM TJN_EXEC_SCRNSHOT WHERE RUN_ID=?");
						pst.setInt(1, runId);
						pst.execute();
						pst = conn.prepareStatement("DELETE FROM TJN_MAIL_LOGS WHERE RUN_ID=?");
						pst.setInt(1, runId);
						pst.execute();
						pst = conn.prepareStatement("DELETE FROM TJN_EXEC_OUTPUT WHERE RUN_ID=?");
						pst.setInt(1, runId);
						pst.execute();
						pst = conn.prepareStatement("DELETE FROM UIVALRESULTS WHERE RUN_ID=?");
						pst.setInt(1, runId);
						pst.execute();
						pst = conn.prepareStatement("DELETE FROM RESVALRESULTS WHERE RUN_ID=?");
						pst.setInt(1, runId);
						pst.execute();
						pst = conn.prepareStatement("DELETE FROM RUNRESULT_TS_TXN WHERE RUN_ID=?");
						pst.setInt(1, runId);
						pst.execute();
						pst = conn.prepareStatement("DELETE FROM MASTESTRUNS WHERE RUN_ID=?");
						pst.setInt(1, runId);
						pst.execute();
						/*modified by paneendra for sql injection ends*/
				   }catch(SQLException e){
					   conn.rollback();
					   throw new DatabaseException("Could not delete runs, "+e.getMessage());
				}
			}
			//conn.commit();
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		public ArrayList<Integer> getChildRuns(ArrayList<Integer> runs) throws DatabaseException, SQLException{
			
			 
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			ArrayList<Integer> childRuns = new ArrayList<>();
			for(int run:runs){
				 String childRunsQuery = "WITH rec_table (PARENT_RUN_ID, RUN_ID) AS ( SELECT e.parent_run_id, e.run_id FROM MASTESTRUNS e WHERE parent_run_id = ? UNION ALL SELECT e.parent_run_id, e.run_id FROM mastestruns e INNER JOIN rec_table d ON e.parent_run_id = d.run_id ) SELECT run_id FROM rec_table";
				 try(PreparedStatement pst = conn.prepareStatement(childRunsQuery)) {
					 pst.setInt(1, run);
					 try (ResultSet rs =pst.executeQuery()) {
						 while(rs.next()) {
							 int r = rs.getInt("RUN_ID");
							 if(r == run) continue;
							 childRuns.add(r);
						 }
					 }
				 }
			}
				
			DatabaseHelper.close(conn);
			return childRuns;
		}
		/*Added by Pushpalatha for TENJINCG-1226 ends*/
		/*added by shruthi for TENJINCG-1224 starts*/
		@SuppressWarnings("resource")
		public void removeDefects(ArrayList<Integer> runs) throws DatabaseException, SQLException {
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement pst = null;
			PreparedStatement pst1 = null;
			ResultSet rs = null;
			for(Integer runId:runs){
				try {
					/*modified by paneendra for sql injection starts*/
					    pst1 = conn.prepareStatement("SELECT DEF_REC_ID FROM TJN_RUN_DEFECTS WHERE RUN_ID=?");
					    pst1.setInt(1, runId);
					    rs=pst1.executeQuery();
					    while(rs.next())
					    {
					    int i =rs.getInt("DEF_REC_ID");
						pst = conn.prepareStatement("DELETE FROM TJN_RUN_DEF_ATTACHMENTS WHERE DEF_REC_ID=?");
						pst.setInt(1, i);
						pst.execute();
						pst = conn.prepareStatement("DELETE FROM TJN_RUN_DEFECT_LINKAGE WHERE DEF_REC_ID=?");
						pst.setInt(1, i);
						pst.execute();
						pst = conn.prepareStatement("DELETE FROM TJN_RUN_DEFECTS WHERE RUN_ID=?");
						pst.setInt(1, i);
						pst.execute();
						/*modified by paneendra for sql injection ends*/
					    }
				   }catch(SQLException e1){
					   conn.rollback();
					   throw new DatabaseException("Could not delete defects, ");
					   
				}
		}
			
				//conn.commit(); 
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(pst1);
				DatabaseHelper.close(conn);
		
		}
		/*added by shruthi for TENJINCG-1224 ends*/

/*Added by Ashiki for TENJINCG-1211 starts*/
public List<TestRun> hydrateConsolidatedRun(String CurrentDateTime,int projectId) throws DatabaseException, ParseException, SQLException {
	
	 List<TestRun> runs = new LinkedList<TestRun>();
	 String CurrentDateStartTime = CurrentDateTime+" "+"00:00:00";
     String CurrentDateEndTime =  CurrentDateTime+" "+"23:59:59";
     
     
    
	try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			){
		//Modified by  Priyanka for Tenj211-30 starts
		String query="";
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query="SELECT * from MASTESTRUNS where (RUN_START_TIME BETWEEN TO_DATE(?,'YYYY-mm-DD HH24:MI:SS') AND\r\n" + 
					"TO_DATE(?,'YYYY-mm-DD HH24:MI:SS'))\r\n" + 
					"  OR (RUN_END_TIME BETWEEN TO_DATE(?,'YYYY-mm-DD HH24:MI:SS')\r\n" + 
					"  AND TO_DATE(?,'YYYY-mm-DD HH24:MI:SS')) AND RUN_PRJ_ID=? order by RUN_ID";
		}
		else {
			query="SELECT * from MASTESTRUNS where ((RUN_START_TIME BETWEEN ? AND ?) OR (RUN_END_TIME BETWEEN ? AND ?)) AND RUN_PRJ_ID=? order by RUN_ID";
		}
		
	    try (PreparedStatement pst = conn.prepareStatement(query);
			){
	    //Modified by  Priyanka for Tenj211-30 ends
		 pst.setString(1, CurrentDateStartTime);
         pst.setString(2,  CurrentDateEndTime);
         pst.setString(3, CurrentDateStartTime);
         pst.setString(4,  CurrentDateEndTime);
		 pst.setInt(5, projectId);
		 try (ResultSet rs = pst.executeQuery()){
			while(rs.next()){
				runs.add(this.buildTestRunObject(rs));
			}
		}
	}
	catch(Exception e){
		logger.error("Could not fetch consolidated runs details user",e);
		throw new DatabaseException("Could not fetch consolidated runs details due to an internal error",e);
	}
	return runs;


}
/*Added by Ashiki for TENJINCG-1211 end*/

}

/*Added by Pushpalatha for TENJINCG-1106 starts*/
public TestRun hydrateRunForAudit(int runId,int projectId) throws DatabaseException{
	Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

	if(conn == null){
		throw new DatabaseException("Invalid Database Connection");
	}
	TestRun run = null;
	PreparedStatement pst =null;
	PreparedStatement pstmt=null;
	ResultSet rset=null;
	ResultSet rs =null;
	try {
		pst = conn.prepareStatement("SELECT A.RUN_ID, A.RUN_TASK_TYPE, A.RUN_STATUS AS RUN_COMPLETION_STATUS, A.RUN_START_TIME,A.RUN_END_TIME,A.RUN_USER,A.RUN_TS_REC_ID,A.RUN_MACHINE_IP,A.RUN_CL_PORT,A.RUN_BROWSER_TYPE,A.RUN_PRJ_NAME,A.PARENT_RUN_ID,A.RUN_DOMAIN_NAME,A.RUN_DEVICE_REC_ID, A.RUN_DEVICE_FARM,B.TOTAL_TCS,B.TOTAL_EXE_TCS,B.TOTAL_PASSED_TCS,B.TOTAL_FAILED_TCS,B.TOTAL_ERROR_TCS,B.RUN_STATUS FROM MASTESTRUNS A, V_RUNRESULTS B WHERE B.RUN_ID = A.RUN_ID AND A.RUN_ID = ? AND A.RUN_PRJ_ID=?");
		pst.setInt(1, runId);
		pst.setInt(2, projectId);
		rs = pst.executeQuery();
		while(rs.next()){
			run = new TestRun();
			run.setId(rs.getInt("RUN_ID"));
			run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
			run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
			run.setUser(rs.getString("RUN_USER"));
			int tsId = rs.getInt("RUN_TS_REC_ID");
			run.setTestSetRecordId(tsId);
			run.setStatus(rs.getString("RUN_STATUS"));
			run.setMachine_ip(rs.getString("RUN_MACHINE_IP"));
			run.setTargetPort(rs.getInt("RUN_CL_PORT"));
			run.setBrowser_type(rs.getString("RUN_BROWSER_TYPE"));
			run.setTaskType(rs.getString("RUN_TASK_TYPE"));
			if(run.getStartTimeStamp() != null && run.getEndTimeStamp() != null){
				long startMillis = run.getStartTimeStamp().getTime();
				long endMillis = run.getEndTimeStamp().getTime();

				long millis = endMillis - startMillis;

				String eTime = String.format(
						"%02d:%02d:%02d",
						TimeUnit.MILLISECONDS.toHours(millis),
						TimeUnit.MILLISECONDS.toMinutes(millis)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
								.toHours(millis)),
						TimeUnit.MILLISECONDS.toSeconds(millis)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
								.toMinutes(millis)));
				run.setElapsedTime(eTime);
			}
			
			run.setTotalTests(rs.getInt("TOTAL_TCS"));
			run.setExecutedTests(rs.getInt("TOTAL_EXE_TCS"));
			run.setPassedTests(rs.getInt("TOTAL_PASSED_TCS"));
			run.setFailedTests(rs.getInt("TOTAL_FAILED_TCS"));
			run.setErroredTests(rs.getInt("TOTAL_ERROR_TCS"));
			run.setDeviceRecId(rs.getInt("RUN_DEVICE_REC_ID"));
			run.setDeviceFarmFlag(rs.getString("RUN_DEVICE_FARM"));
			
			run.setDomainName(rs.getString("RUN_DOMAIN_NAME"));
			run.setProjectName(rs.getString("RUN_PRJ_NAME"));
			run.setParentRunId(rs.getInt("PARENT_RUN_ID"));
			run.setProjectId(projectId);

		}
		int tcRecId=0;
		/*modified by shruthi for sql injection starts*/
		pstmt=conn.prepareStatement("SELECT TC_REC_ID FROM V_RUNRESULTS_TC WHERE RUN_ID=?");
		pstmt.setInt(1, runId);
		/*modified by shruthi for sql injection ends*/
		rset=pstmt.executeQuery();
		if(rset.next())
			tcRecId=rset.getInt("TC_REC_ID");
		run.setTcRecId(String.valueOf(tcRecId));
		
	} catch (SQLException e) {
		
		logger.error("Hydrate run from MASTESTRUNS --> Failed",e);
		throw new DatabaseException("Could not fetch run information",e);
	}finally{
		DatabaseHelper.close(rset);
		DatabaseHelper.close(pstmt);
		DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);
	}

	
		run.setPureRunId(this.getParent(runId));

	TestSet ts = null;
	try {
		pst = conn.prepareStatement("SELECT * FROM TESTSETS A LEFT join TESTSETMAP B on (A.TS_REC_ID = B.TSM_TS_REC_ID) WHERE A.TS_REC_ID = ?  AND A.TS_REC_TYPE IN (?,?,?) order by b.tsm_tc_seq");
		pst.setInt(1, run.getTestSetRecordId());
		pst.setString(2, "TS");
		pst.setString(3, "TSA");
		/*Added by Pushpa for TENJINCG-1281 starts*/
		pst.setString(4, "TSZ");
		/*Added by Pushpa for TENJINCG-1281 ends*/
		rs = pst.executeQuery();
		ArrayList<TestCase> tests = new ArrayList<TestCase>();
		while(rs.next()){
			ts = new TestSet();
			ts.setName(rs.getString("TS_NAME"));
			ts.setRecordType(rs.getString("TS_REC_TYPE"));
			ts.setId(rs.getInt("TS_REC_ID"));
			ts.setType(rs.getString("TS_TYPE"));
			ts.setParent(rs.getInt("TS_FOLDER_ID"));
			ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
			ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
			ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
			ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
			ts.setDescription(rs.getString("TS_DESC"));
			ts.setProject(projectId);
			ts.setMode(rs.getString("TS_EXEC_MODE"));
			int tcRecId = rs.getInt("TSM_TC_REC_ID");
			PreparedStatement tPst =null;
			ResultSet tRs =null;
			try{
				tPst = conn.prepareStatement("SELECT A.*, B.TC_TOTAL_STEPS, B.TC_TOTAL_EXEC, B.TC_TOTAL_PASS, B.TC_TOTAL_FAIL, B.TC_TOTAL_ERROR, B.TC_STATUS AS TC_STAT FROM TESTCASES A, V_RUNRESULTS_TC B WHERE B.TC_REC_ID = A.TC_REC_ID AND A.TC_REC_ID = ? AND B.RUN_ID = ?");
				tPst.setInt(1, tcRecId);
				tPst.setInt(2, runId);
				tRs = tPst.executeQuery();

				while(tRs.next()){
					TestCase tc = new TestCase();
					tc.setTcId(tRs.getString("tc_id"));
					tc.setTcName(tRs.getString("tc_name"));
					tc.setTcType(tRs.getString("tc_type"));
					tc.setTcCreatedBy(tRs.getString("tc_created_by"));
					tc.setTcCreatedOn(tRs.getTimestamp("tc_created_on"));
					tc.setTcModifiedBy(tRs.getString("tc_modified_by"));
					tc.setTcModifiedOn(tRs.getDate("tc_modified_on"));
					tc.setTcDesc(tRs.getString("tc_desc"));
					tc.setTcRecId(tRs.getInt("TC_REC_ID"));
					tc.setTcFolderId(tRs.getInt("TC_FOLDER_ID"));
					tc.setProjectId(tRs.getInt("TC_PRJ_ID"));
					tc.setRecordType(tRs.getString("TC_REC_TYPE"));
					tc.setTcReviewed(tRs.getString("TC_REVIEWED"));
					tc.setTcPriority(tRs.getString("TC_PRIORITY"));
					tc.setTcRecId(tRs.getInt("TC_REC_ID"));
					tc.setTotalSteps(tRs.getInt("TC_TOTAL_STEPS"));
					tc.setTotalExecutedSteps(tRs.getInt("TC_TOTAL_EXEC"));
					tc.setTotalPassedSteps(tRs.getInt("TC_TOTAL_PASS"));
					tc.setTotalFailedSteps(tRs.getInt("TC_TOTAL_FAIL"));
					tc.setTotalErrorredSteps(tRs.getInt("TC_TOTAL_ERROR"));
					tc.setTcStatus(tRs.getString("TC_STAT"));
					tests.add(tc);
				}
			}catch(SQLException e){
				logger.error("Fetch Test Case Details from TESTCASES and RUNRESULTS_TC --> Failed",e);
				continue;
			}finally{
				DatabaseHelper.close(tRs);
				DatabaseHelper.close(tPst);
			}
		}
		ts.setTests(tests);
	} catch (SQLException e) {
		
		logger.error("Fetch Test Set details from TESTSETS and TESTSETMAP --> Failed",e);
		throw new DatabaseException("Could not fetch details of the test set in the current run",e);
	}finally{
		DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);
	}

	ArrayList<TestCase> newTCs = new ArrayList<TestCase>();
	for(TestCase tc:ts.getTests()){
		ArrayList<TestStep> steps = null;
		try {
			pst = conn.prepareStatement("SELECT A.*, C.APP_NAME, B.RUN_ID,B.TSTEP_TOTAL_TXN, B.TSTEP_TOTAL_EXEC, B.TSTEP_TOTAL_PASS, B.TSTEP_TOTAL_FAIL, B.TSTEP_TOTAL_ERROR,B.TSTEP_STATUS AS TSTEP_STAT FROM TESTSTEPS A, V_RUNRESULTS_TS B, MASAPPLICATION C WHERE B.TSTEP_REC_ID = A.TSTEP_REC_ID AND C.APP_ID = A.APP_ID AND A.TC_REC_ID = ? AND B.RUN_ID = ? ORDER BY A.TSTEP_EXEC_SEQ");
			pst.setInt(1, tc.getTcRecId());
			pst.setInt(2, runId);
			rs = pst.executeQuery();
			steps = new ArrayList<TestStep>();
			while(rs.next()){
				TestStep t = new TestStep();
				t.setRecordId(rs.getInt("tstep_rec_id"));
				t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
				t.setDataId(rs.getString("TSTEP_DATA_ID"));
				t.setType(rs.getString("TSTEP_TYPE"));
				t.setDescription(rs.getString("TSTEP_DESC"));
				t.setExpectedResult(rs.getString("TSTEP_EXP_RESULT"));
				t.setAppId(rs.getInt("APP_ID"));
				t.setModuleCode(rs.getString("FUNC_CODE"));
				t.setId(rs.getString("TSTEP_ID"));
				t.setTestCaseRecordId(rs.getInt("TC_REC_ID"));
				t.setOperation(rs.getString("TSTEP_OPERATION"));
				t.setAutLoginType(rs.getString("TSTEP_AUT_LOGIN_TYPE"));
				t.setTotalTransactions(rs.getInt("TSTEP_TOTAL_TXN"));
				t.setTotalExecutedTxns(rs.getInt("TSTEP_TOTAL_EXEC"));
				t.setTotalPassedTxns(rs.getInt("TSTEP_TOTAL_PASS"));
				t.setTotalFailedTxns(rs.getInt("TSTEP_TOTAL_FAIL"));
				t.setTotalErroredTxns(rs.getInt("TSTEP_TOTAL_ERROR"));
				t.setStatus(rs.getString("TSTEP_STAT"));
				t.setTxnMode(rs.getString("TXNMODE"));
				t.setAppName(rs.getString("APP_NAME"));
				t.setValidationType(rs.getString("TSTEP_VAL_TYPE"));
				
				ArrayList<StepIterationResult> results= new ArrayList<StepIterationResult>();
				PreparedStatement rPst =null;
				ResultSet oRs =null;

				try{
					rPst = conn.prepareStatement("SELECT * FROM RUNRESULT_TS_TXN WHERE TSTEP_REC_ID = ? AND RUN_ID = ? ORDER BY TXN_NO");
					rPst.setInt(1, t.getRecordId());
					rPst.setInt(2, runId);
					oRs = rPst.executeQuery();
					while(oRs.next()){
						StepIterationResult r = new StepIterationResult();
						r.setIterationNo(oRs.getInt("TXN_NO"));
						
						ArrayList<RuntimeFieldValue> runtimeValues = new ArrayList<RuntimeFieldValue>();
						String tdUid = oRs.getString("TSTEP_TDUID");
						r.setTsRecId(t.getRecordId());
						r.setTduid(tdUid);
						r.setRunId(runId);
						r.setResult(oRs.getString("TSTEP_RESULT"));
						r.setMessage(oRs.getString("TSTEP_MESSAGE"));
						r.setApiAuthType(oRs.getString("API_AUTH_TYPE"));
						r.setApiAccessToken(oRs.getString("API_ACCESS_TOKEN"));
						/*Added by Pushpa for TENJINCG-1228 starts*/
						r.setApiRequestLog(oRs.getString("API_REQ_LOG"));
						/*Added by Pushpa for TENJINCG-1228 starts*/
						Blob b = oRs.getBlob("TSTEP_SCRSHOT");
						if(b != null){
							byte barr[] = b.getBytes(1,(int)b.length());
							r.setScreenshotByteArray(barr);
						}else{
							r.setScreenshotByteArray(null);
						}
						r.setStartTimeStamp(oRs.getTimestamp("EXEC_START_TIME"));
						r.setEndTimeStamp(oRs.getTimestamp("EXEC_END_TIME"));
						if(r.getEndTimeStamp() != null && r.getStartTimeStamp() != null){
							String elapsedTime= Utilities.calculateElapsedTime(r.getStartTimeStamp().getTime(), r.getEndTimeStamp().getTime());
							r.setElapsedTime(elapsedTime);
						}
						else
							r.setElapsedTime("00:00:00");
						
						PreparedStatement vPst = null;
						ResultSet vRs = null;
						try{
							vPst = conn.prepareStatement("SELECT * FROM RESVALRESULTS WHERE RUN_ID = ? AND RES_VAL_ID =  ? AND TSTEP_REC_ID = ? AND ITERATION =?");
							vPst.setInt(1, runId);
							vPst.setInt(2, 0);
							vPst.setInt(3, t.getRecordId());
							vPst.setInt(4, r.getIterationNo());
							vRs = vPst.executeQuery();
							ArrayList<ValidationResult> valRess = new ArrayList<ValidationResult>();
							while(vRs.next()){
								ValidationResult res = new ValidationResult();
								res.setRunId(runId);
								res.setResValId(vRs.getInt("RES_VAL_ID"));
								res.setTestStepRecordId( t.getRecordId());
								res.setPage(vRs.getString("RES_VAL_PAGE"));
								res.setField(vRs.getString("RES_VAL_FIELD"));
								res.setExpectedValue(vRs.getString("RES_VAL_EXP_VAL"));
								res.setActualValue(vRs.getString("RES_VAL_ACT_VAL"));
								res.setStatus(vRs.getString("RES_VAL_STATUS"));
								res.setIteration(vRs.getInt("ITERATION"));
								res.setDetailRecordNo(vRs.getString("RES_VAL_DETAIL_ID"));
								valRess.add(res);
							}
							r.setValidationResults(valRess);
						}catch(Exception e){
							logger.error("Get Result Validation Results for Step (record id - " + t.getRecordId() + ") from RESVALRESULTS --> Failed",e);
							continue;
						}finally{
							
							DatabaseHelper.close(vRs);
							DatabaseHelper.close(vPst);
						}

						PreparedStatement sPst=null;
						ResultSet sRs=null;;

						try {
							sPst = conn.prepareStatement(
									"SELECT * FROM TJN_EXEC_OUTPUT WHERE RUN_ID = ? AND TSTEP_REC_ID = ? AND TDUID = ?");
							sPst.setInt(1, runId);
							sPst.setInt(2, t.getRecordId());
							sPst.setString(3, tdUid);
							sRs = sPst.executeQuery();
						/*	while (sRs.next()) {*/
								RuntimeFieldValue s = new RuntimeFieldValue();
								s.setRunId(runId);
								s.setTdGid(sRs.getString("TDGID"));
								s.setTdUid(sRs.getString("TDUID"));
								s.setTestStepRecordId(sRs.getInt("TSTEP_REC_ID"));
								s.setDetailRecordNo(sRs.getInt("DETAIL_NO"));
								s.setField(sRs.getString("FIELD_NAME"));
								s.setValue(sRs.getString("FIELD_VALUE"));
								runtimeValues.add(s);
								/* } */
							
						} catch (Exception e) {
							logger.error(e.getMessage());
						}finally{
							DatabaseHelper.close(sRs);
							DatabaseHelper.close(sPst);
						}
						

						r.setRuntimeFieldValues(runtimeValues);
						
						results.add(r);
					}
				}catch(SQLException e){
					logger.error("Get Detailed Step result from RUNRESULT_TS_TXN --> Failed",e);
					continue;
				}finally{
					DatabaseHelper.close(oRs);
					DatabaseHelper.close(rPst);
				}

				t.setDetailedResults(results);
				steps.add(t);
			}
		} catch (SQLException e) {
			
			logger.error("Fetch steps for Test Case " + tc.getTcId() + " from TESTSTEPS and RUNRESULTS_TS --> Failed",e);
			throw new DatabaseException("Could not get Test Run Info",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		tc.setTcSteps(steps);
		newTCs.add(tc);
	}

	ts.setTests(null);
	ts.setTests(newTCs);

	run.setTestSet(null);
	run.setTestSet(ts);

	DatabaseHelper.close(conn);
	return run;

}

public void persisteExecAudit(int runId, int prjId) throws DatabaseException {
	// TODO Auto-generated method stub
	TenjinReflection tenjinReflection=new TenjinReflection();
	String runData=tenjinReflection.hydrateRunForAudit(runId,prjId);
	//String runData=this.hydrateRunForAudit(runId, projectId);


	Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
	PreparedStatement pst = null;


	try {
		/*Modified by Prem for Tenj210-148 start*/
		//      pst = conn.prepareStatement("UPDATE MASTESTRUNS SET RUN_JSON =to_clob(?) WHERE RUN_ID =? ");
		DatabaseMetaData meta = conn.getMetaData();
		String query=null;
		if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query = "UPDATE MASTESTRUNS SET RUN_JSON =? WHERE RUN_ID =? ";
			pst = conn.prepareStatement(query);
			Clob clobOld = pst.getConnection().createClob();
			clobOld.setString(1, runData);
			pst.setClob(1, clobOld);
		}
		else {
			query = "UPDATE MASTESTRUNS SET RUN_JSON =? WHERE RUN_ID =? ";
			pst = conn.prepareStatement(query);
			pst.setString(1, runData);
		}
		/*Modified by Prem for Tenj210-148 End*/
		//  int recId = DatabaseHelper.getGlobalSequenceNumber(conn);

		pst.setInt(2, runId);
		pst.execute();

	}catch (Exception e) {
		logger.error("Couldn't update runJson"+e);
		throw new DatabaseException("Could not insert run json", e);
	} finally {

		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);
	}

}
}

