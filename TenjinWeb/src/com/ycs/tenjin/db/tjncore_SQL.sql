--------------------------------------------------------
--  File created - Monday-March-09-2015   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table MASCONNPOOL
--------------------------------------------------------


CREATE TABLE [dbo].[MASCONNPOOL](
	[POOL_ID] [varchar](40) NULL,
	[POOL_NAME] [varchar](100) NULL,
	[HOST_NAME] [varchar](100) NULL,
	[SID_NAME] [varchar](40) NULL,
	[USER_NAME] [varchar](40) NULL,
	[PASS_WORD] [varchar](40) NULL
)

--------------------------------------------------------
--  DDL for Table MASUSER
--------------------------------------------------------
CREATE TABLE [dbo].[MASUSER](
	[USER_ID] [varchar](20) NULL,
	[FIRST_NAME] [varchar](50) NULL,
	[LAST_NAME] [varchar](50) NULL,
	[EMAIL_ID] [varchar](100) NULL,
	[PHONE_1] [varchar](15) NULL,
	[PHONE_2] [varchar](15) NULL,
	[PASS_WORD] [varchar](200) NULL
)

--------------------------------------------------------
--  DDL for Table MASUSERGROUPS
--------------------------------------------------------
CREATE TABLE [dbo].[MASUSERGROUPS](
	[GROUP_ID] [numeric](18, 0) NULL,
	[GROUP_DESC] [varchar](100) NULL
)

--------------------------------------------------------
--  DDL for Table USERROLES
--------------------------------------------------------
CREATE TABLE [dbo].[USERROLES](
	[ROLE_ID] [numeric](18, 0) NULL,
	[USER_ID] [varchar](20) NULL,
	[GROUP_ID] [numeric](18, 0) NULL
)

--------------------------------------------------------
--  DDL for Table USERSESSIONS
--------------------------------------------------------
CREATE TABLE [dbo].[USERSESSIONS](
	[US_ID] [numeric](18, 0) NULL,
	[US_USER_ID] [varchar](20) NULL,
	[US_LOGIN_TIMESTAMP] [datetime] NULL,
	[US_LOGOUT_TIMESTAMP] [datetime] NULL,
	[US_SESSION_STATE] [varchar](1) NULL
)

--------------------------------------------------------
--  DDL for Table USRAUTCREDENTIALS
--------------------------------------------------------
CREATE TABLE [dbo].[USRAUTCREDENTIALS](
	[USER_ID] [varchar](20) NULL,
	[APP_ID] [numeric](18, 0) NULL,
	[APP_LOGIN_NAME] [varchar](50) NULL,
	[APP_PASSWORD] [varchar](50) NULL
)