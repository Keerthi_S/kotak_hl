 
 /*
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestRunHelper.java   


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 */


/*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
28-July-2017			Gangadhar Badagi		TENJINCG-315
31-July-2017			Gangadhar Badagi		To fetch list of runs based on projectId
* 01-Aug-2017		    Manish					TENJINCG-296
* 01-Sep-2017		    Manish					T25IT-413
* 25-Oct-2017			Preeti					TENJINCG-372
* 20-04-2018			Preeti					TENJINCG-616
* 10-07-2018            Leelaprasad             TENJINCG-604
* 22-10-2018		Sriram Sridharan			File Optimized for TENJINCG-866
* 13-12-2018	     	Padmavathi				for TJNUN262-97
* 20-12-2018			Pushpalatha				TJN262R2-49
* 24-12-2018			Sriram Sridharan		TJN262R2-48
* 20-03-2019            Padmavathi              TENJINCG-1010 
* 13-03-2020			Sriram Sridharan		TENJINCG-1196
* 
*/


package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.run.TestRunProgress;
import com.ycs.tenjin.run.TestRunSearchRequest;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class TestRunHelper {
	private static Logger logger = LoggerFactory.getLogger(TestRunHelper.class);
	/*Added by Padmavathi for TJNUN262-97 starts*/
	private final Map<String, String> fieldToColumnMap = new TreeMap<>();
	
	public TestRunHelper() {
		this.fieldToColumnMap.put("id", "RUN_ID");
		this.fieldToColumnMap.put("testSetName", "TS_NAME");
		this.fieldToColumnMap.put("user", "RUN_USER");
		this.fieldToColumnMap.put("startTimeStamp", "RUN_START_TIME");
		this.fieldToColumnMap.put("endTimeStamp", "RUN_END_TIME");
		this.fieldToColumnMap.put("status", "RUN_STATUS");
		/* Added by Padmavathi for TENJINCG-1010 starts */
		this.fieldToColumnMap.put("executedLevel", "TS_REC_TYPE");
		/* Added by Padmavathi for TENJINCG-1010 ends */
		
	}
	/*Added by Padmavathi for TJNUN262-97 ends*/
	
	public PaginatedRecords<TestRun> searchRuns(TestRunSearchRequest testRunSearchRequest, int maxRecords, int pageNumber, String sortColumn, String sortDirection) throws DatabaseException{
		
		try (Paginator paginator = new Paginator(maxRecords, pageNumber, true)) {
			String query = testRunSearchRequest.buildQuery(paginator.getConnection().getMetaData().getDatabaseProductName());
			List<TestRun> runs = new LinkedList<>();
			/*Added by Padmavathi for TJNUN262-97 starts*/
			String sCol = Utilities.trim(sortColumn).length() > 0 ? this.fieldToColumnMap.get(sortColumn) : "RUN_ID";
			String sDir = Utilities.trim(sortDirection).length() > 0 ? Utilities.trim(sortDirection) : "desc";
			//Changed by Sriram for TJN262R2-48
			
			if(sCol == null || sCol.length() < 1) {
				sCol = "RUN_ID";
				sDir = "desc";
			}
			//Changed by Sriram for TJN262R2-48 ends
			query += " ORDER BY " + sCol + " " + sDir;
			/*Added by Padmavathi for TJNUN262-97 ends*/
			/*Modified by Pushpa for TJN262R2-49 starts*/
			paginator.executeQuery("run",query);
			/*Modified by Pushpa for TJN262R2-49 ends*/
			int totalRecords = 0;
			while(paginator.getResultSet().next()) {
				runs.add(this.buildTestRunObject(paginator.getResultSet()));
				try {
					totalRecords = paginator.getResultSet().getInt("P_REC_COUNT");
				}catch(Exception e) {
					
				}
			}
			
			PaginatedRecords<TestRun> records = new PaginatedRecords<>();
			records.setPageNumber(pageNumber);
			records.setRecords(runs);
			records.setTotalSize(totalRecords);
			records.setCurrentSize(maxRecords);
			return records;
		} catch (Exception e) {
			logger.error("Error searching for runs", e);
			throw new DatabaseException("Could not perform search due to an internal error", e);
		}
		
		
	}
	
	private TestRun buildTestRunObject(ResultSet rs) throws SQLException {
		TestRun run = new TestRun();
		run.setId(rs.getInt("RUN_ID"));
		run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
		run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
		run.setUser(rs.getString("RUN_USER"));
		int tsId = rs.getInt("RUN_TS_REC_ID");
		run.setTestSetRecordId(tsId);
		run.setStatus(rs.getString("RUN_STATUS"));
		run.setMachine_ip(rs.getString("RUN_MACHINE_IP"));
		run.setTargetPort(rs.getInt("RUN_CL_PORT"));
		run.setBrowser_type(rs.getString("RUN_BROWSER_TYPE"));
		run.setTaskType(rs.getString("RUN_TASK_TYPE"));
		if(run.getStartTimeStamp() != null && run.getEndTimeStamp() != null){
			long startMillis = run.getStartTimeStamp().getTime();
			long endMillis = run.getEndTimeStamp().getTime();

			long millis = endMillis - startMillis;

			String eTime = String.format(
					"%02d:%02d:%02d",
					TimeUnit.MILLISECONDS.toHours(millis),
					TimeUnit.MILLISECONDS.toMinutes(millis)
					- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
							.toHours(millis)),
					TimeUnit.MILLISECONDS.toSeconds(millis)
					- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
							.toMinutes(millis)));
			run.setElapsedTime(eTime);
		}
		run.setTotalTests(rs.getInt("TOTAL_TCS"));
		run.setExecutedTests(rs.getInt("TOTAL_EXE_TCS"));
		run.setPassedTests(rs.getInt("TOTAL_PASSED_TCS"));
		run.setFailedTests(rs.getInt("TOTAL_FAILED_TCS"));
		run.setErroredTests(rs.getInt("TOTAL_ERROR_TCS"));
		run.setDeviceRecId(rs.getInt("RUN_DEVICE_REC_ID"));
		run.setDomainName(rs.getString("RUN_DOMAIN_NAME"));
		run.setProjectName(rs.getString("RUN_PRJ_NAME"));
		run.setParentRunId(rs.getInt("PARENT_RUN_ID"));
		run.setProjectId(rs.getInt("RUN_PRJ_ID"));
		run.setTestSetName(rs.getString("TS_NAME"));
		/* Added by Padmavathi for TENJINCG-1010 starts */
		run.setExecutedLevel(rs.getString("TS_REC_TYPE"));
		/* Added by Padmavathi for TENJINCG-1010 ends */
		
		return run;
	}
	
	// Added for TENJINCG-1196 (Sriram)
	public TestStep hydrateCurrentlyExecutedStep(Connection conn, int runId) throws DatabaseException{
		String query = "SELECT A.TSTEP_REC_ID, A.TSTEP_TDUID, B.TC_REC_ID, B.TSTEP_SHT_DESC, B.TSTEP_ID FROM RUNRESULT_TS_TXN A, TESTSTEPS B WHERE B.TSTEP_REC_ID=A.TSTEP_REC_ID AND A.TSTEP_RESULT='X' AND A.RUN_ID=?";
		
		try (PreparedStatement pst = conn.prepareStatement(query)) {
			pst.setInt(1, runId);
			
			try (ResultSet rs = pst.executeQuery()) {
				TestStep step = null;
				while(rs.next()) {
					step = new TestStep();
					step.setRecordId(rs.getInt("TSTEP_REC_ID"));
					step.setDataId(rs.getString("TSTEP_TDUID"));
					step.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
					step.setId(rs.getString("TSTEP_ID"));
					step.setTestCaseRecordId(rs.getInt("TC_REC_ID"));
				}
				
				return step;
			}
		} catch(SQLException e) {
			throw new DatabaseException("Could not fetch currently executed step", e);
		}
		
	}
	
	public TestRun hydrateBasicRunInformation(int runId) throws DatabaseException {
		 try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ)) {
			 return hydrateBasicRunInformation(conn, runId);
		 } catch(SQLException e) {
			 throw new DatabaseException("Could not connect to database", e);
		 }
	 }
	
	public TestRun hydrateBasicRunInformation(Connection conn, int runId) throws DatabaseException{
		 
	 	try (
	 			PreparedStatement pst = conn.prepareStatement("Select A.*, B.TS_NAME from MasTestRuns A, TestSets B where b.TS_REC_ID=a.RUN_TS_REC_ID and A.RUN_ID=?");
	 		){
	 		
	 		pst.setInt(1, runId);
	 		
	 		TestRun run = null;
	 		try(ResultSet rs = pst.executeQuery()) {
	 			while(rs.next()) {
	 				run = new TestRun();
		 			run.setId(rs.getInt("RUN_ID"));
		 			run.setCompletionStatus(rs.getString("RUN_STATUS"));
		 			run.setStatus(run.getCompletionStatus());
		 			run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
		 			run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
		 			run.setUser(rs.getString("RUN_USER"));
		 			run.setMachine_ip(rs.getString("RUN_MACHINE_IP"));
		 			run.setTargetPort(rs.getInt("RUN_CL_PORT"));
		 			run.setBrowser_type(rs.getString("RUN_BROWSER_TYPE"));
		 			run.setDeviceRecId(rs.getInt("RUN_DEVICE_REC_ID"));
		 			run.setDeviceFarmFlag(rs.getString("RUN_DEVICE_FARM"));
		 			run.setTestSetRecordId(rs.getInt("RUN_TS_REC_ID"));
		 			run.setTestSetName(rs.getString("TS_NAME"));
	 			}
	 		}
	 		
	 		return run;
	 		
	 	} catch (Exception e) {
	 		logger.error("Error fetching basic run information", e);
	 		throw new DatabaseException("Could not fetch run information", e);
	 	}
	}
	

	 
	 public TestRunProgress hydrateTestCaseExecutionSummary(Connection conn, int runId) throws DatabaseException {
		 String query = "SELECT C.TSM_TC_SEQ, A.TC_NAME,A.TC_REC_ID,A.TC_ID, B.TC_TOTAL_STEPS, B.TC_TOTAL_EXEC, B.TC_TOTAL_PASS, B.TC_TOTAL_FAIL, B.TC_TOTAL_ERROR, B.TC_STATUS AS TC_STAT, D.RUN_STATUS, D.RUN_START_TIME  " 
			 		+ " FROM TESTCASES A, V_RUNRESULTS_TC B,TESTSETMAP C, MASTESTRUNS D WHERE B.RUN_ID = D.RUN_ID AND B.TC_REC_ID = A.TC_REC_ID AND A.TC_REC_ID = C.TSM_TC_REC_ID AND C.TSM_TS_REC_ID=d.RUN_TS_REC_ID AND d.RUN_ID = ? ORDER BY  C.TSM_TC_SEQ";
		 
		 TestRunProgress progress = null;
		 
		 try (PreparedStatement pst = conn.prepareStatement(query)) {
			 pst.setInt(1, runId);
			 List<TestCase> testCases = null;
			 Timestamp sqlTimestamp = null;
			 try (ResultSet rs = pst.executeQuery()) {
				 progress = new TestRunProgress();
				 testCases = new ArrayList<>();
				 while(rs.next()) {
					 TestCase tc = new TestCase();
					 tc.setTcId(rs.getString("tc_id"));
					 tc.setTcName(rs.getString("tc_name"));
					 tc.setTcRecId(rs.getInt("TC_REC_ID"));
					 tc.setTotalSteps(rs.getInt("TC_TOTAL_STEPS"));
					 tc.setTotalExecutedSteps(rs.getInt("TC_TOTAL_EXEC"));
					 tc.setTotalPassedSteps(rs.getInt("TC_TOTAL_PASS"));
					 tc.setTotalFailedSteps(rs.getInt("TC_TOTAL_FAIL"));
					 tc.setTotalErrorredSteps(rs.getInt("TC_TOTAL_ERROR"));
					 tc.setTcStatus(rs.getString("TC_STAT"));
					 tc.setUploadRow(rs.getInt("TSM_TC_SEQ"));
					 testCases.add(tc);
					 
					 progress.setRunId(runId);
					 progress.setStatus(rs.getString("RUN_STATUS"));
					 
					 sqlTimestamp = rs.getTimestamp("RUN_START_TIME");
					 
				 }
				 if(sqlTimestamp != null) {
					 progress.setStartTimestamp(new Date(sqlTimestamp.getTime()));
				 }
				 progress.getTestCaseExecutionProgress().setTestCases(testCases);
			 }
		 } catch(SQLException e) {
			 throw new DatabaseException("Could not fetch Run progress", e);
		 }
		 
		 return progress;
		 
	 }
	// Added for TENJINCG-1196 (Sriram) ends

}





