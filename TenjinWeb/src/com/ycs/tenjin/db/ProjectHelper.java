/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION

 * 13-03-2018			Preeti					TENJINCG-615
 * 16-03-2018		    Preeti				    TENJINCG-615(for REST API)	
 * 02-05-2018			Preeti					TENJINCG-656
 * 03-05-2018			Preeti					TENJINCG-656
 * 18-06-2018			Preeti					T251IT-47
 * 27-06-2018           Padmavathi              T251IT-54
 * 13-07-2018           Padmavathi              connections closed
 * 30-07-2018			Preeti					Closed connections
 * 15-03-2019			Ashiki				  	TENJINCG-986
 * 26-03-2019			Roshni					TJN252-54
 * 05-02-2020			Roshni					TENJINCG-1168
 * 15-06-2021			Ashiki					TENJINCG-1275
 */

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.MessageValidate;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.defect.DefectManagementInstance;
import com.ycs.tenjin.handler.MessageValidateHandler;
import com.ycs.tenjin.handler.ModuleHandler;
import com.ycs.tenjin.project.Domain;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.ProjectMail;
import com.ycs.tenjin.project.ProjectTestDataPath;
import com.ycs.tenjin.project.TestDataPath;
import com.ycs.tenjin.testmanager.TestManagerInstance;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class ProjectHelper {
	private static final Logger logger = LoggerFactory.getLogger(ProjectHelper.class);

	
	public Hashtable<String, String> hydrateTestDataMapForProject(Connection conn, int projectId) throws DatabaseException{
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		Hashtable<String, String> tdpTable = new Hashtable<String, String>();
		

		//Get all project AUTs
		ArrayList<Aut> projectAuts = this.hydrateProjectAuts(conn, projectId);
		if(projectAuts != null && projectAuts.size() > 0){
			for(Aut aut:projectAuts){
				//For each AUT, get all modules from TJNAPP.MASMODULE
				try {
					if(aut!=null){
						ArrayList<ModuleBean> modules = new ModulesHelper().hydrateModules(conn,aut.getId());
						if(modules != null){
							for(ModuleBean module:modules){
								ProjectTestDataPath p = new ProjectTestDataPath();
								p.setAppId(aut.getId());
								p.setAppName(aut.getName());
								p.setFuncCode(module.getModuleCode());
								//For Each module, query its data path from TJNPROJ.MASTESTDATAPATH
								String tdp = "";
								try{
									pst = conn.prepareStatement("SELECT TEST_DATA_PATH FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");
									pst.setInt(1, projectId);
									pst.setInt(2, aut.getId());
									pst.setString(3, module.getModuleCode());
									rs = pst.executeQuery();

									while(rs.next()){
										tdp = rs.getString("TEST_DATA_PATH");
									}
									//pst.close();
								}catch(Exception e){
									logger.error("Could not get Test DAta path for Application --> " + aut.getName() + ", Function --> " + module.getModuleCode(),e);
									tdp = "";
								}finally{
									DatabaseHelper.close(rs);
									DatabaseHelper.close(pst);
								}
								//Fix for Defect#TEN-116 - Sriram
								/*p.setTestDataPath(tdp);*/
								p.setTestDataPath(Utilities.trim(tdp));
								//Fix for Defect#TEN-116 - Sriram ends
								//ps.add(p);
								tdpTable.put(Integer.toString(p.getAppId()) + "|" + p.getFuncCode(), p.getTestDataPath());
							}
						}
						/* modified by Roshni for TENJINCG-1168 starts */
						/*List<Api> apis = new ApiHelper().hydrateAllApi(Integer.toString(aut.getId()));*/
						List<Api> apis = new ApiHelper().hydrateAllApi(conn,Integer.toString(aut.getId()));
						/* modified by Roshni for TENJINCG-1168 ends */
						if(apis != null) {
							for(Api api: apis) {
								ProjectTestDataPath p = new ProjectTestDataPath();
								p.setAppId(aut.getId());
								p.setAppName(aut.getName());
								p.setFuncCode(api.getCode());
								//For Each module, query its data path from TJNPROJ.MASTESTDATAPATH
								String tdp = "";
								try{
									pst = conn.prepareStatement("SELECT TEST_DATA_PATH FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");
									pst.setInt(1, projectId);
									pst.setInt(2, aut.getId());
									pst.setString(3, api.getCode());
								    rs = pst.executeQuery();

									while(rs.next()){
										tdp = rs.getString("TEST_DATA_PATH");
									}
									//pst.close();
								}catch(Exception e){
									logger.error("Could not get Test DAta path for Application --> " + aut.getName() + ", Function --> " + api.getCode(),e);
									tdp = "";
								}finally{
									DatabaseHelper.close(rs);
									DatabaseHelper.close(pst);
								}
								p.setTestDataPath(Utilities.trim(tdp));
								tdpTable.put(Integer.toString(p.getAppId()) + "|" + p.getFuncCode(), p.getTestDataPath());
							}
						}
						
						
						/*Added by Ashiki for TENJINCG-1275 starts*/
						List<MessageValidate> messages = new MessageValidateHandler().hydrateAllMsgs(Integer.toString(aut.getId()));
						if(messages != null) {
							for(MessageValidate msg: messages) {
								ProjectTestDataPath p = new ProjectTestDataPath();
								p.setAppId(aut.getId());
								p.setAppName(aut.getName());
								p.setFuncCode(msg.getCode());
								String tdp = "";
								try{
									pst = conn.prepareStatement("SELECT TEST_DATA_PATH FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");
									pst.setInt(1, projectId);
									pst.setInt(2, aut.getId());
									pst.setString(3, msg.getCode());
								    rs = pst.executeQuery();

									while(rs.next()){
										tdp = rs.getString("TEST_DATA_PATH");
									}
								}catch(Exception e){
									logger.error("Could not get Test DAta path for Application --> " + aut.getName() + ", Function --> " + msg.getCode(),e);
									tdp = "";
								}finally{
									DatabaseHelper.close(rs);
									DatabaseHelper.close(pst);
								}
								p.setTestDataPath(Utilities.trim(tdp));
								tdpTable.put(Integer.toString(p.getAppId()) + "|" + p.getFuncCode(), p.getTestDataPath());
							}
						}
						
				/*Added by Ashiki for TENJINCG-1275 ends*/
					}
				} catch (SQLException e) {
					
					logger.error("COuld not fetch functions for application --> " + aut.getName(),e);
					continue;
				}finally{
					DatabaseHelper.close(rs);
					DatabaseHelper.close(pst);
				}
			}
		}

		return tdpTable;

	}
	
	/*Added by paneendra for TENJINCG-1257 starts */
	
    public Hashtable<String, String> hydrateUploadedTestDataMapForProject(Connection conn, int projectId) throws DatabaseException{
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		Hashtable<String, String> tdpTable = new Hashtable<String, String>();
		

				try {
						List<TestDataPath> testdatalist = new TestDataHelper().hydrateTestData(projectId);
						
						if(testdatalist!=null) {
							for(TestDataPath datalist:testdatalist) {
								ProjectTestDataPath p = new ProjectTestDataPath();
								p.setAppId(datalist.getAppId());
								p.setAppName(datalist.getAppName());
								p.setFuncCode(datalist.getFunction());
								
								String tdp="";
								try {
									pst = conn.prepareStatement("SELECT REPO_ROOT FROM TJN_PROJECTS WHERE prj_id = ? ");
									pst.setInt(1, projectId);
									rs = pst.executeQuery();
									while(rs.next()){
										tdp = rs.getString("REPO_ROOT");
									}
								}catch(Exception e){
									logger.error("Could not get Test DAta path for Application --> " + datalist.getAppName() + ", Function --> " + datalist.getFunction(),e);
									tdp = "";
								}finally{
									DatabaseHelper.close(rs);
									DatabaseHelper.close(pst);
								}
								
								p.setTestDataPath(Utilities.trim(tdp));
								tdpTable.put(Integer.toString(p.getAppId()) + "|" + p.getFuncCode(), p.getTestDataPath());
							}
						}

						}finally{
					DatabaseHelper.close(rs);
					DatabaseHelper.close(pst);
				}

		return tdpTable;

	}
    
    /*Added by paneendra for TENJINCG-1257 ends */
    
	public void persistTestDataPathsForProject(int projectId, ArrayList<ProjectTestDataPath> paths) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst=null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
    
		for(ProjectTestDataPath p:paths){
			 /*   Changed by Leelaprasad for Testdatapath issue starts*/
			try{
				pst = conn.prepareStatement("DELETE FROM MASTESTDATAPATH WHERE PRJ_ID = ? AND APP_ID=? AND FUNC_CODE=?");
				pst.setInt(1, projectId);
				pst.setInt(2, p.getAppId());
				pst.setString(3, p.getFuncCode());
				pst.execute();

			}catch(SQLException e){
				logger.error("Could not clear Project test data path",e);
				throw new DatabaseException("Could not update project test data paths",e);
			}finally{
				if(null!=pst){	try {
					pst.close();
				} catch (SQLException e) {
					
					logger.error("Error ", e);
				}}
			}
			 /*   Changed by Leelaprasad for Testdatapath issue ends*/
			try {
				pst = conn.prepareStatement("INSERT INTO MASTESTDATAPATH (PRJ_ID,APP_ID,FUNC_CODE,TEST_DATA_PATH) VALUES (?,?,?,?)");
				pst.setInt(1, projectId);
				pst.setInt(2, p.getAppId());
				pst.setString(3, p.getFuncCode());
				pst.setString(4, p.getTestDataPath());
				pst.execute();
				pst.close();
			} catch (SQLException e) {
				
				logger.error("COuld not update test data path for Project --> " + projectId + ", Application --> " + p.getAppId() + ", Function --> " + p.getFuncCode(),e);
			}finally{
				if(null!=pst){
					try {
						pst.close();
					} catch (SQLException e) {
						
						logger.error("Error ", e);
					}
				}
			}

		}

		if(null!=conn){
			try {
				conn.close();
			} catch (SQLException e) {
				
				logger.error("Error ", e);
			}
		}
	}
	
	public ArrayList<ProjectTestDataPath> hydrateTestDataPathsForProject(int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		/*	Changed by Padmavathi for TENJINCG-540 starts*/
		ProjectTestDataPath p = null;
			/*	Changed by Padmavathi for TENJINCG-540 ends*/
		PreparedStatement pst = null;
		PreparedStatement pst2 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<ProjectTestDataPath> ps = new ArrayList<ProjectTestDataPath>();


		//Get all project AUTs
		try{
		ArrayList<Aut> projectAuts = this.hydrateProjectAuts(projectId);
		if(projectAuts != null && projectAuts.size() > 0){
			for(Aut aut:projectAuts){
				//For each AUT, get all modules from TJNAPP.MASMODULE
				try {
					ArrayList<ModuleBean> modules = new ModulesHelper().hydrateModules(aut.getId());
					if(modules != null){
						for(ModuleBean module:modules){
							/*Added by Padmavathi for T251IT-54 starts*/
							ProjectTestDataPath p1 = new ProjectTestDataPath();
							p=	new ProjectTestDataPath();
							/*	Changed by Padmavathi for TENJINCG-540 ends*/
							p.setAppId(aut.getId());
							p.setAppName(aut.getName());
							p.setFuncCode(module.getModuleCode());
							p.setGroupName(module.getGroupName());
						/*	Added by Padmavathi for TENJINCG-540 starts*/
							p.setProjectId(projectId);
								String tdp = "";
							try{
								pst = conn.prepareStatement("SELECT TEST_DATA_PATH FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");
								pst.setInt(1, projectId);
								pst.setInt(2, aut.getId());
								pst.setString(3, module.getModuleCode());
								rs = pst.executeQuery();

								while(rs.next()){
									tdp = rs.getString("TEST_DATA_PATH");
								}
								//pst.close();
							}catch(Exception e){
								logger.error("Could not get Test DAta path for Application --> " + aut.getName() + ", Function --> " + module.getModuleCode(),e);
								tdp = "";
							}finally{
								DatabaseHelper.close(rs);
								DatabaseHelper.close(pst);
							}
							/*Modified by Padmavathi for TENJINCG-545 starts*/
							if (tdp==null || tdp.equalsIgnoreCase("")) {
								/*Added by Padmavathi for T251IT-54 starts*/
								p1=updateTestDataPath(p);
								if(p1.getTestDataPath()!=null){
									p.setTestDataPath(p1.getTestDataPath());
								}
								else{
									p.setTestDataPath("");
								}
								/*Added by Padmavathi for T251IT-54 ends*/
							}
							else{
								p.setTestDataPath(tdp);
							}
							/*Modified by Padmavathi for TENJINCG-545 ends*/
							ps.add(p);
						}
					}		
						/*	Changed by Padmavathi for TENJINCG-540 starts*/
					/* modified by Roshni for TENJINCG-1168 starts */
					/*List<Api> apis = new ApiHelper().hydrateAllApi(String.valueOf(aut.getId()));*/
					List<Api> apis = new ApiHelper().hydrateAllApi(conn,String.valueOf(aut.getId()));
					/* modified by Roshni for TENJINCG-1168 ends */
					if(apis != null) {
						for (Api api : apis) {
							/*Added by Padmavathi for T251IT-54 starts*/
							ProjectTestDataPath p1 = new ProjectTestDataPath();
							/*Added by Padmavathi for T251IT-54 ends*/
							p=new ProjectTestDataPath();
							
							p.setAppId(aut.getId());
							p.setAppName(aut.getName());
							p.setFuncCode(api.getCode());
							/*Changed by Pushpalatha for TENJINCG-568 starts*/
							p.setGroupName(api.getGroup());
							/*Changed by Pushpalatha for TENJINCG-568 ends*/
							p.setProjectId(projectId);
							String tdp = "";
							try{
								pst2 = conn.prepareStatement("SELECT * FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");
								pst2.setInt(1, projectId);
								pst2.setInt(2, aut.getId());
								pst2.setString(3, api.getCode());
								rs2 = pst2.executeQuery();

								while(rs2.next()){
									tdp = rs2.getString("TEST_DATA_PATH");
								}
								rs2.close();
								
								pst2.close();
							}catch(Exception e){
								logger.error("Could not get Test Data path for Application --> " + aut.getName() + ", API --> " +api.getCode(),e);
								tdp = "";
							}finally{
								DatabaseHelper.close(rs2);
								DatabaseHelper.close(pst2);
							}
							/*Modified by Padmavathi for TENJINCG-545 starts*/
							if (tdp==null || tdp.equalsIgnoreCase("")) {
								/*Added by Padmavathi for T251IT-54 starts*/
								p1=updateTestDataPath(p);
								if(p1.getTestDataPath()!=null){
									p.setTestDataPath(p1.getTestDataPath());
								}
								else{
									p.setTestDataPath("");
								}
								/*Added by Padmavathi for T251IT-54 ends*/
							}
							else{
								p.setTestDataPath(tdp);
							}
							/*Modified by Padmavathi for TENJINCG-545 ends*/
							ps.add(p);
						}
							/*	Changed by Padmavathi for TENJINCG-540 ends*/
					}
				} catch (SQLException e) {
					
					logger.error("COuld not fetch functions for application --> " + aut.getName(),e);
					continue;
				}
			}
		}


		}catch(Exception e){
			logger.debug(e.getMessage());
		}
		finally{	
			DatabaseHelper.close(rs2);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}


		return ps;
	}
public Project updateProject(Project project) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		try{
			pst = conn.prepareStatement("UPDATE TJN_PROJECTS SET PRJ_NAME = ?, PRJ_DESC = ?, PRJ_TYPE = ?,PRJ_SCREENSHOT_OPTIONS=? WHERE PRJ_ID = ?");
				pst.setString(1, project.getName());
			pst.setString(2, project.getDescription());
			pst.setString(3, project.getType());
			pst.setString(4, project.getScreenshotoption());
				pst.setInt(5, project.getId());
			pst.execute();
		}catch(Exception e){
			throw new DatabaseException("Could not update project",e);
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);

		}

		return project;
	}
	
	
	
	public ArrayList<Aut> hydrateUnmappedAuts(int projectId) throws DatabaseException{
		
		ArrayList<Aut> unmappedAuts = new ArrayList<Aut>();
		
		/*Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		 * 
		 * 
		 * if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
*/
		try{
			ArrayList<Aut> allAuts = new AutHelper().hydrateAllAut();
			Project project = this.hydrateProject(projectId);

			ArrayList<Aut> projectAuts = project.getAuts();

			if(allAuts != null && projectAuts != null){
				for(Aut aut:allAuts){
					int id = aut.getId();
					boolean exists = false;
					for(Aut a:projectAuts){
						if(a.getId() == id){
							exists=true;
							break;
						}
					}

					if(!exists){
						unmappedAuts.add(aut);
					}
				}
			}else{
				throw new DatabaseException("No AUTs are available to map to this project");
			}
		}catch(Exception e){
			throw new DatabaseException("Could not fetch AUTs because of an internal error",e);
		}/*finally{
			
			DatabaseHelper.close(conn);
		}*/

		return unmappedAuts;
	}

	public String persistDomain(Domain domain) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		/*modified by Priyanka for VAPT Helper fix starts*/
		//Statement st = null;
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		/*modified by Priyanka for VAPT Helper fix ends*/
		ResultSet rs = null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		String fResult = "";

		try{
			
			/*modified by Priyanka for VAPT Helper fix starts*/
			/*st = conn.createStatement();
			rs = st.executeQuery("Select * From TJN_DOMAINS Where DOMAIN_NAME = '" + domain.getName() +  "'");*/
			pst1 = conn.prepareStatement("Select * From TJN_DOMAINS Where DOMAIN_NAME =?");
			pst1.setString(1, domain.getName());
			rs = pst1.executeQuery();
			/*modified by Priyanka for VAPT Helper fix ends*/
			boolean domainFound = false;
			while(rs.next()){
				domainFound = true;
			}

			if(domainFound){
				fResult = "Domain Already Exists";
			}else{
				pst = conn.prepareStatement("INSERT INTO TJN_DOMAINS (DOMAIN_NAME,DOMAIN_CREATE_DATE,DOMAIN_CREATED_BY,DOMAIN_STATE) VALUES(?,?,?,?)");
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 ends*/
				pst.setString(1, domain.getName());
				pst.setTimestamp(2, domain.getCreatedDate());
				pst.setString(3, domain.getOwner());
				pst.setString(4, "A");

				pst.execute();
				fResult = "SUCCESS";
			}
		}catch(Exception e){
			throw new DatabaseException("Could not create record",e);
		}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(conn);
		}

		return fResult;
	}
	public void mapUsersToProject(int projectId, String userList) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		UserHelper uHelper = new UserHelper();
		try{
			String[] arr = userList.split(";");
			
			for(String userDetail:arr){
				try{
				String[] u = userDetail.split("\\|");
				String userId = u[0];
				String role = "";
				if(u.length > 1){
					role = u[1];
				}else{
					role = "Tester";
				}

				User user = uHelper.hydrateUser(userId);
				if(user != null){
					/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/
					/*PreparedStatement pst = conn.prepareStatement("INSERT INTO TJN_PRJ_USERS  VALUES (?,?,?)");*/
					pst = conn.prepareStatement("INSERT INTO TJN_PRJ_USERS (PRJ_ID,USER_ID,USER_ROLE) VALUES (?,?,?)");
					/*Changed by  Parveen for changing insert query REQ #TJN_243_04 ends*/
					pst.setInt(1, projectId);
					pst.setString(2, user.getId());
					pst.setString(3, role);
				
					pst.execute();
					pst.close();
				}else{

				}
			}catch(Exception e){

			}finally{
				DatabaseHelper.close(pst);
			}
			}

		}catch(Exception e){

		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}


	}

	public void unmapAutsFromProject(int projectId, String autList) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		try{
			String[] auts = autList.split(";");

			for(String aut:auts){
				try{
					pst = conn.prepareStatement("DELETE FROM TJN_PRJ_AUTS WHERE PRJ_ID = ? AND APP_ID = ?");
					pst.setInt(1, projectId);
					pst.setInt(2, Integer.parseInt(aut));
					pst.execute();
					pst.close();
				}catch(Exception e){
					logger.error("Could not remove AUT with ID " + aut + " from project with ID " + projectId,e);
				}
				finally{
					DatabaseHelper.close(pst);
				}
			}
		}catch(Exception e){
			throw new DatabaseException("Could not remove the selected AUTs from project",e);
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	public void unmapUsersFromProject(int projectId, String userList) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		try{
			String[] users = userList.split(";");


		    pst = conn.prepareStatement("SELECT USER_ID FROM TJN_PRJ_USERS WHERE PRJ_ID = ? AND USER_ROLE IN (?,?)");
			pst.setInt(1, projectId);
			/*Modified by padmavathi for removing user from APIS*/ 
			pst.setString(2, "Project Administrator");
			/*Modified by padmavathi for removing user from APIS ends*/ 
			pst.setString(3, "Owner");
			rs = pst.executeQuery();
			ArrayList<String> admins = new ArrayList<String>();
			while(rs.next()){
				admins.add(rs.getString("USER_ID"));
			}
			boolean allAdminsMarked = false;
			int foundCount = 0;
			if(admins != null){
				for(String admin:admins){
					for(String u:users){
						if(admin.equalsIgnoreCase(u)){
							foundCount++;
							break;
						}
					}
				}

				if(foundCount >= admins.size()){
					allAdminsMarked = true;
				}
			}

			if(allAdminsMarked){
				throw new DatabaseException("Project needs at least one Administrator");
			}

			for(String u:users){
				try{
				pst = conn.prepareStatement("DELETE FROM TJN_PRJ_USERS WHERE PRJ_ID = ? AND USER_ID = ?");
				pst.setInt(1, projectId);
				pst.setString(2, u);
				pst.execute();
				}catch(Exception e){
					throw new DatabaseException(e.getMessage(),e);
				}finally{
					DatabaseHelper.close(pst);
				}
			}

			try{
				pst.close();
				rs.close();
			}catch(Exception e){
				//logger.warn("Could not close PreparedStatement and ResultSet",e);
			}
		}catch(Exception e){
			/*Changed by Leelaprasad for the defect T25IT-239 starts*/
			/*throw new DatabaseException("Could not remove users due to an internal error",e);*/
			throw new DatabaseException(e.getMessage(),e);
			/*Changed by Leelaprasad for the defect T25IT-239 ends*/
		}finally{
			/*try{
				conn.close();
			}catch(Exception e){
				//logger.warn("Could not close connection to Tenjin Project DB",e);
			}*/
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	public void mapAutsToProject(int projectId, String userList) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		AutHelper uHelper = new AutHelper();
		try{
			String[] arr = userList.split(";");
			for(String userId:arr){
				try{
				Aut aut = uHelper.hydrateAut(Integer.parseInt(userId));
				if(aut != null){
						pst = conn.prepareStatement("INSERT INTO TJN_PRJ_AUTS (PRJ_ID,APP_ID,DTT_PPROJECT_KEY,PRJ_DEF_LOGGING,PRJ_DEF_MANAGER,PRJ_DM_PROJECT) VALUES (?,?,?,?,?,?)");
					/*Changed by  Parveen for changing insert query REQ #TJN_243_04 ends*/
					/*************
					 * Changed by Sriram for TJN_24_06 ends
					 */
					pst.setInt(1, projectId);
					pst.setInt(2, aut.getId());
					/*************
					 * Changed by Sriram for TJN_24_06
					 */
					pst.setString(3, "");
					pst.setString(4, "N");
					pst.setString(5, "");
					pst.setString(6, "");
					/*************
					 * Changed by Sriram for TJN_24_06 ends
					 */
					pst.execute();
					pst.close();
				}else{

				}
			}catch(Exception e){
				logger.error("ERROR --> Could not map AUT(s) to project",e );
				throw new DatabaseException("An internal error occurred while adding the Applications to the Project",e);
			}finally{
				DatabaseHelper.close(pst);
			}
			}

		}catch(Exception e){
			logger.error("ERROR --> Could not map AUT(s) to project",e );
			throw new DatabaseException("An internal error occurred while adding the Applications to the Project",e);
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}


	}
	public ArrayList<Aut> hydrateProjectAuts(int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		ArrayList<Aut> auts = new ArrayList<Aut>();

		try{
			pst = conn.prepareStatement("Select * from TJN_PRJ_AUTS where PRJ_ID = ?");
			pst.setInt(1, projectId);

			rs = pst.executeQuery();

			pst1=null;
			while(rs.next()){
				int appId = rs.getInt("APP_ID");
				try{
					Aut aut = new AutHelper().hydrateAut(appId);
					/*Changed by  sahana for Improvement TENJINCG-27 starts*/
					pst1 = conn.prepareStatement("Select * from TJN_PRJ_AUTS where PRJ_ID = ? and app_id=?");
					pst1.setInt(1, projectId);
					pst1.setInt(2, appId);
					rs1 = pst1.executeQuery();
					while(rs1.next()){
						if(rs1.getString("AUT_URL")!=null){
							aut.setURL(rs1.getString("AUT_URL"));

						}
						aut.setTestDataPath(rs1.getString("TEST_DATA_PATH"));

					}
					
					auts.add(aut);
					
					rs1.close();
					pst1.close();
					
				}catch(Exception e){
					logger.error("Could not fetch details of AUT " + appId);
				}finally{
					DatabaseHelper.close(rs1);
					DatabaseHelper.close(pst1);
				}
			}

		}catch(Exception e){
			throw new DatabaseException("Could not fetch AUTs for Project",e);
		}finally{
			
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return auts;
	}

	public ArrayList<Aut> hydrateProjectAuts(Connection conn, int projectId) throws DatabaseException{
		//Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<Aut> auts = new ArrayList<Aut>();

		try{
			pst = conn.prepareStatement("Select * from TJN_PRJ_AUTS where PRJ_ID = ?");
			pst.setInt(1, projectId);
			rs = pst.executeQuery();

			while(rs.next()){
				int appId = rs.getInt("APP_ID");
				try{
					Aut aut = new AutHelper().hydrateAut(appId);
					auts.add(aut);
				}catch(Exception e){
					logger.error("Could not fetch details of AUT " + appId);
				}
			}

			pst.close();
			rs.close();
		}catch(Exception e){
			throw new DatabaseException("Could not fetch AUTs for Project",e);
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return auts;
	}
	public ArrayList<User> hydrateProjectUsers(int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
		ArrayList<User> users = new ArrayList<User>();
		try{
			

			pst = conn.prepareStatement("SELECT * FROM TJN_PRJ_USERS WHERE PRJ_ID = ?");
			pst.setInt(1, projectId);
			rs = pst.executeQuery();

			while(rs.next()){
				String userId = rs.getString("USER_ID");
				String role = rs.getString("USER_ROLE");
				try{
					User user = new UserHelper().hydrateUser(userId);
					user.setRoleInCurrentProject(role);
					users.add(user);
				}catch(Exception e){
					logger.error("Could not fetch information about user " + userId,e);
				}
			}

			rs.close();
			pst.close();
		}catch(Exception e){
			throw new DatabaseException("Could not fetch users for project " + projectId,e);
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return users;

	}


	public Project hydrateProject(Connection conn,int projectId) throws DatabaseException{

		Project project = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement pst2 = null;
		ResultSet rs2 = null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		try{
			pst = conn.prepareStatement("SELECT * FROM TJN_PROJECTS WHERE PRJ_ID = ?");
			pst.setInt(1, projectId);

			rs = pst.executeQuery();

			while(rs.next()){
				project = new Project();
				project.setName(rs.getString("PRJ_NAME"));
				project.setId(rs.getInt("PRJ_ID"));
				project.setCreatedDate(rs.getTimestamp("PRJ_CREATE_DATE"));
				project.setDomain(rs.getString("PRJ_DOMAIN"));
				project.setOwner(rs.getString("PRJ_OWNER"));
				project.setType(rs.getString("PRJ_TYPE"));
				project.setDescription(rs.getString("PRJ_DESC"));
				project.setScreenshotoption(rs.getString("PRJ_SCREENSHOT_OPTIONS"));
				project.setState(rs.getString("PRJ_STATE"));
			}

			//Get Project Users
			ArrayList<User> users = new ArrayList<User>();

			pst = conn.prepareStatement("SELECT * FROM TJN_PRJ_USERS WHERE PRJ_ID = ?");
			pst.setInt(1, projectId);
			rs = pst.executeQuery();

			while(rs.next()){
				String userId = rs.getString("USER_ID");
				String role = rs.getString("USER_ROLE");
				try{
					User user = new UserHelper().hydrateUser(userId, conn);
					user.setRoleInCurrentProject(role);
					users.add(user);
				}catch(Exception e){
					logger.error("Could not fetch information about user " + userId,e);
				}
			}

			project.setUsers(users);

			//Get Project AUTs
			ArrayList<Aut> auts = new ArrayList<Aut>();
			pst = conn.prepareStatement("SELECT * FROM TJN_PRJ_AUTS WHERE PRJ_ID = ?");
			pst.setInt(1,project.getId());
			rs = pst.executeQuery();
			ArrayList<ProjectTestDataPath> ps = new ArrayList<ProjectTestDataPath>();
			while(rs.next()){
				int appId = rs.getInt("APP_ID");
				try{
					Aut aut = new AutHelper().hydrateAut(conn, appId);
					ArrayList<ModuleBean> modules = new ModulesHelper().hydrateModules(conn, aut.getId());
					if(modules != null){
						for(ModuleBean module:modules){
							/*Changed by  sahana for Improvement TENJINCG-3 starts*/
							ProjectTestDataPath p1 = new ProjectTestDataPath();
							/*Changed by  sahana for Improvement TENJINCG-3 ends*/
							ProjectTestDataPath p = new ProjectTestDataPath();
							p.setAppId(aut.getId());
							p.setAppName(aut.getName());
							p.setFuncCode(module.getModuleCode());
							/*Changed by  sahana for Improvement TENJINCG-3 starts*/
							p.setProjectId(projectId);
							/*Changed by  sahana for Improvement TENJINCG-3 ends*/
							//For Each module, query its data path from TJNPROJ.MASTESTDATAPATH
							String tdp = "";
							try{
							    pst2 = conn.prepareStatement("SELECT TEST_DATA_PATH FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");
								pst2.setInt(1, projectId);
								pst2.setInt(2, aut.getId());
								pst2.setString(3, module.getModuleCode());
								rs2 = pst2.executeQuery();

								while(rs2.next()){
									tdp = rs2.getString("TEST_DATA_PATH");
									/*Changed by  sahana for Improvement TENJINCG-3 starts*/
									//VAPT null check fix
									if (tdp==null && tdp.equalsIgnoreCase("null")) {
										p1=updateTestDataPath(p);
										if(p1.getTestDataPath()!=null){
											p.setTestDataPath(p1.getTestDataPath());
										}
										else{
											p.setTestDataPath("");
										}
									}
									else{
										p.setTestDataPath(tdp);
									}
								}
								
							}catch(Exception e){
								logger.error("Could not get Test Data path for Application --> " + aut.getName() + ", Function --> " + module.getModuleCode(),e);
								tdp = "";
							}finally{
								DatabaseHelper.close(rs2);
								DatabaseHelper.close(pst2);
							}

							ps.add(p);
						}
					}
					auts.add(aut);
				}catch(Exception e){
					logger.error("Could not fetch details of AUT " + appId);
				}
			}
			/******************************************************8
			 * Change by Sriram to add test data path attribute for project
			 */
			project.setAuts(auts);
			project.setTestDataPaths(ps);
			
			rs.close();
			pst.close();

		}catch(Exception e){
			throw new DatabaseException("Could not fetch details of project",e);
		}finally{
			
			DatabaseHelper.close(rs2);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return project;
	}
	public Project hydrateProject(int projectId) throws DatabaseException{
		/* commented by Roshni for TENJINCG-1168 starts */
	/*	Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		Connection coreConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);*/
		/* commented by Roshni for TENJINCG-1168 ends */
		Connection appConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		Project project = null;
		PreparedStatement pst = null;
		PreparedStatement pst2 = null;
		PreparedStatement pst3 = null;
		PreparedStatement pst4 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		ResultSet rsMas =null;
		ResultSet rs3 = null;
		ResultSet rs4 = null;
		ResultSet rs6 = null;
		/* modified by Roshni for TENJINCG-1168 starts */
		/*if(conn == null || appConn == null || coreConn == null)*/
		if( appConn == null ){
			throw new DatabaseException("Invalid Database Connection");
		}

		try{
			/*pst = conn.prepareStatement("SELECT * FROM TJN_PROJECTS WHERE PRJ_ID = ?");*/
			pst = appConn.prepareStatement("SELECT * FROM TJN_PROJECTS WHERE PRJ_ID = ?");
			/* modified by Roshni for TENJINCG-1168 ends */
			pst.setInt(1, projectId);

			rs = pst.executeQuery();

			while(rs.next()){
				project = new Project();
				project.setName(rs.getString("PRJ_NAME"));
				project.setId(rs.getInt("PRJ_ID"));
				project.setCreatedDate(rs.getTimestamp("PRJ_CREATE_DATE"));
				project.setDomain(rs.getString("PRJ_DOMAIN"));
				project.setOwner(rs.getString("PRJ_OWNER"));
				project.setType(rs.getString("PRJ_TYPE"));
				project.setDescription(rs.getString("PRJ_DESC"));
				project.setScreenshotoption(rs.getString("PRJ_SCREENSHOT_OPTIONS"));
				project.setState(rs.getString("PRJ_STATE"));
			}

			//Get Project Users
			ArrayList<User> users = new ArrayList<User>();
			/* modified by Roshni for TENJINCG-1168 starts */
			/*pst = conn.prepareStatement("SELECT * FROM TJN_PRJ_USERS WHERE PRJ_ID = ?");*/
			pst = appConn.prepareStatement("SELECT * FROM TJN_PRJ_USERS WHERE PRJ_ID = ?");
			/* modified by Roshni for TENJINCG-1168 ends */
			pst.setInt(1, projectId);
			rs = pst.executeQuery();

			while(rs.next()){
				String userId = rs.getString("USER_ID");
				String role = rs.getString("USER_ROLE");
				try{
				/* modified by Roshni for TENJINCG-1168 starts */
					/*User user = new UserHelper().hydrateUser(userId, coreConn);*/
					User user = new UserHelper().hydrateUser(userId, appConn);
				/* modified by Roshni for TENJINCG-1168 starts */
					user.setRoleInCurrentProject(role);
					users.add(user);
				}catch(Exception e){
					logger.error("Could not fetch information about user " + userId,e);
				}
			}

			project.setUsers(users);

			//Get Project AUTs
			ArrayList<Aut> auts = new ArrayList<Aut>();
			/* modified by Roshni for TENJINCG-1168 starts */
			/*pst = conn.prepareStatement("SELECT * FROM TJN_PRJ_AUTS WHERE PRJ_ID = ?");*/
			pst = appConn.prepareStatement("SELECT * FROM TJN_PRJ_AUTS WHERE PRJ_ID = ?");
			/* modified by Roshni for TENJINCG-1168 ends */
			pst.setInt(1,project.getId());
			rs = pst.executeQuery();
			ArrayList<ProjectTestDataPath> ps = new ArrayList<ProjectTestDataPath>();
			while(rs.next()){
				int appId = rs.getInt("APP_ID");
				try{
					/*Modified by Preeti for T251IT-47 starts*/
					/*Aut aut = new AutHelper().hydrateAut(appConn, appId);*/
					Aut aut = new AutsHelper().hydrateAut(appConn, appId);
					/*Modified by Preeti for T251IT-47 ends*/
					ArrayList<ModuleBean> modules = new ModulesHelper().hydrateModules(appConn, aut.getId());
					if(modules != null){
						for(ModuleBean module:modules){
							ProjectTestDataPath p = new ProjectTestDataPath();
							/*Changed by  sahana for Improvement TENJINCG-3 starts*/
							ProjectTestDataPath p1 = new ProjectTestDataPath();
							/*Changed by  sahana for Improvement TENJINCG-3 ends*/
							p.setAppId(aut.getId());
							p.setAppName(aut.getName());
							p.setFuncCode(module.getModuleCode());
							p.setGroupName(module.getGroupName());
							/*Changed by  sahana for Improvement TENJINCG-3 starts*/
							p.setProjectId(projectId);
							/*Changed by  sahana for Improvement TENJINCG-3 ends*/
							//For Each module, query its data path from TJNPROJ.MASTESTDATAPATH
							String tdp = "";

							try{
								//PreparedStatement pst2 = conn.prepareStatement("SELECT TEST_DATA_PATH FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");
								/* modified by Roshni for TENJINCG-1168 starts */
								/*pst2 = conn.prepareStatement("SELECT * FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");*/
								pst2 = appConn.prepareStatement("SELECT * FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");
								/* modified by Roshni for TENJINCG-1168 ends */
								pst2.setInt(1, projectId);
								pst2.setInt(2, aut.getId());
								pst2.setString(3, module.getModuleCode());
								rs2 = pst2.executeQuery();

								while(rs2.next()){
									tdp = rs2.getString("TEST_DATA_PATH");
									/*Changed by  sahana for Improvement TENJINCG-3 starts*/
									p.setTestDataPath(rs2.getString("TEST_DATA_PATH"));
									p.setFuncCode(rs2.getString("FUNC_CODE"));
									if (tdp==null || tdp.equalsIgnoreCase("null")) {
										p1=updateTestDataPath(p);
										//VAPT null check fix
										if(p1.getTestDataPath()!=null && !p1.getTestDataPath().equalsIgnoreCase("null")){
											p.setTestDataPath(p1.getTestDataPath());
										}
										else{
											p.setTestDataPath("");
										}
										/*Changed by  sahana for Improvement TENJINCG-3 ends*/
									}
									else{
										p.setTestDataPath(tdp);
									}
								}
								/*Changed by  sahana for Improvement TENJINCG-3 ends*/
								/*rs2.close();
								pst2.close();*/
							}catch(Exception e){
								logger.error("Could not get Test Data path for Application --> " + aut.getName() + ", Function --> " + module.getModuleCode(),e);
								tdp = "";
							}finally{
								DatabaseHelper.close(rs2);
								DatabaseHelper.close(pst2);
							}

							ps.add(p);
						}
						/* modified by Roshni for TENJINCG-1168 starts */
						/*List<Api> apis = new ApiHelper().hydrateAllApi(Integer.toString(aut.getId()));*/
						List<Api> apis = new ApiHelper().hydrateAllApi(appConn,Integer.toString(aut.getId()));
						/* modified by Roshni for TENJINCG-1168 ends */
						for(Api api:apis) {
							ProjectTestDataPath p = new ProjectTestDataPath();
							ProjectTestDataPath p1 = new ProjectTestDataPath();
							p.setAppId(aut.getId());
							p.setAppName(aut.getName());
							p.setFuncCode(api.getCode());
							p.setGroupName(api.getGroup());
							/*added by Padmavathi for TENJINCG-545 ends*/
							p.setProjectId(projectId);
							String tdp = "";
							
							try{
								/* modified by Roshni for TENJINCG-1168 starts */
								/*pst2 = conn.prepareStatement("SELECT * FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");*/
								pst2 = appConn.prepareStatement("SELECT * FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");
								/* modified by Roshni for TENJINCG-1168 ends */
								pst2.setInt(1, projectId);
								pst2.setInt(2, aut.getId());
								pst2.setString(3, api.getCode());
								rs2 = pst2.executeQuery();

								while(rs2.next()){
									tdp = rs2.getString("TEST_DATA_PATH");
									p.setTestDataPath(rs2.getString("TEST_DATA_PATH"));
									p.setFuncCode(rs2.getString("FUNC_CODE"));
									if (tdp==null || tdp.equalsIgnoreCase("null")) {
										p1=updateTestDataPath(p);
										//VAPT null check fix changed || to &&
										if(p1.getTestDataPath()!=null && !p1.getTestDataPath().equalsIgnoreCase("null")){
											p.setTestDataPath(p1.getTestDataPath());
										}
										else{
											p.setTestDataPath("");
										}
									}
									else{
										p.setTestDataPath(tdp);
									}
								}
								/*rs2.close();
								pst2.close();*/
							}catch(Exception e){
								logger.error("Could not get Test Data path for Application --> " + aut.getName() + ", Function --> " + api.getCode(),e);
								tdp = "";
							}finally{
								DatabaseHelper.close(rs2);
								DatabaseHelper.close(pst2);
							}

							ps.add(p);
						}
						
							}
					/* modified by Roshni for TENJINCG-1168 starts */
					/*pst3 = conn.prepareStatement("SELECT TEST_DATA_PATH,AUT_URL,PRJ_DEF_MANAGER,PRJ_DM_PROJECT,DTT_PPROJECT_KEY,PRJ_DEF_LOGGING FROM TJN_PRJ_AUTS WHERE APP_ID=? AND PRJ_ID=?");*/
					pst3 = appConn.prepareStatement("SELECT TEST_DATA_PATH,AUT_URL,PRJ_DEF_MANAGER,PRJ_DM_PROJECT,DTT_PPROJECT_KEY,PRJ_DEF_LOGGING FROM TJN_PRJ_AUTS WHERE APP_ID=? AND PRJ_ID=?");
					/* modified by Roshni for TENJINCG-1168 ends */
					pst3.setInt(1, appId);
					pst3.setInt(2, projectId);
					rs3 = pst3.executeQuery();
					while(rs3.next()){
						aut.setURL(rs3.getString("AUT_URL"));
						aut.setTestDataPath(rs3.getString("TEST_DATA_PATH"));
						/*Changed by  sahana for bug#TENJINCG-36: starts*/
						/*changed by sahana for Improvement #TENJINCG-24:ends*/
						if(aut.getURL()==null)
						{
							/*pst = conn.prepareStatement("SELECT * FROM MASAPPLICATION WHERE APP_ID=?");*/
							pst = appConn.prepareStatement("SELECT * FROM MASAPPLICATION WHERE APP_ID=?");
							pst.setInt(1,appId);
							
							rsMas = pst.executeQuery();
							while(rsMas.next()){
								aut.setURL(rsMas.getString("APP_URL"));
							}
							/*Changed by Leelaprasad for the Requirement TENJINCG-39 ends*/
						}
						/*Changed by  sahana for bug#TENJINCG-36: ends*/
						aut.setInstance(rs3.getString("PRJ_DEF_MANAGER"));
						aut.setDttProject(rs3.getString("PRJ_DM_PROJECT"));
						aut.setDttProjectKey(rs3.getString("DTT_PPROJECT_KEY"));
						aut.setDttEnableStatus(rs3.getString("PRJ_DEF_LOGGING"));
					}
					rs3.close();
					pst3.close();

					/* modified by Roshni for TENJINCG-1168 starts */
					/*pst4 = conn.prepareStatement("SELECT TOOL_NAME FROM TJN_DEF_TOOL_MASTER WHERE INSTANCE_NAME=?");*/
					pst4 = appConn.prepareStatement("SELECT TOOL_NAME FROM TJN_DEF_TOOL_MASTER WHERE INSTANCE_NAME=?");
					/* modified by Roshni for TENJINCG-1168 ends */
					pst4.setString(1, aut.getInstance());
					rs4 = pst4.executeQuery();
					while(rs4.next()){
						aut.setTool(rs4.getString("TOOL_NAME"));
					}
					rs4.close();
					pst4.close();
					/*added by manish for mapping DTT instance and DTT project to project and aut on 06-Dec-2016 ends*/
					auts.add(aut);
				}catch(Exception e){
					logger.error("Could not fetch details of AUT " + appId);
				}
			}
			/* modified by Roshni for TENJINCG-1168 starts */
			/*pst2 = conn.prepareStatement("SELECT * FROM TJN_PRJ_TM_LINKAGE WHERE PRJ_ID=?");*/
			pst2 = appConn.prepareStatement("SELECT * FROM TJN_PRJ_TM_LINKAGE WHERE PRJ_ID=?");
			/* modified by Roshni for TENJINCG-1168 ends */
			pst2.setInt(1, projectId);
			rs6 = pst2.executeQuery();
			while(rs6.next()){
				project.setEtmInstance(rs6.getString("TM_INSTANCE_NAME"));
				project.setEtmProject(rs6.getString("TM_PROJECT"));
				project.setEtmEnable(rs6.getString("TM_ENABLED"));
				project.setTcFilter(rs6.getString("TC_FILTER"));
				project.setTcFilterValue(rs6.getString("TC_FILTER_VALUE"));
				project.setTsFilter(rs6.getString("TS_FILTER"));
				project.setTsFilterValue(rs6.getString("TS_FILTER_VALUE"));
				/* Added by Roshni */
				project.setTcDisplayValue(rs6.getString("TC_DISPLAY_VALUE"));
				project.setTsDisplayValue(rs6.getString("TS_DISPLAY_VALUE"));
			}
			rs6.close();
			pst2.close();
			//Added  by Parveen for Requirment #TENJINCG_55 and TENJINCG_56 ends
			project.setAuts(auts);
			project.setTestDataPaths(ps);
			
			/* modified by Roshni for TENJINCG-1168 starts */
			/*pst4 = conn.prepareStatement("SELECT * FROM TJN_PRJ_TM_ATTRIBUTES WHERE PRJ_ID=?");*/
			pst4 = appConn.prepareStatement("SELECT * FROM TJN_PRJ_TM_ATTRIBUTES WHERE PRJ_ID=?");
			/* modified by Roshni for TENJINCG-1168 ends */
			pst4.setInt(1, projectId);
			rs4 = pst4.executeQuery();
			List<TestManagerInstance> tmInstances = new ArrayList<TestManagerInstance>();
			while(rs4.next()){
				TestManagerInstance tmInstance = new TestManagerInstance();
				tmInstance.setTjnField(rs4.getString("TJN_FIELD_NAME"));
				tmInstance.setTmField(rs4.getString("TM_FIELD_NAME"));
				tmInstances.add(tmInstance);
			}
			project.setTmInstances(tmInstances);
			if(null!=rs4)rs4.close();
			if(null!=pst4)pst4.close();
			/*added by manish for TENJJINCG-57,58 on 31-Jan-2017 ends*/

		}catch(Exception e){
			throw new DatabaseException("Could not fetch details of project",e);
		}finally{
			
			DatabaseHelper.close(rs6);
			DatabaseHelper.close(rs4);
			DatabaseHelper.close(rs3);
			DatabaseHelper.close(rsMas);
			DatabaseHelper.close(rs2);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst4);
			DatabaseHelper.close(pst3);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(appConn);
			/*DatabaseHelper.close(coreConn);
			DatabaseHelper.close(conn);*/
		}

		return project;
	}


	public List<DefectManagementInstance> fetchDefectManagerInstance()
			throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		Statement stmt = null;
		ResultSet rs = null;
		List<DefectManagementInstance> defectManager = new ArrayList<DefectManagementInstance>();

		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		try {
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("SELECT DISTINCT INSTANCE_NAME,TOOL_NAME FROM TJN_DEF_TOOL_MASTER");
			while (rs.next()) {
				DefectManagementInstance defManager = new DefectManagementInstance();
				defManager.setInstance(rs.getString("INSTANCE_NAME"));
				defManager.setTool(rs.getString("TOOL_NAME"));
				defectManager.add(defManager);
			}

		} catch (SQLException e) {
			
			logger.error("ERROR deactivating project", e);
			throw new DatabaseException("Could not deactivate project");
		}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(stmt);
			DatabaseHelper.close(conn);
		}
		return defectManager;
	}
	/*changed by sahana for Improvement #TENJINCG-27:Starts*/
	public void updateAutUrl(Aut aut,int pId) throws DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try {
			pst = conn.prepareStatement("UPDATE TJN_PRJ_AUTS SET AUT_URL = ?,TEST_DATA_PATH=? WHERE APP_ID = ? AND PRJ_ID=? ");
			pst.setString(1, aut.getURL());
			pst.setString(2, aut.getTestDataPath());
			pst.setInt(3, aut.getId());
			pst.setInt(4, pId);
			pst.executeUpdate();

			pst.close();
		} catch (SQLException e) {
			logger.error("Could not update TJN_PRJ_AUTS due to "+e.getMessage());
		}finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}
	
	public Project updateDefMngrProject(Project project, int aId ) throws DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst=null;

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
			/* modified By Ashiki TENJINCG-986 starts */
		try {
			pst = conn
					.prepareStatement("UPDATE TJN_PRJ_AUTS SET PRJ_DEF_MANAGER = ?, PRJ_DM_PROJECT = ?, PRJ_DEF_LOGGING=?, DTT_PPROJECT_KEY=?, PROJECT_SET_NAME=? WHERE APP_ID = ? AND PRJ_ID=?");
			/* modified By Ashiki TENJINCG-986 starts */
			pst.setString(1, project.getInstance());
			pst.setString(2, project.getDefProject());
			pst.setString(3, project.getDttEnable());
			pst.setString(4, project.getDttKey());
			/* Added By Ashiki TENJINCG-986 starts */
			pst.setString(5, project.getSubSet());
			/* Added By Ashiki TENJINCG-986 ends */
			pst.setInt(6, aId);
			pst.setInt(7, project.getId());

			pst.executeUpdate();
		} catch (SQLException e) {
			logger.error("Error-----------> during updating mapping data to project {} and aut {}",project.getId(),aId);
		}finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return project;
	}

	public Project hydrateProject(String domainName, String projectName) throws DatabaseException{
		Project project = null;

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			pst = conn.prepareStatement("SELECT * FROM TJN_PROJECTS WHERE PRJ_NAME=? and PRJ_DOMAIN=?");
			pst.setString(1, projectName);
			pst.setString(2, domainName);
			rs = pst.executeQuery();

			while(rs.next()){
				project = new Project();
				project.setName(rs.getString("PRJ_NAME"));
				project.setId(rs.getInt("PRJ_ID"));
				project.setCreatedDate(rs.getTimestamp("PRJ_CREATE_DATE"));
				project.setDomain(rs.getString("PRJ_DOMAIN"));
				project.setOwner(rs.getString("PRJ_OWNER"));
				project.setType(rs.getString("PRJ_TYPE"));
				project.setDescription(rs.getString("PRJ_DESC"));
				project.setScreenshotoption(rs.getString("PRJ_SCREENSHOT_OPTIONS"));
				project.setState(rs.getString("PRJ_STATE"));
				ProjectMail projectMail=new ProjectMailHelper().hydrateProjectMail(rs.getInt("PRJ_ID"));
				project.setProjectMail(projectMail);
				/*Added by Preeti for TENJINCG-656 ends*/
			}

			rs.close();
			pst.close();
		} catch (SQLException e) {
			
			logger.error("ERROR while hydrating Project [{}] under domain [{}]", projectName, domainName, e);
			throw new DatabaseException("Could not validate Project information. Please contact Tenjin Support");
		}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return project;
	}

	public Project hydrateProject(String domainName, String projectName, String userId) throws DatabaseException{
		Project project = null;

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			pst = conn.prepareStatement("SELECT A.* FROM TJN_PROJECTS A, TJN_PRJ_USERS B WHERE B.PRJ_ID=A.PRJ_ID AND B.USER_ID=? AND A.PRJ_NAME=? and a.prj_domain=?");
			pst.setString(1, userId);
			pst.setString(2, projectName);
			pst.setString(3, domainName);
			rs = pst.executeQuery();

			while(rs.next()){
				project = new Project();
				project.setName(rs.getString("PRJ_NAME"));
				project.setId(rs.getInt("PRJ_ID"));
				project.setCreatedDate(rs.getTimestamp("PRJ_CREATE_DATE"));
				project.setDomain(rs.getString("PRJ_DOMAIN"));
				project.setOwner(rs.getString("PRJ_OWNER"));
				project.setType(rs.getString("PRJ_TYPE"));
				project.setDescription(rs.getString("PRJ_DESC"));
				project.setScreenshotoption(rs.getString("PRJ_SCREENSHOT_OPTIONS"));
				project.setState(rs.getString("PRJ_STATE"));
			}

			rs.close();
			pst.close();
		} catch (SQLException e) {
			
			logger.error("ERROR while hydrating Project [{}] under domain [{}]", projectName, domainName, e);
			throw new DatabaseException("Could not validate Project information. Please contact Tenjin Support");
		}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return project;
	}
	public void checkinstanceCredintials(String userId,String instance) throws DatabaseException{
			
			Connection conn = DatabaseHelper
					.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement pst = null;
			ResultSet rs = null;
			int name=0;
			try {
				pst = conn.prepareStatement("SELECT * FROM TJN_TM_USER_MAPPING where TJN_USER_ID=? and TJN_INSTANCE_NAME=?");
				pst.setString(1, userId);
				pst.setString(2, instance);
				
				rs = pst.executeQuery();
				
				
				while(rs.next()){
					name++;
				}
				if(name==0)
				{
					logger.info(instance+" does not  have  credentials");
					throw new DatabaseException(instance+" does not  have  credentials");
				}
				
			} catch (SQLException e) {
				logger.error("Could not hydrate hydrate TMCredentials ForProject ",instance,e);
				
				
			}finally {
				/*try {
					conn.close();
				} catch (Exception e) {

				}*/
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);

			}
			
		}
		public ProjectTestDataPath updateTestDataPath(ProjectTestDataPath p) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst2 = null;
		ResultSet rs = null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
		try{
			pst2 = conn.prepareStatement("SELECT TEST_DATA_PATH FROM TJN_PRJ_AUTS WHERE PRJ_ID = ? AND APP_ID = ?");
			pst2.setInt(1, p.getProjectId());
			pst2.setInt(2, p.getAppId());
			rs=pst2.executeQuery();
			while(rs.next()){
				p.setTestDataPath(rs.getString("TEST_DATA_PATH"));
			}
			pst2 = conn.prepareStatement("UPDATE MASTESTDATAPATH SET TEST_DATA_PATH =? WHERE PRJ_ID = ? AND APP_ID = ? AND FUNC_CODE=?");
			pst2.setString(1,p.getTestDataPath());
			pst2.setInt(2, p.getProjectId());
			pst2.setInt(3, p.getAppId());
			pst2.setString(4,p.getFuncCode() );
			pst2.executeUpdate();

		}catch(Exception e){
			logger.error("Could not update test data path for project due to "+e.getMessage());
			throw new DatabaseException("Failed to update test data path",e);
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(conn);
		}

		return p;
	}
	
	/*Changed by  sahana for Improvement TENJINCG-3 ends*/

	public ArrayList<ProjectTestDataPath> fetchAppTestDataPath(int appId, int projId) throws DatabaseException {
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		Connection appConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		/*	Added by Padmavathi for TENJINCG-540 starts*/
		ProjectTestDataPath p = null;
		ProjectTestDataPath p1 =null;
		/*	Added by Padmavathi for TENJINCG-540 ends*/
		PreparedStatement pst =null;
		PreparedStatement pst2 =null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		ArrayList<ProjectTestDataPath> ps = new ArrayList<ProjectTestDataPath>();
		try{
			pst = conn.prepareStatement("SELECT * FROM TJN_PRJ_AUTS WHERE PRJ_ID = ? AND APP_ID=?");
			pst.setInt(1,projId);
			pst.setInt(2,appId);
			rs = pst.executeQuery();

			while(rs.next()){
				int appId1 = rs.getInt("APP_ID");
				Aut aut = new AutHelper().hydrateAut(appConn, appId1);
				ArrayList<ModuleBean> modules = new ModulesHelper().hydrateModules(appConn, aut.getId());
				if(modules != null){
					for(ModuleBean module:modules){
					/*	Added by Padmavathi for TENJINCG-540 starts*/
						p=new ProjectTestDataPath();
						p1= new ProjectTestDataPath();
						/*	Added by Padmavathi for TENJINCG-540 ends*/
						p.setAppId(aut.getId());
						p.setAppName(aut.getName());
						p.setFuncCode(module.getModuleCode());
						p.setGroupName(module.getGroupName());
						p.setProjectId(projId);
						//For Each module, query its data path from TJNPROJ.MASTESTDATAPATH
						String tdp = "";
						try{
							pst2 = conn.prepareStatement("SELECT TEST_DATA_PATH FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");
							pst2.setInt(1, projId);
							pst2.setInt(2, aut.getId());
							pst2.setString(3, module.getModuleCode());
							rs2 = pst2.executeQuery();

							while(rs2.next()){
								tdp = rs2.getString("TEST_DATA_PATH");
							//if (tdp.equalsIgnoreCase("null")) {
								if (tdp==null) {
									p1=updateTestDataPath(p);
									/*Modified by Padmavathi for TENJINCG-545 starts*/
									if(p1.getTestDataPath()==null){
										p.setTestDataPath("");
									}
									else{
									p.setTestDataPath(p1.getTestDataPath());
									}
									/*Modified by Padmavathi for TENJINCG-545 ends*/
								}
								else{
									p.setTestDataPath(tdp);
								}
							}
							/*rs2.close();
							pst2.close();*/
						}catch(SQLException e){
							logger.error("Could not get Test Data path for Application --> " + aut.getName() + ", Function --> " + module.getModuleCode(),e);
							tdp = "";
						}finally{
							DatabaseHelper.close(rs2);
							DatabaseHelper.close(pst2);
						}
						ps.add(p);
					}
				}
				/*	Added by Padmavathi for TENJINCG-540 starts*/
				/*List<Api> apis = new ApiHelper().hydrateAllApi(String.valueOf(appId1));*/
				List<Api> apis = new ApiHelper().hydrateAllApi(appConn,String.valueOf(appId1));
				if(apis != null) {
					for (Api api : apis) {
						p=new ProjectTestDataPath();
						p1= new ProjectTestDataPath();
						p.setAppId(aut.getId());
						p.setAppName(aut.getName());
						p.setFuncCode(api.getCode());
						/*Changed by Pushpalatha for TENJINCG-568 starts*/
						p.setGroupName(api.getGroup());
						/*Changed by Pushpalatha for TENJINCG-568 ends*/
						p.setProjectId(projId);
						String tdp = "";
						try{
							pst2 = conn.prepareStatement("SELECT * FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");
							pst2.setInt(1, projId);
							pst2.setInt(2, aut.getId());
							pst2.setString(3, api.getCode());
							rs2 = pst2.executeQuery();

							while(rs2.next()){
								tdp = rs2.getString("TEST_DATA_PATH");
								//if (tdp.equalsIgnoreCase("null")) {
								if (tdp==null) {
									p1=updateTestDataPath(p);
									/*Modified by Padmavathi for TENJINCG-545 starts*/
									if(p1.getTestDataPath()==null){
										p.setTestDataPath("");
									}else{
									p.setTestDataPath(p1.getTestDataPath());
									}
									/*Modified by Padmavathi for TENJINCG-545 ends*/
								}
								else{
									p.setTestDataPath(tdp);
								}
							}
							rs2.close();
							
							pst2.close();
						}catch(Exception e){
							logger.error("Could not get Test Data path for Application --> " + aut.getName() + ", API --> " +api.getCode(),e);
							tdp = "";
						}finally{
							DatabaseHelper.close(rs2);
							DatabaseHelper.close(pst2);
						}
						ps.add(p);
					}
				}/*	Added by Padmavathi for TENJINCG-540 ends*/
			}
		}catch(Exception e){

		}finally{
			DatabaseHelper.close(rs2);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(appConn);
			DatabaseHelper.close(conn);
		}


		return ps;
	}
	/*added by Roshni for fix 152 starts*/
	public int getProjectId(String prjname,String domainName) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		new ArrayList<Aut>();
		int projectId=0;
		try{
			pstmt = conn.prepareStatement("Select PRJ_ID from TJN_PROJECTS where PRJ_NAME = ? and PRJ_DOMAIN=?");
			pstmt.setString(1, prjname);
			pstmt.setString(2, domainName);
			rst=pstmt.executeQuery();
			if(rst.next())
			{
				projectId=rst.getInt("PRJ_ID");
			}
		
			}catch(Exception e){
				//logger.warn("Could not close Tenjin Project DB Connection",e);
			}finally {
				DatabaseHelper.close(rst);
				DatabaseHelper.close(pstmt);
				DatabaseHelper.close(conn);
			}

		return projectId;
	}
	
	public boolean checkAdminOfProject(int prjId, String userId) throws DatabaseException
	{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean isAdmin=false;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
		try {
			pst = conn.prepareStatement("SELECT USER_ID FROM TJN_PRJ_USERS WHERE PRJ_ID = ? AND USER_ROLE IN (?,?)");
			pst.setInt(1, prjId);
			pst.setString(2, "Administrator");
			pst.setString(3, "Owner");
			rs = pst.executeQuery();
			ArrayList<String> admins = new ArrayList<String>();
			while(rs.next()){
				admins.add(rs.getString("USER_ID"));
			}
			if (admins != null) {
				for (String admin : admins) {
					if (admin.equals(userId)) {
						isAdmin = true;
						break;
					}
				}
			}

		} catch (SQLException e) {
			
			logger.error("Error ", e);
		}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return isAdmin;
	}
	public int countAdminOfProject(int prjId) throws DatabaseException
	{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		int found=0;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
		try {
			pst = conn.prepareStatement("SELECT USER_ID FROM TJN_PRJ_USERS WHERE PRJ_ID = ? AND USER_ROLE IN (?,?)");
			pst.setInt(1, prjId);
			pst.setString(2, "Administrator");
			pst.setString(3, "Owner");
		    rs = pst.executeQuery();
			ArrayList<String> admins = new ArrayList<String>();
			while(rs.next()){
				admins.add(rs.getString("USER_ID"));
			}
			if (admins != null) {
				for (String admin : admins) {
					found++;
				}
			}

		} catch (SQLException e) {
			
			logger.error("Error ", e);
		}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return found;
	}
	/*public boolean checkProjectExist(String dname, String pname) throws DatabaseException*/
	public boolean checkProjectExist(String dname, String pname,int projectId) throws DatabaseException
	{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
		boolean exist=false;
		try {
			/*PreparedStatement pst = conn.prepareStatement("SELECT PRJ_NAME FROM TJN_PROJECTS WHERE PRJ_DOMAIN=?");*/
			pst = conn.prepareStatement("SELECT PRJ_NAME ,PRJ_ID FROM TJN_PROJECTS WHERE PRJ_DOMAIN=?");
			pst.setString(1, dname);
			rs = pst.executeQuery();
			/*ArrayList<String> pnames = new ArrayList<String>();*/
			ArrayList<Project> projects = new ArrayList<Project>();
			while(rs.next()){
				/*pnames.add(rs.getString("PRJ_NAME"));*/
				Project project=new Project();
				/*Modified by Pushpa for TJN29AB-2 starts*/
				/*String iD=rs.getString("PRJ_ID");
				project.setId(Integer.parseInt(iD));*/
				project.setId(rs.getInt("PRJ_ID"));
				/*Modified by Pushpa for TJN29AB-2 ends*/
				project.setName(rs.getString("PRJ_NAME"));;
				projects.add(project);
			}
			
			
			if(projects!=null)
			{
				for(Project project:projects)
				{
					if(project.getName().equalsIgnoreCase(pname))
					{
						if(project.getId()!=projectId){
						exist=true;
						break;
						}
					}
						
				}
			}
		} catch (SQLException e) {
			
			logger.error("Error ", e);
		}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return exist;
	}
/*added by Roshni for fix 152 ends*/
	
	
	/*Added by Gangadhar Badagi for TENJINCG-539 starts*/
public void persistTestDataPathForFunction(ProjectTestDataPath testDataPath) throws DatabaseException{
	Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
	PreparedStatement pst=null;

	if(conn == null){
		throw new DatabaseException("Invalid or no database connection");
	}

	try{
		pst = conn.prepareStatement("DELETE FROM MASTESTDATAPATH WHERE PRJ_ID = ? AND APP_ID=? AND FUNC_CODE=?");
		pst.setInt(1, testDataPath.getProjectId());
		pst.setInt(2, testDataPath.getAppId());
		pst.setString(3, testDataPath.getFuncCode());
		pst.execute();
	}catch(SQLException e){
		logger.error("Could not clear Project test data path",e);
		throw new DatabaseException("Could not update test data path for Project");
		
	}finally{
		if(null!=pst){	try {
			pst.close();
		} catch (SQLException e) {
			
		}
		}
	}
	
		try {
			pst = conn.prepareStatement("INSERT INTO MASTESTDATAPATH (PRJ_ID,APP_ID,FUNC_CODE,TEST_DATA_PATH) VALUES (?,?,?,?)");
			pst.setInt(1, testDataPath.getProjectId());
			pst.setInt(2, testDataPath.getAppId());
			pst.setString(3, testDataPath.getFuncCode());
			pst.setString(4, testDataPath.getTestDataPath());
			pst.execute();
			
		} catch (SQLException e) {
			logger.error("Could not update test data path for Project --> " + testDataPath.getProjectId() + ", Application --> " + testDataPath.getAppId() + ", Function --> " + testDataPath.getFuncCode(),e);
			throw new DatabaseException("Could not update test data path for Project --> " + testDataPath.getProjectId() + ", Application --> " + testDataPath.getAppId() + ", Function --> " + testDataPath.getFuncCode(),e);
		}finally{
			if(null!=pst){
				try {
					pst.close();
				} catch (SQLException e) {
				}
			}
		}
	if(null!=conn){
		try {
			pst.close();
			conn.close();
		} catch (SQLException e) {
		}
	}
}
/*Added by Gangadhar Badagi for TENJINCG-539 starts*/
   public void persistTestDataPathForApplication(ProjectTestDataPath testDataPath) throws DatabaseException{
	Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
	PreparedStatement pst=null;
	PreparedStatement pst2=null;
	if(conn == null){
		throw new DatabaseException("Invalid or no database connection");
	}

	try{
		pst = conn.prepareStatement("DELETE FROM MASTESTDATAPATH WHERE PRJ_ID = ? AND APP_ID=?");
		pst.setInt(1, testDataPath.getProjectId());
		pst.setInt(2, testDataPath.getAppId());
		pst.execute();
	}catch(SQLException e){
		logger.error("Could not clear Project test data path",e);
		throw new DatabaseException("Could not update test data path for Project");
		
	}finally{
		if(null!=pst){	try {
			pst.close();
		} catch (SQLException e) {
			
		}
		}
	}
	
		try {
			List<Module> listOfModules=new ModuleHandler().getModules(testDataPath.getAppId());
			pst = conn.prepareStatement("INSERT INTO MASTESTDATAPATH (PRJ_ID,APP_ID,FUNC_CODE,TEST_DATA_PATH) VALUES (?,?,?,?)");
			
			for(Module module:listOfModules){
				pst.setInt(1, testDataPath.getProjectId());
				pst.setInt(2, testDataPath.getAppId());
				pst.setString(3, module.getCode());
				pst.setString(4, testDataPath.getTestDataPath());
				pst.addBatch();

			}
			pst.executeBatch();
			
			/*List<Api> listOfApis=new ApiHelper().hydrateAllApi(String.valueOf(testDataPath.getAppId()));*/
			List<Api> listOfApis=new ApiHelper().hydrateAllApi(conn,String.valueOf(testDataPath.getAppId()));
			pst2 = conn.prepareStatement("INSERT INTO MASTESTDATAPATH (PRJ_ID,APP_ID,FUNC_CODE,TEST_DATA_PATH) VALUES (?,?,?,?)");
			for(Api api:listOfApis){
				pst2.setInt(1, testDataPath.getProjectId());
				pst2.setInt(2, testDataPath.getAppId());
				pst2.setString(3, api.getCode());
				pst2.setString(4, testDataPath.getTestDataPath());
				pst2.addBatch();

			}
			pst2.executeBatch();
			
		} catch (SQLException e) {
			logger.error("Could not update test data path for Project --> " + testDataPath.getProjectId() + ", Application --> " + testDataPath.getAppId() + ", Function or Api --> " + testDataPath.getFuncCode(),e);
			throw new DatabaseException("Could not update test data path for Project --> " + testDataPath.getProjectId() + ", Application --> " + testDataPath.getAppId() + ", Function or Api --> " + testDataPath.getFuncCode(),e);
		}finally{
			if(null!=pst){
				try {
					pst.close();
					pst2.close();
				} catch (SQLException e) {
				}
			}
		}
	if(null!=conn){
		try {
			pst.close();
			pst2.close();
			conn.close();
		
		} catch (SQLException e) {
		}
	}
}
   /*	Added by Padmavathi for TENJINCG-545 starts*/
   public void persistTestDataPathForGroup(ProjectTestDataPath testDataPath) throws DatabaseException{
		ModulesHelper helper=new ModulesHelper();	
		 try {
			   List<Module>  modules= helper.hydrateModulesInGroup(testDataPath.getAppId(), testDataPath.getGroupName());
				for(Module module:modules){ 
					ProjectTestDataPath p=new ProjectTestDataPath();
					p.setAppId(testDataPath.getAppId());
					p.setProjectId(testDataPath.getProjectId());
					p.setFuncCode(module.getCode());
					p.setTestDataPath(testDataPath.getTestDataPath());
					this.persistTestDataPathForFunction(p);
				}
				ArrayList<Api> apis=helper.hydrateApi(testDataPath.getAppId(), testDataPath.getGroupName());
				for(Api api:apis){
					ProjectTestDataPath p=new ProjectTestDataPath();
					p.setAppId(testDataPath.getAppId());
					p.setProjectId(testDataPath.getProjectId());
					p.setFuncCode(api.getCode());
					p.setTestDataPath(testDataPath.getTestDataPath());
					this.persistTestDataPathForFunction(p);
				}
				
			}catch(SQLException e){
				logger.error("Could not clear Project test data path",e);
				throw new DatabaseException("Could not update test data path for Group");
			}
	}
   /*	Added by Padmavathi for TENJINCG-545 ends*/
   
   /*	Added by Padmavathi for TENJINCG-540 starts*/
   public ProjectTestDataPath hydrateTestDataPathForFunction(ProjectTestDataPath testDataPath) throws DatabaseException{
		Connection conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement ps=null;
		ResultSet rs=null;
		if(conn==null){
			throw new DatabaseException("Invalid Database Connection");
		}
		try {
		    ps=conn.prepareStatement("SELECT TEST_DATA_PATH FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");
			ps.setInt(1, testDataPath.getProjectId());
			ps.setInt(2, testDataPath.getAppId());
			ps.setString(3, testDataPath.getFuncCode());
		    rs=ps.executeQuery();
			while(rs.next()){
				testDataPath.setTestDataPath(rs.getString("TEST_DATA_PATH"));
				
			}
			
		
		} catch (SQLException e) {
			throw new DatabaseException("could not fetch TestDataPath for Function");
		}
		finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(ps);
			DatabaseHelper.close(conn);
			
		}
		return testDataPath;
		
	}
  
   public ArrayList<ProjectTestDataPath>  hydrateTestDataPathForFunctionGroup(ProjectTestDataPath testDataPath) throws DatabaseException{
	   ModulesHelper helper=new ModulesHelper();
	   ArrayList<ProjectTestDataPath> projectTestDataPaths=new ArrayList<>();
	     try {
		List<Module>  modules= helper.hydrateModulesInGroup(testDataPath.getAppId(), testDataPath.getGroupName());
		for(Module module:modules){ 
			 ProjectTestDataPath p=new ProjectTestDataPath();
			 p.setAppId(testDataPath.getAppId());
			 p.setGroupName(testDataPath.getGroupName());
			 p.setProjectId(testDataPath.getProjectId());
			 p.setAppName(testDataPath.getAppName());
			 p.setFuncCode(module.getCode());
			 p=this.hydrateTestDataPathForFunction(p);
			 projectTestDataPaths.add(p);
			
		}
		ArrayList<Api> apis=helper.hydrateApi(testDataPath.getAppId(), testDataPath.getGroupName());
		for(Api api:apis){
			 ProjectTestDataPath p=new ProjectTestDataPath();
			 p.setGroupName(testDataPath.getGroupName());
			 p.setAppId(testDataPath.getAppId());
			 p.setProjectId(testDataPath.getProjectId());
			 p.setAppName(testDataPath.getAppName());
			 p.setFuncCode(api.getCode());
			 p=this.hydrateTestDataPathForFunction(p);
			 projectTestDataPaths.add(p);
		}
		
	} catch (SQLException e) {
		throw new DatabaseException("could not fetch TestDataPath for Group");
	}
	return projectTestDataPaths;
   }
   
   /*	Added by Padmavathi for TENJINCG-577 starts*/
   public Project hydrateProjectMailNotification(int projectId) throws DatabaseException{
	   Connection conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
	   PreparedStatement ps=null;
	   Project project=new Project();
		ResultSet rs=null;
		if(conn==null){
			throw new DatabaseException("Invalid Database Connection");
			
		}
		try{
			 ps=conn.prepareStatement("SELECT * FROM TJN_PROJECTS WHERE PRJ_ID= ?");
			 ps.setInt(1, projectId);
			 rs= ps.executeQuery();
			 while(rs.next()){
			 		/*Modified by Preeti for TENJINCG-615 starts*/
					project.setName(rs.getString("PRJ_NAME"));
					project.setId(rs.getInt("PRJ_ID"));
					project.setCreatedDate(rs.getTimestamp("PRJ_CREATE_DATE"));
					project.setDomain(rs.getString("PRJ_DOMAIN"));
					project.setOwner(rs.getString("PRJ_OWNER"));
					project.setType(rs.getString("PRJ_TYPE"));
					project.setDescription(rs.getString("PRJ_DESC"));
					project.setScreenshotoption(rs.getString("PRJ_SCREENSHOT_OPTIONS"));
					project.setState(rs.getString("PRJ_STATE"));
					/*Commented by Preeti for TENJINCG-656 starts*/
					/*project.setEmailNotification(rs.getString("PRJ_MAIL_NOTIFICATIONS"));
					project.setEmailSubject(rs.getString("PRJ_MAIL_SUBJECT"));*/
					/*Commented by Preeti for TENJINCG-656 ends*/
					/*Modified by Preeti for TENJINCG-615 ends*/
				// ArrayList<User> prjUsers=this.hydrateProjectAllUsers(projectId);
					 List<User> prjUsers=new ProjectHelperNew().hydrateProjectUsers(projectId);
					 ArrayList<User> listOfStrings = new ArrayList<>(prjUsers.size());
					 listOfStrings.addAll(prjUsers);
					project.setUsers(listOfStrings);
			 }
		}
		catch(SQLException  e){
			logger.error("error while hydrating project");
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(ps);
			DatabaseHelper.close(conn);
		}
		return project;
	   
   }
   /*	Added by Padmavathi for TENJINCG-577 ends*/

   public String hydrateProjectName(int projectId) throws DatabaseException{
	   Connection conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
	   PreparedStatement ps=null;
	  
		ResultSet rs=null;
		String projname="";
		if(conn==null){
			throw new DatabaseException("Invalid Database Connection");
			
		}
		try{
			 ps=conn.prepareStatement("SELECT PRJ_NAME FROM TJN_PROJECTS WHERE PRJ_ID= ?");
			 ps.setInt(1, projectId);
			 rs= ps.executeQuery();
			 while(rs.next()){
			 		projname=rs.getString("PRJ_NAME");
			 }
		}
		catch(SQLException  e){
			logger.error("error while hydrating project");
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(ps);
			DatabaseHelper.close(conn);
		}
		return projname;
	   
   }

public void persistProject(String accessKey, String accessSecKey) throws DatabaseException {
	Project project = new Project();
	ResultSet rs=null;
	try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement pst = conn.prepareStatement("SELECT ACCESS_KEY_ID FROM TJN_AWSS3_DETAIL")){
				rs = pst.executeQuery();
				while(rs.next()) {
					project.setAccessKey(rs.getString("ACCESS_KEY_ID"));
				}
				
				if(project.getAccessKey()==null) {
					PreparedStatement pst1 = conn.prepareStatement("INSERT INTO TJN_AWSS3_DETAIL(ACCESS_KEY_ID,ACCESS_SEC_KEY) VALUES (?,?)");
					{
						pst1.setString(1, accessKey);
						pst1.setString(2, accessSecKey);
						pst1.executeUpdate();
					}
					
				}/*else if(project.getAccessKey()!=null) {
					PreparedStatement pst2 = conn.prepareStatement("UPDATE TJN_AWSS3_DETAIL SET ACCESS_KEY_ID=?, ACCESS_SEC_KEY=?");   
					{
						pst2.setString(1, accessKey);
						pst2.setString(2, accessSecKey);
						pst2.executeUpdate();
					}
					
				}*/
				
	}catch(SQLException e){
		logger.error("Could not get license key", e.getMessage());
		throw new DatabaseException("Internal error occured.Please contact Tenjin Support.");
	}	
			
 catch (DatabaseException e) {
	 logger.error("error", e.getMessage());
 }
	return;
}


public Project hydrateAwsDeatils() {
	Project project = new Project();
	ResultSet rs=null;
	try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			
			
			PreparedStatement pst = conn.prepareStatement("SELECT * FROM TJN_AWSS3_DETAIL");){
			
		
			rs = pst.executeQuery();
			while(rs.next()) {
			
				project.setAccessKey(rs.getString("ACCESS_KEY_ID"));
				project.setAccessSecKey(rs.getString("ACCESS_SEC_KEY"));
			}
				
			
			pst.close();
			
			
	
} catch (SQLException e) {
	logger.error("error", e.getMessage());
} catch (DatabaseException e) {
	logger.error("error", e.getMessage());
}
	return project;

}
}