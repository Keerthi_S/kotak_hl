/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  FilterCriteria.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 28-07-2017           Leelaprasad         for the requirement of TENJINCG-319 
* 
*/

package com.ycs.tenjin.db;

public class FilterCriteria {

	public static final String MATCH="=";
	public static final String IN="IN";
	public static final String NOT_IN="NOT IN";
	public static final String ORDER_BY = "ORDER BY";
	public static final String LIKE = "LIKE";
	
	private String fieldName;
	private String[] fieldNames;
	private Object value;
	private Object[] values;
	private String condition = MATCH; //Default value
	
	
	public FilterCriteria(){
		
	}
	
	public FilterCriteria(String name, Object value, String condition){
		this.fieldName = name;
		this.value = value;
		this.condition = condition;
	}
	
	public FilterCriteria (String name, Object[] values, String condition){
		this.fieldName = name;
		this.values = values;
		this.condition = condition;
	}
	
	public FilterCriteria (String[] names, String condition){
		this.fieldNames = names;
		this.condition = condition;
	}
	
	public FilterCriteria (String name, String condition){
		this.fieldName = name;
		this.condition = condition;
	}
	
	public FilterCriteria(String name, Object value){
		this.fieldName = name;
		this.value = value;
		this.condition = MATCH;
	}
	
	public FilterCriteria (String name, Object[] values){
		this.fieldName = name;
		this.values = values;
		this.condition = IN;
	}
	
	
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public Object[] getValues() {
		return values;
	}
	public void setValues(Object[] values) {
		this.values = values;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	
	
	public String[] getFieldNames() {
		return fieldNames;
	}
	public void setFieldNames(String[] fieldNames) {
		this.fieldNames = fieldNames;
	}
	
	@Override
	public String toString() {
		
		if(this.condition.equalsIgnoreCase(MATCH)){
			
			if (this.value instanceof String) {
				return this.fieldName + MATCH + "'" + (String)this.value + "'";
			}else if(this.value instanceof Integer){
				return this.fieldName + MATCH + (Integer)this.value ;
			}else{
				return null;
			}
		}else if(this.condition.equalsIgnoreCase(IN) || this.condition.equalsIgnoreCase(NOT_IN)){
			String list = "";
			if(this.values instanceof String[]){
				for(Object o:this.values){
					if(list.length() < 1){
						list += "'" + (String) o + "'";
					}else{
						list += ", " + "'" + (String) o + "'";
					}
				}
			}else if(this.values instanceof Integer[]){
				for(Object o:this.values){
					if(list.length() < 1){
						list +=  (Integer) o;
					}else{
						list += ", " + (Integer) o;
					}
				}
			}else{
				return null;
			}
			
			return this.fieldName + " " + this.condition + "(" + list + ")";
			
		}else if(this.condition.equalsIgnoreCase(ORDER_BY)){
			String list ="";
			
			if(this.fieldNames != null){
				for(String fieldName:fieldNames){
					if(list.length() < 1){
						list += fieldName;
					}else{
						list += ", " + fieldName;
					}
				}
			}else if(this.fieldName != null && !this.fieldName.equalsIgnoreCase("")){
					list = fieldName;
			}else{
				return null;
			}
			
			return ORDER_BY + " " + list;
		}else if(this.condition.equalsIgnoreCase(LIKE)){
			String value = "";
			if (this.value instanceof String) {
				value = "'" + this.value + "'";
			}else if(this.value instanceof Integer){
				value = "'" + Integer.toString((Integer)this.value) + "'";
			}else{
				return null;
			}
			
			return this.fieldName + " " + LIKE + " " + value;
		}
		
		else if(this.condition.equalsIgnoreCase(PATTERN_MATCHES_ANY_FIELD_VALUE)) {
			String value="";
			value = "'%" + this.value + "%'";
			String query = "";
			if(this.fieldNames != null && this.fieldNames.length > 0) {
				int counter=0;
				for(String fieldName : this.fieldNames) {
					if(counter==0)
						query += fieldName + " " + LIKE + " " + value;
					else
						query += " OR " + fieldName + " " + LIKE + " " + value;
					
					counter++;
				}
			}
			
			query = "(" + query + ")";
			return query;
		}else if(this.condition.equalsIgnoreCase(PATTERN_MATCHES_ALL_FIELD_VALUE)) {
			String value="";
			value = "'%" + this.value + "%'";
			String query = "";
			if(this.fieldNames != null && this.fieldNames.length > 0) {
				int counter=0;
				for(String fieldName : this.fieldNames) {
					if(counter==0)
						query += fieldName + " " + LIKE + " " + value;
					else
						query += " AND " + fieldName + " " + LIKE + " " + value;
					
					counter++;
				}
			}
			
			query = "(" + query + ")";
			return query;
		}else{
			return null;
		}
	}	

	public static final String PATTERN_MATCHES_ANY_FIELD_VALUE = "PATTERN_MATCHES_ANY_FIELD_VALUE";
	public static final String PATTERN_MATCHES_ALL_FIELD_VALUE = "PATTERN_MATCHES_ALL_FIELD_VALUE";
	
	public FilterCriteria(String[] fields, String patternToBeMatched, String condition) {

       this.fieldNames = fields;
		this.value = patternToBeMatched;
		this.condition = condition;
	}
		
	
}


