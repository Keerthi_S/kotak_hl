/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestReportHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 *
 * DATE              	CHANGED BY              DESCRIPTION
 * 23-05-2019			Preeti					TENJINCG-1058,1059,1060,1061,1062
 * 28-05-2019           Padmavathi              SqlServer qualification for Test reports
 * 31-05-2019			Pushpa					TENJINCG-1073,1064
 * 14-06-2019			Preeti					V2.8-131
 * 14-06-2019			Preeti					V2.8-135,136,137,138
 * 17-06-2019			Preeti					V2.8-143
 * 17-06-2019			Preeti					V2.8-145
 * 18-06-2019			Preeti					V2.8-133,140
 * 19-06-2019			Preeti					V2.8-158
 * 03-07-2019           Padmavathi              TV2.8R2-12 & TV2.8R2-13 
 * 04-07-2019           Padmavathi              TV2.8R2-33
 * 19-08-2019			Preeti					TENJINCG-1065,1072
 * 21-08-2019			Preeti					TENJINCG-1066,1071
 * 03-02-2021			Paneendra	            Tenj210-158 
 * 17-02-2021			Ashiki					TJN27-202
 * 23-02-2021			Ashiki					TJN27-201
 * 24-02-2021			Ashiki					Tjn27-208
 * 15-03-2021			Ashiki					TENJINCG-1266
 * 24-03-2021			Ashiki					TJN27-218,215
 * 28-03-2021			Ashiki					TJN27-214
 * 30-03-2021			Ashiki					TJN27-216
 * 21-04-2021			Ashiki					TJN27-226
 */

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;


public class TestReportHelper {
	private static final Logger logger = LoggerFactory.getLogger(TestReportHelper.class);
	public ArrayList<HashMap<String,Object>> getTestSetRunHistoryData(int projectId, int tsRecId, int fromRunId, int toRunId,String fromDate, String toDate) throws DatabaseException {
		logger.info("hydrating test set run history");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement("SELECT M.RUN_ID, M.RUN_START_TIME, M.RUN_END_TIME, V.TOTAL_PASSED_TCS,V.TOTAL_FAILED_TCS, V.TOTAL_ERROR_TCS, V.RUN_STATUS "
						+ "FROM MASTESTRUNS M, V_RUNRESULTS V WHERE M.RUN_ID=V.RUN_ID AND M.RUN_PRJ_ID=? AND M.RUN_TS_REC_ID=? AND M.RUN_ID BETWEEN ? AND ? AND M.RUN_START_TIME >=? "
						+ "AND M.RUN_START_TIME<=? AND M.RUN_STATUS IN('Complete','Aborted') ORDER BY M.RUN_ID");
			){
			pst.setInt(1, projectId);
			pst.setInt(2, tsRecId);
			pst.setInt(3, fromRunId);
			pst.setInt(4, toRunId);
			pst.setString(5, fromDate);
			pst.setString(6, toDate);
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Run No", rs.getInt("RUN_ID"));
					record.put("Start Date", rs.getDate("RUN_START_TIME"));
					record.put("End Date", rs.getDate("RUN_END_TIME"));
					record.put("Tests Passed", rs.getInt("TOTAL_PASSED_TCS"));
					record.put("Tests Failed", rs.getInt("TOTAL_FAILED_TCS"));
					record.put("Tests Error", rs.getInt("TOTAL_ERROR_TCS"));
					record.put("Status", rs.getString("RUN_STATUS"));
					data.add(record);
				}
			}
			
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	public ArrayList<HashMap<String,Object>> getTestSetRunHistoryDataForRun(int projectId, int tsRecId, int fromRunId, int toRunId) throws DatabaseException {
		logger.info("hydrating test set run history");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement("SELECT M.RUN_ID, M.RUN_START_TIME, M.RUN_END_TIME, V.TOTAL_PASSED_TCS,V.TOTAL_FAILED_TCS, V.TOTAL_ERROR_TCS, V.RUN_STATUS "
						+ "FROM MASTESTRUNS M, V_RUNRESULTS V WHERE M.RUN_ID=V.RUN_ID AND M.RUN_PRJ_ID=? AND M.RUN_TS_REC_ID=? AND M.RUN_ID BETWEEN ? AND ? AND M.RUN_STATUS IN('Complete','Aborted') ORDER BY M.RUN_ID");
			){
			pst.setInt(1, projectId);
			pst.setInt(2, tsRecId);
			pst.setInt(3, fromRunId);
			pst.setInt(4, toRunId);
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Run No", rs.getInt("RUN_ID"));
					record.put("Start Date", rs.getDate("RUN_START_TIME"));
					/*Added by Paneendra for Tenj210-158 starts*/
                   if(rs.getString("RUN_STATUS").equalsIgnoreCase("Not Started")) {
                	   record.put("End Date", "Empty");	
					}else {
					record.put("End Date", rs.getDate("RUN_END_TIME"));
					}
                   /*Added by Paneendra for Tenj210-158 ends*/
					record.put("Tests Passed", rs.getInt("TOTAL_PASSED_TCS"));
					record.put("Tests Failed", rs.getInt("TOTAL_FAILED_TCS"));
					record.put("Tests Error", rs.getInt("TOTAL_ERROR_TCS"));
					record.put("Status", rs.getString("RUN_STATUS"));
					
					
					data.add(record);
				}
			}
			
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	/*Modified by Preeti for V2.8-135,136 starts*/
	public ArrayList<HashMap<String,Object>> getTestSetRunHistoryDataForDate(int projectId, int tsRecId, String fromDate, String toDate) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			){
			return this.getTestSetRunHistoryDataForDate(conn, projectId, tsRecId, fromDate, toDate);
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	}
	
	public ArrayList<HashMap<String,Object>> getTestSetRunHistoryDataForDate(Connection conn, int projectId, int tsRecId, String fromDate, String toDate) throws DatabaseException, SQLException {
		logger.info("hydrating test set run history");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		String query="";
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query="SELECT M.RUN_ID, M.RUN_START_TIME, M.RUN_END_TIME, V.TOTAL_PASSED_TCS,V.TOTAL_FAILED_TCS, V.TOTAL_ERROR_TCS, V.RUN_STATUS "
					+ "FROM MASTESTRUNS M, V_RUNRESULTS V WHERE M.RUN_ID=V.RUN_ID AND M.RUN_PRJ_ID=? AND M.RUN_TS_REC_ID=? AND M.RUN_START_TIME >=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') "
					+ "AND M.RUN_START_TIME <=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND M.RUN_STATUS IN('Complete','Aborted') ORDER BY M.RUN_ID";
		}else{
			query="SELECT M.RUN_ID, M.RUN_START_TIME, M.RUN_END_TIME, V.TOTAL_PASSED_TCS,V.TOTAL_FAILED_TCS, V.TOTAL_ERROR_TCS, V.RUN_STATUS "
					+ "FROM MASTESTRUNS M, V_RUNRESULTS V WHERE M.RUN_ID=V.RUN_ID AND M.RUN_PRJ_ID=? AND M.RUN_TS_REC_ID=? AND M.RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) "
					+ "AND M.RUN_START_TIME <= CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 AND M.RUN_STATUS IN('Complete','Aborted') ORDER BY M.RUN_ID";
		}
		try (
				PreparedStatement pst = conn.prepareStatement(query);
			){
			pst.setInt(1, projectId);
			pst.setInt(2, tsRecId);
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(3, fromDate+" 00:00:00");
				pst.setString(4, toDate+" 23:59:59");
			}else{
				pst.setString(3, fromDate);
				pst.setString(4, toDate);
			}
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Run No", rs.getInt("RUN_ID"));
					record.put("Start Date", rs.getDate("RUN_START_TIME"));
					/*Added by Paneendra for Tenj210-158 starts*/
					if(rs.getString("RUN_STATUS").equalsIgnoreCase("Not Started")) {
	                	   record.put("End Date", "Empty");	
						}else {
						record.put("End Date", rs.getDate("RUN_END_TIME"));
						}
					//record.put("End Date", rs.getDate("RUN_END_TIME"));
					/*Added by Paneendra for Tenj210-158 ends*/
					record.put("Tests Passed", rs.getInt("TOTAL_PASSED_TCS"));
					record.put("Tests Failed", rs.getInt("TOTAL_FAILED_TCS"));
					record.put("Tests Error", rs.getInt("TOTAL_ERROR_TCS"));
					record.put("Status", rs.getString("RUN_STATUS"));
					data.add(record);
				}
			}
			
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	/*Modified by Preeti for V2.8-135,136 ends*/
	/*Added by Padmavathi to SqlServer qualification for Test reports*/
	public ArrayList<HashMap<String,Object>> getTestSetExecutionDetailData(int projectId, int tsRecId,String execDate) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			){
			return this.getTestSetExecutionDetailData(conn, projectId, tsRecId, execDate);
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	}
	/*Added by Padmavathi to SqlServer qualification for Test reports ends*/
	public ArrayList<HashMap<String,Object>> getTestSetExecutionDetailData(Connection conn,int projectId, int tsRecId,String execDate) throws DatabaseException, SQLException {
		logger.info("hydrating test set execution details");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
	/*Modified by Padmavathi to SqlServer qualification for Test reports*/
		String query="";
		boolean isOracleDB=true; 
		/*Modified by Ashiki for TJN27-214 starts*/
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query="SELECT T.TC_NAME,T.TC_REC_ID,T.TC_ID,max(M.RUN_ID) as RUN_ID, max(M.RUN_START_TIME),COUNT(V.TC_REC_ID) AS EXEC_COUNT, SUM(V.TSTEP_TOTAL_PASS) AS PASS_STEPS, "
					+ "SUM(V.TSTEP_TOTAL_FAIL) AS FAIL_STEPS,  SUM(V.TSTEP_TOTAL_ERROR) AS ERROR_STEPS FROM TESTCASES T, MASTESTRUNS M, V_RUNRESULTS_TS V "
					+ "WHERE T.TC_REC_ID=V.TC_REC_ID AND V.TSTEP_STATUS IN('Error','Pass','Fail') AND M.RUN_ID = V.RUN_ID AND M.RUN_TS_REC_ID = ? AND M.RUN_START_TIME >=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND "
					+ "M.RUN_START_TIME <=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND M.RUN_PRJ_ID=? AND M.RUN_STATUS IN('Complete','Aborted') GROUP BY T.TC_NAME, T.TC_REC_ID,T.TC_ID"
					+ " ORDER BY T.TC_REC_ID";
					
		}else{
			isOracleDB=false;
			/*Modified by Padmavathi for TV2.8R2-33 starts*/
			query="SELECT T.TC_NAME,T.TC_REC_ID,T.TC_ID,max(M.RUN_ID) as RUN_ID, max(M.RUN_START_TIME),COUNT(V.TC_REC_ID) AS EXEC_COUNT, SUM(V.TSTEP_TOTAL_PASS) AS PASS_STEPS, "
					+ "SUM(V.TSTEP_TOTAL_FAIL) AS FAIL_STEPS,  SUM(V.TSTEP_TOTAL_ERROR) AS ERROR_STEPS FROM TESTCASES T, MASTESTRUNS M, V_RUNRESULTS_TS V "
					+ "WHERE T.TC_REC_ID=V.TC_REC_ID AND V.TSTEP_STATUS IN('Error','Pass','Fail') AND M.RUN_ID = V.RUN_ID AND M.RUN_TS_REC_ID = ? AND M.RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND "
					+ "M.RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND M.RUN_PRJ_ID=? AND M.RUN_STATUS IN('Complete','Aborted') GROUP BY T.TC_NAME, T.TC_REC_ID,T.TC_ID"
					+ " ORDER BY T.TC_REC_ID";
					
		}
		try (
				
				PreparedStatement pst = conn.prepareStatement(query);
				
				PreparedStatement pst1 = conn.prepareStatement("select TC_STATUS from V_RUNRESULTS_TC where RUN_ID=? and TC_REC_ID= ?");
			){
			pst.setInt(1, tsRecId);
			if(isOracleDB){
				pst.setString(2, execDate+" 00:00:00");
				pst.setString(3, execDate+" 23:59:59");
				pst.setInt(4, projectId);
			}else{
				pst.setString(2, execDate);
				pst.setString(3, execDate);
				pst.setInt(4, projectId);
			}
			/*Modified by Padmavathi to SqlServer qualification for Test reports ends*/	
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Test Case Name", rs.getString("TC_NAME"));
					record.put("Steps Passed", rs.getInt("PASS_STEPS"));
					record.put("Steps Failed", rs.getInt("FAIL_STEPS"));
					record.put("Steps Error", rs.getInt("ERROR_STEPS"));
					int tcRecId = rs.getInt("TC_REC_ID");
					pst1.setInt(1, tcRecId);
					int runId = rs.getInt("RUN_ID");
					//Added By Ashiki for  TJN27-205 starts
						pst1.setInt(1, runId);
						pst1.setInt(2, tcRecId);
					
					//Added By Ashiki for  TJN27-205 ends
					try (ResultSet rs1 = pst1.executeQuery()){
						while(rs1.next()){
							
							record.put("Last Run Status", rs1.getString("TC_STATUS"));
						}
					}
					/*Modified by Ashiki for TJN27-214 ends*/
					data.add(record);
				}
			}
			
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	/*Modified by Preeti for V2.8-135,136 starts*/
	public ArrayList<HashMap<String,Object>> getTestSetExecutionSummaryData(int projectId,String fromDate, String toDate, String userId) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			){
			return this.getTestSetExecutionSummaryData(conn, projectId, fromDate, toDate, userId);
		}
			catch (SQLException e) {
				logger.error("Could not fetch records");
				throw new DatabaseException("Could not fetch records",e);
			}
	}
	public ArrayList<HashMap<String,Object>> getTestSetExecutionSummaryData(Connection conn, int projectId,String fromDate, String toDate, String userId) throws DatabaseException, SQLException {
		logger.info("hydrating test set execution summary");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		String query="";
		/*Modified by Preeti for V2.8-158 starts*/
		String query1="";
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query = "SELECT TS.TS_NAME, TS.TS_REC_ID, COUNT(V.RUN_ID) AS EXEC_COUNT, "
					+ "SUM(V.TOTAL_PASSED_TCS) AS PASS_TESTS, SUM(V.TOTAL_FAILED_TCS) AS FAIL_TESTS, "
					+ "SUM(V.TOTAL_ERROR_TCS) AS ERROR_TESTS FROM TESTSETS TS, MASTESTRUNS M, V_RUNRESULTS V "
					+ "WHERE M.RUN_ID = V.RUN_ID AND M.RUN_TS_REC_ID = TS.TS_REC_ID AND TS.TS_REC_TYPE='TS' AND V.RUN_STATUS IN('Error','Pass','Fail') "
					+ "AND M.RUN_USER=? AND M.RUN_START_TIME >=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND M.RUN_START_TIME <=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND M.RUN_PRJ_ID=? "
					+ "GROUP BY TS.TS_NAME, TS.TS_REC_ID ORDER BY TS.TS_REC_ID";
			query1 = "SELECT M.RUN_START_TIME,M.RUN_END_TIME,M.RUN_ID, V.RUN_STATUS FROM MASTESTRUNS M, V_RUNRESULTS V "
					+ "WHERE V.RUN_ID=M.RUN_ID AND M.RUN_ID=(SELECT MAX(A.RUN_ID) FROM MASTESTRUNS A,V_RUNRESULTS B "
					+ "WHERE A.RUN_ID=B.RUN_ID AND A.RUN_TS_REC_ID=? AND A.RUN_USER=? AND "
					+ "A.RUN_START_TIME>=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND A.RUN_START_TIME<=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND B.RUN_STATUS IN('Error','Pass','Fail'))";
		}else{
			query = "SELECT TS.TS_NAME, TS.TS_REC_ID, COUNT(V.RUN_ID) AS EXEC_COUNT, "
					+ "SUM(V.TOTAL_PASSED_TCS) AS PASS_TESTS, SUM(V.TOTAL_FAILED_TCS) AS FAIL_TESTS, "
					+ "SUM(V.TOTAL_ERROR_TCS) AS ERROR_TESTS FROM TESTSETS TS, MASTESTRUNS M, V_RUNRESULTS V "
					+ "WHERE M.RUN_ID = V.RUN_ID AND M.RUN_TS_REC_ID = TS.TS_REC_ID AND TS.TS_REC_TYPE='TS' AND V.RUN_STATUS IN('Error','Pass','Fail') "
					+ "AND M.RUN_USER=? AND M.RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND M.RUN_START_TIME < CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 AND M.RUN_PRJ_ID=? "
					+ "GROUP BY TS.TS_NAME, TS.TS_REC_ID ORDER BY TS.TS_REC_ID";
			query1 = "SELECT M.RUN_START_TIME,M.RUN_END_TIME,M.RUN_ID, V.RUN_STATUS FROM MASTESTRUNS M, V_RUNRESULTS V "
					+ "WHERE V.RUN_ID=M.RUN_ID AND M.RUN_ID=(SELECT MAX(A.RUN_ID) FROM MASTESTRUNS A,V_RUNRESULTS B "
					+ "WHERE A.RUN_ID=B.RUN_ID AND A.RUN_TS_REC_ID=? AND A.RUN_USER=? AND "
					+ "A.RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND A.RUN_START_TIME< CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 AND B.RUN_STATUS IN('Error','Pass','Fail'))";
		}
		try (
				PreparedStatement pst = conn.prepareStatement(query);
				PreparedStatement pst1 = conn.prepareStatement(query1);
				){
			pst.setString(1, userId);
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(2, fromDate+" 00:00:00");
				pst.setString(3, toDate+" 23:59:59");
			}else{
				pst.setString(2, fromDate);
				pst.setString(3, toDate);
			}
			pst.setInt(4, projectId);
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					/*Added by Paneendra for  Tenj210-158 starts*/
					record.put("Test Set ID", rs.getInt("TS_REC_ID"));
					/*Added by Paneendra for  Tenj210-158 ends*/
					record.put("Test Set Name", rs.getString("TS_NAME"));
					int tsRecId = rs.getInt("TS_REC_ID");
					pst1.setInt(1, tsRecId);
					pst1.setString(2, userId);
					if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
						pst1.setString(3, fromDate+" 00:00:00");
						pst1.setString(4, toDate+" 23:59:59");
					}else{
						pst1.setString(3, fromDate);
						pst1.setString(4, toDate);
					}
					/*Modified by Preeti for V2.8-158 ends*/
					try (ResultSet rs1 = pst1.executeQuery()){
						while(rs1.next()){
							record.put("Last Execution Date", rs1.getDate("RUN_START_TIME"));
							if(rs1.getDate("RUN_START_TIME") != null && rs1.getDate("RUN_END_TIME") != null) {
								 String elapsedTime = Utilities.calculateElapsedTime(rs1.getTimestamp("RUN_START_TIME").getTime(), rs1.getTimestamp("RUN_END_TIME").getTime());
								 record.put("Duration", elapsedTime);
							 }
							record.put("Last Run Status", rs1.getString("RUN_STATUS"));
						}
					}
					record.put("Times Executed", rs.getInt("EXEC_COUNT"));
					record.put("Tests Passed", rs.getInt("PASS_TESTS"));
					record.put("Tests Failed", rs.getInt("FAIL_TESTS"));
					record.put("Tests Error", rs.getInt("ERROR_TESTS"));
					data.add(record);
				}
			}
			
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	public ArrayList<HashMap<String,Object>> getTestSetExecutionSummaryData(int projectId,String fromDate, String toDate) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			){
			return this.getTestSetExecutionSummaryData(conn, projectId, fromDate, toDate);
		}
			catch (SQLException e) {
				logger.error("Could not fetch records");
				throw new DatabaseException("Could not fetch records",e);
			}
	}
	public ArrayList<HashMap<String,Object>> getTestSetExecutionSummaryData(Connection conn, int projectId,String fromDate, String toDate) throws DatabaseException, SQLException {
		logger.info("hydrating test set execution summary");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		String query="";
		/*Modified by Preeti for V2.8-158 starts*/
		String query1="";
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query = "SELECT TS.TS_NAME, TS.TS_REC_ID, COUNT(V.RUN_ID) AS EXEC_COUNT, "
					+ "SUM(V.TOTAL_PASSED_TCS) AS PASS_TESTS, SUM(V.TOTAL_FAILED_TCS) AS FAIL_TESTS, "
					+ "SUM(V.TOTAL_ERROR_TCS) AS ERROR_TESTS FROM TESTSETS TS, MASTESTRUNS M, V_RUNRESULTS V "
					+ "WHERE M.RUN_ID = V.RUN_ID AND M.RUN_TS_REC_ID = TS.TS_REC_ID AND TS.TS_REC_TYPE='TS' AND V.RUN_STATUS IN('Error','Pass','Fail') "
					+ "AND M.RUN_START_TIME >=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND M.RUN_START_TIME <=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND M.RUN_PRJ_ID=? "
					+ "GROUP BY TS.TS_NAME, TS.TS_REC_ID ORDER BY TS.TS_REC_ID";
			query1 = "SELECT M.RUN_START_TIME,M.RUN_END_TIME,M.RUN_ID, V.RUN_STATUS FROM MASTESTRUNS M, V_RUNRESULTS V "
					+ "WHERE V.RUN_ID=M.RUN_ID AND M.RUN_ID=(SELECT MAX(A.RUN_ID) FROM MASTESTRUNS A,V_RUNRESULTS B "
					+ "WHERE A.RUN_ID=B.RUN_ID AND A.RUN_TS_REC_ID=? AND "
					+ "A.RUN_START_TIME>=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND A.RUN_START_TIME<=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND B.RUN_STATUS IN('Error','Pass','Fail'))";
		}
		else{
			query = "SELECT TS.TS_NAME, TS.TS_REC_ID, COUNT(V.RUN_ID) AS EXEC_COUNT, "
					+ "SUM(V.TOTAL_PASSED_TCS) AS PASS_TESTS, SUM(V.TOTAL_FAILED_TCS) AS FAIL_TESTS, "
					+ "SUM(V.TOTAL_ERROR_TCS) AS ERROR_TESTS FROM TESTSETS TS, MASTESTRUNS M, V_RUNRESULTS V "
					+ "WHERE M.RUN_ID = V.RUN_ID AND M.RUN_TS_REC_ID = TS.TS_REC_ID AND TS.TS_REC_TYPE='TS' AND V.RUN_STATUS IN('Error','Pass','Fail') "
					+ "AND M.RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND M.RUN_START_TIME < CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 AND M.RUN_PRJ_ID=? "
					+ "GROUP BY TS.TS_NAME, TS.TS_REC_ID ORDER BY TS.TS_REC_ID";
			query1 = "SELECT M.RUN_START_TIME,M.RUN_END_TIME,M.RUN_ID, V.RUN_STATUS FROM MASTESTRUNS M, V_RUNRESULTS V "
					+ "WHERE V.RUN_ID=M.RUN_ID AND M.RUN_ID=(SELECT MAX(A.RUN_ID) FROM MASTESTRUNS A,V_RUNRESULTS B "
					+ "WHERE A.RUN_ID=B.RUN_ID AND A.RUN_TS_REC_ID=? AND "
					+ "A.RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND A.RUN_START_TIME < CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 AND B.RUN_STATUS IN('Error','Pass','Fail'))";
		}
		try (
				PreparedStatement pst = conn.prepareStatement(query);
				PreparedStatement pst1 = conn.prepareStatement(query1);
				){
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(1, fromDate+" 00:00:00");
				pst.setString(2, toDate+" 23:59:59");
			}else{
				pst.setString(1, fromDate);
				pst.setString(2, toDate);
			}
			pst.setInt(3, projectId);
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					/*Added by paneendra for Tenj210-158 starts*/
					record.put("Test Set ID", rs.getInt("TS_REC_ID"));
					/*Added by paneendra for Tenj210-158 ends*/
					record.put("Test Set Name", rs.getString("TS_NAME"));
					int tsRecId = rs.getInt("TS_REC_ID");
					pst1.setInt(1, tsRecId);
					if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
						pst1.setString(2, fromDate+" 00:00:00");
						pst1.setString(3, toDate+" 23:59:59");
					}else{
						pst1.setString(2, fromDate);
						pst1.setString(3, toDate);
					}
					/*Modified by Preeti for V2.8-158 ends*/
					try (ResultSet rs1 = pst1.executeQuery()){
						while(rs1.next()){
							record.put("Last Execution Date", rs1.getDate("RUN_START_TIME"));
							if(rs1.getDate("RUN_START_TIME") != null && rs1.getDate("RUN_END_TIME") != null) {
								 String elapsedTime = Utilities.calculateElapsedTime(rs1.getTimestamp("RUN_START_TIME").getTime(), rs1.getTimestamp("RUN_END_TIME").getTime());
								 record.put("Duration", elapsedTime);
							 }
							record.put("Last Run Status", rs1.getString("RUN_STATUS"));
						}
					}
					record.put("Times Executed", rs.getInt("EXEC_COUNT"));
					record.put("Tests Passed", rs.getInt("PASS_TESTS"));
					record.put("Tests Failed", rs.getInt("FAIL_TESTS"));
					record.put("Tests Error", rs.getInt("ERROR_TESTS"));
					data.add(record);
				}
			}
			
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	/*Modified by Preeti for V2.8-135,136 ends*/
	public ArrayList<HashMap<String,Object>> getTestCaseExecutionDetailData(int projectId, int tcRecId, String execDate, String userId) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			){
			return this.getTestCaseExecutionDetailData(conn, projectId, tcRecId, execDate, userId);
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	}
	public ArrayList<HashMap<String,Object>> getTestCaseExecutionDetailData(Connection conn,int projectId, int tcRecId, String execDate, String userId) throws DatabaseException, SQLException {
		logger.info("hydrating test case execution detail");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		/*Modified by Padmavathi to SqlServer qualification for Test reports*/
		String query="";
		boolean isOracleDb=true;
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			/*Modified by Priyanka for  TJN27-198 starts*/
			/*Modified by Ashiki for  TJN27-208 starts*/
			query="SELECT T.TSTEP_REC_ID,T.TSTEP_ID,T.TSTEP_SHT_DESC,T.FUNC_CODE,T.TSTEP_OPERATION,T.TSTEP_DATA_ID, R.TSTEP_MESSAGE, R.TSTEP_RESULT,T.TSTEP_TYPE,MA.APP_NAME "
					+ "FROM TESTSTEPS T, MASTESTRUNS M, RUNRESULT_TS_TXN R,MASAPPLICATION  MA,V_RUNRESULTS_TS B WHERE  T.TSTEP_REC_ID= R.TSTEP_REC_ID AND MA.APP_ID=T.APP_ID AND  M.RUN_ID = R.RUN_ID "
					+ "AND M.RUN_ID=(SELECT MAX(A.RUN_ID) FROM MASTESTRUNS A, V_RUNRESULTS_TS B WHERE A.RUN_ID=B.RUN_ID AND A.RUN_USER =? "
					+ "AND B.TC_REC_ID=? AND A.RUN_PRJ_ID=? AND A.RUN_STATUS IN ('Complete','Aborted') AND A.RUN_START_TIME >=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') "
					+ "AND A.RUN_START_TIME <=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS')) and B.TC_REC_ID=? and B.RUN_ID=R.RUN_ID AND B.TSTEP_REC_ID=R.TSTEP_REC_ID";
			/*Modified by Ashiki for  TJN27-208 ends*/
			/*Modified by Priyanka for  TJN27-198 ends*/
		}else{
			isOracleDb=false;
			/*Modified by Padmavathi for TV2.8R2-12 & TV2.8R2-13 starts*/
			/*Modified by Priyanka for  TJN27-198 starts*/
			/*Modified by Ashiki for  TJN27-208 starts*/
			query="SELECT T.TSTEP_REC_ID,T.TSTEP_ID,T.TSTEP_SHT_DESC,T.FUNC_CODE,T.TSTEP_OPERATION,T.TSTEP_DATA_ID, R.TSTEP_MESSAGE, R.TSTEP_RESULT,T.TSTEP_TYPE,MA.APP_NAME "
					+ "FROM TESTSTEPS T, MASTESTRUNS M, RUNRESULT_TS_TXN R,MASAPPLICATION  MA,V_RUNRESULTS_TS B WHERE  T.TSTEP_REC_ID= R.TSTEP_REC_ID AND MA.APP_ID=T.APP_ID AND M.RUN_ID = R.RUN_ID "
					+ "AND M.RUN_ID=(SELECT MAX(A.RUN_ID) FROM MASTESTRUNS A, V_RUNRESULTS_TS B WHERE A.RUN_ID=B.RUN_ID AND A.RUN_USER =? "
					+ "AND B.TC_REC_ID=? AND A.RUN_PRJ_ID=? AND A.RUN_STATUS IN ('Complete','Aborted') "
					+ "AND CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),RUN_START_TIME), ' ', ' '),101) = CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)) and B.TC_REC_ID=? and B.RUN_ID=R.RUN_ID AND B.TSTEP_REC_ID=R.TSTEP_REC_ID";
			/*Modified by Ashiki for  TJN27-208 ENDS*/
			/*Modified by Priyanka for  TJN27-198 starts*/
			/*Modified by Padmavathi for TV2.8R2-12 & TV2.8R2-13 ends*/
		}
		 
		try (
				
				PreparedStatement pst = conn.prepareStatement(query);
			){
			pst.setString(1, userId);
			pst.setInt(2, tcRecId);
			pst.setInt(3, projectId);
			if(isOracleDb){
				/*Modified by Ashiki for  TJN27-208 starts*/
				pst.setString(4, execDate+" 00:00:00");
				pst.setString(5, execDate+" 23:59:59");
				pst.setInt(6, tcRecId);
				/*Modified by Ashiki for  TJN27-208 ends*/
			}else{
				pst.setString(4, execDate);
				pst.setInt(5, tcRecId);
			}
			/*Modified by Padmavathi to SqlServer qualification for Test reports ends*/
			try(ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Step Id", rs.getString("TSTEP_ID"));
					record.put("Step Summary", rs.getString("TSTEP_SHT_DESC"));
					/*Modified by Paneendra for  Tenj210-158 starts*/
					record.put("Step Type", rs.getString("TSTEP_TYPE"));
					if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("S"))
						record.put("Step Status", "Pass");
					else if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("F"))
						record.put("Step Status", "Fail");
					else
						record.put("Step Status", "Error");
					/*Modified by Paneendra for  Tenj210-158 ends*/
					if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("S"))
						record.put("Failure Reason", "");
					else
						record.put("Failure Reason", rs.getString("TSTEP_MESSAGE"));
					record.put("Function/API", rs.getString("FUNC_CODE"));
					/*Modified by Paneendra for  Tenj210-158 starts*/
					record.put("APPLICATION NAME", rs.getString("APP_NAME"));
					//record.put("Operation", rs.getString("TSTEP_OPERATION"));
					/*Modified by Paneendra for  Tenj210-158 ends*/
					record.put("Test Data Id", rs.getString("TSTEP_DATA_ID"));
					data.add(record);
				}
			}
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	
	public ArrayList<HashMap<String,Object>> getTestCaseExecutionDetailData(int projectId, int tcRecId, String execDate) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				){
					return this.getTestCaseExecutionDetailData(conn, projectId, tcRecId, execDate);
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	}
	public ArrayList<HashMap<String,Object>> getTestCaseExecutionDetailData(Connection conn,int projectId, int tcRecId, String execDate) throws DatabaseException, SQLException {
		logger.info("hydrating test case execution detail");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		/*Modified by Padmavathi to SqlServer qualification for Test reports*/
		boolean isOracleDb=true;
		String query = "";
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			/*Modified by Paneendra for  Tenj210-158 starts*/
			/*Modified by Ashiki for Tjn27-208 starts*/
			query="SELECT T.TSTEP_REC_ID,T.TSTEP_ID,T.TSTEP_SHT_DESC,T.FUNC_CODE,T.TSTEP_OPERATION,T.TSTEP_DATA_ID,R.TSTEP_MESSAGE, R.TSTEP_RESULT,T.TSTEP_TYPE,MA.APP_NAME "
					+ "FROM TESTSTEPS T, MASTESTRUNS M,MASAPPLICATION  MA,RUNRESULT_TS_TXN R,V_RUNRESULTS_TS D WHERE  T.TSTEP_REC_ID= R.TSTEP_REC_ID AND M.RUN_ID = R.RUN_ID  AND T.APP_ID=MA.APP_ID "
					+ "AND M.RUN_ID=(SELECT MAX(A.RUN_ID) FROM MASTESTRUNS A, V_RUNRESULTS_TS B WHERE A.RUN_ID=B.RUN_ID "
					+ "AND B.TC_REC_ID=? AND A.RUN_PRJ_ID=? AND A.RUN_STATUS IN ('Complete','Aborted') AND A.RUN_START_TIME >=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') "
					+ "AND A.RUN_START_TIME <=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS')) AND D.TC_REC_ID=? and D.RUN_ID=R.RUN_ID AND D.TSTEP_REC_ID=R.TSTEP_REC_ID";
			/*Modified by Ashiki for Tjn27-208 ends*/
			/*Modified by Paneendra for  Tenj210-158 ends*/
			
		}else{
			isOracleDb=false;
			/*Modified by Padmavathi for TV2.8R2-12 & TV2.8R2-13 starts*/
			/*Modified by Ashiki for Tjn27-208 starts*/
			query="SELECT T.TSTEP_REC_ID,T.TSTEP_ID,T.TSTEP_SHT_DESC,T.FUNC_CODE,T.TSTEP_OPERATION,T.TSTEP_DATA_ID, R.TSTEP_MESSAGE, R.TSTEP_RESULT,T.TSTEP_TYPE,MA.APP_NAME "
						+ "FROM TESTSTEPS T, MASTESTRUNS M,MASAPPLICATION  MA, RUNRESULT_TS_TXN R,V_RUNRESULTS_TS D WHERE  T.TSTEP_REC_ID= R.TSTEP_REC_ID AND M.RUN_ID = R.RUN_ID "
						+ "AND M.RUN_ID=(SELECT MAX(A.RUN_ID) FROM MASTESTRUNS A, V_RUNRESULTS_TS B WHERE A.RUN_ID=B.RUN_ID "
						+ "AND B.TC_REC_ID=? AND A.RUN_PRJ_ID=? AND A.RUN_STATUS IN ('Complete','Aborted')   "
						+ "AND CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),RUN_START_TIME), ' ', ' '),101) = CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)) AND D.TC_REC_ID=? and D.RUN_ID=R.RUN_ID AND D.TSTEP_REC_ID=R.TSTEP_REC_ID";
			/*Modified by Ashiki for Tjn27-208 ends*/
			/*Modified by Padmavathi for TV2.8R2-12 & TV2.8R2-13 ends*/
		}
		try (
				
				PreparedStatement pst = conn.prepareStatement(query);
			){
			pst.setInt(1, tcRecId);
			pst.setInt(2, projectId);
			if(isOracleDb){
				/*Modified by Ashiki for Tjn27-208 starts*/
				pst.setString(3, execDate+" 00:00:00");
				pst.setString(4, execDate+" 23:59:59");
				pst.setInt(5, tcRecId);
				/*Modified by Ashiki for Tjn27-208 ends*/
			}else{
				pst.setString(3, execDate);
				pst.setInt(4, tcRecId);
			}
			/*Modified by Padmavathi to SqlServer qualification for Test reports ends*/
			try(ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Step Id", rs.getString("TSTEP_ID"));
					record.put("Step Summary", rs.getString("TSTEP_SHT_DESC"));
					/*Modified by Paneendra for  Tenj210-158 starts*/
					record.put("Step Type", rs.getString("TSTEP_TYPE"));
	
					if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("S"))
						record.put("Step Status", "Pass");
					else if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("F"))
						record.put("Step Status", "Fail");
					else
						record.put("Step Status", "Error");
					/*Modified by Paneendra for  Tenj210-158 ends*/
					if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("S"))
						record.put("Failure Reason", "");
					else
						record.put("Failure Reason", rs.getString("TSTEP_MESSAGE"));
					record.put("Function/API", rs.getString("FUNC_CODE"));
					/*Added by Paneendra for  Tenj210-158 starts*/
					record.put("APPLICATION NAME", rs.getString("APP_NAME"));
					
					record.put("Step Type", rs.getString("TSTEP_TYPE"));
					/*Added by Paneendra for  Tenj210-158 ends*/
					
					
					record.put("Test Data Id", rs.getString("TSTEP_DATA_ID"));
					data.add(record);
				}
			}
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	
	public ArrayList<HashMap<String,Object>> getTestCaseExecutionHistoryData(int projectId, String fromDate, String toDate, String userId,int tcRecId) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				){/*Modified by Ashiki for TJN27-216 starts*/
			 		return this.getTestCaseExecutionHistoryData(conn, projectId, fromDate, toDate, userId,tcRecId);
					/*Modified by Ashiki for TJN27-216 starts*/
		}catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	}
	public ArrayList<HashMap<String,Object>> getTestCaseExecutionHistoryData(Connection conn,int projectId, String fromDate, String toDate, String userId,int tcRecId) throws DatabaseException, SQLException {
		logger.info("hydrating test case execution history");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		/*Modified by Preeti for V2.8-131 starts*/
		String query="";
		/*Modified by Ashiki for TJN27-216 starts*/
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query="SELECT TRUNC(M.RUN_START_TIME) AS RUN_DATE,SUM(V.TC_TOTAL_EXEC) AS TOTAL_TEST,SUM(V.TC_TOTAL_PASS) AS PASS_TEST,"
					+ "SUM(V.TC_TOTAL_FAIL) AS FAIL_TEST,SUM(V.TC_TOTAL_ERROR) AS ERROR_TEST "
					+ "FROM V_RUNRESULTS_TC V,MASTESTRUNS M WHERE M.RUN_ID=V.RUN_ID AND M.RUN_USER=? AND M.RUN_START_TIME>=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') "
					+ "AND M.RUN_START_TIME<=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND M.RUN_PRJ_ID=? AND V.TC_REC_ID=? AND M.RUN_STATUS IN('Complete','Aborted') GROUP BY TRUNC( M.RUN_START_TIME) ORDER BY TRUNC(M.RUN_START_TIME)";
					
					
		}else{
			query="SELECT CONVERT(VARCHAR(10), RUN_START_TIME, 120) AS RUN_DATE, SUM(V.TC_TOTAL_EXEC) AS TOTAL_TEST, SUM(V.TC_TOTAL_PASS) AS PASS_TEST,"
					+ " SUM(V.TC_TOTAL_FAIL) AS FAIL_TEST, SUM(V.TC_TOTAL_ERROR) AS ERROR_TEST FROM MASTESTRUNS M, V_RUNRESULTS_TC V "
					+ "WHERE M.RUN_ID=V.RUN_ID AND  M.RUN_USER=? AND M.RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) "
					+ "AND M.RUN_START_TIME < CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 AND M.RUN_PRJ_ID=? AND V.TC_REC_ID=? AND M.RUN_STATUS IN('Complete','Aborted') GROUP BY CONVERT(VARCHAR(10), RUN_START_TIME, 120) ORDER BY CONVERT(VARCHAR(10), RUN_START_TIME, 120)  ";
		}/*Modified by Ashiki for TJN27-216 end*/
			try (
				PreparedStatement pst = conn.prepareStatement(query);
			){
			pst.setString(1, userId);
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(2, fromDate+" 00:00:00");
				pst.setString(3, toDate+" 23:59:59");
			}else{
				pst.setString(2, fromDate);
				pst.setString(3, toDate);
			}
			/*Modified by Preeti for V2.8-131 ends*/
			pst.setInt(4, projectId);
			/*Added by Ashiki for TJN27-216 starts*/
			pst.setInt(5, tcRecId);
			/*Added by Ashiki for TJN27-216 starts*/
			try(ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Date", rs.getDate("RUN_DATE"));
					record.put("Executed", rs.getString("TOTAL_TEST"));
					record.put("Passed", rs.getString("PASS_TEST"));
					record.put("Failed", rs.getString("FAIL_TEST"));
					record.put("Error", rs.getString("ERROR_TEST"));
					data.add(record);
				}
			}
		}catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	
	public ArrayList<HashMap<String,Object>> getTestCaseExecutionHistoryData(int projectId, String fromDate, String toDate,int tcRecId) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				){/*Modified by Ashiki for TJN27-216 starts*/
					return this.getTestCaseExecutionHistoryData(conn, projectId, fromDate, toDate,tcRecId);
				/*Modified by Ashiki for TJN27-216 starts*/
		}catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	}
	public ArrayList<HashMap<String,Object>> getTestCaseExecutionHistoryData(Connection conn,int projectId, String fromDate, String toDate,int tcRecId) throws DatabaseException, SQLException {
		logger.info("hydrating test case execution history");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		/*Modified by Padmavathi to SqlServer qualification for Test reports*/
		String query="";
		/*Modified by Ashiki for TJN27-216 starts*/
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query="SELECT TRUNC(M.RUN_START_TIME) AS RUN_DATE,SUM(V.TC_TOTAL_EXEC) AS TOTAL_TEST,SUM(V.TC_TOTAL_PASS) AS PASS_TEST,"
					+ "SUM(V.TC_TOTAL_FAIL) AS FAIL_TEST,SUM(V.TC_TOTAL_ERROR) AS ERROR_TEST "
					+ "FROM V_RUNRESULTS_TC V,MASTESTRUNS M WHERE M.RUN_ID=V.RUN_ID AND M.RUN_START_TIME>=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') "
					+ "AND M.RUN_START_TIME<=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND M.RUN_PRJ_ID=? AND V.TC_REC_ID=? AND M.RUN_STATUS IN('Complete','Aborted') GROUP BY TRUNC( M.RUN_START_TIME) ORDER BY TRUNC(M.RUN_START_TIME)";
		}else{
			query="SELECT CONVERT(VARCHAR(10), RUN_START_TIME, 120) AS RUN_DATE, SUM(V.TC_TOTAL_EXEC) AS TOTAL_TEST, SUM(V.TC_TOTAL_PASS) AS PASS_TEST,"
					+ " SUM(V.TC_TOTAL_FAIL) AS FAIL_TEST, SUM(V.TC_TOTAL_ERROR) AS ERROR_TEST FROM MASTESTRUNS M, V_RUNRESULTS_TC V "
					+ " WHERE M.RUN_ID=V.RUN_ID AND M.RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) "
					+ " AND M.RUN_START_TIME < CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 AND M.RUN_PRJ_ID=? AND V.TC_REC_ID=? AND M.RUN_STATUS IN('Complete','Aborted') GROUP BY CONVERT(VARCHAR(10), RUN_START_TIME, 120) ORDER BY CONVERT(VARCHAR(10), RUN_START_TIME, 120) ";
		}/*Modified by Ashiki for TJN27-216 ends*/
		try (
			
				PreparedStatement pst = conn.prepareStatement(query);
			){
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(1, fromDate+" 00:00:00");
				pst.setString(2, toDate+" 23:59:59");
			}else{
				pst.setString(1, fromDate);
				pst.setString(2, toDate);
			}
			pst.setInt(3, projectId);
			/*Added by Ashiki for TJN27-216 starts*/
			pst.setInt(4, tcRecId);
			/*Added by Ashiki for TJN27-216 starts*/
			/*Modified by Padmavathi to SqlServer qualification for Test ends*/
			try(ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Date", rs.getDate("RUN_DATE"));
					record.put("Executed", rs.getString("TOTAL_TEST"));
					record.put("Passed", rs.getString("PASS_TEST"));
					record.put("Failed", rs.getString("FAIL_TEST"));
					record.put("Error", rs.getString("ERROR_TEST"));
					data.add(record);
				}
			}
		}catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	
	public ArrayList<Integer> getRunsForTestSet(int tsId, int projectId) throws DatabaseException{
		ArrayList<Integer> runIds = new ArrayList<>();
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				/*Modified by Preeti for V2.8-145 starts*/
				PreparedStatement pst = conn.prepareStatement("SELECT RUN_ID FROM MASTESTRUNS WHERE RUN_TS_REC_ID = ? AND RUN_PRJ_ID = ? AND RUN_STATUS IN('Complete','Aborted')");
				/*Modified by Preeti for V2.8-145 ends*/
			){
			pst.setInt(1, tsId);
			pst.setInt(2, projectId);
			try(ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					runIds.add(rs.getInt("RUN_ID"));
				}
			}
		}
		catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return runIds; 
	}
	/*Modified by Preeti for V2.8-133,140 starts*/
	/*Modified by Ashiki for TJN27-201 starts*/
	public ArrayList<HashMap<String, Object>> getApplicationWiseExecutionData(int appId, String fromDate,
			String toDate, int prjId) throws DatabaseException {
	/*Modified by Ashiki for TJN27-201 ends*/
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				){
				/*Modified by Ashiki for TJN27-201 starts*/
					return this.getApplicationWiseExecutionData(conn, appId, fromDate, toDate,prjId);
				/*Modified by Ashiki for TJN27-201 ends*/
		}catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	}
	/*Added by Pushpa for TENJINCG-1074,1063 starts*/
	/*Modified by Ashiki for TJN27-201 starts*/
	public ArrayList<HashMap<String, Object>> getApplicationWiseExecutionData(Connection conn, int appId, String fromDate,
			String toDate, int prjId) throws DatabaseException, SQLException {
	/*Modified by Ashiki for TJN27-201 ends*/
		LinkedHashSet<Integer> testcasesForApp=this.getTestcaseFroApp(appId,prjId,fromDate,toDate);
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		String query="";
		String query1="";

		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			/*Modified by Ashiki for TJN27-201 starts*/
			query="SELECT count(A.TC_REC_ID) as TC_ID_EXE,SUM(A.TSTEP_TOTAL_PASS) as TOTAL_PASS,max(B.RUN_START_TIME) as RUN_START_TIME,"
					+ "SUM(A.TSTEP_TOTAL_FAIL) AS TOTAL_FAIL,SUM(A.TSTEP_TOTAL_ERROR) AS TOTAL_ERROR,max(B.RUN_STATUS) as RUN_STATUS,max(B.RUN_ID) as RUN_ID ,"
					+ "max(C.TC_NAME)as TC_NAME,max(C.TC_ID ) as TC_ID FROM V_RUNRESULTS_TS A,MASTESTRUNS B,TESTCASES C WHERE A.TC_REC_ID = ? AND "
					+ "A.RUN_ID=B.RUN_ID AND A.TC_REC_ID= C.TC_REC_ID and B.RUN_STATUS IN('Complete','Aborted') AND  B.RUN_START_TIME >= TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') "
					+ "and B.RUN_START_TIME <= TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND A.RUN_ID=B.RUN_ID";
			
					query1="SELECT B.RUN_START_TIME,B.RUN_END_TIME FROM V_RUNRESULTS_TS A,MASTESTRUNS B WHERE A.TC_REC_ID = ? "
							+ "AND A.RUN_ID=B.RUN_ID AND B.RUN_STATUS IN('Complete','Aborted') AND  B.RUN_START_TIME >= TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') "
							+ "and B.RUN_START_TIME <= TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND A.RUN_ID=B.RUN_ID ORDER BY A.RUN_ID ";
		}else{
			query="SELECT count(A.TC_REC_ID) as TC_ID_EXE,SUM(A.TSTEP_TOTAL_PASS) as TOTAL_PASS,max(B.RUN_START_TIME) as RUN_START_TIME,"
					+ "SUM(A.TSTEP_TOTAL_FAIL) AS TOTAL_FAIL,SUM(A.TSTEP_TOTAL_ERROR) AS TOTAL_ERROR,max(B.RUN_STATUS) as RUN_STATUS,max(B.RUN_ID) as RUN_ID ,"
					+ "max(C.TC_NAME)as TC_NAME,max(C.TC_ID ) as TC_ID FROM V_RUNRESULTS_TS A,MASTESTRUNS B,TESTCASES C WHERE A.TC_REC_ID = ? AND "
					+ "A.RUN_ID=B.RUN_ID AND A.TC_REC_ID= C.TC_REC_ID and B.RUN_STATUS IN('Complete','Aborted') AND"
					+ " B.RUN_START_TIME >= CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) "
					+ "and  B.RUN_START_TIME < CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 AND A.RUN_ID=B.RUN_ID";
		
			
			query1="SELECT B.RUN_START_TIME,B.RUN_END_TIME FROM V_RUNRESULTS_TS A,MASTESTRUNS B WHERE A.TC_REC_ID = ? "
					+ "AND A.RUN_ID=B.RUN_ID AND B.RUN_STATUS IN('Complete','Aborted') AND"
					+ "  B.RUN_START_TIME >= CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) and  B.RUN_START_TIME < CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 order by B.RUN_ID DESC";
			
			/*Modified by Ashiki for TJN27-201 ends*/
		}
	
		for(int tcId:testcasesForApp){
			LinkedHashMap<String,Object> map=new LinkedHashMap<String,Object>();
		try (
				
				PreparedStatement pst = conn.prepareStatement(query);
				PreparedStatement pst1 = conn.prepareStatement(query1);
			){
			
			pst.setInt(1, tcId);
			pst1.setInt(1, tcId);
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(2, fromDate+" 00:00:00");
				pst.setString(3, toDate+" 23:59:59");
				pst1.setString(2, fromDate+" 00:00:00");
				pst1.setString(3, toDate+" 23:59:59");
			}else{
				pst.setString(2, fromDate);
				pst.setString(3, toDate);
				pst1.setString(2, fromDate);
				pst1.setString(3, toDate);
			}
			/*Modified by Preeti for V2.8-133,140 ends*/
			
			String elapsedTime="";
			String[] execStartTime=null;
			try(ResultSet rs= pst1.executeQuery();){
				while(rs.next()){
					
					/*Modified by Ashiki for TJN27-201 starts*/
					if(rs.getDate("RUN_START_TIME") != null && rs.getDate("RUN_END_TIME") != null) {
						elapsedTime=Utilities.calculateElapsedTime(rs.getTimestamp("RUN_START_TIME").getTime(), rs.getTimestamp("RUN_END_TIME").getTime());
					 }
					/*Modified by Ashiki for TJN27-201 ends*/
				}
			}
			
			try(ResultSet rs= pst.executeQuery();){
				
				
				int i=0;
				while(rs.next()){
					/*Modified by Ashiki for TJN27-201 starts*/
					
					if(i==0){
						if(rs.getDate("RUN_START_TIME") != null) {
							execStartTime=rs.getString("RUN_START_TIME").split(" ");
						/*Added by Paneendra for  Tenj210-158 starts*/
					map.put("Test Case Id", rs.getString("TC_ID"));
					/*Added by Paneendra for  Tenj210-158 ends*/
					map.put("Test Case Name", rs.getString("TC_NAME"));
					/*Modified by Ashiki for TJN27-202 starts*/
					map.put("Execution Date",execStartTime[0]);
					map.put("Run Status", rs.getString("RUN_STATUS"));
					/*Modified by Ashiki for TJN27-202 ends*/
					map.put("Duration", elapsedTime);
					map.put("Times Executed", rs.getInt("TC_ID_EXE"));
					map.put("Steps Passed", rs.getInt("TOTAL_PASS"));
					map.put("Steps Failed", rs.getInt("TOTAL_FAIL"));
					map.put("Steps Error", rs.getInt("TOTAL_ERROR"));
					}
					}
					
					/*Modified by Ashiki for TJN27-201 ends*/
					i++;
				}
			}
		}
		catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		if(map.size()>0){
		data.add(map);
		}
		}
		return data;
	}
	
	private LinkedHashSet<Integer> getTestcaseFroApp(int appId, int prjId, String fromDate, String toDate) throws DatabaseException {

			try (
					Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					){
						return this.getTestcaseFroApp(conn, appId,prjId, fromDate, toDate);
			}catch (SQLException e) {
				logger.error("Could not fetch records");
				throw new DatabaseException("Could not fetch records",e);
			}
		
	}
	/*Modified by Ashiki for TJN27-201 starts*/
	private LinkedHashSet<Integer> getTestcaseFroApp(Connection conn,int appId, int prjId, String fromDate, String toDate) throws DatabaseException, SQLException {
		
		
		LinkedHashSet<Integer> tcIds=new LinkedHashSet<Integer>();
		/*Modified by Ashiki for TJN27-201 ends*/
		String query="";
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query="SELECT TC_REC_ID,RUN_START_TIME FROM V_TSTEP_EXEC_HISTORY_2 WHERE APP_ID=? and PRJ_ID=? and RUN_START_TIME >= TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') and  RUN_START_TIME <= TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') Group by TC_REC_ID,RUN_START_TIME ORDER BY RUN_START_TIME desc";
		}else{
			query="SELECT TC_REC_ID,RUN_START_TIME FROM V_TSTEP_EXEC_HISTORY_2 WHERE APP_ID=? and PRJ_ID=? and RUN_START_TIME BETWEEN ? AND ? Group by TC_REC_ID,RUN_START_TIME ORDER BY RUN_START_TIME desc";
		}
		
		try (
				PreparedStatement pst = conn.prepareStatement(query);
			)	{
			pst.setInt(1, appId);
			/*Added by Ashiki for TJN27-201 starts*/
			pst.setInt(2, prjId);
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(3,fromDate+" 00:00:00");
				pst.setString(4, toDate+" 23:59:59");
			}else {
				pst.setString(3,fromDate);
				pst.setString(4, toDate);
			}
			
			/*Added by Ashiki for TJN27-201 starts*/
			try(ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					tcIds.add(rs.getInt("TC_REC_ID"));
				}
			}
		}
		catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return tcIds;
	}
	/*Added by Pushpa for TENJINCG-1074,1063 ends*/
	/*Modified by Preeti for V2.8-137,138 starts*/
	public ArrayList<HashMap<String,Object>> getTesterWiseTestSetExecutionSummary(int projectId,String fromDate, String toDate, String userId) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			){
			return this.getTesterWiseTestSetExecutionSummary(conn, projectId, fromDate, toDate, userId);
		}catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	}
	/*Added by Pushpa for TENJINCG-1073,1064 starts*/
	public ArrayList<HashMap<String,Object>> getTesterWiseTestSetExecutionSummary(Connection conn, int projectId,String fromDate, String toDate, String userId) throws DatabaseException, SQLException {
		logger.info("hydrating test set execution summary");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		String query="";
		String query1="";
			/*Modified by Ashiki for TJN27-215 starts*/
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query="SELECT TS.TS_NAME, TS.TS_REC_ID, COUNT(V.RUN_ID) AS EXEC_COUNT, "
					+ "SUM(V.TOTAL_PASSED_TCS) AS PASS_TESTS, SUM(V.TOTAL_FAILED_TCS) AS FAIL_TESTS, "
					+ "SUM(V.TOTAL_ERROR_TCS) AS ERROR_TESTS FROM TESTSETS TS, MASTESTRUNS M, V_RUNRESULTS V "
					+ "WHERE M.RUN_ID = V.RUN_ID AND M.RUN_TS_REC_ID = TS.TS_REC_ID AND TS.TS_REC_TYPE='TS' AND V.RUN_STATUS IN('Error','Pass','Fail') "
					+ "AND M.RUN_USER=? AND M.RUN_START_TIME>=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND M.RUN_START_TIME<=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND M.RUN_PRJ_ID=? "
					+ "GROUP BY TS.TS_NAME, TS.TS_REC_ID,M.RUN_START_TIME ORDER BY M.RUN_START_TIME ";
			/*Modified by Ashiki for TJN27-215 ends*/
			query1="SELECT M.RUN_START_TIME,M.RUN_END_TIME,M.RUN_ID, V.RUN_STATUS FROM MASTESTRUNS M, V_RUNRESULTS V "
					+ "WHERE V.RUN_ID=M.RUN_ID AND M.RUN_ID=(SELECT MAX(A.RUN_ID) FROM MASTESTRUNS A,V_RUNRESULTS B "
					+ "WHERE A.RUN_ID=B.RUN_ID AND A.RUN_TS_REC_ID=? AND A.RUN_USER=? AND "
					+ "A.RUN_START_TIME>=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND A.RUN_START_TIME<=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND B.RUN_STATUS IN('Error','Pass','Fail'))";
		}else{
			/*Modified by Ashiki for TJN27-215 starts*/
			query="SELECT TS.TS_NAME, TS.TS_REC_ID, COUNT(V.RUN_ID) AS EXEC_COUNT, "
					+ "SUM(V.TOTAL_PASSED_TCS) AS PASS_TESTS, SUM(V.TOTAL_FAILED_TCS) AS FAIL_TESTS, "
					+ "SUM(V.TOTAL_ERROR_TCS) AS ERROR_TESTS FROM TESTSETS TS, MASTESTRUNS M, V_RUNRESULTS V "
					+ "WHERE M.RUN_ID = V.RUN_ID AND M.RUN_TS_REC_ID = TS.TS_REC_ID AND TS.TS_REC_TYPE='TS' AND V.RUN_STATUS IN('Error','Pass','Fail') "
					+ "AND M.RUN_USER=? AND M.RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND M.RUN_START_TIME<CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 AND M.RUN_PRJ_ID=? "
					+ "GROUP BY TS.TS_NAME, TS.TS_REC_ID, M.RUN_START_TIME ORDER BY M.RUN_START_TIME";
			/*Modified by Ashiki for TJN27-215 ends*/
			query1="SELECT M.RUN_START_TIME,M.RUN_END_TIME,M.RUN_ID, V.RUN_STATUS FROM MASTESTRUNS M, V_RUNRESULTS V "
					+ "WHERE V.RUN_ID=M.RUN_ID AND M.RUN_ID=(SELECT MAX(A.RUN_ID) FROM MASTESTRUNS A,V_RUNRESULTS B "
					+ "WHERE A.RUN_ID=B.RUN_ID AND A.RUN_TS_REC_ID=? AND A.RUN_USER=? AND "
					+ "A.RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND A.RUN_START_TIME<CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 AND B.RUN_STATUS IN('Error','Pass','Fail'))";
		}
		try (
				PreparedStatement pst = conn.prepareStatement(query);
				PreparedStatement pst1 = conn.prepareStatement(query1);
				PreparedStatement pst3 = conn.prepareStatement("SELECT COUNT(*) AS DEFECT_COUNT FROM TJN_RUN_DEFECTS WHERE RUN_ID IN (SELECT RUN_ID FROM MASTESTRUNS WHERE RUN_TS_REC_ID =?)");
				){
			pst.setString(1, userId);
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(2, fromDate+" 00:00:00");
				pst.setString(3, toDate+" 23:59:59");
			}else{
				pst.setString(2, fromDate);
				pst.setString(3, toDate);
			}
			pst.setInt(4, projectId);
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Test Set Name", rs.getString("TS_NAME"));
					int tsRecId = rs.getInt("TS_REC_ID");
					pst1.setInt(1, tsRecId);
					pst1.setString(2, userId);
					if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
						pst1.setString(3, fromDate+" 00:00:00");
						pst1.setString(4, toDate+" 23:59:59");
					}else{
						pst1.setString(3, fromDate);
						pst1.setString(4, toDate);
					}
					try (ResultSet rs1 = pst1.executeQuery()){
						while(rs1.next()){
							record.put("Last Execution Date", rs1.getDate("RUN_START_TIME"));
							
							record.put("Last Run Status", rs1.getString("RUN_STATUS"));
						}
					}
					record.put("Times Executed", rs.getInt("EXEC_COUNT"));
					record.put("Tests Passed", rs.getInt("PASS_TESTS"));
					record.put("Tests Failed", rs.getInt("FAIL_TESTS"));
					record.put("Tests Error", rs.getInt("ERROR_TESTS"));
					pst3.setInt(1, tsRecId);
					try (ResultSet rs3 = pst3.executeQuery()){
						while(rs3.next()){
						record.put("Defects Raised", rs3.getString("DEFECT_COUNT"));
						}
					}
					data.add(record);
				}
			}
			
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	/*Added by Pushpa for TENJINCG-1073,1064 starts*/
	/*Modified by Preeti for V2.8-137,138 ends*/	
	/*Added by Preeti for TENJINCG-1065,1072 starts*/
	public ArrayList<HashMap<String,Object>> getSeverityWiseDefectsData(int projectId, int tsRecId, int fromRunId, int toRunId) throws DatabaseException {
		logger.info("hydrating severity wise defect data");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement("SELECT D.RUN_ID,M.RUN_START_TIME,COUNT(D.DEF_REC_ID) AS DEFECT_COUNT FROM MASTESTRUNS M, TJN_RUN_DEFECTS D "
						+ "WHERE M.RUN_PRJ_ID=? AND M.RUN_TS_REC_ID=? AND M.RUN_ID=D.RUN_ID AND D.RUN_ID BETWEEN ? AND ? GROUP BY D.RUN_ID, M.RUN_START_TIME ORDER BY D.RUN_ID");
				PreparedStatement pst1 = conn.prepareStatement("SELECT DISTINCT D.DEF_SEVERITY FROM TJN_RUN_DEFECTS D, MASTESTRUNS M "
						+ "WHERE M.RUN_PRJ_ID=? AND M.RUN_TS_REC_ID=? AND M.RUN_ID=D.RUN_ID AND D.RUN_ID BETWEEN ? AND ?");
				PreparedStatement pst2 = conn.prepareStatement("SELECT COUNT(DEF_SEVERITY) AS SEVERITY_COUNT FROM TJN_RUN_DEFECTS WHERE RUN_ID=? AND DEF_SEVERITY=?");
				){
			pst.setInt(1, projectId);
			pst.setInt(2, tsRecId);
			pst.setInt(3, fromRunId);
			pst.setInt(4, toRunId);
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Run No", rs.getInt("RUN_ID"));
					record.put("Execution Date", rs.getDate("RUN_START_TIME"));
					record.put("Defects Raised", rs.getInt("DEFECT_COUNT"));
					pst1.setInt(1, projectId);
					pst1.setInt(2, tsRecId);
					pst1.setInt(3, fromRunId);
					pst1.setInt(4, toRunId);
					try (ResultSet rs1 = pst1.executeQuery()){
						while(rs1.next()){
							pst2.setInt(1, rs.getInt("RUN_ID"));
							pst2.setString(2, rs1.getString("DEF_SEVERITY"));
							try (ResultSet rs2 = pst2.executeQuery()){
								while(rs2.next()){
									record.put(rs1.getString("DEF_SEVERITY"), rs2.getString("SEVERITY_COUNT"));
								}
							}
						}
					}
					data.add(record);
				}
			}
			
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	/*Added by Preeti for TENJINCG-1065,1072 ends*/
	/*Added by Preeti for TENJINCG-1066,1071 starts*/
	public ArrayList<HashMap<String,Object>> getSchedulerData(int projectId, String taskType, String taskStatus ,String fromDate, String toDate) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			){
			return this.getSchedulerData(conn, projectId, taskType, taskStatus, fromDate, toDate);
		}catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	}
	public ArrayList<HashMap<String,Object>> getSchedulerData(Connection conn,int projectId, String taskType, String taskStatus ,String fromDate, String toDate) throws DatabaseException, SQLException {
		logger.info("hydrating scheduler details");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		String query = "";
		/*Modified by Ashiki for TJN27-218 starts*/
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query="SELECT DISTINCT S.SCH_TASKNAME, TS.TS_NAME, S.SCH_DATE, S.CREATED_BY,RS.TSTEP_RESULT  FROM TJN_SCHEDULER S, MASTESTRUNS M, TESTSETS TS,RUNRESULT_TS_TXN RS "
					+ "WHERE S.RUN_ID=M.RUN_ID AND M.RUN_TS_REC_ID=TS.TS_REC_ID AND M.RUN_ID=RS.RUN_ID AND S.PROJECT_ID=? AND S.ACTION=? AND S.STATUS=? "
					+ "AND S.SCH_DATE>=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND S.SCH_DATE <=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') ORDER BY S.SCH_DATE";
		}else{
			query="SELECT DISTINCT S.SCH_TASKNAME, TS.TS_NAME, S.SCH_DATE, S.CREATED_BY,RS.TSTEP_RESULT  FROM TJN_SCHEDULER S, MASTESTRUNS M, TESTSETS TS,RUNRESULT_TS_TXN RS "
					+ "WHERE S.RUN_ID=M.RUN_ID AND M.RUN_TS_REC_ID=TS.TS_REC_ID AND M.RUN_ID=RS.RUN_ID AND S.PROJECT_ID=? AND S.ACTION=? AND S.STATUS=? "
					+ "AND S.SCH_DATE>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND S.SCH_DATE <CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 ORDER BY S.SCH_DATE";
		}
		/*Modified by Ashiki for TJN27-218 ends*/
		try (
				PreparedStatement pst = conn.prepareStatement(query);
			){
			pst.setInt(1, projectId);
			pst.setString(2, taskType);
			pst.setString(3, taskStatus);
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(4, fromDate+" 00:00:00");
				pst.setString(5, toDate+" 23:59:59");
			}else{
				pst.setString(4, fromDate);
				pst.setString(5, toDate);
			}
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Task Name", rs.getString("SCH_TASKNAME"));
					record.put("Test Set", rs.getString("TS_NAME"));
					/*Added by Paneendra for  Tenj210-158 starts*/
					if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("E")) {
						
					    record.put("TestSet status", "Error");
					}else if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("F")) {
						record.put("TestSet status", "Fail");
					}else {
						record.put("TestSet status", "Success");
					}
					/*Added by Paneendra for  Tenj210-158 ends*/
					record.put("Scheduled By", rs.getString("CREATED_BY"));
					record.put("Schedule Date", rs.getDate("SCH_DATE"));
					data.add(record);
				}
			}
			
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	public ArrayList<HashMap<String,Object>> getSchedulerDataForRecurrence(int projectId, String taskType, String taskStatus ,String fromDate, String toDate) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			){
			return this.getSchedulerDataForRecurrence(conn, projectId, taskType, taskStatus, fromDate, toDate);
		}catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	}
	public ArrayList<HashMap<String,Object>> getSchedulerDataForRecurrence(Connection conn,int projectId, String taskType, String taskStatus ,String fromDate, String toDate) throws DatabaseException, SQLException {
		logger.info("hydrating scheduler details");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		String query = "";
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query = "SELECT DISTINCT S.SCH_TASKNAME,TS.TS_NAME,S.CREATED_BY,R.FREQUENCY,R.RECUR_DAYS,S.SCH_DATE,R.END_DATE,RS.TSTEP_RESULT "
					+ "FROM TJN_SCHEDULER S, TJN_SCH_RECURRENCE R, MASTESTRUNS M, TESTSETS TS,RUNRESULT_TS_TXN RS "
					+ "WHERE S.RUN_ID=M.RUN_ID AND M.RUN_TS_REC_ID=TS.TS_REC_ID AND S.SCH_ID=R.SCH_ID AND M.RUN_ID=RS.RUN_ID AND S.PROJECT_ID=? AND S.ACTION=? AND S.STATUS=? "
					+ "AND S.SCH_DATE>=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND S.SCH_DATE <=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS')  ORDER BY S.SCH_TASKNAME";
		}else{
			query = "SELECT DISTINCT S.SCH_TASKNAME,TS.TS_NAME,S.CREATED_BY,R.FREQUENCY,R.RECUR_DAYS,S.SCH_DATE,R.END_DATE,RS.TSTEP_RESULT "
					+ "FROM TJN_SCHEDULER S, TJN_SCH_RECURRENCE R, MASTESTRUNS M, TESTSETS TS,RUNRESULT_TS_TXN RS "
					+ "WHERE S.RUN_ID=M.RUN_ID AND M.RUN_TS_REC_ID=TS.TS_REC_ID AND S.SCH_ID=R.SCH_ID AND M.RUN_ID=RS.RUN_ID AND S.PROJECT_ID=? AND S.ACTION=? AND S.STATUS=? "
					+ "AND S.SCH_DATE>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND S.SCH_DATE <CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 ORDER BY S.SCH_TASKNAME";
		}
		try (
				PreparedStatement pst = conn.prepareStatement(query);
			){
			pst.setInt(1, projectId);
			pst.setString(2, taskType);
			pst.setString(3, taskStatus);
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(4, fromDate+" 00:00:00");
				pst.setString(5, toDate+" 23:59:59");
			}else{
				pst.setString(4, fromDate);
				pst.setString(5, toDate);
			}
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Task Name", rs.getString("SCH_TASKNAME"));
					record.put("Test Set", rs.getString("TS_NAME"));
					/*Added by Paneendra for  Tenj210-158 starts*/
					if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("E")) {
					
					    record.put("TestSet status", "Error");
					}else if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("F")) {
						record.put("TestSet status", "Fail");
					}else {
						record.put("TestSet status", "Success");
					}
					/*Added by Paneendra for  Tenj210-158 ends*/
					record.put("Scheduled By", rs.getString("CREATED_BY"));
					record.put("Frequency", rs.getString("FREQUENCY"));
					record.put("Recur Days", rs.getString("RECUR_DAYS"));
					record.put("Start Date", rs.getDate("SCH_DATE"));
					record.put("Last Run Date", rs.getDate("END_DATE"));
					data.add(record);
				}
			}
			
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	
	public ArrayList<HashMap<String,Object>> getSchedulerData(int projectId, String taskType, String taskStatus ,String fromDate, String toDate, String userId) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			){
			return this.getSchedulerData(conn, projectId, taskType, taskStatus, fromDate, toDate, userId);
		}catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	}
	public ArrayList<HashMap<String,Object>> getSchedulerData(Connection conn,int projectId, String taskType, String taskStatus ,String fromDate, String toDate, String userId) throws DatabaseException, SQLException {
		logger.info("hydrating scheduler details");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		String query = "";
		/*Modified by Ashiki for TJN27-218 starts*/
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query="SELECT DISTINCT S.SCH_TASKNAME, TS.TS_NAME, S.SCH_DATE,RS.TSTEP_RESULT  FROM TJN_SCHEDULER S, MASTESTRUNS M, TESTSETS TS,RUNRESULT_TS_TXN RS "
					+ "WHERE S.RUN_ID=M.RUN_ID AND M.RUN_TS_REC_ID=TS.TS_REC_ID AND M.RUN_ID=RS.RUN_ID AND S.PROJECT_ID=? AND S.ACTION=? AND S.STATUS=? "
					+ "AND S.SCH_DATE>=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND S.SCH_DATE <=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND S.CREATED_BY=? ORDER BY S.SCH_DATE";
		}else{
			query="SELECT DISTINCT S.SCH_TASKNAME, TS.TS_NAME, S.SCH_DATE,RS.TSTEP_RESULT  FROM TJN_SCHEDULER S, MASTESTRUNS M, TESTSETS TS,RUNRESULT_TS_TXN RS "
					+ "WHERE S.RUN_ID=M.RUN_ID AND M.RUN_TS_REC_ID=TS.TS_REC_ID AND M.RUN_ID=RS.RUN_ID AND S.PROJECT_ID=? AND S.ACTION=? AND S.STATUS=? "
					+ "AND S.SCH_DATE>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND S.SCH_DATE <CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 AND S.CREATED_BY=? ORDER BY S.SCH_DATE";
		}
		/*Modified by Ashiki for TJN27-218 ends*/
		try (
				PreparedStatement pst = conn.prepareStatement(query);
			){
			pst.setInt(1, projectId);
			pst.setString(2, taskType);
			pst.setString(3, taskStatus);
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(4, fromDate+" 00:00:00");
				pst.setString(5, toDate+" 23:59:59");
			}else{
				pst.setString(4, fromDate);
				pst.setString(5, toDate);
			}
			pst.setString(6, userId);
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Task Name", rs.getString("SCH_TASKNAME"));
					record.put("Test Set", rs.getString("TS_NAME"));
					/*Added by Paneendra for  Tenj210-158 starts*/
					if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("E")) {
					
					    record.put("TestSet status", "Error");
					}else if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("F")) {
						record.put("TestSet status", "Fail");
					}else {
						record.put("TestSet status", "Success");
					}
					/*Added by Paneendra for  Tenj210-158 ends*/
					record.put("Schedule Date", rs.getDate("SCH_DATE"));
					data.add(record);
				}
			}
			
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	public ArrayList<HashMap<String,Object>> getSchedulerDataForRecurrence(int projectId, String taskType, String taskStatus ,String fromDate, String toDate, String userId) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			){
			return this.getSchedulerDataForRecurrence(conn, projectId, taskType, taskStatus, fromDate, toDate, userId);
		}catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	}
	public ArrayList<HashMap<String,Object>> getSchedulerDataForRecurrence(Connection conn,int projectId, String taskType, String taskStatus ,String fromDate, String toDate, String userId) throws DatabaseException, SQLException {
		logger.info("hydrating scheduler details");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		String query = "";
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			/*Modified by Ashiki for TJN27-218 starts*/
			query = "SELECT DISTINCT S.SCH_TASKNAME,TS.TS_NAME,R.FREQUENCY,R.RECUR_DAYS,S.SCH_DATE,R.END_DATE,RS.TSTEP_RESULT "
					+ "FROM TJN_SCHEDULER S, TJN_SCH_RECURRENCE R, MASTESTRUNS M, TESTSETS TS,RUNRESULT_TS_TXN RS  "
					+ "WHERE S.RUN_ID=M.RUN_ID AND M.RUN_TS_REC_ID=TS.TS_REC_ID AND M.RUN_ID=RS.RUN_ID AND S.SCH_ID=R.SCH_ID AND S.PROJECT_ID=? AND S.ACTION=? AND S.STATUS=? "
					+ "AND S.SCH_DATE>=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND S.SCH_DATE <=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND S.CREATED_BY=? ORDER BY S.SCH_DATE";
		}else{
			query = "SELECT S.SCH_TASKNAME,TS.TS_NAME,R.FREQUENCY,R.RECUR_DAYS,S.SCH_DATE,R.END_DATE,RS.TSTEP_RESULT "
					+ "FROM TJN_SCHEDULER S, TJN_SCH_RECURRENCE R, MASTESTRUNS M, TESTSETS TS,RUNRESULT_TS_TXN RS "
					+ "WHERE S.RUN_ID=M.RUN_ID AND M.RUN_TS_REC_ID=TS.TS_REC_ID AND S.SCH_ID=R.SCH_ID AND S.PROJECT_ID=? AND S.ACTION=? AND S.STATUS=? "
					+ "AND S.SCH_DATE>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND S.SCH_DATE <CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 AND S.CREATED_BY=? ORDER BY S.SCH_DATE";
		}/*Modified by Ashiki for TJN27-218 ends*/
		try (
				PreparedStatement pst = conn.prepareStatement(query);
			){
			pst.setInt(1, projectId);
			pst.setString(2, taskType);
			pst.setString(3, taskStatus);
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(4, fromDate+" 00:00:00");
				pst.setString(5, toDate+" 23:59:59");
			}else{
				pst.setString(4, fromDate);
				pst.setString(5, toDate);
			}
			pst.setString(6, userId);
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Task Name", rs.getString("SCH_TASKNAME"));
					record.put("Test Set", rs.getString("TS_NAME"));
					/*Added by Paneendra for  Tenj210-158 starts*/
					if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("E")) {
					
					    record.put("TestSet status", "Error");
					}else if(rs.getString("TSTEP_RESULT").equalsIgnoreCase("F")) {
						record.put("TestSet status", "Fail");
					}else {
						record.put("TestSet status", "Success");
					}
					/*Added by Paneendra for  Tenj210-158 ends*/
					record.put("Frequency", rs.getString("FREQUENCY"));
					record.put("Recur Days", rs.getString("RECUR_DAYS"));
					record.put("Start Date", rs.getDate("SCH_DATE"));
					record.put("Last Run Date", rs.getDate("END_DATE"));
					data.add(record);
				}
			}
			
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	}
	/*Added by Preeti for TENJINCG-1066,1071 ends*/
	
	public ArrayList<Timestamp> getDatesForTestCase(int tsId, int projectId) throws DatabaseException {

		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			){
			return this.getDatesForTestCase(conn, tsId, projectId);
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	
	}
	/*Added by Paneendra for  Tenj210-158 starts*/
      public ArrayList<Timestamp> getDatesForTestCase(Connection conn,int tsId, int projectId) throws DatabaseException, SQLException {
		
		ArrayList<Timestamp> testdates = new ArrayList<>();		
		String query="";
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query="select distinct trunc(RUN_START_TIME) as RUN_START_TIME from V_TSTEP_EXEC_HISTORY_2 where TC_REC_ID=? and PRJ_ID=?";
		}else{
			query="select distinct CAST(RUN_START_TIME AS DATE) as RUN_START_TIME from V_TSTEP_EXEC_HISTORY_2 where TC_REC_ID=? and PRJ_ID=?";
		}
		try (
				PreparedStatement pst = conn.prepareStatement(query);
			)	
		{
			pst.setInt(1, tsId);
			pst.setInt(2, projectId);
			try(ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					
					
					testdates.add(rs.getTimestamp("RUN_START_TIME"));
				}
			}
		}
		catch (SQLException e) {
			logger.error("Could not fetch execution history for test case");
			throw new DatabaseException("Could not fetch execution history for test case ," +tsId);
		}
		return testdates; 
	}
	
      public ArrayList<Timestamp> getDatesForTestSet(int tsId, int projectId) throws DatabaseException {


  		try (
  				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
  			){
  			return this.getDatesForTestSet(conn, tsId, projectId);
  		} catch (SQLException e) {
  			logger.error("Could not fetch records");
  			throw new DatabaseException("Could not fetch records",e);
  		}
  	
  	
      }
      
	public ArrayList<Timestamp> getDatesForTestSet(Connection conn,int tsId, int projectId) throws DatabaseException, SQLException {
		ArrayList<Timestamp> runDates = new ArrayList<>(); 
		String query="";
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query="SELECT distinct Trunc(RUN_START_TIME) as START_TIME FROM MASTESTRUNS WHERE RUN_TS_REC_ID = ? AND RUN_PRJ_ID = ? AND RUN_STATUS IN('Complete','Aborted')";
		}else{
			query="SELECT distinct CAST(RUN_START_TIME AS DATE) as START_TIME FROM MASTESTRUNS WHERE RUN_TS_REC_ID = ? AND RUN_PRJ_ID = ? AND RUN_STATUS IN('Complete','Aborted')";
		}
		
		
		
		
		try (
				PreparedStatement pst = conn.prepareStatement(query);
			){
			pst.setInt(1, tsId);
			pst.setInt(2, projectId);
			try(ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					runDates.add(rs.getTimestamp("START_TIME"));
				}
			}
		}
		catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return runDates; 
	}
	/*Added by Paneendra for  Tenj210-158 ends*/
	/*Added By Ashiki for TENJINCG-1266 starts*/
	public ArrayList<HashMap<String, Object>> getTestSetDetailData(int projectId, String exeFromDate,
			String exeToDate) throws DatabaseException {
  		try (
  				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
  			){
  			return this.getTestSetDetailData(conn,projectId,exeFromDate,exeToDate);
  		} catch (SQLException e) {
  			logger.error("Could not fetch records");
  			throw new DatabaseException("Could not fetch records",e);
  		}
  	
  	
      
	}
	
	
	private ArrayList<HashMap<String, Object>> getTestSetDetailData(Connection conn, int projectId,
			String fromDate, String toDate) throws SQLException, DatabaseException {

		logger.info("hydrating test set execution summary");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		String query="";
		String query1="";
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query = "select DISTINCT B.TS_REC_ID from MASTESTRUNS A,TESTSETS B where B.TS_REC_TYPE='TS' AND "
					+ "A.RUN_PRJ_ID=B.TS_PRJ_ID and A.RUN_START_TIME >= TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') and "
					+ "A.RUN_START_TIME <= TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') and B.TS_PRJ_ID=? AND B.TS_REC_ID=A.RUN_TS_REC_ID";
			query1 = "SELECT A.TS_NAME,B.RUN_ID,B.RUN_START_TIME,B.RUN_USER,B.RUN_END_TIME,C.TOTAL_PASSED_TCS,C.TOTAL_FAILED_TCS,C.TOTAL_ERROR_TCS,C.RUN_STATUS,SUM(TS.TSTEP_TOTAL_TXN) AS TOTAL_STEPS,"
					+ "SUM(TS.TSTEP_TOTAL_PASS) AS TSTEP_TOTAL_PASS,SUM(TS.TSTEP_TOTAL_FAIL) AS TSTEP_TOTAL_FAIL,SUM(TS.TSTEP_TOTAL_ERROR) AS TSTEP_TOTAL_ERROR from "
					+ "TESTSETS A,MASTESTRUNS B,V_RUNRESULTS C,V_RUNRESULTS_TS TS where B.RUN_ID=(select MAX(RUN_ID) FROM MASTESTRUNS WHERE RUN_TS_REC_ID=? and RUN_START_TIME >= TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') and "
					+ "RUN_START_TIME <= TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') and RUN_STATUS IN('Complete','Aborted'))AND TS.RUN_ID=B.RUN_ID AND TS.RUN_ID=C.RUN_ID AND B.RUN_ID=C.RUN_ID AND A.TS_REC_ID=B.RUN_TS_REC_ID AND C.RUN_STATUS IN('Error','Pass','Fail') GROUP BY A.TS_NAME,B.RUN_ID,B.RUN_START_TIME,B.RUN_USER,B.RUN_END_TIME,C.TOTAL_PASSED_TCS,C.TOTAL_FAILED_TCS,C.TOTAL_ERROR_TCS,C.RUN_STATUS";
		}
		else{
			query = "select DISTINCT B.TS_REC_ID from MASTESTRUNS A,TESTSETS B where B.TS_REC_TYPE='TS' AND "
					+ "A.RUN_PRJ_ID=B.TS_PRJ_ID "
					+ "AND A.RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND A.RUN_START_TIME < CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 and B.TS_PRJ_ID=? AND B.TS_REC_ID=A.RUN_TS_REC_ID";
			query1 = "SELECT A.TS_NAME,B.RUN_ID,B.RUN_START_TIME,B.RUN_USER,B.RUN_END_TIME,C.TOTAL_PASSED_TCS,C.TOTAL_FAILED_TCS,C.TOTAL_ERROR_TCS,C.RUN_STATUS, SUM(TS.TSTEP_TOTAL_TXN) AS TOTAL_STEPS,"
					+ "SUM(TS.TSTEP_TOTAL_PASS) AS TSTEP_TOTAL_PASS,SUM(TS.TSTEP_TOTAL_FAIL) AS TSTEP_TOTAL_FAIL,SUM(TS.TSTEP_TOTAL_ERROR) AS TSTEP_TOTAL_ERROR from "
					+ "TESTSETS A,MASTESTRUNS B,V_RUNRESULTS C,V_RUNRESULTS_TS TS where B.RUN_ID=(select MAX(RUN_ID) FROM MASTESTRUNS WHERE RUN_TS_REC_ID=? and "
					+ "RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND "
					+ "RUN_START_TIME < CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 and RUN_STATUS IN('Complete','Aborted')) AND TS.RUN_ID=B.RUN_ID AND TS.RUN_ID=C.RUN_ID AND B.RUN_ID=C.RUN_ID "
					+ "AND A.TS_REC_ID=B.RUN_TS_REC_ID AND C.RUN_STATUS IN('Error','Pass','Fail') GROUP BY A.TS_NAME,B.RUN_ID,B.RUN_START_TIME,B.RUN_USER,B.RUN_END_TIME,C.TOTAL_PASSED_TCS,C.TOTAL_FAILED_TCS,C.TOTAL_ERROR_TCS,C.RUN_STATUS";
		}
		try (
				PreparedStatement pst = conn.prepareStatement(query);
				PreparedStatement pst1 = conn.prepareStatement(query1);
				){
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(1, fromDate+" 00:00:00");
				pst.setString(2, toDate+" 23:59:59");
			}else{
				pst.setString(1, fromDate);
				pst.setString(2, toDate);
			}
			pst.setInt(3, projectId);
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					
					int tsRecId = rs.getInt("TS_REC_ID");
					pst1.setInt(1, tsRecId);
					if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
						pst1.setString(2, fromDate+" 00:00:00");
						pst1.setString(3, toDate+" 23:59:59");
					}else{
						pst1.setString(2, fromDate);
						pst1.setString(3, toDate);
					}
					try (ResultSet rs1 = pst1.executeQuery()){
						
						while(rs1.next()){
							record.put("Run ID", rs1.getInt("RUN_ID"));
							record.put("Test Set Name", rs1.getString("TS_NAME"));
							record.put("Executed On", rs1.getDate("RUN_START_TIME"));
							record.put("Executed By", rs1.getString("RUN_USER"));
							String execStartTime[]=rs1.getString("RUN_START_TIME").split(" ");
							record.put("Last Execution Date", execStartTime[0]);
							if(rs1.getDate("RUN_START_TIME") != null && rs1.getDate("RUN_END_TIME") != null) {
								 String elapsedTime = Utilities.calculateElapsedTime(rs1.getTimestamp("RUN_START_TIME").getTime(), rs1.getTimestamp("RUN_END_TIME").getTime());
								 record.put("Total Set Duration", elapsedTime);
							 }
							record.put("Test Cases Passed", rs1.getInt("TOTAL_PASSED_TCS"));
							record.put("Test Cases Failed", rs1.getInt("TOTAL_FAILED_TCS"));
							record.put("Test Cases Error", rs1.getInt("TOTAL_ERROR_TCS"));
							/*Added by Ashiki for TJN27-226 starts*/
							record.put("Total Test Steps", rs1.getInt("TOTAL_STEPS"));
							record.put("Test Steps Passed", rs1.getInt("TSTEP_TOTAL_PASS"));
							record.put("Test Steps Failed", rs1.getInt("TSTEP_TOTAL_FAIL"));
							record.put("Test Steps Error", rs1.getInt("TSTEP_TOTAL_ERROR"));
							/*Added by Ashiki for TJN27-226 end*/
							record.put("Last Run Status", rs1.getString("RUN_STATUS"));
							data.add(record);
						}
					}
				}
			}
			
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	
	}
	/*Added By Ashiki for TENJINCG-1266 ends*/
}