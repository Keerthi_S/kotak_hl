/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DomainHelperNew.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 15-10-2018		   	Pushpalatha			  	Newly Added 
* 17-12-2019			Roshni					Tenjincg-1166
*/
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.ycs.tenjin.project.Domain;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.util.Constants;

public class DomainHelperNew {

	public Domain fetchDomain(String domainName) throws DatabaseException {
	
		Domain domain = null;
		ArrayList<Project> projects = new ArrayList<Project>();
		/* modified by Roshni for TENJINCG-1166 starts */
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			
				PreparedStatement pst1 = conn.prepareStatement("SELECT * FROM TJN_DOMAINS WHERE DOMAIN_NAME = ? ORDER BY DOMAIN_NAME");){
		    	pst1.setString(1, domainName);
			try(ResultSet rs=pst1.executeQuery();){
				while(rs.next()){
					domain = new Domain();
					domain.setName(rs.getString("DOMAIN_NAME"));
					domain.setCreatedDate(rs.getTimestamp("DOMAIN_CREATE_DATE"));
					domain.setOwner	(rs.getString("DOMAIN_CREATED_BY"));
					try(/*Statement st1 = conn.createStatement();
						ResultSet rs2=st1.executeQuery("SELECT * FROM TJN_PROJECTS WHERE PRJ_DOMAIN ='" + domainName + "' ORDER BY PRJ_DOMAIN")*/
							PreparedStatement pst2 = conn.prepareStatement("SELECT * FROM TJN_PROJECTS WHERE PRJ_DOMAIN =? ORDER BY PRJ_DOMAIN");){
							pst2.setString(1,domainName);
							try(ResultSet rs2=pst2.executeQuery();){
								while(rs2.next()){
									Project project=new Project();
									project.setName(rs2.getString("PRJ_NAME"));
									project.setId(rs2.getInt("PRJ_ID"));
									project.setCreatedDate(rs2.getTimestamp("PRJ_CREATE_DATE"));
									project.setType(rs2.getString("PRJ_TYPE"));
									project.setOwner(rs2.getString("PRJ_OWNER"));
									project.setDomain(rs2.getString("PRJ_DOMAIN"));
									project.setState(rs2.getString("PRJ_STATE"));
									projects.add(project);
								}
							}
					}
					domain.setProjects(projects);
					/* modified by Roshni for TENJINCG-1166 ends */
				}
			}
			
			
		}catch(Exception e){
			throw new DatabaseException("Could not fetch records",e);
		}
		
		
		return domain;
	}

	public ArrayList<Project> getProjectsForDomain(String domainName, String projectType) throws DatabaseException {
		ArrayList<Project> projects = null;

		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement(
						"select * from TJN_PROJECTS where prj_domain = ? and prj_type = ? order by prj_name");) {
			pst.setString(1, domainName);
			pst.setString(2, projectType);
			try (ResultSet rs = pst.executeQuery();) {
				projects = new ArrayList<Project>();
				while (rs.next()) {
					Project project = new Project();
					project.setId(rs.getInt("PRJ_ID"));
					project.setName(rs.getString("PRJ_NAME"));
					project.setType(rs.getString("PRJ_TYPE"));
					project.setDomain(rs.getString("PRJ_DOMAIN"));
					project.setOwner(rs.getString("PRJ_OWNER"));
					project.setState(rs.getString("PRJ_STATE"));
					projects.add(project);
				}
			}

		} catch (Exception e) {
			throw new DatabaseException(e.getMessage(), e);
		} 
		return projects;
	}

	public ArrayList<Project> getProjectsForDomainForCurrentUser(String domainName, String currentUser)
			throws DatabaseException {
		ArrayList<Project> projects = null;
		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement(
						"select * from TJN_PROJECTS where prj_domain = ? and prj_owner = ? order by prj_name");) {

			pst.setString(1, domainName);
			pst.setString(2, currentUser);

			try (ResultSet rs = pst.executeQuery();) {
				projects = new ArrayList<Project>();
				while (rs.next()) {
					Project project = new Project();
					project.setId(rs.getInt("PRJ_ID"));
					project.setName(rs.getString("PRJ_NAME"));
					project.setType(rs.getString("PRJ_TYPE"));
					project.setDomain(rs.getString("PRJ_DOMAIN"));
					project.setOwner(rs.getString("PRJ_OWNER"));
					project.setState(rs.getString("PRJ_STATE"));
					projects.add(project);
				}
			}
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage(), e);
		} 
		return projects;
	}

	public ArrayList<Project> getProjectsForDomain(String domainName) throws DatabaseException {
		ArrayList<Project> projects = null;

		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn
						.prepareStatement("select * from TJN_PROJECTS where prj_domain = ? order by prj_name");) {
			pst.setString(1, domainName);

			projects = new ArrayList<Project>();
			try (ResultSet rs = pst.executeQuery();) {
				while (rs.next()) {
					Project project = new Project();
					project.setId(rs.getInt("PRJ_ID"));
					project.setName(rs.getString("PRJ_NAME"));
					project.setType(rs.getString("PRJ_TYPE"));
					project.setDomain(rs.getString("PRJ_DOMAIN"));
					project.setOwner(rs.getString("PRJ_OWNER"));
					project.setState(rs.getString("PRJ_STATE"));
					projects.add(project);
				}
			}

		} catch (Exception e) {
			throw new DatabaseException(e.getMessage(), e);
		}
		return projects;
	}

	public String persistDomain(Domain domain) throws DatabaseException {
		
		String fResult = "";
		/* modified by Roshni for TENJINCG-1166 starts */
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			
				PreparedStatement pst1 = conn.prepareStatement("Select * From TJN_DOMAINS Where DOMAIN_NAME = ?");){
			
			boolean domainFound = false;
			pst1.setString(1,domain.getName());
			try (ResultSet rs = pst1.executeQuery();) {
				while(rs.next()){
					domainFound = true;
				}
			}
			/* modified by Roshni for TENJINCG-1166 ends */
			
			if(domainFound){
				fResult = "Record Already Exists";
			}else{
				try(PreparedStatement pst = conn.prepareStatement("INSERT INTO TJN_DOMAINS(DOMAIN_NAME,DOMAIN_CREATED_BY,DOMAIN_CREATE_DATE,DOMAIN_STATE) VALUES(?,?,?,?)");){
			
				
				pst.setString(1, domain.getName());
				pst.setString(2, domain.getOwner());
				pst.setTimestamp(3, domain.getCreatedDate());
				pst.setString(4, "A");
				pst.execute();
				fResult = "SUCCESS";
				}
			}
		}catch(Exception e){
			throw new DatabaseException("Could not create record",e);
		}
		return fResult;
	}

}
