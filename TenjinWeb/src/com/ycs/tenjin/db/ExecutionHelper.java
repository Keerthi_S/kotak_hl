/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutionHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 22-09-2016			SRIRAM					TJN_24_05 AND TJN_24_17
 * 14-12-2016			SRIRAM					Defect#TEN-23
 * 18-01-2017           Leelaprasad           	TENJINCG-42
 * 18-01-2017           Leelaprasad             TENJINCG-43
 * 03-Jul-2017			ROSHNI				    TENJINCG-259
 * 28-July-2017			Gangadhar Badagi		TENJINCG-315
 * 04-08-2017           Manish               	TENJINCG-330
 * 21-11-2017			Preeti					TENJINCG-484
 * 22-Dec-2017			Sahana					Mobility
 * 08-05-2018			Preeti					TENJINCG-625
 * 30-07-2018			Preeti					Closed connections
 * 08-03-2019			Sahana					pCloudy 
 * 04-03-2021			Prem					 TJN210-35
 */

package com.ycs.tenjin.db;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

/*Change History
02-Mar-2015 R2.1 Database agnostic Changes: Babu: NVL Not Supported in SQL Server,use 'coalesce'
08-04-2020     Lokanath               TENJINCG-1193
 */
public class ExecutionHelper {
	private static final Logger logger = LoggerFactory.getLogger(IterationHelper.class);

	

	/******************************************
	 * Added by Sriram for Requirement (TJN_22_01) Tenjin v2.2 starts
	 */
	public String isTestSetBeingExecuted(Connection projConn, int projectId, int testSetRecordId) throws DatabaseException{
		String lockUser = null;

		PreparedStatement pst =null;
		ResultSet rs=null;
		if(projConn == null){
			logger.error("Connection object to {} is null",Constants.DB_TENJIN_PROJ);
			throw new DatabaseException("Invalid or no database connection");
		}

		try{
		    pst = projConn.prepareStatement("SELECT * FROM MASTESTRUNS WHERE RUN_PRJ_ID=? AND RUN_TS_REC_ID=? AND RUN_STATUS=? ORDER BY RUN_ID DESC");
			pst.setInt(1, projectId);
			pst.setInt(2, testSetRecordId);
			pst.setString(3, "Executing");
			rs = pst.executeQuery();

			while(rs.next()){
				lockUser = rs.getString("RUN_USER");
				break;
			}
			
		}catch(Exception e){
			logger.error("ERROR in isTestStepBeingExecuted(int projectId, int testSetRecordId)");
			logger.error(e.getMessage(),e);
			throw new DatabaseException("An internal error occurred. Please contact your system administrator");
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		return lockUser;
	}

	

	

	/* Fix for timestamp Changing the type- Arvind */

	


	public void updateRunStatus(Connection conn,String runStatus, int runId) throws DatabaseException, SQLException{
		PreparedStatement pst =null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		try{
			pst = conn.prepareStatement("UPDATE MASTESTRUNS SET RUN_STATUS=? WHERE RUN_ID = ?");
			pst.setString(1, runStatus);
			pst.setInt(2, runId);
			pst.execute();
		}catch(Exception e){
			throw new DatabaseException("Could not update run status",e);
		}finally{
			DatabaseHelper.close(pst);
		}
		

	}

	

	/****************************************************************************************
	 * Block Added by Arvind to accomodate Requirement TJN_22_07 (Screentshots for information/error message encountered during execution) 15-09-2015 ENDS
	 */ 

	/**************************************************************
	 * Added by Sriram for Execution Screen overhaul (Tenjin v2.2) - 22-09-2015 begins
	 */

	public TestRun hydrateOngoingTestRun(Connection conn, int runId) throws DatabaseException{


		TestRun run = null;

		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;

		try{
			
			/*Modified by Sahana for pCloudy: starts*/
			pst = conn.prepareStatement("SELECT A.RUN_ID, A.RUN_STATUS AS RUN_COMPLETION_STATUS, A.RUN_START_TIME,A.RUN_END_TIME,A.RUN_USER,A.RUN_TS_REC_ID,A.RUN_MACHINE_IP,A.RUN_CL_PORT,A.RUN_BROWSER_TYPE,A.RUN_PRJ_NAME,A.RUN_PRJ_ID,A.RUN_DOMAIN_NAME, A.RUN_DEVICE_REC_ID, A.RUN_DEVICE_FARM, B.TOTAL_TCS,B.TOTAL_EXE_TCS,B.TOTAL_PASSED_TCS,B.TOTAL_FAILED_TCS,B.TOTAL_ERROR_TCS,B.RUN_STATUS FROM MASTESTRUNS A, V_RUNRESULTS B WHERE B.RUN_ID = A.RUN_ID AND A.RUN_ID = ?");
			/*Modified by Sahana for pCloudy: ends*/
		
			pst.setInt(1, runId);

			rs = pst.executeQuery();
			while(rs.next()){
				run = new TestRun();
				run.setId(rs.getInt("RUN_ID"));
				run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
				run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
				run.setUser(rs.getString("RUN_USER"));
				int tsId = rs.getInt("RUN_TS_REC_ID");
				run.setTestSetRecordId(tsId);
				run.setCompletionStatus(rs.getString("RUN_COMPLETION_STATUS"));
				run.setStatus(rs.getString("RUN_STATUS"));
				run.setMachine_ip(rs.getString("RUN_MACHINE_IP"));
				/*Added by Preeti for TENJINCG-484 starts*/
				run.setTargetPort(rs.getInt("RUN_CL_PORT"));
				/*Added by Preeti for TENJINCG-484 ends*/
				run.setBrowser_type(rs.getString("RUN_BROWSER_TYPE"));
				run.setProjectName(rs.getString("RUN_PRJ_NAME"));
				/*Added by Preeti for TENJINCG-625 starts*/
				run.setProjectId(rs.getInt("RUN_PRJ_ID"));
				/*Added by Preeti for TENJINCG-625 ends*/
				run.setDomainName(rs.getString("RUN_DOMAIN_NAME"));
				/*changed by sahana for Mobility: Starts*/
				run.setDeviceRecId(rs.getInt("RUN_DEVICE_REC_ID"));
				/*changed by sahana for Mobility: ends*/
				/*Added by Sahana for pCloudy: starts*/
				run.setDeviceFarmFlag(rs.getString("RUN_DEVICE_FARM"));
				/*Added by Sahana for pCloudy: ends*/
				if(run.getStartTimeStamp() != null && run.getEndTimeStamp() != null){
					long startMillis = run.getStartTimeStamp().getTime();
					long endMillis = run.getEndTimeStamp().getTime();
					long millis = endMillis - startMillis;

					String eTime = String.format(
							"%02d:%02d:%02d",
							TimeUnit.MILLISECONDS.toHours(millis),
							TimeUnit.MILLISECONDS.toMinutes(millis)
							- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
									.toHours(millis)),
									TimeUnit.MILLISECONDS.toSeconds(millis)
									- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
											.toMinutes(millis)));
					run.setElapsedTime(eTime);
				}else if(run.getStartTimeStamp() != null){
					long startMillis = run.getStartTimeStamp().getTime();
					long endMillis = System.currentTimeMillis();
					long millis = endMillis - startMillis;

					String eTime = String.format(
							"%02d:%02d:%02d",
							TimeUnit.MILLISECONDS.toHours(millis),
							TimeUnit.MILLISECONDS.toMinutes(millis)
							- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
									.toHours(millis)),
									TimeUnit.MILLISECONDS.toSeconds(millis)
									- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
											.toMinutes(millis)));
					run.setElapsedTime(eTime);
				}
				/*****************************************************
				 * Change by Sriram for Performance Tuning of Execution History
				 */
				run.setTotalTests(rs.getInt("TOTAL_TCS"));
				run.setExecutedTests(rs.getInt("TOTAL_EXE_TCS"));
				run.setPassedTests(rs.getInt("TOTAL_PASSED_TCS"));
				run.setFailedTests(rs.getInt("TOTAL_FAILED_TCS"));
				run.setErroredTests(rs.getInt("TOTAL_ERROR_TCS"));
				/*****************************************************
				 * Change by Sriram for Performance Tuning of Execution History ends
				 */

			}

			
		}catch(Exception e){
			
			logger.error("Hydrate run from MASTESTRUNS --> Failed",e);
			if(rs != null){
				try{
					rs.close();
				}catch(Exception e1){

				}
			}
			if(pst != null){
				try{
					pst.close();
				}catch(Exception e1){

				}
			}

			try{
			}catch(Exception e1){

			}
			throw new DatabaseException("Could not fetch run information",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}


		if(run == null){
			throw new DatabaseException("Could not fetch run information");
		}

		TestSet ts = null;
		try{
			//Fix for Defect#TEN-23 -Sriram
			/*pst = conn.prepareStatement("SELECT * FROM TESTSETS A LEFT join TESTSETMAP B on (A.TS_REC_ID = B.TSM_TS_REC_ID) WHERE A.TS_REC_ID = ?  AND A.TS_REC_TYPE = ? order by b.tsm_tc_seq");*/
			pst = conn.prepareStatement("SELECT * FROM TESTSETS A LEFT join TESTSETMAP B on (A.TS_REC_ID = B.TSM_TS_REC_ID) WHERE A.TS_REC_ID = ?  AND A.TS_REC_TYPE IN (?,?) order by b.tsm_tc_seq");
			pst.setInt(1, run.getTestSetRecordId());
			pst.setString(2, "TS");
			pst.setString(3, "TSA");//Fix for Defect#TEN-23 -Sriram ends

			rs = pst.executeQuery();

			ArrayList<TestCase> tests = new ArrayList<TestCase>();

			while(rs.next()){
				ts = new TestSet();
				ts.setName(rs.getString("TS_NAME"));
				ts.setRecordType(rs.getString("TS_REC_TYPE"));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_TYPE"));
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				//ts.setStatus(rs.getString("TS_STATUS"));
				ts.setDescription(rs.getString("TS_DESC"));

				int tcRecId = rs.getInt("TSM_TC_REC_ID");

				//Get the details of testcase along with results summary from TESTCASES and RUNRESULTS_TC
				PreparedStatement tPst = null;
				ResultSet tRs = null;
				try{
					tPst = conn.prepareStatement("SELECT A.*, B.TC_TOTAL_STEPS, B.TC_TOTAL_EXEC, B.TC_TOTAL_PASS, B.TC_TOTAL_FAIL, B.TC_TOTAL_ERROR, B.TC_STATUS AS TC_STAT FROM TESTCASES A, V_RUNRESULTS_TC B WHERE B.TC_REC_ID = A.TC_REC_ID AND A.TC_REC_ID = ? AND B.RUN_ID = ?");
					tPst.setInt(1, tcRecId);
					tPst.setInt(2, runId);
					tRs = tPst.executeQuery();

					while(tRs.next()){
						TestCase tc = new TestCase();
						tc.setTcId(tRs.getString("tc_id"));
						tc.setTcName(tRs.getString("tc_name"));
						tc.setTcType(tRs.getString("tc_type"));
						//tc.setTcStatus(tRs.getString("tc_status"));
						tc.setTcCreatedBy(tRs.getString("tc_created_by"));
						/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
						/*tc.setTcCreatedOn(tRs.getDate("tc_created_on"));*/
						tc.setTcCreatedOn(tRs.getTimestamp("tc_created_on"));
						/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
						tc.setTcModifiedBy(tRs.getString("tc_modified_by"));
						tc.setTcModifiedOn(tRs.getDate("tc_modified_on"));
						tc.setTcDesc(tRs.getString("tc_desc"));
						tc.setTcRecId(tRs.getInt("TC_REC_ID"));
						tc.setTcFolderId(tRs.getInt("TC_FOLDER_ID"));
						tc.setProjectId(tRs.getInt("TC_PRJ_ID"));
						tc.setRecordType(tRs.getString("TC_REC_TYPE"));
						tc.setTcReviewed(tRs.getString("TC_REVIEWED"));
						tc.setTcPriority(tRs.getString("TC_PRIORITY"));
						tc.setTcRecId(tRs.getInt("TC_REC_ID"));
						tc.setTotalSteps(tRs.getInt("TC_TOTAL_STEPS"));
						tc.setTotalExecutedSteps(tRs.getInt("TC_TOTAL_EXEC"));
						tc.setTotalPassedSteps(tRs.getInt("TC_TOTAL_PASS"));
						tc.setTotalFailedSteps(tRs.getInt("TC_TOTAL_FAIL"));
						tc.setTotalErrorredSteps(tRs.getInt("TC_TOTAL_ERROR"));
						tc.setTcStatus(tRs.getString("TC_STAT"));
						tests.add(tc);
					}

					tPst.close();
					tRs.close();
				}catch(Exception e){
					logger.error("Fetch Test Case Details from TESTCASES and V_RUNRESULTS_TC --> Failed",e);
					if(tPst != null){
						try{
							tPst.close();
						}catch(Exception e1){

						}
					}

					if(tRs != null){
						try{
							tRs.close();
						}catch(Exception e1){

						}
					}


					continue;
				}finally{
				
					DatabaseHelper.close(tRs);
					DatabaseHelper.close(tPst);
					
				}
			}
			ts.setTests(tests);
		}catch(Exception e){
			
			logger.error("Fetch Test Set details from TESTSETS and TESTSETMAP --> Failed",e);

			if(rs != null){
				try{
					rs.close();
				}catch(Exception e1){

				}
			}
			if(pst != null){
				try{
					pst.close();
				}catch(Exception e1){

				}
			}

			try{
			}catch(Exception e1){

			}

			throw new DatabaseException("Could not fetch details of the test set in the current run",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		ArrayList<TestCase> newTCs = new ArrayList<TestCase>();
		//For each test case in the test set, get all steps along with step results
		for(TestCase tc:ts.getTests()){
			ArrayList<TestStep> steps = null;
			try{
				pst = conn.prepareStatement("SELECT A.*, B.RUN_ID,B.TSTEP_TOTAL_TXN, B.TSTEP_TOTAL_EXEC, B.TSTEP_TOTAL_PASS, B.TSTEP_TOTAL_FAIL, B.TSTEP_TOTAL_ERROR,B.TSTEP_STATUS AS TSTEP_STAT FROM TESTSTEPS A, V_RUNRESULTS_TS B WHERE B.TSTEP_REC_ID = A.TSTEP_REC_ID AND A.TC_REC_ID = ? AND B.RUN_ID = ? ORDER BY A.TSTEP_REC_ID");
				pst.setInt(1, tc.getTcRecId());
				pst.setInt(2, runId);
				rs = pst.executeQuery();
				steps = new ArrayList<TestStep>();
				while(rs.next()){
					TestStep t = new TestStep();
					t.setRecordId(rs.getInt("tstep_rec_id"));
					t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
					t.setDataId(rs.getString("TSTEP_DATA_ID"));
					t.setType(rs.getString("TSTEP_TYPE"));
					t.setDescription(rs.getString("TSTEP_DESC"));
					t.setExpectedResult(rs.getString("TSTEP_EXP_RESULT"));
					t.setAppId(rs.getInt("APP_ID"));
					t.setModuleCode(rs.getString("FUNC_CODE"));
					t.setId(rs.getString("TSTEP_ID"));
					t.setTestCaseRecordId(rs.getInt("TC_REC_ID"));
					t.setOperation(rs.getString("TSTEP_OPERATION"));
					t.setAutLoginType(rs.getString("TSTEP_AUT_LOGIN_TYPE"));
					t.setTotalTransactions(rs.getInt("TSTEP_TOTAL_TXN"));
					t.setTotalExecutedTxns(rs.getInt("TSTEP_TOTAL_EXEC"));
					t.setTotalPassedTxns(rs.getInt("TSTEP_TOTAL_PASS"));
					t.setTotalFailedTxns(rs.getInt("TSTEP_TOTAL_FAIL"));
					t.setTotalErroredTxns(rs.getInt("TSTEP_TOTAL_ERROR"));
					t.setStatus(rs.getString("TSTEP_STAT"));
					

					//Get Detailed step results from RUNRESULT_TS_TXN
					ArrayList<StepIterationResult> results= new ArrayList<StepIterationResult>();

					PreparedStatement rPst = null;
					ResultSet oRs = null;

					try{
						rPst = conn.prepareStatement("SELECT * FROM RUNRESULT_TS_TXN WHERE TSTEP_REC_ID = ? AND RUN_ID = ? ORDER BY TXN_NO");
						rPst.setInt(1, t.getRecordId());
						rPst.setInt(2, runId);
						oRs = rPst.executeQuery();
						while(oRs.next()){
							StepIterationResult r = new StepIterationResult();
							r.setIterationNo(oRs.getInt("TXN_NO"));
							r.setResult(oRs.getString("TSTEP_RESULT"));
							r.setMessage(oRs.getString("TSTEP_MESSAGE"));
							Blob b = oRs.getBlob("TSTEP_SCRSHOT");
							if(b != null){
								byte barr[] = b.getBytes(1,(int)b.length());
								r.setScreenshotByteArray(barr);
							}else{
								r.setScreenshotByteArray(null);
							}
							r.setTduid(oRs.getString("TSTEP_TDUID"));
							//Get Result Validation Results

							PreparedStatement vPst = null;
							ResultSet vRs = null;
							List<ValidationResult> valResults = new ArrayList<ValidationResult>();
							try{
								vPst = conn.prepareStatement("SELECT * FROM RESVALRESULTS WHERE RUN_ID = ? AND RES_VAL_ID =  ? AND TSTEP_REC_ID = ? AND ITERATION =?");
								vPst.setInt(1, runId);
								vPst.setInt(2, 0);
								vPst.setInt(3, t.getRecordId());
								vPst.setInt(4, r.getIterationNo());
								vRs = vPst.executeQuery();
								while(vRs.next()){
									ValidationResult res = new ValidationResult();
									res.setRunId(vRs.getInt("RUN_ID"));
									res.setResValId(vRs.getInt("RES_VAL_ID"));
									res.setTestStepRecordId(vRs.getInt("TSTEP_REC_ID"));
									res.setPage(vRs.getString("RES_VAL_PAGE"));
									res.setField(vRs.getString("RES_VAL_FIELD"));
									res.setExpectedValue(vRs.getString("RES_VAL_EXP_VAL"));
									res.setActualValue(vRs.getString("RES_VAL_ACT_VAL"));
									res.setStatus(vRs.getString("RES_VAL_STATUS"));
									res.setIteration(vRs.getInt("ITERATION"));
									res.setDetailRecordNo(vRs.getString("RES_VAL_DETAIL_ID"));
									valResults.add(res);
								}
								
								r.setValidationResults(valResults);
								
								vPst.close();
								vRs.close();
							}catch(Exception e){
								try{
									vPst.close();
								}catch(Exception e1){

								}
								try{
									vRs.close();
								}catch(Exception e2){

								}

								logger.error("Get Result Validation Results for Step (record id - " + t.getRecordId() + ") from RESVALRESULTS --> Failed",e);
								continue;
							}finally{
								DatabaseHelper.close(vRs);
								DatabaseHelper.close(vPst);
							}
						
							
							results.add(r);
						}

						rPst.close();
						oRs.close();
					}catch(Exception e){
						try{
							rPst.close();
						}catch(Exception e1){

						}

						try{
							oRs.close();
						}catch(Exception e1){

						}



						logger.error("Get Detailed Step result from RUNRESULT_TS_TXN --> Failed",e);
						continue;
					}finally{
						DatabaseHelper.close(oRs);
						DatabaseHelper.close(rPst);
					}

					t.setDetailedResults(results);
					steps.add(t);
				}

				pst.close();
				rs.close();
			}catch(Exception e){
				logger.error("Fetch steps for Test Case " + tc.getTcId() + " from TESTSTEPS and RUNRESULTS_TS --> Failed",e);
				try{
					pst.close();
				}catch(Exception e1){

				}

				try{
					rs.close();
				}catch(Exception e1){

				}


				continue;
			}finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}

			tc.setTcSteps(steps);
			newTCs.add(tc);
		}
		ts.setTests(null);
		ts.setTests(newTCs);
		run.setTestSet(null);
		run.setTestSet(ts);

		
		return run;



	}
	

	/**************************************************************
	 * Added by Sriram for Execution Screen overhaul (Tenjin v2.2) - 22-09-2015 ends
	 */


	/*Changed by Gangadhar Badagi for TENJINCG-315 starts*/
	/*public ArrayList<TestRun> hydrateRunsForProject(int projectId) throws DatabaseException{*/
	public ArrayList<TestRun> hydrateRunsForProject(int projectId,String userId,String status) throws DatabaseException{
		
		/*Changed by Gangadhar Badagi for TENJINCG-315 ends*/
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<TestRun> runs = new ArrayList<TestRun>();

		try{
			if(userId==null && status==null){
					pst = conn.prepareStatement("SELECT A.RUN_ID, A.RUN_TASK_TYPE, A.RUN_START_TIME, A.RUN_END_TIME, A.RUN_USER, A.RUN_MACHINE_IP, A.RUN_BROWSER_TYPE,A.RUN_TS_REC_ID, B.TOTAL_TCS, B.TOTAL_EXE_TCS, B.TOTAL_PASSED_TCS, B.TOTAL_FAILED_TCS, B.TOTAL_ERROR_TCS, B.RUN_STATUS,C.TS_NAME FROM MASTESTRUNS A, V_RUNRESULTS B, TESTSETS C WHERE B.RUN_ID=A.RUN_ID AND A.RUN_PRJ_ID=? AND A.RUN_TS_REC_ID = C.TS_REC_ID ORDER BY A.RUN_ID DESC");
				}
			
			else if(userId!=null){
				pst = conn.prepareStatement("SELECT A.RUN_ID, A.RUN_TASK_TYPE, A.RUN_START_TIME, A.RUN_END_TIME, A.RUN_USER, A.RUN_MACHINE_IP, A.RUN_BROWSER_TYPE,A.RUN_TS_REC_ID, B.TOTAL_TCS, B.TOTAL_EXE_TCS, B.TOTAL_PASSED_TCS, B.TOTAL_FAILED_TCS, B.TOTAL_ERROR_TCS, B.RUN_STATUS,C.TS_NAME FROM MASTESTRUNS A, V_RUNRESULTS B, TESTSETS C WHERE B.RUN_ID=A.RUN_ID AND A.RUN_PRJ_ID=? AND A.RUN_USER=? AND A.RUN_TS_REC_ID = C.TS_REC_ID ORDER BY A.RUN_ID DESC");
				pst.setString(2, userId);
			}
			else if(status!=null){
				pst = conn.prepareStatement("SELECT A.RUN_ID, A.RUN_TASK_TYPE, A.RUN_START_TIME, A.RUN_END_TIME, A.RUN_USER, A.RUN_MACHINE_IP, A.RUN_BROWSER_TYPE,A.RUN_TS_REC_ID, B.TOTAL_TCS, B.TOTAL_EXE_TCS, B.TOTAL_PASSED_TCS, B.TOTAL_FAILED_TCS, B.TOTAL_ERROR_TCS, B.RUN_STATUS,C.TS_NAME FROM MASTESTRUNS A, V_RUNRESULTS B, TESTSETS C WHERE B.RUN_ID=A.RUN_ID AND A.RUN_PRJ_ID=? AND A.RUN_STATUS=? AND A.RUN_TS_REC_ID = C.TS_REC_ID ORDER BY A.RUN_ID DESC");
					pst.setString(2, status);
				}
			pst.setInt(1, projectId);
			

			rs = pst.executeQuery();
			while(rs.next()){
				TestRun run = new TestRun();
				/* To Include Test Set name in the run details - Arvind */
				TestSet tSet = new TestSet();
				/* To Include Test Set name in the run details - Arvind */
				
				run.setId(rs.getInt("RUN_ID"));
				run.setUser(rs.getString("RUN_USER"));
				run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
				run.setMachine_ip(rs.getString("RUN_MACHINE_IP"));
				run.setStatus(rs.getString("RUN_STATUS"));
				run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
				if(run.getEndTimeStamp() != null && run.getStartTimeStamp() != null){
					String elapsedTime= Utilities.calculateElapsedTime(run.getStartTimeStamp().getTime(), run.getEndTimeStamp().getTime());
					run.setElapsedTime(elapsedTime);
				}
				/*Changed by Gangadhar Badagi for TENJINCG-315 starts*/
				int tsRecId=rs.getInt("RUN_TS_REC_ID");
				run.setTestSetRecordId(tsRecId);
				/*Changed by Gangadhar Badagi for TENJINCG-315 ends*/
				run.setTotalTests(rs.getInt("TOTAL_TCS"));
				run.setExecutedTests(rs.getInt("TOTAL_EXE_TCS"));
				run.setPassedTests(rs.getInt("TOTAL_PASSED_TCS"));
				run.setFailedTests(rs.getInt("TOTAL_FAILED_TCS"));
				run.setErroredTests(rs.getInt("TOTAL_ERROR_TCS"));
				
				/****
				 * Added by Sriram for TENJINCG-168 (API Execution)
				 */
				
				run.setTaskType(rs.getString("RUN_TASK_TYPE"));
				
				/****
				 * Added by Sriram for TENJINCG-168 (API Execution) ends
				 */
				/* To Include Test Set name in the run details - Arvind */
				tSet.setName(rs.getString("TS_NAME"));
				run.setTestSet(tSet);
				/* To Include Test Set name in the run details - Arvind */
				
				runs.add(run);
			}
		}catch(Exception e){
			logger.error("ERROR Could not fetch runs for project {}", projectId, e);
			throw new DatabaseException("Could not fetch runs due to an internal error");
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}


		return runs;
	}


	public ArrayList<TestRun> filterProjectRuns(int projectId, int runId, String userId, String machineIp, String status) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		String query = "";

		StringBuilder builder = new StringBuilder();

		builder.append("WHERE ");

		if(runId > 0){
			builder.append("RUN_ID = " + runId + " AND ");
		}

		if(userId != null && !userId.equalsIgnoreCase("")){
			builder.append("RUN_USER = '" + userId + "' AND ");
		}

		if(machineIp != null && !machineIp.equalsIgnoreCase("")){
			builder.append("RUN_MACHINE_IP = '" + machineIp + "' AND ");
		}

		if(status != null && !status.equalsIgnoreCase("")){
			builder.append("RUN_STATUS = '" + status + "' AND ");
		}

		query = "SELECT * FROM MASTESTRUNS " + builder.toString();
		query = Utilities.trim(query);

		if(query.endsWith("AND")){
			query = query.substring(0, query.length()-3);
		}


		Statement st = null;
		ResultSet rs = null;
		ArrayList<TestRun> runs = new ArrayList<TestRun>();
		try{
			st = conn.createStatement();
			rs = st.executeQuery(query);
			while(rs.next()){
				TestRun run = new TestRun();
				run.setId(rs.getInt("RUN_ID"));
				run.setUser(rs.getString("RUN_USER"));
				run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
				run.setMachine_ip(rs.getString("RUN_MACHINE_IP"));
				run.setStatus(rs.getString("RUN_STATUS"));
				runs.add(run);
			}

		}catch(Exception e){
			logger.error("ERROR Could not fetch runs for project {}", projectId, e);
			throw new DatabaseException("Could not fetch runs due to an internal error");
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(st);
			DatabaseHelper.close(conn);
		}

		return runs;

	}


	/*Changed by Leelaprasad for the Requirement TENJINCG-42 starts*/
	public TestRun fetchRunStatus(int runId) throws DatabaseException{
		TestRun run = null;

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;

		try{
			pst = conn.prepareStatement("SELECT * FROM MASTESTRUNS WHERE RUN_ID = ?");
			pst.setInt(1, runId);

			rs = pst.executeQuery();
			while(rs.next()){
				run = new TestRun();
				run.setId(rs.getInt("RUN_ID"));
				run.setStatus(rs.getString("RUN_STATUS"));
			}

			
		}catch(Exception e){
			
			logger.error("Hydrate run from MASTESTRUNS --> Failed",e);
			
			throw new DatabaseException("Could not fetch run information",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return run;


	}
	/*Changed by Leelaprasad for the Requirement TENJINCG-42 ends*/
	
	
	/*Added by Lokanath for TENJINCG-1193 starts*/
	public TestRun hydrateOngoingLearnTestRun(int runId) throws DatabaseException{
		TestRun run = null;
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("SELECT A.*, B.MODULE_NAME,B.GROUP_NAME,B.MODULE_MENU_CONTAINER FROM LRNR_AUDIT_TRAIL A, MASMODULE B WHERE B.APP_ID=A.LRNR_APP_ID AND B.MODULE_CODE=A.LRNR_FUNC_CODE AND A.LRNR_RUN_ID=? ORDER BY A.LRNR_RUN_ID");
			pst.setInt(1, runId);
			
			rs = pst.executeQuery();
			List<Module> functions = new ArrayList<Module>();
			Module module = null;
			LearnerResultBean result = null;
			while(rs.next()){
				module = new Module();
				result = new LearnerResultBean();
				result.setStartTimestamp(rs.getTimestamp("LRNR_START_TIME"));
				result.setEndTimestamp(rs.getTimestamp("LRNR_END_TIME"));
				result.setLearnStatus(rs.getString("LRNR_STATUS"));
				result.setMessage(rs.getString("LRNR_MESSAGE"));
				module.setLastSuccessfulLearningResult(result);
				module.setCode(rs.getString("LRNR_FUNC_CODE"));
				module.setName(rs.getString("MODULE_NAME"));
				module.setMenuContainer(rs.getString("MODULE_MENU_CONTAINER"));
				module.setGroup(rs.getString("GROUP_NAME"));
				functions.add(module);
			}
			pst.close();
			rs.close();
			run = new TestRun();
			run.setId(runId);
			run.setFunctions(functions);
		} catch (SQLException e) {
			logger.error("ERROR Occurred while fetching details of Learning Run [{}]", runId, e);
			throw new DatabaseException("Could not fetch learning status due to an unexpected error. Please contact Tenjin Support.");
		} finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return run;
		}
	/*Added by Lokanath for TENJINCG-1193 ends*/
	
	
}
