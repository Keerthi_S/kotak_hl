/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LicenseHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 20-06-2019            Padmavathi              Added for License TENJINCG-1081

* */

package com.ycs.tenjin.db;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.util.Constants;

public class LicenseHelper {
	private static final Logger logger = LoggerFactory.getLogger(LicenseHelper.class);
	
	public List<String> hydrateLicenseKey() throws DatabaseException{
		List<String> licensekeys=new ArrayList<>();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			PreparedStatement pst=conn.prepareStatement("SELECT * FROM PRODUCTLICENSE");
			ResultSet rs=pst.executeQuery();){
			while(rs.next()){
				licensekeys.add(this.clobStringConversion(rs.getClob("LICENSE_KEY")));
				if(rs.getClob("RENEWAL_LICENSE_KEY")!=null)
				licensekeys.add(this.clobStringConversion(rs.getClob("RENEWAL_LICENSE_KEY")));
			}
			
		}catch(SQLException | IOException e){
			logger.error("Could not get license key", e.getMessage());
			throw new DatabaseException("Could not fetch License key. Please contact Tenjin Support.");
		}
		return licensekeys;
	}
	
	public void persistLicenseKey(String licenseKey) throws DatabaseException{
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("INSERT INTO PRODUCTLICENSE (LICENSE_KEY) VALUES(?)")){
				Clob myClob = conn.createClob();
				myClob.setString( 1, licenseKey);
				pst.setClob(1, myClob);
				pst.execute();
			}catch(SQLException e){
				logger.error("Could not get license key", e.getMessage());
				throw new DatabaseException("Internal error occured.Please contact Tenjin Support.");
			}
	}	public void persistRenewalLicenseKey(String renewalLicenseKey) throws DatabaseException{
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("UPDATE PRODUCTLICENSE SET RENEWAL_LICENSE_KEY=?")){
				Clob myClob = conn.createClob();
				myClob.setString( 1, renewalLicenseKey);
				pst.setClob(1, myClob);
				pst.execute();
			}catch(SQLException e){
				logger.error("Could not get license key", e.getMessage());
				throw new DatabaseException("Internal error occured.Please contact Tenjin Support.");
			}
	}
	public void updateLicenseKeyFromRenewalLicenseKey() throws DatabaseException{

		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("UPDATE PRODUCTLICENSE SET RENEWAL_LICENSE_KEY=?,LICENSE_KEY=(SELECT RENEWAL_LICENSE_KEY FROM PRODUCTLICENSE)")){
				pst.setNull(1, java.sql.Types.CLOB);
				pst.execute();
			}catch(SQLException e){
				logger.error("Could not update license key", e.getMessage());
				throw new DatabaseException("Could not update License key. Please contact Tenjin Support.");
			}
	
	}
	public void updateLicenseKey(String licenseKey) throws DatabaseException{

		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			PreparedStatement pst=conn.prepareStatement("UPDATE PRODUCTLICENSE SET LICENSE_KEY=?")){
			Clob myClob = conn.createClob();
			myClob.setString( 1, licenseKey);
			pst.setClob(1, myClob);
			pst.execute();
			}catch(SQLException e){
				logger.error("Could not update license key", e.getMessage());
				throw new DatabaseException("Could not update License key. Please contact Tenjin Support.");
			}
	
	}
	public void deleteLicenseKey() throws DatabaseException{

		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("DELETE FROM PRODUCTLICENSE ")){
				pst.execute();
			}catch(SQLException e){
				logger.error("Could not get license key", e.getMessage());
				throw new DatabaseException("Could not delete License key. Please contact Tenjin Support.");
			}
	}
	public  String clobStringConversion(Clob clb) throws IOException, SQLException
    {
      StringBuffer str = new StringBuffer();
      String strng;
      BufferedReader bufferRead = new BufferedReader(clb.getCharacterStream());
      while ((strng=bufferRead .readLine())!=null)
       str.append(strng);
    
      return str.toString();
    }     
}
