/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutionSchemeHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 02-12-2016            Parveen              REQ #TJN_243_04  Changing insert query
* 30-07-2018			Preeti					Closed connections
* 
*/

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.project.ExecutionScheme;
import com.ycs.tenjin.util.Constants;

public class ExecutionSchemeHelper {
	private static final Logger logger = LoggerFactory.getLogger(ExecutionSchemeHelper.class);

	public void persistScheme(ExecutionScheme scheme) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;

		int schId = 0;
		try{
			pst = conn.prepareStatement("SELECT coalesce(MAX(SCH_ID),0) AS SCH_ID FROM TJN_EXEC_SCHEME");
			rs = pst.executeQuery();
			while(rs.next()){
				schId = rs.getInt("SCH_ID");
			}
		}catch(Exception e){
			logger.error("ERROR getting new Scheme ID for Execution Scheme",e);
		
			throw new DatabaseException("Could not insert record due to an internal error",e);
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		
			


		}

		schId++;
		scheme.setId(schId);
		try{
			/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/

			pst = conn.prepareStatement("INSERT INTO TJN_EXEC_SCHEME (SCH_ID,SCH_NAME,SCH_DESC,APP_ID,FUNC_CODE,BASE_PAGE_AREA,SCH_ENABLED,CREATE_TIMESTAMP,CREATED_BY) VALUES (?,?,?,?,?,?,?,?,?)");
			/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/

			pst.setInt(1, schId);
			pst.setString(2, scheme.getName());
			pst.setString(3, scheme.getDescription());
			pst.setInt(4, scheme.getAppId());
			pst.setString(5, scheme.getFunctionCode());
			pst.setString(6, scheme.getBasePageArea());
			if(scheme.isEnabled()){
				pst.setString(7, "Y");
			}else{
				pst.setString(7, "N");
			}
			pst.setTimestamp(8, new Timestamp(new Date().getTime()));
			pst.setString(9, scheme.getCreatedBy());
			pst.execute();
			logger.info("Scheme inserted successfully");
			pst.close();
		}catch(Exception e){
			logger.error("ERROR inserting new scheme",e);
			try{
				conn.rollback();
			}catch(Exception e1){
				logger.warn("Could not rollback after error in inserting new scheme",e1);
			}
			try{
				conn.close();
			}catch(Exception e1){
				logger.warn("Could not close DB Connection after error while inserting new scheme",e1);
			}
			throw new DatabaseException("Could not insert record due to an internal error",e);
		}finally{

			DatabaseHelper.close(pst);	
		}

		//INSERT ACTIONS CONFIG
		
			for(TestObject t:scheme.getActions()){
				try{
				/*Changed by  Parveen for  REQ #TJN_243_04 starts*/

				pst = conn.prepareStatement("INSERT INTO TJN_EXEC_SCHEME_CONFIG (SCH_ID,ACT_BLOCK,PAGE_AREA,FIELD,ACTION,SEQ_NO) VALUES (?,?,?,?,?,?)");
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 ends*/

				pst.setInt(1, scheme.getId());
				pst.setString(2, "ACTION");
				pst.setString(3, t.getLocation());
				pst.setString(4, t.getLabel());
				pst.setString(5, t.getAction());
				pst.setInt(6, t.getSequence());
				pst.execute();
				pst.close();
				}catch(Exception e){
					logger.error("ERROR inserting scheme config data",e);
					try{
						conn.rollback();
					}catch(Exception e1){
						logger.warn("Could not rollback after error in inserting new scheme",e1);
					}
					try{
						conn.close();
					}catch(Exception e1){
						logger.warn("Could not close DB Connection after error while inserting new scheme",e1);
					}
					throw new DatabaseException("Could not insert record due to an internal error",e);
				}finally{
					
		        DatabaseHelper.close(pst);
					}
			}
	

		//INSERT PRE-ACTIONS CONFIG
		
			for(TestObject t:scheme.getPreActions()){
				try{
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/

				pst = conn.prepareStatement("INSERT INTO TJN_EXEC_SCHEME_CONFIG (SCH_ID,ACT_BLOCK,PAGE_AREA,FIELD,ACTION,SEQ_NO) VALUES (?,?,?,?,?,?)");
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 ends*/

				pst.setInt(1, scheme.getId());
				pst.setString(2, "PRE");
				pst.setString(3, t.getLocation());
				pst.setString(4, t.getLabel());
				pst.setString(5, t.getAction());
				pst.setInt(6, t.getSequence());
				pst.execute();
				pst.close();
				}catch(Exception e){
					logger.error("ERROR inserting scheme config data",e);
					try{
						conn.rollback();
					}catch(Exception e1){
						logger.warn("Could not rollback after error in inserting new scheme",e1);
					}
					try{
						conn.close();
					}catch(Exception e1){
						logger.warn("Could not close DB Connection after error while inserting new scheme",e1);
					}
					throw new DatabaseException("Could not insert record due to an internal error",e);
				}finally{
				
					DatabaseHelper.close(pst);
				}
			}
		

		//INSERT POST-ACTIONS CONFIG
		
			for(TestObject t:scheme.getPostActions()){
				try{
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/

				pst = conn.prepareStatement("INSERT INTO TJN_EXEC_SCHEME_CONFIG (SCH_ID,ACT_BLOCK,PAGE_AREA,FIELD,ACTION,SEQ_NO) VALUES (?,?,?,?,?,?)");
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 ends*/

				pst.setInt(1, scheme.getId());
				pst.setString(2, "POST");
				pst.setString(3, t.getLocation());
				pst.setString(4, t.getLabel());
				pst.setString(5, t.getAction());
				pst.setInt(6, t.getSequence());
				pst.execute();
				pst.close();
				}catch(Exception e){
					logger.error("ERROR inserting scheme config data",e);
					try{
						conn.rollback();
					}catch(Exception e1){
						logger.warn("Could not rollback after error in inserting new scheme",e1);
					}
					try{
						conn.close();
					}catch(Exception e1){
						logger.warn("Could not close DB Connection after error while inserting new scheme",e1);
					}
					throw new DatabaseException("Could not insert record due to an internal error",e);
				}finally{
					
					DatabaseHelper.close(pst);
				}
			}
		
		
		/*try{
			conn.commit();
		}catch(Exception e){
			logger.error("Could not commit after inserting new scheme",e);
			throw new DatabaseException("Could not insert record due to an internal error",e);
		}*/
		
		try{
			conn.close();
		}catch(Exception e){
			logger.warn("Could not close DB connection after persisting new scheme",e);
		}finally{
			DatabaseHelper.close(conn);
		}
	}
	
	public ArrayList<ExecutionScheme> hydrateSchemes(int appId, String functionCode, String schemePageAreaName) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		ArrayList<ExecutionScheme> schemes = new ArrayList<ExecutionScheme>();
		
		Statement st = null;
		ResultSet rs = null;
		
		String query = "";
		ArrayList<String> queryFilters = new ArrayList<String>();
		if(appId > 0){
			queryFilters.add("APP_ID=" + appId);
		}
		
		if(functionCode != null && !functionCode.equalsIgnoreCase("")){
			queryFilters.add("FUNC_CODE='" + functionCode + "'");
		}
		
		if(schemePageAreaName != null && !schemePageAreaName.equalsIgnoreCase("")){
			queryFilters.add("BASE_PAGE_AREA='" + schemePageAreaName + "'");
		}
		
		query = "SELECT * FROM TJN_EXEC_SCHEME ";
		int fCount=0;
		if(queryFilters.size() > 0){
			for(String filter:queryFilters){
				if(fCount < 1){
					query = query + "WHERE ";
				}else{
					query = query + "AND ";
				}
				
				query = query + filter + " ";
				fCount++;
			}
		}
		
		query = query + " ORDER BY SCH_ID";
		
		try{
			st = conn.createStatement();
			rs = st.executeQuery(query);
			
			schemes = new ArrayList<ExecutionScheme>();
			while(rs.next()){
				int schId = rs.getInt("SCH_ID");
				ExecutionScheme scheme = this.hydrateScheme(conn, schId);
				schemes.add(scheme);
			}
		}catch(Exception e){
			logger.error("ERROR querying for schemes",e);
			throw new DatabaseException("Could not retrieve data due to an internal error",e);
		}finally{
			
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(st);
			DatabaseHelper.close(conn);
		}
		
		return schemes;
	}
	
	
	
	
	public ExecutionScheme hydrateScheme(Connection conn, int schemeId) throws DatabaseException{
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		ExecutionScheme scheme = null;
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try{
			//Hydrate from TJN_EXEC_SCHEME
			logger.info("Fetching basic data for scheme with ID {}", schemeId);
			pst = conn.prepareStatement("SELECT * FROM TJN_EXEC_SCHEME WHERE SCH_ID=?");
			pst.setInt(1, schemeId);
			
			rs = pst.executeQuery();
			scheme = new ExecutionScheme();
			String enabled ="";
			while(rs.next()){
				scheme.setId(rs.getInt("SCH_ID"));
				scheme.setName(rs.getString("SCH_NAME"));
				scheme.setDescription(rs.getString("SCH_DESC"));
				scheme.setAppId(rs.getInt("APP_ID"));
				scheme.setFunctionCode(rs.getString("FUNC_CODE"));
				scheme.setBasePageArea(rs.getString("BASE_PAGE_AREA"));
				enabled = rs.getString("SCH_ENABLED");
				if(enabled != null && enabled.equalsIgnoreCase("Y")){
					scheme.setEnabled(true);
				}else{
					scheme.setEnabled(false);
				}
				
				scheme.setCreatedTimeStamp(rs.getTimestamp("CREATE_TIMESTAMP"));
				scheme.setCreatedBy(rs.getString("CREATED_BY"));
			}
			
			
			pst.close();
			rs.close();
			
			logger.info("Fetching configuration data for scheme {}", scheme.getName());
			ArrayList<TestObject> actions = new ArrayList<TestObject>();
			logger.info("Fetching Action configuration");
			pst = conn.prepareStatement("SELECT * FROM TJN_EXEC_SCHEME_CONFIG WHERE SCH_ID = ? AND ACT_BLOCK=? ORDER BY SEQ_NO");
			pst.setInt(1, scheme.getId());
			pst.setString(2, "ACTION");
			rs = pst.executeQuery();
			while(rs.next()){
				TestObject t = new TestObject();
				t.setAction(rs.getString("ACTION"));
				t.setLocation(rs.getString("PAGE_AREA"));
				t.setLabel(rs.getString("FIELD"));
				t.setSequence(rs.getInt("SEQ_NO"));
				actions.add(t);
			}
			rs.close();
			pst.close();
			
			logger.info("Fetching Pre-Action Configuration");
			ArrayList<TestObject> preActions = new ArrayList<TestObject>();
			pst = conn.prepareStatement("SELECT * FROM TJN_EXEC_SCHEME_CONFIG WHERE SCH_ID = ? AND ACT_BLOCK=? ORDER BY SEQ_NO");
			pst.setInt(1, scheme.getId());
			pst.setString(2, "PRE");
			rs = pst.executeQuery();
			while(rs.next()){
				TestObject t = new TestObject();
				t.setAction(rs.getString("ACTION"));
				t.setLocation(rs.getString("PAGE_AREA"));
				t.setLabel(rs.getString("FIELD"));
				t.setSequence(rs.getInt("SEQ_NO"));
				preActions.add(t);
			}
			rs.close();
			pst.close();
			
			logger.info("Fetching Post-Action Configuration");
			ArrayList<TestObject> postActions = new ArrayList<TestObject>();
			pst = conn.prepareStatement("SELECT * FROM TJN_EXEC_SCHEME_CONFIG WHERE SCH_ID = ? AND ACT_BLOCK=? ORDER BY SEQ_NO");
			pst.setInt(1, scheme.getId());
			pst.setString(2, "POST");
			rs = pst.executeQuery();
			while(rs.next()){
				TestObject t = new TestObject();
				t.setAction(rs.getString("ACTION"));
				t.setLocation(rs.getString("PAGE_AREA"));
				t.setLabel(rs.getString("FIELD"));
				t.setSequence(rs.getInt("SEQ_NO"));
				postActions.add(t);
			}
			rs.close();
			pst.close();
			
			scheme.setPreActions(preActions);
			scheme.setPostActions(postActions);
			scheme.setActions(actions);
		}catch(Exception e){
			logger.error("ERROR hydrating scheme with ID {}", schemeId);
			throw new DatabaseException("Could not fetch record due to an internal error",e);
		}finally{
		
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
		return scheme;
	}
	
	public void updateScheme(ExecutionScheme scheme) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;

	
		try{
			logger.debug("Updating basic info");
			pst = conn.prepareStatement("UPDATE TJN_EXEC_SCHEME SET SCH_NAME=?, SCH_DESC=?, APP_ID=?, FUNC_CODE=?, BASE_PAGE_AREA=? WHERE SCH_ID=?");
			pst.setString(1, scheme.getName());
			pst.setString(2, scheme.getDescription());
			pst.setInt(3, scheme.getAppId());
			pst.setString(4, scheme.getFunctionCode());
			pst.setString(5, scheme.getBasePageArea());
			pst.setInt(6, scheme.getId());
			pst.executeUpdate();
			pst.close();
			logger.debug("Done");
			
			logger.debug("Clearing existing rule configuration data");
			pst = conn.prepareStatement("DELETE FROM TJN_EXEC_SCHEME_CONFIG WHERE SCH_ID=?");
			pst.setInt(1, scheme.getId());
			pst.execute();
			pst.close();
			logger.debug("Done");
			
			logger.debug("Updating rule actions");
			for(TestObject t:scheme.getActions()){
				try{
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/

				pst = conn.prepareStatement("INSERT INTO TJN_EXEC_SCHEME_CONFIG (SCH_ID,ACT_BLOCK,PAGE_AREA,FIELD,ACTION,SEQ_NO) VALUES (?,?,?,?,?,?)");
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 ends*/

				pst.setInt(1, scheme.getId());
				pst.setString(2, "ACTION");
				pst.setString(3, t.getLocation());
				pst.setString(4, t.getLabel());
				pst.setString(5, t.getAction());
				pst.setInt(6, t.getSequence());
				pst.execute();
				pst.close();
				}catch(Exception e){
					logger.error("ERROR occurred while updating scheme details",e);
					try{
						conn.rollback();
					}catch(Exception e1){

					}
					try{
						conn.close();
					}catch(Exception e1){
						logger.error("ERROR while closing connection after failed update",e);
					}
					throw new DatabaseException("Could not update rule due to an internal error");
				}finally{
				
					DatabaseHelper.close(pst);
				}
			}
			logger.debug("Done");
			
			logger.debug("Updating rule pre-actions");
			for(TestObject t:scheme.getPreActions()){
				try{
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/

				/*pst = conn.prepareStatement("INSERT INTO TJN_EXEC_SCHEME_CONFIG VALUES (?,?,?,?,?,?)");*/
				pst = conn.prepareStatement("INSERT INTO TJN_EXEC_SCHEME_CONFIG (SCH_ID,ACT_BLOCK,PAGE_AREA,FIELD,ACTION,SEQ_NO) VALUES (?,?,?,?,?,?)");
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 ends*/

				pst.setInt(1, scheme.getId());
				pst.setString(2, "PRE");
				pst.setString(3, t.getLocation());
				pst.setString(4, t.getLabel());
				pst.setString(5, t.getAction());
				pst.setInt(6, t.getSequence());
				pst.execute();
				pst.close();
			}catch(Exception e){
				logger.error("ERROR occurred while updating scheme details",e);
				try{
					conn.rollback();
				}catch(Exception e1){

				}
				try{
					conn.close();
				}catch(Exception e1){
					logger.error("ERROR while closing connection after failed update",e);
				}
				throw new DatabaseException("Could not update rule due to an internal error");
			}finally{
			
				DatabaseHelper.close(pst);
			}
			}
			logger.debug("Done");
			
			logger.debug("Updating rule post-actions");
			for(TestObject t:scheme.getPostActions()){
				try{
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 starts*/

				/*pst = conn.prepareStatement("INSERT INTO TJN_EXEC_SCHEME_CONFIG VALUES (?,?,?,?,?,?)");*/
				pst = conn.prepareStatement("INSERT INTO TJN_EXEC_SCHEME_CONFIG (SCH_ID,ACT_BLOCK,PAGE_AREA,FIELD,ACTION,SEQ_NO) VALUES (?,?,?,?,?,?)");
				/*Changed by  Parveen for changing insert query REQ #TJN_243_04 ends*/

				pst.setInt(1, scheme.getId());
				pst.setString(2, "POST");
				pst.setString(3, t.getLocation());
				pst.setString(4, t.getLabel());
				pst.setString(5, t.getAction());
				pst.setInt(6, t.getSequence());
				pst.execute();
				pst.close();
				}catch(Exception e){
					logger.error("ERROR occurred while updating scheme details",e);
					try{
						conn.rollback();
					}catch(Exception e1){

					}
					try{
						conn.close();
					}catch(Exception e1){
						logger.error("ERROR while closing connection after failed update",e);
					}
					throw new DatabaseException("Could not update rule due to an internal error");
				}finally{
					
					DatabaseHelper.close(pst);
				}
			}
			logger.debug("Done");
			
		}catch(Exception e){
			logger.error("ERROR occurred while updating scheme details",e);
			try{
				conn.rollback();
			}catch(Exception e1){

			}
			try{
				conn.close();
			}catch(Exception e1){
				logger.error("ERROR while closing connection after failed update",e);
			}
			throw new DatabaseException("Could not update rule due to an internal error");
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		

		/*try{
			conn.commit();
		}catch(Exception e){
			logger.error("ERROR while committing rule update",e);
			throw new DatabaseException("Could not update rule due to an internal error");
		}finally{
			
			DatabaseHelper.close(conn);
			
		}*/


	}
	
	public void deleteSchemes(ArrayList<Integer> schemeIdList) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		
		
		PreparedStatement pst = null;
		
		try{
			for(int schemeId:schemeIdList){
				try{
				logger.debug("Clearing configuration data for rule {}", schemeId);
				pst = conn.prepareStatement("DELETE FROM TJN_EXEC_SCHEME_CONFIG WHERE SCH_ID=?");
				pst.setInt(1, schemeId);
				pst.execute();
				pst.close();
				logger.debug("Done");
				
				logger.debug("Clearing rule");
				pst = conn.prepareStatement("DELETE FROM TJN_EXEC_SCHEME where SCH_ID=?");
				pst.setInt(1, schemeId);
				pst.execute();
				logger.debug("Done");
				pst.close();
			}catch(Exception e){
				logger.error("ERROR clearing rules",e);
				try{
					logger.debug("Rolling back previous deletions");
					conn.rollback();
					logger.debug("Done");
				}catch(Exception e1){
					logger.warn("ERROR while rolling back after failed deletions",e1);
				}
				throw new DatabaseException("Could not remove rules due to an internal error");
			}finally{
				DatabaseHelper.close(pst);
			}
			}
			
			/*logger.debug("Committing deletions");
			conn.commit();*/
			logger.debug("Done");
		}catch(Exception e){
			logger.error("ERROR clearing rules",e);
			try{
				logger.debug("Rolling back previous deletions");
				conn.rollback();
				logger.debug("Done");
			}catch(Exception e1){
				logger.warn("ERROR while rolling back after failed deletions",e1);
			}
			throw new DatabaseException("Could not remove rules due to an internal error");
		}finally{
			
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	
	public void deleteScheme(int schemeId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		PreparedStatement pst = null;
		
		try{
			
			
			logger.debug("Clearing configuration data for rule {}", schemeId);
			pst = conn.prepareStatement("DELETE FROM TJN_EXEC_SCHEME_CONFIG WHERE SCH_ID=?");
			pst.setInt(1, schemeId);
			pst.execute();
			pst.close();
			logger.debug("Done");
			
			logger.debug("Clearing rule");
			pst = conn.prepareStatement("DELETE FROM TJN_EXEC_SCHEME where SCH_ID=?");
			pst.setInt(1, schemeId);
			pst.execute();
			
			/*logger.debug("Committing deletion");
			conn.commit();*/
		}catch(Exception e){
			logger.error("Could not remove rule due to an internal error",e);
			try{
				conn.rollback();
			}catch(Exception e1){
				logger.warn("Could not rollback after failed deletion",e1);
			}
			throw new DatabaseException("Could not remove rule due to an internal error");
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	
	public void disableScheme(int schemeId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		PreparedStatement pst = null;
		
		try{
			pst = conn.prepareStatement("UPDATE TJN_EXEC_SCHEME SET SCH_ENABLED=? WHERE SCH_ID=?");
			pst.setString(1, "N");
			pst.setInt(2, schemeId);
			pst.execute();
			logger.debug("Rule disable procedure complete");
		}catch(Exception e){
			logger.error("ERROR disabling scheme",e);
			throw new DatabaseException("Could not disable this rule due to an internal error");
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
	
	public void enableScheme(int schemeId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		PreparedStatement pst = null;
		
		try{
			pst = conn.prepareStatement("UPDATE TJN_EXEC_SCHEME SET SCH_ENABLED=? WHERE SCH_ID=?");
			pst.setString(1, "Y");
			pst.setInt(2, schemeId);
			pst.execute();
			logger.debug("Rule enable procedure complete");
		}catch(Exception e){
			logger.error("ERROR enabling scheme",e);
			throw new DatabaseException("Could not enable this rule due to an internal error");
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
}
