/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestStepHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 05-03-2018           Padmavathi              Newly added for TENJINCG-612
 * 19-04-2018           Padmavathi              Sequence number added to Test case Object
 * 20-04-2018           Padmavathi              Changed query for fetching test case order by execution sequence number
 * 09-05-2018           Padmavathi              TENJINCG-645
 * 04-06-2018           Padmavathi              TENJINCG-620
 * 07-06-2018           Padmavathi              changed method signature.
 * 27-09-2018           Padmavathi              TENJINCG-741
 * 08-10-2018			Pushpalatha				TENJINCG-844
 * 24-10-2018           Padmavathi              TENJINCG-847
 * 24-10-2018			Preeti					TENJINCG-850
 * 24-10-2018           Padmavathi              TENJINCG-849
 * 25-10-2018			Preeti					TENJINCG-850
 * 02-11-2018			Pushpa					TENJINCG-897
 * 07-12-2018			Pushpa					TENJINCG-910
 * 20-12-2018           Padmavathi              TENJINCG-911
 * 04-01-2019           Padmavathi              TJN252-67
 * 22-01-2019			Ashiki					TJN252-45
 * 25-01-2019           Padmavathi              for TPT-17
 * 01-02-2019           Padmavathi              TJN252-77
 * 13-02-2019			Preeti					TENJINCG-970
 * 19-02-2019			Preeti					TENJINCG-969
 * 11-03-2019           Padmavathi              TENJINCG-997
 * 19-09-2019			Preeti					TENJINCG-1068,1069
 * 13-01-2020			Prem					scripting fix
 * 05-06-2020			Ashiki					Tenj210-108
* 15-06-2021			Ashiki					TENJINCG-1275
 */

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.DependencyRules;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class TestStepHelper {
	private static final Logger logger = LoggerFactory.getLogger(TestStepHelper.class);
	private String STORAGE="Local";
	
	/*Modified by Padmavathi for TENJINCG-997 Starts*/
	/*Modified by Preeti for TENJINCG-850 starts*/
	/*public int persistTestStep(TestStep t)throws DatabaseException{*/
	public TestStep persistTestStep(TestStep t)throws DatabaseException{
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);){
			/*Modified by padmavathi for TPT-17 starts*/
			t=this.persistTestStep(conn, t);
		/*Modified by padmavathi for TPT-17 ends*/
		}catch(Exception e){
			logger.warn("Could not persist teststep", e);
		}
		return t;
	}
	/*Modified by Preeti for TENJINCG-850 ends*/
	/*Modified by Padmavathi for TENJINCG-997 ends*/
	
	public String updateTestStep(TestStep step) throws DatabaseException{

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		String returnval="";
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst=null;
		try{
			boolean isExecuting=this.testStepConcurrencyCheck(conn, step.getRecordId());
			if(!isExecuting){
				logger.info("Updating TestStep {} ",step.getId());
				/*	Modified by Padmavathi for TENJINCG-911 starts*/
				/*String query="UPDATE TESTSTEPS SET TSTEP_ID=?,TSTEP_SHT_DESC=?,TSTEP_DATA_ID=?,TSTEP_TYPE=?,TSTEP_DESC=?,TSTEP_EXP_RESULT=?,APP_ID=?,FUNC_CODE=?,TXNMODE=?,TSTEP_OPERATION=?, TSTEP_AUT_LOGIN_TYPE = ?, TSTEP_ROWS_TO_EXEC=?,TSTEP_VAL_TYPE=?,TSTEP_API_RESP_TYPE=?  WHERE TSTEP_REC_ID = ?";*/
				
				/*Added by Ashiki for TENJINCG-1275 starts*/
				String query="UPDATE TESTSTEPS SET TSTEP_ID=?,TSTEP_SHT_DESC=?,TSTEP_DATA_ID=?,TSTEP_TYPE=?,TSTEP_DESC=?,TSTEP_EXP_RESULT=?,APP_ID=?,FUNC_CODE=?,TXNMODE=?,TSTEP_OPERATION=?, TSTEP_AUT_LOGIN_TYPE = ?, TSTEP_ROWS_TO_EXEC=?,TSTEP_VAL_TYPE=?,TSTEP_API_RESP_TYPE=? ,TSTEP_RE_RUN=?,TSTEP_FILENAME=?,TSTEP_FILEPATH=? WHERE TSTEP_REC_ID = ?";	
				/*Added by Ashiki for TENJINCG-1275 ends*/
				/*	Modified by Padmavathi for TENJINCG-911 ends*/
				pst = conn.prepareStatement(query);
				pst.setString(1, step.getId());
				pst.setString(2, step.getShortDescription());
				pst.setString(3, step.getDataId());
				pst.setString(4, step.getType());
				pst.setString(5, step.getDescription());
				pst.setString(6, step.getExpectedResult());
				pst.setInt(7, step.getAppId());
				pst.setString(8, step.getModuleCode());
				pst.setString(9, step.getTxnMode());
				pst.setString(10, step.getOperation());
				pst.setString(11, step.getAutLoginType());
				if(Utilities.trim(step.getValidationType()).equalsIgnoreCase("ui")) {
					pst.setInt(12, 1);
				}else {
					pst.setInt(12, step.getRowsToExecute());
				}
				pst.setString(13, step.getValidationType());
				pst.setInt(14, step.getResponseType());
				/*	Added by Padmavathi for TENJINCG-911 starts*/
				pst.setString(15,step.getRerunStep());
				/*	Added by Padmavathi for TENJINCG-911 ends*/	
				/*Added by Ashiki for TENJINCG-1275 starts*/
				pst.setString(16, step.getFileName());
				pst.setString(17, step.getFilePath());
				/*Added by Ashiki for TENJINCG-1275 ends*/
				pst.setInt(18, step.getRecordId());
				pst.execute();
			}else{
				logger.info("Could not update test step,test step is executing");
				returnval = "Executing";
			}
		}catch(Exception e){
			logger.error("Could not update test step");
			throw new DatabaseException("Could not update test step",e);
		}finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return returnval;
	
	}
	
	/*Added by Preeti for TENJINCG-970 starts*/
	public String updateTestStep(Connection conn, TestStep step) throws DatabaseException{
		boolean abortUpdate = false;
		try{
			abortUpdate=this.testStepConcurrencyCheck(conn, step.getRecordId());
		}catch(Exception e){
			logger.error("Could not update test step");
			throw new DatabaseException("Could not update test step",e);
		}
		if(abortUpdate){
			DatabaseHelper.close(conn);
			return "Executing";
		}
		logger.info("Updating TestStep {} ",step.getId());
		try(
			PreparedStatement pst = conn.prepareStatement("UPDATE TESTSTEPS SET TSTEP_ID=?,TSTEP_SHT_DESC=?,TSTEP_DATA_ID=?,TSTEP_TYPE=?,TSTEP_DESC=?,TSTEP_EXP_RESULT=?,APP_ID=?,FUNC_CODE=?,TXNMODE=?,TSTEP_OPERATION=?, TSTEP_AUT_LOGIN_TYPE = ?, TSTEP_ROWS_TO_EXEC=?,TSTEP_VAL_TYPE=?,TSTEP_API_RESP_TYPE=? ,TSTEP_RE_RUN=? WHERE TSTEP_REC_ID = ?");
			){
				pst.setString(1, step.getId());
				pst.setString(2, step.getShortDescription());
				pst.setString(3, step.getDataId());
				pst.setString(4, step.getType());
				pst.setString(5, step.getDescription());
				pst.setString(6, step.getExpectedResult());
				pst.setInt(7, step.getAppId());
				pst.setString(8, step.getModuleCode());
				pst.setString(9, step.getTxnMode());
				pst.setString(10, step.getOperation());
				pst.setString(11, step.getAutLoginType());
				if(Utilities.trim(step.getValidationType()).equalsIgnoreCase("ui")) {
					pst.setInt(12, 1);
				}else {
					pst.setInt(12, step.getRowsToExecute());
				}
				pst.setString(13, step.getValidationType());
				pst.setInt(14, step.getResponseType());
				pst.setString(15,step.getRerunStep());
				pst.setInt(16, step.getRecordId());
				pst.execute();
			}catch(Exception e){
				throw new DatabaseException("Could not update test step due to an internal error",e);
			} 
			return "Not Executing";
	}
	/*Added by Preeti for TENJINCG-970 ends*/
	
	/*Added by Ashiki for TJN252-45 starts*/
	public void deleteTestStep(int tcRecId,Connection conn) throws DatabaseException {
			
		if(conn == null){
		     throw new DatabaseException("Invalid or no database connection");
		}
	    try(PreparedStatement pst = conn.prepareStatement("DELETE FROM TESTSTEPS WHERE TC_REC_ID = ?");){
		logger.info("deleting DependencyRules at testcase level for " +tcRecId);
		     			
		pst.setInt(1, tcRecId);
		pst.execute();
		}catch(Exception e){
		logger.error("Could not delete Dependency Rules",e);
		throw new DatabaseException("Could not delete Dependency Rules for "+tcRecId,e);
		}
			
		
	}
	/*Added by Ashiki for TJN252-45 ends*/
	public boolean deleteTestStep(int tStepRecId, int tcRecId)throws DatabaseException{

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		/*Modified by Padmavathi for TJN252-77 Starts*/
		PreparedStatement pst1=null,  pst2=null;
		ResultSet rs = null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		boolean deletion_status=true;
		try{
			
			/*modified by shruthi for VAPT Helper fix starts*/
			pst1 = conn.prepareStatement("SELECT * FROM MASTESTRUNS WHERE RUN_ID IN (SELECT DISTINCT RUN_ID FROM RUNRESULT_TS_TXN WHERE TSTEP_REC_ID=?)");
			pst1.setInt(1,tStepRecId);
			/*modified by shruthi for VAPT Helper fix ends*/
			rs = pst1.executeQuery();
			
			while(rs.next()){
				logger.info("test step executing ");
				deletion_status=false;
				return deletion_status;
			}
			
				pst2 = conn.prepareStatement("DELETE FROM TESTSTEPS WHERE TSTEP_REC_ID = ?");
				pst2.setInt(1, tStepRecId);


			
				pst2.execute();

				this.updateTestStepSeq(tcRecId);
		}catch(Exception e){
			throw new DatabaseException("Could not remove Test Step",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst2);		
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(conn);
		}
		return deletion_status;
		/*Modified by Padmavathi for TJN252-77 ends*/
	} 
	public void reOrderTestCases(int testCaseRecordId,Map<Integer,Integer> sequenceMap) throws DatabaseException
	{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		try {
			
			for(Integer stepRecordId : sequenceMap.keySet()) {
				try{
				int sequenceValue = sequenceMap.get(stepRecordId);
				pst = conn.prepareStatement("UPDATE TESTSTEPS SET TSTEP_EXEC_SEQ=? WHERE TSTEP_REC_ID=?");
				pst.setInt(1, sequenceValue);
				pst.setInt(2, stepRecordId);
				pst.executeUpdate();
				pst.close();
				}finally{
					DatabaseHelper.close(pst);	
				}

			}
			//conn.commit();
		} catch (SQLException e) {
			
			logger.error("ERROR - Could not update sequence", e);
			try{
				conn.rollback();
			}catch(Exception e1) {
				logger.warn("Could not roll-back", e1);
			}
			throw new DatabaseException("Could not update sequence due to an internal error. Please contact Tenjin Support.");
		} finally {

			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	public void updateTestStepSeq(int testCaseRecordId) throws DatabaseException
	{
		int i=1;
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst6=null,pst7=null;
		ResultSet r=null;
		try{
			pst6 = conn.prepareStatement("SELECT TSTEP_REC_ID FROM TESTSTEPS WHERE TC_REC_ID=? ORDER BY TSTEP_EXEC_SEQ ASC");
			pst6.setInt(1, testCaseRecordId);
			r = pst6.executeQuery();
			while(r.next()) {
				int stepId=r.getInt("TSTEP_REC_ID");
				/*modified by shruthi for VAPT Helper fix starts*/
				pst7=conn.prepareStatement("UPDATE TESTSTEPS SET TSTEP_EXEC_SEQ=? WHERE TSTEP_REC_ID=?");
				pst7.setInt(1, i);
				pst7.setInt(2, stepId);
				/*modified by shruthi for VAPT Helper fix ends*/
				pst7.executeUpdate();
				i++;
			}
		}catch(Exception e){
			logger.warn("Could not update teststep sequence", e);
		}finally{
			DatabaseHelper.close(r);
			DatabaseHelper.close(pst6);
			DatabaseHelper.close(pst7);
			DatabaseHelper.close(conn);	
		}
	}
	
	/*Modified by Preeti for TENJINCG-850 starts*/
	/*Added by Padmavathi for TENJINCG-645 starts*/
	public int hydrateTestStepRecId(int sequence,int tcRecId) throws DatabaseException{
		int stepRecId=0;
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);){
			stepRecId=this.hydrateTestStepRecId(conn, sequence, tcRecId);
		}catch(Exception e){
			logger.warn("Could not get teststep sequence", e);
		}
		return stepRecId;
		
	}
	/*Added by Padmavathi for TENJINCG-645 ends*/
	
	public int hydrateTestStepRecId(Connection conn, int sequence,int tcRecId) throws DatabaseException{
		int stepRecId=0;
		try(
				PreparedStatement pst  = conn.prepareStatement("SELECT TSTEP_REC_ID FROM TESTSTEPS WHERE TSTEP_EXEC_SEQ=? AND TC_REC_ID=? ");
			){
			pst.setInt(1, sequence);
			pst.setInt(2, tcRecId);
			try(ResultSet r = pst.executeQuery();){
			while(r.next()) {
				stepRecId=r.getInt("TSTEP_REC_ID");
			}
		}
		}catch(Exception e){
			logger.warn("Could not get teststep sequence", e);
		}
		return stepRecId;
		
	}
	/*Modified by Preeti for TENJINCG-850 ends*/
	
	public boolean testStepConcurrencyCheck(Connection conn, int testStepRecordId) throws DatabaseException{
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		boolean isTestStepExecuting = false;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			logger.info("Checking test step execution ");
			/*modified by shruthi for VAPT Helper fix starts*/
			pst = conn.prepareStatement("SELECT * FROM MASTESTRUNS WHERE RUN_ID IN (SELECT DISTINCT RUN_ID FROM RUNRESULT_TS_TXN WHERE TSTEP_REC_ID=?) AND RUN_STATUS='Executing'");
			pst.setInt(1,testStepRecordId );
			/*modified by shruthi for VAPT Helper fix ends*/
			rs = pst.executeQuery();
			while(rs.next()){
				logger.info("test step executing ");
				isTestStepExecuting = true;
				break;
			}
		}catch(Exception e){
			logger.error("ERROR performing Test Step Concurrency Check",e);
			throw new DatabaseException("Could not check test step concurrency due to an internal error.");
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
	}
		logger.info("Test Step Concurrency Check --> {}", isTestStepExecuting);
		return isTestStepExecuting;
	}
	/*Modified by Padmavathi for TENJINCG-741 starts*/
	public TestStep hydrateTestStep(int tcRecId,String stepId) throws DatabaseException{
		
		TestStep step=null;
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ)){
			if(conn == null){
				throw new DatabaseException("Invalid or no database connection");
			}
			step=this.hydrateTestStep(conn, tcRecId, stepId);
		}catch(Exception e){
			logger.error("Could not hydrate Test Step information",e);
			throw new DatabaseException("Could not hydrate Test Step information",e);
		}
		return step;
	}
	
	public TestStep hydrateTestStep(Connection conn,int tcRecId,String stepId) throws DatabaseException{
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		logger.info("hydrating teststep" +stepId);
		TestStep t = null;
		try(PreparedStatement pst=conn.prepareStatement("SELECT A.*, C.APP_NAME FROM TESTSTEPS A , MASAPPLICATION C WHERE C.APP_ID = A.APP_ID AND  LOWER(A.TSTEP_ID) = ? AND A.TC_REC_ID = ? ")){
			pst.setString(1, stepId.toLowerCase());
			pst.setInt(2, tcRecId);
			try(ResultSet rs =pst.executeQuery()){
				while(rs.next()){
					t=this.buildTestStepObject(rs);
					t.setAppName(Utilities.escapeXml(rs.getString("APP_NAME")));
				}
			}
		}catch(Exception e){
			logger.error("Could not hydrate Test Step information",e);
			throw new DatabaseException("Could not hydrate Test Step information",e);
		}
		return t;
	}
	/*Modified by Padmavathi for TENJINCG-741 ends*/
	
	/*Added by Ashiki for TJN252-45 starts*/
	public boolean hydrateStepsForTestCaseDelete(int[] testCaseRecordId, Connection conn) throws DatabaseException, SQLException{
		boolean deleteStatus=true;
		for(int tcRecId:testCaseRecordId){
			try(PreparedStatement pst=conn.prepareStatement("SELECT * FROM V_TSTEP_EXEC_HISTORY_2 WHERE TC_REC_ID = ? "))
				{
				pst.setInt(1,tcRecId);
				try(ResultSet rs=pst.executeQuery();){
					while(rs.next()){
						deleteStatus=false;
						break;
					}
				}
			}
		}
		return deleteStatus;
	}
	/*Added by Ashiki for TJN252-45 ends*/
	/*Added by Padmavathi for TJN252-67 Starts*/
public TestStep hydrateTestStepWithDependencyRules(Connection conn,int tcRecId,String stepId) throws DatabaseException{
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		logger.info("hydrating teststep" +stepId);
		TestStep t = null;
		try(PreparedStatement pst=conn.prepareStatement("SELECT A.*, C.APP_NAME ,B.EXEC_STATUS FROM MASAPPLICATION C,TESTSTEPS A LEFT JOIN TC_TSTEP_DEPENDENCY_RULES B ON B.TSTEP_REC_ID=A.TSTEP_REC_ID  WHERE C.APP_ID = A.APP_ID AND  LOWER(A.TSTEP_ID) = ? AND A.TC_REC_ID = ? ")){
			pst.setString(1, stepId.toLowerCase());
			pst.setInt(2, tcRecId);
			try(ResultSet rs =pst.executeQuery()){
				while(rs.next()){
					t=this.buildTestStepObject(rs);
					t.setCustomRules(rs.getString("EXEC_STATUS"));
					t.setAppName(rs.getString("APP_NAME"));
				}
			}
		}catch(Exception e){
			logger.error("Could not hydrate Test Step information",e);
			throw new DatabaseException("Could not hydrate Test Step information",e);
		}
		return t;
	}
/*Added by Padmavathi for TJN252-67 ends*/
	public TestStep hydrateTestStep(int tcRecId,int tstepRecId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		TestStep t = null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try{
			logger.info("hydrating teststep" +tstepRecId);
			pst = conn.prepareStatement("SELECT A.*, B.TC_ID, B.TC_NAME, C.APP_NAME FROM TESTSTEPS A, TESTCASES B, MASAPPLICATION C WHERE C.APP_ID = A.APP_ID AND A.TSTEP_REC_ID = ? AND B.TC_REC_ID = ?");
			pst.setInt(1, tstepRecId);
			pst.setInt(2, tcRecId);
			rs = pst.executeQuery();
			while(rs.next()){
				t=this.buildTestStepObject(rs);
				t.setParentTestCaseId(Utilities.escapeXml(rs.getString("TC_ID")));
				t.setTestCaseName(Utilities.escapeXml(rs.getString("TC_NAME")));
				t.setAppName(Utilities.escapeXml(rs.getString("APP_NAME")));
			}
		}catch(Exception e){
			logger.error("Could not hydrate Test Step information",e);
			throw new DatabaseException("Could not hydrate Test Step information",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return t;
	
	
	}
	public TestStep hydrateTestStep(int tStepRecId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		TestStep t = null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try{
			logger.info("hydrating teststep" +tStepRecId);
			/*Modified by Padmavathi for TENJINCG-997 Starts*/
			/*Modified by Preeti for TENJINCG-969 starts*/
			pst = conn.prepareStatement("SELECT A.*, B.TC_NAME,B.TC_ID, C.APP_NAME, D.* ,E.EXEC_STATUS FROM TESTSTEPS A LEFT JOIN (SELECT * FROM TJN_AUDIT WHERE AUDIT_ID=(SELECT MAX(AUDIT_ID) FROM TJN_AUDIT WHERE ENTITY_TYPE=? AND ENTITY_RECORD_ID = ?)) D  ON D.ENTITY_RECORD_ID = A.TSTEP_REC_ID"
					+ " LEFT JOIN TC_TSTEP_DEPENDENCY_RULES E ON E.TSTEP_REC_ID=A.TSTEP_REC_ID , "
					+ " TESTCASES B,MASAPPLICATION C WHERE C.APP_ID = A.APP_ID AND B.TC_REC_ID = A.TC_REC_ID AND A.TSTEP_REC_ID = ?");
			/*Modified by Padmavathi for TENJINCG-997 ends*/
			pst.setString(1, "teststep");
			pst.setInt(2, tStepRecId);
			pst.setInt(3, tStepRecId);
			rs = pst.executeQuery();
			while(rs.next()){
				t=this.buildTestStepObjectWithAudit(rs);
				/*Modified by Preeti for TENJINCG-969 ends*/
				t.setParentTestCaseId(Utilities.escapeXml(rs.getString("TC_ID")));
				t.setTestCaseName(Utilities.escapeXml(rs.getString("TC_NAME")));
				t.setAppName(Utilities.escapeXml(rs.getString("APP_NAME")));
				/*Added by Padmavathi for TENJINCG-997 Starts*/
				t.setCustomRules(rs.getString("EXEC_STATUS"));
				/*Added by Padmavathi for TENJINCG-997 ends*/
			}
		}catch(Exception e){
			logger.error("Could not hydrate Test Step information",e);
			throw new DatabaseException("Could not hydrate Test Step information",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return t;
	
	
	}
	
	/*Modified by Padmavathi for TENJINCG-997 starts*/
	/*public ArrayList<TestStep> hydrateStepsForTestCase(Connection conn, Connection appConn, int testCaseRecordId ) throws DatabaseException{*/
	public ArrayList<TestStep> hydrateStepsForTestCase(Connection conn, Connection appConn, int testCaseRecordId,boolean withDependencyRules ) throws DatabaseException{
		/*Modified by Padmavathi for TENJINCG-997 ends*/
		PreparedStatement pst = null;
		ResultSet rs = null;

		ArrayList<TestStep> steps =null;

		try{
			pst = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID = ? ORDER BY TSTEP_EXEC_SEQ");
			pst.setInt(1, testCaseRecordId);
			rs = pst.executeQuery();

			steps = new ArrayList<TestStep>();

			Map<Integer, String> autMap = new HashMap<Integer, String>();

			while(rs.next()){

				TestStep t = new TestStep();
				t.setRecordId(rs.getInt("TSTEP_REC_ID"));
				t.setId(rs.getString("TSTEP_ID"));
				t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
				String appname= autMap.get(t.getAppId());
				if(appname != null && !appname.equalsIgnoreCase("")){
					t.setAppName(appname);
				}else{
					appname = getappname(appConn, rs.getInt("APP_ID"));
					t.setAppName(appname);
				}
				t.setModuleCode(rs.getString("FUNC_CODE"));
				t.setType(rs.getString("TSTEP_TYPE"));
				t.setDataId(rs.getString("TSTEP_DATA_ID"));
				t.setDescription(rs.getString("TSTEP_DESC"));
				t.setExpectedResult(rs.getString("TSTEP_EXP_RESULT"));
				t.setOperation(rs.getString("TSTEP_OPERATION"));
				t.setAutLoginType(rs.getString("TSTEP_AUT_LOGIN_TYPE"));
				t.setRowsToExecute(rs.getInt("TSTEP_ROWS_TO_EXEC"));
				t.setTstepStorage(rs.getString("TSTEP_STORAGE"));
				t.setSequence(rs.getInt("TSTEP_EXEC_SEQ"));
				t.setAppId(rs.getInt("APP_ID"));
				/*Added by Preeti for TENJINCG-850 starts*/
				t.setValidationType(rs.getString("TSTEP_VAL_TYPE"));
				/*Added by Preeti for TENJINCG-850 ends*/
				String mode=rs.getString("TXNMODE");
				if(mode.equalsIgnoreCase("GUI"))
				{
					mode="GUI";
				}/*Added by Ashiki for TENJINCG-1275 starts*/
				if(mode.equalsIgnoreCase("MSG"))
				{
					mode="GUI";
					t.setActualResult(rs.getString("TSTEP_ACT_RESULT"));
				}
				/*Added by Ashiki for TENJINCG-1275 ends*/
				else
				{
					mode="API";
					t.setResponseType((rs.getInt("TSTEP_API_RESP_TYPE")));
				}
				t.setTxnMode(mode);
				/*Modified by Padmavathi for TENJINCG-997 starts*/
				if(withDependencyRules && t.getSequence()!=1){
					/*Added by Padmavathi for TENJINCG-645 starts*/
					DependencyRules rules=new DependencyRulesHelper().hydrateDependencyRules(t.getRecordId());
					if(rules!=null)
						t.setCustomRules(rules.getExecStatus());
						/*Added by Padmavathi for TENJINCG-645 ends*/
				}
				/*Modified by Padmavathi for TENJINCG-997 ends*/
				steps.add(t);
			}
		}catch(Exception e){
			logger.error("ERROR fetching steps under test case",e);
			throw new DatabaseException("Could not fetch steps under this test case due to an internal error",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return steps;
	}
	
	/*Added by Preeti for TENJINCG-850 starts*/
	public ArrayList<TestStep> hydrateStepsForTestCase(Connection conn, int testCaseRecordId) throws DatabaseException{
		
		ArrayList<TestStep> steps =null;

		try(
				PreparedStatement pst = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID = ? ORDER BY TSTEP_EXEC_SEQ");	
			){
			pst.setInt(1, testCaseRecordId);
			try(ResultSet rs = pst.executeQuery();){

			steps = new ArrayList<TestStep>();

			Map<Integer, String> autMap = new HashMap<Integer, String>();

			while(rs.next()){

				TestStep t = new TestStep();
				t.setRecordId(rs.getInt("TSTEP_REC_ID"));
				t.setId(rs.getString("TSTEP_ID"));
				t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
				String appname= autMap.get(t.getAppId());
				if(appname != null && !appname.equalsIgnoreCase("")){
					t.setAppName(appname);
				}else{
					appname = getappname(conn, rs.getInt("APP_ID"));
					t.setAppName(appname);
				}
				t.setModuleCode(rs.getString("FUNC_CODE"));
				t.setType(rs.getString("TSTEP_TYPE"));
				t.setDataId(rs.getString("TSTEP_DATA_ID"));
				t.setDescription(rs.getString("TSTEP_DESC"));
				t.setExpectedResult(rs.getString("TSTEP_EXP_RESULT"));
				t.setOperation(rs.getString("TSTEP_OPERATION"));
				t.setAutLoginType(rs.getString("TSTEP_AUT_LOGIN_TYPE"));
				t.setRowsToExecute(rs.getInt("TSTEP_ROWS_TO_EXEC"));
				t.setTstepStorage(rs.getString("TSTEP_STORAGE"));
				t.setSequence(rs.getInt("TSTEP_EXEC_SEQ"));
				t.setAppId(rs.getInt("APP_ID"));
				t.setValidationType(rs.getString("TSTEP_VAL_TYPE"));
				/*	Added by Padmavathi for TENJINCG-911 starts*/ 
				t.setRerunStep(rs.getString("TSTEP_RE_RUN"));
				/*	Added by Padmavathi for TENJINCG-911 ends*/ 
				
				/*Added by Ashiki for TENJINCG-1275 starts*/
				t.setFileName(rs.getString("TSTEP_FILENAME"));
				/*Added by Ashiki for TENJINCG-1275 starts*/
				String mode=rs.getString("TXNMODE");
				if(mode.equalsIgnoreCase("GUI"))
				{
					mode="GUI";
				}
				else
				{
					mode="API";
					t.setResponseType((rs.getInt("TSTEP_API_RESP_TYPE")));
				}
				t.setTxnMode(mode);
				DependencyRules rules=new DependencyRulesHelper().hydrateDependencyRules(conn, t.getRecordId());
				if(rules!=null)
					t.setCustomRules(rules.getExecStatus());
				steps.add(t);
			}
		}
		}catch(Exception e){
			logger.error("ERROR fetching steps under test case",e);
			throw new DatabaseException("Could not fetch steps under this test case due to an internal error",e);
		}
		return steps;
	}
	
	public HashMap<Integer, String> getAppIdForSteps(Connection conn, String[] selectedTestCase) throws DatabaseException{
		HashMap<Integer, String> listApp = new HashMap<>();
		try(
				PreparedStatement pst = conn.prepareStatement("SELECT APP_ID FROM TESTSTEPS WHERE TC_REC_ID = ?");	
			){
			for(String testCaseRecordId:selectedTestCase){
				pst.setInt(1, Integer.parseInt(testCaseRecordId));
				try(ResultSet rs = pst.executeQuery();){
					while(rs.next()){
						TestStep t = new TestStep();
						t.setAppId(rs.getInt("APP_ID"));
						t.setAppName(TestStepHelper.getappname(conn, t.getAppId()));
						listApp.put(t.getAppId(),t.getAppName());
					}
				}
			}
		}
		catch(Exception e){
			logger.error("ERROR fetching steps under test case",e);
			throw new DatabaseException("Could not fetch steps under this test case due to an internal error",e);
		}
		
		return listApp;
	}
	/*Modified by padmavathi for TPT-17 starts*/
	/*public int persistTestStep(Connection conn, TestStep t)throws DatabaseException{*/
	public TestStep persistTestStep(Connection conn, TestStep t)throws DatabaseException{
		/*Modified by padmavathi for TPT-17 ends*/
		/*Modified by Preeti for TENJINCG-969 starts*/
				t.setRecordId(DatabaseHelper.getGlobalSequenceNumber(conn));
				/*Modofied by Preeti for TENJINCG-969 ends*/
				if(t.getTstepStorage()==null){
					t.setTstepStorage(STORAGE);
				}
				int i=0;
				try(
						PreparedStatement pst1 = conn.prepareStatement("SELECT coalesce(MAX(TSTEP_EXEC_SEQ),0) AS TSTEP_EXEC_SEQ FROM TESTSTEPS WHERE TC_REC_ID=?");
					){
					pst1.setInt(1, t.getTestCaseRecordId());
					try(ResultSet r = pst1.executeQuery();){
					if(r.next()) {
						i = r.getInt("TSTEP_EXEC_SEQ");
					}
					i++;
					t.setSequence(i);
				}
				}catch(Exception e){
					logger.error("ERROR getting latest sequence number of test step from TESTSTEPS table", e);
				}
				try(
						/*	Modified by Padmavathi for TENJINCG-911 starts*/ 
						/*PreparedStatement pst2 = conn.prepareStatement("INSERT INTO TESTSTEPS (TSTEP_REC_ID,TC_REC_ID,TSTEP_SHT_DESC,TSTEP_DATA_ID,TSTEP_TYPE,TSTEP_DESC,TSTEP_EXP_RESULT,TSTEP_ACT_RESULT,APP_ID,FUNC_CODE,TXN_CONTEXT,EXEC_CONTEXT,TXNMODE,TSTEP_ID,TSTEP_OPERATION,TSTEP_AUT_LOGIN_TYPE,TSTEP_ROWS_TO_EXEC,TSTEP_STORAGE,TSTEP_EXEC_SEQ,TSTEP_VAL_TYPE,TSTEP_API_RESP_TYPE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");*/
					
						/*Added by Ashiki for TENJINCG-1275 starts*/
						PreparedStatement pst2 = conn.prepareStatement("INSERT INTO TESTSTEPS (TSTEP_REC_ID,TC_REC_ID,TSTEP_SHT_DESC,TSTEP_DATA_ID,TSTEP_TYPE,TSTEP_DESC,TSTEP_EXP_RESULT,TSTEP_ACT_RESULT,APP_ID,FUNC_CODE,TXN_CONTEXT,EXEC_CONTEXT,TXNMODE,TSTEP_ID,TSTEP_OPERATION,TSTEP_AUT_LOGIN_TYPE,TSTEP_ROWS_TO_EXEC,TSTEP_STORAGE,TSTEP_EXEC_SEQ,TSTEP_VAL_TYPE,TSTEP_API_RESP_TYPE,TSTEP_RE_RUN,TSTEP_CREATED_BY,TSTEP_CREATED_ON,TSTEP_FILENAME,TSTEP_FILEPATH) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
						/*Added by Ashiki for TENJINCG-1275 ends*/
						/*	Modified by Padmavathi for TENJINCG-911 ends*/ 
					){
				pst2.setInt(1,t.getRecordId());
				pst2.setInt(2, t.getTestCaseRecordId());
				pst2.setString(3, t.getShortDescription());
				pst2.setString(4, t.getDataId());
				pst2.setString(5, t.getType());
				pst2.setString(6, t.getDescription());
				pst2.setString(7, t.getExpectedResult());
				/*Added by Ashiki for TENJINCG-1275 starts*/
				if(t.getTxnMode()!=null && t.getTxnMode().equalsIgnoreCase("MSG")){
					pst2.setString(8, t.getActualResult());
				}
				/*Added by Ashiki for TENJINCG-1275 ends*/
				else{
				pst2.setString(8, "");
				}
				pst2.setInt(9, t.getAppId());
				pst2.setString(10, t.getModuleCode());
				pst2.setString(11, "New");
				if(t.getOperation() != null && !t.getOperation().equalsIgnoreCase("verify")){
					pst2.setString(12, "Submit");
				}else{
					pst2.setString(12, t.getOperation());
				}
				pst2.setString(13, t.getTxnMode().toUpperCase());
				pst2.setString(14, t.getId());
				pst2.setString(15, t.getOperation());
				pst2.setString(16, t.getAutLoginType());
				if(Utilities.trim(t.getValidationType()).equalsIgnoreCase("ui")) {
					pst2.setInt(17, 1);
				}else {
					if(t.getRowsToExecute() == 0){
						pst2.setInt(17, 1);
					}else{
						pst2.setInt(17, t.getRowsToExecute());
					}
				}
				pst2.setString(18, t.getTstepStorage());
				pst2.setInt(19, t.getSequence());
				pst2.setString(20, t.getValidationType());
				pst2.setInt(21, t.getResponseType());
				/*	Added by Padmavathi for TENJINCG-911 starts*/
				pst2.setString(22,t.getRerunStep());
				/*	Added by Padmavathi for TENJINCG-911 ends*/
				/*Added by Preeti for TENJINCG-969 starts*/
				pst2.setString(23, t.getCreatedBy());
				pst2.setTimestamp(24, new Timestamp(new Date().getTime()));
				/*Added by Preeti for TENJINCG-969 ends*/
				/*Added by Ashiki for TENJINCG-1275 starts*/
				pst2.setString(25, t.getFileName());
				pst2.setString(26, t.getFilePath());
				/*Added by Ashiki for TENJINCG-1275 ends*/
				pst2.execute();
			
		}catch(Exception e){
			logger.error("Could not insert Test step record",e);
			throw new DatabaseException("Could not insert record",e);
		}
		return t;
		
	}
	/*Added by Preeti for TENJINCG-850 ends*/
	/*Added by Padmavathi for TENJINCG-645 starts*/
	public ArrayList<TestStep> hydrateTestSteps(int testCaseRecordId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		Connection appConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		ArrayList<TestStep> steps=null;
		try{
			/*Modified by Padmavathi for TENJINCG-997 starts*/
			steps=this.hydrateStepsForTestCase(conn, appConn, testCaseRecordId,false);
			/*Modified by Padmavathi for TENJINCG-997 ends*/
		}finally{
			DatabaseHelper.close(conn);
			DatabaseHelper.close(appConn);
		}
		return steps;
		
	}
	/*Added by Preeti for TENJINCG-1068,1069 starts*/
	public String getappname(int appid) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		String appname="";
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst = conn.prepareStatement("SELECT app_name FROM masapplication WHERE app_id = ?");
			pst.setInt(1, appid);
			rs = pst.executeQuery();

			if(rs.next()){
				appname = rs.getString("app_name");
			}

		}catch(Exception e){
			logger.error("Could not fetch app name for ID {}", appid, e);
			throw new DatabaseException(e.getMessage(),e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return appname;
	}
	/*Added by Preeti for TENJINCG-1068,1069 ends*/
	/*Added by Padmavathi for TENJINCG-645 ends*/
	public static String getappname(Connection conn, int appid) throws DatabaseException{
		String appname="";
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst = conn.prepareStatement("SELECT app_name FROM masapplication WHERE app_id = ?");
			pst.setInt(1, appid);
			rs = pst.executeQuery();

			if(rs.next()){
				appname = rs.getString("app_name");
			}

		}catch(Exception e){
			logger.error("Could not fetch app name for ID {}", appid, e);
			throw new DatabaseException(e.getMessage(),e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return appname;
	}
	
	private TestStep buildTestStepObject(ResultSet rs) throws SQLException{
		TestStep t = new TestStep();
		t.setRecordId(rs.getInt("TSTEP_REC_ID"));
		t.setTestCaseRecordId(rs.getInt("TC_REC_ID"));
		t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
		t.setDataId(rs.getString("TSTEP_DATA_ID"));
		t.setType(rs.getString("TSTEP_TYPE"));
		t.setDescription(rs.getString("TSTEP_DESC"));
		t.setExpectedResult(rs.getString("TSTEP_EXP_RESULT"));
		t.setAppId(rs.getInt("APP_ID"));
		t.setModuleCode(rs.getString("FUNC_CODE"));
		t.setTxnMode(rs.getString("TXNMODE"));
		t.setId(rs.getString("TSTEP_ID"));
		t.setOperation(rs.getString("TSTEP_OPERATION"));
		t.setAutLoginType(rs.getString("TSTEP_AUT_LOGIN_TYPE"));
		t.setRowsToExecute(rs.getInt("TSTEP_ROWS_TO_EXEC"));
		t.setTstepStorage(rs.getString("TSTEP_STORAGE"));
		t.setSequence(rs.getInt("TSTEP_EXEC_SEQ"));
		t.setValidationType(rs.getString("TSTEP_VAL_TYPE"));
		t.setResponseType(rs.getInt("TSTEP_API_RESP_TYPE"));
		/*	Added by Padmavathi for TENJINCG-911 starts*/ 
		t.setRerunStep(rs.getString("TSTEP_RE_RUN"));
		/*	Added by Padmavathi for TENJINCG-911 ends*/ 
		return t;
	}
	/*Added by Preeti for TENJINCG-969 starts*/
	private TestStep buildTestStepObjectWithAudit(ResultSet rs) throws SQLException{
		TestStep t = new TestStep();
		t.setRecordId(rs.getInt("TSTEP_REC_ID"));
		t.setTestCaseRecordId(rs.getInt("TC_REC_ID"));
		t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
		t.setDataId(rs.getString("TSTEP_DATA_ID"));
		t.setType(rs.getString("TSTEP_TYPE"));
		t.setDescription(rs.getString("TSTEP_DESC"));
		t.setExpectedResult(rs.getString("TSTEP_EXP_RESULT"));
		t.setAppId(rs.getInt("APP_ID"));
		t.setModuleCode(rs.getString("FUNC_CODE"));
		t.setTxnMode(rs.getString("TXNMODE"));
		t.setId(rs.getString("TSTEP_ID"));
		t.setOperation(rs.getString("TSTEP_OPERATION"));
		t.setAutLoginType(rs.getString("TSTEP_AUT_LOGIN_TYPE"));
		t.setRowsToExecute(rs.getInt("TSTEP_ROWS_TO_EXEC"));
		t.setTstepStorage(rs.getString("TSTEP_STORAGE"));
		t.setSequence(rs.getInt("TSTEP_EXEC_SEQ"));
		t.setValidationType(rs.getString("TSTEP_VAL_TYPE"));
		t.setResponseType(rs.getInt("TSTEP_API_RESP_TYPE"));
		/*Added by Ashiki for TENJINCG-1275 starts*/
		if(t.getTxnMode()!=null && t.getTxnMode().equalsIgnoreCase("MSG"))
		{
			t.setActualResult(rs.getString("TSTEP_ACT_RESULT"));
		}
		/*Added by Ashiki for TENJINCG-1275 ends*/
		/*	Added by Padmavathi for TENJINCG-911 starts*/ 
		t.setRerunStep(rs.getString("TSTEP_RE_RUN"));
		/*	Added by Padmavathi for TENJINCG-911 ends*/ 
		t.setCreatedBy(rs.getString("TSTEP_CREATED_BY"));
		t.setCreatedOn(rs.getTimestamp("TSTEP_CREATED_ON"));
		/*Added by Ashiki for TENJINCG-1275 starts*/
		t.setFileName(rs.getString("TSTEP_FILENAME"));
		t.setFilePath(rs.getString("TSTEP_FILEPATH"));
		/*Added by Ashiki for TENJINCG-1275 ends*/
		AuditRecord record = new AuditRecord();
		record.setAuditId(rs.getInt("AUDIT_ID"));
		record.setEntityRecordId(rs.getInt("ENTITY_RECORD_ID"));
		record.setEntityType(rs.getString("ENTITY_TYPE"));
		record.setLastUpdatedBy(rs.getString("LAST_MODIFIED_BY"));
		record.setLastUpdatedOn(rs.getTimestamp("LAST_MODIFIED_ON"));
		t.setAuditRecord(record);
		return t;
	}
	/*Added by Preeti for TENJINCG-969 ends*/
	/*Added by Pushpa for TENJINCG-844 starts*/
	public ArrayList<TestStep> hydrateStepsForTcTs(Connection conn, Connection appConn, int testCaseRecordId) throws DatabaseException{
		
		ArrayList<TestStep> steps =null;

		try(PreparedStatement pst = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID = ? ORDER BY TSTEP_EXEC_SEQ");){
			
			pst.setInt(1, testCaseRecordId);
			try(ResultSet rs = pst.executeQuery();){
				
				steps = new ArrayList<TestStep>();
				Map<Integer, String> autMap = new HashMap<Integer, String>();
				while(rs.next()){
					TestStep t = new TestStep();
					t.setSequence(rs.getInt("TSTEP_EXEC_SEQ"));
					t.setId(rs.getString("TSTEP_ID"));
					t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
					t.setRecordId(rs.getInt("TSTEP_REC_ID"));
					t.setType(rs.getString("TSTEP_TYPE"));
					t.setAppId(rs.getInt("APP_ID"));
					String appname= autMap.get(t.getAppId());
					if(appname != null && !appname.equalsIgnoreCase("")){
						t.setAppName(Utilities.escapeXml(appname));
					}else{
						appname = getappname(appConn, rs.getInt("APP_ID"));
						t.setAppName(Utilities.escapeXml(appname));
					}
					steps.add(t);
				}
			}
		}catch(Exception e){
			logger.error("ERROR fetching steps under test case",e);
			throw new DatabaseException("Could not fetch steps under this test case due to an internal error",e);
		}

		return steps;
	}
	/*Added by Pushpa for TENJINCG-844 ends*/
	
	/*Modified by Preeti for TENJINCG-850 starts*/
	/*Added by Padmavathi for TENJINCG-847 Starts*/
	public void copyManualFieldMap(int sourceTcRecId,String sourceStepId,TestStep step) throws DatabaseException{
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);){
			this.copyManualFieldMap(conn, sourceTcRecId, sourceStepId, step);
			}catch(SQLException e){
				logger.error("Could not persists manual field map for test step id " +  step.getId(),e);
				throw new DatabaseException("Could not persists manual field map for test step id " +  step.getId());
			}
 		
	}
	/*Added by Padmavathi for TENJINCG-847 ends*/
	public void copyManualFieldMap(Connection conn, int sourceTcRecId,String sourceStepId,TestStep step) throws DatabaseException{
		try(
			PreparedStatement pst = conn.prepareStatement("INSERT INTO MANUAL_FIELD_MAP (APP_ID,FUNCTION_NAME,TEST_STEP_ID,PAGE_AREA,MANUAL_FIELD_LABEL,MANUAL_STATUS,TEST_CASE_ID) "
							+ "SELECT APP_ID,FUNCTION_NAME , ? ,PAGE_AREA,MANUAL_FIELD_LABEL,MANUAL_STATUS ,? "
									+ "FROM MANUAL_FIELD_MAP WHERE APP_ID=? AND FUNCTION_NAME=? AND TEST_STEP_ID=? AND TEST_CASE_ID=?")){
					pst.setString(1, step.getId());
					pst.setInt(2,step.getTestCaseRecordId());
					pst.setInt(3, step.getAppId());
					pst.setString(4, step.getModuleCode());
					pst.setString(5, sourceStepId);
					pst.setInt(6, sourceTcRecId);
					pst.execute();
			}catch(SQLException e){
				logger.error("Could not persists manual field map for test step id " +  step.getId(),e);
				throw new DatabaseException("Could not persists manual field map for test step id " +  step.getId());
			}
 		
	}
	/*Modified by Pushpa for vapt fix ends */
	/*Modified by Preeti for TENJINCG-850 ends*/	
	
	/*Added by Padmavathi for TENJINCG-849 Starts*/
	public int getStepsCountForCase(int tcRecId) throws DatabaseException{
		int count=0;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement("SELECT COUNT(*) AS STEPCOUNT FROM TESTSTEPS  WHERE  TC_REC_ID = ?")){
					pst.setInt(1, tcRecId);
				try(ResultSet rs=pst.executeQuery()){
					while(rs.next()){
						count=rs.getInt("STEPCOUNT");
					}
				}
			}catch(SQLException e){
				logger.error("Could not fetch test step count for with testcase ID " + tcRecId,e);
				throw new DatabaseException("Could not fetch test cases",e);
			}
		return count;
		
	}
	/*Added by Padmavathi for TENJINCG-849 ends*/
	/*Added by Pushpa for TENJINCG-897 sarts*/
	public boolean hydrateStepsForTestCaseDelete(int[] testCaseRecordId) throws DatabaseException, SQLException{
		boolean deleteStatus=true;
		for(int tcRecId:testCaseRecordId){
			try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst=conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID = ? ");){
				pst.setInt(1,tcRecId);
				try(ResultSet rs=pst.executeQuery();){
					while(rs.next()){
						deleteStatus=false;
						break;
					}
				}
			}
		}
		return deleteStatus;
	}
	/*Added by Pushpa for TENJINCG-897 ends*/
	/*Added by Pushpa for TENJINCG-910 starts*/
	public String getAppDefaultBrowser(int appId) throws SQLException, DatabaseException {
		
		String AppDefaultBrowser="";
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			/*modified by shruthi for VAPT Helper fix starts*/
			PreparedStatement pst=conn.prepareStatement("SELECT APP_DEF_BROWSER FROM MASAPPLICATION WHERE APP_ID = ?");){
			pst.setInt(1,appId);
			/*modified by shruthi for VAPT Helper fix starts*/
			try(ResultSet rs=pst.executeQuery();){
				while(rs.next()){
					AppDefaultBrowser=rs.getString(1);
				}
			}
		}
		return AppDefaultBrowser;
	}
	/*Added by Pushpa for TENJINCG-910 ends*/
	
	/*Added by Ashiki for Tenj210-108 starts*/
	public void updateImportedTeststep(TestStep step) throws DatabaseException {
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("UPDATE TESTSTEPS SET TSTEP_SHT_DESC=?,TSTEP_DATA_ID=?,TSTEP_TYPE=?,TSTEP_DESC=?,TSTEP_EXP_RESULT=?,APP_ID=?,FUNC_CODE=?,TXNMODE=?,TSTEP_OPERATION=?, TSTEP_AUT_LOGIN_TYPE = ?, TSTEP_ROWS_TO_EXEC=?,TSTEP_VAL_TYPE=?,TSTEP_API_RESP_TYPE=? ,TSTEP_RE_RUN=? WHERE TSTEP_ID=? and TC_REC_ID=?");)
			{
			
			pst.setString(1, step.getShortDescription());
			pst.setString(2, step.getDataId());
			pst.setString(3, step.getType());
			pst.setString(4, step.getDescription());
			pst.setString(5, step.getExpectedResult());
			pst.setInt(6, step.getAppId());
			pst.setString(7, step.getModuleCode());
			pst.setString(8, step.getTxnMode());
			pst.setString(9, step.getOperation());
			pst.setString(10, step.getAutLoginType());
			if(Utilities.trim(step.getValidationType()).equalsIgnoreCase("ui")) {
				pst.setInt(11, 1);
			}else {
				pst.setInt(11, step.getRowsToExecute());
			}
			pst.setString(12, step.getValidationType());
			pst.setInt(13, step.getResponseType());
			pst.setString(14,step.getRerunStep());
			pst.setString(15, step.getId());
			pst.setInt(16, step.getTestCaseRecordId());
			pst.execute();
		}catch (Exception e) {
			logger.error("Error ", e);
			throw new DatabaseException("Could not update Application User because of an internal error ");
		}
	}
	/*Added by Ashiki for Tenj210-108 ends*/
}
