/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DeviceHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

  *//******************************************
  * CHANGE HISTORY
  * ==============
  *
  * DATE                	 CHANGED BY                DESCRIPTION
  *  22-Dec-2017				Sahana					Mobility	
  */
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.device.RegisteredDevice;
import com.ycs.tenjin.util.Constants;

public class DeviceHelper {
	private static final Logger logger = LoggerFactory.getLogger(ClientHelper.class);

	public ArrayList<RegisteredDevice> hydrateAllDevices() throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<RegisteredDevice> deviceList = null;

		try {
			pst = conn
					.prepareStatement("SELECT * FROM MASDEVICES");
			rs = pst.executeQuery();
			deviceList = new ArrayList<RegisteredDevice>();
			while (rs.next()) {

				RegisteredDevice rd = new RegisteredDevice();
				rd.setDeviceName(rs.getString("DEVICE_NAME"));
				rd.setDeviceId(rs.getString("DEVICE_ID"));
				rd.setPlatform(rs.getString("DEVICE_PLATFORM"));
				rd.setDeviceType(rs.getString("DEVICE_TYPE"));
				rd.setPlatformVersion(rs.getString("DEVICE_PLATFORM_VERSION"));
				rd.setRegisteredBy(rs.getString("DEVICE_REGISTERED_BY"));
				//rd.setRegisteredOn(rs.getString("DEVICE_REGISTERED_ON"));
				rd.setDeviceRecId(rs.getString("DEVICE_REC_ID"));
				rd.setDeviceStatus(rs.getString("DEVICE_STATUS"));
				deviceList.add(rd);
			}
			

		}catch (Exception e) {
			throw new DatabaseException(
					"Could not fetch list of devices", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return deviceList;

	}
	public int hydrateDeviceWithId(String deviceId) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		ResultSet rs=null;
		int count=0;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {

			pst = conn
					.prepareStatement("SELECT * FROM MASDEVICES WHERE DEVICE_ID =?");
			pst.setString(1,deviceId );
			rs = pst.executeQuery();

			while (rs.next()) {
				count++;
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record ", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return count;
	}
	public RegisteredDevice hydrateDevice(int devRecId) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		RegisteredDevice rd = null;
		try {

			pst = conn
					.prepareStatement("SELECT * FROM MASDEVICES WHERE DEVICE_REC_ID =?");
			pst.setInt(1,devRecId );
			rs = pst.executeQuery();

			while (rs.next()) {
				rd = new RegisteredDevice();
				rd.setDeviceId(rs.getString("DEVICE_ID"));
				rd.setDeviceName(rs.getString("DEVICE_NAME"));
				rd.setDeviceType(rs.getString("DEVICE_TYPE"));
				rd.setPlatform(rs.getString("DEVICE_PLATFORM"));
				rd.setPlatformVersion(rs.getString("DEVICE_PLATFORM_VERSION"));
				rd.setDeviceRecId(rs.getString("DEVICE_REC_ID"));
				rd.setDeviceStatus(rs.getString("DEVICE_STATUS"));
				rd.setDeviceDescription(rs.getString("DEVICE_DESC"));
			}

			
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record ", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return rd;
	}

	public void removeAllDevices(String[] deviceRecIdList) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		/*modified by shruthi for sql injection starts*/
		for(String str :deviceRecIdList ) {
		try {
			
			pst = conn.prepareStatement("DELETE FROM MASDEVICES WHERE DEVICE_REC_ID=?");
			pst.setString(1, str);
			pst.executeUpdate();
		} catch (SQLException e) {
			logger.error("ERROR removing device",e );
			throw new DatabaseException("Could not delete device due to an internal error.", e);
		}finally
		{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		}
		/*modified by shruthi for sql injection ends*/

	}

	public void removeDevice(String deviceRecId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		try {
			pst = conn.prepareStatement("DELETE FROM MASDEVICES WHERE DEVICE_REC_ID=?");
			pst.setInt(1, Integer.valueOf(deviceRecId));

			pst.executeUpdate();
		} catch (SQLException e) {
			logger.error("ERROR removing device",e );
			throw new DatabaseException("Could not delete device due to an internal error.", e);
		}finally
		{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		
		}

	}

	public void persistDevice(RegisteredDevice rd) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}

		try {

			pst = conn.prepareStatement("INSERT INTO MASDEVICES (DEVICE_REC_ID,DEVICE_ID,DEVICE_PLATFORM,DEVICE_PLATFORM_VERSION,DEVICE_REGISTERED_ON,DEVICE_REGISTERED_BY,DEVICE_NAME,DEVICE_TYPE,DEVICE_STATUS) VALUES (?,?,?,?,?,?,?,?,?)");
			int recId = DatabaseHelper.getGlobalSequenceNumber(conn);
			pst.setString(1, String.valueOf(recId));
			pst.setString(2, rd.getDeviceId());
			pst.setString(3, rd.getPlatform());
			pst.setString(4,rd.getPlatformVersion());
			pst.setTimestamp(5, new Timestamp(rd.getRegisteredOn().getTime()));
			pst.setString(6, rd.getRegisteredBy());
			pst.setString(7, rd.getDeviceName());
			pst.setString(8, rd.getDeviceType());
			pst.setString(9, rd.getDeviceStatus());

			pst.execute();
		}catch(DatabaseException e){
			logger.error("ERROR persisting Device", e);
			throw e;
		}catch (Exception e) {
			throw new DatabaseException("Could not create Device", e);
		} finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}


	}

	public void updateDevice(RegisteredDevice rd) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}

		try {
			pst = conn.prepareStatement("UPDATE MASDEVICES SET DEVICE_NAME = ?, DEVICE_PLATFORM_VERSION=?,DEVICE_DESC=?,DEVICE_STATUS=? WHERE DEVICE_REC_ID=?");
			pst.setString(1,rd.getDeviceName());
			pst.setString(2,rd.getPlatformVersion());
			pst.setString(3,rd.getDeviceDescription());
			pst.setString(4,rd.getDeviceStatus());
			pst.setString(5,rd.getDeviceRecId());
			pst.execute();
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new DatabaseException(
					"Could not update Device because of an internal error ");
		}finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}
	
	public List<RegisteredDevice> hydrateAllDevicesWithStatus() throws DatabaseException {


		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<RegisteredDevice> deviceList = null;

		try {
			pst = conn
					.prepareStatement("SELECT * FROM MASDEVICES WHERE DEVICE_STATUS=?");
			pst.setString(1, "Active");
			rs = pst.executeQuery();
			deviceList = new ArrayList<RegisteredDevice>();
			while (rs.next()) {

				RegisteredDevice rd = new RegisteredDevice();
				rd.setDeviceName(rs.getString("DEVICE_NAME"));
				rd.setDeviceId(rs.getString("DEVICE_ID"));
				rd.setPlatform(rs.getString("DEVICE_PLATFORM"));
				rd.setDeviceType(rs.getString("DEVICE_TYPE"));
				rd.setPlatformVersion(rs.getString("DEVICE_PLATFORM_VERSION"));
				rd.setDeviceRecId(rs.getString("DEVICE_REC_ID"));
				rd.setDeviceStatus(rs.getString("DEVICE_STATUS"));
				deviceList.add(rd);
			}
			
		}catch (Exception e) {
			throw new DatabaseException(
					"Could not fetch list of devices", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return deviceList;
		
	}

}
