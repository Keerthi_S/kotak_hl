/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ReportHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 *
 * DATE              	CHANGED BY              DESCRIPTION
 * 21-12-2016			SRIRAM					Defect#TEN-24
 * 21-Dec-2016			Sriram					Defect TEN-22
 * 20-02-2017			SRIRAM					Defect#TENJINCG-132
 * 20-04-2018			Preeti					TENJINCG-616
 * 16-05-2018			Pushpalatha				TENJINCG-557
 * 30-03-2021           Paneendra               TENJINCG-1267
 */

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinReport;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class ReportHelper {
	private static final Logger logger = LoggerFactory.getLogger(ReportHelper.class);

	public ArrayList<Aut> fetchAllAuts() throws DatabaseException {
		logger.debug("Fetching Application Names");
		ArrayList<Aut> autList = new ArrayList<Aut>();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		Statement stmt =null;
		ResultSet rs =null;

		if (conn == null) {
			throw new DatabaseException("Invalid database connection");
		}

		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select app_id, app_name from masapplication");
			while (rs.next()) {
				Aut aut = new Aut();
				aut.setId(rs.getInt("app_id"));
				aut.setName(rs.getString("app_name"));
				autList.add(aut);
			}
			
		} catch (SQLException e) {
			logger.error("ERROR fetching Application information", e);
			throw new DatabaseException("Could not fetch application information due to an internal error");
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(stmt);
			DatabaseHelper.close(conn);
		}
		return autList;
	}
	
	/*Added by Pushpalatha for TENJINCG-557starts*/
	public ArrayList<Aut> hydrateProjectAuts(int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		ArrayList<Aut> auts = new ArrayList<Aut>();

		try{
			pst = conn.prepareStatement("Select * from TJN_PRJ_AUTS where PRJ_ID = ?");
			pst.setInt(1, projectId);

			rs = pst.executeQuery();

			pst1=null;
			while(rs.next()){
				int appId = rs.getInt("APP_ID");
				try{
					Aut aut = new AutHelper().hydrateAut(appId);
					
					pst1 = conn.prepareStatement("Select * from TJN_PRJ_AUTS where PRJ_ID = ? and app_id=?");
					pst1.setInt(1, projectId);
					pst1.setInt(2, appId);
					rs1 = pst1.executeQuery();
					while(rs1.next()){
						if(rs1.getString("AUT_URL")!=null){
							aut.setURL(rs1.getString("AUT_URL"));

						}
						aut.setTestDataPath(rs1.getString("TEST_DATA_PATH"));

					}
					auts.add(aut);
					rs1.close();
					pst1.close();
				}catch(Exception e){
					logger.error("Could not fetch details of AUT " + appId);
				}
			}
			/*Added by paneendra for TENJINCG-1267 starts*/
			rs.close();
			pst.close();
			/*Added by paneendra for TENJINCG-1267 ends*/
		}catch(Exception e){
			throw new DatabaseException("Could not fetch AUTs for Project",e);
		}finally{
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return auts;
	}
	

	public ArrayList<String> getUserDetails() throws DatabaseException {
		ArrayList<String> userNames = new ArrayList<String>();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		logger.debug("Fetching the user details");
		if (conn == null) {
			throw new DatabaseException("Invalid database connection");
		}
		try {
			pstmt = conn.prepareStatement("select user_id from masuser");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				userNames.add(rs.getString("user_id"));
			}
			
		} catch (SQLException e) {
			logger.error("ERROR fetching User Details", e);
			throw new DatabaseException("Could not fetch user information due to an internal error");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);
		}
		return userNames;
	}
	//Added by Sriram to fix defect#TEN-24
	public ArrayList<String> getUserDetails(int projectId) throws DatabaseException {
		ArrayList<String> userNames = new ArrayList<String>();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		logger.debug("Fetching the user details");
		if (conn == null) {
			throw new DatabaseException("Invalid database connection");
		}
		try {
			pstmt = conn.prepareStatement("select user_id from tjn_prj_users where prj_id=?");
			pstmt.setInt(1, projectId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				userNames.add(rs.getString("user_id"));
			}
			
		} catch (SQLException e) {
			logger.error("ERROR fetching User Details", e);
			throw new DatabaseException("Could not fetch user information due to an internal error");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);
		}
		return userNames;
	}
	//Added by Sriram to fix defect#TEN-24 ends
	
	
	/*public List<TenjinReport> generateApplicationWiseExecutionReport(String appId, String fromDate, String toDate) throws DatabaseException {*/
	public List<TenjinReport> generateApplicationWiseExecutionReport(String appId, String functionCode,String fromDate, String toDate, int projectId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		logger.debug("Fetching the information for Application-wise execution report");
		Statement pstmt =null;
		Statement stmt =null;
		ResultSet rs =null;
		ResultSet rs2 = null;
		if (conn == null) {
			throw new DatabaseException("Invalid database connection");
		}
		String reportQuery = "";


		String selectors = "";
		String groupBy = "";
		selectors = "SELECT VTS.APP_ID,VTS.APP_NAME,";
		groupBy = "GROUP BY VTS.APP_ID,VTS.APP_NAME";
		reportQuery = "COUNT(CASE WHEN VTS.TSTEP_STATUS='Pass' THEN 1 ELSE NULL END) AS PASS_COUNT,"
				+ "COUNT(CASE WHEN VTS.TSTEP_STATUS='Fail' THEN 1 ELSE NULL END) AS FAIL_COUNT,"
				+ "COUNT(CASE WHEN VTS.TSTEP_STATUS='Error' THEN 1 ELSE NULL END) AS ERROR_COUNT,"
				+ "SUM(VTS.TOTAL_VALS) AS TOTAL_VALS, "
				+ "SUM(VTS.PASS_VALS) AS PASS_VALS,"
				+ "SUM(VTS.FAIL_VALS) AS FAIL_VALS,"
				+ "SUM(VTS.ERROR_VALS) AS ERROR_VALS "
				+ "FROM v_tstep_exec_history_2 vts where "
				+ "VTS.PRJ_ID=" +  projectId + " ";
		
		//Apply Filters
		int aId = 0;
		try{
			aId = Integer.parseInt(appId);
		}catch(NumberFormatException e){
			logger.error("Invalid App ID [{}]", appId);
			logger.warn("Defaulting App ID to 0");
		}
		
		//AUT Filter
		if(aId > 0){
			reportQuery = reportQuery + " AND VTS.APP_ID=" + aId + " ";
		}
		boolean functionFiltered = false;
		//Function Filter
		if(!Utilities.trim(functionCode).equalsIgnoreCase("") && !Utilities.trim(functionCode).equalsIgnoreCase("-1") && !Utilities.trim(functionCode).equalsIgnoreCase("all")){
			selectors = selectors + "VTS.FUNC_CODE,";
			if(!Utilities.trim(functionCode).equalsIgnoreCase("0")){
				reportQuery = reportQuery + " AND VTS.FUNC_CODE='" + functionCode + "' ";
			}
			groupBy = groupBy + ", VTS.FUNC_CODE";
			functionFiltered = true;
		}
		
		/*Modified by Preeti for TENJINCG-616 starts*/
		List<TenjinReport> reports = new ArrayList<TenjinReport>();
		//Date Filters
		try {
			DatabaseMetaData meta = conn.getMetaData(); 
			if(!Utilities.trim(fromDate).equalsIgnoreCase("")){
				if(!Utilities.trim(toDate).equalsIgnoreCase("")){
					if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
						reportQuery = reportQuery + "and vts.run_id in (Select max(run_Id) from v_tstep_exec_history_2 vts1 where vts1.tstep_rec_id=vts.tstep_rec_id and run_start_time between to_date('" + fromDate + " 00:00:00','DD-MON-YY HH24:MI:SS') and to_date('" + toDate +" 23:59:59','DD-Mon-YY HH24:MI:SS'))";
					else
						reportQuery = reportQuery + "and vts.run_id in (Select max(run_Id) from v_tstep_exec_history_2 vts1 where vts1.tstep_rec_id=vts.tstep_rec_id and run_start_time between CONVERT(DATETIME,'" + fromDate + " 00:00:00',113) and CONVERT(DATETIME,'" + toDate +" 23:59:59',113))";
			}else{
				if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
					reportQuery = reportQuery + "and vts.run_id in (Select max(run_Id) from v_tstep_exec_history_2 vts1 where vts1.tstep_rec_id=vts.tstep_rec_id and run_start_time >= to_date('" + fromDate + " 00:00:00','DD-MON-YY HH:MI:SS'))";
				else
					reportQuery = reportQuery + "and vts.run_id in (Select max(run_Id) from v_tstep_exec_history_2 vts1 where vts1.tstep_rec_id=vts.tstep_rec_id and run_start_time >= CONVERT(DATETIME,'" + fromDate + " 00:00:00',113))";
			}
		/*Modified by Preeti for TENJINCG-616 ends*/
		}else{
			//Add final default filter
			reportQuery = reportQuery + "and vts.run_id in (Select max(run_Id) from v_tstep_exec_history_2 vts1 where vts1.tstep_rec_id=vts.tstep_rec_id)";
		}
		//Build the final query
		reportQuery = selectors + " " + reportQuery + " " + groupBy;
		
		logger.debug("Final Report Query --> {}", reportQuery);
		
		
			pstmt = conn.createStatement();
			rs = pstmt.executeQuery(reportQuery);
			
			int executed=0;
			int pass=0;
			int fail=0;
			int error=0;
			int applId = 0;
			String totalStepsQuery = "";
			while (rs.next()) {
				TenjinReport tjnReport = new TenjinReport();
				
				pass = rs.getInt("PASS_COUNT");
				fail = rs.getInt("FAIL_COUNT");
				error = rs.getInt("ERROR_COUNT");
				executed = pass+fail+error;
				fail = fail+error;
				applId = rs.getInt("APP_ID");
				
				if(functionFiltered && !Utilities.trim(functionCode).equalsIgnoreCase("0")){
					tjnReport.setModuleCode(functionCode);
					totalStepsQuery = "SELECT COUNT(TSTEP_REC_ID) AS TOTAL_STEPS FROM TESTSTEPS WHERE APP_ID=" + applId + " AND FUNC_CODE='" + functionCode + "' AND TC_REC_ID IN (SELECT TC_REC_ID FROM TESTCASES WHERE TC_PRJ_ID=" + projectId + ")";
				}else if(functionFiltered && Utilities.trim(functionCode).equalsIgnoreCase("0")){
					String fCode = rs.getString("FUNC_CODE");
					tjnReport.setModuleCode(fCode);
					totalStepsQuery = "SELECT COUNT(TSTEP_REC_ID) AS TOTAL_STEPS FROM TESTSTEPS WHERE APP_ID=" + applId + " AND FUNC_CODE='" + fCode + "' AND TC_REC_ID IN (SELECT TC_REC_ID FROM TESTCASES WHERE TC_PRJ_ID=" + projectId + ")";
				}else{
					totalStepsQuery = "SELECT COUNT(TSTEP_REC_ID) AS TOTAL_STEPS FROM TESTSTEPS WHERE APP_ID=" + applId + " AND TC_REC_ID IN (SELECT TC_REC_ID FROM TESTCASES WHERE TC_PRJ_ID=" + projectId + ")";
				}
				
				stmt = conn.createStatement();
				rs2 = stmt.executeQuery(totalStepsQuery);
				while(rs2.next()){
					tjnReport.setNoOfTestCases(rs2.getInt("TOTAL_STEPS"));
				}
				
			
				
				tjnReport.setApplication(rs.getString("APP_NAME"));
				tjnReport.setNoOfPassedTestCases(pass);
				tjnReport.setNoOfFailedTestCases(fail);
				tjnReport.setNoOfErrorTestCases(error);
				tjnReport.setExecuted(executed);
				//tjnReport.setFunctionCode(functionCode);
				
				tjnReport.setTotalValidations(rs.getInt("TOTAL_VALS"));
				tjnReport.setPassedValidations(rs.getInt("PASS_VALS"));
				tjnReport.setFailedValidations(rs.getInt("FAIL_VALS"));
				tjnReport.setErrorValidations(rs.getInt("ERROR_VALS"));
				tjnReport.setFailedValidations(tjnReport.getFailedValidations() + tjnReport.getErrorValidations());
				
				double ePercentage = ((double)pass/(double)tjnReport.getNoOfTestCases())*100;
				double roundOff = Math.round(ePercentage * 100.0) / 100.0;
				int rWithoutDecimals = (int) roundOff;
				String roundOffString = Integer.toString(rWithoutDecimals);
				tjnReport.setPercentage(roundOffString);
				
				double vPercentage = ((double) tjnReport.getPassedValidations() / (double) tjnReport.getTotalValidations()) *100;
				roundOff = Math.round(vPercentage * 100.0) / 100.0;
				rWithoutDecimals = (int) roundOff;
				roundOffString = Integer.toString(rWithoutDecimals);
				tjnReport.setValidationPercentage(roundOffString);
				reports.add(tjnReport);
			}
			
		} catch (SQLException e) {
			logger.error("ERROR while fetching the details for application-wise execution report",e);
			throw new DatabaseException("ERROR while fetching the details for application-wise execution report");
		} catch(Exception e){
			logger.error("Unknown Error while fetching the details for application-wise execution report",e);
			throw new DatabaseException("ERROR while fetching the details for application-wise execution report");
		}
		finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(rs2);
			DatabaseHelper.close(stmt);
			DatabaseHelper.close(conn);
			
		}
		return reports;
	}
	
	
	/*Added by ashiki for TJN27-180 by starts*/
	public ArrayList<HashMap<String, Object>> getTestCaseExecutionDetailData(int projectId, int tcRecId,
			String execFromDate, String execToDate) throws DatabaseException {

		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				){
					return this.getTestCaseExecutionDetailData(conn, projectId, tcRecId, execFromDate, execToDate);
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
	
	}
	/*Added by ashiki for TJN27-180 by end*/
	
	/*Added by ashiki for TJN27-180 by starts*/
	private ArrayList<HashMap<String, Object>> getTestCaseExecutionDetailData(Connection conn, int projectId,
			int tcRecId, String fromDate, String toDate) throws SQLException, DatabaseException {

		logger.info("hydrating test case execution detail");
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		/*Modified by Padmavathi to SqlServer qualification for Test reports*/
		HashMap<String,HashMap<String,Object>> obj=new HashMap<String,HashMap<String,Object>>();
		boolean isOracleDb=true;
		String query = "";
		String query1="";
		if(tcRecId!=0) {
			query1="AND B.TC_REC_ID="+tcRecId;
		}
		if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query=" SELECT A.TC_TOTAL_STEPS,A.RUN_ID, A.TC_TOTAL_PASS,A.TC_TOTAL_FAIL,A.TC_TOTAL_ERROR,B.TSTEP_ID,"
					+ "C.RUN_START_TIME,C.RUN_END_TIME,D.RUN_STATUS,C.RUN_USER FROM V_RUNRESULTS_TC A,V_TSTEP_EXEC_HISTORY_2 B, "
					+ "MASTESTRUNS C, V_RUNRESULTS D,TESTCASES E WHERE A.TC_REC_ID=B.TC_REC_ID AND A.RUN_ID=B.RUN_ID AND A.RUN_ID=C.RUN_ID and E.TC_REC_ID=B.TC_REC_ID AND C.RUN_STATUS IN ('Complete','Aborted')"
					+ " AND C.RUN_START_TIME>=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND C.RUN_START_TIME<=TO_DATE(?, 'DD-mm-YYYY HH24:MI:SS') AND B.PRJ_ID=?" + query1;
		}else{
			isOracleDb=false;
			
			query="SELECT A.TC_TOTAL_STEPS,A.RUN_ID,E.TC_NAME, A.TC_TOTAL_PASS,A.TC_TOTAL_FAIL,A.TC_TOTAL_ERROR,B.TSTEP_ID,C.RUN_START_TIME, C.RUN_END_TIME,D.RUN_STATUS,C.RUN_USER,B.FUNC_CODE,D.TOTAL_EXE_TCS "
					+ "FROM V_RUNRESULTS_TC A,V_TSTEP_EXEC_HISTORY_2 B, MASTESTRUNS C, V_RUNRESULTS D ,TESTCASES E "
					+ "WHERE A.TC_REC_ID=B.TC_REC_ID AND A.RUN_ID=D.RUN_ID AND A.RUN_ID=B.RUN_ID AND A.RUN_ID=C.RUN_ID and E.TC_REC_ID=B.TC_REC_ID AND C.RUN_STATUS IN ('Complete','Aborted')"
					+ "AND C.RUN_START_TIME>=CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101) AND C.RUN_START_TIME < CONVERT(DATETIME,REPLACE(CONVERT(VARCHAR(11),?), ' ', ' '),101)+1 AND B.PRJ_ID=?"+query1;
			
		}
		try (PreparedStatement pst = conn.prepareStatement(query);){
			if(conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("Oracle")){
				pst.setString(1, fromDate+" 00:00:00");
				pst.setString(2, toDate+" 23:59:59");
			}else{
				pst.setString(1, fromDate);
				pst.setString(2, toDate);
			}
			pst.setInt(3, projectId);
			try(ResultSet rs = pst.executeQuery()){
				
				while(rs.next()){
					TestRun run = new TestRun();
					HashMap<String,Object> record = new LinkedHashMap<String,Object>();
					record.put("Run Id", rs.getString("RUN_ID"));
					
					run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
					 run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
					 if(run.getStartTimeStamp() != null && run.getEndTimeStamp() != null){
						 long startMillis = run.getStartTimeStamp().getTime();
						 long endMillis = run.getEndTimeStamp().getTime();

						 long millis = endMillis - startMillis;

						 String eTime = String.format(
								 "%02d:%02d:%02d",
								 TimeUnit.MILLISECONDS.toHours(millis),
								 TimeUnit.MILLISECONDS.toMinutes(millis)
								 - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
										 .toHours(millis)),
								 TimeUnit.MILLISECONDS.toSeconds(millis)
								 - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
										 .toMinutes(millis)));
						 record.put("Elapsed Time", eTime);
					 }
					record.put("Function Id", rs.getString("FUNC_CODE")); 
					record.put("Test Case Count", rs.getString("TOTAL_EXE_TCS")); 
					record.put("Test Case Id", rs.getString("TC_NAME"));
					record.put("Test Step Id", rs.getString("TSTEP_ID"));
					
					record.put("Total Steps", rs.getString("TC_TOTAL_STEPS"));
					record.put("Total pass", rs.getString("TC_TOTAL_PASS"));
					record.put("Total Fail", rs.getString("TC_TOTAL_FAIL"));
					record.put("Total Error",rs.getString("TC_TOTAL_ERROR"));
					
					record.put("Executed By", rs.getString("RUN_USER"));
					
					record.put("Run Status", rs.getString("RUN_STATUS"));
					if(!obj.containsKey(rs.getString("RUN_ID"))) {
						obj.put(rs.getString("RUN_ID"), record);
					}else {
					HashMap<String, Object>map=	obj.get(rs.getString("RUN_ID"));
					String stepId=(String) map.get("TestStep Id")+","+record.get("TestStep Id");
					record.replace("TestStep Id", stepId);
					obj.put(rs.getString("RUN_ID"), record);
					}
					
				}
			}
			 for (Map.Entry me : obj.entrySet()) {
		          data.add((HashMap<String, Object>) me.getValue());
		        }
		} catch (SQLException e) {
			logger.error("Could not fetch records");
			throw new DatabaseException("Could not fetch records",e);
		}
		return data;
	
	}
	/*Added by ashiki for TJN27-180 by ends*/

	

	
}
