/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCasePaginator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 03-11-2016           	leelaprasad             Requirement bug num#628
* 19-01-2017		   	Manish				   	DEFECT(TENJINCG-40)
* 31-01-2017           	Abhilash                Bug TENJINCG-51 fixed.
* 28-07-2017           	Padmavathi              TENJINCG-317 
* 04-08-2017           	Manish                  TENJINCG-330
* 27-10-2017		   	Preeti				   	TENJINCG-365
* 21-Nov-2017		   	Gangadhar Badagi	    TENJINCG-471
* 20-04-2018			Preeti					TENJINCG-616
* 27-06-2018			Preeti					T251IT-149
*/

package com.ycs.tenjin.db;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.util.Constants;

public class TestCasePaginator {
	
	private static final Logger logger = LoggerFactory.getLogger(TestCasePaginator.class);
	
	private int pageSize;
	private int currentPage;
	private int numberOfPages;
	private int totalSize;
	private int startRowNumber;
	private int endRowNumber;
	private List<FilterCriteria> criteria ;
	private static final String DB_NAME = Constants.DB_TENJIN_PROJ;
	private static final String TABLE_NAME = "TESTCASES";
	private boolean next;
	private List<TestCase> testCases;
	
	private String tcIdFilter;
	private String tcNameFilter;
	private String tcTypeFilter;
	/*Added by Preeti for TENJINCG-365 starts*/
	private String tcModeFilter;
	
	public String getTcModeFilter() {
		return tcModeFilter;
	}

	public void setTcModeFilter(String tcModeFilter) {
		this.tcModeFilter = tcModeFilter;
	}
	/*Added by Preeti for TENJINCG-365 ends*/

	/*	 Changed by manish for Defect tenjincg-40 on 19-Jan-2017 starts */
	private String tcProjectId;
	public String getTcProjectId() {
		return tcProjectId;
	}

	public void setTcProjectId(String tcProjectId) {
		this.tcProjectId = tcProjectId;
	}
	/*	 Changed by manish for Defect tenjincg-40 on 19-Jan-2017 ends */
	public String getTcIdFilter() {
		return tcIdFilter;
	}

	public void setTcIdFilter(String tcIdFilter) {
		this.tcIdFilter = tcIdFilter;
	}

	public String getTcNameFilter() {
		return tcNameFilter;
	}

	public void setTcNameFilter(String tcNameFilter) {
		this.tcNameFilter = tcNameFilter;
	}

	public String getTcTypeFilter() {
		return tcTypeFilter;
	}

	public void setTcTypeFilter(String tcTypeFilter) {
		this.tcTypeFilter = tcTypeFilter;
	}


	private Map<Integer, String> pageMap;
	
	public boolean hasNext() {
		return next;
	}


	private String[] fieldsToFetch;
	
	public String[] getFieldsToFetch() {
		return fieldsToFetch;
	}

	public void setFieldsToFetch(String[] fieldsToFetch) {
		this.fieldsToFetch = fieldsToFetch;
	}

	public TestCasePaginator(int pageSize, List<FilterCriteria> criteria){
		this.pageSize = pageSize;
		this.criteria = criteria;
	}
	
	public TestCasePaginator(int pageSize) {
		this.pageSize = pageSize;
		this.criteria = new ArrayList<FilterCriteria>();
	}
	
	public TestCasePaginator(){
		this.pageSize = 10; //Default page size to 10 if no pagesize is specified
		this.criteria = new ArrayList<FilterCriteria>();
	}
	
	
	public List<FilterCriteria> getCriteria() {
		return criteria;
	}
	
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getTotalSize() {
		return totalSize;
	}
	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}
	public int getStartRowNumber() {
		return startRowNumber;
	}
	public void setStartRowNumber(int startRowNumber) {
		this.startRowNumber = startRowNumber;
	}
	public int getEndRowNumber() {
		return endRowNumber;
	}
	public void setEndRowNumber(int endRowNumber) {
		this.endRowNumber = endRowNumber;
	}
	
	

	public int getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	
	
	
	public List<TestCase> getTestCases() {
		return testCases;
	}

	public void setTestCases(List<TestCase> testCases) {
		this.testCases = testCases;
	}

	/*****************
	 * Initializes the paginator based on page size specified and total size of records in the database
	 * @throws DatabaseException
	 */
	public void init() throws DatabaseException {
		String initQuery = this.getQuery(new String[]{"Count(*) "});
		
		Connection conn = DatabaseHelper.getConnection(DB_NAME);
		
		Statement st = null;
		ResultSet rs = null;
		
		try{
			st = conn.createStatement();
			rs = st.executeQuery(initQuery);
			while(rs.next()){
				this.totalSize = rs.getInt(1);
			}
		}catch(SQLException e){
			logger.error("ERROR getting total record size", e);
			throw new DatabaseException("Could not fetch test cases", e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(st);
			DatabaseHelper.close(conn);
		}
		
		logger.debug("Total records found --> {}", this.totalSize);
		
		int d = this.totalSize / this.pageSize;
		int remainder = this.totalSize % this.pageSize;
		
		if(remainder < 0 || remainder > 0){
			this.numberOfPages = d+1;
		}else{
			this.numberOfPages = d;
		}
		
		logger.debug("Number of pages --> {}", this.numberOfPages);
		
		if(this.numberOfPages > 0){
			this.next = true;
			this.currentPage = 0;
			
			this.pageMap = new TreeMap<Integer, String>();
			for(int i=1; i<=this.numberOfPages; i++){
				int endRowNumber = i * this.pageSize;
				int startRowNumber = endRowNumber - (this.pageSize-1);
				if(startRowNumber < 1){
					startRowNumber = 1;
				}
				
				if(endRowNumber > this.totalSize){
					endRowNumber = this.totalSize;
				}
				
				if(i==963){
					System.out.println();
				}
				
				this.pageMap.put(i, Integer.toString(startRowNumber) + "," + Integer.toString(endRowNumber));
			}
			
			System.out.println("Page Map");
			for(Integer i:this.pageMap.keySet()){
				if(i == 963){
					System.out.println();
					}
				System.out.println("Page " + i + " --> " + this.pageMap.get(i));
			}
			
		}
	}
	
	public void fetchNextPage() throws DatabaseException{
		
		if(hasNext()){
			this.fetchPage(this.currentPage + 1);
			//Abhilash : Bug TENJINCG-51 start.
			if(this.currentPage > this.numberOfPages){
			//Abhilash : Bug TENJINCG-51 End.	
				this.next = false;
			}else{
				this.next = true;
			}
		}else{
			throw new DatabaseException("No more pages to fetch");
		}
	}
	
	public void fetchPrevious() throws DatabaseException{
		if(this.currentPage <= 1){
			throw new DatabaseException("Already on first page");
		}
		
		this.fetchPage(this.currentPage -1);
	}
	
	public void fetchPage(int pageNumber) throws DatabaseException{
		/*Changed by Leelaprasad for the SQL server search issue starts*/
		Connection conn =null;
		try{
			/*Changed by Leelaprasad for the SQL server search issue ends*/
		this.testCases = new ArrayList<TestCase>();
		if(pageNumber < 1 || pageNumber > this.numberOfPages){
			throw new DatabaseException("Invalid Page Number " + pageNumber);
		}
		/*Changed by Leelaprasad for the SQL server search issue starts*/
		conn = DatabaseHelper.getConnection(DB_NAME);
		/*Changed by Leelaprasad for the SQL server search issue ends*/
		
		Statement st = null;
		ResultSet rs = null;
		
		String baseQuery = this.getQuery();
		
		String indices = this.pageMap.get(pageNumber);
		this.startRowNumber = 0;
		this.endRowNumber = 0;
		try{
			if(indices != null && !indices.equalsIgnoreCase("")){
				String[] split = indices.split(",");
				this.startRowNumber = Integer.parseInt(split[0]);
				this.endRowNumber = Integer.parseInt(split[1]);
			}
		}catch(Exception e){
			logger.error("ERROR getting start and end row for page {}", pageNumber,e);
			throw new DatabaseException("Internal error. Please contact your system administrator");
		}
		
		/*Modified by Preeti for TENJINCG-616 starts*/
		String finalQuery ="";
		try{
			DatabaseMetaData meta = conn.getMetaData();
			if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				finalQuery = "SELECT C.* FROM (SELECT ROWNUM AS ROW_NUM, B.* FROM ( " + baseQuery + ") B) C WHERE ROW_NUM > " + (this.startRowNumber-1) + " AND ROW_NUM < " + (this.endRowNumber+1);
			else
				finalQuery = "SELECT C.* FROM (SELECT B.*, ROW_NUMBER() OVER (ORDER BY TC_REC_ID) AS ROW_NUM FROM ( " + baseQuery + ") B) C WHERE ROW_NUM > " + (this.startRowNumber-1) + " AND ROW_NUM < " + (this.endRowNumber+1);
			logger.debug("Final Query to fetch test cases from page [{}] (Start Row --> [{}], End Row --> [{}])");
			logger.debug(finalQuery);
			this.testCases = new ArrayList<TestCase>();
			st = conn.createStatement();
			rs = st.executeQuery(finalQuery);
			while(rs.next()){
				TestCase t = new TestCase();
				if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				{
					t.setTcRecId((this.getResultSetValue("TC_REC_ID", rs, BigDecimal.class)).intValueExact());
					t.setProjectId((this.getResultSetValue("TC_PRJ_ID", rs, BigDecimal.class)).intValueExact());
				}
				else
				{
					/*Changed by leelaprasad to fix tc search data type mismatch starts*/ 
					/*Changed by Leelaprasad for the SQL server search issue starts*/
					t.setTcRecId((this.getResultSetValue("TC_REC_ID", rs, Double.class)).intValue());
					t.setProjectId((this.getResultSetValue("TC_PRJ_ID", rs, Double.class)).intValue());
					/*Changed by Leelaprasad for the SQL server search issue ends*/
					/*Changed by leelaprasad to fix tc search data type mismatch ends*/
				}
				/*Modified by Preeti for TENJINCG-616 ends*/	
				t.setTcId(this.getResultSetValue("TC_ID", rs, String.class));
				t.setTcName(this.getResultSetValue("TC_NAME", rs, String.class));
				t.setTcType(this.getResultSetValue("TC_TYPE", rs, String.class));
				t.setTcPriority(this.getResultSetValue("TC_PRIORITY", rs, String.class));
				t.setTcReviewed(this.getResultSetValue("TC_REVIEWED", rs, String.class));
				t.setTcStatus(this.getResultSetValue("TC_STATUS", rs, String.class));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
				/* changed by leelaprasad for the requirement bug number #628 on 03-11-2016 starts*/
				t.setTcCreatedOn(this.getResultSetValue("TC_CREATED_ON", rs, Timestamp.class));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				/* changed by leelaprasad for the requirement bug number #628 on 03-11-2016 ends*/
				
				t.setTcModifiedOn(this.getResultSetValue("TC_MODIFIED_ON", rs, Date.class));
				t.setTcDesc(this.getResultSetValue("TC_DESC", rs, String.class));
				t.setTcCreatedBy(this.getResultSetValue("TC_CREATED_BY", rs, String.class));
				t.setTcModifiedBy(this.getResultSetValue("TC_MODIFIED_BY", rs, String.class));
				/*added by padmavathi for req TENJINCG-317 on 28-07-2017 starts*/
				t.setMode(this.getResultSetValue("TC_EXEC_MODE", rs, String.class));
				/*added by padmavathi for req ENJINCG-317 on 28-07-2017 ends*/
				/*added by manish for req TENJINCG-59 on 03-02-2017 starts*/
				t.setStorage(this.getResultSetValue("TC_STORAGE", rs, String.class));
				/*added by manish for req TENJINCG-59 on 03-02-2017 ends*/
				this.testCases.add(t);
			}
		}catch(Exception e){
			logger.error("ERROR fetching test cases on page {}", pageNumber, e);
			throw new DatabaseException("Internal error. Please contact your system administrator");
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(st);
			DatabaseHelper.close(conn);
		}
		
		
		this.currentPage = pageNumber;
	
		logger.debug("Current page now is {}", this.currentPage);
	/*Changed by Leelaprasad for the SQL server search issue starts*/
	}catch(Exception e){
	}finally{
		DatabaseHelper.close(conn);
	}
	/*Changed by Leelaprasad for the SQL server search issue ends*/
	}
	
	
	private <T> T getResultSetValue (String fieldName, ResultSet rs, Class<T> desiredClass){
		try{
			Object o = rs.getObject(fieldName);
			System.out.println(o.getClass());
			if(o.getClass().equals(desiredClass)){
				return desiredClass.cast(o);
			}else{
				throw new RuntimeException("Invalid Data Type");
			}
		}catch(SQLException e){
			logger.warn("Couldn't get value for field {} from result set - Error --> {}", fieldName, e.getMessage());
			return null;
		}catch(ClassCastException e){
			logger.warn("Couldn't get value for field {} from result set - Error --> {}", fieldName, e.getMessage());
			return null;
		}catch(Exception e){
			logger.warn("Couldn't get value for field {} from result set - Error --> {}", fieldName, e.getMessage());
			return null;
		}
	}
	
	
	private String getQuery() {
	/*Changed by Leelaprasad for the SQL server search issue starts*/
		Connection conn=null;
		String query = "";
		try{
			conn = DatabaseHelper.getConnection(DB_NAME);
			DatabaseMetaData meta = conn.getMetaData();
		query = "";
		/*Changed by Leelaprasad for the SQL server search issue ends*/
		String cols = "";
		if(this.fieldsToFetch != null && this.fieldsToFetch.length > 0){
			for(String field:this.fieldsToFetch){
				if(cols.length() < 1){
					cols += field;
				}else{
					cols += ", " + field;
				}
			}
		}else{
			cols = "*";
		}
		
		query = "SELECT " + cols + " FROM " + TABLE_NAME;
		
		String filterList = "";
		FilterCriteria orderBy = null;
		if(this.criteria != null && this.criteria.size() > 0){
			for(FilterCriteria criteria:this.criteria){
				if(criteria.getCondition().equalsIgnoreCase(FilterCriteria.ORDER_BY)){
					orderBy = criteria;
					continue;
				}
				if(criteria.toString() != null){
					if(filterList.length() < 1){
						filterList += criteria.toString();
					}else{
						filterList += " AND " + criteria.toString();
					}
				}
			}
		}
		
		if(filterList.length() > 0){
			query = query + " WHERE " + filterList;
		}
		
		if(orderBy != null && orderBy.toString() != null){
			query = query + " " + orderBy.toString();
		}
		/*Added by Preeti for T251IT-149 starts*/
		else
		{
        /*Changed by Leelaprasad for the SQL server search issue starts*/
			if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle")){
			/*Changed by Leelaprasad for the SQL server search issue starts*/
			query = query + " order by TC_REC_ID ";
			/*Changed by Leelaprasad for the SQL server search issue starts*/
			}
			/*Changed by Leelaprasad for the SQL server search issue starts*/
		}
		/*Added by Preeti for T251IT-149 ends*/
		logger.debug("Generated Query --> {}", query);
		/*Changed by Leelaprasad for the SQL server search issue starts*/
	}catch(Exception e){
	}finally{
		DatabaseHelper.close(conn);
	}
	/*Changed by Leelaprasad for the SQL server search issue ends*/
		return query;
	
	}
	
	private String getQuery(String[] fieldsToFetch) {
		
		String query = "";
		
		String cols = "";
		if(fieldsToFetch != null && fieldsToFetch.length > 0){
			for(String field:fieldsToFetch){
				if(cols.length() < 1){
					cols += field;
				}else{
					cols += ", " + field;
				}
			}
		}else{
			cols = "*";
		}
		
		query = "SELECT " + cols + " FROM " + TABLE_NAME;
		
		String filterList = "";
		FilterCriteria orderBy = null;
		if(this.criteria != null && this.criteria.size() > 0){
			for(FilterCriteria criteria:this.criteria){
				if(criteria.getCondition().equalsIgnoreCase(FilterCriteria.ORDER_BY)){
					orderBy = criteria;
					continue;
				}
				if(criteria.toString() != null){
					if(filterList.length() < 1){
						filterList += criteria.toString();
					}else{
						filterList += " AND " + criteria.toString();
					}
				}
			}
		}
		
		if(filterList.length() > 0){
			
			query = query + " WHERE " + filterList;
			
		}
		
		if(orderBy != null && orderBy.toString() != null){
			query = query + " " + orderBy.toString();
		}
	
		logger.debug("Generated Query --> {}", query);
		return query;
		
	}
	
	/*Added by Gangadhar Badagi for TENJINCG-471 starts*/
	private String getRestQuery() {

	    String query = "SELECT * FROM " + TABLE_NAME;
		
		String filterList = "";
		FilterCriteria orderBy = null;
		if(this.criteria != null && this.criteria.size() > 0){
			for(FilterCriteria criteria:this.criteria){
				if(criteria.getCondition().equalsIgnoreCase(FilterCriteria.ORDER_BY)){
					orderBy = criteria;
					continue;
				}
				if(criteria.toString() != null){
					if(filterList.length() < 1){
						filterList += criteria.toString();
					}else{
						filterList += " AND " + criteria.toString();
					}
				}
			}
		}
		
		if(filterList.length() > 0){
			query = query + " WHERE " + filterList;
		}
		
		if(orderBy != null && orderBy.toString() != null){
			query = query + " " + orderBy.toString();
		}
		logger.debug("Generated Query --> {}", query);
		return query;
		
	}
	
	public ArrayList<TestCase> queryTestCase() throws DatabaseException {
		String initQuery = this.getRestQuery();
		
		Connection conn = DatabaseHelper.getConnection(DB_NAME);
		
		Statement st = null;
		ResultSet rs = null;
		ArrayList<TestCase> listTestCases=new ArrayList<TestCase>();
		
		try{
			st = conn.createStatement();
			rs = st.executeQuery(initQuery);
			while(rs.next()){
				TestCase testCase=new TestCase();
				testCase.setTcId(rs.getString("TC_ID"));
				testCase.setTcName(rs.getString("TC_NAME"));
				testCase.setProjectId(Integer.parseInt(rs.getString("TC_PRJ_ID")));
				testCase.setTcDesc(rs.getString("TC_DESC"));
				testCase.setMode(rs.getString("TC_EXEC_MODE"));
				testCase.setTcType(rs.getString("TC_TYPE"));
				testCase.setTcPriority(rs.getString("TC_PRIORITY"));
				testCase.setTcStatus(rs.getString("TC_STATUS"));
				listTestCases.add(testCase);
			}
		}catch(SQLException e){
			throw new DatabaseException("Could not fetch test cases", e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(st);
			DatabaseHelper.close(conn);
		}
		return listTestCases;
	}
	/*Added by Gangadhar Badagi for TENJINCG-471 ends*/
	public static void main(String[] args) throws DatabaseException {
		
		List<FilterCriteria> criteria = new ArrayList<FilterCriteria>();
		
		criteria.add(new FilterCriteria("TC_REC_TYPE", new String[]{"FLD"}, FilterCriteria.NOT_IN));
		criteria.add(new FilterCriteria("TC_PRJ_ID", 1));
		criteria.add(new FilterCriteria("TC_REC_ID", FilterCriteria.ORDER_BY));
		
		TestCasePaginator p = new TestCasePaginator(10, criteria);
		p.init();
		
		
	}
	
}
