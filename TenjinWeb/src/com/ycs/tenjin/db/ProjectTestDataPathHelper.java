/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectTestDataPathHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 23-10-2018		   	Pushpalatha			  	Newly Added 
* 14-11-2019			Roshni					TENJINCG-1166
* 05-02-2020			Roshni					TENJINCG-1168
* 15-06-2021			Ashiki					TENJINCG-1275

*/

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.MessageValidate;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.handler.MessageValidateHandler;
import com.ycs.tenjin.project.ProjectTestDataPath;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class ProjectTestDataPathHelper {
	
	private static Logger logger = LoggerFactory
			.getLogger(ProjectTestDataPathHelper.class);

	public ArrayList<ProjectTestDataPath> hydrateTDP(int projectId,int applicationId,String groupName)throws DatabaseException {
		
		ArrayList<ProjectTestDataPath> ps = new ArrayList<ProjectTestDataPath>();
		String query="SELECT A.*,B.APP_NAME FROM TJN_PRJ_AUTS A,MASAPPLICATION B WHERE B.APP_ID=A.APP_ID AND PRJ_ID = ?";
		if(applicationId>0){
			query=query+" AND A.APP_ID= ?";
		}
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		    PreparedStatement pst=conn.prepareStatement(query);){
				pst.setInt(1, projectId);
				if(applicationId>0){
					pst.setInt(2, applicationId);
				}
			try(ResultSet rs=pst.executeQuery();){
				while(rs.next()){
					int appId = rs.getInt("APP_ID");
					/* modified by Roshni for TENJINCG-1168 starts */
					List<Module> modules = new ModuleHelper().hydrateModules(conn,appId,groupName);
					/* modified by Roshni for TENJINCG-1168 ends */
					
					if(modules!=null){
						for(Module module:modules){
							ProjectTestDataPath p = new ProjectTestDataPath();
							ProjectTestDataPath p1 = new ProjectTestDataPath();
							p.setAppId(rs.getInt("APP_ID"));
							/*Modified by Roshni for TENJINCG-1166 starts*/
							p.setAppName(Utilities.escapeXml(rs.getString("APP_NAME")));
							p.setFuncCode(Utilities.escapeXml(module.getCode()));
							p.setGroupName(Utilities.escapeXml(module.getGroup()));
							/*Modified by Roshni for TENJINCG-1166 ends*/
							p.setProjectId(projectId);
							
							String tdp = "";

							try(PreparedStatement pst2 = conn.prepareStatement("SELECT * FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");){
								pst2.setInt(1, projectId);
								pst2.setInt(2, appId);
								pst2.setString(3, module.getCode());
								try(ResultSet rs2 = pst2.executeQuery();){
									while(rs2.next()){
										tdp = rs2.getString("TEST_DATA_PATH");
										p.setTestDataPath(rs2.getString("TEST_DATA_PATH"));
										/* modified by Roshni for TENJINCG-1166 starts */
										p.setFuncCode(Utilities.escapeXml(rs2.getString("FUNC_CODE")));
										/* modified by Roshni for TENJINCG-1166 ends */
										if (tdp==null ||tdp.equalsIgnoreCase("null")) {
											p1=updateTestDataPath(p);
											//VAPT null check fix
											if(p1.getTestDataPath()!=null && !p1.getTestDataPath().equalsIgnoreCase("null")){
												/*Modified by Roshni for TENJINCG-1166 starts*/
												p.setTestDataPath(Utilities.escapeXml(p1.getTestDataPath()));
												/*Modified by Roshni for TENJINCG-1166 ends*/
											}
											else{
												p.setTestDataPath("");
											}
										}
										else{
											/*Modified by Roshni for TENJINCG-1166 starts*/
											p.setTestDataPath(Utilities.escapeXml(tdp));
											/*Modified by Roshni for TENJINCG-1166 ends*/
										}
									}
								
								}
								
							}catch(Exception e){
								tdp = "";
							}
							if(p.getTestDataPath()==null)
							{
								p.setTestDataPath("");
							}
							ps.add(p);
						}
						List<Api> apis=new ArrayList<Api>();
						if(!groupName.equalsIgnoreCase("ALL")){
							/* modified by Roshni for TENJINCG-1168 starts */
							apis = new ApiHelper().hydrateAllApi(conn,Integer.toString(appId),groupName);
						}else{
							apis = new ApiHelper().hydrateAllApi(conn,Integer.toString(appId));
							/* modified by Roshni for TENJINCG-1168 ends */
						}
						for(Api api:apis) {
							ProjectTestDataPath p = new ProjectTestDataPath();
							ProjectTestDataPath p1 = new ProjectTestDataPath();
							p.setAppId(appId);
							/*Modified by Roshni for TENJINCG-1166 starts*/
							p.setAppName(Utilities.escapeXml(rs.getString("APP_NAME")));
							p.setFuncCode(Utilities.escapeXml(api.getCode()));
							p.setGroupName(Utilities.escapeXml(api.getGroup()));
							/*Modified by Roshni for TENJINCG-1166 ends*/
							p.setProjectId(projectId);
							String tdp = "";
							
							try(PreparedStatement pst2 = conn.prepareStatement("SELECT * FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");){
								pst2.setInt(1, projectId);
								pst2.setInt(2, appId);
								pst2.setString(3, api.getCode());
								try(ResultSet rs2 = pst2.executeQuery();){
									while(rs2.next()){
										tdp = rs2.getString("TEST_DATA_PATH");
										p.setTestDataPath(rs2.getString("TEST_DATA_PATH"));
										/* modified by Roshni for TENJINCG-1166 starts */
										p.setFuncCode(Utilities.escapeXml(rs2.getString("FUNC_CODE")));
										/* modified by Roshni for TENJINCG-1166 ends */
										if (tdp==null ||tdp.equalsIgnoreCase("null")) {
											p1=updateTestDataPath(p);
											//VAPT null check fix
											if(p1.getTestDataPath()!=null && !p1.getTestDataPath().equalsIgnoreCase("null")){
												/*Modified by Roshni for TENJINCG-1166 starts*/
												p.setTestDataPath(Utilities.escapeXml(p1.getTestDataPath()));
												/*Modified by Roshni for TENJINCG-1166 ends*/
											}
											else{
												p.setTestDataPath("");
											}
										}
										else{
											/*Modified by Roshni for TENJINCG-1166 starts*/
											p.setTestDataPath(Utilities.escapeXml(tdp));
											/*Modified by Roshni for TENJINCG-1166 ends*/
										}
									}
									
								}
								
							}catch(Exception e){
								tdp = "";
							}
							if(p.getTestDataPath()==null){
								p.setTestDataPath("");
							}
							ps.add(p);
						}
						
						/*Added by Ashiki for TENJINCG-1275 starts*/
						List<MessageValidate> msgs=new ArrayList<MessageValidate>();
							msgs = new MessageValidateHandler().hydrateAllMsgs(Integer.toString(appId));
						for(MessageValidate msg:msgs) {
							ProjectTestDataPath p = new ProjectTestDataPath();
							ProjectTestDataPath p1 = new ProjectTestDataPath();
							p.setAppId(appId);
							p.setAppName(Utilities.escapeXml(rs.getString("APP_NAME")));
							p.setFuncCode(Utilities.escapeXml(msg.getCode()));
							p.setProjectId(projectId);
							String tdp = "";
							
							try(PreparedStatement pst2 = conn.prepareStatement("SELECT * FROM MASTESTDATAPATH WHERE prj_id = ? AND APP_ID = ? AND FUNC_CODE = ?");){
								pst2.setInt(1, projectId);
								pst2.setInt(2, appId);
								pst2.setString(3, msg.getCode());
								try(ResultSet rs2 = pst2.executeQuery();){
									while(rs2.next()){
										tdp = rs2.getString("TEST_DATA_PATH");
										p.setTestDataPath(rs2.getString("TEST_DATA_PATH"));
										p.setFuncCode(Utilities.escapeXml(rs2.getString("FUNC_CODE")));
										if (tdp==null ||tdp.equalsIgnoreCase("null")) {
											p1=updateTestDataPath(p);
											if(p1.getTestDataPath()!=null && !p1.getTestDataPath().equalsIgnoreCase("null")){
												p.setTestDataPath(Utilities.escapeXml(p1.getTestDataPath()));
											}
											else{
												p.setTestDataPath("");
											}
										}
										else{
											p.setTestDataPath(Utilities.escapeXml(tdp));
										}
									}
									
								}
								
							}catch(Exception e){
								tdp = "";
							}
							if(p.getTestDataPath()==null){
								p.setTestDataPath("");
							}
							ps.add(p);
						}
						
					/*Added by Ashiki for TENJINCG-1275 ends*/
					}
				}
			}
		}catch(Exception e){
			logger.error("Error ", e);
		}
		return ps;
	}
	
	public ProjectTestDataPath updateTestDataPath(ProjectTestDataPath p) throws DatabaseException {
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn
						.prepareStatement("SELECT TEST_DATA_PATH FROM TJN_PRJ_AUTS WHERE PRJ_ID = ? AND APP_ID = ?");) {
			pst.setInt(1, p.getProjectId());
			pst.setInt(2, p.getAppId());
			try (ResultSet rs = pst.executeQuery();) {
				while (rs.next()) {
					if(rs.getString("TEST_DATA_PATH")==null||rs.getString("TEST_DATA_PATH").equalsIgnoreCase("null")){
						p.setTestDataPath("");
					}else{
						p.setTestDataPath(rs.getString("TEST_DATA_PATH"));
					}
				}
			}
			try (PreparedStatement pst2 = conn.prepareStatement(
					"UPDATE MASTESTDATAPATH SET TEST_DATA_PATH =? WHERE PRJ_ID = ? AND APP_ID = ? AND FUNC_CODE=?");) {

				pst2.setString(1, p.getTestDataPath());
				pst2.setInt(2, p.getProjectId());
				pst2.setInt(3, p.getAppId());
				pst2.setString(4, p.getFuncCode());
				pst2.executeQuery();
			}
		}catch(Exception e){
			throw new DatabaseException("Failed to update test data path",e);
		}
		return p;
	}

	

}
