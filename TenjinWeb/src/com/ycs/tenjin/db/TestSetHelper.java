/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestSetHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
 * 24-Apr-2018			Pushpa					TENJINCG-654
 * 30-07-2018			Preeti					Closed connections
 * 06-03-2019			Preeti					TENJINCG-946,947
 * 06-03-2019			Pushpa					TENJINCG-978
 * 08-10-2019           Padmavathi				TJN27-44 & TJN27-45
 * 09-01-2020			Prem					script fix
 * 05-02-2020			Roshni					TENJINCG-1168
 * 11-05-2020			Ashiki					TJN27-81
*/

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class TestSetHelper {
	private static final Logger logger = LoggerFactory.getLogger(TestSetHelper.class);
	/* modified by Roshni for TENJINCG-1168 starts */
	public TestSet hydrateTestSetBasicDetails(int tsId, int projectId,Connection conn) throws DatabaseException {

		/* modified by Roshni for TENJINCG-1168 ends */
		PreparedStatement pst = null;
		PreparedStatement pst2 = null;
		ResultSet rs = null;
		ResultSet rTc = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		TestCaseHelper tcHelper = new TestCaseHelper();

		TestSet ts = null;

		try {
			pst = conn.prepareStatement("SELECT * FROM TESTSETS WHERE TS_REC_ID = ? AND TS_PRJ_ID = ?");
			pst.setInt(1, tsId);
			pst.setInt(2, projectId);
			rs = pst.executeQuery();
			while (rs.next()) {
				ts = new TestSet();
				ts.setName(rs.getString("TS_NAME"));
				ts.setRecordType(rs.getString("TS_REC_TYPE"));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_TYPE"));
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				ts.setStatus(rs.getString("TS_STATUS"));
				ts.setDescription(rs.getString("TS_DESC"));
				ts.setPriority(rs.getString("TS_PRIORITY"));

				// Added by sakthi on 07-04-2017 starts
				ts.setMode(rs.getString("TS_EXEC_MODE"));
				// Added by sakthi on 07-04-2017 ends

				ArrayList<TestCase> testCases = null;
				if (ts.getRecordType().equalsIgnoreCase("TS")) {
					pst2 = conn.prepareStatement(
							"SELECT TSM_TC_REC_ID FROM TESTSETMAP WHERE TSM_PRJ_ID = ? AND TSM_TS_REC_ID = ? ORDER BY TSM_TC_SEQ");
					pst2.setInt(1, projectId);
					pst2.setInt(2, ts.getId());
					rTc = pst2.executeQuery();
					testCases = new ArrayList<TestCase>();
					while (rTc.next()) {
						int tcId = rTc.getInt("TSM_TC_REC_ID");
						TestCase tc = tcHelper.hydrateTestCaseBasicDetails(conn, projectId, tcId, null);
						testCases.add(tc);
					}

					ts.setTests(testCases);
				}
			}
		} catch (SQLException e) {
			
			logger.error("Exception in hydrateTestSetBasicDetails(int tsId, int projectId)");
			logger.error("Could not fetch details of Test Set", e);
			throw new DatabaseException("Could not fetch Test Set details");
		} finally {
			DatabaseHelper.close(rTc);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return ts;
	}

	/* changed by LATIEF for requirement# TJN_23_12 */
	
/*Modified by pushpa for TENJINCG-1177 starts*/
/*		public ArrayList<TestCase> hydrateUnMappedTCs(int testSetId, int projectId, String mode) throws DatabaseException {*/	
	public ArrayList<TestCase> hydrateUnMappedTCs(int testSetId, int projectId) throws DatabaseException {
		/*Modified by pushpa for TENJINCG-1177 ends*/		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or No database connection");
		}

		ArrayList<TestCase> testCases = null;

		try {

			pst = conn.prepareStatement(
					/*Modified by shruthi for TENJINCG-1177 starts*/

					"SELECT * FROM TESTCASES WHERE TC_REC_ID NOT IN (SELECT TSM_TC_REC_ID FROM TESTSETMAP WHERE TSM_PRJ_ID = ? AND TSM_TS_REC_ID = ?) and TC_PRJ_ID = ? ");
			/*Modified by shruthi for TENJINCG-1177 ends*/
			pst.setInt(1, projectId);
			pst.setInt(2, testSetId);

			/* Changed by parveen for Defect #626 and #621 starts */
			pst.setInt(3, projectId);
			
			// Added by sakthi on 07-04-2017 ends

			rs = pst.executeQuery();
			testCases = new ArrayList<TestCase>();
			while (rs.next()) {
				TestCase tc = new TestCase();
				tc.setTcRecId(rs.getInt("TC_REC_ID"));
				tc.setTcId(Utilities.escapeXml(rs.getString("TC_ID")));
				tc.setTcName(Utilities.escapeXml(rs.getString("TC_NAME")));
				tc.setTcPriority(rs.getString("TC_PRIORITY"));
				testCases.add(tc);
			}
		} catch (Exception e) {
			logger.error("Could not hydrate un_mapped test cases for test set " + testSetId, e);
			throw new DatabaseException("Could not get un_mapped test cases", e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return testCases;
	}

	/* changed by latief for requirement# TJN_23_12 ENDS */

	public TestSet hydrateTestSet(int tsId, int projectId) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement pst2 = null;
		ResultSet rTc = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		TestSet ts = null;
		TestCaseHelper tcHelper = new TestCaseHelper();
		try {
			pst = conn.prepareStatement("SELECT * FROM TESTSETS WHERE TS_REC_ID = ? AND TS_PRJ_ID = ?");
			pst.setInt(1, tsId);
			pst.setInt(2, projectId);
			rs = pst.executeQuery();

			while (rs.next()) {
				ts = new TestSet();
				ts.setName(rs.getString("TS_NAME"));
				ts.setRecordType(rs.getString("TS_REC_TYPE"));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_TYPE"));
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				ts.setStatus(rs.getString("TS_STATUS"));
				ts.setDescription(rs.getString("TS_DESC"));
				ts.setPriority(rs.getString("TS_PRIORITY"));
				// Fix for T25IT-211
				ts.setMode(rs.getString("TS_EXEC_MODE"));
				/* Changed by Leelaprasad for the defect T25IT-340 starts */
				ts.setProject(projectId);
				ArrayList<TestCase> testCases = null;
				if (ts.getRecordType().equalsIgnoreCase("TS")) {
					try {
						pst2 = conn.prepareStatement(
								"SELECT TSM_TC_REC_ID FROM TESTSETMAP WHERE TSM_PRJ_ID = ? AND TSM_TS_REC_ID = ? ORDER BY TSM_TC_SEQ");
						pst2.setInt(1, projectId);
						pst2.setInt(2, ts.getId());
						rTc = pst2.executeQuery();
						testCases = new ArrayList<TestCase>();
						while (rTc.next()) {
							int tcId = rTc.getInt("TSM_TC_REC_ID");
							TestCase tc = tcHelper.hydrateTestCase(projectId, tcId);
							/* Changed by manish for bug# TENJINCG-452 on 16-Nov-2017 starts */
							tc.setTcTsetId(ts.getId());
							/* Changed by manish for bug# TENJINCG-452 on 16-Nov-2017 ends */
							testCases.add(tc);
						}

					} catch (Exception e) {
						
					} finally {
						DatabaseHelper.close(pst2);
						DatabaseHelper.close(rTc);
					}
				}
				/* Added by Gangadhar Badagi for TENJINCG-292 starts */
				if (ts.getRecordType().equalsIgnoreCase("TSA")) {
					try {

						pst2 = conn.prepareStatement(
								"SELECT TSM_TC_REC_ID FROM TESTSETMAP WHERE TSM_PRJ_ID = ? AND TSM_TS_REC_ID = ? ORDER BY TSM_TC_SEQ");

						pst2.setInt(1, projectId);
						pst2.setInt(2, ts.getId());
						rTc = pst2.executeQuery();
						testCases = new ArrayList<TestCase>();
						while (rTc.next()) {
							int tcId = rTc.getInt("TSM_TC_REC_ID");
							TestCase tc = tcHelper.hydrateTestCase(projectId, tcId);
							/* Changed by manish for bug# TENJINCG-452 on 16-Nov-2017 starts */
							tc.setTcTsetId(ts.getId());
							/* Changed by manish for bug# TENJINCG-452 on 16-Nov-2017 ends */
							testCases.add(tc);
						}

					} catch (Exception e) {
						
					} finally {
						DatabaseHelper.close(pst2);
						DatabaseHelper.close(rTc);
					}
				}
				/* Added by Gangadhar Badagi for TENJINCG-292 ends */
				ts.setTests(testCases);
			}
		} catch (Exception e) {
			logger.error("Could not hydrate test set with ID " + tsId + " under project " + projectId, e);
			throw new DatabaseException("Could not fetch details of test set", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(rTc);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(conn);
		}
		return ts;
	}

	public TestSet hydrateTestSetForRun(Connection projConn, Connection appConn, int tsId, int projectId)
			throws DatabaseException {

		if (projConn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement pst2 = null;
		ResultSet rTc = null;
		TestSet ts = null;
		TestCaseHelper tcHelper = new TestCaseHelper();
		try {
			pst = projConn.prepareStatement("SELECT * FROM TESTSETS WHERE TS_REC_ID = ? AND TS_PRJ_ID = ?");
			pst.setInt(1, tsId);
			pst.setInt(2, projectId);
			rs = pst.executeQuery();

			while (rs.next()) {
				ts = new TestSet();
				ts.setName(rs.getString("TS_NAME"));
				ts.setRecordType(rs.getString("TS_REC_TYPE"));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_TYPE"));
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
				/* ts.setCreatedOn(rs.getDate("TS_CREATED_ON")); */
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				ts.setStatus(rs.getString("TS_STATUS"));
				ts.setDescription(rs.getString("TS_DESC"));
				ts.setPriority(rs.getString("TS_PRIORITY"));
				ArrayList<TestCase> testCases = null;
				if (ts.getRecordType().equalsIgnoreCase("TS")) {
					try {
						pst2 = projConn.prepareStatement(
								"SELECT TSM_TC_REC_ID FROM TESTSETMAP WHERE TSM_PRJ_ID = ? AND TSM_TS_REC_ID = ? ORDER BY TSM_TC_SEQ");
						pst2.setInt(1, projectId);
						pst2.setInt(2, ts.getId());
						rTc = pst2.executeQuery();
						testCases = new ArrayList<TestCase>();
						while (rTc.next()) {
							int tcId = rTc.getInt("TSM_TC_REC_ID");
							TestCase tc = tcHelper.hydrateTestCaseForRun(projConn, appConn, projectId, tcId);
							testCases.add(tc);
						}

					} catch (Exception e) {
						
					} finally {
						DatabaseHelper.close(pst2);
						DatabaseHelper.close(rTc);
					}
				}

				ts.setTests(testCases);
			}
		} catch (Exception e) {
			logger.error("Could not hydrate test set with ID " + tsId + " under project " + projectId, e);
			throw new DatabaseException("Could not fetch details of test set", e);
		} finally {

			DatabaseHelper.close(rTc);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);

		}
		return ts;
	}

	public TestSet hydrateTestSetForRun(int tsId, int projectId) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement pst2 = null;
		ResultSet rTc = null;
		TestSet ts = null;
		TestCaseHelper tcHelper = new TestCaseHelper();
		try {
			pst = conn.prepareStatement("SELECT * FROM TESTSETS WHERE TS_REC_ID = ? AND TS_PRJ_ID = ?");
			pst.setInt(1, tsId);
			pst.setInt(2, projectId);
			rs = pst.executeQuery();

			while (rs.next()) {
				ts = new TestSet();
				ts.setName(rs.getString("TS_NAME"));
				ts.setRecordType(rs.getString("TS_REC_TYPE"));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_TYPE"));
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
				/* ts.setCreatedOn(rs.getDate("TS_CREATED_ON")); */
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				ts.setStatus(rs.getString("TS_STATUS"));
				ts.setDescription(rs.getString("TS_DESC"));
				ts.setPriority(rs.getString("TS_PRIORITY"));
				ArrayList<TestCase> testCases = null;
				if (ts.getRecordType().equalsIgnoreCase("TS") || ts.getRecordType().equalsIgnoreCase("TSA")) {
					try {
						pst2 = conn.prepareStatement(
								"SELECT TSM_TC_REC_ID FROM TESTSETMAP WHERE TSM_PRJ_ID = ? AND TSM_TS_REC_ID = ? ORDER BY TSM_TC_SEQ");
						pst2.setInt(1, projectId);
						pst2.setInt(2, ts.getId());
						rTc = pst2.executeQuery();
						testCases = new ArrayList<TestCase>();
						while (rTc.next()) {
							int tcId = rTc.getInt("TSM_TC_REC_ID");
							TestCase tc = tcHelper.hydrateTestCaseForRun(conn, conn, projectId, tcId);
							testCases.add(tc);
						}

					} catch (Exception e) {
						
					} finally {
						DatabaseHelper.close(pst2);
						DatabaseHelper.close(rTc);
					}
				}

				ts.setMode(rs.getString("TS_EXEC_MODE"));

				ts.setTests(testCases);
			}
		} catch (Exception e) {
			logger.error("Could not hydrate test set with ID " + tsId + " under project " + projectId, e);
			throw new DatabaseException("Could not fetch details of test set", e);
		} finally {
			DatabaseHelper.close(rTc);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return ts;
	}

	public TestSet hydrateTestSet(String tsName, int projectId) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement pst2 = null;
		ResultSet rTc = null;
		TestSet ts = null;
		TestCaseHelper tcHelper = new TestCaseHelper();
		try {
			/* changed by manish for bug# TCGST-13 on 05-Dec-2017 starts */
			
			pst = conn.prepareStatement("SELECT * FROM TESTSETS WHERE upper(TS_NAME) = ? AND TS_PRJ_ID = ?");
			pst.setString(1, tsName.toUpperCase());
			/* changed by manish for bug# TCGST-13 on 05-Dec-2017 ends */
			pst.setInt(2, projectId);
			rs = pst.executeQuery();

			while (rs.next()) {
				ts = new TestSet();
				ts.setName(rs.getString("TS_NAME"));
				ts.setRecordType(rs.getString("TS_REC_TYPE"));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_TYPE"));
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				ts.setStatus(rs.getString("TS_STATUS"));
				/* Added by Roshni for TENJINCG-305 starts */
				ts.setMode(rs.getString("TS_EXEC_MODE"));
				/* Added by Roshni for TENJINCG-305 ends */
				ArrayList<TestCase> testCases = null;
				if (ts.getRecordType().equalsIgnoreCase("TS")) {
					try {

						pst2 = conn.prepareStatement(
								"SELECT TSM_TC_REC_ID FROM TESTSETMAP WHERE TSM_PRJ_ID = ? AND TSM_TS_ID = ?");
						pst2.setInt(1, projectId);
						pst2.setInt(2, ts.getId());
						rTc = pst2.executeQuery();
						testCases = new ArrayList<TestCase>();
						while (rTc.next()) {
							int tcId = rTc.getInt("TSM_TC_REC_ID");
							TestCase tc = tcHelper.hydrateTestCase(tcId, projectId);
							testCases.add(tc);
						}

					} catch (Exception e) {
						
					} finally {
						DatabaseHelper.close(pst2);
						DatabaseHelper.close(rTc);
					}
				}

				ts.setTests(testCases);
			}
		} catch (Exception e) {
			logger.error("Could not hydrate test set with Name " + tsName + " under project " + projectId, e);
			throw new DatabaseException("Could not fetch details of test set", e);
		} finally {
			DatabaseHelper.close(rTc);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);

		}
		return ts;
	}

	public ArrayList<TestSet> hydrateAllTestSets(int projectId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<TestSet> testSets = null;

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			/* changed by sahana for defect #TEN-92:Starts */
			pst = conn.prepareStatement("SELECT * FROM TESTSETS WHERE TS_PRJ_ID = ? AND TS_REC_TYPE='TS'");
			pst.setInt(1, projectId);
			rs = pst.executeQuery();
			testSets = new ArrayList<TestSet>();
			while (rs.next()) {
				TestSet ts = new TestSet();
				ts.setName(Utilities.escapeXml(rs.getString("TS_NAME")));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_TYPE"));
				ts.setDescription(rs.getString("TS_DESC"));
				ts.setProject(rs.getInt("TS_PRJ_ID"));
				ts.setPriority(rs.getString("TS_PRIORITY"));
				// Fix for T25IT-256 ends
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				ts.setStatus(rs.getString("TS_STATUS"));
				/* Changed by Leelaprasad for the defect T25IT-132 starts */
				ts.setMode(rs.getString("TS_EXEC_MODE"));

				testSets.add(ts);
			}
		} catch (Exception e) {
			logger.error("Could not fetch all test sets", e);
			throw new DatabaseException("Could not fetch Test Sets due to an internal error", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return testSets;
	}

	/* Added by Pushpa for TENJINCG-978,979 starts */
	public void persistTestSetMap1(int testSetId, String tcList, int projectId, int seq)
			throws RequestValidationException, DatabaseException, SQLException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst1 = null;
		try {
			PreparedStatement pst = null;
			ResultSet rs1 = null;
			String[] tcArray = tcList.split(",");
			for (String tc : tcArray) {
				try {
					pst = conn.prepareStatement(
							"SELECT * FROM TESTSETMAP WHERE TSM_PRJ_ID=? AND TSM_TS_REC_ID=? AND TSM_TC_rEC_ID=?");
					pst.setInt(1, projectId);
					pst.setInt(2, testSetId);
					pst.setInt(3, Integer.parseInt(tc));
					rs1 = pst.executeQuery();
					while (rs1.next()) {
						throw new RequestValidationException(
								"Some selected testcase(s) were already mapped to this test set");
					}

				} catch (RequestValidationException e) {
					throw new RequestValidationException(
							"Some selected testcase(s) were already mapped to this test set");
				} catch (Exception e) {
					logger.warn("Could not check if TC_REC_ID {} exists in Test Set {}", tc, testSetId, e);
				} finally {
					DatabaseHelper.close(rs1);
					DatabaseHelper.close(pst);
				}
			}

			if (tcList.equalsIgnoreCase("")) {
				logger.debug("No test Cases were Mapped for the testset with recordId:" + testSetId);
			} else {
				int tcArrayLen = tcArray.length;
				if (seq > -1) {
					
					try {
						/*modified by shruthi for VAPT Helper fix starts*/
						pst = conn.prepareStatement(
								"SELECT * FROM TESTSETMAP WHERE TSM_TC_SEQ >=? AND TSM_PRJ_ID=? AND TSM_TS_REC_ID=? order by TSM_TC_SEQ");
						pst.setInt(1,seq);
						pst.setInt(2,projectId);
						pst.setInt(3,testSetId);
						/*modified by shruthi for VAPT Helper fix ends*/
						rs1 = pst.executeQuery();
						while (rs1.next()) {
							int tcSeq = rs1.getInt("TSM_TC_SEQ") + tcArrayLen;
							/*modified by shruthi for VAPT Helper fix starts*/
							pst1 = conn.prepareStatement("UPDATE TESTSETMAP SET TSM_TC_SEQ=? WHERE TSM_PRJ_ID=? AND TSM_TS_REC_ID=? AND TSM_TC_REC_ID=?");
							pst1.setInt(1, tcSeq);
							pst1.setInt(2, rs1.getInt("TSM_PRJ_ID"));
							pst1.setInt(3, rs1.getInt("TSM_TS_REC_ID"));
							pst1.setInt(4, rs1.getInt("TSM_TC_REC_ID"));
							/*modified by shruthi for VAPT Helper fix ends*/
							pst1.execute();
						}

					} catch (Exception e) {
						logger.warn("ERROR getting latest sequence number for test case mapping", e);
					} finally {
						DatabaseHelper.close(rs1);
						DatabaseHelper.close(pst);
					}
				}

				int i = seq - 1;
				ResultSet rs = null;
				if (seq == -1) {
					try {
						pst = conn.prepareStatement(
								"SELECT coalesce(MAX(TSM_TC_SEQ),0) AS TSM_TC_SEQ FROM TESTSETMAP WHERE TSM_PRJ_ID=? AND TSM_TS_REC_ID=?");
						pst.setInt(1, projectId);
						pst.setInt(2, testSetId);
						rs = pst.executeQuery();

						while (rs.next()) {
							i = rs.getInt("TSM_TC_SEQ");
							break;
						}
					} catch (Exception e) {
						logger.warn("ERROR getting latest sequence number for test case mapping", e);
					} finally {
						DatabaseHelper.close(rs);
						DatabaseHelper.close(pst);
					}
				}

				for (String tc : tcArray) {
					i++;
					try {
						int tcId = Integer.parseInt(tc);
						pst = conn.prepareStatement(
								"INSERT INTO TESTSETMAP (TSM_PRJ_ID,TSM_TS_REC_ID,TSM_TC_REC_ID,TSM_TC_SEQ) VALUES(?,?,?,?)");
						pst.setInt(1, projectId);
						pst.setInt(2, testSetId);
						pst.setInt(3, tcId);
						pst.setInt(4, i);
						pst.execute();
					} catch (Exception e) {
						logger.error("Could not map test " + tc + " to test set " + testSetId, e);
						throw new DatabaseException("Could not map test " + tc + " to test set " + testSetId, e);
					} finally {
						DatabaseHelper.close(pst);
					}
				}
			}
		} finally {
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(conn);
		}

	}

	/* Added by Pushpa for TENJINCG-978,979 ends */
	// Changed by Sriram for TENJINCG-189 (12th June, 2017)
	/*
	 * public void persistTestSetMap(int testSetId, String tcList, int projectId)
	 * throws DatabaseException{
	 */
	public void persistTestSetMap(int testSetId, String tcList, int projectId, boolean clearMapBeforePersisting)
			throws DatabaseException {
		// Changed by Sriram for TENJINCG-189 (12th June, 2017) ends

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try {

			PreparedStatement pst = null;
			if (clearMapBeforePersisting) {
				try {
					pst = conn.prepareStatement("DELETE FROM TESTSETMAP WHERE TSM_TS_REC_ID = ? AND TSM_PRJ_ID = ?");
					pst.setInt(1, testSetId);
					pst.setInt(2, projectId);
					pst.execute();
					pst.close();
				} catch (Exception e) {
					logger.error("Could not clear existing mapping for test set " + testSetId, e);
					throw new DatabaseException("Could not clear existing mapping for test set " + testSetId, e);
				} finally {
					DatabaseHelper.close(pst);
				}
			}
			// Changed by Sriram for TENJINCG-189 (12th June, 2017) ends

			/* Changed by Leelaprasad for the requirement TJN_243_08 on 08-12-2016 starts */
			if (tcList.equalsIgnoreCase("")) {
				logger.debug("No test Cases were Mapped for the testset with recordId:" + testSetId);
			} else {
				/* Changed by Leelaprasad for the requirement TJN_243_08 on 08-12-2016 ends */
				String[] tcArray = tcList.split(",");
				int i = 0;
				ResultSet rs = null;
				// Added by Sriram for TENJINCG-189 (12th June, 2017) - Get latest sequence
				// number before inserting
				try {
					pst = conn.prepareStatement(
							"SELECT coalesce(MAX(TSM_TC_SEQ),0) AS TSM_TC_SEQ FROM TESTSETMAP WHERE TSM_PRJ_ID=? AND TSM_TS_REC_ID=?");
					pst.setInt(1, projectId);
					pst.setInt(2, testSetId);

					rs = pst.executeQuery();

					while (rs.next()) {
						i = rs.getInt("TSM_TC_SEQ");
						break;
					}

					
				} catch (Exception e) {
					logger.warn("ERROR getting latest sequence number for test case mapping", e);
				} finally {
					DatabaseHelper.close(rs);
					DatabaseHelper.close(pst);

				}
				// Added by Sriram for TENJINCG-189 (12th June, 2017) - Get latest sequence
				// number before inserting ends
				for (String tc : tcArray) {

					boolean tcExists = false;
					try {
						pst = conn.prepareStatement(
								"SELECT * FROM TESTSETMAP WHERE TSM_PRJ_ID=? AND TSM_TS_REC_ID=? AND TSM_TC_rEC_ID=?");
						pst.setInt(1, projectId);
						pst.setInt(2, testSetId);
						pst.setInt(3, Integer.parseInt(tc));

						rs = pst.executeQuery();

						while (rs.next()) {
							tcExists = true;
						}

						
					} catch (Exception e) {
						
						logger.warn("Could not check if TC_REC_ID {} exists in Test Set {}", tc, testSetId, e);
					} finally {
						DatabaseHelper.close(rs);
						DatabaseHelper.close(pst);
					}

					if (!tcExists) {
						i++;
						try {
							int tcId = Integer.parseInt(tc);
							pst = conn.prepareStatement(
									"INSERT INTO TESTSETMAP (TSM_PRJ_ID,TSM_TS_REC_ID,TSM_TC_REC_ID,TSM_TC_SEQ) VALUES(?,?,?,?)");
							// Changed by Parveen for changing insert query REQ #TJN_243_04 ends
							pst.setInt(1, projectId);
							pst.setInt(2, testSetId);
							pst.setInt(3, tcId);
							pst.setInt(4, i);
							pst.execute();
						} catch (Exception e) {
							logger.error("Could not map test " + tc + " to test set " + testSetId, e);
							throw new DatabaseException("Could not map test " + tc + " to test set " + testSetId, e);
						} finally {
							DatabaseHelper.close(pst);
						}
					}
				}
			}

		} finally {
			DatabaseHelper.close(conn);
		}

	}

	/* changed by latief for requirement# TJN_23_12 */
	public void persistTestSetunMap(int testSetId, String tcList, int projectId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		PreparedStatement pst = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try {
			try {
				pst = conn.prepareStatement("DELETE FROM TESTSETMAP WHERE TSM_TS_REC_ID = ? AND TSM_PRJ_ID = ?");
				pst.setInt(1, testSetId);
				pst.setInt(2, projectId);
				pst.execute();
			} catch (Exception e) {
				logger.error("Could not clear existing mapping for test set " + testSetId, e);
				throw new DatabaseException("Could not clear existing mapping for test set " + testSetId, e);
			} finally {
				DatabaseHelper.close(pst);
			}
			/* Changed by Leelaprasad for the requirement TJN_243_08 on 08-12-2016 starts */
			if (tcList.equalsIgnoreCase("")) {

				logger.debug("No test Cases were Mapped for the testset with recordId:" + testSetId);

			} else {
				/* Changed by Leelaprasad for the requirement TJN_243_08 on 08-12-2016 ends */
				String[] tcArray = tcList.split(",");
				int i = 0;

				for (String tc : tcArray) {
					i++;
					try {
						/*Modified by Ashiki for TJN27-81 starts*/
						int tcId = Integer.parseInt(tc.trim());
						/*Modified by Ashiki for TJN27-81 starts*/
						pst = conn.prepareStatement(
								"INSERT INTO TESTSETMAP (TSM_PRJ_ID,TSM_TS_REC_ID,TSM_TC_REC_ID,TSM_TC_SEQ) VALUES(?,?,?,?)");
						/* Changed by Parveen for changing insert query REQ #TJN_243_04 ends */
						pst.setInt(1, projectId);
						pst.setInt(2, testSetId);
						pst.setInt(3, tcId);
						pst.setInt(4, i);
						pst.execute();
					} catch (Exception e) {
						logger.error("Could not map test " + tc + " to test set " + testSetId, e);
						throw new DatabaseException("Could not map test " + tc + " to test set " + testSetId, e);
					} finally {
						DatabaseHelper.close(pst);
					}

				}

			}

		} finally {
			DatabaseHelper.close(conn);
		}
	}
	/* changed by latief for requirement# TJN_23_12 ENDS */
	/*modified by shruthi for VAPT helper fixes starts*/
	public TestSet persistTestSet(TestSet testSet, int projectId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		/*Statement st = null;*/
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs = null;
		ResultSet tRs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or No database connection");
		}

		try {
			
			pst1 = conn.prepareStatement("SELECT COUNT(*) as ts_cnt FROM TESTSETS WHERE TS_NAME =? AND TS_PRJ_ID =?");
			pst1.setString(1, testSet.getName());
			pst1.setInt(2, projectId);
			tRs = pst1.executeQuery();
			
			boolean duplicate = false;
			int tsCount = 0;
			while (tRs.next()) {
				tsCount = tRs.getInt("TS_CNT");
			}

			if (tsCount > 0) {
				duplicate = true;
			}

			if (duplicate) {
				throw new DatabaseException("TestSet arleady exists");
			}

			testSet.setId(DatabaseHelper.getGlobalSequenceNumber(conn));
			/* Modified by Padmavathi for TJN27-44 & TJN27-45 ends */

			pst = conn.prepareStatement(
					"INSERT INTO TESTSETS (TS_REC_ID,TS_REC_TYPE,TS_NAME,TS_FOLDER_ID,TS_PRJ_ID,TS_DESC,TS_CREATED_ON,TS_CREATED_BY,TS_TYPE,TS_STATUS,TS_EXEC_MODE,TS_PRIORITY) "
							+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

			pst.setInt(1, testSet.getId());
			pst.setString(2, testSet.getRecordType());
			pst.setString(3, testSet.getName());
			pst.setInt(4, testSet.getParent());
			pst.setInt(5, projectId);
			pst.setString(6, testSet.getDescription());
			Timestamp timeStamp = new Timestamp(new Date().getTime());
			pst.setTimestamp(7, timeStamp);
			testSet.setCreatedOn(timeStamp);
			pst.setString(8, testSet.getCreatedBy());
			pst.setString(9, testSet.getType());
			pst.setString(10, testSet.getStatus());

			pst.setString(11, testSet.getMode());
			pst.setString(12, testSet.getPriority());
			pst.execute();
		} catch (Exception e) {
			logger.error("Could not persist test set", e);
			/* Changed by Pushpalatha for TENJINCG-654 starts */
			throw new DatabaseException("Could not create record," + e.getMessage());
			/* Changed byb Pushpalatha for TENJINCG-654 ends */
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(tRs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return testSet;

	}
	/*modified by shruthi for VAPT helper fixes ends*/

	public ArrayList<TestCase> hydrateMappedTCs(int testSetId, int projectId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or No database connection");
		}

		ArrayList<TestCase> testCases = null;

		try {
			pst = conn.prepareStatement(
					"SELECT A.TSM_TC_SEQ, A.TSM_TC_REC_ID, B.* FROM TESTSETMAP A, TESTCASES B WHERE B.TC_REC_ID = A.TSM_TC_REC_ID AND A.TSM_PRJ_ID = ? AND A.TSM_TS_REC_ID = ? ORDER BY A.TSM_TC_SEQ ");
			/* Modified by Preeti for TENJINCG-946,947 ends */
			pst.setInt(1, projectId);
			pst.setInt(2, testSetId);
			rs = pst.executeQuery();
			testCases = new ArrayList<TestCase>();

			while (rs.next()) {
				TestCase tc = new TestCase();
				tc.setTcRecId(rs.getInt("TSM_TC_REC_ID"));
				tc.setTcId(Utilities.escapeXml(rs.getString("TC_ID")));
				tc.setTcName(Utilities.escapeXml(rs.getString("TC_NAME")));
				tc.setMode(rs.getString("TC_EXEC_MODE"));
				tc.setTcPriority(rs.getString("TC_PRIORITY"));
				tc.setTcType(rs.getString("TC_TYPE"));
				tc.setTcStatus(rs.getString("TC_STATUS"));
				tc.setTcCreatedBy(rs.getString("TC_CREATED_BY"));
				tc.setTcCreatedOn(rs.getTimestamp("TC_CREATED_ON"));
				tc.setTcModifiedBy(rs.getString("TC_MODIFIED_BY"));
				tc.setTcModifiedOn(rs.getDate("TC_MODIFIED_ON"));
				tc.setTcDesc(rs.getString("TC_DESC"));
				tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
				tc.setProjectId(rs.getInt("TC_PRJ_ID"));
				tc.setRecordType(rs.getString("TC_REC_TYPE"));
				tc.setTcReviewed(rs.getString("TC_REVIEWED"));
				tc.setStorage(rs.getString("TC_STORAGE"));
				testCases.add(tc);
			}
			/* Modified by Preeti for TENJINCG-946,947 ends */
		} catch (Exception e) {
			logger.error("Could not hydrate mapped test cases for test set " + testSetId, e);
			throw new DatabaseException("Could not get mapped test cases", e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return testCases;
	}

	/* Added by Roshni for TENJINCG-305 starts */
	public boolean existMappedTestCase(int testSetId, int projectId, int tcRecId) throws DatabaseException {
		boolean exist = false;
		ArrayList<TestCase> tc = this.hydrateMappedTCs(testSetId, projectId);
		for (TestCase tcase : tc) {
			if (tcase.getTcRecId() == tcRecId) {
				exist = true;
				break;
			}
		}
		return exist;
	}

	/* Added by Roshni for TENJINCG-305 ends */
	// save testset datails
	
	/*Modified by Pushpa for VAPT fix starts*/
	public String savetestsetdetails(int testid, String name, String desc, String type, String priority, String mode)
			throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		/*Statement st = null;*/
		PreparedStatement st = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or No database connection");
		}
		String pass = "";
		try {
			
			st=conn.prepareStatement("update TESTSETS set TS_NAME=?,TS_DESC=?,TS_TYPE=?,TS_PRIORITY=?,TS_EXEC_MODE=? where TS_REC_ID=?");
			st.setString(1, name);
			st.setString(2, desc);
			st.setString(3, type);
			st.setString(4, priority);
			st.setString(5, mode);
			st.setInt(6, testid);
			int i = st.executeUpdate();
			if (i > 0) {
				pass = "Test Set Successfully Updated";
			} else {
				pass = "Test Set Successfully Not Updated";
			}
		} catch (Exception e) {
			logger.error("Could not hydrate mapped test cases for test set " + testid, e);
			throw new DatabaseException("Could not get mapped test cases", e);
		} finally {

			DatabaseHelper.close(st);
			DatabaseHelper.close(conn);

		}

		return pass;
	}
	/*Modified by Pushpa for VAPT fix ends*/
	/* Added by Sriram for TENJINCG-189 (13 June 2017) */
	public ArrayList<TestSet> hydrateAllTestSets(int projectId, String mode) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<TestSet> testSets = null;

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			/* changed by sahana for defect #TEN-92:Starts */
			pst = conn.prepareStatement(
					"SELECT * FROM TESTSETS WHERE TS_PRJ_ID = ? AND TS_REC_TYPE='TS' AND TS_EXEC_MODE=?");
			/* changed by sahana for defect #TEN-92:ends */
			pst.setInt(1, projectId);
			pst.setString(2, mode);
			rs = pst.executeQuery();
			testSets = new ArrayList<TestSet>();
			while (rs.next()) {
				TestSet ts = new TestSet();
				ts.setName(rs.getString("TS_NAME"));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_REC_TYPE"));
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
				/* ts.setCreatedOn(rs.getDate("TS_CREATED_ON")); */
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				ts.setStatus(rs.getString("TS_STATUS"));

				testSets.add(ts);
			}
		} catch (Exception e) {
			logger.error("Could not fetch all test sets", e);
			throw new DatabaseException("Could not fetch Test Sets due to an internal error", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return testSets;
	}
	/* Added by Sriram for TENJINCG-189 (13 June 2017) ends */

	/* Added by Roshni for TENJINCG-305 starts */
	public String getTestSetName(int testSetRecordId, int projectId) throws DatabaseException {
		String testSetName = "";
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("SELECT TS_NAME FROM TESTSETS WHERE TS_REC_ID =? AND TS_PRJ_ID=?");
			pst.setInt(1, testSetRecordId);
			pst.setInt(2, projectId);
			rs = pst.executeQuery();
			while (rs.next()) {
				testSetName = rs.getString("TS_NAME");
			}

		} catch (SQLException e) {
			logger.error("ERROR getting test set name", e);
			throw new DatabaseException(
					"Could not get test set name due to an internal error. Please contact Tenjin Support.");
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return testSetName;
	}
	/* Added by Roshni for TENJINCG-305 ends */
	/*Added by shruthi for TENJINCG-1177 starts*/
	public ArrayList<TestSet> hydrateAllTestSets1(int projectId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<TestSet> testSets = null;

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			/* changed by sahana for defect #TEN-92:Starts */
			pst = conn.prepareStatement(
					"SELECT * FROM TESTSETS WHERE TS_PRJ_ID = ? AND TS_REC_TYPE='TS' ");
			/* changed by sahana for defect #TEN-92:ends */
			pst.setInt(1, projectId);
			
			rs = pst.executeQuery();
			testSets = new ArrayList<TestSet>();
			while (rs.next()) {
				TestSet ts = new TestSet();
				ts.setName(rs.getString("TS_NAME"));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_REC_TYPE"));
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
				/* ts.setCreatedOn(rs.getDate("TS_CREATED_ON")); */
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				ts.setStatus(rs.getString("TS_STATUS"));

				testSets.add(ts);
			}
		} catch (Exception e) {
			logger.error("Could not fetch all test sets", e);
			throw new DatabaseException("Could not fetch Test Sets due to an internal error", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return testSets;
	}
	/*Added by shruthi for TENJINCG-1177 ends*/

	
	
	
}
