/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectDTTHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 23-10-2018		   	Pushpalatha			  	Newly Added 
* 25-02-2019			Ashiki					TENJINCG-985
* 15-03-2019			Ashiki				  	TENJINCG-986
* 14-11-2019			Roshni					TENJINCG-1166
*/
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.defect.DefectManagementInstance;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class ProjectDTTHelper {

	
	
	public ArrayList<Aut> hydrateProjectDTT(int projectId) throws SQLException, DatabaseException {
		
		ArrayList<Aut> projectAUTs=new ArrayList<Aut>();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement pst=conn.prepareStatement("SELECT * FROM TJN_PRJ_AUTS WHERE PRJ_ID = ?")){
			pst.setInt(1, projectId);
			try(ResultSet rs=pst.executeQuery();){
				while(rs.next()){
					int appId = rs.getInt("APP_ID");
					try(PreparedStatement pst1=conn.prepareStatement("SELECT B.*,A.APP_NAME FROM MASAPPLICATION A, TJN_PRJ_AUTS B WHERE B.APP_ID=? AND B.PRJ_ID=? AND A.APP_ID=B.APP_ID")){
						pst1.setInt(1, appId);
						pst1.setInt(2, projectId);
						try(ResultSet rs1=pst1.executeQuery()){
							while(rs1.next()){
								Aut aut=new Aut();
								aut.setId(rs1.getInt("APP_ID"));
								/*Modified by Roshni for TENJINCG-1166 starts*/
								aut.setURL(Utilities.escapeXml(rs1.getString("AUT_URL")));
								aut.setName(Utilities.escapeXml(rs1.getString("APP_NAME")));
								aut.setTestDataPath(Utilities.escapeXml(rs1.getString("TEST_DATA_PATH")));
								aut.setInstance(Utilities.escapeXml(rs1.getString("PRJ_DEF_MANAGER")));
								aut.setDttProject(Utilities.escapeXml(rs1.getString("PRJ_DM_PROJECT")));
								aut.setDttProjectKey(Utilities.escapeXml(rs1.getString("DTT_PPROJECT_KEY")));
								aut.setDttEnableStatus(Utilities.escapeXml(rs1.getString("PRJ_DEF_LOGGING")));
								aut.setSubSet(Utilities.escapeXml(rs1.getString("PROJECT_SET_NAME")));
								projectAUTs.add(aut);
								/*Modified by Roshni for TENJINCG-1166 ends*/
							}
						}
						
					}
				}
			}
		}
		return projectAUTs;
	}

	
	public List<DefectManagementInstance> fetchDefectManagerInstance() throws DatabaseException {
		
		
	
		List<DefectManagementInstance> defectManager = new ArrayList<DefectManagementInstance>();

		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT DISTINCT INSTANCE_NAME,TOOL_NAME FROM TJN_DEF_TOOL_MASTER");) {
			
			
			while (rs.next()) {
				DefectManagementInstance defManager = new DefectManagementInstance();
				defManager.setInstance(rs.getString("INSTANCE_NAME"));
				defManager.setTool(rs.getString("TOOL_NAME"));
				defectManager.add(defManager);
			}

		} catch (SQLException e) {
			
			throw new DatabaseException("Could not deactivate project");
		}
		return defectManager;
	}

}
