/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCaseHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              	DESCRIPTION

 * 30-05-2018           Padmavathi             		TENJINCG-673
 * 15-06-2018           Padmavathi              	T251IT-107
 * 27-09-2018        	Padmavathi              	TENJINCG-741
 * 24-10-2018           Padmavathi              	TENJINCG-849
 * 04-01-2019           Padmavathi              	TJN252-62 
 * 19-02-2019			Ashiki						TJN252-60
 * 29-08-2019			Ashiki						TJN27-37
 * 09-01-2020			Prem						script fix 
 * 05-02-2020			Roshni						TENJINCG-1168
 * 15-06-2021			Ashiki				Newly Added for TENJINCG-1275

*/

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.ValidationDetails;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.run.ResultValidation;
import com.ycs.tenjin.handler.DependencyRulesHandler;
import com.ycs.tenjin.project.DependencyRules;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.testmanager.TestManagerInstance;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

public class TestCaseHelper {
	private static final Logger logger = LoggerFactory.getLogger(TestCaseHelper.class);

	private String STORAGE = "Local";
	/*Added by Prem for TENJINCG-1181*/
	private UnstructuredDataHelper helper = new UnstructuredDataHelper();
	/*Added by Prem for TENJINCG-1181*/
	public String deleteTestStep(TestStep step) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		PreparedStatement prepared = null;
		PreparedStatement pst1 = null, pst2 = null, pst3 = null, pst4 = null, pst5 = null;
		ResultSet rs = null;
		ResultSet rst = null;
		// Added by Roshni for TENJINCG-247
		int tcid = 0;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		String returnval = "";
		int records_exists = 0;
		try {
			/* Added by Roshni for TENJINCG-247 starts */

			/*modified by shruthi for VAPT helper fixes starts*/
			pst = conn.prepareStatement("SELECT TC_REC_ID FROM TESTSTEPS WHERE TSTEP_REC_ID=?");
			pst.setInt(1, step.getRecordId());
			/*modified by shruthi for VAPT helper fixes ends*/
			rs = pst.executeQuery();
			if (rs.next()) {
				tcid = rs.getInt("TC_REC_ID");
			}
			/*modified by shruthi for VAPT helper fixes starts*/
			prepared = conn.prepareStatement(
					"select 1 FROM mastestruns WHERE RUN_ID in (select distinct RUN_ID from RUNRESULTS_TC WHERE TC_REC_ID IN(SELECT TC_REC_ID FROM TESTSTEPS WHERE TSTEP_ID=?)) AND RUN_STATUS='Executing'");
			prepared.setString(1, step.getId());
			/*modified by shruthi for VAPT helper fixes ends*/
			rst = prepared.executeQuery();
			if (rst.next()) {
				records_exists = rst.getInt(1);
				returnval = "Executing";
			}
			rst.close();
			prepared.close();
			if (records_exists == 0) {

				pst1 = conn.prepareStatement("DELETE FROM RESVALRESULTS WHERE TSTEP_REC_ID = ?");
				pst1.setInt(1, step.getRecordId());

				pst2 = conn.prepareStatement("DELETE FROM RESVALMAP WHERE TSTEP_REC_ID = ?");
				pst2.setInt(1, step.getRecordId());

				pst3 = conn.prepareStatement("DELETE FROM RUNRESULT_TS_TXN WHERE TSTEP_REC_ID = ?");
				pst3.setInt(1, step.getRecordId());

				pst4 = conn.prepareStatement("DELETE FROM RUNRESULTS_TS WHERE TSTEP_REC_ID = ?");
				pst4.setInt(1, step.getRecordId());

				pst5 = conn.prepareStatement("DELETE FROM TESTSTEPS WHERE TSTEP_REC_ID = ?");
				pst5.setInt(1, step.getRecordId());

				pst1.execute();
				pst2.execute();
				pst3.execute();
				pst4.execute();
				pst5.execute();

				this.updateTestStepSeq(tcid);
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not remove Test Step", e);
		} finally {

			DatabaseHelper.close(rst);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(prepared);
			DatabaseHelper.close(pst5);
			DatabaseHelper.close(pst4);
			DatabaseHelper.close(pst3);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return returnval;
	}

	public String updateTestStep(TestStep step, String prj_name, String stepid) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		String returnval = "";
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		try {

			boolean abortUpdate = this.testStepConcurrencyCheck(conn, step.getRecordId());
			if (!abortUpdate) {
				String query = "";
				/*Modified by Ashiki for TENJINCG-1275 starts*/
				query = "UPDATE TESTSTEPS SET TSTEP_ID=?,TSTEP_SHT_DESC=?,TSTEP_DATA_ID=?,TSTEP_TYPE=?,TSTEP_DESC=?,TSTEP_EXP_RESULT=?,APP_ID=?,FUNC_CODE=?,TXNMODE=?,TSTEP_OPERATION=?, TSTEP_AUT_LOGIN_TYPE = ?, TSTEP_ROWS_TO_EXEC=?,TSTEP_VAL_TYPE=?,TSTEP_API_RESP_TYPE=?,TSTEP_FILENAME=?  WHERE TSTEP_REC_ID = ?";
				/*Modified by Ashiki for TENJINCG-1275 starts*/
				pst = conn.prepareStatement(query);
				pst.setString(1, step.getId());
				pst.setString(2, step.getShortDescription());
				pst.setString(3, step.getDataId());
				pst.setString(4, step.getType());
				pst.setString(5, step.getDescription());
				pst.setString(6, step.getExpectedResult());
				pst.setInt(7, step.getAppId());
				pst.setString(8, step.getModuleCode());
				/* Changed by Leelaprasad for the defect T25It-171 ends */
				pst.setString(9, step.getTxnMode());
				pst.setString(10, step.getOperation());
				pst.setString(11, step.getAutLoginType());
				if (Utilities.trim(step.getValidationType()).equalsIgnoreCase("ui")) {
					pst.setInt(12, 1);
				} else {
					pst.setInt(12, step.getRowsToExecute());
				}

				pst.setString(13, step.getValidationType());

				pst.setInt(14, step.getResponseType());
				/*Added by Ashiki for TENJINCG-1275 starts*/
				pst.setString(15, step.getFileName());
				pst.setInt(16, step.getRecordId());
				/*Added by Ashiki for TENJINCG-1275 ends*/
				pst.execute();
			} else {
				returnval = "Executing";
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not update test step", e);
		} finally {

			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return returnval;
	}

	@SuppressWarnings("finally")
	public String updateTestCase(TestCase testcase) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		boolean abortUpdate = false;
		try {
			abortUpdate = this.testCaseConcurrencyCheck(conn, testcase.getTcRecId());
		} catch (Exception e) {
			throw new DatabaseException("Test Case concurrency check failed");
		}

		if (abortUpdate) {

			DatabaseHelper.close(conn);
			return "Executing";
		}

		try {
			pst = conn.prepareStatement(
					"UPDATE TESTCASES SET tc_id = ?, tc_name = ?, tc_priority = ?, tc_type =?,tc_desc = ?,TC_EXEC_MODE = ? where tc_rec_id = ?");
			pst.setString(1, testcase.getTcId());
			pst.setString(2, testcase.getTcName());
			pst.setString(3, testcase.getTcPriority());
			pst.setString(4, testcase.getTcType());
			pst.setString(5, testcase.getTcDesc());

			// Added by sakthi on 07-04-2017 starts
			pst.setString(6, testcase.getMode());
			pst.setInt(7, testcase.getTcRecId());
			// Added by sakthi on 07-04-2017 ends

			pst.execute();
		} catch (Exception e) {
			throw new DatabaseException("Could not update test case due to an internal error", e);
		} finally {

			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
			return "Not Executing";

		}

	}
	/*modified by shruthi for VAPT helper fixes starts*/
	public ArrayList<TestCase> hydrateAllTestCases(String[] tcIds, String[] tsIds) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<TestCase> testCases = new ArrayList<TestCase>();

		try {

			for (int i = 0; i < tcIds.length; i++) {
				try {
					pst = conn.prepareStatement("SELECT * FROM TESTCASES WHERE TC_REC_ID =?");
					pst.setString(1, tcIds[i]);
					rs = pst.executeQuery();
					
					while (rs.next()) {
						TestCase tc = new TestCase();
						tc.setTcId(rs.getString("tc_id"));
						tc.setTcName(rs.getString("tc_name"));
						tc.setTcType(rs.getString("tc_type"));
						tc.setTcStatus(rs.getString("tc_status"));
						tc.setTcCreatedBy(rs.getString("tc_created_by"));
						tc.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
						tc.setTcModifiedBy(rs.getString("tc_modified_by"));
						tc.setTcModifiedOn(rs.getDate("tc_modified_on"));
						tc.setTcDesc(rs.getString("tc_desc"));
						tc.setTcRecId(rs.getInt("TC_REC_ID"));
						tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
						tc.setProjectId(rs.getInt("TC_PRJ_ID"));
						tc.setRecordType(rs.getString("TC_REC_TYPE"));
						tc.setTcReviewed(rs.getString("TC_REVIEWED"));
						tc.setTcPriority(rs.getString("TC_PRIORITY"));
						tc.setStorage(rs.getString("TC_STORAGE"));
						tc.setMode(rs.getString("TC_EXEC_MODE"));

						ArrayList<TestStep> steps = this.hydrateStepsForTestCase(conn, conn, tc.getTcRecId(), tsIds);
						tc.setTcSteps(steps);

						testCases.add(tc);
					}
				} finally {
					DatabaseHelper.close(rs);
				}
			}
		} catch (Exception e) {

		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return testCases;
	}
	/* Added by Preeti for TENJINCG-574 ends */
	/*modified by shruthi for VAPT helper fixes ends*/
	
	/*modified by shruthi for VAPT helper fixes starts*/
	public ArrayList<TestCase> hydrateAllTestCases(Connection conn, int projectId) throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<TestCase> testCases = new ArrayList<TestCase>();

		PreparedStatement pst = null;
		ResultSet rs = null;

		try {

			
			pst = conn.prepareStatement("SELECT * FROM TESTCASES WHERE TC_PRJ_ID =? ORDER BY TC_REC_ID");
			pst.setInt(1,projectId);
			rs = pst.executeQuery();
			while (rs.next()) {
				TestCase tc = new TestCase();
				tc.setTcId(rs.getString("tc_id"));
				tc.setTcName(rs.getString("tc_name"));
				tc.setTcType(rs.getString("tc_type"));
				tc.setTcStatus(rs.getString("tc_status"));
				tc.setTcCreatedBy(rs.getString("tc_created_by"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
				/* tc.setTcCreatedOn(rs.getDate("tc_created_on")); */
				tc.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				tc.setTcModifiedBy(rs.getString("tc_modified_by"));
				tc.setTcModifiedOn(rs.getDate("tc_modified_on"));
				tc.setTcDesc(rs.getString("tc_desc"));
				tc.setTcRecId(rs.getInt("TC_REC_ID"));
				tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
				tc.setProjectId(rs.getInt("TC_PRJ_ID"));
				tc.setRecordType(rs.getString("TC_REC_TYPE"));
				tc.setTcReviewed(rs.getString("TC_REVIEWED"));
				tc.setTcPriority(rs.getString("TC_PRIORITY"));
				/* changed by manish for req TENJINCG-32 on 11-Jan-2017 starts */
				tc.setStorage(rs.getString("TC_STORAGE"));
				/* changed by manish for req TENJINCG-32 on 11-Jan-2017 ends */
				/* Added by Roshni for requirement TENJINCG-192 */
				tc.setMode(rs.getString("TC_EXEC_MODE"));
				/* Added by Roshni for TENJINCG-192 */
				/*Added by Poojalakshmi for TJN27-94 starts */
				tc.setAppId(rs.getInt("TC_APP_ID"));
				  try{
						
						String AppName = new AutHelper().getAutName(conn, tc.getAppId());
						tc.setTcAppName(AppName);
					}catch(Exception e){
						tc.setTcAppName(Integer.toString(tc.getAppId()));
					}
				  /*Added by Poojalakshmi for TJN27-94 ends */
				testCases.add(tc);
			}

		} catch (Exception e) {
			logger.error("ERROR fetching all test cases", e);
			throw new DatabaseException("Could not retrieve test cases due to an internal error");
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return testCases;
	}/*modified by shruthi for VAPT helper fixes ends*/

	public ArrayList<TestStep> hydrateStepsForTestCase(Connection conn, Connection appConn, int testCaseRecordId)
			throws DatabaseException {
		PreparedStatement pst = null;
		ResultSet rs = null;

		ArrayList<TestStep> steps = null;

		try {
			pst = conn.prepareStatement(
					"SELECT A.*, B.EXEC_STATUS FROM TESTSTEPS A LEFT JOIN TC_TSTEP_DEPENDENCY_RULES B ON B.TSTEP_REC_ID=A.TSTEP_REC_ID AND B.TC_REC_ID=A.TC_REC_ID  WHERE A.TC_REC_ID = ? ORDER BY A.TSTEP_REC_ID ");
			/* Modified by Padmavathi for TJN252-62 ends */
			pst.setInt(1, testCaseRecordId);
			rs = pst.executeQuery();

			steps = new ArrayList<TestStep>();

			Map<Integer, String> autMap = new HashMap<Integer, String>();

			while (rs.next()) {

				TestStep t = new TestStep();
				t.setRecordId(rs.getInt("TSTEP_REC_ID"));
				t.setId(rs.getString("TSTEP_ID"));
				t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
				String appname = autMap.get(t.getAppId());
				if (appname != null && !appname.equalsIgnoreCase("")) {
					t.setAppName(appname);
				} else {
					appname = getappname(appConn, rs.getInt("APP_ID"));
					t.setAppName(appname);
				}
				t.setModuleCode(rs.getString("FUNC_CODE"));
				t.setType(rs.getString("TSTEP_TYPE"));
				t.setDataId(rs.getString("TSTEP_DATA_ID"));
				t.setDescription(rs.getString("TSTEP_DESC"));
				t.setExpectedResult(rs.getString("TSTEP_EXP_RESULT"));

				t.setOperation(rs.getString("TSTEP_OPERATION"));
				/*Added by Ashiki for TENJINCG-1275 starts*/
				t.setFileName(rs.getString("TSTEP_FILENAME"));
				/*Added by Ashiki for TENJINCG-1275 ends*/
				t.setAutLoginType(rs.getString("TSTEP_AUT_LOGIN_TYPE"));

				t.setRowsToExecute(rs.getInt("TSTEP_ROWS_TO_EXEC"));
				t.setTstepStorage(rs.getString("TSTEP_STORAGE"));
				/* changed by manish for req TENJINCG-33 on 11-Jan-2017 ends */
				String mode = rs.getString("TXNMODE");
				/* Modified by Roshni for TENJINCG-305 starts */
				if (mode.equalsIgnoreCase("GUI")) {
					mode = "GUI";
				} else {
					mode = "API";
					// changes done by parveen, Leelaprasad for #405 starts
					t.setResponseType((rs.getInt("TSTEP_API_RESP_TYPE")));
					// changes done by parveen, Leelaprasad for #405 ends
				}
				/* Modified by Roshni for TENJINCG-305 ends */
				t.setTxnMode(mode);
				/* Added by Padmavathi for TJN252-62 starts */
				t.setRerunStep(rs.getString("TSTEP_RE_RUN"));
				t.setCustomRules(rs.getString("EXEC_STATUS"));
				/* Added by Padmavathi for TJN252-62 ends */
				steps.add(t);

			}

			if (steps == null || steps.size() == 0) {
				System.err.println();
			}
		} catch (Exception e) {
			logger.error("ERROR fetching steps under test case", e);
			throw new DatabaseException("Could not fetch steps under this test case due to an internal error", e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);

		}

		return steps;
	}

	/* Added by Preeti for TENJINCG-574 starts */
	public ArrayList<TestStep> hydrateStepsForTestCase(Connection conn, Connection appConn, int testCaseRecordId,
			String[] tsIds) throws DatabaseException {
		PreparedStatement pst = null;
		ResultSet rs = null;

		ArrayList<TestStep> steps = null;

		try {
			steps = new ArrayList<TestStep>();
			for (int i = 0; i < tsIds.length; i++) {
				try {
					pst = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID = ? AND TSTEP_REC_ID=?");
					pst.setInt(1, testCaseRecordId);
					pst.setString(2, tsIds[i]);
					rs = pst.executeQuery();

					Map<Integer, String> autMap = new HashMap<Integer, String>();

					while (rs.next()) {

						TestStep t = new TestStep();
						t.setRecordId(rs.getInt("TSTEP_REC_ID"));
						t.setId(rs.getString("TSTEP_ID"));
						t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
						String appname = autMap.get(t.getAppId());
						if (appname != null && !appname.equalsIgnoreCase("")) {
							t.setAppName(appname);
						} else {
							appname = getappname(appConn, rs.getInt("APP_ID"));
							t.setAppName(appname);
						}
						/* Added by Padmavathi for TENJINCG-673 starts */
						t.setAppId(rs.getInt("APP_ID"));
						/* Added by Padmavathi for TENJINCG-673 ends */
						t.setModuleCode(rs.getString("FUNC_CODE"));
						t.setType(rs.getString("TSTEP_TYPE"));
						t.setDataId(rs.getString("TSTEP_DATA_ID"));
						t.setDescription(rs.getString("TSTEP_DESC"));
						t.setExpectedResult(rs.getString("TSTEP_EXP_RESULT"));
						t.setOperation(rs.getString("TSTEP_OPERATION"));
						t.setAutLoginType(rs.getString("TSTEP_AUT_LOGIN_TYPE"));
						t.setRowsToExecute(rs.getInt("TSTEP_ROWS_TO_EXEC"));
						t.setTstepStorage(rs.getString("TSTEP_STORAGE"));
						String mode = rs.getString("TXNMODE");
						/*Added by Ashiki for TENJINCG-1275 starts*/
						t.setFileName(rs.getString("TSTEP_FILENAME"));
						/*Added by Ashiki for TENJINCG-1275 ends*/
						if (mode.equalsIgnoreCase("GUI")) {
							mode = "GUI";
						} else {
							mode = "API";
							t.setResponseType((rs.getInt("TSTEP_API_RESP_TYPE")));
						}
						t.setTxnMode(mode);
						steps.add(t);

					}

					if (steps == null || steps.size() == 0) {
						System.err.println();
					}
				} finally {
					DatabaseHelper.close(rs);
					DatabaseHelper.close(pst);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR fetching steps under test case", e);
			throw new DatabaseException("Could not fetch steps under this test case due to an internal error", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);

		}

		return steps;
	}
	/* Added by Preeti for TENJINCG-574 ends */

	/* Added by Pushpalatha for def#T25IT-165 starts */
	public TestStep hydrateTestStepDetailsForTestCase(int projectId, int testStepRecordId, int tcRecId)
			throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		TestStep t = null;

		try {
			/* Changed by Gangadhar Badagi for TENJINCG-506 starts */
			
			pst = conn.prepareStatement(
					"SELECT A.*, B.TC_ID, B.TC_NAME, C.APP_NAME FROM TESTSTEPS A, TESTCASES B, MASAPPLICATION C WHERE C.APP_ID = A.APP_ID AND A.TSTEP_REC_ID = ? AND B.TC_REC_ID = ?");
			/* Changed by Gangadhar Badagi for TENJINCG-506 ends */
			pst.setInt(1, testStepRecordId);
			pst.setInt(2, tcRecId);
			rs = pst.executeQuery();

			while (rs.next()) {
				t = new TestStep();
				t.setRecordId(rs.getInt("tstep_rec_id"));
				t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
				t.setDataId(rs.getString("TSTEP_DATA_ID"));
				t.setType(rs.getString("TSTEP_TYPE"));
				t.setDescription(rs.getString("TSTEP_DESC"));
				t.setExpectedResult(rs.getString("TSTEP_EXP_RESULT"));
				t.setAppId(rs.getInt("APP_ID"));
				t.setModuleCode(rs.getString("FUNC_CODE"));
				t.setId(rs.getString("TSTEP_ID"));
				t.setTestCaseRecordId(rs.getInt("TC_REC_ID"));
				t.setOperation(rs.getString("TSTEP_OPERATION"));
				t.setAutLoginType(rs.getString("TSTEP_AUT_LOGIN_TYPE"));
				t.setRowsToExecute(rs.getInt("TSTEP_ROWS_TO_EXEC"));
				t.setTstepStorage(rs.getString("TSTEP_STORAGE"));
				t.setParentTestCaseId(rs.getString("TC_ID"));
				t.setTestCaseName(rs.getString("TC_NAME"));
				t.setAppName(rs.getString("APP_NAME"));
				// changes done by parveen, Leelaprasad for #405 starts
				t.setResponseType((rs.getInt("TSTEP_API_RESP_TYPE")));

				t.setTxnMode(rs.getString("TXNMODE"));
				/* added by manish to get validation type for test step on 21-Nov-2017 starts */
				t.setValidationType(rs.getString("TSTEP_VAL_TYPE"));
				/* added by manish to get validation type for test step on 21-Nov-2017 ends */
				/*Added by Ashiki for TENJINCG-1275 starts*/
				t.setFileName(rs.getString("TSTEP_FILENAME"));
				/*Added by Ashiki for TENJINCG-1275 ends*/
			}

			if (t != null) {
				p = conn.prepareStatement("SELECT * FROM RESVALMAP WHERE TSTEP_REC_ID = ?");
				p.setInt(1, t.getRecordId());

				ArrayList<ResultValidation> vals = new ArrayList<ResultValidation>();
				rs2 = p.executeQuery();
				while (rs2.next()) {
					int r = rs2.getInt("RES_VAL_ID");
					ResultValidation val = new ResultsHelper().hydrateResultValidation(r);
					vals.add(val);
				}

				t.setValidations(vals);
			}
		} catch (Exception e) {
			throw new DatabaseException("Could not hydrate Test Step information", e);
		} finally {

			DatabaseHelper.close(rs2);
			DatabaseHelper.close(p);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return t;
	}
	/* Added by Pushpalatha for def#T25IT-165 ends */

	public boolean doesTestCaseExist(Connection conn, int projectId, String testCaseId) throws DatabaseException {
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		boolean exists = false;

		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			pst = conn.prepareStatement("SELECT COUNT(*) AS CNT FROM TESTCASES WHERE TC_PRJ_ID = ? AND TC_ID = ?");
			pst.setInt(1, projectId);
			pst.setString(2, testCaseId);

			rs = pst.executeQuery();

			while (rs.next()) {
				if (rs.getInt("CNT") > 0) {
					exists = true;
				}
			}
		} catch (Exception e) {
			logger.error("Could not check if test case " + testCaseId + " already exists", e);
			throw new DatabaseException("Could not check if this test case exists");
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return exists;
	}

	/****************************************************************
	 * Fix for Bug 2326 in Tenjin v2.2 (Sriram) (16-10-2015) starts (Added TCRECID
	 * as argument)
	 */
	/*
	 * public boolean doesTestStepExist(Connection conn, int projectId, String
	 * testStepId) throws DatabaseException{
	 */
	public boolean doesTestStepExist(Connection conn, int projectId, String testStepId, int tcRecId)
			throws DatabaseException {
		/****************************************************************
		 * Fix for Bug 2326 in Tenjin v2.2 (Sriram) (16-10-2015) ends
		 */
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		boolean exists = false;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("SELECT COUNT(*) AS CNT FROM TESTSTEPS WHERE TSTEP_ID = ? AND TC_REC_ID=?");

			pst.setString(1, testStepId);
			pst.setInt(2, tcRecId);
			rs = pst.executeQuery();

			while (rs.next()) {
				if (rs.getInt("CNT") > 0) {
					exists = true;
				}
			}
		} catch (Exception e) {
			logger.error("Could not check if test case " + testStepId + " already exists", e);
			throw new DatabaseException("Could not check if this test case exists");
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return exists;
	}

	public TestCase hydrateTestCase(int projectId, String testCaseId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		PreparedStatement pst1= null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		TestCase tc = null;

		try {
			
			pst= conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_ID =? AND TC_PRJ_ID =?");
			pst.setString(1,testCaseId);
			pst.setInt(2,projectId);
			rs = pst.executeQuery();

			while (rs.next()) {
				tc = new TestCase();
				tc.setTcId(rs.getString("tc_id"));
				tc.setTcName(rs.getString("tc_name"));
				tc.setTcType(rs.getString("tc_type"));
				tc.setTcStatus(rs.getString("tc_status"));
				tc.setTcCreatedBy(rs.getString("tc_created_by"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
				/* tc.setTcCreatedOn(rs.getDate("tc_created_on")); */
				tc.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				tc.setTcModifiedBy(rs.getString("tc_modified_by"));
				tc.setTcModifiedOn(rs.getDate("tc_modified_on"));
				tc.setTcDesc(rs.getString("tc_desc"));
				tc.setTcRecId(rs.getInt("TC_REC_ID"));
				tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
				tc.setProjectId(rs.getInt("TC_PRJ_ID"));
				tc.setRecordType(rs.getString("TC_REC_TYPE"));
				tc.setTcReviewed(rs.getString("TC_REVIEWED"));
				tc.setTcPriority(rs.getString("TC_PRIORITY"));
				/* changed by manish for req TENJINCG-32 on 11-Jan-2017 starts */
				tc.setStorage(rs.getString("TC_STORAGE"));
				/* changed by manish for req TENJINCG-32 on 11-Jan-2017 ends */
				/* Added by Gangadhar Badagi for T25IT-64 starts */
				tc.setMode(rs.getString("TC_EXEC_MODE"));
				/* Added by Gangadhar Badagi for T25IT-64 ends */
			}

			if (tc != null) {

				ArrayList<TestStep> tsteps = new ArrayList<TestStep>();
				
				pst1 = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID =? ORDER BY TSTEP_REC_ID");
				pst1.setInt(1,tc.getTcRecId());
				
				rs1 = pst1.executeQuery();

				while (rs1.next()) {
					TestStep step = new TestStep();
					/*
					 * Change by Nagababu:for Defect:#1341 Add New Column AUT Login Type for
					 * TestCases and TestSteps Upload on 27-0-2015: Ends
					 */
					step.setRecordId(rs1.getInt("TSTEP_REC_ID"));
					step.setTestCaseRecordId(rs1.getInt("TC_REC_ID"));
					step.setShortDescription(rs1.getString("TSTEP_SHT_DESC"));
					step.setDataId(rs1.getString("TSTEP_DATA_ID"));
					step.setType(rs1.getString("TSTEP_TYPE"));
					step.setDescription(rs1.getString("TSTEP_DESC"));
					step.setExpectedResult(rs1.getString("tstep_exp_result"));
					step.setActualResult(rs1.getString("tstep_act_result"));
					step.setAppId(rs1.getInt("APP_ID"));
					step.setModuleCode(rs1.getString("FUNC_CODE"));
					step.setTransactionContext(rs1.getString("TXN_CONTEXT"));
					step.setExecutionContext(rs1.getString("EXEC_CONTEXT"));
					step.setId(rs1.getString("TSTEP_ID"));
					step.setOperation(rs1.getString("TSTEP_OPERATION"));
					step.setAutLoginType(rs1.getString("TSTEP_AUT_LOGIN_TYPE"));
					step.setTstepStorage(rs1.getString("TSTEP_STORAGE"));

					step.setResponseType((rs1.getInt("TSTEP_API_RESP_TYPE")));

					tsteps.add(step);
				}
				tc.setTcSteps(tsteps);
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch test case details", e);
		} finally {

			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return tc;
	}	/*modified by shruthi for VAPT helper fixes ends*/

	/*modified by shruthi for VAPT helper fixes starts*/
	/* Added by Gangadhar Badagi for TENJINCG-292 starts */
	public TestCase hydrateTestCaseByName(int projectId, String testCaseName) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		PreparedStatement pst1= null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		TestCase tc = null;

		try {
			
			pst= conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_NAME =? AND TC_PRJ_ID =?");
			pst.setString(1,testCaseName);
			pst.setInt(2,projectId);
			rs = pst.executeQuery();

			while (rs.next()) {
				tc = new TestCase();
				tc.setTcId(rs.getString("tc_id"));
				tc.setTcName(rs.getString("tc_name"));
				tc.setTcType(rs.getString("tc_type"));
				tc.setTcStatus(rs.getString("tc_status"));
				tc.setTcCreatedBy(rs.getString("tc_created_by"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
				/* tc.setTcCreatedOn(rs.getDate("tc_created_on")); */
				tc.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				tc.setTcModifiedBy(rs.getString("tc_modified_by"));
				tc.setTcModifiedOn(rs.getDate("tc_modified_on"));
				tc.setTcDesc(rs.getString("tc_desc"));
				tc.setTcRecId(rs.getInt("TC_REC_ID"));
				tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
				tc.setProjectId(rs.getInt("TC_PRJ_ID"));
				tc.setRecordType(rs.getString("TC_REC_TYPE"));
				tc.setTcReviewed(rs.getString("TC_REVIEWED"));
				tc.setTcPriority(rs.getString("TC_PRIORITY"));
				tc.setStorage(rs.getString("TC_STORAGE"));

			}

			if (tc != null) {

				ArrayList<TestStep> tsteps = new ArrayList<TestStep>();
				
				pst1 = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID =? ORDER BY TSTEP_REC_ID");
				pst1.setInt(1,tc.getTcRecId());
				rs1 = pst1.executeQuery();

				while (rs1.next()) {
					TestStep step = new TestStep();
					step.setRecordId(rs1.getInt("TSTEP_REC_ID"));
					step.setTestCaseRecordId(rs1.getInt("TC_REC_ID"));
					step.setShortDescription(rs1.getString("TSTEP_SHT_DESC"));
					step.setDataId(rs1.getString("TSTEP_DATA_ID"));
					step.setType(rs1.getString("TSTEP_TYPE"));
					step.setDescription(rs1.getString("TSTEP_DESC"));
					step.setExpectedResult(rs1.getString("tstep_exp_result"));
					step.setActualResult(rs1.getString("tstep_act_result"));
					step.setAppId(rs1.getInt("APP_ID"));
					step.setModuleCode(rs1.getString("FUNC_CODE"));
					step.setTransactionContext(rs1.getString("TXN_CONTEXT"));
					step.setExecutionContext(rs1.getString("EXEC_CONTEXT"));
					step.setId(rs1.getString("TSTEP_ID"));
					step.setOperation(rs1.getString("TSTEP_OPERATION"));
					step.setAutLoginType(rs1.getString("TSTEP_AUT_LOGIN_TYPE"));
					step.setTstepStorage(rs1.getString("TSTEP_STORAGE"));
					step.setResponseType((rs1.getInt("TSTEP_API_RESP_TYPE")));
					tsteps.add(step);
				}
				tc.setTcSteps(tsteps);
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch test case details", e);
		} finally {

			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return tc;
	}/*modified by shruthi for VAPT helper fixes ends*/
	/* Added by Gangadhar Badagi for TENJINCG-292 ends */
	
	
	/*modified by shruthi for VAPT helper fixes starts*/
 /* modified by Roshni for TENJINCG-1168 starts */
	
	public TestCase hydrateTestCaseBasicDetails(Connection conn, int projectId, int recordId,
			Map<Integer, String> passedStepMap) throws DatabaseException {
			 /* modified by Roshni for TENJINCG-1168 ends */
		/* Modified by Padmavathi for TENJINCG-911 ends */
		
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		TestCase tc = null;

		try {
			pst = conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_REC_ID = ? AND TC_PRJ_ID = ?");
			pst.setInt(1, recordId);
			pst.setInt(2, projectId);
			rs = pst.executeQuery();

			while (rs.next()) {
				tc = new TestCase();
				tc.setTcId(rs.getString("tc_id"));
				tc.setTcName(rs.getString("tc_name"));
				tc.setTcType(rs.getString("tc_type"));
				tc.setTcStatus(rs.getString("tc_status"));
				tc.setTcCreatedBy(rs.getString("tc_created_by"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
				/* tc.setTcCreatedOn(rs.getDate("tc_created_on")); */
				tc.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				tc.setTcModifiedBy(rs.getString("tc_modified_by"));
				tc.setTcModifiedOn(rs.getDate("tc_modified_on"));
				tc.setTcDesc(rs.getString("tc_desc"));
				tc.setTcRecId(rs.getInt("TC_REC_ID"));
				tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
				tc.setProjectId(rs.getInt("TC_PRJ_ID"));
				tc.setRecordType(rs.getString("TC_REC_TYPE"));
				tc.setTcReviewed(rs.getString("TC_REVIEWED"));
				tc.setTcPriority(rs.getString("TC_PRIORITY"));

				/*
				 * changed by manish for req TENJINCG-32 on 11-Jan-2017 starts
				 */
				tc.setStorage(rs.getString("TC_STORAGE"));
				/* changed by manish for req TENJINCG-32 on 11-Jan-2017 ends */

				// Added by sakthi on 07-04-2017 starts
				tc.setMode(rs.getString("TC_EXEC_MODE"));
				// Added by sakthi on 07-04-2017 ends

			}

			if (tc != null) {

				ArrayList<TestStep> tsteps = new ArrayList<TestStep>();
				
				pst1 = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID =? ORDER BY TSTEP_EXEC_SEQ");
				pst1.setInt(1,tc.getTcRecId());
				rs1= pst1.executeQuery();
				/* Changed by Padmavathi for TCGST-11 ends */
				while (rs1.next()) {
					/* Added by Padmavathi for TENJINCG-911 starts */
					boolean flag = false;
					if (passedStepMap != null && passedStepMap.get(rs1.getInt("TSTEP_REC_ID")) != null) {
						if (rs1.getString("TSTEP_RE_RUN") == null
								|| !rs1.getString("TSTEP_RE_RUN").equalsIgnoreCase("Y")) {
							flag = true;
						}
					}
					/* Added by Padmavathi for TENJINCG-911 ends */
					TestStep step = new TestStep();
					step.setRecordId(rs1.getInt("TSTEP_REC_ID"));
					step.setTestCaseRecordId(rs1.getInt("TC_REC_ID"));
					step.setShortDescription(rs1.getString("TSTEP_SHT_DESC"));
					step.setDataId(rs1.getString("TSTEP_DATA_ID"));
					step.setType(rs1.getString("TSTEP_TYPE"));
					step.setDescription(rs1.getString("TSTEP_DESC"));
					step.setExpectedResult(rs1.getString("tstep_exp_result"));
					step.setActualResult(rs1.getString("tstep_act_result"));
					step.setAppId(rs1.getInt("APP_ID"));
					step.setModuleCode(rs1.getString("FUNC_CODE"));
					step.setTransactionContext(rs1.getString("TXN_CONTEXT"));
					step.setExecutionContext(rs1.getString("EXEC_CONTEXT"));
					step.setTxnMode(rs1.getString("TXNMODE")); /* 20-Oct-2014 WS */
					step.setId(rs1.getString("TSTEP_ID"));
					step.setResponseType((rs1.getInt("TSTEP_API_RESP_TYPE")));
					/* Added by Padmavathi for TCGST-11 starts */
					step.setSequence(rs1.getInt("TSTEP_EXEC_SEQ"));
					/* Added by Padmavathi for TCGST-11 ends */
					/*******************
					 * Fix by Sriram to add Test Step Operation attribute
					 */
					step.setOperation(rs1.getString("TSTEP_OPERATION"));
					/*******************
					 * Fix by Sriram to add Test Step Operation attribute Ends
					 */

					/**************************************************************************
					 * Fix by Sriram to add AUT Login Type Attribute (1-Dec-2014)
					 */
					step.setAutLoginType(rs1.getString("TSTEP_AUT_LOGIN_TYPE"));
					/**************************************************************************
					 * Fix by Sriram to add AUT Login Type Attribute (1-Dec-2014) ends
					 */

					/*
					 * changed by manish for req TENJINCG-33 on 11-Jan-2017 starts
					 */
					step.setTstepStorage(rs1.getString("TSTEP_STORAGE"));
					/*
					 * changed by manish for req TENJINCG-33 on 11-Jan-2017 ends
					 */

					
					step.setRowsToExecute(rs1.getInt("TSTEP_ROWS_TO_EXEC"));

					try {
						
						String autName = new AutHelper().getAutName(conn, step.getAppId());
						step.setAppName(autName);
					} catch (Exception e) {
						step.setAppName(Integer.toString(step.getAppId()));
					}
					/* Added by Padmavathi for TENJINCG-647 starts */
					try {
						DependencyRules rules = new DependencyRulesHandler().getDependencyRules(step.getRecordId());
						if (rules != null) {
							step.setCustomRules(rules.getExecStatus());
							step.setDependentStepRecId(rules.getDependentStepRecId());
						}
					} catch (DatabaseException e) {
						logger.error(e.getMessage());
					}
					/* Added by Padmavathi for TENJINCG-647 ends */
					if (!flag) {
						tsteps.add(step);
					}

				}
				tc.setTcSteps(tsteps);

			}
		} catch (SQLException e) {
			logger.error(
					"Error in hydrateTestCaseBasicDetails(Connection projConn, Connection appConn, int projectId, int recordId)");
			logger.error("Could not fetch details for test case with Record ID " + recordId, e);
			throw new DatabaseException("Could not fetch Test Case Details", e);
		} finally {
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return tc;
	}/*modified by shruthi for VAPT helper fixes ends*/
	/*modified by shruthi for VAPT helper fixes starts*/

	public TestCase hydrateTestCase(int projectId, int recordId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		TestCase tc = null;

		try {
			pst = conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_REC_ID =? AND TC_PRJ_ID =?");
			pst.setInt(1, recordId);
			pst.setInt(2, projectId);
			rs = pst.executeQuery();

			while (rs.next()) {
				tc = new TestCase();
				tc.setTcId(rs.getString("tc_id"));
				tc.setTcName(rs.getString("tc_name"));
				tc.setTcType(rs.getString("tc_type"));
				tc.setTcStatus(rs.getString("tc_status"));
				tc.setTcCreatedBy(rs.getString("tc_created_by"));
				tc.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				tc.setTcModifiedBy(rs.getString("tc_modified_by"));
				tc.setTcModifiedOn(rs.getDate("tc_modified_on"));
				tc.setTcDesc(rs.getString("tc_desc"));
				tc.setTcRecId(rs.getInt("TC_REC_ID"));
				tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
				tc.setProjectId(rs.getInt("TC_PRJ_ID"));
				tc.setRecordType(rs.getString("TC_REC_TYPE"));
				tc.setTcReviewed(rs.getString("TC_REVIEWED"));
				tc.setTcPriority(rs.getString("TC_PRIORITY"));
				/* changed by manish for req TENJINCG-32 on 11-Jan-2017 starts */
				tc.setStorage(rs.getString("TC_STORAGE"));
				tc.setMode(rs.getString("TC_EXEC_MODE"));
				/* Added by Sriram for TENJINCG-171 ends */
			}

			if (tc != null) {

				ArrayList<TestStep> tsteps = new ArrayList<TestStep>();
				pst1= conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID =? ORDER BY TSTEP_EXEC_SEQ");
				pst1.setInt(1, tc.getTcRecId());
				rs1 = pst1.executeQuery();
				while (rs1.next()) {
					TestStep step = new TestStep();
					step.setRecordId(rs1.getInt("TSTEP_REC_ID"));
					step.setTestCaseRecordId(rs1.getInt("TC_REC_ID"));
					step.setShortDescription(rs1.getString("TSTEP_SHT_DESC"));
					step.setDataId(rs1.getString("TSTEP_DATA_ID"));
					step.setType(rs1.getString("TSTEP_TYPE"));
					step.setDescription(rs1.getString("TSTEP_DESC"));
					step.setExpectedResult(rs1.getString("tstep_exp_result"));
					step.setActualResult(rs1.getString("tstep_act_result"));
					step.setAppId(rs1.getInt("APP_ID"));
					step.setModuleCode(rs1.getString("FUNC_CODE"));
					step.setTransactionContext(rs1.getString("TXN_CONTEXT"));
					step.setExecutionContext(rs1.getString("EXEC_CONTEXT"));
					step.setTxnMode(rs1.getString("TXNMODE")); /* 20-Oct-2014 WS */
					step.setId(rs1.getString("TSTEP_ID"));
					step.setOperation(rs1.getString("TSTEP_OPERATION"));

					step.setAutLoginType(rs1.getString("TSTEP_AUT_LOGIN_TYPE"));
					step.setRowsToExecute(rs1.getInt("TSTEP_ROWS_TO_EXEC"));

					/* changed by manish for req TENJINCG-33 on 11-Jan-2017 starts */
					step.setTstepStorage(rs1.getString("TSTEP_STORAGE"));
					/* changed by manish for req TENJINCG-33 on 11-Jan-2017 ends */

					step.setResponseType((rs1.getInt("TSTEP_API_RESP_TYPE")));

					step.setValidationType(rs1.getString("TSTEP_VAL_TYPE"));

					ArrayList<ResultValidation> val = null;
					try {
					 /* modified by Roshni for TENJINCG-1168 starts */
						val = this.hydrateValidationsForStep(step.getRecordId(),conn);
					 /* modified by Roshni for TENJINCG-1168 ends */
						step.setValidations(val);
					} catch (Exception e) {
						
					}

					try {
						Aut a = new AutHelper().hydrateAut(step.getAppId());
						step.setAppName(a.getName());
					} catch (Exception e) {
						step.setAppName(Integer.toString(step.getAppId()));
					}

					tsteps.add(step);
				}
				tc.setTcSteps(tsteps);
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch test case details", e);
		} finally {

			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return tc;
	}/*modified by shruthi for VAPT helper fixes ends*/
	
	/*modified by shruthi for VAPT helper fixes starts*/

	/* added by Roshni for TENJINCG-1168 starts */
	public TestCase hydrateTestCase(int projectId, int recordId,Connection conn) throws DatabaseException {
		
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		TestCase tc = null;

		try {
			
			pst = conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_REC_ID =? AND TC_PRJ_ID =?");
			pst.setInt(1, recordId);
			pst.setInt(2, projectId);
			rs = pst.executeQuery();

			while (rs.next()) {
				tc = new TestCase();
				tc.setTcId(rs.getString("tc_id"));
				tc.setTcName(rs.getString("tc_name"));
				tc.setTcType(rs.getString("tc_type"));
				tc.setTcStatus(rs.getString("tc_status"));
				tc.setTcCreatedBy(rs.getString("tc_created_by"));
				tc.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
				tc.setTcModifiedBy(rs.getString("tc_modified_by"));
				tc.setTcModifiedOn(rs.getDate("tc_modified_on"));
				tc.setTcDesc(rs.getString("tc_desc"));
				tc.setTcRecId(rs.getInt("TC_REC_ID"));
				tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
				tc.setProjectId(rs.getInt("TC_PRJ_ID"));
				tc.setRecordType(rs.getString("TC_REC_TYPE"));
				tc.setTcReviewed(rs.getString("TC_REVIEWED"));
				tc.setTcPriority(rs.getString("TC_PRIORITY"));
				tc.setStorage(rs.getString("TC_STORAGE"));
				tc.setMode(rs.getString("TC_EXEC_MODE"));
			}

			if (tc != null) {

				ArrayList<TestStep> tsteps = new ArrayList<TestStep>();
				pst1 = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID =? ORDER BY TSTEP_EXEC_SEQ");
				pst1.setInt(1, tc.getTcRecId());
				rs1 = pst1.executeQuery();
				while (rs1.next()) {
					TestStep step = new TestStep();
					step.setRecordId(rs1.getInt("TSTEP_REC_ID"));
					step.setTestCaseRecordId(rs1.getInt("TC_REC_ID"));
					step.setShortDescription(rs1.getString("TSTEP_SHT_DESC"));
					step.setDataId(rs1.getString("TSTEP_DATA_ID"));
					step.setType(rs1.getString("TSTEP_TYPE"));
					step.setDescription(rs1.getString("TSTEP_DESC"));
					step.setExpectedResult(rs1.getString("tstep_exp_result"));
					step.setActualResult(rs1.getString("tstep_act_result"));
					step.setAppId(rs1.getInt("APP_ID"));
					step.setModuleCode(rs1.getString("FUNC_CODE"));
					step.setTransactionContext(rs1.getString("TXN_CONTEXT"));
					step.setExecutionContext(rs1.getString("EXEC_CONTEXT"));
					step.setTxnMode(rs1.getString("TXNMODE"));
					step.setId(rs1.getString("TSTEP_ID"));
					step.setOperation(rs1.getString("TSTEP_OPERATION"));

					step.setAutLoginType(rs1.getString("TSTEP_AUT_LOGIN_TYPE"));
					step.setRowsToExecute(rs1.getInt("TSTEP_ROWS_TO_EXEC"));

					step.setTstepStorage(rs1.getString("TSTEP_STORAGE"));

					step.setResponseType((rs1.getInt("TSTEP_API_RESP_TYPE")));

					step.setValidationType(rs1.getString("TSTEP_VAL_TYPE"));

					ArrayList<ResultValidation> val = null;
					/*Added by Prem for TENJINCG-1181*/
					ArrayList<ValidationDetails> valDetails = null;
					/*Added by Prem for TENJINCG-1181*/
					try {
						val = this.hydrateValidationsForStep(step.getRecordId(),conn);
						step.setValidations(val);
						/*Added by Prem for TENJINCG-1181*/
						valDetails =helper.hydrateUnstructuredData(step.getRecordId());
						step.setValidationdetails(valDetails);
						/*Added by Prem for TENJINCG-1181 end*/
					} catch (Exception e) {
					}

					try {
						Aut a = new AutHelper().hydrateAut(conn,step.getAppId());
						step.setAppName(a.getName());
					} catch (Exception e) {
						step.setAppName(Integer.toString(step.getAppId()));
					}
					
					tsteps.add(step);
				}
				tc.setTcSteps(tsteps);
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch test case details", e);
		} finally {

			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return tc;
	}
	/* added by Roshni for TENJINCG-1168 ends */
	/*modified by shruthi for VAPT helper fixes ends*/
	/****************************************************************************
	 * Added by Sriram for Req#TJN_243_11 (20-Dec-2016)
	 */
	/*modified by shruthi for VAPT helper fixes starts*/
	 /* modified by Roshni for TENJINCG-1168 starts */
	public TestCase hydrateTestCaseForStep(int projectId, int testStepRecordId,Connection conn) throws DatabaseException {
 /* modified by Roshni for TENJINCG-1168 ends */
		
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		PreparedStatement pst1= null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		TestCase tc = null;

		try {
			pst = conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_REC_ID = (SELECT TC_REC_ID FROM TESTSTEPS WHERE TSTEP_REC_ID=?) AND TC_PRJ_ID =?");
			pst.setInt(1, testStepRecordId);
			pst.setInt(2, projectId);
			rs = pst.executeQuery();

			while (rs.next()) {
				tc = new TestCase();
				tc.setTcId(rs.getString("tc_id"));
				tc.setTcName(rs.getString("tc_name"));
				tc.setTcType(rs.getString("tc_type"));
				tc.setTcStatus(rs.getString("tc_status"));
				tc.setTcCreatedBy(rs.getString("tc_created_by"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
				tc.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				tc.setTcModifiedBy(rs.getString("tc_modified_by"));
				tc.setTcModifiedOn(rs.getDate("tc_modified_on"));
				tc.setTcDesc(rs.getString("tc_desc"));
				tc.setTcRecId(rs.getInt("TC_REC_ID"));
				tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
				tc.setProjectId(rs.getInt("TC_PRJ_ID"));
				tc.setRecordType(rs.getString("TC_REC_TYPE"));
				tc.setTcReviewed(rs.getString("TC_REVIEWED"));
				tc.setTcPriority(rs.getString("TC_PRIORITY"));
				/* changed by manish for req TENJINCG-32 on 11-Jan-2017 starts */
				tc.setStorage(rs.getString("TC_STORAGE"));
				/* changed by manish for req TENJINCG-32 on 11-Jan-2017 ends */

				/* Fix for T25IT-139 */
				tc.setMode(rs.getString("TC_EXEC_MODE"));
				/* Fix for T25IT-139 ends */
			}

			if (tc != null) {

				ArrayList<TestStep> tsteps = new ArrayList<TestStep>();
				pst1 = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID =? ORDER BY TSTEP_REC_ID");
				pst1.setInt(1, tc.getTcRecId());
				rs1 = pst1.executeQuery();

				while (rs1.next()) {
					TestStep step = new TestStep();
					step.setRecordId(rs1.getInt("TSTEP_REC_ID"));

					if (step.getRecordId() < testStepRecordId || step.getRecordId() > testStepRecordId) {
						continue;
					}

					step.setTestCaseRecordId(rs1.getInt("TC_REC_ID"));
					step.setShortDescription(rs1.getString("TSTEP_SHT_DESC"));
					step.setDataId(rs1.getString("TSTEP_DATA_ID"));
					step.setType(rs1.getString("TSTEP_TYPE"));
					step.setDescription(rs1.getString("TSTEP_DESC"));
					step.setExpectedResult(rs1.getString("tstep_exp_result"));
					step.setActualResult(rs1.getString("tstep_act_result"));
					step.setAppId(rs1.getInt("APP_ID"));
					step.setModuleCode(rs1.getString("FUNC_CODE"));
					step.setTransactionContext(rs1.getString("TXN_CONTEXT"));
					step.setExecutionContext(rs1.getString("EXEC_CONTEXT"));
					step.setTxnMode(rs1.getString("TXNMODE")); /* 20-Oct-2014 WS */
					step.setId(rs1.getString("TSTEP_ID"));

					step.setOperation(rs1.getString("TSTEP_OPERATION"));

					step.setAutLoginType(rs1.getString("TSTEP_AUT_LOGIN_TYPE"));
					step.setRowsToExecute(rs1.getInt("TSTEP_ROWS_TO_EXEC"));
					step.setTstepStorage(rs1.getString("TSTEP_STORAGE"));
					step.setResponseType(rs1.getInt("TSTEP_API_RESP_TYPE"));

					ArrayList<ResultValidation> val = null;
					try {
					 /* modified by Roshni for TENJINCG-1168 starts */
						val = this.hydrateValidationsForStep(step.getRecordId(),conn);
						 /* modified by Roshni for TENJINCG-1168 ends */
						step.setValidations(val);
					} catch (Exception e) {
						
					}

					try {
					 /* modified by Roshni for TENJINCG-1168 starts */
						Aut a = new AutHelper().hydrateAut(conn,step.getAppId());
					 /* modified by Roshni for TENJINCG-1168 ends */	
						step.setAppName(a.getName());
					} catch (Exception e) {
						step.setAppName(Integer.toString(step.getAppId()));
					}
					/*Added by Prem for TENJINCG-1181 start*/
					ArrayList<ValidationDetails> valDeails = null;
						try {
							valDeails =	helper.hydrateUnstructuredData(step.getRecordId());
							step.setValidationdetails(valDeails);
						} catch (Exception e) {
							
						}
						/*Added by Prem for TENJINCG-1181 end*/
					tsteps.add(step);
				}
				tc.setTcSteps(tsteps);
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch test case details", e);
		} finally {

			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);

		}

		return tc;
	}/*modified by shruthi for VAPT helper fixes ends*/
	
	/*modified by shruthi for VAPT helper fixes starts*/

	public TestCase hydrateTestCase(Connection conn, Connection appConn, int projectId, int recordId)
			throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		PreparedStatement pst1= null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		TestCase tc = null;

		try {
			pst = conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_REC_ID =? AND TC_PRJ_ID =?");
			pst.setInt(1, recordId);
			pst.setInt(2,projectId);
			rs = pst.executeQuery();

			while (rs.next()) {
				tc = new TestCase();
				tc.setTcId(rs.getString("tc_id"));
				tc.setTcName(rs.getString("tc_name"));
				tc.setTcType(rs.getString("tc_type"));
				tc.setTcStatus(rs.getString("tc_status"));
				tc.setTcCreatedBy(rs.getString("tc_created_by"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 starts */
				tc.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				tc.setTcModifiedBy(rs.getString("tc_modified_by"));
				tc.setTcModifiedOn(rs.getDate("tc_modified_on"));
				tc.setTcDesc(rs.getString("tc_desc"));
				tc.setTcRecId(rs.getInt("TC_REC_ID"));
				tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
				tc.setProjectId(rs.getInt("TC_PRJ_ID"));
				tc.setRecordType(rs.getString("TC_REC_TYPE"));
				tc.setTcReviewed(rs.getString("TC_REVIEWED"));
				tc.setTcPriority(rs.getString("TC_PRIORITY"));
				// Added by Roshni for requirement TENJINCG-192
				tc.setMode(rs.getString("TC_EXEC_MODE"));
				/* changed by manish for req TENJINCG-32 on 11-Jan-2017 starts */
				tc.setStorage(rs.getString("TC_STORAGE"));
				/* changed by manish for req TENJINCG-32 on 11-Jan-2017 ends */
				/* Added by Padmavathi for TJN252-62 starts */
				tc.setTcPresetRules(rs.getString("TC_PRESET_RULES"));
				/* Added by Padmavathi for TJN252-62 ends */
				/*Added by Poojalakshmi for TJN27-94 starts */
				tc.setAppId(rs.getInt("TC_APP_ID"));
			    try{
					
					String AppName = new AutHelper().getAutName(appConn, tc.getAppId());
					tc.setTcAppName(AppName);
				}catch(Exception e){
					tc.setTcAppName(Integer.toString(tc.getAppId()));
				}
			    /*Added by Poojalakshmi for TJN27-94 ends */
			}

			if (tc != null) {

				ArrayList<TestStep> tsteps = new ArrayList<TestStep>();
				pst1 = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID =? ORDER BY TSTEP_REC_ID");
				pst1.setInt(1, tc.getTcRecId());
				rs1 = pst1.executeQuery();

				while (rs1.next()) {
					TestStep step = new TestStep();
					step.setRecordId(rs1.getInt("TSTEP_REC_ID"));
					step.setTestCaseRecordId(rs1.getInt("TC_REC_ID"));
					step.setShortDescription(rs1.getString("TSTEP_SHT_DESC"));
					step.setDataId(rs1.getString("TSTEP_DATA_ID"));
					step.setType(rs1.getString("TSTEP_TYPE"));
					step.setDescription(rs1.getString("TSTEP_DESC"));
					step.setExpectedResult(rs1.getString("tstep_exp_result"));
					step.setActualResult(rs1.getString("tstep_act_result"));
					step.setAppId(rs1.getInt("APP_ID"));
					step.setModuleCode(rs1.getString("FUNC_CODE"));
					step.setTransactionContext(rs1.getString("TXN_CONTEXT"));
					step.setExecutionContext(rs1.getString("EXEC_CONTEXT"));
					step.setTxnMode(rs1.getString("TXNMODE")); /* 20-Oct-2014 WS */
					step.setId(rs1.getString("TSTEP_ID"));

					step.setOperation(rs1.getString("TSTEP_OPERATION"));

					step.setAutLoginType(rs1.getString("TSTEP_AUT_LOGIN_TYPE"));
					step.setTstepStorage(rs1.getString("TSTEP_STORAGE"));
					/* changed by manish for req TENJINCG-33 on 11-Jan-2017 ends */

					step.setResponseType(rs1.getInt("TSTEP_API_RESP_TYPE"));
					ArrayList<ResultValidation> val = null;
					try {
						val = this.hydrateValidationsForStep(conn, appConn, step.getRecordId());
						step.setValidations(val);
					} catch (Exception e) {
					}

					try {
						String appName = new AutHelper().getAutName(appConn, step.getAppId());
						step.setAppName(appName);
					} catch (Exception e) {
						step.setAppName(Integer.toString(step.getAppId()));
					}

					tsteps.add(step);
				}
				tc.setTcSteps(tsteps);
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch test case details", e);
		} finally {

			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return tc;
	}
	/*modified by shruthi for VAPT helper fixes ends*/
	/*modified by shruthi for VAPT helper fixes starts*/
	public TestCase hydrateTestCaseForRun(Connection conn, Connection appConn, int projectId, int recordId)
			throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		TestCase tc = null;

		try {
			pst = conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_REC_ID =? AND TC_PRJ_ID =?");
			pst.setInt(1, recordId);
			pst.setInt(2, projectId);
			rs = pst.executeQuery();

			while (rs.next()) {
				tc = new TestCase();
				tc.setTcId(rs.getString("tc_id"));
				tc.setTcName(rs.getString("tc_name"));
				tc.setTcType(rs.getString("tc_type"));
				tc.setTcStatus(rs.getString("tc_status"));
				tc.setTcCreatedBy(rs.getString("tc_created_by"));
				tc.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
				/* changed by manish for TENJINCG-330 on 04-Aug-17 ends */
				tc.setTcModifiedBy(rs.getString("tc_modified_by"));
				tc.setTcModifiedOn(rs.getDate("tc_modified_on"));
				tc.setTcDesc(rs.getString("tc_desc"));
				tc.setTcRecId(rs.getInt("TC_REC_ID"));
				tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
				tc.setProjectId(rs.getInt("TC_PRJ_ID"));
				tc.setRecordType(rs.getString("TC_REC_TYPE"));
				tc.setTcReviewed(rs.getString("TC_REVIEWED"));
				tc.setTcPriority(rs.getString("TC_PRIORITY"));
				/* changed by manish for req TENJINCG-32 on 11-Jan-2017 starts */
				tc.setStorage(rs.getString("TC_STORAGE"));
				/* changed by manish for req TENJINCG-32 on 11-Jan-2017 ends */
			}

			if (tc != null) {

				ArrayList<TestStep> tsteps = new ArrayList<TestStep>();
				pst1 = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID =? ORDER BY TSTEP_EXEC_SEQ");
				pst1.setInt(1,tc.getTcRecId());
				rs1 = pst1.executeQuery();
				while (rs1.next()) {
					TestStep step = new TestStep();
					step.setRecordId(rs1.getInt("TSTEP_REC_ID"));
					step.setTestCaseRecordId(rs1.getInt("TC_REC_ID"));
					step.setShortDescription(rs1.getString("TSTEP_SHT_DESC"));
					step.setDataId(rs1.getString("TSTEP_DATA_ID"));
					step.setType(rs1.getString("TSTEP_TYPE"));
					step.setDescription(rs1.getString("TSTEP_DESC"));
					step.setExpectedResult(rs1.getString("tstep_exp_result"));
					step.setActualResult(rs1.getString("tstep_act_result"));
					step.setAppId(rs1.getInt("APP_ID"));
					step.setModuleCode(rs1.getString("FUNC_CODE"));
					step.setTransactionContext(rs1.getString("TXN_CONTEXT"));
					step.setExecutionContext(rs1.getString("EXEC_CONTEXT"));
					step.setTxnMode(rs1.getString("TXNMODE")); /* 20-Oct-2014 WS */
					step.setId(rs1.getString("TSTEP_ID"));
					step.setOperation(rs1.getString("TSTEP_OPERATION"));

					step.setAutLoginType(rs1.getString("TSTEP_AUT_LOGIN_TYPE"));

					step.setRowsToExecute(rs1.getInt("TSTEP_ROWS_TO_EXEC"));

					/* changed by manish for req TENJINCG-33 on 11-Jan-2017 starts */
					step.setTstepStorage(rs1.getString("TSTEP_STORAGE"));

					step.setResponseType(rs1.getInt("TSTEP_API_RESP_TYPE"));
					/*Added by Ashiki for TENJINCG-1275 starts*/
					step.setFileName(rs1.getString("TSTEP_FILENAME"));
					step.setFilePath(rs1.getString("TSTEP_FILEPATH"));
					/*Added by Ashiki for TENJINCG-1275 ends*/
					/* Added by Padmavathi for TENJINCG-647 (Dependency Rules) starts */
					if (rs1.getInt("TSTEP_EXEC_SEQ") != 1) {
						try {
							DependencyRules rules = new DependencyRulesHandler().getDependencyRules(step.getRecordId());
							if (rules != null) {
								step.setCustomRules(rules.getExecStatus());
								step.setDependentStepRecId(rules.getDependentStepRecId());
							}
						} catch (DatabaseException e) {
							logger.error(e.getMessage());
						}
					}
					/* Added by Padmavathi for TENJINCG-647 (Dependency Rules) ends */

					ArrayList<ResultValidation> val = null;
					try {
						val = this.hydrateValidationsForStep(conn, appConn, step.getRecordId());
						step.setValidations(val);
					} catch (Exception e) {
					}

					try {
						String appName = new AutHelper().getAutName(appConn, step.getAppId());
						step.setAppName(appName);
					} catch (Exception e) {
						step.setAppName(Integer.toString(step.getAppId()));
					}

					/* Added by Sriram for TENJINCG-311 */
					step.setValidationType(rs1.getString("TSTEP_VAL_TYPE"));
					/* Added by Sriram for TENJINCG-311 ends */
					/*Added by Prem for TENJINCG-1181 */
					ArrayList<ValidationDetails> valDeails = null;
					try {
						valDeails =	helper.hydrateUnstructuredData(step.getRecordId());
						step.setValidationdetails(valDeails);
					} catch (Exception e) {
						
					}
					/*Added by Prem for TENJINCG-1181 ends*/
					tsteps.add(step);
				}
				tc.setTcSteps(tsteps);
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch test case details", e);
		} finally {
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return tc;
	}
	/*modified by shruthi for VAPT helper fixes ends*/
	/*Modified by Pushpalatha for VAPT fix starts*/
	public TestStep persistTestStep(TestStep t) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		PreparedStatement p = null;
		PreparedStatement st1=null;
		ResultSet rs1 = null;
		ResultSet rs = null;
		ResultSet r = null;
		try {
			st1=conn.prepareStatement("SELECT 1 FROM TESTSTEPS  WHERE LOWER(TSTEP_ID)=? AND TC_REC_ID =? ");
			st1.setString(1, t.getId().toLowerCase());
			st1.setInt(2, t.getTestCaseRecordId());
			rs1=st1.executeQuery();
			if (rs1.next()) {
				t = null;
			} else {
				p = conn.prepareStatement("SELECT coalesce(MAX(TSTEP_REC_ID),0) AS TSTEP_REC_ID FROM TESTSTEPS");
				/* 02-Mar-2015 R2.1 Database agnostic Changes: Ends */
				rs = p.executeQuery();

				int recId = 0;

				while (rs.next()) {
					recId = rs.getInt("TSTEP_REC_ID");
				}

				recId++;

				t.setRecordId(recId);

				if (t.getTstepStorage() == null) {
					t.setTstepStorage(STORAGE);
				}

				/***********************************************************************************************************************************************
				 * Added by Roshni for TENJINCG-198
				 */
				int i = 0;
				try {
					pst1 = conn.prepareStatement(
							"SELECT coalesce(MAX(TSTEP_EXEC_SEQ),0) AS TSTEP_EXEC_SEQ FROM TESTSTEPS WHERE TC_REC_ID=?");
					pst1.setInt(1, t.getTestCaseRecordId());

					r = pst1.executeQuery();

					if (r.next()) {
						i = r.getInt("TSTEP_EXEC_SEQ");

					}
					i++;
					t.setSequence(i);

				} catch (Exception e) {
					logger.warn("ERROR getting latest sequence number of test step from TESTSTEPS table", e);
				}

				pst = conn.prepareStatement(
						"INSERT INTO TESTSTEPS (TSTEP_REC_ID,TC_REC_ID,TSTEP_SHT_DESC,TSTEP_DATA_ID,TSTEP_TYPE,TSTEP_DESC,TSTEP_EXP_RESULT,TSTEP_ACT_RESULT,APP_ID,FUNC_CODE,TXN_CONTEXT,EXEC_CONTEXT,TXNMODE,TSTEP_ID,TSTEP_OPERATION,TSTEP_AUT_LOGIN_TYPE,TSTEP_ROWS_TO_EXEC,TSTEP_STORAGE,TSTEP_EXEC_SEQ,TSTEP_VAL_TYPE,TSTEP_API_RESP_TYPE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

				pst.setInt(1, recId);
				pst.setInt(2, t.getTestCaseRecordId());
				pst.setString(3, t.getShortDescription());
				pst.setString(4, t.getDataId());
				pst.setString(5, t.getType());
				pst.setString(6, t.getDescription());
				pst.setString(7, t.getExpectedResult());
				pst.setString(8, "");
				pst.setInt(9, t.getAppId());
				pst.setString(10, t.getModuleCode());
				pst.setString(11, "New");
				if (t.getOperation() != null && !t.getOperation().equalsIgnoreCase("verify")) {
					pst.setString(12, "Submit");
				} else {
					pst.setString(12, t.getOperation());
				}
				pst.setString(13, t.getTxnMode()); /* 20-Oct-2014 WS */
				pst.setString(14, t.getId());

				pst.setString(15, t.getOperation());
				pst.setString(16, t.getAutLoginType());
				if (Utilities.trim(t.getValidationType()).equalsIgnoreCase("ui")) {
					pst.setInt(17, 1);
				} else {
					if (t.getRowsToExecute() == 0) {
						pst.setInt(17, 1);
					} else {
						pst.setInt(17, t.getRowsToExecute());
					}
				}

				pst.setString(18, t.getTstepStorage());
				pst.setInt(19, t.getSequence());
				pst.setString(20, t.getValidationType());
				pst.setInt(21, t.getResponseType());
				// changes done by parveen, Leelaprasad for #405 ends

				pst.execute();

				new ResultsHelper().persistResultValidationMapping(0, t.getRecordId());

			}
		} catch (Exception e) {
			logger.error("Could not insert Test step record", e);
			throw new DatabaseException("Could not insert record", e);
		} finally {

			DatabaseHelper.close(r);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(st1);
			DatabaseHelper.close(p);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return t;
	}
/*Modified by Pushpalatha for VAPT fix ends*/
	
	/*modified by shruthi for VAPT helper fixes starts*/
	public TestStep persistTestStep(Connection conn, TestStep t) throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		/***************************************************
		 * Added by Sriram to fix Bulk Test case uploading problem - Maximum open
		 * cursors error (17-10-2015)
		 */
		ResultSet rs1 = null;
		ResultSet rs = null;
		ResultSet r = null;
		PreparedStatement p = null;
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		PreparedStatement pst2= null;
		/* Added by Roshni for TENJINCG-239 starts */
		int i = 0;
		try {
			pst1 = conn.prepareStatement(
					"SELECT coalesce(MAX(TSTEP_EXEC_SEQ),0) AS TSTEP_EXEC_SEQ FROM TESTSTEPS WHERE TC_REC_ID=?");
			pst1.setInt(1, t.getTestCaseRecordId());

			r = pst1.executeQuery();

			if (r.next()) {
				i = r.getInt("TSTEP_EXEC_SEQ");

			}
			i++;
			t.setSequence(i);
			pst1.close();
			r.close();
		} catch (Exception e) {
			logger.warn("ERROR getting latest sequence number of test step from TESTSTEPS table", e);
		}

		/* Added by Roshni for TENJINCG-239 ends */
		try {
			/*modified by shruthi for VAPT helper fixes starts*/
			pst2 = conn.prepareStatement("SELECT 1 FROM TESTSTEPS  WHERE TSTEP_ID=? AND TC_REC_ID =?");
			pst2.setString(1, t.getId());
			pst2.setInt(2, t.getTestCaseRecordId());
			rs1 = pst2.executeQuery();
			/*modified by shruthi for VAPT helper fixes ends*/
			if (rs1.next()) {
				t = null;
			}

			/***************************************************
			 * Changed by Sriram to fix Bulk Test case uploading problem - Maximum open
			 * cursors error (17-10-2015) ends
			 */
			else {
				pst = conn.prepareStatement(
						"INSERT INTO TESTSTEPS (TSTEP_REC_ID,TC_REC_ID,TSTEP_SHT_DESC,TSTEP_DATA_ID,TSTEP_TYPE,TSTEP_DESC,TSTEP_EXP_RESULT,TSTEP_ACT_RESULT,APP_ID,FUNC_CODE,TXN_CONTEXT,EXEC_CONTEXT,TXNMODE,TSTEP_ID,TSTEP_OPERATION,TSTEP_AUT_LOGIN_TYPE,TSTEP_ROWS_TO_EXEC,TSTEP_STORAGE,TSTEP_EXEC_SEQ,TSTEP_RE_RUN) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

				t.setRecordId(DatabaseHelper.getGlobalSequenceNumber(conn));
				pst.setInt(1, t.getRecordId());
				/* Changed by Ashiki for TJN27-37 ends */
				pst.setInt(2, t.getTestCaseRecordId());
				pst.setString(3, t.getShortDescription());
				pst.setString(4, t.getDataId());
				pst.setString(5, t.getType());
				pst.setString(6, t.getDescription());
				pst.setString(7, t.getExpectedResult());
				pst.setString(8, "");
				pst.setInt(9, t.getAppId());
				pst.setString(10, t.getModuleCode());
				pst.setString(11, "New");
				if (t.getOperation() != null && !t.getOperation().equalsIgnoreCase("verify")) {
					pst.setString(12, "Submit");
				} else {
					pst.setString(12, t.getOperation());
				}
				pst.setString(13, t.getTxnMode()); /* 20-Oct-2014 WS */
				pst.setString(14, t.getId());
				pst.setString(15, t.getOperation());

				pst.setString(16, t.getAutLoginType());

				if (t.getRowsToExecute() == 0) {
					pst.setInt(17, 1);
				} else {
					pst.setInt(17, t.getRowsToExecute());
				}
				pst.setString(18, t.getTstepStorage());

				pst.setInt(19, t.getSequence());
				if (t.getRerunStep() == null) {
					pst.setString(20, "N");
				} else {
					pst.setString(20, t.getRerunStep());
				}
				/* Added by Padmavathi for TJN252-62 ends */
				pst.execute();

				new ResultsHelper().persistResultValidationMapping(conn, 0, t.getRecordId());

			}
			rs1.close();
		} catch (Exception e) {
			logger.error("Could not insert Test step record", e);
			throw new DatabaseException("Could not insert record", e);
		} finally {

			DatabaseHelper.close(r);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(p);
		}

		return t;
	}

	public void persistTestCaseTuned(TestCase t) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);


		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement p = null;

		try {

			if (!this.doesTestCaseExist(conn, t.getProjectId(), t.getTcId())) {

				pst = conn.prepareStatement("SELECT coalesce(MAX(TC_REC_ID),0) AS TC_rEC_ID FROM TESTCASES");
				int tcRecId = 0;

				rs = pst.executeQuery();
				while (rs.next()) {
					tcRecId = rs.getInt("TC_REC_ID");
				}

				tcRecId++;
				t.setTcRecId(tcRecId);
				pst.close();

				p = conn.prepareStatement(
						"INSERT INTO TESTCASES (TC_REC_ID,TC_REC_TYPE,TC_ID,TC_FOLDER_ID,TC_PRJ_ID,TC_NAME,TC_TYPE,TC_PRIORITY,TC_CREATED_ON,TC_DESC,TC_CREATED_BY,TC_STATUS,TC_STORAGE,TC_EXEC_MODE) "
								+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

				p.setInt(1, t.getTcRecId());
				p.setString(2, t.getRecordType());
				p.setString(3, t.getTcId());
				p.setInt(4, t.getTcFolderId());
				p.setInt(5, t.getProjectId());
				p.setString(6, t.getTcName());
				p.setString(7, t.getTcType());
				p.setString(8, t.getTcPriority());

				Timestamp timeStamp = new Timestamp(new Date().getTime());
				t.setTcCreatedOn(timeStamp);
				p.setTimestamp(9, timeStamp);
				p.setString(10, t.getTcDesc());
				p.setString(11, t.getTcCreatedBy());
				p.setString(12, "Not Executed");
				p.setString(13, t.getStorage());

				// Added by sakthi on 07-04-2017 starts
				p.setString(14, t.getMode());
				// Added by sakthi on 07-04-2017 ends
				p.execute();
			} else {
				throw new DatabaseException("This test case already exists");
			}

		} catch (DatabaseException e) {
			throw new DatabaseException(e.getMessage(), e);
		} catch (Exception e) {
			logger.error("ERROR inserting test case with ID {}", t.getTcId(), e);
			throw new DatabaseException("Could not insert record due to an internal error", e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(p);
			DatabaseHelper.close(conn);
		}

	}

	public TestCase persistTestCase(Connection conn, TestCase t) throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;

		try {

			t.setStorage(STORAGE);
			pst = conn.prepareStatement(
					"INSERT INTO TESTCASES (TC_REC_ID,TC_REC_TYPE,TC_ID,TC_FOLDER_ID,TC_PRJ_ID,TC_NAME,TC_TYPE,TC_PRIORITY,TC_CREATED_ON,TC_DESC,TC_CREATED_BY,TC_STATUS,TC_STORAGE,TC_EXEC_MODE,TC_PRESET_RULES) "
							+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			t.setTcRecId(DatabaseHelper.getGlobalSequenceNumber(conn));
			pst.setInt(1, t.getTcRecId());
			/* Changed by Ashiki for TJN27-37 starts */
			pst.setString(2, t.getRecordType());
			pst.setString(3, t.getTcId());
			pst.setInt(4, t.getTcFolderId());
			pst.setInt(5, t.getProjectId());
			pst.setString(6, t.getTcName());
			pst.setString(7, t.getTcType());
			pst.setString(8, t.getTcPriority());
			Timestamp timeStamp = new Timestamp(new Date().getTime());
			t.setTcCreatedOn(timeStamp);
			pst.setTimestamp(9, timeStamp);
			/* Modified by Roshni for Defect T25IT-286 ends */
			pst.setString(10, t.getTcDesc());
			pst.setString(11, t.getTcCreatedBy());
			pst.setString(12, "Not Executed");
			pst.setString(13, t.getStorage());
			/* Added by Roshni for Requirement TENJINCG-192 starts */
			pst.setString(14, t.getMode());
			pst.setString(15, t.getTcPresetRules());
			/* Added by Padmavathi for TJN252-62 ends */
			pst.execute();
			pst.close();
		} catch (Exception e) {
			throw new DatabaseException("Could not insert record due to an internal error", e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		return t;
		/* Modified by Padmavathi for TENJINCG-741 ends */
	}
 /* modified by Roshni for TENJINCG-1168 starts */
	public ArrayList<ResultValidation> hydrateValidationsForStep(int tstepRecId,Connection conn) throws DatabaseException {
	 /* modified by Roshni for TENJINCG-1168 ends */	
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		ArrayList<ResultValidation> val = new ArrayList<ResultValidation>();
		try {
			pst = conn.prepareStatement("SELECT * FROM RESVALMAP WHERE TSTEP_REC_ID = ?");
			pst.setInt(1, tstepRecId);
			rs = pst.executeQuery();

			while (rs.next()) {
				int resValId = rs.getInt("RES_VAL_ID");
				ResultValidation rv = new ResultsHelper().hydrateResultValidation(resValId);
				val.add(rv);
			}
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return val;
	}

	public ArrayList<ResultValidation> hydrateValidationsForStep(Connection conn, Connection appConn, int tstepRecId)
			throws DatabaseException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		ArrayList<ResultValidation> val = new ArrayList<ResultValidation>();
		try {
			pst = conn.prepareStatement("SELECT * FROM RESVALMAP WHERE TSTEP_REC_ID = ?");
			pst.setInt(1, tstepRecId);
			rs = pst.executeQuery();

			while (rs.next()) {
				int resValId = rs.getInt("RES_VAL_ID");
				ResultValidation rv = new ResultsHelper().hydrateResultValidationForRun(appConn, resValId);
				val.add(rv);
			}
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return val;
	}

	/*
	 * Change by Nagababu to get the application id using application name on
	 * 17-11-2014
	 */
	public int getappid(String appname) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		int appid = 0;
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try {
			pst = conn.prepareStatement("SELECT app_id FROM masapplication WHERE LOWER(app_name) =LOWER(?)");

			pst.setString(1, appname);
			rs = pst.executeQuery();

			if (rs.next()) {
				appid = rs.getInt("app_id");
			}
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return appid;
	}

	public static String getappname(Connection conn, int appid) throws DatabaseException {
		String appname = "";
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("SELECT app_name FROM masapplication WHERE app_id = ?");
			pst.setInt(1, appid);
			rs = pst.executeQuery();

			if (rs.next()) {
				appname = rs.getString("app_name");
			}

		} catch (Exception e) {
			logger.error("Could not fetch app name for ID {}", appid, e);
			throw new DatabaseException(e.getMessage(), e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return appname;
	}

	public boolean testStepConcurrencyCheck(Connection conn, int testStepRecordId) throws DatabaseException {
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		boolean isTestStepExecuting = false;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement(
					"SELECT A.RUN_ID FROM MASTESTRUNS A, TESTSETMAP B, TESTSTEPS C WHERE A.RUN_TS_REC_ID = B.TSM_TS_REC_ID AND B.TSM_TC_REC_ID = C.TC_REC_ID AND C.TSTEP_REC_ID = ? AND A.RUN_STATUS = ?");
			pst.setInt(1, testStepRecordId);
			pst.setString(2, "Executing");

			rs = pst.executeQuery();

			while (rs.next()) {
				isTestStepExecuting = true;
				break;
			}
		} catch (Exception e) {
			logger.error("ERROR performing Test Step Concurrency Check", e);
			throw new DatabaseException(
					"Could not check test step concurrency due to an internal error. Please contact your system administrator");
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		logger.info("Test Step Concurrency Check --> {}", isTestStepExecuting);
		return isTestStepExecuting;
	}

	/*********************************************************************
	 * Method added by Sriram to fix bug #123 in WebIssues Project Tenjin ends
	 */

	public boolean testCaseConcurrencyCheck(Connection conn, int testCaseRecordId) throws DatabaseException {
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		boolean isTestCaseExecuting = false;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement(
					"SELECT A.RUN_ID FROM MASTESTRUNS A, TESTSETMAP B WHERE A.RUN_TS_REC_ID = B.TSM_TS_REC_ID AND B.TSM_TC_REC_ID = ? AND A.RUN_STATUS = ?");
			pst.setInt(1, testCaseRecordId);
			pst.setString(2, "Executing");

			rs = pst.executeQuery();

			while (rs.next()) {
				isTestCaseExecuting = true;
				break;
			}
		} catch (Exception e) {
			logger.error("ERROR performing Test Case Concurrency Check", e);
			throw new DatabaseException(
					"Could not check test case concurrency due to an internal error. Please contact your system administrator");
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		logger.info("Test Case Concurrency Check --> {}", isTestCaseExecuting);
		return isTestCaseExecuting;
	}

	/*********************************************************************
	 * Method added by nagareddy (18-11-2016) for manual input displaying tree
	 */
	public Map<String, List<String>> fetchModulePageField(int appId, String moduleCode) {
		Connection conn = null;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		} catch (DatabaseException e1) {
			
			logger.error("Error ", e1);
		}

		if (conn == null) {
			try {
				throw new DatabaseException("Invalid or no database connection");
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			}
		}

		List<String> pageAreas = null;
		try {
			pageAreas = this.fetchPageAreas(conn, appId, moduleCode);
		} catch (DatabaseException e1) {
			
			logger.error("Error ", e1);
		}
		Map<String, List<String>> sheetData = new LinkedHashMap<String, List<String>>();

		try {
			if (pageAreas.size() > 0) {

				ResultSet rs = null;
				PreparedStatement pst = null;
				List<String> pageAreafields = null;
				for (String page : pageAreas) {

					try {
						pst = conn.prepareStatement(
								"SELECT FLD_UNAME,FLD_SEQ_NO FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE=? AND FLD_PAGE_AREA=? AND FLD_TYPE NOT IN ('LINK','BUTTON','IMG') ORDER BY FLD_SEQ_NO");
						pst.setInt(1, appId);
						pst.setString(2, moduleCode);
						pst.setString(3, page.toString());
						rs = pst.executeQuery();
						pageAreafields = new ArrayList<String>();
						while (rs.next()) {
							pageAreafields.add(rs.getString("FLD_UNAME"));
						}
					} catch (Exception e) {
						throw new DatabaseException(e.getMessage(), e);
					} finally {
						if (rs != null) {
							try {
								rs.close();
							} catch (Exception e) {

							}
						}
						if (pst != null) {
							try {
								pst.close();
							} catch (Exception e) {

							}
						}
					}
					sheetData.put(page.toString(), pageAreafields);

				} // end of for

			} // end of if
		} catch (Exception e) {

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					
					logger.error("Error ", e);
				}
			}
		}
		return sheetData;
	}

	/*********************************************************************
	 * Method added by nagareddy (18-11-2016) for manual input displaying tree
	 */
	public List<String> fetchPageAreas(Connection conn, int appId, String moduleCode) throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		ResultSet rs = null;
		PreparedStatement pst = null;
		List<String> pageAreas = new ArrayList<String>();
		try {
			pst = conn.prepareStatement(
					"SELECT PA_NAME FROM AUT_FUNC_PAGEAREAS WHERE PA_APP=? AND PA_FUNC_CODE=? ORDER BY PA_SEQ_NO");
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			rs = pst.executeQuery();

			while (rs.next()) {
				pageAreas.add(rs.getString("PA_NAME"));
			}
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					

				}
			}

			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					

				}
			}

		}
		return pageAreas;

	}

	/*********************************************************************
	 * Method added by nagareddy (18-11-2016) for manual input displaying tree
	 */
	public List<String> fetchManualMap(int appId, String moduleCode, String stepId, String pageArea, int tcRecId)
			throws DatabaseException {
		/* Changed by Padmavathi for TENJINCG-849 ends */
		
		List<String> manualFields = new ArrayList<String>();

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		ResultSet rs = null;
		PreparedStatement pst = null;
		int count = 0;
		try {
			pst = conn.prepareStatement(
					"SELECT MANUAL_FIELD_LABEL FROM MANUAL_FIELD_MAP WHERE APP_ID=? AND FUNCTION_NAME=? AND TEST_STEP_ID=? AND PAGE_AREA=? AND TEST_CASE_ID=?",
					ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, stepId);
			pst.setString(4, pageArea);
			/* Changed by Padmavathi for TENJINCG-849 Starts */
			pst.setInt(5, tcRecId);
			/* Changed by Padmavathi for TENJINCG-849 ends */

			rs = pst.executeQuery();
			if (rs.last()) {
				count = rs.getRow();
				rs.beforeFirst();
			}
			if (count > 0) {

				while (rs.next()) {
					manualFields.add(rs.getString("MANUAL_FIELD_LABEL"));
				}
			}
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					

				}
			}

			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					

				}
			}

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					
					logger.error("Error ", e);
				}
			}
		}

		return manualFields;
	}

	/*********************************************************************
	 * Method added by nagareddy (18-11-2016) for manual input displaying tree
	 */
	public void insertOrUpdateManualField(int appId, String moduleCode, String testStepId, JSONArray manualObj,
			int testCaseId) throws JSONException, DatabaseException {
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try {
			for (int i = 0; i < manualObj.length(); i++) { // **line 2**
				JSONObject childJSONObject = manualObj.getJSONObject(i);
				String srcSheet = childJSONObject.getString("srcSheet");
				String srcField = childJSONObject.getString("srcField");
				boolean status = this.checkManualField(conn, appId, moduleCode, srcSheet, srcField, testCaseId,
						testStepId);
				if (!status) {
					////// insert/////////
					this.insertManualField(conn, appId, moduleCode, srcSheet, srcField, testStepId, testCaseId);

				} else {
					/////// update///////

				}

			}
		} finally {
			DatabaseHelper.close(conn);
		}
	}

	/*********************************************************************
	 * Method added by nagareddy (18-11-2016) for manual input displaying tree
	 */
	private void insertManualField(Connection conn, int appId, String moduleCode, String srcSheet, String srcField,
			String testStepId, int testCaseId) throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;

		try {
			/* Changed by Parveen for changing insert query REQ #TJN_243_04 starts */
			
			pst = conn.prepareStatement(
					"INSERT INTO MANUAL_FIELD_MAP (APP_ID,FUNCTION_NAME,TEST_STEP_ID,PAGE_AREA,MANUAL_FIELD_LABEL,MANUAL_STATUS,TEST_CASE_ID)  VALUES (?,?,?,?,?,?,?)");
			/* Changed by Parveen for changing insert query REQ #TJN_243_04 ends */
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, testStepId);
			pst.setString(4, srcSheet);
			pst.setString(5, srcField);
			pst.setString(6, "TRUE");
			pst.setInt(7, testCaseId);
			pst.execute();
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					

				}
			}

		}

	}

	/*********************************************************************
	 * Method added by nagareddy (18-11-2016) for manual input displaying tree
	 */

	private boolean checkManualField(Connection conn, int appId, String moduleCode, String srcSheet, String srcField,
			int testCaseName, String testStepId) throws DatabaseException {

		boolean status = false;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		ResultSet rs = null;
		PreparedStatement pst = null;

		try {
			pst = conn.prepareStatement(
					"SELECT count(*) FROM MANUAL_FIELD_MAP WHERE APP_ID=? AND FUNCTION_NAME=? AND PAGE_AREA=? AND MANUAL_FIELD_LABEL=? AND TEST_CASE_ID=? AND TEST_STEP_ID=?");
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, srcSheet);
			pst.setString(4, srcField);
			pst.setInt(5, testCaseName);
			pst.setString(6, testStepId);
			rs = pst.executeQuery();
			rs.next();
			int result = rs.getInt(1);
			if (result > 0) {
				status = true;
			}
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					

				}
			}

			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					

				}
			}

		}
		return status;

	}

	/* Changed by Naga Reddy to Defect fix TEN-42 on 19-12-2016 starts */
	public void unMapManualField(int appId, String moduleCode, String stepId, String tcId, int testCaseRecordId)
			throws DatabaseException {
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;

		try {
			pst = conn.prepareStatement(
					"DELETE FROM MANUAL_FIELD_MAP WHERE APP_ID=? AND FUNCTION_NAME=? AND TEST_STEP_ID=? AND TEST_CASE_ID=?");
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, stepId);
			pst.setInt(4, testCaseRecordId);
			pst.execute();
		} catch (Exception e) {

		} finally {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					
					logger.error("Error ", e);
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					
					logger.error("Error ", e);
				}
			}
		}
	}

	public void unMapSingleManualField(int appId, String moduleCode, String stepId, String tcId, int testCaseRecordId,
			String unMapSheet, String unMapField) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;

		try {
			pst = conn.prepareStatement(
					"DELETE FROM MANUAL_FIELD_MAP WHERE APP_ID=? AND FUNCTION_NAME=? AND TEST_STEP_ID=? AND TEST_CASE_ID=? AND PAGE_AREA=? AND MANUAL_FIELD_LABEL=?");
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, stepId);
			pst.setInt(4, testCaseRecordId);
			pst.setString(5, unMapSheet);
			pst.setString(6, unMapField);
			pst.execute();
		} catch (Exception e) {

		} finally {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					
					logger.error("Error ", e);
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					
					logger.error("Error ", e);
				}
			}
		}

	}

	/* Changed by Naga Reddy to Defect fix TEN-42 on 19-12-2016 ends */

	/* added by manish for req TENJINCG-54 on 24-01-2017 starts */
	public List<TestManagerInstance> hydrateTmUserCredentials(String userId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<TestManagerInstance> tms = new ArrayList<TestManagerInstance>();
		try {
			/*modified by shruthi for VAPT helper fixes starts*/
			pst = conn.prepareStatement("select * from tjn_tm_user_mapping where TJN_USER_ID=?");
			pst.setString(1, userId);
			/*modified by shruthi for VAPT helper fixes ends*/
			rs = pst.executeQuery();
			while (rs.next()) {
				TestManagerInstance tmInstance = new TestManagerInstance();
				tmInstance.setInstanceName(rs.getString("TJN_INSTANCE_NAME"));
				tmInstance.setUsername(rs.getString("USERNAME"));
				tms.add(tmInstance);
			}
		} catch (Exception e) {
			logger.error("Error during fetching TM instance users", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return tms;
	}

	public List<String> fetchAllInstances() throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<String> instances = new ArrayList<String>();
		String instance = "";
		try {
			pst = conn.prepareStatement("select TM_INSTANCE_NAME from tjn_tm_tool_master");
			rs = pst.executeQuery();
			while (rs.next()) {
				instance = rs.getString("TM_INSTANCE_NAME");
				instances.add(instance);
			}
		} catch (Exception e) {
			logger.error("Error while hydrating TM instances", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return instances;
	}

	public void persistTmUserCredentials(TestManagerInstance dm) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement(
					"INSERT INTO TJN_TM_USER_MAPPING (TJN_USER_ID,TJN_INSTANCE_NAME,USERNAME,PASSWORD) VALUES (?,?,?,?)");
			pst.setString(1, dm.getTjnUsername());
			pst.setString(2, dm.getInstanceName());
			pst.setString(3, dm.getUsername());
			pst.setString(4, dm.getPassword());
			/* Modified by Preeti for TENJINCG-616 starts */
			pst.execute();
			/* Modified by Preeti for TENJINCG-616 ends */
		} catch (Exception e) {
			logger.error("Error while creating a new user for TM Instance", e);
			/* Added by Padmavathi for T25IT-412 starts */
			throw new DatabaseException("Could not insert record due to an internal error", e);
			/* Added by Padmavathi for T25IT-412 ends */
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}

	public TestManagerInstance hydrateTMUserDetails(String userId, String instance) throws DatabaseException {
		TestManagerInstance dmInstance = new TestManagerInstance();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			pst = conn.prepareStatement("select * from TJN_TM_USER_MAPPING where TJN_USER_ID=? and TJN_INSTANCE_NAME=?");
			pst.setString(1, userId);
			pst.setString(2, instance);
			rs = pst.executeQuery();
			while (rs.next()) {
				dmInstance.setTjnUsername(rs.getString("TJN_USER_ID"));
				dmInstance.setInstanceName(rs.getString("TJN_INSTANCE_NAME"));
				dmInstance.setUsername(rs.getString("USERNAME"));
				String encPwd = rs.getString("PASSWORD");
				/*Modified by paneendra for VAPT FIX starts*/
				String pwd = new CryptoUtilities().decrypt(encPwd);
				/*Modified by paneendra for VAPT FIX ends*/
				dmInstance.setPassword(pwd);
			}
		} catch (Exception e) {
			logger.error("Error while fetching details of a TM Instance user credentials", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return dmInstance;
	}

	public void deleteTMUser(String[] userIds, String[] dttInstances) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		try {
			for (int i = 0; i < userIds.length; i++) {
				try {
					/*modified by shruthi for VAPT helper fixes starts*/
					pst = conn.prepareStatement("DELETE FROM TJN_TM_USER_MAPPING WHERE TJN_USER_ID=? AND TJN_INSTANCE_NAME=?");
					pst.setString(1, userIds[i]);
					pst.setString(2, dttInstances[i]);
					/*modified by shruthi for VAPT helper fixes ends*/
					/* Modified by Preeti for TENJINCG-616 starts */
					pst.execute();
					/* Modified by Preeti for TENJINCG-616 ends */
				} finally {
					DatabaseHelper.close(pst);
				}
			}
		} catch (SQLException e) {
			throw new DatabaseException("Could not delete record", e);
		} finally {
			/*try {
				conn.commit();
			} catch (Exception e) {

			}*/
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}

	/*Added by paneendra for VAPT Fix starts*/
	/*Modified by Pushpa for VAPT fix starts*/
	public boolean deleteTMUserByUserId(String[] userIds, String[] dttInstances,String userId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		boolean deleteResult=false;
		try {
			for (int i = 0; i < userIds.length; i++) {
				try {
					//Modified by Priyanka for VAPT Sanity Test starts
					pst = conn.prepareStatement("DELETE FROM TJN_TM_USER_MAPPING WHERE TJN_USER_ID=? AND TJN_INSTANCE_NAME=?  AND USERNAME=?");
					//Modified by Priyanka for VAPT Sanity Test ends
					pst.setString(1, userIds[i]);
					pst.setString(2, dttInstances[i]);
					pst.setString(3, userId);
					//Modified by Priyanka for VAPT Sanity Test starts
					if(pst.executeUpdate()>=1) {
						deleteResult = true;
					}
					//Modified by Priyanka for VAPT Sanity Test starts
				} finally {
					DatabaseHelper.close(pst);
				}
			}
		} catch (SQLException e) {
			throw new DatabaseException("Could not delete record", e);
		} finally {
			/*try {
				conn.commit();
			} catch (Exception e) {

			}*/
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return deleteResult;
	}
	/*Modified by Pushpa for VAPT fix ends*/
	/*Added by paneendra for VAPT Fix ends*/
	
	public void validateUserTMLoginName(String instance, String userId) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement p = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		boolean valid = false;
		try {

			p = conn.prepareStatement(
					"SELECT TJN_INSTANCE_NAME,TJN_USER_ID FROM TJN_TM_USER_MAPPING where TJN_INSTANCE_NAME=? AND TJN_USER_ID=?");
			p.setString(1, instance);
			p.setString(2, userId);
			rs = p.executeQuery();
			while (rs.next()) {

				if (instance.equals(rs.getString("TJN_INSTANCE_NAME")) && userId.equals(rs.getString("TJN_USER_ID"))) {
					valid = true;
					throw new DatabaseException("You have already specified credentials for " + instance
							+ ". Please use a different instance and try again.");
				}

			}

		} catch (Exception e) {
			/* Modified by Padmavathi for T251IT-107 starts */
			if (valid) {
				throw new DatabaseException("You have already specified credentials for " + instance
						+ ". Please use a different instance and try again.");
			} else {
				throw new DatabaseException("Could not create user for this instance", e);
			}
			/* Modified by Padmavathi for T251IT-107 ends */
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(p);
			DatabaseHelper.close(conn);
		}

	}

	public void updateTMUserCredentials(TestManagerInstance tm, String oldInstance) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement(
					"UPDATE TJN_TM_USER_MAPPING SET USERNAME=?,PASSWORD=?,TJN_INSTANCE_NAME=? WHERE TJN_USER_ID=? AND TJN_INSTANCE_NAME=?");
			String pwd = tm.getPassword();
			/*Modified by paneendra for VAPT FIX starts*/
			String encPwd = new CryptoUtilities().encrypt(pwd);
			/*Modified by paneendra for VAPT FIX ends*/
			pst.setString(1, tm.getUsername());
			pst.setString(2, encPwd);
			pst.setString(3, tm.getInstanceName());
			pst.setString(4, tm.getTjnUsername());

			pst.setString(5, oldInstance);
			pst.execute();
		} catch (Exception e) {
			throw new DatabaseException("Could not update record due to Internal error please contact Tenjin support",
					e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}

	/*Added by paneendra for VAPT Fix starts*/
	public boolean updateTMUserCredentialsByUserId(TestManagerInstance tm, String oldInstance) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean updateResult=false;
		try {
			pst = conn.prepareStatement(
					"UPDATE TJN_TM_USER_MAPPING SET USERNAME=?,PASSWORD=?,TJN_INSTANCE_NAME=? WHERE TJN_USER_ID=? AND TJN_INSTANCE_NAME=?");
			String pwd = tm.getPassword();
			/*modified by paneendra for VAPT FIX starts*/
			String encPwd = new CryptoUtilities().encrypt(pwd);
			/*modified by paneendra for VAPT FIX ends*/
			pst.setString(1, tm.getUsername());
			pst.setString(2, encPwd);
			pst.setString(3, tm.getInstanceName());
			pst.setString(4, tm.getTjnUsername());

			pst.setString(5, oldInstance);
			pst.execute();
			updateResult=true;
		} catch (Exception e) {
			throw new DatabaseException("Could not update record due to Internal error please contact Tenjin support",
					e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return updateResult;
	}
	/*Added by paneendra for VAPT Fix ends*/

	
	public void hydrateFilters(Project project) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			pst = conn.prepareStatement("SELECT * FROM TJN_PRJ_TM_LINKAGE WHERE PRJ_ID=?");
			pst.setInt(1, project.getId());
			rs = pst.executeQuery();
			while (rs.next()) {
				project.setEtmTool(rs.getString("TM_INSTANCE_NAME"));
				project.setEtmProject(rs.getString("TM_PROJECT"));
				project.setEtmEnable("TM_ENABLED");
				project.setTcFilter(rs.getString("TC_FILTER"));
				project.setTcFilterValue(rs.getString("TC_FILTER_VALUE"));
				project.setTsFilter(rs.getString("TS_FILTER"));
				project.setTsFilterValue(rs.getString("TS_FILTER_VALUE"));
			}
		} catch (SQLException e) {
			logger.error("could not fetch filters for project {}", project.getId(), e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	/*
	 * changed by manish for fetching tm credentials for a perticular project on
	 * 29-Aug-2017 starts
	 */
	/*
	 * public TestManagerInstance hydrateTMCredentials(String instance, String
	 * userId) throws DatabaseException{
	 */
	public TestManagerInstance hydrateTMCredentials(String instance, String userId, int projectId)
			throws DatabaseException {
		/*
		 * changed by manish for fetching tm credentials for a perticular project on
		 * 29-Aug-2017 ends
		 */
		TestManagerInstance tmInstance = new TestManagerInstance();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("SELECT A.TJN_USER_ID,A.TJN_INSTANCE_NAME,A.USERNAME,A.PASSWORD,"
					+ " B.TM_REC_ID,B.TM_TOOL,B.TM_URL,C.TM_PROJECT FROM" + " TJN_TM_USER_MAPPING A,"
					+ " TJN_TM_TOOL_MASTER B, TJN_PRJ_TM_LINKAGE C WHERE"
					+ " A.TJN_INSTANCE_NAME=B.TM_INSTANCE_NAME AND" + " C.TM_INSTANCE_NAME=B.TM_INSTANCE_NAME AND"
					+ " A.TJN_USER_ID=? AND" + " B.TM_INSTANCE_NAME=? AND C.PRJ_ID=?");
			pst.setString(1, userId);
			pst.setString(2, instance);
			/*
			 * changed by manish for fetching tm credentials for a perticular project on
			 * 29-Aug-2017 starts
			 */
			pst.setInt(3, projectId);
			/*
			 * changed by manish for fetching tm credentials for a perticular project on
			 * 29-Aug-2017 ends
			 */
			rs = pst.executeQuery();
			while (rs.next()) {
				tmInstance.setURL(rs.getString("TM_URL"));
				tmInstance.setPassword(rs.getString("PASSWORD"));
				tmInstance.setTjnUsername(rs.getString("TJN_USER_ID"));
				tmInstance.setTool(rs.getString("TM_TOOL"));
				tmInstance.setUsername(rs.getString("USERNAME"));
				tmInstance.setTmProjectDomain(rs.getString("TM_PROJECT"));
			}
		} catch (SQLException e) {
			logger.error("Could not hydrate TM instance's credentials {}", instance, e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);

		}
		return tmInstance;
	}

	public boolean checkTestCase(String tcId, int projectId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		boolean exist = false;
		ResultSet rs = null;
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement("SELECT * FROM TESTCASES WHERE TC_ID=? AND TC_PRJ_ID=?");
			pst.setString(1, tcId);
			pst.setInt(2, projectId);
			rs = pst.executeQuery();
			while (rs.next()) {
				exist = true;
			}
		} catch (SQLException e) {
			logger.error("error while checking testcase already imported or not", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return exist;
	}

	public boolean checkImportedTestStep(TestStep step, int tcRecId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		boolean exist = false;
		ResultSet rs = null;
		PreparedStatement pst = null;
		try {
			/*
			 * changed by manish to check if test step already exist or not on 29-Aug-2017
			 * starts
			 */
			pst = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TSTEP_ID=? AND TC_REC_ID=?");
			pst.setString(1, step.getId());
			pst.setInt(2, tcRecId);
			/*
			 * changed by manish to check if test step already exist or not on 29-Aug-2017
			 * starts
			 */
			rs = pst.executeQuery();
			while (rs.next()) {
				exist = true;
			}
		} catch (SQLException e) {
			logger.error("error while checking testcase already imported or not", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return exist;
	}
	/*
	 * added by manish for req TENJINCG-59,TENJINCG-57,TENJINCG-58 on 25-01-2017
	 * ends
	 */

	/* added by manish for req TENJINCG-57 and TENJINCG-58 on 26-01-2017 starts */

	public void _persistMappedAttributes(Connection conn, int projectId, TestManagerInstance tmInstance)
			throws DatabaseException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement(
					"INSERT INTO TJN_PRJ_TM_ATTRIBUTES (REC_TYPE,TJN_FIELD_NAME,TM_FIELD_NAME,PRJ_ID) VALUES(?,?,?,?)");
			pst.setString(1, tmInstance.getRec_type());
			pst.setString(2, tmInstance.getTjnField());
			pst.setString(3, tmInstance.getTmField());
			pst.setInt(4, projectId);
			rs = pst.executeQuery();
		} catch (SQLException e) {
			throw new DatabaseException(
					"Could not map fields due to some Internal error, Please contact Tenjin Support", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
	}

	public void persistMappedAttributes(int projectId, TestManagerInstance tmInstance) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		try {
			this._persistMappedAttributes(conn, projectId, tmInstance);
		} finally {
			DatabaseHelper.close(conn);
		}
	}

	public boolean tmAlreadyMapped(int projectId, String tjnField) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean update = false;
		try {
			pst = conn.prepareStatement("SELECT * FROM TJN_PRJ_TM_ATTRIBUTES WHERE PRJ_ID=? AND TJN_FIELD_NAME=?");
			pst.setInt(1, projectId);
			pst.setString(2, tjnField);
			rs = pst.executeQuery();
			while (rs.next()) {
				update = true;
			}

		} catch (SQLException e) {
			logger.error("Error during checking if tm mapped data already exist for a tenjin field");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return update;
	}

	/* Added By Ashiki for TJN252-60 starts */
	public Map<String, String> hydrateMappedAttribute(int projectId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		Map<String, String> mapAttributes = new TreeMap<String, String>();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("SELECT * FROM TJN_PRJ_TM_ATTRIBUTES");
			rs = pst.executeQuery();
			while (rs.next()) {
				mapAttributes.put(rs.getString("TJN_FIELD_NAME"), rs.getString("TM_FIELD_NAME"));
			}
		} catch (SQLException e) {
			logger.error("Error during fetching mapped data for a tenjin field");
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return mapAttributes;
	}

	/* Added By Ashiki for TJN252-60 ends */
	public void updateMappedAttributes(int projectId, TestManagerInstance tmInstance) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			pst = conn.prepareStatement(
					"UPDATE TJN_PRJ_TM_ATTRIBUTES SET TM_FIELD_NAME=? WHERE PRJ_ID=? AND TJN_FIELD_NAME=?");

			pst.setString(1, tmInstance.getTmField());
			pst.setInt(2, projectId);
			pst.setString(3, tmInstance.getTjnField());
			/* Modified by Preeti for TENJINCG-616 starts */
			pst.execute();
			/* Modified by Preeti for TENJINCG-616 ends */
		} catch (SQLException e) {
			throw new DatabaseException(
					"Could not update mapped records due to some internal error, Please contact Tenjin Support");
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	/* added by manish for req TENJINCG-57 and TENJINCG-58 on 26-01-2017 ends */

	/* Added by Sriram for TENJINCG-189 (13th June 2017) */
	/*Added by Pushpa for VAPT fix starts*/
	public String areTestCasesOfSameMode(String testCaseRecordIds) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		String mode = "";
		boolean sameMode = true;
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (testCaseRecordIds != null) {

			if (testCaseRecordIds.endsWith(",")) {
				testCaseRecordIds = testCaseRecordIds.substring(0, testCaseRecordIds.length() - 1);
			}

			String[] tcRecIds=testCaseRecordIds.split(",");
			try {
				for(String tcRecID:tcRecIds) {
						
						pst = conn.prepareStatement(
								"SELECT DISTINCT TC_EXEC_MODE FROM TESTCASES WHERE TC_REC_ID =?");
						pst.setString(1, tcRecID);
					rs = pst.executeQuery();
					pst.close();

				while (rs.next()) {
					if (mode.length() < 1)
						mode = rs.getString("TC_EXEC_MODE");
					else if (!mode.equalsIgnoreCase(rs.getString("TC_EXEC_MODE"))) {
						sameMode = false;
						break;
					}
				}

				rs.close();
				}
				
			} catch (SQLException e) {
				
				logger.error("ERROR checking if all tests are of same mode", e);
			} finally {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}

		if (!sameMode) {
			throw new DatabaseException("You must choose Test Cases of the same Mode to continue.");
		}

		return mode;
	}/*Added by Pushpa for VAPT fix ends*/

	/* Added by Sriram for TENJINCG-189 (13th June 2017) */
	/*******************************************************************************
	 * Added by Roshni for TENJINCG-198
	 * 
	 * @param testCaseRecordId
	 * @param sequenceMap
	 * @throws DatabaseException
	 */

	public void reOrderTestCases(int testCaseRecordId, Map<Integer, Integer> sequenceMap) throws DatabaseException {
		// UPDATE TESTSTEPS SET TSTEP_EXEC_SEQ=? WHERE TSTEP_REC_ID=?
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		PreparedStatement pst = null;
		try {

			for (Integer stepRecordId : sequenceMap.keySet()) {
				try {
					int sequenceValue = sequenceMap.get(stepRecordId);
					pst = conn.prepareStatement("UPDATE TESTSTEPS SET TSTEP_EXEC_SEQ=? WHERE TSTEP_REC_ID=?");
					pst.setInt(1, sequenceValue);
					pst.setInt(2, stepRecordId);
					pst.executeUpdate();

					pst.close();
				} finally {
					DatabaseHelper.close(pst);
				}

			}
			//conn.commit();
		} catch (SQLException e) {
			
			logger.error("ERROR - Could not update sequence", e);
			try {
				conn.rollback();
			} catch (Exception e1) {
				logger.warn("Could not roll-back", e1);
			}
			throw new DatabaseException(
					"Could not update sequence due to an internal error. Please contact Tenjin Support.");
		} finally {

			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	/*Modified by Pushpalatha for VAPT starts*/
	public void updateTestStepSeq(int testCaseRecordId) throws DatabaseException {
		int i = 1;
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst6 = null, pst7 = null;
		ResultSet r = null;
		try {
			pst6 = conn.prepareStatement(
					"select tstep_rec_id from TESTSTEPS where TC_REC_ID=? order by TSTEP_EXEC_SEQ asc");
			pst6.setInt(1, testCaseRecordId);

			r = pst6.executeQuery();

			while (r.next()) {
				try {
					int stepId = r.getInt("TSTEP_REC_ID");
			pst7 = conn.prepareStatement(
					"update TESTSTEPS set TSTEP_EXEC_SEQ=? where tstep_rec_id=?");
			pst7.setInt(1, i);
			pst7.setInt(2, stepId);
					pst7.executeUpdate();
					i++;
				} finally {
					DatabaseHelper.close(pst7);
				}
			}

		} catch (Exception e) {
			logger.warn("Could not update teststep sequence", e);
		} finally {
			DatabaseHelper.close(r);
			DatabaseHelper.close(pst6);
			DatabaseHelper.close(pst7);
			DatabaseHelper.close(conn);
		}
	}/*Modified by Pushpalatha for VAPT ends*/
	/* Added by Roshni for TENJINCG-247 ends */

	// TENJINCG-459
	public TestStep hydrateTestStepDetailsForTestCase(int projectId, String testStepId, int tcRecId)
			throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		TestStep t = null;
		PreparedStatement pst = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		try {

			pst = conn.prepareStatement(
					"SELECT A.*, B.TC_ID, B.TC_NAME, C.APP_NAME FROM TESTSTEPS A, TESTCASES B, MASAPPLICATION C WHERE C.APP_ID = A.APP_ID AND A.TSTEP_ID = ? AND A.TC_REC_ID = ?");

			pst.setString(1, testStepId);
			pst.setInt(2, tcRecId);
			rs = pst.executeQuery();

			while (rs.next()) {
				t = new TestStep();
				t.setRecordId(rs.getInt("tstep_rec_id"));
				t.setShortDescription(rs.getString("TSTEP_SHT_DESC"));
				t.setDataId(rs.getString("TSTEP_DATA_ID"));
				t.setType(rs.getString("TSTEP_TYPE"));
				t.setDescription(rs.getString("TSTEP_DESC"));
				t.setExpectedResult(rs.getString("TSTEP_EXP_RESULT"));
				t.setAppId(rs.getInt("APP_ID"));
				t.setModuleCode(rs.getString("FUNC_CODE"));
				t.setId(rs.getString("TSTEP_ID"));
				t.setTestCaseRecordId(rs.getInt("TC_REC_ID"));
				t.setOperation(rs.getString("TSTEP_OPERATION"));
				t.setAutLoginType(rs.getString("TSTEP_AUT_LOGIN_TYPE"));
				t.setRowsToExecute(rs.getInt("TSTEP_ROWS_TO_EXEC"));
				t.setTstepStorage(rs.getString("TSTEP_STORAGE"));
				t.setParentTestCaseId(rs.getString("TC_ID"));
				t.setTestCaseName(rs.getString("TC_NAME"));
				t.setAppName(rs.getString("APP_NAME"));
				// changes done by parveen, Leelaprasad for #405 starts
				t.setResponseType((rs.getInt("TSTEP_API_RESP_TYPE")));
				// changes done by parveen, Leelaprasad for #405 ends
			}

			if (t != null) {
				p = conn.prepareStatement("SELECT * FROM RESVALMAP WHERE TSTEP_REC_ID = ?");
				p.setInt(1, t.getRecordId());

				ArrayList<ResultValidation> vals = new ArrayList<ResultValidation>();
				rs2 = p.executeQuery();
				while (rs2.next()) {
					int r = rs2.getInt("RES_VAL_ID");
					ResultValidation val = new ResultsHelper().hydrateResultValidation(r);
					vals.add(val);
				}

				t.setValidations(vals);
			}
		} catch (Exception e) {
			throw new DatabaseException("Could not hydrate Test Step information", e);
		} finally {
			DatabaseHelper.close(rs2);
			DatabaseHelper.close(p);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return t;
	}
	// TENJINCG-459 ends
	/*modified by shruthi for VAPT helper fixes starts*/
	public ArrayList<TestCase> hydrateAllTestCases(int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		/*Statement st = null;*/
		PreparedStatement pst = null;
		ResultSet rs =null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<TestCase> testCases = new ArrayList<TestCase>();

		try{
			pst = conn.prepareStatement("SELECT * FROM TESTCASES WHERE TC_PRJ_ID =? ORDER BY TC_REC_ID");
			pst.setInt(1, projectId);
			rs = pst.executeQuery();
			while(rs.next()){
				TestCase tc = new TestCase();
				tc.setTcId(rs.getString("tc_id"));
				tc.setTcName(Utilities.escapeXml(rs.getString("tc_name")));
				tc.setTcType(rs.getString("tc_type"));
				tc.setTcStatus(rs.getString("tc_status"));
				tc.setTcCreatedBy(rs.getString("tc_created_by"));
				tc.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
				tc.setTcModifiedBy(rs.getString("tc_modified_by"));
				tc.setTcModifiedOn(rs.getDate("tc_modified_on"));
				tc.setTcDesc(rs.getString("tc_desc"));
				tc.setTcRecId(rs.getInt("TC_REC_ID"));
				tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
				tc.setProjectId(rs.getInt("TC_PRJ_ID"));
				tc.setRecordType(rs.getString("TC_REC_TYPE"));
				tc.setTcReviewed(rs.getString("TC_REVIEWED"));
				tc.setTcPriority(rs.getString("TC_PRIORITY"));
				tc.setStorage(rs.getString("TC_STORAGE"));
							
				tc.setMode(rs.getString("TC_EXEC_MODE"));

				ArrayList<TestStep> steps = this.hydrateStepsForTestCase(conn, conn, tc.getTcRecId());
				tc.setTcSteps(steps);

				testCases.add(tc);
			}
		}catch(Exception e){

		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return testCases;
	}
}
