/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TesrManagerHelper


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
* 10-02-2017			Manish					defect- TENJINCG-106
* 18-02-2019			Ashiki 					TJN252-60
* 15-02-2019			Pushpalatha				TENJINCG-956
* 26-03-2019			Roshni					TJN252-54
*/	
/*
 * Added File by Parveen for Requirement TENJINCG_53
 */
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.testmanager.TestManagerInstance;
import com.ycs.tenjin.util.Constants;

public class TestManagerHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(TestCaseHelper.class);
	public int checkName(String name) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement p =null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		int chName=0;
		try {

			/*modified by shruthi for VAPT helper fixes starts*/
			p = conn.prepareStatement("SELECT 1 FROM TJN_TM_TOOL_MASTER where upper(TM_INSTANCE_NAME)=?");
			p.setString(1, name.toUpperCase());
			/*modified by shruthi for VAPT helper fixes ends*/
			rs = p.executeQuery();
			if(rs.next()) {

				chName=rs.getInt(1);
			}
			if(chName>1)
			{
				throw new DatabaseException("The name you entered is already in use. Please use a different name");	
			}
			
		}catch (Exception e) {
			logger.error("ERROR while checking for already existed instance name", e);
          throw new DatabaseException("Could not create client", e);
		}finally {
			DatabaseHelper.close(rs);
            DatabaseHelper.close(p);
            DatabaseHelper.close(conn);
		}
		return chName;

	}
	public TestManagerInstance persistTestManager(TestManagerInstance tm) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst1=null;
		PreparedStatement pst=null;
		ResultSet rs1 =null;
		int rcId=0;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}

		try {

		    pst1 = conn.prepareStatement("SELECT MAX(TM_REC_ID) AS TM_REC_ID FROM TJN_TM_TOOL_MASTER");
			rs1 = pst1.executeQuery();
			while(rs1.next()){
				rcId = rs1.getInt("TM_REC_ID");
				rcId++;
			}
			tm.setTmRecId(rcId);
			
		    pst = conn.prepareStatement("INSERT INTO TJN_TM_TOOL_MASTER (TM_REC_ID,TM_INSTANCE_NAME,TM_TOOL,TM_URL) VALUES (?,?,?,?)");
			pst.setInt(1, tm.getTmRecId());
			pst.setString(2, tm.getInstanceName());
			pst.setString(3, tm.getTool());
			pst.setString(4, tm.getURL());
			pst.execute();
			logger.debug("Persisted the TM instance Details");

		}catch (Exception e) {
			logger.error("ERROR occured while persisting the Test manager instance  setails", e);
			throw new DatabaseException("Could not create TM Instance", e);
		} finally {
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(conn);
		}

		return tm;
	}
	
	
	public TestManagerInstance hydrateTestManagerInstance(String name) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		TestManagerInstance tm = null;
		try {

			pst = conn
					.prepareStatement("SELECT * FROM TJN_TM_TOOL_MASTER WHERE TM_INSTANCE_NAME =?");
			pst.setString(1,name );
			rs = pst.executeQuery();

			while (rs.next()) {
				tm = new TestManagerInstance();
				tm.setInstanceName(rs.getString("TM_INSTANCE_NAME"));
				tm.setTool(rs.getString("TM_TOOL"));
				tm.setURL(rs.getString("TM_URL"));
				}

			logger.debug("Hydrated the TM instance details-->",name);
		

		} catch (Exception e) {
			logger.error("ERROR Hydrating test manager instance details", e);
			throw new DatabaseException("Could not fetch record ", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return tm;

	}
	
	public ArrayList<TestManagerInstance> hydrateAllTestManager()  throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_CORE);
		
		PreparedStatement st=null;
		ResultSet rs=null;
		/*Added by Pushpalatha for TENJINCG-956 starts*/
		PreparedStatement pst=null;
		ResultSet rs1=null;
		/*Added by Pushpalatha for TENJINCG-956 ends*/
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<TestManagerInstance> tm = new ArrayList<TestManagerInstance>();

		try {
			st = conn.prepareStatement("SELECT * FROM TJN_TM_TOOL_MASTER ");

			rs = st.executeQuery();
			while (rs.next()) {
				TestManagerInstance tm1 = new TestManagerInstance();
				/*Added by Pushpalatha for TENJINCG-956 starts*/
				pst=conn.prepareStatement("SELECT * FROM TJN_PRJ_TM_LINKAGE WHERE TM_INSTANCE_NAME=?");
				pst.setString(1,rs.getString("TM_INSTANCE_NAME"));
				rs1=pst.executeQuery();
				/*Added by Pushpalatha for TENJINCG-956 ends*/
				tm1.setInstanceName(rs.getString("TM_INSTANCE_NAME"));
				tm1.setTool(rs.getString("TM_TOOL"));
				tm1.setURL(rs.getString("TM_URL"));
				/*Added by Pushpalatha for TENJINCG-956 starts*/
				if(rs1.next()){
					tm1.setTmStatus("Mapped");
				}else{
					tm1.setTmStatus("Not Mapped");
				}
				/*Added by Pushpalatha for TENJINCG-956 ends*/
				tm.add(tm1);


			}
			logger.debug("Hydrated ALL the TM instance details-->");
			

		} catch (Exception e) {
			logger.error("ERROR Hydrating ALL test manager instance details", e);
			throw new DatabaseException("Could not fetch records", e);
		} finally {
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(st);
			DatabaseHelper.close(conn);
		}

		return tm;

	}
	
	public List<String> fetchTMInstances() throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs=null;
		List<String> instances = new ArrayList<String>();
		String instance = "";
		try {
			pst = conn
					.prepareStatement("select TM_INSTANCE_NAME from tjn_tm_tool_master");
			rs = pst.executeQuery();
			while (rs.next()) {
				instance = rs.getString("TM_INSTANCE_NAME");
				instances.add(instance);
			}
			logger.debug("Fetching  the TM instance  details-->");
		} catch (Exception e) {
			logger.error("Error while hydrating TM instances",e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return instances;
	}

	/*Modified by Pushpa for VAPT fix starts*/
	public void clearAllTestManagerInstance(String[] record) throws  DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);
		 PreparedStatement prest = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		
		try {
		      
		      
		      for(String rec:record) {
					prest = conn.prepareStatement("DELETE FROM TJN_TM_TOOL_MASTER WHERE TM_INSTANCE_NAME =?");
					prest.setString(1, rec);
					 prest.executeUpdate();
					 prest.close();
				}
		} catch (Exception e) {
			logger.error("ERROR occured while deleting the TM Instances", e);
			throw new DatabaseException("Could not delete all test manager instances ", e);
		}

	    finally {
			DatabaseHelper.close(prest);
			DatabaseHelper.close(conn);
		}
		/*Modified by Pushpa for VAPT fix ends*/
	}
	public void deleteTestManagerInstance(String name) throws DatabaseException {

		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {

		    pst = conn
					.prepareStatement("DELETE FROM TJN_TM_TOOL_MASTER WHERE TM_INSTANCE_NAME =?");
			pst.setString(1, name);
			pst.executeUpdate();
			logger.debug("Deleted the TM instance details-->",name);
		} catch (Exception e) {
			logger.error("ERROR occured while deleting the test Manager Instance", e);
			throw new DatabaseException("Could not delete record", e);
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}
	
	public void updateTestManager(TestManagerInstance tm, String exName) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null ;

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {


			pst = conn.prepareStatement("UPDATE TJN_TM_TOOL_MASTER SET TM_INSTANCE_NAME=?,TM_TOOL =?, TM_URL =? WHERE TM_INSTANCE_NAME=?");

			pst.setString(1, tm.getInstanceName());
			pst.setString(2, tm.getTool());
			pst.setString(3, tm.getURL());
			pst.setString(4,exName );
            pst.execute();
            logger.debug("Updaing the instance details-->",exName);
		} catch (Exception e) {
			logger.error("ERROR updating test manager instance details", e);
			throw new DatabaseException(
					"Could not update Test manager because of an internal error ");
		}finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}
	
	/*added by manish for defect TENJINCG-106 on 02-10-2017 starts*/
	
	public void deleteInstanceCredentials(String instance) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		ResultSet rs=null;

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		
		try {
			pst = conn.prepareStatement("DELETE FROM TJN_TM_USER_MAPPING WHERE TJN_INSTANCE_NAME=?");
			pst.setString(1, instance);
			rs = pst.executeQuery();
		} catch (SQLException e) {
			logger.error("could not delete credentials for instance "+instance,e);
		}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	
	/*Added by Ashiki for TJN252-60 Starts*/
	public void updateTmFilters(String instanceName) throws DatabaseException{
		 String[] instanceList=instanceName.split(",");
		 
			for(String instance:instanceList) {
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				if (conn == null) {
					throw new DatabaseException("Invalid or no database connection");
				}
				 PreparedStatement pst = null;
				
			try {
				pst = conn.prepareStatement("UPDATE TJN_PRJ_TM_LINKAGE SET TM_PROJECT=?,TM_ENABLED=?,TC_FILTER=?,TC_FILTER_VALUE=?, TS_FILTER=?,TS_FILTER_VALUE=?,TM_INSTANCE_NAME=? where TM_INSTANCE_NAME =?");
				 pst.setString(1,"");
			       pst.setString(2,"");
			       pst.setString(3,"");
			       pst.setString(4,"");
			       pst.setString(5,"");
			       pst.setString(6,"");
			       pst.setString(7,"");
			       pst.setString(8, instance);
			       pst.executeUpdate();
	} catch (Exception e) {
		logger.error("ERROR occured while deleting the TM Instances", e);
		throw new DatabaseException("Could not delete all test manager instances ", e);
	}

    finally {
		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);
	}
	}}

	/*Modified by Pushpa for VAPT fix starts*/
	public void updateTmFilters(String[] record) throws DatabaseException{

		for(String rec:record) {
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			 PreparedStatement pst = null;
			
			if (conn == null) {
				throw new DatabaseException("Invalid or no database connection");
			}
		try {
			pst = conn.prepareStatement("UPDATE TJN_PRJ_TM_LINKAGE SET TM_PROJECT=?,TM_ENABLED=?,TC_FILTER=?,TC_FILTER_VALUE=?, TS_FILTER=?,TS_FILTER_VALUE=?,TM_INSTANCE_NAME=? where TM_INSTANCE_NAME =?");
			 pst.setString(1,"");
		       pst.setString(2,"");
		       pst.setString(3,"");
		       pst.setString(4,"");
		       pst.setString(5,"");
		       pst.setString(6,"");
		       pst.setString(7,"");
		       pst.setString(8, rec);
		       pst.executeUpdate();
			
		} catch (Exception e) {
			logger.error("ERROR occured while deleting the TM Instances", e);
			throw new DatabaseException("Could not delete all test manager instances ", e);
		}

	    finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}}

		/*Modified by Pushpa for VAPT fix ends*/
	}
	
	public Project hydrateInstance(int prjId) throws DatabaseException {


		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		Project tm=null;
		try {

			pst = conn
					.prepareStatement("SELECT A.TJN_INSTANCE_NAME,B.TM_PROJECT FROM TJN_TM_USER_MAPPING A, TJN_PRJ_TM_LINKAGE B WHERE A.TJN_INSTANCE_NAME = B.TM_INSTANCE_NAME AND B.PRJ_ID=?");
			pst.setInt(1,prjId );
			rs = pst.executeQuery();
			
			while (rs.next()) {
				tm=new Project();
				tm.setEtmProject(rs.getString("TM_PROJECT"));
				tm.setEtmInstance(rs.getString("TJN_INSTANCE_NAME"));
			}
			
		

		} catch (Exception e) {
			logger.error("ERROR Hydrating test manager instance details", e);
			throw new DatabaseException("Could not fetch record ", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return tm;

	
	}
	
	public void deleteMultipleInstanceCredentials(String[] record) throws  DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement prest =null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		
		try {
			
			/*modified by paneendra for sql injection starts*/
		     for(String rec:record) {
			 prest = conn.prepareStatement("DELETE FROM TJN_TM_USER_MAPPING WHERE TJN_INSTANCE_NAME =? ");
			 prest.setString(1, rec);
			 prest.executeUpdate();
			 prest.close();
		}/*modified by paneendra for sql injection ends*/
		} catch (Exception e) {
			logger.error("ERROR occured while deleting the TM Instances credentials", e);
			throw new DatabaseException("Could not delete all test manager instances credentials", e);
		}

	    finally {
			DatabaseHelper.close(prest);
			DatabaseHelper.close(conn);
		}

	}
	
	/*added by manish for defect TENJINCG-106 on 02-10-2017 ends*/
	/* Added By Roshni */
	public List<TestManagerInstance> hydrateAllTmInstances(int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement prest =null;
		ResultSet rs=null;
		
		List<TestManagerInstance> tmInstances = new ArrayList<TestManagerInstance>();;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try{
			prest = conn.prepareStatement("SELECT * FROM TJN_PRJ_TM_ATTRIBUTES WHERE PRJ_ID=?");
			prest.setInt(1, projectId);
			rs = prest.executeQuery();
			
			while(rs.next()){
				TestManagerInstance tmInstance = new TestManagerInstance();
				tmInstance.setTjnField(rs.getString("TJN_FIELD_NAME"));
				tmInstance.setTmField(rs.getString("TM_FIELD_NAME"));
				tmInstances.add(tmInstance);
				
			}
			
		}catch(Exception e){
			logger.error("ERROR occured while fetching the TM Instances credentials", e);
			throw new DatabaseException("Could not fetch all test manager instances ", e);
		}
		 finally {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(prest);
				DatabaseHelper.close(conn);
			}
		return tmInstances;
	}
	
}
