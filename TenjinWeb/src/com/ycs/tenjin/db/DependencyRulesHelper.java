/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DependencyRulesHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 09-05-2018           Padmavathi              TENJINCG-645 & TENJINCG-646 
* 14-05-2018           Padmavathi              TENJINCG-645 & TENJINCG-646
* 07-06-2018           Padmavathi              Added methods for after deleting step, updating dependent step Id. 
* 16-10-2018		   Ashiki				   TENJINCG-881
* 24-10-2018			Preeti					TENJINCG-850
* 25-10-2018			Preeti					TENJINCG-850
* 08-01-2019           Padmavathi              TJN252-67
* 17-01-2019		   Ashiki				   TJN252-45
* 31-03-2021           Paneendra               TENJINCG-1267
*/
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.project.DependencyRules;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.util.Constants;

public class DependencyRulesHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(DependencyRulesHelper.class);
	
	public void persistDependencyRules(DependencyRules rules,ArrayList<TestStep> steps) throws DatabaseException{
		
		Connection conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if(conn==null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement ps=null;
		PreparedStatement ps1=null;
		ResultSet rs=null;
		try {
			ps = conn.prepareStatement("SELECT coalesce(MAX(RULE_ID),0) AS RULE_ID FROM TC_TSTEP_DEPENDENCY_RULES");
			rs = ps.executeQuery();
			int recId = 0;
			int dependentRecId=0;
			while(rs.next()){
				recId = rs.getInt("RULE_ID");
			}
			recId++;
			
			logger.info("inserting dependency rules");
			
			ps1=conn.prepareStatement("INSERT INTO TC_TSTEP_DEPENDENCY_RULES (RULE_ID,TC_REC_ID,TSTEP_REC_ID,DEPENDENT_TSTEP_REC_ID,EXEC_STATUS,DEPENDENCY_TYPE) VALUES(?,?,?,?,?,?)");
			for(TestStep step:steps){
				if(step.getSequence()==1){
					dependentRecId=step.getRecordId();
					continue;
				}
				ps1.setInt(1, recId);
				ps1.setInt(2, rules.getTcRecId());
				ps1.setInt(3,step.getRecordId());
				ps1.setInt(4, dependentRecId);
				ps1.setString(5, rules.getExecStatus());
				ps1.setString(6, rules.getDependencyType());
				ps1.addBatch();
				recId++;
				dependentRecId=step.getRecordId();
			}
			ps1.executeBatch();
			
		} catch (SQLException e) {
			
			logger.error("Could not insert dependency rules due to "+e.getMessage());
			throw new DatabaseException("Could not insert dependency rules");
		}finally{
			/*Added by paneendra for TENJINCG-1267 starts*/
			DatabaseHelper.close(rs);
			/*Added by paneendra for TENJINCG-1267 ends*/
			DatabaseHelper.close(ps);
			DatabaseHelper.close(ps1);
			DatabaseHelper.close(conn);
		}
	}
	
	/*Modified by Preeti for TENJINCG-850 starts*/
	public void persistDependencyRules(DependencyRules rules) throws DatabaseException{
		
		try(Connection conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);){
			this.persistDependencyRules(conn, rules);
		} catch (SQLException e) {
			
			logger.error("Could not insert dependency rules due to "+e.getMessage());
			throw new DatabaseException("Could not insert dependency rules");
		}
	}
	/*Modified by Preeti for TENJINCG-850 ends*/
	
	/*Added by Preeti for TENJINCG-850 starts*/
	public void persistDependencyRules(Connection conn, DependencyRules rules) throws DatabaseException{
	try (
			PreparedStatement ps = conn.prepareStatement("SELECT coalesce(MAX(RULE_ID),0) AS RULE_ID FROM TC_TSTEP_DEPENDENCY_RULES");
		){
		
		try(ResultSet rs = ps.executeQuery();){
		int recId = 0;
		while(rs.next()){
			recId = rs.getInt("RULE_ID");
		}
		recId++;
		
		logger.info("inserting dependency rules");
		try(
				PreparedStatement ps1 = conn.prepareStatement("INSERT INTO TC_TSTEP_DEPENDENCY_RULES (RULE_ID,TC_REC_ID,TSTEP_REC_ID,DEPENDENT_TSTEP_REC_ID,EXEC_STATUS,DEPENDENCY_TYPE) VALUES(?,?,?,?,?,?)");
			){
		
				ps1.setInt(1, recId);
				ps1.setInt(2, rules.getTcRecId());
				ps1.setInt(3,rules.getStepRecId());
				ps1.setInt(4, rules.getDependentStepRecId());
				ps1.setString(5, rules.getExecStatus());
				ps1.setString(6, rules.getDependencyType());
				ps1.execute();
			}
		}
		} catch (SQLException e) {
			
			logger.error("Could not insert dependency rules due to "+e.getMessage());
			throw new DatabaseException("Could not insert dependency rules");
		}
	}
	/*Added by Preeti for TENJINCG-850 ends*/
	
      public void updateDependencyType(int tcRecId) throws DatabaseException{
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
			if(conn == null){
				throw new DatabaseException("Invalid or no database connection");
			}
			PreparedStatement pst=null;
			PreparedStatement ps=null;
			try {
				
				ps=conn.prepareStatement("UPDATE TESTCASES SET TC_PRESET_RULES=? WHERE TC_REC_ID=?");
				ps.setString(1, "");
    			ps.setInt(2,tcRecId);
    			ps.execute();
			
    			logger.info("updating dependency type to teststep in Tc_tstep_dependency_rules table ");
			
			
    			pst=conn.prepareStatement("UPDATE TC_TSTEP_DEPENDENCY_RULES SET DEPENDENCY_TYPE=? WHERE TC_REC_ID=?");
    			pst.setString(1, "TestStep");
    			pst.setInt(2,tcRecId);
			
    			pst.execute();
			} catch (SQLException e) {
				
				logger.error("Error ", e);
			}
			finally{
    			DatabaseHelper.close(pst);
    			DatabaseHelper.close(ps);
    			DatabaseHelper.close(conn);
    		}
			
      }public void updateDependentStepRecId(int stepRecId,int dependentStepRecId) throws DatabaseException{
    	  Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
  		
			if(conn == null){
				throw new DatabaseException("Invalid or no database connection");
			}
			PreparedStatement ps=null;
			try {
				
				ps=conn.prepareStatement("UPDATE TC_TSTEP_DEPENDENCY_RULES SET DEPENDENT_TSTEP_REC_ID=? WHERE TSTEP_REC_ID=?");
				ps.setInt(1, dependentStepRecId);
				ps.setInt(2,stepRecId);
				ps.execute();
				
			} catch (SQLException e) {
					
					logger.error("Error ", e);
			}
				finally{
	    			DatabaseHelper.close(ps);
	    			DatabaseHelper.close(conn);
	    		}
      }
      public void deleteDependencyRulesAtTestCaseLevel(int tcRecId) throws DatabaseException{
   	   Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
   		
   		if(conn == null){
   			throw new DatabaseException("Invalid or no database connection");
   		}
   		PreparedStatement pst=null;
   		try{
   			logger.info("deleting DependencyRules at testcase level for " +tcRecId);
   			/*Modified by Ashiki for TENJINCG-881 starts*/
   			/*pst = conn.prepareStatement("DELETE FROM TC_TSTEP_DEPENDENCY_RULES WHERE TC_REC_ID=?AND DEPENDENCY_TYPE='TestCase'");*/
   			pst = conn.prepareStatement("DELETE FROM TC_TSTEP_DEPENDENCY_RULES WHERE TC_REC_ID=? AND DEPENDENCY_TYPE='TestCase'");
   			/*Modified by Ashiki for TENJINCG-881 ends*/
   			pst.setInt(1, tcRecId);
   			pst.execute();
   		}catch(Exception e){
   			logger.error("Could not delete Dependency Rules",e);
   			throw new DatabaseException("Could not delete Dependency Rules for "+tcRecId,e);
   		}finally{
   			DatabaseHelper.close(pst);
   			DatabaseHelper.close(conn);
   		}
      }
      
      public void deleteDependencyRulesForTestCase(int tcRecId) throws DatabaseException{
      	   Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
      		
      		if(conn == null){
      			throw new DatabaseException("Invalid or no database connection");
      		}
      		PreparedStatement pst=null;
      		try{
      			logger.info("deleting DependencyRules at testcase level for " +tcRecId);
      			
      			pst = conn.prepareStatement("DELETE FROM TC_TSTEP_DEPENDENCY_RULES WHERE TC_REC_ID=?");
      			pst.setInt(1, tcRecId);
      			pst.execute();
      		}catch(Exception e){
      			logger.error("Could not delete Dependency Rules",e);
      			throw new DatabaseException("Could not delete Dependency Rules for "+tcRecId,e);
      		}finally{
      			DatabaseHelper.close(pst);
      			DatabaseHelper.close(conn);
      		}
         }
      /*Added by Ashiki for TJN252-45 starts*/
      public void deleteDependencyRulesForTestCase(int tcRecId,Connection conn) throws DatabaseException{
     	   
     		if(conn == null){
     			throw new DatabaseException("Invalid or no database connection");
     		}
     		try(PreparedStatement pst = conn.prepareStatement("DELETE FROM TC_TSTEP_DEPENDENCY_RULES WHERE TC_REC_ID=?");){
     			pst.setInt(1, tcRecId);
     			pst.execute();
     		}catch(Exception e){
     			throw new DatabaseException("Could not delete Teststeps Rules for "+tcRecId,e);
     		}
        }
      /*Added by Ashiki for TJN252-45 ends*/
      
      public void deleteDependencyRulesForTestStep(int stepRecId) throws DatabaseException{
     	   Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
     		
     		if(conn == null){
     			throw new DatabaseException("Invalid or no database connection");
     		}
     		PreparedStatement pst=null;
     		try{
     			logger.info("deleting DependencyRules at testcase level for " +stepRecId);
     			
     			pst = conn.prepareStatement("DELETE FROM TC_TSTEP_DEPENDENCY_RULES WHERE TSTEP_REC_ID=?");
     			pst.setInt(1, stepRecId);
     			pst.execute();
     		}catch(Exception e){
     			logger.error("Could not delete Dependency Rules",e);
     			throw new DatabaseException("Could not delete Dependency Rules for "+stepRecId,e);
     		}finally{
     			DatabaseHelper.close(pst);
     			DatabaseHelper.close(conn);
     		}
        }
      public void deleteDependentTestStepRecIdRules(int stepRecId) throws DatabaseException{
      	   Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
      		
      		if(conn == null){
      			throw new DatabaseException("Invalid or no database connection");
      		}
      		PreparedStatement pst=null;
      		try{
      			logger.info("deleting DependencyRules at testcase level for " +stepRecId);
      			
      			pst = conn.prepareStatement("DELETE FROM TC_TSTEP_DEPENDENCY_RULES WHERE TSTEP_REC_ID IN ((SELECT TSTEP_REC_ID FROM TC_TSTEP_DEPENDENCY_RULES WHERE DEPENDENT_TSTEP_REC_ID =?),?)");
      			pst.setInt(1, stepRecId);
      			pst.setInt(2, stepRecId);
      			pst.execute();
      		}catch(Exception e){
      			logger.error("Could not delete Dependency Rules",e);
      			throw new DatabaseException("Could not delete Dependency Rules for "+stepRecId,e);
      		}finally{
      			DatabaseHelper.close(pst);
      			DatabaseHelper.close(conn);
      		}
         }
   
	/*Modified by Preeti for TENJINCG-850 starts*/
   public DependencyRules hydrateDependencyRules(int stepRecId) throws DatabaseException{
	   DependencyRules rules=null;
	   try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);){
		   rules = this.hydrateDependencyRules(conn, stepRecId);
	   }
		catch(Exception e){
			logger.error("Could not hydrate Dependency Rules",e);
			throw new DatabaseException("Could not hydrate Dependency Rules",e);
		}
		return rules;
   }
   
   public DependencyRules hydrateDependencyRules(Connection conn, int stepRecId) throws DatabaseException{

		DependencyRules rules=null;
		try(
				PreparedStatement pst = conn.prepareStatement("SELECT * FROM TC_TSTEP_DEPENDENCY_RULES WHERE TSTEP_REC_ID=?");
			){
			logger.info("hydrating DependencyRules for " +stepRecId);
			
			pst.setInt(1, stepRecId);
			try(ResultSet rs = pst.executeQuery();){
			while(rs.next()){
				rules=this.mapFields(rs);
			}
		}
		}catch(Exception e){
			logger.error("Could not hydrate Dependency Rules",e);
			throw new DatabaseException("Could not hydrate Dependency Rules",e);
		}
		return rules;
   }
   /*Modified by Preeti for TENJINCG-850 starts*/
   
   public int hydrateStepRecIdByDependentStepId(int DependentStepId) throws DatabaseException{
	   Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst=null;
		ResultSet rs=null;
		int stepRecId=0;
		try{
			logger.info("hydrating DependencyRules for " +DependentStepId);
			pst = conn.prepareStatement("SELECT TSTEP_REC_ID FROM TC_TSTEP_DEPENDENCY_RULES WHERE DEPENDENT_TSTEP_REC_ID=?");
			pst.setInt(1, DependentStepId);
			rs = pst.executeQuery();
			while(rs.next()){
				stepRecId=rs.getInt("TSTEP_REC_ID");
			}
		}catch(Exception e){
			logger.error("Could not hydrate Dependency Rules",e);
			throw new DatabaseException("Could not hydrate Dependency Rules",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return stepRecId;
   }
   public String checkDependencyRules(int tcRecId) throws DatabaseException{
	   Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst=null;
		ResultSet rs=null;
		String ruleType=null;
		try{
			logger.info("hydrating DependencyRules for " +tcRecId);
			pst = conn.prepareStatement("SELECT * FROM TC_TSTEP_DEPENDENCY_RULES WHERE TC_REC_ID=? ");
			pst.setInt(1, tcRecId);
			rs = pst.executeQuery();
			while(rs.next()){
				ruleType=rs.getString("DEPENDENCY_TYPE");
				break;
			}
		}catch(Exception e){
			logger.error("Could not hydrate Dependency Rules",e);
			throw new DatabaseException("Could not hydrate Dependency Rules",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return ruleType;
   }
   private DependencyRules mapFields(ResultSet rs) throws SQLException{
	   DependencyRules rules=new DependencyRules();
		rules.setRuleId(rs.getInt("RULE_ID"));
		rules.setTcRecId(rs.getInt("TC_REC_ID"));
		rules.setStepRecId(rs.getInt("TSTEP_REC_ID"));
		rules.setDependentStepRecId(rs.getInt("DEPENDENT_TSTEP_REC_ID"));
		rules.setExecStatus(rs.getString("EXEC_STATUS"));
		rules.setDependencyType(rs.getString("DEPENDENCY_TYPE"));
	   
	return rules;
	   
   }
}
