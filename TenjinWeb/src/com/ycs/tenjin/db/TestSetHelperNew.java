/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestSetHelperNew.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 10-04-2018		    Pushpalatha			   	Newly Added 
* 17-04-2018			Preeti					TENJINCG-638
  14-05-2018			Pushpalatha				TENJINCG-629
  28-May-2018	  		Pushpa					TENJINCG-386
  14-06-2018            Padmavathi              T251IT-20
  27-09-2018			Pushpa					TENJINCG-740
  08-10-2018			Pushpa					TENJINCG-844
  23-10-2018			Preeti					TENJINCG-848
  18-02-2019            Padmavathi              TENJINCG-973  
  19-02-2019			Preeti					TENJINCG-969
  10-10-2019            Padmavathi              TJN27-47
  13-01-2020			Prem					Script fix
  31-03-2021            Paneendra               TENJINCG-1267
*/
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class TestSetHelperNew {
	
	private static final Logger logger = LoggerFactory.getLogger(TestCaseHelperNew.class);
	private final Map<String, String> fieldToColumnMap = new TreeMap<>();
	
	public TestSetHelperNew() {
		this.fieldToColumnMap.put("id", "TS_REC_ID");
		this.fieldToColumnMap.put("name", "TS_NAME");
		this.fieldToColumnMap.put("type", "TS_TYPE");
		this.fieldToColumnMap.put("createdOn", "TS_CREATED_ON");
		this.fieldToColumnMap.put("createdBy", "TS_CREATED_BY");
		this.fieldToColumnMap.put("mode", "TS_EXEC_MODE");
		
	}

	public List<TestSet> hydrateAllTestSets(int projectId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst =null;
		PreparedStatement pst1 =null;
		ResultSet rs=null;
		ResultSet rs1=null;
		ArrayList<TestSet> testSets = null;
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		try{	
			/*Modified by Padmavathi for T251IT-20 starts*/
			pst = conn.prepareStatement("SELECT * FROM TESTSETS WHERE TS_PRJ_ID = ? AND TS_REC_TYPE='TS' ORDER BY TS_REC_ID");
			/*Modified by Padmavathi for T251IT-20 ends*/
			pst.setInt(1,projectId);
			rs = pst.executeQuery();
			testSets = new ArrayList<TestSet>();
			while(rs.next()){
				TestSet ts = new TestSet();
				/*Modified by Prem for scrpiting fix starts*/
				ts.setName(Utilities.escapeXml(rs.getString("TS_NAME")));
				/*Modified by Prem for scrpiting fix End*/
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setType(rs.getString("TS_TYPE"));
				ts.setDescription(rs.getString("TS_DESC"));
				ts.setProject(rs.getInt("TS_PRJ_ID"));
				ts.setPriority(rs.getString("TS_PRIORITY"));
				ts.setParent(rs.getInt("TS_FOLDER_ID"));
				ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
				ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
				ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
				ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
				ts.setStatus(rs.getString("TS_STATUS"));
				ts.setMode(rs.getString("TS_EXEC_MODE"));
				/*Added by Padmavathi for TENJINCG-973 Starts*/
				pst1 = conn.prepareStatement("SELECT A.RUN_ID, B.RUN_STATUS FROM "
				+ "MASTESTRUNS A, V_RUNRESULTS B WHERE B.RUN_ID = A.RUN_ID AND "
				+ "A.RUN_ID = (SELECT MAX(RUN_ID) FROM MASTESTRUNS WHERE RUN_PRJ_ID=? AND RUN_TS_REC_ID =? AND RUN_STATUS IN('Complete','Aborted'))");
				
				pst1.setInt(1, projectId);
				pst1.setInt(2, ts.getId());
				
				rs1=pst1.executeQuery();
				while(rs1.next()){
					ts.setLastRunId(rs1.getInt("RUN_ID"));
					ts.setLastRunStatus(rs1.getString("RUN_STATUS"));
				}
				/*Added by Padmavathi for TENJINCG-973 ends*/
				testSets.add(ts);
			}
		}catch(Exception e){
			logger.error("Could not fetch all test sets",e);
			throw new DatabaseException("Could not fetch Test Sets due to an internal error",e);
		}finally{
			/*Added by Padmavathi for TENJINCG-973 Starts*/
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			/*Added by Padmavathi for TENJINCG-973 ends*/
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return testSets;
	}
	
	public TestSet persistTestSet(TestSet testSet, int projectId) throws DatabaseException,DuplicateRecordException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		Statement st = null;
		PreparedStatement pst =null;
		PreparedStatement pst1 =null;
		ResultSet rs =null;
		ResultSet tRs = null;
		if(conn == null){
			throw new DatabaseException("Invalid or No database connection");
		}
		
		try{
			
			pst1=conn.prepareStatement("SELECT COUNT(*) as ts_cnt FROM TESTSETS WHERE TS_NAME = ? AND TS_PRJ_ID = ?");
			pst1.setString(1, testSet.getName());
			pst1.setInt(2, projectId);
			tRs=pst1.executeQuery();
			boolean duplicate = false;
			int tsCount = 0;
			while(tRs.next()){
				tsCount= tRs.getInt("TS_CNT");
			}
			
			if(tsCount > 0){
				duplicate = true;
			}
			
			if(duplicate){
				throw new DuplicateRecordException("Record arleady exists");
			}
			/*Modified by Preeti for TENJINCG-969 starts*/
			testSet.setId(DatabaseHelper.getGlobalSequenceNumber(conn));
			
						pst = conn.prepareStatement("INSERT INTO TESTSETS (TS_REC_ID,TS_REC_TYPE,TS_NAME,TS_FOLDER_ID,TS_PRJ_ID,TS_DESC,TS_CREATED_ON,TS_CREATED_BY,TS_TYPE,TS_STATUS,TS_EXEC_MODE,TS_PRIORITY) "
								+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
						
								
			pst.setInt(1, testSet.getId());
			/*Modified by Preeti for TENJINCG-969 ends*/
			pst.setString(2, testSet.getRecordType());
			pst.setString(3, testSet.getName());
			pst.setInt(4, testSet.getParent());
			pst.setInt(5,projectId);
			pst.setString(6,testSet.getDescription());
			Timestamp timeStamp = new Timestamp(new Date().getTime());	
			pst.setTimestamp(7, timeStamp);
			testSet.setCreatedOn(timeStamp);
			pst.setString(8,testSet.getCreatedBy());
			pst.setString(9, testSet.getType());
			pst.setString(10, testSet.getStatus());
			pst.setString(11, testSet.getMode());
			pst.setString(12, testSet.getPriority());
			pst.execute();
		}catch(Exception e){
			logger.error("Could not persist test set",e);
			throw new DatabaseException("Could not create record",e);
		}finally{

			DatabaseHelper.close(rs);
			DatabaseHelper.close(st);
			DatabaseHelper.close(tRs);
			DatabaseHelper.close(pst);
			 /*Added by paneendra for TENJINCG-1267 starts*/
			DatabaseHelper.close(pst1);
			/*Added by paneendra for TENJINCG-1267 ends*/
			DatabaseHelper.close(conn);
		}
		return testSet;
		
	}
	
	
	public void updateTestSet(TestSet testset) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst =null;
		if(conn == null){
			throw new DatabaseException("Invalid or No database connection");
		}
		
		try{
			pst = conn.prepareStatement("UPDATE TESTSETS SET TS_NAME = ?, TS_DESC = ?, TS_TYPE = ?, TS_PRIORITY =?,TS_EXEC_MODE=?  where TS_REC_ID = ?" );
			pst.setString(1, testset.getName());
			pst.setString(2, testset.getDescription());
			pst.setString(3, testset.getType());
			pst.setString(4, testset.getPriority());
			pst.setString(5, testset.getMode());
			pst.setInt(6, testset.getId());
			
			pst.execute();
		}catch(Exception e){
			throw new DatabaseException("Could not update test set due to an internal error",e);
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
			
		}
		
		
	}
	
	public String delete_testsets(String[] testsetids) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement prepared=null;
		PreparedStatement prest=null;
		 Statement statement = null;
		 ResultSet rs=null;;
		 ResultSet rst=null;
		String returnval="";
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		  
		try {
			   ArrayList<String> queries=new ArrayList<String>();
			   statement = conn.createStatement();
			   int records_exists=0;
				 
				for(int i=0;i<testsetids.length;i++)
				{
				try{
					/*modified by shruthi for VAPT Helper fix starts*/
					prepared=conn.prepareStatement("select 1 FROM mastestruns WHERE RUN_TS_REC_ID=? and RUN_STATUS='Executing'");
					prepared.setString(1,testsetids[i]);
					/*modified by shruthi for VAPT Helper fix ends*/
					rst=prepared.executeQuery();
				    if(rst.next())
				    {
				      records_exists=rst.getInt(1);	
				      returnval="TestSet "+testsetids[i]+"is in Execution Mode";
				      break;
				    }
				if(records_exists==0){ 
					/*modified by shruthi for VAPT Helper fix starts*/
				  prest = conn.prepareStatement("SELECT RUN_ID FROM MASTESTRUNS WHERE RUN_TS_REC_ID =?");
				  prest.setString(1,testsetids[i]);
				  /*modified by shruthi for VAPT Helper fix ends*/
				  rs = prest.executeQuery();
				   while(rs.next())
				   {
					 int runid=rs.getInt("RUN_ID");
					 queries.add("delete from RUNRESULT_TS_TXN where run_id='"+runid+"'");
					 queries.add("delete from RESVALRESULTS where run_id='"+runid+"'");
					 queries.add("delete from RUNRESULTS_TS where run_id='"+runid+"'");
					 queries.add("delete from RUNRESULTS_TC where run_id='"+runid+"'");
				   } 
				   
				   queries.add("delete from MASTESTRUNS where RUN_TS_REC_ID='"+testsetids[i]+"'");
				   queries.add("delete from TESTSETMAP where TSM_TS_REC_ID='"+testsetids[i]+"'");
				   queries.add("delete from TESTSETS where TS_REC_ID='"+testsetids[i]+"'");
				  
				}
				}finally{
					DatabaseHelper.close(rst);
					DatabaseHelper.close(prepared);
				}
				}
			   if(records_exists==0){
				for(String query:queries)
				{
					statement.addBatch(query);
				}
				statement.executeBatch();
				//conn.commit();
				
			   }
		} catch (Exception e) {
			if(conn!=null)
				try {
					conn.rollback();
				} catch (SQLException e1) {
					
					logger.error("Error ", e1);
				}
			throw new DatabaseException("Could not delete record", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(prest);
			DatabaseHelper.close(rst);
			DatabaseHelper.close(prepared);
			DatabaseHelper.close(statement);
			DatabaseHelper.close(conn);
		}
	 return returnval;
	   
	}
	
	public ArrayList<TestCase> hydrateMappedTCs(int testSetId, int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst =null;
		ResultSet rs=null;
		if(conn == null){
			throw new DatabaseException("Invalid or No database connection");
		}
		
		ArrayList<TestCase> testCases = null;
		
		try{
			pst = conn.prepareStatement("SELECT A.TSM_TC_SEQ, A.TSM_TC_REC_ID, B.TC_ID FROM TESTSETMAP A, TESTCASES B WHERE B.TC_REC_ID = A.TSM_TC_REC_ID AND A.TSM_PRJ_ID = ? AND A.TSM_TS_REC_ID = ? ORDER BY A.TSM_TC_SEQ ");
			pst.setInt(1, projectId);
			pst.setInt(2, testSetId);
			rs = pst.executeQuery();
			testCases = new ArrayList<TestCase>();
			while(rs.next()){
				TestCase tc = new TestCase();
				tc.setTcRecId(rs.getInt("TSM_TC_REC_ID"));
				tc = new TestCaseHelper().hydrateTestCase(projectId, tc.getTcRecId());
				testCases.add(tc);
			}
		}catch(Exception e){
			logger.error("Could not hydrate mapped test cases for test set " + testSetId,e);
			throw new DatabaseException("Could not get mapped test cases",e);
		}finally{

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return testCases;
	}
	


public TestSet hydrateTestSet(int projectId,int testSetRecordId) throws DatabaseException {
	Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
	Connection appConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
	PreparedStatement pst =null;
	PreparedStatement pst2 =null;
	ResultSet rs =null;
	ResultSet rTc = null;
	if(conn == null || appConn == null){
		throw new DatabaseException("Invalid or no database connection");
	}
	/*Added by pushpalatha for TENJINCG-386 starts */
	TestCaseHelperNew tcHelper = new TestCaseHelperNew();
	/*Added by pushpalatha for TENJINCG-386 end */
	
	TestSet testset = null;
	
	try {
		/*Modified by Preeti for TENJINCG-969 starts*/
		pst = conn.prepareStatement("SELECT * FROM TESTSETS T LEFT JOIN (SELECT * FROM TJN_AUDIT WHERE AUDIT_ID=(SELECT MAX(AUDIT_ID) FROM TJN_AUDIT WHERE ENTITY_TYPE=? AND ENTITY_RECORD_ID = ?)) A ON A.ENTITY_RECORD_ID = T.TS_REC_ID WHERE T.TS_REC_ID = ? AND T.TS_PRJ_ID = ?");
		pst.setString(1, "testset");
		pst.setInt(2, testSetRecordId);
		pst.setInt(3, testSetRecordId);
		pst.setInt(4, projectId);
		rs = pst.executeQuery();
		
		ArrayList<TestCase> testCases = null;
		while(rs.next()){
			
			testset=this.mapFieldsWithAudit(rs);
			/*Modified by Preeti for TENJINCG-969 ends*/
			
			pst2 = conn.prepareStatement("SELECT TSM_TC_REC_ID FROM TESTSETMAP WHERE TSM_PRJ_ID = ? AND TSM_TS_REC_ID = ? ORDER BY TSM_TC_SEQ");
			pst2.setInt(1, projectId);
			pst2.setInt(2,testset.getId());
			rTc = pst2.executeQuery();
			testCases = new ArrayList<TestCase>();
			while(rTc.next()){
				int tcId = rTc.getInt("TSM_TC_REC_ID");
                /*Added by pushpalatha for TENJINCG-386 starts */
				/*Changed by Pushpalatha for TENJINCG-844 starts*/
				TestCase tc = tcHelper.hydrateTestCasesForSet(projectId, tcId);
				/*Changed by Pushpalatha for TENJINCG-844 ends*/
				/*Added by pushpalatha for TENJINCG-386 ends */
				testCases.add(tc);
			}
			
			testset.setTests(testCases);//Below if condition commented by Sriram to allow fetching of test case details for Ad-HOc test sets too (to help automatic redirection to Test Case execution history) - For TENJINCG-876 ends
		}
	} catch (SQLException e) {
		
		logger.error("Exception in hydrateTestSetBasicDetails(int tsId, int projectId)");
		logger.error("Could not fetch details of Test Set",e);
		throw new DatabaseException("Could not fetch Test Set details");
	}finally{
		DatabaseHelper.close(rTc);
		DatabaseHelper.close(pst2);
		DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);
		DatabaseHelper.close(appConn);
	}
	
	return testset;

}
/*Changed by Pushpa for TENJINCG-740 starts*/
public TestSet hydrateTestSet(String tsName, int projectId) throws DatabaseException{
	
	TestSet ts = null;
	TestCaseHelper tcHelper = new TestCaseHelper();
	try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = conn.prepareStatement("SELECT * FROM TESTSETS WHERE upper(TS_NAME) = ? AND TS_PRJ_ID = ?");){
		
		pst.setString(1, tsName.toUpperCase());
		pst.setInt(2, projectId);
		
		
		try(ResultSet rs = pst.executeQuery();){
		while(rs.next()){
			
			ts = new TestSet();
			ts.setName(rs.getString("TS_NAME"));
			ts.setRecordType(rs.getString("TS_REC_TYPE"));
			ts.setId(rs.getInt("TS_REC_ID"));
			ts.setType(rs.getString("TS_TYPE"));
			ts.setParent(rs.getInt("TS_FOLDER_ID"));
			ts.setCreatedBy(rs.getString("TS_CREATED_BY"));
			
			ts.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
			ts.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
			ts.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
			ts.setStatus(rs.getString("TS_STATUS"));
			ts.setMode(rs.getString("TS_EXEC_MODE"));
			ArrayList<TestCase> testCases = null;
			if(ts.getRecordType().equalsIgnoreCase("TS")){
				try(PreparedStatement pst2 = conn.prepareStatement("SELECT TSM_TC_REC_ID FROM TESTSETMAP WHERE TSM_PRJ_ID = ? AND TSM_TS_REC_ID = ?");){

					pst2.setInt(1, projectId);
					pst2.setInt(2,ts.getId());
					try(ResultSet rTc = pst2.executeQuery();){
						testCases = new ArrayList<TestCase>();
						while(rTc.next()){
							int tcId = rTc.getInt("TSM_TC_REC_ID");
							TestCase tc = tcHelper.hydrateTestCase(tcId, projectId);
							testCases.add(tc);
						}
					}
				}
			}
			
			ts.setTests(testCases);
		
		}
		}
	}catch(Exception e){
		logger.error("Could not hydrate test set with Name " + tsName + " under project " + projectId, e);
		throw new DatabaseException("Could not fetch details of test set",e);
	}
	return ts;
}
/*Changed by Pushpa for TENJINCG-740 Ends*/
/*Added by Preeti for TENJINCG-638 starts*/
public ArrayList<TestRun> hydrateTestSetExecutionHistory(int projectId, int testSetRecordId) throws DatabaseException{
	Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);	
	PreparedStatement pst =null;
	ResultSet rs =null;
	if(conn == null){
		throw new DatabaseException("Invalid Database Connection");
	}
	ArrayList<TestRun> summary = null;

	try {
		pst = conn.prepareStatement("SELECT A.RUN_ID,A.RUN_TS_REC_ID,A.RUN_START_TIME,A.RUN_END_TIME,A.RUN_USER,C.RUN_STATUS,C.TOTAL_TCS,C.TOTAL_EXE_TCS,C.TOTAL_PASSED_TCS,C.TOTAL_FAILED_TCS,C.TOTAL_ERROR_TCS, B.TS_NAME FROM MASTESTRUNS A, V_RUNRESULTS C, TESTSETS B WHERE B.TS_REC_ID = A.RUN_TS_REC_ID AND C.RUN_ID=A.RUN_ID AND A.RUN_TS_REC_ID = ? and A.run_prj_id = ? order by run_id desc");
		pst.setInt(1, testSetRecordId);
		pst.setInt(2, projectId);

		rs = pst.executeQuery();
		summary = new ArrayList<TestRun>();
		while(rs.next()){
			TestRun run = new TestRun();
			run.setId(rs.getInt("RUN_ID"));
			run.setStartTimeStamp(rs.getTimestamp("RUN_START_TIME"));
			run.setEndTimeStamp(rs.getTimestamp("RUN_END_TIME"));
			run.setUser(rs.getString("RUN_USER"));
			run.setTestSetRecordId(rs.getInt("RUN_TS_REC_ID"));
			run.setStatus(rs.getString("RUN_STATUS"));
			if(run.getStartTimeStamp() != null && run.getEndTimeStamp() != null){
				long startMillis = run.getStartTimeStamp().getTime();
				long endMillis = run.getEndTimeStamp().getTime();

				long millis = endMillis - startMillis;

				String eTime = String.format(
						"%02d:%02d:%02d",
						TimeUnit.MILLISECONDS.toHours(millis),
						TimeUnit.MILLISECONDS.toMinutes(millis)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
								.toHours(millis)),
						TimeUnit.MILLISECONDS.toSeconds(millis)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
								.toMinutes(millis)));
				run.setElapsedTime(eTime);
			}

			run.setTotalTests(rs.getInt("TOTAL_TCS"));
			run.setExecutedTests(rs.getInt("TOTAL_EXE_TCS"));
			run.setPassedTests(rs.getInt("TOTAL_PASSED_TCS"));
			run.setFailedTests(rs.getInt("TOTAL_FAILED_TCS"));
			run.setErroredTests(rs.getInt("TOTAL_ERROR_TCS"));
			TestSet t = new TestSet();
			t.setId(run.getTestSetRecordId());
			t.setName(rs.getString("TS_NAME"));
			run.setTestSet(t);
			summary.add(run);
		}

	} catch (SQLException e) {
		throw new DatabaseException("Could not fetch execution history for test set " + testSetRecordId,e);
	}finally{
		DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);
	}

	return summary;
}
/*Added by Preeti for TENJINCG-638 ends*/
/*Added by Pushpalath for TENJINCG-629 starts*/
public ArrayList<TestRun> hydrateTestSetExecutionHistoryForEscMail(int projectId, int testSetRecordId) throws DatabaseException{
	Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);	
	PreparedStatement pst =null;
	ResultSet rs =null;
	if(conn == null){
		throw new DatabaseException("Invalid Database Connection");
	}
	ArrayList<TestRun> summary = null;

	try {
		pst = conn.prepareStatement("SELECT A.RUN_ID,A.RUN_TS_REC_ID,A.RUN_START_TIME,A.RUN_END_TIME,A.RUN_USER,C.RUN_STATUS,C.TOTAL_TCS,C.TOTAL_EXE_TCS,C.TOTAL_PASSED_TCS,C.TOTAL_FAILED_TCS,C.TOTAL_ERROR_TCS, B.TS_NAME,B.TS_REC_TYPE FROM MASTESTRUNS A, V_RUNRESULTS C, TESTSETS B WHERE B.TS_REC_ID = A.RUN_TS_REC_ID AND C.RUN_ID=A.RUN_ID AND A.RUN_TS_REC_ID = ? and A.run_prj_id = ? order by run_id desc");
		pst.setInt(1, testSetRecordId);
		pst.setInt(2, projectId);

		rs = pst.executeQuery();
		summary = new ArrayList<TestRun>();
		while(rs.next()){
			TestRun run = new TestRun();
			run.setId(rs.getInt("RUN_ID"));
			
			run.setTestSetRecordId(rs.getInt("RUN_TS_REC_ID"));
			run.setStatus(rs.getString("RUN_STATUS"));
			
			TestSet t = new TestSet();
			t.setId(run.getTestSetRecordId());
			t.setName(rs.getString("TS_NAME"));
			t.setRecordType(rs.getString("TS_REC_TYPE"));
			run.setTestSet(t);
			summary.add(run);
		}

	} catch (SQLException e) {
		throw new DatabaseException("Could not fetch execution history for test set " + testSetRecordId,e);
	}finally{
		DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);
	}

	return summary;
}
/*Added by Pushpalath for TENJINCG-629 ends*/
private TestSet mapFields(ResultSet rs) throws SQLException {
	
	try {
		TestSet testSet=new TestSet();
		testSet.setId(rs.getInt("TS_REC_ID"));
		testSet.setRecordType(rs.getString("TS_REC_TYPE"));
		testSet.setName(Utilities.escapeXml(rs.getString("TS_NAME")));
		testSet.setProject(rs.getInt("TS_PRJ_ID"));
		testSet.setDescription(rs.getString("TS_DESC"));
		
		testSet.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
		testSet.setCreatedBy(rs.getString("TS_CREATED_BY"));
		testSet.setType(rs.getString("TS_TYPE"));
		testSet.setStatus(rs.getString("TS_STATUS"));
		testSet.setMode(rs.getString("TS_EXEC_MODE"));
		testSet.setPriority(rs.getString("TS_PRIORITY"));
		
		testSet.setParent(rs.getInt("TS_FOLDER_ID"));
		testSet.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
		testSet.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
		
		
		return testSet;
	} catch (SQLException e) {
		logger.error("ERROR creating Test Set from ResultSet", e);
		throw e;
	}
	
}
	/*Added by Preeti for TENJINCG-969 starts*/
	private TestSet mapFieldsWithAudit(ResultSet rs) throws SQLException {
	
	try {
		TestSet testSet=new TestSet();
		testSet.setId(rs.getInt("TS_REC_ID"));
		testSet.setRecordType(rs.getString("TS_REC_TYPE"));
		testSet.setName(Utilities.escapeXml(rs.getString("TS_NAME")));
		testSet.setProject(rs.getInt("TS_PRJ_ID"));
		testSet.setDescription(Utilities.escapeXml(rs.getString("TS_DESC")));
		
		testSet.setCreatedOn(rs.getTimestamp("TS_CREATED_ON"));
		testSet.setCreatedBy(rs.getString("TS_CREATED_BY"));
		testSet.setType(rs.getString("TS_TYPE"));
		testSet.setStatus(rs.getString("TS_STATUS"));
		testSet.setMode(rs.getString("TS_EXEC_MODE"));
		testSet.setPriority(rs.getString("TS_PRIORITY"));
		
		testSet.setParent(rs.getInt("TS_FOLDER_ID"));
		testSet.setModifiedBy(rs.getString("TS_MODIFIED_BY"));
		testSet.setModifiedOn(rs.getDate("TS_MODIFIED_ON"));
		AuditRecord record = new AuditRecord();
		record.setAuditId(rs.getInt("AUDIT_ID"));
		record.setEntityRecordId(rs.getInt("ENTITY_RECORD_ID"));
		record.setEntityType(rs.getString("ENTITY_TYPE"));
		record.setLastUpdatedBy(rs.getString("LAST_MODIFIED_BY"));
		record.setLastUpdatedOn(rs.getTimestamp("LAST_MODIFIED_ON"));
		testSet.setAuditRecord(record);
		return testSet;
	} catch (SQLException e) {
		logger.error("ERROR creating Test Set from ResultSet", e);
		throw e;
	}
	
}
/*Added by Preeti for TENJINCG-969 ends*/
/*Added by Pushpalatha for TENJINCG-637 starts*/

public ArrayList<TestCase> hydrateUnMappedTCs(int testSetId, int projectId, String mode) throws DatabaseException{
	Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
	PreparedStatement pst =null;
	ResultSet rs=null;
	if(conn == null){
		throw new DatabaseException("Invalid or No database connection");
	}
	ArrayList<TestCase> testCases = null;
	try{
		pst = conn.prepareStatement("SELECT * FROM TESTCASES WHERE TC_REC_ID NOT IN (SELECT TSM_TC_REC_ID FROM TESTSETMAP WHERE TSM_PRJ_ID = ? AND TSM_TS_REC_ID = ?) and TC_PRJ_ID = ? and TC_EXEC_MODE = ?");
		pst.setInt(1, projectId);
		pst.setInt(2, testSetId);
		pst.setInt(3, projectId);
		pst.setString(4, mode);
		rs = pst.executeQuery();
		testCases = new ArrayList<TestCase>();
		while(rs.next()){
			TestCase tc = new TestCase();
			tc.setTcRecId(rs.getInt("TC_REC_ID"));
			tc.setTcId(rs.getString("TC_ID"));
			tc.setTcName(rs.getString("TC_NAME"));
			tc.setMode(rs.getString("TC_EXEC_MODE"));
			tc.setTcPriority(rs.getString("TC_PRIORITY"));
			testCases.add(tc);
		}
	}catch(Exception e){
		logger.error("Could not hydrate un_mapped test cases for test set " + testSetId,e);
		throw new DatabaseException("Could not get un_mapped test cases",e);
	}finally{

		DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);
	}
	return testCases;
}


public void persistTestSetunMap(int testSetId, String tcList, int projectId) throws DatabaseException{
	Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
	
	
	PreparedStatement pst =null;
	if(conn == null){
		throw new DatabaseException("Invalid or no database connection");
	}
	
	try{
		pst = conn.prepareStatement("DELETE FROM TESTSETMAP WHERE TSM_TS_REC_ID = ? AND TSM_PRJ_ID = ?");
		pst.setInt(1, testSetId);
		pst.setInt(2, projectId);
		pst.execute();
	}catch(Exception e){
		logger.error("Could not clear existing mapping for test set " + testSetId,e);
		throw new DatabaseException("Could not clear existing mapping for test set " + testSetId,e);
	}
	finally{
		DatabaseHelper.close(pst);
	}
	
	if(tcList.equalsIgnoreCase("")){
		
		logger.debug("No test Cases were Mapped for the testset with recordId:"+testSetId);
	
	}else{
		
	String[] tcArray = tcList.split(",");
	int i=0;
	
	for(String tc:tcArray)
	{
		if(!Utilities.trim(tc).equalsIgnoreCase("")){
		i++;
		try{
			
			int tcId = Integer.parseInt(tc);
			
			pst = conn.prepareStatement("INSERT INTO TESTSETMAP (TSM_PRJ_ID,TSM_TS_REC_ID,TSM_TC_REC_ID,TSM_TC_SEQ) VALUES(?,?,?,?)");
			
			pst.setInt(1, projectId);
			pst.setInt(2, testSetId);
			pst.setInt(3, tcId);
			pst.setInt(4, i);
			pst.execute();
		}catch(Exception e){
			logger.error("Could not map test " + tc + " to test set " + testSetId,e);
			throw new DatabaseException("Could not map test " + tc + " to test set " + testSetId,e);
		}
		finally{
			DatabaseHelper.close(pst);
		}
		}
	}
	
	}
	
	try{
		conn.close();
	}catch(Exception e){
	}finally{
		DatabaseHelper.close(conn);
	}
	
	
}

public void persistTestSetMap(int testSetId, String tcList, int projectId, boolean clearMapBeforePersisting) throws DatabaseException{
	
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		PreparedStatement pst =null;
		if(clearMapBeforePersisting) {
			try{
			  pst = conn.prepareStatement("DELETE FROM TESTSETMAP WHERE TSM_TS_REC_ID = ? AND TSM_PRJ_ID = ?");
				pst.setInt(1, testSetId);
				pst.setInt(2, projectId);
				pst.execute();
				pst.close();
			}catch(Exception e){
				logger.error("Could not clear existing mapping for test set " + testSetId,e);
				throw new DatabaseException("Could not clear existing mapping for test set " + testSetId,e);
			}finally{
				DatabaseHelper.close(pst);
			}
		}
		
		if(tcList.equalsIgnoreCase("")){
			logger.debug("No test Cases were Mapped for the testset with recordId:"+testSetId);
		}else{
			
		String[] tcArray = tcList.split(",");
		int i=0;
		ResultSet rs=null;
		
		try{
			pst = conn.prepareStatement("SELECT coalesce(MAX(TSM_TC_SEQ),0) AS TSM_TC_SEQ FROM TESTSETMAP WHERE TSM_PRJ_ID=? AND TSM_TS_REC_ID=?");
			pst.setInt(1, projectId);
			pst.setInt(2, testSetId);
			
			 rs = pst.executeQuery();
			
			while(rs.next()) {
				i = rs.getInt("TSM_TC_SEQ");
				break;
			}
			
			pst.close();
			rs.close();
		}catch(Exception e){
			logger.warn("ERROR getting latest sequence number for test case mapping", e);
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			
		}
		
		for(String tc:tcArray){
			if(!Utilities.trim(tc).equalsIgnoreCase("")){
			
			boolean tcExists = false;
			try {
				pst = conn.prepareStatement("SELECT * FROM TESTSETMAP WHERE TSM_PRJ_ID=? AND TSM_TS_REC_ID=? AND TSM_TC_rEC_ID=?");
				pst.setInt(1, projectId);
				pst.setInt(2, testSetId);
				pst.setInt(3, Integer.parseInt(tc));
				
				rs = pst.executeQuery();
				
				while(rs.next()){
					tcExists = true;
				}
				
				rs.close();
				pst.close();
			} catch (Exception e) {
				
				logger.warn("Could not check if TC_REC_ID {} exists in Test Set {}", tc, testSetId, e);
			}finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}
			
			if(!tcExists) {
				i++;
				try{
					int tcId = Integer.parseInt(tc);
					
					pst = conn.prepareStatement("INSERT INTO TESTSETMAP (TSM_PRJ_ID,TSM_TS_REC_ID,TSM_TC_REC_ID,TSM_TC_SEQ) VALUES(?,?,?,?)");
					
					pst.setInt(1, projectId);
					pst.setInt(2, testSetId);
					pst.setInt(3, tcId);
					pst.setInt(4, i);
					pst.execute();
				}catch(Exception e){
					logger.error("Could not map test " + tc + " to test set " + testSetId,e);
					throw new DatabaseException("Could not map test " + tc + " to test set " + testSetId,e);
				}
				finally{
					DatabaseHelper.close(pst);
				}
			}
			
			}
		
		}
		}
		
		try{
			conn.close();
		}catch(Exception e){
			
		}finally{
			DatabaseHelper.close(conn);
		}
	
	}

/*Added by Preeti for TENJINCG-848 starts*/
public TestSet hydrateTestSetBasicDetails(int projectId,int testSetRecordId) throws DatabaseException {
	
	TestSet testset = null;
	TestCaseHelperNew tcHelper = new TestCaseHelperNew();
	try (
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement pst = conn.prepareStatement("SELECT * FROM TESTSETS WHERE TS_REC_ID = ? AND TS_PRJ_ID = ?");
			PreparedStatement pst2 = conn.prepareStatement("SELECT TSM_TC_REC_ID FROM TESTSETMAP WHERE TSM_PRJ_ID = ? AND TSM_TS_REC_ID = ? ORDER BY TSM_TC_SEQ");
		){
		pst.setInt(1, testSetRecordId);
		pst.setInt(2, projectId);
		try (ResultSet rs = pst.executeQuery()){
		while(rs.next()){
			
		testset=this.mapFields(rs);
		if(testset.getRecordType().equalsIgnoreCase("TS")){
			pst2.setInt(1, projectId);
			pst2.setInt(2,testset.getId());
			ArrayList<TestCase> testCases = new ArrayList<TestCase>();
			try (ResultSet rs1 = pst2.executeQuery()){
				while(rs1.next()){
					int tcId = rs1.getInt("TSM_TC_REC_ID");
					TestCase tc = tcHelper.hydrateTestCaseBasicDetails(projectId, tcId, false);
					testCases.add(tc);
				}
			}
			testset.setTests(testCases);
			}
			
		}
	} 
	}catch (SQLException e) {
		
		logger.error("Exception in hydrateTestSetBasicDetails(int tsId, int projectId)");
		logger.error("Could not fetch details of Test Set",e);
		throw new DatabaseException("Could not fetch Test Set details");
	}
	return testset;

}

public ArrayList<Integer> hydrateMappedTCsId(int testSetId, int projectId) throws DatabaseException{
		
		ArrayList<Integer> testCasesId = null;
		
		try(
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement("SELECT TSM_TC_REC_ID FROM TESTSETMAP WHERE TSM_PRJ_ID = ? AND TSM_TS_REC_ID = ?");
			){
			pst.setInt(1, projectId);
			pst.setInt(2, testSetId);
			try(ResultSet rs = pst.executeQuery()){
			testCasesId = new ArrayList<Integer>();
			while(rs.next()){
				TestCase tc = new TestCase();
				tc.setTcRecId(rs.getInt("TSM_TC_REC_ID"));
				testCasesId.add(tc.getTcRecId());
			}
			}
		}catch(Exception e){
			logger.error("Could not hydrate mapped test cases for test set " + testSetId,e);
			throw new DatabaseException("Could not get mapped test cases",e);
		}
		return testCasesId;
	}

public void persistTestSetMap(int testSetId, List<Integer> tcList, int projectId) throws DatabaseException{
	int i=0;
	try(
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement pst = conn.prepareStatement("SELECT coalesce(MAX(TSM_TC_SEQ),0) AS TSM_TC_SEQ FROM TESTSETMAP WHERE TSM_PRJ_ID=? AND TSM_TS_REC_ID=?");
			PreparedStatement pst1 = conn.prepareStatement("INSERT INTO TESTSETMAP (TSM_PRJ_ID,TSM_TS_REC_ID,TSM_TC_REC_ID,TSM_TC_SEQ) VALUES(?,?,?,?)");
		){
			pst.setInt(1, projectId);
			pst.setInt(2, testSetId);
			try(ResultSet rs = pst.executeQuery();){
				while(rs.next()) {
					i = rs.getInt("TSM_TC_SEQ");
					break;
				}
			}
		for(Integer tcId:tcList){
			if(tcId>0){
					i++;
					pst1.setInt(1, projectId);
					pst1.setInt(2, testSetId);
					pst1.setInt(3, tcId);
					pst1.setInt(4, i);
					pst1.addBatch();
			}
			
			}
		pst1.executeBatch();
	}catch(Exception e){
		logger.warn("ERROR getting latest sequence number for test case mapping", e);
	}
}
/*Added by Preeti for TENJINCG-848 ends*/
/*modified by shruthi for VAPT helper fixes ends*/
public TestCase hydrateTestCase(int projectId, int testCaseRecId) throws DatabaseException{
	Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
	PreparedStatement pst = null;
	PreparedStatement pst1 = null;
	ResultSet rs = null;
	ResultSet rs1 = null;
	if(conn == null){
		throw new DatabaseException("Invalid or no database connection");
	}

	TestCase tc = null;

	try{
		pst = conn.prepareStatement("SELECT * FROM TESTCASES  WHERE TC_REC_ID =? AND TC_PRJ_ID =?");
		pst.setInt(1, testCaseRecId);
		pst.setInt(2, projectId);
		rs = pst.executeQuery();

		while(rs.next()){
			tc = new TestCase();
			tc.setTcId(rs.getString("tc_id"));
			tc.setTcName(rs.getString("tc_name"));
			tc.setTcType(rs.getString("tc_type"));
			tc.setTcStatus(rs.getString("tc_status"));
			tc.setTcCreatedBy(rs.getString("tc_created_by"));
			
			tc.setTcCreatedOn(rs.getTimestamp("tc_created_on"));
			
			tc.setTcModifiedBy(rs.getString("tc_modified_by"));
			tc.setTcModifiedOn(rs.getDate("tc_modified_on"));
			tc.setTcDesc(rs.getString("tc_desc"));
			tc.setTcRecId(rs.getInt("TC_REC_ID"));
			tc.setTcFolderId(rs.getInt("TC_FOLDER_ID"));
			tc.setProjectId(rs.getInt("TC_PRJ_ID"));
			tc.setRecordType(rs.getString("TC_REC_TYPE"));
			tc.setTcReviewed(rs.getString("TC_REVIEWED"));
			tc.setTcPriority(rs.getString("TC_PRIORITY"));
			tc.setStorage(rs.getString("TC_STORAGE"));
			tc.setMode(rs.getString("TC_EXEC_MODE"));
		}

		if (tc != null) {

			ArrayList<TestStep> tsteps = new ArrayList<TestStep>();
			pst1 = conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID =? ORDER BY TSTEP_REC_ID");
			pst1.setInt(1, tc.getTcRecId());
			rs1 = pst1.executeQuery();
			while (rs1.next()) {
				TestStep step = new TestStep();
				
				step.setRecordId(rs1.getInt("TSTEP_REC_ID"));
				step.setTestCaseRecordId(rs1.getInt("TC_REC_ID"));
				step.setShortDescription(rs1.getString("TSTEP_SHT_DESC"));
				step.setDataId(rs1.getString("TSTEP_DATA_ID"));
				step.setType(rs1.getString("TSTEP_TYPE"));
				step.setDescription(rs1.getString("TSTEP_DESC"));
				step.setExpectedResult(rs1.getString("tstep_exp_result"));
				step.setActualResult(rs1.getString("tstep_act_result"));
				step.setAppId(rs1.getInt("APP_ID"));
				step.setModuleCode(rs1.getString("FUNC_CODE"));
				step.setTransactionContext(rs1.getString("TXN_CONTEXT"));
				step.setExecutionContext(rs1.getString("EXEC_CONTEXT"));
				step.setId(rs1.getString("TSTEP_ID"));
				step.setOperation(rs1.getString("TSTEP_OPERATION"));
				step.setAutLoginType(rs1.getString("TSTEP_AUT_LOGIN_TYPE"));
				step.setTstepStorage(rs1.getString("TSTEP_STORAGE"));
				step.setResponseType((rs1.getInt("TSTEP_API_RESP_TYPE")));
				tsteps.add(step);
			}
			tc.setTcSteps(tsteps);
		}

	}catch(Exception e){
		throw new DatabaseException("Could not fetch test case details",e);
	}finally{
		
		DatabaseHelper.close(rs1);		
		DatabaseHelper.close(pst1);
		DatabaseHelper.close(rs);		
		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);
	}

	return tc;
}
/*modified by shruthi for VAPT helper fixes ends*/
/*Added by Pushpalatha for TENJINCG-637 ends*/
/*Added by Pushpa TENJINCG-740 starts*/
public ArrayList<TestSet> hydrateTestSets(int projectId, String[] tsetIds) throws DatabaseException {
	
	ArrayList<TestSet> testSets = new ArrayList<TestSet>();
	
	try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement pst=conn.prepareStatement("SELECT * FROM TESTSETS WHERE TS_PRJ_ID = ? AND TS_REC_ID=? ORDER BY TS_REC_ID");){	
		for(String tsetRecId:tsetIds){
		
			pst.setInt(1,projectId);
			pst.setInt(2,Integer.parseInt(tsetRecId));
			
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					
					TestSet ts = new TestSet();
					ts=this.mapFields(rs);
					testSets.add(ts);
				}
			}
		}
	}catch(Exception e){
		logger.error("Could not fetch selected test sets",e);
		throw new DatabaseException("Could not fetch Test Sets due to an internal error",e);
	}
	return testSets;
}



public boolean existMappedTestCase(int testSetId, int projectId,int tcRecId) throws DatabaseException
{
	boolean exist=false;
	ArrayList<TestCase> tc=this.hydrateMappedTCs(testSetId, projectId);
	for(TestCase tcase:tc)
	{
		if(tcase.getTcRecId()==tcRecId)
		{
			exist=true;
			break;
		}
	}
	return exist;
}
/*Added by Pushpa TENJINCG-740 Ends*/
/*Added by Padmavathi for TJN27-47 starts*/
public PaginatedRecords<TestSet> hydrateAllTestSets(int projectId, List<FilterCriteria> filterCriteria,int maxRecords, int pageNumber, String sortColumn, String sortDirection) throws DatabaseException {
	Paginator paginator = null;
	try {
		paginator = new Paginator(maxRecords, pageNumber);
		List<TestSet> testSets = new LinkedList<>();
		
		String sCol = Utilities.trim(sortColumn).length() > 0 ? this.fieldToColumnMap.get(sortColumn) : "TS_REC_ID";
		String sDir = Utilities.trim(sortDirection).length() > 0 ? Utilities.trim(sortDirection) : "asc";
		if(sCol == null || sCol.length() < 1) {
			logger.warn("Sort Column {} is invalid. Default sorting will be applied", sortColumn);
			sCol = "TS_REC_ID";
		}
		String baseQuery = this.getQuery(new String[] {"*"},projectId,filterCriteria);
		baseQuery += " ORDER BY " + sCol + " " + sDir;
		paginator.executeQuery("testset",baseQuery);
		int totalRecords = 0;
		while(paginator.getResultSet().next()) {
			TestSet ts = this.mapFields(paginator.getResultSet());
			this.hydrateLastestRunAndStatusForSet(paginator.getConnection(), ts, projectId);
			testSets.add(ts);
			try {
				totalRecords = paginator.getResultSet().getInt("P_REC_COUNT");
			}catch(Exception e) {
				
			}
		}
		PaginatedRecords<TestSet> records = new PaginatedRecords<>();
		records.setPageNumber(pageNumber);
		records.setRecords(testSets);
		records.setTotalSize(totalRecords);
		records.setCurrentSize(maxRecords);
		return records;
	} catch(Exception e) {
		logger.error("ERROR occurred while fetching test sets under project", e);
		throw new DatabaseException("Could not fetch test sets due to an internal error.", e);
	} finally {
		if(paginator != null) {
			try {
				paginator.close();
			} catch (Exception e) {
				logger.warn("ERROR while closing paginator. This could potentially be a connection leak", e);
			}
		}
			
	}
}

private String getQuery(String[] fieldsToFetch,int projectId, List<FilterCriteria> criteria) {
	
	String query = "";
	
	String cols = "";
	if(fieldsToFetch != null && fieldsToFetch.length > 0){
		for(String field:fieldsToFetch){
			if(cols.length() < 1){
				cols += field;
			}else{
				cols += ", " + field;
			}
		}
	}else{
		cols = "*";
	}
	
	query = "SELECT " + cols + " FROM TESTSETS";
	String filterList = "";
	FilterCriteria orderBy = null;
	if(criteria != null && criteria.size() > 0){
		for(FilterCriteria criterion:criteria){
			if(criterion.getCondition().equalsIgnoreCase(FilterCriteria.ORDER_BY)){
				orderBy = criterion;
				continue;
			}
			if(criterion.toString() != null){
				if(filterList.length() < 1){
					filterList += criterion.toString();
				}else{
					filterList += " AND " + criterion.toString();
				}
			}
		}
	}
	
	if(filterList.length() > 0){
		
		query = query + " WHERE " + filterList;
		
	}
	
	if(orderBy != null && orderBy.toString() != null){
		query = query + " " + orderBy.toString();
	}

	logger.debug("Generated Query --> {}", query);
	return query;
	}

	public void hydrateLastestRunAndStatusForSet(Connection conn,TestSet ts,int projectId ){
		
		try(PreparedStatement pst=conn.prepareStatement("SELECT A.RUN_ID, B.RUN_STATUS FROM "
				+ "MASTESTRUNS A, V_RUNRESULTS B WHERE B.RUN_ID = A.RUN_ID AND "
				+ "A.RUN_ID = (SELECT MAX(RUN_ID) FROM MASTESTRUNS WHERE RUN_PRJ_ID=? AND RUN_TS_REC_ID =? AND RUN_STATUS IN('Complete','Aborted'))");) {
					
					pst.setInt(1, projectId);
					pst.setInt(2, ts.getId());
					
					try (ResultSet rs=pst.executeQuery();){
						while(rs.next()){
							ts.setLastRunId(rs.getInt("RUN_ID"));
							ts.setLastRunStatus(rs.getString("RUN_STATUS"));
						}
					}
		} catch (SQLException e) {
			
			logger.error("error while fetching latest run and status",e.getMessage());
			logger.error("Error ", e);
			
		}
	}
	/*Added by Padmavathi for TJN27-47 ends*/

	/*Added by Prem for TJN27-197 & TJN27-203 Starts */
	public List<TestSet> hydrateAllTestSetsForReport(int projectId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst =null;
		PreparedStatement pst1 =null;
		ResultSet rs=null;
		ResultSet rs1=null;
		ArrayList<TestSet> testSets = null;
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		try{	
			pst = conn.prepareStatement("SELECT TS_NAME,TS_REC_ID,TS_PRJ_ID FROM TESTSETS WHERE TS_PRJ_ID = ? AND TS_REC_TYPE='TS' ORDER BY TS_REC_ID");
			pst.setInt(1,projectId);
			rs = pst.executeQuery();
			testSets = new ArrayList<TestSet>();
			while(rs.next()){
				TestSet ts = new TestSet();
				ts.setName(rs.getString("TS_NAME"));
				ts.setId(rs.getInt("TS_REC_ID"));
				ts.setProject(rs.getInt("TS_PRJ_ID"));
				testSets.add(ts);
			}
		}catch(Exception e){
			logger.error("Could not fetch all test sets",e);
			throw new DatabaseException("Could not fetch Test Sets due to an internal error",e);
		}finally{
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return testSets;
	}
	/*Added by Prem for TJN27-197 & TJN27-203 Ends */
}
