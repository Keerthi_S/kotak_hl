/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  CoreDefectHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              	DESCRIPTION
* 22-Nov-2016           Sriram Sridharan          	Newly Added For 
* 08-Dec-2016			Sriram						Changed by Sriram to show defect linkages
* 16-Dec-2016			Manish						Defect TEN-86
* 16-Dec-2016			Manish						Defect TEN-83
* 16-Dec-2016			Manish						Defect TEN-82
* 21-Dec-2016			Sriram						Defect TEN-22
* 10-Jan-2017			Manish						TENJINCG-5
   30-08-2017			Sriram Sridharan			T25IT-106
   01-09-2017			Sriram Sridharan			T25IT-423
   20-04-2018			Preeti						TENJINCG-616
   18-06-2018           Padmavathi                  T251IT-48
   30-07-2018			Preeti						Closed connections
   24-08-2018			Preeti						Burgon bank(SQL server)
   27-09-2018			Preeti						Changes for ENBD
   26-10-2018           Leelaprasad                 TENJINCG-883
   12-12-2018           Padmavathi                  for TENJINCG-883 
   21-12-2018           Padmavathi                  for TJN262R2-55
   25-02-2019			Ashiki						TENJINCG-985
   10-01-2020			Prem						Scripting fix
   11-12-2020			Pushpalatha				TENJINCG-1221
   15-12-2020			Pushpalatha				TENJINCG-1222
*/


package com.ycs.tenjin.db;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.defect.Attachment;
import com.ycs.tenjin.defect.Defect;
import com.ycs.tenjin.defect.DefectLinkage;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class CoreDefectHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(CoreDefectHelper.class);
	/*changed by manish for defect TEN-82 on 16-Dec-2016 starts*/
	private String REC_STATUS = "A";
	/*changed by manish for defect TEN-82 on 16-Dec-2016 ends*/
	public Attachment hydrateAttachment(int recordId) throws DatabaseException{
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		try{
			return this._hydrateAttachment(conn, recordId);
		}catch(DatabaseException e){
			throw e;
		}finally{
			DatabaseHelper.close(conn);
		}
	}
	
	private Attachment _hydrateAttachment(Connection conn, int recordId) throws DatabaseException{
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("SELECT * FROM TJN_RUN_DEF_ATTACHMENTS WHERE REC_ATTACHMENT_ID=? ");
			pst.setInt(1, recordId);
			
			rs = pst.executeQuery();
			Attachment att = null;
			while(rs.next()){
				att = new Attachment();
				att.setAttachmentRecId(rs.getInt("REC_ATTACHMENT_ID"));
				att.setAttachmentIdentifier(rs.getString("ATTACHMENT_ID"));
				att.setFileName(rs.getString("ATT_FILE_NAME"));
				att.setFilePath(rs.getString("FILE_PATH"));
				att.setFileContent(rs.getString("ATT_FILE_CONTENT"));
				
			}
			
			
			
			return att;
		} catch (SQLException e) {
			
			logger.error("ERROR occurred while fetching attachment [{}] from DB", e);
			throw new DatabaseException("Could not fetch attachment due to an internal error. Please contact Tenjin Support");
		} finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
	}
	
	private List<DefectLinkage> _getDefectLinkages(Connection conn, int defectRecordId){
		PreparedStatement pst = null;
		PreparedStatement pst2 = null;
		ResultSet rs2 = null;
		ResultSet rs = null;
		
		List<DefectLinkage> linkages = new ArrayList<DefectLinkage>();
		
		try {
			pst = conn.prepareStatement("select a.def_run_id,a.def_tc_rec_id,a.def_tstep_rec_id,a.def_app_id,a.def_function_code,count(a.def_tduid) as TDUID_COUNT, "
					+ "b.tc_id, c.tstep_id, d.app_name from tjn_run_defect_linkage a, testcases b, teststeps c, masapplication d where "
					+ "b.tc_rec_id=a.def_tc_rec_id and "
					+ "c.tstep_rec_id=a.def_tstep_rec_id and " 
					+ "d.app_id = a.def_app_id and "
					+ "a.def_rec_id=? group by a.def_run_id, a.def_tc_rec_id, a.def_tstep_rec_id, a.def_app_id, a.def_function_code,  "
					+ "b.tc_id, c.tstep_id, d.app_name");
			pst.setInt(1, defectRecordId);
			rs = pst.executeQuery();
			
			while(rs.next()){
				DefectLinkage link = new DefectLinkage();
				link.setRunId(rs.getInt("DEF_RUN_ID"));
				link.setTestCaseRecordId(rs.getInt("DEF_TC_REC_ID"));
				link.setTestCaseId(rs.getString("TC_ID"));
				link.setTestStepRecordId(rs.getInt("DEF_TSTEP_REC_ID"));
				link.setTestStepId(rs.getString("TSTEP_ID"));
				link.setAppId(rs.getInt("DEF_APP_ID"));
				link.setApplicationName(rs.getString("APP_NAME"));
				link.setFunctionCode(rs.getString("DEF_FUNCTION_CODE"));
				link.setTdUidCount(rs.getInt("TDUID_COUNT"));
				
				pst2 = conn.prepareStatement("SELECT DEF_TDUID FROM tjn_run_defect_linkage WHERE DEF_REC_ID=? AND DEF_TC_REC_ID=? AND DEF_TSTEP_REC_ID=?");
				pst2.setInt(1, defectRecordId);
				pst2.setInt(2, link.getTestCaseRecordId());
				pst2.setInt(3, link.getTestStepRecordId());
				
				rs2 = pst2.executeQuery();
				List<String> tdUids = new ArrayList<String>();
				while(rs2.next()){
					tdUids.add(rs2.getString("DEF_TDUID"));
				}
				
				
				link.setTdUids(tdUids);
				linkages.add(link);
			}
			
			
		} catch (SQLException e) {
			
			logger.error("An Error Occurred while fetching linkages for defect [{}]", defectRecordId, e);
		}
		finally {
			DatabaseHelper.close(rs2);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
		return linkages;
	}
	
	private Defect getDefect(Connection conn, int defectRecordId) throws DatabaseException{
		PreparedStatement pst = null;
		ResultSet rs = null;
		Defect defect=null;
		
		try {
			/*pst = conn.prepareStatement("SELECT distinct A.*,"
					+ "B.DEF_APP_ID,C.APP_NAME,B.DEF_FUNCTION_CODE"
					+ " FROM TJN_RUN_DEFECTS A, TJN_RUN_DEFECT_LINKAGE B,MASAPPLICATION C, TESTSTEPS D"
					+ " WHERE B.DEF_REC_ID = A.DEF_REC_ID AND C.APP_ID = B.DEF_APP_ID AND A.DEF_REC_ID=?");*/
			
			/* Changed by Sriram for API testing*/
			/*Modified by Pushpa for TENJINCG-1221 starts*/
			pst = conn.prepareStatement("SELECT distinct A.*,"
					+ "B.DEF_APP_ID,C.APP_NAME,B.DEF_FUNCTION_CODE,B.DEF_API_CODE,B.DEF_OPERATION,B.REPO_OWNER"
					+ " FROM TJN_RUN_DEFECTS A, TJN_RUN_DEFECT_LINKAGE B,MASAPPLICATION C, TESTSTEPS D"
					+ " WHERE B.DEF_REC_ID = A.DEF_REC_ID AND C.APP_ID = B.DEF_APP_ID AND A.DEF_REC_ID=?");
			/*Modified by Pushpa for TENJINCG-1221 ends*/
			/* Changed by Sriram for API testing ends*/
			
			
			pst.setInt(1, defectRecordId);
			rs = pst.executeQuery();
			
			while(rs.next()){
				defect = new Defect();
				defect.setRecordId(rs.getInt("DEF_REC_ID"));
				defect.setCreatedBy(rs.getString("DEF_CREATED_BY"));
				defect.setSeverity(rs.getString("DEF_SEVERITY"));
				defect.setSummary(rs.getString("DEF_SUMMARY"));
				defect.setCreatedOn(rs.getTimestamp("DEF_CREATED_ON"));
				defect.setStatus(rs.getString("DEF_STATUS"));
				defect.setAppId(rs.getInt("DEF_APP_ID"));
				defect.setAppName(rs.getString("APP_NAME"));
				defect.setFunctionCode(rs.getString("DEF_FUNCTION_CODE"));
				defect.setIdentifier(rs.getString("DEF_ID"));
				defect.setDescription(rs.getString("DEF_DESC"));
			
				defect.setCreatedBy(rs.getString("DEF_CREATED_BY"));
				defect.setCreatedOn(rs.getTimestamp("DEF_CREATED_ON"));
				defect.setModifiedBy(rs.getString("DEF_MODIFIED_BY"));
				defect.setModifiedOn(rs.getTimestamp("DEF_MODIFIED_ON"));
				defect.setPriority(rs.getString("DEF_PRIORITY"));
				defect.setPosted(rs.getString("DEF_POSTED"));
				defect.setLastRefreshed(rs.getTimestamp("DEF_LAST_REFRESHED"));
				defect.setRunId(rs.getInt("RUN_ID"));
				defect.setDttInstanceName(rs.getString("DTT_INSTANCE"));
				defect.setEdmProjectName(rs.getString("DTT_PROJECT"));
				defect.setTjnStatus(rs.getString("TJN_STATUS"));
				if(Utilities.trim(defect.getTjnStatus()).equalsIgnoreCase("")){
					defect.setTjnStatus("NOREVIEW");
				}
				/* Added by Sriram for API testing*/
				defect.setApiCode(rs.getString("DEF_API_CODE"));
				defect.setOperation(rs.getString("DEF_OPERATION"));
				
				/* Added by Sriram for API testing ends*/
				/*Added by Pushpa for TENJINCG-1221 starts*/
				defect.setRepoOwner(rs.getString("REPO_OWNER"));
				/*Added by Pushpa for TENJINCG-1221 starts*/
			}
			
			rs.close();
			pst.close();
			
			/*********
			 * Changed by Sriram to show defect linkages
			 */
		
			logger.debug("Getting linkages for defect [{}]", defectRecordId);
			List<DefectLinkage> linkages = this._getDefectLinkages(conn, defectRecordId);
			defect.setLinkages(linkages);
			defect.setLinkCount(linkages.size());
			/*********
			 * Changed by Sriram to show defect linkages ends
			 */
			rs.close();
			pst.close();
			
			pst = conn.prepareStatement("SELECT * FROM TJN_RUN_DEF_ATTACHMENTS WHERE DEF_REC_ID=? ORDER BY REC_ATTACHMENT_ID");
			pst.setInt(1, defectRecordId);
			
			rs = pst.executeQuery();
			
			List<Attachment> attachments = new ArrayList<Attachment>();
			while(rs.next()){
				Attachment att = new Attachment();
				att.setAttachmentRecId(rs.getInt("REC_ATTACHMENT_ID"));
				att.setAttachmentIdentifier(rs.getString("ATTACHMENT_ID"));
				att.setFileName(rs.getString("ATT_FILE_NAME"));
				att.setFilePath(rs.getString("FILE_PATH"));
				attachments.add(att);
			}
			
			defect.setAttachments(attachments);
			
			rs.close();
			pst.close();
			
		} catch (SQLException e) {
			
			logger.error("ERROR fetching defect with record id [{}]", defectRecordId, e);
			throw new DatabaseException("Could not fetch Defect Information. Please contact Tenjin Support.");
		} finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
	
		return defect;
	}
	
	private List<Defect> getDefects(Connection conn, int runId) throws DatabaseException{
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Defect> defects = new ArrayList<Defect>();
		
		try {
			
			/* Changed by Sriram for API testing*/
			pst = conn.prepareStatement("SELECT distinct A.*,"
					+ "B.DEF_APP_ID,C.APP_NAME,B.DEF_FUNCTION_CODE,b.DEF_API_CODE,b.DEF_OPERATION"
					+ " FROM TJN_RUN_DEFECTS A, TJN_RUN_DEFECT_LINKAGE B,MASAPPLICATION C, TESTSTEPS D"
					+ " WHERE B.DEF_REC_ID = A.DEF_REC_ID AND C.APP_ID = B.DEF_APP_ID AND A.RUN_ID=?");
			/* Changed by Sriram for API testing ends*/
			pst.setInt(1, runId);
			rs = pst.executeQuery();
			
			while(rs.next()){
				Defect defect = new Defect();
				defect.setRecordId(rs.getInt("DEF_REC_ID"));
				defect.setCreatedBy(rs.getString("DEF_CREATED_BY"));
				defect.setSeverity(rs.getString("DEF_SEVERITY"));
				defect.setSummary(rs.getString("DEF_SUMMARY"));
				defect.setCreatedOn(rs.getTimestamp("DEF_CREATED_ON"));
				defect.setStatus(rs.getString("DEF_STATUS"));
				defect.setAppId(rs.getInt("DEF_APP_ID"));
				defect.setAppName(rs.getString("APP_NAME"));
				defect.setFunctionCode(rs.getString("DEF_FUNCTION_CODE"));
				defect.setIdentifier(rs.getString("DEF_ID"));
				defect.setDescription(rs.getString("DEF_DESC"));
				defect.setCreatedBy(rs.getString("DEF_CREATED_BY"));
				defect.setCreatedOn(rs.getTimestamp("DEF_CREATED_ON"));
				defect.setModifiedBy(rs.getString("DEF_MODIFIED_BY"));
				defect.setModifiedOn(rs.getTimestamp("DEF_MODIFIED_ON"));
				defect.setPriority(rs.getString("DEF_PRIORITY"));
				defect.setPosted(rs.getString("DEF_POSTED"));
				defect.setLastRefreshed(rs.getTimestamp("DEF_LAST_REFRESHED"));
				defect.setRunId(rs.getInt("RUN_ID"));
				defect.setDttInstanceName(rs.getString("DTT_INSTANCE"));
				defect.setEdmProjectName(rs.getString("DTT_PROJECT"));
				defect.setTjnStatus(rs.getString("TJN_STATUS"));
				if(Utilities.trim(defect.getTjnStatus()).equalsIgnoreCase("")){
					defect.setTjnStatus("NOREVIEW");
				}
				
				/* Added by Sriram for API testing*/
				defect.setApiCode(rs.getString("DEF_API_CODE"));
				defect.setOperation(rs.getString("DEF_OPERATION"));
				/* Added by Sriram for API testing ends*/
				defects.add(defect);
			}
			
		
		} catch (SQLException e) {
			
			logger.error("ERROR fetching defect for run id [{}]", runId, e);
			throw new DatabaseException("Could not fetch Defect Information. Please contact Tenjin Support.");
		} finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
	
		return defects;
	}
	
	public Defect hydrateDefect(int defectRecordId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		try{
			Defect defect = this.getDefect(conn, defectRecordId);
			return defect;
		} catch(DatabaseException e){
			throw e;
		} finally{
			
			DatabaseHelper.close(conn);
		}
	}
	
	public List<Defect> hydrateDefects(int runId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		try{
			return this.getDefects(conn, runId);
		} catch(DatabaseException e){
			throw e;
		} finally{
			
			DatabaseHelper.close(conn);
		}
	}
	
	public Defect hydrateDefect(Connection conn, int defectRecordId) throws DatabaseException{
		return this.getDefect(conn, defectRecordId);
	}
	
	
	
	
	private void _updateDefectReviewInfo(Connection conn, int defectRecordId, String action, String severity) throws DatabaseException{
		PreparedStatement pst = null;
		
		try {
			pst = conn.prepareStatement("UPDATE TJN_RUN_DEFECTS SET DEF_SEVERITY=?, TJN_STATUS=? WHERE DEF_REC_ID=?");
			pst.setString(1, severity);
			pst.setString(2, action);
			pst.setInt(3, defectRecordId);
			pst.executeUpdate();
			pst.close();
		} catch (SQLException e) {
			
			logger.error("ERROR while updating defect review info for defect ID [{}]", defectRecordId, e);
			throw new DatabaseException("Could not update review information due to an internal error. Please contact Tenjin Support.");
		} finally{
		
			DatabaseHelper.close(pst);
		}
	}
	
	
	
	public void updateDefectReviewInfo(Connection conn, int defectRecordId, String action, String severity) throws DatabaseException{
		this._updateDefectReviewInfo(conn, defectRecordId, action, severity);
	}
	
	
	private String _getEDMProjectIdentifier(Connection conn, int appId, int defectRecordId) throws DatabaseException{
		PreparedStatement pst = null;
		ResultSet rs = null;
		String edmProjectName = null;
		try {
			pst = conn.prepareStatement("SELECT PRJ_DM_PROJECT FROM TJN_PRJ_AUTS WHERE APP_ID=? AND PRJ_ID IN (SELECT RUN_PRJ_ID FROM MASTESTRUNS WHERE RUN_ID IN (SELECT RUN_ID FROM TJN_RUN_DEFECTS WHERE DEF_REC_ID=?))");
			pst.setInt(1,appId);
			pst.setInt(2, defectRecordId);
			rs = pst.executeQuery();
			while(rs.next()){
				edmProjectName = rs.getString("PRJ_DM_PROJECT");
			}
			
		} catch (SQLException e) {
			
			logger.error("ERROR while getting EDM Project Name for App ID [{}], Defect Record ID [{}]", appId, defectRecordId, e);
			/*Changed By Ashiki for TENJINCG-985 starts*/
			/*throw new DatabaseException("Could not get DTT Project Name. Please contact Tenjin Support.");*/
			throw new DatabaseException("Could not get Defect Management Project Name. Please contact Tenjin Support.");
			/*Changed By Ashiki for TENJINCG-985 ends*/
		} finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
		return edmProjectName;
	}
	
	private void _hydrateAttachments(Connection conn, Defect defect) throws DatabaseException{
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		List<Attachment> attachments = new ArrayList<Attachment>();
		
		try {
			pst = conn.prepareStatement("SELECT * FROM TJN_RUN_DEF_ATTACHMENTS WHERE DEF_REC_ID=?");
			pst.setInt(1, defect.getRecordId());
			
			rs = pst.executeQuery();
			
			while(rs.next()){
				Attachment att = new Attachment();
				att.setAttachmentIdentifier(rs.getString("ATTACHMENT_ID"));
				att.setAttachmentRecId(rs.getInt("REC_ATTACHMENT_ID"));
				att.setFileName(rs.getString("ATT_FILE_NAME"));
				att.setFilePath(rs.getString("FILE_PATH"));
				/*Modified by Prem for TENJINCG-1236 starts*/
				
				/*Modified by Paneendra for TENJINCG-1265 Starts*/
				att.setFileContent(rs.getBlob("ATT_FILE_CONTENT").toString());
				/*Modified by Paneendra for TENJINCG-1265 Ends*/
				
				/*Modified by Prem for TENJINCG-1236 Ends*/
				
				/*Commented by Paneendra for TENJINCG-1262 Starts*/
				//att.setFileContent(rs.getClob("ATT_FILE_CONTENT").toString());
				/*Commented by Paneendra for TENJINCG-1262 Ends*/
				
				/*Added by Pushpalatha for TENJINCG-1203 starts*/
				att.setAttachmentUrl("#");
				/*Added by Pushpalatha for TENJINCG-1203 ends*/
				attachments.add(att);
			}
			defect.setAttachments(attachments);
		
		} catch (SQLException e) {
			
			logger.error("ERROR getting attachments for defect", e);
			throw new DatabaseException("Could not get attachments for this defect. Please contact Tenjin Support");
		} finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
	}
	
	public void updateDefectPostingStatus(Connection conn, Defect defect, String instanceName) throws DatabaseException{
		this._updateDefectPostingStatus(conn, defect, instanceName);
	}
	private void _updateDefectPostingStatus(Connection conn, Defect defect, String instanceName) throws DatabaseException{
		PreparedStatement pst = null;
		
		try{
			pst = conn.prepareStatement("UPDATE TJN_RUN_DEFECTS SET DEF_POSTED=?, DEF_ID=?,DTT_INSTANCE=?,DTT_PROJECT=? WHERE DEF_REC_ID=?");
			pst.setString(1,"YES");
			pst.setString(2, defect.getIdentifier());
			pst.setString(3, instanceName);
			pst.setString(4, defect.getEdmProjectName());
			pst.setInt(5, defect.getRecordId());
			pst.executeUpdate();
		}catch(SQLException e){
			logger.error("ERROR while updating defect post details", e);
			throw new DatabaseException("Could not update posted status for this defect. Please contact Tenjin Support");
		}finally{
		
			DatabaseHelper.close(pst);
		}
	}
	
	public void hydrateAttachments(Connection conn, Defect defect) throws DatabaseException{
		this._hydrateAttachments(conn, defect);
	}
	public String getEDMProjectIdentifier(Connection conn, int appId, int defectRecordId) throws DatabaseException{
		return this._getEDMProjectIdentifier(conn, appId, defectRecordId);
	}
	
	public void syncDefect(Connection conn, Defect defect) throws DatabaseException{
		this._syncDefect(conn, defect);
	}
	
	public void syncDefectAttachments(Connection conn, Defect defect) throws DatabaseException{
		this._syncDefectAttachments(conn, defect.getRecordId(), defect.getAttachments());
	}
	
	private void _syncDefect(Connection conn, Defect defect) throws DatabaseException{
		PreparedStatement pst = null;
		
		
		try {
			pst = conn.prepareStatement("UPDATE TJN_RUN_DEFECTS SET DEF_SUMMARY=?, DEF_DESC=?, DEF_MODIFIED_BY=?,"
					+ "DEF_MODIFIED_ON=?,DEF_SEVERITY=?,DEF_PRIORITY=?,DEF_STATUS=?,DEF_LAST_REFRESHED=? WHERE DEF_REC_ID=?");
			
			pst.setString(1, defect.getSummary());
			pst.setString(2, defect.getDescription());
			pst.setString(3, defect.getModifiedBy());
			pst.setTimestamp(4, defect.getModifiedOn());
			/*Modified by Pushpa for tENJINCG-1222 starts*/
			if(defect.getSeverity()==null){
				pst.setString(5, "NA");
			}else{
				pst.setString(5, defect.getSeverity());
			}
			
			if(defect.getPriority()==null){
				pst.setString(6, "NA");
			}
			else{
				pst.setString(6, defect.getPriority());
			}
			
			if(defect.getStatus()==null){
				pst.setString(7, "NA");
			}else{
				pst.setString(7, defect.getStatus());
			}
			/*Modified by Pushpa for tENJINCG-1222 ends*/
			pst.setTimestamp(8, new Timestamp(new Date().getTime()));
			pst.setInt(9, defect.getRecordId());
			
			pst.executeUpdate();
		} catch (SQLException e) {
			
			logger.error("ERROR while syncing defect", e);
			throw new DatabaseException("Could not sync defect. Please contact Tenjin Support");
		} finally{
			
			DatabaseHelper.close(pst);
		}
	}
	
	public List<TestCase> fetchTestCasesForRun(int runId) throws DatabaseException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		List<TestCase> testCases = new ArrayList<TestCase>();
		conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		try {
			pstmt = conn
					.prepareStatement("SELECT * FROM TESTCASES WHERE TC_REC_ID IN (SELECT TC_REC_ID FROM V_RUNRESULTS_TC WHERE RUN_ID=?) ORDER BY TC_ID");
			pstmt.setInt(1, runId);
			/* pstmt.setInt(1, tsRecId); */
			rs = pstmt.executeQuery();
			while (rs.next()) {
				TestCase tc = new TestCase();
				tc.setTcId(Utilities.escapeXml(rs.getString("TC_ID")));
				tc.setTcRecId(rs.getInt("TC_REC_ID"));
				tc.setTcName(Utilities.escapeXml(rs.getString("TC_NAME")));
				testCases.add(tc);
			}
			
		
		} catch (Exception e) {
			logger.error("ERROR occurred while fetching test cases for Run {}", runId, e);
			throw new DatabaseException("Could not fetch Test Cases for run. Please contact Tenjin Support.");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);
		}
		return testCases;

	}
	
	public List<TestStep> fetchTStepsForTestCase(int testCaseRecordId,int runId) 	throws DatabaseException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		List<TestStep> tSteps = new ArrayList<TestStep>();
		conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		try {
			/*Modified by Padmavathi for T251IT-48 starts*/
			/*pstmt = conn.prepareStatement("SELECT A.*, B.APP_NAME, C.MODULE_NAME FROM TESTSTEPS A, MASAPPLICATION B, MASMODULE C WHERE C.MODULE_CODE=A.FUNC_CODE AND C.APP_ID=A.APP_ID  AND B.APP_ID = A.APP_ID AND A.TC_REC_ID=?");*/
			/* pstmt.setInt(1, tsRecId); */
			pstmt = conn.prepareStatement("SELECT A.*,C.APP_NAME FROM TESTSTEPS A, RUNRESULT_TS_TXN B, MASAPPLICATION C WHERE  B.TSTEP_REC_ID = A.TSTEP_REC_ID AND  C.APP_ID=A.APP_ID AND A.TC_REC_ID = ? AND B.RUN_ID = ? ORDER BY A.TSTEP_REC_ID");
			/*Modified by Padmavathi for T251IT-48 ends*/
			pstmt.setInt(1, testCaseRecordId);
			/*Added by Padmavathi for T251IT-48 starts*/
			pstmt.setInt(2, runId);
			/*Added by Padmavathi for T251IT-48 ends*/
			rs = pstmt.executeQuery();
			while (rs.next()) {
				TestStep tStep = new TestStep();
				tStep.setRecordId(rs.getInt("TSTEP_REC_ID"));
				tStep.setId(Utilities.escapeXml(rs.getString("TSTEP_ID")));
				tStep.setAppId(rs.getInt("APP_ID"));
				tStep.setAppName(Utilities.escapeXml(rs.getString("APP_NAME")));
				tStep.setModuleCode(Utilities.escapeXml(rs.getString("FUNC_CODE")));
				/*tStep.setModuleName(rs.getString("MODULE_NAME"));*/
				tSteps.add(tStep);
			}
			
		
		} catch (Exception e) {
			logger.error("ERROR occurred while fetching test steps for Test Case {}", testCaseRecordId, e);
			throw new DatabaseException("Could not fetch Test Steps. Please contact Tenjin Support.");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);
		}
		return tSteps;
	}
	
	private void persistDefectAttachment(Connection conn, int defectRecordId, Attachment attachment) throws DatabaseException{
		PreparedStatement pst = null;
		/*Modified by Preeti for Burgon bank(SQL server) starts*/
		RunDefectHelper runDefHelper = new RunDefectHelper();
		int attachmentId = runDefHelper.getMaxAttachmentIdDefect(conn);
		/*String query = "INSERT INTO TJN_RUN_DEF_ATTACHMENTS (REC_ATTACHMENT_ID,DEF_REC_ID,FILE_PATH,ATT_FILE_NAME,ATT_POSTED) "
				+ " VALUES(SQ_RUN_DEF_ATTACHMENTS.nextval,?,?,?,?)";*/
		/*Modified by Prem for TENJINCG-1236 starts*/
		/*String query = "INSERT INTO TJN_RUN_DEF_ATTACHMENTS (REC_ATTACHMENT_ID,DEF_REC_ID,FILE_PATH,ATT_FILE_NAME,ATT_POSTED) "
				+ " VALUES("+attachmentId+",?,?,?,?)";*/
		/*Modified by Pushpa for Sql Injection VAPT fix starts*/
		String query = "INSERT INTO TJN_RUN_DEF_ATTACHMENTS (DEF_REC_ID,FILE_PATH,ATT_FILE_NAME,ATT_POSTED,ATT_FILE_CONTENT,REC_ATTACHMENT_ID) "
				+ " VALUES(?,?,?,?,?,?)";
		/*Modified by Pushpa for Sql Injection VAPT fix ends*/
		/*Modified by Prem for TENJINCG-1236 starts*/
		/*Modified by Preeti for Burgon bank(SQL server) ends*/
		try {
			pst = conn.prepareStatement(query);
			pst.setInt(1, defectRecordId);
			pst.setString(2, attachment.getFilePath());
			pst.setString(3, attachment.getFileName());
			pst.setString(4, "No");
			/*Modified by Paneendra for TENJINCG-1262 Starts*/
			Blob bLob = conn.createBlob();
			bLob.setBytes(1,attachment.getFileContent().toString().getBytes());
			pst.setBlob(5,bLob);
			/*Modified by Paneendra for TENJINCG-1262 Ends*/
			/*Added by Pushpa for Sql Injection VAPT fix starts*/
			pst.setInt(6, attachmentId);
			/*Added by Pushpa for Sql Injection VAPT fix ends*/
			pst.execute();
			
		} catch (SQLException e) {
			
			logger.error("ERROR posting attachment [{}] to defect [{}]", attachment.getFileName(), defectRecordId, e);
			throw new DatabaseException("Could not save attachment due to an unexpected error. Please contact Tenjin Support.");
		} finally{
			DatabaseHelper.close(pst);
		}
		
	}
	
	
	private void _syncDefectAttachments(Connection conn, int defectRecordId, List<Attachment> attachments) throws DatabaseException{
		PreparedStatement pst = null;
		/*Modified by Preeti for Burgon bank(SQL server) starts*/
		RunDefectHelper runDefHelper = new RunDefectHelper();
		/*commented by Padmavathi for TENJINCG-883 starts*/
		/*int attachmentId = runDefHelper.getMaxAttachmentIdDefect(conn);*/
		/*commented by Padmavathi for TENJINCG-883 ends*/
		/*String query = "INSERT INTO TJN_RUN_DEF_ATTACHMENTS (REC_ATTACHMENT_ID,DEF_REC_ID,FILE_PATH,ATT_FILE_NAME,ATT_POSTED,ATTACHMENT_ID) "
				+ " VALUES(SQ_RUN_DEF_ATTACHMENTS.nextval,?,?,?,?,?)";*/
		/*Changed by leelaprasad for TENJINCG-883 starts*/
		/*String query = "INSERT INTO TJN_RUN_DEF_ATTACHMENTS (REC_ATTACHMENT_ID,DEF_REC_ID,FILE_PATH,ATT_FILE_NAME,ATT_POSTED,ATTACHMENT_ID) "
				+ " VALUES("+attachmentId+",?,?,?,?,?)";*/
		String query = "INSERT INTO TJN_RUN_DEF_ATTACHMENTS (DEF_REC_ID,FILE_PATH,ATT_FILE_NAME,ATT_POSTED,ATTACHMENT_ID,REC_ATTACHMENT_ID) "
				+ " VALUES(?,?,?,?,?,?)";
		/*Changed by leelaprasad for TENJINCG-883 ends*/
		/*Modified by Preeti for Burgon bank(SQL server) ends*/
		try {
			logger.debug("Clearing existing attachment information");
			pst = conn.prepareStatement("DELETE FROM TJN_RUN_DEF_ATTACHMENTS WHERE DEF_REC_ID=?");
			pst.setInt(1, defectRecordId);
			pst.execute();
			
		} catch (SQLException e) {
			
			logger.error("ERROR syncing attachments for defect [{}]", defectRecordId, e);
			throw new DatabaseException("Could not Synchronize attachments due to an unexpected error. Please contact Tenjin Support.");
		} finally{
			
			DatabaseHelper.close(pst);
		}
		
			
			for(Attachment attachment:attachments){
				try {
				/*Added by Padmavathi for TENJINCG-883 starts*/
				int attachmentId = runDefHelper.getMaxAttachmentIdDefect(conn);
				/*Added by Padmavathi for TENJINCG-883 ends*/
				pst = conn.prepareStatement(query);
				pst.setInt(1, defectRecordId);
				/*Added by Pushpalatha for TENJINCG-1203 starts*/
				if(attachment.getFilePath()!=null){
				pst.setString(2, attachment.getFilePath());
				}else{
					pst.setString(2," ");
				}
				/*Added by Pushpalatha for TENJINCG-1203 ends*/
				pst.setString(3, attachment.getFileName());
				pst.setString(4, "Yes");
				pst.setString(5, attachment.getAttachmentIdentifier());
				/*Changed by leelaprasad for TENJINCG-883 starts*/
				pst.setInt(6, attachmentId);
				/*Changed by leelaprasad for TENJINCG-883 ends*/
				pst.execute();
				} catch (SQLException e) {
					
					logger.error("ERROR syncing attachments for defect [{}]", defectRecordId, e);
					throw new DatabaseException("Could not Synchronize attachments due to an unexpected error. Please contact Tenjin Support.");
				} finally{
					
					DatabaseHelper.close(pst);
				}
			}
			
		
	}
	
	public void persistDefectAttachments(int defectRecordId, List<Attachment> attachments) throws DatabaseException{
		if(attachments != null && attachments.size() > 0){
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			
			try{
			for(Attachment att:attachments){
				logger.debug("Persisting attachment [{}]", att.getFileName());
				this.persistDefectAttachment(conn, defectRecordId, att);
			}
			
			
				//conn.commit();
			}catch(Exception e){
				logger.error("ERROR committing transaction after adding attachments", e);
				try {
					conn.rollback();
				} catch (SQLException e1) {
					throw new DatabaseException("Could not add attachments due to an internal error. Please contact Tenjin Support.");
				}
				
			} finally{
				
				DatabaseHelper.close(conn);
			}
		}else{
			logger.warn("Attachments list is null. Nothing to save.");
		}
	}
	
	public void newDefect(Defect defect, int prjId) throws DatabaseException {
		Connection conn = null;
		PreparedStatement pst = null;
		RunDefectHelper runDefHelper = new RunDefectHelper();
		int defRecId = runDefHelper.getMaxRecIdDefect();
		conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		try {
			
			/*Modified by Padmavathi for TJN262R2-55 starts*/
			/*changed by manish for req TENJINCG-5 on 12-Jan-2017 starts*/
			/*Modified by Pushpa for Sql injection VAPT Fix starts*/
			pst = conn
					.prepareStatement("INSERT INTO TJN_RUN_DEFECTS (DEF_SUMMARY,DEF_DESC, DEF_SEVERITY, DEF_PRIORITY,DEF_CREATED_BY,RUN_ID,DEF_CREATED_ON,DEF_STATUS,DTT_INSTANCE,DTT_PROJECT,TJN_STATUS,DEF_MODIFIED_BY,DEF_MODIFIED_ON,REC_STATUS,DEF_POSTED,DEF_REC_ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			/*Modified by Pushpa for Sql injection VAPT Fix ends*/
			/*changed by manish for req TENJINCG-5 on 12-Jan-2017 ends*/
			/*Modified by Padmavathi for TJN262R2-55 ends*/
			pst.setString(1, defect.getSummary());
			pst.setString(2, defect.getDescription());
			pst.setString(3, defect.getSeverity());
			pst.setString(4, defect.getPriority());
			
			pst.setString(5, defect.getCreatedBy());
			pst.setInt(6, defect.getRunId());
			pst.setTimestamp(7, defect.getCreatedOn());
			pst.setString(8, "NEW");
			pst.setString(9, defect.getDttInstanceName());
			pst.setString(10,defect.getEdmProjectName());
			pst.setString(11, defect.getTjnStatus());
			pst.setString(12, defect.getCreatedBy());
			pst.setTimestamp(13, defect.getCreatedOn());
			/*changed by manish for req TENJINCG-5 on 12-Jan-2017 starts*/
			pst.setString(14, REC_STATUS);
			/*changed by manish for req TENJINCG-5 on 12-Jan-2017 ends*/
			/*Modified by Preeti for TENJINCG-616 starts*/
			/*pst.executeQuery();*/
			/*Added by Padmavathi for TJN262R2-55 starts*/
			if(defect.getPosted()==null){
				pst.setString(15,"NO");
			}else{
				pst.setString(15, defect.getPosted());
			}
			/*Added by Padmavathi for TJN262R2-55 ends*/
			/*Added by Pushpa for Sql injection VAPT Fix starts*/
			pst.setInt(16, defRecId);
			/*Added by Pushpa for Sql injection VAPT Fix ends*/
			pst.execute();
			/*Modified by Preeti for TENJINCG-616 ends*/
			pst.close();
			

			
			int tcId = defect.getTestCaseId();
			int tstepRecid = defect.getTestStepId();
			int appId= defect.getAppId();
			/*Modified by Preeti for TENJINCG-616 starts*/
			DatabaseMetaData meta = conn.getMetaData(); 
			/*Modified by Pushpa for TENJINCG-1221 starts*/
			if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				pst = conn.prepareStatement("INSERT INTO TJN_RUN_DEFECT_LINKAGE (LINK_ID,DEF_REC_ID,DEF_PROJECT_ID,DEF_RUN_ID,DEF_TC_REC_ID,DEF_TSTEP_REC_ID,DEF_TDUID,DEF_APP_ID,DEF_FUNCTION_CODE,REPO_OWNER) VALUES(SQ_RUN_DEFECTS_LINKAGE.nextval,?,?,?,?,?,?,?,?,?)");
			else
				pst = conn.prepareStatement("INSERT INTO TJN_RUN_DEFECT_LINKAGE (LINK_ID,DEF_REC_ID,DEF_PROJECT_ID,DEF_RUN_ID,DEF_TC_REC_ID,DEF_TSTEP_REC_ID,DEF_TDUID,DEF_APP_ID,DEF_FUNCTION_CODE,REPO_OWNER) VALUES(next value for SQ_RUN_DEFECTS_LINKAGE,?,?,?,?,?,?,?,?,?)");
			/*Modified by Pushpa for TENJINCG-1221 ends*/
			
			/*Modified by Preeti for TENJINCG-616 ends*/
			pst.setInt(1, defRecId);
			pst.setInt(2, prjId);
			pst.setInt(3, defect.getRunId());
			pst.setInt(4, tcId);
			pst.setInt(5, tstepRecid);
			pst.setString(6, defect.getTdUId());
			pst.setInt(7, appId);
			pst.setString(8, defect.getFunctionCode());
			/*Added by Pushpa for TENJINCG-1221 starts*/
			pst.setString(9, defect.getRepoOwner());
			/*Added by Pushpa for TENJINCG-1221 ends*/
			
			/*Modified by Preeti for TENJINCG-616 starts*/
			/*pst.executeQuery();*/
			pst.execute();
			/*Modified by Preeti for TENJINCG-616 ends*/
			//conn.commit();
			
			pst.close();
			
			defect.setRecordId(defRecId);
			
			
		} catch (SQLException e) {
			logger.error("Error ", e);
			logger.error(
					"Error------------> during updating defect{} into database",
					defRecId);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				
				logger.error("Error ", e1);
			}
		} finally {
			
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	
	
	
	private void _updateDefectInfo(Connection conn, Defect defect) throws DatabaseException{
		PreparedStatement pst=null;
		try {
			pst = conn.prepareStatement("UPDATE TJN_RUN_DEFECTS SET DEF_SUMMARY=?,DEF_DESC=?,DEF_MODIFIED_BY=?,DEF_MODIFIED_ON=?,DEF_SEVERITY=?,DEF_PRIORITY=?,DEF_STATUS=?,TJN_STATUS=? WHERE DEF_REC_ID=?");
			pst.setString(1, defect.getSummary());
			pst.setString(2, defect.getDescription());
			pst.setString(3, defect.getModifiedBy());
			pst.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			pst.setString(5, defect.getSeverity());
			pst.setString(6, defect.getPriority());
			pst.setString(7, defect.getStatus());
			pst.setString(8, defect.getTjnStatus());
			pst.setInt(9, defect.getRecordId());
			pst.executeUpdate();
			
			
		} catch (SQLException e) {
			
			logger.error("ERROR updating defect", e);
			throw new DatabaseException("Could not update defect details due to an internal error. Please contact Tenjin Support");
		}
		finally{
			DatabaseHelper.close(pst);
		}
	}
	
	
	public void updateDefectInfo(Connection conn, Defect defect) throws DatabaseException{
		this._updateDefectInfo(conn, defect);
	}
	
	public void removeAttachments(Connection conn, int defectRecordId, String[] attachmentRecordIds) throws DatabaseException{
		PreparedStatement pst = null;
		
			
			for(String att:attachmentRecordIds){
				try {
				if(Utilities.trim(att).equalsIgnoreCase("") || !Utilities.isNumeric(att)){
					continue;
				}
				int aRecId = Integer.parseInt(att);
				 pst = conn.prepareStatement("DELETE FROM tjn_run_def_attachments WHERE  REC_ATTACHMENT_ID=?");
				pst.setInt(1, aRecId);
				
				pst.executeUpdate();
				
				} catch (SQLException e) {
					
					logger.error("ERROR updating defect", e);
					throw new DatabaseException("Could not remove attachments due to an internal error. Please contact Tenjin Support");
				}
				finally{
					DatabaseHelper.close(pst);
				}
			}
			
			
		
	}
/*added by manish for filter project defects on 06-Dec-2016 starts*/
	
	public List<Defect> hydrateProjectDefects(int projectId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<Defect> defects = new ArrayList<Defect>();
		
		
		try {
/*changed by manish for defect TEN-82 on 16-Dec-2016 starts*/
			
			/*modified by paneendra for sql injection starts*/
			pst = conn.prepareStatement("SELECT A.*, "
					+"B.DEF_FUNCTION_CODE,B.DEF_APP_ID,C.APP_NAME,B.DEF_API_CODE,B.DEF_OPERATION FROM TJN_RUN_DEFECTS A, TJN_RUN_DEFECT_LINKAGE B, MASAPPLICATION C "
					+" WHERE B.DEF_APP_ID=C.APP_ID AND A.DEF_REC_ID=B.DEF_REC_ID AND B.DEF_PROJECT_ID=? AND REC_STATUS=?");
			/*changed by manish for req TENJINCG-5 on 10-Jan-17 ends*/
			pst.setInt(1, projectId);
			pst.setString(2, REC_STATUS);
			/*modified by paneendra for sql injection ends*/
			rs = pst.executeQuery();
			
			while(rs.next()){
				Defect defect = new Defect();
				defect.setRecordId(rs.getInt("DEF_REC_ID"));
				defect.setCreatedBy(rs.getString("DEF_CREATED_BY"));
				defect.setSeverity(Utilities.escapeXml(rs.getString("DEF_SEVERITY")));
				defect.setSummary(Utilities.escapeXml(rs.getString("DEF_SUMMARY")));
				defect.setCreatedOn(rs.getTimestamp("DEF_CREATED_ON"));
				defect.setStatus(rs.getString("DEF_STATUS"));
				defect.setAppId(rs.getInt("DEF_APP_ID"));
				defect.setAppName(Utilities.escapeXml(rs.getString("APP_NAME")));
				defect.setFunctionCode(rs.getString("DEF_FUNCTION_CODE"));
				defect.setIdentifier(rs.getString("DEF_ID"));
				defect.setDescription(rs.getString("DEF_DESC"));
				defect.setCreatedBy(rs.getString("DEF_CREATED_BY"));
				defect.setCreatedOn(rs.getTimestamp("DEF_CREATED_ON"));
				defect.setModifiedBy(rs.getString("DEF_MODIFIED_BY"));
				defect.setModifiedOn(rs.getTimestamp("DEF_MODIFIED_ON"));
				defect.setPriority(rs.getString("DEF_PRIORITY"));
				defect.setPosted(rs.getString("DEF_POSTED"));
				defect.setLastRefreshed(rs.getTimestamp("DEF_LAST_REFRESHED"));
				defect.setRunId(rs.getInt("RUN_ID"));
				defect.setDttInstanceName(rs.getString("DTT_INSTANCE"));
				defect.setEdmProjectName(rs.getString("DTT_PROJECT"));
				defect.setTjnStatus(rs.getString("TJN_STATUS"));
				if(Utilities.trim(defect.getTjnStatus()).equalsIgnoreCase("")){
					defect.setTjnStatus("NOREVIEW");
				}
				
				defect.setApiCode(rs.getString("DEF_API_CODE"));
				defect.setOperation(rs.getString("DEF_OPERATION"));
				
				/*changed by manish for req TENJINCG-5 on 10-Jan-17 starts*/
				defect.setRec_status(rs.getString("REC_STATUS"));
				/*changed by manish for req TENJINCG-5 on 10-Jan-17 ends*/
				
				defects.add(defect);
			}
			
		} catch (SQLException e) {
			
			logger.error("ERROR fetching defect for project id [{}]", projectId, e);
			throw new DatabaseException("Could not fetch Defect Information. Please contact Tenjin Support.");
		} finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	
		return defects;
	}
	
	public List<Defect> hydratrFilterProjectDefects(int recordId, String testCaseId,
			String testStepId, int projectId, String app,
			String functionCode, String reporter, String status,String posted)
			throws DatabaseException {
		
		
		String finalQuery="";

		finalQuery = "SELECT DISTINCT A.DEF_REC_ID,A.DEF_SUMMARY,A.DEF_SEVERITY,A.DEF_STATUS,A.DEF_CREATED_BY,A.DEF_CREATED_ON,A.DEF_POSTED,"
				+" B.DEF_FUNCTION_CODE,C.APP_NAME FROM TJN_RUN_DEFECTS A, TJN_RUN_DEFECT_LINKAGE B, MASAPPLICATION C"
				+" WHERE A.DEF_REC_ID=B.DEF_REC_ID AND B.DEF_APP_ID=C.APP_ID AND (B.DEF_PROJECT_ID="+projectId+" AND REC_STATUS='A' AND ";

		boolean criteriaSpecified=false;
		
		if(reporter!=null && reporter != ""){
			finalQuery = finalQuery+"A.DEF_CREATED_BY = '"+reporter+"' AND ";
			criteriaSpecified =true;
		}
					
		
		if(functionCode!=null && functionCode != ""){
			finalQuery = finalQuery+"B.DEF_FUNCTION_CODE ='"+functionCode+"' AND ";
			criteriaSpecified =true;
		}
		
		
		if(status!=null && status != ""){
			
			finalQuery = finalQuery + "A.TJN_STATUS ='" + status + "' AND ";
			
			criteriaSpecified =true;
		}
		
		if(testCaseId!=null && testCaseId != ""){
			finalQuery = finalQuery+"D.TC_ID ='"+testCaseId+"' AND ";
			criteriaSpecified =true;
		}
		
		if(testStepId!=null && testStepId != ""){
			finalQuery = finalQuery+"E.TSTEP_ID = '"+testStepId+"' AND ";
			criteriaSpecified =true;
		}
		
		if(app != null && app != ""){
			finalQuery = finalQuery+"C.APP_ID ='"+app+"' AND ";
			criteriaSpecified =true;
		}
		
		if(posted != null && posted != ""){
			finalQuery = finalQuery+"A.DEF_POSTED ='"+posted+"' AND ";
			criteriaSpecified =true;
		}
		
		if(criteriaSpecified){
		 int index=finalQuery.lastIndexOf(" ");
		 finalQuery=finalQuery.substring(0,index-3);

			finalQuery = finalQuery+")";
		}else{
			finalQuery = finalQuery+")";
			finalQuery = finalQuery.replace("and()","");
		}
		
		
		
		ResultSet rs = null;

		PreparedStatement pst2=null;
		PreparedStatement pst1 =null;
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		List<Defect> defects = null;

		try {
			/*Added by Roshni starts*/
			defects = new ArrayList<Defect>();
			/*Added by Roshni ends*/
			/*modified by shruthi for sql injection starts*/
			pst1 = conn
					.prepareStatement("SELECT DISTINCT A.DEF_REC_ID,A.DEF_SUMMARY,A.DEF_SEVERITY,A.DEF_STATUS,A.DEF_CREATED_BY,A.DEF_CREATED_ON,A.DEF_POSTED,"
				+" B.DEF_FUNCTION_CODE,C.APP_NAME FROM TJN_RUN_DEFECTS A, TJN_RUN_DEFECT_LINKAGE B, MASAPPLICATION C"
				+" WHERE A.DEF_REC_ID=B.DEF_REC_ID AND B.DEF_APP_ID=C.APP_ID AND B.DEF_PROJECT_ID=? AND  A.DEF_REC_ID=? AND REC_STATUS='A' ");
			pst1.setInt(1, projectId);
			pst1.setInt(2, recordId);
			/*modified by shruthi for sql injection ends*/
			pst2 = conn
					.prepareStatement(finalQuery);
			

			if (recordId != 0 && recordId > 0) {
				rs = pst1.executeQuery();
			} else {
				rs = pst2.executeQuery();
			}

			while (rs.next()) {
				Defect defect = new Defect();
				defect.setAppName(rs.getString("APP_NAME"));
				defect.setCreatedBy(rs.getString("DEF_CREATED_BY"));
				defect.setCreatedOn(rs.getTimestamp("DEF_CREATED_ON"));
				defect.setFunctionCode(rs.getString("DEF_FUNCTION_CODE"));
				defect.setPosted(rs.getString("DEF_POSTED"));
				defect.setRecordId(rs.getInt("DEF_REC_ID"));
				defect.setSeverity(rs.getString("DEF_SEVERITY"));
				defect.setSummary(rs.getString("DEF_SUMMARY"));
				defect.setStatus(rs.getString("DEF_STATUS"));
				defects.add(defect);
			}
		} catch (SQLException e) {

			logger.error("ERROR fetching filtered defect for project id [{}]", projectId, e);
		} finally {
		
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(conn);
		}
		return defects;
	}
	
	/*added by manish for filter project defects on 06-Dec-2016 ends*/

	
	/* added by manish for defect TEN-46 on 13-12-2016 starts */
	public void validateUserDttLoginName(String instance, String userId) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement p=null;
		ResultSet rs =null;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		try {

			/*PreparedStatement p = conn.prepareStatement("SELECT DEF_TOOL_USERID FROM TJN_DEF_USER_MAPING where upper(DEF_TOOL_USERID)='"+ name.toUpperCase() + "'");*/
			/*PreparedStatement p = conn.prepareStatement("SELECT DEF_TOOL_USERID,TJN_INSTANCE_NAME,TJN_USERID FROM TJN_DEF_USER_MAPING where DEF_TOOL_USERID=? AND TJN_INSTANCE_NAME=? AND TJN_USERID=?");*/
			p = conn.prepareStatement("SELECT TJN_INSTANCE_NAME,TJN_USERID FROM TJN_DEF_USER_MAPING where TJN_INSTANCE_NAME=? AND TJN_USERID=?");
			/*p.setString(1, name);*/
			p.setString(1, instance);
			p.setString(2, userId);
			rs = p.executeQuery();
			while (rs.next()) {

				if(instance.equals(rs.getString("TJN_INSTANCE_NAME")) && userId.equals(rs.getString("TJN_USERID"))){
					throw new DatabaseException(
							"You have already specified credentials for"+instance+". To create a new one, please delete the existing credential and try again.");
				}
				
			}
		
		} catch (Exception e) {
			logger.error("Error ", e);

			throw new DatabaseException("Could not create client", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(p);
			DatabaseHelper.close(conn);
		}

	}

	/* added by manish for defect TEN-46 on 13-12-2016 ends */
	
	/*changed by manish for defect TEN-86 and TEN-83 on 16-Dec-2016 starts*/
	public String hydrateKey(String dttProject, String instance) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		ResultSet rs=null;
		String dttkey="";

		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		try {
			pst = conn.prepareStatement("SELECT DTT_PPROJECT_KEY FROM TJN_PRJ_AUTS WHERE PRJ_DM_PROJECT=? AND PRJ_DEF_MANAGER=?");
			pst.setString(1, dttProject);
			pst.setString(2, instance);
			rs = pst.executeQuery();
			while(rs.next()){
				dttkey = rs.getString("DTT_PPROJECT_KEY");
			}
		} catch (SQLException e) {
			
			logger.error("Error ", e);
		}
		finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		
		return dttkey;
		
	}
	/*changed by manish for defect TEN-86 and TEN-83 on 16-Dec-2016 ends*/

	
	/*changed by manish for req TENJINCG-5 on 10-Jan-17 starts*/
	
	public void deleteMultipleDefects(String id, Connection conn) throws DatabaseException{
		
		PreparedStatement pst=null;
		try {
			pst = conn.prepareStatement("UPDATE TJN_RUN_DEFECTS SET REC_STATUS=? WHERE DEF_REC_ID=?");
			pst.setString(1, "I");
			pst.setString(2, id);
			pst.executeUpdate();
		} catch (SQLException e) {
			logger.error("error while deactivating defects",e);
		}finally{
			
			DatabaseHelper.close(pst);
		}
	}
	
	/*changed by manish for req TENJINCG-5 on 10-Jan-17 ends*/
	
	
	//Fix for T25IT-106
	public List<Defect> hydrateDefectsForTestCase(int testCaseRecordId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst =null;
		ResultSet rs=null;
		List<Defect> defects = new ArrayList<Defect> ();
		try {
			pst = conn.prepareStatement("SELECT DEF_REC_ID FROM TJN_RUN_DEFECT_LINKAGE WHERE DEF_TC_REC_ID=? ORDER BY DEF_REC_ID");
			pst.setInt(1, testCaseRecordId);
			
			rs = pst.executeQuery();
			while(rs.next()) {
				int defRecId = rs.getInt("DEF_REC_ID");
				Defect defect = this.hydrateDefect(conn, defRecId);
				defects.add(defect);
			}
			
			
		} catch (SQLException e) {
			logger.error("ERROR hydrating defects for test case {}", testCaseRecordId, e);
			throw new DatabaseException("Could not get defects for test case due to an internal error.");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return defects;
	}
	
	public List<Defect> hydrateDefectsForTestSet(int testSetRecordId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst =null;
		ResultSet rs=null;
		List<Defect> defects = new ArrayList<Defect> ();
		try {
			pst = conn.prepareStatement("SELECT DEF_REC_ID FROM TJN_RUN_DEFECTS WHERE RUN_ID IN (select run_id from mastestruns where RUN_TS_REC_ID=?) ORDER BY DEF_REC_ID");
			pst.setInt(1, testSetRecordId);
			
			rs = pst.executeQuery();
			while(rs.next()) {
				int defRecId = rs.getInt("DEF_REC_ID");
				Defect defect = this.hydrateDefect(conn, defRecId);
				defects.add(defect);
			}
			
			
		} catch (SQLException e) {
			logger.error("ERROR hydrating defects for test set {}", testSetRecordId, e);
			throw new DatabaseException("Could not get defects for test case due to an internal error.");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return defects;
	}
	
	//Fix for T25IT-106 ends
	   
	   /*  added by shruthi for TENJINCG-1264 starts*/
		public String fetchTool(String instance, String userId) throws DatabaseException {
			Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			PreparedStatement p=null;
			ResultSet rs =null;
			String dtool ="";
			if (conn == null) {
				throw new DatabaseException("Invalid Database Connection");
			}
			
			try {
				p = conn.prepareStatement("SELECT TOOL_NAME FROM TJN_DEF_TOOL_MASTER WHERE INSTANCE_NAME=?");
				p.setString(1, instance);
				rs = p.executeQuery();
				while(rs.next()){
					 dtool = rs.getString("TOOL_NAME");
				}
			
				
			} catch (SQLException e1) {
				
				logger.error("ERROR hydrating toolname for the instance", e1);
				throw new DatabaseException("Could not get toolname  for the instance due to an internal error.");
			}finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(p);
			DatabaseHelper.close(conn);
			}
			return  dtool ;	
		}
		   /*  added by shruthi for TENJINCG-1264 ends*/

}
