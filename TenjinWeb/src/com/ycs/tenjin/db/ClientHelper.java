/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ClientHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                	CHANGED BY              DESCRIPTION
* 19-Oct-2017			Sriram Sridharan		Entire file changed for TENJINCG-397
* 15-11-2017			Preeti Singh			TENJINCG-410
* 28-06-2018            Padmavathi              T251IT-160
* 28-02-2019             Leelaprasad P           TENJINCG-983
* 08-03-2019			Sahana					pCloudy
* 05-02-2020			Roshni					TENJINCG-1168
* */

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.util.Constants;

/* Entire class changed for TENJINCG-397 */

public class ClientHelper {
	private static final Logger logger = LoggerFactory.getLogger(ClientHelper.class);
	
	public List<RegisteredClient> hydrateAllClients() throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}


		ArrayList<RegisteredClient> clientList = null;

		try {
			pst = conn.prepareStatement("SELECT * FROM MASREGCLIENTS");
			rs = pst.executeQuery();
			clientList = new ArrayList<RegisteredClient>();
			while (rs.next()) {

				RegisteredClient rc = this.buildRegClientObject(rs);
				clientList.add(rc);
			}

			
		}catch (Exception e) {
			throw new DatabaseException("Could not fetch Applications Functions", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return clientList;

	}
	/* added by Roshni for TENJINCG-1168 starts */
	public List<RegisteredClient> hydrateAllClients(Connection conn) throws DatabaseException {
		PreparedStatement pst=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}


		ArrayList<RegisteredClient> clientList = null;

		try {
			pst = conn.prepareStatement("SELECT * FROM MASREGCLIENTS");
			rs = pst.executeQuery();
			clientList = new ArrayList<RegisteredClient>();
			while (rs.next()) {
				RegisteredClient rc = this.buildRegClientObject(rs);
				clientList.add(rc);
			}
		}catch (Exception e) {
			throw new DatabaseException("Could not fetch Applications Functions", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return clientList;

	}
	/* added by Roshni for TENJINCG-1168 ends */
	public RegisteredClient hydrateClient(String clientName) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		RegisteredClient rc = null;
		try {
			rc = this.hydrateClient(conn, clientName);
		} finally {
			
			DatabaseHelper.close(conn);
		}
		
		return rc;
	}
	
	
	public RegisteredClient hydrateClient(String hostName, int port) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no connection");
		}
		
		RegisteredClient rc = null;
		try {
			rc = this.hydrateClient(conn, hostName, Integer.toString(port));
		} finally {
			
			DatabaseHelper.close(conn);
		}
		
		return rc;
	}
	
	public RegisteredClient hydrateClient(int clientRecId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no connection");
		}
		
		RegisteredClient rc = null;
		try {
			rc = this.hydrateClient(conn, clientRecId);
		} finally {
			
			DatabaseHelper.close(conn);
		}
		
		return rc;
	}
	
	
	
	public RegisteredClient persistRegClient(RegisteredClient regclient)  throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}

		try {
		
			/*Changed by Leelaprasad for TENJINCG-983 starts*/
			/*pst = conn.prepareStatement("INSERT INTO MASREGCLIENTS (RC_REC_ID,RC_NAME,RC_HOST,RC_PORT,RC_CREATED_ON,RC_CREATED_BY,RC_STATUS) VALUES (?,?,?,?,?,?,?)");*/
			/*Changed by Sahana for pCloudy: starts*/
			//pst = conn.prepareStatement("INSERT INTO MASREGCLIENTS (RC_REC_ID,RC_NAME,RC_HOST,RC_PORT,RC_CREATED_ON,RC_CREATED_BY,RC_STATUS,RC_OS_TYPE) VALUES (?,?,?,?,?,?,?,?)");
			pst = conn.prepareStatement("INSERT INTO MASREGCLIENTS (RC_REC_ID,RC_NAME,RC_HOST,RC_PORT,RC_CREATED_ON,RC_CREATED_BY,RC_STATUS,RC_OS_TYPE,RC_DEVICE_FARM_FLAG,RC_DEVICE_FARM_USERNAME,RC_DEVICE_FARM_PASSWORD,RC_DEVICE_FARM_KEY) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
			/*Changed by Sahana for pCloudy: ends*//*Changed by Leelaprasad for TENJINCG-983 ends*/
			  int recId = DatabaseHelper.getGlobalSequenceNumber(conn);
			pst.setInt(1, recId);
			pst.setString(2, regclient.getName());
			pst.setString(3, regclient.getHostName());
			pst.setString(4, regclient.getPort());
			pst.setTimestamp(5, new Timestamp(regclient.getCreatedOn().getTime()));
			pst.setString(6, regclient.getCreatedBy());
			pst.setString(7, "IDLE");
			/*Changed by Leelaprasad for TENJINCG-983 starts*/
			pst.setString(8, regclient.getOsType());
			/*Changed by Leelaprasad for TENJINCG-983 ends*/
			/*Added by Sahana for pCloudy:Starts*/
			pst.setString(9, regclient.getDeviceFarmCheck());
			pst.setString(10, regclient.getDeviceFarmUsrName());
			pst.setString(11,regclient.getDeviceFarmPwd());
			pst.setString(12,regclient.getDeviceFarmKey());
			/*Added by Sahana for pCloudy:ends*/
			pst.execute();
			/*pst.close();*/
			
			regclient.setRecordId(recId);
		}catch(DatabaseException e){
			logger.error("ERROR persisting client", e);
			throw e;
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new DatabaseException("Could not create Client", e);
		} finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return regclient;
	}
	
	public void updateClient(RegisteredClient regclient1,String exName) throws  DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
		
			/*Changed by Leelaprasad for TENJINCG-983 starts*/
		   /* pst = conn.prepareStatement("UPDATE MASREGCLIENTS SET RC_NAME=?,RC_HOST=?, RC_PORT = ?, RC_STATUS=? WHERE RC_NAME=?");*/
			 pst = conn.prepareStatement("UPDATE MASREGCLIENTS SET RC_NAME=?,RC_HOST=?, RC_PORT = ?, RC_STATUS=?,RC_OS_TYPE=? WHERE RC_NAME=?");
		    /*Changed by Leelaprasad for TENJINCG-983 ends*/
			
			pst.setString(1,regclient1.getName());
		   
			pst.setString(2, regclient1.getHostName());
			
		    pst.setString(3, regclient1.getPort());
			
			pst.setString(4, "IDLE");
			/*Changed by Leelaprasad for TENJINCG-983 starts*/
			/*pst.setString(5, exName);*/
			pst.setString(5, regclient1.getOsType());
			pst.setString(6, exName);
			/*Changed by Leelaprasad for TENJINCG-983 ends*/
			
			pst.execute();
	       
	    
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new DatabaseException(
					"Could not update Client because of an internal error ");
		}
		finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
	
	public void updateClient(RegisteredClient client, int clientRecordId) throws  DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
		
			/*Changed by Leelaprasad for TENJINCG-983 starts*/
			/*pst = conn.prepareStatement("UPDATE MASREGCLIENTS SET RC_NAME=?,RC_HOST=?, RC_PORT = ?, RC_STATUS=? WHERE RC_REC_ID=?");*/
			/*Changed by Sahana for pCloudy: starts*/
			//pst = conn.prepareStatement("UPDATE MASREGCLIENTS SET RC_NAME=?,RC_HOST=?, RC_PORT = ?, RC_STATUS=?,RC_OS_TYPE=? WHERE RC_REC_ID=?");
			pst = conn.prepareStatement("UPDATE MASREGCLIENTS SET RC_NAME=?,RC_HOST=?, RC_PORT = ?, RC_STATUS=?,RC_OS_TYPE=?, RC_DEVICE_FARM_FLAG=?, RC_DEVICE_FARM_USERNAME=?, RC_DEVICE_FARM_PASSWORD=?, RC_DEVICE_FARM_KEY=? WHERE RC_REC_ID=?");
			/*Changed by Sahana for pCloudy: ends*/
			/*Changed by Leelaprasad for TENJINCG-983 ends*/
			
			pst.setString(1,client.getName());
		   
			pst.setString(2, client.getHostName());
			
		    pst.setString(3, client.getPort());
			
			pst.setString(4, "IDLE");
			/*Changed by Leelaprasad for TENJINCG-983 starts*/
			/*pst.setInt(5, clientRecordId);*/
			pst.setString(5, client.getOsType());
			/*Commented by Sahana */
			//pst.setInt(6, clientRecordId);
			/*Changed by Leelaprasad for TENJINCG-983 ends*/
			/*Added by Sahana for pCloudy: starts*/
			pst.setString(6, client.getDeviceFarmCheck());
			pst.setString(7, client.getDeviceFarmUsrName());
			pst.setString(8, client.getDeviceFarmPwd());
			pst.setString(9, client.getDeviceFarmKey());
			pst.setInt(10, clientRecordId);
			/*Added by Sahana for pCloudy: ends*/
			pst.execute();
	       
	    
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new DatabaseException(
					"Could not update Client because of an internal error ");
		}
		finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
	
	private RegisteredClient hydrateClient(Connection conn, String clientName) throws DatabaseException {
		RegisteredClient client;
		PreparedStatement pst =null;
		ResultSet rs=null;
		try {
			pst = conn.prepareStatement("SELECT * FROM MASREGCLIENTS WHERE RC_NAME =?");
			pst.setString(1, clientName);
			
		    rs = pst.executeQuery();
			client = null;
			while(rs.next()) {
				client = this.buildRegClientObject(rs);
			}
			
			
		} catch (SQLException e) {
			logger.error("ERROR fetching client information", e);
			throw new DatabaseException("Could not fetch information about client machine. Please contact Tenjin Support.");
		}
		finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			
		}
		
		return client;
	}
	
	public RegisteredClient hydrateClient(Connection conn, String hostName, String port) throws DatabaseException {
		RegisteredClient client;
		PreparedStatement pst =null;
		ResultSet rs=null;
		try {
			pst = conn.prepareStatement("SELECT * FROM MASREGCLIENTS WHERE RC_HOST =? AND RC_PORT=?");
			pst.setString(1, hostName);
			pst.setString(2, port);
			
			rs = pst.executeQuery();
			client = null;
			while(rs.next()) {
				client = this.buildRegClientObject(rs);
			}
			
			/*rs.close();
			
			pst.close();*/
		} catch (SQLException e) {
			logger.error("ERROR fetching client information", e);
			throw new DatabaseException("Could not fetch information about client machine. Please contact Tenjin Support.");
		}
		finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			
		}
		return client;
	}
	/*Added by Padmavathi for T251IT-160 starts*/
	public RegisteredClient hydrateClientByHostName( String hostName) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		RegisteredClient client;
		PreparedStatement pst =null;
		ResultSet rs=null;
		try {
			pst = conn.prepareStatement("SELECT * FROM MASREGCLIENTS WHERE RC_HOST =?");
			pst.setString(1, hostName);			
			rs = pst.executeQuery();
			client = null;
			while(rs.next()) {
				client = this.buildRegClientObject(rs);
			}
			
		} catch (SQLException e) {
			logger.error("ERROR fetching client information", e);
			throw new DatabaseException("Could not fetch information about client machine. Please contact Tenjin Support.");
		}
		finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
			
		}
		return client;
	}
	/*Added by Padmavathi for T251IT-160 ends*/
	public RegisteredClient hydrateClient(Connection conn, int clientRecId) throws DatabaseException {
		RegisteredClient client;
		PreparedStatement pst =null;
		ResultSet rs=null;
		try {
			pst = conn.prepareStatement("SELECT * FROM MASREGCLIENTS WHERE RC_REC_ID =?");
			pst.setInt(1, clientRecId);
			
			rs = pst.executeQuery();
			client = null;
			while(rs.next()) {
				client = this.buildRegClientObject(rs);
			}
			
			
		} catch (SQLException e) {
			logger.error("ERROR fetching client information", e);
			throw new DatabaseException("Could not fetch information about client machine. Please contact Tenjin Support.");
		}
		finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			
		}
		return client;
	}
	
	public void removeClient(int clientRecordId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		
		try {
			this.removeClient(conn, clientRecordId);
		} finally {
			
			DatabaseHelper.close(conn);
		}
		
	}
	
	public void removeClient(int[] clientRecordIds) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		
		try {
			this.removeClient(conn, clientRecordIds);
		} finally {
			
			DatabaseHelper.close(conn);
		}
		
	}
	
	public void removeClient(Connection conn, int clientRecordId) throws DatabaseException {
		PreparedStatement pst=null;
		try {
			pst = conn.prepareStatement("DELETE FROM MASREGCLIENTS WHERE RC_REC_ID=?");
			pst.setInt(1, clientRecordId);
			
			pst.executeUpdate();
			
		} catch (SQLException e) {
			logger.error("ERROR removing client",e );
			throw new DatabaseException("Could not delete client due to an internal error.", e);
		}finally{
			DatabaseHelper.close(pst);
		}
	}
	/*modified by shruthi for sql injection starts*/
	public void removeClient(Connection conn, int[] clientRecordIds) throws DatabaseException {
		PreparedStatement pst=null;
		for(int str : clientRecordIds) {
		try {
			
			pst = conn.prepareStatement("DELETE FROM MASREGCLIENTS WHERE RC_REC_ID=?");
			pst.setInt(1, str);
			pst.executeUpdate();
			
		} catch (SQLException e) {
			logger.error("ERROR removing client",e );
			throw new DatabaseException("Could not delete client due to an internal error.", e);
		}finally{
			DatabaseHelper.close(pst);
		}
		}
		/*modified by shruthi for sql injection ends*/
	}
	
	
	private RegisteredClient buildRegClientObject(ResultSet rs) throws SQLException {
		RegisteredClient client = new RegisteredClient();
		client.setRecordId(rs.getInt("RC_REC_ID"));
		client.setName(rs.getString("RC_NAME"));
		client.setHostName(rs.getString("RC_HOST"));
		client.setPort(rs.getString("RC_PORT"));
		client.setCreatedBy(rs.getString("RC_CREATED_BY"));
		client.setCreatedOn(new Date(rs.getTimestamp("RC_CREATED_ON").getTime()));
		client.setStatus(rs.getString("RC_STATUS"));
		/*Changed by Leelaprasad for TENJINCG-983 starts*/
		client.setOsType(rs.getString("RC_OS_TYPE"));
		/*Changed by Leelaprasad for TENJINCG-983 ends*/
		/*Added by Sahana for pCloudy: starts*/
		client.setDeviceFarmCheck(rs.getString("RC_DEVICE_FARM_FLAG"));
		client.setDeviceFarmUsrName(rs.getString("RC_DEVICE_FARM_USERNAME"));
		client.setDeviceFarmPwd(rs.getString("RC_DEVICE_FARM_PASSWORD"));
		client.setDeviceFarmKey(rs.getString("RC_DEVICE_FARM_KEY"));
		/*Added by Sahana for pCloudy: starts*/
		
		return client;
		
	}
	
	public boolean isClientBusy(int clientRecordId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pst =null;
		ResultSet rs=null;
		try {
			pst = conn.prepareStatement("SELECT run_status FROM MASTESTRUNS WHERE RUN_MACHINE_IP IN (SELECT RC_HOST FROM MASREGCLIENTS WHERE RC_REC_ID=?) AND RUN_STATUS IN (?, ?)");
			pst.setInt(1, clientRecordId);
			pst.setString(2, BridgeProcess.NOT_STARTED);
			pst.setString(3, BridgeProcess.IN_PROGRESS);
			rs = pst.executeQuery();
			
			boolean busy = false;
			
			while(rs.next()) {
				busy = true;
			}
			
			
			return busy;
		} catch (SQLException e) {
			logger.error("ERROR getting client status", e);
			throw new DatabaseException("Could not get client status due to an internal error. Please contact Tenjin Support.");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
}
