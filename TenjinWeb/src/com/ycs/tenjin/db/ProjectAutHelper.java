/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectAutHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 23-10-2018		   	Pushpalatha			  	Newly Added 
* 08-11-2018            Ramya                   for TENJINCG-831
* 24-04-2019			Roshni					TENJINCG-1038\
* 27-05-2019			Prem					 V2.8-58
* 14-11-2019			Roshni					TENJINCG-1166
* 05-02-2020			Roshni					TENJINCG-1168
*/
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class ProjectAutHelper {
	private static final Logger logger = LoggerFactory.getLogger(ProjectHelper.class);

	public ArrayList<Aut> hydrateUnmappedAuts(int projectId) throws DatabaseException {

		ArrayList<Aut> autList = null;
		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement(
						"SELECT * FROM MASAPPLICATION WHERE APP_ID NOT IN (SELECT APP_ID FROM TJN_PRJ_AUTS WHERE PRJ_ID = ?)");) {
			pst.setInt(1, projectId);
			autList = new ArrayList<Aut>();
			try (ResultSet rs = pst.executeQuery();) {
				while (rs.next()) {
					Aut aut = new Aut();
					aut.setId(rs.getInt("APP_ID"));
					aut.setName(rs.getString("APP_NAME"));
					aut.setURL(rs.getString("APP_URL"));
					aut.setLoginReqd(rs.getString("APP_LOGIN_REQD"));
					aut.setLoginName(rs.getString("APP_LOGIN_NAME"));
					aut.setPassword(rs.getString("APP_PASSWORD"));
					aut.setBrowser(rs.getString("APP_DEF_BROWSER"));

					aut.setApplicationType(rs.getInt("APP_TYPE"));
					aut.setPlatform(rs.getString("APP_PLATFORM"));
					aut.setPlatformVersion(rs.getString("APP_PLATFORM_VERSION"));
					aut.setApplicationPackage(rs.getString("APP_PACKAGE"));
					aut.setApplicationActivity(rs.getString("APP_ACTIVITY"));
					autList.add(aut);
				}
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch Applications Under Test", e);
		}
		return autList;
	}

	
	public ArrayList<Aut> hydratePrjectAuts(int projectId) throws DatabaseException {
		ArrayList<Aut> autList = null;
		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				
				
				/*Modified by Roshni for TENJINCG-1038 starts*/
				PreparedStatement pst = conn.prepareStatement(
						"SELECT A.*,B.APP_NAME,B.TEMPLATE_PASSWORD FROM TJN_PRJ_AUTS A,MASAPPLICATION B WHERE B.APP_ID=A.APP_ID AND PRJ_ID = ? ");){
				/*Modified by Roshni for TENJINCG-1038 starts*/
			pst.setInt(1, projectId);
			autList = new ArrayList<Aut>();
			try (ResultSet rs = pst.executeQuery();) {
				while (rs.next()) {
					Aut aut = new Aut();
					aut.setId(rs.getInt("APP_ID"));
					aut.setName(rs.getString("APP_NAME"));
					if(rs.getString("AUT_URL")==null){
						aut.setURL("");
					}else{
					
						aut.setURL(rs.getString("AUT_URL"));
					}
					
					if(rs.getString("TEST_DATA_PATH")==null){
						aut.setTestDataPath("");
					}else{
					aut.setTestDataPath(rs.getString("TEST_DATA_PATH"));
					}
					
					/*Modified by Roshni for TENJINCG-1166 starts*/
					aut.setId(rs.getInt("APP_ID"));
					aut.setName(Utilities.escapeXml(rs.getString("APP_NAME")));
					if(rs.getString("AUT_URL")==null){
						aut.setURL("");
					}else{
					
						aut.setURL(Utilities.escapeXml(rs.getString("AUT_URL")));
					}
					/* Added by Roshni for TENJINCG-1038 starts */
					/* Modified  by Pushpa for TV2.8R2-7 starts */
					if(rs.getString("TEMPLATE_PASSWORD")==null || rs.getString("TEMPLATE_PASSWORD").equals("")){
						/* Modified  by Pushpa for TV2.8R2-7 End */
						aut.setTemplatePwd("");
					}else{
					aut.setTestDataPath(Utilities.escapeXml(rs.getString("TEST_DATA_PATH")));
					}
					/*Modified by Roshni for TENJINCG-1166 ends*/
					autList.add(aut);
				}
			}
			
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch Applications Under Test", e);
		}
		return autList;
	}

	public void mapAutsToProject(int projectId, String auList) throws DatabaseException {
		
		PreparedStatement pst = null;
		AutsHelper uHelper = new AutsHelper();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);){
			String[] arr = auList.split(";");
			for(String userId:arr){
				try{
					/* modified by Roshni for TENJINCG-1168 starts */
					Aut aut = uHelper.hydrateAut(conn,Integer.parseInt(userId));
					/* modified by Roshni for TENJINCG-1168 ends */
				if(aut != null){
					
					
					pst = conn.prepareStatement("INSERT INTO TJN_PRJ_AUTS (PRJ_ID,APP_ID,DTT_PPROJECT_KEY,PRJ_DEF_LOGGING,PRJ_DEF_MANAGER,PRJ_DM_PROJECT,AUT_URL,TEST_DATA_PATH) VALUES (?,?,?,?,?,?,?,?)");
					
					pst.setInt(1, projectId);
					pst.setInt(2, aut.getId());
					
					pst.setString(3, "");
					pst.setString(4, "N");
					pst.setString(5, "");
					pst.setString(6, "");
					pst.setString(7, aut.getURL());
					pst.setString(8, aut.getTestDataPath());
					
					
					
					pst.execute();
					pst.close();
				}else{

				}
			}catch(Exception e){
				logger.error("ERROR --> Could not map AUT(s) to project",e );
				throw new DatabaseException("An internal error occurred while adding the Applications to the Project",e);
			}finally{
				DatabaseHelper.close(pst);
			}
			}

		}catch(Exception e){
			logger.error("ERROR --> Could not map AUT(s) to project",e );
			throw new DatabaseException("An internal error occurred while adding the Applications to the Project",e);
		}finally{
			
			DatabaseHelper.close(pst);
			
		}


	}
 
	
	public void unmapAutsFromProject(int projectId, String autList) throws DatabaseException, SQLException, RequestValidationException {
		
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			){
			
			String[] auts = autList.split(";");
			for(String aut:auts){
				 /*added by Ramya for TENJINCG-831 starts*/
				try(PreparedStatement pst1=conn.prepareStatement("SELECT * FROM TESTSTEPS WHERE TC_REC_ID IN (SELECT TC_REC_ID FROM TESTCASES WHERE TC_PRJ_ID=?) AND APP_ID=?")){
					pst1.setInt(1, projectId);
					pst1.setInt(2, Integer.parseInt(aut));
					try(ResultSet rs=pst1.executeQuery();){
						while(rs.next()){
							/*Modified by Prem for V2.8-58 start*/
							throw new RequestValidationException("Could not delete AUT because it contains some steps ");
							/*Modified by Prem for V2.8-58 ends*/
						}
					}
				}
				 /*added by Ramya for TENJINCG-831 ends*/
				
				try(PreparedStatement pst = conn.prepareStatement("DELETE FROM TJN_PRJ_AUTS WHERE PRJ_ID = ? AND APP_ID = ?");){
					pst.setInt(1, projectId);
					pst.setInt(2, Integer.parseInt(aut));
					pst.execute();
					pst.close();
				}
				
				/*added by Ramya for TENJINCG-831 starts*/
				//conn.commit();
				/*added by Ramya for TENJINCG-831 ends*/
			}
		}catch(DatabaseException e){
			throw new DatabaseException("Could not remove the selected AUTs from project",e);
		}
		
	}

	public void updateAutUrl(ArrayList<Aut> autList, int projectId) throws DatabaseException {

		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);) {
			for (Aut aut : autList) {
				try (PreparedStatement pst = conn.prepareStatement(
						"UPDATE TJN_PRJ_AUTS SET AUT_URL = ?,TEST_DATA_PATH=? WHERE APP_ID = ? AND PRJ_ID=? ");) {
					pst.setString(1, aut.getURL());
					pst.setString(2, aut.getTestDataPath());
					pst.setInt(3, aut.getId());
					pst.setInt(4, projectId);
					pst.executeUpdate();
				}
			}
		} catch (SQLException e) {
			logger.error("Could not update TJN_PRJ_AUTS due to " + e.getMessage());
		}

	}

}
