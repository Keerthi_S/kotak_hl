/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DomainHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 02-Aug-2017          	Gangadhar Badagi        TENJINCG-181
* 02-Aug-2017          	Gangadhar Badagi        TENJINCG-182
* 17-08-2017           	Padmavathi              T25IT-148
* 26-08-2017           	Padmavathi              T25IT-303
* 19-01-2018           	Padmavathi              for TENJINCG-576
* 16-03-2018		   	Preeti				   	TENJINCG-615	
* 20-04-2018			Preeti					TENJINCG-616
* 02-05-2018			Preeti					TENJINCG-656
* 14-06-2018			Preeti					T251IT-13
*/

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;



import com.ycs.tenjin.project.Domain;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.ProjectMail;
import com.ycs.tenjin.util.Constants;

/*Change History
02-Mar-2015 R2.1 Database agnostic Changes: Babu: column(DOMAIN_NAME) should be specified for insertion at line 156
*/
public class DomainHelper {

	public ArrayList<Domain> fetchAllDomains() throws DatabaseException {
		ArrayList<Domain> domains = null;
		Statement st = null;
		ResultSet rs = null;
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if (conn == null) {
			throw new DatabaseException("Invalid database connection");
		}

		try {
			st = conn.createStatement();
			rs = st.executeQuery("SELECT * FROM TJN_DOMAINS ORDER BY DOMAIN_NAME");
			domains = new ArrayList<Domain>();
			while (rs.next()) {
				Domain domain = new Domain();
				domain.setName(rs.getString("DOMAIN_NAME"));
				domain.setCreatedDate(rs.getTimestamp("DOMAIN_CREATE_DATE"));
				domain.setOwner(rs.getString("DOMAIN_CREATED_BY"));
				domains.add(domain);
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch records", e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(st);
			DatabaseHelper.close(conn);
		}

		return domains;

	}

	public ArrayList<Project> getProjectsForDomain(String domainName) throws DatabaseException {
		ArrayList<Project> projects = null;

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid database connection");
		}

		try {
			pst = conn.prepareStatement("select * from TJN_PROJECTS where prj_domain = ? order by prj_name");
			pst.setString(1, domainName);
			rs = pst.executeQuery();
			projects = new ArrayList<Project>();
			while (rs.next()) {
				Project project = new Project();
				project.setId(rs.getInt("PRJ_ID"));
				project.setName(rs.getString("PRJ_NAME"));
				project.setDbServer(rs.getString("PRJ_DB_SERVER"));
				project.setType(rs.getString("PRJ_TYPE"));
				project.setDomain(rs.getString("PRJ_DOMAIN"));
				project.setOwner(rs.getString("PRJ_OWNER"));
				project.setState(rs.getString("PRJ_STATE"));
				project.setScreenshotoption(rs.getString("PRJ_SCREENSHOT_OPTIONS"));
				/* Commented by Preeti for TENJINCG-656 starts */
				/* Added by Padmavathi for TENJINCG_576-starts */
				/* project.setEmailNotification(rs.getString("PRJ_MAIL_NOTIFICATIONS")); */
				/* Added by Padmavathi for TENJINCG_576-ends */
				/* Added by Preeti for TENJINCG-615 starts */
				/* project.setEmailSubject(rs.getString("PRJ_MAIL_SUBJECT")); */
				/* Added by Preeti for TENJINCG-615 ends */
				/* Commented by Preeti for TENJINCG-656 ends */
				projects.add(project);
			}
			
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return projects;
	}
	/*modified by shruthi for sql injection starts*/
	/* Modified by Preeti for TENJINCG-616 starts */
	public Domain fetchDomain(String domainName) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		Domain domain = null;
		/* Added by Gangadhar Badagi for TENJINCG-181 starts */
		ArrayList<Project> projects = new ArrayList<Project>();
		/* Added by Gangadhar Badagi for TENJINCG-181 starts */
		try {
			
			pst = conn.prepareStatement("SELECT * FROM TJN_DOMAINS WHERE DOMAIN_NAME =? ORDER BY DOMAIN_NAME");
			pst.setString(1, domainName);
			rs=pst.executeQuery();
			while (rs.next()) {
				domain = new Domain();
				domain.setName(rs.getString("DOMAIN_NAME"));
				domain.setCreatedDate(rs.getTimestamp("DOMAIN_CREATE_DATE"));
				domain.setOwner(rs.getString("DOMAIN_CREATED_BY"));

				/* Added by Gangadhar Badagi for TENJINCG-181 starts */
				/*rs2 = st1.executeQuery(
						"SELECT * FROM TJN_PROJECTS WHERE PRJ_DOMAIN ='" + domainName + "' ORDER BY PRJ_DOMAIN");*/
				pst1 = conn.prepareStatement("SELECT * FROM TJN_PROJECTS WHERE PRJ_DOMAIN =? ORDER BY PRJ_DOMAIN");
				pst1.setString(1, domainName);
				rs2=pst1.executeQuery();
				pst1.close();
				while (rs2.next()) {
					Project project = new Project();
					project.setName(rs2.getString("PRJ_NAME"));
					project.setId(rs2.getInt("PRJ_ID"));
					/* Added by Padmavathi for T25IT-148-starts */
					project.setCreatedDate(rs2.getTimestamp("PRJ_CREATE_DATE"));
					project.setDescription(rs2.getString("PRJ_DESC"));
					String state = rs2.getString("PRJ_STATE");
					if (state.equalsIgnoreCase("I")) {
						project.setState("Inactive");
					} else if (state.equalsIgnoreCase("A")) {
						project.setState("Active");
					} else {
						project.setState(rs2.getString("PRJ_STATE"));
					}
					project.setType(rs2.getString("PRJ_TYPE"));
					project.setOwner(rs2.getString("PRJ_OWNER"));
					project.setDomain(rs2.getString("PRJ_DOMAIN"));
					/* Added by Padmavathi for T25IT-303-starts */
					project.setScreenshotoption(rs2.getString("PRJ_SCREENSHOT_OPTIONS"));

					projects.add(project);
				}
				domain.setProjects(projects);
			}

			/* Added by Gangadhar Badagi for TENJINCG-181 starts */
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch records", e);
		} finally {
			DatabaseHelper.close(rs2);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return domain;
	}
	/* Modified by Preeti for TENJINCG-616 ends */
	/*modified by shruthi for sql injection ends*/
	
	/*modified by shruthi for sql injection starts*/
	/* Added by Gangadhar Badagi for TENJINCG-182 starts */
	public Project fetchProjectDetailsForDomain(String domainName, String projectName) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		Project project = null;
		try {
			
			pst = conn.prepareStatement("SELECT * FROM TJN_PROJECTS WHERE PRJ_DOMAIN =? AND PRJ_NAME=? ORDER BY PRJ_DOMAIN");
			pst.setString(1,  domainName);
			pst.setString(2,  projectName);
			rs=pst.executeQuery();
			while (rs.next()) {
				project = new Project();
				;
				project.setName(rs.getString("PRJ_NAME"));
				project.setId(rs.getInt("PRJ_ID"));
				project.setDomain(rs.getString("PRJ_DOMAIN"));
				project.setOwner(rs.getString("PRJ_OWNER"));
				project.setType(rs.getString("PRJ_TYPE"));
				project.setScreenshotoption(rs.getString("PRJ_SCREENSHOT_OPTIONS"));
				project.setState(rs.getString("PRJ_STATE"));
				project.setDescription(rs.getString("PRJ_DESC"));
				project.setCreatedDate(Timestamp.valueOf(rs.getString("PRJ_CREATE_DATE")));

				ProjectMail projectMail = new ProjectMailHelper().hydrateProjectMail(rs.getInt("PRJ_ID"));
				project.setProjectMail(projectMail);
				/* Added by Preeti for TENJINCG-656 ends */
			}
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch records", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return project;
	}
	/* Added by Gangadhar Badagi for TENJINCG-182 ends */
	/*modified by shruthi for sql injection ends*/
}
