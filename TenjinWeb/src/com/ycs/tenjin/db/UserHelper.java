/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UserHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              	DESCRIPTION

21-12-2018				Prem					 TJN262R2-46
24-12-2018				Preeti					TJN262R2-66
26-12-2018				Prem					 TJN262R2-46
19-02-2019				Preeti					TENJINCG-928
24-06-2019              Padmavathi              for license
*/

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.DuplicateUserSessionException;
import com.ycs.tenjin.handler.TenjinSessionHandler;
import com.ycs.tenjin.user.FailedLoginAttempts;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Constants;

public class UserHelper {
	private static Logger logger = LoggerFactory.getLogger(UserHelper.class);

	private boolean status = false;

	public static String user_id;
	public static String machinIp;
	public static Date loginTime;

	public boolean isUserSessionAlive(String userId, String sessionKey) throws DatabaseException {
		/* Modified by Padmavathi for TJNUN262-79 ends */
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean sessionAlive = false;
		try {
			/* Modified by Padmavathi for TJNUN262-79 starts */
			pst = conn.prepareStatement("SELECT * FROM TJN_CURRENT_USERS WHERE USER_ID = ? and TJN_SESSION_ID= ?");
			/* Modified by Padmavathi for TJNUN262-79 ends */
			pst.setString(1, userId);
			pst.setString(2, sessionKey);
			rs = pst.executeQuery();

			while (rs.next()) {
				sessionAlive = true;
				break;
			}

		} catch (Exception e) {
			logger.error("ERROR Session Alive check failed", e);
			try {
				conn.close();
			} catch (Exception e1) {

			}
			throw new DatabaseException("Could not check if session is alive");
		}

		finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return sessionAlive;
	}

	public ArrayList<User> hydrateUsersNotInProject(int projectId) throws DatabaseException {

		Connection conn, pConn;
		conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		pConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		if (pConn == null) {
			throw new DatabaseException("Could not connect to Tenjin Project Database");
		}

		ArrayList<User> users = new ArrayList<User>();
		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement oPst = null;
		ResultSet oRs = null;
		try {
			pst = conn.prepareStatement("SELECT * FROM MASUSER ORDER BY USER_ID");
			rs = pst.executeQuery();

			while (rs.next()) {
				User user = new User();
				user.setId(rs.getString("USER_ID"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setLastName(rs.getString("LAST_NAME"));
				user.setPassword(rs.getString("PASS_WORD"));
				user.setPrimaryPhone(rs.getString("PHONE_1"));
				user.setSecondaryPhone(rs.getString("PHONE_2"));
				user.setEmail(rs.getString("EMAIL_ID"));

				if (user.getLastName() != null && !user.getLastName().equalsIgnoreCase("")) {
					user.setFullName(user.getFirstName() + " " + user.getLastName());
				} else {
					user.setFullName(user.getFirstName());
				}
				oPst = pConn.prepareStatement(
						"SELECT COUNT(USER_ID) AS CNT FROM TJN_PRJ_USERS WHERE PRJ_ID = ? AND USER_ID = ?");
				oPst.setInt(1, projectId);
				oPst.setString(2, user.getId());
				oRs = oPst.executeQuery();
				int pCnt = 0;
				while (oRs.next()) {
					pCnt = oRs.getInt("CNT");
				}

				if (pCnt < 1) {
					users.add(user);
				}

				oRs.close();
				oPst.close();

			}

			rs.close();
			pst.close();
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch unmapped users", e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(oRs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(oPst);
			DatabaseHelper.close(pConn);
			DatabaseHelper.close(conn);
		}

		return users;

	}

	public ArrayList<User> hydrateCurrentUsersInProject(int projectId) throws DatabaseException {

		Connection conn, pConn;
		conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		pConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		if (pConn == null) {
			throw new DatabaseException("Could not connect to Tenjin Project Database");
		}

		ArrayList<User> users = new ArrayList<User>();

		try {
			pst = conn.prepareStatement(
					"SELECT * FROM MASUSER WHERE USER_ID in (SELECT USER_ID AS CNT FROM TJN_PRJ_USERS WHERE PRJ_ID =?)");
			pst.setInt(1, projectId);
			rs = pst.executeQuery();

			while (rs.next()) {
				User user = new User();
				user.setId(rs.getString("USER_ID"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setLastName(rs.getString("LAST_NAME"));
				user.setPassword(rs.getString("PASS_WORD"));
				user.setPrimaryPhone(rs.getString("PHONE_1"));
				user.setSecondaryPhone(rs.getString("PHONE_2"));
				user.setEmail(rs.getString("EMAIL_ID"));

				if (user.getLastName() != null && !user.getLastName().equalsIgnoreCase("")) {
					user.setFullName(user.getFirstName() + " " + user.getLastName());
				} else {
					user.setFullName(user.getFirstName());
				}
				users.add(user);

			}

			/*
			 * rs.close(); pst.close();
			 */
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch unmapped users", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(pConn);
			DatabaseHelper.close(conn);
		}

		return users;

	}
/*modified by shruthi for VAPT helper fixes starts*/
	public User hydrateUser(String userid, Connection conn) throws DatabaseException {


		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
	
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		PreparedStatement pst2 = null;
		PreparedStatement  st= null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		User user = null;
		try {

			st = conn.prepareStatement("SELECT  COUNT(A.USER_ID) AS ACTIVE_USER_COUNT FROM TJN_CURRENT_USERS A, MASUSER B WHERE B.USER_ID = A.USER_ID");
			pst = conn.prepareStatement("SELECT * FROM MASUSER WHERE USER_ID =?");
			pst.setString(1, userid);
			rs = pst.executeQuery();

			while (rs.next()) {
				user = new User();
				String roles = "";
				String tStamp = "";
				int count = 0;
				user.setId(rs.getString("USER_ID"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setLastName(rs.getString("LAST_NAME"));
				user.setPassword(rs.getString("PASS_WORD"));
				user.setPrimaryPhone(rs.getString("PHONE_1"));
				user.setSecondaryPhone(rs.getString("PHONE_2"));
				user.setEmail(rs.getString("EMAIL_ID"));
				if (user.getLastName() != null && !user.getLastName().equalsIgnoreCase("")) {
					user.setFullName(user.getFirstName() + " " + user.getLastName());
				} else {
					user.setFullName(user.getFirstName());
				}
				if (user != null) {

					pst1 = conn.prepareStatement("SELECT A.GROUP_DESC FROM MASUSERGROUPS A, USERROLES B WHERE A.GROUP_ID = B.GROUP_ID AND B.USER_ID =?");
					pst1.setString(1, user.getId());
					rs1 = pst1.executeQuery();

					while (rs1.next()) {
						roles = roles + rs1.getString("GROUP_DESC") + ",";
					}

					rs1.close();

					if (roles != null && !roles.equalsIgnoreCase("")) {
						if (roles.charAt(roles.length() - 1) == ',') {
							roles = roles.substring(0, roles.length() - 1);
						}

						user.setRoles(roles);
					}

					pst2= conn.prepareStatement("Select * From USERSESSIONS Where Us_Id In (Select coalesce(MAX(US_ID),0) AS US_ID FROM USERSESSIONS WHERE US_USER_ID =?)");
					pst2.setString(1, user.getId());
					rs2 = pst2.executeQuery();
					
					while (rs2.next()) {
						tStamp = rs2.getString("US_LOGIN_TIMESTAMP");
					}

					rs2.close();

					user.setLastLogin(tStamp);

					rs3 = st.executeQuery();
					/* 02-Mar-2015 R2.1 Database agnostic Changes: Ends */
					while (rs3.next()) {
						count = rs3.getInt("ACTIVE_USER_COUNT");
					}
					user.setMaxNumberUsers(count);
					rs3.close();
				}

			}
	        st.close();
			pst.close();
			pst1.close();
			pst2.close();
			st.close();
			rs.close();
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(rs2);
			DatabaseHelper.close(rs3);
			DatabaseHelper.close(st);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(pst);

		}

		return user;
	}
	/*modified by shruthi for VAPT helper fixes ends*/
	/*modified by shruthi for VAPT helper fixes starts*/
	public User hydrateUser(String userid) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement st = null;
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		PreparedStatement pst2 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		User user = null;
		try {

			st = conn.prepareStatement("SELECT  COUNT(A.USER_ID) AS ACTIVE_USER_COUNT FROM TJN_CURRENT_USERS A, MASUSER B WHERE B.USER_ID = A.USER_ID");
			pst = conn.prepareStatement("SELECT * FROM MASUSER WHERE USER_ID =?");
			pst.setString(1, userid);
			rs = pst.executeQuery();
			while (rs.next()) {
				user = new User();
				String roles = "";
				String tStamp = "";
				int count = 0;
				user.setId(rs.getString("USER_ID"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setLastName(rs.getString("LAST_NAME"));
				user.setPassword(rs.getString("PASS_WORD"));
				user.setPrimaryPhone(rs.getString("PHONE_1"));
				user.setSecondaryPhone(rs.getString("PHONE_2"));
				user.setEmail(rs.getString("EMAIL_ID"));
				user.setEnabledStatus(rs.getString("USER_ENABLED"));
				/* Modified by Preeti for TENJINCG-507 ends */
				/* added by manish for bug T25IT-41 on 08-Aug-2017 ends */
				if (user.getLastName() != null && !user.getLastName().equalsIgnoreCase("")) {
					user.setFullName(user.getFirstName() + " " + user.getLastName());
				} else {
					user.setFullName(user.getFirstName());
				}
				if (user != null) {
					pst1 = conn.prepareStatement("SELECT A.GROUP_ID FROM MASUSERGROUPS A, USERROLES B WHERE A.GROUP_ID = B.GROUP_ID AND B.USER_ID =?");
					pst1.setString(1, user.getId());
					rs1 = pst1.executeQuery();
					while (rs1.next()) {
						roles = roles + rs1.getString("GROUP_ID") + ",";
					}

					rs1.close();

					if (roles != null && !roles.equalsIgnoreCase("")) {
						if (roles.charAt(roles.length() - 1) == ',') {
							roles = roles.substring(0, roles.length() - 1);
						}

						user.setRoles(roles);
					}

					/* 02-Mar-2015 R2.1 Database agnostic Changes: Ends */
					pst2= conn.prepareStatement("Select * From USERSESSIONS Where Us_Id In (Select coalesce(MAX(US_ID),0) AS US_ID FROM USERSESSIONS WHERE US_USER_ID =?)");
					pst2.setString(1, user.getId());
					rs2 = pst2.executeQuery();

					while (rs2.next()) {
						tStamp = rs2.getString("US_LOGIN_TIMESTAMP");
					}

					rs2.close();

					user.setLastLogin(tStamp);
					
					rs3 = st.executeQuery();
					while (rs3.next()) {
						count = rs3.getInt("ACTIVE_USER_COUNT");
					}
					user.setMaxNumberUsers(count);
					rs3.close();
					/* Added by Prem for TJN262R2-46 ends */
				}

			}

			st.close();
			pst1.close();
			pst2.close();
			pst.close();
			rs.close();
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(rs2);
			DatabaseHelper.close(rs3);
			DatabaseHelper.close(st);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return user;
	}
	/*modified by shruthi for VAPT helper fixes ends*/

	public void clearUserSession(String userId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pst = null;
		if (conn == null) {
			logger.error("ERROR connecting to Database " + Constants.DB_TENJIN_CORE);
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			pst = conn.prepareStatement(
					"UPDATE USERSESSIONS SET US_SESSION_STATE = ?,US_LOGOUT_TIMESTAMP=? WHERE US_USER_ID = ?");
			pst.setString(1, "I");
			pst.setTimestamp(2, new Timestamp(new Date().getTime()));
			pst.setString(3, userId);
			pst.executeUpdate();
		} catch (SQLException e) {
			logger.error("ERROR clearning Tenjin user session for user {}", userId);
			logger.error(e.getMessage(), e);
			throw new DatabaseException("Could not clear user session because of an internal error");
		} finally {
			DatabaseHelper.close(pst);
		}

		/********************************************************************
		 * Added by Sriram for Requirement TJN_22_15 (Clear User Screen needs to be
		 * added) Begins
		 */
		try {
			pst = conn.prepareStatement("DELETE FROM TJN_CURRENT_USERS WHERE USER_ID = ?");
			pst.setString(1, userId);
			pst.execute();

			pst.close();
			logger.debug("User {} unlocked", userId);
		} catch (Exception e) {
			logger.error("ERROR clearning Tenjin user session for user {}", userId);
			logger.error(e.getMessage(), e);
			throw new DatabaseException("Could not clear user session because of an internal error");
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		logger.info("Tenjin session for user {} cleared successfully");

	}

	public ArrayList<User> hydrateCurrentUsers() throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<User> currentUsers = null;

		try {
			pst = conn.prepareStatement(
					"SELECT B.*, A.* FROM TJN_CURRENT_USERS A, MASUSER B WHERE B.USER_ID = A.USER_ID");
			rs = pst.executeQuery();
			currentUsers = new ArrayList<User>();
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getString("USER_ID"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setLastName(rs.getString("LAST_NAME"));
				user.setPassword(rs.getString("PASS_WORD"));
				user.setPrimaryPhone(rs.getString("PHONE_1"));
				user.setSecondaryPhone(rs.getString("PHONE_2"));
				user.setEmail(rs.getString("EMAIL_ID"));
				if (user.getLastName() != null && !user.getLastName().equalsIgnoreCase("")) {
					user.setFullName(user.getFirstName() + " " + user.getLastName());
				} else {
					user.setFullName(user.getFirstName());
				}

				user.setLastLogin(rs.getString("LOGIN_TIME"));
				user.setCurrentTerminal(rs.getString("TERMINAL"));
				currentUsers.add(user);
			}

			rs.close();
			pst.close();
		} catch (Exception e) {
			logger.error("ERROR hydrating current users", e);
			try {
				conn.close();
			} catch (Exception e1) {

			}
			throw new DatabaseException("Could not fetch currently logged-on users due to an internal error");
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return currentUsers;
	}

	public void insertFailedLoginRecords(String userId, String ipAddress) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		FailedLoginAttempts attempt = new FailedLoginAttempts();

		Timestamp timestamp = new Timestamp(new Date().getTime());
		attempt.setTimeStamp(timestamp);
		PreparedStatement pstmt = null;
		try {

			pstmt = conn
					.prepareStatement("INSERT INTO FAILED_LOGIN_ATTEMPTS  (USER_ID,TERMINAL,TIME_STAMP) values(?,?,?)");
			/* Changed by Parveen for changing insert query REQ #TJN_243_04 ends */
			pstmt.setString(1, userId);
			pstmt.setString(2, ipAddress);
			pstmt.setTimestamp(3, attempt.getTimeStamp());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("Error during inserting failed login attmpt records");
			logger.error("Error ", e);
		} finally {

			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);
		}

	}

	public void clearFailedLoginAttemptRecords(String userId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pstmt = null;
		try {
			/*modified by shruthi for VAPT Helper fix starts*/
			pstmt = conn.prepareStatement("DELETE FROM FAILED_LOGIN_ATTEMPTS WHERE USER_ID =?");
			pstmt.setString(1, userId);
			/*modified by shruthi for VAPT Helper fix ends*/
			pstmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("error during deleting failed login attempt records");
			logger.error("Error ", e);
		} finally {

			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);

		}

	}
	/*modified by shruthi for VAPT helper fixes starts*/
	public List<FailedLoginAttempts> hydrateFailedLoginRecords(String userId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pst = null;
		List<FailedLoginAttempts> attempts = new ArrayList<FailedLoginAttempts>();
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement("SELECT * FROM FAILED_LOGIN_ATTEMPTS WHERE USER_ID=? ORDER BY TIME_STAMP DESC");
			pst.setString(1, userId);
			rs = pst.executeQuery();

			while (rs.next()) {
				FailedLoginAttempts attempt = new FailedLoginAttempts();
				attempt.setIpAddress(rs.getString("TERMINAL"));
				attempt.setTimeStamp(rs.getTimestamp("TIME_STAMP"));
				attempts.add(attempt);
			}
		} catch (SQLException e) {
			logger.error("Error during hydrating login attempt records");
			logger.error("Error ", e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);

		}
		return attempts;
	}
	/*modified by shruthi for VAPT helper fixes ends*/

	public int noOfFailedAttempts(String userId) throws DatabaseException {
		int count = 0;
		PreparedStatement pstmt = null;
		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
			/*modified by shruthi for VAPT Helper fix starts*/
			pstmt = conn.prepareStatement(
					"SELECT COUNT(*) AS CNT FROM FAILED_LOGIN_ATTEMPTS WHERE USER_ID=?");
			pstmt.setString(1, userId);
			/*modified by shruthi for VAPT Helper fix end*/
			rs = pstmt.executeQuery();
			rs.next();
			count = rs.getInt(1);
		} catch (DatabaseException | SQLException e) {
			
			logger.error("Error ", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);

		}
		return count;
	}

	public void blockUser(String userId) throws DatabaseException {
		PreparedStatement pstmt = null;
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		try {
			/*modified by shruthi for VAPT Helper fix starts*/
			pstmt = conn.prepareStatement("update masuser set user_enabled='N' where user_id=?");
			pstmt.setString(1, userId);
			/*modified by shruthi for VAPT Helper fix ends*/
			pstmt.executeUpdate();
		} catch (SQLException e) {
			
			logger.error("Error ", e);
		} finally {
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);

		}

	}

	public boolean userBlockedStatus(String userId) throws DatabaseException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean flag = false;
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		try {
			/*modified by shruthi for VAPT Helper fix starts*/
			pstmt = conn.prepareStatement("select user_enabled from masuser where user_Id =?");
			pstmt.setString(1, userId);
			/*modified by shruthi for VAPT Helper fix ends*/
			rs = pstmt.executeQuery();
		} catch (SQLException e) {
			
			logger.error("Error ", e);
		}
		try {
			while (rs.next()) {
				if (rs.getString("user_enabled").equals("N")) {
					flag = true;
				}
			}
		} catch (SQLException e1) {
			
			logger.error("Error ", e1);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pstmt);
			DatabaseHelper.close(conn);

		}
		return flag;

	}
	/*modified by shruthi for VAPT helper fixes starts*/
	public User authenticateRest(String userId, String password)
			throws DatabaseException, DuplicateUserSessionException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);

		Timestamp timeStamp = new Timestamp(new Date().getTime());
		FailedLoginAttempts attempt = new FailedLoginAttempts();
		int count = 0;
		User user = null;
		user = this.hydrateUser(userId, conn);

		if (user == null) {
			logger.error("User is null. Authentication Failed");
			throw new DatabaseException("Authentication Failed");
		}

		attempt.setTimeStamp(timeStamp);
		 /*added by shruthi for VAPT encryption issue starts*/
		String dbPassword=user.getPassword();
		 /*added by shruthi for VAPT encryption issue ends*/

		try {
			boolean loggedInUser = new UserHelperNew().checkLoggedInUser(user.getId());
			if (loggedInUser) {
				throw new DuplicateUserSessionException("User is already logged in");
			}
		} catch (DatabaseException e) {

			throw new DuplicateUserSessionException("User is already logged in");

		} catch (DuplicateUserSessionException e) {
			throw new DuplicateUserSessionException("User is already logged in");

		}
		List<FailedLoginAttempts> failedAttempts = null;
		count = this.noOfFailedAttempts(userId);
		status = this.userBlockedStatus(userId);
		/* Modified by Padmavathi for license starts */
		if (status) {
			throw new DatabaseException(userId + " is blocked, Please contact administrator");
		}/*modified by shruthi for VAPT encryption issue starts*/
		else if (!TenjinSessionHandler.checkPassword(password ,dbPassword) && count >= 2) {
			blockUser(userId);
			insertFailedLoginRecords(userId, "REST Client");
			throw new DatabaseException(userId + " is blocked, Please contact administrator");
		} else if (!TenjinSessionHandler.checkPassword(password ,dbPassword) && count < 2) {
			insertFailedLoginRecords(userId, "REST Client");
			throw new DatabaseException("Authentication Failed");
		} else if (!TenjinSessionHandler.checkPassword(password ,dbPassword) && count > 2) {
			/*modified by shruthi for VAPT encryption issue ends*/
			blockUser(userId);
			insertFailedLoginRecords(userId, "REST Client");
			throw new DatabaseException(userId + " is blocked, Please contact administrator");
		}
		/* Modified by Padmavathi for license ends */
		else {
			failedAttempts = this.hydrateFailedLoginRecords(userId);
			user.setFailedLoginAttempts(failedAttempts);
			logger.debug("Authenticate user {} --> SUCCESS", user.getId());
			this.clearFailedLoginAttemptRecords(userId);

		}
		PreparedStatement pst1 = null;
		PreparedStatement st3 = null;
		ResultSet rs = null;
		ResultSet rs3= null;
		PreparedStatement pst = null;
		try {
			pst1 = conn.prepareStatement("Select * From USERSESSIONS Where Us_Id In (Select coalesce(MAX(US_ID),0) AS US_ID FROM USERSESSIONS WHERE US_USER_ID =?)");
			pst1.setString(1, user.getId());
			rs = pst1.executeQuery();
			
			String tStamp = "";

			while (rs.next()) {
				tStamp = rs.getString("US_LOGIN_TIMESTAMP");
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			/* Modified by Preeti for TENJINCG-928 ends */
			while (rs.next()) {
				tStamp = sdf.format(rs.getTimestamp("US_LOGIN_TIMESTAMP"));
			}

			user.setLastLogin(tStamp);
			rs.close();
		
			/*Modified by Pushpa for TJN27-58 starts*/
			/*sId = sId + 1;*/
			int sId = 0;
			sId = DatabaseHelper.getGlobalSequenceNumber(conn);
			/*Modified by Pushpa for TJN27-58 ends*/
			pst = conn.prepareStatement(
					"INSERT INTO USERSESSIONS (US_ID,US_USER_ID,US_LOGIN_TIMESTAMP,US_SESSION_STATE) VALUES(?,?,?,?)");
			pst.setInt(1, sId);
			pst.setString(2, user.getId());
			pst.setTimestamp(3, new Timestamp(new Date().getTime()));
			pst.setString(4, "A");
			pst.execute();
			pst.close();
			user.setTenjinSessionId(sId);
			/* Added by Padmavathi for license starts */
			
			 st3 = conn.prepareStatement("SELECT  COUNT(A.USER_ID) AS ACTIVE_USER_COUNT FROM TJN_CURRENT_USERS A, MASUSER B WHERE B.USER_ID = A.USER_ID");
			 rs3 = st3.executeQuery();
			
			/* 02-Mar-2015 R2.1 Database agnostic Changes: Ends */
			while (rs3.next()) {
				count = rs3.getInt("ACTIVE_USER_COUNT");
			}
			user.setMaxNumberUsers(count);
			rs3.close();
			/* Added by Padmavathi for license ends */
		} catch (Exception e) {
			logger.error("ERROR updating USERSESSIONS for user {}", user.getId());
			logger.error(e.getMessage(), e);
			try {
				conn.close();
			} catch (Exception e1) {

			}
			throw new DatabaseException("Authentication Error. Please contact your system administrator");
		} finally {
			DatabaseHelper.close(rs3);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		logger.debug("Authentication routine completed successfully for {}", userId);
		return user;

	}
	/*modified by shruthi for VAPT helper fixes ends*/
	/*modified by shruthi for VAPT helper fixes starts*/
	public Boolean CheckUserAssociateToProj(int prjId, String userId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean flag = false;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try {
			pst = conn.prepareStatement("select * from tjn_prj_users where prj_id=? and user_id=?");
			pst.setInt(1, prjId);
			pst.setString(2, userId);
			rs = pst.executeQuery();

			if (rs.next()) {
				flag = true;

			} else {
				flag = false;
			}
		} catch (Exception e) {

		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return flag;
	}
}
/*modified by shruthi for VAPT helper fixes ends*/
