/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MessageHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

  *//******************************************
  * CHANGE HISTORY
  * ==============
  *
  * DATE                	 CHANGED BY                DESCRIPTION
  	15-06-2021					Ashiki				Newly added for TENJINCG-1275
  */
package com.ycs.tenjin.db;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.MessageValidate;
import com.ycs.tenjin.util.Constants;

public class MessageValidateHelper {
	private static final Logger logger = LoggerFactory.getLogger(MessageValidateHelper.class);

	public void persistMessage(MessageValidate message) throws DatabaseException {

		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("INSERT INTO TJN_AUT_MESSAGES (ID,TYPE,MESSAGES,CONTENTEN,FILENAME,APP_ID,SUBTYPE) VALUES (?,?,?,?,?,?,?)");)
		{
			message.setMessageId(DatabaseHelper.getGlobalSequenceNumber(conn));
			pst.setInt(1,message.getMessageId());
			pst.setString(2, message.getMessageFormat());
			pst.setString(3, message.getMessageName());
			Clob clobFile = pst.getConnection().createClob();
			clobFile.setString(1, message.getFileContent());
			pst.setClob(4, clobFile);
			pst.setString(5, message.getFileName());
			pst.setInt(6,message.getApplicationId() );
			pst.setString(7,message.getSubType() );
			pst.execute();
		}catch(DatabaseException e){
			logger.error("ERROR persisting Message", e);
			throw e;
		}catch(Exception e){
			logger.error("Could not create message",e);
			throw new DatabaseException("Could not insert Message details due to an internal error",e);
		}
	
	}
	
	
	
	public List<MessageValidate> hydrateAllMessage() throws DatabaseException {
		ArrayList<MessageValidate> messageList = new ArrayList<MessageValidate>();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT * FROM TJN_AUT_MESSAGES");)
		{
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					MessageValidate message=new MessageValidate();
					message.setMessageId(rs.getInt("ID"));
					message.setMessageFormat(rs.getString("TYPE"));
					message.setMessageName(rs.getString("MESSAGES"));
					message.setFileContent(rs.getString("CONTENTEN"));
					message.setApplicationId(rs.getInt("APP_ID"));
					message.setSubType(rs.getString("SUBTYPE"));
					messageList.add(message);
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch selected application user",e);
			throw new DatabaseException("Could not fetch selected application user due to an internal error",e);
		}
		return messageList;
	
	}


	public void deleteMessage(String values) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst=null;
		ResultSet rs=null;
		if(values.endsWith(",")){
			values=values.substring(0, values.length()-1);
		}
		try {
			pst = conn.prepareStatement("DELETE FROM TJN_AUT_MESSAGES where ID IN (?)");
			pst.setInt(1, Integer.parseInt(values));
			rs = pst.executeQuery();
		} catch (Exception e) {
			throw new DatabaseException("Could not delete records ", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		
	}

	
	public MessageValidate hydrateXML(int msgId) throws DatabaseException{
		MessageValidate message = new MessageValidate();
		PreparedStatement pst=null;
		/*Modified by Pushpa for Sql Injection VAPT Starts*/
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);)
		{
			 pst=conn.prepareStatement("Select * from TJN_AUT_MESSAGES where ID=?");
			pst.setInt(1, msgId);
			/*Modified by Pushpa for Sql Injection VAPT ends*/
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					message.setMessageId(rs.getInt("ID"));
					message.setMessageName(rs.getString("MESSAGES"));
					message.setMessageFormat(rs.getString("TYPE"));
					message.setContextType(rs.getString("CONTENTEN"));
					message.setSubType(rs.getString("SUBTYPE"));
				}
			}
			pst.close();
		}
		catch(Exception e){
			logger.error("Could not fetch Message details",e);
			throw new DatabaseException("Could not fetch message details due to an internal error",e);
		}finally{
			DatabaseHelper.close(pst);
		}
		return message;
		
	}

	public boolean hydrateAllMessage(String messageName) throws DatabaseException {
		boolean messageExist=false;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT * FROM TJN_AUT_MESSAGES WHERE MESSAGES=?");)
		{
			pst.setString(1, messageName);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					messageExist=true;
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch selected application user",e);
			throw new DatabaseException("Could not fetch selected application user due to an internal error",e);
		}
		return messageExist;
	
	
	}



	public Map<String, String> hydrateFieldLabels(String messageType, String subType) throws DatabaseException {

		Map<String,String> fieldMap = new HashMap<String, String>();;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT * FROM TJN_PYMNTMSG_MASTER WHERE FORMAT=? AND MESSAGE_TYPE=?");)
		{
			pst.setString(1, messageType);
			 pst.setString(2,subType);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					fieldMap.put(rs.getString("TAG_NAME"), rs.getString("FIELD_LABEL"));
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch selected application user",e);
			throw new DatabaseException("Could not fetch selected application user due to an internal error",e);
		}
		return fieldMap;
    
	}



	public List<MessageValidate> getMessageFormat(int applicationId) throws DatabaseException {
		ArrayList<MessageValidate> messages = new ArrayList<MessageValidate>();
		PreparedStatement pst=null;
		/*Modified by Pushpa for Sql Injection VAPT Starts*/
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);)
		{
			 pst=conn.prepareStatement("SELECT * FROM TJN_AUT_MESSAGES WHERE APP_ID=?");
			pst.setInt(1, applicationId);
		/*Modified by Pushpa for Sql Injection VAPT Ends*/
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					MessageValidate message = this.mapFieldMsgs(rs);
					messages.add(message);
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch Message details",e);
			throw new DatabaseException("Could not fetch message details due to an internal error",e);
		}finally {
			DatabaseHelper.close(pst);
		}
		return messages;
	
	}
	
	private MessageValidate mapFieldMsgs(ResultSet rs) throws SQLException {
		try {
			MessageValidate message = new MessageValidate();
			message.setMessageId(rs.getInt("ID"));
			message.setMessageFormat(rs.getString("TYPE"));
			message.setMessageName(rs.getString("MESSAGES"));
			message.setFileContent(rs.getString("CONTENTEN"));
			message.setApplicationId(rs.getInt("APP_ID"));
			message.setSubType(rs.getString("SUBTYPE"));
			return message;
		} catch (SQLException e) {
			logger.error("ERROR creating Module from ResultSet", e);
			throw e;
		}
	}



	public String fetchMessageType(int appId) throws DatabaseException {
		String messageFormat="";
		int i=0;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT TYPE  FROM TJN_AUT_MESSAGES WHERE APP_ID = ?");)
		{
			pst.setInt(1, appId);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					if(i==0){
						messageFormat=rs.getString("TYPE");
						i++;
					}else{
						messageFormat=messageFormat+","+rs.getString("TYPE");
					}
					
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch Message Format user",e);
			throw new DatabaseException("Could not fetch Format due to an internal error",e);
		}
		return messageFormat;
    
	
	}



	public String hydrateMessageName(int appId, String msgFormat) throws DatabaseException {
		String messageName="";
		int i=0;
		PreparedStatement pst=null;
		/*Modified by Pushpa for Sql Injection VAPT Starts*/
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);)
		{
			 pst=conn.prepareStatement("SELECT MESSAGES  FROM TJN_AUT_MESSAGES WHERE APP_ID = ? AND TYPE=?");
			pst.setInt(1, appId);
			pst.setString(2, msgFormat);
		/*Modified by Pushpa for Sql Injection VAPT Ends*/	
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					if(i==0){
						messageName=rs.getString("MESSAGES");
						i++;
					}else{
						messageName=messageName+","+rs.getString("MESSAGES");
					}
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch Message Format user",e);
			throw new DatabaseException("Could not fetch Format due to an internal error",e);
		}finally {
			DatabaseHelper.close(pst);
		}
		return messageName;
	
	}


	public List<MessageValidate> hydrateMessageNames(int appId, String msgFormat) throws DatabaseException {

		ArrayList<MessageValidate> messages = new ArrayList<MessageValidate>();
		PreparedStatement pst=null;
		/*Modified by Pushpa for Sql Injection VAPT Starts*/
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);)
		{
			 pst=conn.prepareStatement("SELECT  A.*,B.APP_NAME AS APP_NAME FROM TJN_AUT_MESSAGES A, MASAPPLICATION B WHERE A.APP_ID = B.APP_ID AND  A.APP_ID=? and A.MESSAGES=?");
			pst.setInt(1, appId);
			pst.setString(2, msgFormat);
			/*Modified by Pushpa for Sql Injection VAPT Ends*/
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					MessageValidate message = this.mapFieldMsgs(rs);
					message.setApplicationId(appId);
					messages.add(message);
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch Message Format user",e);
			throw new DatabaseException("Could not fetch Format due to an internal error",e);
		}finally {
			DatabaseHelper.close(pst);
		}
		return messages;
	
	}
}
