/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectHelperNew.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 15-10-2018		   	Pushpalatha			  	Newly Added 
* 17-10-2018            Leelaprasad             TENJINCG-827
* 19-10-2018           	Leelaprasad             TENJINCG-829
* 19-10-2018           	Sriram            		TENJINCG-873
* 24-10-2018			Preeti					TENJINCG-850
* 26-10-2017			Sriram					TENJINCG-875
* 26-10-2018            Padmavathi              TENJINCG-824
* 07-11-2018			Sriram					TENJINCG-896
* 07-11-2018            Padmavathi              to display full name in project users tab
* 28-11-2018            Padmavathi              for  TJNUN262-22 
* 28-11-2018			Ramya					TJNUN262-5
* 27-12-2018			Preeti					TJN252-57
* 18-02-2019			Preeti					TENJINCG-969
* 28-02-2018			Pushpa					TENJINCG-980,981
* 11-03-2019			Preeti					TENJINCG-966
* 29-03-2019			Preeti					TENJINCG-1003
* 14-11-2019			Roshni					TENJINCG-1166
* 12-03-2020			Ashiki					TENJINCG-1123
* 05-02-2021            Shruthi A N             TENJINCG-1255 
*/
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.Domain;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class ProjectHelperNew {
	
	private static final Logger logger = LoggerFactory.getLogger(ProjectHelper.class);

	public Project hydrateProject(String domainName, String projectName) throws DatabaseException{
		Project project = null;

		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement pst = conn.prepareStatement("SELECT * FROM TJN_PROJECTS WHERE PRJ_NAME=? and PRJ_DOMAIN=?"); )
		{
			pst.setString(1, projectName);
			pst.setString(2, domainName);
			try(ResultSet rs = pst.executeQuery();){

			while(rs.next()){
				project = new Project();
				project.setName(rs.getString("PRJ_NAME"));
				project.setState(rs.getString("PRJ_STATE"));
				
			}
		}
			
		} catch (SQLException e) {
			logger.error("ERROR while hydrating Project [{}] under domain [{}]", projectName, domainName, e);
			throw new DatabaseException("Could not validate Project information. Please contact Tenjin Support");
		}
		return project;
	}
	

	public Project persistProject(Project project) throws DatabaseException {
		
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				/*added by shruthi for TENJINCG-1255 starts*/
			/*Modified by Pushpa for TENJINCG-980,981 starts*/
			PreparedStatement pst = conn.prepareStatement("INSERT INTO TJN_PROJECTS (PRJ_ID,PRJ_NAME,PRJ_TYPE,PRJ_CREATE_DATE,PRJ_DOMAIN,PRJ_OWNER,PRJ_DB_SERVER,PRJ_DESC,PRJ_SCREENSHOT_OPTIONS,PRJ_STATE,PRJ_DEF_MANAGER,PRJ_DM_PROJECT,PRJ_START_DATE,PRJ_END_DATE,PRJ_OWNER_EMAIL,TEST_DATA_REPO_TYPE,REPO_ROOT) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");){
			/*Modified by Pushpa for TENJINCG-980,981 ends*/
			/*added by shruthi for TENJINCG-1255 ends*/
			int prjId = DatabaseHelper.getGlobalSequenceNumber(conn);
			project.setId(prjId);
			pst.setInt(1, prjId);
			pst.setString(2, project.getName());
			/*Modified by Padmavathi for  TJNUN262-22 starts*/
			if(project.getType()== null || project.getType().equalsIgnoreCase("null")){
				pst.setString(3, "Public");
			}else{
				pst.setString(3, project.getType());
			}
			/*Modified by Padmavathi for  TJNUN262-22 ends*/
			pst.setTimestamp(4, project.getCreatedDate());
			pst.setString(5,project.getDomain());
			pst.setString(6, project.getOwner());
			pst.setString(7, "localhost");
			pst.setString(8, project.getDescription());
			pst.setString(9, "1");
			pst.setString(10, "A");
			pst.setString(11,"");
			pst.setString(12,"");
			/*Added by Pushpa for TENJINCG-980,981 starts*/
			pst.setTimestamp(13, project.getStartDate());
			pst.setTimestamp(14, project.getEndDate());
			pst.setString(15, project.getProjectOwnerEmail());
			/*added by shruthi for TENJINCG-1255 starts*/
			pst.setString(16, project.getRepoType());
			pst.setString(17, project.getRepoRoot());
			/*added by shruthi for TENJINCG-1255 ends*/
			pst.execute();
			/*Added by Pushpa for TENJINCG-980,981 ends*/
			try(PreparedStatement pst2 = conn.prepareStatement("INSERT INTO TJN_PRJ_TM_LINKAGE (PRJ_ID)VALUES (?)");){
				pst2.setInt(1, project.getId());
				pst2.execute();
			}
			try(PreparedStatement pst1 = conn.prepareStatement("INSERT INTO TJN_PRJ_USERS (PRJ_ID,USER_ID,USER_ROLE) VALUES (?,?,?)");)
			{
				pst1.setInt(1, project.getId());
				pst1.setString(2, project.getOwner());
				pst1.setString(3, "Project Administrator");
				pst1.execute();
			}
			try(PreparedStatement pst3 = conn.prepareStatement("INSERT INTO TJN_PRJ_MAIL_SETTINGS (PRJ_ID,PRJ_MAIL_NOTIFICATIONS,TJN_PRJ_MAIL_ESCALATION,TJN_PRJ_CONSOLIDATE_MAIL) VALUES (?,?,?,?)");)
			{
				pst3.setInt(1, project.getId());
				pst3.setString(2, "Y");
				pst3.setString(3, "Y");
				pst3.setString(4, "Y");
				pst3.execute();
			}
		}catch(Exception e){
			throw new DatabaseException("Could not create record",e);
		}

		return project;
	}


	public Project hydrateProject(int projectId) throws DatabaseException {
		Project project = null;
		ArrayList<User> users = new ArrayList<User>();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			/*Modified by Preeti for TENJINCG-969 starts*/
			PreparedStatement pst = conn.prepareStatement("SELECT * FROM TJN_PROJECTS P LEFT JOIN (SELECT * FROM TJN_AUDIT WHERE AUDIT_ID=(SELECT MAX(AUDIT_ID) FROM TJN_AUDIT WHERE ENTITY_TYPE=? AND ENTITY_RECORD_ID = ?)) A ON A.ENTITY_RECORD_ID = P.PRJ_ID WHERE P.PRJ_ID=?"); )
		{
			pst.setString(1, "project");
			pst.setInt(2, projectId);
			pst.setInt(3, projectId);
			/*Modified by Preeti for TENJINCG-969 ends*/
			try(ResultSet rs = pst.executeQuery();){

			while(rs.next()){
				project = new Project();
				/* Changed by Leelaprasad for TENJINCG-827 starts */
				project.setId(projectId);
				/* Changed by Leelaprasad for TENJINCG-827 ends */
				project.setName(rs.getString("PRJ_NAME"));
				project.setState(rs.getString("PRJ_STATE"));
				project.setType(rs.getString("PRJ_TYPE"));
				project.setDescription(rs.getString("PRJ_DESC"));
				project.setOwner(rs.getString("PRJ_OWNER"));
				project.setDomain(rs.getString("PRJ_DOMAIN"));
				project.setCreatedDate(rs.getTimestamp("PRJ_CREATE_DATE"));
				project.setScreenshotoption(rs.getString("PRJ_SCREENSHOT_OPTIONS"));
				/*Added by Pushpa for TENJINCG-980,981 starts*/
				project.setStartDate(rs.getTimestamp("PRJ_START_DATE"));
				project.setEndDate(rs.getTimestamp("PRJ_END_DATE"));
				project.setProjectOwnerEmail(rs.getString("PRJ_OWNER_EMAIL"));
				/*Added by Pushpa for TENJINCG-980,981 ends*/
				
				/*Added by Preeti for TENJINCG-969 starts*/
				AuditRecord record = new AuditRecord();
				record.setAuditId(rs.getInt("AUDIT_ID"));
				record.setEntityRecordId(rs.getInt("ENTITY_RECORD_ID"));
				record.setEntityType(rs.getString("ENTITY_TYPE"));
				record.setLastUpdatedBy(rs.getString("LAST_MODIFIED_BY"));
				record.setLastUpdatedOn(rs.getTimestamp("LAST_MODIFIED_ON"));
				/*added by shruthi for TENJINCG-1255 starts*/
				project.setRepoType(rs.getString("TEST_DATA_REPO_TYPE"));
				project.setRepoRoot(rs.getString("REPO_ROOT"));
				/*added by shruthi for TENJINCG-1255 ends*/
				project.setAuditRecord(record);
				/*Added by Preeti for TENJINCG-969 ends*/
			}
		}
		try(PreparedStatement pst1= conn.prepareStatement("SELECT * FROM TJN_PRJ_USERS WHERE PRJ_ID = ?");)
		{
			pst1.setInt(1, projectId);
			try(ResultSet rs = pst1.executeQuery();){
				while(rs.next()){
					String userId = rs.getString("USER_ID");
					String role = rs.getString("USER_ROLE");
					try{
						User user =new UserHandler().getUser(userId);
						user.setRoleInCurrentProject(role);
						users.add(user);
					}catch(Exception e){
						logger.error("Could not fetch information about user " + userId,e);
					}
				}

				project.setUsers(users);
			}
		}
		
		} catch (SQLException e) {
			logger.error("ERROR while hydrating Project [{}] under domain [{}]", project.getName(), project.getDomain(), e);
			throw new DatabaseException("Could not validate Project information. Please contact Tenjin Support");
		}
		return project;
	}
	/*Changed by Leelaprasad for TENJINCG-827 starts*/
	public void persistProjectSession(int projectId,String userId) {
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement("INSERT INTO TJN_PRJ_USER_SESSIONS (PRJ_USER_SESSSION_ID,PRJ_ID,USER_ID,ENTRY_TIME) VALUES (?,?,?,?)");){
			int prjSessionId = DatabaseHelper.getGlobalSequenceNumber(conn);
		
			pst.setInt(1, prjSessionId);
			pst.setInt(2, projectId);
			pst.setString(3, userId);
			pst.setTimestamp(4, new Timestamp(new Date().getTime()));
			pst.execute();
		
		}catch(Exception e){
			logger.error("ERROR while while persisting project user session"+e);
		}
	}
	public void updateProjectSession(int projectId,String userId) {
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement("UPDATE TJN_PRJ_USER_SESSIONS SET EXIT_TIME =? WHERE PRJ_USER_SESSSION_ID=(SELECT MAX(PRJ_USER_SESSSION_ID) FROM TJN_PRJ_USER_SESSIONS WHERE PRJ_ID=? AND USER_ID=?)");
				){
			pst.setTimestamp(1, new Timestamp(new Date().getTime()));
			pst.setInt(2, projectId);
			pst.setString(3, userId);
			pst.executeUpdate();
	}catch(Exception e){
		logger.error("ERROR while while updating project user session"+e);
		
		
	}
	}
/*Changed by Leelaprasad for TENJINCG-827 ends*/
	/*Changed by leelaprasad for TENJINCG-829 starts*/
	public void updateUserProjectPreference(int projectId,String userId)
{
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement("UPDATE TJN_USER_PREFERENCE SET PRJ_PREFERENCE=? WHERE USER_ID=?");){
			
			pst.setInt(1, projectId);
			pst.setString(2, userId);
			pst.executeUpdate();
		} catch (Exception e) {
			
			logger.error("Error ", e);
		}
	
}
	public int hydrateUserProjectPreference(String userId){
	int projectId=0;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				PreparedStatement pst = conn.prepareStatement("SELECT PRJ_PREFERENCE FROM TJN_USER_PREFERENCE WHERE USER_ID=?");){
			   pst.setString(1, userId);
			 try(ResultSet rs=pst.executeQuery();){
				 while(rs.next()){
					 projectId=rs.getInt("PRJ_PREFERENCE");
				 }
			 }
		} catch (Exception e) {
			
			logger.error("Error ", e);
		}
		return projectId;
		
	}
	public ArrayList<Project> hydrateAllProjectsForUser(String userId) throws DatabaseException{
		
		ArrayList<Project> projects = new ArrayList<Project>();
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				/*Modified by Preeti for TENJINCG-966 starts*/
				/*Modified By Prem starts */
				PreparedStatement	pst = conn.prepareStatement("SELECT A.* FROM  TJN_PRJ_USERS B ,TJN_PROJECTS A LEFT JOIN (SELECT D.PRJ_ID FROM TJN_PRJ_USER_SESSIONS D WHERE D.USER_ID = ? GROUP BY D.PRJ_ID ) C ON C.PRJ_ID=A.PRJ_ID  WHERE B.PRJ_ID = A.PRJ_ID AND B.USER_ID = ? AND A.PRJ_STATE=?");
				/*Modified By Prem Ends */
				/*Modified by Preeti for TENJINCG-966 ends*/
				){
			pst.setString(1, userId);
			pst.setString(2, userId);
			pst.setString(3, "A");
			try(ResultSet rs = pst.executeQuery();){
				while(rs.next()){
					Project p = new Project();
					p.setId(rs.getInt("PRJ_ID"));
					p.setDomain(rs.getString("PRJ_DOMAIN"));
					p.setName(rs.getString("PRJ_NAME"));
					p.setOwner(rs.getString("PRJ_OWNER"));
					projects.add(p);
				}
				
			}
		}	
		catch(Exception e){
			logger.error("Could not fetch projects for user " + userId,e);
			logger.error("Exception in fetchAllProjectsForUser(String userId)");
			throw new DatabaseException("Could not fetch projects for user " + userId, e);
		}return projects;
	}
	/*Changed by leelaprasad for TENJINCG-829 ends*/
	

	//SRIRAM for TENJINCG-873
		public List<User> hydrateProjectUsers(int projectId) throws DatabaseException {
			String query = "SELECT A.*, B.FIRST_NAME, B.LAST_NAME, B.EMAIL_ID FROM TJN_PRJ_USERS A, MASUSER B WHERE B.USER_ID=A.USER_ID AND A.PRJ_ID=? ORDER BY B.FIRST_NAME";
			List<User> projectUsers = new LinkedList<>();
			try (
					Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					PreparedStatement pst = conn.prepareStatement(query);
				) {
				pst.setInt(1, projectId);
				
				try(ResultSet rs = pst.executeQuery()) {
					while(rs.next()) {
						projectUsers.add(this.buildProjectUserObject(rs));
					}
				}
			} catch (SQLException e) {
				logger.error("ERror occurred while fetching project users for project {}", projectId, e);
				throw new DatabaseException("Could not fetch project users", e);
			}
			
			return projectUsers;
			
		}
		/*Added by Preeti for TENJINCG-850 starts*/
		public ArrayList<Project> hydrateProjectsForCurrentUser(String userId, int currentProjectid) throws DatabaseException {
			ArrayList<Project> projects = new ArrayList<>();
			try(
					Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					/*Modified by Ashiki for TENJINCG-1123 starts*/
					PreparedStatement pst = conn.prepareStatement("SELECT A.*,B.USER_ROLE FROM TJN_PROJECTS A, TJN_PRJ_USERS B WHERE B.PRJ_ID=A.PRJ_ID AND B.USER_ID=? AND A.PRJ_ID!=?");
					/*Modified by Ashiki for TENJINCG-1123 starts*/
					){
					pst.setString(1, userId);
					pst.setInt(2, currentProjectid);
					try(ResultSet rs = pst.executeQuery()){
						while(rs.next()){
							Project project = new Project();
							project.setName(rs.getString("PRJ_NAME"));
							project.setId(rs.getInt("PRJ_ID"));
							project.setCreatedDate(rs.getTimestamp("PRJ_CREATE_DATE"));
							project.setDomain(rs.getString("PRJ_DOMAIN"));
							project.setOwner(rs.getString("PRJ_OWNER"));
							project.setType(rs.getString("PRJ_TYPE"));
							project.setDescription(rs.getString("PRJ_DESC"));
							project.setScreenshotoption(rs.getString("PRJ_SCREENSHOT_OPTIONS"));
							project.setState(rs.getString("PRJ_STATE"));
							/*Added by Ashiki for TENJINCG-1123 starts*/
							project.setUserRole(rs.getString("USER_ROLE"));
							/*Added by Ashiki for TENJINCG-1123 ends*/
							project.setRepoType(rs.getString("TEST_DATA_REPO_TYPE"));
							projects.add(project);
						}	
					}
			}catch (SQLException e) {
				logger.error("ERROR while hydrating for user [{}]", userId, e);
				throw new DatabaseException("Could not validate Project information. Please contact Tenjin Support");
			}
			return projects;
		}
		
		public ArrayList<Integer> hydrateProjectAutIds(Connection conn, int projectId) throws DatabaseException{
			ArrayList<Integer> projectAppIds = new ArrayList<>();
			int appId = 0;
			try(
					PreparedStatement pst = conn.prepareStatement("Select APP_ID from TJN_PRJ_AUTS where PRJ_ID = ?");
					
				){
					pst.setInt(1, projectId);
					try(ResultSet rs = pst.executeQuery();){
					while(rs.next()){
						appId = rs.getInt("APP_ID");
						projectAppIds.add(appId);
					}
				}
			}catch(Exception e){
				throw new DatabaseException("Could not fetch AUTs for Project",e);
			}
			return projectAppIds;
		}
		/*Added by Preeti for TENJINCG-850 ends*/
		
   public void modifyProjectState(int prjId, String state) {
	   String query="UPDATE TJN_PROJECTS SET PRJ_STATE=? WHERE PRJ_ID=?";
			try(Connection conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					PreparedStatement pst = conn.prepareStatement(query);
					){
				pst.setString(1, state);
				pst.setInt(2, prjId);
				pst.executeUpdate();
			} catch (Exception e) {
				logger.error("Error ", e);
			}
			
		}
		private User buildProjectUserObject(ResultSet rs) throws SQLException {
			User user = new User();	
			/*modified by Roshni for TENJINCG-1166 starts*/
			user.setId(Utilities.escapeXml(rs.getString("USER_ID")));
			user.setRoleInCurrentProject(Utilities.escapeXml(rs.getString("USER_ROLE")));
			user.setFirstName(Utilities.escapeXml(rs.getString("FIRST_NAME")));
			user.setLastName(Utilities.escapeXml(rs.getString("LAST_NAME")));
			user.setFullName(Utilities.escapeXml(Utilities.trim(user.getFirstName()) + " " + Utilities.trim(user.getLastName())));
			user.setEmail(Utilities.escapeXml(rs.getString("EMAIL_ID")));
			/*modified by Roshni for TENJINCG-1166 ends*/
			
			
			return user;
		}//SRIRAM for TENJINCG-873 ends


		public void updateProject(Project project) throws DatabaseException {
			/*Modified by Pushpa for TENJINCG-980,981 starts*/
			String query="UPDATE TJN_PROJECTS SET PRJ_NAME=?,PRJ_TYPE=?,PRJ_DESC=?,PRJ_SCREENSHOT_OPTIONS=?,PRJ_OWNER_EMAIL=?,PRJ_END_DATE=? WHERE PRJ_ID=?";
			/*Modified by Pushpa for TENJINCG-980,981 ends*/
			try(Connection conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					PreparedStatement pst = conn.prepareStatement(query);
					){
				pst.setString(1, project.getName());
				pst.setString(2, project.getType());
				pst.setString(3, project.getDescription());
				pst.setString(4, project.getScreenshotoption());
				/*Added by Pushpa for TENJINCG-980,981 starts*/
				pst.setString(5, project.getProjectOwnerEmail());
				pst.setTimestamp(6, project.getEndDate());
				/*Added by Pushpa for TENJINCG-980,981 ends*/
				pst.setInt(7, project.getId());
				pst.executeUpdate();
			} catch (Exception e) {
				logger.error("Unable to update the project");
				throw new DatabaseException("Could not update Project",e);
			}
			
		}
		
		
		//TENJINCG-896 (Sriram)
		public List<Domain> hydrateAllProjectsGroupedByDomain() throws DatabaseException {
			String query = "SELECT A.DOMAIN_NAME, B.* FROM TJN_DOMAINS A LEFT JOIN TJN_PROJECTS B ON B.PRJ_DOMAIN=A.DOMAIN_NAME ORDER BY B.PRJ_ID";
			List<Domain> domains = new LinkedList<>();
			Map<String, Domain> domainMap = new LinkedHashMap<>();
			try(
					Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					PreparedStatement pst = conn.prepareStatement(query);
					ResultSet rs = pst.executeQuery();
					) {
				while(rs.next()) {
					int projectId = rs.getInt("PRJ_ID");
					String domainName = rs.getString("DOMAIN_NAME");
					Project p = null;
					if(projectId <= 0) {
						//This is a record for a Domain without any projects
						p = new Project();
						p.setDomain(domainName);
					}else {
						p = this.buildProjectObject(rs);
					}
					
					if(!domainMap.keySet().contains(domainName)) {
						Domain d = new Domain();
						d.setName(domainName);
						domainMap.put(domainName, d);
					}
					
					Domain d = domainMap.get(p.getDomain());
					if(d.getProjects() == null) {
						d.setProjects(new ArrayList<Project>());
					}
					
					d.getProjects().add(p);
					
				}
			} catch (SQLException e) {
				throw new DatabaseException("Could not fetch projects due to an internal error", e);
			}
			
			for(Entry<String, Domain> entry : domainMap.entrySet()) {
				domains.add(entry.getValue());
			}
			
			return domains;
		}
		
		public List<Domain> hydrateAllProjectsGroupedByDomain(String owner) throws DatabaseException {
			String query = "SELECT A.DOMAIN_NAME, B.* FROM TJN_DOMAINS A LEFT JOIN TJN_PROJECTS B ON B.PRJ_DOMAIN=A.DOMAIN_NAME WHERE B.PRJ_OWNER=? ORDER BY B.PRJ_ID";
			List<Domain> domains = new LinkedList<>();
			Map<String, Domain> domainMap = new LinkedHashMap<>();
			try(
					Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					PreparedStatement pst = conn.prepareStatement(query);
					
					) {
				
				pst.setString(1, owner);
				try(ResultSet rs = pst.executeQuery()) {
					while(rs.next()) {
						int projectId = rs.getInt("PRJ_ID");
						String domainName = rs.getString("DOMAIN_NAME");
						Project p = null;
						if(projectId <= 0) {
							//This is a record for a Domain without any projects
							p = new Project();
							p.setDomain(domainName);
						}else {
							p = this.buildProjectObject(rs);
						}

						if(!domainMap.keySet().contains(domainName)) {
							Domain d = new Domain();
							d.setName(domainName);
							domainMap.put(domainName, d);
						}

						Domain d = domainMap.get(domainName);
						if(d.getProjects() == null) {
							d.setProjects(new ArrayList<Project>());
						}

						d.getProjects().add(p);

					}
				}
			} catch (SQLException e) {
				throw new DatabaseException("Could not fetch projects due to an internal error", e);
			}
			
			for(Entry<String, Domain> entry : domainMap.entrySet()) {
				domains.add(entry.getValue());
			}
			
			return domains;
		}
		//TENJINCG-896 (Sriram) ends
		
		
		
		private Project buildProjectObject(ResultSet rs) throws SQLException {
			Project project = new Project();
			project.setId(rs.getInt("PRJ_ID"));
			project.setName(rs.getString("PRJ_NAME"));
			project.setState(rs.getString("PRJ_STATE"));
			project.setType(rs.getString("PRJ_TYPE"));
			project.setDescription(rs.getString("PRJ_DESC"));
			project.setOwner(rs.getString("PRJ_OWNER"));
			project.setDomain(rs.getString("PRJ_DOMAIN"));
			project.setCreatedDate(rs.getTimestamp("PRJ_CREATE_DATE"));
			return project;
		}
		
		//TENJINCG-875 (Sriram) ends
		
		/*Added by Padmavathi for TENJINCG-824 starts*/
		public boolean checkProjectAccessForUser(String userId, int projectId) throws DatabaseException {
			boolean flag=true;
			try(
					Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					PreparedStatement pst = conn.prepareStatement("SELECT A.PRJ_STATE, B.USER_ID FROM TJN_PROJECTS A, TJN_PRJ_USERS B WHERE B.PRJ_ID=A.PRJ_ID AND B.USER_ID=? AND A.PRJ_ID=?");
				){
					pst.setString(1, userId);
					pst.setInt(2, projectId);
					try(ResultSet rs = pst.executeQuery()){
						if(rs.next()){
							if(rs.getString("PRJ_STATE").equalsIgnoreCase("I")){
								flag=false;
							}
						}else{
							flag=false;
						}	
					}
			}catch (SQLException e) {
				logger.error("ERROR while checking project access for user [{}]", userId, e);
				throw new DatabaseException("Could not check Project access for user. Please contact Tenjin Support");
			}
			return flag;
		}
		/*Added by Padmavathi for TENJINCG-824 ends*/
		
		/*Added by Ramya for TJNUN262-5 Starts*/
		public User hydrateCurrentUserForProject(String userId, int currentProjectid) throws DatabaseException {
			User user=null;
			try(
					Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					PreparedStatement pst = conn.prepareStatement("SELECT B.USER_ROLE FROM TJN_PROJECTS A, TJN_PRJ_USERS B WHERE B.PRJ_ID=A.PRJ_ID AND B.USER_ID=? AND A.PRJ_ID=?");
				){
					pst.setString(1, userId);
					pst.setInt(2, currentProjectid);
					try(ResultSet rs = pst.executeQuery()){
						while(rs.next()){
							 user=new User();
							 user.setRoleInCurrentProject(rs.getString("USER_ROLE"));
						}	
					}
			}catch (SQLException e) {
				logger.error("ERROR while hydrating for user [{}]", userId, e);
				throw new DatabaseException("Could not validate Project information. Please contact Tenjin Support");
			}
			return user;
		}
		/*Added by Ramya for TJNUN262-5 Ends*/
		
		/*Added by Preeti for TENJINCG-1003 starts*/
		public String hydrateCurrentUserRoleForProject(String userId, int currentProjectid) throws DatabaseException {
			String userRole=null;
			try(
					Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					PreparedStatement pst = conn.prepareStatement("SELECT B.USER_ROLE FROM TJN_PRJ_USERS B WHERE B.USER_ID=? AND B.PRJ_ID=?");
				){
					pst.setString(1, userId);
					pst.setInt(2, currentProjectid);
					try(ResultSet rs = pst.executeQuery()){
						while(rs.next()){
							userRole = rs.getString("USER_ROLE");
						}	
					}
			}catch (SQLException e) {
				logger.error("ERROR while hydrating user role for current user for current project [{}]", userId, e);
				throw new DatabaseException("Could not validate Project information. Please contact Tenjin Support");
			}
			return userRole;
		}
		/*Added by Preeti for TENJINCG-1003 ends*/
}
