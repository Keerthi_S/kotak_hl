/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectMailHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 * DATE                 CHANGED BY              DESCRIPTION
 * 02-05-2018			Preeti					Newly added for TENJINCG-656
 * 10-05-2018			Pushpalatha				TENJINCG-629
 * 03-05-2019           Padmavathi              TENJINCG-1048&1049&1050
   12-03-2020           Lokanath                TENJINCG-1185 
   13-03-2020			Jagadish				TENJINCG-1184
*/

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.project.ProjectMail;
import com.ycs.tenjin.project.ProjectMailPreference;
import com.ycs.tenjin.util.Constants;

public class ProjectMailHelper {
	private static final Logger logger = LoggerFactory.getLogger(ProjectMailHelper.class);
	
	public ProjectMail hydrateProjectMail(int projectId) throws DatabaseException {
		   Connection conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		   PreparedStatement ps=null;
		   ProjectMail projectMail=new ProjectMail();
			ResultSet rs=null;
			if(conn==null){
				throw new DatabaseException("Invalid Database Connection");
				
			}
			try{
				 ps=conn.prepareStatement("SELECT * FROM TJN_PRJ_MAIL_SETTINGS WHERE PRJ_ID= ?");
				 ps.setInt(1, projectId);
				 rs= ps.executeQuery();
				 while(rs.next()){
					 	projectMail.setPrjId(rs.getInt("PRJ_ID"));
						projectMail.setPrjMailNotification(rs.getString("PRJ_MAIL_NOTIFICATIONS"));
						projectMail.setPrjMailSubject(rs.getString("PRJ_MAIL_SUBJECT"));
						/*Added by Pushpalatha TENJINCG-629 starts*/
						projectMail.setPrjEscalationMails(rs.getString("TJN_PRJ_ESCALATION_MAILS"));
						projectMail.setPrjMailEscalation(rs.getString("TJN_PRJ_MAIL_ESCALATION"));
						/*projectMail.setMailEscalationRuleId(rs.getInt("TJN_MAIL_ESCALATION_RULE_ID"));*/
						projectMail.setMailEscalationRuleId(rs.getString("TJN_MAIL_ESCALATION_RULE_ID"));
						projectMail.setPrjEscalationRuleDesc(rs.getString("TJN_PRJ_MAIL_ESCRULE_DESC"));
						/*Added by Lokanath for TENJINCG-1185 starts*/
						projectMail.setPrjAddAdditionalMails(rs.getString("TJN_ADDITIONAL_MAILS"));
						/*Added by Lokanath for TENJINCG-1185 ends*/
						/*Added by Jagadish for TENJINCG-1184 starts*/
						projectMail.setPrjMailtype(rs.getString("PRJ_MAIL_TYPE"));
						/*Added by Jagadish for TENJINCG-1184 ends*/
						/*Added by Pushpalatha for TENJINCG-629 ends*/
				 }
			}
			catch(SQLException  e){
				logger.error("error while hydrating project mail");
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(ps);
				DatabaseHelper.close(conn);
			}
			return projectMail;
		   
	   }
	public ProjectMail updateProjectForMail(ProjectMail projectMail) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		try{
			/*Changed and Added by Pushpalatha for TENJINCG-629 starts*/
			
			/*Modified by Jagadish for TENJINCG-1184 starts*/
			pst = conn.prepareStatement("UPDATE TJN_PRJ_MAIL_SETTINGS SET PRJ_MAIL_SUBJECT=?,PRJ_MAIL_NOTIFICATIONS=?,TJN_PRJ_MAIL_ESCALATION=?,TJN_MAIL_ESCALATION_RULE_ID=?,TJN_PRJ_ESCALATION_MAILS=?,TJN_PRJ_MAIL_ESCRULE_DESC=?,TJN_ADDITIONAL_MAILS=?,PRJ_MAIL_TYPE=? WHERE PRJ_ID = ?");
			/*Modified by Jagadish for TENJINCG-1184 ends*/
			pst.setString(1, projectMail.getPrjMailSubject());
			pst.setString(2, projectMail.getPrjMailNotification());
			pst.setString(3, projectMail.getPrjMailEscalation());
			/*pst.setInt(4, projectMail.getMailEscalationRuleId());*/
			pst.setString(4, projectMail.getMailEscalationRuleId());
			pst.setString(5, projectMail.getPrjEscalationMails());
			pst.setString(6, projectMail.getPrjEscalationRuleDesc());
			/*Added by lokanath for TENJINCG-1185 starts*/
			pst.setString(7, projectMail.getPrjAddAdditionalMails());
			/*Added by lokanath for TENJINCG-1185 ends*/
			/*Added by Jagadish for TENJINCG-1184 Strats*/
			pst.setString(8, projectMail.getPrjMailtype());
			/*Added by Jagadish for TENJINCG-1184 ends*/
			pst.setInt(9, projectMail.getPrjId());
			
			/*Changed and Added by Pushpalatha for TENJINCG-629 starts*/
			pst.execute();
		}catch(Exception e){
			throw new DatabaseException("Could not update project",e);
		}finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);

		}

		return projectMail;
	}
	/*Added by Pushpalatha for TENJINCG-629 starts*/
	public LinkedHashMap<String, String> getEscalationRules() throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		Statement st=null;
		ResultSet rs=null;
		LinkedHashMap<String,String> map=new LinkedHashMap<String,String>();
		logger.info("hydrating escalation rules");
		try{
			 st = conn.createStatement();
			 rs = st.executeQuery("SELECT * FROM TJN_MAIL_ESCALATION_RULES");
			while(rs.next()){
				map.put(rs.getString("TJN_MAIL_ESCALATION_RULE_ID"), rs.getString("TJN_MAIL_ESCALATION_RULE_NAME")+","+rs.getString("TJN_MAIL_ESCALATION_RULE_DESC"));
			
			}
			logger.info("hydrating escalation rules completed");
			}catch(Exception e){
				logger.info("error occured hydrating Escalation Rules ");
				throw new DatabaseException(e.getMessage());
			}finally{
				DatabaseHelper.close(st);
				DatabaseHelper.close(rs);
				DatabaseHelper.close(conn);	
			}
		return map;
	}
	/*Added by Pushpalatha for TENJINCG-629 ends*/
	
	
	public void persistProjectMailPreference(Connection conn ,int prjId,String action,String prjUsers) throws DatabaseException{
        logger.info("inserting project mail preferences");
		try(PreparedStatement pst = conn.prepareStatement("INSERT INTO PRJ_MAIL_PREFERENCE (PRJ_ID,ACTION,PRJ_USERS) VALUES (?,?,?)");){
			pst.setInt(1, prjId);
			pst.setString(2,action);
			pst.setString(3,prjUsers);
			pst.execute();
		
		}catch(Exception e){
			logger.error(e.getMessage());
		       logger.error("Error ", e);
			throw new DatabaseException("Could not insert record",e);
		}
	}
	public void persistProjectMailPreference(Connection conn ,int prjId,Map<String,String> preferenceMap) throws DatabaseException{
        logger.info("inserting project mail preferences");
        
		try(PreparedStatement pst = conn.prepareStatement("INSERT INTO PRJ_MAIL_PREFERENCE (PRJ_ID,ACTION,PRJ_USERS) VALUES (?,?,?)");){
			for(Entry<String, String> entry : preferenceMap.entrySet()){
				pst.setInt(1, prjId);
				pst.setString(2,  entry.getKey());
				pst.setString(3,  entry.getValue());
				pst.addBatch();
			}
			pst.executeBatch();
		
		}catch(Exception e){
			logger.error(e.getMessage());
		       logger.error("Error ", e);
			throw new DatabaseException("Could not insert record",e);
		}
	}
	public void updateProjectMailPreference(Connection conn ,int prjId,Map<String,String> preferenceMap) throws DatabaseException{
		 logger.info("updating project mail preferences");
		try(PreparedStatement pst = conn.prepareStatement("UPDATE PRJ_MAIL_PREFERENCE SET PRJ_USERS=? WHERE PRJ_ID=? AND ACTION=?")){
			for(Entry<String, String> entry : preferenceMap.entrySet()){
				pst.setString(1,  entry.getValue());
				pst.setInt(2, prjId);
				pst.setString(3,  entry.getKey());
				pst.addBatch();
			}
			pst.executeBatch();
		
		}catch(Exception e){
			logger.error(e.getMessage());
		       logger.error("Error ", e);
			throw new DatabaseException("Could not update record",e);
		}
	}
	public void updateProjectMailPreference(Connection conn ,int prjId,String action,String prjUsers) throws DatabaseException{
		 logger.info("updating project mail preferences");
		try(PreparedStatement pst = conn.prepareStatement("UPDATE PRJ_MAIL_PREFERENCE SET PRJ_USERS=? WHERE PRJ_ID=? AND ACTION=?")){
			pst.setString(1,prjUsers);
			pst.setInt(2, prjId);
			pst.setString(3,action);
			pst.execute();
		
		}catch(Exception e){
			logger.error(e.getMessage());
		       logger.error("Error ", e);
			throw new DatabaseException("Could not update record",e);
		}
	}
	
	public String hydrateProjectMailPreferenceUsers(int prjId,String action) throws DatabaseException{
		 logger.info("fetching project mail preferences for prjid="+prjId);
		String ProjectUsers="";
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			PreparedStatement pst = conn.prepareStatement("SELECT * FROM PRJ_MAIL_PREFERENCE WHERE PRJ_ID=? AND ACTION=?");){
			pst.setInt(1, prjId);
			pst.setString(2, action);
			try(ResultSet rs=pst.executeQuery()){
				while(rs.next()){
					ProjectUsers=rs.getString("PRJ_USERS");
				}
			}
		}catch(Exception e){
	       logger.error(e.getMessage());
	       logger.error("Error ", e);
			throw new DatabaseException("Could not get record",e);
		}
		
		return ProjectUsers;
		
	}
	public String deleteProjectMailPreference(Connection conn,int prjId,String actions) throws DatabaseException{
		String ProjectUsers="";
		/*modified by shruthi for sql injection starts*/
		 String[] actionList=actions.split(",");
		 for(String str : actionList) {
		try(
				PreparedStatement pst = conn.prepareStatement("DELETE FROM PRJ_MAIL_PREFERENCE WHERE PRJ_ID=? AND ACTION IN=?");){
			pst.setInt(1, prjId);
			pst.setString(2, str);
			pst.execute();
		}catch(Exception e){
	       logger.error(e.getMessage());
	       logger.error("Error ", e);
			throw new DatabaseException("Could not delete record",e);
		}} /*modified by shruthi for sql injection ends*/
		return ProjectUsers;
	}

	public boolean doesProjectMailPreferenceExits(Connection conn ,int prjId,String action) throws DatabaseException{
			boolean flag=false;
			try(PreparedStatement pst = conn.prepareStatement("SELECT * FROM PRJ_MAIL_PREFERENCE WHERE PRJ_ID=? AND ACTION=?");){
			pst.setInt(1, prjId);
			pst.setString(2, action);
			try(ResultSet rs=pst.executeQuery()){
				while(rs.next()){
					flag=true;
					break;
				}
			}
		}catch(Exception e){
	       logger.error(e.getMessage());
	       logger.error("Error ", e);
			throw new DatabaseException("Could not get record",e);
		}
		
		return flag;
	}
	public List<ProjectMailPreference> hydrateProjectMailPreference(Connection conn ,int prjId) throws DatabaseException{
		List<ProjectMailPreference> preferenceList=new ArrayList<ProjectMailPreference>();
		
		try(PreparedStatement pst = conn.prepareStatement("SELECT * FROM PRJ_MAIL_PREFERENCE WHERE PRJ_ID=?");){
		pst.setInt(1, prjId);
		try(ResultSet rs=pst.executeQuery()){
			while(rs.next()){
				ProjectMailPreference preference=new ProjectMailPreference(); 
				preference.setProjectId(prjId);
				preference.setAction(rs.getString("ACTION"));
				preference.setUsers(rs.getString("PRJ_USERS"));
				preferenceList.add(preference);
			}
		}
	}catch(Exception e){
       logger.error(e.getMessage());
       logger.error("Error ", e);
		throw new DatabaseException("Could not get record",e);
	}
		return preferenceList;
	
	}
	/*Added by Padmavathi for TENJINCG-1048&1049&1050 ends*/
}
