/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UnstructuredDataHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 08-04-2020			Ashiki			Newly added for TENJINCG-1204
* */

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.ValidationDetails;
import com.ycs.tenjin.util.Constants;

public class UnstructuredDataHelper {

	
	private static Logger logger = LoggerFactory
			.getLogger(UnstructuredDataHelper.class);
	public void persistUnstructuredData(ValidationDetails validateDetails) throws DatabaseException {

		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("INSERT INTO UNSTRUCTURED_DATA_VALIDATION (RECORD_ID,TESTSTEP_ID,VALIDATION_DATA)  VALUES(?,?,?)");)
		{
			int recordId=DatabaseHelper.getGlobalSequenceNumber(conn);
			pst.setInt(1, recordId);
			pst.setInt(2,validateDetails.getTestStepId() );
			pst.setString(3, validateDetails.getValidationData());
			pst.execute();
			validateDetails.setRecordId(recordId);
		}catch(DatabaseException e){
			throw e;
		}catch(Exception e){
			throw new DatabaseException("Could not insert Unstructured Data due to an internal error",e);
		}
			
	}

	public ArrayList<ValidationDetails> hydrateUnstructuredData(int stepRecordId) throws DatabaseException {
		
		ArrayList<ValidationDetails> unstructuredDatas = new ArrayList<ValidationDetails>();
		
		ValidationDetails unstructuredData = new ValidationDetails();
		
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("select RECORD_ID,VALIDATION_DATA from UNSTRUCTURED_DATA_VALIDATION where TESTSTEP_ID=?");)
		{
			pst.setInt(1, stepRecordId);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					unstructuredData=buildValidationDetailsObject(rs);
					unstructuredDatas.add(unstructuredData);
				}
			}
		}
		catch(Exception e){
			throw new DatabaseException("Could not fetch unstructured Data user due to an internal error",e);
		}
		return unstructuredDatas;
	}

	private ValidationDetails buildValidationDetailsObject(ResultSet rs) throws SQLException {

		ValidationDetails unstructuredData = new ValidationDetails();
		unstructuredData.setRecordId(rs.getInt("RECORD_ID"));

		try {
			JSONObject json = new JSONObject(rs.getString("VALIDATION_DATA"));
			unstructuredData.setValidationName(json.getString("validationName"));
			unstructuredData.setFileType(json.getString("fileType"));
			unstructuredData.setSearchText(json.getString("searchText"));
			unstructuredData.setSheetName(json.getString("sheetName"));
			unstructuredData.setPageNumber(json.getInt("pageNumber"));
			unstructuredData.setTestStepId(json.getInt("testStepId"));
			unstructuredData.setPageNumber(json.getInt("pageNumber"));
			unstructuredData.setExpectedOccurance(json.getInt("expectedOccurance"));
			unstructuredData.setCellReference(json.getString("cellReference"));
			unstructuredData.setCellRange(json.getString("cellRange"));
			unstructuredData.setSearchPage(json.getInt("searchPage"));
			//unstructuredData.setPasswordProtected(json.getBoolean("isPasswordProtected"));
			unstructuredData.setPassword(json.getString( "password"));  
		} catch (JSONException e) {
			
			logger.error("Error ", e);
		}

		return unstructuredData;

	}

	public ValidationDetails hydrateValidation(int recId) throws DatabaseException {
		ValidationDetails validationData = new ValidationDetails();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("select RECORD_ID,VALIDATION_DATA from UNSTRUCTURED_DATA_VALIDATION where RECORD_ID=?");)
		{
			pst.setInt(1, recId);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					validationData=buildValidationDetailsObject(rs);
				}
			}
		}
		catch(Exception e){
			throw new DatabaseException("Could not fetch unstructured Data user due to an internal error",e);
		}
		return validationData;
	}
	/*Modified by Pushpa for VAPT fix starts*/
	public void deleteUnstructuredData(String[] selectedRecIds) throws DatabaseException {

		for(String id:selectedRecIds) {
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("DELETE UNSTRUCTURED_DATA_VALIDATION WHERE RECORD_ID =? ");)
		{
			pst.setString(1, id);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DatabaseException("Could not delete Unstructured Data due to an internal error.", e);
		}
		}
	
		
	}
	/*Modified by Pushpa for VAPT fix ends*/

	public void updateUnstructuredData(ValidationDetails validateDetails, int recId) throws DatabaseException {

		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("UPDATE UNSTRUCTURED_DATA_VALIDATION SET VALIDATION_DATA=? WHERE RECORD_ID=? and TESTSTEP_ID=?");)
			{
			pst.setString(1,validateDetails.getValidationData());
			pst.setInt(2, recId);
			pst.setInt(3, validateDetails.getTestStepId());
			pst.execute();
		}catch (Exception e) {
			logger.error("Error ", e);
			throw new DatabaseException("Could not update Unstructured Data because of an internal error ");
		}
	
	
		
	}

}

