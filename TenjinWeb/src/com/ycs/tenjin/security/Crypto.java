/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Crypto.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 19-03-2020			 Ashiki					TJN28-4
* 
*/

package com.ycs.tenjin.security;

import static org.apache.commons.codec.binary.Hex.encodeHex;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/* Change History
 * 02-Mar-2015 Authentication Changes (Mahesh): User/AUT/Schema Password Encryption/Decryption changes
 *
*/

public class Crypto {

	private static Logger logger = LoggerFactory
			.getLogger(Crypto.class);

	
	
	
	public String generateSalt() 
	{
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[20];
		random.nextBytes(bytes);
		/*Modified by Ashiki for TJN28-4 starts*/
		String s=null;
		try {
			s = new String(bytes,"US-ASCII");
		} catch (UnsupportedEncodingException e) {
			
			logger.error("Error ", e);
		}
		/*Modified by Ashiki for TJN28-4 ends*/
		return s;
	}
	
	
	public String encryptOneWay(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException 
	{
		int iterationNb=0;
		/*modified by paneendra for VAPT fix starts*/
		/*iterationNb=new Random().nextInt(100);*/
		SecureRandom random = new SecureRandom(); 
		byte bytes[] = new byte[32];
		random.nextBytes(bytes);
		iterationNb=bytes.length;
		/*modified by paneendra for VAPT fix ends*/
		String encodedPassword = null;
		// byte[] btPass={};
		String saltvalue =generateSalt();
		byte salt[]=saltvalue.getBytes();
		String encrypted=String.valueOf(encodeHex(salt));
		byte[] hashkey=getHash(iterationNb,password,salt); 
		/*16-Mar-2015 R2.1:--By--Mahesh:Encrypt password with hashkey not with salt: Starts*/
		//encodedPassword ="SHA-512"+"!#!"+iterationNb+"!#!"+encrypted+"!#!"+String.valueOf(encodeHex(salt));
		encodedPassword ="SHA-512"+"!#!"+iterationNb+"!#!"+encrypted+"!#!"+String.valueOf(encodeHex(hashkey));
		/*16-Mar-2015 R2.1:--By--Mahesh:Encrypt password with hashkey not with salt: Ends*/
		return encodedPassword;
	}
	
	
	
	public byte[] getHash(int iterationNb, String password, byte[] salt) throws NoSuchAlgorithmException, UnsupportedEncodingException 
	 {
	       MessageDigest digest = MessageDigest.getInstance("SHA-512");
	       digest.reset();
	       digest.update(salt);
	       byte[] input = digest.digest(password.getBytes("UTF-8"));
	       for (int i = 0; i < iterationNb; i++) 
	       {
	    	   //System.out.println(Arrays.toString(input));
	           digest.reset();
	           input = digest.digest(input);
	       }
	       return input;
	   }
	
}
