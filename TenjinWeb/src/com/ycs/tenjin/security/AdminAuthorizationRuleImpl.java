/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AdminAuthorizationRuleImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 04-12-2019			Preeti					TENJINCG-1174
* */

package com.ycs.tenjin.security;


import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.servlet.util.SessionUtils;

public class AdminAuthorizationRuleImpl extends AuthorizationRule {
	private static final Logger logger = LoggerFactory.getLogger(AdminAuthorizationRuleImpl.class);
	
	@Override
	public boolean checkAccess(HttpServletRequest request) {
		TenjinSession session = SessionUtils.getTenjinSession(request);
		if(session == null || session.getUser() == null) {
			logger.error("Authorization error --> No session exists");
			return false;
		}
		
		if(!session.getUser().getRoles().equalsIgnoreCase("site administrator")) {
			logger.error("Authorization error --> User {} is not an administrator and hence cannot access {}", request.getRequestURI());
			return false;
		}
		
		return true;
		
	}

}
