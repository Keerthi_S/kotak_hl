/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SpreadSheetPorter.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 3-Nov-2016				Sriram					Defect#673
* 05-Nov-2016            Leelaprasad              Defect #737
* 12-Dec-2016            Leelaprasad             Defect fix#TEN-29,TEN-30
* 17-02-2017             Leelaprasad             Defect #TENJINCG-117   
* 21-02-2017             Leelaprasad             Defect #TENJINCG-117   
* 04-Jul-2017			 Pushpalatha			Req#TENJINCG-266    
* 07-07-2017             Leelaprasad            TENJINCG-274  
* 07-07-2017             Leelaprasad            TENJINCG-281 
* 12-07-2017             Leelaprasad            TenjiNCG-282
* 04-09-2017             Leelaprasad            T25IT-427
* 18-01-2018			 Pushpalatha			TENJINCG-584
* 19-06-2019             Leelaprasad            V2.8-152
*/


package com.ycs.tenjin.port;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class SpreadsheetPorter {

	//Fix for Defect#673 - Sriram 
	/*private int startRowNum = 3;*/
	private int startRowNum = 4;
	//Fix for Defect#673 - Sriram ends
	private Workbook sourceWB = null;
	private Workbook targetWB = null;
	
	/****************************************************************************
	 * Added by Pushpalatha for #TENJINCG-266 starts
	 */

	private static final short QUERY_FIELD_COLOR = IndexedColors.LIGHT_ORANGE
			.getIndex();
	private static final short VERIFY_FIELD_COLOR = IndexedColors.YELLOW
			.getIndex();
	private static final short ADD_FIELD_COLOR = IndexedColors.LAVENDER
			.getIndex();
	/*Changed by Leelaprasad for the Requirement Defect TenjinCG-282  starts*/
	/*private static final short DELETE_FIELD_COLOR = IndexedColors.GREY_50_PERCENT
			.getIndex();*/
	private static final short DELETE_FIELD_COLOR = IndexedColors.GREY_40_PERCENT
			.getIndex();
	/*Changed by Leelaprasad for the Requirement Defect TenjinCG-282  starts*/
	private static final short MAND_FIELD_COLOR = IndexedColors.DARK_RED
			.getIndex();
	private static final short NONMAND_FIELD_COLOR = IndexedColors.DARK_BLUE
			.getIndex();

	/****************************************************************************
	 * Added by Pushpalatha for #TENJINCG-266 ends
	 */
	private static final Logger logger = LoggerFactory.getLogger(SpreadsheetPorter.class);
	
	public void setHeaderRow(int headerRowNumber){
		this.startRowNum = headerRowNumber;
	}
	
	public Map<String, List<PortMap>> checkSheets(String sourceFilePath, String targetFilePath){
		Map<String, List<PortMap>> map = new LinkedHashMap<String, List<PortMap>>();
		
		logger.info("Loading source workbook from path {}", sourceFilePath);
		try {
			this.sourceWB = WorkbookFactory.create(new File(sourceFilePath));
		} catch (InvalidFormatException e) {
			
			logger.error("Error ", e);
		} catch (IOException e) {
			
			logger.error("Error ", e);
		}
		
		
		logger.info("Loading target workbook from path {}", targetFilePath);
		try {
			this.targetWB = WorkbookFactory.create(new File(targetFilePath));
		} catch (InvalidFormatException e) {
			
			logger.error("Error ", e);
		} catch (IOException e) {
			
			logger.error("Error ", e);
		}
		
		logger.info("Both workbooks loaded");
		
		logger.info("Getting source sheets");
		List<String> sourceSheets = this.getSheetNames(this.sourceWB);
		logger.info("Source workbook has {} sheets", sourceSheets.size());

		logger.info("Getting target sheets");
		List<String> targetSheets = this.getSheetNames(this.targetWB);
		logger.info("Target workbook has {} sheets", targetSheets.size());
		
		for(String sourceSheet:sourceSheets){
			logger.info("Processing source sheet {}", sourceSheet);
			List<PortMap> portMap = this.scanSheet(sourceSheet);
			map.put(sourceSheet, portMap);
		}
		
		return map;
		
		
	}
	
	/*private List<PortMap> scanSheet(String sheetName){*/
	private List<PortMap> scanSheet(String pageAreaName){
		/*************
		 * This method will create a List<PortMap> for the specified sheet name,
		 * by scanning both the source and target workbooks
		 */
		List<PortMap> maps = new ArrayList<PortMap>();
		
		String sheetName=this.getExcelSheetName(sourceWB, pageAreaName);
		
		Sheet sourceSheet = this.sourceWB.getSheet(sheetName);
		
		String targetSheetName=this.getExcelSheetName(targetWB, pageAreaName);
		
		Sheet targetSheet = null;
		if(targetSheetName!=null) {
		 targetSheet = this.targetWB.getSheet(targetSheetName);
		}
		
		logger.debug("Getting the list of fields from source sheet");
		List<String> sourceFields = this.getHeaderColumnsInSheet(sourceSheet);
		//commented code deleted by Leelaprasad to format the code
		if(targetSheet != null){
			logger.debug("Target Sheet found for source sheet {}", sheetName);
			List<String> targetFields = this.getHeaderColumnsInSheet(targetSheet);
			for(String sourceField:sourceFields){
				PortMap portMap = new PortMap();
				portMap.setSourceField(sourceField);
				portMap.setTargetSheet(targetSheetName);
				if(targetFields.contains(sourceField)){
					logger.debug("Found target field for source field {}", sourceField);
					portMap.setTargetField(sourceField);
				}else{
					logger.debug("Target Field NOT FOUND for source field {}", sourceField);
					portMap.setTargetField("");
				}
				
				maps.add(portMap);
				
			}
		}else{
			logger.debug("Target Sheet NOT FOUND for source sheet {}", sheetName);
			for(String sourceField:sourceFields){
				PortMap portMap = new PortMap();
				portMap.setSourceField(sourceField);
				portMap.setTargetSheet("");
				portMap.setTargetField("");
				maps.add(portMap);
			}
		}
		
		return maps;
		
		
	}
	
	private List<String> getHeaderColumnsInSheet(Sheet sheet){
		List<String> headers = new ArrayList<String>();
		
		Row row = sheet.getRow(this.startRowNum);
		Iterator<Cell> cellIter = row.iterator();
		
		int buttonFlag=0;
		while(cellIter.hasNext()){
			String fieldName="";
			Cell cell = cellIter.next();
			cell.setCellType(Cell.CELL_TYPE_STRING);
			fieldName=cell.getStringCellValue();
			if(fieldName.equalsIgnoreCase("Button")){
				if(buttonFlag!=0){
					fieldName=fieldName+buttonFlag;
				}
				buttonFlag++;
			}
			headers.add(fieldName);
		}
		
		return headers;
	}
	/*Changed by Leelaprasad for the requirement demo tenjin gold copy fixes*/
	private List<String> getSheetNames(Workbook workbook){
		List<String> sheets = new ArrayList<String>();
		
		try {
			int total = workbook.getNumberOfSheets();
			for(int i=0; i<total; i++){
				Sheet sheet = workbook.getSheetAt(i);
				if(sheet.getSheetName().equalsIgnoreCase("TTD_HELP")){
					continue;
				}		
				String sheetName=sheet.getRow(0).getCell(1).getStringCellValue();
				sheets.add(sheetName);
				/*sheets.add(sheet.getSheetName());*/
			}
		} catch (Exception e) {
			
			logger.error("ERROR while getting all sheet names in workbook", e);
		}
		
		return sheets;
	}
	

	public Map<String,List<String>> getSheetData(String spreadSheetPath){
		Map<String,List<String>> sheetData=new  LinkedHashMap<String,List<String>>();
		try {
			Workbook workbook = WorkbookFactory.create(new File(spreadSheetPath));
			
			int total = workbook.getNumberOfSheets();
			//this.setHeaderRow(4); Commented by Sriram to ffix defect#673
			for(int i=0; i<total; i++){
				Sheet sheet = workbook.getSheetAt(i);
				String pageName=sheet.getRow(0).getCell(1).getStringCellValue();
				List<String> headers=this.getHeaderColumnsInSheet(sheet);
				sheetData.put(pageName,headers);
			}
		}catch(Exception e){
			logger.error("ERROR while getting all sheet names in workbook and columns names", e);
		}
		return sheetData;
	}
	/*Changed by Leelaprasad for the requirement demo tenjin gold copy fixes*/
	public Sheet addButtonIndex(Sheet sheet){
		
		Row row = sheet.getRow(this.startRowNum);
		Iterator<Cell> cellIter = row.iterator();
		
		int buttonFlag=0;
		while(cellIter.hasNext()){
			String fieldName="";
			Cell cell = cellIter.next();
			cell.setCellType(Cell.CELL_TYPE_STRING);
			fieldName=cell.getStringCellValue();
			if(fieldName.equalsIgnoreCase("Button")){
				if(buttonFlag!=0){
					cell.setCellValue(fieldName+buttonFlag);
				}
				buttonFlag++;
			}
			
		}
		return sheet;
		
	}
	
	public String getExcelSheetName(Workbook workbook,String pageAreaName) {
		
		String excelSheetName=null;
		for(int i=0;i<workbook.getNumberOfSheets();i++) {
			if(pageAreaName!=null) {
			if(pageAreaName.equalsIgnoreCase(workbook.getSheetAt(i).getRow(0).getCell(1).getStringCellValue())) {
				excelSheetName=workbook.getSheetName(i);
				break;
			}
			}
			
		}
		return excelSheetName;
		
	}
	/*Changed by Leelaprasad for the requirement demo tenjin gold copy fixes*/
	
	/*Method formatted and comments removed by Leelaprasad */
	public String portData(String targetFunctionCode, String sourceFilePath, String targetFilePath, String portMapPath, String destinationFolderPath, String dataStyle) throws Exception {	

		boolean withStyleOrDataOnly = false;
		try {
			if (dataStyle != null && dataStyle.equalsIgnoreCase("DATASTYLE")) {
				withStyleOrDataOnly = true;
			} else if (dataStyle != null
					&& dataStyle.equalsIgnoreCase("DATAONLY")) {
				withStyleOrDataOnly = false;
			}
				
			this.sourceWB = WorkbookFactory.create(new File(sourceFilePath));
			this.targetWB = WorkbookFactory.create(new File(targetFilePath));
		} catch (InvalidFormatException e) {
			
			logger.error(e.getMessage(),e);
			throw new Exception("Could not port data due to an internal error");
		} catch (IOException e) {
			
			logger.error(e.getMessage(),e);
			throw new Exception("Could not port data due to an internal error");
		}
		
		
		CellStyle manStyle = getMandatoryFieldStyle(this.targetWB);
		CellStyle queryCellStyle = getQueryCellStyle(this.targetWB);
		CellStyle addCellStyle = getAddCellStyle(this.targetWB);
		CellStyle deleteCellStyle = getDeleteCellStyle(this.targetWB);
		CellStyle verifyCellStyle = getVerifyCellStyle(this.targetWB);
		CellStyle nonmanCellStyle = getNonManCellStyle(this.targetWB);
		
		PortXmlHelper portHandler = new PortXmlHelper();
		File file = new File(portMapPath);
		Map<String, List<PortMap>> map = portHandler.parseXml(file);
		
		Map<String, String> alredyTravelled = new LinkedHashMap<String, String>();
		Sheet destinationSheet =null;
		
		int temp = 0;
		 boolean dataFlag=false;
		for (Entry<String, List<PortMap>> m : map.entrySet()) {
       /*Changed by Leelaprasad for the requirement demo tenjin gold copy fixes*/
		/*	Sheet sourceSheet = sourceWB.getSheet(m.getKey());*/
			Sheet sourceSheet = this.addButtonIndex(sourceWB.getSheet(this.getExcelSheetName(sourceWB, m.getKey())));
/*Changed by Leelaprasad for the requirement demo tenjin gold copy fixes*/
			PortMap portMap = null;

			List<PortMap> portObj = m.getValue();
			int colnum = portObj.size();
			List<String> sfList=new  ArrayList<String>();
		
			List<Integer> sfListColmColor = new ArrayList<Integer>();
			
			Map<String, String> sourceButtList =null;
			
			// rows
			int nRowsSourceSheet=sourceSheet.getLastRowNum();  
			Row sourceRow = null;
			
				for (int j = 5; j <= sourceSheet.getLastRowNum(); j++) {

					sourceRow = sourceSheet.getRow(j);
					boolean rowFlag=false;
					
					 int addedButtons=0;
					
					for(int fno=0;fno<colnum;fno++){
						String str = ""; int colorCode=0;
						try{
						 str = sourceSheet.getRow(this.startRowNum)
								.getCell(fno).getStringCellValue();
						  colorCode = sourceSheet.getRow(this.startRowNum)
									.getCell(fno).getCellStyle()
									.getFillForegroundColor();
						}catch(Exception ro){
							logger.error(ro.getMessage(), ro);
							throw new Exception("Please check your source Sheet '"+sourceSheet.getSheetName()+"' having empty columns ");
						}
						
						sfListColmColor.add(colorCode);
						
						sfList.add(str);
						
					}
					
					// adding button duplicates
					for (int i = 0; i < sfList.size(); i++) {
						int counter = 2;
						for (int k = i + 1; k < sfList.size(); k++) {
							if (sfList.get(i).equals(sfList.get(k))) {
								sfList.set(
										k,
										sfList.get(k).concat(
												Integer.toString(counter)));
								counter++;
							}
						}
					}

					// get buttons from sflist
					int indexValue = 0;
					sourceButtList= new LinkedHashMap<String, String>();
					for (String cellButton : sfList) {
						
						if (cellButton.toLowerCase().contains("button")
								|| cellButton.toLowerCase().contains("link")) {
							sourceButtList.put(cellButton,
									String.valueOf(indexValue) + ";;"
											+ sfListColmColor.get(indexValue));
							
						}
						indexValue++;
					}
					// cloumn
					
					for(int stat=0;stat<colnum;stat++){
						 
						
					portMap = m.getValue().get(stat);
					String tfield = portMap.getTargetField();
					String sfield=portMap.getSourceField();
					String tSheet=portMap.getTargetSheet();
					//Chnaged by Leelaprasad for demo fixes
					if(tfield.equalsIgnoreCase("") || tSheet.equalsIgnoreCase("")){
						continue;
					}
					sourceRow = sourceSheet.getRow(j);
					int sFIdx=sfList.indexOf(sfield);
					if(j>=9 && j<=12){
						System.err.println();
					}
					
					Cell cell = null;
					try {
						cell = sourceRow.getCell(sFIdx);
					} catch (Exception e) {

					}
					if(cell==null){
						if(tfield.toLowerCase().contains("button")&&sfield.toLowerCase().contains("button")){
							dataFlag=true;
						}
					}
					
					if(cell!=null){
						
					sourceRow.getCell(sFIdx).setCellType(Cell.CELL_TYPE_STRING);
					}
					else{
						continue;
					
					}
					String value=sourceRow.getCell(sFIdx).getStringCellValue();
					
					int colorCodeSource = sourceRow.getCell(sFIdx)
							.getCellStyle().getFillForegroundColor();
					   /*Changed by Leelaprasad for the requirement demo tenjin gold copy fixes*/
					   /*destinationSheet = targetWB.getSheet(tSheet);*/
					destinationSheet = this.addButtonIndex(targetWB.getSheet(this.getExcelSheetName(targetWB, tSheet)));
					   /*Changed by Leelaprasad for the requirement demo tenjin gold copy fixes*/
					int tsheetIndex = targetWB.getSheetIndex(tSheet);
					
					Row destinationRow=null;
					
					
					try{
						if(destinationSheet!=null && destinationSheet.getRow(j)!=null){
							rowFlag=true;
						}
						else{
						rowFlag=false;
						}
						}
						catch(Exception e){
									continue;
						}
				
					
					if(!rowFlag){
						
						 try{
							
							 Row	destinationRow1 = destinationSheet.getRow(j);
							 if(destinationRow1==null){
							 destinationRow = destinationSheet.createRow(j);
							 rowFlag=true;
							 }
							 else{
									destinationRow = destinationSheet.getRow(j);
								}
														}
							catch(Exception e){
								
								try{
									destinationRow = destinationSheet.createRow(j);
									rowFlag = true;
									}catch(Exception er){
										continue;
									}
								
							}
					
						
						}else{
							destinationRow = destinationSheet.getRow(j);
						}
					
					
					
					try {
						if ((alredyTravelled.get(tSheet) == null
								|| !alredyTravelled.get(tSheet).equals(tSheet)) && tfield.toLowerCase().contains("button")) {
							alredyTravelled.put(tSheet, tSheet);
							int increment = 0;
								int sourcephyLength = sourceSheet.getRow(4)
									.getPhysicalNumberOfCells();
						
							
							int endButtonTarget=this.getSizeOfEndButtons(this.getHeaderColumnsInSheet(destinationSheet));
							
							
							for (Map.Entry<String, String> entry : sourceButtList
									.entrySet()) {
								
								int phyLength = destinationSheet.getRow(4)
										.getPhysicalNumberOfCells();
								
								System.out.println("SourceSheet:="
										+ sourceSheet.getSheetName()
										+ "   entry :" + entry.getKey() + "  "
										+ entry.getValue());
								Cell buttonColm = null;
								String[] entryValue = entry.getValue().split(
										";;");
								int entryIndex = Integer
										.parseInt(entryValue[0]);
									int entryColorCode = Integer
										.parseInt(entryValue[1]);
								if (entryIndex > 2	&& entryIndex < phyLength - endButtonTarget ) {
									buttonColm = destinationSheet.getRow(4)
											.getCell(entryIndex);
									
									boolean buttonFalse=true;
									if(buttonColm==null){
										buttonColm = destinationSheet.getRow(4)
												.getCell(entryIndex-1);
										if(!buttonColm.toString().toLowerCase().contains("button") && buttonColm!=null){
											
											buttonFalse=true;
										}else{
											buttonFalse=false;
										}
									}
									if((sourcephyLength-1)!=entryIndex ||buttonFalse==true){
										
									
									
									buttonColm = destinationSheet.getRow(4)
											.getCell(entryIndex);
									if (buttonColm.getStringCellValue()
											.toLowerCase().contains("button")
											|| buttonColm.getStringCellValue()
													.toLowerCase()
													.contains("link")) { 
										 logger.info("start debugging");

									} else {
										
										
										insertNewColumnBefore(tsheetIndex,
												entryIndex, entry.getKey(),nRowsSourceSheet);
										destinationSheet.getRow(4)
												.getCell(entryIndex)
												.setCellValue(entry.getKey());
									
										addedButtons++;
										if (entryColorCode == QUERY_FIELD_COLOR) {
											destinationSheet
													.getRow(4)
													.getCell(entryIndex)
													.setCellStyle(
															queryCellStyle);
										} else if (entryColorCode == MAND_FIELD_COLOR) {
											destinationSheet.getRow(4)
													.getCell(entryIndex)
													.setCellStyle(manStyle);
										} else if (entryColorCode == NONMAND_FIELD_COLOR) {
											destinationSheet
													.getRow(4)
													.getCell(entryIndex)
													.setCellStyle(
															nonmanCellStyle);
										} else if (entryColorCode == DELETE_FIELD_COLOR) {
											destinationSheet
													.getRow(4)
													.getCell(entryIndex)
													.setCellStyle(
															deleteCellStyle);
										} else if (entryColorCode == VERIFY_FIELD_COLOR) {
											destinationSheet
													.getRow(4)
													.getCell(entryIndex)
													.setCellStyle(
															verifyCellStyle);
										} else if (entryColorCode == ADD_FIELD_COLOR) {
											destinationSheet.getRow(4)
													.getCell(entryIndex)
													.setCellStyle(addCellStyle);
										}

									}
									}
                                
							}else {
									int indexCell=0;
											if(phyLength<8){
												indexCell=phyLength + increment+1;
											}
											else{
												indexCell=phyLength + increment;
											}
											
									buttonColm = destinationSheet.getRow(4)
											.getCell(phyLength + increment-1);
										if (buttonColm.getStringCellValue().equalsIgnoreCase(entry.getKey()))	 {
									
										increment++;
									}
									
								
									else {
										increment++;
										
									}
									if (entryColorCode == QUERY_FIELD_COLOR) {
										destinationSheet.getRow(4) 
												.getCell(indexCell)
												.setCellStyle(queryCellStyle);
									} else if (entryColorCode == MAND_FIELD_COLOR) {
										destinationSheet.getRow(4)
												.getCell(indexCell)
												.setCellStyle(manStyle);
									} else if (entryColorCode == NONMAND_FIELD_COLOR) {
										try{
										destinationSheet.getRow(4)
												.getCell(indexCell)
												.setCellStyle(nonmanCellStyle);
										}catch(Exception se){}
									} else if (entryColorCode == DELETE_FIELD_COLOR) {
										destinationSheet.getRow(4)
												.getCell(indexCell)
												.setCellStyle(deleteCellStyle);
									} else if (entryColorCode == VERIFY_FIELD_COLOR) {
										destinationSheet.getRow(4)
												.getCell(indexCell)
												.setCellStyle(verifyCellStyle);
									} else if (entryColorCode == ADD_FIELD_COLOR) {
										destinationSheet.getRow(4)
												.getCell(indexCell)
												.setCellStyle(addCellStyle);
									}

								}
							}
							
							
							
						}
					} catch (Exception but) {
					}
					
					
					Iterator<Cell> destItr = destinationSheet.getRow(4)
							.cellIterator();
                     if(j==5 && dataFlag && !tfield.toLowerCase().contains("button")&& !sfield.toLowerCase().contains("button")){
                    	 destItr=sourceSheet.getRow(4).cellIterator();
                    	 
                    	 }
					while (destItr.hasNext()) {
						
						Cell destCellColumn = destItr.next();
						int colindex=destCellColumn.getColumnIndex();
						String cellValueCloumn = destCellColumn
								.getStringCellValue();
						try{
						if (sfListColmColor.get(colindex) == QUERY_FIELD_COLOR) {
							destCellColumn.setCellStyle(queryCellStyle);
						} else if (sfListColmColor.get(colindex) == VERIFY_FIELD_COLOR) {
							destCellColumn.setCellStyle(verifyCellStyle);
						} else if (sfListColmColor.get(colindex) == ADD_FIELD_COLOR) {
							destCellColumn.setCellStyle(addCellStyle);
						}
						}catch(Exception e){}

						if (tfield.equalsIgnoreCase(cellValueCloumn)) {
							// column header
							
							int i = destCellColumn.getColumnIndex();
							
							
							// row data
							Cell targetDataCell =null;
                               try{
							 targetDataCell = destinationRow.createCell(i);
							targetDataCell.setCellValue(value);
                               }catch(Exception er){}

							if (withStyleOrDataOnly) {
								if (colorCodeSource == QUERY_FIELD_COLOR) {
									targetDataCell.setCellStyle(queryCellStyle);
								} else if (colorCodeSource == MAND_FIELD_COLOR) {
									targetDataCell.setCellStyle(manStyle);
								} else if (colorCodeSource == NONMAND_FIELD_COLOR) {
									targetDataCell
											.setCellStyle(nonmanCellStyle);
								} else if (colorCodeSource == DELETE_FIELD_COLOR) {
									targetDataCell
											.setCellStyle(deleteCellStyle);
								} else if (colorCodeSource == VERIFY_FIELD_COLOR) {
									targetDataCell
											.setCellStyle(verifyCellStyle);
								} else if (colorCodeSource == ADD_FIELD_COLOR) {
									targetDataCell.setCellStyle(addCellStyle);
								}
							}
							
							break;
						}

					}

				}// end of columns
			}// end of rows
			temp++;
			
			
			try {
				int phyLengthColumn = destinationSheet.getRow(4)
						.getPhysicalNumberOfCells();

				for(int kl=0;kl<phyLengthColumn;kl++){
					String buttLink="";
					if(destinationSheet!=null && destinationSheet.getRow(4)!=null && destinationSheet.getRow(4).getCell(kl)!=null){
				        buttLink=destinationSheet.getRow(4).getCell(kl).getStringCellValue();
					
					if(buttLink.toLowerCase().contains("button")){
						destinationSheet.getRow(4).getCell(kl).setCellValue("Button");
					}
					else if(buttLink.toLowerCase().contains("link")){
						destinationSheet.getRow(4).getCell(kl).setCellValue("Link");
					}
					}
					
				
				
}
			} catch (Exception e) {
				
				
			}
		
		/// normalize button
		}
	
			
		
		/*Changed by Pushpalatha for TENJINCG-584 starts*/
		String outputFileName = destinationFolderPath + File.separator + targetFunctionCode + "_Ported.xlsx";
		/*Changed by Pushpalatha for TENJINCG-584 ends*/
		try {
			logger.info("Writing output to file");
			OutputStream out = new FileOutputStream(new File(outputFileName));
			this.targetWB.write(out);
			out.close();
		} catch (Exception e) {
			
			logger.error("Could not write output Excel file after porting", e);
			throw new Exception("Could not port data due to an intenal error");
		}

		return targetFunctionCode + "_Ported.xlsx";
	}

	public Map<Boolean, String> validateExcelSheets(String sourceFilePath, String targetFilePath) {

		Boolean flag = false;
		String sheetName = null;
		Map<Boolean, String> map = new HashMap<Boolean, String>();

		Workbook soucreWorkbook = null;
		Workbook targetWorkbook = null;
		try {
			soucreWorkbook = WorkbookFactory.create(new File(sourceFilePath));
		} catch (InvalidFormatException | IOException e) {
			
			logger.error("Error ", e);
		}
		Sheet srcSheet = soucreWorkbook.getSheetAt(0);

		String srcName = srcSheet.getSheetName();
		if (srcName.equalsIgnoreCase("TTD_HELP")) {

			sheetName = "";
		} else {
			sheetName = "sourceFile";

		}
		try {
			targetWorkbook = WorkbookFactory.create(new File(targetFilePath));
		} catch (InvalidFormatException | IOException e) {
			
			logger.error("Error ", e);
		}
		Sheet tarSheet = targetWorkbook.getSheetAt(0);
		String tarName = tarSheet.getSheetName();

		if (tarName.equalsIgnoreCase("TTD_HELP")) {

			sheetName = "";
		} else {
			sheetName = sheetName + "targetfile";

		}

		if (tarName.equalsIgnoreCase("TTD_HELP") && srcName.equalsIgnoreCase("TTD_HELP")) {
			flag = true;
		} else {
			flag = false;
		}
		map.put(flag, sheetName);
		return map;

	}
	/*Changed by Leelaprasad for the requirement V2.8-152 ends*/
    /*Changed by Leelaprasad for the Requirement Defect Fix TEN-29,TEN-30 on 12-12-2016 ends*/
	
    /****************************************************************************
	 * Added by Pushpalatha for #TENJINCG-266 starts
	 */
	private static CellStyle getNonManCellStyle(Workbook wb) {

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

		private static CellStyle getQueryCellStyle(Workbook wb) {

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);


		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getAddCellStyle(Workbook wb) {

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);


		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.LAVENDER.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getDeleteCellStyle(Workbook wb) {

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.WHITE.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getVerifyCellStyle(Workbook wb) {

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getMandatoryFieldStyle(Workbook wb) {

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.DARK_RED.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	public void insertNewColumnBefore(int sheetIndex, int columnIndex,
			String newColName,int sourceNRows) {
		assert this.targetWB != null;

		FormulaEvaluator evaluator = this.targetWB.getCreationHelper()
				.createFormulaEvaluator();
		evaluator.clearAllCachedResultValues();

		Sheet sheet = this.targetWB.getSheetAt(sheetIndex);
		//int nrRows = getNumberOfRows(sheetIndex);
		int nrCols = getNrColumns(sheetIndex);
		System.out.println("Inserting new column at " + columnIndex);
		
		for (int row = 4; row < 5; row++) {
		//for (int row = 4; row < nrRows; row++) {
			Row r = sheet.getRow(row);

			if (r == null) {
				continue;
			}

			// shift to right
			for (int col = nrCols; col > columnIndex; col--) {
				Cell rightCell = r.getCell(col);
				if (rightCell != null) {
					r.removeCell(rightCell);
				}

				Cell leftCell = r.getCell(col - 1);

				if (leftCell != null) {
					Cell newCell = r.createCell(col, leftCell.getCellType());
					cloneCell(newCell, leftCell);
					if (newCell.getCellType() == Cell.CELL_TYPE_FORMULA) {
						// newCell.setCellFormula(ExcelHelper.updateFormula(newCell.getCellFormula(),
						// columnIndex));
						evaluator.notifySetFormula(newCell);
						CellValue cellValue = evaluator.evaluate(newCell);
						evaluator.evaluateFormulaCell(newCell);
						System.out.println("cellValue := " + cellValue);
					}

				}
			}

			// delete old column
			int cellType = Cell.CELL_TYPE_BLANK;

			Cell currentEmptyWeekCell = r.getCell(columnIndex);
			if (currentEmptyWeekCell != null) {
				// cellType = currentEmptyWeekCell.getCellType();
				r.removeCell(currentEmptyWeekCell);
			}

			// create new column
			r.createCell(columnIndex, cellType);
		}

		XSSFFormulaEvaluator
				.evaluateAllFormulaCells((XSSFWorkbook) this.targetWB);
	}

	private static void cloneCell(Cell cNew, Cell cOld) {
		cNew.setCellComment(cOld.getCellComment());
		cNew.setCellStyle(cOld.getCellStyle());

		switch (cOld.getCellType()) {
		case Cell.CELL_TYPE_BOOLEAN: {
			cNew.setCellValue(cOld.getBooleanCellValue());
			break;
		}
		case Cell.CELL_TYPE_NUMERIC: {
			cNew.setCellValue(cOld.getNumericCellValue());
			break;
		}
		case Cell.CELL_TYPE_STRING: {
			cNew.setCellValue(cOld.getStringCellValue());
			break;
		}
		case Cell.CELL_TYPE_ERROR: {
			cNew.setCellValue(cOld.getErrorCellValue());
			break;
		}
		case Cell.CELL_TYPE_FORMULA: {
			cNew.setCellFormula(cOld.getCellFormula());
			break;
		}
		}
	}


	public int getNrColumns(int sheetIndex) {
		assert this.targetWB != null;

		Sheet sheet = this.targetWB.getSheetAt(sheetIndex);

		// get header row
		//Row headerRow = sheet.getRow(0);
		Row headerRow = sheet.getRow(4);
		int nrCol = headerRow.getLastCellNum();
		System.out.println("Found " + nrCol + " columns.");
		return nrCol;

	}
	public int getSizeOfEndButtons(List<String> fieldsList){
		int numFlag=0;
		for(int number=fieldsList.size()-1;number>0;number--){
			
			if(fieldsList.get(number).toString().contains("Button")||fieldsList.get(number).toString().contains("link")){
				numFlag++;
			}else{
				break;
			}
	}
		return numFlag;
		
	}
	
	/*Added by Pushpalatha for #TENJINCG-266 ends*/
}

