/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  PortingSession.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 20-05-2020           Priyanka				   TENJINCG-1208 
* */

package com.ycs.tenjin.port;

import java.util.List;
import java.util.Map;

public class PortingSession {
	private String sourceFilePath;
	private String targetFilePath;
	private String portMapXmlPath;
	private String sourceFunction;
	private String targetFunc;
	private int sourceAppId;
	private int targetAppId;
	private boolean portMapExists;
	private Map<String, List<String>> targetSheetMap;
	/*Added by Priyanka for TENJINCG-1208 start*/
	private String sourceAppName;
	private String targetAppName;
	/*Added by Priyanka for TENJINCG-1208 end*/
	
	
	public Map<String, List<String>> getTargetSheetMap() {
		return targetSheetMap;
	}
	public void setTargetSheetMap(Map<String, List<String>> targetSheetMap) {
		this.targetSheetMap = targetSheetMap;
	}
	
	public void setPortMapExists(boolean portMapExists) {
		this.portMapExists = portMapExists;
	}
	public String getSourceFilePath() {
		return sourceFilePath;
	}
	public void setSourceFilePath(String sourceFilePath) {
		this.sourceFilePath = sourceFilePath;
	}
	public String getTargetFilePath() {
		return targetFilePath;
	}
	public void setTargetFilePath(String targetFilePath) {
		this.targetFilePath = targetFilePath;
	}
	public String getPortMapXmlPath() {
		return portMapXmlPath;
	}
	public void setPortMapXmlPath(String portMapXmlPath) {
		this.portMapXmlPath = portMapXmlPath;
	}
	public String getSourceFunction() {
		return sourceFunction;
	}
	public void setSourceFunction(String sourceFunction) {
		this.sourceFunction = sourceFunction;
	}
	public String getTargetFunc() {
		return targetFunc;
	}
	public void setTargetFunc(String targetFunc) {
		this.targetFunc = targetFunc;
	}
	public int getSourceAppId() {
		return sourceAppId;
	}
	public void setSourceAppId(int sourceAppId) {
		this.sourceAppId = sourceAppId;
	}
	public int getTargetAppId() {
		return targetAppId;
	}
	public void setTargetAppId(int targetAppId) {
		this.targetAppId = targetAppId;
	}
	
	public boolean doesPortMapExist() {
		return portMapExists;
	}
	
	/*Added by Priyanka for TENJINCG-1208 start*/
	public String getSourceAppName() {
		return sourceAppName;
	}
	public void setSourceAppName(String sourceAppName) {
		this.sourceAppName = sourceAppName;
	}
	public String getTargetAppName() {
		return targetAppName;
	}
	public void setTargetAppName(String targetAppName) {
		this.targetAppName = targetAppName;
	}
	/*Added by Priyanka for TENJINCG-1208 end*/
	
}
