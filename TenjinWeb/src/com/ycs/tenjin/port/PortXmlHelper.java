/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  PortXmlHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 05-11-2016           Leelaprasad             to fix the defect #737
* 13-12-2016        Leelaprasad             Fix for Defect#TEN-35
* 04-Jul-2017		Pushpalatha					#TENJINCG-266
* 05-07-2017        Leelprasad             TENJINCG-275 
* 31-08-2017        Leelaprasad            T25IT-396
* 18-01-2018		 Pushpalatha		    TENJINCG-584
*/

package com.ycs.tenjin.port;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;

public class PortXmlHelper {
	
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PortXmlHelper.class);
	
	public void createXML(Map<String,List<PortMap>> map, int sApp, int tApp, String sFunc, String tFunc) throws Exception {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.newDocument();

		Element portMap = doc.createElement("portmap");
		doc.appendChild(portMap);
		
		Element user = doc.createElement("tenjin_user");
		portMap.appendChild(user);
		/*user.appendChild(doc.createTextNode("Gangadhar"));*/
		user.appendChild(doc.createTextNode("tenjin"));

		
		Element application = doc.createElement("application");
		portMap.appendChild(application);
		/*application.appendChild(doc.createTextNode("Finacle"));*/
		application.appendChild(doc.createTextNode("Tenjin"));


		Element function = doc.createElement("function");
		portMap.appendChild(function);
		/*function.appendChild(doc.createTextNode("HOACCCLA"));*/
		function.appendChild(doc.createTextNode("CRTUSER"));

		Element sheets = doc.createElement("Sheets");
		portMap.appendChild(sheets);
		
        for(Map.Entry<String,List<PortMap>> m:map.entrySet()){
        	String str=m.getKey();
        
		Element sheet = doc.createElement("sheet");
		sheet.setAttribute("name", str);
		sheets.appendChild(sheet);
        
		Element fields = doc.createElement("fields");
		sheet.appendChild(fields);
		int num=m.getValue().size();
		
		for(int i=0;i<num;i++){
			PortMap portHelper=new PortMap();
			portHelper=m.getValue().get(i);
			String source_field_Name=portHelper.getSourceField();
			String target_field_Name=portHelper.getTargetField();
			String target_sheet_Name=portHelper.getTargetSheet();

		Element field = doc.createElement("field");
		field.setAttribute("name", source_field_Name);
		field.setAttribute("target-field", target_field_Name);
		field.setAttribute("target-sheet", target_sheet_Name);
		fields.appendChild(field);
		
        }
        }
        
        String folderPath = "";
        
        try{
        	folderPath = TenjinConfiguration.getProperty("PORT_MAP_LOCATION");
        }catch(Exception e){
        	folderPath = "D:\\";
        }
        
        String fileName = sApp + "_" + sFunc + "_" + tApp + "_" + tFunc + ".xml";
        
		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		/*Changed by Pushpalatha for TENJINCG-584 starts*/
		StreamResult result = new StreamResult(new File(
				folderPath + File.separator + fileName));
		/*Changed by Pushpalatha for TENJINCG-584 ends*/
		transformer.transform(source, result);

	}
	
	public void updateMapIndividual(int sAppId, int tAppId, String sFuncCode, String tFuncCode, String sourceSheet, String targetSheet, String sourceField, String targetField) throws Exception{
		String portMapPath = null;
		
		try{
			portMapPath = TenjinConfiguration.getProperty("PORT_MAP_LOCATION");
			logger.info("Port Map Path --> {}", portMapPath);
		}catch(TenjinConfigurationException e){
			logger.error("Could not find port map XML path from config file", e);
			return;
		}
		
		String xmlFileName = sAppId + "_" + sFuncCode + "_" + tAppId + "_" + tFuncCode + ".xml";
		/*Changed by Pushpalatha for TENJINCG-584 starts*/
		logger.info("Locating port map file from {}", portMapPath + File.separator + xmlFileName);
		/*Changed by Pushpalatha for TENJINCG-584 ends*/
		/*Changes done by Pushpalatha for TENJINCG-266 starts*/
		/*Changes done by Pushpalatha for TENJINCG-266 ends*/
		try{
			File xmlFile = new File(portMapPath + "\\" + xmlFileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilderFactory.newInstance("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl", ClassLoader.getSystemClassLoader());
			/*Added by paneendra for VAPT FIX starts*/
			dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			//dbFactory.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, "true");
			dbFactory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
			/*Added by paneendra for VAPT FIX ends*/
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			
			logger.info("Successfully parsed XML");
			
			doc.getDocumentElement().normalize();
			logger.info("Normalize document -> Done");
			
			logger.info("Getting all sheets");
			NodeList sheetNodes = doc.getElementsByTagName("sheet");
			logger.info("{} sheets loaded", sheetNodes.getLength());
			
			for(int i=0; i<sheetNodes.getLength(); i++){
				Node sheetNode = sheetNodes.item(i);
				NamedNodeMap attrs = sheetNode.getAttributes();
				Node nameAttr =  attrs.getNamedItem("name");
				if(sourceSheet.equalsIgnoreCase(nameAttr.getTextContent())){
					Element sNode = (Element) sheetNode;
					NodeList fieldNodes = sNode.getElementsByTagName("field");
					/*Added by Leelprasad for the bug TENJINCG-275 starts*/
					Boolean flag= true;
					for(int j=0; j<fieldNodes.getLength(); j++){
						Node fieldNode = fieldNodes.item(j);
						NamedNodeMap fAttrs = fieldNode.getAttributes();
						Node sNameAttr = fAttrs.getNamedItem("target-field");
						if(targetField.equalsIgnoreCase(sNameAttr.getTextContent())){
							flag=false;
							break;
						}
					}
					if(flag){
						/*Added by Leelprasad for the bug TENJINCG-275 ends*/
					for(int j=0; j<fieldNodes.getLength(); j++){
						Node fieldNode = fieldNodes.item(j);
						NamedNodeMap fAttrs = fieldNode.getAttributes();
						Node fNameAttr = fAttrs.getNamedItem("name");
						
						if(sourceField.equalsIgnoreCase(fNameAttr.getTextContent())){
							Node tSheetAttr = fAttrs.getNamedItem("target-sheet");
							Node tFieldAttr = fAttrs.getNamedItem("target-field");
							
							tSheetAttr.setTextContent(targetSheet);
							tFieldAttr.setTextContent(targetField);
							
							logger.info("Updated source and target mappings for Source field {} on sheet {}", sourceField, sourceSheet);
							
							break;
						}
					}
					/*Added by Leelprasad for the bug TENJINCG-275 starts*/
					}else{
						logger.error("Mapping for target field alread exists");
						/*Chnaged by Leelaprasad for the defect T25IT-396 starts*/
						/*throw new Exception("Mapping for target field " +targetField+ " alread exists.you cant map multiple source fields to single target field");*/
						throw new Exception("Mapping for target field " +targetField+ " already exists.You cannot map multiple source fields to single target field");
						/*Chnaged by Leelaprasad for the defect T25IT-396 ends*/
					}
					/*Added by Leelprasad for the bug TENJINCG-275 ends*/
					
					break;
				}
				/*Changed by Leelprasad for the requirement defect T25IT-404 starts*/
				else{
					Element sNode = (Element) sheetNode;
					NodeList fieldNodes = sNode.getElementsByTagName("field");
					/*Added by Leelprasad for the bug TENJINCG-275 starts*/
					Boolean flag= true;
					for(int j=0; j<fieldNodes.getLength(); j++){
						Node fieldNode = fieldNodes.item(j);
						NamedNodeMap fAttrs = fieldNode.getAttributes();
						Node sNameAttr = fAttrs.getNamedItem("target-field");
						if(targetField.equalsIgnoreCase(sNameAttr.getTextContent())){
							flag=false;
							break;
						}
					}
					logger.error("Mapping for target field alread exists");
				}
				/*Changed by Leelprasad for the requirement defect T25IT-404 ends*/
			}
			
			logger.info("Saving changes");
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			/*Added by paneendra for VAPT FIX starts*/
			transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
			/*Added by paneendra for VAPT FIX ends*/
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(portMapPath + "\\" + xmlFileName));
			transformer.transform(source, result);
			logger.info("Done");
		}catch(Exception e){
			logger.error("ERROR updating Port Mapping", e);
			/*Added by Leelprasad for the bug TENJINCG-275 starts*/
			/*throw new Exception("An internal error occurred while updating the mapping");*/
			throw new Exception(e.getMessage());
			/*Added by Leelprasad for the bug TENJINCG-275 ends*/
		}
	}
	
	/*Changes done by Pushpalatha for TENJINCG-266 starts*/
	public void updateUnMappedeSourceFields(int sAppId, int tAppId, String sFuncCode, String tFuncCode, String sourceSheet, String targetSheet, String sourceField, String targetField) throws Exception{
		String portMapPath = null;
		
		try{
			portMapPath = TenjinConfiguration.getProperty("PORT_MAP_LOCATION");
			logger.info("Port Map Path --> {}", portMapPath);
		}catch(TenjinConfigurationException e){
			logger.error("Could not find port map XML path from config file", e);
			return;
		}
		
		String xmlFileName = sAppId + "_" + sFuncCode + "_" + tAppId + "_" + tFuncCode + ".xml";
		logger.info("Locating port map file from {}", portMapPath + "\\" + xmlFileName);
		PortXmlHelper helper1 = new PortXmlHelper();
		String s=helper1.getJSTreePortMapJsonNotMapped(sAppId, tAppId, sFuncCode, tFuncCode);
		System.out.println( s);
		
		try{
			File xmlFile = new File(portMapPath + "\\" + xmlFileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilderFactory.newInstance("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl", ClassLoader.getSystemClassLoader());
			/*Added by paneendra for VAPT FIX starts*/
			dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			//dbFactory.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, "true");
			dbFactory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
			/*Added by paneendra for VAPT FIX ends*/
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			
			logger.info("Successfully parsed XML");
			
			doc.getDocumentElement().normalize();
			logger.info("Normalize document -> Done");
			
			logger.info("Getting all sheets");
			NodeList sheetNodes = doc.getElementsByTagName("sheet");
			logger.info("{} sheets loaded", sheetNodes.getLength());
			
			for(int i=0; i<sheetNodes.getLength(); i++){
				Node sheetNode = sheetNodes.item(i);
				NamedNodeMap attrs = sheetNode.getAttributes();
				Node nameAttr =  attrs.getNamedItem("name");
				if(sourceSheet.equalsIgnoreCase(nameAttr.getTextContent())){
					Element sNode = (Element) sheetNode;
					NodeList fieldNodes = sNode.getElementsByTagName("field");
					for(int j=0; j<fieldNodes.getLength(); j++){
						Node fieldNode = fieldNodes.item(j);
						NamedNodeMap fAttrs = fieldNode.getAttributes();
						Node fNameAttr = fAttrs.getNamedItem("name");
						Node sNameAttr = fAttrs.getNamedItem("target-field");
						System.out.println(sNameAttr);
						String s1=sNameAttr.getTextContent();
						String s2=fNameAttr.getTextContent();
						if(s1.isEmpty() &&  s2.equalsIgnoreCase(targetField))
							{
								Node tSheetAttr = fAttrs.getNamedItem("target-sheet");
								Node tFieldAttr = fAttrs.getNamedItem("target-field");
								
								tSheetAttr.setTextContent(targetSheet);
								tFieldAttr.setTextContent(targetField);
							logger.info("Updated source and target mappings for Source field {} on sheet {}", targetField, tSheetAttr);
								
								break;
						}
					}
					
					
					break;
				}
			}
			
			logger.info("Saving changes");
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			/*Added by paneendra for VAPT FIX starts*/
			transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
			/*Added by paneendra for VAPT FIX ends*/
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(portMapPath + "\\" + xmlFileName));
			transformer.transform(source, result);
			logger.info("Done");
		}catch(Exception e){
			logger.error("ERROR updating Port Mapping", e);
			throw new Exception("An internal error occurred while updating the mapping");
		}
	}
	
	
	
	/*Changes done by Pushpalatha for TENJINCG-266 ends*/
	public String getJSTreePortMapJson(int sAppId, int tAppId, String sFuncCode, String tFuncCode) {
		String finalString = "";
		
		String portMapPath = null;
		
		try{
			portMapPath = TenjinConfiguration.getProperty("PORT_MAP_LOCATION");
			logger.info("Port Map Path --> {}", portMapPath);
		}catch(TenjinConfigurationException e){
			logger.error("Could not find port map XML path from config file", e);
			return null;
		}
		
		String xmlFileName = sAppId + "_" + sFuncCode + "_" + tAppId + "_" + tFuncCode + ".xml";
		/*Changed by Pushpalatha for TENJINCG-584 starts*/
		logger.info("Locating port map file from {}", portMapPath + File.separator + xmlFileName);
		
		try {
			
			File xmlFile = new File(portMapPath + File.separator + xmlFileName);
			/*Changed by Pushpalatha for TENJINCG-584 starts*/
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilderFactory.newInstance("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl", ClassLoader.getSystemClassLoader());
			/*Added by paneendra for VAPT FIX starts*/
			dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			//dbFactory.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, "true");
			dbFactory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
			/*Added by paneendra for VAPT FIX ends*/
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			
			logger.info("Successfully parsed XML");
			
			doc.getDocumentElement().normalize();
			logger.info("Normalize document -> Done");
			
			logger.info("Getting all sheets");
			NodeList sheetNodes = doc.getElementsByTagName("sheet");
			logger.info("{} sheets loaded", sheetNodes.getLength());
			
			JSONArray jArray = new JSONArray();
			
			for(int i=0; i<sheetNodes.getLength(); i++){
				Node sheetNode = sheetNodes.item(i);
				
				if(sheetNode.getNodeType() == Node.ELEMENT_NODE){
					Element node = (Element) sheetNode;
					String sName = node.getAttribute("name");
					logger.info("Processing node for sheet {}", sName);
					String sheetId = "sheet_" + (i+1);
					boolean mapSuccess = true;
				
					
					
					JSONObject json = new JSONObject();
					json.put("id", sheetId);
					json.put("text", sName);
					JSONObject liAttrs = new JSONObject();
					liAttrs.put("nodetype", "sheet");
					liAttrs.put("nodeid", sheetId);
					json.put("li_attr", liAttrs);
					
					NodeList fieldNodes = node.getElementsByTagName("field");
					logger.debug("{} field nodes found under this sheet node", fieldNodes.getLength());
					JSONArray fieldsArray = new JSONArray();
					for(int j=0; j<fieldNodes.getLength(); j++){
						Node fieldNode = fieldNodes.item(j);
						
						if(fieldNode.getNodeType() == Node.ELEMENT_NODE){
							Element fNode = (Element) fieldNode;
							
							String fName = fNode.getAttribute("name");
							String tSheet = fNode.getAttribute("target-sheet");
							String tField = fNode.getAttribute("target-field");
							boolean mapStatus = true;
							if(trim(tSheet).equalsIgnoreCase("") || trim(tField).equalsIgnoreCase("")){
								mapSuccess = false;
								mapStatus = false;
								
								if(trim(tSheet).equalsIgnoreCase("")){
									tSheet = "-1";
								}
								
								if(trim(tField).equalsIgnoreCase("")){
									tField = "-1";
								}
							}
							
							
							
							logger.debug("Field [{}], Target Sheet [{}], Target Field [{}], Mapped [{}]", fName, tSheet, tField, mapStatus);
							
							JSONObject field = new JSONObject();
							String fieldId = sheetId + "_field_" + (j+1);
							field.put("id", fieldId);
							field.put("text", fName);
							
							JSONObject lAttrs = new JSONObject();
							lAttrs.put("nodetype", "field");
							lAttrs.put("nodeid", fieldId);
							lAttrs.put("mapped", mapStatus);
							lAttrs.put("targetsheet", tSheet);
							lAttrs.put("targetfield", tField);
							lAttrs.put("sourcesheet", sName);
							lAttrs.put("sourcefield", fName);
							
							field.put("li_attr", lAttrs);
							
							if(mapStatus){
								field.put("icon", "jstree/mapped.png");
							}else{
								field.put("icon", "jstree/unmapped.png");
							}
							
							fieldsArray.put(field);
						}
						
						
					}
					
					if(mapSuccess){
						json.put("icon", "jstree/sheetmapped.png");
					}else{
						json.put("icon", "jstree/sheetunmapped.png");
					}
					
					json.put("children", fieldsArray);
					jArray.put(json);
				}
			}
			
			finalString = jArray.toString();
			logger.info("Final JSON Generated below");
			logger.info(finalString);
		} catch (ParserConfigurationException e) {
			
			logger.error(e.getMessage());
		} catch (SAXException e) {
			
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			
			logger.error(e.getMessage(), e);
		} catch (JSONException e) {
			
			logger.error(e.getMessage(), e);
		}
		
		
		
		return finalString;
	}
	
	private static String trim(String str){
		
		if(str == null)
			return "";
		return str.replace(String.valueOf((char) 160), " ").trim();
	}
	
	/*Changes done by Pushpalatha for TENJINCG-266 starts*/
	

public  Map<String, List<String>> getNotMappedSourceFields2(int sAppId, int tAppId, String sFuncCode, String tFuncCode,String sheetName) {
	
	Map<String, List<String>> addedfields=new HashMap<String, List<String>>();
	
	String portMapPath = null;
	try{
		portMapPath = TenjinConfiguration.getProperty("PORT_MAP_LOCATION");
		logger.info("Port Map Path --> {}", portMapPath);
	}catch(TenjinConfigurationException e){
		logger.error("Could not find port map XML path from config file", e);
		return null;
	}
	
	String xmlFileName = sAppId + "_" + sFuncCode + "_" + tAppId + "_" + tFuncCode + ".xml";
	try {
		File xmlFile = new File(portMapPath + "\\" + xmlFileName);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		/*Added by paneendra for VAPT FIX starts*/
		DocumentBuilderFactory.newInstance("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl", ClassLoader.getSystemClassLoader());
		dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		//dbFactory.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, "true");
		dbFactory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
		/*Added by paneendra for VAPT FIX ends*/
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(xmlFile);
		
		logger.info("Successfully parsed XML");
		
		doc.getDocumentElement().normalize();
		logger.info("Normalize document -> Done");
		
		logger.info("Getting all sheets");
		NodeList sheetNodes = doc.getElementsByTagName("sheet");
		logger.info("{} sheets loaded", sheetNodes.getLength());
		
		
		List<String> unmapped=new  ArrayList<String>();
		
		for(int i=0; i<sheetNodes.getLength(); i++){
			Node sheetNode = sheetNodes.item(i);
			
			
			if(sheetNode.getNodeType() == Node.ELEMENT_NODE){
				Element node = (Element) sheetNode;
				String sName = node.getAttribute("name");
				logger.info("Processing node for sheet {}", sName);
				List<String> tfList1=new  ArrayList<String>();
				List<String> sfList1=new  ArrayList<String>();
				
				if(sheetName.equalsIgnoreCase(sName)){
				
				NodeList fieldNodes = node.getElementsByTagName("field");
				logger.debug("{} field nodes found under this sheet node", fieldNodes.getLength());
				
				
				for(int j=0; j<fieldNodes.getLength(); j++){
					Node fieldNode = fieldNodes.item(j);
					
					if(fieldNode.getNodeType() == Node.ELEMENT_NODE){
						Element fNode = (Element) fieldNode;
						
						String fName = fNode.getAttribute("name");							
						String tSheet = fNode.getAttribute("target-sheet");
						String tField = fNode.getAttribute("target-field");
						System.out.println(fName +"and"+tSheet+"and"+tField);
						 
						List<String> tfList=new  ArrayList<String>();
						List<String> sfList=new  ArrayList<String>();
						tfList.add(tField);
						sfList.add(fName);
						tfList1.addAll(tfList);
						sfList1.addAll(sfList);
					}
					System.out.println(tfList1);
					System.out.println(sfList1);
				}
				
				for(int k=0; k<fieldNodes.getLength(); k++){
					if(!sfList1.get(k).equalsIgnoreCase(tfList1.get(k))){
						List<String> List=new  ArrayList<String>();
						String sf=sfList1.get(k);
						/*Changed by pushpalatha for defect#TENJINCG-277 starts*/
						if(sf.toLowerCase().contains("button")||sf.toLowerCase().contains("link"))
						/*Changed by pushpalatha for defect#TENJINCG-277 starts*/
						List.add(sf);
						unmapped.addAll(List);
						
						
				}
					
			}
		
			
			}//sheet comparing ends
				
			}
		
		
			addedfields.put(sheetName, unmapped);
		}
		logger.info("Final JSON Generated below");
		logger.info("added fields"+addedfields);
	} catch (ParserConfigurationException e) {
		
		logger.error(e.getMessage());
	} catch (SAXException e) {
		
		logger.error(e.getMessage(), e);
	} catch (IOException e) {
		
		logger.error(e.getMessage(), e);
	} 
	
	return addedfields;

	}

public  String getJSTreePortMapJsonNotMapped(int sAppId, int tAppId, String sFuncCode, String tFuncCode) {
	String notMappedSourceFields = "";
	
	String portMapPath = null;
	
	try{
		portMapPath = TenjinConfiguration.getProperty("PORT_MAP_LOCATION");
		logger.info("Port Map Path --> {}", portMapPath);
	}catch(TenjinConfigurationException e){
		logger.error("Could not find port map XML path from config file", e);
		return null;
	}
	
	String xmlFileName = sAppId + "_" + sFuncCode + "_" + tAppId + "_" + tFuncCode + ".xml";
	logger.info("Locating port map file from {}", portMapPath + "\\" + xmlFileName);
	
	try {
		File xmlFile = new File(portMapPath + "\\" + xmlFileName);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		/*Added by paneendra for VAPT FIX starts*/
		DocumentBuilderFactory.newInstance("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl", ClassLoader.getSystemClassLoader());
		dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		//dbFactory.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, "true");
		dbFactory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
		/*Added by paneendra for VAPT FIX ends*/
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(xmlFile);
		
		logger.info("Successfully parsed XML");
		
		doc.getDocumentElement().normalize();
		logger.info("Normalize document -> Done");
		
		logger.info("Getting all sheets");
		NodeList sheetNodes = doc.getElementsByTagName("sheet");
		logger.info("{} sheets loaded", sheetNodes.getLength());
		
		JSONArray jArray = new JSONArray();
		
		for(int i=0; i<sheetNodes.getLength(); i++){
			Node sheetNode = sheetNodes.item(i);
			
			if(sheetNode.getNodeType() == Node.ELEMENT_NODE){
				Element node = (Element) sheetNode;
				String sName = node.getAttribute("name");
				logger.info("Processing node for sheet {}", sName);
				String sheetId = "sheet_" + (i+1);
				
				JSONObject json = new JSONObject();
				json.put("id", sheetId);
				json.put("text", sName);
				JSONObject liAttrs = new JSONObject();
				liAttrs.put("nodetype", "sheet");
				liAttrs.put("nodeid", sheetId);
				json.put("li_attr", liAttrs);
				
				NodeList fieldNodes = node.getElementsByTagName("field");
				logger.debug("{} field nodes found under this sheet node", fieldNodes.getLength());
				JSONArray fieldsArray = new JSONArray();
				for(int j=0; j<fieldNodes.getLength(); j++){
					Node fieldNode = fieldNodes.item(j);
					
					if(fieldNode.getNodeType() == Node.ELEMENT_NODE){
						Element fNode = (Element) fieldNode;
						
						String fName = fNode.getAttribute("name");
						String tSheet = fNode.getAttribute("target-sheet");
						String tField = fNode.getAttribute("target-field");
						System.out.println(fName +"and"+tSheet+"and"+tField);
						
						boolean mapStatus =false;
						
						JSONObject notmappedfield = new JSONObject();
						
						
						
						if(tField.isEmpty()){
							String s=tField.replace(tField, fName);
							
							JSONObject lAttrs2 = new JSONObject();
							lAttrs2.put("targetfield", s);
							notmappedfield.put("li_attr", lAttrs2);
							logger.debug("Field [{}], Target Sheet [{}], Target Field [{}], Mapped [{}]", fName, tSheet, tField, mapStatus);
							
						}
						System.out.println(notmappedfield);
						fieldsArray.put(notmappedfield);
					}
					
					
				}
				
			
				json.put("children", fieldsArray);
				jArray.put(json);
				System.out.println(jArray);
			}
		}
		
		notMappedSourceFields = jArray.toString();
		logger.info("Final JSON Generated below");
		logger.info(notMappedSourceFields);
	} catch (ParserConfigurationException e) {
		
		logger.error(e.getMessage());
	} catch (SAXException e) {
		
		logger.error(e.getMessage(), e);
	} catch (IOException e) {
		
		logger.error(e.getMessage(), e);
	} catch (JSONException e) {
		
		logger.error(e.getMessage(), e);
	}
	
	
	
	return notMappedSourceFields;
}


	
	public Map<String, List<PortMap>> parseXml(File file) {
		
		
		Map<String, List<PortMap>> map = new LinkedHashMap<String, List<PortMap>>();

		List<PortMap> objList = null;

		Document document = null;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		/*Added by paneendra for VAPT FIX starts*/
		DocumentBuilderFactory.newInstance("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl", ClassLoader.getSystemClassLoader());
		dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		//dbf.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, "true");
		try {
			dbf.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
		} catch (ParserConfigurationException e1) {
            logger.error("Error in parsing xml"+e1);
		}
		/*Added by paneendra for VAPT FIX ends*/
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			document = db.parse(file);
			document.getDocumentElement().normalize();
			System.out.println("Root element :"
					+ document.getDocumentElement().getNodeName());
			
			NodeList sheetList = document.getElementsByTagName("Sheets").item(0).getChildNodes();

			
				int sheetNum = sheetList.getLength();


			for (int temp = 0; temp < sheetNum; temp++) {

				/*Added by leelaprasad for the requirement bug number 737 on 05-11-2016 starts*/
				/*Node node = list.item(temp);*/
				 Node node = sheetList.item(temp);
				 /*Added by leelaprasad for the requirement bug number 737 on 05-11-2016 ends*/

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element sheet = (Element) node;

					String str1 = sheet.getAttribute("name");
					Element fields = (Element) sheet.getFirstChild();
					NodeList fieldList = fields.getElementsByTagName("field");

					int field_num = fieldList.getLength();

					objList = new ArrayList<PortMap>();
					for (int i = 0; i < field_num; i++) {
						PortMap portMap = new PortMap();

						Node fnode = fieldList.item(i);

						if (node.getNodeType() == Node.ELEMENT_NODE) {
							Element field = (Element) fnode;

							portMap.setSourceField(field.getAttribute("name"));
							portMap.setTargetSheet(field
									.getAttribute("target-sheet"));
							portMap.setTargetField(field
									.getAttribute("target-field"));
							objList.add(portMap);
						}

					}
					map.put(str1, objList);

				}

			}
		}

		catch (Exception e) {
			
			logger.error("Error ", e);
		}
		return map;

	}
	/*Changed by Leelaprasad for the requirement Defect fix TEN-35 on 13-12-2016 starts*/
	public Boolean  validatXML(File file){

		Document document = null;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		/*Added by paneendra for VAPT FIX starts*/
		DocumentBuilderFactory.newInstance("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl", ClassLoader.getSystemClassLoader());
		dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		//dbf.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, "true");
		try {
			dbf.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
		} catch (ParserConfigurationException e1) {
			logger.error("Error in parsing xml"+e1);
		}
		/*Added by paneendra for VAPT FIX ends*/
		DocumentBuilder db;
		  Boolean flag=false;
		try {
			db = dbf.newDocumentBuilder();
			document = db.parse(file);
			document.getDocumentElement().normalize();
			System.out.println("Root element :"
					+ document.getDocumentElement().getNodeName());
			
			NodeList sheetList = document.getElementsByTagName("Sheets").item(0).getChildNodes();

			
				int sheetNum = sheetList.getLength();

            int count=0;
          
			for (int temp = 0; temp < sheetNum; temp++) {

				
				 Node node = sheetList.item(temp);
				
   
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element sheet = (Element) node;

					Element fields = (Element) sheet.getFirstChild();
					NodeList fieldList = fields.getElementsByTagName("field");

					int field_num = fieldList.getLength();

					for (int i = 0; i < field_num; i++) {
						

						Node fnode = fieldList.item(i);

						if (node.getNodeType() == Node.ELEMENT_NODE) {
							Element field = (Element) fnode;

						field.getAttribute("name");
					 if(field.getAttribute("target-sheet").equalsIgnoreCase("")){
						
					 }else{
						 if( field.getAttribute("target-field").equalsIgnoreCase("")){
							
						 }else{
							 count++;
						 }
						
					 }
						
						}
					}
				}
			}
			if(count>0){
				flag=true;
			}
		}
			catch (Exception e) {
				
				logger.error("Error ", e);
			}
		return flag;
		
	}
	/*Changed by Leelaprasad for the requirement Defect fix TEN-35 on 13-12-2016 ends*/
}
