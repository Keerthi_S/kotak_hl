/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SchedulerHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 01-Dec-2016			Sahana					Req#TJN_243_03
 * 22-Dec-2016          Sahana				defect #TEN-152(In scheduler,Execution goes to scheduled task to aborted task )
 *  27-Jan-2017			Sahana					TENJINCG-61(A Tenjin user should be able to specify Screenshot Option while Scheduling an Execution task)
 *  01-Feb-2017			Sahana                  TENJINCG-19(User should be able to abort / cancel a Recurrent Scheduled Task)
 *  02-Feb-2017			Sahana					[Daily]To get next recursive date's for any start date
 *  29-08-2017          Leelaprasad            defcet T25IT-299
 *  31-08-2017        Leelaprasad                T25IT-393
 *  21-09-2017          Gangadhar Badagi		TENJINCG-348
 *  31-12-2018          Padmavathi              TJN262R2-67
 *  24-06-2019          Padmavathi              for license 
 */


/*
 * Added file by nagababu for Req#TJN_24_03 on 16/10/2016
 */
package com.ycs.tenjin.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.BridgeProcessor;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.SchedulerHelper;
import com.ycs.tenjin.run.ExecutorGateway;
import com.ycs.tenjin.run.ExtractorGateway;
import com.ycs.tenjin.run.LearnerGateway;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;


public class SchedulerHandler {
	private static final Logger logger = LoggerFactory.getLogger(SchedulerHandler.class);   
	public void initiateschedulerTask(Scheduler sch){
		/*Changed by Gangadhar for the requirement story TENJINCG-348 starts*/
		SchedulerHelper	insertAbortMessage=new SchedulerHelper();
		/*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
		try{

			logger.info("Initializing Task [{}], Task Type [{}]", sch.getSchedule_Id(), sch.getAction());

			/*************************************************
			 * Added by Sriram for TENJINCG-169 (Schedule API Learning)
			 */
			if(sch.getAction().equalsIgnoreCase("LearnApi")){
				SchedulerHelper schHelper=new SchedulerHelper();
				LearnerGateway gateway=new LearnerGateway();
				new SchedulerHelper().updateScheduleStatus(BridgeProcess.IN_PROGRESS,sch.getSchedule_Id());
				gateway.loadApiRun(sch.getRun_id(), sch.getApiLearnType());
				if(sch.getSchRecur().equalsIgnoreCase("Y"))
				{
					int runId=schHelper.getTestRunId();
					new SchedulerHandler().createCopyOfScheduledTask(sch.getSchedule_Id(),runId,gateway.getManifest().getAut().getBrowser(),sch.getAutLoginTYpe());
				}
				/*Added by Padmavathi for license starts*/
				gateway.validateAdapterLicense();
				/*Added by Padmavathi for license ends*/
				BridgeProcessor handler = new BridgeProcessor(gateway.getManifest());
				handler.createTask();
				handler.runTask();
				
			}
			/*************************************************
			 * Added by Sriram for TENJINCG-169 (Schedule API Learning) ends
			 */
			else if(sch.getAction().equalsIgnoreCase("Learn")){
				SchedulerHelper schHelper=new SchedulerHelper();
				boolean flag=schHelper.checkExecutingClientStatus(sch.getReg_client());
				if(flag==false){
					LearnerGateway gateway=new LearnerGateway();
					new SchedulerHelper().updateScheduleStatus(BridgeProcess.IN_PROGRESS,sch.getSchedule_Id());
					gateway.loadRun(sch.getRun_id(), sch.getReg_client());
					/*changed by sahana for Req#TJN_243_03: Starts*/
					//recursive scheduler logic
					if(sch.getSchRecur().equalsIgnoreCase("Y"))
					{
						int runId=schHelper.getTestRunId();
						new SchedulerHandler().createCopyOfScheduledTask(sch.getSchedule_Id(),runId,gateway.getManifest().getAut().getBrowser(),sch.getAutLoginTYpe());
					}
					/*changed by sahana for Req#TJN_243_03: ends*/
					/*Added by Padmavathi for license starts*/
					gateway.validateAdapterLicense();
					/*Added by Padmavathi for license ends*/
					BridgeProcessor handler = new BridgeProcessor(gateway.getManifest());
					handler.createTask();
					handler.runTask(); 
				}
				else{
					try {
						new SchedulerHelper().updateScheduleStatus("Aborted",sch.getSchedule_Id());
						/*Changed by Gangadhar for the requirement story TENJINCG-348 starts*/
						insertAbortMessage.insertAbortedReason("Some tasks are in executing state in the client", sch.getSchedule_Id());
					   /*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
					} catch (DatabaseException e1) {
						/*Changed by Gangadhar for the requirement story TENJINCG-348 starts*/
						insertAbortMessage.insertAbortedReason(e1.getMessage(), sch.getSchedule_Id());
						/*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
						logger.error("Scheduler Task aborted due to "+e1.getMessage());
						logger.error("Error ", e1);
					}
				}
			}
			else if(sch.getAction().equalsIgnoreCase("Execute")){
				SchedulerHelper schHelper=new SchedulerHelper();
				boolean flag=schHelper.checkExecutingClientStatus(sch.getReg_client());
				/*Changed by Leelaparasad for defect T25IT-299 starts*/
				/*if(flag==false){*/
				if(flag==false ||sch.getReg_client().equalsIgnoreCase("NA") ){
				/*Changed by Leelaparasad for defect T25IT-299 ends*/
					ExecutorGateway gateway=new ExecutorGateway();
					new SchedulerHelper().updateScheduleStatus(BridgeProcess.IN_PROGRESS,sch.getSchedule_Id());
					/*changed by sahana for improvement #TENJINCG-61: Starts*/
					gateway.loadRun(sch.getRun_id(), sch.getProjectId(), sch.getCreated_by(), sch.getReg_client(), 0, "N",sch.getScreenShotOption(), sch.getBrowserType());
					/*changed by sahana for Req#TJN_243_03: Starts*/
					/*changed by sahana for improvement #TENJINCG-61: ENDS*/
					//recursive scheduler logic
					if(sch.getSchRecur().equalsIgnoreCase("Y"))
					{
						int runId=schHelper.getTestRunId();
						/*changed by sahana for defect #TEN_152: Starts*/
						new SchedulerHandler().createCopyOfScheduledTask(sch.getSchedule_Id(),runId,sch.getBrowserType(),"");
					/*changed by sahana for defect #TEN_152: ends*/					}
					
					
					/*Added by Padmavathi for TJN262R2-67 starts*/
					if(gateway.getTaskManifest().getProjectId()==0){
						gateway.getTaskManifest().setProjectId(sch.getProjectId());	
					}
					/*Added by Padmavathi for TJN262R2-67 ends*/
					/*Added by Padmavathi for license starts*/
					Set<String> al =null;
					String msg = "";
					try {
						Map<String, String> adapterLicenseMap = gateway.validateAdapterCredentials(al);
						if (adapterLicenseMap != null && adapterLicenseMap.size() > 0) {
							msg = "";
							for (String key : adapterLicenseMap.keySet()) {
								msg = msg + key + " - " + adapterLicenseMap.get(key) + "\n";
							}
							logger.error("Adapter license Validation failed\n [{}]", msg);
							throw new TenjinServletException(msg);
						}
					} catch (TenjinConfigurationException e) {
						throw new TenjinServletException(e.getMessage());
					}
					/*Added by Padmavathi for license ends*/
					BridgeProcessor handler = new BridgeProcessor(gateway.getTaskManifest());
					handler.createTask();
					handler.runTask();
				}
				else{
					try {
						new SchedulerHelper().updateScheduleStatus("Aborted",sch.getSchedule_Id());
						/*Changed by Gangadhar for the requirement story TENJINCG-348 starts*/
						insertAbortMessage.insertAbortedReason("Some tasks are in executing state in client", sch.getSchedule_Id());
					   /*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
					} catch (DatabaseException e1) {
						/*Changed by Gangadhar for the requirement story TENJINCG-348 starts*/
						insertAbortMessage.insertAbortedReason(e1.getMessage(), sch.getSchedule_Id());
						/*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
						logger.error("Scheduler Task aborted due to "+e1.getMessage());
						logger.error("Error ", e1);
					}
				}
			}
			else{
				SchedulerHelper schHelper=new SchedulerHelper();

				logger.debug("Checking if the client [{}] is busy or idle", sch.getReg_client());
				boolean flag=schHelper.checkExecutingClientStatus(sch.getReg_client());
				logger.debug("Client in use --> [{}]", flag);
				if(flag==false){
					ExtractorGateway gateway=new ExtractorGateway();

					logger.debug("Changing schedule task to In Progress");
					new SchedulerHelper().updateScheduleStatus(BridgeProcess.IN_PROGRESS,sch.getSchedule_Id());

					logger.debug("Loading Run [{}]", sch.getRun_id());
					gateway.loadRun(sch.getRun_id(), sch.getReg_client(),sch.getBrowserType());
					/*changed by sahana for Req#TJN_243_03: Starts*/
					//recursive scheduler logic
					if(sch.getSchRecur().equalsIgnoreCase("Y"))
					{
						logger.debug("Creating Recurring schedule task");
						int runId=schHelper.getTestRunId();
						new SchedulerHandler().createCopyOfScheduledTask(sch.getSchedule_Id(),runId,gateway.getManifest().getAut().getBrowser(),gateway.getManifest().getAut().getLoginUserType());
						logger.debug("Recurring task created for task [{}]", sch.getSchedule_Id());
					}
					/*changed by sahana for Req#TJN_243_03: ends*/
					/*new SchedulerHelper().updateScheduleStatus(AdapterTask.IN_PROGRESS,sch.getSchedule_Id());*/
					/*Added by Padmavathi for license starts*/
					gateway.validateAdapterLicense();
					/*Added by Padmavathi for license ends*/
					logger.debug("Starting adapter task for Schedule ID [{}]", sch.getSchedule_Id());
					BridgeProcessor handler = new BridgeProcessor(gateway.getManifest());
					handler.createTask();

					logger.debug("Running Scheduled Task");
					handler.runTask();
				}
				else{

					logger.debug("Aborting Schedule task [{}]", sch.getSchedule_Id());
					try {
						new SchedulerHelper().updateScheduleStatus("Aborted",sch.getSchedule_Id());
						/*Changed by Gangadhar for the requirement story TENJINCG-348 starts*/
						insertAbortMessage.insertAbortedReason("Some tasks are in executing state in client", sch.getSchedule_Id());
					    /*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
					} catch (DatabaseException e1) {
						logger.error("Scheduler Task aborted due to "+e1.getMessage());
						/*Changed by Gangadhar for the requirement story TENJINCG-348 starts*/
						insertAbortMessage.insertAbortedReason(e1.getMessage(), sch.getSchedule_Id());
						/*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
						logger.error("Error ", e1);
					}
				}

			}
			//new SchedulerHelper().updateScheduleStatus("Completed",sch.getSchedule_Id());
		}
		catch(TenjinServletException e){
			try {
				new SchedulerHelper().updateScheduleStatus("Aborted",sch.getSchedule_Id());
				logger.error("Scheduler Task aborted due to "+e.getMessage());
				/*Changed by Gangadhar for the requirement story TENJINCG-348 starts*/
				insertAbortMessage.insertAbortedReason(e.getMessage(), sch.getSchedule_Id());
				/*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
			} catch (DatabaseException e1) {
				
				logger.error("Error ", e1);
				logger.error("Scheduler Task aborted due to "+e.getMessage());
				/*Changed by Gangadhar for the requirement story TENJINCG-348 starts*/
				try {
					insertAbortMessage.insertAbortedReason(e1.getMessage(), sch.getSchedule_Id());
				} catch (DatabaseException e2) {
					
					e2.printStackTrace();
				}
				/*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
			}
			logger.error("Scheduler Task aborted due to "+e.getMessage());
			//logger.error("Error ", e);
		}catch(Exception e){
			try {
				new SchedulerHelper().updateScheduleStatus("Aborted",sch.getSchedule_Id());
				logger.error("Scheduler Task aborted due to "+e.getMessage());
				/*Changed by Gangadhar for the requirement story TENJINCG-348 starts*/
				insertAbortMessage.insertAbortedReason(e.getMessage(), sch.getSchedule_Id());
				/*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
			} catch (DatabaseException e1) {
				
				logger.error("Error ", e1);
				logger.error("Scheduler Task aborted due to "+e.getMessage());
				/*Changed by Gangadhar for the requirement story TENJINCG-348 starts*/
				try {
					insertAbortMessage.insertAbortedReason(e1.getMessage(), sch.getSchedule_Id());
				} catch (DatabaseException e2) {
					
					e2.printStackTrace();
				}
				/*Changed by Gangadhar for the requirement story TENJINCG-348 ends*/
			}
			logger.error("Scheduler Task aborted due to "+e.getMessage());
			//logger.error("Error ", e);
		}
	}
	/*changed by sahana for Req#TJN_243_03: Starts*/
	public void createCopyOfScheduledTask(int schId,int runId,String browserType,String autLoginType) throws DatabaseException{
		SchedulerHelper schHelper=new SchedulerHelper();
		Scheduler schedule=schHelper.copyOfSchedulerTask(schId);
		Scheduler copyTask=null;
		if(schedule!=null){
			copyTask=new Scheduler();
			copyTask.setSch_time(schedule.getSch_time());
			copyTask.setAction(schedule.getAction());
			copyTask.setReg_client(schedule.getReg_client());
			copyTask.setStatus("Scheduled");
			copyTask.setCreated_by(schedule.getCreated_by());
			copyTask.setBrowserType(browserType);
			copyTask.setAutLoginTYpe(autLoginType);
			copyTask.setEndDate(schedule.getEndDate());
			copyTask.setCreated_on(new SimpleDateFormat("dd-MMM-yyyy").format(new Date()));
			copyTask.setRun_id(runId);
			copyTask.setSchRecur("Y");
			copyTask.setProjectId(schedule.getProjectId());
			copyTask.setFrequency(schedule.getFrequency());
			copyTask.setTaskName(schedule.getTaskName());
			/*changed by sahana for improvement #TENJINCG-61: Starts*/
			copyTask.setScreenShotOption(schedule.getScreenShotOption());
			/*changed by sahana for improvement #TENJINCG-61: ends*/
			String startDate=null;
			String startTime=null;
			boolean flag=false;
			if(schedule.getFrequency().equalsIgnoreCase("daily")){
				if(schedule.getRecurDays().equalsIgnoreCase("Every day")){
					copyTask.setRecurCycles(schedule.getRecurCycles());
					copyTask.setRecurDays(schedule.getRecurDays());
					/*changed by sahana to get next recursive date's for any start date:starts*/	
					//startDate=getNextRecursiveDate(copyTask.getRecurCycles(),copyTask.getEndDate());
					startDate=getNextRecursiveDate(copyTask.getRecurCycles(),copyTask.getEndDate(),schedule.getSch_date());
					/*changed by sahana to get next recursive date's for any start date:ends*/	
					if(startDate!=null && (!startDate.equalsIgnoreCase("Completed")))
						copyTask.setSch_date(startDate);
					else
						flag=true;
				}
			}
			else{
				copyTask.setRecurCycles(schedule.getRecurCycles());
				copyTask.setRecurDays(schedule.getRecurDays());

				try {
					if(schedule.getFrequency().equalsIgnoreCase("Weekly"))
						startDate=getWeekDayDate(schedule.getSch_time(),schedule.getFrequency(),schedule.getEndDate(),schedule.getRecurDays(),schedule.getSch_date(),copyTask.getRecurCycles(),"copy");
					else{
						startTime=getRecurHours(schedule.getSch_date(),schedule.getSch_time(),schedule.getRecurCycles());
						startDate=schedule.getSch_date();
						copyTask.setSch_time(startTime);
					}
					if(startDate.equalsIgnoreCase("completed"))
					{
						schHelper.recurUpdateStatus(copyTask.getSchedule_Id());
						flag=true;
					}

				}catch (Exception e) {
					logger.error("Could not create copy of scheduler task due to "+e.getMessage());
				}
				if(startDate!=null )
					copyTask.setSch_date(startDate);
				else{
					flag=true;
				}

			}
			if(flag==false)
				schHelper.persistSchedule(copyTask);
		}
	}
	/*changed by sahana to get next recursive date's for any start date:starts*/	
	public String getNextRecursiveDate(int cycles,String endDate,String strtDate)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date date=new Date();
		String startDate=null;
		try {
			boolean flag=false;
			Date enddate = sdf.parse(endDate);
			Date stDate = sdf.parse(strtDate);
			long diff1=stDate.getTime() - date.getTime();
			long diff = enddate.getTime() - stDate.getTime();
			long days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			days=days+1;
			/*changed by sahana to get next recursive date's for any start date:starts*/
			boolean sameDates=compareDates(stDate,date);
			if(sameDates==false && flag==false){
				long days1=TimeUnit.DAYS.convert(diff1, TimeUnit.MILLISECONDS)+1;
				if(days1>=1)
				{
					cycles+=(int)days1;
					days1=0;
				}
				flag=true;
			}
			/*changed by sahana to get next recursive date's for any start date:ends*/
			if(days>=cycles){
				Calendar c = Calendar.getInstance();    
				c.add(Calendar.DATE, cycles);
				startDate=sdf.format(c.getTime());
			}
			if(diff==0)
				startDate="Completed";
		} catch (ParseException e) {

			logger.error("Could not find the number of days between start and end date of Task "+e.getMessage());
		}
		return startDate;
	}
	public boolean compareDates(Date stDate, Date date) {
		String sd=stDate.toString();
		String d=date.toString();
		String x=sd.substring(0, 10) + sd.substring(23,28);
		String y=d.substring(0, 10) + sd.substring(23,28);
		if(x.equalsIgnoreCase(y))
			return true;
		else
			return false;
	}
	/*changed by sahana to get next recursive date's for any start date: ends*/	
	public String getWeekDayDate(String dateTime,String frequency,String endDate,String recurDays,String start,int cycles,String occurance) throws Exception{
		String startDate=null;
		String weekDays[]={"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
		String days[]=recurDays.split(",");

		String curDay=new SimpleDateFormat("EEEE").format(new Date());

		int toDayindex=Arrays.asList(weekDays).indexOf(curDay);
		List<Integer> selDayIndexes=new ArrayList<Integer>();
		int countDays=0;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date strtDate = sdf.parse(start);
		Date endDt=sdf.parse(endDate);
		Date curDate=new Date();
		Calendar cal = Calendar.getInstance();
		boolean flag=false;
		for(int i=0;i<days.length;i++)
		{
			selDayIndexes.add(Arrays.asList(weekDays).indexOf(days[i]));
		}
		for(int i=0;i<selDayIndexes.size();i++){
			if(strtDate.after(curDate) || occurance.equalsIgnoreCase("copy"))
			{//start date is greater than today s date
				sdf.applyPattern("EEEE");
				String dayName = sdf.format(strtDate);
				int diffInDays = 1+(int) ((strtDate.getTime() - curDate.getTime()) / (1000 * 60 * 60 * 24));
				List<String> dates=new ArrayList<String>();
				for(int j=0;j<7;j++){
					if(j==0){
						cal.add(Calendar.DATE, diffInDays);
						dates.add(new SimpleDateFormat("dd-MMM-yyyy").format(cal.getTime()));

					}
					else{
						cal.add(Calendar.DATE, 1);
						dates.add(new SimpleDateFormat("dd-MMM-yyyy").format(cal.getTime()));
					}
				}
				String nextDay=new SchedulerHandler().recursiveDay(days, curDay);
				for(int k=0;k<dates.size();k++){
					if(new SimpleDateFormat("EEEE").format(new Date(dates.get(k))).equalsIgnoreCase(nextDay)){
						startDate=dates.get(k);
						flag=true;
						break;
					}
				}
				if(flag==true)
					break;
			}
			else{
				if(toDayindex==selDayIndexes.get(i))
				{//when current day is selected, check for hr & min validations
					countDays=0;
					String arr[]=dateTime.split(":");
					int hours = cal.get(Calendar.HOUR_OF_DAY);
					int minutes=cal.get(Calendar.MINUTE);
					if(hours<=Integer.parseInt(arr[0]) && minutes<=Integer.parseInt(arr[1])){
						countDays=0;
						flag=true;
					}
					else
					{
						countDays=selDayIndexes.get(i+1)-toDayindex;

						flag=true;
					}
				}
				else if(selDayIndexes.get(i)>toDayindex)
				{
					countDays=selDayIndexes.get(i)-toDayindex;
					flag=true;
				}

			}
			if(flag==true){
				cal.add(Calendar.DATE, countDays);
				startDate=new SimpleDateFormat("dd-MMM-yyyy").format(cal.getTime());
				break;
			}
		}
		/*changed by Leelaprasad for T25IT-393 starts*/
		if(startDate==null){
			startDate=start;
		}
		/*changed by Leelaprasad for T25IT-393 ends*/
		//if start date crosses the end date, stop the scheduler recurrence 
		if(new SimpleDateFormat("dd-MMM-yyyy").parse(startDate).after(endDt)){
			//stop the scheduler recurrence by changing the recurrence value to "N"
			startDate="completed";
		}
		return startDate;
	}

	public String checkstartDate(String startDate,String startTime,int cycles){
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MMM-yyyy");
		if(startDate.equals(sdf.format(new Date()))){
			//check for time
			String arr[]=startTime.split(":");
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());  
			int hours = cal.get(Calendar.HOUR_OF_DAY);
			int minutes=cal.get(Calendar.MINUTE);
			if(!(hours<=Integer.parseInt(arr[0]) && minutes<=Integer.parseInt(arr[1]))){
				if(cycles==0){
					startDate=null;
				}
				else
				{
					cal.add(Calendar.DATE,cycles);				
					startDate=new SimpleDateFormat("dd-MMM-yyyy").format(cal.getTime());
				}
			}
		}
		else{
			startDate=null;
		}
		return startDate;
	}
	public String getRecurHours(String startDate,String startTime,int cycles)
	{
		String arr[]=startTime.split(":");
		int hour=Integer.parseInt(arr[0]);
		if((hour+cycles)<=23){
			startTime=(hour+cycles)+":"+arr[1];
		}
		else
			startTime="Completed";

		return startTime;

	}

	
	/*changed by sahana for Req#TJN_243_03: ends*/

	public String recursiveDay(String[] days,String day){
		List<String> weekdays=new ArrayList<String>();
		for(int i=0;i<days.length;i++){
			weekdays.add(days[i]);
		}
		int index=weekdays.indexOf(day);
		List<String> daysAfter=new ArrayList<String>();
		List<String> daysBefore=new ArrayList<String>();
		for(int i=index+1;i<weekdays.size();i++){
			daysAfter.add(weekdays.get(i));
		}
		for(int i=0;i<index;i++){
			daysBefore.add(weekdays.get(i));
		}
		List<String> remainingDays=new ArrayList<String>();
		remainingDays.addAll(daysAfter);
		remainingDays.addAll(daysBefore);
		remainingDays.add(day);
		return remainingDays.get(0);
	}
	/*changed by sahana for improvement #TENJINCG-19: Starts*/
	public void cancelRecTask(List<Integer> schIds) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			PreparedStatement pst1=null;
			//conn.setAutoCommit(false);
			for(int j=0;j<schIds.size();j++)
			{
				pst1=conn.prepareStatement("SELECT SCH_ID,ACTION,BROWSERTYPE,AUTLOGINTYPE,SCH_RECURRING FROM TJN_SCHEDULER WHERE SCH_ID=?");
				pst1.setInt(1, schIds.get(j));
				ResultSet rs=pst1.executeQuery();
				while(rs.next()){
					if(rs.getString("SCH_RECURRING").equalsIgnoreCase("Y"))
					{
						int runId=new SchedulerHelper().getTestRunId();
						if(rs.getString("ACTION").equalsIgnoreCase("Learn"))
							createCopyOfScheduledTask(schIds.get(j),runId,rs.getString("BROWSERTYPE"),rs.getString("AUTLOGINTYPE"));
						else if(rs.getString("ACTION").equalsIgnoreCase("Execute"))
							createCopyOfScheduledTask(schIds.get(j),runId,rs.getString("BROWSERTYPE"),"");
						else
							createCopyOfScheduledTask(schIds.get(j),runId,rs.getString("BROWSERTYPE"),rs.getString("AUTLOGINTYPE"));
					}
				}		
			}
			pst1.close();
			//conn.commit();

		}catch(Exception e){
			logger.error("Could not fetch details due to "+e.getMessage());
			throw new DatabaseException("Could not fetch details due to "+e.getMessage());
		}finally {
			try {
				conn.close();
			} catch (Exception e) {
				logger.error("Connection error due to "+e.getMessage());
			}
		}

	}
	/*changed by sahana for improvement #TENJINCG-19: ends*/
}
