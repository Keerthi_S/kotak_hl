/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TenjinScheduler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 02-05-2019			Roshni				   TENJINCG-1046
* 06-05-2019            Padmavathi              TENJINCG-1050
* 
*/


/*
 * Added file by nagababu for Req#TJN_24_03 on 16/10/2016
 */
package com.ycs.tenjin.scheduler;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TreeMap;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.SchedulerHelper;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.handler.ProjectMailHandler;
import com.ycs.tenjin.handler.TenjinMailHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.user.User;

public class TenjinScheduler{
    static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	static Timer timer = new Timer() ;
    private static final Logger logger = LoggerFactory.getLogger(TenjinScheduler.class);   

	public void executeScheduler(){
		logger.info("Checking for the schedules list");
		try{
			Map<Integer,Scheduler> scheduler_details = new TreeMap<Integer,Scheduler>();
			logger.info("Fetching schedules list");
			scheduler_details=new SchedulerHelper().fetchSchedulerRunDetails();
			int schedule_Id=0;
			Scheduler sh=new Scheduler();
			if(scheduler_details.size()>0){
				logger.info("initializing schedule");
				for (Map.Entry<Integer, Scheduler> entry : scheduler_details.entrySet())
				{
				/* Added by Roshni for TENJINCG-1046 starts */
					logger.info("sending mail from TenjinSchedular");
					this.sendMail(entry.getValue());
				/* Added by Roshni for TENJINCG-1046 ends */	
					if(timer!=null)
					{
						logger.info("cancel the timer");
						timer.cancel();
					}
					timer=new Timer();
					schedule_Id=entry.getKey();
					sh=entry.getValue();
					String schedule_time=sh.getSch_date();	
					Date date = df.parse(schedule_time);	
					timer.schedule(new ScheduleTimer(timer,new Date(),schedule_Id,sh),date);
					Thread.sleep(4000);
				}
			}else {
				logger.info("No Schedules found");
			}
			
			excludeSecondsToExecute();
			executeScheduler();
		}catch(Exception e){
			logger.error("Checking for the schedules list failed due to "+e.getMessage());
			logger.error("Error ", e);
		}
	}
	/* Added by Roshni for TENJINCG-1046 starts */
  private void sendMail(Scheduler sch) {
		
	  try {
		  TenjinMailHandler tmhandler=new TenjinMailHandler();
		  User user=new UserHandler().getUser(sch.getCreated_by());
		  	/*Added by Padmavathi for TENJINCG-1050 starts*/
		  if(!new ProjectMailHandler().isUserOptOutFromMail(sch.getProjectId(),"ScheduleTriggered",user.getId())){
				/*Added by Padmavathi for TENJINCG-1050 ends*/ 
			  JSONObject mailContent=tmhandler.getMailContent(String.valueOf(sch.getSchedule_Id()), sch.getTaskName(), "Schedule Task", "", sch.getAction(),
				new Timestamp(new Date().getTime()), user.getEmail(), "");
		
			  new TenjinMailHandler().sendEmailNotification(mailContent);
		  }
		} catch (TenjinConfigurationException e) {
			logger.error("Error ", e);
		} catch (RecordNotFoundException e) {
			
			logger.error("Error ", e);
		} catch (DatabaseException e) {
			
			logger.error("Error ", e);
		}
		
	}
	/* Added by Roshni for TENJINCG-1046 ends */
public void createNewThread(){
		logger.info("Initialization of Scheduler for Tenjin");
	  try{
		  Task task=new Task();
		  Thread t=new Thread(task);
		  t.setName("Tenjin Scheduler");
		  t.start();
	  }catch(Exception e){
			logger.error("Initialization of Scheduler for Tenjin has failed due to "+e.getMessage());
		  logger.error("Error ", e);
	  }
  }
	public void excludeSecondsToExecute(){
		try{
			int seconds=new Date().getSeconds();
			int remaining=60-seconds;
			remaining=remaining+8;
			Thread.sleep(remaining*1000);
		} catch (Exception e) {
			logger.error("Error ", e);
		}
	}
}
