/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ScheduleTimer.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/


/*
 * Added file by nagababu for Req#TJN_24_03 on 16/10/2016
 */
package com.ycs.tenjin.scheduler;

import java.util.ArrayList;
import java.util.Date;
import java.util.Timer ;
import java.util.TimerTask ;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class ScheduleTimer extends TimerTask { 
    private static final Logger logger = LoggerFactory.getLogger(ScheduleTimer.class);   

	Timer timer ; 
	Date date;
	int scheduler_Id;
	Scheduler sh;
	public ArrayList<String> dates=new ArrayList<String>();
	public ScheduleTimer () {
	}
	public ScheduleTimer ( Timer timer,Date date,int scheduler_Id,Scheduler sh) {
		this.date=date;
		this.timer = timer;
		this.scheduler_Id=scheduler_Id;
		this.sh=sh;
	}
	public ArrayList<String> getDates() {
		return dates;
	}
	public void setDates(ArrayList<String> dates) {
		this.dates = dates;
	}

	@Override
	public void run(){
		logger.info("Scheduler Task Timer is initialized");
		ExecutorThreadPool etPool=new ExecutorThreadPool(this.scheduler_Id,sh);
		etPool.createTask();
	}
	
}
