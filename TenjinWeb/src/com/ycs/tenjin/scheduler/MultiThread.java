/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MultiThread.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/


/*
 * Added file by nagababu for Req#TJN_24_03 on 16/10/2016
 */
package com.ycs.tenjin.scheduler;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MultiThread implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(MultiThread.class);   

	int scheduler_Id;
	Thread t;
	Scheduler scheduler;
	public MultiThread(){

	}
	public int getScheduler_Id() {
		return scheduler_Id;
	}
	public Thread getT() {
		return t;
	}
	public Scheduler getScheduler() {
		return scheduler;
	}
	public MultiThread(int scheduler_Id,Scheduler scheduler) {
		this.scheduler_Id=scheduler_Id;
		this.scheduler=scheduler;
	}
	@Override
	public void run(){
		logger.info("Scheduler Task Timer is Called for Schedule Id: "+this.scheduler_Id+" "+new Date());
		synchronized(this){
			try{
				new SchedulerHandler().initiateschedulerTask(this.scheduler);
			}catch(Exception e){
				logger.error("Scheduler Task Timer is failed for Schedule Id: "+this.scheduler_Id+" "+"due to "+e.getMessage());
				logger.error("Error ", e);
			}
		}
	}
}
