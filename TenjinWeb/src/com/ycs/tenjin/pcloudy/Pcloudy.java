/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Pcloudy.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION 
 * 20-06-2018			Sameer Gupta			Rewritten for Pcloudy 
 * 25-01-2019			Sahana					pCloudy
 */

package com.ycs.tenjin.pcloudy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ssts.pcloudy.Connector;
import com.ssts.pcloudy.Version;
import com.ssts.pcloudy.dto.device.MobileDevice;
import com.ssts.pcloudy.exception.ConnectError;
import com.ycs.tenjin.device.RegisteredDevice;

public class Pcloudy {

	private static final Logger logger = LoggerFactory.getLogger(Pcloudy.class);
	/*Added by Sahana for pCloudy:Starts*/
	String baseUrl = "https://device.pcloudy.com/api";

	private String username;
	private String password;
	private String key;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Pcloudy() {
		
	}
	/*Added by Sahana for pCloudy:Ends*/
	public ArrayList<RegisteredDevice> getDevices(String userName, String key) {

		logger.info("Get List of devices from pcloudy.");

		System.out.println(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss aaa")
				.format(System.currentTimeMillis()));

		String authToken = "";
		Connector con = null;
		ArrayList<RegisteredDevice> listDevices = new ArrayList<RegisteredDevice>();
		try {

			// Get Connection
			con = new Connector("https://device.pcloudy.com/api/");

			// User Authentication over pCloudy
			authToken = con.authenticateUser(userName,key);
			ArrayList<MobileDevice> availableDevices = new ArrayList<MobileDevice>();
			availableDevices
			.addAll(con.chooseDevices(authToken, "android", new Version("7.0.*"), new Version("9.*.*"), 400));

			//uncommented by shivam sharma for pCloudy starts
			availableDevices.addAll(con.chooseDevices(authToken, "ios", new Version("9.*"), new Version("11.*"), 400));
			//uncommented by shivam sharma for pCloudy ends

			for (MobileDevice mobileDevice : availableDevices) {
				RegisteredDevice rd = new RegisteredDevice();
				rd.setDeviceRecId(mobileDevice.id.toString());
				rd.setDeviceId(mobileDevice.id.toString());
				rd.setDeviceName(mobileDevice.full_name.toString());
				/*Commented by Sahana for pCloudy:Starts*/
				/*rd.setDeviceType("Physical");*/
				/*Commented by Sahana for pCloudy:Starts*/
				listDevices.add(rd);
			}

		} catch (ConnectError | IOException e) {
			logger.error("Failed to get list of devices.");
		} finally {
			try {
				con.revokeTokenPrivileges(authToken);
			} catch (ConnectError | IOException e) {
				logger.error(e.getMessage());
			}
		}
		return listDevices;
	}
	/*Added by Sahana for pCloudy:Starts*/
	private String getToken(String username, String apiKey) {
		String token = null;
		try {
			URL url = new URL(baseUrl + "/" + "access");
			Base64 b = new Base64();
			String encoding = b.encodeAsString(new String(username + ":" + apiKey).getBytes());
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);
			connection.setRequestProperty("Authorization", "Basic " + encoding);
			InputStream content = (InputStream) connection.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(content));
			String line;
			while ((line = in.readLine()) != null) {
				if (line.contains("200")) {
					JSONObject json = new JSONObject(line.toString());
					token = new JSONObject(json.get("result").toString()).get("token").toString();
				} else {
					throw new Exception();
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return token;
	}

	public JSONObject getAllFilters() {
		String token = this.getToken(this.username, this.key);
		URL url = null;
		JSONObject filterValue=null;
		try {
			url = new URL(baseUrl + "/" + "get_all_filters");
		} catch (MalformedURLException e) {
			logger.error(e.getMessage());
		}
		try {
			String json = "{\"token\":\"" + token + "\"}";
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

			connection.setRequestMethod("POST");
			OutputStream os = connection.getOutputStream();
			os.write(json.getBytes("UTF-8"));
			os.close();

			InputStream content = (InputStream) connection.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(content));
			String line;
			while ((line = in.readLine()) != null) {
				if (!line.contains("error")) {
					filterValue = new JSONObject(new JSONObject(line.toString()).get("result").toString());
				} else {
					throw new Exception();
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return filterValue;
	}

	public JSONObject deviceListOnFilter(Map<String, String> map) {
		String token = this.getToken(this.username, this.key);
		String s = "{\"token\":\"" + token;
		String str = "";
		for (String key : map.keySet()) {
			str = "\",\"" + key + "\":\"" + map.get(key) + str;
		}
		URL url = null;
		JSONObject devices=null;
		String json = s + str+"\"}";
		try {
			url = new URL(baseUrl + "/" + "get_devices_by_filters");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

			connection.setRequestMethod("POST");
			OutputStream outputStream = connection.getOutputStream();
			outputStream.write(json.getBytes("UTF-8"));
			outputStream.close();
			InputStream content = (InputStream) connection.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(content));
			String line;
			while ((line = in.readLine()) != null) {
				if (line.contains("200")) {
					devices = new JSONObject(new JSONObject(line.toString()).get("result").toString());
				} else {
					throw new Exception();
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return devices;
	}

	public JSONObject deviceById(int id) {
		String token = this.getToken(this.username, this.key);
		URL url = null;
		JSONObject deviceValues=null;
		String json = "{\"token\":\"" + token + "\",\"id\":" + id + "}";
		try {
			url = new URL(baseUrl + "/" + "get_deviceDetails_byId");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

			connection.setRequestMethod("POST");
			OutputStream os = connection.getOutputStream();
			os.write(json.getBytes("UTF-8"));
			os.close();
			InputStream content = (InputStream) connection.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(content));
			String line;
			while ((line = in.readLine()) != null) {
				if (line.contains("200")) {
					JSONArray deviceArray =  new JSONArray(
							new JSONObject(new JSONObject(line.toString()).get("result").toString()).get("models")
							.toString());
					deviceValues=(JSONObject) deviceArray.get(0);
				} else {
					throw new Exception();
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return deviceValues;
	}

	public JSONObject getPcloudyReport(String pCloudySessionName) {
		String token = this.getToken(this.username, this.key);
		URL url = null;
		JSONObject results=null;
		String json = "{\"token\":\"" + token + "\",\"session_name\":\"" + pCloudySessionName + "\"}";
		try {
			url = new URL(baseUrl + "/" + "report/report_path.php");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

			connection.setRequestMethod("POST");
			OutputStream os = connection.getOutputStream();
			os.write(json.getBytes("UTF-8"));
			os.close();
			InputStream content = (InputStream) connection.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(content));
			String line;
			while ((line = in.readLine()) != null) {
				if (line.contains("200")) {
					results = new JSONObject(new JSONObject(line.toString()).get("result").toString());
				} else {
					throw new Exception();
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return results;
	}
}
/*Added by Sahana for pCloudy:Ends*/
