/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: PcloudyProcessor.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2018-19 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
#301,�Trinity Woods�, 872/D, 
Sir CV Raman Road, Indiranagar, 
Bengaluru, Karnataka 560038
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 25-01-2019			Sahana 					Newly Added for pCloudy
 */
package com.ycs.tenjin.pcloudy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.PcloudyDeviceHelper;
import com.ycs.tenjin.device.RegisteredDevice;
import com.ycs.tenjin.util.CryptoUtilities;

public class PcloudyProcessor {
	private static final Logger logger = LoggerFactory.getLogger(PcloudyProcessor.class);
	//Weekly once pCloudy filters gets updated
	public static final int MINUTES_INTERVAL = 10080;
	Pcloudy pcloudy;
	
	public PcloudyProcessor(){
		this.pcloudy = new Pcloudy();
	}
	
	public PcloudyProcessor(Pcloudy objPcloudy){
		this.pcloudy = objPcloudy;
	}
	
	public Pcloudy getPcloudyCredentials(String clientIp)
	{
		Pcloudy pCloudyObj=new Pcloudy();
		try {

			RegisteredClient client = new ClientHelper().hydrateClientByHostName(clientIp);
			if(client.getDeviceFarmCheck()!=null && client.getDeviceFarmCheck().equalsIgnoreCase("Y"))
			{
				pCloudyObj.setUsername(client.getDeviceFarmUsrName().trim());
				/*Modified by paneendra for VAPT FIX starts*/
				/*pCloudyObj.setPassword(new Crypto().decrypt(client.getDeviceFarmPwd().split("!#!")[1],
                        decodeHex(client.getDeviceFarmPwd().split("!#!")[0].toCharArray())));*/
				pCloudyObj.setPassword(new CryptoUtilities().decrypt(client.getDeviceFarmPwd()));
				/*pCloudyObj.setKey(new Crypto().decrypt(client.getDeviceFarmKey().split("!#!")[1],
						decodeHex(client.getDeviceFarmKey().split("!#!")[0].toCharArray())));*/
				pCloudyObj.setKey(new CryptoUtilities().decrypt(client.getDeviceFarmKey()));
				/*Modified by paneendra for VAPT FIX ends*/
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return pCloudyObj;
	}
	public JSONObject getAllFilters(){
		/*
		 * 1.Check whether 'pcloudyFilters' file exist or not
		 * 2.If file exists, for defined  minutes interval, check the difference in minutes with present and previous call
		 * 3.If minutes interval exceeds certain range, then do the API call, get the filter details and store in file
		 * 4.If not,retrieve the details from file specified
		 * */

		JSONObject filters=null;
		try {
			String	filePathString = "D:\\Tenjin_App_Data\\pcloudy\\pcloudyFilters.txt";
			Path path = Paths.get(filePathString);
			BufferedReader reader=null;
			int minutesCount=checkFilterCallDuration(filePathString);
			if (Files.exists(path) && (minutesCount<MINUTES_INTERVAL)) {
				try {
					reader = new BufferedReader(new FileReader(filePathString)); 
					String jsonString = reader.readLine();
					filters=new JSONObject(jsonString);
				} catch (JSONException e) {
					logger.error(e.getMessage());
				} finally {
					if(reader!=null)
						reader.close();
				}
			}
			else
			{
				filters=this.pcloudy.getAllFilters();
				Files.write(path, filters.toString().getBytes());
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return filters;
	}

	private static int checkFilterCallDuration(String filePathString) {
		File file = new File(filePathString);
		SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
		Date date = new Date();
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = sdf.parse(sdf.format(file.lastModified()));
			d2 = sdf.parse(sdf.format(date));
		} catch (ParseException e) {
			logger.error(e.getMessage());
		}
		long diff = d2.getTime() - d1.getTime();
		int diffMinutes = (int) (diff / (60 * 1000) % 60);
		return diffMinutes;
	}
	public JSONObject deviceListOnFilter(Map<String, String> filter) {
		return this.pcloudy.deviceListOnFilter(filter);
	}

	public RegisteredDevice deviceById(int id) throws DatabaseException {

		/*
		 * 1.check whether device details is available in DB using deviceId
		 * 2.If exist, get the device details from DB
		 * 3.If not, then do the API call and get details from pCloudy 
		 * */

		boolean deviceFound=false;
		RegisteredDevice rd = null;
		try {
			PcloudyDeviceHelper objPcloudyDeviceHelper=	new PcloudyDeviceHelper();
			deviceFound=objPcloudyDeviceHelper.checkDeviceExist(id);
			if(!deviceFound)
			{
				JSONObject device=this.pcloudy.deviceById(id);
				objPcloudyDeviceHelper.persistPcloudyDevice(device);
			}
			rd=objPcloudyDeviceHelper.hydrateDevice(id);

		} catch (DatabaseException e) {
			logger.error(e.getMessage());
			throw new DatabaseException("Could not fetch record ", e);
		}
		return rd;
	}

	public JSONObject getPcloudyReport(String pCloudySessionName)
	{
		/*
		 * For the given "pCloudySessionName",get report link using API
		 * */

		return this.pcloudy.getPcloudyReport(pCloudySessionName);
	}
}
