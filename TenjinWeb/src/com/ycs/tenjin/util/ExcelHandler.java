/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExcelHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 25-Oct-2016		Sriram Sridharan		Fix for Bug in Test Case upload - Removed dependency of Test Case Tree 
 * 03-Nov-2016		Sriram					Defect#653
 * 14-Mar-2017       Leelaprasad             for Adding storage
 * 14-Jun-2017       Roshni Das             	Modified and added for requirement TENJINCG-192
 * 15-Jun-2017       Roshni Das             	Modified and added for requirement TENJINCG-190
 * 16-Jun-2017       Roshni Das             	Fixed by Roshni for TENJINCG-209
 * 16-Jun-2017		Roshni Das				For TENJINCG-219
 * 16-Jun-2017		Roshni Das				For TENJINCG-220,TENJINCG-221
 * 21-Jun-2017		Roshni					TENJINCG-249,TENJINCG-221
 * 11-08-2017        Padmavathi              T25IT-109
 * 18-08-2017        Leelaprasad             Defect T25IT-200
 * 18-08-2017        Leelaprasad             Defect T25IT-249
 * 19-08-2017        Leelaprasad             Defect T25IT-230
 * 24-Aug-2017		Roshni					Defect T25IT-286,275,285
 * 28-Aug-2017		Roshni					Defect T25IT-270
 * 29-Aug-2017		Roshni					Defect T25IT-338
 * 30-Aug-2017		Roshni					Defect T25IT-357
 * 31-Aug-2017		Roshni					Defect T25IT-363
 * 26-Oct-2017		Roshni				   	TENJINCG-305
 * 26-Oct-2017		Padmavathi				TENJINCG-353  
 * 06-Dec-2017       Gangadhar Badagi        TCGST-3
 * 18-01-2018		Pushpalatha				TENJINCG-567,TENJINCG-580,TENJINCG-583
 * 30-01-2018		Preeti					TENJINCG-503
 * 31-01-2018		Preeti					TENJINCG-503
 * 20-06-2018        Padmavathi              T251IT-93
 * 22-06-2018	    Padmavathi				T251IT-59
 * 27-09-2018        Padmavathi              TENJINCG-741
 * 27-09-2018        Pushpa              	TENJINCG-740
 * 28-09-2018        Pushpa             		TENJINCG-819
 * 01-10-2018        Padmavathi              TENJINCG-820
 * 01-10-2018		Pushpa					TENJINCG-821
 * 25-10-2018        Padmavathi              for jenkins
 * 02-11-2018		Ashiki					TENJINCG-895
 * 07-11-2018        Padmavathi              TENJINCG-893
 * 10-12-2018		Ashiki					 TJN252-44 
 * 12-12-2018		Ashiki					TJNUN262-120
 * 13-12-2018		Ashiki					TJNUN262-107
 * 19-12-2018		Pushpa					TJN262R2-30
 * 04-01-2019        Padmavathi              TJN252-62
 * 08-01-2019		Pushpa					Performance testing fixes
 * 08-01-2019        Padmavathi              TJN252-67
 * 25-01-2019        Padmavathi              for TPT-17 
 * 08-02-2019		Padmavathi				TJN252-86
 * 13-02-2019		Preeti					TENJINCG-970
 * 18-02-2019		Preeti					TENJINCG-969
 * 11-03-2019        Padmavathi              TENJINCG-997
 * 26-03-2019		Prem					TNJNR2-17
 * 24-04-2019		Roshni					TENJINCG-1038
 * 25-05-2019		Ashiki					V2.8-70
 * 13-06-2019		Roshni					V2.8-95
 * 08-07-2019        Padmavathi              TV2.8R2-20 & 22 
 * 04-09-2019		Ashiki					TJN27-38,TJN27-40
 * 23-09-2019		Pushpalatha				TENJINCG-1102
 * 05-02-2020		Roshni					TENJINCG-1168
 * 06-03-2020		Sriram					Fix for TENJINCG-1102 Rolled back
 * 16-06-2020		Ashiki					v210Reg-21
 * 11-02-2020        Paneendra               TENJINCG-1257
 * 09-04-2021        Paneendra               TENJINCG-1268
 * 31-03-2021        Paneendra               TENJINCG-1267
 * 31-05-2021        Paneendra               Removed exec mode and dependencyrules header
 * */

package com.ycs.tenjin.util;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.poi.POIXMLException;
import org.apache.poi.poifs.crypt.Decryptor;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFTable;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTable;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableColumns;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.ScreenState;
import com.ycs.tenjin.aws.FileUploadImpl;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.AutsHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ModuleHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.db.TestCaseHelper;
import com.ycs.tenjin.db.TestCaseHelperNew;
import com.ycs.tenjin.db.TestSetHelper;
import com.ycs.tenjin.db.TestSetHelperNew;
import com.ycs.tenjin.db.TestStepHelper;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.AuditHandler;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.handler.DependencyRulesHandler;
import com.ycs.tenjin.handler.TestSetHandler;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestDataPath;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.project.TestStep;




public class ExcelHandler {
	private static final Logger logger = LoggerFactory.getLogger(ExcelHandler.class);
	private Connection projConn = null;
	/***********************************************
	 * Added by Sriram for Requirement TJN_22_04 (Lesser Handling Through app specific package) Tenjin v2.2 (14-10-2015)
	 */
	/***********************************************
	 * Added by Sriram for Requirement TJN_22_04 (Lesser Handling Through app specific package) Tenjin v2.2 (14-10-2015) ends
	 */
	/**************************************************************
	 * Added by Sriram for Execution Screen overhaul (Tenjin v2.2) - 22-09-2015 begins
	 */
	public ExcelHandler(Connection appConn){
		this.projConn = appConn;
	}

	public ExcelHandler(){

	}
	/**************************************************************
	 * Added by Sriram for Execution Screen overhaul (Tenjin v2.2) - 22-09-2015 ends
	 */


	private Workbook workbook;



	public Workbook getWorkbook() {
		return workbook;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}

	public ExcelHandler(Connection projConn, Connection appConn){
		this.projConn = projConn;
	}



	/*Added by Pushpalatha for TENJINCG-1030 starts*/
	public void processFunctionsDownload(String folderPath,Aut aut,String funcCode) throws ExcelException{

		String app_name=aut.getName();
		try{
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Functions");
			XSSFSheet sheet1 = workbook.createSheet("Application");
			CellStyle cellStyle = workbook.createCellStyle();
			Font hSSFFont = workbook.createFont();

			hSSFFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
			hSSFFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

			cellStyle = workbook.createCellStyle();
			cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
			cellStyle.setFont(hSSFFont);
			cellStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
			cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

			Row row=sheet.createRow(0);
			Cell c1=row.createCell(0);
			c1.setCellValue("APPLICATION NAME");
			Cell c2=row.createCell(1);
			c2.setCellValue("FUNCTION CODE");
			Cell c3=row.createCell(2);
			c3.setCellValue("FUNCTION NAME");
			Cell c4 = row.createCell(3);
			c4.setCellValue("MENU");
			Cell c5 = row.createCell(4);
			c5.setCellValue("DATE FORMAT");
			sheet.autoSizeColumn(0);
			sheet.autoSizeColumn(1);
			sheet.autoSizeColumn(2);
			sheet.autoSizeColumn(3);
			sheet.autoSizeColumn(4);
			cellStyle.setFont(hSSFFont);
			c1.setCellStyle(cellStyle);
			c2.setCellStyle(cellStyle);
			c3.setCellStyle(cellStyle);
			c4.setCellStyle(cellStyle);
			c5.setCellStyle(cellStyle);
			if(funcCode.equals("")){
				ArrayList<ModuleBean> modules=new ModulesHelper().hydrateModules(app_name);
				/*added by shruthi for Tenj210-25 starts*/
				if(modules.size()<=0){
					throw new ExcelException("No function available for this application");
				}
				/*added by shruthi for Tenj210-25 ends*/

				int count=1;
				for(ModuleBean m:modules)
				{
					row=sheet.createRow(count);
					row.createCell(0).setCellValue(app_name);
					row.createCell(1).setCellValue(m.getModuleCode());
					row.createCell(2).setCellValue(m.getModuleName());
					row.createCell(3).setCellValue(m.getMenuContainer());
					row.createCell(4).setCellValue(m.getDateFormat());
					count++;
				}
			}else{
				ModuleBean m= new ModulesHelper().hydrateModule(aut.getId(),funcCode);
				row=sheet.createRow(1);
				row.createCell(0).setCellValue(app_name);
				row.createCell(1).setCellValue(m.getModuleCode());
				row.createCell(2).setCellValue(m.getModuleName());
				row.createCell(3).setCellValue(m.getMenuContainer());
				row.createCell(4).setCellValue(m.getDateFormat());
			}


			Row r1=sheet1.createRow(0);

			Cell a1=r1.createCell(0);
			a1.setCellValue("APPLICATION NAME");
			Cell a2=r1.createCell(1);
			a2.setCellValue("APPLICATION TYPE");
			Cell a3=r1.createCell(2);
			a3.setCellValue("ADAPTER");
			Cell a4=r1.createCell(3);
			a4.setCellValue("URI");
			Cell a5=r1.createCell(4);
			a5.setCellValue("BROWSER");
			Cell a6=r1.createCell(5);
			a6.setCellValue("PAUSE TIME");
			Cell a7=r1.createCell(6);
			a7.setCellValue("PAUSE LOCATION");
			Cell a8=r1.createCell(7);
			a8.setCellValue("AUT OPERATIONS");
			Cell a9=r1.createCell(8);
			a9.setCellValue("AUT USER TYPES");
			Cell a10=r1.createCell(9);
			a10.setCellValue("DATE FORMAT");


			sheet1.autoSizeColumn(0);
			sheet1.autoSizeColumn(1);
			sheet1.autoSizeColumn(2);
			sheet1.autoSizeColumn(3);
			sheet1.autoSizeColumn(4);
			sheet1.autoSizeColumn(5);
			sheet1.autoSizeColumn(6);
			sheet1.autoSizeColumn(7);
			sheet1.autoSizeColumn(8);
			sheet1.autoSizeColumn(9);
			sheet1.autoSizeColumn(10);
			cellStyle.setFont(hSSFFont);
			a1.setCellStyle(cellStyle);
			a2.setCellStyle(cellStyle);
			a3.setCellStyle(cellStyle);
			a4.setCellStyle(cellStyle);
			a5.setCellStyle(cellStyle);
			a6.setCellStyle(cellStyle);
			a7.setCellStyle(cellStyle);
			a8.setCellStyle(cellStyle);
			a9.setCellStyle(cellStyle);
			a10.setCellStyle(cellStyle);


			r1=sheet1.createRow(1);

			r1.createCell(0).setCellValue(aut.getName());
			r1.createCell(1).setCellValue(aut.getApplicationType());
			r1.createCell(2).setCellValue(aut.getAdapterPackage());
			r1.createCell(3).setCellValue(aut.getURL());
			r1.createCell(4).setCellValue(aut.getBrowser());
			r1.createCell(5).setCellValue(aut.getPauseTime());
			r1.createCell(6).setCellValue(aut.getPauseLocation());
			r1.createCell(7).setCellValue(aut.getOperation());
			r1.createCell(8).setCellValue(aut.getLoginUserType());
			r1.createCell(9).setCellValue(aut.getDateFormat());
			FileOutputStream fout=null;
			try{
				app_name=this.removeSpaces(app_name);
				 fout = new FileOutputStream(folderPath + "\\" + app_name + "-Functions.xlsx");
				workbook.write(fout);
				logger.debug("File saved on path " + folderPath + "\\" + app_name + "-output.xlsx");
			}catch(Exception e){
				logger.error("Could not save file on path " + folderPath + "\\" + app_name + "-output.xlsx" + " because of an exception");
				logger.error(e.getMessage());
				throw new ExcelException("Could not save generated Excel file to disk",e);
			}finally {
				fout.close();
			}

		}
		/*added by shruthi for Tenj210-25 starts*/
		catch(ExcelException e1)
		{
			throw new ExcelException("No function available for this application");
		}
		/*added by shruthi for Tenj210-25 ends*/
		catch(Exception e){


		}finally{
			logger.debug("Done");
		}

	}
	/*Added by Pushpalatha for TENJINCG-1030 ends*/
	/*Changed by Leelprasad for the Defect T25IT-200 starts*/
	public String removeSpaces(String str){
		String[] appStrings=str.split(" ");
		String app_name="";
		int i=0;
		for(String exmString:appStrings){
			if(i==0){
				app_name=exmString;
			}else{
				app_name=app_name+exmString;
			}
			i++;
		}
		return app_name;
	}
	/*Added by pushpalatha for TENJINCG-1030 starts*/
	public void processFunctionsUpload(String folderPath,String fileNameWithoutExtension) throws ExcelException{

		Connection appConn = null;
		FileInputStream file=null;
		FileOutputStream fout=null;
		try{
			appConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		}catch(Exception e){
			throw new ExcelException(e.getMessage(),e);
		}
		try{
			 file = new FileInputStream(new File(folderPath + "\\" + fileNameWithoutExtension + ".xlsx"));
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);
			XSSFSheet sheet1 = workbook.getSheetAt(1);

			CellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
			cellStyle.setFillPattern(CellStyle.BIG_SPOTS);

			Font hSSFFont = workbook.createFont();
			hSSFFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
			hSSFFont.setFontName("Calibri");

			sheet.getRow(0).createCell(5).setCellStyle(cellStyle);
			sheet.getRow(0).getCell(5).setCellValue("UPLOAD_STATUS");
			sheet1.getRow(0).createCell(10).setCellStyle(cellStyle);
			sheet1.getRow(0).getCell(10).setCellValue("UPLOAD_STATUS");
			sheet.autoSizeColumn(5);
			sheet1.autoSizeColumn(11);
			cellStyle.setFont(hSSFFont);
			logger.info("Processing functions");
			int rcount=1;
			int autid=0;
			String appName=sheet.getRow(1).getCell(0).getStringCellValue();

			int n=sheet.getLastRowNum();
			Set<Object> set=new HashSet<>();
			set.add(sheet.getRow(1).getCell(0).getStringCellValue());
			boolean flag=true;
			for(int i=2; i<=n; i++){
				Row row=sheet.getRow(i);
				if(set.add(row.getCell(0).getStringCellValue())){
					flag=false;
					break;
				}
			}
			if(flag){
				if(sheet1.getRow(1)!=null){
					Row row=sheet1.getRow(1);
					if(appName.equals(row.getCell(0).getStringCellValue())){
						Aut aut=null;
						try{
							aut=new AutsHelper().hydrateAut(row.getCell(0).getStringCellValue());
						}catch(NullPointerException e){
							logger.error("Error ", e);
						}
						Aut aut1=new Aut();
						if(aut==null){
							aut1.setName(row.getCell(0).getStringCellValue());
							aut1.setApplicationType((int)row.getCell(1).getNumericCellValue());
							aut1.setAdapterPackage(row.getCell(2).getStringCellValue());
							aut1.setURL(row.getCell(3).getStringCellValue());
							aut1.setBrowser(row.getCell(4).getStringCellValue());
							aut1.setPauseTime((int)row.getCell(5).getNumericCellValue());
							aut1.setPauseLocation(row.getCell(6).getStringCellValue());
							aut1.setOperation(row.getCell(7).getStringCellValue());


							aut1.setLoginUserType(row.getCell(8,Row.CREATE_NULL_AS_BLANK).getStringCellValue());
							aut1.setDateFormat(row.getCell(9,Row.CREATE_NULL_AS_BLANK).getStringCellValue());
							AutHandler ah=new AutHandler();
							try{
								ah.persist(aut1);
								sheet1.getRow(1).createCell(10).setCellValue("Success");
							}catch(Exception e)
							{
								sheet1.getRow(1).createCell(10).setCellValue(e.getMessage());
							}

						}else{
							sheet1.getRow(1).createCell(10).setCellValue("Application already exist");
						}

					}else{
						sheet1.getRow(1).getCell(10).setCellValue("Applicatin name should be same in both function and application sheet");
					}
				}



				while(sheet.getRow(rcount)!=null){
					Row row=sheet.getRow(rcount);
					if(row.getCell(0)==null || row.getCell(1)==null || row.getCell(2)==null)
					{
						sheet.getRow(rcount).createCell(5).setCellValue("All fields are Mandatory");
					}
					else if(row.getCell(0)!=null){
						autid=new AutHelper().getAutId(appConn, row.getCell(0).getStringCellValue());
						if(autid==0)
						{
							sheet.getRow(rcount).createCell(5).setCellValue("Application Name Does not Exist");
						}
						else{
							ModulesHelper mh=new ModulesHelper();
							ModuleBean mb=mh.hydrateModule(autid,row.getCell(1).getStringCellValue()); 
							ModuleBean m=new ModuleBean();
							if(mb==null){
								m.setAut(autid);
								m.setModuleCode(row.getCell(1).getStringCellValue());
								m.setModuleName(row.getCell(2).getStringCellValue());
								m.setMenuContainer(row.getCell(3).getStringCellValue());

								m.setDateFormat(row.getCell(4,Row.CREATE_NULL_AS_BLANK).getStringCellValue());
								try{
									mh.persistModule(m);
									sheet.getRow(rcount).createCell(5).setCellValue("Success");
								}catch(Exception e)
								{
									sheet.getRow(rcount).createCell(5).setCellValue(e.getMessage());
								}
							}
							else{

								sheet.getRow(rcount).createCell(5).setCellValue("Function Code Already Exists");
							}
						}
					}
					rcount++;
				}
			}else{
				sheet.getRow(1).createCell(5).setCellValue("Application Name should be same for all the functions");
			}
			try{
				Thread.sleep(4000);
				fileNameWithoutExtension=removeSpaces(fileNameWithoutExtension);
				 fout = new FileOutputStream(folderPath + "\\" + fileNameWithoutExtension + "-output.xlsx");
				workbook.write(fout);
				logger.debug("File saved on path " + folderPath + "\\" + fileNameWithoutExtension + "-output.xlsx");
			}catch(Exception e){
				logger.error("Could not save file on path " + folderPath + "\\" + fileNameWithoutExtension + "-output.xlsx" + " because of an exception");
				logger.error(e.getMessage());
				throw new ExcelException("Could not save generated Excel file to disk",e);
			}

		}catch(Exception e){
			logger.error("Error ", e);
		}finally{
			logger.debug("Done");
			try {
				fout.close();
				file.close();
			} catch (IOException e) {
				logger.error("Error ", e);
			}
			
			/*Added by paneendra for TENJINCG-1267 starts*/
			DatabaseHelper.close(appConn);
			/*Added by paneendra for TENJINCG-1267 ends*/
		}

	}
	/*Added by pushpalatha for TENJINCG-1030 ends*/
	/*Modified by Padmavathi for 	T251IT-93 starts*/
	
	public String processTestCaseUploadSheet(String folderPath, String fileNameWithoutExtension, int projectId,String createdBy )
			throws ExcelException, RequestValidationException {
		/*Modified by Padmavathi for 	T251IT-93 ends*/
		Connection projConn = null;
		String uploadStatus = "";
		FileInputStream file=null;
		FileOutputStream fout=null;
		try {
			projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		} catch (Exception e) {
			throw new ExcelException(e.getMessage(), e);
		}

		Map<String, String> autMap = new HashMap<String, String>();
		Map<String, String> map = new HashMap<String, String>();
		int rootTestCaseRecId = -1;
		try {
			/*Changed by Pushpalatha for TENJINCG-567 starts*/
			logger.info("Loading the file from path");
			 file = new FileInputStream(
					new File(folderPath + File.separator + fileNameWithoutExtension + ".xlsx"));
			/*Changed by Pushpalatha for TENJINCG-567 ends*/
			logger.info("creating workbook");
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);
			CellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
			cellStyle.setFillPattern(CellStyle.BIG_SPOTS);
			Font hSSFFont = workbook.createFont();
			hSSFFont.setFontName("Calibri");

			hSSFFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
			/*Modified by Padmavathi for TJN252-62 starts*/
			/*sheet.getRow(0).createCell(7).setCellStyle(cellStyle);
			 * sheet.getRow(0).getCell(7).setCellValue("UPLOAD_STATUS");
			sheet.autoSizeColumn(7);*/
			/* Modified by Poojalakshmi for TJN27-94 starts */
			/*
			 * sheet.getRow(0).createCell(8).setCellStyle(cellStyle);
			 * sheet.getRow(0).getCell(8).setCellValue("UPLOAD_STATUS");
			 * sheet.autoSizeColumn(8);
			 */

			/*Modified by Padmavathi for TJN252-62 ends*/
			/*modified by paneendra for altered column size in template starts*/
			sheet.getRow(0).createCell(7).setCellStyle(cellStyle);
			sheet.getRow(0).getCell(7).setCellValue("UPLOAD_STATUS");
			sheet.autoSizeColumn(7);
			/*modified by paneendra for altered column size in template ends*/
			/* Modified by Poojalakshmi for TJN27-94 ends */
			cellStyle.setFont(hSSFFont);
			;

			int startRow = 1;
			int currentRow = startRow;
			int tcIdCol = 0;
			int tcNameCol = 1;
			int tcDescCol = 2;
			/* Modified by Poojalakshmi for TJN27-94 starts */
			/*
			 * int tcTypeCol = 3; int tcPriorityCol = 4;
			 */
			int tcAppNameCol = 3;
			int tcTypeCol = 4;
			int tcPriorityCol = 5;
			/* Modified by Poojalakshmi for TJN27-94 ends */
			int tsIdCol = 1;
			int tsNameCol = 2;
			int tsAppCol = 3;
			int tsModuleCol = 4;
			int tsTypeCol = 5;
			int tsDataIdCol = 6;
			int tsModeCol = 7;
			int tsDescCol = 8;
			int tsExpResultCol = 9;
			int tsOperationCol = 10;
			int tsAutLoginTypeCol = 11;
			int tsRowsToExecute = 12;
			/*Added by Padmavathi for TJN252-62 starts*/
			int tsCustomRules = 13;
			int tsAlwaysShowForRerun=14;
			/*Added by Padmavathi for TJN252-62 ends*/
			/* Commented by Poojalakshmi for TJN27-94 starts */
			/* int tcExecMode = 5; */
			/* Commented by Poojalakshmi for TJN27-94 ends */
			/*Commented by Preeti for TENJINCG-503 starts*/
			/*int tsApiCol = 5;*/
			/*Commented by Preeti for TENJINCG-503 ends*/
			/*Modified by Padmavathi for TJN252-62 starts*/
			/*Modified by Poojalakshmi for TJN252-62 starts*/
			//int tcPresetRules=6;
			//int tcTestSetNameCol = 7;
			/*Modified by Poojalakshmi for TJN27-94 starts*/
			/*Modified by Padmavathi for TJN252-62 ends*/
			/*modified by paneendra for altered column size in template starts*/
			/*int tcExecMode = 6;*/
			/*Modified by Priyanka for TCGST-59 starts*/
			int tcPresetRules = 5;
			int tcTestSetNameCol = 6;
			/*Modified by Priyanka for TCGST-59 ends*/
			/*modified by paneendra for altered column size in template ends*/
			/*Modified by Poojalakshmi for TJN27-94 ends*/
			ArrayList<String> testCaseIds = new ArrayList<String>();
			int tcCount = 0;
			ArrayList<TestCase> testCases = new ArrayList<TestCase>();
			Map<Integer,String> successTcs = new HashMap<Integer,String>();

			logger.info("Processing Test Cases");

			/*Modified by Padmavathi for TENJINCG-820 starts*/
			/*while (sheet.getRow(currentRow) != null && (sheet.getRow(currentRow).getCell(tcIdCol) != null
					&& sheet.getRow(currentRow).getCell(tcIdCol).getStringCellValue() != null
					&& !sheet.getRow(currentRow).getCell(tcIdCol).getStringCellValue().equalsIgnoreCase(""))) {*/
			/*Added by Ashiki for TJNUN262-120 starts*/
			logger.info("validating test case fields");
			this.validateTestcaseFields(sheet,"testcase");
			logger.info("Testcase fields validation done");
			/*Added by Ashiki for TJNUN262-120 ends*/
			while (sheet.getRow(currentRow) != null) {
				logger.info("Gettig each row of the sheet");
				/*Modified by Padmavathi for TENJINCG-820 ends*/
				tcCount++;
				Row cRow = sheet.getRow(currentRow);
				TestCase t = new TestCase();
				/*Added by Ashiki for TJN252-44 starts*/
				/*Modified by Padmavathi for TENJINCG-820 starts*/
				/*if((sheet.getRow(currentRow).getCell(tcIdCol) != null)){
					sheet.getRow(currentRow).getCell(tcIdCol).setCellType(Cell.CELL_TYPE_STRING);
					t.setTcId(cRow.getCell(tcIdCol).getStringCellValue());
					testCaseIds.add(t.getTcId());	
				}else{
					sheet.getRow(currentRow).createCell(7).setCellValue("Test case Id is mandatory.");
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					currentRow++;
					continue;
				}*/
				/*Modified by Padmavathi for TENJINCG-820 ends*/
				/*Added by Pushpa for TJN262R2-30 starts */
				String tcTd=Utilities.trim(sheet.getRow(currentRow).getCell(tcIdCol).toString());
				if(tcTd.contains(" ")){
					/*Modified by Padmavathi for TJN252-62 starts*/
					/*sheet.getRow(currentRow).createCell(7).setCellValue("Testcase Id cannot contains spaces in between.");*/
					/* Modified by Poojalakshmi for TJN27-94 starts */
					/*
					 * sheet.getRow(currentRow).createCell(8).
					 * setCellValue("Testcase Id cannot contains spaces in between.");
					 */
					/* Modified by Padmavathi for TJN252-62 ends */
					/*modified by paneendra for altered column size in template starts*/
					sheet.getRow(currentRow).createCell(8)
					.setCellValue("Testcase Id cannot contains spaces in between.");
					/*modified by paneendra for altered column size in template ends*/
					/* Modified by Poojalakshmi for TJN27-94 ends */

					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					currentRow++;
					continue;
				}else
					/*Added by Pushpa for TJN262R2-30 ends */
					if(sheet.getRow(currentRow).getCell(tcIdCol)==null||Utilities.trim(sheet.getRow(currentRow).getCell(tcIdCol).toString()).equals("")){
						/*Modified by Padmavathi for TJN252-62 starts*/
						/*sheet.getRow(currentRow).createCell(7).setCellValue("Test case Id is mandatory.");*/
						/* Modified by Poojalakshmi for TJN27-94 starts */
						/*
						 * sheet.getRow(currentRow).createCell(8).
						 * setCellValue("Test case Id is mandatory.");
						 */
						/* Modified by Padmavathi for TJN252-62 ends */
						/*modified by paneendra for altered column size in template starts*/
						sheet.getRow(currentRow).createCell(8).setCellValue("Test case Id is mandatory.");
						/*modified by paneendra for altered column size in template ends*/
						/* Modified by Poojalakshmi for TJN27-94 ends */

						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						currentRow++;
						continue;
					} else {
						sheet.getRow(currentRow).getCell(tcIdCol).setCellType(Cell.CELL_TYPE_STRING);
						t.setTcId(Utilities.trim(cRow.getCell(tcIdCol).getStringCellValue()));

						testCaseIds.add(t.getTcId());	
					}
				/*try {
					sheet.getRow(currentRow).getCell(tcNameCol).setCellType(Cell.CELL_TYPE_STRING);
					t.setTcName(cRow.getCell(tcNameCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Case Name NULL for " + t.getTcId());
				}*/
				logger.info("getting each field of test case from row");
				if(sheet.getRow(currentRow).getCell(tcNameCol)==null||Utilities.trim(sheet.getRow(currentRow).getCell(tcNameCol).toString()).equals("")){
					/*Modified by Padmavathi for TJN252-62 starts*/
					/*sheet.getRow(currentRow).createCell(7).setCellValue("Test case Name is mandatory.");*/
					/* Modified by Poojalakshmi for TJN27-94 starts */
					/*
					 * sheet.getRow(currentRow).createCell(8).
					 * setCellValue("Test case Name is mandatory.");
					 */
					/* Modified by Padmavathi for TJN252-62 ends */
					/*modified by paneendra for altered column size in template starts*/
					sheet.getRow(currentRow).createCell(8).setCellValue("Test case Name is mandatory.");
					/*modified by paneendra for altered column size in template ends*/
					/* Modified by Poojalakshmi for TJN27-94 ends */
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					currentRow++;
					continue;
				} else {
					sheet.getRow(currentRow).getCell(tcNameCol).setCellType(Cell.CELL_TYPE_STRING);
					t.setTcName(Utilities.trim(cRow.getCell(tcNameCol).getStringCellValue()));

				}
				/*Added by Ashiki for TJN252-44 ends*/
				try {
					sheet.getRow(currentRow).getCell(tcDescCol).setCellType(Cell.CELL_TYPE_STRING);
					t.setTcDesc(Utilities.trim(cRow.getCell(tcDescCol).getStringCellValue()));
				} catch (NullPointerException e) {
					logger.warn("Test Case Description NULL for " + t.getTcId());
				}
				/* Added by Poojalakshmi for TJN27-94 starts */
				try {
					sheet.getRow(currentRow).getCell(tcAppNameCol).setCellType(Cell.CELL_TYPE_STRING);
					t.setTcAppName(Utilities.trim(cRow.getCell(tcAppNameCol).getStringCellValue()));
					if (!autMap.keySet().contains(t.getTcAppName())) {
						logger.debug("Getting APP ID for " + t.getTcAppName());
						int oId = new AutHelper().getAutId(projConn, t.getTcAppName());
						autMap.put(t.getTcAppName(), Integer.toString(oId));
						t.setAppId(oId);
					} else {

						t.setAppId(Integer.parseInt(autMap.get(t.getTcAppName())));
					}
				} catch (NullPointerException e) {
					logger.warn("Test Case Application Name NULL for " + t.getTcId());
				}
				/* Added by Poojalakshmi for TJN27-94 ends */
				try {
					sheet.getRow(currentRow).getCell(tcTypeCol, Row.CREATE_NULL_AS_BLANK)
					.setCellType(Cell.CELL_TYPE_STRING);
					/*Added by Ashiki for TJN252-44 starts*/
					/*if (cRow.getCell(tcTypeCol).getStringCellValue().equals("")) {*/
					if (Utilities.trim(cRow.getCell(tcTypeCol).getStringCellValue()).equals("")) {
						/*Added by Ashiki for TJN252-44 ends*/
						t.setTcType("Acceptance");
					} else {
						/*t.setTcType(cRow.getCell(tcTypeCol).getStringCellValue());*/
						t.setTcType(Utilities.trim(cRow.getCell(tcTypeCol).getStringCellValue()));
					}
				} catch (NullPointerException e) {
					logger.warn("Test Case Type NULL for " + t.getTcId());
				}
				try {
					sheet.getRow(currentRow).getCell(tcPriorityCol).setCellType(Cell.CELL_TYPE_STRING);
					/*changed by Ashiki for TJN252-44 starts*/
					/*t.setTcPriority(cRow.getCell(tcPriorityCol).getStringCellValue());*/
					t.setTcPriority((Utilities.trim(cRow.getCell(tcPriorityCol).getStringCellValue())));
					/*changed by Ashiki for TJN252-44 ends*/
				} catch (NullPointerException e) {
					logger.warn("Test Case Priority NULL for " + t.getTcId());
				}
				
				
				try {
					logger.info("setting test set name");
					sheet.getRow(currentRow).getCell(tcTestSetNameCol).setCellType(Cell.CELL_TYPE_STRING);
					String tsNames = cRow.getCell(tcTestSetNameCol).getStringCellValue();
					/*changed by Ashiki for TJN252-44 starts*/
					/*t.setTcTestSetName(tsNames);*/
					t.setTcTestSetName(Utilities.trim(tsNames));
					/*changed by Ashiki for TJN252-44 ends*/
					map.put(t.getTcId(), t.getTcTestSetName());
				} catch (NullPointerException e) {
					logger.warn("Test Case test set name(s) NULL for " + t.getTcId());
				}
				try {
					t.setStorage("Local");
				} catch (NullPointerException e) {
					logger.warn("Test Case Storage NULL for " + t.getTcId());
				}
				t.setUploadRow(currentRow);
				testCases.add(t);

				currentRow++;
				if (sheet.getRow(currentRow) != null && sheet.getRow(currentRow).getCell(tcIdCol)!=null) {
					sheet.getRow(currentRow).getCell(tcIdCol).setCellType(Cell.CELL_TYPE_STRING);
				}
				logger.info("getting each fields of the testcase from row is done");
			}

			logger.info(testCases.size() + " test cases were found");

			logger.info("Processing Test Steps");
			XSSFSheet stepSheet = workbook.getSheetAt(1);
			hSSFFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
			hSSFFont.setFontName("Calibri");

			/***Changed column index value from 14 to 13: API Code removed from sheet***/
			/*Modified by Padmavathi for TJN252-62 starts*/
			/*stepSheet.getRow(0).createCell(13).setCellStyle(cellStyle);
			stepSheet.getRow(0).getCell(13).setCellValue("UPLOAD_STATUS");
			stepSheet.autoSizeColumn(13);*/
			stepSheet.getRow(0).createCell(15).setCellStyle(cellStyle);
			stepSheet.getRow(0).getCell(15).setCellValue("UPLOAD_STATUS");
			stepSheet.autoSizeColumn(15);
			/*Modified by Padmavathi for TJN252-62 ends*/
			cellStyle.setFont(hSSFFont);
			currentRow = startRow;
			/*Added by Ashiki for TJNUN262-120 starts*/
			logger.info("validating test step fields");
			validateTestStepFields(stepSheet);
			logger.info("Test step field validation is done");
			/*Added by Ashiki for TJNUN262-120 starts*/
			ArrayList<TestStep> steps = new ArrayList<TestStep>();
			Multimap<String, TestStep> stepMap = ArrayListMultimap.create();
			while (stepSheet.getRow(currentRow) != null && stepSheet.getRow(currentRow).getCell(tcIdCol) != null) {
				stepSheet.getRow(currentRow).getCell(tcIdCol).setCellType(Cell.CELL_TYPE_STRING);

				if (stepSheet.getRow(currentRow).getCell(tcIdCol).getStringCellValue() == null
						|| stepSheet.getRow(currentRow).getCell(tcIdCol).getStringCellValue().equalsIgnoreCase("")) {
					break;
				}
				Row cRow = stepSheet.getRow(currentRow);
				logger.info("creating new step");
				TestStep step = new TestStep();
				/*changed by Ashiki for TJN252-44 starts*/
				/*step.setParentTestCaseId(cRow.getCell(tcIdCol).getStringCellValue());*/
				step.setParentTestCaseId(Utilities.trim(cRow.getCell(tcIdCol).getStringCellValue()));
				/*changed by Ashiki for TJN252-44 ends*/
				logger.info("setting value for each field of step from sheet");
				try {
					stepSheet.getRow(currentRow).getCell(tsIdCol).setCellType(Cell.CELL_TYPE_STRING);
					/*changed by Ashiki for TJN252-44 starts*/
					step.setId(cRow.getCell(tsIdCol).getStringCellValue());
					step.setId(Utilities.trim(cRow.getCell(tsIdCol).getStringCellValue()));
					/*changed by Ashiki for TJN252-44 ends*/
				} catch (NullPointerException e) {
					logger.warn("Test Step ID is NULL for test step");
				}
				/*changed by Ashiki for TJN252-44 starts*/
				if(stepSheet.getRow(currentRow).getCell(tsNameCol)==null||Utilities.trim(stepSheet.getRow(currentRow).getCell(tsNameCol).toString()).equals("")){
					/*Modified by Padmavathi for TJN252-62 starts*/
					/*stepSheet.getRow(currentRow).createCell(13).setCellValue("Name is mandatory.");*/
					stepSheet.getRow(currentRow).createCell(15).setCellValue("Name is mandatory.");
					/*Modified by Padmavathi for TJN252-62 ends*/
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					currentRow++;
					continue;
				}else {
					stepSheet.getRow(currentRow).getCell(tsNameCol).setCellType(Cell.CELL_TYPE_STRING);

					/*step.setShortDescription(cRow.getCell(tsNameCol).getStringCellValue());*/
					step.setShortDescription(Utilities.trim(cRow.getCell(tsNameCol).getStringCellValue()));

				} 
				

				if(stepSheet.getRow(currentRow).getCell(tsAppCol)==null||Utilities.trim(stepSheet.getRow(currentRow).getCell(tsAppCol).toString()).equals("")){
				
					stepSheet.getRow(currentRow).createCell(15).setCellValue("Application is mandatory.");
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					currentRow++;
					continue;
				}else {
					/*try {*/
					stepSheet.getRow(currentRow).getCell(tsAppCol).setCellType(Cell.CELL_TYPE_STRING);
					/*step.setAppName(cRow.getCell(tsAppCol).getStringCellValue());*/
					step.setAppName(Utilities.trim(cRow.getCell(tsAppCol).getStringCellValue()));
					/*changed by Ashiki for TJN252-44 ends*/
					if (!autMap.keySet().contains(step.getAppName())) {
						logger.debug("Getting APP ID for " + step.getAppName());
						/*int oId = new AutHelper().getAutId(appConn, step.getAppName());*/
						int oId = new AutHelper().getAutId(projConn, step.getAppName());
						autMap.put(step.getAppName(), Integer.toString(oId));
						step.setAppId(oId);
					} else {

						step.setAppId(Integer.parseInt(autMap.get(step.getAppName())));
					}
				} /*changed by Ashiki for TJN252-44 starts*/
				/*catch (NullPointerException e) {
					logger.warn("Test Step AUT is NULL for test step " + step.getId());
				}*/

				if(stepSheet.getRow(currentRow).getCell(tsModuleCol)==null||Utilities.trim(stepSheet.getRow(currentRow).getCell(tsModuleCol).toString()).equals("")){
					/*Modified by Padmavathi for TJN252-62 starts*/
					/*stepSheet.getRow(currentRow).createCell(13).setCellValue("Function/API code is mandatory.");*/
					stepSheet.getRow(currentRow).createCell(15).setCellValue("Function/API code is mandatory.");
					/*Modified by Padmavathi for TJN252-62 ends*/
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					currentRow++;
					continue;	
				}else {
					/*try {*/
					stepSheet.getRow(currentRow).getCell(tsModuleCol).setCellType(Cell.CELL_TYPE_STRING);
					/*step.setModuleCode(cRow.getCell(tsModuleCol).getStringCellValue());*/
					/* Added by Priyanka for TJN27-106 and TJN27-170 starts */
					String fcode = Utilities.trim(cRow.getCell(tsModuleCol).getStringCellValue());
					/* Modified  by Priyanka starts*/
					boolean functionExists = false;
					stepSheet.getRow(currentRow).getCell(tsModeCol).setCellType(Cell.CELL_TYPE_STRING);
					String tsModeValue = cRow.getCell(tsModeCol).getStringCellValue();
					step.setTxnMode(tsModeValue);
					if(step.getTxnMode().equalsIgnoreCase("GUI")) {
						ModulesHelper moduleHelper = new ModulesHelper();
						ModuleBean module = moduleHelper.hydrateModule(step.getAppId(), fcode);
						//boolean functionExists = false;
						if ((module != null) && (module.getModuleCode().equals(fcode))) {
							functionExists = true;
						}
					}
					else if(step.getTxnMode().equalsIgnoreCase("API")){
						ApiHelper apiHelper  = new ApiHelper();
						Api api =apiHelper.hydrateApi(step.getAppId(), fcode);
						if ((api != null) && (api.getCode().equals(fcode))) {
							functionExists = true;	
						}
					}
					/* Modified  by Priyanka ends*/
					if (!functionExists) {
						logger.error("Test step application does not contains the function");
						/* Added by Priyanka for TJN27-106 and TJN27-170 ends */
						/* Modified by Padmavathi for TJN252-62 starts */
						/*
						 * stepSheet.getRow(step.getUploadRow()).createCell(13)
						 * .setCellValue("This function does not exists for this application");
						 */
						/* Added by Priyanka for TJN27-106 and TJN27-170 starts */
						stepSheet.getRow(currentRow).createCell(15)
						.setCellValue("This function does not exists for this application/Case senstive");
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						currentRow++;
						continue;
					} else
						step.setModuleCode(Utilities.trim(cRow.getCell(tsModuleCol).getStringCellValue()));
					// step.setModuleCode(Utilities.trim(cRow.getCell(tsModuleCol).getStringCellValue()).toUpperCase());
					/* Added by Priyanka for TJN27-106 and TJN27-170 ends */

				}

				if(stepSheet.getRow(currentRow).getCell(tsTypeCol)==null||Utilities.trim(stepSheet.getRow(currentRow).getCell(tsTypeCol).toString()).equals("")){
					/*Modified by Padmavathi for TJN252-62 starts*/
					/*stepSheet.getRow(currentRow).createCell(13).setCellValue("Type is mandatory");*/
					stepSheet.getRow(currentRow).createCell(15).setCellValue("Type is mandatory");
					/*Modified by Padmavathi for TJN252-62 ends*/
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					currentRow++;
					continue;
				}else {
					/*try {*/
					stepSheet.getRow(currentRow).getCell(tsTypeCol).setCellType(Cell.CELL_TYPE_STRING);
					/*step.setType(cRow.getCell(tsTypeCol).getStringCellValue());*/
					step.setType(Utilities.trim(cRow.getCell(tsTypeCol).getStringCellValue()));
					// Changed by Akshay For the Issue# : TJN27-154 starts
					String type = cRow.getCell(tsTypeCol).getStringCellValue();
					if (type.equalsIgnoreCase("Positive") || type.equalsIgnoreCase("Negative")) {
						step.setType(Utilities.trim(type));
					} else {
						stepSheet.getRow(currentRow).createCell(15)
						.setCellValue("Type Should be Either Positive or Negative");
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						currentRow++;
						continue;
					}
					// Changed by Akshay For the Issue# : TJN27-154 ends
				} 

				if(stepSheet.getRow(currentRow).getCell(tsDataIdCol)==null||Utilities.trim(stepSheet.getRow(currentRow).getCell(tsDataIdCol).toString()).equals("")){
					/*Modified by Padmavathi for TJN252-62 starts*/
					/*stepSheet.getRow(currentRow).createCell(13).setCellValue("Test Data ID is mandatory.");*/
					stepSheet.getRow(currentRow).createCell(15).setCellValue("Test Data ID is mandatory.");
					/*Modified by Padmavathi for TJN252-62 ends*/
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					currentRow++;
					continue;
				}else {
					/*try {*/
					stepSheet.getRow(currentRow).getCell(tsDataIdCol).setCellType(Cell.CELL_TYPE_STRING);
					/*step.setDataId(cRow.getCell(tsDataIdCol).getStringCellValue());*/
					step.setDataId(Utilities.trim(cRow.getCell(tsDataIdCol).getStringCellValue()));
				}/* catch (NullPointerException e) {
					logger.warn("Test Step Data ID is NULL for test step " + step.getId());
				}*/

				if(stepSheet.getRow(currentRow).getCell(tsModeCol)==null||Utilities.trim(stepSheet.getRow(currentRow).getCell(tsModeCol).toString()).equals("")){
					/*Modified by Padmavathi for TJN252-62 starts*/
					/*stepSheet.getRow(currentRow).createCell(13).setCellValue("Mode is mandatory.");*/
					stepSheet.getRow(currentRow).createCell(15).setCellValue("Mode is mandatory.");
					/*Modified by Padmavathi for TJN252-62 ends*/
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					currentRow++;
					continue;
				}else {
					/*try {*/
					/*changed by Ashiki for TJN252-44 ends*/
					/*Changed by Gangadhar Badagi for TCGST-3 starts*/
					/*stepSheet.getRow(currentRow).getCell(tsModeCol).setCellType(Cell.CELL_TYPE_STRING);
					String tsModeValue = cRow.getCell(tsModeCol, Row.CREATE_NULL_AS_BLANK).getStringCellValue();*/
					stepSheet.getRow(currentRow).getCell(tsModeCol, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_STRING);
					String tsModeValue = cRow.getCell(tsModeCol).getStringCellValue();
					/* Changed by Gangadhar Badagi for TCGST-3 ends*/
					/*Modified by Padmavathi for TENJINCG-820 starts*/
					/*for (String tcid : testCaseIds) {
						if (stepSheet.getRow(currentRow).getCell(tcIdCol).getStringCellValue().equals(tcid)) {
							step.setTxnMode(modeValueMap.get(tcid));
							break;
						}
					}*/
					step.setTxnMode(Utilities.trim(tsModeValue));
					/*Modified by Padmavathi for TENJINCG-820 ends*/
				}/* catch (NullPointerException e) {
					logger.warn("Test Step Mode is NULL for test step " + step.getId());
				}*/

				try {
					stepSheet.getRow(currentRow).getCell(tsDescCol).setCellType(Cell.CELL_TYPE_STRING);
					/*changed by Ashiki for TJN252-44 starts*/
					/*step.setDescription(cRow.getCell(tsDescCol).getStringCellValue());*/
					step.setDescription(Utilities.trim(cRow.getCell(tsDescCol).getStringCellValue()));
					/*changed by Ashiki for TJN252-44 ends*/
				} catch (NullPointerException e) {
					logger.warn("Test Step Description is NULL for test step " + step.getId());
				}
				try {
					stepSheet.getRow(currentRow).getCell(tsExpResultCol).setCellType(Cell.CELL_TYPE_STRING);
					/*changed by Ashiki for TJN252-44 starts*/
					/*step.setExpectedResult(cRow.getCell(tsExpResultCol).getStringCellValue());*/
					step.setExpectedResult(Utilities.trim(cRow.getCell(tsExpResultCol).getStringCellValue()));
					/*changed by Ashiki for TJN252-44 ends*/
				} catch (NullPointerException e) {
					logger.warn("Test Step Expected Result is NULL for test step " + step.getId());
				}

				/*changed by Ashiki for TJN252-44 starts*/
				/*Aut aut = new AutHandler().getApplication(step.getAppName());*/
				Aut aut = new AutsHelper().hydrateAut(projConn, step.getAppName());
				/*Added by shruthi for TENJINCG-1177 starts*/
				Api api=new ApiHelper().hydrateApi(aut.getId(),step.getModuleCode());
				/*Added by shruthi for TENJINCG-1177 ends*/
				if(stepSheet.getRow(currentRow).getCell(tsOperationCol)==null||Utilities.trim(stepSheet.getRow(currentRow).getCell(tsOperationCol).toString()).equals("")){
					/*Modified by Padmavathi for TJN252-62 starts*/
					/*stepSheet.getRow(currentRow).createCell(13).setCellValue("Operation is mandatory.");*/
					stepSheet.getRow(currentRow).createCell(15).setCellValue("Operation is mandatory.");
					/*Modified by Padmavathi for TJN252-62 ends*/
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					currentRow++;
					continue;
				}else {
					/*try {*/
					stepSheet.getRow(currentRow).getCell(tsOperationCol).setCellType(Cell.CELL_TYPE_STRING);
					/*step.setOperation(cRow.getCell(tsOperationCol).getStringCellValue());*/
					/*Modified by shruthi for TENJINCG-1177 starts*/
					boolean autOperationExist=false;
					String userEnteredOperation=Utilities.trim(cRow.getCell(tsOperationCol).getStringCellValue());

					if(step.getTxnMode().equalsIgnoreCase("GUI")){
						String autOperation=aut.getOperation();
						String[] autOperationSplit = autOperation.split(",");

						for(String uType:autOperationSplit){
							if(uType.equalsIgnoreCase(userEnteredOperation)){
								/*step.setAutLoginType(Utilities.trim(cRow.getCell(tsAutLoginTypeCol).getStringCellValue()));*/
								autOperationExist=true;
							}
						}
					}
					if(step.getTxnMode().equalsIgnoreCase("API")){
						List<ApiOperation> apiOps=api.getOperations();
						for(ApiOperation apiOp:apiOps){

							if(apiOp.getName().equals(userEnteredOperation)){
								autOperationExist=true;
							}
						}
					}
					/*Modified by shruthi for TENJINCG-1177 ends*/
					if(autOperationExist) {
						step.setOperation(Utilities.trim(cRow.getCell(tsOperationCol).getStringCellValue()));
					}else {
						/*Modified by Padmavathi for TJN252-62 starts*/
						/*stepSheet.getRow(currentRow).createCell(13).setCellValue("Invalid operation");*/
						stepSheet.getRow(currentRow).createCell(15).setCellValue("Invalid operation");
						/*Modified by Padmavathi for TJN252-62 ends*/
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						currentRow++;
						continue;

					}
				} /*catch (NullPointerException e) {
					logger.warn("Test Step Operation is NULL for test step " + step.getId());
				}*/

				if(stepSheet.getRow(currentRow).getCell(tsAutLoginTypeCol)==null||Utilities.trim(stepSheet.getRow(currentRow).getCell(tsAutLoginTypeCol).toString()).equals("")){
					/*Modified by Padmavathi for TJN252-62 starts*/
					/*stepSheet.getRow(currentRow).createCell(13).setCellValue("AUT Login Type is mandatory.");*/
					stepSheet.getRow(currentRow).createCell(15).setCellValue("AUT Login Type is mandatory.");
					/*Modified by Padmavathi for TJN252-62 ends*/
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					currentRow++;
					continue;
				}else {
					/*try {*/
					stepSheet.getRow(currentRow).getCell(tsAutLoginTypeCol).setCellType(Cell.CELL_TYPE_STRING);
					/*step.setAutLoginType(cRow.getCell(tsAutLoginTypeCol).getStringCellValue());*/
					String autLoginType=aut.getLoginUserType();
					String userEnteredLoginType=Utilities.trim(cRow.getCell(tsAutLoginTypeCol).getStringCellValue());
					String[] userTypeSplit = autLoginType.split(",");
					boolean autLoginTypeExist=false;
					for(String uType:userTypeSplit){
						if(uType.equalsIgnoreCase(userEnteredLoginType)){
							/*step.setAutLoginType(Utilities.trim(cRow.getCell(tsAutLoginTypeCol).getStringCellValue()));*/
							autLoginTypeExist=true;
						}
					}
					if(autLoginTypeExist) {
						step.setAutLoginType(Utilities.trim(cRow.getCell(tsAutLoginTypeCol).getStringCellValue()));
					}else {
						/*Modified by Padmavathi for TJN252-62 starts*/
						/*stepSheet.getRow(currentRow).createCell(13).setCellValue("Invalid AUT Login Type.");*/
						stepSheet.getRow(currentRow).createCell(15).setCellValue("Invalid AUT Login Type.");
						/*Modified by Padmavathi for TJN252-62 ends*/
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						currentRow++;
						continue;

					}

				} /*catch (NullPointerException e) {
					logger.warn("Test Step AUT Login Type is NULL for test step " + step.getId());
				}*/


				/*try {
					cRow.getCell(tsRowsToExecute).setCellType(Cell.CELL_TYPE_STRING);
					String r = cRow.getCell(tsRowsToExecute).getStringCellValue();
					int rowsToExecute = Integer.parseInt(r);
					step.setRowsToExecute(rowsToExecute);
				} catch (Exception e) {
					logger.warn("Could not process Rows To Execute value for step {}", step.getId());
				}*/
				if(stepSheet.getRow(currentRow).getCell(tsRowsToExecute)==null||Utilities.trim(stepSheet.getRow(currentRow).getCell(tsRowsToExecute).toString()).equals("")){
					/*Modified by Padmavathi for TJN252-62 starts*/
					/*stepSheet.getRow(currentRow).createCell(13).setCellValue("Rows to Execute is mandatory.");*/
					stepSheet.getRow(currentRow).createCell(15).setCellValue("Rows to Execute is mandatory.");
					/*Modified by Padmavathi for TJN252-62 ends*/
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					currentRow++;
					continue;
				}else {
					try {
						cRow.getCell(tsRowsToExecute).setCellType(Cell.CELL_TYPE_STRING);
						String r = cRow.getCell(tsRowsToExecute).getStringCellValue();
						int rowsToExecute = Integer.parseInt(r);
						if(rowsToExecute<=4) {
							step.setRowsToExecute(rowsToExecute);
						}
						else {
							/*Modified by Padmavathi for TJN252-62 starts*/
							/*stepSheet.getRow(currentRow).createCell(13).setCellValue("Rows To Execute should not exceed its max length 4.");*/
							stepSheet.getRow(currentRow).createCell(15).setCellValue("Rows To Execute should not exceed its max length 4.");
							/*Modified by Padmavathi for TJN252-62 ends*/
							uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
							currentRow++;
							continue;
						}
					} catch (NumberFormatException e) {
						/*Modified by Padmavathi for TJN252-62 starts*/
						/*stepSheet.getRow(currentRow).createCell(13).setCellValue("Enter only Numbers in Rows To Execute");*/
						stepSheet.getRow(currentRow).createCell(15).setCellValue("Enter only Numbers in Rows To Execute");
						/*Modified by Padmavathi for TJN252-62 ends*/
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						currentRow++;
						continue;
					}
				}
				/*changed by Ashiki for TJN252-44 ends*/
				/*Commented by Preeti for TENJINCG-503 starts*/
				/*try {
					stepSheet.getRow(currentRow).getCell(tsApiCol).setCellType(Cell.CELL_TYPE_STRING);
					step.setApiCode(cRow.getCell(tsApiCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Step AUT Login Type is NULL for test step " + step.getId());
				}*/
				/*Commented by Preeti for TENJINCG-503 ends*/
				/*Added by Padmavathi for TJN252-62 starts*/
				try{
					stepSheet.getRow(currentRow).getCell(tsCustomRules).setCellType(Cell.CELL_TYPE_STRING);
					step.setCustomRules(Utilities.trim(cRow.getCell(tsCustomRules).getStringCellValue()));
					/*Added by Padmavathi for TJN252-67 Starts*/
					/*Changed by Padmavathi for TJN252-86 Starts*/
					/*if(step.getCustomRules()!=null && !step.getCustomRules().equalsIgnoreCase("Error")&&!step.getCustomRules().equalsIgnoreCase("Fail")&&!step.getCustomRules().equalsIgnoreCase("Pass")){*/
					if(step.getCustomRules()!=null && !step.getCustomRules().equalsIgnoreCase("")&& !step.getCustomRules().equalsIgnoreCase("Error")&&!step.getCustomRules().equalsIgnoreCase("Fail")&&!step.getCustomRules().equalsIgnoreCase("Pass")){
						/*Changed by Padmavathi for TJN252-86 ends*/
						stepSheet.getRow(currentRow).createCell(15).setCellValue("Custom Rules value should be Error or Pass or Fail.");
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						currentRow++;
						continue;
					}
					/*Added by Padmavathi for TJN252-67 ends*/
				}catch(NullPointerException e){
					logger.warn("Test Step customrule is NULL for test step " + step.getId());
				}
				try{
					stepSheet.getRow(currentRow).getCell(tsAlwaysShowForRerun).setCellType(Cell.CELL_TYPE_STRING);
					step.setRerunStep(Utilities.trim(cRow.getCell(tsAlwaysShowForRerun).getStringCellValue()));
					/*Added by Padmavathi for TJN252-67 Starts*/
					/*Added by Padmavathi for TJN252-86 Starts*/
					/*if(step.getRerunStep()!=null && !step.getRerunStep().equalsIgnoreCase("Y")&&!step.getRerunStep().equalsIgnoreCase("N")){*/
					if(step.getRerunStep()!=null && !step.getRerunStep().equalsIgnoreCase("") && !step.getRerunStep().equalsIgnoreCase("Y")&&!step.getRerunStep().equalsIgnoreCase("N")){
						/*Added by Padmavathi for TJN252-86 Starts*/
						stepSheet.getRow(currentRow).createCell(15).setCellValue("Always show for rerun value should be either Y or N.");
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						currentRow++;
						continue;
					}
					/*Added by Padmavathi for TJN252-67 ends*/
				}catch(NullPointerException e){
					logger.warn("Test Step always show for rerun is NULL for test step " + step.getId());
				}
				/*Added by Padmavathi for TJN252-62 ends*/
				try {
					step.setTstepStorage("Local");
				} catch (NullPointerException e) {
					logger.warn("Test Step Storage is NULL for test step " + step.getId());
				}
				step.setUploadRow(currentRow);
				steps.add(step);
				stepMap.put(step.getParentTestCaseId(), step);
				currentRow++;
			}

			logger.debug(steps.size() + " test steps were found");

			logger.info("Persisting Test Cases");
			TestCaseHelper helper = new TestCaseHelper();
			TestCaseHelperNew tcHelper=	new TestCaseHelperNew();
			int cnt = 0;
			for (TestCase t : testCases) {
				cnt++;
				logger.info("Persisting Test Case " + cnt + " out of " + testCases.size());


				try { 
					/*Modified by Padmavathi for TENJINCG-741 starts*/
					/*if (helper.doesTestCaseExist(projConn, projectId, t.getTcId())) {
						logger.error("Test Case with ID {} already exists for Project {}", t.getTcId(), projectId);
						sheet.getRow(t.getUploadRow()).createCell(7).setCellValue("Record Already Exists");
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						continue;
					}*/
					TestCase testcase=null;
					testcase=	tcHelper.hydrateTestCase(projConn,projectId, t.getTcId());
					if(testcase!=null){
						/*Added by Padmavathi for TENJINCG-820 starts*/
						/*Modified by Padmavathi for TENJINCG-820 ends*/
						/*Commented by Ashiki for TENJINCG-895 starts*/
						if(t.getTcName()!=null && !t.getTcName().equals("") && !t.getTcName().equalsIgnoreCase(testcase.getTcName())){
							if(tcHelper.doesTestCaseNameExist(projectId, t.getTcName())){
								logger.error("Test Case with Name {} already exists for Project {}", t.getTcName(), projectId);
								sheet.getRow(t.getUploadRow()).createCell(7).setCellValue("Testcase name already Exists.Please use a different Testcase name.");
								uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
								continue;
							}
						}
						/*Commented by Ashiki for TENJINCG-895 ends*/
						/*Added by Padmavathi for TJN252-62 starts*/
						String oPresetRules=testcase.getTcPresetRules();
						/*Added by Padmavathi for TJN252-62 ends*/
						testcase.merge(t,true);
						String tc=tcHelper.updateTestCase(testcase);
						if(tc.equalsIgnoreCase("Executing")){
							logger.error("Can not update testcase while it is executing.");
							/*Modified by Padmavathi for TJN252-62 starts*/
							/*sheet.getRow(t.getUploadRow()).createCell(7).setCellValue("Cannot update test case while it is executing.");*/
							/*modified by paneendra for altered column size in template starts*/
							sheet.getRow(t.getUploadRow()).createCell(7).setCellValue("Cannot update test case while it is executing.");
							/*modified by paneendra for altered column size in template ends*/
							/*Modified by Padmavathi for TJN252-62 ends*/
							uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
							continue;
						}
						/*Added by Preeti for TENJINCG-969 starts*/
						AuditRecord auditRecord = new AuditRecord();
						auditRecord.setEntityRecordId(testcase.getTcRecId());
						auditRecord.setLastUpdatedBy(createdBy);
						auditRecord.setEntityType("testcase");
						t.setAuditRecord(auditRecord);
						AuditHandler auditHandler = new AuditHandler();
						auditHandler.persistAuditRecord(t.getAuditRecord());
						/*Added by Preeti for TENJINCG-969 ends*/
						/*Modified by Padmavathi for TJN252-62 starts*/
						/*sheet.getRow(t.getUploadRow()).createCell(7).setCellValue("Test case updated successfully.");*/
						/* Modified by Poojalakshmi for TJN27-94 starts */
						/*
						 * sheet.getRow(t.getUploadRow()).createCell(8).
						 * setCellValue("Test case updated successfully.");
						 */
						/* Modified by Padmavathi for TJN252-62 ends */
						/*modified by paneendra for altered column size in template starts*/
						sheet.getRow(t.getUploadRow()).createCell(7).setCellValue("Test case updated successfully.");
						/*modified by paneendra for altered column size in template ends*/
						/* Modified by Poojalakshmi for TJN27-94 ends */
						/*Added by Padmavathi for TJN252-62 starts*/
						if(t.getTcPresetRules()!=null &&!t.getTcPresetRules().equals("") )
							successTcs.put(testcase.getTcRecId(),t.getTcPresetRules());

						if(oPresetRules!=null&& t.getTcPresetRules()==null){
							new DependencyRulesHandler().deleteDependencyRules(t.getTcRecId());
						}
						/*Added by Padmavathi for TJN252-62 ends*/
					}else{
						t.setProjectId(projectId);
						t.setRecordType("TC");
						/*Modified by Padmavathi for 	T251IT-93 starts*/
						/*String createdBy1 = helper.projOwnerName(projConn, t);*/
						t.setTcCreatedBy(createdBy);
						/*Modified by Padmavathi for 	T251IT-93 ends*/
						t.setTcFolderId(rootTestCaseRecId);

						/*Commented by Ashiki for TENJINCG-895 starts*/
						if(tcHelper.doesTestCaseNameExist(projectId, t.getTcName())){
						logger.error("Test Case with Name {} already exists for Project {}", t.getTcName(), projectId);
						sheet.getRow(t.getUploadRow()).createCell(7).setCellValue("Testcase name already Exists.Please use a different Testcase name.");
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						continue;
					}
						/*Commented by Ashiki for TENJINCG-895 ends*/
						logger.info("persisting casee");
						testcase=helper.persistTestCase(projConn, t);
						logger.info("case persisting done");
						/*commentd by Priyanka for TCGST-59 starts*/
						/*sheet.getRow(t.getUploadRow()).createCell(5).setCellValue(t.getMode());*/
						/*commentd by Priyanka for TCGST-59 ends*/
						/*sheet.getRow(t.getUploadRow()).createCell(7).setCellValue("Test case created successfully.");*/
						/* Modified by Poojalakshmi for TJN27-94 starts */
						/*
						 * sheet.getRow(t.getUploadRow()).createCell(8).
						 * setCellValue("Test case created successfully.");
						 */
						/*modified by paneendra for altered column size in template starts*/
						sheet.getRow(t.getUploadRow()).createCell(7).setCellValue("Test case created successfully.");
						/*modified by paneendra for altered column size in template ends*/
						/* Modified by Poojalakshmi for TJN27-94 ends */
					}	/*Modified by Padmavathi for TENJINCG-741 ends*/
					Collection<TestStep> tSteps = stepMap.get(t.getTcId());
					int scount = 0;
					/*Added by Padmavathi for TJN252-67 Starts*/
					boolean isPresetRule=true;
					/*Added by Padmavathi for TJN252-67 ends*/
					for (TestStep step : tSteps) {
						scount++;
						logger.info("Persisting Step " + scount + " out of " + tSteps.size());
						try {
							/*Modified by Padmavathi for TENJINCG-741 starts*/

							step.setTestCaseRecordId(testcase.getTcRecId());

							step.setTxnMode(step.getTxnMode());
							TestStepHelper stepHelper=new TestStepHelper();
							TestStep testStepToUpdate=stepHelper.hydrateTestStepWithDependencyRules(projConn,step.getTestCaseRecordId(), step.getId());
							/*Added by Padmavathi for TJN252-62 starts*/
							String oCustomRules=null;
							/*Added by Padmavathi for  ends*/
							if(testStepToUpdate!=null){
								/*Added by Padmavathi for TJN252-62 starts*/
								oCustomRules=testStepToUpdate.getCustomRules();
								/*Added by Padmavathi for TJN252-62 ends*/
								testStepToUpdate.merge(step,true);
								try{
									this.validateFields(testStepToUpdate, projectId, projConn);
								}catch(ExcelException e){
									/*Modified by Padmavathi for TJN252-62 starts*/
									/*stepSheet.getRow(step.getUploadRow()).createCell(13).setCellValue(e.getMessage());*/
									stepSheet.getRow(step.getUploadRow()).createCell(15).setCellValue(e.getMessage());
									/*Modified by Padmavathi for TJN252-62 ends*/
									uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
									continue;
								}
								stepHelper.updateTestStep(testStepToUpdate);
								/*Added by Preeti for TENJINCG-969 starts*/
								AuditRecord auditRecord = new AuditRecord();
								auditRecord.setEntityRecordId(testStepToUpdate.getRecordId());
								auditRecord.setLastUpdatedBy(createdBy);
								auditRecord.setEntityType("teststep");
								step.setAuditRecord(auditRecord);
								AuditHandler auditHandler = new AuditHandler();
								auditHandler.persistAuditRecord(step.getAuditRecord());
								/*Added by Preeti for TENJINCG-969 ends*/
								/*Modified by Padmavathi for TJN252-62 starts*/
								/*stepSheet.getRow(step.getUploadRow()).createCell(13).setCellValue("Test step updated successfully.");*/
								stepSheet.getRow(step.getUploadRow()).createCell(15).setCellValue("Test step updated successfully.");
								/*Modified by Padmavathi for TJN252-62 ends*/
								/*Added by Padmavathi for TJN252-62 starts*/
								if((t.getTcPresetRules()!=null||step.getCustomRules()!=null) && testStepToUpdate.getSequence()!=1){
									if(step.getCustomRules()==null){
										testStepToUpdate.setCustomRules(t.getTcPresetRules());
									}
									new DependencyRulesHandler().updateDependencyRulesForStep(testStepToUpdate,testStepToUpdate.getCustomRules());
									if(!testStepToUpdate.getCustomRules().equalsIgnoreCase(t.getTcPresetRules())&& isPresetRule){
										successTcs.remove(testcase.getTcRecId());
										new DependencyRulesHandler().updateDependencyType(step.getTestCaseRecordId());
										isPresetRule=false;
									}
								}else if(step.getCustomRules()==null && oCustomRules!=null && testStepToUpdate.getSequence()!=1){
									new DependencyRulesHandler().deleteDependencyRulesforTestStep(testStepToUpdate.getRecordId(),step.getTestCaseRecordId());
									new DependencyRulesHandler().updateDependencyType(step.getTestCaseRecordId());
								}

								/*Added by Padmavathi for TJN252-62 ends*/
							}else{
								try{
									/*Added by Preeti for TENJINCG-969 starts*/
									step.setCreatedBy(createdBy);
									/*Added by Preeti for TENJINCG-969 ends*/
									this.validateFields(step, projectId, projConn);
								}catch(ExcelException e){
									/*Modified by Padmavathi for TJN252-62 starts*/
									/*stepSheet.getRow(step.getUploadRow()).createCell(13).setCellValue(e.getMessage());*/
									stepSheet.getRow(step.getUploadRow()).createCell(15).setCellValue(e.getMessage());
									/*Modified by Padmavathi for TJN252-62 ends*/
									uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
									continue;
								}
								/*Modified by padmavathi for TPT-17 starts*/
								/*TestStep TestStep=helper.persistTestStep(projConn, step);*/
								TestStep TestStep=stepHelper.persistTestStep(projConn, step);
								/*Modified by padmavathi for TPT-17 ends*/
								/*Added by Padmavathi for TJN252-62 starts*/
								if((t.getTcPresetRules()!=null ||step.getCustomRules()!=null)&& TestStep.getSequence()!=1){
									if(step.getCustomRules()!=null){
										/*Modified by Padmavathi for TENJINCG-997 starts*/
										/*new DependencyRulesHandler().persistDependencyRules(TestStep.getRecordId(), "TestStep", step.getCustomRules());*/
										new DependencyRulesHandler().persistDependencyRules(TestStep, "TestStep", step.getCustomRules());
										/*Modified by Padmavathi for TENJINCG-997 ends*/
										if(t.getTcPresetRules()!=null &&!t.getTcPresetRules().equalsIgnoreCase(step.getCustomRules())&& isPresetRule){
											successTcs.remove(testcase.getTcRecId());
											new DependencyRulesHandler().updateDependencyType(step.getTestCaseRecordId());
											isPresetRule=false;
										}
									}else{
										if(isPresetRule)
											/*Modified by Padmavathi for TENJINCG-997 starts*/
											/*new DependencyRulesHandler().persistDependencyRules(TestStep.getRecordId(), "TestCase", testcase.getTcPresetRules());*/
											new DependencyRulesHandler().persistDependencyRules(TestStep, "TestCase", testcase.getTcPresetRules());
										/*Modified by Padmavathi for TENJINCG-997 ends*/
										else
											/*Modified by Padmavathi for TENJINCG-997 starts*/
											/*new DependencyRulesHandler().persistDependencyRules(TestStep.getRecordId(), "TestStep", testcase.getTcPresetRules());*/
											new DependencyRulesHandler().persistDependencyRules(TestStep, "TestStep", testcase.getTcPresetRules());
										/*Modified by Padmavathi for TENJINCG-997 ends*/
									}

								}
								/*Added by Padmavathi for TJN252-62 ends*/
								/*Modified by Padmavathi for TJN252-62 starts*/
								/*stepSheet.getRow(step.getUploadRow()).createCell(13).setCellValue("Test step created successfully.");*/
								stepSheet.getRow(step.getUploadRow()).createCell(15).setCellValue("Test step created successfully.");
								/*Modified by Padmavathi for TJN252-62 ends*/
							}
						} catch (Exception e) {
							/*Modified by Padmavathi for TJN252-62 starts*/
							/*stepSheet.getRow(step.getUploadRow()).createCell(13).setCellValue(e.getMessage());*/
							stepSheet.getRow(step.getUploadRow()).createCell(15).setCellValue(e.getMessage());
							/*Modified by Padmavathi for TJN252-62 ends*/
							uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						}

					}
				
					TestSetHelper set = new TestSetHelper();
					String testSets = map.get(t.getTcId());
					if (Utilities.trim(testSets).length() > 0) {
						String[] testset = testSets.split(",");
						for (int i = 0; i < testset.length; i++) {
							String testSetName = testset[i];
							TestSet checkValue = set.hydrateTestSet(testSetName, testcase.getProjectId());
							if(checkValue!=null)
							{
								/*Added by padmavathi for TENJINCG-353 starts*/
								/*commentd by Priyanka for TCGST-59 starts*/
								/*if(checkValue.getMode().equalsIgnoreCase(testcase.getMode())){
									set.persistTestSetMap(checkValue.getId(), Integer.toString(testcase.getTcRecId()), testcase.getProjectId(), false);
								}else{
									throw new ExcelException("TestCase created, but it is not mapped to testset due to testset mode and testcase mode is different");
								}*/
								/*commentd by Priyanka for TCGST-59 ends*/
								/*Added by padmavathi for TENJINCG-353 ends*/
							} else {
								TestSet ts = new TestSet();
								/*Modified by Padmavathi for 	T251IT-93 starts*/
								/*String createdBy = helper.projOwnerName(projConn, t);*/
								/*Modified by Padmavathi for 	T251IT-93 ends*/
								ts.setName(testSetName);
								ts.setCreatedBy(createdBy);
								ts.setDescription("");
								ts.setMode(testcase.getMode());
								ts.setParent(-1);
								ts.setPriority("Low");
								ts.setProject(testcase.getProjectId());
								ts.setRecordType("TS");
								ts.setType("Functional");
								set.persistTestSet(ts, testcase.getProjectId());

								/*checkValue = set.hydrateTestSet(testSetName, t.getProjectId());*/
								set.persistTestSetMap(ts.getId(), Integer.toString(testcase.getTcRecId()), testcase.getProjectId(),
										true);
							}
							/*Modified by Padmavathi for TENJINCG-741 ends*/
						}
					}
				} catch (Exception e) {
					/*Modified by Padmavathi for TJN252-62 starts*/
					/*sheet.getRow(t.getUploadRow()).createCell(7).setCellValue(e.getMessage());*/
					sheet.getRow(t.getUploadRow()).createCell(8).setCellValue(e.getMessage());
					/*Modified by Padmavathi for TJN252-62 ends*/
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
				}

			}/*Added by Padmavathi for TJN27-33 starts*/
			try {
				for(Integer tcRecId:successTcs.keySet()){ 
					/*new DependencyRulesHandler().UpdateDependencyRules(tcRecId, "TestCase", successTcs.get(tcRecId),projConn,appConn)*/;
					new DependencyRulesHandler().UpdateDependencyRules(tcRecId, "TestCase", successTcs.get(tcRecId),projConn,projConn);
				}
			} catch (Exception e) {
				
				logger.error("error persisting dependency rules at testcase level");
				logger.error("Error ", e);
			}
			/*Added by Padmavathi for TJN27-33 ends*/
			try {
				/*Commented by Padmavathi for TENJINCG-893  starts*/
				/*Thread.sleep(tcCount * 2500);*/
				/*Commented by Padmavathi for TENJINCG-893  ends*/
				fileNameWithoutExtension = removeSpaces(fileNameWithoutExtension);
				/*Changed by Pushpalatha for TENJINCG-567 starts*/
				 fout = new FileOutputStream(
						folderPath + File.separator + fileNameWithoutExtension + "-output.xlsx");

				workbook.write(fout);
				logger.debug("File saved on path " + folderPath + File.separator + fileNameWithoutExtension + "-output.xlsx");
				/*Changed by Pushpalatha for TENJINCG-567 ends*/
			} catch (Exception e) {
				/*Changed by Pushpalatha for TENJINCG-567 starts*/
				logger.error("Could not save file on path " + folderPath + File.separator + fileNameWithoutExtension
						+ "-output.xlsx" + " because of an exception");
				/*Changed by Pushpalatha for TENJINCG-567 ends*/
				logger.error(e.getMessage());
				throw new ExcelException("Could not save generated Excel file to disk", e);
			}
			/*Added by Ashiki for TJNUN262-120 starts*/
		}catch (RequestValidationException e) {
			throw new RequestValidationException("Invalid");
		}
		/*Added by Ashiki for TJNUN262-120 ends*/
		/*Added by Ashiki for TJNUN262-107 starts*/
		catch (NullPointerException e) {
			throw new RequestValidationException("Invalid");
		}
		/*Added by Ashiki for TJNUN262-107 ends*/
		catch (Exception e) {
			logger.error("ERROR while uploading test cases", e);
		} finally {
			logger.debug("Done");
			try {
				file.close();
				fout.close();
			} catch (IOException e) {
				logger.error("ERROR while uploading test cases", e);
			}
			/*Added by paneendra for TENJINCG-1267 starts*/
			DatabaseHelper.close(projConn);
			/*Added by paneendra for TENJINCG-1267 ends*/
		}

		if (uploadStatus != null && !uploadStatus.equalsIgnoreCase("")) {
			return uploadStatus;
		} else {
			return TenjinConstants.ACTION_RESULT_SUCCESS;
		}
	}


	/*20-Oct-2014 WS: Starts*/
	//private CellStyle getHeaderRowStyle(Workbook wb){
	public CellStyle getHeaderRowStyle(Workbook wb){
		/*20-Oct-2014 WS: Ends*/
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	



	/*commented by paneendra for unused methods*/
	/*public String loadExcelToTempFolder(String absolutePath, String fileName) throws ExcelException, IOException{
		XSSFWorkbook workbook = null;

		//Changed by sriram to support cross-platform file separators (26-May-2017)
		workbook = new XSSFWorkbook(new FileInputStream(new File(absolutePath + "\\" + fileName + ".xlsx")));
		workbook = new XSSFWorkbook(new FileInputStream(new File(absolutePath + File.separator + fileName + ".xlsx")));
		//Changed by sriram to support cross-platform file separators (26-May-2017) ends
		logger.debug("Workbook loaded from {}\\{}.xlsx", absolutePath, fileName);


		File tempFile = null;

		try{
			tempFile = File.createTempFile(fileName + Utilities.getRawTimeStamp(),".xlsx");
			logger.debug("Temp file created");
		}catch(Exception e){
			logger.error("Could not create temp file");
			logger.error(e.getMessage(),e);
			throw new ExcelException("Could not load excel from path " + absolutePath + "\\" + fileName);
		}

		try{
			FileOutputStream fout = new FileOutputStream(tempFile);
			workbook.write(fout);
			//fileName = scriptId + ".xlsx";
			logger.debug("File saved on path {}", tempFile.getAbsolutePath());
		}catch(Exception e){
			logger.error("Could not save file on path {} because of an exception", tempFile.getAbsolutePath());
			logger.error(e.getMessage());
			throw new ExcelException("Could not load excel from path " + absolutePath + "\\" + fileName);
		}

		return tempFile.getAbsolutePath();
	}
*//*commented by paneendra for unused methods*/

	/*Added by paneendra for TENJINCG-1257 starts */
	public String loadUploadedExcelToTempFolder(String absolutePath, String fileName,int appId,String userId,Project project,TestDataPath testdatapath) throws ExcelException, IOException{
		XSSFWorkbook workbook = null;
		String password="";
		InputStream is=null;
		NPOIFSFileSystem pfs =null;
		String repoType=project.getRepoType();

		String awsPath="";

		FileUploadImpl fileuploadImpl = new FileUploadImpl();
		awsPath=project.getName()+"/"+testdatapath.getAppName()+"/"+testdatapath.getFunction()+"/"+userId+"/"+fileName+ ".xlsx";
		File tempFile = null;

		try{
			String directories = System.getProperty("java.io.tmpdir")+File.separator+userId+File.separator+fileName + Utilities.getRawTimeStamp()+".xlsx";
			//String directories = System.getProperty("java.io.tmpdir")+File.separator+userId+File.separator+Utilities.getRawTimeStamp();
			tempFile = new File(directories);
			tempFile.getParentFile().mkdirs();
			tempFile.createNewFile();
			logger.debug("Temp file created");
		}catch(Exception e){
			logger.error("Could not create temp file");
			logger.error(e.getMessage(),e);
			throw new ExcelException("Could not load excel from path " + absolutePath + "\\" + fileName);
		}finally{
			if(is!=null)
				is.close();
			if(pfs!=null)
				pfs.close();
		}

		try{
			password=new AutHelper().hydrateTemplatePassword(appId);
		}
		catch(Exception e){
			logger.debug("couldn't hydrate template password for appliation id {}",appId);
		}
		if(!password.equalsIgnoreCase("")){
			try{

				if(repoType.equalsIgnoreCase("Native")) {
					logger.info(" Repo type is Native ");
					pfs= new NPOIFSFileSystem(new File(absolutePath + File.separator + fileName + ".xlsx"),true);
				}else {
					logger.info(" Repo type is AWS S3 ");
					/*modified by shruthi for Tenj212-15 starts*/
					/*modified by paneendra for removing unused parameter starts*/
					File file=fileuploadImpl.downloadTestDataFileFromS3(project.getRepoRoot(),awsPath, tempFile.getPath());
					/*modified by paneendra for removing unused parameter ends*/
					/*modified by shruthi for Tenj212-15 ends*/
					pfs= new NPOIFSFileSystem((file),true);
				}
				EncryptionInfo info = new EncryptionInfo(pfs);
				Decryptor d = Decryptor.getInstance(info);

				/*Modified by paneendra for VAPT FIX starts*/
				/*password=new Crypto().decrypt(password.split("!#!")[1],decodeHex(password.split("!#!")[0].toCharArray()));*/
				password=new CryptoUtilities().decrypt(password);
				/*Modified by paneendra for VAPT FIX ends*/
				if (d.verifyPassword(password)) {
					is = d.getDataStream(pfs); 
					workbook= new XSSFWorkbook(is);
				}
				else{
					throw new ExcelException("Test Data Template and Application password did not match");
				}
			}catch(OfficeXmlFileException e){
				throw new ExcelException("Application has Test Data Template password");
			}
			catch(Exception e){
				throw new ExcelException(e.getMessage());
			}finally{
				if(is!=null)
					is.close();
				if(pfs!=null)
					pfs.close();
			}
		}else{

			try{
				if(repoType.equalsIgnoreCase("Native")) {
					logger.info(" Repo type is Native ");
					workbook = new XSSFWorkbook(new FileInputStream(new File(absolutePath + File.separator + fileName + ".xlsx")));
				}else {
					logger.info(" Repo type is AWS S3 ");
					/*modified by shruthi for Tenj212-15 starts*/
					/*modified by paneendra for removing unused parameter starts*/
					File file=fileuploadImpl.downloadTestDataFileFromS3(project.getRepoRoot(),awsPath, tempFile.getPath());
					/*modified by paneendra for removing unused parameter ends*/
					/*modified by shruthi for Tenj212-15 ends*/
					workbook = new XSSFWorkbook(new FileInputStream(file));

				}
			}
			catch(POIXMLException e1){

				throw new ExcelException("Test Data Template is password protected");
			}
			catch(Exception e){
				throw new ExcelException("Could not load test data file from " + absolutePath);
			}
			finally{
				if(is!=null)
					is.close();
				if(pfs!=null)
					pfs.close();
			}
		}

		logger.debug("Workbook loaded from {}\\{}.xlsx", absolutePath, fileName);


		try{
			FileOutputStream fout = new FileOutputStream(tempFile);
			workbook.write(fout);
			fout.close();
			logger.debug("File saved on path {}", tempFile.getAbsolutePath());
		}catch(Exception e){
			logger.error("Could not save file on path {} because of an exception", tempFile.getAbsolutePath());
			logger.error(e.getMessage());
			throw new ExcelException("Could not load excel from path " + absolutePath + "\\" + fileName);
		}finally{
			if(is!=null)
				is.close();
			if(pfs!=null)
				pfs.close();
		}

		return tempFile.getAbsolutePath();
	}

	/*Added by paneendra for TENJINCG-1257 ends */


	/*Added by paneendra for TENJINCG-1257 ends */


	/************************************************************************************************
	 * Method added by Sriram for performance tuning of Bulk Test Cases download (17-10-2015)
	 */
	/***************************************************************
	 * Added by Sriram to download individual / only a few test cases (Tenjin v2.2) 19-10-2015
	 */
	public void downloadTestCases(int projectId, String destination, String fileName) throws ExcelException{
		Connection projConn = null;
		Connection appConn = null;

		try{
			logger.debug("Getting projconn");
			projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			logger.debug("Done");
			logger.debug("Getting appconn");
			appConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			logger.debug("Done");
		}catch(Exception e){
			logger.error("ERROR connecting to database",e);
			throw new ExcelException("Could not download test case(s) due to an internal error");
		}

		ArrayList<TestCase> testCases = null;
		ArrayList<TestStep> steps = null;

		try{
			TestCaseHelper helper = new TestCaseHelper();
			logger.info("Fetching all test cases under project");
			testCases = helper.hydrateAllTestCases(projConn, projectId);
			logger.info("{} test cases found in all", testCases.size());

			logger.info("Processing test cases");
			steps = new ArrayList<TestStep>();
			for(TestCase tc:testCases){
				ArrayList<String> t = new ArrayList<String>();
				/*if(tc.getTcId().equalsIgnoreCase("Root")){
					//testCases.remove(tc);
				}*/
				t.add(tc.getTcId());
				t.add(tc.getTcName());
				t.add(tc.getTcDesc());
				t.add(tc.getTcType());
				/* Added by Poojalakshmi for TJN27-94 starts */
				t.add(tc.getTcAppName());
				/* Added by Poojalakshmi for TJN27-94 ends */
				t.add(tc.getTcType());
				t.add(tc.getTcPriority());

				/* Added by Roshni for Requirement TENJIN-192 starts */
				t.add(tc.getMode());
				/* Added by Roshni for Requirement TENJIN-192 ends */
				//added by Roshni for Requirement TENJINCG-190
				t.add(tc.getTcTestSetName());

				logger.debug("Fetching steps under test case {}", tc.getTcId());
				ArrayList<TestStep> stepsList = helper.hydrateStepsForTestCase(projConn, appConn, tc.getTcRecId());
				logger.debug("{} steps found", stepsList.size());
				logger.debug("Processing steps");
				if(stepsList.size() > 0){
					for(TestStep step:stepsList){
						step.setTestCaseName(tc.getTcId());
						steps.add(step);
					}
				}

			}
		}catch(Exception e){
			logger.error("ERROR hydrating test cases and steps for project {}", e);
			throw new ExcelException("Could not download test cases due to an internal error");
		}finally{
			try{
				projConn.close();
			}catch(Exception e){
				logger.warn("Could not close ProjConn after test case download",e);
			}

			try{
				appConn.close();
			}catch(Exception e){
				logger.warn("Could not close appConn after test case download",e);
			}

		}


		try{
			logger.info("Beginning writing to file");
			this.generateTestCasesExcelSheet(testCases, steps, destination, fileName);
			logger.info("Donwload created successfully");
		}catch(ExcelException e){
			throw new ExcelException(e.getMessage(),e);
		}


	}

	

	public void downloadTestCases(ArrayList<Integer> testCaseRecordIds, int projectId, String destination, String fileName) throws ExcelException{
		Connection projConn = null;
		Connection appConn = null;

		try{
			logger.debug("Getting projconn");
			projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			logger.debug("Done");
			logger.debug("Getting appconn");
			appConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			logger.debug("Done");
		}catch(Exception e){
			logger.error("ERROR connecting to database",e);
			throw new ExcelException("Could not download test case(s) due to an internal error");
		}

		ArrayList<TestCase> testCases = null;
		ArrayList<TestStep> steps = null;

		try{
			TestCaseHelper helper = new TestCaseHelper();
			logger.info("Fetching test case details");
			testCases = new ArrayList<TestCase>();
			for(int testCaseRecordId:testCaseRecordIds){
				TestCase testCase = helper.hydrateTestCase(projConn, appConn, projectId, testCaseRecordId);
				testCases.add(testCase);
			}
			logger.info("{} test cases found in all", testCases.size());

			logger.info("Processing test cases");
			steps = new ArrayList<TestStep>();
			for(TestCase tc:testCases){
				ArrayList<String> t = new ArrayList<String>();
				/*if(tc.getTcId().equalsIgnoreCase("Root")){
					//testCases.remove(tc);
				}*/
				t.add(tc.getTcId());
				t.add(tc.getTcName());
				t.add(tc.getTcDesc());
				/* Added by Poojalakshmi for TJN27-94 starts */
				t.add(tc.getTcAppName());
				/* Added by Poojalakshmi for TJN27-94 ends */
				t.add(tc.getTcType());
				t.add(tc.getTcPriority());

				//added by Roshni for Requirement TENJINCG-192
				/*commented by paneendra for removing EXE mode column in  template starts*/
				/*t.add(tc.getMode());*/
				/*commented by paneendra for removing EXE mode column in  template ends*/
				//added by Roshni for Requirement TENJINCG-190
				t.add(tc.getTcTestSetName());
				/*Added by Padmavathi for TJN252-62 starts*/
				t.add(tc.getTcPresetRules());
				/*Added by Padmavathi for TJN252-62 ends*/
				logger.debug("Fetching steps under test case {}", tc.getTcId());
				ArrayList<TestStep> stepsList = helper.hydrateStepsForTestCase(projConn, appConn, tc.getTcRecId());
				logger.debug("{} steps found", stepsList.size());
				logger.debug("Processing steps");
				if(stepsList.size() > 0){
					for(TestStep step:stepsList){
						step.setTestCaseName(tc.getTcId());
						steps.add(step);
					}
				}

			}
		}catch(Exception e){
			logger.error("ERROR hydrating test cases and steps for project {}", e);
			throw new ExcelException("Could not download test cases due to an internal error");
		}finally{
			try{
				projConn.close();
			}catch(Exception e){
				logger.warn("Could not close ProjConn after test case download",e);
			}

			try{
				appConn.close();
			}catch(Exception e){
				logger.warn("Could not close appConn after test case download",e);
			}

		}


		try{
			logger.info("Beginning writing to file");
			this.generateTestCasesExcelSheet(testCases, steps, destination, fileName);
			logger.info("Donwload created successfully");
		}catch(ExcelException e){
			throw new ExcelException(e.getMessage(),e);
		}


	}

	/*Added by Roshni fro TENJINCG-305 starts */
	public void downloadTestCasesForSet(int projectId, String destination, String fileName, int testsetid) throws ExcelException{
		Connection projConn = null;
		Connection appConn = null;

		try{
			logger.debug("Getting projconn");
			projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			logger.debug("Done");
			logger.debug("Getting appconn");
			appConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			logger.debug("Done");
		}catch(Exception e){
			logger.error("ERROR connecting to database",e);
			throw new ExcelException("Could not download test case(s) due to an internal error");
		}

		ArrayList<TestCase> testCases = null;
		ArrayList<TestStep> steps = null;

		try{
			TestSetHelper helper = new TestSetHelper();
			TestCaseHelper tcHelper=new TestCaseHelper();
			String testSetName=helper.getTestSetName(testsetid,projectId);
			logger.info("Fetching all test cases under project");
			testCases = helper.hydrateMappedTCs(testsetid, projectId);
			logger.info("{} test cases found in all", testCases.size());

			logger.info("Processing test cases");
			steps = new ArrayList<TestStep>();
			for(TestCase tc:testCases){
				tc.setTcTestSetName(testSetName);
				ArrayList<String> t = new ArrayList<String>();
				/*if(tc.getTcId().equalsIgnoreCase("Root")){
					//testCases.remove(tc);
				}*/
				t.add(tc.getTcId());
				t.add(tc.getTcName());
				t.add(tc.getTcDesc());
				t.add(tc.getTcType());
				t.add(tc.getTcPriority());

				/* Added by Roshni for Requirement TENJIN-192 starts */
				t.add(tc.getMode());
				/* Added by Roshni for Requirement TENJIN-192 ends */
				//added by Roshni for Requirement TENJINCG-190
				t.add(tc.getTcTestSetName());

				logger.debug("Fetching steps under test case {}", tc.getTcId());
				ArrayList<TestStep> stepsList = tcHelper.hydrateStepsForTestCase(projConn, appConn, tc.getTcRecId());
				logger.debug("{} steps found", stepsList.size());
				logger.debug("Processing steps");
				if(stepsList.size() > 0){
					for(TestStep step:stepsList){
						step.setTestCaseName(tc.getTcId());
						steps.add(step);
					}
				}

			}
		}catch(Exception e){
			logger.error("ERROR hydrating test cases and steps for project {}", e);
			throw new ExcelException("Could not download test cases due to an internal error");
		}finally{
			try{
				projConn.close();
			}catch(Exception e){
				logger.warn("Could not close ProjConn after test case download",e);
			}

			try{
				appConn.close();
			}catch(Exception e){
				logger.warn("Could not close appConn after test case download",e);
			}

		}


		try{
			logger.info("Beginning writing to file");
			this.generateTestCasesExcelSheet(testCases, steps, destination, fileName);
			logger.info("Donwload created successfully");
		}catch(ExcelException e){
			throw new ExcelException(e.getMessage(),e);
		}


	}
	/*Added by Roshni fro TENJINCG-305 ends */
	/***************************************************************
	 * Added by Sriram to download individual / only a few test cases (Tenjin v2.2) 19-10-2015 ends
	 */
	private void generateTestCasesExcelSheet(ArrayList<TestCase> testCases, ArrayList<TestStep> steps, String destination, String fileName) throws ExcelException{

		ArrayList<ArrayList<String>> masterTCList = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> masterStepList = new ArrayList<ArrayList<String>>();

		ArrayList<String> tcSheetHeader = new ArrayList<String>();
		tcSheetHeader.add("TEST_CASE_ID");
		tcSheetHeader.add("NAME");
		tcSheetHeader.add("DESCRIPTION");
		/* Added by Poojalakshmi for TJN27-94 starts */
		tcSheetHeader.add("APPLICATION");
		/* Added by Poojalakshmi for TJN27-94 ends */
		tcSheetHeader.add("TYPE");
		tcSheetHeader.add("PRIORITY");
		//added by Roshni for Requirement TENJINCG-192
		/*commented by paneendra for removing EXE mode column in  template starts*/
		/*tcSheetHeader.add("EXEC_MODE");*/
		/*commented by paneendra for removing EXE mode column in  template ends*/
		//Added by Roshni for Requirement TENJINCG-190
		/*Added by Padmavathi for TJN252-62 starts*/
		/*Modified by Prem for TNJNR2-17 starts*/
		/*tcSheetHeader.add("PRESET_RULES");*/
		/*commented by paneendra for removing DEFAULT DEPENDENCY RULES column in  template starts*/
		/*tcSheetHeader.add("DEFAULT DEPENDENCY RULES");*/
		/*commented by paneendra for removing DEFAULT DEPENDENCY RULES column in  template ends*/
		/*Modified by Prem for TNJNR2-17 Ends*/
		tcSheetHeader.add("TEST_SET_NAME");
		/*Added by Padmavathi for TJN252-62 ends*/
		masterTCList.add(tcSheetHeader);

		ArrayList<String> tsSheetHeader = new ArrayList<String>();
		tsSheetHeader.add("TEST_CASE_ID");
		tsSheetHeader.add("STEP_ID");
		tsSheetHeader.add("NAME");
		tsSheetHeader.add("APPLICATION");
		/*Modified by Preeti for TENJINCG-503 starts*/
		/*tsSheetHeader.add("FUNCTION");*/
		tsSheetHeader.add("FUNCTION/API CODE");
		//added by Roshni for Requirement TENJINCG-192
		/*tsSheetHeader.add("API_CODE");*/
		/*Modified by Preeti for TENJINCG-503 ends*/
		tsSheetHeader.add("TYPE");
		tsSheetHeader.add("TEST_DATA_ID");
		tsSheetHeader.add("MODE");
		tsSheetHeader.add("DESCRIPTION");
		tsSheetHeader.add("EXPECTED_RESULT");
		tsSheetHeader.add("OPERATION");
		tsSheetHeader.add("AUT_LOGIN_TYPE");

		//added by Roshni for Requirement TENJINCG-192
		tsSheetHeader.add("ROWS_TO_EXECUTE");
		/*Added by Padmavathi for TJN252-62 starts*/
		/*Modified by Prem for TNJNR2-17 starts*/
		//		tsSheetHeader.add("CUSTOM_RULES");
		tsSheetHeader.add("RULES");
		/*Modified by Prem for TNJNR2-17 Ends*/
		tsSheetHeader.add("ALWAYS_SHOW_FOR_RERUN");
		/*Added by Padmavathi for TJN252-62 ends*/
		masterStepList.add(tsSheetHeader);

		try{
			logger.info("Preparing test cases for download");
			for(TestCase tc:testCases){
				ArrayList<String> t = new ArrayList<String>();
				if(tc.getTcId().equalsIgnoreCase("Root")){
					continue;
				}
				t.add(tc.getTcId());
				t.add(tc.getTcName());
				t.add(tc.getTcDesc());
				/* Added by Poojalakshmi for TJN27-94 starts */
				t.add(tc.getTcAppName());
				/* Added by Poojalakshmi for TJN27-94 ends */
				t.add(tc.getTcType());



				t.add(tc.getTcPriority());
				//Added by Roshni
				/*commented by paneendra for removing EXE mode column in  template starts*/
				/*t.add(tc.getMode());*/
				/*commented by paneendra for removing EXE mode column in  template ends*/
				//added by Roshni for Requirement TENJINCG-190
				/*Added by Padmavathi for TJN252-62 starts*/
				/*commented by paneendra for removing DEFAULT DEPENDENCY RULES column in  template starts*/
				/*t.add(tc.getTcPresetRules());*/
				/*commented by paneendra for removing DEFAULT DEPENDENCY RULES column in  template ends*/
				/*Added by Padmavathi for TJN252-62 ends*/
				t.add(tc.getTcTestSetName());
				masterTCList.add(t);
			}
			logger.info("Successfully processed {} test cases", testCases.size());

			logger.info("Preparing steps for download");
			for(TestStep step:steps){
				ArrayList<String> s = new ArrayList<String>();
				s.add(step.getTestCaseName());
				s.add(step.getId());
				s.add(step.getShortDescription());
				s.add(step.getAppName());
				s.add(step.getModuleCode());
				/*Added by roshni for Requirement TENJINCG-192*/
				/*Commented by Preeti for TENJINCG-503 starts*/
				/*s.add(step.getApiCode());*/
				/*Commented by Preeti for TENJINCG-503 ends*/
				/*Added by roshni for Requirement TENJINCG-192*/
				s.add(step.getType());
				s.add(step.getDataId());
				s.add(step.getTxnMode());
				s.add(step.getDescription());
				s.add(step.getExpectedResult());
				s.add(step.getOperation());
				s.add(step.getAutLoginType());
				/*Added by roshni for Requirement TENJINCG-192*/
				s.add(String.valueOf(step.getRowsToExecute()));
				/*Added by roshni for Requirement TENJINCG-192*/
				/*Added by Padmavathi for TJN252-62 starts*/
				s.add(step.getCustomRules());
				s.add(step.getRerunStep());
				/*Added by Padmavathi for TJN252-62 ends*/
				masterStepList.add(s);
			}
			logger.info("Successfully processed {} speps", steps.size());
			logger.info("Writing to file");
			logger.debug("Creating workbook");
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet testCaseSheet = workbook.createSheet("Test Cases");

			logger.debug("Initializing fonts and styles");
			CellStyle cellStyle = this.getHeaderRowStyle(workbook);

			logger.debug("Create TEST CASE Sheet --> DONE");

			int rowId = 0;
			for(ArrayList<String> rowData:masterTCList){
				logger.debug("Writing TESTCASE data in row {}", rowId);
				int col=0;
				testCaseSheet.createRow(rowId);
				for(String s:rowData){
					testCaseSheet.getRow(rowId).createCell(col).setCellType(Cell.CELL_TYPE_STRING);
					testCaseSheet.getRow(rowId).getCell(col).setCellValue(s);
					if(rowId == 0){
						testCaseSheet.getRow(rowId).getCell(col).setCellStyle(cellStyle);
					}

					/*testCaseSheet.autoSizeColumn(col);*/
					col++;
				}

				rowId++;
			}

			int tcColCount = masterTCList.get(0).size();
			for(int i=0;i<tcColCount;i++){
				testCaseSheet.autoSizeColumn(i);
			}

			logger.debug("Written {} test cases to file", masterTCList.size()-1);

			XSSFSheet stepSheet = workbook.createSheet("Test Steps");
			logger.debug("Create TEST STEP sheet --> DONE");

			rowId = 0;
			for(ArrayList<String> rowData:masterStepList){
				logger.debug("Writing TESTSTEP data in row {}", rowId);
				int col=0;
				stepSheet.createRow(rowId);
				for(String s:rowData){
					stepSheet.getRow(rowId).createCell(col).setCellType(Cell.CELL_TYPE_STRING);
					stepSheet.getRow(rowId).getCell(col).setCellValue(s);
					if(rowId == 0){
						stepSheet.getRow(rowId).getCell(col).setCellStyle(cellStyle);
					}
					/*stepSheet.autoSizeColumn(col);*/
					col++;
				}
				rowId++;
			}

			int stepColCount = masterStepList.get(0).size();
			for(int i=0;i<stepColCount;i++){
				stepSheet.autoSizeColumn(i);
			}
			logger.debug("Written {} test steps to file", masterStepList.size()-1);

			logger.info("Saving file to disk");
			if(fileName != null && !fileName.endsWith(".xlsx")){
				fileName = fileName + ".xlsx";
			}else if(fileName == null || fileName.equalsIgnoreCase("")){
				fileName = "Tenjin_TestCases_Download.xlsx";
			}
			/*Changed by Pushpalatha for TENJINCG-567 starts*/
			FileOutputStream fout = new FileOutputStream(destination + File.separator+ fileName);
			/*Changed by Pushpalatha for TENJINCG-567 ends*/
			workbook.write(fout);
			fout.close();

			logger.info("Done");
		}catch(Exception e){
			logger.error("ERROR while generating report",e);
			throw new ExcelException("Could not download test case(s) due to an internal error");
		}



	}




	//Added by Sriram for Export Report Functionality
	public String generateReportSpreadsheet(String reportType, String jsonData, String folderPath) throws ExcelException {
		String fileName="";
		if(Utilities.trim(reportType).equalsIgnoreCase("aut")){
			fileName = "Aut_Wise_Execution_Report.xlsx";
		}else{
			fileName = "User_Wise_Execution_Report.xlsx";
		}

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Execution Report");

		int dataRow=2;
		CellStyle hStyle = this.getHeaderRowStyle(workbook);

		Row hRow = sheet.createRow(0);
		Row hRow1 = sheet.createRow(1);
		if(Utilities.trim(reportType).equalsIgnoreCase("aut")){
			hRow.createCell(0).setCellValue("Application");
			sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 0));
			hRow.createCell(1).setCellValue("Function");
			sheet.addMergedRegion(new CellRangeAddress(0, 1, 1, 1));
			hRow.createCell(2).setCellValue("Transaction Status");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 2, 6));
			hRow.createCell(7).setCellValue("Validation Status");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 7, 10));
			hRow1.createCell(2).setCellValue("Total Steps");
			hRow1.createCell(3).setCellValue("Executed");
			hRow1.createCell(4).setCellValue("Passed");
			hRow1.createCell(5).setCellValue("Failed");
			hRow1.createCell(6).setCellValue("Percentage");
			hRow1.createCell(7).setCellValue("Total Validations");
			hRow1.createCell(8).setCellValue("Passed");
			hRow1.createCell(9).setCellValue("Failed");
			hRow1.createCell(10).setCellValue("Percentage");
		}else if(Utilities.trim(reportType).equalsIgnoreCase("user")){
			hRow.createCell(0).setCellValue("User");
			sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 0));
			hRow.createCell(1).setCellValue("Application");
			sheet.addMergedRegion(new CellRangeAddress(0, 1, 1, 1));
			hRow.createCell(2).setCellValue("Transaction Status");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 2, 6));
			hRow.createCell(7).setCellValue("Validation Status");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 7, 10));
			hRow1.createCell(2).setCellValue("Total Steps");
			hRow1.createCell(3).setCellValue("Executed");
			hRow1.createCell(4).setCellValue("Passed");
			hRow1.createCell(5).setCellValue("Failed");
			hRow1.createCell(6).setCellValue("Percentage");
			hRow1.createCell(7).setCellValue("Total Validations");
			hRow1.createCell(8).setCellValue("Passed");
			hRow1.createCell(9).setCellValue("Failed");
			hRow1.createCell(10).setCellValue("Percentage");
			hRow.createCell(11).setCellValue("Defects Logged");
			sheet.addMergedRegion(new CellRangeAddress(0, 1, 11, 11));
		}


		Iterator<Cell> cellIter = hRow.cellIterator();
		while(cellIter.hasNext()){
			Cell cell = cellIter.next();
			cell.setCellStyle(hStyle);
		}

		cellIter = hRow1.cellIterator();
		while(cellIter.hasNext()){
			Cell cell = cellIter.next();
			cell.setCellStyle(hStyle);
		}

		reportType=Utilities.trim(reportType);
		try {
			JSONArray jArray = new JSONArray(jsonData);
			for(int i=0; i<jArray.length(); i++){
				JSONObject json = jArray.getJSONObject(i);
				sheet.createRow(dataRow);
				if(reportType.equalsIgnoreCase("aut")){
					sheet.getRow(dataRow).createCell(0).setCellValue(json.getString("application"));
					String fCode = "All";
					try{
						fCode = json.getString("functioncode");
					}catch(JSONException e){
						logger.debug("functioncode not found in json. using All");
					}
					sheet.getRow(dataRow).createCell(1).setCellValue(fCode);
				}else if(reportType.equalsIgnoreCase("user")){
					sheet.getRow(dataRow).createCell(0).setCellValue(json.getString("userid"));
					sheet.getRow(dataRow).createCell(1).setCellValue(json.getString("application"));
				}

				sheet.getRow(dataRow).createCell(2).setCellValue(json.getString("totaltc"));
				sheet.getRow(dataRow).createCell(3).setCellValue(json.getString("executed"));
				sheet.getRow(dataRow).createCell(4).setCellValue(json.getString("passedtc"));
				sheet.getRow(dataRow).createCell(5).setCellValue(json.getString("failedtc"));
				sheet.getRow(dataRow).createCell(6).setCellValue(json.getString("percentage") + "%");
				sheet.getRow(dataRow).createCell(7).setCellValue(json.getString("totalvals"));
				sheet.getRow(dataRow).createCell(8).setCellValue(json.getString("passvals"));
				sheet.getRow(dataRow).createCell(9).setCellValue(json.getString("failedvals"));
				sheet.getRow(dataRow).createCell(10).setCellValue(json.getString("valpercent") + "%");

				if(reportType.equalsIgnoreCase("user")){
					sheet.getRow(dataRow).createCell(11).setCellValue(json.getString("defectslogged"));
				}

				dataRow++;
			}
		} catch (JSONException e) {
			
			logger.error("ERROR occured while generating Report Excel", e);
			throw new ExcelException("Could not export report due to an internal error. Please contact Tenjin Support.");
		}
		FileOutputStream fout=null;
		/*Changed by Pushpalatha for TENJINCG-583 starts*/
		try{

			 fout = new FileOutputStream(folderPath + File.separator + fileName);
			workbook.write(fout);
			//fileName = scriptId + ".xlsx";
			logger.debug("File saved on path " + folderPath + File.separator + fileName);
		}catch(Exception e){
			logger.error("Could not save file on path " + folderPath + File.separator + fileName + " because of an exception");
			logger.error(e.getMessage());
			throw new ExcelException("Could not save generated Excel file to disk",e);
		}finally {
			try {
				fout.close();
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
		/*Changed by Pushpalatha for TENJINCG-583 endss*/
		return fileName;
	}
	/*Added by Preeti for TENJINCG-970 starts*/
	public void processCaseImportFromOtherProjectSheet(Map<TestCase,String> testCases, Map<TestStep, String> steps, String destination, String fileName) throws ExcelException{

		ArrayList<ArrayList<String>> masterTCList = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> masterStepList = new ArrayList<ArrayList<String>>();

		ArrayList<String> tcSheetHeader = new ArrayList<String>();
		tcSheetHeader.add("TEST_CASE_ID");
		tcSheetHeader.add("NAME");
		tcSheetHeader.add("DESCRIPTION");
		tcSheetHeader.add("TYPE");
		tcSheetHeader.add("PRIORITY");
		/*commented by shruthi for TCGST-52 issue starts*/
		/*tcSheetHeader.add("EXEC_MODE");*/
		/*Modified by Prem for TNJNR2-17 starts*/
		//		tcSheetHeader.add("PRESET_RULES");
		/*tcSheetHeader.add("DEFAULT DEPENDENCY RULES");*/
		/*Modified by Prem for TNJNR2-17 Ends*/
		/*commented by shruthi for TCGST-52 issue ends*/
		tcSheetHeader.add("TEST_SET_NAME");
		tcSheetHeader.add("IMPORT STATUS");
		masterTCList.add(tcSheetHeader);

		ArrayList<String> tsSheetHeader = new ArrayList<String>();
		tsSheetHeader.add("TEST_CASE_ID");
		tsSheetHeader.add("STEP_ID");
		tsSheetHeader.add("NAME");
		tsSheetHeader.add("APPLICATION");
		tsSheetHeader.add("FUNCTION/API CODE");
		tsSheetHeader.add("TYPE");
		tsSheetHeader.add("TEST_DATA_ID");
		tsSheetHeader.add("MODE");
		tsSheetHeader.add("DESCRIPTION");
		tsSheetHeader.add("EXPECTED_RESULT");
		tsSheetHeader.add("OPERATION");
		tsSheetHeader.add("AUT_LOGIN_TYPE");
		tsSheetHeader.add("ROWS_TO_EXECUTE");
		/*Modified by Prem for TNJNR2-17 starts*/
		//tsSheetHeader.add("CUSTOM_RULES");
		tsSheetHeader.add("RULES");
		/*Modified by Prem for TNJNR2-17 Ends*/
		tsSheetHeader.add("ALWAYS_SHOW_FOR_RERUN");
		tsSheetHeader.add("IMPORT STATUS");
		masterStepList.add(tsSheetHeader);

		try{
			logger.info("Preparing test cases for download");
			for(Entry<TestCase, String> entry : testCases.entrySet()){
				TestCase tc = entry.getKey();
				ArrayList<String> t = new ArrayList<String>();
				if(tc.getTcId().equalsIgnoreCase("Root")){
					continue;
				}
				t.add(tc.getTcId());
				t.add(tc.getTcName());
				t.add(tc.getTcDesc());
				t.add(tc.getTcType());
				t.add(tc.getTcPriority());
				/*commented by shruthi for TCGST-52 issue starts*/
				/*t.add(tc.getMode());*/
				/*t.add(tc.getTcPresetRules());*/
				/*commented by shruthi for TCGST-52 issue ends*/
				t.add(tc.getTcTestSetName());
				t.add(entry.getValue());
				masterTCList.add(t);
			}
			logger.info("Successfully processed {} test cases", testCases.size());

			logger.info("Preparing steps for download");
			for(Entry<TestStep, String> entry : steps.entrySet()){
				TestStep step = entry.getKey();
				ArrayList<String> s = new ArrayList<String>();
				s.add(step.getTestCaseName());
				s.add(step.getId());
				s.add(step.getShortDescription());
				s.add(step.getAppName());
				s.add(step.getModuleCode());
				s.add(step.getType());
				s.add(step.getDataId());
				s.add(step.getTxnMode());
				s.add(step.getDescription());
				s.add(step.getExpectedResult());
				s.add(step.getOperation());
				s.add(step.getAutLoginType());
				s.add(String.valueOf(step.getRowsToExecute()));
				s.add(step.getCustomRules());
				s.add(step.getRerunStep());
				s.add(entry.getValue());
				masterStepList.add(s);
			}
			logger.info("Successfully processed {} steps", steps.size());
			logger.info("Writing to file");
			logger.debug("Creating workbook");
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet testCaseSheet = workbook.createSheet("Test Cases");

			logger.debug("Initializing fonts and styles");
			CellStyle cellStyle = this.getHeaderRowStyle(workbook);

			logger.debug("Create TEST CASE Sheet --> DONE");

			int rowId = 0;
			for(ArrayList<String> rowData:masterTCList){
				logger.debug("Writing TESTCASE data in row {}", rowId);
				int col=0;
				testCaseSheet.createRow(rowId);
				for(String s:rowData){
					testCaseSheet.getRow(rowId).createCell(col).setCellType(Cell.CELL_TYPE_STRING);
					testCaseSheet.getRow(rowId).getCell(col).setCellValue(s);
					if(rowId == 0){
						testCaseSheet.getRow(rowId).getCell(col).setCellStyle(cellStyle);
					}
					col++;
				}

				rowId++;
			}

			int tcColCount = masterTCList.get(0).size();
			for(int i=0;i<tcColCount;i++){
				testCaseSheet.autoSizeColumn(i);
			}

			logger.debug("Written {} test cases to file", masterTCList.size()-1);

			XSSFSheet stepSheet = workbook.createSheet("Test Steps");
			logger.debug("Create TEST STEP sheet --> DONE");

			rowId = 0;
			for(ArrayList<String> rowData:masterStepList){
				logger.debug("Writing TESTSTEP data in row {}", rowId);
				int col=0;
				stepSheet.createRow(rowId);
				for(String s:rowData){
					stepSheet.getRow(rowId).createCell(col).setCellType(Cell.CELL_TYPE_STRING);
					stepSheet.getRow(rowId).getCell(col).setCellValue(s);
					if(rowId == 0){
						stepSheet.getRow(rowId).getCell(col).setCellStyle(cellStyle);
					}
					col++;
				}
				rowId++;
			}

			int stepColCount = masterStepList.get(0).size();
			for(int i=0;i<stepColCount;i++){
				stepSheet.autoSizeColumn(i);
			}
			logger.debug("Written {} test steps to file", masterStepList.size()-1);

			logger.info("Saving file to disk");
			if(fileName != null && !fileName.endsWith(".xlsx")){
				fileName = fileName + ".xlsx";
			}else if(fileName == null || fileName.equalsIgnoreCase("")){
				fileName = "Tenjin_TestCases_Download.xlsx";
			}
			FileOutputStream fout = new FileOutputStream(destination + File.separator+ fileName);
			workbook.write(fout);
			fout.close();

			logger.info("Done");
		}catch(Exception e){
			logger.error("ERROR while generating report",e);
			throw new ExcelException("Could not import test case(s) due to an internal error");
		}
	}
	/*Added by Preeti for TENJINCG-970 ends*/
	/*Added by Roshni fro TENJINCG-305 starts */
	/*Modified by Padmavathi for 	T251IT-93 starts*/
	/*public String processTestCaseUploadSheet(String folderPath, String fileNameWithoutExtension, int projectId, String mode,
			String setName) throws ExcelException {*/
	public String processTestCaseUploadSheet(String folderPath, String fileNameWithoutExtension, int projectId, String mode,
			String setName,String createdBy,String setId) throws ExcelException, RequestValidationException {
		/*Modified by Padmavathi for 	T251IT-93 ends*/
		Connection appConn = null;
		Connection projConn = null;
		String uploadStatus = "";
		try {
			appConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		} catch (Exception e) {
			throw new ExcelException(e.getMessage(), e);
		}

		Map<String, String> autMap = new HashMap<String, String>();
		int rootTestCaseRecId = -1;
		FileOutputStream fout=null;
		try {
			/*Changed by Pushpalatha for TENJINCG-583 starts*/
			FileInputStream file = new FileInputStream(
					new File(folderPath + File.separator + fileNameWithoutExtension + ".xlsx"));
			/*Changed by Pushpalatha for TENJINCG-583 ends*/
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);

			CellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
			cellStyle.setFillPattern(CellStyle.BIG_SPOTS);
			Font hSSFFont = workbook.createFont();
			hSSFFont.setFontName("Calibri");
			hSSFFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
			/*Modified by Padmavathi for TJN252-62 starts*/
			/*sheet.getRow(0).createCell(7).setCellStyle(cellStyle);
			sheet.getRow(0).getCell(7).setCellValue("UPLOAD_STATUS");
			sheet.autoSizeColumn(7);*/
			sheet.getRow(0).createCell(8).setCellStyle(cellStyle);
			sheet.getRow(0).getCell(8).setCellValue("UPLOAD_STATUS");
			sheet.autoSizeColumn(8);
			/*Modified by Padmavathi for TJN252-62 ends*/
			cellStyle.setFont(hSSFFont);
			;

			int startRow = 1;
			int currentRow = startRow;
			int tcIdCol = 0;
			int tcNameCol = 1;
			int tcDescCol = 2;
			int tcTypeCol = 3;
			int tcPriorityCol = 4;
			int tcExecMode = 5;
			/*Added by Padmavathi for TJN252-62 starts*/
			int tcPresetRules=6;
			/*Added by Padmavathi for TJN252-62 ends*/
			int tsIdCol = 1;
			int tsNameCol = 2;
			int tsAppCol = 3;
			int tsModuleCol = 4;
			int tsTypeCol = 5;
			int tsDataIdCol = 6;
			int tsModeCol = 7;
			int tsDescCol = 8;
			int tsExpResultCol = 9;
			int tsOperationCol = 10;
			int tsAutLoginTypeCol = 11;
			int tsRowsToExecute = 12;
			/*Added by Padmavathi for TJN252-62 starts*/
			int tsCustomRules = 13;
			int tsAlwaysShowForRerun=14;
			/*Added by Padmavathi for TJN252-62 ends*/
			/*Commented by Preeti for TENJINCG-503 starts*/
			/*int tsApiCol = 5;*/
			/*Commented by Preeti for TENJINCG-503 ends*/
			int tcCount = 0;
			Map<String, String> modeValueMap = new HashMap<String, String>();
			ArrayList<String> testCaseIds = new ArrayList<String>();
			ArrayList<TestCase> testCases = new ArrayList<TestCase>();
			logger.info("Processing Test Cases");
			/*Added By Ashiki for V2.8-70 starts*/
			this.validateTestcaseFields(sheet,"testcase");
			/*Added By Ashiki for V2.8-70 end*/
			/*  Added by Padmavathi for T251IT-59 Starts*/
			TestSetHelper set = new TestSetHelper();
			TestSet tset = set.hydrateTestSet(Integer.parseInt(setId), projectId);
			if(tset!=null)
				setName=tset.getName();
			/*  Added by Padmavathi for T251IT-59 ends*/
			/*Added By Ashiki for V2.8-70 starts*/
			if(sheet.getRow(currentRow).getCell(tcIdCol)==null||Utilities.trim(sheet.getRow(currentRow).getCell(tcIdCol).toString()).equals("")) {
				sheet.getRow(currentRow).createCell(8).setCellValue("Test case id is mandatory.");
				uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
				currentRow++;
			}else {
				sheet.getRow(currentRow).getCell(tcIdCol).setCellType(Cell.CELL_TYPE_STRING);
			}
			/*Added By Ashiki for V2.8-70 ends*/


			while (sheet.getRow(currentRow) != null && (sheet.getRow(currentRow).getCell(tcIdCol) != null
					&& sheet.getRow(currentRow).getCell(tcIdCol).getStringCellValue() != null
					&& !sheet.getRow(currentRow).getCell(tcIdCol).getStringCellValue().equalsIgnoreCase(""))) {
				tcCount++;
				Row cRow = sheet.getRow(currentRow);
				TestCase t = new TestCase();
				t.setTcId(cRow.getCell(tcIdCol).getStringCellValue());
				testCaseIds.add(t.getTcId());
				/*Modified By Ashiki for V2.8-70 starts*/
				/*try {*/
				if(sheet.getRow(currentRow).getCell(tcNameCol)==null||Utilities.trim(sheet.getRow(currentRow).getCell(tcNameCol).toString()).equals("")) {
					sheet.getRow(currentRow).createCell(8).setCellValue("Test case Name is mandatory.");
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					currentRow++;
					continue;
				}else {
					sheet.getRow(currentRow).getCell(tcNameCol).setCellType(Cell.CELL_TYPE_STRING);
					t.setTcName(cRow.getCell(tcNameCol).getStringCellValue());	
				}

				/*} catch (NullPointerException e) {
					logger.warn("Test Case Name NULL for " + t.getTcId());
				}*/
				/*Modified By Ashiki for V2.8-70 ends*/
				try {
					sheet.getRow(currentRow).getCell(tcDescCol).setCellType(Cell.CELL_TYPE_STRING);
					t.setTcDesc(cRow.getCell(tcDescCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Case Description NULL for " + t.getTcId());
				}
				try {
					sheet.getRow(currentRow).getCell(tcTypeCol, Row.CREATE_NULL_AS_BLANK)
					.setCellType(Cell.CELL_TYPE_STRING);
					if (cRow.getCell(tcTypeCol).getStringCellValue().equals("")) {
						t.setTcType("Acceptance");
					} else {
						t.setTcType(cRow.getCell(tcTypeCol).getStringCellValue());
					}
				} catch (NullPointerException e) {
					logger.warn("Test Case Type NULL for " + t.getTcId());
				}
				try {
					sheet.getRow(currentRow).getCell(tcPriorityCol).setCellType(Cell.CELL_TYPE_STRING);
					t.setTcPriority(cRow.getCell(tcPriorityCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Case Priority NULL for " + t.getTcId());
				}
				try {
					/*
					 * t.setMode(mode); modeValueMap.put(t.getTcId(), mode);
					 */
					String tcMode="";
					sheet.getRow(currentRow).getCell(tcExecMode, Row.CREATE_NULL_AS_BLANK)
					.setCellType(Cell.CELL_TYPE_STRING);
					String modeValue = cRow.getCell(tcExecMode).getStringCellValue();
					if (Utilities.trim(modeValue).contentEquals("") || !modeValue.equalsIgnoreCase("API")
							|| modeValue == null) {
						t.setMode("GUI");
					} else {
						t.setMode(modeValue.toUpperCase());
					}
					tcMode = t.getMode();
					modeValueMap.put(t.getTcId(), tcMode);
				} catch (NullPointerException e) {
					logger.warn("Test Case Execution Mode NULL for " + t.getTcId());
				}
				/*Added by Padmavathi for TJN252-62 starts*/
				try {
					sheet.getRow(currentRow).getCell(tcPresetRules).setCellType(Cell.CELL_TYPE_STRING);
					t.setTcPresetRules((Utilities.trim(cRow.getCell(tcPresetRules).getStringCellValue())));
				} catch (NullPointerException e) {
					logger.warn("Test Case preset rules NULL for " + t.getTcId());
				}
				/*Added by Padmavathi for TJN252-62 ends*/
				try {
					t.setTcTestSetName(setName);
				} catch (NullPointerException e) {
					logger.warn("Test Case test set name(s) NULL for " + t.getTcId());
				}
				try {
					t.setStorage("Local");
				} catch (NullPointerException e) {
					logger.warn("Test Case Storage NULL for " + t.getTcId());
				}
				t.setUploadRow(currentRow);
				testCases.add(t);

				currentRow++;
				if (sheet.getRow(currentRow) != null) {
					sheet.getRow(currentRow).getCell(tcIdCol).setCellType(Cell.CELL_TYPE_STRING);
				}
			}

			logger.info(testCases.size() + " test cases were found");
			logger.info("Processing Test Steps");
			XSSFSheet stepSheet = workbook.getSheetAt(1);
			hSSFFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
			hSSFFont.setFontName("Calibri");

			/***Changed column index value from 14 to 13: API Code removed from sheet***/
			/*Modified by Padmavathi for TJN252-62 starts*/
			/*stepSheet.getRow(0).createCell(13).setCellStyle(cellStyle);
			stepSheet.getRow(0).getCell(13).setCellValue("UPLOAD_STATUS");
			stepSheet.autoSizeColumn(13);*/
			stepSheet.getRow(0).createCell(15).setCellStyle(cellStyle);
			stepSheet.getRow(0).getCell(15).setCellValue("UPLOAD_STATUS");
			stepSheet.autoSizeColumn(15);
			/*Modified by Padmavathi for TJN252-62 ends*/
			cellStyle.setFont(hSSFFont);

			currentRow = startRow;
			/*Added By Ashiki for V2.8-70 starts*/
			validateTestStepFields(stepSheet);
			/*Added By Ashiki for V2.8-70 end*/
			ArrayList<TestStep> steps = new ArrayList<TestStep>();
			Multimap<String, TestStep> stepMap = ArrayListMultimap.create();
			while (stepSheet.getRow(currentRow) != null && stepSheet.getRow(currentRow).getCell(tcIdCol) != null) {
				stepSheet.getRow(currentRow).getCell(tcIdCol).setCellType(Cell.CELL_TYPE_STRING);

				if (stepSheet.getRow(currentRow).getCell(tcIdCol).getStringCellValue() == null
						|| stepSheet.getRow(currentRow).getCell(tcIdCol).getStringCellValue().equalsIgnoreCase("")) {
					break;
				}
				Row cRow = stepSheet.getRow(currentRow);
				TestStep step = new TestStep();
				step.setParentTestCaseId(cRow.getCell(tcIdCol).getStringCellValue());

				try {
					stepSheet.getRow(currentRow).getCell(tsIdCol).setCellType(Cell.CELL_TYPE_STRING);
					step.setId(cRow.getCell(tsIdCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Step ID is NULL for test step");
				}

				try {
					stepSheet.getRow(currentRow).getCell(tsNameCol).setCellType(Cell.CELL_TYPE_STRING);
					step.setShortDescription(cRow.getCell(tsNameCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Step Name is NULL for test step " + step.getId());
				}

				try {
					stepSheet.getRow(currentRow).getCell(tsAppCol).setCellType(Cell.CELL_TYPE_STRING);
					step.setAppName(cRow.getCell(tsAppCol).getStringCellValue());
					if (!autMap.keySet().contains(step.getAppName())) {
						logger.debug("Getting APP ID for " + step.getAppName());
						int oId = new AutHelper().getAutId(appConn, step.getAppName());
						autMap.put(step.getAppName(), Integer.toString(oId));
						step.setAppId(oId);
					} else {

						step.setAppId(Integer.parseInt(autMap.get(step.getAppName())));
					}
				} catch (NullPointerException e) {
					logger.warn("Test Step AUT is NULL for test step " + step.getId());
				}
				try {
					stepSheet.getRow(currentRow).getCell(tsModuleCol).setCellType(Cell.CELL_TYPE_STRING);
					step.setModuleCode(cRow.getCell(tsModuleCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Step Module is NULL for test step " + step.getId());
				}
				try {
					stepSheet.getRow(currentRow).getCell(tsTypeCol).setCellType(Cell.CELL_TYPE_STRING);
					step.setType(cRow.getCell(tsTypeCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Step Type is NULL for test step " + step.getId());
				}
				try {
					stepSheet.getRow(currentRow).getCell(tsDataIdCol).setCellType(Cell.CELL_TYPE_STRING);
					step.setDataId(cRow.getCell(tsDataIdCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Step Data ID is NULL for test step " + step.getId());
				}
				try {
					stepSheet.getRow(currentRow).getCell(tsModeCol).setCellType(Cell.CELL_TYPE_STRING);
					for (String tcid : testCaseIds) {
						if (stepSheet.getRow(currentRow).getCell(tcIdCol).getStringCellValue().equals(tcid)) {
							step.setTxnMode(modeValueMap.get(tcid));
							break;
						}
					}

				} catch (NullPointerException e) {
					logger.warn("Test Step Mode is NULL for test step " + step.getId());
				}
				try {
					stepSheet.getRow(currentRow).getCell(tsDescCol).setCellType(Cell.CELL_TYPE_STRING);
					step.setDescription(cRow.getCell(tsDescCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Step Description is NULL for test step " + step.getId());
				}
				try {
					stepSheet.getRow(currentRow).getCell(tsExpResultCol).setCellType(Cell.CELL_TYPE_STRING);
					step.setExpectedResult(cRow.getCell(tsExpResultCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Step Expected Result is NULL for test step " + step.getId());
				}
				//fix for expected result in burgan
				finally{
					if(step.getExpectedResult()==null){
						step.setExpectedResult("");
					}
				}
				try {
					stepSheet.getRow(currentRow).getCell(tsOperationCol).setCellType(Cell.CELL_TYPE_STRING);
					step.setOperation(cRow.getCell(tsOperationCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Step Operation is NULL for test step " + step.getId());
				}

				try {
					stepSheet.getRow(currentRow).getCell(tsAutLoginTypeCol).setCellType(Cell.CELL_TYPE_STRING);
					step.setAutLoginType(cRow.getCell(tsAutLoginTypeCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Step AUT Login Type is NULL for test step " + step.getId());
				}
				try {
					cRow.getCell(tsRowsToExecute).setCellType(Cell.CELL_TYPE_STRING);
					String r = cRow.getCell(tsRowsToExecute).getStringCellValue();
					int rowsToExecute = Integer.parseInt(r);
					step.setRowsToExecute(rowsToExecute);
				} catch (Exception e) {
					logger.warn("Could not process Rows To Execute value for step {}", step.getId());
				}
				/*Added by Padmavathi for TJN252-62 starts*/
				try{
					stepSheet.getRow(currentRow).getCell(tsCustomRules).setCellType(Cell.CELL_TYPE_STRING);
					step.setCustomRules(Utilities.trim(cRow.getCell(tsCustomRules).getStringCellValue()));
				}catch(NullPointerException e){
					logger.warn("Test Step customrule is NULL for test step " + step.getId());
				}
				try{
					stepSheet.getRow(currentRow).getCell(tsAlwaysShowForRerun).setCellType(Cell.CELL_TYPE_STRING);
					step.setRerunStep(Utilities.trim(cRow.getCell(tsAlwaysShowForRerun).getStringCellValue()));
				}catch(NullPointerException e){
					logger.warn("Test Step always show for rerun is NULL for test step " + step.getId());
				}
				/*Added by Padmavathi for TJN252-62 ends*/
				/*Commented by Preeti for TENJINCG-503 starts*/
				/*try {
					stepSheet.getRow(currentRow).getCell(tsApiCol).setCellType(Cell.CELL_TYPE_STRING);
					step.setApiCode(cRow.getCell(tsApiCol).getStringCellValue());
					step.setModuleCode(cRow.getCell(tsApiCol).getStringCellValue());
				} catch (NullPointerException e) {
					logger.warn("Test Step AUT Login Type is NULL for test step " + step.getId());
				}*/
				/*Commented by Preeti for TENJINCG-503 ends*/
				try {
					step.setTstepStorage("Local");
				} catch (NullPointerException e) {
					logger.warn("Test Step Storage is NULL for test step " + step.getId());
				}
				step.setUploadRow(currentRow);
				steps.add(step);
				stepMap.put(step.getParentTestCaseId(), step);
				currentRow++;
			}

			logger.debug(steps.size() + " test steps were found");

			logger.info("Persisting Test Cases");
			TestCaseHelper helper = new TestCaseHelper();
			/* commented by Padmavathi for T251IT-59 Starts*/
			/*TestSetHelper set = new TestSetHelper();
			TestSet tset = set.hydrateTestSet(setName, projectId);*/
			/* commented by Padmavathi for T251IT-59 ends*/
			int cnt = 0;
			for (TestCase t : testCases) {
				cnt++;
				logger.info("Persisting Test Case " + cnt + " out of " + testCases.size());
				try {
					if (helper.doesTestCaseExist(projConn, projectId, t.getTcId())) {
						TestCase tcase = helper.hydrateTestCase(projectId, t.getTcId());
						if (set.existMappedTestCase(tset.getId(), projectId, tcase.getTcRecId())) {
							logger.error(
									"Test Case with ID {} already exists for Project {} and already mapped to the TestSet{}",
									t.getTcId(), projectId, setName);
							/*Modified by Padmavathi for TJN252-62 starts*/
							/*sheet.getRow(t.getUploadRow()).createCell(7)
									.setCellValue("Record Already Exists and already mapped to the testset");*/
							sheet.getRow(t.getUploadRow()).createCell(8)
							.setCellValue("Record Already Exists and already mapped to the testset");
							/*sheet.getRow(t.getUploadRow()).createCell(6)
							.setCellValue(setName);*/
							sheet.getRow(t.getUploadRow()).createCell(7)
							.setCellValue(setName);
							/*Modified by Padmavathi for TJN252-62 ends*/
							uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
							continue;
						} else {
							/*Commented by Pushpa for Tenj211-6 starts*/
							/*if (tset.getMode().equalsIgnoreCase(tcase.getMode())) {*/
							/*Commented by Pushpa for Tenj211-6 ends*/
							set.persistTestSetMap(tset.getId(), Integer.toString(tcase.getTcRecId()), projectId,
									false);
							logger.error(
									"Test Case with ID {} already exists for Project {} and mapped to the TestSet{}",
									t.getTcId(), projectId, setName);
							/*Modified by Padmavathi for TJN252-62 starts*/
							/*Changed by Ashiki for TJN27-38 starts*/
							/*sheet.getRow(t.getUploadRow()).createCell(7)
										.setCellValue("Record Already Exists and Mapped to the Testset");
								sheet.getRow(t.getUploadRow()).createCell(6)
								.setCellValue(setName);*/
							sheet.getRow(t.getUploadRow()).createCell(8)
							.setCellValue("TestCase Mapped to the Testset successfully.");
							/*Changed by Ashiki for TJN27-38 ends*/
							sheet.getRow(t.getUploadRow()).createCell(7)
							.setCellValue(setName);
							/*Modified by Padmavathi for TJN252-62 ends*/
							uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
							continue;
							/*Commented by Pushpa for Tenj211-6 starts*/
							/*} else {
								logger.error(
										"Test Case with ID {} already exists for Project {} and mode is different from TestSet{} mode",
										t.getTcId(), projectId, setName);
								Modified by Padmavathi for TJN252-62 starts
								sheet.getRow(t.getUploadRow()).createCell(7)
										.setCellValue("Record already exists and its not mapped to the testset as the mode of both is not same");
								sheet.getRow(t.getUploadRow()).createCell(6)
								.setCellValue(" ");
								sheet.getRow(t.getUploadRow()).createCell(8)
								.setCellValue("Record already exists and its not mapped to the testset as the mode of both is not same");
						sheet.getRow(t.getUploadRow()).createCell(7)
						.setCellValue(" ");
						Modified by Padmavathi for TJN252-62 ends
								uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
								continue;
							}*/
							/*Commented by Pushpa for Tenj211-6 ends*/
						}
					}
					t.setProjectId(projectId);
					t.setRecordType("TC");
					/*Modified by Padmavathi for 	T251IT-93 starts*/
					/*String createdBy1 = helper.projOwnerName(projConn, t);*/
					/*t.setTcCreatedBy(createdBy1);*/
					t.setTcCreatedBy(createdBy);
					/*Modified by Padmavathi for 	T251IT-93 ends*/
					t.setTcFolderId(rootTestCaseRecId);

					helper.persistTestCase(projConn, t);
					sheet.getRow(t.getUploadRow()).createCell(5).setCellValue(t.getMode());
					/*Modified by Padmavathi for TJN252-62 starts*/
					/*sheet.getRow(t.getUploadRow()).createCell(7).setCellValue("Success");*/
					////Modified By Priyanka for  TestSet Upload starts
					sheet.getRow(t.getUploadRow()).createCell(9).setCellValue("Success");
					////Modified By Priyanka for  TestSet Upload ends
					/*Modified by Padmavathi for TJN252-62 ends*/
					Collection<TestStep> tSteps = stepMap.get(t.getTcId());
					int scount = 0;
					for (TestStep step : tSteps) {
						scount++;
						logger.info("Persisting Step " + scount + " out of " + tSteps.size());
						try {
							if (helper.doesTestStepExist(projConn, projectId, step.getId(), t.getTcRecId())) {
								logger.error("Test Step with ID {} already exists for Project {}", step.getId(),
										projectId);
								/*Modified by Padmavathi for TJN252-62 starts*/
								/*stepSheet.getRow(step.getUploadRow()).createCell(13)
										.setCellValue("Record Already Exists");*/
								stepSheet.getRow(step.getUploadRow()).createCell(15)
								.setCellValue("Record Already Exists");
								/*Modified by Padmavathi for TJN252-62 ends*/
								uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
								continue;
							}
							Boolean appExists = false;
							Boolean functionExists = false;
							Boolean apiExists = false;

							ProjectHelper prjHelper = new ProjectHelper();
							ArrayList<Aut> prjAutList = prjHelper.hydrateProjectAuts(projectId);
							for (Aut aut : prjAutList) {
								if (step.getAppId() == aut.getId()) {
									appExists = true;
								}
							}
							if (appExists) {
								if (t.getMode().equalsIgnoreCase("GUI")) {

									ModulesHelper moduleHelper = new ModulesHelper();
									ModuleBean module = moduleHelper.hydrateModule(step.getAppId(),
											step.getModuleCode());
									if (module != null) {
										functionExists = true;
									}
									if (!functionExists) {
										logger.error("Test step application does not contains the function");

										/*Modified by Padmavathi for TJN252-62 starts*/
										/*stepSheet.getRow(step.getUploadRow()).createCell(13)
												.setCellValue("This function does not exists for this application");*/
										stepSheet.getRow(step.getUploadRow()).createCell(15)
										.setCellValue("This function does not exists for this application");
										/*Modified by Padmavathi for TJN252-62 ends*/
										uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;

										continue;
									}
								} else {
									ApiHelper apiHelper = new ApiHelper();
									/*Modified by Preeti for TENJINCG-503 starts*/
									/*Api api = apiHelper.hydrateApi(step.getAppId(), step.getApiCode());*/
									Api api = apiHelper.hydrateApi(step.getAppId(), step.getModuleCode());
									/*Modified by Preeti for TENJINCG-503 ends*/
									if (api != null) {
										apiExists = true;
									}
									if (!apiExists) {
										logger.error("Test step application does not contains the API");

										/*Modified by Padmavathi for TJN252-62 starts*/
										/*stepSheet.getRow(step.getUploadRow()).createCell(13)
												.setCellValue("This API does not exists for this application");*/
										stepSheet.getRow(step.getUploadRow()).createCell(15)
										.setCellValue("This API does not exists for this application");
										/*Modified by Padmavathi for TJN252-62 ends*/
										uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;

										continue;
									}
								}
							} else {
								logger.error(
										"Test step application may not mapped with project or application does not exists in Tenjin");
								/*Modified by Padmavathi for TJN252-62 starts*/
								/*stepSheet.getRow(step.getUploadRow()).createCell(13).setCellValue(
										"Application does not mapped with project or Application does not exists");*/
								stepSheet.getRow(step.getUploadRow()).createCell(15).setCellValue(
										"Application does not mapped with project or Application does not exists");
								/*Modified by Padmavathi for TJN252-62 ends*/
								uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;

								continue;
							}
							step.setTestCaseRecordId(t.getTcRecId());
							helper.persistTestStep(projConn, step);
							/*Modified by Padmavathi for TJN252-62 starts*/
							/*stepSheet.getRow(step.getUploadRow()).createCell(13).setCellValue("Success");*/
							stepSheet.getRow(step.getUploadRow()).createCell(15).setCellValue("Success");
						} catch (Exception e) {
							/*stepSheet.getRow(step.getUploadRow()).createCell(13).setCellValue(e.getMessage());*/
							stepSheet.getRow(step.getUploadRow()).createCell(15).setCellValue(e.getMessage());
							uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
							/*Modified by Padmavathi for TJN252-62 ends*/
						}

					}
					if(tset.getMode().equalsIgnoreCase(t.getMode())){
						set.persistTestSetMap(tset.getId(), Integer.toString(t.getTcRecId()), t.getProjectId(), false);
						/*Modified by Padmavathi for TJN252-62 starts*/
						/*sheet.getRow(t.getUploadRow()).createCell(6)
					.setCellValue(setName);*/
						sheet.getRow(t.getUploadRow()).createCell(7)
						.setCellValue(setName);
						/*Modified by Padmavathi for TJN252-62 ends*/
					}
					else{
						logger.error(
								"Test Case's mode with ID {} for Project {} is different from TestSet{} mode",
								t.getTcId(), projectId, setName);
						/*Modified by Padmavathi for TJN252-62 starts*/
						/*sheet.getRow(t.getUploadRow()).createCell(7)
								.setCellValue("TestCase is created but not mapped with the TestSet as the mode of both is not same.");*/
						sheet.getRow(t.getUploadRow()).createCell(8)
						.setCellValue("TestCase is created but not mapped with the TestSet as the mode of both is not same.");
						/*Modified by Padmavathi for TJN252-62 ends*/
						/*sheet.getRow(t.getUploadRow()).createCell(6)
						.setCellValue(" ");*/
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						continue;
					}
				} catch (Exception e) {
					/*Modified by Padmavathi for TJN252-62 starts*/
					/*sheet.getRow(t.getUploadRow()).createCell(7).setCellValue(e.getMessage());*/
					sheet.getRow(t.getUploadRow()).createCell(8).setCellValue(e.getMessage());
					/*Modified by Padmavathi for TJN252-62 ends*/
					uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;

				}

			}
			
			/*Changed by Pushpalatha for TENJINCG-583 starts changed "//" to File.separator*/
			try {
				/*Commented by Ashiki for TJN27-40 starts*/
				/*Thread.sleep(tcCount*2500);*/
				/*Commented by Ashiki for TJN27-40 ends*/
				fileNameWithoutExtension = removeSpaces(fileNameWithoutExtension);

				 fout = new FileOutputStream(
						folderPath + File.separator + fileNameWithoutExtension + "-output.xlsx");
				workbook.write(fout);
				logger.debug("File saved on path " + folderPath + File.separator + fileNameWithoutExtension + "-output.xlsx");
			} catch (Exception e) {
				logger.error("Could not save file on path " + folderPath + File.separator + fileNameWithoutExtension
						+ "-output.xlsx" + " because of an exception");
				logger.error(e.getMessage());
				throw new ExcelException("Could not save generated Excel file to disk", e);
			}
			/*Changed by Pushpalatha for TENJINCG-583 ends*/
			/*Added By Ashiki for V2.8-70 starts*/
		}catch (RequestValidationException e) {
			throw new RequestValidationException("Invalid");
		}
		/*Added By Ashiki for V2.8-70 ends*/
		catch (Exception e) {
			logger.error("ERROR while uploading test cases", e);
		} finally {
			logger.debug("Done");
			try {
				fout.close();
			} catch (IOException e) {
				logger.error("ERROR while uploading test cases", e);
			}
			/*Added by paneendra for  TENJINCG-1267 starts*/
			DatabaseHelper.close(projConn);
			DatabaseHelper.close(appConn);
			/*Added by paneendra for  TENJINCG-1267 ends*/
		}
		if (uploadStatus != null && !uploadStatus.equalsIgnoreCase("")) {
			return uploadStatus;
		} else {
			return TenjinConstants.ACTION_RESULT_SUCCESS;
		}

	}
	/*Added by Roshni fro TENJINCG-305 ends */

	/*Added by Padmavathi for TENJINCG-741 starts*/
	public void validateFields(TestStep step,int projectId,Connection conn) throws ExcelException{
		try {
			if(new AutsHelper().hydrateProjectAut(projectId, step.getAppId())==null){
				logger.error("Test step application may not mapped with project or application does not exists in Tenjin");
				throw new ExcelException("Application does not mapped with project or Application does not exists");
			}
			if(step.getTxnMode().equalsIgnoreCase("GUI")){
				if(new ModuleHelper().hydrateModule(conn, step.getAppId(), step.getModuleCode())==null){
					logger.error("This function does not exists for this application");
					throw new ExcelException("This function does not exists for this application");
				}
			}else{
				if(new ApiHelper().hydrateApi(conn, step.getAppId(), step.getModuleCode())==null){
					logger.error("This API does not exists for this application");
					throw new ExcelException("This API does not exists for this application");
				}
			}

		} catch (DatabaseException e) {
			
			logger.error("Error ", e);
		}
	}
	/*Added by Padmavathi for TENJINCG-741 ends*/


	/*Added by Pushpa for TENJINCG-740 Strats*/
	public void downloadTestSet(int projectId, String destination, String fileName, ArrayList<TestSet> testset) throws ExcelException {
		


		ArrayList<TestCase> testCases = new ArrayList<TestCase>();

		for(TestSet tset:testset){
			try{
				TestSetHelper helper = new TestSetHelper();

				logger.info("Fetching all test cases under project");
				ArrayList<TestCase> tcList= helper.hydrateMappedTCs(tset.getId(), projectId);
				logger.info("Processing test cases");

				logger.debug("{} steps found", tcList.size());
				logger.debug("Processing steps");
				if(tcList.size() > 0){
					for(TestCase tc:tcList){
						tc.setTcTestSetName(tset.getName());
						testCases.add(tc);
					}
				}

			}catch(Exception e){
				logger.error("ERROR hydrating test cases for project {}", e);
				throw new ExcelException("Could not download test cases due to an internal error");
			}
		}


		try{
			logger.info("Beginning writing to file");
			this.generateTestSetExcelSheet(testset,testCases,destination,fileName);
			logger.info("Donwload created successfully");
		}catch(ExcelException e){
			throw new ExcelException(e.getMessage(),e);
		}


	}


	private void generateTestSetExcelSheet(ArrayList<TestSet> testset, ArrayList<TestCase> testCases,
			String destination, String fileName) throws ExcelException {
		

		ArrayList<ArrayList<String>> masterTsetList = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> masterTCList = new ArrayList<ArrayList<String>>();

		ArrayList<String> tsetSheetHeader = new ArrayList<String>();
		tsetSheetHeader.add("TS_NAME");
		tsetSheetHeader.add("TS_DESC");
		/*commented by paneendra for for removing EXE mode column in template starts*/
		/*tsetSheetHeader.add("TS_EXEC_MODE");*/
		/*commented by paneendra for for removing EXE mode column in template ends*/
		tsetSheetHeader.add("TS_TYPE");
		tsetSheetHeader.add("TS_PRIORITY");

		masterTsetList.add(tsetSheetHeader);

		ArrayList<String> tcSheetHeader = new ArrayList<String>();
		tcSheetHeader.add("TEST_CASE_ID");
		tcSheetHeader.add("NAME");
		tcSheetHeader.add("DESCRIPTION");
		//Added By Priyanka for Test set Upload starts
		tcSheetHeader.add("APPLICATION");
		//Added By Priyanka for  TestSet Upload ends
		tcSheetHeader.add("TYPE");
		tcSheetHeader.add("PRIORITY");
		/*commented by paneendra for for removing EXE mode column in template starts*/
		/*tcSheetHeader.add("EXEC_MODE");*/
		/*commented by paneendra for for removing EXE mode column in template ends*/
		tcSheetHeader.add("TEST_SET_NAME");
		masterTCList.add(tcSheetHeader);



		try{
			logger.info("Preparing test set for download");
			for(TestSet tset:testset){
				ArrayList<String> t = new ArrayList<String>();
				t.add(tset.getName());
				t.add(tset.getDescription());
				/*commented by paneendra for for removing EXE mode column in template starts*/
				/*t.add(tset.getMode());*/
				/*commented by paneendra for for removing EXE mode column in template ends*/
				t.add(tset.getType());
				t.add(tset.getPriority());

				masterTsetList.add(t);

			}
			logger.info("Successfully processed {} test sets", testset.size());

			logger.info("Preparing test cases for download");
			for(TestCase tc:testCases){
				ArrayList<String> t = new ArrayList<String>();
				if(tc.getTcId().equalsIgnoreCase("Root")){
					continue;
				}
				t.add(tc.getTcId());
				t.add(tc.getTcName());
				t.add(tc.getTcDesc());
				//Added By Priyanka for  TestSet Upload starts
				t.add(tc.getTcAppName());
				//Added By Priyanka for  TestSet Upload starts
				t.add(tc.getTcType());
				t.add(tc.getTcPriority());
				/*commented by paneendra for for removing EXE mode column in template starts*/
				/*t.add(tc.getMode());*/
				/*commented by paneendra for for removing EXE mode column in template ends*/
				t.add(tc.getTcTestSetName());
				masterTCList.add(t);
			}
			logger.info("Successfully processed {} test cases", testCases.size());


			logger.info("Writing to file");
			logger.debug("Creating workbook");
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet testSetSheet = workbook.createSheet("Test Sets");
			XSSFSheet testCaseSheet = workbook.createSheet("Test Cases");
			logger.debug("Initializing fonts and styles");
			CellStyle cellStyle = this.getHeaderRowStyle(workbook);

			logger.debug("Create TEST SET Sheet --> DONE");

			int rowId = 0;
			for(ArrayList<String> rowData:masterTsetList){
				logger.debug("Writing TESTSET data in row {}", rowId);
				int col=0;
				testSetSheet.createRow(rowId);
				for(String s:rowData){
					testSetSheet.getRow(rowId).createCell(col).setCellType(Cell.CELL_TYPE_STRING);
					testSetSheet.getRow(rowId).getCell(col).setCellValue(s);
					if(rowId == 0){
						testSetSheet.getRow(rowId).getCell(col).setCellStyle(cellStyle);
					}
					col++;
				}

				rowId++;
			}

			int tsetColCount = masterTsetList.get(0).size();
			for(int i=0;i<tsetColCount;i++){
				testSetSheet.autoSizeColumn(i);
			}

			logger.debug("Written {} test set to file", masterTsetList.size()-1);



			logger.debug("Create TEST CASE Sheet --> DONE");

			rowId = 0;
			for(ArrayList<String> rowData:masterTCList){
				logger.debug("Writing TESTCASE data in row {}", rowId);
				int col=0;
				testCaseSheet.createRow(rowId);
				for(String s:rowData){
					testCaseSheet.getRow(rowId).createCell(col).setCellType(Cell.CELL_TYPE_STRING);
					testCaseSheet.getRow(rowId).getCell(col).setCellValue(s);
					if(rowId == 0){
						testCaseSheet.getRow(rowId).getCell(col).setCellStyle(cellStyle);
					}


					col++;
				}

				rowId++;
			}

			int tcColCount = masterTCList.get(0).size();
			for(int i=0;i<tcColCount;i++){
				testCaseSheet.autoSizeColumn(i);
			}

			logger.debug("Written {} test cases to file", masterTCList.size()-1);



			logger.info("Saving file to disk");
			if(fileName != null && !fileName.endsWith(".xlsx")){
				fileName = fileName + ".xlsx";
			}else if(fileName == null || fileName.equalsIgnoreCase("")){
				fileName = "Tenjin_Testset_Download.xlsx";
			}

			FileOutputStream fout = new FileOutputStream(destination + File.separator+ fileName);

			workbook.write(fout);
			fout.close();

			logger.info("Done");
		}catch(Exception e){
			logger.error("ERROR while generating report",e);
			throw new ExcelException("Could not download test case(s) due to an internal error");
		}


	}
	/*Added by Pushpa for TENJINCG-740 Ends*/
	/*Added by Pushpa for TENJINCG-740 Starts*/
	public String processTestSetUploadSheet(String folderPath, String fileNameWithoutExtension, int projectId,String createdBy) throws ExcelException, SQLException, DatabaseException, RequestValidationException {



		try(Connection projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);){



			String uploadStatus = "";
			int rootTestCaseRecId = -1;
			try {
				FileInputStream file = new FileInputStream(
						new File(folderPath + File.separator + fileNameWithoutExtension + ".xlsx"));
				XSSFWorkbook workbook = new XSSFWorkbook(file);
				XSSFSheet sheet = workbook.getSheetAt(0);

				CellStyle cellStyle = workbook.createCellStyle();
				cellStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
				cellStyle.setFillPattern(CellStyle.BIG_SPOTS);
				Font hSSFFont = workbook.createFont();
				hSSFFont.setFontName("Calibri");
				hSSFFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
				/*modified by paneendra for altered column size in template starts*/
				sheet.getRow(0).createCell(4).setCellStyle(cellStyle);
				sheet.getRow(0).getCell(4).setCellValue("UPLOAD_STATUS");
				sheet.autoSizeColumn(4);
				/*modified by paneendra for altered column size in template ends*/
				cellStyle.setFont(hSSFFont);
				;

				int startRow = 1;
				int currentRow = startRow;

				int tsetNameCol=0;
				int tsetDescCol=1;
				/*modified by paneendra for altered column size in template starts*/
				/*int tsetModeCol=2;*/
				int tsetTypeCol=2;
				int tsetPriorityCol=3;
				/*modified by paneendra for altered column size in template ends*/

				int tcIdCol = 0;
				int tcNameCol = 1;
				int tcDescCol = 2;
				//Modified By Priyanka for  TestSet Upload starts
				int tcAppCol =3;
				int tcTypeCol = 4;
				int tcPriorityCol = 5;
				/*commented by paneendra for removing execmode header in template starts*/
				/*int tcExecMode = 6;*/
				/*commented by paneendra for removing execmode header in template ends*/
				//Modified By Priyanka for  TestSet Upload starts

				/*Added by Ashiki for TJNUN262-120 starts*/
				this.validateTestSetFields(sheet);
				/*Added by Ashiki for TJNUN262-120 ends*/
				ArrayList<TestSet> testsets= new ArrayList<TestSet>();
				logger.info("Processing Test Sets");

				while (sheet.getRow(currentRow) != null){

					Row cRow = sheet.getRow(currentRow);
					TestSet tset=new TestSet();

					if(sheet.getRow(currentRow).getCell(tsetNameCol) == null || Utilities.trim(sheet.getRow(currentRow).getCell(tsetNameCol) .toString()).equals("")){
						logger.error("Test Set Name is mandatory");
						sheet.getRow(currentRow).createCell(5).setCellValue("Test Set Name is mandatory");
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						currentRow++;
						continue;
					}
					else{/*changed by Ashiki for TJN252-44 starts*/
						/* tset.setName(cRow.getCell(tsetNameCol).getStringCellValue());*/
						tset.setName(Utilities.trim(cRow.getCell(tsetNameCol).getStringCellValue()));
						/*changed by Ashiki for TJN252-44 ends*/
					}

					try {
						sheet.getRow(currentRow).getCell(tsetDescCol).setCellType(Cell.CELL_TYPE_STRING);
						/*changed by Ashiki for TJN252-44 starts*/
						/*tset.setDescription(cRow.getCell(tsetDescCol).getStringCellValue());*/
						tset.setDescription(Utilities.trim(cRow.getCell(tsetDescCol).getStringCellValue()));
						/*changed by Ashiki for TJN252-44 ends*/
					} catch (NullPointerException e) {
						tset.setDescription("");
						logger.warn("Test set Description NULL for " + tset.getName());
					}
					/*commented by paneendra for removing execmode header in template starts*/
					/*try {
						sheet.getRow(currentRow).getCell(tsetModeCol, Row.CREATE_NULL_AS_BLANK)
						.setCellType(Cell.CELL_TYPE_STRING);
						changed by Ashiki for TJN252-44 starts
						tset.setMode(cRow.getCell(tsetModeCol).getStringCellValue());
						tset.setMode(Utilities.trim(cRow.getCell(tsetModeCol).getStringCellValue()));
						changed by Ashiki for TJN252-44 ends
					} catch (NullPointerException e) {
						logger.warn("Test set Execution Mode NULL for " + tset.getName());
					}*/

					/*commented by paneendra for removing execmode header in template ends*/
					try {
						sheet.getRow(currentRow).getCell(tsetTypeCol).setCellType(Cell.CELL_TYPE_STRING);
						/*changed by Ashiki for TJN252-44 starts*/
						/*tset.setType(cRow.getCell(tsetTypeCol).getStringCellValue());*/
						tset.setType(Utilities.trim(cRow.getCell(tsetTypeCol).getStringCellValue()));
						/*changed by Ashiki for TJN252-44 ends*/
					} catch (NullPointerException e) {
						tset.setType("Functional");
						logger.warn("Test Set Type  NULL for " +tset.getName());
					}

					try {
						sheet.getRow(currentRow).getCell(tsetPriorityCol,Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_STRING);
						/*changed by Ashiki for TJN252-44 starts*/
						/*tset.setPriority(cRow.getCell(tsetPriorityCol).getStringCellValue());*/
						tset.setPriority(Utilities.trim(cRow.getCell(tsetPriorityCol).getStringCellValue()));
						/*changed by Ashiki for TJN252-44 ends*/
					} catch (NullPointerException e) {
						logger.warn("Test Set Priority NULL for " + tsetPriorityCol);
					}
					tset.setUploadRow(currentRow);
					testsets.add(tset);

					currentRow++;
				}

				TestSetHelperNew tsetHelper=new TestSetHelperNew();

				int count=0;
				for(TestSet ts:testsets){
					count++;
					logger.info("Persisting Test Set " + count + " out of " + testsets.size());
					TestSet hydratedTs=tsetHelper.hydrateTestSet(ts.getName(), projectId);
					try {


						if(hydratedTs!=null){

							if(ts.getDescription().equals("")){
								/*changed by Ashiki for TJN252-44 starts*/
								/*ts.setDescription(hydratedTs.getDescription());*/
								ts.setDescription(Utilities.trim(hydratedTs.getDescription()));
								/*changed by Ashiki for TJN252-44 ends*/
							}
							/*commented by paneendra for removing execmode header in template starts*/
							/*	if(ts.getMode().equals("")){
								changed by Ashiki for TJN252-44 starts
								ts.setMode(hydratedTs.getMode());
								ts.setMode(Utilities.trim(hydratedTs.getMode()));
								changed by Ashiki for TJN252-44 ends
							}*/
							/*commented by paneendra for removing execmode header in template ends*/
							if(ts.getPriority().equals("")){
								/*changed by Ashiki for TJN252-44 starts*/
								/*ts.setPriority(hydratedTs.getPriority());*/
								ts.setPriority(Utilities.trim(hydratedTs.getPriority()));
								/*changed by Ashiki for TJN252-44 ends*/
							}
							ts.setId(hydratedTs.getId());
							ts.setProject(projectId);
							/*Added by Preeti for TENJINCG-969 starts*/
							AuditRecord audit = new AuditRecord();
							audit.setEntityRecordId(ts.getId());
							audit.setLastUpdatedBy(createdBy);
							audit.setEntityType("testset");
							ts.setAuditRecord(audit);
							/*Added by Preeti for TENJINCG-969 ends*/
							new TestSetHandler().updateTestSet(ts);
							/*Modified by Priyanka for TCGST-65 starts*/
							sheet.getRow(ts.getUploadRow()).createCell(4).setCellValue("TestSet Updated");
							/*Modified by Priyanka for TCGST-65 starts*/
							uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
							continue;
						}
						ts.setProject(projectId);
						ts.setRecordType("TS");
						ts.setCreatedBy(createdBy);
						/*commented by paneendra for removing execmode header in template starts*/
						/*if(ts.getMode().equals("")){
							ts.setMode("GUI");
						}*/
						/*	if(!ts.getMode().equals("GUI") && !ts.getMode().equals("API")){
							sheet.getRow(ts.getUploadRow()).createCell(5).setCellValue("Invalid TestSet Mode");
							uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
							continue;
						}*/
						/*commented by paneendra for removing execmode header in template ends*/
						if(ts.getPriority().equals("")){
							ts.setPriority("Low");
						}
						if(!ts.getPriority().equals("Low") && !ts.getPriority().equals("High")&& !ts.getPriority().equals("Medium")){
							sheet.getRow(ts.getUploadRow()).createCell(5).setCellValue("The value of testset priority is wrong ");
							uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
							continue;
						}
						if(!ts.getType().equals("Functional")){
							sheet.getRow(ts.getUploadRow()).createCell(5).setCellValue("Invalid TestSet Type");
							uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
							continue;
						}


						tsetHelper.persistTestSet(ts, projectId);
						sheet.getRow(ts.getUploadRow()).createCell(2).setCellValue(ts.getMode());
						/*modified by paneendra for altering column size in  template starts*/
						sheet.getRow(ts.getUploadRow()).createCell(4).setCellValue("Success");
						/*modified by paneendra for altering column size in  template ends*/

					}
					catch (RequestValidationException e) {
						/*Added by Pushpa for TENJINCG-819 starts*/
						ScreenState status= new ScreenState("",e.getMessage());
						status.loadMessage();
						String message=status.getMessage();
						if(message.contains("{0}")){
							/*Changed by Pushpala for TENJINCG-821 starts*/
							message=message.replace("{0}"," Testset Mode");
							/*Changed by Pushpala for TENJINCG-821 ends*/
							ts.setMode(hydratedTs.getMode());
						}
						/*Added by Pushpa for TENJINCG-819 ends*/
						sheet.getRow(ts.getUploadRow()).createCell(5).setCellValue(message);
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
					}
					catch (Exception e) {
						sheet.getRow(ts.getUploadRow()).createCell(5).setCellValue(e.getMessage());
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;

					}

				}


				logger.info("Processing testcases");

				XSSFSheet sheet1 = workbook.getSheetAt(1);

				hSSFFont.setFontName("Calibri");
				hSSFFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
				//Added By Priyanka for  TestSet Upload starts
				/*modified by paneendra for altering column size in  template starts*/
				sheet1.getRow(0).createCell(7).setCellStyle(cellStyle);
				sheet1.getRow(0).getCell(7).setCellValue("UPLOAD_STATUS");
				sheet1.autoSizeColumn(7);
				/*modified by paneendra for altering column size in  template ends*/
				//Added By Priyanka for  TestSet Upload starts
				cellStyle.setFont(hSSFFont);
				currentRow = startRow;
				int tcCount = 0;

				ArrayList<TestCase> testCases = new ArrayList<TestCase>();
				logger.info("Processing Test Cases");
				TestSetHelperNew set = new TestSetHelperNew();
				/*Added by Ashiki for TJNUN262-120 starts*/
				validateTestcaseFields(sheet1,"testset");
				/*Added by Ashiki for TJNUN262-120 starts*/
				while (sheet1.getRow(currentRow) != null){
					//Modified By Priyanka for  TestSet Upload starts
					/*modified by paneendra for altering column size in  template starts*/
					if(sheet1.getRow(currentRow).getCell(6)==null||sheet1.getRow(currentRow).getCell(6).equals(""))
					{
						sheet1.getRow(currentRow).createCell(7).setCellValue("This testcase is not linked with  uploaded testset.");
						/*modified by paneendra for altering column size in  template ends*/
						//Added By Priyanka for  TestSet Upload ends
						currentRow++;
						continue;
					}

					if(sheet1.getRow(currentRow).getCell(tcIdCol)==null||sheet1.getRow(currentRow).getCell(tcIdCol).equals("")){
						//Modified By Priyanka for  TestSet Upload starts
						/*modified by paneendra for altering column size in  template starts*/
						sheet1.getRow(currentRow).createCell(7).setCellValue("Testcase Id is mandatory.");
						/*modified by paneendra for altering column size in  template ends*/
						//Modified By Priyanka for  TestSet Upload starts
						currentRow++;
						continue;
					}
					if(sheet1.getRow(currentRow).getCell(tcNameCol)==null||sheet1.getRow(currentRow).getCell(tcNameCol).equals("")){
						/*modified by paneendra for altering column size in  template starts*/
						//Modified By Priyanka for  TestSet Upload starts
						sheet1.getRow(currentRow).createCell(7).setCellValue("Testcase Name is mandatory.");
						/*modified by paneendra for altering column size in  template ends*/
						//Modified By Priyanka for  TestSet Upload ends
						currentRow++;
						continue;
					}
					TestCase t = new TestCase();
					/*changed by Ashiki for TJN252-44 starts*/
					/*t.setTcTestSetName(sheet1.getRow(currentRow).getCell(6).getStringCellValue());*/
					/*modified by paneendra for altering column size in  template starts*/
					//Modified By Priyanka for  TestSet Upload starts
					t.setTcTestSetName(Utilities.trim(sheet1.getRow(currentRow).getCell(6).getStringCellValue()));
					//Modified By Priyanka for  TestSet Upload ends
					/*modified by paneendra for altering column size in  template ends*/
					/*changed by Ashiki for TJN252-44 ends*/
					boolean flag=false;
					for(TestSet ts:testsets){
						if(ts.getName().equalsIgnoreCase(t.getTcTestSetName()))
						{
							flag=true;
							t.setTcTsetId(ts.getId());
							/*changed by Ashiki for TJN252-44 starts
						t.setTcTestSetName(ts.getName());
						t.setTcTsetMode(ts.getMode());*/
							t.setTcTestSetName(Utilities.trim(ts.getName()));
							t.setTcTsetMode(Utilities.trim((ts.getMode())));
							/*changed by Ashiki for TJN252-44 ends*/
							break;
						}
					}

					if(flag){
						tcCount++;

						Row cRow = sheet1.getRow(currentRow);
						/*changed by Ashiki for TJN252-44 starts*/
						/*t.setTcId(cRow.getCell(tcIdCol).getStringCellValue());*/
						t.setTcId(Utilities.trim(cRow.getCell(tcIdCol).getStringCellValue()));
						/*changed by Ashiki for TJN252-44 ends*/

						try {
							sheet1.getRow(currentRow).getCell(tcNameCol).setCellType(Cell.CELL_TYPE_STRING);
							/*changed by Ashiki for TJN252-44 starts*/
							/*t.setTcName(cRow.getCell(tcNameCol).getStringCellValue());*/
							t.setTcName(Utilities.trim(cRow.getCell(tcNameCol).getStringCellValue()));
							/*changed by Ashiki for TJN252-44 ends*/
						} catch (NullPointerException e) {
							logger.warn("Test Case Name NULL for " + t.getTcId());
						}

						try {
							sheet1.getRow(currentRow).getCell(tcDescCol).setCellType(Cell.CELL_TYPE_STRING);
							/*changed by Ashiki for TJN252-44 starts*/
							/*t.setTcDesc(cRow.getCell(tcDescCol).getStringCellValue());*/
							t.setTcDesc(Utilities.trim(cRow.getCell(tcDescCol).getStringCellValue()));
							/*changed by Ashiki for TJN252-44 ends*/

						} catch (NullPointerException e) {
							logger.warn("Test Case Description NULL for " + t.getTcId());
						}
						try {
							sheet1.getRow(currentRow).getCell(tcTypeCol, Row.CREATE_NULL_AS_BLANK)
							.setCellType(Cell.CELL_TYPE_STRING);
							if (cRow.getCell(tcTypeCol).getStringCellValue().equals("")) {
								t.setTcType("Acceptance");
							} else {
								/*changed by Ashiki for TJN252-44 starts*/
								/*t.setTcType(cRow.getCell(tcTypeCol).getStringCellValue());*/
								t.setTcType(Utilities.trim(cRow.getCell(tcTypeCol).getStringCellValue()));
								/*changed by Ashiki for TJN252-44 ends*/
							}
						} catch (NullPointerException e) {
							logger.warn("Test Case Type NULL for " + t.getTcId());
						}

						try {
							sheet1.getRow(currentRow).getCell(tcPriorityCol).setCellType(Cell.CELL_TYPE_STRING);
							/*changed by Ashiki for TJN252-44 starts*/
							/*t.setTcPriority(cRow.getCell(tcPriorityCol).getStringCellValue());*/
							t.setTcPriority(Utilities.trim(cRow.getCell(tcPriorityCol).getStringCellValue()));
							/*changed by Ashiki for TJN252-44 ends*/
						} catch (NullPointerException e) {

							logger.warn("Test Case Priority NULL for " + t.getTcId());
						}
						/*commented by paneendra for removing EXE mode column in  template starts*/
						/*try {

							String tcMode="";
							sheet1.getRow(currentRow).getCell(tcExecMode, Row.CREATE_NULL_AS_BLANK)
							.setCellType(Cell.CELL_TYPE_STRING);
							String modeValue = cRow.getCell(tcExecMode).getStringCellValue();
							if (Utilities.trim(modeValue).contentEquals("") || !modeValue.equalsIgnoreCase("API")
									|| modeValue == null) {
								t.setMode("GUI");
							} else {
								changed by Ashiki for TJN252-44 starts
								t.setMode(modeValue.toUpperCase());
								t.setMode(Utilities.trim(modeValue.toUpperCase()));
								changed by Ashiki for TJN252-44 ends
							}
							tcMode = t.getMode();
							modeValueMap.put(t.getTcId(), tcMode);
						} catch (NullPointerException e) {
							logger.warn("Test Case Execution Mode NULL for " + t.getTcId());
						}*/
						/*commented by paneendra for removing EXE mode column in  template ends*/
						try {
							/*modified by paneendra for altering column size in  template starts*/
							//Modified By Priyanka for  TestSet Upload ends
							sheet1.getRow(currentRow).getCell(6, Row.CREATE_NULL_AS_BLANK)
							.setCellType(Cell.CELL_TYPE_STRING);
							/*modified by paneendra for altering column size in  template ends*/
							/*changed by Ashiki for TJN252-44 starts*/
							/*t.setTcTestSetName(sheet1.getRow(currentRow).getCell(6).getStringCellValue());*/
							/*modified by paneendra for altering column size in  template starts*/
							t.setTcTestSetName(Utilities.trim(sheet1.getRow(currentRow).getCell(6).getStringCellValue()));
							/*modified by paneendra for altering column size in  template ends*/
							//Modified By Priyanka for  TestSet Upload ends
							/*changed by Ashiki for TJN252-44 ends*/
						} catch (NullPointerException e) {
							logger.warn("Test set  NULL for " + t.getTcId());
						}

						try {
							t.setStorage("Local");
						} catch (NullPointerException e) {
							logger.warn("Test Case Storage NULL for " + t.getTcId());
						}
						t.setUploadRow(currentRow);
						testCases.add(t);


						if (sheet1.getRow(currentRow) != null) {
							sheet1.getRow(currentRow).getCell(tcIdCol).setCellType(Cell.CELL_TYPE_STRING);
							currentRow++;
						}
					}else{
						sheet1.getRow(currentRow).createCell(7).setCellValue("This testcase is not linked with  uploaded testset.");
						currentRow++;
						continue;
					}
				}

				/*Added by Pushpa for TENJINCG-821 starts*/
				TestCaseHelperNew tcHelper=new TestCaseHelperNew();
				Set<String> tcType=new HashSet<String>();

				tcType.add("Acceptance");
				tcType.add("Accessibility");
				tcType.add("Compatibility");
				tcType.add("Regression");
				tcType.add("Functional");
				tcType.add("Smoke");
				tcType.add("Usability");
				tcType.add("Other");
				/*Added by Pushpa for TENJINCG-821 ends*/
				int cnt = 0;
				for(TestCase t:testCases){
					cnt++;
					/*Added by Pushpa for TENJINCG-821 starts*/
					if(!tcType.contains(t.getTcType())){
						//Modified By Priyanka for  TestSet Upload starts
						sheet1.getRow(t.getUploadRow()).createCell(8)
						.setCellValue("Testcase type is wrong.");
						//Modified By Priyanka for  TestSet Upload ends
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						continue;
					}

					/*commented by paneendra for removing EXE mode column in  template starts*/
					/*if(!t.getMode().equals("GUI") && !t.getMode().equals("API")){
						//Modified By Priyanka for  TestSet Upload starts
						sheet1.getRow(t.getUploadRow()).createCell(8).setCellValue("Invalid Testcase Mode");
						//Modified By Priyanka for  TestSet Upload ends
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						continue;
					}*/
					/*commented by paneendra for removing EXE mode column in  template ends*/
					if(t.getTcPriority()==null){
						t.setTcPriority("Low");
					}
					if(!t.getTcPriority().equals("Low") && !t.getTcPriority().equals("High")&& !t.getTcPriority().equals("Medium")){
						//Modified By Priyanka for  TestSet Upload starts
						sheet1.getRow(t.getUploadRow()).createCell(8).setCellValue("The value of testcase priority is wrong ");
						//Modified By Priyanka for  TestSet Upload ends
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
						continue;
					}

					/*Added by Pushpa for TENJINCG-821 starts*/
					logger.info("Persisting Test Case " + cnt + " out of " + testCases.size());
					try {
						if (tcHelper.doesTestCaseExist(projConn, projectId, t.getTcId())) {

							TestCase tcase = tcHelper.hydrateTestCase(projConn,projectId, t.getTcId());
							if (set.existMappedTestCase(t.getTcTsetId(), projectId, tcase.getTcRecId())) {
								logger.error(
										"Test Case with ID {} already exists for Project {} and already mapped to the TestSet{}",
										t.getTcId(), projectId, t.getTcTestSetName());
								/*modified by paneendra for altered column size in template starts*/
								//Modified By Priyanka for  TestSet Upload starts
								sheet1.getRow(t.getUploadRow()).createCell(7)
								.setCellValue("Record Already Exists and already mapped to the testset");
								sheet1.getRow(t.getUploadRow()).createCell(6)
								.setCellValue(t.getTcTestSetName());
								//Modified By Priyanka for  TestSet Upload ends
								/*modified by paneendra for altered column size in template ends*/

								uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
								continue;
							} else {
								/*commented by paneendra for removing EXE mode column in  template starts*/
								/*if (t.getTcTsetMode().equalsIgnoreCase(tcase.getMode())) {*/
								/*commented by paneendra for removing EXE mode column in  template ends*/
								set.persistTestSetMap(t.getTcTsetId(), Integer.toString(tcase.getTcRecId()), projectId,
										false);
								logger.error(
										"Test Case with ID {} already exists for Project {} and mapped to the TestSet{}",
										t.getTcId(), projectId, t.getTcTestSetName());
								/*Changed by Ashiki for TJN27-38 starts*/
								/*modified by paneendra for altered column size in template starts*/
								sheet1.getRow(t.getUploadRow()).createCell(7)
								.setCellValue("Record Already Exists and Mapped to the Testset");
								//Modified By Priyanka for  TestSet Upload starts
								sheet1.getRow(t.getUploadRow()).createCell(7)
								.setCellValue("Test Case Mapped to the Testset successfully");
								/*Changed by Ashiki for TJN27-38 starts*/
								sheet1.getRow(t.getUploadRow()).createCell(6)
								//Modified By Priyanka for  TestSet Upload ends
								/*modified by paneendra for altered column size in template ends*/

								.setCellValue(t.getTcTestSetName());
								uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
								continue;
								/*commented by paneendra for removing EXE mode column in  template starts*/
								/*} else {
									logger.error(
											"Test Case with ID {} already exists for Project {} and mode is different from TestSet{} mode",
											t.getTcId(), projectId, t.getTcTestSetName());
									//Modified By Priyanka for  TestSet Upload starts
									sheet1.getRow(t.getUploadRow()).createCell(7)
									//Modified By Priyanka for  TestSet Upload ends
									.setCellValue("Record already exists and its not mapped to the testset as the mode of both is not same");
										sheet1.getRow(t.getUploadRow()).createCell(6)
								.setCellValue(" ");
									uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
									continue;
								}*/
								/*commented by paneendra for removing EXE mode column in  template ends*/
							}
						}
						if(tcHelper.hydrateTestCaseByName(projConn, projectId, t.getTcName())!=null){
							/*modified by paneendra for altered column size in template starts*/
							//Modified By Priyanka for  TestSet Upload starts
							sheet1.getRow(t.getUploadRow()).createCell(7)
							.setCellValue("Testcase Name already exist.");
							//Modified By Priyanka for  TestSet Upload ends
							/*modified by paneendra for altered column size in template starts*/
							uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
							continue;
						}

						t.setProjectId(projectId);
						t.setRecordType("TC");

						t.setTcCreatedBy(createdBy);

						t.setTcFolderId(rootTestCaseRecId);


						tcHelper.persistTestCaseTuned(t);
						//Modified By Priyanka for  TestSet Upload starts
						/*sheet1.getRow(t.getUploadRow()).createCell(6).setCellValue(t.getMode());*/
						/*modified by paneendra for altered column size in template starts*/
						sheet1.getRow(t.getUploadRow()).createCell(7).setCellValue("Success");
						//Modified By Priyanka for  TestSet Upload ends
						/*modified by paneendra for altered column size in template ends*/

						/*commented by paneendra for removing EXE mode column in  template starts*/
						/*if(t.getTcTsetMode().equalsIgnoreCase(t.getMode())){*/
						/*commented by paneendra for removing EXE mode column in  template ends*/
						set.persistTestSetMap(t.getTcTsetId(), Integer.toString(t.getTcRecId()), t.getProjectId(), false);
						/*modified by paneendra for altered column size in template starts*/
						//Modified By Priyanka for  TestSet Upload starts
						sheet1.getRow(t.getUploadRow()).createCell(6)
						.setCellValue(t.getTcTestSetName());
						//Modified By Priyanka for  TestSet Upload ends
						/*modified by paneendra for altered column size in template ends*/

						/*commented by paneendra for removing EXE mode column in  template starts*/
						/*}
						else{
							logger.error(
									"Test Case's mode with ID {} for Project {} is different from TestSet{} mode",
									t.getTcId(), projectId, t.getTcTestSetName());
							//Modified By Priyanka for  TestSet Upload starts
							sheet1.getRow(t.getUploadRow()).createCell(7)
							//Modified By Priyanka for  TestSet Upload ends
							.setCellValue("TestCase is created but not mapped with the TestSet as the mode of both is not same.");
							sheet1.getRow(t.getUploadRow()).createCell(6)
						.setCellValue(" ");
							uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;
							continue;
						}*/
						/*commented by paneendra for removing EXE mode column in  template ends*/

					} catch (Exception e) {
						/*modified by paneendra for altered column size in template starts*/
						//Modified By Priyanka for  TestSet Upload starts
						sheet1.getRow(t.getUploadRow()).createCell(7).setCellValue(e.getMessage());
						//Modified By Priyanka for  TestSet Upload ends
						/*modified by paneendra for altered column size in template ends*/
						uploadStatus = TenjinConstants.ACTION_RESULT_WARNING;

					}

				}

				try {


					/*Commented by Padmavathi for TENJINCG-893  starts*/
					/*Thread.sleep(1000*tcCount+testsets.size());*/
					/*Commented by Padmavathi for TENJINCG-893  ends*/
					fileNameWithoutExtension = removeSpaces(fileNameWithoutExtension);

					FileOutputStream fout = new FileOutputStream(
							folderPath + File.separator + fileNameWithoutExtension + "-output.xlsx");

					workbook.write(fout);
					fout.close();
					//Thread.sleep(1000*tcCount+testsets.size());
					logger.debug("File saved on path " + folderPath + File.separator + fileNameWithoutExtension + "-output.xlsx");
				} catch (Exception e) {
					logger.error("Could not save file on path " + folderPath + File.separator + fileNameWithoutExtension
							+ "-output.xlsx" + " because of an exception");
					logger.error(e.getMessage());
					throw new ExcelException("Could not save generated Excel file to disk", e);
				}

			} 
			/*Added by Ashiki for TJNUN262-120 starts*/
			catch (RequestValidationException e) {
				throw new RequestValidationException("Invalid");
			} 
			/*Added by Ashiki for TJNUN262-120 ends*/
			/*Added by Ashiki for TJNUN262-107 starts*/
			catch (NullPointerException e) {
				throw new RequestValidationException("Invalid");
			}
			/*Added by Ashiki for TJNUN262-107 starts*/
			catch (Exception e) {
				logger.error("ERROR while uploading test cases", e);
			} finally {
				logger.debug("Done");
			}
			if (uploadStatus != null && !uploadStatus.equalsIgnoreCase("")) {
				return uploadStatus;
			} else {
				return TenjinConstants.ACTION_RESULT_SUCCESS;
			}
		}
	}
	/*Added by Pushpa for TENJINCG-740 ends*/

	/*Added by Ashiki for TJNUN262-120 starts*/
	public void validateTestSetFields(XSSFSheet sheet) throws RequestValidationException{

		if(!sheet.getRow(0).getCell(0).toString().equalsIgnoreCase("TS_NAME")) {
			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(1).toString().equalsIgnoreCase("TS_DESC")) {
			throw new RequestValidationException("Invalid Excel sheet");
		}
		/*commented by paneendra for removing EXE mode column in  template starts*/
		/*if(!sheet.getRow(0).getCell(2).toString().equalsIgnoreCase("TS_EXEC_MODE")) {
			throw new RequestValidationException("Invalid Excel sheet");
		}*/
		/*commented by paneendra for removing EXE mode column in  template ends*/
		/*modified by paneendra for altered column size in template starts*/
		if(!sheet.getRow(0).getCell(2).toString().equalsIgnoreCase("TS_TYPE")) {
			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(3).toString().equalsIgnoreCase("TS_PRIORITY")) {
			throw new RequestValidationException("Invalid Excel sheet");
			/*modified by paneendra for altered column size in template ends*/
		}

	}


	public  void validateTestcaseFields(XSSFSheet sheet,String entity) throws RequestValidationException{

		if(!sheet.getRow(0).getCell(0).toString().equalsIgnoreCase("TEST_CASE_ID")) {
			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(1).toString().equalsIgnoreCase("NAME")) {
			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(2).toString().equalsIgnoreCase("DESCRIPTION")) {
			throw new RequestValidationException("Invalid Excel sheet");
		}
		/* Added by Poojalakshmi for TJN27-94 starts */
		if (!sheet.getRow(0).getCell(3).toString().equalsIgnoreCase("APPLICATION")) {
			throw new RequestValidationException("Invalid Excel sheet");
		}
		/* Added by Poojalakshmi for TJN27-94 ends */
		/* Modified by Poojalakshmi for TJN27-94 starts */
		// if(!sheet.getRow(0).getCell(3).toString().equalsIgnoreCase("TYPE")) {
		if (!sheet.getRow(0).getCell(4).toString().equalsIgnoreCase("TYPE")) {
			/* Modified by Poojalakshmi for TJN27-94 ends */
			throw new RequestValidationException("Invalid Excel sheet");
		}
		/* Modified by Poojalakshmi for TJN27-94 starts */
		// if(!sheet.getRow(0).getCell(4).toString().equalsIgnoreCase("PRIORITY")) {
		if (!sheet.getRow(0).getCell(5).toString().equalsIgnoreCase("PRIORITY")) {
			/* Modified by Poojalakshmi for TJN27-94 ends */
			throw new RequestValidationException("Invalid Excel sheet");
		}
		/* Modified by Poojalakshmi for TJN27-94 starts */
		// if(!sheet.getRow(0).getCell(5).toString().equalsIgnoreCase("EXEC_MODE")) {
		/*commented by paneendra for removing EXE mode column in  template starts*/
		/*if (!sheet.getRow(0).getCell(6).toString().equalsIgnoreCase("EXEC_MODE")) {
			 Modified by Poojalakshmi for TJN27-94 ends 
			throw new RequestValidationException("Invalid Excel sheet");
		}*/
		/*commented by paneendra for removing EXE mode column in  template ends*/
		/*Modified by Pushpa for Performance testing fix starts*/
		if(!entity.equalsIgnoreCase("testset"))
		{
			/*Modified by Padmavathi for TJN252-62 starts*/
			/*Modified by Prem for TNJNR2-17 starts*/
			//		if(!sheet.getRow(0).getCell(6).toString().equalsIgnoreCase("PRESET_RULES")) {
			/* Modified by Poojalakshmi for TJN27-94 starts */
			// if(!sheet.getRow(0).getCell(6).toString().equalsIgnoreCase("DEFAULT
			// DEPENDENCY RULES")) {
			/* Modified by Prem for TNJNR2-17 Ends */
			/*commented by paneendra for removing DEFAULT DEPENDENCY RULES  in  template starts*/
			/*	if (!sheet.getRow(0).getCell(6).toString().equalsIgnoreCase("DEFAULT DEPENDENCY RULES")) {
				 Modified by Poojalakshmi for TJN27-94 ends 
				throw new RequestValidationException("Invalid Excel sheet");
			}*/
			/*commented by paneendra for removing DEFAULT DEPENDENCY RULES  in  template ends*/
			/* Modified by Poojalakshmi for TJN27-94 starts */
			// if(!sheet.getRow(0).getCell(7).toString().equalsIgnoreCase("TEST_SET_NAME"))
			// {
			/*modified by paneendra for altered column size in template starts*/
			if (!sheet.getRow(0).getCell(6).toString().equalsIgnoreCase("TEST_SET_NAME")) {
				/*modified by paneendra for altered column size in template ends*/
				/* Modified by Poojalakshmi for TJN27-94 ends */
				throw new RequestValidationException("Invalid Excel sheet");
			}
		}else{
			/* Modified by Poojalakshmi for TJN27-94 starts */
			// if(!sheet.getRow(0).getCell(6).toString().equalsIgnoreCase("TEST_SET_NAME"))
			// {
			/*modified by paneendra for altered column size in template starts*/
			if (!sheet.getRow(0).getCell(6).toString().equalsIgnoreCase("TEST_SET_NAME")) {
				/* Modified by Poojalakshmi for TJN27-94 ends */
				/*modified by paneendra for altered column size in template ends*/
				throw new RequestValidationException("Invalid Excel sheet");
			}
		}
		/*Modified by Padmavathi for TJN252-62 ends*/
		/*Modified by Pushpa for Performance testing fix ends*/
	}

	public  void validateTestStepFields(XSSFSheet sheet) throws RequestValidationException{

		if(!sheet.getRow(0).getCell(0).toString().equalsIgnoreCase("TEST_CASE_ID")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(1).toString().equalsIgnoreCase("STEP_ID")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(2).toString().equalsIgnoreCase("NAME")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(3).toString().equalsIgnoreCase("APPLICATION")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(4).toString().equalsIgnoreCase("FUNCTION/API CODE")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(5).toString().equalsIgnoreCase("TYPE")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(6).toString().equalsIgnoreCase("TEST_DATA_ID")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(7).toString().equalsIgnoreCase("MODE")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(8).toString().equalsIgnoreCase("DESCRIPTION")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(9).toString().equalsIgnoreCase("EXPECTED_RESULT")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(10).toString().equalsIgnoreCase("OPERATION")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(11).toString().equalsIgnoreCase("AUT_LOGIN_TYPE")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(12).toString().equalsIgnoreCase("ROWS_TO_EXECUTE")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		/*Added by Padmavathi for TJN252-62 starts*/
		/*Modified by Prem for TNJNR2-17 starts*/
		//	if(!sheet.getRow(0).getCell(13).toString().equalsIgnoreCase("CUSTOM_RULES")) {
		if(!sheet.getRow(0).getCell(13).toString().equalsIgnoreCase("RULES")) {
			/*Modified by Prem for TNJNR2-17 Ends*/
			throw new RequestValidationException("Invalid Excel sheet");
		}
		if(!sheet.getRow(0).getCell(14).toString().equalsIgnoreCase("ALWAYS_SHOW_FOR_RERUN")) {

			throw new RequestValidationException("Invalid Excel sheet");
		}
		/*Added by Padmavathi for TJN252-62 ends*/
	}
	

	/*Added by Pushpa for Excel Reports starts*/
	public CellStyle getHeaderRowStyleForExcelReport(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);


		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());

		return style;
	}


	public CellStyle getDetailsRowStyleForExcelReport(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_LEFT);

		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.GOLD.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);

		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());

		return style;
	}


	


	public CellStyle getBorderStyleForExcelReport(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_RIGHT);

		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);

		style = wb.createCellStyle();
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());

		return style;
	}





	private void createTableforExcelReport1(XSSFWorkbook workbook,HashMap<String,String> headerTable,ArrayList<HashMap<String, Object>> al) {
		
		logger.info("Creating table for excel report...");
		CellStyle cellStyle=this.getHeaderRowStyleForExcelReport(workbook);
		CellStyle cellStyle1=this.getBorderStyleForExcelReport(workbook);
		CellStyle cellStyle2 = this.getDetailsRowStyleForExcelReport(workbook);
		XSSFSheet userDetails = workbook.getSheetAt(0);

		XSSFTable headerTab = userDetails.createTable();
		CTTable ctable = headerTab.getCTTable();
		ctable.setDisplayName("Table1");
		ctable.setId(1);
		ctable.setName("Test");
		ctable.setRef("A1:C11");
		ctable.setTotalsRowShown(false);

		CTTableColumns cols = ctable.addNewTableColumns();
		cols.setCount(2);
		int rowNum=2;

		for(String key:headerTable.keySet()){
			XSSFRow r = userDetails.createRow(rowNum);
			XSSFCell cell = r.createCell(0);
			cell.setCellValue(key);
			cell.setCellStyle(cellStyle2);
			XSSFCell cell1 = r.createCell(1);
			cell1.setCellValue(String.valueOf(headerTable.get(key)));
			cell1.setCellStyle(cellStyle1);
			rowNum=rowNum+1;
		}


		XSSFTable table = userDetails.createTable();
		CTTable cttable = table.getCTTable();
		cttable.setDisplayName("Table1");
		cttable.setId(1);
		cttable.setName("Test");
		cttable.setRef("A1:C11");
		cttable.setTotalsRowShown(false);


		CTTableColumns columns = cttable.addNewTableColumns();
		logger.info("size of list="+al.size());
		columns.setCount(al.size());

		Map<String,Object> columnHeader=al.get(0);
		logger.info("columnHeader="+columnHeader.size());
		rowNum=rowNum+2;
		XSSFRow row = userDetails.createRow(rowNum);
		int c=0;

		for(String data:columnHeader.keySet()){
			logger.info("Header Data");
			XSSFCell cell = row.createCell(c); 
			cell.setCellValue(data);
			cell.setCellStyle(cellStyle);
			userDetails.autoSizeColumn(c);

			c++;
			logger.info("Done-Header Data");
		}
		logger.info("Table body");
		for(Map<String,Object> map:al){

			XSSFRow tableRow = userDetails.createRow(++rowNum);
			int tCell=0;
			for(String data:map.keySet()){
				XSSFCell cell = tableRow.createCell(tCell); 
				cell.setCellValue(String.valueOf(map.get(data)));
				cell.setCellStyle(cellStyle1);

				tCell++;

			}
		}
		logger.info("Done");
	}


	public void generateExcelReport(String title, String subTitle, HashMap<String, String> map, ArrayList<HashMap<String, Object>> list,
			String destination, String fileName) throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet userDetails = workbook.createSheet(title);
		logger.debug("Initializing fonts and styles");

		CellStyle cellStyle = this.getHeaderRowStyleForExcelReport(workbook);
		CellStyle cellStyle2=this.getBorderStyleForExcelReport(workbook);
		//Header row
		XSSFRow row = userDetails.createRow(0);
		row.createCell(0).setCellType(Cell.CELL_TYPE_STRING);
		row.getCell(0).setCellValue(title);
		row.getCell(0).setCellStyle(cellStyle);
		userDetails.autoSizeColumn(0);

		row.createCell(1).setCellType(Cell.CELL_TYPE_STRING);
		row.getCell(1).setCellValue(subTitle);
		row.getCell(1).setCellStyle(cellStyle2);

		this.createTableforExcelReport1(workbook,map,list);
		logger.info("Saving file to disk");
		if(fileName != null && !fileName.endsWith(".xlsx")){
			fileName = fileName + ".xlsx";
		}

		FileOutputStream fout = new FileOutputStream(destination + File.separator+ fileName);

		workbook.write(fout);
		fout.close();

		logger.info("Done");
	}
	/*Added by Pushpa for excel reports ends*/
	

	




	/*Added by ashiki for TJN27-180 by ends*/
	/*Added by Pushpa for TENJINCG-1210 starts*/
	public void downloadTestRuns(ArrayList<Integer> runs, int projectId, String path, String filename) throws DatabaseException, JSONException, IOException {

		List<HashMap> runsData=new ArrayList<HashMap>();
		ArrayList<HashMap<String,String>> tjnExecOutput = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String,String>> mastetRuns = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String,String>> uiValRes = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String,String>> resValResults = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String,String>> runTransaction = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String,String>> tjnExecScrnShot = new ArrayList<HashMap<String,String>>();
		for(int runId:runs){
			HashMap<String, Object> hashMap=new HashMap<>();
			mastetRuns=this.getDataFromMasTestRuns(runId,projectId);
			tjnExecOutput=this.getDataForTjnExecOutput(runId,projectId);
			uiValRes=this.getDataForUiValRes(runId,projectId);
			resValResults=this.getDataForResValResults(runId,projectId);
			runTransaction=this.getrunTransaction(runId,projectId);
			tjnExecScrnShot=this.getDataForTjnExecScrnShot(runId);
			hashMap.put("TESTRUNS",mastetRuns);
			hashMap.put("OUTPUT",tjnExecOutput);
			hashMap.put("UIVALIDATIONRESULTS", uiValRes);
			hashMap.put("VALIDATIONRESULTS", resValResults);
			hashMap.put("RUNTRANSACTIONS", runTransaction);
			hashMap.put("EXECSCRNSHOT", tjnExecScrnShot);
			runsData.add(hashMap);
		}

		this.generateExcelForRuns(runsData, path, filename);
		this.downloadScreenshotsForRuns(runs,path);


	}
	/*Added by Pushpa for TENJINCG-1210 ends*/
	/*Added by Pushpa for TENJINCG-1210 starts*/
	public void zipDirectory(File folder, String parentFolder, ZipOutputStream zos) throws FileNotFoundException, IOException {
		BufferedInputStream bis=null;
		for (File file : folder.listFiles()) {
			if (file.isDirectory()) {
				zipDirectory(file, parentFolder + "/" + file.getName(), zos);
				continue;
			}
			zos.putNextEntry(new ZipEntry(parentFolder + "/" + file.getName()));
			 bis = new BufferedInputStream(
					new FileInputStream(file));
			long bytesRead = 0;
			byte[] bytesIn = new byte[1024];
			int read = 0;
			while ((read = bis.read(bytesIn)) != -1) {
				zos.write(bytesIn, 0, read);
				bytesRead += read;
			}
			zos.closeEntry();
		}
		bis.close();
	}
	/*Added by Pushpa for TENJINCG-1210 ends*/
	/*Added by Pushpa for TENJINCG-1210 starts*/
	private ArrayList<HashMap<String, String>> getDataForTjnExecScrnShot(int runId) throws DatabaseException {

		ArrayList<HashMap<String,String>> listOfTjnExecEcrnShot = new ArrayList<HashMap<String,String>>();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst=conn.prepareStatement("SELECT * FROM TJN_EXEC_SCRNSHOT WHERE RUN_ID=? ");
			pst.setInt(1, runId);

			rs = pst.executeQuery();
			HashMap<String,String> execScrnShot = new HashMap<String,String>();

			while(rs.next()){

				execScrnShot.put("RUN_ID", rs.getString("RUN_ID"));
				execScrnShot.put("TDGID", rs.getString("TDGID"));
				execScrnShot.put("TDUID", rs.getString("TDUID"));
				execScrnShot.put("ITERATION_NO", rs.getString("ITERATION_NO"));
				execScrnShot.put("SEQ_NO", rs.getString("SEQ_NO"));
				execScrnShot.put("SCR_TIMESTAMP", rs.getString("SCR_TIMESTAMP"));
				execScrnShot.put("SCR_DESC", rs.getString("SCR_DESC"));


				listOfTjnExecEcrnShot.add(execScrnShot);
			}

		}catch(Exception e){
			logger.error("Could not fetch data ",e);
			throw new DatabaseException(e.getMessage(),e);
		}finally{

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return listOfTjnExecEcrnShot;
	}
	/*Added by Pushpa for TENJINCG-1210 ends*/
	/*Added by Pushpa for TENJINCG-1210 starts*/
	private void downloadScreenshotsForRuns(ArrayList<Integer> runs,String path) throws DatabaseException {
		
		String filePath="";
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		for(int runId:runs){
			filePath=path+File.separator+runId+File.separator+"ScreenShots";
			File file = new File(filePath);
			//Creating the directory
			file.mkdirs();
			try{
				pst=conn.prepareStatement("SELECT * FROM TJN_EXEC_SCRNSHOT WHERE RUN_ID=? ");
				pst.setInt(1, runId);
				rs = pst.executeQuery();
				while(rs.next())
				{
					String TDGID=rs.getString("TDGID");
					String scrShotseq=rs.getString("SEQ_NO");
					Blob b=rs.getBlob("SCRNSHOT");  
					byte barr[]=b.getBytes(1,(int)b.length()); 
					String scrShotName= TDGID+"_"+scrShotseq+".png";             
					FileOutputStream fout=new FileOutputStream(filePath+File.separator+scrShotName);  
					fout.write(barr);  
					fout.close();  
				}

				pst=conn.prepareStatement("SELECT * FROM RUNRESULT_TS_TXN WHERE RUN_ID=? ");
				pst.setInt(1, runId);
				rs = pst.executeQuery();
				while(rs.next())
				{



					String txnNo=rs.getString("TXN_NO");
					String steprecId=rs.getString("TSTEP_REC_ID");
					if(rs.getBlob("TSTEP_SCRSHOT")!=null){
						Blob b=rs.getBlob("TSTEP_SCRSHOT");  
						byte barr[]=b.getBytes(1,(int)b.length()); 
						String scrShotName= txnNo+"_"+steprecId+".png";             
						FileOutputStream fout=new FileOutputStream(filePath+File.separator+scrShotName);  
						fout.write(barr);  
						fout.close();  
					}
				}

			}catch(Exception e){
				logger.error("Could not fetch data for ",e);
				throw new DatabaseException(e.getMessage(),e);
			}finally{

				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}

		}
	}
	/*Added by Pushpa for TENJINCG-1210 ends*/
	/*Added by Pushpa for TENJINCG-1210 starts*/
	private ArrayList<HashMap<String, String>> getrunTransaction(int runId, int projectId) throws DatabaseException {

		ArrayList<HashMap<String,String>> listOfRunTrasactions = new ArrayList<HashMap<String,String>>();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst=conn.prepareStatement("SELECT * FROM RUNRESULT_TS_TXN WHERE RUN_ID=? ");
			pst.setInt(1, runId);

			rs = pst.executeQuery();
			HashMap<String,String> runTransaction = new HashMap<String,String>();

			while(rs.next()){

				runTransaction.put("RUN_ID", rs.getString("RUN_ID"));
				runTransaction.put("TXN_NO", rs.getString("TXN_NO"));
				runTransaction.put("TSTEP_REC_ID", rs.getString("TSTEP_REC_ID"));
				runTransaction.put("TSTEP_RESULT", rs.getString("TSTEP_RESULT"));
				runTransaction.put("TSTEP_MESSAGE", rs.getString("TSTEP_MESSAGE"));
				//runTransaction.put("TSTEP_SCRSHOT", rs.getString("TSTEP_SCRSHOT"));
				runTransaction.put("TSTEP_WS_REQ_MSG", rs.getString("TSTEP_WS_REQ_MSG"));
				runTransaction.put("TSTEP_WS_RES_MSG", rs.getString("TSTEP_WS_RES_MSG"));
				runTransaction.put("TSTEP_TDUID", rs.getString("TSTEP_TDUID"));
				runTransaction.put("EXEC_START_TIME", rs.getString("EXEC_START_TIME"));
				runTransaction.put("EXEC_END_TIME", rs.getString("EXEC_END_TIME"));
				runTransaction.put("API_ACCESS_TOKEN", rs.getString("API_ACCESS_TOKEN"));
				runTransaction.put("API_AUTH_TYPE", rs.getString("API_AUTH_TYPE"));

				listOfRunTrasactions.add(runTransaction);
			}

		}catch(Exception e){
			logger.error("Could not fetch data ",e);
			throw new DatabaseException(e.getMessage(),e);
		}finally{

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return listOfRunTrasactions;
	}
	/*Added by Pushpa for TENJINCG-1210 ends*/
	/*Added by Pushpa for TENJINCG-1210 starts*/
	private ArrayList<HashMap<String, String>> getDataForResValResults(int runId, int projectId) throws DatabaseException {

		ArrayList<HashMap<String,String>> listOfResValRes = new ArrayList<HashMap<String,String>>();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst=conn.prepareStatement("SELECT * FROM RESVALRESULTS WHERE RUN_ID=? ");
			pst.setInt(1, runId);

			rs = pst.executeQuery();
			HashMap<String,String> resValRes = new HashMap<String,String>();

			while(rs.next()){

				resValRes.put("RUN_ID", rs.getString("RUN_ID"));
				resValRes.put("RES_VAL_ID", rs.getString("RES_VAL_ID"));
				resValRes.put("TSTEP_REC_ID", rs.getString("TSTEP_REC_ID"));
				resValRes.put("RES_VAL_PAGE", rs.getString("RES_VAL_PAGE"));
				resValRes.put("RES_VAL_FIELD", rs.getString("RES_VAL_FIELD"));
				resValRes.put("RES_VAL_EXP_VAL", rs.getString("RES_VAL_EXP_VAL"));
				resValRes.put("RES_VAL_ACT_VAL", rs.getString("RES_VAL_ACT_VAL"));
				resValRes.put("RES_VAL_STATUS", rs.getString("RES_VAL_STATUS"));
				resValRes.put("ITERATION", rs.getString("ITERATION"));
				resValRes.put("RES_VAL_DETAIL_ID", rs.getString("RES_VAL_DETAIL_ID"));

				listOfResValRes.add(resValRes);
			}

		}catch(Exception e){
			logger.error("Could not fetch data ",e);
			throw new DatabaseException(e.getMessage(),e);
		}finally{

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return listOfResValRes;
	}
	/*Added by Pushpa for TENJINCG-1210 ends*/
	/*Added by Pushpa for TENJINCG-1210 starts*/
	private ArrayList<HashMap<String, String>> getDataForUiValRes(int runId, int projectId) throws DatabaseException {

		ArrayList<HashMap<String,String>> listOfUiValRes = new ArrayList<HashMap<String,String>>();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst=conn.prepareStatement("SELECT * FROM UIVALRESULTS WHERE RUN_ID=? ");
			pst.setInt(1, runId);

			rs = pst.executeQuery();
			HashMap<String,String> uiValres = new HashMap<String,String>();

			while(rs.next()){

				uiValres.put("RUN_ID", rs.getString("RUN_ID"));
				uiValres.put("UI_VAL_REC_ID", rs.getString("UI_VAL_REC_ID"));
				uiValres.put("TSTEP_REC_ID", rs.getString("TSTEP_REC_ID"));
				uiValres.put("UI_VAL_PAGE", rs.getString("UI_VAL_PAGE"));
				uiValres.put("UI_VAL_FIELD", rs.getString("UI_VAL_FIELD"));
				uiValres.put("UI_VAL_EXPECTED", rs.getString("UI_VAL_EXPECTED"));
				uiValres.put("UI_VAL_ACTUAL", rs.getString("UI_VAL_ACTUAL"));
				uiValres.put("UI_VAL_STATUS", rs.getString("UI_VAL_STATUS"));
				uiValres.put("ITERATION", rs.getString("ITERATION"));
				uiValres.put("UI_VAL_DETAIL_ID", rs.getString("UI_VAL_DETAIL_ID"));

				listOfUiValRes.add(uiValres);
			}

		}catch(Exception e){
			logger.error("Could not fetch data  ",e);
			throw new DatabaseException(e.getMessage(),e);
		}finally{

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return listOfUiValRes;
	}
	/*Added by Pushpa for TENJINCG-1210 ends*/
	/*Added by Pushpa for TENJINCG-1210 starts*/
	private ArrayList<HashMap<String,String>> getDataForTjnExecOutput(int runId, int projectId) throws DatabaseException {

		ArrayList<HashMap<String,String>> listOfExecOutPut = new ArrayList<HashMap<String,String>>();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst=conn.prepareStatement("SELECT * FROM TJN_EXEC_OUTPUT WHERE RUN_ID=? ");
			pst.setInt(1, runId);

			rs = pst.executeQuery();
			HashMap<String,String> execOutPut = new HashMap<String,String>();

			while(rs.next()){

				execOutPut.put("RUN_ID", rs.getString("RUN_ID"));
				execOutPut.put("TDGID", rs.getString("TDGID"));
				execOutPut.put("TDUID", rs.getString("TDUID"));
				execOutPut.put("TSTEP_REC_ID", rs.getString("TSTEP_REC_ID"));
				execOutPut.put("ITERATION_NO", rs.getString("ITERATION_NO"));
				execOutPut.put("DETAIL_NO", rs.getString("DETAIL_NO"));
				execOutPut.put("FIELD_NAME", rs.getString("FIELD_NAME"));
				execOutPut.put("FIELD_VALUE", rs.getString("FIELD_VALUE"));
				listOfExecOutPut.add(execOutPut);
			}

		}catch(Exception e){
			logger.error("Could not fetch data  ",e);
			throw new DatabaseException(e.getMessage(),e);
		}finally{

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return listOfExecOutPut;
	}

	/*Added by Pushpa for TENJINCG-1210 ends*/
	/*Added by Pushpa for TENJINCG-1210 starts*/
	private void generateExcelForRuns(List<HashMap> runsData, String path, String fileName) throws JSONException, IOException {
		String downloadPath="";
		String fName="";

		for(HashMap map:runsData){

			logger.debug("Creating workbook");
			XSSFWorkbook workbook = new XSSFWorkbook();
			boolean createDir=true;
			Iterator<?> it=map.entrySet().iterator();
			while(it.hasNext()){
				Map.Entry<String, ArrayList<HashMap>> entry = (Entry<String, ArrayList<HashMap>>) it.next();
				ArrayList<HashMap> list=entry.getValue();

				XSSFSheet sheet = workbook.createSheet(entry.getKey());
				int rowId = 1;
				int rowHeaderId=0;
				sheet.createRow(rowHeaderId);
				logger.debug("Initializing fonts and styles");
				CellStyle cellStyle = this.getHeaderRowStyle(workbook);
				logger.debug("Create "+entry.getKey()+" Sheet --> DONE");


				for(HashMap<String,String> data:list){
					logger.debug("Writing data in row {}", rowId);
					int col=0;
					sheet.createRow(rowId);
					for (Map.Entry<String,String> cellData : data.entrySet()) 
					{
						if(rowHeaderId==0){
							sheet.getRow(rowHeaderId).createCell(col).setCellType(Cell.CELL_TYPE_STRING);
							sheet.getRow(rowHeaderId).getCell(col).setCellStyle(cellStyle);
							sheet.getRow(rowHeaderId).getCell(col).setCellValue(cellData.getKey());
							if(createDir && cellData.getKey().equalsIgnoreCase("RUN_ID")){
								downloadPath=path+File.separator+cellData.getValue();
								fName=fileName+cellData.getValue();
								File file = new File(downloadPath);
								//Creating the directory
								file.mkdirs();
								createDir=false;
							}
						}
						sheet.getRow(rowId).createCell(col).setCellType(Cell.CELL_TYPE_STRING);
						sheet.getRow(rowId).getCell(col).setCellValue(cellData.getValue());
						sheet.autoSizeColumn(col);
						col++;
					}
					rowHeaderId++;
					rowId++;
				}
			}
			logger.info("Saving file to disk");
			if(fName != null && !fName.endsWith(".xlsx")){
				fName = fName + ".xlsx";
			}else if(fName == null || fName.equalsIgnoreCase("")){
				fName = "Tenjin_TestRuns_Download.xlsx";
			}

			FileOutputStream fout = new FileOutputStream(downloadPath + File.separator+ fName);
			try {
				workbook.write(fout);
			} catch (IOException e) {
				
				logger.error("Error ", e);
			}finally{
				fout.close();
			}
		}

	}
	/*Added by Pushpa for TENJINCG-1210 ends*/
	/*Added by Pushpa for TENJINCG-1210 starts*/
	private ArrayList<HashMap<String,String>> getDataFromMasTestRuns(int runId, int projectId) throws DatabaseException {
		ArrayList<HashMap<String,String>> mastetRuns = new ArrayList<HashMap<String,String>>();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst=conn.prepareStatement("SELECT * FROM MASTESTRUNS WHERE RUN_ID=? AND RUN_PRJ_ID=? ");
			pst.setInt(1, runId);
			pst.setInt(2,projectId);
			rs = pst.executeQuery();
			HashMap<String,String> masTestRuns = new HashMap<String,String>();
			while(rs.next()){
				masTestRuns.put("RUN_ID", rs.getString("RUN_ID"));
				masTestRuns.put("RUN_PRJ_ID", rs.getString("RUN_PRJ_ID"));
				masTestRuns.put("RUN_TS_REC_ID", rs.getString("RUN_TS_REC_ID"));
				masTestRuns.put("RUN_START_TIME", rs.getString("RUN_START_TIME"));
				masTestRuns.put("RUN_END_TIME", rs.getString("RUN_END_TIME"));
				masTestRuns.put("RUN_USER", rs.getString("RUN_USER"));
				masTestRuns.put("RUN_STATUS", rs.getString("RUN_STATUS"));
				masTestRuns.put("TOTAL_TCS", rs.getString("TOTAL_TCS"));
				masTestRuns.put("TOTAL_EXE_TCS", rs.getString("TOTAL_EXE_TCS"));
				masTestRuns.put("TOTAL_PASSED_TCS", rs.getString("TOTAL_PASSED_TCS"));
				masTestRuns.put("TOTAL_FAILED_TCS", rs.getString("TOTAL_FAILED_TCS"));
				masTestRuns.put("TOTAL_ERROR_TCS", rs.getString("TOTAL_ERROR_TCS"));
				masTestRuns.put("RUN_MACHINE_IP", rs.getString("RUN_MACHINE_IP"));
				masTestRuns.put("RUN_BROWSER_TYPE", rs.getString("RUN_BROWSER_TYPE"));
				masTestRuns.put("RUN_PRJ_NAME", rs.getString("RUN_PRJ_NAME"));
				masTestRuns.put("RUN_DOMAIN_NAME", rs.getString("RUN_DOMAIN_NAME"));
				masTestRuns.put("RUN_EXEC_SPEED", rs.getString("RUN_EXEC_SPEED"));
				masTestRuns.put("RUN_TASK_TYPE", rs.getString("RUN_TASK_TYPE"));
				masTestRuns.put("RUN_CL_PORT", rs.getString("RUN_CL_PORT"));
				masTestRuns.put("RUN_APP_ID", rs.getString("RUN_APP_ID"));
				masTestRuns.put("PARENT_RUN_ID", rs.getString("PARENT_RUN_ID"));
				masTestRuns.put("RUN_DEVICE_REC_ID", rs.getString("RUN_DEVICE_REC_ID"));
				masTestRuns.put("RUN_DEVICE_FARM", rs.getString("RUN_DEVICE_FARM"));
				masTestRuns.put("RUN_JSON", rs.getString("RUN_JSON"));
				mastetRuns.add(masTestRuns);
			}

		}catch(Exception e){
			logger.error("Could not fetch data for ",e);
			throw new DatabaseException(e.getMessage(),e);
		}finally{

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return mastetRuns;
	}
	/*Added by Pushpa for TENJINCG-1210 ends*/

}




