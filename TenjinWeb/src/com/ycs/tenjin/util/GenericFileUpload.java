package com.ycs.tenjin.util;

import java.io.InputStream;

import com.ycs.tenjin.project.TestDataPath;

public interface GenericFileUpload {
	
	public void uploadTestDataFile(String rootPath,TestDataPath tdp,String file,InputStream inputstream,String repotype,String uploadFolderPath);

	

}
