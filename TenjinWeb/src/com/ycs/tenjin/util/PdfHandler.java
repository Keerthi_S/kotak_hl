/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  PdfHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE              CHANGED BY           	DESCRIPTION
 * 29-Aug-2016		SRIRAM SRIDHARAN		Req#TJN_23_27
 * 03-Nov-2016		SRIRAM SRIDHARAN		Defect#697
 * 18-Aug-2017		SRIRAM SRIDHARAN		T25IT-203
 * 01-Sep-2017		SRIRAM SRIDHARAN		T25IT-414
 * 09-10-2017       Gangadhar Badagi        To order the screenshots
 * 30-01-2018		Preeti					TENJINCG-503
 * 09-02-2018		Preeti					TENJINCG-598
 * 12-04-2018       Padmavathi              TENJINCG-633
 * 22-Dec-2017		Sahana					Mobility
 * 05-07-2018		Preeti					T251IT-178
 * 24-08-2018       Padmavathi              for TENJINCG-726
 * 15-10-2018		Preeti					TENJINCG-878
 * 24-10-2018       Ramya                   for TENJINCG-832
 * 26-10-2018       Ramya                   for TENJINCG-832
 * 26-Nov 2018		Shivam	Sharma			TENJINCG-904
 * 30-11-2018       Padmavathi              TENJINCG-853
 * 06-Dec-2018		Sahana					TENJINCG-904
 * 07-dec-2018		Pushpa					TENJINCG-909,TENJINCG-910	
 * 26-12-2018		Preeti					TJN262R2-72
 * 26-12-2018		Pushpa					TJN262R2-9 
 * 31-12-2018		Preeti					TJNUN262-48
 * 03-01-2019		Ashiki					TJN252-42
 * 09-01-2019		Pushpa					TJN252-64
 * 22-03-2019		Preeti					TENJINCG-1018
 * 25-Jan 2019		Sahana					pCloudy
 * 23-05-2019		Preeti					for Test reports pdf method
 * 21-06-2019		Preeti					V2.8-71
 * 18-20-2019		Roshni					TJN252-8
 * 01-06-2020		Ashiki					Tenj210-104
 * * 28-09-2020		Ashiki					TENJINCG-1211
 * 10-12-2020       Shruthi                 TENJINCG-1238
 */  

package com.ycs.tenjin.util;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.pojo.project.UIValidation;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.RuntimeScreenshot;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.bridge.pojo.run.UIValidationResult;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.bridge.utils.ApiUtilities;
import com.ycs.tenjin.bridge.utils.MessageUtil;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.AutsHelper;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.DeviceHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.db.ResultsHelper;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.db.UIValidationHelper;
import com.ycs.tenjin.device.RegisteredDevice;
import com.ycs.tenjin.handler.TestStepHandler;
import com.ycs.tenjin.pcloudy.PcloudyProcessor;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.ExecutionSettings;
import com.ycs.tenjin.run.TestRun;

public class PdfHandler {
	private static final Logger logger = LoggerFactory.getLogger(PdfHandler.class);
	private TestRun run;
	private Project project;
	/*added by sahana for Mobility: Starts*/
	private RegisteredDevice device;
	/*added by sahana for Mobility: ends*/
	private String contextRoot = "";
	private static Font titleFont = new Font(Font.FontFamily.HELVETICA,42,Font.BOLD);
	private String userFullName;
	private SimpleDateFormat sdf;
	/*Added by Padmavathi for TENJINCG-726 starts*/
	private boolean isPureRun=true;
	/*Added by Padmavathi for TENJINCG-726 ends*/
	/*Added by Ashiki for TENJINCG-1211 starts*/
	private List<TestRun> runs;
	/*Added by Ashiki for TENJINCG-1211 end*/
	public PdfHandler(String contextRoot, String userFullName){
		this.contextRoot = contextRoot;
		FontFactory.register(this.contextRoot + "\\css\\fonts\\Open_Sans\\OpenSans-Bold-webfont.ttf", "Open_Sans_Bold");
		FontFactory.register(this.contextRoot + "\\css\\fonts\\Open_Sans\\OpenSans-BoldItalic-webfont.ttf", "Open_Sans_Bold_Italic");
		FontFactory.register(this.contextRoot + "\\css\\fonts\\Open_Sans\\OpenSans-Italic-webfont.ttf", "Open_Sans_Italic");
		FontFactory.register(this.contextRoot + "\\css\\fonts\\Open_Sans\\OpenSans-Regular-webfont.ttf", "Open_Sans");
		//FIx for T25IT-414
		this.sdf = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
		//FIx for T25IT-414 ends
		this.userFullName =userFullName;
	}

	public static void main(String args[]) throws DatabaseException{
		PdfHandler h = new PdfHandler("G:","Sriram Sridharan");
		try {
			h.generatePdfReportForRun(19, 1, "G:\\");
			System.out.println("Done");
		} catch (TenjinPdfException e) {
			
			logger.error("Error ", e);
		}
	}
	public String generatePdfReportForRun(int runId, int projectId, String folderPath) throws TenjinPdfException, DatabaseException{
		String fileName = "Tenjin_Execution_Report_Run_" + runId + ".pdf";
		Document document = new Document();

		/* For Issue TJN_22_21 PDF Generation is time consuming
		 * Using one Connection and passing it to the subsequent method calls
		 */

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}

		/* For Issue TJN_22_21 PDF Generation is time consuming
		 * Using one Connection and passing it to the subsequent method calls
		 */
		try {
			PdfWriter.getInstance(document, new FileOutputStream(folderPath + "\\" + fileName));
			document.open();

			ResultsHelper helper = new ResultsHelper();
			//Fix for Defect#731 - Sriram
			TestRun run = helper.hydrateRun(runId, projectId);
			//Fix for Defect#731 - Sriram ends
			this.project = new ProjectHelper().hydrateProject(conn,projectId);
			/*Added by Padmavathi for TENJINCG-726 starts*/
			if(run.getPureRunId()!=runId){
				isPureRun=false;
				run.setStartTimeStamp(helper.hydrateBasicRunInformation(run.getPureRunId()).getStartTimeStamp());
				run.setElapsedTime(helper.getElapsedTimeForOveralRun(runId));
			}
			/*Added by Padmavathi for TENJINCG-726 ends*/
			this.run = run;
			/*Added by Padmavathi for TENJINCG-853 starts*/
			if(this.run.getDeviceRecId()>0){
				/*Added by Padmavathi for TENJINCG-853 ends*/
				ExecutionSettings execSettings=new RunHelper().fetchExecutionSettings(this.run.getId());
				/*Added by Sahana for pCloudy:Starts*/
				RegisteredClient client=new ClientHelper().hydrateClientByHostName(execSettings.getClient());
				if(client.getDeviceFarmCheck().equalsIgnoreCase("Y"))
					this.device=new PcloudyProcessor().deviceById(run.getDeviceRecId());
				/*Added by Sahana for pCloudy:Ends*/
				else
					this.device=new DeviceHelper().hydrateDevice(this.run.getDeviceRecId());
			}
			
			//Create Metadata
			this.addMetadata(document);

			//Create title age
			this.createScriptReportTitlePage(document);

			//Create Summary Page
			this.createSummaryPage(document);

			if(this.run.getTestSet().getTests() != null && this.run.getTestSet().getTests().size() > 0){
				for(TestCase t:this.run.getTestSet().getTests()){
					this.createTestCaseDetailedResultPage(conn,document, t);
				}
			}

			document.close();

			return fileName;
		} catch (FileNotFoundException e) {
			
			throw new TenjinPdfException("Could not generate PDF Report due to an internal error",e);
		} catch (DocumentException e) {
			
			throw new TenjinPdfException("Could not generate PDF Report due to an internal error",e);
		} catch (DatabaseException e) {
			
			throw new TenjinPdfException("Could not generate PDF Report due to an internal error",e);
		} /*catch (SQLException e) {
			
			throw new TenjinPdfException("Could not generate PDF Report due to an internal error",e);
		}*/catch (Exception e) {
			
			throw new TenjinPdfException("Could not generate PDF Report due to an internal error",e);
		}/*Added by paneendra for TENJINCG-1267 starts*/
		finally {

			DatabaseHelper.close(conn);
		}
		/*Added by paneendra for TENJINCG-1267 ends*/

	}
	

	private void addMetadata(Document document){
		document.addTitle("Tenjin Execution Report");
		document.addSubject("Execution Report of a Tenjin Test Run");
		document.addKeywords("Tenjin, Results");
		document.addAuthor("Tenjin");
		document.addCreator("Tenjin");
	}

	private void addEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

	private void createSummaryPage(Document document) throws TenjinPdfException{
		try{

			Font summaryTitleFont = FontFactory.getFont("Open_Sans_Bold",22,Font.BOLD, BaseColor.BLUE);
			/*Added by Padmavathi for TENJINCG-726 starts*/
			Font summaryMessage = FontFactory.getFont("Open_Sans",10,BaseColor.BLACK);
			/*Added by Padmavathi for TENJINCG-726 ends*/
			document.newPage();

			Paragraph overview = new Paragraph("Overview",summaryTitleFont);
			this.addEmptyLine(overview, 1);

			document.add(overview);



			PdfPTable overviewTable = new PdfPTable(2);
			overviewTable.setWidths(new int[]{4,6});
			overviewTable.setWidthPercentage(75);

			PdfPCell d = this.createSummaryTableCell("Particulars", "Domain");
			PdfPCell dName = this.createSummaryTableCell("Detail", this.project.getDomain());
			PdfPCell p = this.createSummaryTableCell("Particulars", "Project");
			PdfPCell pName = this.createSummaryTableCell("Detail", this.project.getName());
			PdfPCell t = this.createSummaryTableCell("Particulars", "Test Set Name");
			PdfPCell tName = this.createSummaryTableCell("Detail", this.run.getTestSet().getName());
			PdfPCell r = this.createSummaryTableCell("Particulars", "Run ID");
			PdfPCell rName = this.createSummaryTableCell("Detail", this.run.getId());
			PdfPCell u = this.createSummaryTableCell("Particulars", "Executed By");
			PdfPCell uName = this.createSummaryTableCell("Detail", this.run.getUser());


			overviewTable.addCell(d);
			overviewTable.addCell(dName);
			overviewTable.addCell(p);
			overviewTable.addCell(pName);
			overviewTable.addCell(t);
			overviewTable.addCell(tName);
			overviewTable.addCell(r);
			overviewTable.addCell(rName);
			overviewTable.addCell(u);
			overviewTable.addCell(uName);
			document.add(overviewTable);

			Paragraph executionTimeSummary = new Paragraph("Execution Time Summary",summaryTitleFont);
			this.addEmptyLine(executionTimeSummary, 1);

			document.add(executionTimeSummary);

			PdfPTable eTimeSummary = new PdfPTable(2);
			eTimeSummary.setWidths(new int[]{4,6});
			eTimeSummary.setWidthPercentage(75);


			PdfPCell e = this.createSummaryTableCell("particulars", "Execution Start Time");
			PdfPCell eValue = this.createSummaryTableCell("Detail", this.sdf.format(this.run.getStartTimeStamp()));
			PdfPCell eEnd = this.createSummaryTableCell("particulars", "Execution End Time");
			PdfPCell eEndValue = this.createSummaryTableCell("detail", this.sdf.format(this.run.getEndTimeStamp()));
			/*modified by shruthi for TENJINCG-1226 starts*/
			PdfPCell elapsedTime = this.createSummaryTableCell("particulars", "Duration");
			/*modified by shruthi for TENJINCG-1226 ends*/
			PdfPCell elapsedTimeValue = this.createSummaryTableCell("detail", this.run.getElapsedTime());

			eTimeSummary.addCell(e);
			eTimeSummary.addCell(eValue);
			eTimeSummary.addCell(eEnd);
			eTimeSummary.addCell(eEndValue);
			eTimeSummary.addCell(elapsedTime);
			eTimeSummary.addCell(elapsedTimeValue);
			document.add(eTimeSummary);

			/*Added by Padmavathi for TENJINCG-726 starts*/
			if(!isPureRun){
				Paragraph msg = new Paragraph("This is consolidated report of multiple runs.",summaryMessage);
				this.addEmptyLine(msg, 1);
				msg.setAlignment(Element.ALIGN_CENTER);
				document.add(msg);
			}
			/*Added by Padmavathi for TENJINCG-726 ends*/
			Paragraph execSummary = new Paragraph("Execution Summary",summaryTitleFont);
			this.addEmptyLine(execSummary, 1);

			document.add(execSummary);

			PdfPTable eSummary = new PdfPTable(2);
			eSummary.setWidths(new int[]{4,6});
			eSummary.setWidthPercentage(75);

			PdfPCell c1 = this.createSummaryTableCell("particulars", "Total Test Cases");
			PdfPCell c2 = this.createSummaryTableCell("detail", this.run.getTestSet().getTests().size());
			PdfPCell c3 = this.createSummaryTableCell("particulars", "Test Cases Executed");
			PdfPCell c4 = this.createSummaryTableCell("detail", this.run.getExecutedTests());
			PdfPCell c5 = this.createSummaryTableCell("particulars", "Test Cases Passed");
			PdfPCell c6 = this.createSummaryTableCell("detail", this.run.getPassedTests());
			PdfPCell c7 = this.createSummaryTableCell("particulars", "Test Cases Failed");
			PdfPCell c8 = this.createSummaryTableCell("detail", this.run.getFailedTests());
			PdfPCell c9 = this.createSummaryTableCell("particulars", "Test Cases with Error");
			PdfPCell c10 = this.createSummaryTableCell("detail", this.run.getErroredTests());


			eSummary.addCell(c1);
			eSummary.addCell(c2);
			eSummary.addCell(c3);
			eSummary.addCell(c4);
			eSummary.addCell(c5);
			eSummary.addCell(c6);
			eSummary.addCell(c7);
			eSummary.addCell(c8);
			eSummary.addCell(c9);
			eSummary.addCell(c10);

			document.add(eSummary);



			ExecutionSettings exeSettings=new RunHelper().fetchExecutionSettings(this.run.getId());
				Paragraph execSettingsSummary = new Paragraph("Execution Settings Summary",summaryTitleFont);
				this.addEmptyLine(execSettingsSummary, 1);

				document.add(execSettingsSummary);

				PdfPTable exeSetSummary = new PdfPTable(2);
				exeSetSummary.setWidths(new int[]{4,6});
				exeSetSummary.setWidthPercentage(75);

				PdfPCell client = this.createSummaryTableCell("particulars", "Client");
				/*Modified by Preeti for V2.8-71 starts*/
				PdfPCell clientVal =null;
				/* Modified by Ruksar for TENJINCG-1212 starts*/
				PdfPCell browserType=null;
				try{
				if(exeSettings.getClient().equalsIgnoreCase("0:0:0:0:0:0:0:1"))
					clientVal = this.createSummaryTableCell("detail", "Localhost");
				else
					clientVal = this.createSummaryTableCell("detail", exeSettings.getClient());
				}catch(NullPointerException e1){
					clientVal = this.createSummaryTableCell("detail", "NA");
				}
			 /* Modified by Ruksar for TENJINCG-1212 ends*/
				/*Modified by Preeti for V2.8-71 ends*/
				PdfPCell browser = this.createSummaryTableCell("particulars", "Browser");
				/* Modified by Ruksar for TENJINCG-1212 starts*/
				try{
				browserType = this.createSummaryTableCell("detail", exeSettings.getBrowser());
				}catch(NullPointerException e1){
					clientVal = this.createSummaryTableCell("detail", "NA");
				}
				 /* Modified by Ruksar for TENJINCG-1212 ends*/
			
				PdfPCell scrnShotOption = this.createSummaryTableCell("particulars", "Screenshot Option");
				PdfPCell scrnShotOptionVal = this.createSummaryTableCell("detail", exeSettings.getScrnShotOption());

				exeSetSummary.addCell(client);
				exeSetSummary.addCell(clientVal);
				exeSetSummary.addCell(browser);
				exeSetSummary.addCell(browserType);
				
				exeSetSummary.addCell(scrnShotOption);
				exeSetSummary.addCell(scrnShotOptionVal);
				document.add(exeSetSummary);
			/*}*/
			
			/*added by sahana for Mobility: Starts*/
			if(run.getDeviceRecId()>0)
			{
				/*Added by Ramya for TENJINCG-832 starts*/
				document.newPage();
				/*Added by Ramya for TENJINCG-832 ends*/
				Paragraph deviceSummary = new Paragraph("Device Summary",summaryTitleFont);
				this.addEmptyLine(deviceSummary, 1);

				document.add(deviceSummary);

				PdfPTable dSummary = new PdfPTable(2);
				dSummary.setWidths(new int[]{4,6});
				dSummary.setWidthPercentage(75);
				/*Modified by Preeti for T251IT-178 starts*/
				if(this.device != null){
					PdfPCell d1 = this.createSummaryTableCell("particulars", "Device Id");
					PdfPCell d2 = this.createSummaryTableCell("detail", this.device.getDeviceId());
					PdfPCell d3 = this.createSummaryTableCell("particulars", "Device Name");
					PdfPCell d4 = this.createSummaryTableCell("detail",this.device.getDeviceName());
					PdfPCell d5 = this.createSummaryTableCell("particulars", "Device Type");
					PdfPCell d6 = this.createSummaryTableCell("detail",this.device.getDeviceType());
					PdfPCell d7 = this.createSummaryTableCell("particulars", "Platform");
					PdfPCell d8 = this.createSummaryTableCell("detail",this.device.getPlatform() );
					PdfPCell d9 = this.createSummaryTableCell("particulars", "Platform Version");
					PdfPCell d10 = this.createSummaryTableCell("detail", this.device.getPlatformVersion() );
					dSummary.addCell(d1);
					dSummary.addCell(d2);
					dSummary.addCell(d3);
					dSummary.addCell(d4);
					dSummary.addCell(d5);
					dSummary.addCell(d6);
					dSummary.addCell(d7);
					dSummary.addCell(d8);
					dSummary.addCell(d9);
					dSummary.addCell(d10);
				}
				else{
					PdfPCell d1 = this.createSummaryTableCell("particulars", "Device details");
					PdfPCell d2 = this.createSummaryTableCell("detail", "Device information not available.");
					dSummary.addCell(d1);
					dSummary.addCell(d2);
				}
				/*Modified by Preeti for T251IT-178 ends*/
				document.add(dSummary);	
			}
			/*added by sahana for Mobility: Starts*/
		}catch(Exception e){
			throw new TenjinPdfException("Could not create Summary Page",e);
		}
	}

	private void createTestCaseDetailedResultPage(Connection conn,Document document, TestCase testCase) throws TenjinPdfException{
		try{

			Font title = FontFactory.getFont("Open_Sans_Bold",19,Font.BOLD, BaseColor.BLUE);
			Font testStepTitle = FontFactory.getFont("Open_Sans_Bold",17,Font.BOLD);
			Font iterationTitle = FontFactory.getFont("Open_Sans_Bold_Italic",14,Font.BOLD);
			Font rvTitle = FontFactory.getFont("Open_Sans",12,Font.ITALIC, BaseColor.MAGENTA);
			document.newPage();

			Paragraph tcTitle = new Paragraph("Test Case ID " + testCase.getTcId(), title);
			this.addEmptyLine(tcTitle, 1);
			document.add(tcTitle);

			PdfPTable table = new PdfPTable(2);
			table.setWidths(new int[]{4,6});
			table.setWidthPercentage(75);

			PdfPCell c1 = this.createSummaryTableCell("Particulars", "Name");
			PdfPCell c2 = this.createSummaryTableCell("detail", testCase.getTcName());
			PdfPCell c3 = this.createSummaryTableCell("Particulars", "Description");
			PdfPCell c4 = this.createSummaryTableCell("detail", testCase.getTcDesc());
			PdfPCell c5 = this.createSummaryTableCell("Particulars", "Status");
			PdfPCell c6 = this.createSummaryTableCell("detail", testCase.getTcStatus());
			/*PdfPCell c6 = this.createSummaryTableCell("detail", testCase.getResult().getStatus());*/

			table.addCell(c1);
			table.addCell(c2);
			table.addCell(c3);
			table.addCell(c4);
			table.addCell(c5);
			table.addCell(c6);

			document.add(table);
			int stepCount = 0;
			for(TestStep step:testCase.getTcSteps()){
				stepCount++;
				Paragraph stepTitle = new Paragraph("Step " + stepCount + " - " + step.getId(), testStepTitle);
				this.addEmptyLine(stepTitle, 1);

				document.add(stepTitle);

				PdfPTable stepSummary = new PdfPTable(2);
				stepSummary.setWidthPercentage(75);
				stepSummary.setWidths(new int[]{4,6});

				PdfPCell cell1 = this.createSummaryTableCell("Particulars", "Short Description");
				PdfPCell cell2 = this.createSummaryTableCell("detail", step.getShortDescription());
				PdfPCell cell3 = this.createSummaryTableCell("Particulars", "Type");
				PdfPCell cell4 = this.createSummaryTableCell("Detail", step.getType());
				PdfPCell cell5 = this.createSummaryTableCell("Particulars", "Application");
				PdfPCell cell6 = this.createSummaryTableCell("Detail", step.getAppName());
				/* Changes for API Testing (TENJINCG-168) - Sriram */
				
				PdfPCell cell7;
				PdfPCell cell8;
				PdfPCell cell7_1 = null;
				PdfPCell cell8_1 = null;
				if(Utilities.trim(step.getTxnMode()).equalsIgnoreCase("api")){
					cell7 = this.createSummaryTableCell("Particulars", "API");
					/*Modified by Preeti for TENJINCG-503 starts*/
					cell8 = this.createSummaryTableCell("Detail", step.getModuleCode());
					/*Modified by Preeti for TENJINCG-503 ends*/
					cell7_1 = this.createSummaryTableCell("Particulars", "Operation");
					cell8_1 = this.createSummaryTableCell("Detail", step.getOperation());
				}else{
					cell7 = this.createSummaryTableCell("Particulars", "Function");
					cell8 = this.createSummaryTableCell("Detail", step.getModuleCode());
				}
				/* Changes for API Testing (TENJINCG-168) - Sriram ends*/
				PdfPCell cell9 = this.createSummaryTableCell("Particulars", "Expected Result");
				PdfPCell cell10 = this.createSummaryTableCell("Detail", step.getExpectedResult());
				PdfPCell cell11 = this.createSummaryTableCell("Particulars", "Status");
				//Fix for Defect#731 - Sriram
				PdfPCell cell12 = this.createSummaryTableCell("Detail", step.getStatus());
				//Fix for Defect#731 - Sriram ends

				stepSummary.addCell(cell1);
				stepSummary.addCell(cell2);
				stepSummary.addCell(cell3);
				stepSummary.addCell(cell4);
				stepSummary.addCell(cell5);
				stepSummary.addCell(cell6);
				stepSummary.addCell(cell7);
				stepSummary.addCell(cell8);

				/* Changes for API Testing (TENJINCG-168) - Sriram */
				if(Utilities.trim(step.getTxnMode()).equalsIgnoreCase("api")){
					stepSummary.addCell(cell7_1);
					stepSummary.addCell(cell8_1);
				}
				/* Changes for API Testing (TENJINCG-168) - Sriram ends*/

				stepSummary.addCell(cell9);
				stepSummary.addCell(cell10);
				stepSummary.addCell(cell11);
				stepSummary.addCell(cell12);

				document.add(stepSummary);

				//ArrayList<StepIterationResult> results = new ResultsHelper().hydrateDetailedStepResult(step.getRecordId(), testCase.getTcRecId(), this.run.getId());
				//Fix for Defect#731 - Sriram
				//ArrayList<StepIterationResult> results = new ResultsHelper().hydrateDetailedStepResult(conn,step.getRecordId(), testCase.getTcRecId(), this.run.getId());
				int iterCount = 0;
				for(StepIterationResult result:step.getDetailedResults()){
					iterCount++;
					/*Changed by Pushpa for  TJN252-64 starts*/
					if(step.getDetailedResults().size()>1) {
					Paragraph iter = new Paragraph("Results for Test Data " + iterCount,iterationTitle);
					this.addEmptyLine(iter, 1);
					document.add(iter);
					}
					/*Changed by Pushpa for  TJN252-64 ends*/
					/*Modified by Preeti for TENJINCG-598 starts*/
					Paragraph iterDesc;
					if(step.getTxnMode().equalsIgnoreCase("GUI"))
						iterDesc = new Paragraph("Following are the GUI results for this transaction");
					else
						iterDesc = new Paragraph("Following are the API results for this transaction");
					/*Modified by Preeti for TENJINCG-598 ends*/	
					this.addEmptyLine(iterDesc, 1);
					document.add(iterDesc);

					PdfPTable guiResults = new PdfPTable(2);
					guiResults.setWidthPercentage(75);
					guiResults.setWidths(new int[]{4,6});

					String status = "";
					if(result.getResult().equalsIgnoreCase("S")){
						status = "Pass";
					}else if(result.getResult().equalsIgnoreCase("F")){
						status = "Fail";
					}else if(result.getResult().equalsIgnoreCase("E")){
						status = "Error";
					}

					PdfPCell cel1 = this.createSummaryTableCell("Particulars", "Output");
					PdfPCell cel2 = this.createSummaryTableCell("Detail", result.getMessage());
					PdfPCell cel3 = this.createSummaryTableCell("Particulars", "Status");
					PdfPCell cel4 = this.createSummaryTableCell("Detail", status);
					/*Added by Preeti for TENJINCG-598 starts*/
					PdfPCell cel5 = this.createSummaryTableCell("Particulars", "Elapsed Time");
					PdfPCell cel6 = this.createSummaryTableCell("Detail", result.getElapsedTime());
					/*Added by Preeti for TENJINCG-598 ends*/
					guiResults.addCell(cel1);
					guiResults.addCell(cel2);
					guiResults.addCell(cel3);
					guiResults.addCell(cel4);
					/*Added by Preeti for TENJINCG-598 starts*/
					guiResults.addCell(cel5);
					guiResults.addCell(cel6);
					/*Added by Preeti for TENJINCG-598 ends*/
					document.add(guiResults);

					Paragraph empty = new Paragraph("");
					this.addEmptyLine(empty, 1);
					document.add(empty);

					

					/* Changes for API Testing (TENJINCG-168) - Sriram */
					if(result.getRuntimeFieldValues() != null && result.getRuntimeFieldValues().size() > 0) {
						Paragraph rtvTitle = new Paragraph("Run-time Values", rvTitle);
						this.addEmptyLine(rtvTitle, 1);
						document.add(rtvTitle);

						PdfPTable rtvTable = new PdfPTable(3);
						rtvTable.setWidthPercentage(75);
						rtvTable.setWidths(new int[]{4,4,2});

						PdfPCell rtvHfield = this.createSummaryTableCell("Particulars", "Field");
						PdfPCell rtvHvalue = this.createSummaryTableCell("Particulars", "Value");
						PdfPCell rtvHdetail = this.createSummaryTableCell("Particulars", "Detail ID");
						rtvTable.addCell(rtvHfield);
						rtvTable.addCell(rtvHvalue);
						rtvTable.addCell(rtvHdetail);

						for(RuntimeFieldValue rv: result.getRuntimeFieldValues()) {
							PdfPCell f = this.createSummaryTableCell("Detail", rv.getField());
							PdfPCell v = this.createSummaryTableCell("Detail", rv.getValue());
							PdfPCell d = this.createSummaryTableCell("Detail", rv.getDetailRecordNo());

							rtvTable.addCell(f);
							rtvTable.addCell(v);
							rtvTable.addCell(d);
						}

						document.add(rtvTable);

					}
					/* Changes for API Testing (TENJINCG-168) - Sriram ends*/


					//Changed by Sriram for Defect#731
					if(result.getValidationResults() != null && result.getValidationResults().size() > 0){
						Paragraph valTitle = new Paragraph("Field Validations", rvTitle);
						this.addEmptyLine(valTitle, 1);
						document.add(valTitle);
						PdfPTable valResults = new PdfPTable(6);
						valResults.setWidthPercentage(100);
						valResults.setWidths(new int[]{1, 2, 2, 2, 2, 1});

						PdfPCell h = this.createSummaryTableCell("Particulars", "#");
						valResults.addCell(h);
						h = this.createSummaryTableCell("Particulars", "Page Name");
						valResults.addCell(h);
						h = this.createSummaryTableCell("Particulars", "Field Name");
						valResults.addCell(h);
						h = this.createSummaryTableCell("Particulars", "Expected Value");
						valResults.addCell(h);
						h = this.createSummaryTableCell("Particulars", "Actual Value");
						valResults.addCell(h);
						h = this.createSummaryTableCell("Particulars", "Status");
						valResults.addCell(h);
						int vCount = 0;
						for(ValidationResult r:result.getValidationResults()){
							vCount++;
							h = this.createSummaryTableCell("Detail", vCount);
							valResults.addCell(h);
							h = this.createSummaryTableCell("Detail", r.getPage());
							valResults.addCell(h);
							h = this.createSummaryTableCell("Detail", r.getField());
							valResults.addCell(h);
							h = this.createSummaryTableCell("Detail", r.getExpectedValue());
							valResults.addCell(h);
							h = this.createSummaryTableCell("Detail", r.getActualValue());
							valResults.addCell(h);
							h = this.createSummaryTableCell("Detail", r.getStatus());
							valResults.addCell(h);
						}
						document.add(valResults);
					}else{
						//Fix for T25IT-203
						/*Paragraph valParagraph = new Paragraph("There are no validations for this step");*/
						Paragraph valParagraph = new Paragraph("There are no field validations for this step");
						//Fix for T25IT-203 ends
						this.addEmptyLine(valParagraph, 1);
						document.add(valParagraph);
					}

					
					//Changed by Sriram for Defect#731 ends


					//Fix for T25IT-203
					if("ui".equalsIgnoreCase(step.getValidationType())) {
						Map<String, Map<String, List<UIValidation>>> map = this.getUIValidationResultsForStep(this.run.getId(), step.getRecordId());
						if(map != null && map.keySet().size() > 0) {
							for(String validationType : map.keySet()) {
								for(String pageName : map.get(validationType).keySet()) {
									Paragraph valTitle = new Paragraph("UI Validation: " + validationType + " on page " + pageName, rvTitle);
									this.addEmptyLine(valTitle, 1);
									document.add(valTitle);
									PdfPTable valResults = new PdfPTable(3);
									valResults.setWidthPercentage(100);
									valResults.setWidths(new int[]{2,4,4});
									PdfPCell h = this.createSummaryTableCell("Particulars", "Field");
									valResults.addCell(h);
									h = this.createSummaryTableCell("Particulars", "Expected");
									valResults.addCell(h);
									h = this.createSummaryTableCell("Particulars", "Actual");
									valResults.addCell(h);
									for(UIValidation val : map.get(validationType).get(pageName)) {
										for(UIValidationResult uResult : val.getResults()) {
											h = this.createSummaryTableCell("Detail", uResult.getField());
											valResults.addCell(h);
											h = this.createSummaryTableCell("Detail", uResult.getExpectedValue());
											valResults.addCell(h);
											h = this.createSummaryTableCell("Detail", uResult.getActualValue());
											valResults.addCell(h);
										}
									}
									document.add(valResults);
								}
							}
						}else {
							Paragraph valParagraph = new Paragraph("There are no UI validations for this step");
							this.addEmptyLine(valParagraph, 1);
							document.add(valParagraph);
						}
					}

					//Fix for T25IT-203 ends


					//Fix by Sriram for Defect#816
					if(result.getRuntimeSnapshots() != null && result.getRuntimeSnapshots().size() > 0){
						/*Changed by Pushpa for  TJN252-64 starts*/
						if(result.getRuntimeSnapshots().size()>1) {
						Paragraph screenshotTitle = new Paragraph("Screenshot(s) for Test Data " + iterCount,iterationTitle);
						this.addEmptyLine(screenshotTitle, 1);
						document.add(screenshotTitle);
						}

						for(RuntimeScreenshot shot:result.getRuntimeSnapshots()){
							Paragraph shotTitle = new Paragraph(shot.getSequence() + " - " + shot.getType());
							this.addEmptyLine(shotTitle, 1);
							document.add(shotTitle);

							
							Image image = Image.getInstance(shot.getScreenshotByteArray());
							image.setScaleToFitLineWhenOverflow(true);
							image.scaleAbsolute(520,300);
							Chunk imageChunk  = new Chunk(image,5,3,true);
							document.add(imageChunk);
						}
						/*Changed by Gangadhar Badagi to order the screenshots starts*/
						Paragraph emptyScreenshotTitle = new Paragraph("");
						this.addEmptyLine(emptyScreenshotTitle, 1);
						document.add(emptyScreenshotTitle);
						/*Changed by Gangadhar Badagi to order the screenshots ends*/
					}else{
						Paragraph screenshotTitle = new Paragraph("There are no snapshots for this transaction");
						this.addEmptyLine(screenshotTitle, 1);
						document.add(screenshotTitle);
					}
					//Fix by Sriram for Defect#816 ends

				}

			}
		}catch(Exception e){
			throw new TenjinPdfException("Could not create Detailed Results page for Test Case " + testCase.getTcId());
		}
	}
	/* Added by Padmavathi for TENJINCG-633 starts */
	private void createTestCaseDetailedResultPage(Connection conn,Document document, TestCase testCase,String screenShotOption) throws TenjinPdfException{
		try{


			Font title = FontFactory.getFont("Open_Sans_Bold",19,Font.BOLD, BaseColor.BLUE);
			Font testStepTitle = FontFactory.getFont("Open_Sans_Bold",17,Font.BOLD);
			Font iterationTitle = FontFactory.getFont("Open_Sans_Bold_Italic",14,Font.BOLD);
			/*Changed by Pushpa for  TJN252-64 starts*/
			Font rvTitle = FontFactory.getFont("Open_Sans",12,Font.ITALIC, BaseColor.BLACK);
			/*Changed by Pushpa for TJN252-64 ends*/
			document.newPage();

			Paragraph tcTitle = new Paragraph("Test Case ID " + testCase.getTcId(), title);
			this.addEmptyLine(tcTitle, 1);
			document.add(tcTitle);

			PdfPTable table = new PdfPTable(2);
			table.setWidths(new int[]{4,6});
			table.setWidthPercentage(75);

			PdfPCell c1 = this.createSummaryTableCell("Particulars", "Name");
			PdfPCell c2 = this.createSummaryTableCell("detail", testCase.getTcName());
			PdfPCell c3 = this.createSummaryTableCell("Particulars", "Description");
			PdfPCell c4 = this.createSummaryTableCell("detail", testCase.getTcDesc());
			PdfPCell c5 = this.createSummaryTableCell("Particulars", "Status");
			PdfPCell c6 = this.createSummaryTableCell("detail", testCase.getTcStatus());

			table.addCell(c1);
			table.addCell(c2);
			table.addCell(c3);
			table.addCell(c4);
			table.addCell(c5);
			table.addCell(c6);

			document.add(table);
			int stepCount = 0;
			for(TestStep step:testCase.getTcSteps()){
				stepCount++;
				Paragraph stepTitle = new Paragraph("Step " + stepCount + " - " + step.getId(), testStepTitle);
				this.addEmptyLine(stepTitle, 1);

				document.add(stepTitle);

				PdfPTable stepSummary = new PdfPTable(2);
				stepSummary.setWidthPercentage(75);
				stepSummary.setWidths(new int[]{4,6});

				PdfPCell cell1 = this.createSummaryTableCell("Particulars", "Short Description");
				PdfPCell cell2 = this.createSummaryTableCell("detail", step.getShortDescription());
				PdfPCell cell3 = this.createSummaryTableCell("Particulars", "Type");
				PdfPCell cell4 = this.createSummaryTableCell("Detail", step.getType());
				PdfPCell cell5 = this.createSummaryTableCell("Particulars", "Application");
				PdfPCell cell6 = this.createSummaryTableCell("Detail", step.getAppName());
				
				
				PdfPCell cell7;
				PdfPCell cell8;
				PdfPCell cell7_1 = null;
				PdfPCell cell8_1 = null;
				if(Utilities.trim(step.getTxnMode()).equalsIgnoreCase("api")){
					cell7 = this.createSummaryTableCell("Particulars", "API");
					cell8 = this.createSummaryTableCell("Detail", step.getModuleCode());
					cell7_1 = this.createSummaryTableCell("Particulars", "Operation");
					cell8_1 = this.createSummaryTableCell("Detail", step.getOperation());
				}else{
					cell7 = this.createSummaryTableCell("Particulars", "Function");
					cell8 = this.createSummaryTableCell("Detail", step.getModuleCode());
				}
				/*Modified by Preeti for TENJINCG-878 starts*/
				PdfPCell cell9 = this.createSummaryTableCell("Particulars", "Description");
				PdfPCell cell10 = this.createSummaryTableCell("Detail", step.getDescription());
				PdfPCell cell11 = this.createSummaryTableCell("Particulars", "Expected Result");
				PdfPCell cell12 = this.createSummaryTableCell("Detail", step.getExpectedResult());
				PdfPCell cell13 = this.createSummaryTableCell("Particulars", "Status");
				PdfPCell cell14 = this.createSummaryTableCell("Detail", step.getStatus());
				/*Added by Pushpa for TENJINCG-910 starts*/
				PdfPCell cell15 = this.createSummaryTableCell("Particulars", "Browser");
				/*Added by Pushpa for TENJINCG-910 ends*/
				/*Modified by Preeti for TENJINCG-878 ends*/
				stepSummary.addCell(cell1);
				stepSummary.addCell(cell2);
				stepSummary.addCell(cell3);
				stepSummary.addCell(cell4);
				stepSummary.addCell(cell5);
				stepSummary.addCell(cell6);
				stepSummary.addCell(cell7);
				stepSummary.addCell(cell8);

				if(Utilities.trim(step.getTxnMode()).equalsIgnoreCase("api")){
					stepSummary.addCell(cell7_1);
					stepSummary.addCell(cell8_1);
				}

				stepSummary.addCell(cell9);
				stepSummary.addCell(cell10);
				stepSummary.addCell(cell11);
				stepSummary.addCell(cell12);
				/*Added by Preeti for TENJINCG-878 starts*/
				stepSummary.addCell(cell13);
				stepSummary.addCell(cell14);
				/*Added by Preeti for TENJINCG-878 ends*/
				/*Added by Pushpa for TENJINCG-910 starts*/
				stepSummary.addCell(cell15);
				/*Modified by Preeti for TJN262R2-72 starts*/
				if(step.getTxnMode().equalsIgnoreCase("gui")){
					String browser=this.run.getBrowser_type();
					
					if(browser.equalsIgnoreCase("APPDEFAULT")){
						browser=new TestStepHandler().getAppDefaultBrowser(step.getAppId());
					}
					PdfPCell cell16 = this.createSummaryTableCell("Detail", browser);
					stepSummary.addCell(cell16);
				}
				/*Modified by Preeti for TJN262R2-72 ends*/
				/*Added by Pushpa for TENJINCG-910 ends*/
				document.add(stepSummary);

				int iterCount = 0;
				for(StepIterationResult result:step.getDetailedResults()){
					iterCount++;
					/*Changed by Pushpa for  TJN252-64 starts*/
					Paragraph iter = new Paragraph("Results for Test Data " + iterCount,iterationTitle);
					/*Changed by Pushpa for  TJN252-64 ends*/
					this.addEmptyLine(iter, 1);
					document.add(iter);
					
					/* added by shruthi for TENJINCG-1238 starts*/			
					if(step.getTxnMode().equalsIgnoreCase("API"))
							{
						PdfPTable apireqres = new PdfPTable(2);
						apireqres.setWidths(new int[]{4,6});
						apireqres.setWidthPercentage(75);
						PdfPCell R1 = this.createSummaryTableCell("Particulars", "Request");
						apireqres.addCell(R1);
						 String json =result.getWsReqMessage();
						 PdfPCell R2;
						 if(json==null)
						 {
							R2= this.createSummaryTableCell("detail", "NA");
							 apireqres.addCell(R2);
						 }
						 else{
				            ObjectMapper mapper = new ObjectMapper();
				            boolean validJSONFormat=false;
				            try {
								if(step.getMediaType().get("REQUEST").equalsIgnoreCase("application/json"))
				            	
								validJSONFormat= ApiUtilities.isValidJSON(json);
							} catch (Exception e) {
							}
				           
				            if(validJSONFormat) {
				            try {
				                Object jsonObject = mapper.readValue(json, Object.class);
				                String prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
				                R2= this.createSummaryTableCell("detail", prettyJson);
				                apireqres.addCell(R2);
				            } catch (IOException e) {
				                logger.error("Error ", e);
				            }
				            }else {
				            	 R2= this.createSummaryTableCell("detail", json);
					               apireqres.addCell(R2);
				            }
						 }
						PdfPCell R3= this.createSummaryTableCell("Particulars", "Response");
						apireqres.addCell(R3);
						String json1 =result.getWsResMessage();
						PdfPCell R4;
						if(json1==null)
						{
						     R4= this.createSummaryTableCell("detail", "NA");
							apireqres.addCell(R4);
						}
						else{
			            ObjectMapper mapper1 = new ObjectMapper();
			            boolean validFormat=false;
			            try {
			            	if(step.getMediaType().get("RESPONSE").equalsIgnoreCase("application/json"))
				            	 validFormat= ApiUtilities.isValidJSON(json1);
						} catch (Exception e) {
						}
			           
			            if(validFormat) {
			            try {
			                Object jsonObject1 = mapper1.readValue(json1, Object.class);
			                String prettyJson1 = mapper1.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject1);
			                R4= this.createSummaryTableCell("detail", prettyJson1);
			                apireqres.addCell(R4);
			            } catch (IOException e) {
			                logger.error("Error ", e);
			            }
			            }else {
			            	 R4= this.createSummaryTableCell("detail", json1);
				               apireqres.addCell(R4);
			            }
						}
						document.add(apireqres);
							}
					  /* added by shruthi for TENJINCG-1238 ends*/	
					
					
					Paragraph iterDesc;
					if(step.getTxnMode().equalsIgnoreCase("GUI"))
						iterDesc = new Paragraph("Following are the GUI results for this transaction");
					else
						iterDesc = new Paragraph("Following are the API results for this transaction");	
					this.addEmptyLine(iterDesc, 1);
					document.add(iterDesc);

					PdfPTable guiResults = new PdfPTable(2);
					guiResults.setWidthPercentage(75);
					guiResults.setWidths(new int[]{4,6});

					String status = "";
					if(result.getResult().equalsIgnoreCase("S")){
						status = "Pass";
					}else if(result.getResult().equalsIgnoreCase("F")){
						status = "Fail";
					}else if(result.getResult().equalsIgnoreCase("E")){
						status = "Error";
					}

					PdfPCell cel1 = this.createSummaryTableCell("Particulars", "Output");
					PdfPCell cel2 = this.createSummaryTableCell("Detail", result.getMessage());
					PdfPCell cel3 = this.createSummaryTableCell("Particulars", "Status");
					PdfPCell cel4 = this.createSummaryTableCell("Detail", status);
					PdfPCell cel5 = this.createSummaryTableCell("Particulars", "Elapsed Time");
					PdfPCell cel6 = this.createSummaryTableCell("Detail", result.getElapsedTime());
					guiResults.addCell(cel1);
					guiResults.addCell(cel2);
					guiResults.addCell(cel3);
					guiResults.addCell(cel4);
				 
                    /*Added by Ruksar for TENJINCG-1212 starts*/
					PdfPCell cel11 = this.createSummaryTableCell("Particulars", "Start Time");
					PdfPCell cel12 = this.createSummaryTableCell("Detail", this.run.getStartTimeStamp().toString());
					PdfPCell cel13 = this.createSummaryTableCell("Particulars", "End Time");
					PdfPCell cel14 = this.createSummaryTableCell("Detail",this.run.getEndTimeStamp().toString());
					
					guiResults.addCell(cel11);
					guiResults.addCell(cel12);
					guiResults.addCell(cel13);
					guiResults.addCell(cel14);
					
					/*Added by Ruksar for TENJINCG-1212 ends*/
					
					guiResults.addCell(cel5);
					guiResults.addCell(cel6);
					
					if(step.getTxnMode().equalsIgnoreCase("API") && !Utilities.trim(result.getApiAuthType()).isEmpty()){
						//Code changes for OAuth 2.0 requirement - Avinash -TENJINCG-1018 Starts
						PdfPCell cel7 = this.createSummaryTableCell("Particulars", "Authentication Type");
						PdfPCell cel8 = null; 
						if(result.getApiAuthType().equalsIgnoreCase("oAuth2")){
							cel8 = this.createSummaryTableCell("Detail", "OAuth 2.0");
							PdfPCell cel9 = this.createSummaryTableCell("Particulars", "Access Token");
							PdfPCell cel10 = this.createSummaryTableCell("Detail", result.getApiAccessToken());
							guiResults.addCell(cel9);
							guiResults.addCell(cel10);
						}else
							cel8 = this.createSummaryTableCell("Detail", result.getApiAuthType());

						guiResults.addCell(cel7);
						guiResults.addCell(cel8);
						//Code changes for OAuth 2.0 requirement - Avinash -TENJINCG-1018 Ends
						
					}
					
					document.add(guiResults);

					Paragraph empty = new Paragraph("");
					this.addEmptyLine(empty, 1);
					document.add(empty);


					if(result.getRuntimeFieldValues() != null && result.getRuntimeFieldValues().size() > 0) {
						Paragraph rtvTitle = new Paragraph("Run-time Values", rvTitle);
						this.addEmptyLine(rtvTitle, 1);
						document.add(rtvTitle);

						PdfPTable rtvTable = new PdfPTable(3);
						rtvTable.setWidthPercentage(75);
						rtvTable.setWidths(new int[]{4,4,2});

						PdfPCell rtvHfield = this.createSummaryTableCell("Particulars", "Field");
						PdfPCell rtvHvalue = this.createSummaryTableCell("Particulars", "Value");
						PdfPCell rtvHdetail = this.createSummaryTableCell("Particulars", "Detail ID");
						rtvTable.addCell(rtvHfield);
						rtvTable.addCell(rtvHvalue);
						rtvTable.addCell(rtvHdetail);

						for(RuntimeFieldValue rv: result.getRuntimeFieldValues()) {
							PdfPCell f = this.createSummaryTableCell("Detail", rv.getField());
							PdfPCell v = this.createSummaryTableCell("Detail", rv.getValue());
							PdfPCell d = this.createSummaryTableCell("Detail", rv.getDetailRecordNo());

							rtvTable.addCell(f);
							rtvTable.addCell(v);
							rtvTable.addCell(d);
						}

						document.add(rtvTable);

					}
					
						document.add( Chunk.NEWLINE );
				        document.add( Chunk.NEWLINE );
				        document.add( Chunk.NEWLINE );
				        document.add( Chunk.NEWLINE );
				        document.add( Chunk.NEWLINE );
				        document.add( Chunk.NEWLINE );



					
					if(result.getValidationResults() != null && result.getValidationResults().size() > 0){
						Paragraph valTitle = new Paragraph("Field Validations", rvTitle);
						this.addEmptyLine(valTitle, 1);
						document.add(valTitle);
						PdfPTable valResults = new PdfPTable(6);
						valResults.setWidthPercentage(100);
						valResults.setWidths(new int[]{1, 2, 2, 2, 2, 1});

						PdfPCell h = this.createSummaryTableCell("Particulars", "#");
						valResults.addCell(h);
						h = this.createSummaryTableCell("Particulars", "Page Name");
						valResults.addCell(h);
						h = this.createSummaryTableCell("Particulars", "Field Name");
						valResults.addCell(h);
						h = this.createSummaryTableCell("Particulars", "Expected Value");
						valResults.addCell(h);
						h = this.createSummaryTableCell("Particulars", "Actual Value");
						valResults.addCell(h);
						h = this.createSummaryTableCell("Particulars", "Status");
						valResults.addCell(h);
						int vCount = 0;
						for(ValidationResult r:result.getValidationResults()){
							vCount++;
							h = this.createSummaryTableCell("Detail", vCount);
							valResults.addCell(h);
							h = this.createSummaryTableCell("Detail", r.getPage());
							valResults.addCell(h);
							h = this.createSummaryTableCell("Detail", r.getField());
							valResults.addCell(h);
							h = this.createSummaryTableCell("Detail", r.getExpectedValue());
							valResults.addCell(h);
							h = this.createSummaryTableCell("Detail", r.getActualValue());
							valResults.addCell(h);
							h = this.createSummaryTableCell("Detail", r.getStatus());
							valResults.addCell(h);
						}
						document.add(valResults);
					}else{
						Paragraph valParagraph = new Paragraph("There are no field validations for this step");
						this.addEmptyLine(valParagraph, 1);
						document.add(valParagraph);
					}


					if("ui".equalsIgnoreCase(step.getValidationType())) {
						Map<String, Map<String, List<UIValidation>>> map = this.getUIValidationResultsForStep(this.run.getId(), step.getRecordId());
						if(map != null && map.keySet().size() > 0) {
							for(String validationType : map.keySet()) {
								for(String pageName : map.get(validationType).keySet()) {
									Paragraph valTitle = new Paragraph("UI Validation: " + validationType + " on page " + pageName, rvTitle);
									this.addEmptyLine(valTitle, 1);
									document.add(valTitle);
									PdfPTable valResults = new PdfPTable(3);
									valResults.setWidthPercentage(100);
									valResults.setWidths(new int[]{2,4,4});
									PdfPCell h = this.createSummaryTableCell("Particulars", "Field");
									valResults.addCell(h);
									h = this.createSummaryTableCell("Particulars", "Expected");
									valResults.addCell(h);
									h = this.createSummaryTableCell("Particulars", "Actual");
									valResults.addCell(h);
									for(UIValidation val : map.get(validationType).get(pageName)) {
										for(UIValidationResult uResult : val.getResults()) {
											h = this.createSummaryTableCell("Detail", uResult.getField());
											valResults.addCell(h);
											h = this.createSummaryTableCell("Detail", uResult.getExpectedValue());
											valResults.addCell(h);
											h = this.createSummaryTableCell("Detail", uResult.getActualValue());
											valResults.addCell(h);
										}
									}
									document.add(valResults);
								}
							}
						}else {
							Paragraph valParagraph = new Paragraph("There are no UI validations for this step");
							this.addEmptyLine(valParagraph, 1);
							document.add(valParagraph);
						}
					}
					if(screenShotOption.equalsIgnoreCase("true")){
						if(result.getRuntimeSnapshots() != null && result.getRuntimeSnapshots().size() > 0){
							/*Added by Pushpa for TJN262R2-9 starts*/
							int autType=new AutsHelper().getAutType(step.getAppId());
							/*Added by Pushpa for TJN262R2-9 ends*/
							/*Changed by Pushpa for  TJN252-64 starts*/
							if(result.getRuntimeSnapshots().size()>1) {
							Paragraph screenshotTitle = new Paragraph("Screenshot(s) for Test Data " + iterCount,iterationTitle);
							this.addEmptyLine(screenshotTitle, 1);
							document.add(screenshotTitle);
							}

							for(RuntimeScreenshot shot:result.getRuntimeSnapshots()){
								if(shot.getType().contains("Failure")) {
									Paragraph shotTitle = new Paragraph("Screenshot "+shot.getSequence()+"(" +shot.getTimestamp()+ ") - " + "Failure");
									this.addEmptyLine(shotTitle, 1);
									document.add(shotTitle);
								}else {
									Paragraph shotTitle = new Paragraph("Screenshot "+shot.getSequence()+"(" +shot.getTimestamp()+ ")");
									this.addEmptyLine(shotTitle, 1);
									document.add(shotTitle);
								}
								/*Changed by Pushpa for  TJN252-64 ends*/
								try {
								Image image = Image.getInstance(shot.getScreenshotByteArray());
								image.setScaleToFitLineWhenOverflow(true);
								/*Changed by Pushpa for  TJN252-64 starts*/
								BaseColor basecolor=new BaseColor(66, 134, 244);
								image.setBorder(1);
								/*Changed by Pushpa for  TJN252-64 ends*/
								/*Modified by Pushpa for TJN262R2-9 starts*/
								Chunk imageChunk =null;
								if(autType==3){
									image.scaleAbsolute(300,520);
									/*Changed by Pushpa for  TJN252-64 starts*/
									image.setBorderWidthLeft(2f);
									image.setBorderWidthRight(2f);
									image.setBorderWidthTop(2f);
									image.setBorderWidthBottom(2f);
									image.setBorderColorTop(basecolor);
									image.setBorderColorBottom(basecolor);
									image.setBorderColorLeft(basecolor);
									image.setBorderColorRight(basecolor);
									/*Changed by Pushpa for  TJN252-64 ends*/
									imageChunk  = new Chunk(image,3,5,true);
								}else{
									image.scaleAbsolute(520,300);
									/*Changed by Pushpa for  TJN252-64 starts*/
									image.setBorderWidthLeft(2f);
									image.setBorderWidthRight(2f);
									image.setBorderWidthTop(2f);
									image.setBorderWidthBottom(2f);
									image.setBorderColorTop(basecolor);
									image.setBorderColorBottom(basecolor);
									image.setBorderColorLeft(basecolor);
									image.setBorderColorRight(basecolor);
									/*Changed by Pushpa for  TJN252-64 ends*/
									imageChunk  = new Chunk(image,5,3,true);
								}
								/*Modified by Pushpa for TJN262R2-9 ends*/
								document.add(imageChunk);
								/*Changed by Pushpa for  TJN252-64 starts*/
								Paragraph shotTitle = new Paragraph("");
								this.addEmptyLine(shotTitle, 1);
								document.add(shotTitle);
								/*Changed by Pushpa for  TJN252-64 ends*/
								}/*Added by Ashiki for TJN252-42 Starts*/
								catch (Exception e) {
									Paragraph errorMsg = new Paragraph("The screenshot may be Blank or Corrupted");
									document.add(errorMsg);
								}/*Added by Ashiki for TJN252-42 ends*/
							}
							Paragraph emptyScreenshotTitle = new Paragraph("");
							this.addEmptyLine(emptyScreenshotTitle, 1);
							document.add(emptyScreenshotTitle);
						}else{
							Paragraph screenshotTitle = new Paragraph("There are no snapshots for this transaction");
							this.addEmptyLine(screenshotTitle, 1);
							document.add(screenshotTitle);
						}
					}

				}

			}
		}catch(Exception e){
			throw new TenjinPdfException("Could not create Detailed Results page for Test Case " + testCase.getTcId());
		}
	}
	/* Added by Padmavathi for TENJINCG-633 ends */

	private void createScriptReportTitlePage(Document document) throws TenjinPdfException{

		try{
			//Add 3 empty lines
			Paragraph headerSpace = new Paragraph();
			addEmptyLine(headerSpace,3);
			document.add(headerSpace);

			
			Image hatsLogo = Image.getInstance(this.contextRoot + "\\images\\tenjin_logo_noversion.png");
			/******************
			 * Fix by Sriram for Defect#697 ends
			 */
			hatsLogo.setAlignment(Image.MIDDLE);


			document.add(hatsLogo);

			//Add Report Title
			Paragraph reportTitle = new Paragraph("Test Run Report",titleFont);
			reportTitle.setAlignment(Element.ALIGN_CENTER);
			addEmptyLine(reportTitle, 2);
			document.add(reportTitle);

			SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy hh:mm:ss");
			Chunk c1 = new Chunk("Generated On");
			c1.setFont(new Font(FontFamily.HELVETICA, 14, Font.BOLD));
			Chunk c2 = new Chunk(sdf.format(new Date()));

			Paragraph generatedOnBlock = new Paragraph();
			generatedOnBlock.add(c1);
			generatedOnBlock.add(new Chunk(" "));
			generatedOnBlock.add(c2);
			generatedOnBlock.setAlignment(Element.ALIGN_CENTER);
			Chunk c3 = new Chunk("Generated By");
			c3.setFont(new Font(FontFamily.HELVETICA, 14, Font.BOLD));
			Chunk c4 = new Chunk(this.userFullName);

			Paragraph generatedByBlock = new Paragraph();
			generatedByBlock.add(c3);
			generatedByBlock.add(new Chunk(" "));
			generatedByBlock.add(c4);
			generatedByBlock.setAlignment(Element.ALIGN_CENTER);
			document.add(generatedOnBlock);
			document.add(generatedByBlock);




		}
		catch(DocumentException e){
			throw new TenjinPdfException("Could not create Title page for Report",e);
		} catch (MalformedURLException e) {
			
			throw new TenjinPdfException("Could not create Title page for Report",e);
		} catch (IOException e) {
			
			throw new TenjinPdfException("Could not create Title page for Report",e);
		}

	}

	private PdfPCell createSummaryTableCell(String cellType, String cellValue){
		try{
			Font summaryDetailFont = FontFactory.getFont("Open_Sans");
			/*Modified by Preeti for TJNUN262-48 starts*/
			/*Font summaryParticularsFont = FontFactory.getFont("Open_Sans_Bold",12,Font.BOLD);*/
			Font summaryParticularsFont = FontFactory.getFont("TIMES_ROMAN",12);
			/*Modified by Preeti for TJNUN262-48 ends*/
			Phrase phrase;
			if(cellType.equalsIgnoreCase("particulars")){
				phrase = new Phrase(cellValue, summaryParticularsFont);
			}else{
				phrase = new Phrase(cellValue, summaryDetailFont);
			}

			PdfPCell cell = new PdfPCell(phrase);
			if(cellType.equalsIgnoreCase("particulars")){
				cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			}
			cell.setPadding(8);
			return cell;
		}catch(Exception e){
			logger.error("An exception occurred while creating summary table cell",e);
			return null;
		}
	}

	private PdfPCell createSummaryTableCell(String cellType, int cellValue){
		try{
			String c = Integer.toString(cellValue);
			PdfPCell cell = this.createSummaryTableCell(cellType, c);
			return cell;
		}catch(Exception e){
			return null;
		}
	}

	//Added for T25IT-203
	private Map<String, Map<String, List<UIValidation>>> getUIValidationResultsForStep(int runId, int stepRecordId) {
		try {
			Map<String, Map<String, List<UIValidation>>> map = new UIValidationHelper().hydrateUIValidationsForStepWithResults(stepRecordId, runId, "type");

			for(String type : map.keySet()) {
				String messageKeyPrefix = type.replace(" ", "_");
				for(String page : map.get(type).keySet()) {
					for(UIValidation val : map.get(type).get(page)) {
						if(val.getResults() != null) {
							for(UIValidationResult result : val.getResults()) {
								String expMessageKey = messageKeyPrefix;
								String actMessageKey = messageKeyPrefix;
								if(Utilities.trim(result.getExpectedValue()).equalsIgnoreCase("true") || Utilities.trim(result.getExpectedValue()).equalsIgnoreCase("false")) {
									/* Fix for T25IT-123 */
									/*expMessageKey += "." + Utilities.trim(result.getExpectedValue()) + ".expected";
									actMessageKey += "." + Utilities.trim(result.getActualValue()) + ".actual";*/
									expMessageKey += "." + Utilities.trim(result.getExpectedValue()).toLowerCase() + ".expected";
									actMessageKey += "." + Utilities.trim(result.getActualValue()).toLowerCase() + ".actual";
									/* Fix for T25IT-123 ends*/
								}else {
									expMessageKey += ".expected";
									actMessageKey += ".actual";
								}

								try {
									/* Fix for T25IT-123 */
									/*result.setExpectedValue(MessageUtil.getMessage(expMessageKey, result.getExpectedValue()));
									result.setActualValue(MessageUtil.getMessage(actMessageKey, result.getActualValue()));*/

									String expMsg = MessageUtil.getMessage(expMessageKey, result.getExpectedValue());
									String actMsg = MessageUtil.getMessage(actMessageKey, result.getActualValue());
									result.setExpectedValue(Utilities.trim(expMsg).length() > 0 ? Utilities.trim(expMsg) : result.getExpectedValue());
									result.setActualValue(Utilities.trim(actMsg).length()> 0 ? Utilities.trim(actMsg) : result.getActualValue());
									/* Fix for T25IT-123 ends*/
								} catch (TenjinConfigurationException e) {
									
									logger.warn("Could not get message from configuration", e);
								}
							}
						}

					}
				}
			}
			return map;
		} catch (DatabaseException e) {
			logger.error("ERROR getting uI Validation results for step", e);
			return null;
		}
	}
	//Added for T25IT-203 ends
	
	
	/*Added by Preeti for Test report starts*/
	/*	commented by shruthi for TCGST-67 ends*/
	public void generateEntityReport(String title, String subTitle, HashMap<String,String> entityMap, ArrayList<HashMap<String,Object>> entityData, String folderPath,String fileName) throws TenjinPdfException{
		Font summaryTitleFont = FontFactory.getFont("Open_Sans_Bold",22,Font.BOLD, BaseColor.BLUE);
		Document document = new Document();
		try{	
			PdfWriter.getInstance(document, new FileOutputStream(folderPath + "\\" + fileName));
			document.open();
			this.createEntityReportTitlePage(title,document);
			document.newPage();
			
			Paragraph overview = new Paragraph("Overview",summaryTitleFont);
			this.addEmptyLine(overview, 1);
			document.add(overview);
			logger.info("Creating overview table for "+title);
			PdfPTable overviewTable = new PdfPTable(2);
			overviewTable.setWidthPercentage(100);
			overviewTable.setWidths(new int[]{4,6});
			for (Map.Entry<String,String> entry : entityMap.entrySet()) {
			    PdfPCell d = this.createSummaryTableCell("Particulars", entry.getKey());
			    PdfPCell d1 = this.createSummaryTableCell("Detail", entry.getValue());
			    overviewTable.addCell(d);
			    overviewTable.addCell(d1);
			}
			document.add(overviewTable);
			
			Paragraph p = new Paragraph("",summaryTitleFont);
			this.addEmptyLine(p, 1);
			document.add(p);
			
			logger.info("Creating content table for "+title);
			Paragraph details = new Paragraph(subTitle,summaryTitleFont);
			this.addEmptyLine(details, 1);
			document.add(details);
			logger.info("Creating cell for header of table");
			PdfPTable content = new PdfPTable(entityData.get(0).size());
			content.setWidthPercentage(100);
			for (Map.Entry<String,Object> entry : entityData.get(0).entrySet()) {
				    String key = entry.getKey();
				    PdfPCell d = this.createSummaryTableCell("Particulars", key);
				    content.addCell(d);
				}
			logger.info("Creating cell for data of table");
			for (HashMap<String,Object> data : entityData){
				 for (Map.Entry<String,Object> entry : data.entrySet()) {
				    String value = entry.getValue().toString();
				    PdfPCell d = this.createSummaryTableCell("Detail", value);
				    content.addCell(d);
				}
				 content.completeRow();
			}
			document.add(content);
			document.close();
		}catch(Exception e){
			throw new TenjinPdfException("Could not create execution Report",e);
		}
	}
	private void createEntityReportTitlePage(String Title,Document document) throws TenjinPdfException{

		try{
			//Add 3 empty lines
			Paragraph headerSpace = new Paragraph();
			addEmptyLine(headerSpace,3);
			document.add(headerSpace);

			Image hatsLogo = Image.getInstance(this.contextRoot + "\\images\\tenjin_logo_noversion.png");
			hatsLogo.setAlignment(Image.MIDDLE);
			document.add(hatsLogo);

			//Add Report Title
			Paragraph reportTitle = new Paragraph(Title,titleFont);
			reportTitle.setAlignment(Element.ALIGN_CENTER);
			addEmptyLine(reportTitle, 2);
			document.add(reportTitle);

			SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy hh:mm:ss");
			Chunk c1 = new Chunk("Generated On");
			c1.setFont(new Font(FontFamily.HELVETICA, 14, Font.BOLD));
			Chunk c2 = new Chunk(sdf.format(new Date()));

			Paragraph generatedOnBlock = new Paragraph();
			generatedOnBlock.add(c1);
			generatedOnBlock.add(new Chunk(" "));
			generatedOnBlock.add(c2);
			generatedOnBlock.setAlignment(Element.ALIGN_CENTER);
			Chunk c3 = new Chunk("Generated By");
			c3.setFont(new Font(FontFamily.HELVETICA, 14, Font.BOLD));
			Chunk c4 = new Chunk(this.userFullName);

			Paragraph generatedByBlock = new Paragraph();
			generatedByBlock.add(c3);
			generatedByBlock.add(new Chunk(" "));
			generatedByBlock.add(c4);
			generatedByBlock.setAlignment(Element.ALIGN_CENTER);
			document.add(generatedOnBlock);
			document.add(generatedByBlock);
		}
		catch(DocumentException e){
			throw new TenjinPdfException("Could not create Title page for Report",e);
		} catch (MalformedURLException e) {
			
			throw new TenjinPdfException("Could not create Title page for Report",e);
		} catch (IOException e) {
			
			throw new TenjinPdfException("Could not create Title page for Report",e);
		}

	}
	/*Added by Preeti for Test report ends*/
	
	/*Added by Roshni for TJN252-8 starts */
	public byte[] generatePdfReportForRunResult(int runId, int projectId, String folderPath, String screenShotOption)
			throws TenjinPdfException, DatabaseException {
		Document document = new Document();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null)
			throw new DatabaseException("Invalid Database Connection");

		ByteArrayOutputStream ops = new ByteArrayOutputStream();

		try {
			PdfWriter.getInstance(document, ops);
			document.open();

			ResultsHelper helper = new ResultsHelper();
			TestRun run = helper.hydrateRun(runId, projectId);
			this.project = new ProjectHelper().hydrateProject(conn, projectId);
			if (run.getPureRunId() != runId) {
				isPureRun = false;
				run.setStartTimeStamp(helper.hydrateBasicRunInformation(run.getPureRunId()).getStartTimeStamp());
				run.setElapsedTime(helper.getElapsedTimeForOveralRun(runId));
			}
			this.run = run;
			this.device = new DeviceHelper().hydrateDevice(this.run.getDeviceRecId());
			this.addMetadata(document);
			this.createScriptReportTitlePage(document);
			this.createSummaryPage(document);

			if (this.run.getTestSet().getTests() != null && this.run.getTestSet().getTests().size() > 0) {
				for (TestCase t : this.run.getTestSet().getTests()) {
					this.createTestCaseDetailedResultPage(conn, document, t, screenShotOption);
				}
			}
			document.close();
			return ops.toByteArray();
		} catch (DocumentException e) {
			
			throw new TenjinPdfException("Could not generate PDF Report due to an internal error", e);
		} catch (DatabaseException e) {
			
			throw new TenjinPdfException("Could not generate PDF Report due to an internal error", e);
		} catch (Exception e) {
			
			throw new TenjinPdfException("Could not generate PDF Report due to an internal error", e);
		}/*Added by paneendra for TENJINCG-1267 starts*/
		finally {

			DatabaseHelper.close(conn);
		}
		/*Added by paneendra for TENJINCG-1267 ends*/
	}

	public byte[] generatePdfReportForRunResult(int runId, int projectId, String folderPath)
			throws TenjinPdfException, DatabaseException {
		Document document = new Document();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if (conn == null)
			throw new DatabaseException("Invalid Database Connection");
		
		ByteArrayOutputStream ops = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(document, ops);
			document.open();

			ResultsHelper helper = new ResultsHelper();
			TestRun run = helper.hydrateRun(runId, projectId);
			this.project = new ProjectHelper().hydrateProject(conn, projectId);
			if (run.getPureRunId() != runId) {
				isPureRun = false;
				run.setStartTimeStamp(helper.hydrateBasicRunInformation(run.getPureRunId()).getStartTimeStamp());
				run.setElapsedTime(helper.getElapsedTimeForOveralRun(runId));
			}
			this.run = run;
			this.device = new DeviceHelper().hydrateDevice(this.run.getDeviceRecId());
			this.addMetadata(document);
			this.createScriptReportTitlePage(document);
			this.createSummaryPage(document);

			if (this.run.getTestSet().getTests() != null && this.run.getTestSet().getTests().size() > 0) {
				for (TestCase t : this.run.getTestSet().getTests()) {
					this.createTestCaseDetailedResultPage(conn, document, t);
				}
			}
			document.close();
			return ops.toByteArray();
		} catch (DocumentException e) {
			
			throw new TenjinPdfException("Could not generate PDF Report due to an internal error", e);
		} catch (DatabaseException e) {
			
			throw new TenjinPdfException("Could not generate PDF Report due to an internal error", e);
		} catch (Exception e) {
			
			throw new TenjinPdfException("Could not generate PDF Report due to an internal error", e);
		}/*Added by paneendra for TENJINCG-1267 starts*/
		finally {

			DatabaseHelper.close(conn);
		}
		/*Added by paneendra for TENJINCG-1267 ends*/
	
	}
	/*Added by Roshni for TJN252-8 ends */

	/*Added by Ashiki for TENJINCG-1192 starts*/
	public byte[] generatePdfReportForRunTimeValues(int runId, int id, String folderPath) throws DatabaseException {
		Document document = new Document();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if (conn == null)
			throw new DatabaseException("Invalid Database Connection");
		
		ByteArrayOutputStream ops = new ByteArrayOutputStream();
		
		try {
			PdfWriter.getInstance(document, ops);
			document.open();

			ResultsHelper helper = new ResultsHelper();
			List<RuntimeFieldValue> runTimeValues = helper.hydrateRunTimeValues(runId);
			this.addMetadataruntimevalues(document);
			this.createScriptRunTimeValueReportTitlePage(document);
					this.createRunTimeValuesPage(conn, document, runTimeValues);
			document.close();

		} catch (DocumentException e) {
			
			logger.error("Error ", e);
		} catch (TenjinPdfException e) {
			
			logger.error("Error ", e);
		}/*Added by paneendra for TENJINCG-1267 starts*/
		finally {

			DatabaseHelper.close(conn);
		}
		/*Added by paneendra for TENJINCG-1267 ends*/
		return ops.toByteArray();
	}

	
	private void createScriptRunTimeValueReportTitlePage(Document document) throws TenjinPdfException {


		try{
			Paragraph headerSpace = new Paragraph();
			addEmptyLine(headerSpace,3);
			document.add(headerSpace);
			Image hatsLogo = Image.getInstance(this.contextRoot + "\\images\\tenjin_logo_noversion.png");
			hatsLogo.setAlignment(Image.MIDDLE);
			document.add(hatsLogo);
			Paragraph reportTitle = new Paragraph("Run Time Values Report",titleFont);
			reportTitle.setAlignment(Element.ALIGN_CENTER);
			addEmptyLine(reportTitle, 2);
			document.add(reportTitle);
			SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy hh:mm:ss");
			Chunk c1 = new Chunk("Generated On");
			c1.setFont(new Font(FontFamily.HELVETICA, 14, Font.BOLD));
			Chunk c2 = new Chunk(sdf.format(new Date()));
			Paragraph generatedOnBlock = new Paragraph();
			generatedOnBlock.add(c1);
			generatedOnBlock.add(new Chunk(" "));
			generatedOnBlock.add(c2);
			generatedOnBlock.setAlignment(Element.ALIGN_CENTER);
			Chunk c3 = new Chunk("Generated By");
			c3.setFont(new Font(FontFamily.HELVETICA, 14, Font.BOLD));
			Chunk c4 = new Chunk(this.userFullName);
			Paragraph generatedByBlock = new Paragraph();
			generatedByBlock.add(c3);
			generatedByBlock.add(new Chunk(" "));
			generatedByBlock.add(c4);
			generatedByBlock.setAlignment(Element.ALIGN_CENTER);
			document.add(generatedOnBlock);
			document.add(generatedByBlock);


		}
		catch(DocumentException e){
			throw new TenjinPdfException("Could not create Title page for Report",e);
		} catch (MalformedURLException e) {
			
			throw new TenjinPdfException("Could not create Title page for Report",e);
		} catch (IOException e) {
			
			throw new TenjinPdfException("Could not create Title page for Report",e);
		}

			
	}
	
	private void addMetadataruntimevalues(Document document) {
		document.addTitle("Tenjin Execution Run Time Values Report");
		document.addSubject("Run Time values");
		document.addKeywords("Tenjin, Results");
		document.addAuthor("Tenjin");
		document.addCreator("Tenjin");
	}
	
	private void createRunTimeValuesPage(Connection conn, Document document, List<RuntimeFieldValue> runTimeValues) throws DocumentException {
		Font title = FontFactory.getFont("Open_Sans_Bold",19,Font.BOLD, BaseColor.BLUE);
		document.newPage();
		Paragraph rtvTitle = new Paragraph("Run-time Values", title);
		this.addEmptyLine(rtvTitle, 1);
		document.add(rtvTitle);
		PdfPTable rtvTable = new PdfPTable(4);
		rtvTable.setWidthPercentage(100);
		rtvTable.setWidths(new int[]{4,4,4,4});
		PdfPCell c5 = this.createSummaryTableCell("Particulars", "TDGID");
		PdfPCell c6 = this.createSummaryTableCell("Particulars", "TDUID");
		PdfPCell rtvHfield = this.createSummaryTableCell("Particulars", "Field");
		PdfPCell rtvHvalue = this.createSummaryTableCell("Particulars", "Value");
		rtvTable.addCell(c5);
		rtvTable.addCell(c6);
		rtvTable.addCell(rtvHfield);
		rtvTable.addCell(rtvHvalue);
			for(RuntimeFieldValue rtv: runTimeValues) {
				PdfPCell c8 = this.createSummaryTableCell("detail", rtv.getTdGid());
				PdfPCell c7 = this.createSummaryTableCell("detail", rtv.getTdUid());
				PdfPCell f = this.createSummaryTableCell("Detail", rtv.getField());
				PdfPCell v = this.createSummaryTableCell("Detail", rtv.getValue());
				
				rtvTable.addCell(c8);
				rtvTable.addCell(c7);
				rtvTable.addCell(f);
				rtvTable.addCell(v);
			}
			document.add(rtvTable);


	}
	/*Added by Ashiki for TENJINCG-1192 ends*/

/*Added by Ashiki for TENJINCG-1211 starts*/
public String generatePdfReportConsolitatedRun( int projectId, String folderPath,String currentDateTime) throws TenjinPdfException, DatabaseException{
	String fileName = "Tenjin_Execution_Consolidated_Report_Run_.pdf";
	Document document = new Document();
	Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
	if(conn == null){
		throw new DatabaseException("Invalid Database Connection");
	}
	try {
		PdfWriter.getInstance(document, new FileOutputStream(folderPath + "\\" + fileName));
		document.open();
		this.project = new ProjectHelper().hydrateProject(conn,projectId);
		ResultsHelper runsValues=new ResultsHelper();
		this.runs=runsValues.hydrateConsolidatedRun(currentDateTime, projectId);
		this.addMetadata(document);
		this.createScriptConsolidatedReportTitlePage(document);
		this.createConsolidatedRunsSummaryPage(document);
		document.close();
		return fileName;
	} catch (FileNotFoundException e) {
		throw new TenjinPdfException("Could not generate PDF Report due to an internal error",e);
	} catch (DocumentException e) {
		throw new TenjinPdfException("Could not generate PDF Report due to an internal error",e);
	} catch (DatabaseException e) {
		throw new TenjinPdfException("Could not generate PDF Report due to an internal error",e);
	}catch (Exception e) {
		throw new TenjinPdfException("Could not generate PDF Report due to an internal error",e);
	}

}

private void createConsolidatedRunsSummaryPage(Document document) throws TenjinPdfException {
	try{
		Font summaryTitleFont = FontFactory.getFont("Open_Sans_Bold",22,Font.BOLD, BaseColor.BLUE);
		document.newPage();

		Paragraph overview = new Paragraph("Overview",summaryTitleFont);
		this.addEmptyLine(overview, 1);
		document.add(overview);

		PdfPTable overviewTable = new PdfPTable(2);
		overviewTable.setWidths(new int[]{4,6});
		overviewTable.setWidthPercentage(75);

		PdfPCell d = this.createSummaryTableCell("Particulars", "Domain");
		PdfPCell dName = this.createSummaryTableCell("Detail", this.project.getDomain());
		PdfPCell p = this.createSummaryTableCell("Particulars", "Project");
		PdfPCell pName = this.createSummaryTableCell("Detail", this.project.getName());

		overviewTable.addCell(d);
		overviewTable.addCell(dName);
		overviewTable.addCell(p);
		overviewTable.addCell(pName);
		document.add(overviewTable);

		Paragraph title = new Paragraph("Consolidated Runs",summaryTitleFont);
		this.addEmptyLine(title, 1);

		document.add(title);
		
		PdfPTable consolidatedRunTable = new PdfPTable(5);
		consolidatedRunTable.setWidthPercentage(100);
		consolidatedRunTable.setWidths(new int[]{2,3,3,3,3});
		PdfPCell c5 = this.createSummaryTableCell("Particulars", "Run Id");
		PdfPCell c6 = this.createSummaryTableCell("Particulars", "Run Task Type");
		PdfPCell c7 = this.createSummaryTableCell("Particulars", "Run Start Time");
		PdfPCell c8 = this.createSummaryTableCell("Particulars", "Run End Time");
		PdfPCell c9 = this.createSummaryTableCell("Particulars", "User");
		consolidatedRunTable.addCell(c5);
		consolidatedRunTable.addCell(c6);
		consolidatedRunTable.addCell(c7);
		consolidatedRunTable.addCell(c8);
		consolidatedRunTable.addCell(c9);
		
			for(TestRun run: this.runs) {
				PdfPCell c10  = this.createSummaryTableCell("detail", run.getId());
				PdfPCell c11  = this.createSummaryTableCell("detail", run.getTaskType());
				PdfPCell c12  = this.createSummaryTableCell("Detail", this.sdf.format(run.getStartTimeStamp()));
				PdfPCell c13 = this.createSummaryTableCell("Detail", this.sdf.format(run.getEndTimeStamp()));
				PdfPCell c14 = this.createSummaryTableCell("Detail", run.getUser());
				
				consolidatedRunTable.addCell(c10);
				consolidatedRunTable.addCell(c11);
				consolidatedRunTable.addCell(c12);
				consolidatedRunTable.addCell(c13);
				consolidatedRunTable.addCell(c14);
			}
			document.add(consolidatedRunTable);
}
	catch(Exception e){
		throw new TenjinPdfException("Could not create Summary Page",e);
	}
}


private void createScriptConsolidatedReportTitlePage(Document document) throws TenjinPdfException {

	try{
		Paragraph headerSpace = new Paragraph();
		addEmptyLine(headerSpace,3);
		document.add(headerSpace);

		Image hatsLogo = Image.getInstance(this.contextRoot + "\\images\\tenjin_logo_noversion.png");
		hatsLogo.setAlignment(Image.MIDDLE);
		document.add(hatsLogo);

		Paragraph reportTitle = new Paragraph("Consolidated Run Report",titleFont);
		reportTitle.setAlignment(Element.ALIGN_CENTER);
		addEmptyLine(reportTitle, 2);
		document.add(reportTitle);

		SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy hh:mm:ss");
		Chunk c1 = new Chunk("Generated On");
		c1.setFont(new Font(FontFamily.HELVETICA, 14, Font.BOLD));
		Chunk c2 = new Chunk(sdf.format(new Date()));

		Paragraph generatedOnBlock = new Paragraph();
		generatedOnBlock.add(c1);
		generatedOnBlock.add(new Chunk(" "));
		generatedOnBlock.add(c2);
		generatedOnBlock.setAlignment(Element.ALIGN_CENTER);
		Chunk c3 = new Chunk("Generated By");
		c3.setFont(new Font(FontFamily.HELVETICA, 14, Font.BOLD));
		Chunk c4 = new Chunk(this.userFullName);

		Paragraph generatedByBlock = new Paragraph();
		generatedByBlock.add(c3);
		generatedByBlock.add(new Chunk(" "));
		generatedByBlock.add(c4);
		generatedByBlock.setAlignment(Element.ALIGN_CENTER);
		document.add(generatedOnBlock);
		document.add(generatedByBlock);

	}
	catch(DocumentException e){
		throw new TenjinPdfException("Could not create Title page for Report",e);
	} catch (MalformedURLException e) {
		
		throw new TenjinPdfException("Could not create Title page for Report",e);
	} catch (IOException e) {
		
		throw new TenjinPdfException("Could not create Title page for Report",e);
	}


	
}		
/*Added by Ashiki for TENJINCG-1211 end*/
}
