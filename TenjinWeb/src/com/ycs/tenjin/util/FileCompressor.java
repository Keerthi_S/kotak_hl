/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  FileCompressor.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 15-05-2017			Sriram Sridharan		For TENJINCG-172
* 22-10-2018			Sriram Sridharan		TENJINCG-876
*/

package com.ycs.tenjin.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileCompressor {

	private static final Logger logger = LoggerFactory.getLogger(FileCompressor.class);

	private String sourceFolderPath;
	private String outputZipFilePath;
	private List<String> fileList = new ArrayList<String>();

	public FileCompressor(String sourceFolderPath, String outputZipFilePath) {
		super();
		this.sourceFolderPath = sourceFolderPath;
		this.outputZipFilePath = outputZipFilePath;
	}
	
	public void compress() throws Exception {
		
		if(Utilities.trim(this.sourceFolderPath).length() < 1 || Utilities.trim(this.outputZipFilePath).length() < 1) {
			logger.error("Either source folder path or output zip file path is not set");
			throw new Exception("Source and destination paths not set.");
		}
		
		try{
			//TENJINCG-876 - Sriram
			File file = new File(this.sourceFolderPath);
			this.sourceFolderPath = file.getAbsolutePath();
			this.generateFileList(file);
			//TENJINCG-876 - Sriram ends
			this.zipIt(this.outputZipFilePath);
		} catch(Exception e){
			throw e;
		}
	}
	
	
	private void zipIt(String zipFile){

		byte[] buffer = new byte[1024];

		try{

			FileOutputStream fos = new FileOutputStream(zipFile);
			ZipOutputStream zos = new ZipOutputStream(fos);

			logger.debug("Output to Zip : " + zipFile);

			for(String file : this.fileList){

				System.out.println("File Added : " + file);
				ZipEntry ze= new ZipEntry(file);
				zos.putNextEntry(ze);

				FileInputStream in =
						new FileInputStream(this.sourceFolderPath + File.separator + file);

				int len;
				while ((len = in.read(buffer)) > 0) {
					zos.write(buffer, 0, len);
				}

				in.close();
			}

			zos.closeEntry();
			zos.close();

			logger.debug("Done");
		}catch(IOException ex){
			logger.error("Error in file operation"+ex);
		}
	}

	private void generateFileList(File node){

		//add file only
		if(node.isFile()){
			this.fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
		}

		if(node.isDirectory()){
			String[] subNote = node.list();
			for(String filename : subNote){
				generateFileList(new File(node, filename));
			}
		}

	}


	private String generateZipEntry(String file){
	//TENJINCG-876 - Sriram
		if(file.startsWith(this.sourceFolderPath+ File.separator)) {
			return file.substring((this.sourceFolderPath + File.separator).length());
		}else {
			return file.substring(this.sourceFolderPath.length());
		}//TENJINCG-876 - Sriram ends
	}

}	
