/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TenjinMailThread.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 25-09-2019			Roshni Das				Newly added for TenjinCg-1097
 * */

package com.ycs.tenjin.util;

import java.sql.Timestamp;
import java.util.Date;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.TestSetHelper;
import com.ycs.tenjin.handler.TenjinMailHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.mail.TenjinJavaxMail;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.user.User;

public class TenjinMailThread implements Runnable {
	
	private int projectId;
	private int tsRecId;
	private String operation;
	private User user;
	private String module;
	private static final Logger logger = LoggerFactory
			.getLogger(TenjinMailThread.class);
	
	public TenjinMailThread(int projectId,int tsRecId,String module,String operation,User user) {
		this.projectId=projectId;
		this.tsRecId=tsRecId;
		this.module=module;
		this.operation=operation;
		this.user=user;
		
	}
	@Override
	public void run() {
		try{
			
			TenjinMailHandler tmhandler=new TenjinMailHandler();
			TestSet set= new TestSetHelper().hydrateTestSet(tsRecId, projectId);
			User tsOwner=new UserHandler().getUser(set.getCreatedBy());
			JSONObject mailContent=tmhandler.getMailContent(String.valueOf(tsRecId), set.getName(), module, user.getFullName(),
				operation, new Timestamp(new Date().getTime()), tsOwner.getEmail(), "");
			new TenjinJavaxMail().buildTenjinMailInformation(mailContent);
		}
		catch (DatabaseException e1) {
			
			logger.error("Error ", e1);
		}
		
		
	}
	

}
