/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Messages.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

package com.ycs.tenjin.util;


public enum Messages {
	
	REMOTE_DRIVER_URL_ERROR("RWD-ERR-01","Invalid Remove WebDriver URL"),
	REMOTE_DRIVER_INIT_ERROR("RWD-ERR-02","Could not initialize Remote Web Driver"),
	REMOTE_DRIVER_AUT_ERROR("RWD-ERR-03","Could not get AUT Information"),
	
	AUT_CONFIG_LOAD_ERROR("AUT-ERR-00","Could not load Configuration File"),
	AUT_CONFIG_ERROR("AUT-ERR-01","AUT Configuration Error"),
	AUT_LOGIN_ERROR("AUT-ERR-02","Could not login to the Application"),
	AUT_LOGOFF_ERROR("AUT-ERR-03","Could not log out of the Application"),
	AUT_MODULE_NAV_ERROR("AUT-ERR-04","Could not navigate to Application Function"),
	AUT_FRAME_NOT_FOUND("AUT-ERR-05","Specified frame does not exist"),
	AUT_REFRESH_FAILED("AUT-ERR-06","Failed to refresh Driver Context"),
	AUT_GENERIC_ERROR("AUT-ERR-07","An internal error occured which has aborted this action on the AUT"),
	AUT_EXIT_ELE_NOT_FOUND("AUT-ERR-08","Could not find exit element on the Application"),
	AUT_INSTANCE_UNAVAILABLE("AUT-ERR-09","Could not create Application Instance"),
	AUT_EXCLUSION_ERROR("AUT-ERR-10","Could not determine if the current link is excluded"),
	AUT_RECOVERY_ERROR("AUT-ERR-11","Could not navigate to Home Page"),
	
	LEARNING_CONFIG_ERROR("LNG-CNF-ERR-01","Could not load Configuration File for the current application"),
	LEARNING_SCAN_UID_ERROR("LNG-SCN-ERR-01","Could not get UID of element"),
	LEARNING_SCAN_LBL_ERRLR("LNG-SCN-ERR-02","Could not get Label of Element"),
	LOCATION_PERSIST_ERROR("LNG-LOC-ERR-01","Could not persist location details");
	
	
	private final String code;
	private final String description;
	
	private Messages(String code,String description){
		this.code = code;
		this.description = description;
	}
	
	public String getMessage(){
		return description;
	}
	
	public String getCode(){
		return code;
	}
	
	@Override
	public String toString(){
		return this.code + " - " + this.description;
	}
}

