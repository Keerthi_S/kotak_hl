/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MetaDataUtils.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              	DESCRIPTION
 * 23-04-2018           Padmavathi       	        for TENJINCG-617
 */

package com.ycs.tenjin.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.management.modelmbean.XMLParseException;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.utils.AnnotationUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.MetaDataHelper;

public class MetaDataUtils {

	private static final Logger logger = LoggerFactory
			.getLogger(MetaDataUtils.class);

	public int totalFunctionCount = 0;
	public int currentFunctionCount = 0;

	public int getTotalFunctionCount() {
		return totalFunctionCount;
	}

	public int getCurrentFunctionCount() {
		return currentFunctionCount;
	}

	public MetaDataHelper metaDataHelper = new MetaDataHelper();

	public void generateMetadataXML(String folderPath, String fileName,
			String modules, String apps, String selector)
			throws DatabaseException, SQLException,
			ParserConfigurationException, Exception {

		logger.debug("Begin to generate Export File with name " + fileName);
		int[] appid = null;
		if (apps != null) {
			String[] apps1 = apps.split(";");
			appid = new int[apps1.length];
			int i = 0;
			for (String string : apps1) {
				appid[i] = Integer.parseInt(string);
				i++;
			}
		}

		// Getting the total count
		if (selector.equalsIgnoreCase("allAut")) {
			totalFunctionCount = metaDataHelper.getFunctionCountofAllAuts();
		} else if (selector.equalsIgnoreCase("selected_auts")) {
			totalFunctionCount = metaDataHelper
					.getFunctionCountofPartialAuts(appid);
		} else if (selector.equalsIgnoreCase("gfunc")) {
			totalFunctionCount = modules.split(";").length;
		}

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();

			Element metadataRootNode = doc.createElement("tenjin-aut-metadata");
			doc.appendChild(metadataRootNode);

			Element auts = doc.createElement("auts");
			metadataRootNode.appendChild(auts);

			ArrayList<Aut> autList = new ArrayList<Aut>();
			if (selector.equalsIgnoreCase("allAut")) {
				autList = metaDataHelper.getAllAuts();
			} else if (selector.equalsIgnoreCase("selected_auts")) {
				autList = metaDataHelper.getSetOfAuts(appid);
			} else if (selector.equalsIgnoreCase("gfunc")) {
				autList = metaDataHelper.getSetOfAuts(appid);
			}

			// Loop for applications
			for (Aut aut : autList) {

				Element application = doc.createElement("aut");
				auts.appendChild(application);
				Map<Method, String> getterMethods = AnnotationUtils
						.getGettersWithXMLNode(aut.getClass());

				for (Entry<Method, String> entrySet : getterMethods.entrySet()) {
					Element appIdEle = doc.createElement(entrySet.getValue());
					application.appendChild(appIdEle);

					if (entrySet.getKey().getReturnType()
							.isAssignableFrom(int.class)) {
						appIdEle.appendChild(doc
								.createTextNode(nullCheck(String
										.valueOf(entrySet.getKey().invoke(aut,
												null)))));
					} else if (entrySet.getKey().getReturnType()
							.isAssignableFrom(String.class)) {
						appIdEle.appendChild(doc
								.createTextNode(nullCheck((String) entrySet
										.getKey().invoke(aut, null))));
					}
				}

				Element functions = doc.createElement("functions");
				application.appendChild(functions);

				ArrayList<Module> moduleList = new ArrayList<Module>();
				if (selector.equalsIgnoreCase("allAut")
						|| selector.equalsIgnoreCase("selected_auts")) {
					moduleList = metaDataHelper.getAllModules(aut.getId());
				} else if (selector.equalsIgnoreCase("gfunc")) {
					String[] moduleArray = modules.split(";");
					moduleList = metaDataHelper.getSetOfModules(aut.getId(),
							moduleArray);
				}

				// Loop for functions under the application
				for (Module module : moduleList) {

					Element function = doc.createElement("function");
					functions.appendChild(function);
					getterMethods = AnnotationUtils
							.getGettersWithXMLNode(module.getClass());

					for (Entry<Method, String> entrySet : getterMethods
							.entrySet()) {
						Element funcCode = doc.createElement(entrySet
								.getValue());
						function.appendChild(funcCode);
						if (entrySet.getKey().getReturnType()
								.isAssignableFrom(int.class)) {
							funcCode.appendChild(doc
									.createTextNode(nullCheck(String
											.valueOf(entrySet.getKey().invoke(
													module, null)))));
						} else if (entrySet.getKey().getReturnType()
								.isAssignableFrom(String.class)) {
							funcCode.appendChild(doc
									.createTextNode(nullCheck((String) entrySet
											.getKey().invoke(module, null))));
						} else if (entrySet.getKey().getReturnType()
								.isAssignableFrom(LearnerResultBean.class)) {
							String status = metaDataHelper.getFunctionStatus(
									module.getCode(), aut.getId());
							funcCode.appendChild(doc.createTextNode(status));
						}
					}

					Element metadata = doc.createElement("metadata");
					function.appendChild(metadata);

					ArrayList<Location> locations = metaDataHelper
							.getFunctionPageAreas(aut.getId(), module.getCode());

					for (Location location : locations) {

						Element pagearea = doc.createElement("pagearea");
						metadata.appendChild(pagearea);
						getterMethods = AnnotationUtils
								.getGettersWithXMLNode(location.getClass());
						for (Entry<Method, String> entrySet : getterMethods
								.entrySet()) {
							Element paName = doc.createElement(entrySet
									.getValue());
							pagearea.appendChild(paName);

							if (entrySet.getKey().getReturnType()
									.isAssignableFrom(int.class)) {
								paName.appendChild(doc.createTextNode(nullCheck(String
										.valueOf(entrySet.getKey().invoke(
												location, null)))));
							} else if (entrySet.getKey().getReturnType()
									.isAssignableFrom(String.class)) {
								paName.appendChild(doc
										.createTextNode(nullCheck((String) entrySet
												.getKey()
												.invoke(location, null))));
							} else if (entrySet.getKey().getReturnType()
									.isAssignableFrom(boolean.class)) {
								if ((boolean) entrySet.getKey().invoke(
										location, null)) {
									paName.appendChild(doc.createTextNode("Y"));
								} else {
									paName.appendChild(doc.createTextNode("N"));
								}
							}
						}

						Element fields = doc.createElement("fields");
						pagearea.appendChild(fields);
						ArrayList<TestObject> testObjects = metaDataHelper
								.getFunctionFields(aut.getId(),
										module.getCode(),
										location.getLocationName());
						for (TestObject testObject : testObjects) {

							Element field = doc.createElement("field");
							fields.appendChild(field);
							getterMethods = AnnotationUtils
									.getGettersWithXMLNode(testObject
											.getClass());
							for (Entry<Method, String> entrySet : getterMethods
									.entrySet()) {
								Element fldSeqNo = doc.createElement(entrySet
										.getValue());
								field.appendChild(fldSeqNo);

								if (entrySet.getKey().getReturnType()
										.isAssignableFrom(int.class)) {
									fldSeqNo.appendChild(doc
											.createTextNode(nullCheck(String
													.valueOf(entrySet.getKey()
															.invoke(testObject,
																	null)))));
								} else if (entrySet.getKey().getReturnType()
										.isAssignableFrom(String.class)) {
									fldSeqNo.appendChild(doc
											.createTextNode(nullCheck((String) entrySet
													.getKey().invoke(
															testObject, null))));
								}
							}
						}
					}
					currentFunctionCount++;
				}
			}
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			/*Added by paneendra for VAPT FIX starts*/
			transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
			/*Added by paneendra for VAPT FIX ends*/
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(folderPath + "\\"
					+ fileName + ".xml"));
			transformer.transform(source, result);
			logger.debug("Export File generated Successfully");
		} catch (Exception e) {
			logger.error("An exception occured while generating Export file", e);
			throw new XMLParseException(
					"An exception occured while generating Export file");
		}
	}

	/*****************************************************************
	 * Added by Sameer for Upload of rest metadata
	 */
	public JSONObject parseImportedFile(String filePath)
			throws ParserConfigurationException, SAXException, IOException,
			JSONException, DatabaseException, SQLException, LearnerException,
			IllegalArgumentException, SecurityException,
			InvocationTargetException, NoSuchMethodException {
		File xmlFile = new File(filePath);
		JSONObject json = this.parseImportedFile(xmlFile);
		return json;
	}

	public JSONObject parseImportedFile(File xmlFile)
			throws ParserConfigurationException, SAXException, IOException,
			JSONException, DatabaseException, SQLException, LearnerException,
			IllegalArgumentException, SecurityException,
			InvocationTargetException, NoSuchMethodException {

		JSONObject json = new JSONObject();
		JSONArray autArray = new JSONArray();
		ArrayList<String> missingFieldsInAnno = new ArrayList<String>();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		/*Added by paneendra for VAPT FIX starts*/
		DocumentBuilderFactory.newInstance("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl", ClassLoader.getSystemClassLoader());
		dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		//dbFactory.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, "true");
		dbFactory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
		/*Added by paneendra for VAPT FIX ends*/
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(xmlFile);

		doc.getDocumentElement().normalize();

		int length = doc.getElementsByTagName("auts").getLength();
		if (length == 0) {
			throw new ParserConfigurationException();
		}

		logger.debug("Conversion of JSON Begins");

		ArrayList<String> autXMLNodes = new ArrayList<String>();
		ArrayList<String> moduleXMLNodes = new ArrayList<String>();
		ArrayList<String> paXMLNodes = new ArrayList<String>();
		ArrayList<String> fieldXMLNodes = new ArrayList<String>();

		// For AUT
		NodeList autNodes = doc.getElementsByTagName("aut");
		for (int i = 0; i < autNodes.getLength(); i++) {
			JSONObject autDetails = new JSONObject();
			JSONObject autJson = new JSONObject();
			JSONArray funcArray = new JSONArray();

			Node autNode = autNodes.item(i);
			NodeList autChildNodes = autNode.getChildNodes();
			for (int j = 0; j < autChildNodes.getLength() - 1; j++) {
				Node autChildNode = autChildNodes.item(j);
				autDetails.put(autChildNode.getNodeName(),
						autChildNode.getTextContent());
				if (i == 0) {
					autXMLNodes.add(autChildNode.getNodeName());
				}
			}

			// For Function
			NodeList functionNodes = ((Element) autNode)
					.getElementsByTagName("function");
			for (int j = 0; j < functionNodes.getLength(); j++) {
				JSONObject functionDetails = new JSONObject();
				JSONObject functionJson = new JSONObject();
				JSONArray pageAreaJArray = new JSONArray();

				Node functionNode = functionNodes.item(j);
				NodeList functionChildNodes = functionNode.getChildNodes();
				for (int k = 0; k < functionChildNodes.getLength() - 1; k++) {
					Node functionChildNode = functionChildNodes.item(k);
					functionDetails.put(functionChildNode.getNodeName(),
							functionChildNode.getTextContent());
					if (i == 0 && j == 0) {
						moduleXMLNodes.add(functionChildNode.getNodeName());
					}
				}
				functionJson.put("tjn_function", functionDetails);

				// For Pagearea
				NodeList pageAreaNodes = ((Element) functionNode)
						.getElementsByTagName("pagearea");
				for (int k = 0; k < pageAreaNodes.getLength(); k++) {

					JSONObject pageAreaDetails = new JSONObject();
					JSONObject pageAreaJson = new JSONObject();
					JSONArray fieldJArray = new JSONArray();

					Node pageAreaNode = pageAreaNodes.item(k);
					NodeList pageAreaChildNodes = pageAreaNode.getChildNodes();
					for (int l = 0; l < pageAreaChildNodes.getLength() - 1; l++) {
						Node pageAreaChildNode = pageAreaChildNodes.item(l);
						pageAreaDetails.put(pageAreaChildNode.getNodeName(),
								pageAreaChildNode.getTextContent());
						if (i == 0 && j == 0 && k == 0) {
							paXMLNodes.add(pageAreaChildNode.getNodeName());
						}

					}
					pageAreaJson.put("pagearea", pageAreaDetails);

					// For Fields
					NodeList fieldNodes = ((Element) pageAreaNode)
							.getElementsByTagName("field");
					for (int l = 0; l < fieldNodes.getLength(); l++) {
						Node fieldNode = fieldNodes.item(l);
						NodeList fieldChildNodes = fieldNode.getChildNodes();
						JSONObject fieldJson = new JSONObject();
						JSONObject fieldDetails = new JSONObject();

						for (int m = 0; m < fieldChildNodes.getLength(); m++) {
							Node fieldChildNode = fieldChildNodes.item(m);
							fieldDetails.put(fieldChildNode.getNodeName(),
									fieldChildNode.getTextContent());
							if (i == 0 && j == 0 && k == 0 && l == 0) {
								fieldXMLNodes.add(fieldChildNode.getNodeName());
							}
						}

						fieldJson.put("field", fieldDetails);
						fieldJArray.put(fieldJson);
					}

					pageAreaDetails.put("fields", fieldJArray);
					pageAreaJArray.put(pageAreaJson);
				}
				functionDetails.put("pageareas", pageAreaJArray);
				funcArray.put(functionJson);
			}
			autDetails.put("functions", funcArray);
			autJson.put("aut", autDetails);
			autArray.put(autJson);

			autXMLNodes.removeAll(AnnotationUtils.getXMLNodes(Aut.class));
			moduleXMLNodes.removeAll(AnnotationUtils.getXMLNodes(Module.class));
			paXMLNodes.removeAll(AnnotationUtils.getXMLNodes(Location.class));
			fieldXMLNodes.removeAll(AnnotationUtils
					.getXMLNodes(TestObject.class));

			missingFieldsInAnno.addAll(autXMLNodes);
			missingFieldsInAnno.addAll(moduleXMLNodes);
			missingFieldsInAnno.addAll(paXMLNodes);
			missingFieldsInAnno.addAll(fieldXMLNodes);
		}

		json.put("auts", autArray);
		json.put("missing-anno", missingFieldsInAnno);
		logger.debug("Attains the JSON Format by parsing the imported Metadata file");
		return json;
	}

	public Multimap<String, JSONObject> selectedData(String selector,
			List<String> funcList, String[] autList, JSONObject autJson,
			boolean importFlag,String status,String learnMode) throws Exception {
		Multimap<String, JSONObject> res1 = ArrayListMultimap.create();
		if (selector.equalsIgnoreCase("selected_auts")) {
			JSONArray functionsArray = autJson.getJSONArray("functions");
			totalFunctionCount += functionsArray.length();
			/*Changed by Padmavathi for TENJINCG-617 starts*/
			res1 = metaDataHelper.persistCompleteAut(autJson, importFlag,status,learnMode);
			/*Changed by Padmavathi for TENJINCG-617 ends*/
		} else if (selector.equalsIgnoreCase("gfunc")) {
			totalFunctionCount += funcList.size();
			/*Changed by Padmavathi for TENJINCG-617 starts*/
			res1 = metaDataHelper.persistSetOfModules(funcList, autJson,
					importFlag,status,learnMode);
			/*Changed by Padmavathi for TENJINCG-617 ends*/
		}
		return res1;
	}

	private String nullCheck(String value) {
		String nullValue = "";
		if (value != null) {
			nullValue = value;
		} else {
			nullValue = "BLANK";
		}
		return nullValue;
	}

	public void overrideFunctions(List<String> dataMap, JSONObject autJson,
			Multimap<String, JSONObject> overrideMap, String appName,String status,String learnMode)
			throws Exception {

		List<String> functions = new ArrayList<String>();
		ArrayList<String> newFunctionList = new ArrayList<String>();
		String functionNodeName = AnnotationUtils
				.getFunctionNodeName(Module.class);
		for (JSONObject functionJson : overrideMap.get("New")) {
			newFunctionList.add(functionJson.getString(functionNodeName));
		}

		if (dataMap.get(0).equalsIgnoreCase("all")) {
			totalFunctionCount += overrideMap.size();
			for (JSONObject functionJson : overrideMap.get("Existing")) {
				functions.add(functionJson.getString(functionNodeName));
			}
		} else {
			totalFunctionCount += (dataMap.size() + overrideMap.get("New")
					.size());
			List<JSONObject> existingFunctions = (List<JSONObject>) overrideMap
					.get("Existing");
			for (JSONObject functionJson : existingFunctions) {
				for (String functionString : dataMap) {
					if (functionJson.getString(functionNodeName)
							.equalsIgnoreCase(functionString)) {
						functions.add(functionJson.getString(functionNodeName));
					}
				}
			}
		}

		if (newFunctionList.size() > 0)
			/*Changed by Padmavathi for TENJINCG-617 starts*/
			metaDataHelper.persistSetOfModules(newFunctionList, autJson, true,status,learnMode);
		/*Changed by Padmavathi for TENJINCG-617 ends*/
		if (overrideMap.get("Existing").size() > 0)
			/*Changed by Padmavathi for TENJINCG-617 starts*/
			metaDataHelper.overrideExistingFunctions(functions, appName,
					autJson,status,learnMode);
		/*Changed by Padmavathi for TENJINCG-617 ends*/
		currentFunctionCount = metaDataHelper.getCurrentFunctionCount();
	}

	

	public void persistData(File file, String userId, String status)
			throws Exception {

		List<String> funcList = new ArrayList<String>();
		String[] autList = null;
		JSONObject finalJson = this.parseImportedFile(file);
		JSONArray auts = finalJson.getJSONArray("auts");
		autList = new String[auts.length()];
		for (int i = 0; i < auts.length(); i++) {
			JSONObject aut = (JSONObject) auts.get(i);
			JSONObject json1 = aut.getJSONObject("aut");
			json1.put("userId", userId);
			String appName = json1.getString("name");
			autList[i] = appName;

			List<String> list = new ArrayList<String>();
			list.add("all");
			
			Multimap<String, JSONObject> result = this.selectedData(
					"selected_auts", funcList, autList, json1, false, status,"UFTLearn");
			if (result.size() > 0) {
				this.overrideFunctions(list, json1, result, appName, status,"UFTLearn");
			} else {
				this.selectedData("selected_auts", funcList, autList, json1,
						true, status,"UFTLearn");
			}
			/*Changed by Padmavathi for TENJINCG-617 ends*/
		}
	}

}