/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  HatsStack.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

package com.ycs.tenjin.util;

import java.util.ArrayList;

public class HatsStack {
	private ArrayList<String> stack;
	
	public HatsStack(){
		this.stack = new ArrayList<String>();
	}
	
	public void push(String str){
		this.stack.add(str);
	}
	
	public String pop(){
		String retVal = this.stack.get(this.stack.size()-1);
		this.stack.remove(this.stack.size()-1);
		
		return retVal;
	}
	
	public int size(){
		return this.stack.size();
	}
	
	public String peek(){
		return this.stack.get(this.stack.size()-1);
	}
	
	public String peek(int arg){
		return this.stack.get(this.stack.size() + (arg-1));
	}
	
	public boolean contains(String str){
		if(str == null){
			return false;
		}
		
		boolean match = false;
		for(int i=0;i<this.stack.size();i++){
			if(this.stack.get(i).equalsIgnoreCase(str)){
				match = true;
				break;
			}
		}
		
		return match;
	}
	
	public void show(){
		for(int i=this.stack.size()-1;i>-1;i--){
			System.out.println(this.stack.get(i));
		}
	}
	
	
}
