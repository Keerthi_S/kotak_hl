/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  FileView.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

package com.ycs.tenjin.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;

import javax.swing.JEditorPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.EditorKit;


public class FileView {

	public String readRTF(File filename) throws IOException,
			BadLocationException {

		// read rtf from file
		JEditorPane p = new JEditorPane();
		p.setContentType("text/rtf");

		EditorKit rtfKit = p.getEditorKitForContentType("text/rtf");
		rtfKit.read(new FileReader(filename), p.getDocument(), 0);
		rtfKit = null;

		// convert to text
		EditorKit txtKit = p.getEditorKitForContentType("text/plain");
		Writer writer = new StringWriter();
		txtKit.write(writer, p.getDocument(), 0, p.getDocument().getLength());
		String documentText = writer.toString();

		return documentText;

	}

	public String fileInput(String filename) throws IOException {

		String file = "";
		// reading
		try {
			InputStream ips = new FileInputStream(filename);
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(ipsr);
			String line;
			while ((line = br.readLine()) != null) {
				file += line + "\n";
			}
			System.out.println(file);
			br.close();
			ipsr.close();
			ips.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return file;
	}


}