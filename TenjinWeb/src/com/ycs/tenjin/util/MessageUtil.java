/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MessageUtil.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 19-Jul-2017		Sriram Sridharan		Added for TENJINCG-312
*/
package com.ycs.tenjin.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;

public class MessageUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageUtil.class);
	
	public static String getMessage(String key, String... args) throws TenjinConfigurationException {
		String message="";
		
		String propertyValue = "";
		
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			input = new TenjinConfiguration().getClass().getClassLoader().getResourceAsStream("ui-val-messages.properties");
			prop.load(input);
			propertyValue = prop.getProperty(key);
			
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage(),e);
			throw new TenjinConfigurationException("Could not load Tenjin Message Configuration",e);
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
			throw new TenjinConfigurationException("Could not load Tenjin Message Configuration",e);
		}
		
		//return propertyValue;
		
		if(Utilities.trim(propertyValue).length() > 0) {
			message = MessageFormat.format(propertyValue, args);
		}
		
		return message;
	}
	
	public static void main(String[] args) throws TenjinConfigurationException {
		String message = getMessage("Field_Value_Check.actual", "The following fields are missing: Individual");
		System.out.println(message);
	}
}
