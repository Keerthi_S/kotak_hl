/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCaseAjaxServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
*25-10-2018			   	Sriram Sridharan		Newly Added for TENJINCG-875
*02-11-2018				Sriram Sridharan		TENJINCG-896
*22-06-2019             Padmavathi              V2.8-37 
*/


package com.ycs.tenjin.util;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ycs.tenjin.project.Domain;
import com.ycs.tenjin.project.Project;


public class JsTreeUtils {
	public String getJsTreeJson(List<Domain> domains) {
		String json = "";
		if(domains != null) {
			JsonArray jar = new JsonArray();
			int counter=0;
			for(Domain domain : domains) {
				counter++;
				JsonObject d = this.getDomainJsonObject(domain, counter);
				
				JsonObject publicProjects = this.getProjectTypeJsonObject("Public", escapeHtml(domain.getName()), counter);
				JsonArray publicProjectsChildren  = new JsonArray();
				JsonArray privateProjectsChildren = new JsonArray();
				JsonObject privateProjects = this.getProjectTypeJsonObject("Private", escapeHtml(domain.getName()), counter);
				if(domain.getProjects() != null) {
					for(Project project : domain.getProjects()) {
						//TENJINCG-896 (Sriram)
						if(project.getId() < 1) {
							continue;
						}
						//TENJINCG-896 (Sriram) ends
						JsonObject pJson = this.getProjectJsonObject(project);
						if(Utilities.trim(pJson.get("ptype").getAsString()).equalsIgnoreCase("private")) {
							privateProjectsChildren.add(pJson);
						}else {
							publicProjectsChildren.add(pJson);
						}
					}
				}
				
				publicProjects.add("children", publicProjectsChildren);
				privateProjects.add("children", privateProjectsChildren);
				JsonArray domainChildren = new JsonArray();
				if(publicProjectsChildren.size() > 0)
					domainChildren.add(publicProjects);
				if(privateProjectsChildren.size() > 0)
					domainChildren.add(privateProjects);
				d.add("children", domainChildren);
				
				jar.add(d);
			}
			json = jar.toString();
		}
		
		return json;
	}
	
	private JsonObject getProjectTypeJsonObject(String projectType, String domainName, int counter) {
		String nodeId = "domain_" + counter + projectType;
		JsonObject j = new JsonObject();
		j.addProperty("id", nodeId);
		j.addProperty("text", projectType);
		j.addProperty("icon", "jstree/folder.png");
		
		JsonObject aattr = new JsonObject();
		aattr.addProperty("href", "DomainServletNew?param=domain_view&paramval=" + domainName + "&type=" + projectType);
		j.add("a_attr", aattr);
		
		JsonObject liattr = new JsonObject();
		liattr.addProperty("nodeType", "project_folder");
		liattr.addProperty("projectType", projectType);
		j.add("li_attr",liattr);
		return j;
	}
	
	
	private JsonObject getDomainJsonObject(Domain domain, int counter) {
		JsonObject d = new JsonObject();
		if(domain != null) {
			/*Modified by Padmavathi for V2.8-37 starts*/
			d.addProperty("id", "domain_" + escapeHtml(domain.getName()));
			/*Modified by Padmavathi for V2.8-37 ends*/
			d.addProperty("text", escapeHtml(domain.getName()));
			d.addProperty("icon","jstree/domain.png");
			
			JsonObject liattr = new JsonObject();
			liattr.addProperty("domainName", escapeHtml(domain.getName()));
			liattr.addProperty("nodeType", "domain");
			d.add("li_attr", liattr);
			
			JsonObject aattr = new JsonObject();
			aattr.addProperty("href", "DomainServletNew?param=domain_view&paramval=" + domain.getName());
			aattr.addProperty("domainName", domain.getName());
			d.add("a_attr", aattr);
		}
		return d;
	}
	
	private JsonObject getProjectJsonObject(Project project) {
		JsonObject json = new JsonObject();
		if(project != null) {
			json.addProperty("id", "project_" + project.getId());
			json.addProperty("text", escapeHtml(project.getName()));
			if(project.getState() != null && project.getState().equalsIgnoreCase("A")) {
				json.addProperty("icon", "jstree/project.png");
			}else {
				json.addProperty("icon", "jstree/inactiveproject.png");
			}
			json.addProperty("ptype", project.getType());
			JsonObject liattr = new JsonObject();
			liattr.addProperty("nodeType", "project");
			liattr.addProperty("projectId", project.getId());
			json.add("li_attr", liattr);
			
			JsonObject aattr = new JsonObject();
			aattr.addProperty("href", "ProjectServletNew?t=load_project&paramval=" + project.getId() + "&view_type=ADMIN");
			aattr.addProperty("projectId", project.getId());
			aattr.addProperty("owner", project.getOwner());
			aattr.addProperty("state", project.getState());
			aattr.addProperty("type", project.getType());
			
			json.add("a_attr", aattr);
		}
		return json;
	}
	
	private static String escapeHtml(String str) {
		return StringEscapeUtils.escapeHtml4(StringUtils.trim(str));
	}
}
