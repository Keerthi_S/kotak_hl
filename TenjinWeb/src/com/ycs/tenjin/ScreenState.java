/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ScreenState.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 19-Oct-2017		Sriram Sridharan		Newly added for TENJINCG-396 and TENJINCG-403	
* */

package com.ycs.tenjin;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.util.Utilities;

public class ScreenState {
	private String status;
	private String messageKey;
	private String[] targetFields;
	private String message;
	
	private static final Logger logger = LoggerFactory.getLogger(ScreenState.class);
	
	public ScreenState(String status, String messageKey) {
		super();
		this.status = status;
		this.messageKey = messageKey;
	}
	
	public ScreenState(String status, String messageKey, String[] targetFields) {
		super();
		this.status = status;
		this.messageKey = messageKey;
		this.targetFields = targetFields;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessageKey() {
		return messageKey;
	}
	public void setMessageKey(String message) {
		this.messageKey = message;
	}

	public String[] getTargetFields() {
		return targetFields;
	}

	public void setTargetFields(String[] targetFields) {
		this.targetFields = targetFields;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public static void main(String[] args) {
		String bundleMessage = "L''utilisateur {0} a �t� mis � jour avec succ�s";
		
		System.out.println(MessageFormat.format(bundleMessage, "user01"));
	}
	
	public void loadMessage() {
		Locale locale = new Locale("en", "US");
		this.loadMessage(locale);
	}
	
	public void loadMessage(Locale locale) {
		logger.info("Loading message for {}", locale.toString());
		ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.tjn_messages", locale);
		try {
			String messageFromBundle = resourceBundle.getString(this.messageKey);
			this.message = Utilities.trim(messageFromBundle).length() > 0 ? MessageFormat.format(messageFromBundle, (Object[]) this.targetFields) : messageKey;
			
		} catch (Exception e) {
			logger.error("ERROR getting value for Message Key {} from Message bundle", messageKey, e);
			this.message = messageKey;
		}
	}
	
	
	
}
