/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  OperationSelectorTag.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 07-03-2018           Padmavathi              Newly Added For TENJINCG-612
 * 23-11-2018           Padmavathi              TENJINCG-903
 * 17-12-2018           Padmavathi              TJN252-36
 * 
 */

package com.ycs.tenjin.core.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.util.Utilities;

public class OperationSelectorTag extends SimpleTagSupport  {

	private static final Logger logger = LoggerFactory.getLogger(OperationSelectorTag.class);
	private String cssClass;
	private String id;
	private String name;
	private String defaultValue;
	private int applicationId;
	private String mandatory;
	private String title;
	public String getCssClass() {
		return cssClass;
	}
	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public String getMandatory() {
		return mandatory;
	}
	public void setMandatory(String mandatory) {
		this.mandatory = mandatory;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Override
	public void doTag() throws JspException, IOException {
		String finalHtml = "<select id='" + this.id +"' applicationId='"+ this.applicationId+"' name='"+ this.name+ "' mandatory='"+this.mandatory+"' title='"+this.title+"' class='" + this.cssClass + "'>";
		finalHtml += "<option value='-1'>-- Select One --</option>";
		if(this.applicationId > 0) {
			try {
				Aut aut = new AutHandler().getApplication(this.applicationId);
				String [] operations= aut.getOperation().split(",");
				/*Added by Padmavathi for TENJINCG-903 starts*/
				/*commented by Padmavathi for TJN252-36 starts*/
				/*boolean defaultOperation=false*/;
				/*commented by Padmavathi for TJN252-36 ends*/
				/*Added by Padmavathi for TENJINCG-903 ends*/
				for(String operation : operations) {
					if(Utilities.trim(operation).equalsIgnoreCase(this.defaultValue)) {
						/*Added by Padmavathi for TENJINCG-903 starts*/
						/*commented by Padmavathi for TJN252-36 starts*/
						/*defaultOperation=true;*/
						/*commented by Padmavathi for TJN252-36 ends*/
						/*Added by Padmavathi for TENJINCG-903 ends*/
						finalHtml += "<option value='" + Utilities.trim(operation) + "' selected>" + Utilities.trim(operation) +  "</option>";
					}else {
						finalHtml += "<option value='" + Utilities.trim(operation) + "' >" + Utilities.trim(operation) +  "</option>";
					}
				}
				/*Added by Padmavathi for TENJINCG-903 starts*/
				/*commented by Padmavathi for TJN252-36 starts*/
				//To show operation value in test step,if operation is not exists in AUT. 
				/*if(!defaultOperation){
					this.defaultValue=this.defaultValue+" (Not exists in AUT)";
					finalHtml += "<option value='" + Utilities.trim(this.defaultValue) + "' selected>" + Utilities.trim(this.defaultValue) +  "</option>";
				}*/
				/*commented by Padmavathi for TJN252-36 ends*/
				/*Added by Padmavathi for TENJINCG-903 ends*/
			} catch (DatabaseException e) {
				logger.error("ERROR getting function groups for application {}", this.applicationId, e);
			}
		}
		finalHtml += "</select>";
		
		getJspContext().getOut().write(finalHtml);
	}
}
