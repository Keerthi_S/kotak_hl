/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApplicationSelectorTag.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 24-Oct-2017           Sriram Sridharan          Newly Added For TENJINCG-396
 * 14-03-2018            Padmavathi                TENJINCG-612
 * 13-11-2019            Ramya                     TENJINCG-1166
 * 
 */

package com.ycs.tenjin.core.tags;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.AutsHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.util.Utilities;

public class ApplicationSelectorTag extends SimpleTagSupport {

	static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(ApplicationSelectorTag.class);
	
	private boolean loadFunctionGroups;
	private boolean loadFunctions;
	/*added by Padmavathi for TENJINCG-612 starts*/
	private boolean loadApiOperations;
	private boolean loadOperations;
	private boolean loadAutLoginTypes;
	private String apiSelector;
	private String operationSelector;
	private String autLoginTypeSelector;
	private boolean loadApis;
	/*added by Padmavathi for TENJINCG-612 ends*/
	private int projectId;
	private String functionSelector;
	private String groupSelector;
	private String defaultValue;
	private String cssClass;
	private String id;
	private String name;
	private String mandatory;
	private String title;

	/*Added by Ashiki for TENJINCG-1275 starts*/
	private boolean loadMsg;
	/*Added by Ashiki for TENJINCG-1275 starts*/
	
	
	
	public String getMandatory() {
		return mandatory;
	}
	public void setMandatory(String mandatory) {
		this.mandatory = mandatory;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	/*added by Padmavathi for TENJINCG-612 starts*/
	public boolean isLoadApiOperations() {
		return loadApiOperations;
	}
	public void setLoadApiOperations(boolean loadApiOperations) {
		this.loadApiOperations = loadApiOperations;
	}
	public boolean isLoadApis() {
		return loadApis;
	}
	public void setLoadApis(boolean loadApis) {
		this.loadApis = loadApis;
	}
	public boolean isLoadOperations() {
		return loadOperations;
	}
	public void setLoadOperations(boolean loadOperations) {
		this.loadOperations = loadOperations;
	}
	public String getApiSelector() {
		return apiSelector;
	}
	public void setApiSelector(String apiSelector) {
		this.apiSelector = apiSelector;
	}
	public String getOperationSelector() {
		return operationSelector;
	}
	public void setOperationSelector(String operationSelector) {
		this.operationSelector = operationSelector;
	}
	public String getAutLoginTypeSelector() {
		return autLoginTypeSelector;
	}
	public void setAutLoginTypeSelector(String autLoginTypeSelector) {
		this.autLoginTypeSelector = autLoginTypeSelector;
	}
	public boolean isLoadAutLoginTypes() {
		return loadAutLoginTypes;
	}
	public void setLoadAutLoginTypes(boolean loadAutLoginTypes) {
		this.loadAutLoginTypes = loadAutLoginTypes;
	}
	/*added by Padmavathi for TENJINCG-612 ends*/
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isLoadFunctionGroups() {
		return loadFunctionGroups;
	}

	public void setLoadFunctionGroups(boolean loadFunctionGroups) {
		this.loadFunctionGroups = loadFunctionGroups;
	}

	public boolean isLoadFunctions() {
		return loadFunctions;
	}
	

	
	public void setLoadFunctions(boolean loadFunctions) {
		this.loadFunctions = loadFunctions;
	}

	
	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	
	public String getFunctionSelector() {
		return functionSelector;
	}

	public void setFunctionSelector(String functionSelector) {
		this.functionSelector = functionSelector;
	}

	public String getGroupSelector() {
		return groupSelector;
	}

	public void setGroupSelector(String groupSelector) {
		this.groupSelector = groupSelector;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	
	@Override
	/*Modified by Padmavathi for TENJINCG-612 starts*/
	public void doTag() throws JspException, IOException {
		List<Aut> applications = new ArrayList<Aut>();
		if(this.projectId > 0) {
			try {
				applications = new AutsHelper().hydrateProjectAuts(projectId);
			} catch (DatabaseException e) {
				logger.error("ERROR getting all applications", e);
			}
		}else {
			try {
				applications = new AutHandler().getAllApplications();
			} catch (DatabaseException e) {
				logger.error("ERROR getting all applications", e);
			}
		}
		
		String finalHtml = "";
		finalHtml += "<select mandatory='" + this.mandatory +  "' title='" + this.title + "' id='"+ this.id +"' name='"+ this.name +"' class='" + this.cssClass + 
		" applicationSelector' data-load-functions='" + this.loadFunctions + "'data-load-apis='" + this.loadApis +"'data-load-msg='" + this.loadMsg+"'data-operation-selector='" + this.operationSelector + 
		"' data-load-function-groups='" + this.loadFunctionGroups +"' data-load-api-operations='" + this.loadApiOperations + "' data-load-operations='" + this.loadOperations + 
		"' data-function-selector='" + this.functionSelector +"'data-api-selector='"+this.apiSelector+
		"'data-aut-login-type-selector='"+this.autLoginTypeSelector+"'data-load-aut-login-types='"+this.loadAutLoginTypes+ "' data-function-group-selector='" + this.groupSelector + "'>";
		finalHtml += "<option value='-1'>-- Select One --</option>";
		for(Aut aut : applications) {
			if(Integer.toString(aut.getId()).equalsIgnoreCase(this.defaultValue)) {
				/* Modified by Ramya for TENJINCG-1166  starts*/
				finalHtml += "<option selected value=" + aut.getId() + ">" + Utilities.escapeXml(aut.getName()) + "</option>";
			}else {
				finalHtml += "<option value=" + aut.getId() + ">" + Utilities.escapeXml(aut.getName()) + "</option>";
				/* Modified by Ramya for TENJINCG-1166  ends*/
			}
		}
		
		finalHtml += "</select>";
		getJspContext().getOut().write(finalHtml);
	}
	/*Modified by Padmavathi for TENJINCG-612 ends*/

	/*Added by Ashiki for TENJINCG-1275 starts*/
	public boolean isLoadMsg() {
		return loadMsg;
	}
	public void setLoadMsg(boolean loadMsg) {
		this.loadMsg = loadMsg;
	}

	/*Added by Ashiki for TENJINCG-1275 starts*/
}
