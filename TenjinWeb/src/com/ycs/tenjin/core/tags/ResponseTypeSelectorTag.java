/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ResponseTypeSelectorTag.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 07-03-2018           Padmavathi              Newly Added For TENJINCG-612
 * 
 */


package com.ycs.tenjin.core.tags;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.util.Utilities;

public class ResponseTypeSelectorTag extends SimpleTagSupport {

	private static final Logger logger = LoggerFactory.getLogger(ResponseTypeSelectorTag.class);
	private String cssClass;
	private String id;
	private String name;
	private String defaultValue;
	private int applicationId;
	private String apiCode;
	private String operation;
	private String mandatory;
	private String title;
	public String getCssClass() {
		return cssClass;
	}
	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public String getApiCode() {
		return apiCode;
	}
	public void setApiCode(String apiCode) {
		this.apiCode = apiCode;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getMandatory() {
		return mandatory;
	}
	public void setMandatory(String mandatory) {
		this.mandatory = mandatory;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Override
	public void doTag() throws JspException, IOException {
		String finalHtml = "<select id='" + this.id +"' applicationId='"+ this.applicationId+"'  apiCode='"+ this.apiCode+"' operation='"+ this.operation+"' name='"+ this.name+ "' mandatory='"+this.mandatory+"' title='"+this.title+"' class='" + this.cssClass + "'>";
		finalHtml += "";
		if(this.applicationId > 0&&this.apiCode!=null&&this.operation!=null) {
			try {
				String apiResponseType = new ApiHelper().fetchAPIResponseTypes(applicationId,apiCode,operation);
				if(apiResponseType!=null&&!apiResponseType.equals("0")){
					String [] responseTypes= apiResponseType.split(",");
					if(Arrays.asList(responseTypes).contains(String.valueOf(this.defaultValue))){
				for(String responseType : responseTypes) {
					if(Utilities.trim(responseType).equalsIgnoreCase(this.defaultValue)) {
						finalHtml += "<option value='" + Utilities.trim(responseType) + "' selected>" + Utilities.trim(responseType) +  "</option>";
					}else {
						finalHtml += "<option value='" + Utilities.trim(responseType) + "' >" + Utilities.trim(responseType) +  "</option>";
					}
				  }
				}else{
					finalHtml += "<option value='" +" " + "' >"  +  "</option>";
					for(String responseType : responseTypes) {
						if(Utilities.trim(responseType).equalsIgnoreCase(this.defaultValue)) {
							finalHtml += "<option value='" + Utilities.trim(responseType) + "' selected>" + Utilities.trim(responseType) +  "</option>";
						}else {
							finalHtml += "<option value='" + Utilities.trim(responseType) + "' >" + Utilities.trim(responseType) +  "</option>";
						}
					}
				}
			}} catch (DatabaseException e) {
				logger.error("ERROR getting function groups for application {}", this.applicationId, e);
			}
		}
		finalHtml += "</select>";
		
		getJspContext().getOut().write(finalHtml);
	}
	
}
