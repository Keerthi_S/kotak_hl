/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  FunctionGroupSelectorTag.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 24-Oct-2017           Sriram Sridharan          Newly Added For TENJINCG-396
 * 
 */

package com.ycs.tenjin.core.tags;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.util.Utilities;

public class FunctionGroupSelectorTag extends SimpleTagSupport {
	private static final Logger logger = LoggerFactory.getLogger(FunctionGroupSelectorTag.class);
	
	private String applicationSelector;
	private int applicationId;
	private boolean loadFunctions;
	private String functionSelector;
	private String defaultValue;
	private String cssClass;
	private String id;
	private String name;
	private boolean showAllGroupsOption = true;
	private String mandatory;
	private String title;
	
	
	
	
	public String getMandatory() {
		return mandatory;
	}
	public void setMandatory(String mandatory) {
		this.mandatory = mandatory;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public boolean isShowAllGroupsOption() {
		return showAllGroupsOption;
	}
	public void setShowAllGroupsOption(boolean showAllGroupsOption) {
		this.showAllGroupsOption = showAllGroupsOption;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getApplicationSelector() {
		return applicationSelector;
	}
	public void setApplicationSelector(String applicationSelector) {
		this.applicationSelector = applicationSelector;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public boolean isLoadFunctions() {
		return loadFunctions;
	}
	public void setLoadFunctions(boolean loadFunctions) {
		this.loadFunctions = loadFunctions;
	}
	public String getFunctionSelector() {
		return functionSelector;
	}
	public void setFunctionSelector(String functionSelector) {
		this.functionSelector = functionSelector;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public String getCssClass() {
		return cssClass;
	}
	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		String finalHtml = "<select mandatory='"+this.mandatory+"' title='"+this.title+"' id='"+ this.id +"' name='"+ this.name +"' class='" + this.cssClass + " functionGroupSelector' data-application-selector='" + this.applicationSelector + "' data-load-functions='" + this.loadFunctions + "' data-function-selector='" + this.functionSelector + "' >";
		if(this.showAllGroupsOption) {
			finalHtml += "<option value='All'>-- Select One --</option>";
		}
		/*finalHtml += "<option value='Ungrouped'>Ungrouped</option>";*/
		if(this.applicationId > 0) {
			try {
				List<String> functionGroups = new  AutHandler().getFunctionGroups(this.applicationId);
				for(String group : functionGroups) {
					if(Utilities.trim(group).equalsIgnoreCase(this.defaultValue)) {
						finalHtml += "<option value='" + Utilities.escapeXml(Utilities.trim(group)) + "' selected>" + Utilities.escapeXml(Utilities.trim(group)) +  "</option>";
					}else {
						finalHtml += "<option value='" + Utilities.escapeXml(Utilities.trim(group)) + "' >" + Utilities.escapeXml(Utilities.trim(group)) +  "</option>";
					}
				}
			} catch (DatabaseException e) {
				logger.error("ERROR getting function groups for application {}", this.applicationId, e);
			}
		}
		
		finalHtml += "</select>";
		
		getJspContext().getOut().write(finalHtml);
	}
}
