/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExtractorServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION 
22-Sep-2017			sameer gupta			For new adapter spec
19-01-2018			Pushpalatha				TenjinCG-581
02-01-2018			Prem					For validating adapter license
24-06-2019          Padmavathi              for license 
18-11-2020			Ashiki					TENJINCG-1213
*/

package com.ycs.tenjin.servlet.adapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.google.gson.Gson;
import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.MaxActiveUsersException;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.BridgeProcessor;
import com.ycs.tenjin.bridge.exceptions.TestDataException;
import com.ycs.tenjin.bridge.oem.api.datahandler.ApiTestDataHandler;
import com.ycs.tenjin.bridge.oem.api.datahandler.ApiTestDataHandlerImpl;
import com.ycs.tenjin.bridge.pojo.LearnerProgress;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.run.LearnerGateway;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class ApiLearnerServlet
 */
//@WebServlet("/ApiLearnerServlet")
public class ApiLearnerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private static final Logger logger = LoggerFactory.getLogger(ApiLearnerServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApiLearnerServlet() {
        super();
        
    }

    public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		@Override
		public boolean checkAccess(HttpServletRequest request) {
			if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
				return true;
			}
			return false;
		}  
	};
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String txn = Utilities.trim(request.getParameter("param"));
		
		if(txn.equalsIgnoreCase("learner_result")) {
		//Added by Padmavathi for T25IT-308 starts	
		String callback=request.getParameter("callback");
		//Added by Padmavathi for T25IT-308 ends	
			String rId = request.getParameter("runid");
			Map<String, Object> map = new HashMap<String, Object>();
			String redirect = "";
			try {
				TestRun run = new RunHelper().hydrateRun(Integer.parseInt(rId));
				int appId = run.getAppId();
				Aut aut= new AutHelper().hydrateAut(appId);
				Api api = new ApiHelper().hydrateApi(appId, run.getApi().getCode());
				map.put("LRNR_AUT_ID", Integer.toString(aut.getId()));
				map.put("LRNR_AUT", aut.getName());
				map.put("LRNR_API", api);
				//Fix for T25IT-300
				/*if(run.getCompletionStatus().equalsIgnoreCase(AdapterTask.IN_PROGRESS)){*/
				if(run.getCompletionStatus().equalsIgnoreCase(BridgeProcess.IN_PROGRESS) || run.getCompletionStatus().equalsIgnoreCase(BridgeProcess.NOT_STARTED)){
				//Fix for T25IT-300		
					LearnerGateway gateway = new LearnerGateway(false);
					gateway.setTestRun(run);
					
					map.put("STATUS", "SUCCESS");
					request.getSession().setAttribute("API_LRNR_GATEWAY", gateway);
					redirect="api_learner_progress.jsp";
				}else{
					map.put("TEST_RUN", run);
					map.put("STATUS", "SUCCESS");
					//Added by Padmavathi for T25IT-308 starts	
					map.put("callback", callback);
					//Added by Padmavathi for T25IT-308 ends	
					redirect="api_learner_result.jsp";
				}
				
			} catch (NumberFormatException e) {
				
				logger.error("Invalid Run id {}", rId, e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "An internal error occurred. Please contact Tenjin Administrator.");
			} catch (DatabaseException e) {
				
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "An internal error occurred. Please contact Tenjin Administrator.");
			} catch (TenjinServletException e) {
				
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "An internal error occurred. Please contact Tenjin Administrator.");
			}
			
			request.getSession().setAttribute("API_LRNR_MAP", map);
			response.sendRedirect(redirect);
		}
		
		if(txn.equalsIgnoreCase("learner_progress")) {
			
			String rId = request.getParameter("runid");
			request.getSession().setAttribute("run_id", rId);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("api_learner_progress.jsp");
			dispatcher.forward(request, response);
			
		} else if(txn.equalsIgnoreCase("download_data_template")){
			
			String apiCode = request.getParameter("api");
			String app = request.getParameter("a");
			String mandatoryFlag = request.getParameter("type");
			String operation = request.getParameter("op");
			
			int appId = 0;
			
			try{
				appId = Integer.parseInt(app);
			}catch(NumberFormatException e){
				logger.error("Invalid App ID [{}]", app, e);
			}
			String folderPath = request.getServletContext().getRealPath("/");
			Utilities.checkForDownloadsFolder(folderPath);
			/*Changed by Pushpalatha for TENJINCG-581 starts*/
			folderPath = folderPath +File.separator+ "Downloads";
			/*Changed by Pushpalatha for TENJINCG-581 ends*/
			String retData = "";
			JSONObject json = new JSONObject();
			try {
				try {
					String templatePath = this.generateTestDataTemplate(appId, apiCode, operation, mandatoryFlag, folderPath);
					json.put("status", "SUCCESS");
					json.put("message", "");
					/*Changed by Pushpalatha for TENJINCG-581 starts*/
					json.put("path", "Downloads"+File.separator + templatePath);
					/*Changed by Pushpalatha for TENJINCG-581 ends*/
				} catch (TestDataException e) {
					
					json.put("status","ERROR");
					json.put("message", e.getMessage());
				}finally{
					retData = json.toString();
				}
			} catch (JSONException e) {
				
				logger.error("ERROR occurred while generating JSON after template generation", e);
				retData = "An internal error occurred. Please contact Tenjin Support";
			}
			
			response.getWriter().write(retData);
		} else if(txn.equalsIgnoreCase("check_progress")) {
			
			MDC.put("logFileName", "api_progress_check");
			
			
			String rId = request.getParameter("runId");
			/*logger.info("Entered check_progress for Run ID [{}]", rId);*/
			logger.debug("Entered check_progress for Run ID [{}]", rId);
			MDC.remove("logFileName");
			int runId = Integer.parseInt(rId);
			String status ="";
			
			LearnerProgress progress = null;
			
			try {
				
				progress = (LearnerProgress) CacheUtils.getObjectFromRunCache(runId);
				
			} catch (TenjinConfigurationException e) {
				
				logger.error("ERROR getting status from cahce. Please ensure the RUN_CACHE_NAME property is defined in the Tenjin Configuration", e);
			} catch(NullPointerException e) {
				logger.error("Status not available yet");
				status = "Please Wait...";
			}
			
			String ret = "";
			
			try {
				JSONObject json = new JSONObject();
				
				if(progress != null){
					String jsonString = new Gson().toJson(progress);
					json.put("status", "success");
					json.put("progress", new JSONObject(jsonString));
				}else{
					logger.error("Could not get learning progress from cache.");
					json.put("status", "error");
					json.put("message", "Could not load progress. Please contact Tenjin Support.");
				}
				ret = json.toString();
			} catch (JSONException e) {
				
				logger.error("JSONException occurred", e);
				ret = "{status:error,message:An internal error occurred while refreshing status. Please contact Tenjin support.}";
			}
			
			response.getWriter().write(ret);
		}
		/*Added by Ashiki for TENJINCG-1213 starts*/
		if(txn.equalsIgnoreCase("apicode_learner_progress")) {
			
			String rId = request.getParameter("runid");
			
			request.getSession().setAttribute("run_id", rId);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("apicode_learner_progress.jsp");
			dispatcher.forward(request, response);
			
		}if(txn.equalsIgnoreCase("apicode_learner_result")) {

			String callback=request.getParameter("callback");
				String rId = request.getParameter("runid");
				Map<String, Object> map = new HashMap<String, Object>();
				String redirect = "";
				try {
					TestRun run = new RunHelper().hydrateRun(Integer.parseInt(rId));
					int appId = run.getAppId();
					Aut aut= new AutHelper().hydrateAut(appId);
					List<Api> apis=new ArrayList<Api>();
					for (Api api : run.getApis()) {
						Api api1 = new ApiHelper().hydrateApi(appId, api.getCode());
						apis.add(api1);
					}
					
					map.put("LRNR_AUT_ID", Integer.toString(aut.getId()));
					map.put("LRNR_AUT", aut.getName());
					map.put("LRNR_API", apis);
					if(run.getCompletionStatus().equalsIgnoreCase(BridgeProcess.IN_PROGRESS) || run.getCompletionStatus().equalsIgnoreCase(BridgeProcess.NOT_STARTED)){
						LearnerGateway gateway = new LearnerGateway(false);
						gateway.setTestRun(run);
						map.put("TEST_RUN", run);
						map.put("STATUS", "SUCCESS");
						request.getSession().setAttribute("API_LRNR_GATEWAY", gateway);
						redirect="apicode_learner_progress.jsp";
					}else{
						map.put("TEST_RUN", run);
						map.put("STATUS", "SUCCESS");
						map.put("callback", callback);
						redirect="apicode_learner_result.jsp";
					}
					
				} catch (NumberFormatException e) {
					logger.error("Invalid Run id {}", rId, e);
					map.put("STATUS", "ERROR");
					map.put("MESSAGE", "An internal error occurred. Please contact Tenjin Administrator.");
				} catch (DatabaseException e) {
					map.put("STATUS", "ERROR");
					map.put("MESSAGE", "An internal error occurred. Please contact Tenjin Administrator.");
				} catch (TenjinServletException e) {
					map.put("STATUS", "ERROR");
					map.put("MESSAGE", "An internal error occurred. Please contact Tenjin Administrator.");
				}
				
				request.getSession().setAttribute("API_LRNR_MAP", map);
				response.sendRedirect(redirect);
			
		}
		/*Added by Ashiki for TENJINCG-1213 end*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String reqType = Utilities.trim(request.getParameter("requesttype"));/*Added by Pushpa for Desjardins VAPT Fix starts*/
		 Authorizer authorizer=new Authorizer();
			if(reqType.equalsIgnoreCase("initlearner") || reqType.equalsIgnoreCase("learn")) {
				if(!authorizer.hasAccess(request)){
					response.sendRedirect("noAccess.jsp");
					return;
				}
			}
			/*Added by Pushpa for Desjardins VAPT Fix ends*/
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		
		if(reqType.equalsIgnoreCase("initlearner")){
			String app = request.getParameter("lstAppId");
			String scriptList = request.getParameter("operationsList");
			String apiCode = request.getParameter("txtApiCode");
			String message = "";
			
			String[] oArray = scriptList.split(",");
			List<ApiOperation> operationsToLearn = new ArrayList<ApiOperation>();
			for(String op:oArray) {
				ApiOperation operation =new ApiOperation();
				operation.setName(op);
				operationsToLearn.add(operation);
			}
			
			boolean okToLearn = true;
			try {
				LearnerGateway gateway = new LearnerGateway(Integer.parseInt(app), apiCode, operationsToLearn, tjnSession.getUser().getId());
				gateway.performAllValidationsForApiLearning();
				
				request.getSession().setAttribute("API_LRNR_GATEWAY", gateway);
				
				Map<String, Object> map = new HashMap<String, Object> ();
				map.put("LRNR_AUT_ID", app);
				map.put("LRNR_AUT", gateway.getAut().getName());
				map.put("LRNR_API", gateway.getApi());
				request.getSession().setAttribute("API_LRNR_MAP", map);
				
			} catch (NumberFormatException e) {
				
				logger.error("Invalid Application ID [{}]", app);
				message = "Invalid Application selected. Please try again.";
				okToLearn = false;
			} catch (TenjinServletException e) {
				
				message = e.getMessage();
				okToLearn = false;
			}
			
			if(okToLearn) {
				RequestDispatcher dispatcher = request.getRequestDispatcher("api_learner_selection.jsp");
				request.getSession().removeAttribute("LRNR_INIT_STATUS");
				request.getSession().removeAttribute("LRNR_INIT_MESSAGE");
				dispatcher.forward(request, response);
			}else{
				request.getSession().setAttribute("LRNR_INIT_STATUS", "ERROR");
				request.getSession().setAttribute("LRNR_INIT_MESSAGE", message);
				
				Map<String, Object> scrMap = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
				if(scrMap == null ) {
					scrMap = new HashMap<String, Object>();
				}
				scrMap.put("STATUS", "ERROR");
				scrMap.put("MESSAGE", message);
				
				request.getSession().removeAttribute("SCR_MAP");
				request.getSession().setAttribute("SCR_MAP", scrMap);
				
				response.sendRedirect("editapidetails.jsp");
			}
			
		} else if(reqType.equalsIgnoreCase("learn")) {
			LearnerGateway gateway = (LearnerGateway) request.getSession().getAttribute("API_LRNR_GATEWAY");
			String learnType = Utilities.trim(request.getParameter("learnType"));
			
			logger.info("Setting learn type to {}", learnType);
			gateway.setApiLearnType(learnType);
			
			logger.info("Initializing Learner...");
			String message = "";
			boolean okToLearn = true;
			try {
			/*Added by Padmavathi for license starts*/
				gateway.validateAdapterLicense();
				/*Added by Padmavathi for license ends*/
				gateway.createRun();
				/*Added by Prem for validating adapter license start*/
			} catch (TenjinServletException | MaxActiveUsersException | LicenseInactive  e) {
				/*Added by Prem for validating adapter license start*/
				okToLearn = false;
				message = e.getMessage();
			}
			
			if(okToLearn){
				
				BridgeProcessor handler = new BridgeProcessor(gateway.getManifest());
				request.getSession().removeAttribute("API_LRNR_GATEWAY");
				request.getSession().setAttribute("API_LRNR_GATEWAY", gateway);
				
				handler.createTask();
				request.getSession().setAttribute("LRNR_START_TIME", System.currentTimeMillis());
				handler.runTask();
				logger.info("Learner Started");
				response.sendRedirect("ApiLearnerServlet?param=learner_progress&runid=" + gateway.getTestRun().getId());
			}else{
				request.getSession().setAttribute("LRNR_INIT_STATUS", "ERROR");
				request.getSession().setAttribute("LRNR_INIT_MESSAGE", message);
				response.sendRedirect("api_learner_selection.jsp");
			}
		}
		/*Added by Ashiki for TENJINCG-1213 starts*/
		else if(reqType.equalsIgnoreCase("init_Api_learn")) {
			String appId = request.getParameter("appId");;
			String apiCodeList = request.getParameter("apiList");
			
			String message = "";
			
			boolean okToLearn = true;
			try {
				LearnerGateway gateway = new LearnerGateway(Integer.parseInt(appId), apiCodeList, tjnSession.getUser().getId());
				gateway.performApiValidations();
				request.getSession().setAttribute("API_LRNR_GATEWAY", gateway);
				
				Map<String, Object> map = new HashMap<String, Object> ();
				map.put("LRNR_AUT_ID", appId);
				map.put("LRNR_AUT", gateway.getAut().getName());
				map.put("LRNR_API", gateway.getApi_learn());
				map.put("LRNR_API_Type", gateway.getApiType());
				request.getSession().setAttribute("API_LRNR_MAP", map);
			} catch (NumberFormatException e) {
				logger.error("Invalid Application ID [{}]", appId);
				message = "Invalid Application selected. Please try again.";
				okToLearn = false;
			} catch (TenjinServletException e) {
				message = e.getMessage();
				okToLearn = false;
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			}
			if(okToLearn) {
				RequestDispatcher dispatcher = request.getRequestDispatcher("apicode_learner_selection.jsp");
				request.getSession().removeAttribute("LRNR_INIT_STATUS");
				request.getSession().removeAttribute("LRNR_INIT_MESSAGE");
				dispatcher.forward(request, response);
			}else{
				request.getSession().setAttribute("LRNR_INIT_STATUS", "ERROR");
				request.getSession().setAttribute("LRNR_INIT_MESSAGE", message);
				
				Map<String, Object> scrMap = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
				if(scrMap == null ) {
					scrMap = new HashMap<String, Object>();
				}
				scrMap.put("STATUS", "ERROR");
				scrMap.put("MESSAGE", message);
				
				request.getSession().removeAttribute("SCR_MAP");
				request.getSession().setAttribute("SCR_MAP", scrMap);
				
				response.sendRedirect("apilist.jsp");
			}
			
		}else if(reqType.equalsIgnoreCase("apicode_learn")) {
			LearnerGateway gateway = (LearnerGateway) request.getSession().getAttribute("API_LRNR_GATEWAY");
			String learnType = Utilities.trim(request.getParameter("learnType"));
			
			logger.info("Setting learn type to {}", learnType);
			gateway.setApiLearnType(learnType);
			
			logger.info("Initializing Learner...");
			String message = "";
			boolean okToLearn = true;
			try {
				gateway.validateAdapterLicense();
				gateway.createRun();
			} catch (TenjinServletException | MaxActiveUsersException | LicenseInactive  e) {
				okToLearn = false;
				message = e.getMessage();
			}
			
			if(okToLearn){
				
				BridgeProcessor handler = new BridgeProcessor(gateway.getManifest());
				request.getSession().removeAttribute("API_LRNR_GATEWAY");
				request.getSession().setAttribute("API_LRNR_GATEWAY", gateway);
				
				handler.createTask();
				request.getSession().setAttribute("LRNR_START_TIME", System.currentTimeMillis());
				handler.runTask();
				logger.info("Learner Started");
				response.sendRedirect("ApiLearnerServlet?param=apicode_learner_progress&runid=" + gateway.getTestRun().getId());
			}else{
				request.getSession().setAttribute("LRNR_INIT_STATUS", "ERROR");
				request.getSession().setAttribute("LRNR_INIT_MESSAGE", message);
				response.sendRedirect("api_learner_selection.jsp");
			}
		}
		/*Added by Ashiki for TENJINCG-1213 end*/
		
	}
	
	private String generateTestDataTemplate(int appId, String apiCode, String operation, String mandatoryFlag, String folderPath) throws TestDataException{
		boolean mandatoryOnly = false;
		
		if(Utilities.trim(mandatoryFlag).equalsIgnoreCase("M")){
			mandatoryOnly = true;
		}
		
		
		ApiTestDataHandler handler = new ApiTestDataHandlerImpl();
		
		logger.info("Template manifest --> App [{}], API [{}], Operation [{}], Mandatory [{}]", appId, apiCode, operation,  mandatoryOnly);
		handler.generateTemplate(appId, apiCode, operation, folderPath, apiCode + "_" + operation + "_TTD.xlsx", mandatoryOnly);
		logger.info("Template generation successful");
		return apiCode + "_" + operation + "_TTD.xlsx";
		
		
	}

}
