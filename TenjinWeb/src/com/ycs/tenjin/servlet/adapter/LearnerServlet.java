/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LearnerServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION 
22-Sep-2017			sameer gupta			For new adapter spec
19-Jan-2018			Pushpalatha				TenjinCG-581
26-Nov 2018			Shivam	Sharma			 TENJINCG-904
02-Jan-2019			Prem					for validating adapter license
19-03-2019			Preeti					TNJN27-12
20-Feb 2019			Sahana					pCloudy
24-04-2019			Prem					TNJNR2-6
11-04-2019			Ashiki					TENJINCG-1032
24-06-2019          Padmavathi              for license 
26-08-2019			Ashiki					TENJINCG-1104
09-10-2019			Pushpalatha				TJN252-7
05-02-2020			Roshni					TENJINCG-1168
31-03-2021          Paneendra               TENJINCG-1267
 */
package com.ycs.tenjin.servlet.adapter;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.MaxActiveUsersException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.BridgeProcessor;
import com.ycs.tenjin.bridge.exceptions.TestDataException;
import com.ycs.tenjin.bridge.oem.gui.datahandler.GuiDataHandler;
import com.ycs.tenjin.bridge.oem.gui.datahandler.GuiDataHandlerImpl;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.AutsHelper;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.DeviceHelper;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.device.RegisteredDevice;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.LearnerHandler;
import com.ycs.tenjin.pcloudy.Pcloudy;
import com.ycs.tenjin.pcloudy.PcloudyProcessor;
import com.ycs.tenjin.run.LearnerGateway;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class LearnerServlet
 */
//@WebServlet("/LearnerServlet")
public class LearnerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(LearnerServlet.class);   
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LearnerServlet() {
		super();
		
	}
	public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		@Override
		public boolean checkAccess(HttpServletRequest request) {
			if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
				return true;
			}
			return false;
		}  
	};

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String txn = Utilities.trim(request.getParameter("param"));
		Authorizer authorizer=new Authorizer();
		if(txn.equalsIgnoreCase("") || txn.equalsIgnoreCase("download_data_template") || txn.equalsIgnoreCase("learner_progress") ) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}

		if(txn.equalsIgnoreCase("download_data_template")){

			String functionCode = request.getParameter("m");
			String app = request.getParameter("a");
			String templateType = request.getParameter("templatetype");
			String mandatoryFlag = request.getParameter("type");


			int appId = 0;

			try{
				appId = Integer.parseInt(app);
			}catch(NumberFormatException e){
				logger.error("Invalid App ID [{}]", app, e);
			}
			String folderPath = request.getServletContext().getRealPath("/");
			Utilities.checkForDownloadsFolder(folderPath);
			/*Changed by Pushpalatha for TENJINCG-581 starts*/
			folderPath = folderPath + File.separator+"Downloads";
			/*Changed by Pushpalatha for TENJINCG-581 ends*/
			String retData = "";
			JSONObject json = new JSONObject();
			try {
				try {
					String templatePath = this.generateTestDataTemplate(appId, functionCode, templateType, mandatoryFlag, folderPath);
					json.put("status", "SUCCESS");
					json.put("message", "");
					/*Changed by Pushpalatha for TENJINCG-581 starts*/
					json.put("path", "Downloads"+File.separator + templatePath);
					/*Changed by Pushpalatha for TENJINCG-581 ends*/
				} catch (TestDataException e) {
					
					json.put("status","ERROR");
					json.put("message", e.getMessage());
				}finally{
					retData = json.toString();
				}
			} catch (JSONException e) {
				
				logger.error("ERROR occurred while generating JSON after template generation", e);
				retData = "An internal error occurred. Please contact Tenjin Support";
			}

			response.getWriter().write(retData);
		} else if(txn.equalsIgnoreCase("learner_progress")){

			request.getSession().removeAttribute("LRNR_RUN");
			request.getSession().removeAttribute("LRNR_CURRENT_FUNC");
			request.getSession().removeAttribute("LRNR_PROGRESS");

			String rId = request.getParameter("runid");
			TestRun run = null;
			/*Added by Preeti for TNJN27-12 starts*/
			String callback = request.getParameter("callback");
			/*Added by Preeti for TNJN27-12 ends*/
			Connection conn=null;
			try {
				run = new RunHelper().hydrateLearningRun(Integer.parseInt(rId));
				if (request.getSession().getAttribute("LRNR_START_TIME") != null) {
					long runStartTime = (long) request.getSession()
							.getAttribute("LRNR_START_TIME");
					run.setStartTimeStamp(new Date(runStartTime));
					run.setElapsedTime(Utilities.calculateElapsedTime(
							runStartTime, System.currentTimeMillis()));
				}
				double progressPercentage = ((double)run.getExecutedTests()/(double)run.getTotalTests())*100;
				double roundOff = Math.round(progressPercentage * 100.0) / 100.0;
				int rWithoutDecimals = (int) roundOff;
				String roundOffString = Integer.toString(rWithoutDecimals);
				request.getSession().setAttribute("LRNR_PROGRESS", roundOffString);
				Module cFunc = null;
				for(Module function:run.getFunctions()){
					if(function.getLastSuccessfulLearningResult() != null && function.getLastSuccessfulLearningResult().getLearnStatus().equalsIgnoreCase("learning")){
						cFunc = function;
						//break;
					}

					else if(function.getLastSuccessfulLearningResult() != null && !function.getLastSuccessfulLearningResult().getLearnStatus().equalsIgnoreCase("queued")){
						String elapsedTime = "";
						if(function.getLastSuccessfulLearningResult().getStartTimestamp() != null && function.getLastSuccessfulLearningResult().getEndTimestamp() != null){
							elapsedTime = Utilities.calculateElapsedTime(function.getLastSuccessfulLearningResult().getStartTimestamp().getTime(), function.getLastSuccessfulLearningResult().getEndTimestamp().getTime());
							function.getLastSuccessfulLearningResult().setElapsedTime(elapsedTime);
						}
					}
				}
				/*Added by sahana for pCloudy: Starts*/
				RegisteredDevice device=null;
				/*Modified by paneendra for TENJINCG-1267 starts*/
			    conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				TestRun deviceRun=new RunHelper().hydrateRun(conn, run.getId());
				if(deviceRun.getDeviceFarmFlag()!=null && deviceRun.getDeviceFarmFlag().equalsIgnoreCase("Y")){
					Pcloudy objPcloudy=new PcloudyProcessor().getPcloudyCredentials(deviceRun.getMachine_ip());
					device=new PcloudyProcessor(objPcloudy).deviceById(deviceRun.getDeviceRecId());
				}else{
					device=new DeviceHelper().hydrateDevice(deviceRun.getDeviceRecId());	
				}	
				
				/*Modified by Prem for TNJNR2-6 : Starts*/
				if(device!=null)
				{
					request.getSession().setAttribute("DEVICE", device);
					run.setDeviceRecId((deviceRun.getDeviceRecId()));
				}
				run.setDeviceFarmFlag(deviceRun.getDeviceFarmFlag());
				/*Modified by Prem for TNJNR2-6 :  Ends */
				/*Added by sahana for pCloudy: ends*/
				request.getSession().setAttribute("LRNR_CURRENT_FUNC", cFunc);
				request.getSession().setAttribute("LRNR_RUN", run);
				/*Added by Preeti for TNJN27-12 starts*/
				request.getSession().setAttribute("callback", callback);
				/*Added by Preeti for TNJN27-12 ends*/
			} catch (NumberFormatException e) {
				
				logger.error("NumberFormat --> ", e);
			} catch (DatabaseException e) {
				
				logger.error(e.getMessage(),e);
			}/*Added by paneendra for TENJINCG-1267 starts*/
			finally {
				DatabaseHelper.close(conn);
			}
			/*Added by paneendra for TENJINCG-1267 ends*/

			RequestDispatcher dispatcher = request.getRequestDispatcher("live_learner_progress.jsp");
			dispatcher.forward(request, response);

		}

	}

	/****************************
	 * Entire doPost() method changed by Sriram for Req#TJN_23_18
	 */
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String reqType = Utilities.trim(request.getParameter("requesttype"));
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		Authorizer authorizer=new Authorizer();
		if(reqType.equalsIgnoreCase("") || reqType.equalsIgnoreCase("initlearner") || reqType.equalsIgnoreCase("learn") || reqType.equalsIgnoreCase("reLearn")) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		if(reqType.equalsIgnoreCase("initlearner")){
			logger.info("Initiatinig Learner");
			
			/* added by Roshni for TENJINCG-1168 starts */
			Connection conn=null;
			try {
				conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			} catch (DatabaseException e2) {
				
				e2.printStackTrace();
			}
			LearnerHandler handler=new LearnerHandler(conn);
			
		/* added by Roshni for TENJINCG-1168 ends */
			String app = request.getParameter("lstLrnrApplication");

			String scriptList = request.getParameter("moduleList");

			String message = "";

			logger.info("Script List --> [{}]", scriptList);
			List<String> funcs = new ArrayList<String>();
			boolean okToLearn = true;
			try{
				String[] mSplit = scriptList.split(";");
				for(String funcString:mSplit){
					String[] sSplit= funcString.split("\\|");
					String functionCode = sSplit[0];
					funcs.add(functionCode);
				}
				
				/*Added by Pushpalatha for TJN252-7 starts*/
				/*new ModuleHandler().validateFunc(funcs,Integer.valueOf(app));*/
				handler.validateFunc(funcs, Integer.valueOf(app));
				
			}catch(RequestValidationException e) {
				message =e.getMessage();
				okToLearn = false;
				/*Added by Pushpalatha for TJN252-7 ends*/
			}catch(Exception e){
				logger.error("ERROR getting all function codes");
				//status = "ERROR";
				message ="Could not initialize learner due to an internal error. Please contact Tenjin Support";
				okToLearn = false;
			}
			
			LearnerGateway gateway = null;
			if(okToLearn){
				try {
					gateway = new LearnerGateway(Integer.parseInt(app), funcs, tjnSession.getUser().getId());
					logger.info("Performing Initial validation of learning information");
					gateway.performInitialValidation();
				} catch (NumberFormatException e) {
					
					logger.error("Invalid application [{}]", app, e);
					//status = "ERROR";
					message = "Could not initialize learner due to an internal error. Please contact Tenjin Support.";
					okToLearn = false;
				} catch (TenjinServletException e) {
					
					//status = "ERROR";
					message = e.getMessage();
					okToLearn = false;
				}
			}

			if(okToLearn){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("LRNR_AUT_ID", app);
				map.put("LRNR_AUT", gateway.getAut().getName());
				map.put("LRNR_FUNCS", gateway.getFunctions());
				/*	added by Sahana for Mobility:Starts*/
				try {
					/*int appType=new AutHandler().getAutType(Integer.valueOf(app));*/
					int appType=new AutsHelper().getAutType(Integer.valueOf(app),conn);
					map.put("LRNR_AUT_TYPE", appType);
				} catch (NumberFormatException e1) {
					logger.error("ERROR fetching AUT for User [{}], application [{}]", tjnSession.getUser().getId(),  gateway.getAut().getName());
					okToLearn = false;
					message = "COuld not fetch application details for " +  gateway.getAut().getName() + " due to an internal error. Please contact Tenjin Support";
				} catch (DatabaseException e1) {
					logger.error("ERROR fetching AUT for User [{}], application [{}]", tjnSession.getUser().getId(),  gateway.getAut().getName());
					okToLearn = false;
					message = "COuld not fetch application details for " +  gateway.getAut().getName() + " due to an internal error. Please contact Tenjin Support";
				}
				/*	added by Sahana for Mobility:ends*/

				try {
					logger.info("Fetching credentials for [{}] for Tenjin user [{}]", gateway.getAut().getName(), tjnSession.getUser().getId());
					/*ArrayList<String> userAutCreds = new AutHelper().hydrateAllAutLoginTypes(tjnSession.getUser().getId(), gateway.getAut().getId());*/
					ArrayList<String> userAutCreds = new AutHelper().hydrateAllAutLoginTypes(tjnSession.getUser().getId(), gateway.getAut().getId(),conn);
					if(userAutCreds != null && userAutCreds.size() > 0){
						map.put("OWN_CREDS", userAutCreds);
					}else{
						okToLearn = false;
						message = "You have not defined any login credentials for " +  gateway.getAut().getName() + ". Please define Credentials in AUT User Maintenance";
					}
				} catch (NumberFormatException e) {
					logger.error("ERROR fetching AUT Credentials for User [{}], application [{}]", tjnSession.getUser().getId(),  gateway.getAut().getName());
					okToLearn = false;
					message = "COuld not fetch Login credentials for " +  gateway.getAut().getName() + " due to an internal error. Please contact Tenjin Support";
				} catch (DatabaseException e) {
					logger.error("ERROR fetching AUT Credentials for User [{}], application [{}]", tjnSession.getUser().getId(),  gateway.getAut().getName());
					okToLearn = false;
					message = "COuld not fetch Login credentials for " +  gateway.getAut().getName() + " due to an internal error. Please contact Tenjin Support";
				}

				try{
					logger.info("Loading all registered clients");
					List<RegisteredClient> clients = new ClientHelper().hydrateAllClients(conn);
					map.put("LRNR_CLIENTS", clients);
					/*Added by paneendra for TENJINCG-1267 starts*/
					try{
						if(conn!=null)
						conn.close();
					}catch(Exception e){}
					/*Added by paneendra for TENJINCG-1267 ends*/
				}catch(Exception e){
					logger.error("ERROR occurred while fetching list of registered clients", e);
				}

				logger.info("Learning validation complete");
				request.getSession().setAttribute("LRNR_GATEWAY", gateway);
				request.getSession().setAttribute("LRNR_MAP", map);
				RequestDispatcher dispatcher = request.getRequestDispatcher("learnerprogress.jsp?txnMode=");
				request.getSession().removeAttribute("LRNR_INIT_STATUS");
				request.getSession().removeAttribute("LRNR_INIT_MESSAGE");
				/*Added by Pushpalatha for TJN252-7 starts*/
				request.getSession().setAttribute("funList",scriptList);
				request.getSession().setAttribute("app",app);
				/*Added by Pushpalatha for TJN252-7 ends*/
				dispatcher.forward(request, response);
			} else{
				/*Added by Pushpalatha for TJN252-7 starts*/
				response.sendRedirect("FunctionServlet?appId=" +app+"&message="+message+"&status=ERROR");
				/*Added by Pushpalatha for TJN252-7 ends*/
			}

		}else if(reqType.equalsIgnoreCase("learn")){
			//added by shivam sharma for  TENJINCG-904 starts
			String clientName = request.getParameter("clientName");
			//added by shivam sharma for  TENJINCG-904 starts
			String browser = request.getParameter("lstBrowserType");
			String autLoginType = request.getParameter("lstAutCred");
			/*Added by Pushpalatha for TJN252-7 starts*/
			String scriptList = request.getParameter("funcList");
			String app = request.getParameter("app");
			/*Added by Pushpalatha for TJN252-7 end*/
			/*Added by Ashiki for TENJINCG-1104 starts*/
			String moduleCode = request.getParameter("moduleCode");
			String appId = request.getParameter("appId");
			String message = "";
			boolean okToLearn = true;
			List<String> funcs = new ArrayList<String>();
			try {
			JSONArray jsonArray = new JSONArray(moduleCode);
			for(int i=0; i<jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				funcs.add(json.getString("funCode"));
			}
			
			}
			catch (Exception e) {
				logger.error("ERROR getting all function codes");
				message ="Could not initialize learner due to an internal error. Please contact Tenjin Support";
				okToLearn = false;
			}
			/*Added by Ashiki for TENJINCG-1104 ends*/
			
			//TENJINCG-731 - By Sameer, Sahana - Mobility - Start
			//deviceRecId = request.getParameter("listDevice");
			String deviceRecId=null;
			if(request.getParameter("listDevice")!=null)
				deviceRecId = request.getParameter("listDevice");
			else
				deviceRecId="-1";
			
			
			/*Added by Ashiki for TENJINCG-1104 ends*/
			LearnerGateway gateway=null;
			try {
				gateway = new LearnerGateway(Integer.parseInt(appId), funcs, tjnSession.getUser().getId());
			/*Added by Ashiki for TENJINCG-1104 ends*/
				gateway.setAutLoginType(autLoginType);
				gateway.setBrowserSelection(browser);
				gateway.setRegClientName(clientName);
				//TENJINCG-731 - By Sameer - Mobility - Start
				gateway.setDeviceRecId(deviceRecId);
				logger.info("Validating learning settings");
				/*Added by Ashiki for TENJINCG-1104 starts*/
				gateway.performInitialValidation();
				/*Added by Ashiki for TENJINCG-1104 ends*/
				gateway.performFinalValidation();
				/*Added by Padmavathi for license starts*/
				gateway.validateAdapterLicense();
				/*Added by Padmavathi for license ends*/
				logger.info("Initializing Learner...");
				
				
				/*Modified by Pushpalatha for TJN252-7 starts*/
				List<String> functions = new ArrayList<String>();
				try{
					String[] mSplit = scriptList.split(";");
					for(String funcString:mSplit){
						String[] sSplit= funcString.split("\\|");
						String functionCode = sSplit[0];
						functions.add(functionCode);
					}
					
				}catch (Exception e) {
					
				}
				
				logger.info("Initializing Learner...");
				gateway.createRun1(functions,Integer.valueOf(app));
				
				
				
				/*Added by Prem for validating adapter license start*/
			} catch (TenjinServletException | MaxActiveUsersException | LicenseInactive e) {
				/*Added by Prem for validating adapter license Ends*/
				okToLearn = false;
				message = e.getMessage();
			} catch (NumberFormatException e) {
				
				logger.error("Error ", e);
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			} catch (RequestValidationException e) {
				
				logger.error("Error ", e);
			}

			if(okToLearn){

				BridgeProcessor handler = new BridgeProcessor(gateway.getManifest());
				request.getSession().removeAttribute("LRNR_GATEWAY");
				request.getSession().setAttribute("LRNR_GATEWAY", gateway.getManifest());
				
				handler.createTask();
				request.getSession().setAttribute("LRNR_START_TIME", System.currentTimeMillis());
				handler.runTask();
				logger.info("Learner Started");
				response.sendRedirect("LearnerServlet?param=learner_progress&runid=" + gateway.getTestRun().getId());
			}else{
				request.getSession().setAttribute("LRNR_INIT_STATUS", "ERROR");
				request.getSession().setAttribute("LRNR_INIT_MESSAGE", message);
				response.sendRedirect("learnerprogress.jsp");
			}

		}/*Added by Ashiki for TENJINCG-1032 starts*/
		else if(reqType.equalsIgnoreCase("reLearn")) {
			String rId = request.getParameter("runid");
			String app = request.getParameter("app");
			String clientName = request.getParameter("clientName");
			String browser = request.getParameter("lstBrowserType");
			String autLoginType = request.getParameter("lstAutCred");
			String deviceRecId = request.getParameter("runid");
			TestRun run = null;
			boolean okToLearn = true;
			String message = "";
			LearnerGateway gateway=null;
			try {
				run = new RunHelper().hydrateLearningRun(Integer.parseInt(rId));
			
			List<Module> functions=run.getFunctions();
			List<String> funcs = new ArrayList<String>();
			for(Module module:functions) {
				funcs.add(module.getCode());
			}
			
				gateway = new LearnerGateway(Integer.parseInt(app), funcs, tjnSession.getUser().getId());
				gateway.setAutLoginType(autLoginType);
				gateway.setBrowserSelection(browser);
				gateway.setRegClientName(clientName);
				gateway.setDeviceRecId(deviceRecId);
				gateway.performInitialValidation();
				gateway.performFinalValidation();
				/*Added by Padmavathi for license starts*/
				gateway.validateAdapterLicense();
				/*Added by Padmavathi for license ends*/
				gateway.createRun();
			}  catch (TenjinServletException |MaxActiveUsersException | LicenseInactive e) {
				okToLearn = false;
				message = e.getMessage();
			} catch (DatabaseException e) {
				okToLearn = false;
				message = e.getMessage();
			} 
			
			if(okToLearn){

				BridgeProcessor handler = new BridgeProcessor(gateway.getManifest());
				request.getSession().removeAttribute("LRNR_GATEWAY");
				request.getSession().setAttribute("LRNR_GATEWAY", gateway.getManifest());
				handler.createTask();
				request.getSession().setAttribute("LRNR_START_TIME", System.currentTimeMillis());
				handler.runTask();
				logger.info("Learner Started");
				response.sendRedirect("LearnerServlet?param=learner_progress&runid=" + gateway.getTestRun().getId());
			}else{
				request.getSession().setAttribute("LRNR_INIT_STATUS", "ERROR");
				request.getSession().setAttribute("LRNR_INIT_MESSAGE", message);
				response.sendRedirect("learnerprogress.jsp");
				
			}
		}
		/*Added by Ashiki for TENJINCG-1032 ends*/
	}


	private String generateTestDataTemplate(int appId, String functionCode, String templateType, String mandatoryFlag, String folderPath) throws TestDataException{
		boolean editableOnly = true;
		boolean mandatoryOnly = false;
		if(Utilities.trim(templateType).equalsIgnoreCase("master")){
			editableOnly = false;
		}

		if(Utilities.trim(mandatoryFlag).equalsIgnoreCase("M")){
			mandatoryOnly = true;
		}


		GuiDataHandler handler = new GuiDataHandlerImpl();

		logger.info("Template manifest --> App [{}], Function [{}], Editable Only [{}], Mandatory [{}]", appId, functionCode, editableOnly, mandatoryOnly);
		handler.generateTemplate(appId, functionCode, folderPath , functionCode + "_TTD.xlsx", mandatoryOnly, editableOnly);
		logger.info("Template generation successful");
		return functionCode + "_TTD.xlsx";


	}

}
