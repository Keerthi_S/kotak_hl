/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutorServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 	CHANGED BY              DESCRIPTION
 * 31-Aug-2016			LATIEF					Requiremen#TJN_23_12
 * 13-Sep-2016			Sriram Sridharan		Added Run Button (for Req#TJN_24_01)
 * 29-Sep-2016			Sriram Sridharan		Made many changes for Req#TJN_24_05
 * 18-Nov-2016        	Nagareddy              	Ehcahe for manual field & Continue manual field
 * 06-Dec-16				Sriram					Requirement#TJN_243_11 (Run Test Step)
 * 13-06-2017			Sriram					TENJINCG-189
 * 16-06-2017			Sriram					TENJINCG-187
 * 11-Aug-2017			Roshni					T25IT-120
 * 30-Aug-2017			Roshni					T25IT-345
 * 16-Nov-2017			Sriram Sridharan		Fixed Execution Percentage Calculation
 * 08-05-2018			Preeti					TENJINCG-625
 * 22-Dec-2017		    Sahana					Mobility
 * 28-May-2018	        Pushpa					TENJINCG-386
 * 24-08-2018			Preeti					TENJINCG-732
 * 19-11-2018			Prem					TENJINCG-843
 * 26-Nov 2018			Shivam	Sharma			TENJINCG-904	
 * 22-03-2019			Preeti					TENJINCG-1018
 * 25-Jan 2019			Sahana					pCloudy
 * 24-06-2019           Padmavathi              for license
 * 13-03-2020			Sriram					TENJINCG-1196  
 * 08-04-2020			Lokanath				TENJINCG-1193
 * 11-02-2021           Paneendra				TENJINCG-1257
 * 09-04-2021           Paneendra               TENJINCG-1268
 */

package com.ycs.tenjin.servlet.adapter;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.google.gson.JsonObject;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.BridgeProcessor;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.pojo.ExecutorProgress;
import com.ycs.tenjin.bridge.pojo.TaskManifest;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.DeviceHelper;
import com.ycs.tenjin.db.ExecutionHelper;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.db.TestSetHelper;
import com.ycs.tenjin.device.RegisteredDevice;
import com.ycs.tenjin.pcloudy.Pcloudy;
import com.ycs.tenjin.pcloudy.PcloudyProcessor;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.ExecutionProgressView;
import com.ycs.tenjin.run.ExecutorGateway;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

/**
 * Servlet implementation class ExecutorServlet
 */
//@WebServlet("/ExecutorServlet")
public class ExecutorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(ExecutorServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ExecutorServlet() {
		super();
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String txn = request.getParameter("param");
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Set<String> al =null;
		if(txn != null && txn.equalsIgnoreCase("test_set_val")){
			String ret = "";
			ExecutorGateway gateway = (ExecutorGateway)request.getSession().getAttribute("EXEC_GATEWAY");
			if(gateway == null){
				logger.error("ERROR in ExecutorServlet --> ExecutorGateway object is null");
				ret = "{status:error,message:An internal error occurred. Please contact your system administrator}";
				response.getWriter().write(ret);
			}
			JSONObject json = new JSONObject();
			try{

				gateway.initExecution();
				try{
					json.put("status", "success");
				}catch(JSONException e){
					logger.error("ERROR in ExecutorServlet --> Error generating JSON after successful validation of Test Set",e);
					ret = "{status:success}";
				}
			}catch(Exception e){
				logger.error("ERROR initializing run --> ",e);
				try{
					json.put("status", "error");
					json.put("message", e.getMessage());
				}catch(Exception e1){
					logger.error("ERROR in ExecutorServlet --> Error generating JSON after failed validation of Test Set",e);
					ret = "{status:error,message:" + e.getMessage() + "}";
				}
			}finally{
				ret = json.toString();
				response.getWriter().write(ret);
			}


		}else if(txn != null && txn.equalsIgnoreCase("load_test_data")){
			String ret = "";
			ExecutorGateway gateway = (ExecutorGateway)request.getSession().getAttribute("EXEC_GATEWAY");
			if(gateway == null){
				logger.error("ERROR in ExecutorServlet --> ExecutorGateway object is null");
				ret = "{status:error,message:An internal error occurred. Please contact your system administrator}";
				response.getWriter().write(ret);
			}
			JSONObject json = new JSONObject();
			try {
				try{
					/*Modified by paneendra for TENJINCG-1257 starts */
					//TreeMap<String, String> map = gateway.validateTestDataPaths();
					
					/*Modified by paneendra for TENJINCG-1268  starts */
					//TreeMap<String, String> map = gateway.validateUploadedTestDataPaths(tempawsPath);
					TreeMap<String, String> map = gateway.validateUploadedTestDataPaths();
					/*Modified by paneendra for TENJINCG-1268  ends */
					/*Modified by paneendra for TENJINCG-1257 ends */
					if(map != null && map.size() > 0){
						json.put("status", "failure");
						JSONArray failures = new JSONArray();
						for(String key:map.keySet()){
							if(!key.equalsIgnoreCase("status")){
								String[] split = key.split(",");
								String app = split[0];
								String func = split[1];
								JSONObject j =new JSONObject();
								j.put("app",app);
								j.put("func", func);
								j.put("message", map.get(key));
								failures.put(j);
							}
						}
						json.put("message", "Could not load all test data files");
						json.put("failures", failures);
					}else{
						json.put("status", "success");
						request.getSession().removeAttribute("EXEC_GATEWAY");
						request.getSession().setAttribute("EXEC_GATEWAY", gateway);
					}
				}catch(TenjinServletException e){
					logger.error("ERROR occurred in ExecutorServlet",e);
					json.put("status", "error");
					json.put("message", e.getMessage());
				}finally{
					ret = json.toString();
				}
			} catch (JSONException e) {
				
				logger.error("Error creating JSON for loading test data files",e);
				ret = "{status:error,message:An internal error occurred. Please contact your system administrator}";
			}finally{
				response.getWriter().write(ret);
			}
		}else if(txn != null && txn.equalsIgnoreCase("test_data_val")){
			String ret = "";
			ExecutorGateway gateway = (ExecutorGateway)request.getSession().getAttribute("EXEC_GATEWAY");
			if(gateway == null){
				logger.error("ERROR in ExecutorServlet --> ExecutorGateway object is null");
				ret = "{status:error,message:An internal error occurred. Please contact your system administrator}";
				response.getWriter().write(ret);
			}
			JSONObject json = new JSONObject();
			try {
				try{
					Map<String, List<String>> map = gateway.getTestDataRowUids();
					JSONArray failures = new JSONArray();
					for(String key:map.keySet()){
						String[] split = key.split(",");
						String tc = split[0];
						String s = split[1];
						List<String> uids = map.get(key);

						int iterCount = 0;
						if(uids != null){
							iterCount = uids.size();
						}
						if(iterCount == 0){
							JSONObject j = new JSONObject();
							j.put("tc", tc);
							j.put("step", s);
							j.put("message", "No test data specified for this step");
							failures.put(j);
						}else if(iterCount < 0){
							JSONObject j = new JSONObject();
							j.put("step", key);
							j.put("message", "An internal error occurred while fetching test data for this step");
							failures.put(j);
						}else{
							//System.out.println(key + " --> " + iterCount + " rows available");
						}
					}

					if(failures.length() > 0){
						json.put("status", "failure");
						json.put("message", "Test data validation failed");
						json.put("failures", failures);
					}else{
						json.put("status", "success");
						request.getSession().removeAttribute("EXEC_GATEWAY");
						request.getSession().setAttribute("EXEC_GATEWAY", gateway);
					}
				}catch(TenjinServletException e){
					logger.error("ERROR occurred in ExecutorServlet",e);
					json.put("status", "error");
					json.put("message", e.getMessage());
				}finally{
					ret = json.toString();
				}
			} catch (JSONException e) {
				
				logger.error("Error creating JSON for test data validation",e);
				ret = "{status:error,message:An internal error occurred. Please contact your system administrator}";
			}finally{
				response.getWriter().write(ret);
			}
		}else if(txn != null && txn.equalsIgnoreCase("user_aut_val")){
			String ret = "";
			ExecutorGateway gateway = (ExecutorGateway)request.getSession().getAttribute("EXEC_GATEWAY");
			if(gateway == null){
				logger.error("ERROR in ExecutorServlet --> ExecutorGateway object is null");
				ret = "{status:error,message:An internal error occurred. Please contact your system administrator}";
				response.getWriter().write(ret);
			}
			JSONObject json = new JSONObject();
			try {
				try{
					Map<String, String> map = gateway.validateUserAutCredentials();
					if(map != null && map.size() > 0){
						json.put("status", "failure");
						JSONArray failures = new JSONArray();
						for(String key:map.keySet()){
							JSONObject j =new JSONObject();
							if(!key.equalsIgnoreCase("status")){
								j.put("aut", key);
								j.put("message", map.get(key));
								failures.put(j);
							}
						}
						json.put("message", "User AUT Credentials validation failed");
						json.put("failures", failures);
					}else{
						json.put("status", "success");
						request.getSession().removeAttribute("EXEC_GATEWAY");
						request.getSession().setAttribute("EXEC_GATEWAY", gateway);
					}
				}catch(TenjinServletException e){
					logger.error("ERROR occurred in ExecutorServlet",e);
					json.put("status", "error");
					json.put("message", e.getMessage());
				}finally{
					ret = json.toString();
				}
			} catch (JSONException e) {
				
				logger.error("Error creating JSON for AUT credentials validation",e);
				ret = "{status:error,message:An internal error occurred. Please contact your system administrator}";
			}finally{
				response.getWriter().write(ret);
			}
		}
		/*************
		 * Added by Prem for TENJINCG-843 
		 */
		 /* Uncommented by Padmavathi for license starts */
		else if(txn.equalsIgnoreCase("adapter_license")){

			String ret = "";
			ExecutorGateway gateway = (ExecutorGateway)request.getSession().getAttribute("EXEC_GATEWAY");
			if(gateway == null){
				logger.error("ERROR in ExecutorServlet --> ExecutorGateway object is null");
				ret = "{status:error,message:An internal error occurred. Please contact your system administrator}";
				response.getWriter().write(ret);
			}
			JSONObject json = new JSONObject();
			try {
				try{
					Map<String, String> map = gateway.validateAdapterCredentials(al);
					//if(gateway.getAdapter().equals(null))
					//	adapter=map.get("adapter");
					if(map != null && map.size() > 0){
						json.put("status", "failure");
						JSONArray failures = new JSONArray();
						for(String key:map.keySet()){
							JSONObject j =new JSONObject();
							if(!key.equalsIgnoreCase("status")){
								j.put("aut", key);
								j.put("message", map.get(key));
								failures.put(j);
							}
						}
						json.put("message", "Adapter License validation failed");
						json.put("failures", failures);
					}else{
						json.put("status", "success");
						request.getSession().removeAttribute("EXEC_GATEWAY");
						request.getSession().setAttribute("EXEC_GATEWAY", gateway);
					}
				} catch (TenjinConfigurationException e) {
					
					logger.error("Error ", e);
				}finally{
					ret = json.toString();
				}
			} catch (JSONException e) {
				
				logger.error("Error creating JSON for AUT credentials validation",e);
				ret = "{status:error,message:An internal error occurred. Please contact your system administrator}";
			}finally{
				response.getWriter().write(ret);
			}

		}
		/* Uncommented by Padmavathi for license ends */
		//Added by Avinash for OAuth 2.0 requirement TENJINCG-1018- Starts
		else if(txn.equalsIgnoreCase("oauth_val")){
			ExecutorGateway gateway = (ExecutorGateway)request.getSession().getAttribute("EXEC_GATEWAY");
			Object oAuthObj = request.getSession().getAttribute("oAuthFlag");
			boolean oAuthFlag = false;
			if(oAuthObj != null)
				oAuthFlag = (Boolean)oAuthObj;
			
			JSONObject json = new JSONObject();
			if(!oAuthFlag){
				try {
					json.put("status", "continue");
					response.getWriter().write(json.toString());
					return;
				} catch (JSONException e) {}
			}
			Map<String, Map<String, String>> map = gateway.validateoAuthTokens();
			
			try {
				if(map.size() > 0){
					json.put("status", "failure");
					JSONArray failures = new JSONArray();
					for(String appName : map.keySet()){
						Map<String, String> userTypes = map.get(appName);
						for(String userType : userTypes.keySet()){
							String msg = userTypes.get(userType);
							JSONObject msgJson = new JSONObject();
							msgJson.put("appName", appName);
							msgJson.put("userType", userType);
							msgJson.put("desc", msg);
							failures.put(msgJson);
						}
					}
					json.put("message", "OAuth 2.0 validation failed");
					json.put("failures", failures);
				}else{
					json.put("status", "success");
					request.getSession().removeAttribute("EXEC_GATEWAY");
					request.getSession().setAttribute("EXEC_GATEWAY", gateway);
				}
			} catch (Exception e) {
				logger.debug("Error while processing the failures for oauth validation.");
			}
			response.getWriter().write(json.toString());
		}
		//Added by Avinash for OAuth 2.0 requirement TENJINCG-1018- Ends
		else if(txn.equalsIgnoreCase("create_run")){
			String ret = "";
			ExecutorGateway gateway = (ExecutorGateway)request.getSession().getAttribute("EXEC_GATEWAY");
			if(gateway == null){
				logger.error("ERROR in ExecutorServlet --> ExecutorGateway object is null");
				ret = "{status:error,message:An internal error occurred. Please contact your system administrator}";
				response.getWriter().write(ret);
			}

			JSONObject json = new JSONObject();
			try{
				try{
					gateway.createRun();
					request.getSession().removeAttribute("EXEC_GATEWAY");
					request.getSession().setAttribute("EXEC_GATEWAY", gateway);
					json.put("status", "success");
					json.put("message", "");
					ret = json.toString();
				}catch(Exception e){
					json.put("status", "ERROR");
					json.put("message", e.getMessage());
					ret = json.toString();
				}
			}catch(Exception e){
				
				logger.error("Error creating JSON for Create Run",e);
				ret = "{status:error,message:An internal error occurred. Please contact your system administrator}";
			}finally{
				response.getWriter().write(ret);
			}
		}else if(txn.equalsIgnoreCase("start_run")){
			String dUrl;
			try {
				ExecutorGateway gateway = (ExecutorGateway)request.getSession().getAttribute("EXEC_GATEWAY");
				TaskManifest manifest = gateway.getTaskManifest();
				/********************************
				 * Commented by Sriram for TENJINCG-168 (API Execution)
				 */
				/*manifest.setTaskType(AdapterTask.EXECUTE);*/
				/********************************
				 * Commented by Sriram for TENJINCG-168 (API Execution)
				 */
				BridgeProcessor handler = new BridgeProcessor(manifest);
				handler.createTask();
				logger.info("Beginning Execution...");
				Connection conn = (Connection)request.getSession().getAttribute("PROJ_CONN");
				/*Modified by Ashiki for Tenj210-159 starts*/
				if(conn == null||conn.isClosed()){
				/*Modified by Ashiki for Tenj210-159 ends*/
					logger.info("No DB Connection. Creating one");
					conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					request.getSession().setAttribute("PROJ_CONN", conn);
				}

				new ExecutionHelper().updateRunStatus(conn, TenjinConstants.SCRIPT_EXECUTING, gateway.getRun().getId());
				handler.runTask();
				logger.info("Done");
				/********************************
				 * Changed by Sriram for TENJINCG-168 (API Execution)
				 */

				dUrl = "TestRunServlet?param=run_progress&run=" + manifest.getRunId();
				//TENJINCG-1196 (sriram) ends
				/********************************
				 * Changed by Sriram for TENJINCG-168 (API Execution) ends
				 */
				response.sendRedirect(dUrl);
			} catch (Exception e) {
				
				logger.error("ERROR --> Could not start Run", e);
				String ret = "{status:error,message:An internal error occurred. Please contact your system administrator}";
				response.getWriter().write(ret);
			} 

		}

		/*************
		 * Added by Sriram for TENJINCG-166 
		 */
		else if(txn.equalsIgnoreCase("api_run_progress")) {
			String paramval = request.getParameter("paramval");
			int runId = 0;
			try{
				runId = Integer.parseInt(paramval);
			}catch(Exception e){
				logger.error("ERROR in Run_Progress --> Invalid Run ID {}", paramval,e);
			}

			Connection conn = null;

			try {
				conn = (Connection)request.getSession().getAttribute("PROJ_CONN");
				if(conn == null){
					logger.info("No DB Connection. Creating one");
					conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					request.getSession().setAttribute("PROJ_CONN", conn);
				}

				TestRun run = new ExecutionHelper().hydrateOngoingTestRun(conn, runId);

				if(run.getStatus() != null && !run.getCompletionStatus().equalsIgnoreCase(BridgeProcess.NOT_STARTED) && !run.getCompletionStatus().equalsIgnoreCase(BridgeProcess.IN_PROGRESS)){
					response.sendRedirect("ResultsServlet?param=run_result&run=" + runId);

					try{
						conn.close();
						logger.info("PROJ Connection Closed");
					}catch(Exception e){

					}

					request.getSession().removeAttribute("PROJ_CONN");
				} else {
					ExecutionProgressView ep = new ExecutionProgressView();
					ep.setRun(run);
					request.getSession().setAttribute("EXEC_PROGRESS_VIEW", ep);
					RequestDispatcher dispatcher = request.getRequestDispatcher("api_run_progress.jsp");
					dispatcher.forward(request, response);
				}
			} catch (DatabaseException e) {
				
				logger.error("ERROR fetching run progress",e);
				response.sendRedirect("error.jsp");
			} finally {
				try{
					conn.close();
					logger.info("PROJ Connection Closed");
				}catch(Exception e){

				}

				request.getSession().removeAttribute("PROJ_CONN");
			}
		} else if(txn.equalsIgnoreCase("check_api_run_progress")) {
			MDC.put("logFileName", "api_progress_check");


			String rId = request.getParameter("runId");
			logger.debug("Entered check_api_run_progress for Run ID [{}]", rId);
			MDC.remove("logFileName");

			int runId = Integer.parseInt(rId);

			ExecutorProgress progress = null;
			try {
				progress = (ExecutorProgress) CacheUtils.getObjectFromRunCache(runId);
			} catch (TenjinConfigurationException e) {
				
				logger.error("ERROR getting status from cahce. Please ensure the RUN_CACHE_NAME property is defined in the Tenjin Configuration", e);
			} catch(NullPointerException e) {
				logger.error("Status not available yet");
			}
			String ret = "";
			try {
				JSONObject json = new JSONObject();

				if(progress != null){
					String jsonString = progress.toJson();
					json.put("status", "success");
					json.put("progress", new JSONObject(jsonString));
				}else{
					logger.error("Could not get learning progress from cache. Progress may not be available yet.");
					json.put("status", "wait");
					json.put("message", "Waiting for progress data...");
				}
				ret = json.toString();
			} catch (JSONException e) {
				
				logger.error("JSONException occurred", e);
				ret = "{status:error,message:An internal error occurred while refreshing status. Please contact Tenjin support.}";
			}

			response.getWriter().write(ret);
		}

		else if(txn.equalsIgnoreCase("run_progress")){
			String paramval = request.getParameter("paramval");
			String refreshTimeout = request.getParameter("rto");
			String autoRefreshFlag = request.getParameter("arf");
			int runId = -1;
			try{
				runId = Integer.parseInt(paramval);
			}catch(Exception e){
				logger.error("ERROR in Run_Progress --> Invalid Run ID {}", paramval,e);
			}

			try{
				Connection conn = (Connection)request.getSession().getAttribute("PROJ_CONN");
				if(conn == null){
					logger.info("No DB Connection. Creating one");
					conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
					request.getSession().setAttribute("PROJ_CONN", conn);
				}

				TestRun run = new ExecutionHelper().hydrateOngoingTestRun(conn, runId);
				/*changed by sahana for Mobility: Starts*/
				//added by shivam sharma for  TENJINCG-904 starts
				RegisteredDevice device=null;
				//RegisteredDevice device=new DeviceHelper().hydrateDevice(run.getDeviceRecId());
				//if(run.getMachine_ip().equalsIgnoreCase("999.999.999.999")){
				/*Added by Sahana for pCloudy:Starts*/
				if(run.getDeviceFarmFlag()!=null && run.getDeviceFarmFlag().equalsIgnoreCase("Y")){
					Pcloudy objPcloudy=new PcloudyProcessor().getPcloudyCredentials(run.getMachine_ip());
					device=new PcloudyProcessor(objPcloudy).deviceById(run.getDeviceRecId());
				/*Added by Sahana for pCloudy:Ends*/
				}else{
					device=new DeviceHelper().hydrateDevice(run.getDeviceRecId());	
				}
				//added by shivam sharma for  TENJINCG-904 ends
				request.getSession().setAttribute("DEVICE", device);
				/*changed by sahana for Mobility: ends*/
				


				/* Changed by Sriram for TENJINCG-187 */
				/*if(run.getStatus() != null && !run.getCompletionStatus().equalsIgnoreCase(AdapterTask.NOT_STARTED) && !run.getCompletionStatus().equalsIgnoreCase(AdapterTask.IN_PROGRESS)){*/
				if(run.getStatus() != null && !run.getCompletionStatus().equalsIgnoreCase(BridgeProcess.NOT_STARTED) && !run.getCompletionStatus().equalsIgnoreCase(BridgeProcess.IN_PROGRESS) && !run.getCompletionStatus().equalsIgnoreCase(BridgeProcess.ABORTING)){
					/* Changed by Sriram for TENJINCG-187 Ends */
					response.sendRedirect("ResultsServlet?param=run_result&run=" + runId);

					try{
						conn.close();
						logger.info("PROJ Connection Closed");
					}catch(Exception e){

					}

					request.getSession().removeAttribute("PROJ_CONN");
				}else{
					int totalIterations = 0;
					TestCase currentTc= null;
					TestStep currentStep = null;
					String currentTdUid = null;
					int currentIteration = 0;
					int totalIterationsCompleted=0;
					int totalPass = 0;
					int totalFail = 0;
					int totalError = 0;
					for(TestCase t:run.getTestSet().getTests()){
						if(t.getTcStatus() != null && t.getTcStatus().equalsIgnoreCase(TenjinConstants.SCRIPT_EXECUTING)){
							currentTc = t;
							for(TestStep step:t.getTcSteps()){
								//Added by Sriram to correct  Execution Percentage calculation
								totalPass = totalFail  = totalError = 0;
								//Added by Sriram to correct  Execution Percentage calculation ends
								totalIterations = totalIterations + step.getTotalTransactions();
								totalPass = totalPass + step.getTotalPassedTxns();
								totalFail = totalFail + step.getTotalFailedTxns();
								totalError = totalError + step.getTotalErroredTxns();
								totalIterationsCompleted = totalIterationsCompleted + (totalPass + totalFail + totalError);
								if(step.getStatus() != null && step.getStatus().equalsIgnoreCase(TenjinConstants.SCRIPT_EXECUTING)){
									currentStep = step;
									int counter=0;
									for(StepIterationResult r:step.getDetailedResults()){
										counter++;
										if(r.getResult() != null && r.getResult().equalsIgnoreCase("X")){
											currentTdUid = r.getTduid();
											currentIteration = counter;
											break;
										}
									}
								}
							}
						}else{
							//currentTc = t;
							for(TestStep step:t.getTcSteps()){
								//Added by Sriram to correct  Execution Percentage calculation
								totalPass = totalFail  = totalError = 0;
								//Added by Sriram to correct  Execution Percentage calculation ends
								totalIterations = totalIterations + step.getTotalTransactions();
								totalPass = totalPass + step.getTotalPassedTxns();
								totalFail = totalFail + step.getTotalFailedTxns();
								totalError = totalError + step.getTotalErroredTxns();
								totalIterationsCompleted = totalIterationsCompleted + (totalPass + totalFail + totalError);

							}
						}

					}

					ExecutionProgressView ep = new ExecutionProgressView();
					ep.setRun(run);
					ep.setTotalCompletedIterations(totalIterationsCompleted);
					ep.setTotalIterations(totalIterations);
					ep.setCurrentTestCase(currentTc);
					ep.setCurrentTestStep(currentStep);
					ep.setCurrentIterationUid(currentTdUid);
					ep.setCurrentIteration(currentIteration);
					double eProgress = ((double)totalIterationsCompleted/(double)totalIterations)*100;
					/*Changed by Leelaprasad by Leelaprasad for progress bar on 10 th november starts*/
					double roundOff = Math.round(eProgress * 100.0) / 100.0;
					/*double roundOff = Math.round(((eProgress * 100.0)/100.0) / totalIterations);*/
					/*vChanged by Leelaprasad by Leelaprasad for progress bar on 10 th november starts*/
					/* Fixed by Sriram to remove decimals in execution progress percentage */
					/*ep.setExecutionProgress(roundOff);*/
					ep.setExecutionProgress((int) roundOff);
					/* Fixed by Sriram to remove decimals in execution progress percentage ends*/
					ep.setTotalPass(totalPass);
					ep.setTotalFail(totalFail);
					ep.setTotalError(totalError);
					ep.setTotalRemainingIterations(totalIterations-totalIterationsCompleted);
					ep.setAutoRefreshFlag(autoRefreshFlag);
					ep.setRefreshTimeout(refreshTimeout);
					request.getSession().setAttribute("EXEC_PROGRESS_VIEW", ep);	/***********************************************
					 *  added by nagareddy (18-11-2016) for Manual Input field Ehcache 
					 */

					TestObject output=null;String manualdata="";
					try{
						CacheManager cm = CacheManager.getInstance();				

						//3. Get a cache called "cacheStore"
						Cache cache = cm.getCache("cacheStore");						

						Element ele = cache.get(runId);
						output=(TestObject) ele.getObjectValue();
						if(output.getData().equalsIgnoreCase("false"))
						{
							manualdata=	output.getData();						
						}						
					}catch(Exception e){							
					}
					request.getSession().setAttribute("pauseOrContinue",manualdata);
					request.getSession().setAttribute("MANUAL_FLD",output);
					// end of Manual Input field 

					/* Added by Sriram for TENJINCG-187 */
					ExecutorProgress eProgressFromCache = (ExecutorProgress) CacheUtils.getObjectFromRunCache(run.getId());
					if(eProgressFromCache != null && eProgressFromCache.isRunAborted()){
						response.sendRedirect("runprogress.jsp?status=aborted");
					}else{
						response.sendRedirect("runprogress.jsp?status=active");
					}
					/*response.sendRedirect("runprogress.jsp");*/
					/* Added by Sriram for TENJINCG-187 Ends */
				}


			}catch(Exception e){
				logger.error("ERROR fetching run progress",e);
				response.sendRedirect("error.jsp");
			}
		}
		/***********************************************
		 *  added by nagareddy (18-11-2016) for Contining the manual field
		 */
		else if(txn.equalsIgnoreCase("manual_continue")){
			String paramval = request.getParameter("paramval");
			JSONObject json=new JSONObject();
			String ret="";
			if(paramval!=null && !paramval.equalsIgnoreCase("")){
				int runId = Integer.parseInt(paramval);
				CacheManager cm = CacheManager.getInstance();			

				//3. Get a cache called "cacheStore"
				Cache cache = cm.getCache("cacheStore");


				//5. Get element from cache
				Element ele = cache.get(runId);
				System.out.println("In ExecutorServlet Before "+ele.getObjectValue());

				TestObject output=(TestObject) ele.getObjectValue();
				output.setData("true");
				//4. Put few elements in cache
				cache.put(new Element(runId,output));
				//4. Put few elements in cache
				Element ele1 = cache.get(runId);
				System.out.println("In ExecutorServlet After "+ele1.getObjectValue());
				try {
					json.put("status", "SUCCESS");
				} catch (JSONException e) {

				}
			}

			ret = json.toString();
			response.getWriter().write(ret);
		}/*Added by Lokanath for TENJINCG-1193 starts*/
		else if(txn.equalsIgnoreCase("learn_abort_progress")){
			String userName=tjnSession.getUser().getId();
			String paramval = request.getParameter("paramval");
			int runId = -1;
			JsonObject json=new JsonObject();
			try{
				runId = Integer.parseInt(paramval);
			}catch(Exception e){
				logger.error("ERROR in Run_Progress --> Invalid Run ID {}", paramval,e);
			}
			try{
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				if(conn !=null){
				new RunHelper().updateLearnAbort(runId,userName);
				logger.info("Update run status to Aborted in Mastestruns and LearnerAuditTrail");
				TestRun testRun=new ExecutionHelper().hydrateOngoingLearnTestRun(runId);
				request.getSession().setAttribute("LRNR_RUN", testRun);
				response.sendRedirect("live_learner_progress.jsp");
				}
			}catch (DatabaseException e) {
				logger.error(e.getMessage());
				json.addProperty("status", "error");
				json.addProperty("message", e.getMessage());
			}
             catch(Exception e){
			}
			finally{
				response.getWriter().write(json.toString());
			}
			
		}/*Added by Lokanath for TENJINCG-1193 ends*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/************************************************************
		 * doPost() method re-organized by Sriram for Req#TJN_24_05
		 */
		String testSetId = request.getParameter("txtTsRecId");
		String browserType = request.getParameter("lstBrowserType");
		/*Added by pushpalatha for TENJINCG-386 starts */
		Boolean mobflag= (Boolean) request.getSession().getAttribute("mobFlag");
		/*Added by pushpalatha for TENJINCG-386 ends */

		/****************************************************
		 * Fix by Sharath to add Execution Speed Control (09-June-2016)
		 */

		String execSpeed = request.getParameter("txtExecutionSpeed");
		if(Utilities.trim(execSpeed).equalsIgnoreCase("")) {
			execSpeed = "0";
		}
		/****************************************************
		 * Fix by Sharath to add Execution Speed Control (09-June-2016) ends
		 */

		/**********************************8
		 * Added for Screenshot_Option (10-06-2016) - SRIRAM
		 */
		String screenshotOption = request.getParameter("Screen");
		if(Utilities.trim(screenshotOption).equalsIgnoreCase("")){
			screenshotOption = "0";
		}
		/**********************************8
		 * Added for Screenshot_Option (10-06-2016) - SRIRAM ends
		 */

		/********************************
		 * Added by Sriram for TENJINCG-168 (API Execution)
		 */
		String executionMode = request.getParameter("executionMode");
		/********************************
		 * Added by Sriram for TENJINCG-168 (API Execution) ends
		 */
		/* Added by Roshni for T25IT-120 starts */
		String entity_id=request.getParameter("entity_id");
		String entity_name=request.getParameter("entity_name");
		/* Added by Roshni for T25IT-120 ends */
		/* Added by Roshni for T25IT-345 starts */
		String set_mode=request.getParameter("set_mode");
		/* Added by Roshni for T25IT-345 ends */
		/*changed by sahana for Mobility: Starts*/
		String deviceId=request.getParameter("listDevice");
		/*changed by sahana for Mobility: ends*/

		boolean okToExecute = true;
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		String message = "";
		Map<String,Object> map = (Map<String,Object>) request.getSession().getAttribute("SCR_MAP");
		if(map == null){
			map = new HashMap<String, Object>();
		}
		/****************************************************************************
		 * Added by Sriram for Req#TJN_24_01 (19-Sep-2016) Test Case Level Execution
		 */
		String createTestSet = request.getParameter("createTestSet");
		List<TestStep> stepsToExecute = new ArrayList<TestStep>();//Sriram for Req#TJN_243_11
		if(createTestSet != null && "yes".equalsIgnoreCase(createTestSet)){
			//			/Map<String,Object> tMap = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
			TestSet set = (TestSet)map.get("TEST_SET");
			if(set != null){
				ArrayList<TestCase> tcs = set.getTests();
				try{
					set.setDescription("Auto generated test set");
					set.setType("Functional");
					set.setRecordType("TSA");
					set.setParent(Integer.parseInt("1"));
					set.setPriority("Medium");
					set.setCreatedBy(tjnSession.getUser().getId());
					logger.info("Persisting adhoc test set");
					set = new TestSetHelper().persistTestSet(set, tjnSession.getProject().getId());
					testSetId = Integer.toString(set.getId());
					String tcList = "";
					int counter=1;
					if(tcs != null){
						for(TestCase t:tcs){
							if(counter < tcs.size()){
								tcList = tcList + t.getTcRecId() + ",";
							}else{
								tcList = tcList + t.getTcRecId();
							}
						}
					}

					logger.info("Persisting test set map");
					/*Changed by Sriram for TENJINCG-189 (13th June 2017) */
					/*new TestSetHelper().persistTestSetMap(Integer.parseInt(testSetId), tcList, tjnSession.getProject().getId());*/
					new TestSetHelper().persistTestSetMap(Integer.parseInt(testSetId), tcList, tjnSession.getProject().getId(), true);
					/*Changed by Sriram for TENJINCG-189 (13th June 2017) ends*/

					okToExecute = true;
				}catch(DatabaseException e){
					logger.error("ERROR while persisting Ad-Hoc test set", e);
					message = "Could not start run due to an unexpected error. Please contact Tenjin Support.";
					okToExecute = false;
				}

				//Sriram for Req#TJN_243_11
				for(TestCase testCase:set.getTests()){
					for(TestStep step:testCase.getTcSteps()){
						stepsToExecute.add(step);
					}
				}//Sriram for Req#TJN_243_11 ends
			}else{
				logger.error("ERROR creating ad-hoc test set. Test Set is null");
				message = "Could not start run due to an unexpected error. Please contact Tenjin Support.";
				okToExecute = false;
			}
		}
		/****************************************************************************
		 * Added by Sriram for Req#TJN_24_01 (19-Sep-2016) ends
		 */


		/*************************************
		 * Parveen - TJN_24_11 starts
		 */
		String videoCaptureFlag = Utilities.trim(request.getParameter("videoCaptureFlag"));
		if(videoCaptureFlag.equalsIgnoreCase("")){
			videoCaptureFlag = "N";
		}
		//added by shivam sharma for  TENJINCG-904 starts
		/*String clientName = request.getParameter("txtclientregistered");*/
		String clientName = request.getParameter("client");
		//added by shivam sharma for  TENJINCG-904 ends
		String dUrl = "";
		String oem = "";
		String targetIp = "";
		String port = "4444";

		try {
			logger.info("Getting Automation Engine configuration");
			oem = TenjinConfiguration.getProperty("OEM_TOOL");
		} catch (TenjinConfigurationException e) {
			
			logger.error("CONFIG ERROR --> {}", e.getMessage());
			logger.info("Automation Engine will be defaulted to Selenium WebDriver");
			oem = "Selenium";
		}

		logger.info("Automation Engine --> [{}]", oem);
		/*Added by pushpalatha for TENJINCG-386 starts */
if(Utilities.trim(executionMode).equalsIgnoreCase("msg")){
			
		}
else if(! Utilities.trim(executionMode).equalsIgnoreCase("api")) {
			if(mobflag==true){
				if(request.getParameter("listDevice").equals("-1")){
					try {
						throw new TenjinServletException("Please select a device");
					} catch (TenjinServletException e) {
						
						logger.error("Error ", e);
						message = e.getMessage();
						okToExecute = false;
					}
				}
			}
		}
		/*Added by pushpalatha for TENJINCG-386 ends */



		RegisteredClient client = null;

		/********************************
		 * Changed by Sriram for TENJINCG-168 (API Execution)
		 */
		
		if(! Utilities.trim(executionMode).equalsIgnoreCase("api")) {
			if(okToExecute){
				try{
					logger.info("Validating Client...");
					client = new ClientHelper().hydrateClient(clientName);
					if(client == null){
						logger.error("Client [{}] not found");
						throw new TenjinServletException("Invalid client selected. Please choose a different client");
					}

					targetIp = client.getHostName();
					port = client.getPort();

				}catch(DatabaseException e){
					logger.error("CLIENT [{}] not found", e);
					message = "Could not verify Client Information. Please contact Tenjin Support";
					okToExecute = false;
				} catch (TenjinServletException e) {
					
					message = e.getMessage();
					okToExecute = false;
				}
			}
		}
		/********************************
		 * Changed by Sriram for TENJINCG-168 (API Execution) ends
		 */
		
		//Code change for OAuth 2.0 requirement TENJINCG-1018- Avinash - Starts
		boolean oAuthValidation = false;
		/*if(executionMode.equalsIgnoreCase("api")){*/
			oAuthValidation = new ApiHelper().checkForoAuthOperation(Integer.parseInt(testSetId), "oAuth2");
			request.getSession().setAttribute("oAuthFlag", oAuthValidation);
		/*}*/
		//Code change for OAuth 2.0 requirement TENJINCG-1018- Avinash - Ends

		if(okToExecute){
			logger.info("Initializing Executor Gateway...");
			try {
				/********************************
				 * Changed by Sriram for TENJINCG-168 (API Execution)
				 */
				/*ExecutorGateway gateway = new ExecutorGateway(tjnSession, Integer.parseInt(testSetId), browserType, targetIp, Integer.parseInt(port), oem, Integer.parseInt(execSpeed), Integer.parseInt(screenshotOption), videoCaptureFlag);*/
				/*changed by sahana for Mobility: Starts*/
				//ExecutorGateway gateway = new ExecutorGateway(tjnSession, Integer.parseInt(testSetId), browserType, targetIp, Integer.parseInt(port), oem, Integer.parseInt(execSpeed), Integer.parseInt(screenshotOption), videoCaptureFlag, executionMode);
				ExecutorGateway gateway = new ExecutorGateway(tjnSession, Integer.parseInt(testSetId), browserType, targetIp, Integer.parseInt(port), oem, Integer.parseInt(execSpeed), Integer.parseInt(screenshotOption), videoCaptureFlag, executionMode, deviceId,"");
				/*changed by sahana for Mobility: ends*//********************************
				 * Changed by Sriram for TENJINCG-168 (API Execution) ends
				 */
				gateway.setStepsToExecute(stepsToExecute); //Sriram for Req#TJN_243_11

				request.getSession().setAttribute("EXEC_GATEWAY", gateway);
				dUrl = "runvalidate.jsp";
			} catch (NumberFormatException e) {
				
				logger.error("NumberFormatException caught while initializing gateway", e);
				message = "An Internal Error occurred. Please contact your System Administrator";
				okToExecute = false;
			} catch (TenjinServletException e) {
				
				okToExecute = false;
				message = e.getMessage();
			}
		}


		if(okToExecute){
			
			/* Modified by Roshni for T25IT-345 starts */
			dUrl = "runvalidate.jsp?entity_id="+entity_id+"&entity_name="+entity_name+"&set_mode="+set_mode;
			/* Modified by Roshni for T25IT-345 ends */
			
			//Added by Avinash for OAuth 2.0 requirement TENJINCG-1018- Starts
			if(oAuthValidation){
				dUrl+="&oauthval=Y";
			}
			//Added by Avinash for OAuth 2.0 requirement TENJINCG-1018- Ends
		}else{
			map.put("STATUS", "ERROR");
			map.put("MESSAGE",message);
			request.getSession().setAttribute("SCR_MAP", map);
			dUrl = "runinit.jsp";
		}

		logger.info("Redirecting...");
		response.sendRedirect(dUrl);

	}
}
