/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExtractorServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 08-Nov-2016           Sriram Sridharan          Newly Added For Data Extraction in Tenjin 2.4.1
* 13-Dec-2016          Leelaprasad             Defect fix#TEN-28
* 22-Sep-2017			sameer gupta			For new adapter spec
* 09-Oct-2017			Preeti					Added code to get Application name
* 22-01-2018			Pushpalatha				TenjinCG-585
* 02-01-2019			Prem					For validating adapter license
* 24-06-2019            Padmavathi              for license 
*/

package com.ycs.tenjin.servlet.adapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.Tika;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.MaxActiveUsersException;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.BridgeProcessor;
import com.ycs.tenjin.bridge.exceptions.TestDataException;
import com.ycs.tenjin.bridge.oem.gui.datahandler.GuiDataHandlerImpl;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ExtractionRecord;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.run.ExtractorGateway;
import com.ycs.tenjin.run.ExtractorProgressView;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Utilities;
/**
 * Servlet implementation class ExtractorServlet
 */
//@WebServlet("/ExtractorServlet")
public class ExtractorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(ExtractorServlet.class);  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExtractorServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String txn = request.getParameter("param");
		if(txn != null && txn.equalsIgnoreCase("new")){
			request.getSession().removeAttribute("EXTRACTOR_SCREEN_MAP");
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				logger.info("Hydrating all AUTs");
				ArrayList<Aut> allAuts = new AutHelper().hydrateAllAut();
				logger.info("Done");
				map.put("STATUS", "SUCCESS");
				map.put("MESSAGE","");
				map.put("AUTS", allAuts);
			} catch (DatabaseException e) {
				logger.error("ERROR while fetching AUT list",e);
				map.put("STATUS","ERROR");
				map.put("MESSAGE","Could not load AUTs due to an internal error");
			}finally{
				request.getSession().setAttribute("EXTRACTOR_SCREEN_MAP", map);
				logger.info("Forwarding...");
				RequestDispatcher dispatcher = request.getRequestDispatcher("data_extraction_new.jsp");
				dispatcher.forward(request, response);
			}
		} else if(txn.equalsIgnoreCase("extractor_progress")){
			String rId = request.getParameter("runid");
			TestRun run = null;
			/* Added by preeti to get Application name starts*/
			String appName="";
			/* Added by preeti to get Application name ends*/
			request.getSession().removeAttribute("EXTR_PROGRESS_VIEW");
			ExtractorProgressView view = new ExtractorProgressView();
			try {
				run = new RunHelper().hydrateRun(Integer.parseInt(rId));
				/* Added by preeti to get Application name starts*/
				appName=new AutHelper().getAutName(run.getAppId());
				/* Added by preeti to get Application name ends*/
				if(run.getEndTimeStamp() == null){
					run.setElapsedTime(Utilities.calculateElapsedTime(run.getStartTimeStamp().getTime(), System.currentTimeMillis()));
				}else{
					run.setElapsedTime(Utilities.calculateElapsedTime(run.getStartTimeStamp().getTime(), run.getEndTimeStamp().getTime()));
				}
				view.setRun(run);
				double progressPercentage = 0;
				
				int totalRecords = 0;
				int completedRecords = 0;
				for(Module module:run.getFunctions()){
					if(module.getLastSuccessfulExtractionStatus() != null && module.getLastSuccessfulExtractionStatus().getRecords() != null){
						totalRecords = module.getLastSuccessfulExtractionStatus().getRecords().size();
						for(ExtractionRecord record:module.getLastSuccessfulExtractionStatus().getRecords()){
							String status = Utilities.trim(record.getResult());
							if(status.equalsIgnoreCase("aborted") || status.equalsIgnoreCase("s") || status.equalsIgnoreCase("e")){
								completedRecords++;
							}else if(status.equalsIgnoreCase("x")){
								view.setCurrentRecord(record);
								view.setCurrentFunction(module);
							}
						}
					}
				}
				
				progressPercentage = ((double)completedRecords / (double) totalRecords) * 100;
				double roundOff = Math.round(progressPercentage * 100.0) / 100.0;
				int rWithoutDecimals = (int) roundOff;
				String roundOffString = Integer.toString(rWithoutDecimals);
				view.setProgressPercentage(roundOffString);
				view.setScreenStatus("success");
				
			} catch (NumberFormatException e) {
				
				logger.error("Invalid Run ID [{}]", rId);
				view.setScreenStatus("error");
				view.setMessage("Invalid Run ID");
			} catch (DatabaseException e) {
				
				view.setScreenStatus("error");
				view.setMessage(e.getMessage());
			} catch(Exception e){
				
				view.setScreenStatus("error");
				view.setMessage("An internal error occurrd. Please contact Tenjin Support.");
			}
			
			
			String functionCode = request.getParameter("func");
			String dUrl = "live_extr_progress.jsp";
			if(!Utilities.trim(functionCode).equalsIgnoreCase("")){
				request.getSession().removeAttribute("EXTR_FUNC");
				Module functionToShow = null;
				if(run != null && run.getFunctions() != null){
					for(Module function:run.getFunctions()){
						if(function.getCode().equalsIgnoreCase(functionCode)){
							functionToShow = function;
							break;
						}
					}
				}
				
				if(functionCode == null){
					view.setScreenStatus("error");
					view.setMessage("An internal error occurrd. Please contact Tenjin Support.");
				}else{
					view.setScreenStatus("success");
					view.setMessage("");
				}
				
				dUrl = "live_extr_detail_view.jsp";
				request.getSession().setAttribute("EXTR_FUNC", functionToShow);
			}
			
			request.getSession().setAttribute("EXTR_PROGRESS_VIEW", view);
			/* Added by preeti to get Application name starts*/
			request.getSession().setAttribute("APP_NAME", appName);
			/* Added by preeti to get Application name ends*/
			RequestDispatcher dispatcher = request.getRequestDispatcher(dUrl);
			dispatcher.forward(request, response);
		} else if(txn.equalsIgnoreCase("output")){
			String ret = "";
			try {
				int runId = Integer.parseInt(request.getParameter("runid"));
				String functionCode = request.getParameter("func");
				String tdUids = request.getParameter("tduids");
				String contextRoot = request.getServletContext().getRealPath("/");
				ret = this.getExtractionOutput(runId, functionCode, tdUids, contextRoot);
			} catch (NumberFormatException e) {
				
				logger.error("Invalid Run ID [{}]", request.getParameter("runid"));
				ret = "{status:error,message:An internal error occurred. Please contact Tenjin Support.}";
			} catch (JSONException e) {
				
				logger.error("JSON Exception caught", e);
				ret = "{status:error,message:An internal error occurred. Please contact Tenjin Support.}";
			}
			
			response.getWriter().write(ret);
		}
	}
	
	private String getExtractionOutput(int runId, String functionCode, String tdUids, String contextRoot) throws JSONException {
		String ret ="";
		
		JSONObject json = new JSONObject();
		
		try {
			TestRun run = new RunHelper().hydrateRun(runId);
			Module cFunction = null;
			for(Module function:run.getFunctions()){
				if(function.getCode().equalsIgnoreCase(functionCode)){
					cFunction = function;
					break;
				}
			}
			
			if(cFunction == null){
				json.put("status", "error");
				json.put("message", "An internal error occurred. Please contact Tenjin Support.");
				return json.toString();
			}
			
			if(Utilities.trim(tdUids).equalsIgnoreCase("all")){
				tdUids = "";
				
				
				int i=0;
				if(cFunction.getLastSuccessfulExtractionStatus() != null && cFunction.getLastSuccessfulExtractionStatus().getRecords() != null){
					for(ExtractionRecord record:cFunction.getLastSuccessfulExtractionStatus().getRecords()){
						i++;
						tdUids = tdUids + record.getTdUid();
						if(i < cFunction.getLastSuccessfulExtractionStatus().getRecords().size()){
							tdUids = tdUids + ",";
						}
					}
				}
			}
			
			String[] tdUidArray = tdUids.split(",");
			String fileName = new GuiDataHandlerImpl().generateExtractionOutput(runId, run.getAppId(), cFunction, tdUidArray, false);
			//String folderPath = request.getServletContext().getRealPath("/");
			Utilities.checkForDownloadsFolder(contextRoot);
			/*Changed by Pushpalatha for TENJINCG-585 starts*/
			contextRoot = contextRoot + File.separator+"Downloads"+File.separator + fileName;
			/*Changed by Pushpalatha for TENJINCG-585 ends*/
			/*Changed by Pushpalatha for TENJINCG-585 starts*/
			File sFile = new File(TenjinConfiguration.getProperty("TJN_WORK_PATH") + File.separator+"EXTR_OUTPUT"+ File.separator + fileName);
			/*Changed by Pushpalatha for TENJINCG-585 ends*/
			File dFile = new File(contextRoot);
			FileUtils.copyFile(sFile, dFile);
			json.put("status", "success");
			json.put("message", "");
			json.put("path", "Downloads/" + fileName);
		} catch (DatabaseException e) {
			
			json.put("status", "success");
			json.put("message", e.getMessage());
		} catch (TestDataException e) {
			
			json.put("status", "success");
			json.put("message", e.getMessage());
		} catch (TenjinConfigurationException e) {
			
			json.put("status", "success");
			json.put("message", e.getMessage() + ". Please contact Tenjin Administrator.");
		} catch (IOException e) {
			
			logger.error("Exception occurred--> ",e );
			json.put("status", "success");
			json.put("message", "An internal error occurred. Please contact Tenjin Support.");
		}
		
		ret = json.toString();
		return ret;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		boolean isMultiPart;
		
		isMultiPart = ServletFileUpload.isMultipartContent(request);
		
		Map<String, Object> map = new HashMap<String, Object>();
		@SuppressWarnings("unchecked")
		//List<Aut> auts = (List<Aut>)((Map<String,Object>)request.getSession().getAttribute("EXTRACTOR_SCREEN_MAP")).get("AUTS");	
		String dUrl = "";
		if(!isMultiPart){
			map = (Map<String, Object>) request.getSession().getAttribute("EXTRACTOR_SCREEN_MAP");
			if(map == null){
				map = new HashMap<String, Object>();
			}
			//Map<String, String> map = new HashMap<String, Object>();
			String autLoginType = request.getParameter("autLoginType");
			String rClientName = request.getParameter("rClient");
			String browserType = request.getParameter("browser");
			ExtractorGateway gateway = (ExtractorGateway) request.getSession().getAttribute("EXTR_GATEWAY");
			
			boolean ok = true;
			String message = "";
			if(gateway != null){
				gateway.setClientName(rClientName);
				gateway.setAutLoginType(autLoginType);
				gateway.setBrowserType(browserType);
				try {
					logger.info("Performing Final Validations");
					gateway.performFinalValidations();
					/*Added by Padmavathi for license starts*/
					gateway.validateAdapterLicense();
					/*Added by Padmavathi for license ends*/
					logger.info("Initializing Extraction Run...");
					gateway.createRun();
					/*Added by Prem for validating adapter license start*/
				} catch (TenjinServletException | MaxActiveUsersException | LicenseInactive e) {
					/*Added by Prem for validating adapter license start*/
					message = e.getMessage();
					ok = false;
				}
			}else{
				logger.error("ERROR --> Extractor Gateway is null");
				ok = false;
				message = "An internal error occurred. Please contact Tenjin Support.";
			}
			
			
			if(ok){
				BridgeProcessor handler = new BridgeProcessor(gateway.getManifest());
				request.getSession().removeAttribute("EXTR_GATEWAY");
				request.getSession().setAttribute("EXTR_GATEWAY", gateway.getManifest());
				
				handler.createTask();
				request.getSession().setAttribute("EXTR_START_TIME", System.currentTimeMillis());
				handler.runTask();
				logger.info("Extractor Started");
				response.sendRedirect("ExtractorServlet?param=extractor_progress&runid=" + gateway.getTestRun().getId());
			}else{
				map.put("STATUS","ERROR");
				map.put("MESSAGE", message);
				request.getSession().setAttribute("EXTRACTOR_SCREEN_MAP", map);
				response.sendRedirect("data_extraction_confirmation.jsp");
			}
			
		}else{
			ExtractorGateway gateway = null;
			boolean ok = true;
			request.getSession().removeAttribute("EXTRACTOR_SCREEN_MAP");
			DiskFileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			
			try {
				List fileItems = upload.parseRequest(request);
				Iterator fileItemsIter = fileItems.iterator();
				Map<String, String> formValues = new HashMap<String, String>();
				
				String uploadpath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
				File uploadFilePath= new File(uploadpath+File.separator+"EXTRACTOR");

				if(!uploadFilePath.exists()){
					uploadFilePath.mkdirs();
				}
				/*Modified by Priyanka for Vapt File Content Upload ends*/
				String fullFilePath = "";
				File uploadedFileRP=null;
				while(fileItemsIter.hasNext()){
					FileItem fi = (FileItem)fileItemsIter.next();
					if(fi.isFormField()){
						formValues.put(fi.getFieldName(), fi.getString());
					}
					
					else{
						/*Modified by Priyanka for Vapt File Content Upload starts*/
						//String fileName = fi.getName();

						String fileName = StringUtils.deleteWhitespace(fi.getName());
						uploadedFileRP = new File(uploadFilePath.getPath()+File.separator + fileName);
						fi.write(uploadedFileRP);
						/*Modified by Priyanka for Vapt File Content Upload ends*/
						//Changed by Pushpalatha for TENJINCG-585 starts
						if( fileName.lastIndexOf(File.separator) >= 0 ){
							fullFilePath = uploadFilePath + File.separator + fileName.substring( fileName.lastIndexOf(File.separator));
						}else{
							fullFilePath = uploadFilePath + File.separator + fileName.substring(fileName.lastIndexOf(File.separator)+1);
						}
				
						Tika tika = new Tika();
						String Exfile ="";
						try {
							Exfile = tika.detect(uploadedFileRP);
						} catch (IOException e) {
							logger.debug("Unable to Detect the File Content: " + e.getMessage());
						}
						
						   if(!(Exfile.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")||Exfile.equalsIgnoreCase("application/vnd.ms-excel"))){
							   throw new RequestValidationException("Invalid Excel sheet");
							   
						   }
						logger.info("Input file written to path {}", uploadedFileRP);
					}
				}
				
				int appId = Integer.parseInt(formValues.get("lstApplication"));
				String functionCode = formValues.get("lstModules");
				Map<String, String> functionMap = new HashMap<String,String>();
				functionMap.put(functionCode, fullFilePath);
				gateway = new ExtractorGateway(appId, functionMap, tjnSession.getUser().getId());
				/*Changed by Leelaprasad for the Requirement of defect fix#TEN-28 on 13-12-2016 starts*/
				Boolean flag=gateway.validateExcelSheet(fullFilePath);
				if(flag){
					/*Changed by Leelaprasad for the Requirement of defect fix#TEN-28 on 13-12-2016 ends*/
				gateway.performInitialValidations();
				request.getSession().setAttribute("EXTR_GATEWAY", gateway);
				map.put("STATUS", "SUCCESS");
				map.put("MESSAGE", "File uploaded successfully");
				map.put("AUT", gateway.getAut());
				map.put("FUNCTIONS", gateway.getFunctions());
				dUrl = "data_extraction_confirmation.jsp";
				
				/*Changed by Leelaprasad for the Requirement of defect fix#TEN-28 on 13-12-2016 starts*/
				}
				else{
					ArrayList<Aut> allAuts = new AutHelper().hydrateAllAut();
					ok=false;
					map.put("AUTS", allAuts);
					map.put("STATUS", "Failure");
					map.put("MESSAGE", "File you uploaded does not have any fields");
					dUrl = "data_extraction_new.jsp";
					
				}
				/*Changed by Leelaprasad for the Requirement of defect fix#TEN-28 on 13-12-2016 ends*/
				
			} catch (FileUploadException e) {
				ok = false;
				
				logger.error(e.getMessage(), e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "There was a problem uploading your file. Please contact Tenjin Support.");
				dUrl = "data_extraction_new.jsp";
			} catch (TenjinServletException e) {
				ok = false;
				
				//logger.error(e.getMessage(), e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", e.getMessage());
				dUrl = "data_extraction_new.jsp";
			}catch (Exception e) {
				ok = false;
				
				logger.error("ERROR occurred while initializing extraction", e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "An internal error occurred. Please contact Tenjin Support.");
				dUrl = "data_extraction_new.jsp";
			}
			
			
			if(ok){
				String message = "";
				try {
					logger.info("Fetching credentials for [{}] for Tenjin user [{}]", gateway.getAut().getName(), tjnSession.getUser().getId());
					ArrayList<String> userAutCreds = new AutHelper().hydrateAllAutLoginTypes(tjnSession.getUser().getId(), gateway.getAut().getId());
					if(userAutCreds != null && userAutCreds.size() > 0){
						map.put("OWN_CREDS", userAutCreds);
					}else{
						ok = false;
						message = "You have not defined any login credentials for " +  gateway.getAut().getName() + ". Please define Credentials in AUT User Maintenance";
					}
				} catch (NumberFormatException e) {
					logger.error("ERROR fetching AUT Credentials for User [{}], application [{}]", tjnSession.getUser().getId(),  gateway.getAut().getName());
					ok = false;
					message = "COuld not fetch Login credentials for " +  gateway.getAut().getName() + " due to an internal error. Please contact Tenjin Support";
				} catch (DatabaseException e) {
					logger.error("ERROR fetching AUT Credentials for User [{}], application [{}]", tjnSession.getUser().getId(),  gateway.getAut().getName());
					ok = false;
					message = "COuld not fetch Login credentials for " +  gateway.getAut().getName() + " due to an internal error. Please contact Tenjin Support";
				}
				
				try{
					logger.info("Loading all registered clients");
					List<RegisteredClient> clients = new ClientHelper().hydrateAllClients();
					map.put("CLIENTS", clients);
				}catch(Exception e){
					ok = false;
					message = "Could not fetch list of clients. Please contact Tenjin Support.";
					logger.error("ERROR occurred while fetching list of registered clients", e);
				}
				
				if(!ok){
					map.put("STATUS", "ERROR");
					map.put("MESSAGE", message);
				}else{
					map.put("STATUS", "SUCCESS");
					map.put("MESSAGE", "");
				}
				
			}
			
			request.getSession().setAttribute("EXTRACTOR_SCREEN_MAP", map);
			response.sendRedirect(dUrl);
		}
		
	}

}
