/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DeviceServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

  *//******************************************
  * CHANGE HISTORY
  * ==============
  *
  * DATE                	 	CHANGED BY                DESCRIPTION
  *  22-Dec-2017				Sahana						Mobility	
  *  26-Nov 2018				Shivam	Sharma				TENJINCG-904
  *  25-Jan 2019				Sahana						pCloudy
  *  04-12-2019					Preeti						TENJINCG-1174
  */
package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.remoteapiserver.RemoteApiServer;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DeviceHelper;
import com.ycs.tenjin.device.RegisteredDevice;
import com.ycs.tenjin.pcloudy.Pcloudy;
import com.ycs.tenjin.pcloudy.PcloudyProcessor;
import com.ycs.tenjin.rest.DeviceService;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;


public class DeviceServlet  extends HttpServlet {
	
	public static AuthorizationRule authorizationRule = new AuthorizationRule() {
        
        @Override
        public boolean checkAccess(HttpServletRequest request) {
        	if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
                    return true;
            }
            return false;
        }  
    };

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory
			.getLogger(DeviceServlet.class);
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 
		TenjinSession tjnSession = (TenjinSession) request.getSession()
				.getAttribute("TJN_SESSION");
		String txn = request.getParameter("param");

		Authorizer authorizer=new Authorizer();
		if(txn.equalsIgnoreCase("") || txn.equalsIgnoreCase("Device_VIEW")||  txn.equalsIgnoreCase("NEW_DEVICE") ||txn.equalsIgnoreCase("Add_Device")|| txn.equalsIgnoreCase("Add_Devices")|| txn.equalsIgnoreCase("UPDATE_DEVICE")|| txn.equalsIgnoreCase("Delete_Devices")||
				txn.equalsIgnoreCase("Delete_Device") || txn.equalsIgnoreCase("FETCH_ALL_DEVICES") || txn.equalsIgnoreCase("SCAN_DEVICES") ) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		
		if (txn.equalsIgnoreCase("FETCH_ALL_DEVICES")) {
			Map<String, Object> map = new HashMap<String, Object>();

			try {
				request.getSession().setAttribute("TJN_SESSION", tjnSession);
				DeviceHelper helper = new DeviceHelper();
				ArrayList<RegisteredDevice> rdList = helper.hydrateAllDevices();

				if (rdList != null) {
					logger.info("Fetched {} Devices successfully", rdList.size());
					map.put("DEVICES", rdList);
					map.put("DEVICE_STATUS", "SUCCESS");
					map.put("MESSAGE", "");
					request.getSession().setAttribute("SCR_MAP", map);
					response.sendRedirect("reg_device_list.jsp");
				}

			}catch (Exception e) {
				logger.error("ERROR occurred while fetching devices");
				logger.error(e.getMessage(),e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", e.getMessage());
				request.getSession().setAttribute("SCR_MAP", map);
				response.sendRedirect("error.jsp");

			}
		}
		else if(txn.equalsIgnoreCase("NEW_DEVICE_REG"))
		{
			String jsonString = request.getParameter("json");
			JSONObject retJson = new JSONObject();   
			String retData = ""; 
			try {
				JSONObject j = new JSONObject(jsonString);
				RegisteredDevice rd= new RegisteredDevice();
				rd.setDeviceName(j.getString("deviceName"));
				rd.setDeviceId(j.getString("deviceId"));
				rd.setDeviceType(j.getString("deviceType"));
				rd.setPlatform(j.getString("platform"));
				rd.setPlatformVersion(j.getString("platformVersion"));
				rd.setRegisteredBy(tjnSession.getUser().getId());
				rd.setRegisteredOn(new Date());
				DeviceHelper helper=new DeviceHelper();
				int count= helper.hydrateDeviceWithId(rd.getDeviceId());
				if(!(count>=1))
				{
					helper.persistDevice(rd);
					retJson.put("status", "SUCCESS");   
					retJson.put("message", "Device registered successfully");    
				}
				else
				{
					retJson.put("status", "ERROR");   
					retJson.put("message", "Device exists");  
				}

			} catch (Exception e) {
				try {
					retJson.put("status","ERROR");
					retJson.put("message", "An internal error occured. Please Try again");   
				} catch (JSONException e1) {
					logger.error(e.getMessage());
				}   
			} 			
			retData = retJson.toString();   
			response.getWriter().write(retData);  
		}
		else if (txn.equalsIgnoreCase("UPDATE_DEVICE")) {

			String jsonString = request.getParameter("json");
			JSONObject retJson = new JSONObject();   
			String retData = ""; 
			try {

				JSONObject j = new JSONObject(jsonString);
				RegisteredDevice rd= new RegisteredDevice();
				rd.setDeviceName(j.getString("deviceName"));			
				rd.setPlatformVersion(j.getString("platformVersion"));
				rd.setDeviceRecId(j.getString("deviceRecId"));
				rd.setDeviceDescription(j.getString("desc"));
				rd.setDeviceStatus(j.getString("deviceStatus"));

				DeviceHelper helper=new DeviceHelper();
				int count= helper.hydrateDeviceWithId(rd.getDeviceId());
				if(!(count>=1))
				{
					helper.updateDevice(rd);
					retJson.put("status", "SUCCESS");   
					retJson.put("message", "Device updated successfully");   
				}
				else
				{
					retJson.put("status", "ERROR");   
					retJson.put("message", "Device exists");  
				}
			} catch (Exception e) {
				try {
					retJson.put("status","ERROR");
					retJson.put("message", "An internal error occured. Please try again");   
				} catch (JSONException e1) {
					logger.error(e.getMessage());
				}   

			} 			
			retData = retJson.toString();   
			response.getWriter().write(retData);  

		}

		else if (txn.equalsIgnoreCase("Device_VIEW")) {
			String devRecId = request.getParameter("devRecId");

			Map<String, Object> map = new HashMap<String, Object>();
			try {
				RegisteredDevice rd = new DeviceHelper().hydrateDevice(Integer.parseInt(devRecId));

				map.put("STATUS", "SUCCESS");
				map.put("REGISTERED_DEVICE_BEAN", rd);

			} catch (Exception e) {
				map.put("STATUS", "ERROR");
				map.put("MESSAGE",
						"An Internal Error Occurred. Please contact your System Administrator");
			} finally {
				request.getSession().setAttribute("SCR_MAP", map);
			}
			response.sendRedirect("reg_device_view.jsp");
		}
		
		/***************
		 * Logic to scan devices using API Server Protocol
		 * Changed by Sameer Gupta on (1st Jan, 2018)
		 */
		else if (txn.equalsIgnoreCase("SCAN_DEVICES")) {

			String clientName = request.getParameter("client");
			DeviceService device = new DeviceService();
			JSONObject retJson = new JSONObject();
			String retData = "";

			try {
				CacheUtils.initializeRunCache();
			} catch (TenjinConfigurationException e) {
				logger.warn(
						"Could not initialize Cache Store. Please ensure RUN_CACHE_NAME property is set in Tenjin Configuration.",
						e);
			}

			String jsonDeviceList = null;
			try {
				RegisteredClient objClient = new ClientHelper()
						.hydrateClient(clientName);
				RemoteApiServer objRemoteApiServer = new RemoteApiServer(1000,10,30);
				jsonDeviceList = objRemoteApiServer.processRequest(
						objClient.getHostName(), "getDevices", null);
			} catch (Exception e1) {
				logger.error(e1.getMessage());
				try {
					retJson.put("status", "ERROR");
					retJson.put("message", "Client is not active");
				} catch (JSONException e) {
					logger.error("Error ", e);
				} finally {
					retData = retJson.toString();
				}
			}

			if (jsonDeviceList != null) {
				
				JSONArray devicesArray = new JSONArray();
				JSONArray existArray = new JSONArray();
				try {

					List<RegisteredDevice> deviceList = device
							.readJson(jsonDeviceList);
					for (int i = 0; i < deviceList.size(); i++) {
						try {

							DeviceHelper helper = new DeviceHelper();
							JSONObject deviceObject = new JSONObject();
							deviceObject.put("deviceId", deviceList.get(i)
									.getDeviceId());
							if (deviceList.get(i).getDeviceName()
									.equalsIgnoreCase("")) {
								deviceList.get(i).setDeviceName("Emulator");

							}
							deviceObject.put("deviceName", deviceList.get(i)
									.getDeviceName());
							deviceObject.put("deviceType", deviceList.get(i)
									.getDeviceType());
							deviceObject.put("platform", deviceList.get(i)
									.getPlatform());
							deviceObject.put("pltVersion", deviceList.get(i)
									.getPlatformVersion());
							devicesArray.put(deviceObject);
							int count = helper.hydrateDeviceWithId(deviceList
									.get(i).getDeviceId());
							if (count >= 1) {
								JSONObject exObj = new JSONObject();
								exObj.put("deviceId", deviceList.get(i)
										.getDeviceId());
								existArray.put(exObj);
							}
						} catch (DatabaseException e) {
							logger.error(e.getMessage());
						} catch (JSONException e) {
							logger.error(e.getMessage());
						}
					}

					try {
						JSONArray jArray = new JSONArray();
						ArrayList<RegisteredDevice> rdList = new DeviceHelper()
								.hydrateAllDevices();
						for (RegisteredDevice rd : rdList) {
							JSONObject jObj = new JSONObject();
							jObj.put("deviceId", rd.getDeviceId());
							jObj.put("deviceName", rd.getDeviceName());
							jObj.put("deviceType", rd.getDeviceType());
							jObj.put("platform", rd.getPlatform());
							jObj.put("pltVersion", rd.getPlatformVersion());
							jArray.put(jObj);
						}
						retJson.put("registeredDevices", existArray);
						retJson.put("devices", devicesArray);
						retJson.put("status", "SUCCESS");

					} catch (JSONException e) {
						logger.error(e.getMessage());
					} catch (DatabaseException e) {
						logger.error(e.getMessage());
					} finally {
						retData = retJson.toString();
					}

					// response.sendRedirect("DeviceServlet?param=FETCH_ALL_DEVICES");

				} catch (Exception e1) {
					logger.error(e1.getMessage());
					try {
						retJson.put("status", "ERROR");
						retJson.put("message", "Could not update details due to an internal error.");
					} catch (JSONException e) {
						
						logger.error("Error ", e);
					} finally {
						retData = retJson.toString();
					}
				}
			}

			response.getWriter().write(retData);
		}		
		
		else if (txn.equalsIgnoreCase("Delete_Devices")) {
			String devicesRecIds = request.getParameter("paramval");
			String[] deviceRecIdList = devicesRecIds.split(",");
			JSONObject retJson = new JSONObject();
			String retData = "";

			try {

				new DeviceHelper().removeAllDevices(deviceRecIdList);
				retJson.put("status", "SUCCESS");
				retJson.put("message", "All devices removed successfully");

			} catch (Exception e) {
				try {
					logger.error(e.getMessage());
					retJson.put("status", "ERROR");
					retJson.put("message", "Could not delete devices records");
				} catch (JSONException e1) {
					logger.error(e.getMessage());
				}

			} finally {
				retData = retJson.toString();
			}

			response.getWriter().write(retData);
		}
		else if (txn.equalsIgnoreCase("Delete_Device")) {
			String deviceRecId = request.getParameter("paramval");
			JSONObject retJson = new JSONObject();
			String retData = "";
			try {
				new DeviceHelper().removeDevice(deviceRecId);
				retJson.put("status", "SUCCESS");
				retJson.put("message", " Device removed successfully");

			} catch (Exception e) {
				try {
					logger.error(e.getMessage());
					retJson.put("status", "ERROR");
					retJson.put("message", "Could not delete device records");
				} catch (JSONException e1) {
					logger.error(e.getMessage());
				}
			} finally {
				retData = retJson.toString();
			}

			response.getWriter().write(retData);
		}
		else if (txn.equalsIgnoreCase("Add_Device")) {
			String deviceRecId = request.getParameter("paramval");
			String devArray = request.getParameter("devices");
			JSONObject retJson = new JSONObject();
			String retData = "";
			try {
				JSONArray jArray=new JSONArray(devArray);
				for(int i=0;i<jArray.length();i++)
				{
					JSONObject j = jArray.getJSONObject(i);

					if(deviceRecId.equalsIgnoreCase(j.getString("deviceId")))
					{
						RegisteredDevice rd= new RegisteredDevice();
						rd.setDeviceName(j.getString("deviceName"));
						rd.setDeviceId(j.getString("deviceId"));
						rd.setDeviceType(j.getString("deviceType"));
						rd.setPlatform(j.getString("platform"));
						rd.setPlatformVersion(j.getString("pltVersion"));
						rd.setDeviceStatus("Active");
						rd.setDeviceDescription("");
						rd.setRegisteredBy(tjnSession.getUser().getId());
						rd.setRegisteredOn(new Date());

						DeviceHelper helper=new DeviceHelper();
						int count= helper.hydrateDeviceWithId(rd.getDeviceId());
						if(!(count>=1))
						{
							helper.persistDevice(rd);
							retJson.put("status", "SUCCESS");
							retJson.put("message", " Device registered successfully"); 
						}
						else
						{
							retJson.put("status", "ERROR");   
							retJson.put("message", "Device exists");  
						}

					}
				}


			} catch (Exception e) {
				try {
					logger.error(e.getMessage());
					retJson.put("status", "ERROR");
					retJson.put("message", "Could not register device");
				} catch (JSONException e1) {
					logger.error(e.getMessage());
				}
			} finally {
				retData = retJson.toString();
			}

			response.getWriter().write(retData);
		}
		else if (txn.equalsIgnoreCase("Add_Devices")) {
			String deviceRecId = request.getParameter("paramval");
			String devArray = request.getParameter("devices");
			String[] deviceRecIdList = deviceRecId.split(",");
			JSONObject retJson = new JSONObject();
			String retData = "";

			try {
				JSONArray jArray=new JSONArray(devArray);
				for(int i=0;i<jArray.length();i++)
				{
					for(int j=0;j<deviceRecIdList.length;j++)
					{
						JSONObject j1 = jArray.getJSONObject(i);
						if(deviceRecIdList[j].equalsIgnoreCase(j1.getString("deviceId")))
						{
							RegisteredDevice rd= new RegisteredDevice();
							rd.setDeviceName(j1.getString("deviceName"));
							rd.setDeviceId(j1.getString("deviceId"));
							rd.setDeviceType(j1.getString("deviceType"));
							rd.setPlatform(j1.getString("platform"));
							rd.setPlatformVersion(j1.getString("pltVersion"));
							rd.setDeviceStatus("Active");
							rd.setDeviceDescription("");
							rd.setRegisteredBy(tjnSession.getUser().getId());
							rd.setRegisteredOn(new Date());
							DeviceHelper helper=new DeviceHelper();
							int count= helper.hydrateDeviceWithId(rd.getDeviceId());
							if(!(count>=1))
							{
								helper.persistDevice(rd);

							}
						}
					}
				}
				retJson.put("status", "SUCCESS");
				retJson.put("message", " All devices registered successfully");

			} catch (Exception e) {
				try {
					logger.error(e.getMessage());
					retJson.put("status", "ERROR");
					retJson.put("message", "Could not register devices");
				} catch (JSONException e1) {
					logger.error(e.getMessage());
				}

			} finally {
				retData = retJson.toString();
			}

			response.getWriter().write(retData);
		}
		//added by shivam sharma for  TENJINCG-904 starts
		else if(txn.equalsIgnoreCase("populateDevices")){
			logger.debug("fetch Devices list ");
			String retData = "";
			List<RegisteredDevice> devices=null;
			/* added by Sahana for pCloudy : starts  */
			String	deviceFarmFlag=	request.getParameter("deviceFarmCheck");
			String	clientIp= request.getParameter("clientIP");	
			/* added by Sahana for pCloudy : ends  */
			//Commented by Sahana 
			//String	clientname=	request.getParameter("cilentName");
			try {
				JSONObject json = new JSONObject();
				try {
					//Commented by Sahana 
					/*if(clientname!=null&&clientname.equalsIgnoreCase("Pcloudy")){
						devices=new Pcloudy().getDevices();
					}*/
					if(deviceFarmFlag!=null && deviceFarmFlag.equalsIgnoreCase("Y")){
						Pcloudy objPcloudy=new PcloudyProcessor().getPcloudyCredentials(clientIp);
						devices=new Pcloudy().getDevices(objPcloudy.getUsername(),objPcloudy.getKey());
					}else{
						devices=new DeviceHelper().hydrateAllDevicesWithStatus();
					}
						JSONArray jArray = new JSONArray();
						for (RegisteredDevice  device: devices) {
							JSONObject j = new JSONObject();
							j.put("deviceId", device.getDeviceId());
							j.put("deviceName", device.getDeviceName());
							j.put("recordId", device.getDeviceRecId());
							jArray.put(j);
						}
						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("devices", jArray);

				} catch (Exception e) {
					logger.error("An error occurred while fetching devices", e);
					json.put("status", "ERROR");
					json.put("message",
							"An internal error occurred. Please try again.");


				} finally {
					retData = json.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}
		//added by shivam sharma for  TENJINCG-904 ends
		
		/* added by Sahana for pCloudy : starts  */
		else if(txn.equalsIgnoreCase("GET_FILTERS")){
			logger.debug("Get pCloudy filters list ");
			String clientIp=request.getParameter("clientIP");
			String retData = "";
			try {
				JSONObject json = new JSONObject();
				try {
					Pcloudy objPcloudy=new PcloudyProcessor().getPcloudyCredentials(clientIp);
					JSONObject objPcloudyFilter=new PcloudyProcessor(objPcloudy).getAllFilters();
					json.put("status", "SUCCESS");
					json.put("Filters",objPcloudyFilter );
				} catch (Exception e) {
					logger.error("An error occurred while fetching pCloudy filters", e);
					json.put("status", "ERROR");
					json.put("message","Could not get device filters");

				} finally {
					retData = json.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}
		else if(txn.equalsIgnoreCase("GET_DEVICES_BY_FILTERS")){
			logger.debug("Get devices list based on pCloudy filters list ");
			String retData = "";
			String jsonString = request.getParameter("json");
			try {
				JSONObject json = new JSONObject();
				try {
					Map<String, String> filter = new HashMap<String, String>();
					JSONObject jObj = new JSONObject(jsonString);
					String osWithVersion=jObj.getString("os");
					String[] osVersionArr=osWithVersion.split(" ");
					filter.put("os", osVersionArr[0].trim());
					filter.put("manufacturer", jObj.getString("oem"));
					filter.put("display", jObj.getString("screenSize"));
					filter.put("network", jObj.getString("network"));
					filter.put("duration", "30");;
					filter.put("version", osWithVersion);
					filter.put("available", "true");
					Pcloudy objPcloudy=new PcloudyProcessor().getPcloudyCredentials(jObj.getString("clientIp").trim());
					JSONObject devicesJson=new PcloudyProcessor(objPcloudy).deviceListOnFilter(filter);
					json.put("status", "SUCCESS");
					json.put("Devices",devicesJson );
				} catch (Exception e) {
					logger.error("An error occurred while fetching devices from pCloudy filters", e);
					json.put("status", "ERROR");
					json.put("message","Could not get devices");

				} finally {
					retData = json.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}
		/* added by Sahana for pCloudy : ends  */
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		


	}

}
