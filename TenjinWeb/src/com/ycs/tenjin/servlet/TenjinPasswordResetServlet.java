/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TenjinPasswordResetServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION

 * 06-05-2019		newly Added by Roshni		TENJINCG-1036
 * 21-08-2019			Prem					TENJINCG-1100
 * 24-09-2019			Prem					TJN2.9R1-11
 * 30-09-2019			Prem					Sprint27-1


 */

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.UserHelperNew;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.TenjinMailHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class TenjinPasswordResetServlet
 */
@WebServlet("/TenjinPasswordResetServlet")
public class TenjinPasswordResetServlet extends HttpServlet {
	private static final Logger logger = LoggerFactory
			.getLogger(TenjinPasswordResetServlet.class);
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TenjinPasswordResetServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String param=request.getParameter("param");
		if(param.equalsIgnoreCase("resetreq")){
			
			/*modified by paneendra for VAPT fix starts*/
			/*String useremail=request.getParameter("usermail");*/
			String userid=request.getParameter("userid");
			String emailId=request.getParameter("email");
			/*String username=request.getParameter("username");*/
			User user=null;
			try {
				user=new UserHandler().getUser(userid);
				if(user!=null && user.getEmail().equals(emailId)) {
					/*modified by paneendra for VAPT fix ends*/
			StringBuffer url = request.getRequestURL();
			String uri = request.getRequestURI();
			String ctx = request.getContextPath();
			String baseUrl = url.substring(0, url.length() - uri.length() + ctx.length()) + "/";
			/*modified by paneendra for VAPT fix starts*/

			String useremail=user.getEmail();
			String username=user.getFirstName();
			/*modified by paneendra for VAPT fix ends*/
			user.setId(userid);
			user.setFirstName(username);
			user.setEmail(useremail);
			TenjinMailHandler handler=new TenjinMailHandler();
			JSONObject mailDetails=handler.getPasswordResetContent(user, baseUrl);
			
				handler.sendPasswordResetMail(mailDetails);
				/*added by shruthi for VAPT fixes starts*/
				 new UserHandler().updateCount(userid , "0");
				 /*added by shruthi for VAPT fixes ends*/
				SessionUtils.setScreenState("success", "pwd.resetmail.success", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("login.jsp")
				.forward(request, response);
				}/*added by paneendra for VAPT fix starts*/
				else {
					SessionUtils.setScreenState("success", "pwd.resetmail.success", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.getRequestDispatcher("login.jsp")
					.forward(request, response);
				}
			}catch (RecordNotFoundException e) {
				SessionUtils.setScreenState("success", "pwd.resetmail.success", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("login.jsp")
				.forward(request, response);
				logger.error("user is not found");
			}/*modified by paneendra for VAPT fix ends*/
			catch (TenjinConfigurationException e) {
				SessionUtils.setScreenState("error", "mail.config.not.found", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("login.jsp")
				.forward(request, response);
			}
			/*added by shruthi for VAPT fixes starts*/
			catch (DatabaseException e) {
				logger.error("Error ", e);
			}
			/*added by shruthi for VAPT fixes ends*/
			
			
			
		}
		else if(param.equalsIgnoreCase("resetpwdscreen")){
		
			String userid=request.getParameter("userid");
			String userName=request.getParameter("username");
			request.setAttribute("userid", userid);
			/*Added by Prem for TENJINCG-1100 start*/
			request.setAttribute("username", userName);
			Map<String,String> securityQueAndAns=null;
			try {
				/*Modified by Prem for TJN2.9R1-11 start */
				securityQueAndAns=new UserHandler().hydrateSecurityQuestionForUser(userid);
				logger.debug(" securityQueAndAns " + securityQueAndAns);
				/*Modified by Prem for TJN2.9R1-11 End */
			} catch (SQLException | DatabaseException e) {
				
				logger.error("Error ", e);
			}
			request.getSession().setAttribute("securityQueAndAns", securityQueAndAns);
			/*Added by Prem for TENJINCG-1100 Ends*/
			SessionUtils.loadScreenStateToRequest(request);
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("changepassword.jsp");
			dispatcher.forward(request, response);
			
		}
		else if(param.equalsIgnoreCase("redirectLoginPage")){
			StringBuffer url = request.getRequestURL();
			String uri = request.getRequestURI();
			String ctx = request.getContextPath();
			String baseUrl = url.substring(0, url.length() - uri.length() + ctx.length()) + "/";
			response.sendRedirect(baseUrl);
		}/*Added by paneendra for VAPT fix starts*/
		  else if(param.equalsIgnoreCase("reqresetpassword")){
			SessionUtils.loadScreenStateToRequest(request);
			request.getRequestDispatcher("resetpassword.jsp")
			.forward(request, response);
		}/*Added by paneendra for VAPT fix ends*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String userid=request.getParameter("userId");
		String newPassword=request.getParameter("newPassword");
		String confirmPassword=request.getParameter("confirmPassword");
		String changeReason="Password Reset Through Mail";
		String dispatchUrl = "";
		String question=request.getParameter("question");
		String answer=request.getParameter("answer");
		/*Added by Prem for TENJINCG-1100 start*/
		String userName=request.getParameter("userName");
		Map<String, String> map = null;
		try {
			/*Modified by Prem for Sprint27-1 start */
			map = new UserHandler().hydrateSecurityQuestionForUser(userid);
			/*Modified by Prem for Sprint27-1 start */
		} catch (SQLException | DatabaseException e1) {
			
			logger.error("Error ", e1);
		}
		if(Utilities.trim(answer).equals("")){
			SessionUtils.setScreenState("error", "password.empty.fail", request);
			request.setAttribute("login", "fail");
			request.getSession().setAttribute("securityQueAndAns", map);
			request.setAttribute("securityQueAndAns",map);
			SessionUtils.loadScreenStateToRequest(request);
			request.getRequestDispatcher("changepassword.jsp?userid="+userid+"&username="+userName).forward(
					request, response);
		}
		
		else{
			boolean securityMatch=false;
			for (Map.Entry<String, String> entry : map.entrySet()) {
			   if(Utilities.trim(question).equals(entry.getKey()) && Utilities.trim(answer).equals(entry.getValue())){
				   securityMatch=true;		
			   }
			}
			
			if(!securityMatch){
				try {
					new UserHelperNew().blockUser(userid);
				} catch (DatabaseException e) {
					
					logger.error("Error ", e);
				}
				SessionUtils.setScreenState("error", "answer.wrong", request);
				request.setAttribute("login", "fail");
				request.getSession().setAttribute("securityQueAndAns", map);
				request.setAttribute("securityQueAndAns",map);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("changepassword.jsp?userid="+userid+"&username="+userName).forward(
						request, response);
			}
			else{
				/*Added by Prem for TENJINCG-1100 Ends*/	
				try {
					new UserHandler().updateUserPassword("",newPassword,confirmPassword,userid,"",changeReason);
					
					SessionUtils.setScreenState("success", "user.password.update.success", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.getRequestDispatcher("changepassword.jsp?userid="+userid).forward(request, response);
					
				} catch (InvalidKeyException e) {
					
					logger.error("Error ", e);
				} catch (NoSuchAlgorithmException e) {
					
					logger.error("Error ", e);
				} catch (NoSuchPaddingException e) {
					
					logger.error("Error ", e);
				} catch (IllegalBlockSizeException e) {
					
					logger.error("Error ", e);
				} catch (BadPaddingException e) {
					
					logger.error("Error ", e);
				} catch (DatabaseException e) {
					logger.error(e.getMessage(), e);
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.getRequestDispatcher("changepassword.jsp?userid="+userid).forward(request, response);
				} catch (RequestValidationException e) {
					request.getSession().setAttribute("securityQueAndAns", map);
					dispatchUrl="changepassword.jsp?userid="+userid;
					SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.getRequestDispatcher(dispatchUrl).forward(request, response);
				}
				
				
			}
		}
		
		
		
		
			
	
	}

}
