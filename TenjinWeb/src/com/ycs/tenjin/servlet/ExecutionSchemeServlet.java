/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutionSchemeServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ExecutionSchemeHelper;
import com.ycs.tenjin.db.LearningHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.project.ExecutionScheme;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class ExecutionSchemeServlet
 */
//@WebServlet("/ExecutionSchemeServlet")
public class ExecutionSchemeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(ExecutionSchemeServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExecutionSchemeServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String txn = request.getParameter("param");
		if(txn != null && txn.equalsIgnoreCase("init_view_new")){
			request.getSession().removeAttribute("EXEC_SCHM_MAP");
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				logger.info("Hydrating all AUTs");
				ArrayList<Aut> allAuts = new AutHelper().hydrateAllAut();
				logger.info("Done");
				map.put("STATUS", "SUCCESS");
				map.put("MESSAGE","");
				map.put("AUTS", allAuts);
			} catch (DatabaseException e) {
				logger.error("ERROR while fetching AUT list",e);
				map.put("STATUS","ERROR");
				map.put("MESSAGE","Could not load AUTs due to an internal error");
			}finally{
				request.getSession().setAttribute("EXEC_SCHM_MAP", map);
				logger.info("Forwarding...");
				RequestDispatcher dispatcher = request.getRequestDispatcher("exec_rule_new.jsp");
				dispatcher.forward(request, response);
			}
		}else if(txn.equalsIgnoreCase("init_view_list")){
			request.getSession().removeAttribute("EXEC_SCHM_MAP");
			Map<String, Object> map = new HashMap<String, Object>();
			String a = request.getParameter("a");
			String funcCode = request.getParameter("f");
			String bPageAreaName =request.getParameter("p");
			try{
				int appId = 0;
				if(a != null){
					try{
						appId = Integer.parseInt(a);
					}finally{
						
					}
				}
				
				
				logger.info("Hydrating all available schemes");
				ArrayList<ExecutionScheme> schemes = new ExecutionSchemeHelper().hydrateSchemes(appId, funcCode, bPageAreaName);
				logger.info("Done");
				logger.info("Hydrating all AUTs");
				ArrayList<Aut> allAuts = new AutHelper().hydrateAllAut();
				logger.info("Done");
				map.put("SCHM_LIST", schemes);
				map.put("STATUS","SUCCESS");
				String message = request.getParameter("message");
				if(message != null && !message.equalsIgnoreCase("")){
					map.put("MESSAGE",message);
				}else{
					map.put("MESSAGE","");
				}
				map.put("SEL_AUT", a);
				map.put("SEL_FUNC", funcCode);
				map.put("SEL_PAGE_AREA", bPageAreaName);
				map.put("AUTS", allAuts);
			}catch(Exception e){
				logger.error("ERROR while fetching Schemes list",e);
				map.put("STATUS","ERROR");
				map.put("MESSAGE","Could not load rules due to an internal error");
			}finally{
				request.getSession().setAttribute("EXEC_SCHM_MAP",map);
				RequestDispatcher dispatcher = request.getRequestDispatcher("exec_rule_list.jsp");
				dispatcher.forward(request, response);
			}
			
		}else if(txn.equalsIgnoreCase("fetch_page_areas")){
			String a = request.getParameter("a");
			String f = request.getParameter("f");
			
			int appId = Integer.parseInt(a);
			ArrayList<Location> pageAreas = null;
			
			LearningHelper lHelper = new LearningHelper();
			try{
				pageAreas = lHelper.hydrateLocations(f, appId);
			}catch(Exception e){
				logger.error("ERROR hydrating page areas",e);
			}
			
			String ret = "";
			if(pageAreas != null){
				try{
					JSONArray jArray = new JSONArray();
					for(Location page:pageAreas){
						JSONObject json = new JSONObject();
						json.put("name",page.getLocationName());
						json.put("type", page.getLocationType());
						json.put("parent", page.getParent());
						jArray.put(json);
					}
					JSONObject json = new JSONObject();
					json.put("status", "success");
					json.put("pages",jArray);
					ret = json.toString();
				}catch(JSONException e){
					logger.error("ERROR generating JSON for Page areas",e);
					ret = "{status:error,message:An internal error occurred while fetching page areas}";
				}
			}else{
				ret = "{status:error,message:An internal error occurred while fetching page areas}";
			}
			
			response.getWriter().write(ret);
		}else if(txn.equalsIgnoreCase("fetch_fields")){
			String a = request.getParameter("a");
			String f = request.getParameter("f");
			String p = request.getParameter("p");
			int appId = Integer.parseInt(a);
			ArrayList<TestObject> testObjects = null;
			
			LearningHelper lHelper = new LearningHelper();
			try{
				testObjects =lHelper.hydrateTestObjectsOnPage(f, appId, p);
			}catch(Exception e){
				logger.error("ERROR hydrating fields",e);
			}
			
			String ret = "";
			if(testObjects != null){
				try{
					JSONArray jArray = new JSONArray();
					for(TestObject t:testObjects){
						JSONObject json = new JSONObject();
						json.put("uname",t.getName());
						json.put("label", t.getLabel());
						json.put("type", t.getObjectClass());
						json.put("uid",t.getUniqueId());
						json.put("page", t.getLocation());
						jArray.put(json);
					}
					JSONObject json = new JSONObject();
					json.put("status", "success");
					json.put("fields",jArray);
					ret = json.toString();
				}catch(JSONException e){
					logger.error("ERROR generating JSON for fields",e);
					ret = "{status:error,message:An internal error occurred while fetching fields}";
				}
			}else{
				ret = "{status:error,message:An internal error occurred while fetching fields}";
			}
			
			response.getWriter().write(ret);
		}else if(txn.equalsIgnoreCase("view_scheme")){
			String sId = request.getParameter("sid");
			int schemeId = Integer.parseInt(sId);
			String message = request.getParameter("message");
			request.getSession().removeAttribute("EXEC_SCHM_MAP");
			Map<String,Object> map = new HashMap<String,Object>();
			Connection conn = null;
			try{
				conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				ExecutionScheme scheme = new ExecutionSchemeHelper().hydrateScheme(conn, schemeId);
				map.put("SCHEME", scheme);
				logger.info("Hydrating all AUTs");
				ArrayList<Aut> allAuts = new AutHelper().hydrateAllAut();
				logger.info("Done");
				logger.info("Hydrating functions for app {}", scheme.getAppId());
				ArrayList<ModuleBean> modules = new ModulesHelper().hydrateModules(conn, scheme.getAppId());
				logger.info("Done");
				logger.info("Hydrating page areas for function {}", scheme.getFunctionCode());
				LearningHelper lHelper = new LearningHelper();
				ArrayList<Location> pageAreas = lHelper.hydrateLocations(conn, scheme.getFunctionCode(), scheme.getAppId());
				logger.info("Done");
				logger.info("Hydrating Fields for page area {}", scheme.getBasePageArea());
				ArrayList<TestObject> testObjects =lHelper.hydrateTestObjectsOnPage(scheme.getFunctionCode(), scheme.getAppId(), scheme.getBasePageArea());
				logger.info("Done");
				
				map.put("SCHEME", scheme);
				map.put("AUTS", allAuts);
				map.put("FUNCS", modules);
				map.put("PAGES", pageAreas);
				map.put("FIELDS", testObjects);
				map.put("STATUS", "SUCCESS");
				if(message != null && !message.equalsIgnoreCase("")){
					map.put("MESSAGE", message);
				}
			}catch(Exception e){
				logger.error("Could not prepare rule details view",e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "An internal error occurred. Please contact your system administrator");
			}finally{
				DatabaseHelper.close(conn);
				request.getSession().setAttribute("EXEC_SCHM_MAP", map);
				RequestDispatcher dispatcher = request.getRequestDispatcher("exec_rule_detail.jsp");
				dispatcher.forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String txn = request.getParameter("exec_schm_action");
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		
		logger.info("Reading form and building scheme");
		ExecutionScheme scheme = null;
		request.getSession().removeAttribute("EXEC_SCHM_MAP");
		request.getSession().removeAttribute("EXEC_SCHM_UNSAVED_BEAN");
		try{
			scheme = new ExecutionScheme();
			String schemeId = request.getParameter("txtId");
			if(schemeId != null && Utilities.isNumeric(schemeId)){
				scheme.setId(Integer.parseInt(schemeId));
			}else{
				logger.warn("Scheme ID is 0");
			}
			scheme.setName(request.getParameter("txtName"));
			scheme.setDescription(request.getParameter("txtDesc"));
			String a = request.getParameter("lstApplication");
			scheme.setAppId(Integer.parseInt(a));
			scheme.setFunctionCode(request.getParameter("lstModules"));
			String ruleJson = request.getParameter("ruleConfig");
			JSONObject j = null;
			int sequence=0;
			j = new JSONObject(ruleJson);
			JSONArray actions = j.getJSONArray("action");
			JSONArray preActions = j.getJSONArray("pre");
			JSONArray postActions = j.getJSONArray("post");
			ArrayList<TestObject> actionsList = new ArrayList<TestObject>();
			ArrayList<TestObject> preActionsList = new ArrayList<TestObject>();
			ArrayList<TestObject> postActionsList = new ArrayList<TestObject>();
			if(actions != null && actions.length() > 0){
				JSONObject action = actions.getJSONObject(0);
				scheme.setBasePageArea(action.getString("page"));
				sequence=0;
				for(int i=0;i<actions.length();i++){
					JSONObject act = actions.getJSONObject(i);
					TestObject t = new TestObject();
					sequence++;
					t.setSequence(sequence);
					t.setLocation(act.getString("page"));
					t.setUniqueId(act.getString("uid"));
					t.setLabel(act.getString("uname"));
					t.setObjectClass(act.getString("type"));
					t.setAction(act.getString("action"));
					actionsList.add(t);
				}
			}else{
				logger.error("No Actions Found for this rule. Persistence will be aborted");
				scheme.setActions(null);
				throw new ExecutorException("No actions found for this rule. This rule cannot be saved");
			}
			
			if(preActions != null && preActions.length() > 0){
				sequence=0;
				for(int i=0;i<preActions.length();i++){
					JSONObject act = preActions.getJSONObject(i);
					TestObject t = new TestObject();
					sequence++;
					t.setSequence(sequence);
					t.setLocation(act.getString("page"));
					t.setUniqueId(act.getString("uid"));
					t.setLabel(act.getString("uname"));
					t.setObjectClass(act.getString("type"));
					t.setAction(act.getString("action"));
					preActionsList.add(t);
				}
			}
			
			if(postActions != null && postActions.length() > 0){
				sequence=0;
				for(int i=0;i<postActions.length();i++){
					JSONObject act = postActions.getJSONObject(i);
					TestObject t = new TestObject();
					sequence++;
					t.setSequence(sequence);
					t.setLocation(act.getString("page"));
					t.setUniqueId(act.getString("uid"));
					t.setLabel(act.getString("uname"));
					t.setObjectClass(act.getString("type"));
					t.setAction(act.getString("action"));
					postActionsList.add(t);
				}
			}
			
			scheme.setActions(actionsList);
			scheme.setPreActions(preActionsList);
			scheme.setPostActions(postActionsList);
		}catch(Exception e){
			logger.error("ERROR creating scheme object",e);
			scheme = null;
		}
		if(txn != null && txn.equalsIgnoreCase("new")){
			//RequestDispatcher dispatcher = null;
			String dUrl = "";
			Map<String, Object> map = new HashMap<String, Object>();
			if(scheme != null){
				scheme.setEnabled(true);//Enable the scheme by default
				scheme.setCreatedBy(tjnSession.getUser().getId());
				
				map.put("EXEC_SCHM_UNSAVED_BEAN", scheme);
				
				ExecutionSchemeHelper helper = new ExecutionSchemeHelper();
				try {
					helper.persistScheme(scheme);
					logger.info("Scheme created successfully");
					map.remove("EXEC_SCHM_UNSAVED_BEAN");
					map.put("STATUS","SUCCESS");
					map.put("MESSAGE","Rule created successfully");
					dUrl = "ExecutionSchemeServlet?param=view_scheme&sid=" + scheme.getId() + "&message=Rule created successfully";
				} catch (DatabaseException e) {
					logger.error("ERROR creating scheme",e);
					map.put("STATUS","ERROR");
					map.put("MESSAGE", "Could not create rule because of an internal error");
					dUrl = "exec_rule_new.jsp";
				}
			}else{
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "Could not create rule because of an internal error");
				logger.error("Could not persist scheme as scheme object was not created");
				dUrl = "exec_rule_new.jsp";
			}
			logger.info("Forwarding...");
			request.getSession().setAttribute("EXEC_SCHM_MAP", map);
			response.sendRedirect(dUrl);
		}else if(txn != null && txn.equalsIgnoreCase("update")){
			RequestDispatcher dispatcher = null;
			Map<String, Object> map = new HashMap<String, Object>();
			String dUrl = "";
			if(scheme != null){
				scheme.setEnabled(true);//Enable the scheme by default
				scheme.setCreatedBy(tjnSession.getUser().getId());

				map.put("EXEC_SCHM_UNSAVED_BEAN", scheme);

				ExecutionSchemeHelper helper = new ExecutionSchemeHelper();
				try {
					helper.updateScheme(scheme);
					logger.info("Scheme updated successfully");
					map.remove("EXEC_SCHM_UNSAVED_BEAN");
					map.put("STATUS","SUCCESS");
					map.put("MESSAGE","Rule updated successfully");
					dUrl = "ExecutionSchemeServlet?param=view_scheme&sid=" + scheme.getId() + "&message=Rule updated successfully";
				} catch (DatabaseException e) {
					logger.error("ERROR update scheme",e);
					map.put("STATUS","ERROR");
					map.put("MESSAGE", "Could not update rule because of an internal error");
					dispatcher = request.getRequestDispatcher("exec_rule_detail.jsp");
					dUrl = "exec_rule_detail.jsp";
				}
			}else{
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "Could not create rule because of an internal error");
				logger.error("Could not persist scheme as scheme object was not created");
				dispatcher = request.getRequestDispatcher("exec_rule_detail.jsp");
				dUrl = "exec_rule_detail.jsp";
			}
			logger.info("Forwarding...");
			request.getSession().setAttribute("EXEC_SCHM_MAP", map);
			//dispatcher.forward(request, response);
			response.sendRedirect(dUrl);
		}else if(txn.equalsIgnoreCase("delete")){
			Map<String, Object> map = new HashMap<String, Object>();
			String dUrl = "";
			
			if(scheme != null){
				map.put("EXEC_SCHM_UNSAVED_BEAN", scheme);

				ExecutionSchemeHelper helper = new ExecutionSchemeHelper();
				try {
					helper.deleteScheme(scheme.getId());
					logger.info("Scheme removed successfully");
					map.remove("EXEC_SCHM_UNSAVED_BEAN");
					map.put("STATUS","SUCCESS");
					map.put("MESSAGE","Rule removed successfully");
					dUrl = "ExecutionSchemeServlet?param=init_view_list&message=Rule removed successfully";
				} catch (DatabaseException e) {
					logger.error("ERROR update scheme",e);
					map.put("STATUS","ERROR");
					map.put("MESSAGE", "Could not remove rule because of an internal error");
					dUrl = "exec_rule_detail.jsp";
				}
			}else{
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "Could not remove rule because of an internal error");
				logger.error("Could not persist scheme as scheme object was not created");
				dUrl = "exec_rule_detail.jsp";
			}
			
			logger.info("Forwarding...");
			request.getSession().setAttribute("EXEC_SCHM_MAP", map);
			//dispatcher.forward(request, response);
			response.sendRedirect(dUrl);
		}else if(txn.equalsIgnoreCase("delete_all_rules")){
			Map<String, Object> map = new HashMap<String, Object>();
			String dUrl = "";
			
			try{
				String schemeIds = request.getParameter("sel_rule_ids");
				if(schemeIds != null && !schemeIds.equalsIgnoreCase("")){
					String[] split = schemeIds.split(",");
					ArrayList<Integer> schemes = new ArrayList<Integer>(); 
					for(String s:split){
						schemes.add(Integer.parseInt(s));
					}

					ExecutionSchemeHelper helper = new ExecutionSchemeHelper();
					logger.info("Calling DAO to clear schemes");
					helper.deleteSchemes(schemes);
					logger.info("Schemes cleared successfully");
					map.put("STATUS", "SUCCESS");
					map.put("MESSAGE", "The selected rule(s) were removed successfully");
					dUrl = "ExecutionSchemeServlet?param=init_view_list&message=The selected rule(s) were removed successfully";
				}else{
					logger.error("Request param sel_rule_ids is null or empty");
					map.put("STATUS", "ERROR");
					map.put("MESSAGE","No rules were found to delete");
					dUrl = "exec_rule_list.jsp";
				}

			}catch(Exception e){
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "Could not remove rule(s) because of an internal error");
				logger.error("Could not remove rule(s) because of an internal error");
				// = request.getRequestDispatcher("exec_rule_detail.jsp");
				dUrl = "exec_rule_detail.jsp";
			}finally{
				request.getSession().setAttribute("EXEC_SCHM_MAP", map);
				logger.info("Forwarding...");
				response.sendRedirect(dUrl);
			}
		}else if(txn.equalsIgnoreCase("disable")){
			Map<String, Object> map = new HashMap<String, Object>();
			String dUrl = "";
			
			if(scheme != null){
				ExecutionSchemeHelper helper = new ExecutionSchemeHelper();
				try {
					helper.disableScheme(scheme.getId());
					logger.info("Scheme disabled successfully");
					map.remove("EXEC_SCHM_UNSAVED_BEAN");
					map.put("STATUS","SUCCESS");
					map.put("MESSAGE","Rule disabled successfully");
					dUrl = "ExecutionSchemeServlet?param=view_scheme&sid=" + scheme.getId() + "&message=Rule disabled successfully";
				} catch (DatabaseException e) {
					logger.error("ERROR disabling scheme",e);
					map.put("STATUS","ERROR");
					map.put("MESSAGE", "Could not disable rule because of an internal error");
					dUrl = "exec_rule_detail.jsp";
				}
			}else{
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "Could not disable rule because of an internal error");
				logger.error("Could not disable scheme as scheme object was not created");
				dUrl = "exec_rule_detail.jsp";
			}
			
			logger.info("Forwarding...");
			request.getSession().setAttribute("EXEC_SCHM_MAP", map);
			response.sendRedirect(dUrl);
		}else if(txn.equalsIgnoreCase("enable")){
			Map<String, Object> map = new HashMap<String, Object>();
			String dUrl = "";
			
			if(scheme != null){
				ExecutionSchemeHelper helper = new ExecutionSchemeHelper();
				try {
					helper.enableScheme(scheme.getId());
					logger.info("Scheme enabled successfully");
					map.remove("EXEC_SCHM_UNSAVED_BEAN");
					map.put("STATUS","SUCCESS");
					map.put("MESSAGE","Rule enabled successfully");
					dUrl = "ExecutionSchemeServlet?param=view_scheme&sid=" + scheme.getId() + "&message=Rule enabled successfully";
				} catch (DatabaseException e) {
					logger.error("ERROR enabling scheme",e);
					map.put("STATUS","ERROR");
					map.put("MESSAGE", "Could not enable rule because of an internal error");
					dUrl = "exec_rule_detail.jsp";
				}
			}else{
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "Could not enable rule because of an internal error");
				logger.error("Could not enable scheme as scheme object was not created");
				dUrl = "exec_rule_detail.jsp";
			}
			
			logger.info("Forwarding...");
			request.getSession().setAttribute("EXEC_SCHM_MAP", map);
			response.sendRedirect(dUrl);
		}
	}

}
