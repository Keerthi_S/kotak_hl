/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UserLandingServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright  2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 12-10-2018        	Leelaprasad P         	newly added for TENJINCG-872 
* 19-10-2018           	Leelaprasad             TENJINCG-829
* 25-10-2018            Leelaprasad             re written for TENJINCG-826,TENJINCG-828
* 26-10-2018           	Padmavathi              TENJINCG-824
* 14-10-2018			Prem					TJNUN262-57 
* 22-12-2018			Prem					TJNUN262-57
* 11-03-2019			Preeti					TENJINCG-966
* 02-04-2019           	Padmavathi              TNJN27-21
* 22-06-2019            Leelaprasad P           V2.8-4
* 
*/

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.user.User;

/**
 * Servlet implementation class UserLandingServlet
 */
public class UserLandingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(UserLandingServlet.class);
	
	Date endDate = null;
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	Calendar calendar = Calendar.getInstance();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserLandingServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Map<String, Object> map = new HashMap<String, Object>();
			String key=request.getParameter("key");
			int projectId=0;
			User user=tjnSession.getUser();
			String prefrence=null;
			if(key==null || !key.equalsIgnoreCase("first")){
				projectId=new ProjectHandler().hydrateUserProjectPreference(user.getId());
				 if(projectId!=0){
					 prefrence="Y";
				 }
                  if(projectId==0 || key.equalsIgnoreCase("landing")){
                	  if(projectId==0){
                	  prefrence="N";
                	  }
				  ProjectHandler projectHandler=new ProjectHandler();
					try {
						ArrayList<Project> projects = projectHandler
								.hydrateAllProjectsForUser(user.getId());
						map.put("STATUS", "SUCCESS");
						if (projects != null && projects.size() > 0) {
							/*Modified by Preeti for TENJINCG-966 starts*/
							/*Multimap<String, Object> projectMap = ArrayListMultimap
									.create();*/
							Multimap<String, Object> projectMap = LinkedListMultimap
									.create();
							/*Modified by Preeti for TENJINCG-966 ends*/
							/*Added by Padmavathi for  TNJN27-21 starts*/
							String domainName=null;
							/*Added by Padmavathi for  TNJN27-21 ends*/
							for (Project project : projects) {
								projectMap.put(project.getDomain(), project);
								/*Added by Padmavathi for  TNJN27-21 starts*/
								if(domainName==null){
									domainName=project.getDomain();
								}
								/*Added by Padmavathi for  TNJN27-21 ends*/
							}
							/*Added by Padmavathi for  TNJN27-21 starts*/
							Collection<Object> prjs = projectMap.get(domainName);
							map.put("PRJS", prjs);
							/*Added by Padmavathi for  TNJN27-21 ends*/
							map.put("PRJ_MAP", projectMap);
						} else {
							map.put("PRJ_MAP", null);
						}
						map.put("MESSAGE", "");

					} catch (DatabaseException e) {
						map.put("STATUS", "ERROR");
						map.put("MESSAGE", e.getMessage());
					}
                  }
                 
                  map.put("prjPrefrence", prefrence);
			request.getSession().setAttribute("SCR_MAP", map);
			
		   
			
			}
			
			if(prefrence!=null && prefrence.equalsIgnoreCase("Y") && key!=null && !key.equalsIgnoreCase("landing")){
					
					if(key!=null && key.equalsIgnoreCase("login") ||key.equalsIgnoreCase("gotoProject")){
					/*Modified by Padmavathi for TENJINCG-824 starts*/
						try {
							boolean checkPrjAccess= new ProjectHandler().checkProjectAccessForuser(tjnSession.getUser().getId(), projectId);
								if(checkPrjAccess){
									response.sendRedirect("ProjectServletNew?t=project_launch&prefrence=Y&projectId="+projectId);
								} else{
									new ProjectHandler().updateUserProjectPrefrence(0, tjnSession.getUser().getId());
									
								request.getRequestDispatcher("v2/project_access_denial.jsp").forward(request, response);
							
								}
							} catch (DatabaseException e) {
							
							logger.error(e.getMessage());
							logger.error("Error ", e);
							request.getRequestDispatcher("landing_new.jsp").forward(request, response);
						}
						/*Modified by Padmavathi for TENJINCG-824 ends*/
				
					}
					
			}else if(prefrence!=null && prefrence.equalsIgnoreCase("N") &&key!=null && key.equalsIgnoreCase("login")|| key.equalsIgnoreCase("gotoProject")){
				request.getRequestDispatcher("landing_new.jsp").forward(
							request, response);
			}else if(key!=null && key.equalsIgnoreCase("landing")){
				try {
					if(tjnSession !=null && tjnSession.getProject()!=null &&tjnSession.getUser()!=null){
					new ProjectHandler().endProjectSession(tjnSession.getProject().getId(), tjnSession.getUser().getId());
					}
				} catch (DatabaseException e) {
					
					logger.error("Error ", e);
				}
				
				request.getRequestDispatcher("landing_new.jsp").forward(
						request, response);
			}else{
			 /*Changed by prem for V2.8-4 starts*/
				request.getSession().setAttribute("SCR_MAP", map);
				map.put("STATUS1", "SUCCESS");
				map.put("MESSAGE1", "Password Changed Successfully");
				 /*Changed by prem for V2.8-4 starts*/
				request.getRequestDispatcher("landing_new.jsp").forward(
						request, response);
			}
			
	
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.getRequestDispatcher("landing_new.jsp").forward(
				request, response);
	}

}
