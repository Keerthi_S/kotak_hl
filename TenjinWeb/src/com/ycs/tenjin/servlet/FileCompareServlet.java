/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  FileCompareServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.project.ProjectTestDataPath;
import com.ycs.tenjin.util.FileView;

public class FileCompareServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = LoggerFactory
			.getLogger(FileCompareServlet.class);

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, java.io.IOException {

		
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)	throws ServletException, java.io.IOException {
		
		String txn = request.getParameter("param");
		
		if(txn.equalsIgnoreCase("SWIFT")){
		
		String a=request.getParameter("a");
		int appId = Integer.parseInt(a);
		String m=request.getParameter("m");
		/*String filePath = "D:/Tenjin_Test_Data/SWIFT/";*/
		String filePath = "";
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		
		for(ProjectTestDataPath p:tjnSession.getProject().getTestDataPaths()){
			if(p.getAppId() == appId && p.getFuncCode().equalsIgnoreCase(m)){
				filePath = p.getTestDataPath();
			}
		}
		
		filePath = filePath + "\\SWIFT\\";
		String filename1 = request.getParameter("file1");
		String filename2 = request.getParameter("file2");
				
		FileView test = new FileView();
		String expFile = test.fileInput(filePath + "" + filename1);
		String actFile = test.fileInput(filePath + "" + filename2);

        String headerActFile = null;
        String headerExpFile = null;
        String bodyBlkActFile = null;
        String bodyBlkExpFile = null;

        String msgType = "";
        
        if((!expFile.replace("\n", "").startsWith("{") && actFile.replace("\n", "").startsWith("{")) || (expFile.replace("\n", "").startsWith("{") && !actFile.replace("\n", "").startsWith("{")))  {
        
        	request.getSession().setAttribute("ACTFILE",actFile);
    		request.getSession().setAttribute("EXPFILE",expFile);
    		RequestDispatcher dispatcher = request.getRequestDispatcher("compare2.jsp");
    		dispatcher.forward(request, response);
        }
        else{
        	
        if(expFile.replace("\n", "").startsWith("{") && actFile.replace("\n", "").startsWith("{"))  {
        	
        	msgType = "TAG";
        	
        	headerActFile = StringUtils.substringBefore(actFile,"{4:");
        	headerExpFile = StringUtils.substringBefore(expFile,"{4:");
                        
        	bodyBlkActFile = StringUtils.substringAfter(actFile,"{4:");
            bodyBlkExpFile = StringUtils.substringAfter(expFile,"{4:");           	        	                                               
        }
        else if (!expFile.replace("\n", "").startsWith("{") && !actFile.replace("\n", "").startsWith("{"))  {
        	
        	msgType = "DESC";
        	headerActFile = StringUtils.substringBefore(actFile,"----------------------------------Message Text----------------------------------\n");
        	headerExpFile = StringUtils.substringBefore(expFile,"----------------------------------Message Text----------------------------------\n"); 
            
            bodyBlkActFile = StringUtils.substringAfter(actFile,"----------------------------------Message Text----------------------------------\n");
            bodyBlkExpFile = StringUtils.substringAfter(expFile,"----------------------------------Message Text----------------------------------\n");                
                       
        }

		String[] expHeaderCont = headerExpFile.split("\n");
		String[] actHeaderCont = headerActFile.split("\n");
		ArrayList<Integer> indx = new ArrayList<Integer>();
		ArrayList<Integer> diffheader = new ArrayList<Integer>();
		ArrayList<Integer> diffbody = new ArrayList<Integer>(); 
		Map<String,Object> map = new HashMap<String, Object>();
		Integer hdIndx = 0;
		
		if(actHeaderCont.length > expHeaderCont.length || actHeaderCont.length == expHeaderCont.length){
			hdIndx = actHeaderCont.length; 
		}
		else{
			hdIndx = expHeaderCont.length;
		}
		
		
		for(int i=0;i<actHeaderCont.length;i++){			
			diffheader.add(StringUtils.indexOfDifference(actHeaderCont[i], expHeaderCont[i]));
		}        
		                
        Map<Integer,String> bodyActFileCont = new LinkedHashMap<Integer,String>();
        Map<Integer,Object> bodyExpFileCont = new LinkedHashMap<Integer,Object>();      
        
        String actBodyContent[] = new String [38];
        String expBodyContent[] = new String [38];
        
		String[] mt103Fields = {":20:",":13C:",":23B:",":23E:",":26T:",":32A:",":33B:",":36:",":50A:",":50F:",":50K:",":51A:",":52A:",":52D:",":53A:",":53B:",":53D:",":54A:",":54B:",":54D",":55A",":55B",":55D",":56A",":56C",":56D",":57A:",":57B:",":57C:",":57D:",":59:",":59A:",":70:",":71A:",":71F:",":71G:",":72:",":77B:"};
        
		bodyBlkActFile = bodyBlkActFile.replace("\n", "");
		bodyBlkExpFile = bodyBlkExpFile.replace("\n", "");
		
		String tmpActBodyContent[] = new String [38];
		String tmpExpBodyContent[] = new String [38];
		
		for(int i=0;i<mt103Fields.length;i++){
			for(int j=i;j<mt103Fields.length;j++){

				if(bodyBlkActFile.indexOf(mt103Fields[i]) != -1 && bodyBlkActFile.indexOf(mt103Fields[j]) != -1  ){
					tmpActBodyContent[i] = bodyBlkActFile.substring(bodyBlkActFile.indexOf(mt103Fields[i]), bodyBlkActFile.indexOf(mt103Fields[j]));
					if(tmpActBodyContent[i].length() > 0){				
						actBodyContent[i] =tmpActBodyContent[i];
						break;
					}
				}
				if(i == (mt103Fields.length -1)){
					for(int k = i;;k--){
						
						if(bodyBlkActFile.indexOf(mt103Fields[k]) != -1){
							tmpActBodyContent[i] = bodyBlkActFile.substring(bodyBlkActFile.indexOf(mt103Fields[k]), bodyBlkActFile.length());
							if(tmpActBodyContent[i].length() > 0){				
								actBodyContent[i] =tmpActBodyContent[i];
								break;
							}
						}
						else{
							continue;
						}					
					}
				}				
				else{
					continue;	
				}				
			}	
			for(int j=i;j<mt103Fields.length;j++){

				if(bodyBlkExpFile.indexOf(mt103Fields[i]) != -1 && bodyBlkExpFile.indexOf(mt103Fields[j]) != -1  ){
					tmpExpBodyContent[i] = bodyBlkExpFile.substring(bodyBlkExpFile.indexOf(mt103Fields[i]), bodyBlkExpFile.indexOf(mt103Fields[j]));
					
					if(tmpExpBodyContent[i].length() > 0){				
						expBodyContent[i] =tmpExpBodyContent[i];
						break;
					}
				}
				if(i == (mt103Fields.length -1)){
					for(int k = i;;k--){
						
						if(bodyBlkExpFile.indexOf(mt103Fields[k]) != -1){
							tmpExpBodyContent[i] = bodyBlkExpFile.substring(bodyBlkExpFile.indexOf(mt103Fields[k]), bodyBlkExpFile.length());
							if(tmpExpBodyContent[i].length() > 0){				
								expBodyContent[i] =tmpExpBodyContent[i];
								break;
							}
						}
						else{
							continue;
						}					
					}
				}
				else{
					continue;	
				}				
			}
			
			if(actBodyContent[i] != null && expBodyContent[i] != null){
				bodyActFileCont.put(i, actBodyContent[i].replace("\n", ""));
				bodyExpFileCont.put(i, expBodyContent[i].replace("\n", ""));
				diffbody.add(StringUtils.indexOfDifference(actBodyContent[i], expBodyContent[i]));
				indx.add(i);
			}
			else if(actBodyContent[i] != null && expBodyContent[i] == null){
				bodyActFileCont.put(i, actBodyContent[i].replace("\n", ""));
				bodyExpFileCont.put(i, " ");	
				diffbody.add(0);
				indx.add(i);
			}
			else if(actBodyContent[i] == null && expBodyContent[i] != null){
				bodyActFileCont.put(i, " ");
				bodyExpFileCont.put(i, expBodyContent[i].replace("\n", ""));
				diffbody.add(0);
				indx.add(i);
			}
		}

		map.put("HEADERDIFF",diffheader);
		map.put("BODYDIFF",diffbody);
		map.put("HEADERINDEX",hdIndx);
		map.put("BODYINDEX",indx);
		map.put("MSGTYPE",msgType);

		
		request.getSession().setAttribute("FILE_DETAILS",map);
		request.getSession().setAttribute("HEADEREXP",expHeaderCont);
		request.getSession().setAttribute("HEADERACT",actHeaderCont);		
		request.getSession().setAttribute("BODYACT",bodyActFileCont);
		request.getSession().setAttribute("BODYEXP",bodyExpFileCont);
		RequestDispatcher dispatcher = request.getRequestDispatcher("compare.jsp");
		dispatcher.forward(request, response);
        }
		
		}
		
		if(txn.equalsIgnoreCase("REPORT")){
			
	    	InputStream inStream = null;
	    	OutputStream outStream = null;
	     
	    	String filePath = "D:/Tenjin_Test_Data/Reports/";
	    	
        	try{
        		
    		/*12th-Nov-2014 Code Change: Starts Here
    		 * By : Badal
    		 * Code Changed For: Checking the file name Recently Created or not 
    		 * If created Returns the File name  
    		 */
        		//Giving the Destination File name
        		File bfile =new File(filePath+"MSRDSWFT_ACT.rtf");
        		
        		bfile.createNewFile();
    	    
        		File root = new File("//WIN-NI7V62P5PD7/tmp/report/ENG");
        		FilenameFilter beginswithm = new FilenameFilter()
        		{
        			public boolean accept(File directory, String filename) {
        				return filename.startsWith("000_MSRDSWFT_00011") && filename.endsWith(".rtf");
        			}
        		};

        		File[] files = root.listFiles(beginswithm);
        		String temp=null;
                        
        		for(int i=0;i<files.length-1; i++){
        			if(files[i+1].lastModified() > files[i].lastModified())
        				temp = files[i+1].getName();
        		}
            	
        		String fpath_name = "//WIN-NI7V62P5PD7/tmp/report/ENG/"+temp;
        		// Actual file name is: fpath_name
        		//Giving the Existing File name to afile
        		File afile =new File(fpath_name);
        		inStream = new FileInputStream(afile);
    
        		outStream = new FileOutputStream(bfile);
            	
        		byte[] buffer = new byte[1024];
            	
        		int length;
        		//copy the file content in bytes 
        		while ((length = inStream.read(buffer)) > 0){
        			outStream.write(buffer, 0, length);
        		}

        		inStream.close();
        		outStream.close();
        		
        	

        	}catch(IOException e){
        		logger.error("Error ", e);
        	}			
		}
	}

}