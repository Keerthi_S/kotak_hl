/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SchedulerServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 01-Dec-2016			Sahana					Req#TJN_243_03
 * 16-Dec-2016			Sahana					Req#TEN-72
 * 27-Jan-2017			Sahana					TENJINCG-61(A Tenjin user should be able to specify Screenshot Option while Scheduling an Execution task)
 * 01-Feb-2017			Sahana                  TENJINCG-19(User should be able to abort / cancel a Recurrent Scheduled Task)
 * 20-July-2017			Gangadhar Badagi		TENJINCG-292
 * 21-July-2017			Gangadhar Badagi		TENJINCG-300
 * 10-Aug-2017          Gangadhar Badagi        T25IT-90
 * 17-08-2017           Leelaprasad             T25IT-60
 * 18-08-2017           Leelaprasad             T25IT-132
 * 18-08-2017           Leelaprasad             T25IT-173
 * 19-08-2017           Leelaprasad             T25IT-262
 * 19-08-2017           Leelaprasad             T25IT-291
 * 29-08-2017           Leelaprasad             defcet T25IT-299
   22-Sep-2017			sameer gupta		 	For new adapter spec
*  19-Oct-2017		    Sriram Sridharan		TENJINCG-397
*  06-Dec-2017			Gangadhar Badagi	    TCGST-8
*  12-04-2018			Preeti					TENJINCG-104
*  04-06-2018           Padmavathi              TENJINCG-678
*  21-06-2018           Padmavathi              T251IT-111
*  22-06-2018           Padmavathi              T251IT-112
*  22-06-2018			Preeti					T251IT-120
*  28-12-2018			Preeti					TJN262R2-83
*  02-05-2019			Roshni					TENJINCG-1046
*  06-05-2019           Padmavathi              TENJINCG-1050
*  10-06-2019			Ashiki					V2.8-111
*  13-06-2019			Roshni					V2.8-95
*  24-06-2019			Ashiki					V2.8-167
   24-06-2019		    Padmavathi 				for V2.8-157
*  */


/*
 * Added file by sahana for Req#TJN_24_03 on 14/09/2016
 */
package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.db.SchedulerHelper;
import com.ycs.tenjin.db.TestCaseHelper;
import com.ycs.tenjin.db.TestSetHelper;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.handler.ProjectMailHandler;
import com.ycs.tenjin.handler.TenjinMailHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.run.ExecutorGateway;
import com.ycs.tenjin.run.ExtractorGateway;
import com.ycs.tenjin.run.LearnerGateway;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.scheduler.SchMapping;
import com.ycs.tenjin.scheduler.Scheduler;
import com.ycs.tenjin.scheduler.SchedulerHandler;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class SchedulerServlet
 */
//@WebServlet("/SchedulerServlet")
public class SchedulerServlet extends HttpServlet {

	private static final Logger logger = LoggerFactory.getLogger(SchedulerServlet.class);
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SchedulerServlet() {
		super();
		
	}

	/*added by shruthi for VAPT FIX to Servlets starts*/
	public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		@Override
		public boolean checkAccess(HttpServletRequest request) {
			if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
				return true;
			}
			return false;
		}  
	};
	/*added by shruthi for VAPT FIX to Servlets ends*/
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	@SuppressWarnings("unused")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		TenjinSession tjnSession = (TenjinSession) request.getSession()
				.getAttribute("TJN_SESSION");
		String txn = request.getParameter("param");

		if (txn.equalsIgnoreCase("FETCH_ALL_SCHEDULES")) {
			String status = request.getParameter("paramval");
			String projectId=request.getParameter("id");
			String[] status1=status.split(" ");
			if(status1.length>1){
				status=status1[0];
			}
			logger.info("Fetch All Schedules  ");
			ArrayList<Scheduler> scheduleList = new ArrayList<Scheduler>();
			Map<String, Object> map = new HashMap<String, Object>();

			SchedulerHelper schHelper = null;

			schHelper = new SchedulerHelper();
			String retData="";
			if (schHelper != null) {
				try {
					scheduleList = schHelper.hydrateAllSchedulers(status,Integer.parseInt(projectId));

				} catch (DatabaseException e) {
					
					logger.error("Error ", e);
				}

				JSONArray jArray = new JSONArray();
				JSONObject resJSON = new JSONObject();
				for (Scheduler  sh: scheduleList) {
					try{
						JSONObject j = new JSONObject();
						j.put("schId", sh.getSchedule_Id());
						/*Changed by Leelaprasad for defect T25IT-173 starts*/
						/*j.put("schDate", sh.getSch_date()+":"+sh.getSch_time());*/
						j.put("schDate", sh.getSch_date()+"  "+sh.getSch_time());
						/*Changed by Leelaprasad for defect T25IT-173 ends*/
						j.put("schAction", sh.getAction());
						j.put("schClient", sh.getReg_client());
						j.put("schStatus", sh.getStatus());
						/*added by shruthi for TENJINCG-1232 starts*/
						j.put("schRecurrence", sh.getSchRecur());
						/*added by shruthi for TENJINCG-1232 ends*/
						j.put("schCreatedBy", sh.getCreated_by());
						j.put("schCreatedOn", sh.getCreated_on());
						j.put("schrunId",sh.getRun_id());
						/*Added by Preeti for T251IT-120 starts*/
						j.put("message",sh.getMessage());
						/*Added by Preeti for T251IT-120 ends*/
						/*changed by sahana for req#TEN-72 starts*/
						j.put("taskName", sh.getTaskName());
						/*changed by sahana for req#TEN-72 Ends*/
						/*changed by sahana for Req#TJN_243_03: Starts*/
						j.put("schRecur",sh.getSchRecur());
						if(sh.getSchRecur().equalsIgnoreCase("Y")){
							Scheduler schRec=schHelper.copyOfSchedulerTask(sh.getSchedule_Id());
							j.put("schRecurCycles",schRec.getRecurCycles());
							j.put("schFrequency",schRec.getFrequency());
							j.put("schRecurDays",schRec.getRecurDays());
							j.put("schEndDate",schRec.getEndDate());
						}
						/*changed by sahana for Req#TJN_243_03: ends*/
						jArray.put(j);
					}catch(Exception e){
						logger.error("Could not fetch details due to "+e.getMessage());
						logger.error("Error ", e);
					}
				}

				map.put("SCH_STATUS", "SUCCESS");
				/*Added by Ashiki for  V2.8-111 starts*/
				String message = request.getParameter("message");
				if(message!=null) {
					map.put("MESSAGE", message);
				}else {
					map.put("MESSAGE", "");
				}
				/*Added by Ashiki for  V2.8-111 ends*/
				/*Added by Preeti for TJN262R2-83 starts*/
				map.put("projectId", projectId);
				/*Added by Preeti for TJN262R2-83 ends*/
				map.put("SCH_LIST", scheduleList);
				if(status1.length>1){
					try {
						resJSON.put("SCHEDULES", jArray);
						resJSON.put("STATUS", "SUCCESS");
					} catch (JSONException e) {
						logger.error("Could not fetch details due to "+e.getMessage());
						
						logger.error("Error ", e);
					}
					retData = resJSON.toString();
				}
			}

			request.getSession().setAttribute("SCR_SCH", map);
			if(status1.length>1){
				response.getWriter().write(retData);
			}
			else{
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("schedule_list.jsp");
				dispatcher.forward(request, response);
			}
		}
		else if (txn.equalsIgnoreCase("NEW_SCHEDULE")) {

			RequestDispatcher dispatcher = request
					.getRequestDispatcher("schedule_new.jsp");
			dispatcher.forward(request, response);
		}	
		/*changed by sahana for Req#TJN_243_03: Starts*/
		else if (txn.equalsIgnoreCase("NEW_SCH_RECURRENCE")) {
			logger.info("Creating New Scheduler Recurrence record");

			String jsonString = request.getParameter("paramval");
			String taskType=request.getParameter("task");
			try{
				JSONObject json = new JSONObject(jsonString);
				Scheduler sch = new Scheduler();
				sch.setFrequency(json.getString("frequency"));
				sch.setRecurCycles(Integer.parseInt(json.getString("recurCycles")));
				sch.setRecurDays(json.getString("recurDays"));
				sch.setEndDate(json.getString("endDate"));
				request.getSession().setAttribute("SCH_REC", sch);

			}catch(Exception e)
			{
				logger.error("Could not create Schedule Recurrence due to "+e.getMessage());
			}
			RequestDispatcher dispatcher=null;
			if(!taskType.equalsIgnoreCase("Execute")){
				dispatcher = request
						.getRequestDispatcher("schedule_admin_new.jsp");
			}
			else{
				dispatcher = request
						.getRequestDispatcher("schedule_new.jsp");
			}
			dispatcher.forward(request, response);
		}
		/*changed by sahana for Req#TJN_243_03: ends*/
		else if (txn.equalsIgnoreCase("FETCH_ALL_ADMIN_SCH")) {
			String status = request.getParameter("paramval");
			/*Added by Preeti for TENJINCG-104 starts*/
			User user = tjnSession.getUser();
			String userRole = user.getRoles();
			/*Added by Preeti for TENJINCG-104 ends*/
			String[] status1=status.split(" ");
			if(status1.length>1){
				status=status1[0];
			}
			logger.info("Fetch All Schedules in Administrator ");

			ArrayList<Scheduler> scheduleList = new ArrayList<Scheduler>();
			Map<String, Object> map = new HashMap<String, Object>();

			SchedulerHelper schHelper = null;

			schHelper = new SchedulerHelper();
			JSONObject resJSON = new JSONObject();
			JSONArray jArray = new JSONArray();	
			String retData="";
			if (schHelper != null) {
				try {
					/*Modified by Preeti for TENJINCG-104 starts*/
					if(userRole.equalsIgnoreCase("Site administrator"))
						scheduleList = schHelper.hydrateAllAdminSchedulers(status);
					else
						scheduleList = schHelper.hydrateAllAdminSchedulers(status,userRole);
					/*Modified by Preeti for TENJINCG-104 ends*/
				} catch (DatabaseException e) {
					
					logger.error("Error ", e);
				}
				for (Scheduler  sh: scheduleList) {
					try{
						JSONObject j = new JSONObject();
						j.put("schId", sh.getSchedule_Id());
						j.put("schDate", sh.getSch_date()+" "+sh.getSch_time());
						j.put("schAction", sh.getAction());
						j.put("schClient", sh.getReg_client());
						j.put("schStatus", sh.getStatus());
						j.put("schCreatedBy", sh.getCreated_by());
						j.put("schCreatedOn", sh.getCreated_on());
						j.put("schrunId",sh.getRun_id());
						/*Added by Padmavathi for T251IT-111 starts*/
						j.put("message",sh.getMessage());
						/*Added by Padmavathi for T251IT-111 ends*/
						
						/*changed by sahana for Req#TJN_243_03: Starts*/
						j.put("schRecur",sh.getSchRecur());
						/*changed by sahana for req#TEN-72 starts*/
						j.put("taskName", sh.getTaskName());
						/*changed by sahana for req#TEN-72 Ends*/
						if(sh.getSchRecur().equalsIgnoreCase("Y")){
							Scheduler schRec=schHelper.copyOfSchedulerTask(sh.getSchedule_Id());
							j.put("schRecurCycles",schRec.getRecurCycles());
							j.put("schFrequency",schRec.getFrequency());
							j.put("schRecurDays",schRec.getRecurDays());
							j.put("schEndDate",schRec.getEndDate());
						}
						/*changed by sahana for Req#TJN_243_03: ends*/
						jArray.put(j);
					}catch(Exception e){
						logger.error("Could not fetch details due to "+e.getMessage());
						logger.error("Error ", e);
					}
				}
				map.put("SCH_STATUS", "SUCCESS");
				/*Added by Ashiki for  V2.8-111 starts*/
				String message = request.getParameter("message");
				if(message!=null) {
					map.put("MESSAGE", message);
				}else {
					map.put("MESSAGE", "");
				}
				/*Added by Ashiki for  V2.8-111 ends*/
				map.put("SCH_LIST", scheduleList);
			}
			if(status1.length>1){
				try {
					resJSON.put("SCHEDULES", jArray);
					resJSON.put("STATUS", "SUCCESS");
				} catch (JSONException e) {
					logger.error("Could not fetch details due to "+e.getMessage());
					
					logger.error("Error ", e);
				}
				retData = resJSON.toString();
			}
			request.getSession().setAttribute("SCR_SCH", map);
			if(status1.length>1){
				response.getWriter().write(retData);
			}
			else{
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("schedule_list.jsp");
				dispatcher.forward(request, response);
			}
		}
		else if (txn.equalsIgnoreCase("UPDATE_TASK_STATUS")) {
			logger.info("Cancelling Schedule in Administrator ");
			String status = request.getParameter("paramval");
			String schIds=request.getParameter("schIds");
			/*changed by sahana for improvement #TENJINCG-19: Starts*/
			String taskType=request.getParameter("taskType");
			/*changed by sahana for improvement #TENJINCG-19: ENDS*/
			/*Added by Padmavathi for T251IT-112 starts*/
			String checkreq = Utilities.trim(request.getParameter("paramval1"));
			/*Added by Padmavathi for T251IT-112 ends*/
			
			String retData = "";
			JSONObject retJson= new JSONObject();
			JSONArray scheduleIds=null;
			if(schIds!=null)
				try {
					scheduleIds=new JSONArray(schIds);
				} catch (JSONException e1) {
					
					logger.error("Error ", e1);
				}

			List<Integer> sIds=new ArrayList<Integer>();
			for(int i=0;i<scheduleIds.length();i++){
				JSONObject j=new JSONObject();
				try {
					j.put("schId", scheduleIds.get(i));
					sIds.add(Integer.parseInt(j.getString("schId")));
				} catch (JSONException e) {
					logger.error("Could not update task  details due to "+e.getMessage());
					
					logger.error("Error ", e);
				}
			}
			SchedulerHelper schHelper =new SchedulerHelper();
			/*changed by sahana for improvement #TENJINCG-19: Starts*/
			SchedulerHandler sh=new SchedulerHandler();
			/*changed by sahana for improvement #TENJINCG-19: ends*/
			try {

				try {
					/*Added by Padmavathi for T251IT-112 starts*/
					if(checkreq.equalsIgnoreCase("ScheduleNow")){
						schHelper.updateScheduleTime(status, sIds);
					}/*Added by Padmavathi for T251IT-112 ends*/
					else{
						schHelper.updateMultipleScheduleStatus(status,sIds);
					}
					/*changed by sahana for improvement #TENJINCG-19: Starts*/
					if(taskType.equalsIgnoreCase("notRec"))
					{
						sh.cancelRecTask(sIds);
						/*Changed by Ashiki for  V2.8-111 starts*/
						retJson.put("message", "Schedule task(s) cancelled successfully");
						/*Changed by Ashiki for  V2.8-111 ends*/
					}else {
						/*Added by Ashiki for  V2.8-167 starts*/
						retJson.put("message", "Recurrence task(s) cancelled successfully");
						/*Added by Ashiki for  V2.8-167 end*/
					}
					/*changed by sahana for improvement #TENJINCG-19: ends*/
					retJson.put("status", "SUCCESS");
					
				} catch (DatabaseException e) {
					logger.error("Could not update schedule status due to "+e.getMessage());
					retJson.put("status", "ERROR");
					retJson.put("message", "Could not update schedule status");
				} finally {
					retData = retJson.toString();
				}
			} catch (Exception e) {
				logger.error("Could not fetch details due to "+e.getMessage());
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			/* Added by Roshni for TENJINCG-1046 starts */
			if(status.equalsIgnoreCase("cancelled")){
				
				for(Integer sid:sIds){
				Scheduler sch=new Scheduler();
				
				
				try {
					sch=schHelper.hydrateSchedule(Integer.toString(sid));
					User user = new UserHandler().getUser(sch.getCreated_by());
					/*Added by Padmavathi for TENJINCG-1050 starts*/
					if(!new ProjectMailHandler().isUserOptOutFromMail(tjnSession.getProject().getId(),"CancelSchedule",user.getId())){
					/*Added by Padmavathi for TENJINCG-1050 ends*/
						TenjinMailHandler tmhandler=new TenjinMailHandler();
						JSONObject mailContent=tmhandler.getMailContent(String.valueOf(sid), sch.getTaskName(), "Scheduled Task", tjnSession.getUser().getFullName(),
								"cancel", new Timestamp(new Date().getTime()), user.getEmail(), "");
						new TenjinMailHandler().sendEmailNotification(mailContent);
					}
				} catch (RecordNotFoundException e1) {
					
					logger.error("Error ", e1);
				} catch (DatabaseException e1) {
					
					logger.error("Error ", e1);
				}catch (TenjinConfigurationException e) {
					logger.error("Error ", e);
					}
				}
			}
			/* Added by Roshni for TENJINCG-1046 ends */
			response.getWriter().write(retData);
		}

		else if (txn.equalsIgnoreCase("deleteall")) {
			logger.info("Delete All Schedules  ");
			SchedulerHelper schHelper = null;
			String schId = request.getParameter("paramval");

			String[] record = schId.split(",");

			JSONObject retJson = new JSONObject();
			String retData = "";
			try {

				try {
					schHelper = new SchedulerHelper();
					schHelper.deleteAllSchedules(record);

					retJson.put("status", "SUCCESS");
					retJson.put("message", "All Records Deleted Successfully");

				} catch (DatabaseException e) {
					logger.error("Could not delete records due to "+e.getMessage());
					retJson.put("status", "ERROR");
					retJson.put("message", "Could not delete user records");
				} finally {

					retData = retJson.toString();
				}
			} catch (Exception e) {
				logger.error("Could not fetch details due to "+e.getMessage());
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}

		else if (txn.equalsIgnoreCase("delete")) {
			logger.info("Delete Schedule  ");
			String schId = request.getParameter("schId");
			String fnCode = request.getParameter("paramval2");
			String sId = request.getParameter("paramval");

			JSONObject retJson = new JSONObject();
			String retData = "";
			try {

				try {
					SchedulerHelper helper = new SchedulerHelper();
					helper.deleteSchedule(sId);
					helper.deleteScheduledMap(fnCode,schId);
					retJson.put("status", "SUCCESS");
					retJson.put("message", "Schedule Deleted Successfully");

				} catch (DatabaseException e) {
					logger.error("Could not delete records due to "+e.getMessage());
					retJson.put("status", "ERROR");
					retJson.put("message", "Could not delete schedule records");
				} finally {
					retData = retJson.toString();
				}
			} catch (Exception e) {
				logger.error("Could not fetch details due to "+e.getMessage());
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}
		else if (txn.equalsIgnoreCase("VIEW_SCH")) {
			logger.info("Viewing a Schedule  ");
			String schCode = request.getParameter("paramval");
			String tabType=request.getParameter("tabType");
			Scheduler sch1=null;
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				SchedulerHelper helper = new SchedulerHelper();
				sch1 = helper.hydrateSchedule(schCode);
				
				/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
				TestRun run=new RunHelper().hydrateRun(sch1.getRun_id());
				
				TestSet objTestSet=new TestSetHelper().hydrateTestSet(run.getTestSetRecordId(), sch1.getProjectId());
				
				/*Added by Gangadhar Badagi for TENJINCG-300 starts*/
				String RecType=null;
				if(objTestSet!=null){
			  /*Added by Gangadhar Badagi for TENJINCG-300 ends*/
				RecType=objTestSet.getRecordType();
				}
				/*Added by Gangadhar Badagi for TENJINCG-292 ends*/
				ArrayList<SchMapping> scheduleList = new ArrayList<SchMapping>();

				scheduleList = helper.hydrateScheduleMap1(sch1);

				/* For TENJINCG-397 */
				/*ArrayList<RegisteredClient> clients = new ClientHelper().hydrateAllClients();*/
				List<RegisteredClient> clients = new ClientHelper().hydrateAllClients();
				/* For TENJINCG-397 ends*/							
				if (sch1 != null) {
					map.put("STATUS","");
					map.put("MESSAGE","");
					map.put("SCH_BEAN", sch1);
					map.put("SCH_Map_List", scheduleList);
					map.put("CLIENT_LIST", clients);
					map.put("TABTYPE", tabType);
					/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
					/*Added by Gangadhar Badagi for TENJINCG-300 starts*/
					if(sch1.getAction().equalsIgnoreCase("Execute")){
					/*Added by Gangadhar Badagi for TENJINCG-300 ends*/
					map.put("RECTYPE", RecType);
					map.put("ID", objTestSet.getId());
					map.put("TC_ID", objTestSet.getTests().get(0).getTcRecId());
					}
					/*Added by Gangadhar Badagi for TENJINCG-292 ends*/
					if("learnapi".equalsIgnoreCase(sch1.getAction())){
						String apiCode = "";
						if(scheduleList != null && scheduleList.size() > 0) {
							apiCode = scheduleList.get(0).getFunc_Code();
						}
						map.put("API_CODE", apiCode);
					}
					
					
				} else {
					map.put("STATUS", "FAILURE");
					map.put("MESSAGE", "Schedule not found");
				}
			} catch (Exception e) {
				logger.error("Could not fetch records due to "+e.getMessage());
				map.put("STATUS", "ERROR");
				map.put("MESSAGE",
						"An Internal Error occurred. Please contact your System Administrator");
			} finally {
				request.getSession().removeAttribute("SCR_MAP");
				request.getSession().setAttribute("SCR_MAP", map);
			}
			if(sch1.getAction().equalsIgnoreCase("Execute")){
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("schedule_view.jsp");
				dispatcher.forward(request, response);

			}else{
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("schedule_admin_view.jsp");
				dispatcher.forward(request, response);
			}		

		} 
		
		/*Added by Gangadhar Badagi for TENJINCG-292 starts*/
		else if(txn.equalsIgnoreCase("create_testset"))
		{
			String testCaseRecId = request.getParameter("tc");
			String retData = "";
			JSONObject json = null;
			
			ArrayList<TestCase> tcs =null;
	    try {
	    	    json=new JSONObject();
				TestSet t = new TestSet();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_hhmmss");
				/*Changed by leelaprasad for TJNUN262-53 starts*/
				/*t.setName("AdHoc_Test" + "_" + sdf.format(new Date()));*/
				t.setName("Test" + "_" + sdf.format(new Date()));
				/*Changed by leelaprasad for TJNUN262-53 ends*/	TestCase testCase = new TestCaseHelper().hydrateTestCase(tjnSession.getProject().getId(), Integer.parseInt(testCaseRecId));
				t.setMode(testCase.getMode());
				ArrayList<TestCase> tests = new ArrayList<TestCase>();
				tests.add(testCase);
				t.setTests(tests);
				t.setDescription("Auto generated test set");
				t.setType("Functional");
				t.setRecordType("TSA");
				t.setParent(Integer.parseInt("1"));
				t.setPriority("Medium");
				t.setCreatedBy(tjnSession.getUser().getId());
				logger.info("Persisting adhoc test set");
				t = new TestSetHelper().persistTestSet(t, tjnSession.getProject().getId());
				String testSetId = Integer.toString(t.getId());
				tcs=t.getTests();
				String tcList = "";
				int counter=1;
				if(tcs != null){
					for(TestCase tc:tcs){
						if(counter < tcs.size()){
							tcList = tcList + tc.getTcRecId() + ",";
						}else{
							tcList = tcList + tc.getTcRecId();
						}
					}
				}
				new TestSetHelper().persistTestSetMap(t.getId(), tcList, tjnSession.getProject().getId(), true);
				json.put("TEST_SET", t);
				json.put("testSetId", testSetId);
				json.put("status", "success");
				json.put("message", "test set is created");
					
	        }catch(Exception e){
			logger.error("Could not initiate run",e);
			try {
				json.put("status", "ERROR");
				json.put("message", "An internal error occurred while auto creating test set. Please try again");
			} catch (JSONException e1) {
				logger.error("Error ", e1);
			}
		  }finally{
			  retData = json.toString();
		  }
			response.getWriter().write(retData);
		}
		/*Added by Gangadhar Badagi for TENJINCG-292 ends*/
		
		
         /*Added by Gangadhar Badagi for TCGST-8 starts*/
		
		else if(txn.equalsIgnoreCase("FETCH_ALL_REG_CLIENTS"))
		{
			logger.debug("fetch Clients list ");
			ClientHelper clientHelper=new ClientHelper();
			String retData = "";
			List<RegisteredClient> clients = null;
			try {
				JSONObject json = new JSONObject();
				try {
						clients=clientHelper.hydrateAllClients();
						JSONArray jArray = new JSONArray();
						for (RegisteredClient  client: clients) {
							JSONObject j = new JSONObject();
							j.put("name", client.getName());
							j.put("port", client.getPort());
							j.put("hostName",client.getHostName());
							j.put("status",client.getStatus());
							
							jArray.put(j);
						}
						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("clients", jArray);

				} catch (Exception e) {
					logger.error("An error occurred while fetching clients", e);
					json.put("status", "ERROR");
					json.put("message",
							"An internal error occurred. Please try again.");


				} finally {
					retData = json.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}
		 /*Added by Gangadhar Badagi for TCGST-8 ends*/
		
		
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*changed by sahana .................starts */
		TenjinSession tjnSession = (TenjinSession) request.getSession()
				.getAttribute("TJN_SESSION");
		String txn = request.getParameter("param");

		if (txn.equalsIgnoreCase("NEW_SCH")) {

			logger.info("Creating New Scheduler");

			String jsonString = request.getParameter("json");
			String funcs=request.getParameter("functions");
			/* Added by Roshni for TENJINCG-1046 starts */
			String source=request.getParameter("source");
			/* Added by Roshni for TENJINCG-1046 ends */
			logger.info("JSON String: " + jsonString);
			/*Added by Padmavathi for V2.8-157 starts*/
            String emailParam=request.getParameter("emailParam");
            /*Added by Padmavathi for V2.8-157 ends*/
			String retData = "";
			try {
				//String[] functions=funcs.split(",");
				JSONArray functions=null;
				/*if(funcs!=null)
				 functions=new JSONArray(funcs);*/

				JSONObject json = new JSONObject(jsonString);

				Scheduler sch = new Scheduler();
				int tsetId=0;

				sch.setSch_date(json.getString("date"));
				sch.setSch_time(json.getString("time"));
				sch.setAction(json.getString("task"));
				sch.setReg_client(json.getString("client"));
				sch.setStatus(json.getString("status"));
				sch.setCreated_by(json.getString("crtBy"));
				sch.setCreated_on(json.getString("crtOn"));
				sch.setBrowserType(json.getString("browser"));
				/*Changed by Leelaprasad for the requirement defect fix T25IT-60 starts*/
				/*Changed by Leelaprasad for the requirement defect fix T25IT-262 starts*/
				/*sch.setType(json.getString("type"));*/
				if(sch.getAction().equalsIgnoreCase("Execute")){
				sch.setType(json.getString("type"));
				/*Changed by Leelaparasad for defect T25IT-299 starts*/
				if(sch.getReg_client().equalsIgnoreCase("-1")){
					sch.setReg_client("NA");
					
				}
				/*Changed by Leelaparasad for defect T25IT-299 ends*/
				}
				/*Changed by Leelaprasad for the requirement defect fix T25IT-262 ends*/
				/*Changed by Leelaprasad for the requirement defect fix T25IT-60 ends*/
				/*changed by sahana for req#TEN-72 starts*/
				sch.setTaskName(json.getString("taskName"));
				/*changed by sahana for req#TEN-72 Ends*/
				/*changed by sahana for Req#TJN_243_03: Starts*/
				String schRecur=json.getString("schRecur");
				sch.setSchRecur(schRecur);
				if(schRecur.equalsIgnoreCase("Y")){
					sch.setFrequency(json.getString("frequency"));
					sch.setRecurCycles(Integer.parseInt(json.getString("recurCycles")));
					sch.setRecurDays(json.getString("recurDays"));
					if(!sch.getFrequency().equalsIgnoreCase("daily-Repetitive"))
						sch.setEndDate(json.getString("endDate"));
					String startDate=null;
					String time=sch.getSch_time();
					if(sch.getFrequency().equalsIgnoreCase("daily")){
						startDate=new SchedulerHandler().checkstartDate(sch.getSch_date(),sch.getSch_time(),sch.getRecurCycles());
						if(startDate!=null){
							sch.setSch_date(startDate);
						}

					}
					else if(sch.getFrequency().equalsIgnoreCase("daily-Repetitive"))
					{
						Calendar cal = Calendar.getInstance();
						cal.setTime(new Date());  
						int hours = cal.get(Calendar.HOUR_OF_DAY);
						int minutes=cal.get(Calendar.MINUTE);
						String[] dateTime=sch.getSch_time().split(":");
						int selHours=Integer.parseInt(dateTime[0]);
						int selMinutes=Integer.parseInt(dateTime[1]);
						if(selHours>hours && selMinutes>minutes){
							String startTime=new SchedulerHandler().getRecurHours(sch.getSch_date(), sch.getSch_time(), sch.getRecurCycles());
							if(startTime.equalsIgnoreCase("Completed")){
								new SchedulerHelper().recurUpdateStatus(sch.getSchedule_Id());
							}
							else{
								sch.setSch_time(startTime);
							}
						}	
						else
							sch.setSch_time(time);
					}
					else{
						startDate=new SchedulerHandler().getWeekDayDate(sch.getSch_time(), sch.getFrequency(), sch.getEndDate(), sch.getRecurDays(), sch.getSch_date(), sch.getRecurCycles(),"new");
						if(startDate!=null){
							sch.setSch_date(startDate);
						}
					}
				}
				/*changed by sahana for Req#TJN_243_03: ends*/
				if(!sch.getAction().equalsIgnoreCase("Execute")){
					sch.setAutLoginTYpe(json.getString("autLogin"));
					sch.setProjectId(0);
				}
				if(sch.getAction().equalsIgnoreCase("Execute")){
					tsetId=Integer.parseInt(json.getString("testSetId"));
					sch.setProjectId(Integer.parseInt(json.getString("projectId")));
					/*changed by sahana for improvement #TENJINCG-61: STARTS*/
					sch.setScreenShotOption(Integer.parseInt(json.getString("screenShot")));
					/*changed by sahana for improvement #TENJINCG-61: ends*/
				}

				SchedulerHelper helper = new SchedulerHelper();

				int checkSch_task=0;
				String task=sch.getAction();
				int appId=0;
				String apiCode = "";
				String learnType = "";
				if(task.equalsIgnoreCase("learnapi")) {
					apiCode = json.getString("apicode");
					learnType = json.getString("apilearntype");
					sch.setApiLearnType(learnType);
					/*Added by Gangadhar Badagi for T25IT-90 starts*/
					sch.setReg_client("NA");
					/*Added by Gangadhar Badagi for T25IT-90 ends*/
				}
				
				Map<String,String> funcMap=null;


				List<String> func=null;
				if(!task.equalsIgnoreCase("Execute"))
					appId=Integer.parseInt(json.getString("appId"));

				func= new ArrayList<String>();
				//Changed by sriram for TENJINCG-169 (Schedule API Learning)
				/*if(task.equalsIgnoreCase("Learn")){*/
				if(task.equalsIgnoreCase("Learn") || task.equalsIgnoreCase("learnapi")){
				//Changed by sriram for TENJINCG-169 (Schedule API Learning) ends
					/*added by shruthi for VAPT FIX to Servlets starts*/
					Authorizer authorizer=new Authorizer();
						if(!authorizer.hasAccess(request)){
							JSONObject resJSON1=new JSONObject();
							resJSON1.put("status", "ERROR");
							resJSON1.put("message", "Access forbidden! Please contact Tenjin Support.");
							retData =resJSON1.toString();
							response.getWriter().write(retData);
							return;
						}
					/*added by shruthi for VAPT FIX to Servlets ends*/
					if(funcs!=null)
						functions=new JSONArray(funcs);

					for(int i=0;i<functions.length();i++){
						JSONObject j=new JSONObject();
						j.put("func", functions.get(i));
						func.add(j.getString("func"));
					}
					checkSch_task=helper.checkSch_Task(sch,tsetId);
				}
				else if(task.equalsIgnoreCase("Execute")){
					checkSch_task=helper.checkSch_Task(sch,tsetId);
				}
				else if(task.equalsIgnoreCase("Extract"))
				{if(funcs!=null)
					functions=new JSONArray(funcs);

				for(int i=0;i<functions.length();i++){
					JSONObject j=new JSONObject();
					j.put("func", functions.get(i));
					func.add(j.getString("func"));
				}
				checkSch_task=helper.checkSch_Task(sch,tsetId);
				}
				if(checkSch_task==0){
					if(task.equalsIgnoreCase("Learn")){
						LearnerGateway gateway = new LearnerGateway(appId, func, tjnSession.getUser().getId());
						gateway.setAutLoginType(sch.getAutLoginTYpe());
						gateway.setBrowserSelection(sch.getBrowserType());
						gateway.setRegClientName(sch.getReg_client());
						gateway.performAllValidations();
						logger.info("Initializing Learner...");
						gateway.createRun();
						sch.setRun_id(gateway.getTestRun().getId());
					}else if(task.equalsIgnoreCase("Learnapi")){
						List<ApiOperation> operationsToLearn = new ArrayList<ApiOperation>();
						for(String op:func) {
							ApiOperation operation = new ApiOperation();
							operation.setName(op);
							operationsToLearn.add(operation);
						}
						LearnerGateway gateway = new LearnerGateway(appId, apiCode, operationsToLearn, tjnSession.getUser().getId());
						/*gateway.setAutLoginType(sch.getAutLoginTYpe());
						gateway.setBrowserSelection(sch.getBrowserType());
						gateway.setRegClientName(sch.getReg_client());*/
						gateway.performAllValidationsForApiLearning();
						logger.info("Setting learn type to {}", learnType);
						gateway.setApiLearnType(learnType);
						logger.info("Initializing Learner...");
						gateway.createRun();
						sch.setRun_id(gateway.getTestRun().getId());
					}
					else if(task.equalsIgnoreCase("Execute")){
						String oem="";
						try {
							logger.info("Getting Automation Engine configuration");
							oem = TenjinConfiguration.getProperty("OEM_TOOL");
						} catch (TenjinConfigurationException e) {
							logger.error("CONFIG ERROR --> {}", e.getMessage());
							logger.info("OEM will be defaulted to Selenium WebDriver");
						}
						RegisteredClient client =null;
						/*Changed by Leelaprasad for the defect T25IT-132 starts*/
						String clientName=null;
						int clientPort=0;
						/*Changed by Leelaprasad for the defect T25IT-132 ends*/
						try {
							client = new ClientHelper().hydrateClient(sch.getReg_client());
							
							if(client != null){
								logger.info("Client Info --> Host [{}], Port [{}]", client.getHostName(), client.getPort());
								/*Changed by Leelaprasad for the defect T25IT-132 starts*/
								clientName= client.getHostName();
								clientPort=Integer.parseInt(client.getPort());
								/*Changed by Leelaprasad for the defect T25IT-132 ends*/
							}
						} catch (Exception e) {
							logger.error("ERROR validating client [{}]", sch.getReg_client(), e);
						}
						
						/*changed by sahana for improvement #TENJINCG-61: STARTS*/
						//ExecutorGateway gateway=new ExecutorGateway(tjnSession, tsetId, sch.getBrowserType(), client.getHostName(), Integer.parseInt(client.getPort()),oem, 0, 0, "N");
						/*Changed by Leelaprasad for the defect T25IT-132 starts*/
						/*ExecutorGateway gateway=new ExecutorGateway(tjnSession, tsetId, sch.getBrowserType(), client.getHostName(), Integer.parseInt(client.getPort()),oem, 0, sch.getScreenShotOption(), "N");*/
						ExecutorGateway gateway=new ExecutorGateway(tjnSession, tsetId, sch.getBrowserType(),clientName, clientPort,oem, 0, sch.getScreenShotOption(), "N");
						/*Changed by Leelaprasad for the defect T25IT-132 ends*/
						/*changed by sahana for improvement #TENJINCG-61: ends*/
						/*Modified by Roshni for V2.8-95 starts*/
						try{
						gateway.performAllValidations();
						}
						catch(TenjinServletException e){
							if(e.getMessage().contains("password")){
							String[] msg=e.getMessage().split("-");
							String[] msg2=msg[1].split("\n");
							throw new TenjinServletException("Could not schedule the task as "+msg2[0]);
							}else
								throw new TenjinServletException(e.getMessage());
							
						}
						/*Modified by Roshni for V2.8-95 ends*/
						gateway.createRun();
						sch.setRun_id(gateway.getTaskManifest().getRunId());
					}
					else if(task.equalsIgnoreCase("Extract"))
					{
						funcMap=new HashMap<String,String>();
						for(int i=0;i<func.size();i++){
							JSONObject j=new JSONObject(func.get(i));
							funcMap.put(j.getString("funcCode"),j.getString("path"));
						}
						ExtractorGateway gateway=new ExtractorGateway(appId,funcMap,tjnSession.getUser().getId());
						gateway.setClientName(sch.getReg_client());
						gateway.setAutLoginType(sch.getAutLoginTYpe());
						gateway.performAllValidations();
						logger.info("Initializing Extracter...");
						gateway.createRun();
						sch.setRun_id(gateway.getTestRun().getId());

					}

					/*Added by paneendra for TENJINCG-1268 starts */
					String datetime=sch.getSch_date()+" "+sch.getSch_time();
					DateFormat formatterIST = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
					formatterIST.setTimeZone(TimeZone.getTimeZone("IST"));
				    Date date = formatterIST.parse(datetime);
				    logger.info("client machine time:"+date);
				    DateFormat formatterUTC = new SimpleDateFormat("dd-MMM-yyyy");
				    DateFormat formatterUTCTime = new SimpleDateFormat("HH:mm");
				   /* formatterUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
				    formatterUTCTime.setTimeZone(TimeZone.getTimeZone("UTC"));*/
				    formatterUTC.setTimeZone(TimeZone.getDefault());
				    formatterUTCTime.setTimeZone(TimeZone.getDefault());

				    sch.setSch_date(formatterUTC.format(date));
					sch.setSch_time(formatterUTCTime.format(date));
					logger.info("Scheduled date:"+sch.getSch_date() +" ,"+"Scheduled time:"+sch.getSch_time());
					/*Added by paneendra for TENJINCG-1268 ends */
					sch = helper.persistSchedule(sch);				
					JSONObject r1 = new JSONObject();
					r1.put("status","success");
					r1.put("message","Schedule created Successfully with Task Id: "+sch.getSchedule_Id());
					r1.put("schID",sch.getSchedule_Id());
					retData = r1.toString();
					/* Added by Roshni for TENJINCG-1046 starts */
					List<User> users=null;
					String sendUsers="";
					if(source!=null && source.equalsIgnoreCase("adminpage")){

						User user=new UserHandler().getUser(sch.getCreated_by());
						sendUsers=user.getEmail();
					}/*Added by Padmavathi for V2.8-157 starts*/
					else if(emailParam!=null&&emailParam.equalsIgnoreCase("reschedule")){
						if( (!new ProjectMailHandler().isUserOptOutFromMail(tjnSession.getProject().getId(),"Reschedule",tjnSession.getUser().getId())))
						sendUsers=tjnSession.getUser().getEmail();
					}
					/*Added by Padmavathi for V2.8-157 ends*/
					else{
						try {
							users = new ProjectHandler().getProjectUsers(tjnSession.getProject().getId());
								/*Added by Padmavathi for TENJINCG-1050 starts*/
							String mailSendUsers=new ProjectMailHandler().getPrjMailPreferenceUsers(tjnSession.getProject().getId(), "ScheduleCreation");
							List<String> mailSendUsersList=Arrays.asList(mailSendUsers.split(","));
								/*Added by Padmavathi for TENJINCG-1050 ends*/
							int count=0;
							for(User user:users){
								/*Added by Padmavathi for TENJINCG-1050 starts*/
								if(mailSendUsersList.contains(user.getId())){
									/*Added by Padmavathi for TENJINCG-1050 ends*/
								sendUsers+=user.getEmail();
								if(count<users.size()-1)
									sendUsers+=",";
								count++;
								}
								
							}
						} catch (DatabaseException e1) {
							
							logger.error("Error ", e1);
							}
					}
					/*Added by Padmavathi for V2.8-157 starts*/
					String actionName="";
					if(emailParam!=null&&emailParam.equalsIgnoreCase("reschedule")){
						actionName="update";
					}else{
						actionName="create";
					}
					/*Added by Padmavathi for V2.8-157 ends*/
					if(!sendUsers.equals("")){
						TenjinMailHandler tmhandler=new TenjinMailHandler();
						JSONObject mailContent=tmhandler.getMailContent(String.valueOf(sch.getSchedule_Id()), sch.getTaskName(), "Scheduled Task", tjnSession.getUser().getFullName(), actionName,
								new Timestamp(new Date().getTime()), sendUsers, "");
						try {
							new TenjinMailHandler().sendEmailNotification(mailContent);
						} catch (TenjinConfigurationException e) {
							logger.error("Error ", e);
						}
					}
					/* Added by Roshni for TENJINCG-1046 ends */
				}
				else{
					JSONObject r = new JSONObject();
					r.put("status", "ERROR");
					r.put("message", "Schedule Already Exist");
					r.put("schID", sch.getSchedule_Id());
					retData = r.toString();
				}
				
			}catch (TenjinServletException e) {
				JSONObject r = new JSONObject();
				try {
					logger.error("Could not create record due to "+e.getMessage());
					r.put("status", "ERROR");
					r.put("message",e.getMessage());
					retData = r.toString();
					//System.out.println("regdat is= "+retData);

				} catch (JSONException e1) {
					logger.error("Could not fetch records due to "+e1.getMessage());
					retData = "An internal error occurred. Please contact your System Administrator ";

				}
			}catch (Exception e) {
				JSONObject r = new JSONObject();
				try {
					logger.error("Could not create Schedule due to "+e.getMessage());
					r.put("status", "ERROR");
					r.put("message","Could not create Schedule");
					retData = r.toString();

				} catch (JSONException e1) {
					logger.error("Could not fetch records due to "+e1.getMessage());
					retData = "An internal error occurred. Please contact your System Administrator ";

				}
			}
			response.getWriter().write(retData);
		}
		else if (txn.equalsIgnoreCase("EDIT_SCH")) {

			logger.info("Updating a Schedule  ");

			String jsonString = request.getParameter("json");
			String funcs=request.getParameter("functions");
			JSONObject resJSON = new JSONObject();
			Map<String,String> funcMap=null;
			String retData = "";
			try {
				int tsetId=0;
				try {
					JSONObject j = new JSONObject(jsonString);
					Scheduler sch = new Scheduler();
					SchedulerHelper helper=new SchedulerHelper();
					sch.setSchedule_Id(Integer.parseInt(j.getString("schId")));
					sch.setSch_date(j.getString("date"));
					sch.setSch_time(j.getString("time"));
					sch.setAction(j.getString("task"));
					sch.setReg_client(j.getString("client"));
					sch.setStatus(j.getString("status"));
					sch.setCreated_by(j.getString("crtBy"));
					sch.setCreated_on(j.getString("crtOn"));
					sch.setBrowserType(j.getString("browser"));
					/*changed by sahana for req#TEN-72 starts*/
					sch.setTaskName(j.getString("taskName"));
					/*changed by sahana for req#TEN-72 Ends*/
					/*Changed by Leelaprasad for the requirement defect fix T25IT-132 starts*/
					/*Changed by Leelaprasad for the requirement defect fix T25IT-291 starts*/
					/*sch.setType(j.getString("type"));*/
					if(sch.getAction().equalsIgnoreCase("Execute")){
						sch.setType(j.getString("type"));
						/*Changed by Leelaparasad for defect T25IT-299 starts*/
						if(sch.getReg_client().equalsIgnoreCase("-1")){
							sch.setReg_client("NA");
							
						}
						/*Changed by Leelaparasad for defect T25IT-299 ends*/
					}
					/*Changed by Leelaprasad for the requirement defect fix T25IT-291 ends*/
					/*Changed by Leelaprasad for the requirement defect fix T25IT-132 ends*/
					String apiCode = "";
					int appId=0;
					String learnType = "";
					if(!sch.getAction().equalsIgnoreCase("Execute")){
						sch.setAutLoginTYpe(j.getString("autLogin"));
						sch.setProjectId(0);
						appId=Integer.parseInt(j.getString("appId"));
						if(sch.getAction().equalsIgnoreCase("learnapi")) {
							apiCode = j.getString("apicode");
							learnType = j.getString("apilearntype");
							sch.setApiLearnType(learnType);
							/*Added by Gangadhar Badagi for T25IT-90 starts*/
							sch.setReg_client("NA");
							/*Added by Gangadhar Badagi for T25IT-90 ends*/
						}
					}
					if(sch.getAction().equalsIgnoreCase("Execute")){
						tsetId=Integer.parseInt(j.getString("testSetId"));
						sch.setProjectId(Integer.parseInt(j.getString("projectId")));
						/*changed by sahana for improvement #TENJINCG-61: STARTS*/
						sch.setScreenShotOption(Integer.parseInt(j.getString("screenShot")));
						/*changed by sahana for improvement #TENJINCG-61: ends*/
					}

					int checkSch_task=0;
					List<String> func=null;
					JSONArray functions=null;
					if(sch.getAction().equalsIgnoreCase("Learn") || sch.getAction().equalsIgnoreCase("learnapi")){
						/*added by shruthi for VAPT FIX to Servlets starts*/
						 Authorizer authorizer=new Authorizer();
								if(!authorizer.hasAccess(request)){
									JSONObject resJSON1=new JSONObject();
									resJSON1.put("status", "ERROR");
									resJSON1.put("message", "Access forbidden! Please contact Tenjin Support.");
									retData =resJSON1.toString();
									response.getWriter().write(retData);
									return;
									/*added by shruthi for VAPT FIX to Servlets ends*/
								}
						func=new ArrayList<String>();
						if(funcs!=null)
							functions=new JSONArray(funcs);

						for(int i=0;i<functions.length();i++){
							JSONObject jo=new JSONObject();
							jo.put("func", functions.get(i));
							func.add(jo.getString("func"));
						}
					}
					else if(sch.getAction().equalsIgnoreCase("Extract")){
						func=new ArrayList<String>();
						if(funcs!=null)
							functions=new JSONArray(funcs);

						for(int i=0;i<functions.length();i++){
							JSONObject jo=new JSONObject();
							jo.put("func", functions.get(i));
							func.add(jo.getString("func"));
						}

					}
					/*Modified by Padmavathi for TENJINCG-678 starts*/
					/*checkSch_task=helper.checkSch_Task(sch,tsetId);*/
					Scheduler sch1=helper.hydrateSchedule(String.valueOf(sch.getSchedule_Id()));
					String oRegClient=sch1.getReg_client();
					if(!sch1.getSch_date().equals(sch.getSch_date())){
						checkSch_task=helper.checkSch_Task(sch,tsetId);
					}else if(!sch1.getSch_time().equals(sch.getSch_time())){
						checkSch_task=helper.checkSch_Task(sch,tsetId);
					}else if(!oRegClient.equalsIgnoreCase(sch.getReg_client())){
						checkSch_task=helper.checkSch_Task(sch,tsetId);
					}
					/*Modified by Padmavathi for TENJINCG-678 ends*/

					if(checkSch_task==0){
						if(sch.getAction().equalsIgnoreCase("Learn")){
							LearnerGateway gateway = new LearnerGateway(appId, func, tjnSession.getUser().getId());
							gateway.setAutLoginType(sch.getAutLoginTYpe());
							gateway.setBrowserSelection(sch.getBrowserType());
							gateway.setRegClientName(sch.getReg_client());
							gateway.performAllValidations();
							logger.info("Updating Learner...");
							gateway.createRun();
							sch.setRun_id(gateway.getTestRun().getId());
						}else if(sch.getAction().equalsIgnoreCase("LearnApi")){
							
							List<ApiOperation> ops = new ArrayList<ApiOperation>();
							for(String op:func) {
								ApiOperation operation = new ApiOperation();
								operation.setName(op);
								ops.add(operation);
							}
							
							LearnerGateway gateway = new LearnerGateway(appId, apiCode, ops, tjnSession.getUser().getId());
							gateway.performAllValidationsForApiLearning();
							gateway.setApiLearnType(learnType);
							logger.info("Updating Learner...");
							gateway.createRun();
							sch.setRun_id(gateway.getTestRun().getId());
						}
						else if(sch.getAction().equalsIgnoreCase("Extract")){
							funcMap = new HashMap<String,String>();
							for(int i=0;i<func.size();i++){
								JSONObject j1=new JSONObject(func.get(i));
								funcMap.put(j1.getString("funcCode"),j1.getString("path"));
							}

							ExtractorGateway gateway=new ExtractorGateway(appId,funcMap,tjnSession.getUser().getId());
							gateway.setClientName(sch.getReg_client());
							gateway.setAutLoginType(sch.getAutLoginTYpe());
							gateway.performAllValidations();
							logger.info("Updating Extracter...");
							gateway.createRun();
							sch.setRun_id(gateway.getTestRun().getId());
						}
						else if(sch.getAction().equalsIgnoreCase("Execute")){
							String oem="";
							try {
								logger.info("Getting Automation Engine configuration");
								oem = TenjinConfiguration.getProperty("OEM_TOOL");
							} catch (TenjinConfigurationException e) {
								logger.error("CONFIG ERROR --> {}", e.getMessage());
								logger.info("OEM will be defaulted to Selenium WebDriver");
							}
							RegisteredClient client =null;
							/*Changed by Leelaprasad for the defect T25IT-132 starts*/
							String clientName=null;
							int clientPort=0;
							/*Changed by Leelaprasad for the defect T25IT-132 ends*/
							try {
								client = new ClientHelper().hydrateClient(sch.getReg_client());
								if(client != null){
									logger.info("Client Info --> Host [{}], Port [{}]", client.getHostName(), client.getPort());
									/*Changed by Leelaprasad for the defect T25IT-132 starts*/
									clientName=client.getHostName();
									clientPort=Integer.parseInt(client.getPort());
									/*Changed by Leelaprasad for the defect T25IT-132 ends*/
								}
							} catch (Exception e) {
								logger.error("ERROR validating client [{}]", sch.getReg_client(), e);
							}
							/*changed by sahana for improvement #TENJINCG-61: STARTS*/
							//ExecutorGateway gateway=new ExecutorGateway(tjnSession, tsetId, sch.getBrowserType(), client.getHostName(), Integer.parseInt(client.getPort()),oem, 0,  0, "N");
							/*Changed by Leelaprasad for the defect T25IT-132 starts*/
							/*ExecutorGateway gateway=new ExecutorGateway(tjnSession, tsetId, sch.getBrowserType(), client.getHostName(), Integer.parseInt(client.getPort()),oem, 0,  sch.getScreenShotOption(), "N");*/
							ExecutorGateway gateway=new ExecutorGateway(tjnSession, tsetId, sch.getBrowserType(),clientName,clientPort ,oem, 0,  sch.getScreenShotOption(), "N");
							/*Changed by Leelaprasad for the defect T25IT-132 ends*/
							/*changed by sahana for improvement #TENJINCG-61: ends*/
							gateway.performAllValidations();
							gateway.createRun();
							sch.setRun_id(gateway.getTaskManifest().getRunId());
						}
						
						/*Added by paneendra for TENJINCG-1268 starts */
						String datetime=sch.getSch_date()+" "+sch.getSch_time();
						DateFormat formatterIST = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
						formatterIST.setTimeZone(TimeZone.getTimeZone("IST"));
						Date date = formatterIST.parse(datetime);

						DateFormat formatterUTC = new SimpleDateFormat("dd-MMM-yyyy");
						DateFormat formatterUTCTime = new SimpleDateFormat("HH:mm");
						/*formatterUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
						formatterUTCTime.setTimeZone(TimeZone.getTimeZone("UTC"));*/
						formatterUTC.setTimeZone(TimeZone.getDefault());
					    formatterUTCTime.setTimeZone(TimeZone.getDefault());
						 

						sch.setSch_date(formatterUTC.format(date).toString());
						sch.setSch_time(formatterUTCTime.format(date));
						/*Added by paneendra for TENJINCG-1268 ends */
						helper.updateSchedule(sch);				
						resJSON.put("status","success");
						resJSON.put("message","Schedule updated Successfully with Task Id: "+sch.getSchedule_Id());
						resJSON.put("schID",sch.getSchedule_Id());
						retData = resJSON.toString();
					}
					else{
						resJSON.put("status", "ERROR");
						resJSON.put("message", "Schedule Already Exist");
						resJSON.put("schID", sch.getSchedule_Id());
						retData =resJSON.toString();
					}
					/* Added by Roshni for TENJINCG-1046 starts */
					User user=new UserHandler().getUser(sch.getCreated_by());
					/*Added by Padmavathi for TENJINCG-1050 starts*/
					if(!new ProjectMailHandler().isUserOptOutFromMail(tjnSession.getProject().getId(),"Reschedule",user.getId())){
					/*Added by Padmavathi for TENJINCG-1050 ends*/
						TenjinMailHandler tmhandler=new TenjinMailHandler();
						JSONObject mailContent=tmhandler.getMailContent(String.valueOf(sch.getSchedule_Id()),sch.getTaskName(), "Scheduled Task", tjnSession.getUser().getFullName(),
							"update", new Timestamp(new Date().getTime()), user.getEmail(), "");
					
						try {
							new TenjinMailHandler().sendEmailNotification(mailContent);
						} catch (TenjinConfigurationException e) {
							logger.error("Error ", e);
						}
					}
					/* Added by Roshni for TENJINCG-1046 ends */
				} catch (TenjinServletException e) {
					logger.error("Could not update record due to "+e.getMessage());
					resJSON.put("status", "ERROR");
					resJSON.put("message",e.getMessage());
				}catch (Exception e) {
					logger.error("Could not update record due to "+e.getMessage());
					resJSON.put("status", "ERROR");
					resJSON.put("message","Could not update schedule due to an internal error ");
				} finally {
					retData = resJSON.toString();
				}
			} catch (Exception e) {
				logger.error("Could not fetch details due to "+e.getMessage());
				retData = "An internal error occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);

		}
		
		
	}
	
}
