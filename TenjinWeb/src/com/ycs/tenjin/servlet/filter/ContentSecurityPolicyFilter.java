/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TenjinSessionServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 31-03-2021           Ashiki            newly added for ContentSecurityPolicy
* 
*/


package com.ycs.tenjin.servlet.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ContentSecurityPolicyFilter implements Filter {
	
	public static final String POLICY = "default-src 'self'; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; img-src 'self'; frame-src 'self'; connect-src 'self'; form-action 'self'; ";
	
	 private static final String HEADER_NAME = "Strict-Transport-Security";
	 private static final String MAX_AGE_DIRECTIVE = "max-age=%s";
	 private static final String INCLUDE_SUB_DOMAINS_DIRECTIVE = "includeSubDomains";
	 
	 private int maxAgeSeconds = 0;
	 private boolean includeSubDomains = false;
	 private String directives;
	
	@Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain)
        throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		Map<String, Object> map = new HashMap<String, Object>();
		
		
		if (/*request.isSecure() &&*/ response instanceof HttpServletResponse) {
			   
			   res.addHeader(HEADER_NAME, this.directives);
			   //Added by Prem for content-security-policy
			   ((HttpServletResponse)response).setHeader("Content-Security-Policy", ContentSecurityPolicyFilter.POLICY);
			   chain.doFilter(request, response);
			  }
		else {
			map.put("MESSAGE", "Referrer for the response is invalid");
			req.getSession().setAttribute("SCR_MAP", map);
			res.sendRedirect("noAccess.jsp");
		}
    }
    


	@Override
    public void init(FilterConfig filterConfig) throws ServletException { 
		
		maxAgeSeconds = Integer.parseInt(filterConfig.getInitParameter("maxAgeSeconds"));
		  includeSubDomains = "true".equals(filterConfig.getInitParameter("includeSubDomains"));

		  if (this.maxAgeSeconds <= 0) {
		   throw new ServletException("Invalid maxAgeSeconds value :: " + maxAgeSeconds);
		  }

		  this.directives = String.format(MAX_AGE_DIRECTIVE, this.maxAgeSeconds);
		  if (this.includeSubDomains) {
		   this.directives += (" ; " + INCLUDE_SUB_DOMAINS_DIRECTIVE);
		  }
		
	}

	@Override
    public void destroy() { }

}