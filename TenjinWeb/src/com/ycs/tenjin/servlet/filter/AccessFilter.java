/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AccessFilter.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 19-10-2018           Leelaprasad             TENJINCG-829
* 29-11-2018           Prem            		   TENJINCG-837
* 10-12-2018		   Prem		               TJNUN262-108
* 13-12-2018           Padmavathi              TJNUN262-79
* 06-05-2019		   Roshni				   TENJINCG-1036
* 09-09-2019		   Prem				       TJN2.9R1-11

*/

package com.ycs.tenjin.servlet.filter;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.LicenseExpiredException;
import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.UserHelper;
import com.ycs.tenjin.handler.LicenseHandler;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.handler.TenjinSessionHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.license.License;

/**
 * Servlet Filter implementation class AccessFilter
 */
//@WebFilter("/AccessFilter")
public class AccessFilter implements Filter {
	private static final Logger logger = LoggerFactory.getLogger(AccessFilter.class);
    /**
     * Default constructor. 
     */
    public AccessFilter() {
        
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		
	}

	int limit = 0;
	Date startDate = new Date();
	Date endDate = null;
	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		/*logger.info("****************************************************");
		logger.info("Request URL: {}", req.getRequestURL());*/
		//logger.info("Request URL: {}", req.getRequestURL());
		TenjinSession tjnSession = (TenjinSession)req.getSession().getAttribute("TJN_SESSION");
		/* Added by Roshni for TENJINCG-1036 starts */
		String param=request.getParameter("param");
		/* Added by Roshni for TENJINCG-1036 ends */
		if(req.getRequestURL() != null && req.getRequestURL().toString().toLowerCase().endsWith("servlet")){
			if(tjnSession != null && tjnSession.getUser() != null){
				boolean sessionAlive = false;
				try {
					/*Modified by Padmavathi for TJNUN262-79 starts*/
					 sessionAlive = new UserHelper().isUserSessionAlive(tjnSession.getUser().getId(),tjnSession.getUser().getSessionKey());
					 /*Modified by Padmavathi for TJNUN262-79 ends*/
				} catch (DatabaseException e) {
					
					logger.error("ERROR occurred while performing session alive check",e);
				}
				
				if(!sessionAlive){
					logger.error("Session for this user has either been cleared or has expired. Re-login is required");
					Map<String, Object> map = new HashMap<String,Object>();
					map.put("STATUS", "ERROR");
					map.put("MESSAGE", "Your session has expired. Please login again");
					req.getSession().removeAttribute("TJN_SESSION");
					/*Changed by Leelaprasadfor the TENJINCG-827 starts*/
					/*res.sendRedirect("error.jsp");*/
					res.sendRedirect("TenjinSessionServlet");
					/*Changed by Leelaprasadfor the TENJINCG-827 ends*/
				}else{
					 /* Modified by Padmavathi for license starts */
					try {
						try {
							License license = (License) CacheUtils.getObjectFromRunCache("licensedetails");
							if(license==null){
								request.setAttribute("systemId",new LicenseHandler().generateSystemId());
								request.getRequestDispatcher("license_installation.jsp").forward(request, response);
							}else{
								new LicenseHandler().licenseActiveValidation(license, false);
								chain.doFilter(request, response);
							}
						} catch (TenjinConfigurationException | LicenseExpiredException | LicenseInactive | ParseException | DatabaseException e) {
							if(tjnSession.getUser()!=null){
								String[] users={tjnSession.getUser().getId()};
								new TenjinSessionHandler().clearUsers(users);
								if(tjnSession.getProject()!=null){
									new ProjectHandler().endProjectSession(tjnSession.getProject().getId(), tjnSession.getUser().getId());
								}
							}
							if (tjnSession != null) {
							tjnSession.setProject(null);
							tjnSession.setUser(null);
						}
							((HttpServletRequest) request).getSession().invalidate();
							res.sendRedirect("TenjinSessionServlet");
						}
					} catch (Exception e) {
						res.sendRedirect("TenjinSessionServlet");
					}
					 /* Modified by Padmavathi for license ends */
					
					}
			}
			/* Added by Roshni for TENJINCG-1036 starts */
			else if(param!=null&&param.equalsIgnoreCase("resetpwdscreen")){
				String userid=request.getParameter("userid");
				/*<!-- Added by Prem for TENJINCG-1100 start -->*/
				/*added by shruthi for VAPT fixes starts*/
				String count;
				/*added by shruthi for VAPT fixes ends*/
				Map<String,String> securityQueAndAns=null;  Date expireDate = null;
				try {
					SimpleDateFormat sdf = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
					String time = request.getParameter("time");
					Date date = sdf.parse(time);
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(date);
					calendar.add(calendar.HOUR,24);
					expireDate = calendar.getTime();
					Date today = new Date();
					/*modified by shruthi for VAPT fixes starts*/
					count=new UserHandler().hydrateCount(userid);
					if(Integer.parseInt(count)>4 || today.after(expireDate))
					{
						req.getRequestDispatcher("linkExpiryError.jsp").forward(req, res);
					}
					/*modified by shruthi for VAPT fixes ends*/
					
					else{
						/*Modified by Prem for TJN2.9R1-11 start */
					securityQueAndAns=new UserHandler().hydrateSecurityQuestionForUser(userid);
					logger.debug(" securityQueAndAns " + securityQueAndAns);
					/*Modified by Prem for TJN2.9R1-11 end*/
					request.setAttribute("securityQueAndAns", securityQueAndAns);
					/*<!-- Added by Prem for TENJINCG-1100 Ends -->*/
					/*added by shruthi for VAPT fixes starts*/
					int count1=Integer.parseInt(count)+1;
					String count2=String.valueOf(count1);
				    new UserHandler().updateCount(userid , count2);
				    /*added by shruthi for VAPT fixes ends*/
					RequestDispatcher dispatcher = request
							.getRequestDispatcher("changepassword.jsp?userid="+userid);
					dispatcher.forward(request, response);
					}
				} catch (SQLException | DatabaseException | ParseException e) {
					
					logger.error("Error ", e);
				}
				
	
			}
			/* Added by Roshni for TENJINCG-1036 ends */
			else{
				//logger.info("Empty session");
				
				chain.doFilter(request, response);
			
			}
		}else{
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
