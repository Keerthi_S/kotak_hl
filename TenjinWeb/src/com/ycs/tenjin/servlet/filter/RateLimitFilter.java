package com.ycs.tenjin.servlet.filter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.google.common.util.concurrent.RateLimiter;

/**
 * Servlet Filter implementation class RateLimitFilter
 */
@WebFilter("/RateLimitFilter")
public class RateLimitFilter implements Filter {

    /**
     * Default constructor. 
     */
    public RateLimitFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		Map<String, Object> map = new HashMap<String, Object>();
		String servletName = httpRequest.getRequestURI().toString();
		HttpSession session=httpRequest.getSession(true);
		long elapsedTime=0L;
		long elapsedTimeLimit=60L;
		long IdealTimeLimit=120L;
		if(httpRequest.getMethod().equalsIgnoreCase("POST") && !servletName.endsWith("AuthenticationServlet") && !servletName.endsWith("AdminLaunchServlet")){
		if(session!=null){  
			try {
	     String rate=(String) session.getAttribute("rate");
	     double ratecount=Double.parseDouble(rate);
	     String startTime=(String)session.getAttribute("Start_Time"); 
	        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			String EndTimeRequest = sdf.format(new Date());
			 Date d1 = sdf.parse(startTime);
	            Date d2 = sdf.parse(EndTimeRequest);

	            // Calucalte time difference
	            // in milliseconds
	            long difference_In_Time
	                = d2.getTime() - d1.getTime();
	  
	            // Calucalte time difference in seconds,
	            // minutes, hours, years, and days
	            long difference_In_Seconds
	                = TimeUnit.MILLISECONDS.toSeconds(difference_In_Time) % 60;
	  
	            long difference_In_Minutes
	                = TimeUnit.MILLISECONDS.toMinutes(difference_In_Time)% 60;
	  
	  
	            elapsedTime=difference_In_Minutes*60+difference_In_Seconds;
			
	     if((ratecount>1)&&(elapsedTime<elapsedTimeLimit)){
	    	 ratecount=ratecount-1;
	    	 String rateLimit = String.valueOf(ratecount); 
				session.setAttribute("rate", rateLimit);
				chain.doFilter(request, response);
	    	 
	     }else if(elapsedTime>IdealTimeLimit){
	    	 session.removeAttribute("rate");
	    	 session.removeAttribute("Start_Time");
	    	 RateLimiter limiter = RateLimiter.create(3.0);
				if(limiter.tryAcquire()){
					// pass the request along the filter chain
					double rate1 = limiter.getRate();
					String rateLimit = String.valueOf(rate1); 
					session.setAttribute("rate", rateLimit);
					SimpleDateFormat sdfmt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
					String StartTimeReq = sdfmt.format(new Date());
					session.setAttribute("Start_Time", StartTimeReq);
					chain.doFilter(request, response);
	     }else{
	    	 session.removeAttribute("rate");
	    	 session.removeAttribute("Start_Time");
				map.put("MESSAGE", "TOO MANY REQUEST");
				session.setAttribute("SCR_MAP", map);
				request.getRequestDispatcher("error.jsp").forward(request, response);
	     }
				}
	     else{
	    	 session.removeAttribute("rate");
	    	 session.removeAttribute("Start_Time");
				map.put("MESSAGE", "TOO MANY REQUEST");
				session.setAttribute("SCR_MAP", map);
				request.getRequestDispatcher("error.jsp").forward(request, response);
	     }
			} catch (Exception e) {
				RateLimiter limiter = RateLimiter.create(10.0);
				if(limiter.tryAcquire()){
					// pass the request along the filter chain
					double rate1 = limiter.getRate();
					String rate = String.valueOf(rate1); 
					session.setAttribute("rate", rate);
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
					String StartTimeRequest = sdf.format(new Date());
					session.setAttribute("Start_Time", StartTimeRequest);
					chain.doFilter(request, response);
			}else{
				 session.removeAttribute("rate");
		    	 session.removeAttribute("Start_Time");
					map.put("MESSAGE", "TOO MANY REQUEST");
					session.setAttribute("SCR_MAP", map);
					request.getRequestDispatcher("error.jsp").forward(request, response);
			}
	       }
		}
		}else{
			chain.doFilter(request, response);
		}
		

		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
