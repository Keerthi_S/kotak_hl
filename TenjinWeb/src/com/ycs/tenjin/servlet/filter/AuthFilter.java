package com.ycs.tenjin.servlet.filter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.LicenseExpiredException;
import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.UserHelper;
import com.ycs.tenjin.handler.LicenseHandler;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.handler.TenjinSessionHandler;
import com.ycs.tenjin.license.License;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.servlet.util.SessionUtils;

/**
 * Servlet Filter implementation class AuthFilter
 */
public class AuthFilter implements Filter {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthFilter.class);

    public AuthFilter() {
    }

	public void destroy() {
	}
	
	String[] publicPaths = {"/InitialLaunchServlet", "/AuthenticationServlet","/TenjinSessionServlet","/TenjinPasswordResetServlet","/LicenseServlet"};

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		
		// Get tenjin session
		TenjinSession tjnSession = SessionUtils.getTenjinSession(httpRequest);
		
		// Validate License
		try {
			checkLicense();
		} catch (LicenseExpiredException | LicenseInactive e) {
			doSessionCleanup(tjnSession);
			logger.error("Error --> License Validation Failed", e);
			request.setAttribute("systemId", getSystemId());
			request.getRequestDispatcher("license_installation.jsp").forward(request, response);
			return;
		}
		
		// Check for authentication
		String path = getEndPoint(httpRequest.getRequestURL().toString()).replace(httpRequest.getContextPath(), "");
		if(path != null && path.toLowerCase().endsWith("servlet")) {
			if((tjnSession == null || tjnSession.getUser() == null) && !Arrays.asList(publicPaths).contains(path)) {
				logger.error("Error -> Access forbidden. User is not authenticated");
				httpResponse.sendRedirect("InitialLaunchServlet");
				return;
			}else if(tjnSession != null && !isSessionAlive(tjnSession)) {
				logger.error("Session for this user has either been cleared or has expired. Re-login is required");
				Map<String, Object> map = new HashMap<>();
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "Your session has expired. Please login again");
				httpRequest.getSession().removeAttribute("TJN_SESSION");
				httpResponse.sendRedirect("TenjinSessionServlet");
				return;
			}
		}
		
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	}
	
	private boolean isSessionAlive(TenjinSession tjnSession) {
		if(tjnSession == null || tjnSession.getUser() == null) return false;
		
		try {
			return new UserHelper().isUserSessionAlive(tjnSession.getUser().getId(),tjnSession.getUser().getSessionKey());
		} catch (DatabaseException e) {
			logger.error("Error --> Could not check if session is active for {}", tjnSession.getUser().getId(), e);
			return false;
		}
		
		
		
	}
	
	private String getEndPoint(String url) {
		try {
			URL oUrl = new URL(url);
			return oUrl.getPath();
		} catch (MalformedURLException e) {
			logger.warn("Invalid URL: {}", url);
			return "";
		}
	}
	
	private String getSystemId() {
		try {
			return new LicenseHandler().generateSystemId();
		} catch (TenjinServletException | TenjinConfigurationException  e) {
			logger.error("Error generating system ID", e);
			return null;
		}
	}
	
	private void checkLicense() throws LicenseExpiredException, LicenseInactive {
		try {
			License license = (License) CacheUtils.getObjectFromRunCache("licensedetails");
			if(license != null) 
				new LicenseHandler().licenseActiveValidation(license, false);
		} catch (LicenseExpiredException | LicenseInactive e) {
			throw e;
		} catch (ParseException | TenjinConfigurationException | DatabaseException e) {
			throw new LicenseInactive("Could not validate license due to an internal error.");
		} 
	}
	
	private void doSessionCleanup(TenjinSession tjnSession) {
		try {
			if (tjnSession != null && tjnSession.getUser() != null) {
				String[] users = { tjnSession.getUser().getId() };
				new TenjinSessionHandler().clearUsers(users);
				if (tjnSession.getProject() != null) {
					new ProjectHandler().endProjectSession(tjnSession.getProject().getId(),
							tjnSession.getUser().getId());
				}
			}
		} catch (DatabaseException e) {
			logger.warn("Could not clean up active sessions after failed license check: {}", e.getMessage());
		}
		if (tjnSession != null) {
			tjnSession.setProject(null);
			tjnSession.setUser(null);
		}
	}
}
