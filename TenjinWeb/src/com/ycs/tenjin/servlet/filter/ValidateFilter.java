package com.ycs.tenjin.servlet.filter;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.AutsHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet Filter implementation class ValidateFilter
 */
@WebFilter("/ValidateFilter")
public class ValidateFilter implements Filter {

	private static final Logger logger = LoggerFactory
			.getLogger(ValidateFilter.class);

	/**
	 * Default constructor.
	 */
	public ValidateFilter() {
		
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	@SuppressWarnings("unused")
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		String hostDomain = request.getServerName();

		TenjinSession tjnSession = (TenjinSession) req.getSession()
				.getAttribute("TJN_SESSION");
		String referer = req.getHeader("Referer");
		String agent = req.getHeader("User-agent");

		Map<String, Object> map = new HashMap<String, Object>();
		if (req.getRequestURL() != null
				&& req.getRequestURL().toString().toLowerCase()
						.endsWith("servlet")
				|| !req.getRequestURL().toString().toLowerCase()
						.endsWith("jsp")
				&& req.getRequestURL().toString().toLowerCase()
						.endsWith("error.jsp")) {

			/*Modified by Padmavathi for T251IT-38 starts*/
			/*if (agent.contains("Chrome/") || agent.contains("Firefox/")
					|| agent.contains("Safari/") || agent.contains("Edge/")) {*/
			if (agent.contains("Chrome/") || agent.contains("Firefox/")
					|| agent.contains("Safari/") || agent.contains("Edge/")||agent.contains("Mozilla")) {
				/*Modified by Padmavathi for T251IT-38 ends*/

				logger.debug("Referer --> [{}], URL --> [{}]", referer,
						req.getRequestURL());
				logger.debug("User agent --> [{}], URL --> [{}]", agent,
						req.getRequestURL());
				logger.debug("Referer Domain --> [{}]", getDomainName(referer));
				List<String> domainList = new ArrayList<String>();
				
				if (referer != null) {
					domainList.add(getDomainName(referer));
				}
				/*Changes by Avinash for TENJINCG-1018 starts*/
				boolean isTrustedDomain = false;
				if(req.getRequestURL().toString().endsWith("ApplicationUserServlet") && Utilities.trim(req.getParameter("t")).equalsIgnoreCase("getaccesstoken")){
					try {
						List<Aut> auts = new AutsHelper().hydrateAllAut();
						for (Aut aut : auts) {
							String accessTokenURL = aut.getAccessTokenUrl();
							if(!Utilities.trim(accessTokenURL).isEmpty() && domainList.contains(getDomainName(accessTokenURL))){
								isTrustedDomain = true;
								break;
							}
								
						}
					} catch (DatabaseException e) {
						logger.debug("Error occured while checking for trusted site.");
					}
				}
				/*Changes by Avinash for TENJINCG-1018 ends*
				/*if (domainList.contains(hostDomain) || isTrustedDomain) {*/
					chain.doFilter(request, response);
					/*} else {
					map.put("MESSAGE", "Referrer for the request is in valid");
					req.getSession().setAttribute("SCR_MAP", map);
					res.sendRedirect("noAccess.jsp");
				}*/
			} else {
				map.put("MESSAGE", "Access for the User-Agent is restricted");
				req.getSession().setAttribute("SCR_MAP", map);
				res.sendRedirect("noAccess.jsp");
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		
	}

	private static String getDomainName(String url) {
		try {
			URI uri = new URI(Utilities.trim(url));
			String domain = Utilities.trim(uri.getHost());
			return domain.startsWith("www.") ? domain.substring(4) : domain;
		} catch (URISyntaxException e) {
			
			logger.warn("Could not get domain of URL [{}]", url, e);
			return "";
		} catch (Exception e) {
			
			logger.warn("Could not get domain of URL [{}]", url, e);
			return "";
		}
	}
}
