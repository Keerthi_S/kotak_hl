/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ValidateCSRF.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 19-10-2018           Leelaprasad             TENJINCG-829
 * 29-11-2018           Prem            		   TENJINCG-837
 * 10-12-2018		   Prem		               TJNUN262-108
 * 13-12-2018           Padmavathi              TJNUN262-79
 * 06-05-2019		   Roshni				   TENJINCG-1036
 * 09-09-2019		   Prem				       TJN2.9R1-11

 */

package com.ycs.tenjin.servlet.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.common.cache.Cache;

/**
 * Servlet Filter implementation class AccessFilter
 */
//@WebFilter("/ValidateCSRF")
public class ValidateCSRF implements Filter {


	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {


		HttpServletRequest httpReq = (HttpServletRequest) request;
		Map<String, Object> map = new HashMap<String, Object>();
		/*modified by paneendra starts*/
		/*String servletName = httpReq.getHttpServletMapping().getServletName();*/
		String servletName = httpReq.getRequestURI().toString();
		/*modified by paneendra ends*/




		
		HttpSession session=httpReq.getSession(true);

		Cache<String, Boolean> csrfPreventionCache = (Cache<String, Boolean>)
				session.getAttribute("csrfTokenCache");
		boolean isMultiPart=false;
		isMultiPart = ServletFileUpload.isMultipartContent((HttpServletRequest) request);
		 /*modified by shruthi for TCGST-52 starts*/
		/*modified by paneendra starts*/
			
		if(httpReq.getMethod().equalsIgnoreCase("POST")&& isMultiPart!=true&&
	                !servletName.endsWith("AdminLaunchServlet") && !servletName.endsWith("jersey-serlvet") &&  !servletName.contains("TenjinWeb/rest")){
			/*modified by paneendra ends*/
			
			/* modified by shruthi for TCGST-52 ends*/
			String csrfsessiontoken=(String) session.getAttribute("csrftoken_session");
			String csrftoken=httpReq.getParameter("csrftoken_form");

			if (csrfPreventionCache != null &&
					csrftoken != null && csrftoken.equalsIgnoreCase(csrfsessiontoken)&&
					csrfPreventionCache.getIfPresent(csrftoken) != null){
				session.removeAttribute("csrftoken_session");
				session.removeAttribute("csrfTokenCache");
				chain.doFilter(request, response);
			} else {
				map.put("MESSAGE", "Referrer for the response is invalid");
				session.setAttribute("SCR_MAP", map);
				session.removeAttribute("csrftoken_session");
				session.removeAttribute("csrfTokenCache");
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
