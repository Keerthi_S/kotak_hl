/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AdminLaunchServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 25-10-2018            Leelaprasad             TENJINCG-826,TENJINCG-828
* 01-12-2018            Padmavathi               for TJNUN262-38 
* 08-12-2018            Sriram                   for TJNUN262-38
* 14-12-2018			Prem					 TJNUN262-57
* 
*/

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.util.Utilities;
/**
 * Servlet implementation class AdminLaunchServlet
 */
//@WebServlet("/AdminLaunchServlet")
public class AdminLaunchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(AdminLaunchServlet.class);
   
	Date endDate = null;
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	Calendar calendar = Calendar.getInstance();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminLaunchServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String param = request.getParameter("trigger");
		/*Changed by sriram for TJNUN262-32 starts*/
		param = Utilities.trim(param);
		/*Changed by sriram for TJNUN262-32 starts*/
		if(Utilities.trim(param).length() < 1) {
			//Redirect to admin page
			request.setAttribute("user_View", request.getSession().getAttribute("user_View"));
			request.getSession().removeAttribute("user_View");
			
			request.getRequestDispatcher("admin.jsp").forward(request, response);
		}
		/*Changed by sriram for TJNUN262-32 ends*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.info("Verifying Administrator Credentials");
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		RequestDispatcher dispatcher = null;
		logger.info("Verifying administrator credentials");
		/* Changed by Leelaprasad for TENJINCG-826,TENJINCG-828 starts */
		String entry=request.getParameter("entry");
		/*Added by Padmavathi for TJNUN262-38 starts */
		String param=request.getParameter("param");
		/*Added by Padmavathi for TJNUN262-38 ends */
		if(entry!=null&& entry.equalsIgnoreCase("project")){
			try {
				if(tjnSession !=null && tjnSession.getProject()!=null &&tjnSession.getUser()!=null){
				new ProjectHandler().endProjectSession(tjnSession.getProject().getId(), tjnSession.getUser().getId());
				}
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			}
		}
		/*Added by Padmavathi for TJNUN262-38 starts */
		if(tjnSession != null && tjnSession.getUser() != null &&param!=null && param.equalsIgnoreCase("user_view")){
			/*request.setAttribute("user_View", "true");*/
			request.getSession().setAttribute("user_View", "true");
			/*dispatcher = request.getRequestDispatcher("admin.jsp");*/
			dispatcher = request.getRequestDispatcher("AdminLaunchServlet");
		}
		/*Added by Padmavathi for TJNUN262-38 ends */
		/*Added by Padmavathi for TJNUN262-38 starts */
		if(tjnSession != null && tjnSession.getUser() != null &&param!=null && param.equalsIgnoreCase("user_view")){
			request.setAttribute("user_View", "true");
			dispatcher = request.getRequestDispatcher("admin.jsp");
		}
		/*Added by Padmavathi for TJNUN262-38 ends */
		/* Changed by Leelaprasad for TENJINCG-826,TENJINCG-828 ends */
		else if(tjnSession != null && tjnSession.getUser() != null && tjnSession.getUser().getRoles().equalsIgnoreCase("Site Administrator")){
			logger.info("Redirecting to Admin");
			/*Changed by sriram for TJNUN262-32 ends*/
			dispatcher = request.getRequestDispatcher("AdminLaunchServlet");
			/*Changed by sriram for TJNUN262-32 ends*/
		}else{
			logger.error("Invalid credentials.");
			request.getSession().setAttribute("ERROR_MSG", "Access to this page is restricted to Tenjin Administrators");
			/******************************************************************************
			 * Changed by Sriram for requirement TJN_22_14 (Role definition) 02-09-2015 starts
			 */
			/*Changed by sriram for TJNUN262-32 ends*/
			dispatcher = request.getRequestDispatcher("AdminLaunchServlet");
			/*Changed by sriram for TJNUN262-32 ends*/
			/******************************************************************************
			 * Changed by Sriram for requirement TJN_22_14 (Role definition) 02-09-2015 ends
			 */
		}
		/*Changed by sriram for TJNUN262-32 starts*/
		response.sendRedirect("AdminLaunchServlet");
		/*Changed by sriram for TJNUN262-32 ends*/
	}

}
