/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  PortingServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
*  31-Aug-2016		  SRiram					Req#TJN_23_28
*  3-Nov-2016		  Sriram					Defect#673
*  12-Dec-2016       Leelaprasad             Defect fix#TEN-29,TEN-30
*  13-12-2016        Leelaprasad             Fix for Defect#TEN-35
*  04-jul-2017		 Pushpalatha			 Req TENJINCG-266
*  05-07-2017        Leelprasad             TENJINCG-275 
*  18-01-2018		 Pushpalatha		    TENJINCG-584
*  01-02-2019		 Pushpalatha			TJN252-79
*  14-06-2019		 Pushpalatha			V2.8-82 
*  19-06-2019        Leelaprasad            V2.8-152
*  20-05-2020		 Priyanka               TENJINCG-1208
*/

package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.tika.Tika;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.port.PortMap;
import com.ycs.tenjin.port.PortXmlHelper;
import com.ycs.tenjin.port.PortingSession;
import com.ycs.tenjin.port.SpreadsheetPorter;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class PortingServlet
 */
//@WebServlet("/PortingServlet")
public class PortingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(PortingServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PortingServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String txn = request.getParameter("param");
		if(txn != null && txn.equalsIgnoreCase("new")){
			
			
			
			try {
				logger.info("Fetch All AUTs");
				List<Aut> auts = new AutHelper().hydrateAllAut();
				request.getSession().setAttribute("AUTS", auts);
				logger.info("Done");
			} catch (DatabaseException e) {
				
				logger.error("ERROR while fetching all AUTs", e);
			}
			logger.info("Redirecting");
			/*Changed by Leelaprasad for the Requirement Defect Fix TEN-29,TEN-30 on 12-12-2016 starts*/
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("message", "");
			map.put("status",  "");
			request.getSession().setAttribute("SCR_MAP", map);
			/*Changed by Leelaprasad for the Requirement Defect Fix TEN-29,TEN-30 on 12-12-2016 ends*/
			response.sendRedirect("porting_new.jsp");
		}else if(txn.equalsIgnoreCase("disc_view")){
			
			PortingSession pSession = (PortingSession)request.getSession().getAttribute("PORTING_SESSION");
			
			logger.info("Getting Target sheet map");
			Map<String, List<String>> targetSheetMap = new SpreadsheetPorter().getSheetData(pSession.getTargetFilePath());
			
			logger.info("Setting map to session");
			pSession.setTargetSheetMap(targetSheetMap);
			
			logger.info("Redirecting");
			response.sendRedirect("porting_discrepancy_view.jsp");
		}else if(txn.equalsIgnoreCase("tree")){
			try{
				
				PortingSession pSession = (PortingSession) request.getSession().getAttribute("PORTING_SESSION");
				String json = new PortXmlHelper().getJSTreePortMapJson(pSession.getSourceAppId(), pSession.getTargetAppId(), pSession.getSourceFunction(), pSession.getTargetFunc());
				response.getWriter().write(json);
			}catch(Exception e){
				response.getWriter().write("Internal error");
			}
		}else if(txn.equalsIgnoreCase("get_target_fields")){
			try {
				PortingSession pSession = (PortingSession) request.getSession().getAttribute("PORTING_SESSION");
				logger.info("Accessing target sheet map");
				Map<String, List<String>> targetSheetMap = pSession.getTargetSheetMap();
				
				
				String sheetName = request.getParameter("s");
				List<String> fields = targetSheetMap.get(sheetName);
				
				
				JSONArray jArray = new JSONArray();
				if(fields != null ){
					logger.info("{} fields found in target sheet {}", fields.size(), sheetName);
					for(String field:fields){
						JSONObject f = new JSONObject();
						f.put("field", field);
						
						jArray.put(f);
					}
				}
				
				JSONObject json = new JSONObject();
				json.put("status", "success");
				json.put("fields", jArray);
				logger.info("Forwarding response");
				
				response.getWriter().write(json.toString());
			} catch (Exception e) {
				
				logger.error("ERROR occurred while fetching fields for sheet", e);
				response.getWriter().write("{status:error, message:Could not fetch fields due to an internal error");
			}
			
		}
		  /*Changes Done by Pushpalatha For #TENJINCG-266 Starts*/
		else if(txn.equalsIgnoreCase("get_notmappedsource_fields")){
			try {
				PortingSession pSession = (PortingSession) request.getSession().getAttribute("PORTING_SESSION");
				logger.info("Accessing target sheet map");
				
				PortXmlHelper helper = new PortXmlHelper();
				
				String sheetName = request.getParameter("s");
				
	Map<String, List<String>> sfields=helper.getNotMappedSourceFields2(pSession.getSourceAppId(), pSession.getTargetAppId(), pSession.getSourceFunction(), pSession.getTargetFunc(), sheetName);
				
				System.out.println(sfields);
				List<String> fields = sfields.get(sheetName);
				
				System.out.println(fields);
				
				JSONArray jArray = new JSONArray();
				if(fields != null ){
					logger.info("{} fields found in target sheet {}", fields.size(), sheetName);
					for(String field:fields){
						JSONObject f = new JSONObject();
						f.put("field", field);
						System.out.println(f);
						jArray.put(f);
						System.out.println(jArray);
						
					}
				}
				
				JSONObject json = new JSONObject();
				json.put("status", "success");
				json.put("fields", jArray);
				
				System.out.println(jArray);
				logger.info("Forwarding response");
				
				response.getWriter().write(json.toString());
				
				System.out.println(response);
			} catch (Exception e) {
				
				logger.error("ERROR occurred while fetching fields for sheet", e);
				response.getWriter().write("{status:error, message:Could not fetch fields due to an internal error");
			}
			
		}
		/* Changes Done by Pushpalatha For #TENJINCG-266 ends*/
		else if(txn.equalsIgnoreCase("edit_map")){
			PortingSession pSession = (PortingSession) request.getSession().getAttribute("PORTING_SESSION");
			
			PortXmlHelper helper = new PortXmlHelper();
			
			String sourceSheet = request.getParameter("sourceSheet");
			String targetSheet = request.getParameter("targetSheet");
			String sourceField = request.getParameter("sourceField");
			String targetField = request.getParameter("targetField");
			
			try{
				logger.info("Updating map");
				helper.updateMapIndividual(pSession.getSourceAppId(), pSession.getTargetAppId(), pSession.getSourceFunction(), pSession.getTargetFunc(),sourceSheet, targetSheet, sourceField, targetField);
				JSONObject j = new JSONObject();
				j.put("status", "success");
				response.getWriter().write(j.toString());
			}catch(Exception e){
				/*Added by Leelprasad for the bug TENJINCG-275 starts*/
				/*response.getWriter().write("{status:error,message:Could not update due to an internal error. Please contact your Tenjin Administrator}");*/
				JSONObject j = new JSONObject();
				try {
					j.put("status", "fail");
					j.put("messege", e.getMessage());
				} catch (JSONException e1) {
					
					logger.error("Error ", e1);
				}
				response.getWriter().write(j.toString());
				/*Added by Leelprasad for the bug TENJINCG-275 starts*/
			}
		}
		/*<!--  Changes Done by Pushpalatha For #TENJINCG-266 Starts-->*/
		else if(txn.equalsIgnoreCase("update_map")){
			PortingSession pSession = (PortingSession) request.getSession().getAttribute("PORTING_SESSION");
			
			PortXmlHelper helper = new PortXmlHelper();
			
			String sourceSheet = request.getParameter("sourceSheet");
			String targetSheet = request.getParameter("targetSheet");
			String sourceField = request.getParameter("sourceField");
			String targetField ="";
			if(sourceField.toLowerCase().contains("button")){
				targetField =sourceField;
			}
			else{
			 targetField = request.getParameter("targetField");
			}
			try{
				logger.info("Updating map");
				
				helper.updateUnMappedeSourceFields(pSession.getSourceAppId(), pSession.getTargetAppId(), pSession.getSourceFunction(), pSession.getTargetFunc(),sourceSheet, targetSheet, sourceField, targetField);
				JSONObject j = new JSONObject();
				j.put("status", "success");
				System.out.println(j);
				response.getWriter().write(j.toString());
				System.out.println(response);
			}catch(Exception e){
				response.getWriter().write("{status:error,message:Could not update due to an internal error. Please contact your Tenjin Administrator}");
			}
		}
		/*<!--  Changes Done by Pushpalatha For #TENJINCG-266 ends-->*/
		else if(txn.equalsIgnoreCase("port")){
			PortingSession pSession = (PortingSession) request.getSession().getAttribute("PORTING_SESSION");
			
			SpreadsheetPorter porter = new SpreadsheetPorter();
			
			JSONObject json = new JSONObject();
			/*<!--  Changes Done by Pushpalatha For #TENJINCG-266 Starts-->*/
			String dataOrStyle=(String)request.getParameter("portData");
			/*<!--  Changes Done by Pushpalatha For #TENJINCG-266 ends-->*/
			
			try {
				String folderPath = request.getServletContext().getRealPath("/");
				String portMapXmlPath = TenjinConfiguration.getProperty("PORT_MAP_LOCATION");
				/************
				 * Fix by Sriram for Test data download path problem
				 */
				Utilities.checkForDownloadsFolder(folderPath);
				/************
				 * Fix by Sriram for Test data download path problem ends
				 * */
				/*Changed by Pushpalatha for TENJINCG-584 starts*/
				folderPath = folderPath + File.separator+"Downloads";
				portMapXmlPath = portMapXmlPath + File.separator + pSession.getSourceAppId() + "_" + pSession.getSourceFunction() + "_" + pSession.getTargetAppId() + "_" + pSession.getTargetFunc() + ".xml";
				/*Changed by Pushpalatha for TENJINCG-584 ends*/
				/*Changed by Leelaprasad for the requirement Defect fix TEN-35 on 13-12-2016 starts*/
				File file = new File(portMapXmlPath);
				Boolean xmlFlag=new PortXmlHelper().validatXML(file);
				if(xmlFlag){
					/*Changed by Leelaprasad for the requirement Defect fix TEN-35 on 13-12-2016 ends*/
				String outputFileName = porter.portData(pSession.getTargetFunc(), pSession.getSourceFilePath(), pSession.getTargetFilePath(), portMapXmlPath, folderPath,dataOrStyle);
				json.put("status", "success");
				/*Changed by Pushpalatha for TENJINCG-584 starts*/
				json.put("filepath", "Downloads"+File.separator + outputFileName);
				/*Changed by Pushpalatha for TENJINCG-584 ends*/
				response.getWriter().write(json.toString());
				}/*Changed by Leelaprasad for the requirement Defect fix TEN-35 on 13-12-2016 starts*/
				else{
					json.put("status", "Error");
					
					response.getWriter().write(json.toString());
				}
				/*Changed by Leelaprasad for the requirement Defect fix TEN-35 on 13-12-2016 ends*/
			} catch (Exception e) {
				
				logger.error("ERROR porting data", e);
				response.getWriter().write("{status:error,message:'An internal error occurred. Please contact your Tenjin Administrator");
			}
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		logger.debug("Entered doPost() in PortingServlet");
		/*<!--  Changes Done by Pushpalatha For #TENJINCG-266 Starts-->*/
		String txnType =null;
		boolean isMultiPart=false;
		Map<String, String> formValues = new HashMap<String, String>();
		Map<String, String> fileMap = new HashMap<String, String>();
		try{
		isMultiPart = ServletFileUpload.isMultipartContent(request);
		}catch(Exception port){}
		
		if(isMultiPart){
			DiskFileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			
			/*Changed by Pushpalatha for TENJINCG-584 starts*/
			/*File dir = new File(request.getSession().getServletContext().getRealPath("/") + File.separator+"Uploads");*/
			String uploadPath="";
			try {
				 uploadPath=TenjinConfiguration.getProperty("PORT_MAP_LOCATION");
			} catch (TenjinConfigurationException e1) {
				logger.error("logger.error(e.getMessage());"+e1.getMessage());
			}
			
			File dir = new File(uploadPath + File.separator+"Uploads");
			/*Changed by Pushpalatha for TENJINCG-584 ends*/
			if(!dir.exists()){
				dir.mkdirs();
			}
			/*Changed by Pushpalatha for TENJINCG-584 starts*/
			//String filePath = request.getSession().getServletContext().getRealPath("/") + File.separator+"Uploads";
			String filePath = dir.getAbsolutePath();
			/*Changed by Pushpalatha for TENJINCG-584 ends*/
			try {
				List fileItems = upload.parseRequest(request);
				Iterator fileItemsIter = fileItems.iterator();
				//gateway = new ExtractorGateway();
			
				String fullFilePath = "";
				while(fileItemsIter.hasNext()){
					FileItem fi = (FileItem)fileItemsIter.next();
					if(fi.isFormField()){
						formValues.put(fi.getFieldName(), fi.getString());
					}else{
						String fileName = fi.getName();
						String fieldName= fi.getFieldName();
						/*Added by Pushpa for TJN252-79 starts*/
						//Added  By Priyanka for VAPT Starts
						  if(!(fileName.endsWith(".xls") || fileName.endsWith(".xlsx")))
							{
		                    	 throw new RequestValidationException("Invalid Excel sheet");
							}
						//Added  By Priyanka for VAPT ends
						//File dir1 = new File(request.getSession().getServletContext().getRealPath("/") + File.separator+"Uploads"+File.separator+fieldName);
						File dir1 = new File(filePath+File.separator+fieldName);
						filePath=filePath+File.separator+fieldName;
						if(!dir1.exists()){
							dir1.mkdirs();
						}
						/*Added by Pushpa for TJN252-79 ends*/
						File file;
						/*Changed by Pushpalatha for TENJINCG-584 starts*/
						if( fileName.lastIndexOf(File.separator) >= 0 ){
							fullFilePath = filePath + File.separator + fileName.substring( fileName.lastIndexOf(File.separator));
						}else{
							fullFilePath = filePath + File.separator + fileName.substring(fileName.lastIndexOf(File.separator)+1);
						}
						/*Changed by Pushpalatha for TENJINCG-584 ends*/
						file = new File(fullFilePath);
						fi.write(file) ;
						/*Added by Priyanka for Vapt FileContent validation starts*/	
						File uploadedFile = new File(filePath,fileName);
						Tika tika = new Tika();
						String Exfile ="";
						try {
							Exfile = tika.detect(uploadedFile);
						} catch (IOException e) {
							logger.debug("Unable to Detect the File Content: " + e.getMessage());
						}
						
						   if(!(Exfile.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")||Exfile.equalsIgnoreCase("application/vnd.ms-excel"))){
							   throw new RequestValidationException("Invalid Excel sheet");
							   
						   }
						   /*Added by Priyanka for Vapt FileContent validation ends*/
						fileMap.put(fieldName, fullFilePath); 
						logger.info("{} written to path {}", fieldName, filePath);
					}
				}
				
				txnType=formValues.get("TXNTYPE");
				
			}catch(Exception er){}
			}
			else{
			txnType = request.getParameter("TXNTYPE");
			}
		/*<!--  Changes Done by Pushpalatha For #TENJINCG-266 ends-->*/
		if(txnType == null){
			txnType = "init";
		}
		if(txnType != null && txnType.equalsIgnoreCase("init")){
			
			//Fix for Defect#673 - Sriram 
			//boolean isMultiPart=false;
			/* Changes Done by Pushpalatha For #TENJINCG-266 Starts*/
			Map<String, Object> map = new HashMap<String, Object>();
			/* Changes Done by Pushpalatha For #TENJINCG-266 ends*/
			try {
				
				logger.info("Initiating Porting");
				
				String sApp = formValues.get("application");
				String tApp = formValues.get("applicationT");
				String sFunc = formValues.get("lstModules");
				String tFunc = formValues.get("lstModulesT");
				
				/*Added by Priyanka for TENJINCG-1208 starts*/
				String sAppName = formValues.get("appName");
				String tAppName = formValues.get("appNameT");
				/*Added by Priyanka for TENJINCG-1208 ends*/
				
				/**************
				 * Hard-coded path here. Replace with Test data template path
				 */
				
				String sourceFilePath = fileMap.get("sFile");
				String targetFilePath = fileMap.get("tFile");
				/*Changed by Leelaprasad for the Requirement Defect Fix TEN-29,TEN-30 on 12-12-2016 starts*/
				SpreadsheetPorter porter = new SpreadsheetPorter();
				Map<Boolean,String> validateMap = porter.validateExcelSheets(sourceFilePath, targetFilePath);
				String sheetName=null;
				Boolean key=true;
				for (Entry<Boolean, String> m : validateMap.entrySet()) {
			      sheetName=m.getValue();
			      key=m.getKey();
				}
				if(key){	
					/*Changed by Leelaprasad for the Requirement Defect Fix TEN-29,TEN-30 on 12-12-2016 ends*/
				PortingSession pSession = new PortingSession();
				pSession.setSourceAppId(Integer.parseInt(sApp));
				pSession.setTargetAppId(Integer.parseInt(tApp));
				pSession.setSourceFilePath(sourceFilePath);
				pSession.setSourceFunction(sFunc);
				pSession.setTargetFilePath(targetFilePath);
				pSession.setTargetFunc(tFunc);
				
				/*Added by Priyanka for TENJINCG-1208 start*/
				pSession.setSourceAppName(sAppName);
				pSession.setTargetAppName(tAppName);
				/*Added by Priyanka for TENJINCG-1208 ends*/
				
				String xmlFileName = sApp + "_" + sFunc + "_" + tApp + "_" + tFunc + ".xml";
				
				try {
					String portMapXmlPath = TenjinConfiguration.getProperty("PORT_MAP_LOCATION");
					/*Changed by Pushpalatha for TENJINCG-584 starts*/
					File file = new File(portMapXmlPath + File.separator + xmlFileName);
					/*Changed by Pushpalatha for TENJINCG-584 ends*/
					if(file.exists()){
						logger.info("Port map already exists");
						pSession.setPortMapExists(true);
					}else{
						logger.info("Port Map does not exist");
						pSession.setPortMapExists(false);
					}
					
				} catch (TenjinConfigurationException e) {
					
					logger.error(e.getMessage());
					pSession.setPortMapExists(false);
				}
				request.getSession().setAttribute("PORTING_SESSION", pSession);
				response.sendRedirect("porting_selection.jsp");
				/*Changed by Leelaprasad for the Requirement Defect Fix TEN-29,TEN-30 on 12-12-2016 starts*/
				}
				else{
					String message=null;
					/*Changed by Leelaprasad for the requirement V2.8-152 starts*/
					
					if(sheetName.contains("sourceFile")&&sheetName.contains("targetfile")){
						message="Uploaded source and target sheets are not valid test data templates";
					}else if(sheetName.contains("targetfile")){
						message="Uploaded target sheet is not a valid test data template";
					}else{
						message="Uploaded source sheet is not a valid test data template";
					}
					/*Changed by Leelaprasad for the requirement V2.8-152 ends*/
					map.put("message", message);
					map.put("status",  "Empty");
					request.getSession().setAttribute("SCR_MAP", map);
					response.sendRedirect("porting_new.jsp");
				}
				/*Changed by Leelaprasad for the Requirement Defect Fix TEN-29,TEN-30 on 12-12-2016 ends*/
			} catch (Exception e) {
				
				logger.error("ERROR --> an unknown error occurred while initiating porting session", e);
				response.sendRedirect("error.jsp");
			}
			
			//Fix for Defect#673 - Sriram ends
		}
		
		else if(txnType != null && txnType.equalsIgnoreCase("portchoice")){
			String option = request.getParameter("portChoice");
			if(option.equalsIgnoreCase("N")){
				logger.info("Beginning new porting");
				
				PortingSession pSession = (PortingSession)request.getSession().getAttribute("PORTING_SESSION");
				SpreadsheetPorter porter = new SpreadsheetPorter();
				Map<String, List<PortMap>> portMaps = porter.checkSheets(pSession.getSourceFilePath(), pSession.getTargetFilePath());
				
				try {
					new PortXmlHelper().createXML(portMaps, pSession.getSourceAppId(), pSession.getTargetAppId(), pSession.getSourceFunction(), pSession.getTargetFunc());
				} catch (Exception e) {
					
					logger.error("Error ", e);
				}
				
			}
			
			response.sendRedirect("PortingServlet?param=disc_view");
		}
		
	}

}
