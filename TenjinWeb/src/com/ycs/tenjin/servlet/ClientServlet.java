/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ClientServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 21-June-2017         Pushpalatha G           TENJINCG-246
* 19-August-2017       Sriram Sridharan        T25IT-259
* 31-Aug-2017		   Pushpalatha			   T25IT-381
* 19-Oct-2017		   Sriram Sridharan		   Entire file changed for TENJINCG-397
* 08-11-2017		   Preeti Singh			   TENJINCG-410
* 15-11-2017		   Preeti Singh			   TENJINCG-410
* 28-02-2019           Leelaprasad P           TENJINCG-983
* 22-03-2019           Padmavathi              TNJN27-36 &&TNJN27-35 
* 08-03-2019		   Sahana				   pCloudy
* 21-06-2019			Preeti					V2.8-71
* 04-12-2019			Preeti					TENJINCG-1174
*/

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.handler.ClientHandler;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Utilities;


/**
 * Servlet implementation class ClientServlet
 */
@WebServlet("/ClientServlet")
public class ClientServlet extends HttpServlet {
	
	  public static AuthorizationRule authorizationRule = new AuthorizationRule() {
	        
	        @Override
	        public boolean checkAccess(HttpServletRequest request) {
	        	if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
	                    return true;
	            }
	            return false;
	        }  
	    };
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory	.getLogger(ClientServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SessionUtils.loadScreenStateToRequest(request);
		
		String task = Utilities.trim(request.getParameter("t"));
		
		 Authorizer authorizer=new Authorizer();
			if(task.equalsIgnoreCase("view")||task.equalsIgnoreCase("new")||task.equalsIgnoreCase("")) {
				if(!authorizer.hasAccess(request)){
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("STATUS", "ERROR");
					map.put("MESSAGE", "User is not an administrator and hence cannot access");
					request.getSession().setAttribute("SCR_MAP", map);
					response.sendRedirect("noAccess.jsp");
					return;
				}
			}
		
		
		if(task.length() < 1 || task.equalsIgnoreCase("list")) {
			try {
				List<RegisteredClient> clients = new ClientHandler().getAllClients();
				request.setAttribute("clients", clients);
				request.getRequestDispatcher("v2/client_list.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching all clients", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}else if(task.equalsIgnoreCase("view")) {
			String cRecId = Utilities.trim(request.getParameter("key"));
			
			try {
				RegisteredClient client = new ClientHandler().getClient(Integer.parseInt(cRecId));
				request.setAttribute("client", client);
				request.getRequestDispatcher("v2/client_view.jsp").forward(request, response);
			} catch (NumberFormatException e) {
				logger.error("Invalid client ID {}", cRecId);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching client information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}else if(task.equalsIgnoreCase("new")) {
			RegisteredClient client = new RegisteredClient();
			client.setHostName(request.getRemoteHost());
			request.setAttribute("client", client);
			request.getRequestDispatcher("v2/client_new.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		TenjinSession tjnSession = SessionUtils.getTenjinSession(request);
		logger.info("entered doPost()");
		
		String delFlag = Utilities.trim(request.getParameter("del"));
		 Authorizer authorizer=new Authorizer();
		if(delFlag.equalsIgnoreCase(Utilities.trim(request.getParameter("del")))){
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "User is not an administrator and hence cannot access");
				request.getSession().setAttribute("SCR_MAP", map);
				return;
			}
		}
		
		
		if(delFlag.equalsIgnoreCase("true")) {
			doDelete(request, response);
		}else {
			String cId = Utilities.trim(request.getParameter("recordId"));
			
			RegisteredClient client = new RegisteredClient();
			client.setName(Utilities.trim(request.getParameter("name")));
			/*Modified by Preeti for V2.8-71 starts*/
			String hostName = Utilities.trim(request.getParameter("hostName"));
			if(hostName.equalsIgnoreCase("Localhost"))
				client.setHostName("0:0:0:0:0:0:0:1");
			else
				client.setHostName(Utilities.trim(request.getParameter("hostName")));
			/*Modified by Preeti for V2.8-71 ends*/
			client.setPort(Utilities.trim(request.getParameter("port")));
			/*Changed by Leelaprasad for TENJINCG-983 starts*/
			client.setOsType(Utilities.trim(request.getParameter("osType")));
			/*Changed by Leelaprasad for TENJINCG-983 starts*/
			/*Added by Sahana for pCloudy:Starts*/
			client.setDeviceFarmCheck(Utilities.trim(request.getParameter("devFarmCheck")));
			client.setDeviceFarmUsrName(Utilities.trim(request.getParameter("devFarmUsrName")));
			client.setDeviceFarmPwd(Utilities.trim(request.getParameter("devFarmPwd")));
			client.setDeviceFarmKey(Utilities.trim(request.getParameter("devFarmKey")));
			/*Added by Sahana for pCloudy:Ends*/
			
			ClientHandler handler = new ClientHandler();
			String dispatchUrl = "";
			
			if(cId.length() < 1 ) {
				logger.info("Request to register a new client");
				try {
					handler.persist(client, tjnSession.getUser().getId());
					/*Modified by Padmavathi for TNJN27-35 starts*/
					/*SessionUtils.setScreenState("success", "client.create.success", client.getRecordId(), request);*/
					SessionUtils.setScreenState("success", "client.create.success", client.getName(), request);
					/*Modified by Padmavathi for TNJN27-35 ends*/
					response.sendRedirect("ClientServlet?t=view&key=" + client.getRecordId());
				} catch (DatabaseException e) {
					logger.error(e.getMessage(), e);
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("client", client);
					request.getRequestDispatcher("v2/client_new.jsp").forward(request, response);
				} catch (RequestValidationException e) {
					logger.error(e.getMessage());
					dispatchUrl = "v2/client_new.jsp";
					SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("client", client);
					request.getRequestDispatcher(dispatchUrl).forward(request, response);
				} 
				/*Added by Preeti for TENJINCG-410 starts*/
				catch (ResourceConflictException e) {
					logger.error(e.getMessage());
					dispatchUrl = "v2/client_new.jsp";
					SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("client", client);
					request.getRequestDispatcher(dispatchUrl).forward(request, response);
				} catch (TenjinConfigurationException e) {
					logger.error("Error ", e);
				}
				/*Added by Preeti for TENJINCG-410 ends*/
			}else {
				logger.info("Request to update client.");
				try {
					client.setRecordId(Integer.parseInt(cId));
					handler.update(client);
					/*Modified by Preeti for TENJINCG-410 starts*/
					/*Modified by Padmavathi for TNJN27-36 starts*/
					/*SessionUtils.setScreenState("success", "client.update.success", cId, request);*/
					SessionUtils.setScreenState("success", "client.update.success", client.getName(), request);
					/*Modified by Padmavathi for TNJN27-36 ends*/
					/*Modified by Preeti for TENJINCG-410 ends*/
					response.sendRedirect("ClientServlet?t=view&key=" + client.getRecordId());
				} catch (DatabaseException e) {
					logger.error(e.getMessage());
					dispatchUrl = "v2/client_view.jsp";
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("client", client);
					request.getRequestDispatcher(dispatchUrl).forward(request, response);
				} catch (RequestValidationException e) {
					logger.error(e.getMessage());
					dispatchUrl = "v2/client_view.jsp";
					SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("client", client);
					request.getRequestDispatcher(dispatchUrl).forward(request, response);
				} 
			}
		}
		
		
	}
	
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] selectedRecordIds = request.getParameterValues("selectedClients");
		int[] cRecIds = new int[selectedRecordIds.length];
		
		for(int i=0; i<selectedRecordIds.length; i++) {
			cRecIds[i] = Integer.parseInt(selectedRecordIds[i]);
		}
		
		String clientId = Utilities.trim(request.getParameter("recordId"));
		boolean requestServedFromListPage = false;
		
		if(clientId.length() > 0) {
			try {
				Integer.parseInt(clientId);
				requestServedFromListPage = false;
			}catch(Exception e) {
				requestServedFromListPage = true;
			}
		}else {
			requestServedFromListPage = true;
		}
		
		ClientHandler handler = new ClientHandler();
		try {
			handler.deleteClients(cRecIds);
			if(cRecIds.length>1) {
				SessionUtils.setScreenState("success", "client.delete.multiple.success", request);
			}else {
				/*Modified by Padmavathi for TNJN27-35 starts*/
				SessionUtils.setScreenState("success", "client.delete.success", request);
				/*Modified by Padmavathi for TNJN27-35 ends*/
			}
			response.sendRedirect("ClientServlet");
		} catch (DatabaseException e) {
			SessionUtils.setScreenState("error", "generic.db.error", request);
			SessionUtils.loadScreenStateToRequest(request);
			if(requestServedFromListPage) {
				response.sendRedirect("ClientServlet");
			}else {
				response.sendRedirect("ClientServlet?t=view&key=" + clientId);
			}
		} catch (ResourceConflictException e) {
			SessionUtils.setScreenState("error", e.getMessage(), request);
			
			if(requestServedFromListPage) {
				response.sendRedirect("ClientServlet");
			}else {
				response.sendRedirect("ClientServlet?t=view&key=" + clientId);
			}
		}
		
	}

}
