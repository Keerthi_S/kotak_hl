/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UserPasswordServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 30-10-2017			Roshni Das			Newly added for TENJINCG-398
* 19-10-2018           	Leelaprasad             TENJINCG-829
* 29-04-2019			Pushpalatha				TENJINCG-1037
* 29-04-2019			Pushpalatha				TENJINCG-1035
* 02-05-2019			Roshni					TENJINCG-1035
* 03-12-2019			Pushpalatha				TENJINCG-1170
* */


package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.TenjinSessionHelper;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.TenjinMailHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;

/**
 * Servlet implementation class UserPasswordServlet
 */
@WebServlet("/UserPasswordServlet")
public class UserPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//Fix for TENJINCG-1164 (Sriram)
		private static final Logger logger = LoggerFactory	.getLogger(UserPasswordServlet.class);
		//Fix for TENJINCG-1164 (Sriram) ends
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserPasswordServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		SessionUtils.loadScreenStateToRequest(request);

		String userid = request.getParameter("userid");
		if(request.getParameter("operation").equalsIgnoreCase("firstLogin")){
			String loggedInUserId = SessionUtils.getTenjinSession(request).getUser().getId();
			/*Added by Pushpa for Desjardins VAPT fix starts*/
			if(!userid.equalsIgnoreCase(loggedInUserId)) {
				//TenjinSession session = new TenjinSession();
				TenjinSessionHelper sessionHelper=new TenjinSessionHelper();
				try {
					sessionHelper.removeSessionForUser(loggedInUserId);
					TenjinSession session = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
					session.setUser(null);
					session.setUrlSession(false);
					request.getSession().setAttribute("TJN_SESSION", session);
				} catch (DatabaseException e) {
					
					logger.error(e.getMessage(), e);
				}
				response.sendRedirect("noAccess.jsp");
				return;
			}
			/*Added by Pushpa for Desjardins VAPT fix ends*/
			request.setAttribute("userid", userid);
			request.setAttribute("entryPoint", "first");
			
		}
		else if(request.getParameter("operation").equalsIgnoreCase("changepassword")){
			//Fix for TENJINCG-1164 (Sriram)
			String loggedInUserId = SessionUtils.getTenjinSession(request).getUser().getId();
			if(!loggedInUserId.equalsIgnoreCase(userid)) {
				/*Modified by Pushpa for TENJINCG-1170 ends*/
				if(!SessionUtils.getTenjinSession(request).getUser().getRoles().equalsIgnoreCase("Site Administrator")) {
				/*Modified by Pushpa for TENJINCG-1170 starts*/
					logger.error("Error --> User [{}] is not allowed to change password of user [{}]", loggedInUserId, userid);
					SessionUtils.setScreenState("error",  "You are not authorized to view this page", request);
					response.sendRedirect("TenjinUserServlet?t=view&key="+loggedInUserId);
					return;
				}
			}
			//Fix for TENJINCG-1164 (Sriram) ends			request.setAttribute("userid", userid);
			request.setAttribute("userid", userid);
			request.setAttribute("CH_PWD_ENTRY", "");
		}//Further fixing for Admin Account takeover issue
		else {
			response.sendError(404);
			return;
		}
		//Further fixing for Admin Account takeover issue ends
		request.getRequestDispatcher("v2/user_password_change.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SessionUtils.loadScreenStateToRequest(request);
		logger.info("entered doPost()");
		UserHandler handler=new UserHandler();
		String currentPassword = request.getParameter("currentPassword");
		String newPassword = request.getParameter("newPassword");
		String confirmPassword = request.getParameter("confirmPassword");
		String userid = request.getParameter("userid");
		String entryPoint = request.getParameter("entryPoint");
		String dispatchUrl = "";
		/*Added by Pushpa for TENJINCG-1037 starts*/
		String securityQue1=request.getParameter("question1");
		String answer1=request.getParameter("answer1");
		String securityQue2=request.getParameter("question2");
		String answer2=request.getParameter("answer2");
		/*Added by Pushpa for TENJINCG-1037 ends*/		
		/*Added by Pushpa for TENJINCG-1035 starts*/
		String changeReason=request.getParameter("changeReason");
		/*Added by Pushpa for TENJINCG-1035 ends*/
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		String loggedInUserId = tjnSession.getUser().getId();
		try {
			/*Modified by Pushpa for TENJINCG-1035 starts*/
			handler.updateUserPassword(currentPassword,newPassword,confirmPassword,userid,loggedInUserId,changeReason);
			/*Modified by Pushpa for TENJINCG-1035 ends*/
			/*Added by Roshni for TENJINCG-1035 starts */
			try {
				TenjinMailHandler tmhandler=new TenjinMailHandler();
				User user= new UserHandler().getUser(userid);
				JSONObject mailContent=tmhandler.getMailContent(user.getId(),user.getFullName(), "Password", tjnSession.getUser().getFullName(), "update",
					new Timestamp(new Date().getTime()), user.getEmail(), "");
		
				new TenjinMailHandler().sendEmailNotification(mailContent);
			} catch (TenjinConfigurationException e) {
				logger.error("Error ", e);
			}
			/*Added by Roshni for TENJINCG-1035 ends */
			if (entryPoint.equals("first")) {
			/*Changed by leelaprasad for TENJINCG-829 starts*/
				/*RequestDispatcher dispatcher = request.getRequestDispatcher("landing_new.jsp");
				dispatcher.forward(request, response);*/
				/*Added by Pushpa for TENJINCG-1037 starts*/
				Map<String, String> usersecurity = new HashMap<String, String>();
				usersecurity.put(securityQue1,answer1);
				usersecurity.put(securityQue2,answer2);
				
				handler.updatePasswordSecurity(usersecurity,loggedInUserId);
				/*Added by Pushpa for TENJINCG-1037 ends*/
				response.sendRedirect("UserLandingServlet?key=first");
				/*Changed by leelaprasad for TENJINCG-829 ends*/
			} else {
				SessionUtils.setScreenState("success",  "user.password.update.success", request);
				response.sendRedirect("TenjinUserServlet?t=view&key="+userid);
			}
		} catch (DatabaseException e1) {
			
			logger.error(e1.getMessage(), e1);
			SessionUtils.setScreenState("error", "generic.db.error", request);
			SessionUtils.loadScreenStateToRequest(request);
			request.setAttribute("userid", userid);
			request.setAttribute("entryPoint", entryPoint);
			request.getRequestDispatcher("v2/user_password_change.jsp").forward(request, response);
		} catch (RequestValidationException e) {
			
			dispatchUrl="v2/user_password_change.jsp";
			SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
			SessionUtils.loadScreenStateToRequest(request);
			request.setAttribute("userid", userid);
			request.setAttribute("entryPoint", entryPoint);
			request.getRequestDispatcher(dispatchUrl).forward(request, response);
			}/*Added by Pushpa for TENJINCG-1037 starts*/
		catch (SQLException e) {
			
				logger.error(e.getMessage(), e);
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("userid", userid);
				request.setAttribute("entryPoint", entryPoint);
				request.getRequestDispatcher("v2/user_password_change.jsp").forward(request, response);
		}
		/*Added by Pushpa for TENJINCG-1037 ends*/
		/*Added by Pushpalatha for TENJINCG-1042 starts*/
		 catch (InvalidKeyException e) {
			
			logger.error("Error ", e);
		} catch (NoSuchAlgorithmException e) {
			
			logger.error("Error ", e);
		} catch (NoSuchPaddingException e) {
			
			logger.error("Error ", e);
		} catch (IllegalBlockSizeException e) {
			
			logger.error("Error ", e);
		} catch (BadPaddingException e) {
			
			logger.error("Error ", e);
		}
		/*Added by Pushpalatha for TENJINCG-1042 ends*/
	}

}
