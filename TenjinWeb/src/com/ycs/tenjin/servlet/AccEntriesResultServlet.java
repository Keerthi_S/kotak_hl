/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AccEntriesResultServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.run.ResultValidation;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.TestRun;

/**
 * Servlet implementation class AccEntriesResultServlet
 */
//@WebServlet("/AccEntriesResultServlet")
public class AccEntriesResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(AccEntriesResultServlet.class);   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccEntriesResultServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("deprecation")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String txn = request.getParameter("param");
		if(txn.equalsIgnoreCase("ACCENT_RES_PAINT")){
			request.getSession().removeAttribute("ACCEN_RES_MAP");
			@SuppressWarnings("unchecked")
			Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
			TestRun run = (TestRun)map.get("RUN");
			String srid = request.getParameter("recid");
			String iter = request.getParameter("i");
			int iteration = Integer.parseInt(iter);
			int stepRecordId = Integer.parseInt(srid);
			TestStep targetStep = null;
			for(TestCase tc:run.getTestSet().getTests()){
				for(TestStep step:tc.getTcSteps()){
					if(step.getRecordId() == stepRecordId){
						targetStep = step;
						break;
					}
				}
			}
			
			ArrayList<ValidationResult> results = new ArrayList<ValidationResult>();
			if(targetStep == null){
				logger.error("Invalid step record ID {}",srid);
				response.sendRedirect("error.jsp");
			}else{
				for(StepIterationResult ir:targetStep.getDetailedResults()){
					if(ir.getIterationNo() == iteration){
						for(ResultValidation validation:ir.getValidations()){
							if(validation.getName().equalsIgnoreCase("validations within step")){
								for(ValidationResult result:validation.getResults()){
									results.add(result);
								}
								break;
							}
						}
						break;
					}
				}
			}
			
			Map<String, Object> oMap = new HashMap<String,Object>();
			oMap.put("STATUS", "SUCCESS");
			oMap.put("RESULTS", results);
			oMap.put("STEP", targetStep);
			request.getSession().setAttribute("ACCEN_RES_MAP", oMap);
			response.sendRedirect("accentres.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
