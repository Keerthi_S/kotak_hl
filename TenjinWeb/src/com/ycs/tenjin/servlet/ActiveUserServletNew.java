/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ActiveUserServletNew.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 30-10-2017			Roshni Das			Newly added for TENJINCG-398
* 14-05-2018			Pushpalatha			TENJINCG-635
* */
package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;

/**
 * Servlet implementation class ActiveUserServletNew
 */
@WebServlet("/ActiveUserServletNew")
public class ActiveUserServletNew extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(ActiveUserServletNew.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ActiveUserServletNew() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SessionUtils.loadScreenStateToRequest(request);
		doGet(request, response);
		doGet(request, response);
		/*Changed by Pushpa for TENJINCG-635 starts*/
		String[] selectedUsers = request.getParameterValues("selectedUsers");
		TenjinSession tjnSession = SessionUtils.getTenjinSession(request);
		if(selectedUsers != null && selectedUsers.length > 0){
			/*Changed by Pushpa for TENJINCG-635 ends*/
			List<User> users;
			try {
				new UserHandler().clearUsers(selectedUsers);
				String loggedUser=tjnSession.getUser().getId();
				users = new UserHandler().getActiveUsers(loggedUser);
				request.setAttribute("users", users);
				SessionUtils.setScreenState("success","user.cleared.success",request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("v2/current_users.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching all clients", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}
	}

}
