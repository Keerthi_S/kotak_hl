
/***
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApiUploadServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
  09-Aug-2017          Gangadhar Badagi        T25IT-16,18
  16-Aug-2017          Gangadhar Badagi        T25IT-16
  19-Aug-2017          Gangadhar Badagi        T25IT-24
  24-Aug-2017		Gangadhar Badagi	       T25IT-16,18,24
  26-Aug-2017		Gangadhar Badagi	       T25IT-307
  29-Aug-2017		Gangadhar Badagi	       Changed the messages
22-Sep-2017			sameer gupta			For new adapter spec
  16-Nov-2017		  Gangadhar Badagi	       TENJINCG-451
  19-01-2017		Pushpalatha				   TenjinCG-588
  19-02-2018        Padmavathi                 added groupName to Ungrouped by default.
  06-02-2020			Roshni					TENJINCG-1168
  29-05-2020         Leelaprasad P              TJN210-100
  31-03-2021        Paneendra                  TENJINCG-1267

  */
	 



package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.XMLConstants;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.adapter.api.rest.wadl.Wadl;
import com.ycs.tenjin.adapter.api.rest.wadl.WadlException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.api.ApiApplicationFactory;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.utils.ApiUtilities;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ModuleHelper;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class ApiUploadServlet
 */
@WebServlet("/ApiUploadServlet")
public class ApiUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private static final Logger logger = LoggerFactory.getLogger(ApiUploadServlet.class);
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApiUploadServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String resourceType = request.getParameter("rtype");
		String app = request.getParameter("app");
		/*Added by Pushpalatha for defect T25IT-T25IT-313 starts*/
		Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("AUT_API_INFO_MAP");
		map.put("STATUS", null);
		/*Added by Pushpalatha for defect T25IT-T25IT-313 starts*/
		 Authorizer authorizer=new Authorizer();
			if(resourceType.equalsIgnoreCase("WSDL") || resourceType.equalsIgnoreCase("Swagger")||resourceType.equalsIgnoreCase("WADL")  ) {
				if(!authorizer.hasAccess(request)){
					response.sendRedirect("noAccess.jsp");
					return;
				}
			}
		
		request.getSession().removeAttribute("AVL_API_ADAPTERS");
		
		try {
			List<String> apiAdapters = ApiApplicationFactory.getAvailableAPIAdapters();
			request.getSession().setAttribute("AVL_API_ADAPTERS", apiAdapters);
		} catch (BridgeException e) {
			
			logger.error("ERROR getting list of availabe API Adapters", e);
		}
		
		if(resourceType.equalsIgnoreCase("Swagger")){
			request.getSession().setAttribute("listOfAPI", null);
			request.getSession().setAttribute("APP", app);
			RequestDispatcher dispatcher = request.getRequestDispatcher("v2/swagger_file_upload.jsp?rtype=" + resourceType + "&app=" + app);
			dispatcher.forward(request, response);
		}
		else{
		RequestDispatcher dispatcher = request.getRequestDispatcher("api_file_upload.jsp?rtype=" + resourceType + "&app=" + app);
		dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		boolean isMultiPart;
		isMultiPart = ServletFileUpload.isMultipartContent(request);
		String ret = "";
		Connection conn = null;		
		JSONObject json = new JSONObject();
		 Authorizer authorizer=new Authorizer();
			if(isMultiPart==true || isMultiPart==false  ) {
				if(!authorizer.hasAccess(request)){
					response.sendRedirect("noAccess.jsp");
					return;
				}
			}
		
		if(isMultiPart){
			logger.debug("Form is multipart");
			String status = "";
			try {
				try {


					DiskFileItemFactory factory = new DiskFileItemFactory();
					ServletFileUpload upload = new ServletFileUpload(factory);
					List fileItems = upload.parseRequest(request);

					Iterator fileItemsIter = fileItems.iterator();

					Map<String, String> formValues = new HashMap<String, String>();
					List<FileItem> uploadedFiles = new ArrayList<FileItem>();
					while(fileItemsIter.hasNext()){
						FileItem fi = (FileItem)fileItemsIter.next();
						if(fi.isFormField()){
							formValues.put(fi.getFieldName(), fi.getString());
						}else{
							uploadedFiles.add(fi);
						}
					}

					String resourceType = formValues.get("resourceType");
					String appId = formValues.get("appId");
					String createApi = formValues.get("createApi");
					String apiType = formValues.get("apiType");
					/*added by Padmavathi for setting  groupName to ungrouped bydefault starts*/
					String groupName="Ungrouped";
					/*added by Padmavathi for setting  groupName to ungrouped bydefault ends*/
					boolean createApiFromWSDL = false;
					if(Utilities.trim(createApi).equalsIgnoreCase("y")) {
						createApiFromWSDL = true;
					}
					
					//Connection conn = null;
					Aut aut = null;
					if(createApiFromWSDL) {
						try {
							conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
							aut = new AutHelper().hydrateAut(Integer.parseInt(appId));
						} catch (DatabaseException e) {
							
							logger.error("ERROR obtaining database connection");
							status = "warn";
						}
					}
					
					String mediaRepoPath = TenjinConfiguration.getProperty("MEDIA_REPO_PATH");
					/*Changed by Pushpalatha for TENJINCG-588 starts*/
					String finalPath = mediaRepoPath + File.separator+"applications"+ File.separator + appId + File.separator+"api"+File.separator + resourceType.toLowerCase();
					/*Changed by Pushpalatha for TENJINCG-588 ends*/
					ApiUtilities.checkPath(finalPath);
					status = "success";
					String message = "";
					/*Added by Gangadhar Badagi for T25IT-16,18,24 starts*/
					List<String> failedFiles=new ArrayList<String>();
					List<String> passedFiles=new ArrayList<String>();
					JSONArray jsonArray=new JSONArray();
					/*Added by Gangadhar Badagi for T25IT-16,18,24 ends*/
					for(FileItem fi: uploadedFiles) {
						/*Added by Gangadhar Badagi for T25IT-16,18,24 starts*/
						JSONObject jsonFiles=new JSONObject();
						/*Added by Gangadhar Badagi for T25IT-16,18,24 ends*/
						String file = fi.getName();
						String[] fileName = file.split(Pattern.quote("\\"));
						String finalFilePath = "";
						/*Added by Gangadhar Badagi for T25IT-18 starts*/
						if(resourceType.equalsIgnoreCase("wsdl")){
								try {
									hasElement(fi.getString(),fileName[fileName.length-1]);
									/*Added by Gangadhar Badagi for T25IT-16,18,24 starts*/
									jsonFiles.put("name", fileName[fileName.length-1]);
									jsonFiles.put("status", "success");
									jsonFiles.put("message", "Uploaded successfully.");
									jsonArray.put(jsonFiles);
									/*Added by Gangadhar Badagi for T25IT-16,18,24 ends*/
									passedFiles.add(fileName[fileName.length-1]);
								} catch (XMLStreamException e) {
										status = "error";
										failedFiles.add(fileName[fileName.length-1]);
										/*Added by Gangadhar Badagi for T25IT-16,18,24 starts*/
										jsonFiles.put("name", fileName[fileName.length-1]);
										jsonFiles.put("status", "error");
										jsonFiles.put("message", "Invalid WSDL File.");
										jsonArray.put(jsonFiles);
										/*Added by Gangadhar Badagi for T25IT-16,18,24 ends*/
										fi.delete();
										continue;
									
								}
								
						}
						/*Added by Gangadhar Badagi for T25IT-18 ends*/
						if(Utilities.trim(fileName[fileName.length-1]).equalsIgnoreCase("")){
							continue;
						}
						if(fileName[fileName.length-1].toLowerCase().endsWith(".xml")) {
							fileName[fileName.length-1] = fileName[fileName.length-1].substring(0, fileName[fileName.length-1].length()-4);
							fileName[fileName.length-1] = fileName[fileName.length-1] + "." + resourceType.toLowerCase();
						}
					
						try {
							/*Changed by Pushpalatha for TENJINCG-588 starts*/
							fi.write(new File(finalPath + File.separator + fileName[fileName.length-1]));
							finalFilePath = finalPath + File.separator + fileName[fileName.length-1];
							/*Changed by Pushpalatha for TENJINCG-588 ends*/
						} catch (Exception e) {
							
							logger.warn("Could not process uploaded file [{}]", fileName[fileName.length-1], e);
						}
						
						if(createApiFromWSDL && resourceType.equalsIgnoreCase("wsdl")) {
							Api api = new Api();
							String apiCode = fileName[fileName.length-1].replace(".wsdl", "");
							api.setCode(apiCode);
							api.setName(apiCode);
							/*api.setType("soap");*/
							api.setType(apiType);
							/*added by Padmavathi for setting  groupName to ungrouped by default starts*/
							api.setGroup(groupName);
							/*added by Padmavathi for setting  groupName to ungrouped by default ends*/
							api.setUrl(ApiUtilities.generateSOAPApiUrl(aut.getURL(), apiCode));
							api.setApplicationId(Integer.parseInt(appId));
							try {
								if(new ModuleHelper().hydrateModule(Integer.parseInt(appId), api.getCode())!=null){
									logger.error("ERROR - Module {} already exists for application {}", api.getCode(), appId);
								}else{
								ApiHelper helper = new ApiHelper();
								helper.clearApi(conn, Integer.parseInt(appId), apiCode);
								new ApiHelper().persistApi(api);
								json.put("status", "success");
								}
							} catch (DatabaseException e) {
								
								logger.warn("Could not create API from WSDL [{}]", fileName[fileName.length-1], e);
								status = "warn";
								message = "WSDLs were loaded successfully, but Tenjin could not create APIs due to an internal error.";
							}
						}else if(createApiFromWSDL && resourceType.equalsIgnoreCase("wadl")) {
							try {
								Wadl wadl = new Wadl(new File(finalFilePath));
								List<Api> apis = wadl.parseWadl();
								passedFiles.add(fileName[fileName.length-1]);
								/*Added by Gangadhar Badagi for T25IT-16,18,24 starts*/
								jsonFiles.put("name", fileName[fileName.length-1]);
								jsonFiles.put("status", "success");
								jsonFiles.put("message", "Uploaded successfully.");
								jsonArray.put(jsonFiles);
								/*Added by Gangadhar Badagi for T25IT-16,18,24 ends*/
								for(Api api : apis) {
									try {
										api.setType(apiType);
										/*added by Padmavathi for setting  groupName to ungrouped by default starts*/
										api.setGroup(groupName);
										/*added by Padmavathi for setting  groupName to ungrouped by default ends*/
										/*Added by Gangadhar Badagi for T25IT-24 starts*/
										/*Modified by Roshni TENJINCG-1168 starts */
										/*if(new ModuleHelper().hydrateModule(Integer.parseInt(appId), api.getCode())!=null){*/
										if(new ModuleHelper().hydrateModule(conn,Integer.parseInt(appId), api.getCode())!=null){
										/*Modified by Roshni TENJINCG-1168 ends */
											logger.error("ERROR - Module {} already exists for application {}", api.getCode(), appId);
											continue;
										}
										/*Modified by Roshni TENJINCG-1168 starts */
										/*Api doesApiExist=new ApiHelper().hydrateApi(Integer.parseInt(appId), api.getCode());*/
										Api doesApiExist=new ApiHelper().hydrateApi(Integer.parseInt(appId), api.getCode(),conn);
										/*Modified by Roshni TENJINCG-1168 ends */
										if(doesApiExist==null){
											/*Added by Gangadhar Badagi for T25IT-24 ends*/
											
											/* Changed by leelaprasad for TJN210-100 starts */
											/* new ApiHelper().persistApi(conn, aut.getId(), api); */
											 new ApiHelper().persistApi(conn, aut, api); 
											 /* Changed by leelaprasad for TJN210-100 ends */
										/*json.put("status", "success");*/
										}
										/*Changed by Gangadhar Badagi for TENJINCG-451 starts*/
										else{
											/* Changed by leelaprasad for TJN210-100 starts */
											/* new ApiHelper().updateApi(conn, aut.getId(), api,doesApiExist); */
											new ApiHelper().updateApi(conn, aut, api,doesApiExist);
											/* Changed by leelaprasad for TJN210-100 ends */
										}
										/*Changed by Gangadhar Badagi for TENJINCG-451 ends*/
									} catch (DatabaseException e) {
										
										status = "warn";
										message = "WADL was loaded successfully, but Tenjin was unable to create one or more APIs due to an internal error. ";
									}
								}
							} catch(WadlException e) {
								logger.warn("Could not create APIs from WADL", e);
								status = "error";
								/*Added by Gangadhar Badagi for T25IT-16 starts*/
								/*message = fileName+" is Invalid WADL File.Please choose proper WADL file.";*/
								failedFiles.add(fileName[fileName.length-1]);
								/*Added by Gangadhar Badagi for T25IT-16,18,24 starts*/
								jsonFiles.put("name", fileName[fileName.length-1]);
								jsonFiles.put("status", "error");
								jsonFiles.put("message", "Invalid WADL File.");
								jsonArray.put(jsonFiles);
								/*Added by Gangadhar Badagi for T25IT-16,18,24 ends*/
								Path path = Paths.get(finalFilePath);
								Files.delete(path);
								/*Added by Gangadhar Badagi for T25IT-16 ends*/
							
							}
						}
						/*Added by Gangadhar Badagi for T25IT-16 starts*/
						else if(!createApiFromWSDL && resourceType.equalsIgnoreCase("wadl")) {
							try {
								Wadl wadl = new Wadl(new File(finalFilePath));
								passedFiles.add(fileName[fileName.length-1]);
								/*Added by Gangadhar Badagi for T25IT-16,18,24 starts*/
								jsonFiles.put("name", fileName[fileName.length-1]);
								jsonFiles.put("status", "success");
								jsonFiles.put("message", "Uploaded successfully.");
								jsonArray.put(jsonFiles);
								/*Added by Gangadhar Badagi for T25IT-16,18,24 ends*/
								json.put("status", "success");
							} catch(WadlException e) {
								logger.warn("Could not create APIs from WADL", e);
								status = "error";
								failedFiles.add(fileName[fileName.length-1]);
								/*Added by Gangadhar Badagi for T25IT-16,18,24 starts*/
								jsonFiles.put("name", fileName[fileName.length-1]);
								jsonFiles.put("status", "error");
								jsonFiles.put("message", "Invalid WADL File.");
								jsonArray.put(jsonFiles);
								/*Added by Gangadhar Badagi for T25IT-16,18,24 ends*/
								Path path = Paths.get(finalFilePath);
								Files.delete(path);
							}
						}
						/*Added by Gangadhar Badagi for T25IT-16 ends*/
						
					}

					json.put("status", status);
					json.put("message", message);
					if(Utilities.trim(status).equalsIgnoreCase("warn")) {
						json.put("message", "WSDLs were loaded successfully, but Tenjin could not create APIs due to an internal error.");
					}
					if(Utilities.trim(status).equalsIgnoreCase("error")) {
						if(resourceType.equalsIgnoreCase("wadl")){
							/*Changed by Gangadhar Badagi for T25IT-307 starts*/
						/*json.put("message", failedFiles+" are invalid WADL File.Please choose proper WADL file");*/
							json.put("message", "Invalid WADL File(s).Please choose proper WADL file(s)");
							/*Changed by Gangadhar Badagi for T25IT-307 ends*/
						}
						if(resourceType.equalsIgnoreCase("wsdl")){
							/*Changed by Gangadhar Badagi for T25IT-307 starts*/
							/*json.put("message", failedFiles+" are invalid WSDL File.Please choose proper WSDL file");*/
							json.put("message","Invalid WSDL File(s).Please choose proper WSDL file(s)");
							/*Changed by Gangadhar Badagi for T25IT-307 ends*/
							}
					}
					/*Added by Gangadhar Badagi for T25IT-16,18,24 starts*/
					json.put("failed", failedFiles);
					json.put("passed", passedFiles);
					json.put("jsonArray", jsonArray);
					/*Added by Gangadhar Badagi for T25IT-16,18,24 ends*/
					
				} catch (FileUploadException e) {

					
					logger.error("ERROR processing files",e );
					json.put("status", "error");
					json.put("message", "Could not access Media Repository Path. Please contact your Tenjin Administrator.");
				} catch (TenjinConfigurationException e) {
					
					logger.error("ERROR processing files",e );
					json.put("status", "error");
					json.put("message", "Media Repository Path is not set. Please contact your Tenjin Administrator.");
				}

/*
				json.put("status", "success");*/
				ret = json.toString();
			}catch(JSONException e) {
				logger.error("JSONException occurred", e);
				ret = "{status:error,message:An internal error occurred. Please contact Tenjin Support.}";
			}


		}else{
			ret = "{status:error,message:Unsupported operation. Please contact Tenjin Support.}";
		}/*Added by paneendra for TENJINCG-1267 starts*/
		try {
			DatabaseHelper.close(conn);
		}catch(Exception e) {
			logger.error("Error in closing the db connection", e);
		}
		/*Added by paneendra for TENJINCG-1267 starts*/
		
		response.getWriter().write(ret);
	}
	
	/*Added by Gangadhar Badagi for T25IT-18 starts*/
	public static boolean hasElement(String document, String localName) throws XMLStreamException
		      {
		    Reader reader = new StringReader(document);
		    XMLStreamReader xml = null;
			try {
				/*modified by paneendra for VAPT FIX starts*/
				XMLInputFactory inputFactory = XMLInputFactory.newFactory();
				// This disables DTDs entirely for that factory
				inputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
				// This causes XMLStreamException to be thrown if external DTDs are accessed.
				inputFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
				// disable external entities
				inputFactory.setProperty("javax.xml.stream.isSupportingExternalEntities", false);

				xml = inputFactory.createXMLStreamReader(reader);
				
				/*modified by paneendra for VAPT FIX ends*/
				 while (xml.hasNext()) {
				        if (xml.next() == XMLStreamConstants.START_ELEMENT
				            && localName.equals(xml.getLocalName())) {
				          return true;
				        }
				      }
			} 
		    finally {
		      xml.close();
		    }
		    return false;
		  }
	/*Added by Gangadhar Badagi for T25IT-18 ends*/
}
