/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApiServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 28-03-2017           Bablu kumar     
 * 01-08-2017           Padmavathi              API story review fixed 
 * 01-08-2017           Gangadhar Badagi        TENJINCG-320
 * 01-08-2017           Gangadhar Badagi        To get start learning time 
 * 08-Aug-2017			Roshni					T25IT-9
 * 10-Aug-2017			Manish					T25IT-39
 * 16-Aug-2017			Manish					T25IT-149
 * 19-08-2017			Manish					T25IT-68
 * 26-08-2017			Pushpalatha				T25IT-290
 * 26-08-2017			Pushpalatha				T25IT-310
 * 18-08-2017           Leelaprasad             T25IT-329
 * 29-08-2017			Pushpalatha				T25IT-331
 * 29-08-2017			Pushpalatha				T25IT-288
 * 22-Sep-2017			sameer gupta			For new adapter spec
 * 13-Oct-2017			Preeti					TENJINCG-366 
 * 02-Feb-2018			Sriram					TENJINCG-415
 * 02-Feb-2018			Pushpalatha				TENJINCG-568
 * 08-Feb-2018          Padmavathi              TENJINCG-601
 * 16-02-2018			Preeti					TENJINCG-600
 * 25-06-2018           Padmavathi              T251IT-127
 * 28-06-2018			Preeti					T251IT-133
 * 03-07-2018			Preeti					T251IT-169
 * 05-07-2018           Padmavathi              T251IT-180
 * 24-12-2018			Preeti					TJN262R2-68
 * 22-03-2019			Preeti					TENJINCG-1018
 * 22-05-2019			Ashiki					V2.8-28,V2.8-27
 * 04-12-2019			Preeti					TENJINCG-1174
 * 06-02-2020			Roshni					TENJINCG-1168
 * 29-05-2020           Leelaprasad             TJN210-100
 * 28-09-2020           Shruthi                 TENJINCG-1216
 * 18-12-2020           Paneendra               TENJINCG-1239
 */

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.adapter.api.rest.wadl.Wadl;
import com.ycs.tenjin.aut.Group;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.api.ApiApplicationFactory;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiLearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Parameter;
import com.ycs.tenjin.bridge.pojo.aut.Representation;
import com.ycs.tenjin.bridge.utils.ApiUtilities;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.AutsHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ModuleHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.db.UserHelper;
import com.ycs.tenjin.handler.ApiHandler;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.util.Utilities;

public class APIServlet extends HttpServlet {
	private static final Logger logger = LoggerFactory
			.getLogger(APIServlet.class);
	private static final long serialVersionUID = 1L;
	UserHelper helper = new UserHelper();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public APIServlet() {
		super();
		
	}

	public static AuthorizationRule authorizationRule = new AuthorizationRule() {
        
        @Override
        public boolean checkAccess(HttpServletRequest request) {
        	if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
                    return true;
            }
            return false;
        }  
    };
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		TenjinSession tjnSession = (TenjinSession) request.getSession()
				.getAttribute("TJN_SESSION");
		String txn = request.getParameter("param");
		
		/*Added by Prem for  VAPT fix Start*/
		 Authorizer authorizer=new Authorizer();
			if(txn.equalsIgnoreCase("show_api_list") || txn.equalsIgnoreCase("new_api_aut") || 
					txn.equalsIgnoreCase("fetch_api") || txn.equalsIgnoreCase("delete_api") ||
					txn.equalsIgnoreCase("new_rest_operation") || txn.equalsIgnoreCase("refresh_operations") || 
					txn.equalsIgnoreCase("api_learning_history") || txn.equalsIgnoreCase("show_aut_api_info") ) {
				if(!authorizer.hasAccess(request)){
					response.sendRedirect("noAccess.jsp");
					return;
				}
			}
			
			/*Added by Prem for  VAPT fix Ends*/
		
		if(txn.equalsIgnoreCase("show_api_list")){
			Map<String, Object> map = new HashMap<>();
			String status = "";
			String message = "";
			String appId = request.getParameter("app");
			map.put("CURRENT_APP_ID", appId);
			if(Utilities.trim(appId).length() > 0) {
				try {
					List<Api> apis = this.getAppApisForApplication(appId);
					
					map.put("API_LIST", apis);
				} catch (DatabaseException e) {
					
					logger.error("ERROR getting list of APIs",e );
					status = "ERROR";
					message = "Could not get APIs due to an unexpected error. Please contact Tenjin Support.";
				}
			}
			
			try {
				List<Aut> auts = new AutHelper().hydrateAllAut();
				map.put("AUTS", auts);
				/*added by Preeti for TENJINCG-366 starts*/
				List<String> adapters = ApiApplicationFactory.getAvailableAPIAdapters();
				map.put("ADAPTERS", adapters);
				/*added by Preeti for TENJINCG-366 ends*/
			} catch (DatabaseException e) {
				
				logger.error("ERROR getting list of AUTs", e);
				status = "ERROR";
				message = "Could not get Application List due to an unexpected error. Please contact Tenjin Support.";
			}
			catch (BridgeException e) {
				
				logger.error("Error ", e);
			}
			
			if(!Utilities.trim(status).equalsIgnoreCase("error")){
				map.put("status", "SUCCESS");
			}
			
			map.put("message", message);
			
			request.setAttribute("API_MAP", map);
			request.getRequestDispatcher("apilist.jsp").forward(request, response);
		}
		
		/*Modified By Preeti for TENJINCG-600 starts*/
		/*added by Preeti for TENJINCG-366 starts*/
		if(txn.equalsIgnoreCase("show_filter_api_list")){
		/*else if(txn.equalsIgnoreCase("show_apiType_list")){*/
			Map<String, Object> map = new HashMap<>();
			String status = "";
			String message = "";
			String appId = request.getParameter("app");
			map.put("CURRENT_APP_ID", appId);
			String apiType=request.getParameter("apiType");
			map.put("CURRENT_API_TYPE", apiType);
			String apiGroup = request.getParameter("apiGroup");
			map.put("CURRENT_GRP_NAME", apiGroup);
			if(Utilities.trim(appId).length() > 0) {
				try {
					/*List<Api> apis = this.getAppApisForApplication(appId,apiType);*/
					List<Api> apis = this.getAppApisForApplication(appId,apiType,apiGroup);
					map.put("API_LIST", apis);
				} catch (DatabaseException e) {
					
					logger.error("ERROR getting list of APIs",e );
					status = "ERROR";
					message = "Could not get APIs due to an unexpected error. Please contact Tenjin Support.";
				}
			}
			
			try {
				List<Aut> auts = new AutsHelper().hydrateAllAut();
				map.put("AUTS", auts);
				List<String> adapters = ApiApplicationFactory.getAvailableAPIAdapters();
				map.put("ADAPTERS", adapters);
			} catch (DatabaseException e) {
				
				logger.error("ERROR getting list of AUTs", e);
				status = "ERROR";
				message = "Could not get Application List due to an unexpected error. Please contact Tenjin Support.";
			}
			catch (BridgeException e) {
				
				logger.error("Error ", e);
			}
			
			if(!Utilities.trim(status).equalsIgnoreCase("error")){
				map.put("status", "SUCCESS");
			}
			
			map.put("message", message);
			
			request.setAttribute("API_MAP", map);
			request.getRequestDispatcher("apilist.jsp").forward(request, response);
		}
		/*added by Preeti for TENJINCG-366 ends*/
		/*Modified By Preeti for TENJINCG-600 ends*/
		
		else if(txn.equalsIgnoreCase("show_aut_api_info")) {
			String appId = request.getParameter("app");
			/*Added by Preeti for T251IT-133 starts*/
			String showFile = request.getParameter("showFile");
			/*Added by Preeti for T251IT-133 ends*/
			Map<String, Object> map = new HashMap<>();
			try {
				Aut aut = new AutHelper().hydrateAut(Integer.parseInt(appId));
				
				if(aut != null && Utilities.trim(aut.getURL()).length() > 0){
					String autBaseUrl = ApiUtilities.getApplicationBaseUrl(aut.getURL());
					map.put("AUT_BASE", autBaseUrl);
					//TENJINCG-415
					map.put("API_AUTH_TYPE", aut.getApiAuthenticationType());
					//TENJINCG-415 ends
					aut.setApiBaseUrl(aut.getApiBaseUrl().replace(autBaseUrl, ""));
				}
				
				map.put("AUT", aut);
				/*Added by Preeti for T251IT-133 starts*/
				map.put("showFile", showFile);
				/*Added by Preeti for T251IT-133 ends*/
				map.put("STATUS", "SUCCESS");
			} catch (Exception e) {
				logger.error("ERROR hydrating AUT Information",e );
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "An internal error occurred. Please contact Tenjin Support.");
			}
			request.getSession().removeAttribute("AUT_API_INFO_MAP");
			request.getSession().setAttribute("AUT_API_INFO_MAP", map);
			
			request.getRequestDispatcher("aut_api_information.jsp").forward(request, response);
		}
		
		else if (txn.equalsIgnoreCase("fetch_all_apis")) {
			logger.debug("entered ApiServlet --> fetch_all_apis");
			Map<String, Object> map = new HashMap<String, Object>();
			//String apiId = request.getParameter("apiId");
			//String apiId ="";
			String appId = request.getParameter("lstAppId");

			// Added by suhasini on 11-04-2017 starts
			String redirect = request.getParameter("redirect");
			// Added by suhasini on 11-04-2017 ends

			//int app = Integer.parseInt(apiId);
			ArrayList<Aut> autList = new ArrayList<Aut>();

			try {
				/*Modified by Roshni TENJINCG-1168 starts */
				/*ApiHelper helper = new ApiHelper();*/
				ApiHandler handler = new ApiHandler();
				/*Modified by Roshni TENJINCG-1168 ends */
				request.getSession().setAttribute("TJN_SESSION", tjnSession);
				autList =new AutHelper().hydrateAllAut();

				// Added by suhasini on 11-04-2017 starts
				if (appId == null) {
					appId = "-1";
				}
				// Added by suhasini on 11-04-2017 ends
				/*Modified by Roshni TENJINCG-1168 starts */
				ArrayList<Api> api = handler.hydrateAllApi(appId);
				/*Modified by Roshni TENJINCG-1168 ends */
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				/*Changed by Pushpalatha for defect T25IT-290 starts*/
				if (api != null && api.size()>0) {
					/*Changed by Pushpalatha for defect T25IT-290 ends*/
					logger.info("Fetched {} APIs successfully", api.size());
					
					map.put("APILIST", autList);
					map.put("STATUS", "SUCCESS");
					map.put("MESSAGE", "");
					map.put("API", api);

					// Added by suhasini on 11-04-2017 starts
					if (redirect != null && redirect.equalsIgnoreCase("false")) {

						
						jsonMap.put("STATUS", "SUCCESS");
						jsonMap.put("API", new Gson().toJson(api));

						String finalData = new JSONObject(jsonMap).toString();
						response.getWriter().write(finalData);
						response.getWriter().flush();
						response.getWriter().close();
					}else {
						request.getSession().setAttribute("SCR_MAP", map);
						response.sendRedirect("apilist.jsp");
					}
					// Added by suhasini on 11-04-2017 ends


				}
				/*Added by Pushpalatha for defect T25IT-290 starts*/
				else{
					logger.info("No {} APIs found");
					jsonMap.put("STATUS", "ERROR");
					jsonMap.put("MESSAGE", "No APIs found.");
					String finalData = new JSONObject(jsonMap).toString();
					response.getWriter().write(finalData);
					response.getWriter().flush();
					response.getWriter().close();
				}
				/*Added by Pushpalatha for defect T25IT-290 ends*/

			} catch (Exception e) {
				logger.error("ERROR occurred while fetching APIs");
				logger.error(e.getMessage(), e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", e.getMessage());
				request.getSession().setAttribute("SCR_MAP", map);
				response.sendRedirect("error.jsp");
			}

		}
		//Added by Parveen for #405 Starts
		else if(txn.equalsIgnoreCase("FETCH_RESPONSE_TYPES")) {
			int appId = Integer.parseInt(request.getParameter("lstAppId"));
			String apiSel = request.getParameter("apiSel");
			String OperationSel = request.getParameter("OperationSel");
            String retData = "";
			try {
				JSONObject json = new JSONObject();
				try {
					
					String apiRespponseTypes = new ApiHelper().fetchAPIResponseTypes(appId, apiSel, OperationSel);

					json.put("status", "SUCCESS");
					json.put("message", "");
					json.put("responseTypes", apiRespponseTypes);
				} catch (Exception e) {
					logger.error("An error occurred while processing Response Types", e);
					json.put("status", "ERROR");
					json.put("message",
							"An internal error occurred. Please try again.");

				} finally {
					retData = json.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);
		
		}
		//Added by Parveen for #405 Starts
		/*Added by Pushpalatha for defect T25IT-288 starts*/
		else if(txn.equalsIgnoreCase("fetch_apis_type")){
			
			Map<String, Object> map = new HashMap<String, Object>();
			String appId = request.getParameter("lstAppId");
			String appCode = request.getParameter("appCode");
			String redirect = request.getParameter("redirect");
			String apiType;
			try {
				ApiHelper helper = new ApiHelper();
				request.getSession().setAttribute("TJN_SESSION", tjnSession);
				apiType=helper.getApiType(Integer.parseInt(appId), appCode);
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				if (apiType!=null) {
					map.put("APITYPE", apiType);
					map.put("STATUS", "SUCCESS");
					map.put("MESSAGE", "");
					if (redirect != null && redirect.equalsIgnoreCase("false")) {
						jsonMap.put("STATUS", "SUCCESS");
						jsonMap.put("API", new Gson().toJson(apiType));

						String finalData = new JSONObject(jsonMap).toString();
						response.getWriter().write(finalData);
						response.getWriter().flush();
						response.getWriter().close();
					}else {
						request.getSession().setAttribute("SCR_MAP", map);
						response.sendRedirect("apilist.jsp");
					}
					
				}
				else{
					logger.info("No {} APIs found");
					jsonMap.put("STATUS", "ERROR");
					jsonMap.put("MESSAGE", "No APIs are found.");
					String finalData = new JSONObject(jsonMap).toString();
					response.getWriter().write(finalData);
					response.getWriter().flush();
					response.getWriter().close();
				}
				
			} catch (Exception e) {
				logger.error("ERROR occurred while fetching APIs");
				logger.error(e.getMessage(), e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", e.getMessage());
				request.getSession().setAttribute("SCR_MAP", map);
				response.sendRedirect("error.jsp");
			}

			
		}
		/*Added by Pushpalatha for defect T25IT-288 ends*/
		else if (txn.equalsIgnoreCase("new_api_aut")) {
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				List<Aut> autList = new AutHelper().hydrateAllAut();
				List<String> adapters = ApiApplicationFactory.getAvailableAPIAdapters();
				map.put("AUTS", autList);
				map.put("ADAPTERS", adapters);
				
			} catch (DatabaseException e) {
				
				logger.error("ERROR fetching all applications", e);
			} catch (BridgeException e) {
				
				logger.error("ERROR getting all API Bridges", e);
			}
			
			request.getSession().removeAttribute("API_MAP");
			request.getSession().setAttribute("API_MAP", map);
			
			response.sendRedirect("apidetails.jsp");
		}else if(txn.equalsIgnoreCase("fetch_api")){

			 String apiName = request.getParameter("paramval");
			 /*added by Preeti for TENJINCG-366 starts*/
			 String apiFilterType=request.getParameter("apitypefilter");
			 /*added by Preeti for TENJINCG-366 ends*/
			 String appId = request.getParameter("appid");
			 /*Added by Preeti for TENJINCG-600 starts*/
			 String apiGroup = request.getParameter("apigroup");
			 /*Added by Preeti for TENJINCG-600 ends*/
			 ApiHelper apiHelper = null;
			 Api api = null;
			 Map<String, Object> map = new HashMap<String, Object>();

			 try {
				 apiHelper = new ApiHelper();

				 api = apiHelper.hydrateApi(Integer.parseInt(appId), apiName);
				 
				 List<String> adapters = ApiApplicationFactory.getAvailableAPIAdapters();
				 if (api != null) {

					 api.setWsdlAvailable(ApiUtilities.apiHasWSDL(Integer.parseInt(appId), api.getCode()));

					 map.put("STATUS", "");
					 map.put("MESSAGE", "");
					 map.put("API", api);
					 map.put("ADAPTERS", adapters);
					 /*added by Preeti for TENJINCG-366 starts*/
					 map.put("API_FILTER_TYPE", apiFilterType);
					 /*added by Preeti for TENJINCG-366 ends*/
					/* Added by Preeti for TENJINCG-600 starts*/
					 map.put("API_GROUP", apiGroup);
					/* Added by Preeti for TENJINCG-600 ends*/
				 } else {
					 map.put("STATUS", "FAILURE");
					 map.put("MESSAGE", "API not found");
				 }
			 } catch (Exception e) {
				 map.put("STATUS", "ERROR");
				 map.put("MESSAGE",
						 "An Internal Error occurred. Please contact your System Administrator");
			 } finally {
				 request.getSession().setAttribute("SCR_MAP", map);
			 }

			 request.getSession().removeAttribute("LRNR_INIT_STATUS");
			 request.getSession().removeAttribute("LRNR_INIT_MESSAGE");

			 response.sendRedirect("editapidetails.jsp");

		 }else if(txn.equalsIgnoreCase("edit_api")){
			/* Modified by Padmavathi for T251IT-127 starts*/
			 logger.debug("Updating API details");
				String json = request.getParameter("json");

				JSONObject retJson = new JSONObject();
				String retData = "";
				try {

						JSONObject j = new JSONObject(json);
						Api api = new Api();
						api.setApplicationId(Integer.parseInt(j.getString("appId")));
						api.setName(j.getString("apiname"));
						api.setCode(j.getString("apicode"));
						api.setUrl(j.getString("apiurl"));
						api.setGroup(j.getString("gropuName"));
						api.setEncryptionKey(j.getString("encryptionKey"));
						ApiHelper apiHelper = new ApiHelper();
						apiHelper.updateApi(api);
						retJson.put("status", "SUCCESS");
						retJson.put("message","API updated Successfully.");
					
					} catch (Exception e) {
							try {
							retJson.put("message", "Could not update due to an internal error. Please contact Tenjin Support.");
						} catch (JSONException e1) {
							
							logger.error("Error ", e1);
						}
					} finally {
						retData = retJson.toString();
					}
					response.getWriter().write(retData);
					/* Modified by Padmavathi for T251IT-127 ends*/
		 }else if(txn.equalsIgnoreCase("delete_api")){
			 String apiCodes = request.getParameter("apiCodes");
			 /*changed by manish for bug T25IT-68 on 19-Aug-2017 starts*/
			 String appId = request.getParameter("appId");
			 /*changed by manish for bug T25IT-68 on 19-Aug-2017 ends*/
			 String[] apiCode = apiCodes.split(",");
			 JSONObject retJson = new JSONObject();
			 String retData = "";
			 try {

				 try {
					 ApiHelper apiHelper = new ApiHelper();
					 apiHelper.clearApiOperations(apiCode, Integer.parseInt(appId));
				
					 /*Added by Pushpalatha for defect T25IT-331 starts*/
					 Api api=null;
					 ArrayList<Api> apilist=new ArrayList<Api>();
					 for(int i=0; i<apiCode.length; i++){
						 api=apiHelper.hydrateApi(Integer.parseInt(appId), apiCode[i]);
						 if(api!=null){
						 apilist.add(api);
						 }
					 }
					 if(apilist.size()>0){
						 if(apilist.size()==1){
							 retJson.put("status", "ERROR");
							/*Changed by Ashiki for V2.8-28 starts*/
							 retJson.put("message", "Could not delete API because API is already learnt");
							 /*Changed by Ashiki for V2.8-28 ends*/
						 }else{
						 retJson.put("status", "ERROR");
						 /*Changed by Ashiki for V2.8-28 starts*/
						 retJson.put("message", "Could not delete API(s) because some of the API(s) are already learnt");
						 /*Changed by Ashiki for V2.8-28 ends*/
						 }
						 
						/* Added by Pushpalatha for defect T25IT-331 ends*/
					 }else{
						 retJson.put("status", "SUCCESS");
						 /*Changed by Ashiki for V2.8-28 starts*/
						 retJson.put("message", "API(s) Deleted Successfully");
						 /*Changed by Ashiki for V2.8-28 ends*/
					 }

				 } catch (DatabaseException e) {
					 retJson.put("status", "ERROR");
					 retJson.put("message", "Could not delete user records");
				 } finally {
					 retData = retJson.toString();
				 }
			 } catch (Exception e) {
				 retData = "An internal error occurred. Please contact your System Administrator";
			 }
			 response.getWriter().write(retData);
		 }

		 else if(txn.equalsIgnoreCase("refresh_operations")) {
			 String aId = request.getParameter("appId");
			 String apiCode = request.getParameter("apiCode");
			 JSONObject responseJson = new JSONObject();
			 String ret = "";

			 try {
				 try{
					 ApiUtilities.getAllApiOperations(Integer.parseInt(aId), apiCode, true);
					 responseJson.put("status", "success");
				 } catch (NumberFormatException e) {
					 
					 logger.error("Invalid Application ID [{}]", aId);
					 responseJson.put("status", "error");
					 responseJson.put("message", "An internal error occurred. Please contact Tenjin Support.");
				 } catch (BridgeException e) {
					 
					 responseJson.put("status", "error");
					 /*changed by manish for bug T25IT-39 on 10-09-2017 starts*/
					/*Modified by Pushpa for Tenj210-58 starts	*/
					 responseJson.put("message", e.getMessage());
					 /*Modified by Pushpa for Tenj210-58 ends	*/
					 /*changed by manish for bug T25IT-39 on 10-09-2017 ends*/
				 }

				 ret = responseJson.toString();
			 } catch (JSONException e) {
				 logger.error("JSONException occurred", e);
				 ret = "{status:error,message:An internal error occurred. Please contact Tenjin Support.}";
			 }
			 response.getWriter().write(ret);
		 }

		 else if(txn.equalsIgnoreCase("fetch_wsdls_for_aut")) {
			 String ret = "";
			 String aId = request.getParameter("appId");
			 /*Added by Preeti for T251IT-133 starts*/
			 String showFile = request.getParameter("showFile");
			 /*Added by Preeti for T251IT-133 ends*/
			 JSONObject json = new JSONObject();
			 try {
				 json.put("status", "success");
				 try {
					 int appId = Integer.parseInt(aId);
					 /*Modified by Preeti for T251IT-169 starts*/
					 JSONArray jArray = ApiUtilities.getWSDLsForAut(appId);
					 if(jArray.length()>0){
					 /*Added by Preeti for T251IT-133 starts*/
					 if(showFile.equalsIgnoreCase("no"))
						 ApiUtilities.deleteWSDLsForAut(appId);
					 }
					 /*Added by Preeti for T251IT-133 ends*/
					 jArray = ApiUtilities.getWSDLsForAut(appId);
					 /*Modified by Preeti for T251IT-169 ends*/
					 json.put("wsdls", jArray);
				 } catch (NumberFormatException e) {
					 
					 json.put("status", "error");
					 json.put("message", "Invalid Application ID " + aId);
				 } catch (AutException e) {
					 json.put("status", "error");
					 json.put("message", e.getMessage());
				 } catch (TenjinConfigurationException e) {
					 
					 json.put("status", "error");
					 json.put("message", "A Tenjin configuration issue occurred. Please contact your Tenjin Administrator.");
				 }

				 ret = json.toString();
			 } catch (JSONException e) {
				 logger.error("JSONException occurred", e);
				 ret = "{status:success,message:An internal error occurred. Please contact Tenjin Support.}";
			 } 

			 response.getWriter().write(ret);
		 }

		 else if(txn.equalsIgnoreCase("fetch_xsds_for_aut")) {
			 String ret = "";
			 String aId = request.getParameter("appId");

			 JSONObject json = new JSONObject();
			 try {
				 json.put("status", "success");
				 try {
					 int appId = Integer.parseInt(aId);
					 JSONArray jArray = ApiUtilities.getXSDsForAut(appId);
					 json.put("wsdls", jArray);
				 } catch (NumberFormatException e) {
					 
					 json.put("status", "error");
					 json.put("message", "Invalid Application ID " + aId);
				 } catch (AutException e) {
					 json.put("status", "error");
					 json.put("message", e.getMessage());
				 } catch (TenjinConfigurationException e) {
					 
					 json.put("status", "error");
					 json.put("message", "A Tenjin configuration issue occurred. Please contact your Tenjin Administrator.");
				 }

				 ret = json.toString();
			 } catch (JSONException e) {
				 logger.error("JSONException occurred", e);
				 ret = "{status:success,message:An internal error occurred. Please contact Tenjin Support.}";
			 } 

			 response.getWriter().write(ret);
		 } else if (txn.equalsIgnoreCase("validate_url")) {

		 } else if(txn.equalsIgnoreCase("load_descriptor")) {

			 String a = request.getParameter("app");
			 String apiCode = request.getParameter("api");
			 String operationName = request.getParameter("op");
			 String entityType = request.getParameter("type");

			 JSONObject json = new JSONObject();


			 String ret ="";
			 try {
				 try {
					 ApiOperation operation = new ApiHelper().hydrateApiOperation(Integer.parseInt(a), apiCode, operationName);
					
					 if("request".equalsIgnoreCase(entityType)) {
							
							 json.putOpt("descriptor", operation.getListRequestRepresentation());
						 }else {
							
							 json.putOpt("descriptor", operation.getListResponseRepresentation());
						 }


					 json.put("status", "success");
				 } catch (NumberFormatException e) {
					 
					 json.put("status", "error");
					 logger.error("Invalid Application ID [{}}", a);
					 json.put("message", "Invalid Application ID");
				 } catch (DatabaseException e) {
					 
					 json.put("status", "error");
					 json.put("message", "An internal error occurred. Please contact Tenjin Support.");
				 }

				 ret = json.toString();
			 } catch (JSONException e) {
				 
				 logger.error("JSONException occurred", e);

				 ret = "{status:error,message:An internal error occurred. Please contact Tenjin Support.}";
			 }

			 response.getWriter().write(ret);

			 // Added by suhasini on 11-04-2017 starts
		 }
		/* Added by Roshni for T25IT-9 starts */
		 else if(txn.equalsIgnoreCase("fetch_wadls_for_aut")) {
			 String ret = "";
			 String aId = request.getParameter("appId");
			 /*Added by Preeti for T251IT-133 starts*/
			 String showFile = request.getParameter("showFile");
			 /*Added by Preeti for T251IT-133 ends*/
			 JSONObject json = new JSONObject();
			 try {
				 json.put("status", "success");
				 try {
					 int appId = Integer.parseInt(aId);
					 /*Modified by Preeti for T251IT-169 starts*/
					 JSONArray jArray = ApiUtilities.getWADLsForAut(appId);
					 /*Added by Preeti for T251IT-133 starts*/
					 if(jArray.length()>0){
					 if(showFile.equalsIgnoreCase("no"))
						 ApiUtilities.deleteWADLsForAut(appId);
					 }
					 /*Added by Preeti for T251IT-133 ends*/
					 jArray = ApiUtilities.getWADLsForAut(appId);
					 /*Modified by Preeti for T251IT-169 ends*/
					 json.put("wadls", jArray);
				 } catch (NumberFormatException e) {
					 
					 json.put("status", "error");
					 json.put("message", "Invalid Application ID " + aId);
				 } catch (AutException e) {
					 json.put("status", "error");
					 json.put("message", e.getMessage());
				 } catch (TenjinConfigurationException e) {
					 
					 json.put("status", "error");
					 json.put("message", "A Tenjin configuration issue occurred. Please contact your Tenjin Administrator.");
				 }

				 ret = json.toString();
			 } catch (JSONException e) {
				 logger.error("JSONException occurred", e);
				 ret = "{status:success,message:An internal error occurred. Please contact Tenjin Support.}";
			 } 

			 response.getWriter().write(ret);
		 }
		 /* Added by Roshni for T25IT-9 ends */
		 else if (txn.equalsIgnoreCase("fetch_operations")) {
			 String app = request.getParameter("app");
			 String code = request.getParameter("code");
			 ApiHelper apiHelper = new ApiHelper();
			 List<ApiOperation> opreations = apiHelper.fetchApiOperations(app, code);

			 Map<String, Object> map = new HashMap<String, Object>();

			 if (opreations!= null && opreations.size() > 0) {
				 map.put("STATUS", "SUCCESS");
			 } else {
				 map.put("STATUS", "ERROR");
				/* Added by Pushpalatha for defect T25IT-290 starts*/
			   map.put("MESSAGE", "No operations found");
			  /* Added by Pushpalatha for defect T25IT-290 ends*/
			 }
			 map.put("OPERATIONS", new Gson().toJson(opreations));

			 response.getWriter().write(new JSONObject(map).toString());
			 response.getWriter().flush();
			 response.getWriter().close();

			 // Added by suhasini on 11-04-2017 ends	..
			 /*soap.generic,rest.generic, soap.fcubs*/
		 }else if (txn.equalsIgnoreCase("new_rest_operation")) {
			 String appid = request.getParameter("appid");
			 String code = request.getParameter("code");
			 String optName = request.getParameter("optName");
			 String apiType=request.getParameter("apiType");
			 String apiURL=request.getParameter("apiURL");
				
			 String status = null;
			 String message = null;
				
				Map<String,Object> map = new HashMap<String, Object>();
				request.getSession().setAttribute("SCR_MAP", map);
			 if(optName != null){
				 ApiOperation operation = null;
					try {
						operation = new ApiHelper().hydrateApiOperation(Integer.parseInt(appid), code, optName);
					}catch (Exception e) {
						
						status = "error";
						message = "Could not fetch information about API operation ["+optName+"].";
					}
					map.put("STATUS", status);
					map.put("MESSAGE", message);
					map.put("API_OPERATION", operation);
					if(apiType.equalsIgnoreCase("rest.generic")){
						
					response.sendRedirect("viewRestOperation.jsp?appid="+ appid+"&code="+code+"&optName="+optName);
					
					}else if(apiType.contains("soap")){
						
						response.sendRedirect("viewSoapOperation.jsp?appid="+ appid+"&code="+code+"&optName="+optName);
					}
				}else{
					// changes done by Parveen for parameter List starts 
					if(apiURL!=""){
						Wadl objWadl= new Wadl();
						List<Parameter> listParameters=objWadl.getParameterNodesTemp(apiURL);
						if(listParameters.size()>0){
							/*ApiOperation operation=new ApiOperation();
							operation.setResourceParameters(listParameters);*/
							map.put("API_OPERATION_PARAMLIST", listParameters);
						}
					}
					// changes done by Parveen for parameter List ends
					response.sendRedirect("newRestOperation.jsp?appid="+ appid+"&code="+code+"&optName="+optName);
				}
				/*response.sendRedirect("newRestOperation.jsp?appid="+ appid+"&code="+code+"&optName="+optName);*/
			   
			 // Added by suhasini on 11-04-2017 ends	
		 }
		
		/*Added by Gangadhar Badagi for TENJINCG-320 starts*/		
		 else if (txn.equalsIgnoreCase("api_learning_history")) {		
				String appId=request.getParameter("appId");		
				String apiCode=request.getParameter("apiCode");		
				String apiName=request.getParameter("apiName");		
				String appName=null;		
				try {		
					Aut aut=new AutHelper().hydrateAut(Integer.parseInt(appId));		
					if(aut!=null){		
						appName=aut.getName();		
					}		
				} catch (DatabaseException e1) {		
							
					logger.error("could not fetch aut name");		
				}		
				Map<String, Object> map = new HashMap<String, Object>();		
				try{		
					ArrayList<ApiLearnerResultBean> lrnr_runs=new ApiHelper().hydrateLearningHistory(apiCode, Integer.parseInt(appId));		
					map.put("APPID", appId);		
					map.put("API_CODE",apiCode);		
					map.put("API_NAME", apiName);		
					map.put("APP_NAME",appName);		
							
					if(lrnr_runs != null && lrnr_runs.size() > 0){		
						map.put("STATUS","SUCCESS");		
						map.put("SUMMARY", lrnr_runs);		
						map.put("MESSAGE", "API Learning History successfully fetched");		
					}else{		
						map.put("STATUS", "FAILURE");		
						map.put("MESSAGE","This API has not been learnt yet");		
					}		
					
				}catch(Exception e){		
					map.put("STATUS", "ERROR");		
					map.put("MESSAGE","An Internal error occurred.");		
				}finally{		
					request.getSession().setAttribute("SCR_MAP", map);		
				}		
						
				response.sendRedirect("api_learning_history.jsp");		
						
			}		
		/*Added by Gangadhar Badagi for TENJINCG-320 ends*/		
	
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {		
		String param = request.getParameter("requesttype");
		if (param != null && param.equalsIgnoreCase("learning_api")) {
			String appId = request.getParameter("lstAppId");
			//int app = Integer.parseInt(appId);
			Map<String, Object> map = new HashMap<String, Object>();
			ArrayList<Aut> autList = new ArrayList<Aut>();

			try {
			/*Modified by Roshni TENJINCG-1168 starts */
				/*ApiHelper helper = new ApiHelper();*/
				ApiHandler handler=new ApiHandler();
				autList = /*helper.hydrateAllApp();*/ new AutHelper().hydrateAllAut();
				/*ArrayList<Api> api = helper.hydrateAllApi(appId);*/
				ArrayList<Api> api=handler.hydrateAllApi(appId);
				/*Modified by Roshni TENJINCG-1168 ends */
				if (api != null) {
					logger.info("Fetched {} APIs successfully", api.size());

					map.put("APILIST", autList);
					map.put("STATUS", "SUCCESS");
					map.put("MESSAGE", "");
					map.put("API", api);
					request.getSession().setAttribute("SCR_MAP", map);
					response.sendRedirect("apilist.jsp");

				}

			} catch (Exception e) {
				logger.error("ERROR occurred while fetching APIs");
				logger.error(e.getMessage(), e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", e.getMessage());
				request.getSession().setAttribute("SCR_MAP", map);
				response.sendRedirect("error.jsp");
			}
		}else if(param!=null && param.equalsIgnoreCase("New_api")){
			logger.debug("Creating New API Details");	
			String dUrl = "";
            Api api = new Api();
            /*Added by Pushpalatha for TENJINCG-568 starts*/
            Group grp = new Group();
            /*Added by Pushpalatha for TENJINCG-568 ends*/
            int appId = 0;
		    Map<String, Object> r = new HashMap<String, Object>();
		    ArrayList<Aut> autList = new ArrayList<Aut>();
		  /* <!-- Added  by padmavathi for API story review  fixes  on 01-08-2017 starts -->*/
			List<String> adapters = null;
			 try {
				 /*Added by Pushpalatha for TENJINCG-568 ends*/
		    	  adapters = ApiApplicationFactory.getAvailableAPIAdapters();
		    	   /* <!-- Added  by padmavathi for API story review  fixes  on 01-08-2017 ends -->*/
		          api.setApplicationId(Integer.parseInt(request.getParameter("lstAppId")));
		          api.setName(request.getParameter("txtApiName"));
		          api.setCode(request.getParameter("txtApiCode"));
		          api.setType(request.getParameter("txtApiType"));
		          api.setEncryptionType(request.getParameter("encryptionType"));
		          
		          api.setEncryptionKey(request.getParameter("encryptionKey"));
		          /*Added by Pushpalatha for TENJINCG-568 starts*/
		         if(request.getParameter("txtApiGroup").equals("-1")){
		        	 api.setGroup("Ungrouped");
		         }else{
		          api.setGroup(request.getParameter("txtApiGroup"));
		         }
		         /*Added by Pushpalatha for TENJINCG-568 ends*/
		          /*api.setUrl(request.getParameter("txtApiUrl"));*/
		          String apiUrl = request.getParameter("txtApiUrl");
		          Aut aut = new AutHelper().hydrateAut(Integer.parseInt(request.getParameter("lstAppId")));
		           /* <!-- Added  by padmavathi for  API story review  fixes on 01-08-2017 starts -->*/
		          api.setApplicationName(aut.getName());
		           /* <!-- Added  by padmavathi for  API story review  fixes on 01-08-2017 ends -->*/
		          if(aut != null) {
		        	  api.setUrl(apiUrl);
		        	  api.setUrlValid(ApiUtilities.isUrlValid(api.getUrl()));
		        	   /* <!-- Added  by padmavathi for API story review  fixes  on 01-08-2017 ends -->*/
		          }
		          
		          /*Added by Pushpalatha for TENJINCG-568 starts*/
	 				grp.setAut(Integer.parseInt(request.getParameter("lstAppId")));
	 				grp.setAppName(aut.getName());
	 				 if(request.getParameter("txtApiGroup").equals("-1")){
	 					 grp.setGroupName("Ungrouped");
	 				 }else{
	 					 grp.setGroupName(request.getParameter("txtApiGroup"));
	 				 }
	 				 /*Added by Pushpalatha for TENJINCG-568 ends*/
		    	}
			catch (Exception e) {
				System.out.println("could not get val");
			 }
		 
		    if (api != null) {
				ApiHelper apiHelper = null;
				logger.debug("Initiating ApiHelper");
				apiHelper = new ApiHelper();
				logger.debug("Done");
				if (apiHelper != null) {
					// Check for duplicates
					Api tempApi = null;
				 try {	
					 	autList = new AutHelper().hydrateAllAut();
					 	tempApi = apiHelper.hydrateApi(appId,api);
					 	if(tempApi != null){
					 		/*changed by manish for bug T25IT-149 on 16-Aug-2017 starts*/
					 		/*try{*/
					 		r.put("STATUS", "ERROR");
					 				/*Changed by Ashiki for V2.8-27 starts*/
					 				r.put("MESSAGE", "API already exists");
					 				/*Changed by Ashiki for V2.8-27 ends*/
						 			r.put("AUTS", autList);
					 				r.put("ADAPTERS", adapters);
						 			dUrl = "apidetails.jsp";
						 			request.getSession().setAttribute("API_MAP", r);
					 			
						 	/*changed by manish for bug T25IT-149 on 16-Aug-2017 ends*/
						 			
						 			/*Added by Padmavathi for TENJINCG-601 starts*/
					 	}else if(new ModuleHelper().hydrateModule(api.getApplicationId(), api.getCode())!=null){
					 		r.put("STATUS", "ERROR");
				 			r.put("MESSAGE", "The API Code "+api.getCode() +" already being used by a Function under Application "+ api.getApplicationName()+". Please use a different API Code.");
				 			r.put("AUTS", autList);
			 				r.put("ADAPTERS", adapters);
				 			dUrl = "apidetails.jsp";
				 			request.getSession().setAttribute("API_MAP", r);
					 	}
					 		/*Added by Padmavathi for TENJINCG-601 ends*/

					 	else{
					 		logger.debug("No duplicates. Persisting User");
				
					 		try{
					 				apiHelper.persistApi(api);
					 				 /*Added by Pushpalatha for TENJINCG-568 starts*/
					 				grp = new ModulesHelper().persistGroup(grp);
					 				 /*Added by Pushpalatha for TENJINCG-568 ends*/
					 				r.put("STATUS", "SUCCESS");
					 				/*Changed by Ashiki for V2.8-27 starts*/
					 				r.put("MESSAGE", "API created successfully");
					 				/*Changed by Ashiki for V2.8-27 ends*/
					 				r.put("API",api);
					 				r.put("API_LIST", autList);
					 				r.put("ADAPTERS", adapters);
					 				dUrl = "editapidetails.jsp";
					 			}
					 		catch (Exception e) {
							r.put("API", api);
							}
				        }
			      }
			    catch(Exception e){
				  logger.error("An unexpected error occurred while creating New Api",e);
				  r.put("STATUS", "ERROR");
				  r.put("MESSAGE", "An internal error occurred. Please try again");
				 dUrl = "apidetails.jsp";
			    }
				
		      }
		    }
		   
		 
		      request.getSession().setAttribute("SCR_MAP", r);
			  response.sendRedirect(dUrl);
		 /* <!-- Added  by padmavathi for  API story review  fixes on 01-08-2017 ends -->*/
		    
	     } else if(param!=null && param.equalsIgnoreCase("Edit_Api")){

			logger.debug("Updating API details");

			Map<String, Object> retData = new HashMap<String, Object>();
			List<String> adapters = null;
	     	try { 
	     		adapters = ApiApplicationFactory.getAvailableAPIAdapters();
				Api api = new Api();
				api.setApplicationId(Integer.parseInt(request.getParameter("lstAppId")));
				api.setName(request.getParameter("txtApiName"));
				api.setCode(request.getParameter("txtApiCode"));
				api.setUrl(request.getParameter("txtApiUrl"));
				/* Added by Pushpalatha for TENJINCG-568 starts*/
				api.setGroup(request.getParameter("txtApiGroup"));
				/* Added by Pushpalatha for TENJINCG-568 ends*/
				api.setEncryptionType(request.getParameter("encryptionType"));
				 api.setEncryptionKey(request.getParameter("encryptionKey"));
				
				ApiHelper apiHelper = new ApiHelper();
				apiHelper.updateApi(api);
				Api updatedApi = apiHelper.hydrateApi(api.getApplicationId(), api.getCode());
				retData.put("STATUS", "success");
				retData.put("MESSAGE","API updated Successfully");
                retData.put("API", updatedApi);
                retData.put("ADAPTERS", adapters);

		    	} catch (Exception e) {
				retData.put("STATUS", "ERROR");
				retData.put("MESSAGE", "Could not update due to an internal error. Please contact Tenjin Support.");

			} 
			//response.getWriter().write(retData);

			request.getSession().setAttribute("SCR_MAP", retData);
			RequestDispatcher rd=request.getRequestDispatcher("editapidetails.jsp");  
			rd.forward(request, response);


		}

		/*Commented by Leelaprsad for multiple responses story TENJINCG-404,405,406,407,408 starts*/
		else if(param!=null && param.equalsIgnoreCase("new_rest_operation_form")){
			String appid = request.getParameter("appid");
			String code = request.getParameter("code");
			String requestDesc = request.getParameter("optReqDesc");
			String optReqType = request.getParameter("optReqType");
			String optResType = request.getParameter("optResType");
			
		/*	Added By Paneendra  for TENJINCG-1239 starts*/
			String optHeadType = request.getParameter("optHeadType");
			String headerDesc = request.getParameter("optHeadDesc");
			
		/*	Added By Paneendra for TENJINCG-1239 ends*/	
			String optName = request.getParameter("optName");
			String authType = request.getParameter("authType");
			String optMethod = request.getParameter("optMethod");
			
			
			
			
			List<Parameter> resourceParameters = new ArrayList<Parameter>();
			String[] names = request.getParameterValues("paramName");
			String[] types = request.getParameterValues("paramType");
			String[] styles = request.getParameterValues("paramStyle");
			
			Parameter p = null;
			if(names != null){
				for(int i=0; i < names.length; i++){
					 if(!names[i].equals("")){
						 p = new Parameter();
						 p.setName(names[i]);
						 p.setType(types[i]);
						 p.setStyle(styles[i]);
						 resourceParameters.add(p);
					}
				}
			}
			
			
			
			/*	Added By Paneendra for TENJINCG-1239 starts*/
			
			//Header Representation
			Representation headerRepresentation = new Representation();
			headerRepresentation.setMediaType(optHeadType);
			headerRepresentation.setRepresentationDescription(headerDesc);
			headerRepresentation.setResponseCode(-1);
			
			ArrayList<Representation> headerRepresentaions = new ArrayList<Representation>();
			headerRepresentaions.add(headerRepresentation);
			
			
			/*	Added By Paneendra for TENJINCG-1239 ends*/
			
			
			
			//Request Representation
			Representation requestRepresentation = new Representation();
			requestRepresentation.setMediaType(optReqType);
			requestRepresentation.setRepresentationDescription(requestDesc);
			ArrayList<Representation> requestRepresentaions = new ArrayList<Representation>();
			requestRepresentaions.add(requestRepresentation);
			
			
	
			//Response Representation
			ArrayList<Representation> responseRepresentaions = new ArrayList<Representation>();
			
			
             
              //opRespDesc
            String[] responseJsonString = request.getParameterValues("opRespDesc");
            String[] responseCode = request.getParameterValues("opRespCode");
			Representation resp=null;
			if(responseCode != null){
				for(int i=0; i < responseCode.length; i++){
					 if(!responseCode[i].equals("")){
						 resp = new Representation();
						
						 new String(responseJsonString[i]);
						 
						resp.setMediaType(optResType);
						resp.setResponseCode(Integer.parseInt(responseCode[i]));
						resp.setRepresentationDescription(responseJsonString[i]);
						
						responseRepresentaions.add(resp);
						
					}
				}
			}
			
			if(responseRepresentaions.size()<1){
				resp= new Representation();
				resp.setMediaType(optResType);
				responseRepresentaions.add(resp);
			}
			
			
			
			String status = "";
			String message = "";

			JSONObject json = new JSONObject();
			Map<String, Object> map = new HashMap<String, Object>();
			
			
			
			//Fix for T25IT-89
			@SuppressWarnings("unchecked")
			Map<String, Object> oMap = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
			request.getSession().setAttribute("SCR_MAP", map);
			for (Representation reqRepresentation : requestRepresentaions) {
				if(Utilities.trim(reqRepresentation.getRepresentationDescription()).length() > 0) {
					if(reqRepresentation != null && Utilities.trim(reqRepresentation.getMediaType()).equalsIgnoreCase("application/xml")) {
						if(!ApiUtilities.isValidXML(reqRepresentation.getRepresentationDescription())) {
							status = "error";
							message = "The request you have entered is not a valid XML.";
							break;
						}
					}else if(reqRepresentation != null && Utilities.trim(reqRepresentation.getMediaType()).equalsIgnoreCase("application/json")) {
						if(!ApiUtilities.isValidJSON(reqRepresentation.getRepresentationDescription())) {
							status = "error";
							message = "The request you have entered is not a valid JSON String.";
							break;
						}
					}
				}
			}
			
			
			
			for (Representation responseRepresentation : responseRepresentaions) {
				if(Utilities.trim(responseRepresentation.getRepresentationDescription()).length() > 0) {
					if(responseRepresentation != null && Utilities.trim(responseRepresentation.getMediaType()).equalsIgnoreCase("application/xml")) {
						if(!ApiUtilities.isValidXML(responseRepresentation.getRepresentationDescription())) {
							status = "error";
							message = "The response you have entered is not a valid XML.";
							break;
						}
					}else if(responseRepresentation != null && Utilities.trim(responseRepresentation.getMediaType()).equalsIgnoreCase("application/json")) {
						if(!ApiUtilities.isValidJSON(responseRepresentation.getRepresentationDescription())) {
							status = "error";
							message = "The response you have entered is not a valid JSON String.";
							break;
						}
					}
				}
			}
			
			
			/*	Added By Paneendra for TENJINCG-1239 starts*/
			
			for (Representation headRepresentation : headerRepresentaions) {
				if(Utilities.trim(headRepresentation.getRepresentationDescription()).length() > 0) {
					if(headRepresentation != null && Utilities.trim(headRepresentation.getMediaType()).equalsIgnoreCase("application/xml")) {
						if(!ApiUtilities.isValidXML(headRepresentation.getRepresentationDescription())) {
							status = "error";
							message = "The request you have entered is not a valid XML.";
							break;
						}
					}else if(headRepresentation != null && Utilities.trim(headRepresentation.getMediaType()).equalsIgnoreCase("application/json")) {
						if(!ApiUtilities.isValidJSON(headRepresentation.getRepresentationDescription())) {
							status = "error";
							message = "The request you have entered is not a valid JSON String.";
							break;
						}
					}
				}
			}
			
			/*	Added By Paneendra for TENJINCG-1239 ends*/
			
			if(Utilities.trim(status).equalsIgnoreCase("error")) {
				if(oMap != null) {
					map.put("API_OPERATION", (ApiOperation) oMap.get("API_OPERATION"));
				}
				
				map.put("STATUS", status);
				map.put("MESSAGE", message);
				response.sendRedirect("newRestOperation.jsp?appid="+ appid+"&code="+code+"&optName="+optName);
				return;
			}
			//Fix for T25IT-89 ends
			/*Added by Padmavathi for T251IT-180 starts*/
			String url="";
			/*Added by Padmavathi for T251IT-180 ends*/
			try {
				
				try {
					
					ApiOperation operation = new ApiHelper().hydrateApiOperation(Integer.parseInt(appid), code, optName);
					
					if(operation == null){
						operation = new ApiOperation();
						operation.setName(optName);
						operation.setMethod(optMethod);
						/*Added by Preeti for TJN262R2-68 starts*/
						//operation.setAuthenticationType("Basic");
						/*Added by Preeti for TJN262R2-68 ends*/
						
						operation.setAuthenticationType(authType);	
						operation.setResourceParameters(resourceParameters);
						operation.setListResponseRepresentation(responseRepresentaions);
						operation.setListRequestRepresentation(requestRepresentaions);
						
						/*	Added By Paneendra for TENJINCG-1239 starts */
						operation.setListHeaderRepresentation(headerRepresentaions);
						/*	Added By Paneendra   for TENJINCG-1239 ends */
						//operation.setResourceParameters(resourceParameters);
						new ApiHelper().persistApiOperationInDetail(operation, code, Integer.parseInt(appid));
						message = "Added successfully.";
						json.put("status", "success");
						status = "success";
						/*Added by Padmavathi for T251IT-180 starts*/
						url="viewRestOperation.jsp";
						/*Added by Padmavathi for T251IT-180 ends*/
						map.put("API_OPERATION", operation);
						
					}
				
					else{
						message = "Record already exist.";
						json.put("status", "error");
						status = "error";
						/*optName = null;*/
					 /*Added by Padmavathi for T251IT-180 starts*/
						map.put("API_OPERATION", operation);
						map.put("API_OPERATION_PARAMLIST",resourceParameters);
						url="newRestOperation.jsp";
						/*Added by Padmavathi for T251IT-180 ends*/
						
					}
					
				} catch (NumberFormatException e) {
					
					json.put("status", "error");
					logger.error("Invalid Application ID [{}}", appid);
					json.put("message", "Invalid Application ID");
					status = "error";
					message = "Invalid Application ID";
				} catch (DatabaseException e) {
					
					json.put("status", "error");
					json.put("message", "An internal error occurred. Please contact Tenjin Support.");
					status = "error";
					message = "An internal error occurred. Please contact Tenjin Support.";
				}
				json.toString();
			} catch (JSONException e) {
				
				logger.error("JSONException occurred", e);

				status = "error";
				message = "An internal error occurred. Please contact Tenjin Support.";
			}
			map.put("STATUS",status);
			map.put("MESSAGE",message);
			/*Modified by Padmavathi for T251IT-180 starts*/
			/*response.sendRedirect("viewRestOperation.jsp?appid="+ appid+"&code="+code+"&optName="+optName);*/
			response.sendRedirect(url+"?appid="+ appid+"&code="+code+"&optName="+optName);
			/*Modified by Padmavathi for T251IT-180 ends*/
		}else if(param!=null && param.equalsIgnoreCase("edit_rest_operation_form")){
			String appid = request.getParameter("appid");
			String code = request.getParameter("code");
			String requestDesc = request.getParameter("optReqDesc");
			String optReqType = request.getParameter("optReqType");
			String optResType = request.getParameter("optResType");
			
			/*	Added By Paneendra for TENJINCG-1239 starts */
			String optHeadType = request.getParameter("optHeadType");
			String headerDesc = request.getParameter("optHeadDesc");
			
			/*	Added By Paneendra for TENJINCG-1239 ends */
			String optName = request.getParameter("optName");
			String optName1=request.getParameter("optName1");
			String optMethod = request.getParameter("optMethodVal");
			//TENJINCG-415
			String authType = request.getParameter("authType");
			if(Utilities.trim(authType).length() < 1) {
				authType = "NOAUTH";
			}
			//TENJINCG-415 ends
			
			
			List<Parameter> resourceParameters = new ArrayList<Parameter>();
			String[] names = request.getParameterValues("paramName");
			String[] types = request.getParameterValues("paramType");
			String[] styles = request.getParameterValues("paramStyle");
			Parameter p = null;
			if(names != null){
				for(int i=0; i < names.length; i++){
					 if(!names[i].equals("")){
						 p = new Parameter();
						 p.setName(names[i]);
						 p.setType(types[i]);
						 p.setStyle(styles[i]);
						 resourceParameters.add(p);
					}
				}
			}
			
			
		
			//Request Representation
			Representation reqRepresentation = new Representation();
			reqRepresentation.setMediaType(optReqType);
			reqRepresentation.setRepresentationDescription(requestDesc);
			ArrayList<Representation> requestRepresentaions = new ArrayList<Representation>();
			requestRepresentaions.add(reqRepresentation);
			
			/*	Added By Paneendra for TENJINCG-1239 starts */
			//Header Representation
			Representation headRepresentation = new Representation();
			headRepresentation.setMediaType(optHeadType);
			headRepresentation.setRepresentationDescription(headerDesc);
			headRepresentation.setResponseCode(-1);

			ArrayList<Representation> headerRepresentaions = new ArrayList<Representation>();
			headerRepresentaions.add(headRepresentation);
			
			/*	Added By Paneendra for TENJINCG-1239 ends */
			
			//Response Representation
			ArrayList<Representation> responseRepresentaions = new ArrayList<Representation>();
			
			 String[] responseJsonString = request.getParameterValues("opRespDesc");
	         String[] responseCode = request.getParameterValues("opRespCode");
			Representation resp=null;
			if(responseCode != null){
				for(int i=0; i < responseCode.length; i++){
					 if(!responseCode[i].equals("")){
						 resp = new Representation();
						 new String(responseJsonString[i]);
						 
						resp.setMediaType(optResType);
						resp.setResponseCode(Integer.parseInt(responseCode[i]));
						resp.setRepresentationDescription(responseJsonString[i]);
						
						responseRepresentaions.add(resp);
						
					}
				}
			}
			
			if(responseRepresentaions.size()<1){
				resp= new Representation();
				resp.setMediaType(optResType);
				resp.setRepresentationDescription("");
				responseRepresentaions.add(resp);
			}
			
			String status = "";
			String message = "";

			JSONObject json = new JSONObject();
			Map<String, Object> map = new HashMap<String, Object>();
			
			/*request.getSession().setAttribute("SCR_MAP", map);*/
			
			//Fix for T25IT-89
			@SuppressWarnings("unchecked")
			Map<String, Object> oMap = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
			request.getSession().setAttribute("SCR_MAP", map);
			for(Representation requestRepresentation:requestRepresentaions){
			if(Utilities.trim(requestRepresentation.getRepresentationDescription()).length() > 0) {
				if(requestRepresentation != null && Utilities.trim(requestRepresentation.getMediaType()).equalsIgnoreCase("application/xml")) {
					if(!ApiUtilities.isValidXML(requestRepresentation.getRepresentationDescription())) {
						status = "error";
						message = "The request you have entered is not a valid XML.";
						break;
					}
				}else if(requestRepresentation != null && Utilities.trim(requestRepresentation.getMediaType()).equalsIgnoreCase("application/json")) {
					if(!ApiUtilities.isValidJSON(requestRepresentation.getRepresentationDescription())) {
						status = "error";
						message = "The request you have entered is not a valid JSON String.";
						break;
					}
				}
			}
			}
			for(Representation responseRepresentation:responseRepresentaions){
			if(Utilities.trim(responseRepresentation.getRepresentationDescription()).length() > 0) {
				if(responseRepresentation != null && Utilities.trim(responseRepresentation.getMediaType()).equalsIgnoreCase("application/xml")) {
					if(!ApiUtilities.isValidXML(responseRepresentation.getRepresentationDescription())) {
						status = "error";
						message = "The response you have entered is not a valid XML.";
						break;
					}
				}else if(responseRepresentation != null && Utilities.trim(responseRepresentation.getMediaType()).equalsIgnoreCase("application/json")) {
					if(!ApiUtilities.isValidJSON(responseRepresentation.getRepresentationDescription())) {
						status = "error";
						message = "The response you have entered is not a valid JSON String.";
						break;
					}
				}
			}
		}
		
			
			/*	Added By Paneendra for TENJINCG-1239 starts */
			
			for(Representation headerRepresentation:headerRepresentaions){
				if(Utilities.trim(headerRepresentation.getRepresentationDescription()).length() > 0) {
					if(headerRepresentation != null && Utilities.trim(headerRepresentation.getMediaType()).equalsIgnoreCase("application/xml")) {
						if(!ApiUtilities.isValidXML(headerRepresentation.getRepresentationDescription())) {
							status = "error";
							message = "The response you have entered is not a valid XML.";
							break;
						}
					}else if(headerRepresentation != null && Utilities.trim(headerRepresentation.getMediaType()).equalsIgnoreCase("application/json")) {
						if(!ApiUtilities.isValidJSON(headerRepresentation.getRepresentationDescription())) {
							status = "error";
							message = "The response you have entered is not a valid JSON String.";
							break;
						}
					}
				}
			}
			
			/*	Added By Paneendra for TENJINCG-1239 ends */
			
			
			
			
			
			
			
			
			if(Utilities.trim(status).equalsIgnoreCase("error")) {
				if(oMap != null) {
					map.put("API_OPERATION", (ApiOperation) oMap.get("API_OPERATION"));
				}
				
				map.put("STATUS", status);
				map.put("MESSAGE", message);
				response.sendRedirect("viewRestOperation.jsp?appid="+ appid+"&code="+code+"&optName="+optName);
				return;
			}
			//Fix for T25IT-89 ends
			try {
				try {
					ApiOperation operation = new ApiHelper().hydrateApiOperation(Integer.parseInt(appid), code, optName);
					
					if(operation == null || operation.getName().equals(optName1)){
						logger.info("The API Opearation [{}] will be cleared and updated with new data", code );
						new ApiHelper().clearApiOperationInDetail(Integer.parseInt(appid), code, optName1);
						
						operation = new ApiOperation();
						operation.setName(optName);
						operation.setMethod(optMethod);
						
						//TENJINCG-415
						operation.setAuthenticationType(authType);
						//TENJINCG-415 ends
						operation.setResourceParameters(resourceParameters);
						operation.setListRequestRepresentation(requestRepresentaions);
						operation.setListResponseRepresentation(responseRepresentaions);
						/*	Added By Paneendra for TENJINCG-1239 starts */
						operation.setListHeaderRepresentation(headerRepresentaions);
						/*	Added By Paneendra for TENJINCG-1239 ends */
						//operation.setResourceParameters(resourceParameters);
						new ApiHelper().persistApiOperationInDetail(operation, code, Integer.parseInt(appid));
						message = "Updated successfully.";
						
						json.put("status", "success");
						status = "success";
						map.put("API_OPERATION", operation);
						
					}
				
					else{
						ApiOperation operation1 = new ApiHelper().hydrateApiOperation(Integer.parseInt(appid), code, optName1);
						message = "Record already exist.";
						json.put("status", "error");
						status = "error";
						
						map.put("API_OPERATION", operation1);
					
					}
					
				} catch (NumberFormatException e) {
					
					json.put("status", "error");
					logger.error("Invalid Application ID [{}}", appid);
					json.put("message", "Invalid Application ID");
					status = "error";
					message = "Invalid Application ID";
				} catch (DatabaseException e) {
					
					json.put("status", "error");
					json.put("message", "An internal error occurred. Please contact Tenjin Support.");
					status = "error";
					message = "An internal error occurred. Please contact Tenjin Support.";
				}
				json.toString();
			} catch (JSONException e) {
				
				logger.error("JSONException occurred", e);

				status = "error";
				message = "An internal error occurred. Please contact Tenjin Support.";
			}
			map.put("STATUS",status);
			map.put("MESSAGE",message);
			
			response.sendRedirect("viewRestOperation.jsp?appid="+ appid+"&code="+code+"&optName="+optName1);
		}
		else if(param!=null && param.equalsIgnoreCase("edit_soap_operation_form")){

			String appid = request.getParameter("appid");
			String code = request.getParameter("code");
			String requestDesc = request.getParameter("optReqDesc");
			String responseDesc = request.getParameter("optResDesc");
			String optReqType = request.getParameter("optReqType");
			String optResType = request.getParameter("optResType");
			request.getParameter("optName");
			String optName1=request.getParameter("optName1");
			String optMethod = request.getParameter("optMethod");
			
			
		
			
		
			//Request Representation
			Representation reqRepresentation = new Representation();
			reqRepresentation.setMediaType(optReqType);
			reqRepresentation.setRepresentationDescription(requestDesc);
			ArrayList<Representation> reqRepresentaionsList = new ArrayList<Representation>();
			reqRepresentaionsList.add(reqRepresentation);
			
			//Request Representation
			Representation respRepresentation = new Representation();
			respRepresentation.setMediaType(optResType);
			respRepresentation.setRepresentationDescription(responseDesc);
			ArrayList<Representation> respRepresentaionsList = new ArrayList<Representation>();
			respRepresentaionsList.add(respRepresentation);
			
		
			String status = "";
			String message = "";

			JSONObject json = new JSONObject();
			Map<String, Object> map = new HashMap<String, Object>();
			
			
			
			//Fix for T25IT-89
			@SuppressWarnings("unchecked")
			Map<String, Object> oMap = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
			request.getSession().setAttribute("SCR_MAP", map);
			for(Representation requestRepresentation:reqRepresentaionsList){
			if(Utilities.trim(requestRepresentation.getRepresentationDescription()).length() > 0) {
				if(requestRepresentation != null && Utilities.trim(requestRepresentation.getMediaType()).equalsIgnoreCase("application/xml")) {
					if(!ApiUtilities.isValidXML(requestRepresentation.getRepresentationDescription())) {
						status = "error";
						message = "The request you have entered is not a valid XML.";
						break;
					}
				}else if(requestRepresentation != null && Utilities.trim(requestRepresentation.getMediaType()).equalsIgnoreCase("application/json")) {
					if(!ApiUtilities.isValidJSON(requestRepresentation.getRepresentationDescription())) {
						status = "error";
						message = "The request you have entered is not a valid JSON String.";
						break;
					}
				}
			}
			}
			for(Representation responseRepresentation:respRepresentaionsList){
			if(Utilities.trim(responseRepresentation.getRepresentationDescription()).length() > 0) {
				if(responseRepresentation != null && Utilities.trim(responseRepresentation.getMediaType()).equalsIgnoreCase("application/xml")) {
					if(!ApiUtilities.isValidXML(responseRepresentation.getRepresentationDescription())) {
						status = "error";
						message = "The response you have entered is not a valid XML.";
						break;
					}
				}else if(responseRepresentation != null && Utilities.trim(responseRepresentation.getMediaType()).equalsIgnoreCase("application/json")) {
					if(!ApiUtilities.isValidJSON(responseRepresentation.getRepresentationDescription())) {
						status = "error";
						message = "The response you have entered is not a valid JSON String.";
						break;
					}
				}
			}
		}
			if(Utilities.trim(status).equalsIgnoreCase("error")) {
				if(oMap != null) {
					map.put("API_OPERATION", (ApiOperation) oMap.get("API_OPERATION"));
				}
				
				map.put("STATUS", status);
				map.put("MESSAGE", message);
				response.sendRedirect("viewSoapOperation.jsp?appid="+ appid+"&code="+code+"&optName="+optName1);
				return;
			}
			//Fix for T25IT-89 ends
			try {
				try {
					ApiOperation operation = new ApiHelper().hydrateApiOperation(Integer.parseInt(appid), code, optName1);
					
					if(operation.getListRequestRepresentation().size()>0||operation.getListResponseRepresentation().size()>0){
						operation.setListRequestRepresentation(reqRepresentaionsList);
						operation.setListResponseRepresentation(respRepresentaionsList);
						new ApiHelper().updateOperationDescriptors(Integer.parseInt(appid),code,operation);
                        message = "Updated successfully.";
						
						json.put("status", "success");
						status = "success";
						map.put("API_OPERATION", operation);
					}else{
						operation = new ApiOperation();
						operation.setName(optName1);
						operation.setMethod(optMethod);
					
						operation.setListRequestRepresentation(reqRepresentaionsList);
						operation.setListResponseRepresentation(respRepresentaionsList);
						
						new ApiHelper().persistOperationRepresentations(Integer.parseInt(appid),code,operation);
						message = "Updated successfully.";
						json.put("status", "success");
						status = "success";
						map.put("API_OPERATION", operation);
					}
					
					
				} catch (NumberFormatException e) {
					
					json.put("status", "error");
					logger.error("Invalid Application ID [{}}", appid);
					json.put("message", "Invalid Application ID");
					status = "error";
					message = "Invalid Application ID";
				} catch (DatabaseException e) {
					
					json.put("status", "error");
					json.put("message", "An internal error occurred. Please contact Tenjin Support.");
					status = "error";
					message = "An internal error occurred. Please contact Tenjin Support.";
				}
				json.toString();
			} catch (JSONException e) {
				
				logger.error("JSONException occurred", e);

				status = "error";
				message = "An internal error occurred. Please contact Tenjin Support.";
			}
			map.put("STATUS",status);
			map.put("MESSAGE",message);
			
			response.sendRedirect("viewSoapOperation.jsp?appid="+ appid+"&code="+code+"&optName="+optName1);
		
		}
		
		
		
		/*Changes done by Pushpalatha for defect T25IT-77 ends*/
		else if(param.equalsIgnoreCase("update_aut_api_info")) {
			String apiBaseUrl = request.getParameter("apiBasePath");
			String autBaseUrl = request.getParameter("autBase");
			
			//TENJINCG-415
			String apiAuthType = request.getParameter("authType");
			//TENJINCG-415 ends
			
			String finalUrl = "";
			if(Utilities.trim(apiBaseUrl).startsWith("/")) {
				finalUrl = autBaseUrl + apiBaseUrl;
			}else{
				finalUrl = autBaseUrl + "/" + apiBaseUrl;
			}
			String aId = request.getParameter("app");
			@SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("AUT_API_INFO_MAP");
			try{
				AutHelper helper = new AutHelper();
				//TENJINCG-415
				/*helper.updateAutApiBaseURL(Integer.parseInt(aId), finalUrl);*/
				//Code Changes for OAuth 2.0 requirement - Sunitha - Starts
				Aut aut = helper.hydrateAut(Integer.parseInt(aId));
				if(!Utilities.trim(aut.getApiAuthenticationType()).isEmpty() && "oauth2".equalsIgnoreCase(aut.getApiAuthenticationType()) && !aut.getApiAuthenticationType().equalsIgnoreCase(apiAuthType)){
					aut.setAccessToken(null);
					aut.setAccessTokenUrl(null);
					aut.setCallBackUrl(null);
					aut.setClientId(null);
					aut.setClientSecret(null);
					aut.setAuthUrl(null);
					aut.setAddAuthDataTo(null);
					aut.setAccessTokenUrl(null);
					new AutsHelper().persistOAuthRecord(aut);
				}
				//Code Changes for OAuth 2.0 requirement - Sunitha - Ends
				helper.updateAutApiProperties(Integer.parseInt(aId), finalUrl, apiAuthType);
				/* Changed by leelaprasad for TJN210-100 starts */
				new ApiHelper().updateApiAutOperationsAuthTypes(Integer.parseInt(aId),apiAuthType);
				/* Changed by leelaprasad for TJN210-100 ends */
				//TENJINCG-415 ends
				map.put("AUT", aut);
				map.put("AUT_BASE", ApiUtilities.getApplicationBaseUrl(aut.getURL()));
				//TENJINCG-415
				map.put("API_AUTH_TYPE", apiAuthType);
				//TENJINCG-415 ends
				aut.setApiBaseUrl(aut.getApiBaseUrl().replace(autBaseUrl, ""));
				map.put("STATUS", "SUCCESS");
				map.put("MESSAGE", "API Information updated successfully.");
			}catch(Exception e) {
				logger.error("ERROR updating API Information", e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "Could not update details due to an internal error. Please contact Tenjin Support.");
			}
			
			request.getSession().setAttribute("AUT_API_IFO_MAP", map);
			request.getRequestDispatcher("aut_api_information.jsp").forward(request, response);
		}
		
		/*Change done by Shruthi for TENJINCG-1206 starts*/
		else if(param.equalsIgnoreCase("deleteApiOperations"))
		{
			String[] selectedOperation = request.getParameterValues("operationname");
			String appId = request.getParameter("appid");
			String apiCode = request.getParameter("apicode");
			ApiHandler handler = new ApiHandler();
			/*Change done by Shruthi for TENJINCG-1216 ends*/
			String apiGroup = request.getParameter("apigroup");	
			String apiFilterType=request.getParameter("apitypefilter");
			ApiHelper apiHelper = null;
			Api api = null;
			Map<String, Object> map = new HashMap<String , Object>();
			try {
				 handler.deleteApiOperations(selectedOperation,appId,apiCode );
				 apiHelper = new ApiHelper();
				 api = apiHelper.hydrateApi(Integer.parseInt(appId), apiCode);
				 List<String> adapters = ApiApplicationFactory.getAvailableAPIAdapters();
				 map.put("STATUS", "SUCCESS");
				 map.put("MESSAGE", "The selected operation(s) deleted successfully.");
				 map.put("API", api);
				 map.put("ADAPTERS", adapters);
				 map.put("API_FILTER_TYPE", apiFilterType);
				 map.put("API_GROUP", apiGroup);
			 } 
		  catch (Exception e) {
			 map.put("STATUS", "ERROR");
			 map.put("MESSAGE",
					 "An Internal Error occurred. Please contact your System Administrator");
		 } finally {
			 request.getSession().setAttribute("SCR_MAP", map);
		 }
			 response.sendRedirect("editapidetails.jsp");
		}
			/*Change done by Shruthi for TENJINCG-1216 ends*/
		/*Change done by Shruthi for TENJINCG-1206 ends*/
		
		
		
	}
	
	private List<Api> getAppApisForApplication(String appId) throws DatabaseException {
		/*Changed by Gangadhar Badagi to get start learning time starts*/	
		/*Modified by Roshni TENJINCG-1168 starts */
		List<Api> listApis=new ApiHandler().hydrateAllApi(appId);
		/*Modified by Roshni TENJINCG-1168 ends */
		for (Api api : listApis) {		
			ArrayList<ApiLearnerResultBean> lrnr_runs=new ApiHelper().hydrateLearningHistory(api.getCode(), Integer.parseInt(appId));		
			api.setLearningHistory(lrnr_runs);		
		}		
		return listApis;		
		/*Changed by Gangadhar Badagi to get start learning time ends*/	
		
	}
	
	/*Modified By Preeti for TENJINCG-600 starts*/
	/*Added by Preeti for TENJINCG-366 starts*/
	private List<Api> getAppApisForApplication(String appId,String apiType, String groupName) throws DatabaseException {
		List<Api> listApis=new ApiHelper().hydrateAllApi(appId,apiType,groupName);	
		for (Api api : listApis) {		
			ArrayList<ApiLearnerResultBean> lrnr_runs=new ApiHelper().hydrateLearningHistory(api.getCode(), Integer.parseInt(appId));		
			api.setLearningHistory(lrnr_runs);		
		}			
		return listApis;				
	}
	/*Added by Preeti for TENJINCG-366 ends*/
	/*Modified By Preeti for TENJINCG-600 ends*/
}