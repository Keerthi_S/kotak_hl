/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ResultsServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
   12/03/2020           Khalid                  TENJINCG-1194
   13-03-2020			Sriram					TENJINCG-1196
 */

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.ResultsHandler;
import com.ycs.tenjin.handler.TestRunHandler;
import com.ycs.tenjin.run.TestRunProgress;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class RunAjaxServlet
 */
/*@WebServlet("/RunAjaxServlet")*/
public class RunAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(RunAjaxServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RunAjaxServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String task = Utilities.trim(request.getParameter("param"));

		if(task.equalsIgnoreCase("run_time_values")) {
			JsonObject json = new JsonObject();
			int runId=0;
			int testStepRecordId = 0;
			String tdUid = Utilities.trim(request.getParameter("tduid"));

			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
				testStepRecordId = Integer.parseInt(Utilities.trim(request.getParameter("tsrecid")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run ID [{}] or Test Step Record ID [{}]", Utilities.trim(request.getParameter("run")), Utilities.trim(request.getParameter("tsrecid")));
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}

			try {
				List<RuntimeFieldValue> values = new ResultsHandler().fetchRuntimeFieldValues(runId, testStepRecordId, tdUid);
				String valuesJson = new Gson().toJson(values);
				json.addProperty("runtimevalues", valuesJson);
				
			} catch (DatabaseException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (Exception e) {
				
				logger.error("Error ", e);
			}finally {
				response.getWriter().write(json.toString());
			}

			
		}else if(task.equalsIgnoreCase("validation_results")) {
			JsonObject json = new JsonObject();
			int runId=0;
			int testStepRecordId = 0;
			int iteration = 0;

			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
				testStepRecordId = Integer.parseInt(Utilities.trim(request.getParameter("tsrecid")));
				iteration = Integer.parseInt(Utilities.trim(request.getParameter("txn")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run ID [{}] or Test Step Record ID [{}] or Iteration [{}]", Utilities.trim(request.getParameter("run")), Utilities.trim(request.getParameter("tsrecid")),Utilities.trim(request.getParameter("txn")));
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}

			try {
				StepIterationResult result = new ResultsHandler().fetchValidationResults(runId, testStepRecordId, iteration);
				String validationResultJson = new Gson().toJson(result);
				json.addProperty("validationresult", validationResultJson);
				
			} catch (DatabaseException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (Exception e) {
				
				logger.error("Error ", e);
			}finally {
				response.getWriter().write(json.toString());
			}
		}
		//TENJINCG-1196 (Sriram)
		else if(task.equalsIgnoreCase("tc_summary")) {
			JsonObject jsonToReturn = new JsonObject();
			String errorFlag = "error";
			jsonToReturn.addProperty(errorFlag, false);
			try {
				int runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
				TestRunProgress progress = new TestRunHandler().getRunProgress(runId);
				jsonToReturn = new Gson().fromJson(new Gson().toJson(progress), JsonObject.class);
				
			} catch(NumberFormatException e) {
				jsonToReturn.addProperty(errorFlag, true);
				jsonToReturn.addProperty("message", "Invalid Run ID");
				logger.error("Error fetching test case summary. Invalid Run ID {}", request.getParameter("run"));
			} catch (Exception e) {
				logger.error("Error fetching test case summary. Invalid Run ID {}", request.getParameter("run"));
				jsonToReturn.addProperty(errorFlag, true);
				jsonToReturn.addProperty("message", e.getMessage());
			} finally {
				response.getWriter().write(jsonToReturn.toString());
			}
		}
		//TENJINCG-1196 (Sriram) ends
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
