
/***
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestDataUploadServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 15-06-2021			Ashiki					TENJINCG-1275
 * 09-11-2021			Ashiki					Tenj212-22
 * 10-11-2021			Ashiki					Tenj212-23
 */

package com.ycs.tenjin.servlet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.aws.FileUploadImpl;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiLearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ProjectHelperNew;
import com.ycs.tenjin.db.TestDataHelper;
import com.ycs.tenjin.handler.ApiHandler;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestDataPath;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListBucketsRequest;
import software.amazon.awssdk.services.s3.model.ListBucketsResponse;

/**
 * Servlet implementation class TestDataUploadServlet
 */
@WebServlet("/TestDataUploadServlet")
public class TestDataUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TestDataUploadServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TestDataUploadServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		@Override
		public boolean checkAccess(HttpServletRequest request) {
			if (new AdminAuthorizationRuleImpl().checkAccess(request)) {
				return true;
			}
			return false;
		}
	};

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		SessionUtils.loadScreenStateToRequest(request);
		User user = tjnSession.getUser();
		String userId = user.getId();
		/* commented by paneendra for Tenj212-13 starts */
		/*
		 * Project project=tjnSession.getProject(); int projectId=project.getId();
		 */
		/* commented by paneendra for Tenj212-13 ends */

		String task = Utilities.trim(request.getParameter("param"));

		if (task.equalsIgnoreCase("FETCH_ALL_APIS")) {
			Map<String, Object> map = new HashMap<>();
			String status = "";
			String message = "";
			String retData = "";
			String appId = request.getParameter("app");
			JSONObject json = new JSONObject();
			if (Utilities.trim(appId).length() > 0) {
				try {
					List<Api> apis = this.getAppApisForApplication(appId);
					if (apis != null && apis.size() > 0) {
						JSONArray jArray = new JSONArray();
						for (Api api : apis) {
							JSONObject js = new JSONObject();
							js.put("code", api.getCode());
							js.put("name", api.getName());
							js.put("api", api);
							jArray.put(js);
						}
						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("apis", jArray);
					} else {
						json.put("status", "FAILURE");
						json.put("message", "No API'S are available");
					}

					retData = json.toString();
					response.getWriter().write(retData);

				} catch (JSONException e) {
					logger.error("ERROR getting list of APIs", e);
					status = "ERROR";
					message = "Could not get APIs due to an unexpected error. Please contact Tenjin Support.";
				}

				catch (DatabaseException db) {
					logger.error("ERROR getting list of APIs", db);
					retData = "An internal error occurred. Please contact your System Administrator";
				}

			}

		} else if (task.equalsIgnoreCase("load_testdatalist")) {
			Project project = tjnSession.getProject();
			int projectId = project.getId();
			List<TestDataPath> testDataList = null;
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				/* Modified by Ashiki for Tenj212-22 starts */
				// Project prj = new ProjectHelperNew().hydrateProject(projectId);
				// if(prj.getType().equalsIgnoreCase("private"))
				/* Modified by Ashiki for Tenj212-22 ends */
				testDataList = new TestDataHelper().hydrateAllTestData(projectId, userId);
				/* Modified by Ashiki for Tenj212-22 starts */
				// else
				// testDataList=new TestDataHelper().hydrateAllTestData(projectId);
				/* Modified by Ashiki for Tenj212-22 ends */
				request.setAttribute("testDataList", testDataList);
				map.put("STATUS", "SUCCESS");
				map.put("MESSAGE", "");
				request.getRequestDispatcher("v2/uploadtestdata_list.jsp").forward(request, response);
			} catch (DatabaseException db) {
				logger.error("ERROR while fetching Test Data list", db);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "Could not load test datalist due to an internal error");
				request.getRequestDispatcher("error.jsp").forward(request, response);

			}
		} else if (task.equalsIgnoreCase("new")) {
			Project project = tjnSession.getProject();
			int projectId = project.getId();
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				logger.info("Hydrating project AUTs");
				ArrayList<Aut> allAuts = new ProjectHandler().hydratePrjectAuts((projectId));
				map.put("STATUS", "SUCCESS");
				map.put("MESSAGE", "");
				map.put("AUTS", allAuts);
			} catch (DatabaseException e) {
				logger.error("ERROR while fetching AUT list", e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "Could not load AUTs due to an internal error");
			} finally {
				request.getSession().setAttribute("EXTRACTOR_SCREEN_MAP", map);
				logger.info("Forwarding...");
				RequestDispatcher dispatcher = request.getRequestDispatcher("v2/upload_newfile.jsp");
				dispatcher.forward(request, response);
			}
		} else if (task.equalsIgnoreCase("DOWNLOAD")) {
			Project project = tjnSession.getProject();
			int projectId = project.getId();
			String appId = request.getParameter("app");
			String funcCode = request.getParameter("func");
			String fileName = request.getParameter("fname");
			String folderPath = request.getServletContext().getRealPath("/");
			Utilities.checkForDownloadsFolder(folderPath);
			folderPath = folderPath + File.separator + "Downloads";
			String path = "";
			String retData = "";
			JSONObject json = new JSONObject();
			try {
				String appName = new AutHelper().getAutName(Integer.parseInt(appId));
				FileUploadImpl fileupload = new FileUploadImpl();
				Project prj = new ProjectHelperNew().hydrateProject(projectId);
				String reporoot = prj.getRepoRoot();
				String repoType = prj.getRepoType();
				/* modified by shruthi for Tenj212-15 starts */
				if (repoType.equalsIgnoreCase("AWS S3")) {
					File file = new File(folderPath);
					if (!file.exists()) {
						file.mkdirs();
						file = new File(folderPath + File.separator + fileName);
						file.createNewFile();
					}
					file = new File(folderPath + File.separator + fileName);
					file.createNewFile();
					path = project.getName() + "/" + appName + "/" + funcCode + "/" + userId + "/" + fileName;
					fileupload.downloadTestDataFileFromS3(reporoot, path, file.getPath());
				}
				/* modified by shruthi for Tenj212-15 ends */
				else {
					/* modified by paneendra for Tenjn212-15 starts */
					// reporoot="file:"+File.separator+reporoot;
					reporoot = "file:" + reporoot;
					/* modified by paneendra for Tenjn212-15 ends */
					path = reporoot + File.separator + project.getName() + File.separator + appName + File.separator
							+ funcCode + File.separator + userId + File.separator + fileName;
					fileupload.downloadTestDataFile(path, folderPath, fileName);
				}

				json.put("status", "SUCCESS");
				json.put("message", "");
				json.put("path", "Downloads" + File.separator + fileName);
			} catch (Exception e) {
				/* Added by Ashiki for Tenj212-23 starts */
				try {
					json.put("status", "ERROR");
					json.put("message", e.getMessage());
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				/* Added by Ashiki for Tenj212-23 ends */
			} finally {
				retData = json.toString();
			}
			response.getWriter().write(retData);
		} else if (task.equalsIgnoreCase("fetchAWSBucket")) {
			String retData = "";
			String AccessKey = request.getParameter("AccessKey");
			String AccessSecKey = request.getParameter("AccessSecKey");
			List<String> awsBuckets = new ArrayList<String>();
			try {
				JSONObject json = new JSONObject();
				
				S3Client s3ClientBuild = null;
				FileUploadImpl fileUploadImpl = new FileUploadImpl();
				try {
					/* modified by paneendra for redundant code in building s3 client starts */
					/*
					 * Region region = Region.AP_SOUTH_1;
					 * 
					 * System.setProperty("aws.accessKeyId",
					 * TenjinConfiguration.getProperty("ACCESS_KEY_ID"));
					 * System.setProperty("aws.secretAccessKey",
					 * TenjinConfiguration.getProperty("ACCESS_SEC_KEY"));
					 * 
					 * S3Client s3 = S3Client.builder() .region(region)
					 * .credentialsProvider(SystemPropertyCredentialsProvider.create()) .build();
					 */
					if(AccessKey==null) {
						
						
						s3ClientBuild = fileUploadImpl.buildS3Client();
					}else {
						
					
					s3ClientBuild = fileUploadImpl.buildS3Client(AccessKey,AccessSecKey);
					}
					//s3ClientBuild = fileUploadImpl.buildS3Client();
					/* modified by paneendra for redundant code in building s3 client ends */
                  
                   
					ListBucketsRequest listBucketsRequest = ListBucketsRequest.builder().build();
					ListBucketsResponse listBucketsResponse = s3ClientBuild.listBuckets(listBucketsRequest);
					ProjectHandler projectHandler = new ProjectHandler();
					projectHandler.persistProject(AccessKey,AccessSecKey);
					listBucketsResponse.buckets().stream().forEach(x -> awsBuckets.add(x.name()));
					
					json.put("status", "SUCCESS");
					json.put("awsBuckets", awsBuckets);
					json.put("message", "");

				
				}catch (Exception e) {
					logger.error("An exception occured while fetching the s3 buckets", e);
					json.put("status", "ERROR");
					json.put("message", "Invalid AWS credentials,Please contact your system administrator");
				} finally {
					retData = json.toString();
				}
			} catch (Exception e) {
				retData = "An Internal Error Occurred. Please contact your system administrator";
			}

			response.getWriter().write(retData);
		}
		else if (task.equalsIgnoreCase("import")) {
			Project project = tjnSession.getProject();
			int projectId = project.getId();
			try {
				List<User> projectUsers = new ProjectHandler().getProjectUsers(projectId);
				List<String> projectUserIds = projectUsers.stream().map(prjUser -> prjUser.getId())
						.collect(Collectors.toList());
				projectUserIds.remove(userId);
				ArrayList<Aut> projectAuts = new ProjectHandler().hydratePrjectAuts(projectId);
				request.setAttribute("ProjectUsers", projectUserIds);
				request.setAttribute("ProjectAuts", projectAuts);
			} catch (DatabaseException e) {
				logger.error("ERROR while fetching Project Users", e);
			} finally {
				logger.info("Forwarding...");
				RequestDispatcher dispatcher = request.getRequestDispatcher("v2/import_testdata.jsp");
				dispatcher.forward(request, response);
			}
		} else if (task.equalsIgnoreCase("show_testdata")) {
			String selectedUserId = request.getParameter("userId");
			String selectedAppId = request.getParameter("appId");
			String selectedAppName = request.getParameter("appName");
			Project project = tjnSession.getProject();
			/* String userName = tjnSession.getUser().getId(); */
			int projectId = project.getId();
			try {
				List<TestDataPath> selectedUserTestData = new TestDataHelper().hydrateAllTestData(projectId,
						selectedUserId, selectedAppId);
				/*
				 * List<TestDataPath> userTestData = new
				 * TestDataHelper().hydrateAllTestData(projectId, userName, selectedAppId);
				 * selectedUserTestData.removeAll(userTestData);
				 */
				request.setAttribute("testDataList", selectedUserTestData);
				List<User> projectUsers = new ProjectHandler().getProjectUsers(projectId);
				List<String> projectUserIds = projectUsers.stream().map(prjUser -> prjUser.getId())
						.collect(Collectors.toList());
				projectUserIds.remove(userId);
				ArrayList<Aut> projectAuts = new ProjectHandler().hydratePrjectAuts(projectId);

				request.setAttribute("ProjectUsers", projectUserIds);
				request.setAttribute("ProjectAuts", projectAuts);
				request.setAttribute("SelectedUser", selectedUserId);
				request.setAttribute("SelectedApp", selectedAppName);
			} catch (DatabaseException e) {
				logger.error("ERROR while fetching Project Users", e);
				e.printStackTrace();
			} finally {
				logger.info("Forwarding...");
				RequestDispatcher dispatcher = request.getRequestDispatcher("v2/import_testdata.jsp");
				dispatcher.forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		SessionUtils.loadScreenStateToRequest(request);
		User user = tjnSession.getUser();
		String userId = user.getId();
		Project project = tjnSession.getProject();
		int projectId = project.getId();

		String importParameter = request.getParameter("importing");
		if (!StringUtils.isEmpty(importParameter) && importParameter.equalsIgnoreCase("true")) {

			String[] funValues = request.getParameterValues("selectedFunctions");
			funValues = StringUtils.stripAll(funValues);
			String appName = request.getParameter("selectedAppName");
			String appId = request.getParameter("selectedAppId");
			String uploadedBy = request.getParameter("selectedUserName");
			String importingUser = userId;
			Project hydratedProject = null;
			String repoType = null;
			String repoRoot = null;
			Map<String, Object> map = new HashMap<>();
			BufferedInputStream inputstream = null;
			try {
				hydratedProject = new ProjectHandler().hydrateProject(projectId);
			} catch (DatabaseException e) {
				logger.error("Error"+e);
			}
			if (hydratedProject != null) {
				repoRoot = hydratedProject.getRepoRoot();
				repoType = hydratedProject.getRepoType();
			}

			try {
				for (String funCode : funValues) {
					TestDataPath testdatapath = new TestDataPath();
					testdatapath.setAppId(Integer.parseInt(appId));
					testdatapath.setAppName(appName);
					testdatapath.setFunction(funCode);
					testdatapath.setUser(importingUser);
					testdatapath.setProjectId(projectId);
					testdatapath.setProjectName(project.getName());
					String importingFileName = funCode + "_TTD.xlsx";
					testdatapath.setFileName(importingFileName);

					String srcPath = repoRoot + File.separator + project.getName() + File.separator + appName
							+ File.separator + funCode + File.separator + uploadedBy + File.separator
							+ importingFileName;
					File srcFile = new File(srcPath);
					String tempawsuploaderPath = null;

					if (srcFile.exists()) {
						srcPath = "file:" + srcPath;
						inputstream = new BufferedInputStream(new URL(srcPath).openStream());
						new FileUploadImpl().uploadTestDataFile(repoRoot, testdatapath, importingFileName, inputstream,
								repoType, tempawsuploaderPath);
					} else {
						logger.error("Source File doesn't exists");
					}
				}
			} catch (Exception e) {
				logger.error("Error"+e);
			}
			try {
				List<TestDataPath> testDataList = new TestDataHelper().hydrateAllTestData(projectId, userId);
				request.setAttribute("testDataList", testDataList);
				map.put("STATUS", "SUCCESS");
				map.put("MESSAGE", "");
				SessionUtils.setScreenState("success", "Test Data Copied Successfully", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("v2/uploadtestdata_list.jsp").forward(request, response);

			} catch (DatabaseException db) {
				logger.error("ERROR while fetching Test Data list", db);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "Could not load test datalist due to an internal error");
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		} else {

			boolean isMultiPart = false;
			Map<String, String> formValues = new HashMap<String, String>();
			Map<String, String> fileMap = new HashMap<String, String>();
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				isMultiPart = ServletFileUpload.isMultipartContent(request);
			} catch (Exception e) {
				logger.error("Error"+e);
			}

			List<TestDataPath> testDataList = null;
			if (isMultiPart) {
				DiskFileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);

				String appId = "";
				String funcCode = "";
				String appName = "";
				String apicode = "";
				String valtype = "";
				/* Added by Ashiki for TENJINCG-1275 starts */
				String msgSubType = "";
				/* Added by Ashiki for TENJINCG-1275 ends */
				try {
					List fileItems = upload.parseRequest(request);
					Iterator fileItemsIter = fileItems.iterator();

					String fullFilePath = "";
					String fileName = "";
					InputStream inpstream = null;
					while (fileItemsIter.hasNext()) {
						FileItem fi = (FileItem) fileItemsIter.next();
						if (fi.isFormField()) {
							formValues.put(fi.getFieldName(), fi.getString());
						} else {
							fileName = fi.getName();
							String fieldName = fi.getFieldName();
							inpstream = fi.getInputStream();
							fileMap.put(fieldName, fullFilePath);
						}
						appId = formValues.get("lstApplication");
						funcCode = formValues.get("lstModules");
						/* Added by Ashiki for TENJINCG-1275 starts */
						msgSubType = formValues.get("msgFormat");
						/* Added by Ashiki for TENJINCG-1275 ends */
						apicode = formValues.get("Apitype");
						valtype = formValues.get("validationType");
					}

					FileUploadImpl fileupload = new FileUploadImpl();

					Project prj = null;
					try {
						prj = new ProjectHelperNew().hydrateProject(projectId);
					} catch (DatabaseException e) {
						logger.error("Error"+e);
					}
					String reporoot = prj.getRepoRoot();
					String repoType = prj.getRepoType();
					TestDataPath testdatapath = new TestDataPath();
					try {
						appName = new AutHelper().getAutName(Integer.parseInt(appId));
					} catch (DatabaseException e) {
						logger.error("Error"+e);
					}
					testdatapath.setAppId(Integer.parseInt(appId));
					testdatapath.setAppName(appName);
					testdatapath.setFunction(funcCode);
					testdatapath.setUser(userId);
					testdatapath.setProjectId(projectId);
					testdatapath.setProjectName(prj.getName());
					if (valtype.equalsIgnoreCase("Api")) {
						testdatapath.setFunction(apicode);
					} /* Added by Ashiki for TENJINCG-1275 starts */
					else if (valtype.equalsIgnoreCase("MSG")) {
						testdatapath.setFunction(msgSubType);
					} /* Added by Ashiki for TENJINCG-1275 ends */
					else {
						testdatapath.setFunction(funcCode);
					}
					testdatapath.setFileName(fileName);

					String tempawsuploaderPath = request.getServletContext().getRealPath("/");
					Utilities.checkForUploadsFolder(tempawsuploaderPath);
					tempawsuploaderPath = tempawsuploaderPath + File.separator + "Uploads" + File.separator
							+ testdatapath.getUser();
					fileupload.uploadTestDataFile(reporoot, testdatapath, fileName, inpstream, repoType,
							tempawsuploaderPath);

				} catch (FileUploadException fe) {
					logger.error("ERROR while uploading the template", fe);
					map.put("STATUS", "ERROR");
					map.put("MESSAGE", " An internal error occured");
					request.getRequestDispatcher("v2/upload_newfile.jsp").forward(request, response);
				}
				try {
					testDataList = new TestDataHelper().hydrateAllTestData(projectId, userId);
					request.setAttribute("testDataList", testDataList);
					map.put("STATUS", "SUCCESS");
					map.put("MESSAGE", "");
					SessionUtils.setScreenState("success", "Test Data Uploaded Successfully", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.getRequestDispatcher("v2/uploadtestdata_list.jsp").forward(request, response);

				} catch (DatabaseException db) {
					logger.error("ERROR while fetching Test Data list", db);
					map.put("STATUS", "ERROR");
					map.put("MESSAGE", "Could not load test datalist due to an internal error");
					request.getRequestDispatcher("error.jsp").forward(request, response);
				}

			}
		}

	}

	private List<Api> getAppApisForApplication(String appId) throws DatabaseException {

		List<Api> listApis = new ApiHandler().hydrateAllApi(appId);
		for (Api api : listApis) {
			ArrayList<ApiLearnerResultBean> lrnr_runs = new ApiHelper().hydrateLearningHistory(api.getCode(),
					Integer.parseInt(appId));
			api.setLearningHistory(lrnr_runs);
		}
		return listApis;
	}

}
