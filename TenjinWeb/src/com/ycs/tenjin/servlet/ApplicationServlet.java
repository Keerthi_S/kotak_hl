/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApplicationServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 03-11-2107           Padmavathi              Newly added for TENJINCG-400
 * 24-Nov-2017			Sahana					Updated appType to integer,UI changes
 * 16-03-2018			Preeti					TENJINCG-73
 * 26-Nov 2018			Shivam	Sharma			TENJINCG-904
 * 17-12-2018           Padmavathi              TJN252-36
 * 22-03-2019			Preeti					TENJINCG-1018
 * 27-03-2019			Ashiki					TENJINCG-1004
 * 09-04-2019				Prem					TNJN27-57
 * 24-04-2019			Roshni					TENJINCG-1038
 * 12-08-2019			Prem					TENJINCG-1085
 * 04-12-2019			Preeti					TENJINCG-1174
 * */

package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.AutAuditRecord;
import com.ycs.tenjin.bridge.utils.ApiUtilities;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class ApplicationServlet
 */
@WebServlet("/ApplicationServlet")
public class ApplicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(ApplicationServlet.class); 
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ApplicationServlet() {
		super();
		
	}
	
	public static AuthorizationRule authorizationRule = new AuthorizationRule() {
        
        @Override
        public boolean checkAccess(HttpServletRequest request) {
        	if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
                    return true;
            }
            return false;
        }  
    };
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		String task = Utilities.trim(request.getParameter("t"));
		AutHandler handler=new AutHandler();
		
		 Authorizer authorizer=new Authorizer();
			if(task.equalsIgnoreCase("") || task.equalsIgnoreCase("view") || task.equalsIgnoreCase("new") || task.equalsIgnoreCase("oauth_view") || task.equalsIgnoreCase("exist_check") || task.equalsIgnoreCase("download_APK_file") ) {
				if(!authorizer.hasAccess(request)){
					response.sendRedirect("noAccess.jsp");
					return;
				}
			}
		
		if(task.equalsIgnoreCase("view")) {
			try {
				List<String> adapters= handler.getAllAdapters();
				/*Modified by Prem for TENJINCG-1085 starts*/
				int appId = Integer.parseInt(request.getParameter("key").trim());
				/*Modified by Prem for TENJINCG-1085 starts*/
				Aut aut = new AutHandler().getApplication(appId);
				request.setAttribute("aut", aut);
				request.setAttribute("adapters", adapters);
				request.getRequestDispatcher("v2/aut_view.jsp").forward(request, response);
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid App ID [{}]", request.getParameter("appId"));
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching function information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}

		}
		else if(task.equalsIgnoreCase("new")){
			Aut aut =new Aut();
			List<String> adapters=null;
			try {
				adapters = handler.getAllAdapters();
				request.setAttribute("aut", aut);
				request.setAttribute("adapters", adapters);
				request.getRequestDispatcher("v2/aut_new.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching all adapters", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}


		}
		/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Starts*/
		else if(task.equalsIgnoreCase("oauth_view")){
			Aut oAuth = new Aut();
			String appId = request.getParameter("app");
			try {
				oAuth = handler.getOAuthRecord(Integer.parseInt(appId));
				request.setAttribute("appId", appId);
				request.setAttribute("oauth", oAuth);
			} catch (NumberFormatException e) {
				logger.error("ERROR while fetching oAuth Details");
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR while fetching oAuth Details");
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
			request.getRequestDispatcher("v2/oauth_details.jsp").forward(request, response);
		}
		
		else if(task.equalsIgnoreCase("exist_check")){

			String appId = request.getParameter("app");
			String ret = "";
			try{
				JSONObject json = new JSONObject();
				try {
					boolean exists = handler.oAuthRecordExists(Integer.parseInt(appId));
					if(exists){
						json.put("status", "SUCCESS");
						ret = json.toString();
					}
					else{
						json.put("status","NOT_FOUND");
						ret = json.toString();
					}
				} catch (NumberFormatException e) {
					logger.error("ERROR in checking the OAuth existence",e.getMessage());
					json.put("status","ERROR");
					ret = json.toString();
				} catch (DatabaseException e) {
					logger.error("ERROR in checking the OAuth existence",e.getMessage());
					json.put("status","ERROR");
					json.put("message", e.getMessage());
					ret = json.toString();
				} 
			}catch(JSONException e){
				logger.error("ERROR in checking the OAuth existence",e.getMessage());
				logger.error("JSONException.", e);
				ret = "{status:ERROR,message:Please contact Tenjin Support.";
			}
			response.getWriter().write(ret);

		}
		/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Ends*/

/*Added by Ashiki TENJINCG-1004 starts*/
		else if(task.equalsIgnoreCase("download_APK_file")) {
			String retData = "";
			String appId=request.getParameter("appId");
			String mediaRepoPath = null;
			String fileName="";
			String finalPath = "";
	    	FileInputStream instream = null;
	    	
	    	try {
			JSONObject json = new JSONObject();
			String folderPath = request.getServletContext().getRealPath("/");
			 /*added by Prem for  TNJN27-57 Starts*/ 
			Utilities.checkForDownloadsFolder(folderPath);
			/*added by Prem for  TNJN27-57 Ends */
			FileOutputStream outstream = null;
	    	try{
	    		
	    		fileName = handler.downloadApkFile(Integer.parseInt(appId));
				mediaRepoPath = TenjinConfiguration.getProperty("APP_FILE_PATH");
				 
				finalPath = mediaRepoPath+appId ;
	    	    File infile =new File(finalPath+File.separator+fileName);
	    	    File outfile =new File(folderPath + File.separator+"Downloads"+ File.separator+fileName);
	 
	    	    instream = new FileInputStream(infile);
	    	    outstream = new FileOutputStream(outfile);
	 
	    	    byte[] buffer = new byte[1024];
	 
	    	    int length;
	    	    while ((length = instream.read(buffer)) > 0){
	    	    	outstream.write(buffer, 0, length);
	    	    }

	    	    instream.close();
	    	    outstream.close();
	    	    logger.info("Downloaded file successfully");
	    	    json.put("status", "SUCCESS");
				json.put("message","File Downloaded Successfully");
				json.put("path","Downloads"+File.separator +fileName);
	 
	    	} 
	    	/*added by Prem for  TNJN27-57 Starts*/ 
	    	catch (FileNotFoundException e) {
	    		logger.error("File Not Uploaded",e);
				logger.error("Error ", e);
				 json.put("status", "error");
				 json.put("message","File Not Uploaded");
			}
	    	/*added by Prem for  TNJN27-57 Ends*/ 
	    	catch (Exception e) {
	    		logger.error("An exception occured while Downloading file",e);
				logger.error("Error ", e);
				 json.put("status", "error");
				 json.put("message","An Internal Error Occurred. Please try again");
			}
			finally{
				retData = json.toString();
			}
	    	}catch (Exception e) {
	    		retData = "An Internal Error Occurred. Please contact your system administrator";
			}
			response.getWriter().write(retData);
		}
		/*Added by Ashiki TENJINCG-1004 ends*/
		else{
			try {
				List<Aut> aut = handler.getAllApplications();
				request.setAttribute("aut", aut);
				request.getRequestDispatcher("v2/aut_list.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching all application", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		AutHandler handler=new AutHandler();
		Aut aut=this.mapAttributes(request);
		//added by shivam sharma for  TENJINCG-904 starts
		List<String> adapters=null;
		//added by shivam sharma for  TENJINCG-904 ends
		String type = Utilities.trim(request.getParameter("transactionType"));
		String delFlag = Utilities.trim(request.getParameter("del"));
		
		
		 Authorizer authorizer=new Authorizer();
			if(delFlag.equalsIgnoreCase("") || delFlag.equalsIgnoreCase("true") || delFlag.equalsIgnoreCase("new") || delFlag.equalsIgnoreCase("saveoAuthDetails")) {
				if(!authorizer.hasAccess(request)){
					response.sendRedirect("noAccess.jsp");
					return;
				}
			}
		if(delFlag.equalsIgnoreCase("true")) {
			this.doDelete(request, response);
		}
		/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Starts*/
		else if(delFlag.equalsIgnoreCase("saveoAuthDetails")) {
		Aut oAuth = new Aut();
		try{
			oAuth = this.mapoAuthAttributes(request);
			handler.persistOAuthRecord(oAuth);
			SessionUtils.setScreenState("success", "oAuth.save.success", request);
			response.sendRedirect("ApplicationServlet?t=oauth_view&app=" +oAuth.getId());
		}
		catch (DatabaseException e) {
			logger.error("ERROR while persiting oAuth",e.getMessage()); 
			SessionUtils.setScreenState("error", "generic.db.error", request);
			SessionUtils.loadScreenStateToRequest(request);
			request.setAttribute("oauth", oAuth);
			request.setAttribute("appId", oAuth.getId());
			request.getRequestDispatcher("v2/oauth_details.jsp").forward(request, response);
		}
		catch (RequestValidationException e) {
		logger.error("ERROR while persiting oAuth",e.getMessage());
		SessionUtils.setScreenState("error", e.getMessage(),e.getTargetFields(), request);
		SessionUtils.loadScreenStateToRequest(request);
		request.setAttribute("oauth", oAuth);
		request.setAttribute("appId", oAuth.getId());
		request.getRequestDispatcher("v2/oauth_details.jsp").forward(request, response);
	}
	
		}
		/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Ends*/
		else{
			if(type.equalsIgnoreCase("update")) {
				//added by shivam sharma for  TENJINCG-904 starts
				//List<String> adapters=null;
				//added by shivam sharma for  TENJINCG-904 ends
				/*Added by Roshni for TenjinCG-1038 starts*/ 
				TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
				AutAuditRecord audit=new AutAuditRecord();
				audit.setEntityRecordId(aut.getId());
				audit.setLastUpdatedBy(tjnSession.getUser().getId());
				audit.setEntityType("application");
				aut.setAutAuditRecord(audit);
				/*Added by Roshni for TenjinCG-1038 ends*/ 
				try {
					handler.update(aut,aut.getId());
					SessionUtils.setScreenState("success", "aut.update.success", request);
					response.sendRedirect("ApplicationServlet?t=view&key=" + aut.getId());
				} catch (DatabaseException e) {
					logger.error(e.getMessage(), e);
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("aut", aut);
					try {
						adapters= handler.getAllAdapters();
					} catch (DatabaseException e1) {}
					request.setAttribute("adapters", adapters);
					request.getRequestDispatcher("v2/aut_view.jsp").forward(request, response);
				} catch (RequestValidationException e) {
					logger.error(e.getMessage(), e);
					SessionUtils.setScreenState("error", e.getMessage(),e.getTargetFields(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("aut", aut);
					try {
						adapters= handler.getAllAdapters();
					} catch (DatabaseException e1) {}
					request.setAttribute("adapters", adapters);
					request.getRequestDispatcher("v2/aut_view.jsp").forward(request, response);
				} /*Added by Padmavathi for TJN252-36 starts*/
				catch (ResourceConflictException e) {
					logger.error(e.getMessage(), e);
					SessionUtils.setScreenState("error", e.getMessage(),e.getTargetFields(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("aut", aut);
					try {
						adapters= handler.getAllAdapters();
					} catch (DatabaseException e1) {}
					request.setAttribute("adapters", adapters);
					request.getRequestDispatcher("v2/aut_view.jsp").forward(request, response);
				}
				/*Added by Padmavathi for TJN252-36 ends*/
			}
			//added by shivam sharma for  TENJINCG-904 starts
			//else{
			//List<String> adapters=null;
			else if(type.equalsIgnoreCase("New")){
				//added by shivam sharma for  TENJINCG-904 ends
				try {
					handler.persist(aut);
					SessionUtils.setScreenState("success", "aut.create.success", request);
					response.sendRedirect("ApplicationServlet?t=view&key=" + aut.getId());
				} catch (DatabaseException e) {
					logger.error(e.getMessage(), e);
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					try {
						adapters= handler.getAllAdapters();
					} catch (DatabaseException e1) {}
					request.setAttribute("aut", aut);
					request.setAttribute("adapters", adapters);
					request.getRequestDispatcher("v2/aut_new.jsp").forward(request, response);
				} catch (RequestValidationException e) {
					logger.error(e.getMessage(), e);
					SessionUtils.setScreenState("error", e.getMessage(),e.getTargetFields(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("aut", aut);
					try {
						adapters= handler.getAllAdapters();
					} catch (DatabaseException e1) {}
					request.setAttribute("adapters", adapters);
					request.getRequestDispatcher("v2/aut_new.jsp").forward(request, response);
				}
			}
			//added by shivam sharma for  TENJINCG-904 starts
			else{
				boolean isMultiPart;
				isMultiPart = ServletFileUpload.isMultipartContent(request);
				if(isMultiPart){
					logger.debug("Form is multipart");
					String appId =null;
					try {
						DiskFileItemFactory factory = new DiskFileItemFactory();
						ServletFileUpload upload = new ServletFileUpload(factory);
						List fileItems = upload.parseRequest(request);

						Iterator fileItemsIter = fileItems.iterator();

						Map<String, String> formValues = new HashMap<String, String>();
						FileItem fi =null;
						while(fileItemsIter.hasNext()){
							fi = (FileItem)fileItemsIter.next();
							if(fi.isFormField()){
								formValues.put(fi.getFieldName(), fi.getString());
							}
						}
						String fileName = fi.getName();
						appId = formValues.get("appId");
						String mediaRepoPath = TenjinConfiguration.getProperty("APP_FILE_PATH");
						String finalPath = mediaRepoPath+appId+File.separator ;
						/*Added by Ashiki TENJINCG-1004 starts*/
				    	File fileExist = new File(mediaRepoPath+appId);
				    	if (fileExist.isDirectory()) {
				    		String[] file = fileExist.list();
				    		for(String s: file){
				    			File currentFile = new File(fileExist.getPath(),s);
				    			currentFile.delete();
				    		}
				    	}
				    	/*Added by Ashiki TENJINCG-1004 starts*/
						ApiUtilities.checkPath(finalPath);
						try {
							fi.write(new File(finalPath + fileName)); 
						} catch (Exception e) {
							logger.warn("Could not process uploaded file [{}]", fileName, e);
							logger.error(e.getMessage(), e);
							SessionUtils.setScreenState("error", "Could not process uploaded file",null );
							SessionUtils.loadScreenStateToRequest(request);
							response.sendRedirect("ApplicationServlet?t=view&key=" + Integer.parseInt(appId));
						}
						try {
							handler.updateAppFileName(fileName, Integer.parseInt(appId));
							RequestDispatcher rs=request.getRequestDispatcher("apk_upload.jsp?param=autView&appId="+appId+"&msg=File uploaded sucessfully.");
							rs.forward(request, response);
						} catch (NumberFormatException | DatabaseException e) {
							logger.warn("Could not process uploaded file [{}]", fileName, e);
							logger.error(e.getMessage(), e);
							RequestDispatcher rs=request.getRequestDispatcher("apk_upload.jsp?param=autView&appId="+appId+"&msg=Could not process uploaded file");
							rs.forward(request, response);
						}
					}catch (FileUploadException e) {
						logger.warn("Could not process uploaded file [{}]", e);
						logger.error(e.getMessage(), e);
						RequestDispatcher rs=request.getRequestDispatcher("apk_upload.jsp?param=autView&appId="+appId+"&msg=Could not process uploaded file");
						rs.forward(request, response);

					} catch (TenjinConfigurationException e1) {
						logger.warn("Could not process uploaded file [{}]", e1);
						RequestDispatcher rs=request.getRequestDispatcher("apk_upload.jsp?param=autView&appId="+appId+"&msg=Could not process uploaded file");
						rs.forward(request, response);
					}
				}

			}
			//added by shivam sharma for  TENJINCG-904 ends
		}

	}

	private Aut mapAttributes(HttpServletRequest request) {
		Aut aut=new Aut();
		String id=	Utilities.trim(request.getParameter("id"));
		if(id!=null&&!id.equals(""))
			aut.setId(Integer.parseInt(id));
		aut.setName(Utilities.trim(request.getParameter("name")));
		/*Added by sahana for changing appType to integer:starts*/
		if(request.getParameter("type")!=null)
			aut.setApplicationType(Integer.parseInt(request.getParameter("type")));
		/*Added by sahana for changing appType to integer:ends*/
		aut.setURL(Utilities.trim(request.getParameter("URL")));
		aut.setApplicationPackage(Utilities.trim(request.getParameter("applicationPackage")));
		aut.setAdapterPackage(Utilities.trim(request.getParameter("adapterPackage")));
		aut.setBrowser(Utilities.trim(request.getParameter("browser")));
		aut.setApplicationActivity(Utilities.trim(request.getParameter("applicationActivity")));
		aut.setPlatformVersion(Utilities.trim(request.getParameter("platformVersion")));
		aut.setDateFormat(Utilities.trim(request.getParameter("dateFormat")));
		aut.setLoginUserType(Utilities.trim(request.getParameter("loginUserType")));
		aut.setOperation(Utilities.trim(request.getParameter("operation")));
		/*Added by Preeti for TENJINCG-73 starts*/
		/*Added by Roshni for TenjinCG-1038 starts*/ 
		aut.setTemplatePwd(Utilities.trim(request.getParameter("templatePwd")));
		/*Added by Roshni for TenjinCG-1038 ends*/
		String pauseTime = Utilities.trim(request.getParameter("pauseTime"));
		if(pauseTime!=null && !pauseTime.equals(""))
			aut.setPauseTime(Integer.parseInt(pauseTime));
		else
			aut.setPauseTime(0);
		aut.setPauseLocation(Utilities.trim(request.getParameter("pauseLocation")));
		/*Added by Preeti for TENJINCG-73 ends*/
		return aut;
	}
	/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Starts*/
	private Aut mapoAuthAttributes(HttpServletRequest request) {
		Aut oAuth = new Aut();
		oAuth.setId(Integer.parseInt(request.getParameter("appId")));
		oAuth.setGrantType(request.getParameter("grantType"));
		oAuth.setCallBackUrl(request.getParameter("callBackUrl"));
		oAuth.setAuthUrl(request.getParameter("authUrl"));
		oAuth.setAccessTokenUrl(request.getParameter("accessToken"));
		oAuth.setClientId(request.getParameter("clientId"));
		oAuth.setClientSecret(request.getParameter("clientSecret"));
		oAuth.setAddAuthDataTo(request.getParameter("authData"));
		return oAuth;
	}
	/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Ends*/
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] selectedAutCodes = request.getParameterValues("selectedAutCodes");
		AutHandler handler = new AutHandler();
		try {
			handler.deleteAuts(selectedAutCodes);
			if(selectedAutCodes.length>1) {
				SessionUtils.setScreenState("success", "aut.delete.multiple.success", request);
			}else {
				SessionUtils.setScreenState("success", "aut.delete.success",request);
			}
			response.sendRedirect("ApplicationServlet");
		} catch (NumberFormatException e) {
			logger.error("ERROR - Invalid Application ID {}", selectedAutCodes, e);
			SessionUtils.setScreenState("error", "generic.error", request);
			response.sendRedirect("ApplicationServlet");
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			SessionUtils.setScreenState("error", "generic.db.error", request);
			response.sendRedirect("ApplicationServlet");
		} catch (ResourceConflictException e) {
			SessionUtils.setScreenState("error", e.getMessage(), request);
			response.sendRedirect("ApplicationServlet");
		}
	}

}
