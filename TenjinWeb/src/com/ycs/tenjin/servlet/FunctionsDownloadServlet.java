/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  FunctionsDownloadServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	  CHANGED BY              DESCRIPTION
* 12-04-2019			  Pushpalatha			  TENJINCG-1030
* 03-12-2019			  Roshni				  TENJINCG-1168
* 
*/

package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.util.ExcelException;
import com.ycs.tenjin.util.ExcelHandler;

/**
 * Servlet implementation class UploadServlet
 */
/*@WebServlet(name = "/UploadServlet", urlPatterns = { "//UploadServlet" })*/
public class FunctionsDownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(UploadServlet.class);


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FunctionsDownloadServlet() {
		super();
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String appId=request.getParameter("aut");
		String funcCode=request.getParameter("functionCode");
		Aut aut=null;
		/* modified by Roshni for TENJINCG-1168 starts */
		AutHandler handler=new AutHandler();
		try {
			/*aut = new AutsHelper().hydrateAut(Integer.parseInt(appId));*/
			aut = handler.getApplication(Integer.parseInt(appId));
			/* modified by Roshni for TENJINCG-1168 ends */
		} catch (NumberFormatException e1) {
			logger.error("Error ", e1);
		} catch (DatabaseException e1) {
			logger.error("Error ", e1);
		}
		String app=aut.getName();
		Map<String, Object> map = new HashMap<String, Object>();

		String dUrl = "";
		
		if(app.equalsIgnoreCase("-1"))
		{
			
			map.put("status", "NoApp");
			map.put("message", "Please Select an Application");
		}
		else{
		
	
		try{
			
			
			/*modified by paneendra for VAPT fix starts*/
			String rootPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
			File dir = new File(rootPath + "\\Downloads");
			if(!dir.exists()){
				dir.mkdirs();
			}
			String folderPath =rootPath+"\\Downloads\\";
            /*modified by paneendra for VAPT fix ends*/
			
			ExcelHandler e=new ExcelHandler();
			
			
			
			
			e.processFunctionsDownload(folderPath,aut,funcCode);
			
			app=e.removeSpaces(app);
			map.put("message", "Functions Download Successfully");
			map.put("status", "SUCCESS");
			map.put("path","./Downloads/"+app+"-Functions.xlsx");
			map.put("tc_id", "");
		}catch(Exception e){
			logger.error("An exception occured while Downloading functions",e);
			map.put("status", "ERROR");
			map.put("message","An Internal Error Occurred. Please try again");
		}
		}
		request.getSession().setAttribute("SCR_MAP", map);
		dUrl = "downloadtempFunctions.jsp";
		response.sendRedirect(dUrl);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String appId=request.getParameter("lstApplication");
		Aut aut=null;
		/* modified by Roshni for TENJINCG-1168 starts */
		AutHandler handler=new AutHandler();
		try {
			aut = handler.getApplication(Integer.parseInt(appId));
			/* modified by Roshni for TENJINCG-1168 ends */
		} catch (NumberFormatException e1) {
			logger.error("Error ", e1);
		} catch (DatabaseException e1) {
			logger.error("Error ", e1);
		}
		String app=aut.getName();
		Map<String, Object> map = new HashMap<String, Object>();

		String dUrl = "";
		
		if(app.equalsIgnoreCase("-1"))
		{
			
			map.put("status", "NoApp");
			map.put("message", "Please Select an Application");
		}
		else{
		
	
		try{
			
			
			String rootPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
			File dir = new File(rootPath + "\\Downloads");
			if(!dir.exists()){
				dir.mkdirs();
			}
			
			String folderPath =rootPath+"\\Downloads\\";
			
			ExcelHandler e=new ExcelHandler();
			
			
			
			
			e.processFunctionsDownload(folderPath,aut,"");
			
			app=e.removeSpaces(app);
			map.put("message", "Functions Download Successfully");
			map.put("status", "SUCCESS");
			map.put("path","./Downloads/"+app+"-Functions.xlsx");
			map.put("tc_id", "");
		}
		/*added by shruthi for Tenj210-25 starts*/
		catch(ExcelException e1)
		{
			map.put("message", "Selected application does not contain any function(s)");
			map.put("status", "ERROR");
			
		}
		/*added by shruthi for Tenj210-25 ends*/
		
		catch(Exception e){
			logger.error("An exception occured while Downloading functions",e);
			map.put("status", "ERROR");
			map.put("message","An Internal Error Occurred. Please try again");
		}
		}
		request.getSession().setAttribute("SCR_MAP", map);
		dUrl = "downloadtempFunctions.jsp";
		response.sendRedirect(dUrl);
	}
	

}

