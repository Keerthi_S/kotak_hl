/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectServletNew.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 15-10-2018		   	Pushpalatha			  	Newly Added 
* 17-10-2018            Leelaprasad             TENJINCG-827
* 19-10-2018           	Leelaprasad             TENJINCG-829
* 26-10-2017			Sriram					TENJINCG-875
* 08-11-2018          	Leelaprasad             TENJINCG-874
* 09-11-2018           	Ramya                   TENJINCG-831
* 28-11-2018			Ramya					TJNUN262-5
* 06-12-2018            Leelaprasad             TJNUN262-62
* 14-12-2018			Prem					TJNUN262-57
* 24-12-2018			Pushpalatha				TJN262R2-42
* 08-02-2019			Pushpalatha				TENJINCG-925
* 18-02-2019			Preeti					TENJINCG-969
* 19-02-2019			Padmavathi		        TENJINCG-924
* 28-02-2019			Pushpa					TENJINCG-980,981
* 26-03-2019			Roshni					TJN252-54
* 05-04-2019            Padmavathi              TNJN27-65
* 11-04-2019            Padmavathi              TNJN27-84
* 11-06-2019			Pushpalatha				TJN27-5
* 14-10-2019			Ashiki					Sprint27-9
* 04-12-2019			Preeti					TENJINCG-1174
* 06-02-2020			Roshni					TENJINCG-1168
* 05-02-2021            Shruthi A N             TENJINCG-1255 
*/

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DefectHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.defect.DefectManagementInstance;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.ModuleHandler;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.ProjectTestDataPath;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.testmanager.ETMException;
import com.ycs.tenjin.testmanager.TestManager;
import com.ycs.tenjin.testmanager.TestManagerFactory;
import com.ycs.tenjin.testmanager.TestManagerInstance;
import com.ycs.tenjin.testmanager.UnreachableETMException;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class ProjectServletNew
 */
@WebServlet("/ProjectServletNew")
public class ProjectServletNew extends HttpServlet {
	
	public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		@Override
		public boolean checkAccess(HttpServletRequest request) {
		
			String txn = request.getParameter("t");
			if (request.getMethod().equalsIgnoreCase("GET")) {
				
				if(txn.equalsIgnoreCase("project_new")) {
					if (new AdminAuthorizationRuleImpl().checkAccess(request)) {
						return true;
					}
				}
				else if(txn.equalsIgnoreCase("load_project")) {
					if (new AdminAuthorizationRuleImpl().checkAccess(request)) {
						return true;
					}
				}
				else {
					return true;
				}
			} else if (request.getMethod().equalsIgnoreCase("POST")) {
				if(txn.equalsIgnoreCase("create")) {
				if (new AdminAuthorizationRuleImpl().checkAccess(request)) {
					return true;
				}
				
				}else {
					return true;
				}
			}
			return false;
		}
		
	};
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory
			.getLogger(ProjectServletNew.class);
	
	Date endDate = null;
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	Calendar calendar = Calendar.getInstance();
	/*Added by Padmavathi for  TNJN27-65 starts*/
	private String prjViewCallback=null; 
	/*Added by Padmavathi for  TNJN27-65 ends*/
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjectServletNew() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String txn=request.getParameter("t");
		/*added by shruthi for VAPT FIX to Servlets starts*/
		Authorizer authorizer=new Authorizer();
		if(txn.equalsIgnoreCase("") || txn.equalsIgnoreCase("project_new") ||  txn.equalsIgnoreCase("load_project")) {
			if(!authorizer.hasAccess(request)){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "User is not an administrator and hence cannot access");
				request.getSession().setAttribute("SCR_MAP", map);
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		/*added by shruthi for VAPT FIX to Servlets ends*/
		/*Added by Ashiki for Sprint27-9 starts*/
		SessionUtils.loadScreenStateToRequest(request);
		/*Added by Ashiki for Sprint27-9 ends*/
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		User currentUser=tjnSession.getUser();
		Project currentProject=tjnSession.getProject();
		
		
		SessionUtils.loadScreenStateToRequest(request); 
       /*Changed by Leelaprasadfor the TENJINCG-827 starts*/
		ProjectHandler projectHandler=new ProjectHandler();
		/*Changed by Leelaprasadfor the TENJINCG-827 ends*/
		Project proj= new Project();
		if(txn.equalsIgnoreCase("project_new")){
			String domainName=request.getParameter("name");
			String projectType=request.getParameter("projectType");
			UserHandler userHandler=new UserHandler();
			try {
				List<User> user=userHandler.getAllUsers();
				request.setAttribute("callback", domainName);
				request.setAttribute("currentUser", currentUser);
				request.setAttribute("user", user);
				request.setAttribute("projectType", projectType);
				
				proj= projectHandler.hydrateAwsDeatils();
				String AccessKey = proj.getAccessKey();
				String AccessSecKey  = proj.getAccessSecKey();
				request.setAttribute("AccessKey", AccessKey);
				request.setAttribute("AccessSecKey", AccessSecKey);
				request.getRequestDispatcher("v2/project_new.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("Error ", e);
			}
		}else if(txn.equals("load_project")){
			String projectId=request.getParameter("paramval");
			/*Changed by Leelaprasadfor the TENJINCG-827 starts*/
			String projectViewType=request.getParameter("view_type");
			/*Changed by Leelaprasadfor the TENJINCG-827 ends*/
			
			/*Commented by Padmavathi for TENJINCG-924 starts*/
			String prefrence=request.getParameter("prefrence");
			/*Commented by Padmavathi for TENJINCG-924 ends*/
			
			/*Added by Ramya for TJNUN262-5 Starts*/
			try {
				User user=projectHandler.hydrateCurrentUserForProject(currentUser.getId(),Integer.parseInt(projectId));
				if(user!=null)
					currentUser.setRoleInCurrentProject(user.getRoleInCurrentProject());
				//Keerthi TJN27-184 Starts
				else {
					currentUser.setRoleInCurrentProject(null);
					//Added by prem to restrict unauthorized users starts
					if(!"admin".equalsIgnoreCase(Utilities.trim(projectViewType))) {
					Map<String, Object> map = new HashMap<String, Object>();
					logger.error("Project Not Found For the User");
					map.put("STATUS", "ERROR");
					map.put("MESSAGE", "User does't have access");
					request.getSession().setAttribute("SCR_MAP", map);
					response.sendRedirect("noAccess.jsp");
					return;
					//Added by prem to restrict unauthorized users End
					}}
				//Keerthi TJN27-184 Ends
			} catch (NumberFormatException e1) {
				
				logger.error("Error ", e1);
			} catch (DatabaseException e1) {
				
				logger.error("Error ", e1);
			}
			/*Added by Ramya for TJNUN262-5 ends*/
			try {
				Project project=new ProjectHandler().hydrateProject(Integer.parseInt(projectId));
				/*Commented by Padmavathi for TENJINCG-924 starts*/
				/*Changed by Leelaprasadfor the TENJINCG-827 starts*/
				if (project != null && projectViewType != null
						&& projectViewType.equalsIgnoreCase("project")) {
					String userId = tjnSession.getUser().getId();
					/*Changed by leelaprasad for TENJINCG-829 starts*/
						int preferenceProjectId=projectHandler.hydrateUserProjectPreference(userId);
						if(preferenceProjectId!=0 &&preferenceProjectId!=project.getId()){
							if(prefrence!=null && prefrence.equalsIgnoreCase("Y"))
							projectHandler.updateUserProjectPrefrence(Integer.parseInt(projectId), userId);
						
					}
					/*Changed by leelaprasad for TENJINCG-829 starts*/
					if (tjnSession.getProject() != null) {
						int oldProjectId = tjnSession.getProject().getId();
						if (oldProjectId != project.getId()) {
							// end old project session for user
							projectHandler.endProjectSession(oldProjectId,
									userId);
							// start new project session for user
							projectHandler.startProjectSession(project.getId(),
									userId);
							tjnSession.setProject(project);
						}
					} else {
						projectHandler.startProjectSession(project.getId(),
								userId);
					}
					

				}
				/*Commented by Padmavathi for TENJINCG-924 ends*/
				//SRIRAM for TENJINCG-875
				if("admin".equalsIgnoreCase(Utilities.trim(projectViewType))) {
					request.setAttribute("refresh_tree", Utilities.trim(request.getParameter("refresh_tree")));
					request.setAttribute("select_project", Utilities.trim(request.getParameter("select_project")));
				}
				//SRIRAM for TENJINCG-875 ends
				
				/*Changed by Leelaprasadfor the TENJINCG-827 ends*/
				request.setAttribute("project", project);  
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				if(project.getStartDate()==null){
					request.setAttribute("sdate","NA");
				}else{
				String sdate=sdf.format(project.getStartDate());
				
				request.setAttribute("sdate",sdate);
				}
				if(project.getEndDate()==null){
					request.setAttribute("edate","NA");
				}else{
					request.setAttribute("edate",sdf.format(project.getEndDate()));
				}
				request.setAttribute("currentUser", currentUser);
				/*Added by Pushpa for TENJINCG-925 starts*/
				/*Modifed by Padmavathi for  TNJN27-65 starts*/
				if(projectViewType!=null){
					prjViewCallback=projectViewType;
					
				}
				/*request.setAttribute("View_Type",projectViewType);*/
				request.setAttribute("View_Type",prjViewCallback);
				/*Modifed by Padmavathi for  TNJN27-65 ends*/
				/*Added by Pushpa for TENJINCG-925 ends*/
				tjnSession.setProject(project);
				request.getRequestDispatcher("v2/project_view.jsp").forward(request, response);
			} catch (NumberFormatException e) {
				
				logger.error("Error ", e);
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			}
			
		}else if(txn.equals("UnmappedProjectUsers")){
			String projectId=request.getParameter("projectId");
			try {
				ArrayList<User> unMappedProjectUser=new ProjectHandler().hydrateUsersNotInProject(Integer.parseInt(projectId));
				request.setAttribute("unMappedProjectUser", unMappedProjectUser);
				request.setAttribute("projectId", projectId);
				request.setAttribute("user", currentUser);
				request.setAttribute("project",currentProject);
				request.getRequestDispatcher("v2/project_users.jsp").forward(request, response);
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			}
		}else if(txn.equals("mapUserToProject")){
			String projectId=request.getParameter("projectId");
			String selectedUser=request.getParameter("selectedUsers");
			try {
				new ProjectHandler().mapUsersToProject(Integer.parseInt(projectId),selectedUser,currentProject.getType());
				SessionUtils.setScreenState("success", "Projectuser.added.success", request);
				response.sendRedirect("ProjectServletNew?t=load_project&paramval="+projectId);
			} catch (NumberFormatException e) {
				
				logger.error("Error ", e);
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			} catch (RequestValidationException e) {
				
				logger.error("Error ", e);
			}
		}else if(txn.equals("UnMappedProjectAuts")){
			String projectId=request.getParameter("projectId");
			try {
				
				ArrayList<Aut> unMappedProjectAuts=projectHandler.hydrateUnmappedAuts(Integer.parseInt(projectId));
				request.setAttribute("unMappedProjectAuts", unMappedProjectAuts);
				request.setAttribute("projectId", projectId);
				request.getRequestDispatcher("v2/project_auts.jsp").forward(request, response);
			} catch (NumberFormatException e) {
				
				logger.error("Error ", e);
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			}
		}/*Changed by Leelaprasadfor the TENJINCG-827 starts*/
		else if (txn.equals("project_launch")) {
			String projectId = request.getParameter("projectId");
			/*Changed by leelaprasad for TENJINCG-829 starts*/
			 String prefrence=request.getParameter("prefrence");
			 /*Changed by leelaprasad for TENJINCG-829 starts*/
			Map<String, Object> map = new HashMap<String, Object>();

			/* Added by Prem for the defect TJNUN262-57 starts 
			String startdateString;
			Date today;
			try {
				ProductLicense productLicense = (ProductLicense) CacheUtils.getObjectFromRunCache("licensedetails");
//				ProductLicense p =tjnSession.getUser().getProductLicense();
				calendar.setTime(productLicense.getExpiryDate());
		       endDate = calendar.getTime();
				startdateString = formatter.format(startDate);
				today = formatter.parse(startdateString);
					long days = TimeUnit.DAYS.convert(endDate.getTime()-today.getTime(), TimeUnit.MILLISECONDS);
					if((days<=30) && !(days<0)) {
						map.put("MESSAGE", "Your License will expire on "+ formatter.format(endDate) + ". Please contact your Tenjin Administrator for renewal");
						 request.getSession().setAttribute("LICENSE_NOTIFICATION", map);
					}
					else if (days<0){
						map.put("MESSAGE", "Your License has expired. You have " + productLicense.getGraceDays() + " days in grace period to renew your license. Please contact your Tenjin Administrator for renewal");
						 request.getSession().setAttribute("LICENSE_NOTIFICATION", map);
					}
			} catch (ParseException | TenjinConfigurationException e1) {
				
				logger.error("Error ", e1);
			}
			 Added by Prem for the defect TJNUN262-57 ends 
			*/
			
			if (projectId != null && Utilities.isNumeric(projectId)) {
				try {
					logger.info("Hydrating Project Details for Project "
							+ projectId);

					Project project=projectHandler.hydrateProject(Integer.parseInt(projectId));
					logger.info("Project with ID " + projectId + " hydrated");
					ArrayList<Project> projects = projectHandler.hydrateAllProjectsForUser(tjnSession.getUser().getId());
					
					if (project != null && projects.size() > 0) {
						projectHandler.startProjectSession(project.getId(),
								tjnSession.getUser().getId());
								/*Changed by leelaprasad for TENJINCG-829 starts*/
						if(prefrence.equalsIgnoreCase("Y")){
							projectHandler.updateUserProjectPrefrence(Integer.parseInt(projectId), tjnSession.getUser().getId());
						}
						/*Changed by leelaprasad for TENJINCG-829 starts*/
						tjnSession.setProject(project);
						User user=projectHandler.hydrateCurrentUserForProject(currentUser.getId(),Integer.parseInt(projectId));
						tjnSession.getUser().setRoleInCurrentProject(user.getRoleInCurrentProject());
						
						request.getSession().setAttribute("TJN_SESSION",
								tjnSession);
						logger.info("Done");
                        map.put("prefrence", prefrence);
						request.getSession().setAttribute("SCR_MAP", map);
						request.getSession().setAttribute("PRJ_VIEW_TYPE",
								"PROJECT");
						logger.info("Redirectig to Project Landing Page");
						request.getRequestDispatcher("projectlanding.jsp")
								.forward(request, response);
					} else {
						logger.error("Project Not Found");
						map.put("STATUS", "ERROR");
						map.put("MESSAGE",
								"Project Not Found / Invalid Project");
						request.getSession().setAttribute("SCR_MAP", map);
						response.sendRedirect("landing_new.jsp");
					}
				} catch (Exception e) {
					logger.error("Project Not Found");
					map.put("STATUS", "ERROR");
					map.put("MESSAGE",
							"Project Not Found / Invalid Project");
					request.getSession().setAttribute("SCR_MAP", map);
					response.sendRedirect("landing_new.jsp");
				}
			} else {
				logger.error("Invalid Project ID " + projectId);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "Project Not Found / Invalid Project");
				request.getSession().setAttribute("SCR_MAP", map);
				response.sendRedirect("landing_new.jsp");
			}
		} /*Changed by Leelaprasadfor the TENJINCG-827 ends*/
		/*Changed by leelaprasad for TENJINCG-829 starts*/
		else if (txn.equalsIgnoreCase("load_domain_projects")) {
			String domainName = request.getParameter("paramval");
			/*Changed by Leelaprasad for the requirement of defect TJNUN262-62 starts*/
			String prefrence=request.getParameter("prefrence");
			/*Changed by Leelaprasad for the requirement of defect TJNUN262-62 ends*/
			@SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>) request
					.getSession().getAttribute("SCR_MAP");
			try {
				@SuppressWarnings("unchecked")
				Multimap<String, Object> prjMap = (Multimap<String, Object>) map
						.get("PRJ_MAP");
				Collection<Object> projects = prjMap.get(domainName);
				map.put("PRJS", projects);
				map.put("STATUS", "SUCCESS");
				/*Changed by Leelaprasad for the requirement of defect TJNUN262-62 starts*/
				map.put("prjPrefrence", prefrence);
				/*Changed by Leelaprasad for the requirement of defect TJNUN262-62 ends*/
			} catch (Exception e) {
				logger.error("ERROR while fetching projects for domain "
						+ domainName);
				map.put("STATUS", "FAILURE");
				map.put("MESSAGE",
						"An Internal Error Occurred. Please Try again");
			} finally {
				request.getSession().removeAttribute("SCR_MAP");
				request.getSession().setAttribute("SCR_MAP", map);
				request.getRequestDispatcher("landing_new.jsp").forward(request, response);
			}
		}
		else if (txn.equalsIgnoreCase("state_change")){
			String state=request.getParameter("currentState");
			String projectId=request.getParameter("project");
			projectHandler.changeProjectState(Integer.parseInt(projectId), state);
			/*Modified by Pushpa to TJN262R2-42 starts*/
			if(state.equalsIgnoreCase("A")){
				SessionUtils.setScreenState("success", "project.deactivate", request);	
				tjnSession.getProject().setState("I");
				String abc="";
			}else{
				SessionUtils.setScreenState("success", "project.activate", request);
				tjnSession.getProject().setState("A");
			}
			/*Modified by Pushpa TJN262R2-42 ends*/
			response.sendRedirect("ProjectServletNew?t=load_project&paramval="+Integer.parseInt(projectId));
		}
		/*Changed by leelaprasad for TENJINCG-829 ends*/
		/*Changed by Leelaprasa dfor TENJINCG-874 starts*/
		else if (txn.equalsIgnoreCase("UPDATE_EXT_PRJ_DETAIL")) {
			int msgval = 0;
			String tjnPrjId = request.getParameter("tjnprjId");
			String tjnUserId = request.getParameter("tjnUserId");
			String editvaltc = request.getParameter("change1");
			String editvalts = request.getParameter("change2");
			 Map<String, Object> map = new HashMap<String, Object>();
			TestManagerInstance tmInstance = new TestManagerInstance();

			
			Project project = new Project();
			logger.info("Updating External TestManager Instance");

			String jsonString = request.getParameter("json");
			logger.debug("JSON String: " + jsonString);

			String retData = "";
			JSONObject retJson = new JSONObject();
			try {

				if (editvalts == null && editvaltc == null) {
					JSONObject json = new JSONObject(jsonString);

					project.setId(Integer.parseInt(json.getString("tjnprjId")));
					project.setEtmInstance(json.getString("instance"));
					project.setEtmProject(json.getString("prjName"));
					project.setEtmEnable(json.getString("isEnable"));

					/*project.setTcFilter(json.getString("tcFilter"));
					project.setTcFilterValue(json.getString("tcValue"));
					project.setTsFilter(json.getString("tsFilter"));
					project.setTsFilterValue(json.getString("tsValue"));*/
					projectHandler.validateInstanceCredintials(tjnUserId,
							json.getString("instance"));
					
					projectHandler.updateProjectTestManagerDetails(project);
					if (!(json.getString("tcFilter").equalsIgnoreCase("")) && !(json.getString("tcValue").equalsIgnoreCase("")) && !(json.getString("tsFilter").equalsIgnoreCase(""))&& !(json.getString("tsValue").equalsIgnoreCase(""))) {
						project.setTcFilter(json.getString("tcFilter"));
						project.setTcFilterValue(json.getString("tcValue"));
						project.setTsFilter(json.getString("tsFilter"));
						project.setTsFilterValue(json.getString("tsValue"));
						/*added by Roshni */
						project.setTcDisplayValue(json.getString("tcDisplayValue"));
						project.setTsDisplayValue(json.getString("tsDisplayValue"));
						projectHandler.updateFilterValues(project);
						msgval = 1;
					}
				}
				Project projectForETM = projectHandler
						.hydrateProjectETMDetails(tjnPrjId);
				tmInstance = projectHandler.hydrateTMCredentialsForProject(
						projectForETM.getEtmInstance(), tjnUserId, tjnPrjId);
				/*Modified by paneendra for VAPT FIX starts*/
				/*String decryptedPwd = new Crypto().decrypt(tmInstance
						.getPassword().split("!#!")[1], decodeHex(tmInstance
						.getPassword().split("!#!")[0].toCharArray()));*/
				String decryptedPwd = new CryptoUtilities().decrypt(tmInstance.getPassword());
				/*Modified by paneendra for VAPT FIX ends*/

				TestManager alm = null;
				alm = TestManagerFactory.getTestManagementTools(
						tmInstance.getTool(), tmInstance.getURL(),
						tmInstance.getUsername(), decryptedPwd);
				alm.login();
				alm.setProjectDesc(tmInstance.getTmProjectDomain());
				List<String> tcFields = alm.getTestCase();
				List<String> tsFields = alm.getTestStep();
				alm.logout();

				JSONArray jArrayTc = new JSONArray();
				JSONArray jArrayTs = new JSONArray();
				for (String tcField : tcFields) {
					JSONObject j = new JSONObject();
					j.put("tcField", tcField);
					jArrayTc.put(j);
				}
				for (String tsField : tsFields) {
					JSONObject j = new JSONObject();
					j.put("tsField", tsField);
					jArrayTs.put(j);
				}
				retJson.put("status", "success");
				tjnSession.setProject(projectForETM);
                request.getSession().setAttribute("TJN_SESSION", tjnSession);
				if (msgval == 0) {
					retJson.put("message", "TM instance updated successfully..");
					retJson.put("messTMval", "TM");
				} else {
					retJson.put("message", "Filter values updated successfully");

				}
				map.put("tcfields", tcFields);
				map.put("tsfields", tsFields);
				request.getSession().setAttribute("SCR_MAP", map);
				retJson.put("tcfields", jArrayTc);
				retJson.put("tsfields", jArrayTs);
				retData = retJson.toString();

			} catch (UnreachableETMException e) {
				logger.error("could not able to login to TM Tool", e);
			} catch (ETMException e) {

				try {
					retJson.put("status", "ERROR");
					
					retJson.put("message", e.getMessage());
					retData = retJson.toString();

				} catch (JSONException e1) {
					logger.error("Instance is not creted for the user", e);
					retData = "An internal error occurred. Please contact your System Administrator ";

				}
				logger.error("could not able to login to TM Tool", e);
			} catch (DatabaseException e) {

				try {
					retJson.put("status", "ERROR");
					retJson.put("message", e.getMessage());
					retData = retJson.toString();

				} catch (JSONException e1) {
					logger.error("Instance is not creted for the user", e);
					retData = "An internal error occurred. Please contact your System Administrator ";

				}
			} catch (Exception e) {
				try {
					retJson.put("status", "ERROR");
					retJson.put("message",
							"Could not create External Test Management Linkage due to an internal error");
					retData = retJson.toString();

				} catch (JSONException e1) {
					logger.error(
							"An internal error occurred. Please contact your System Administratorr",
							e);
					retData = "An internal error occurred. Please contact your System Administrator ";

				}
			}
			response.getWriter().write(retData);

		} else if (txn.equalsIgnoreCase("fetch_tc_and_ts_from_tm")) {
			int projectId = tjnSession.getProject().getId();
			String userId = tjnSession.getUser().getId();
			Map<String, Object> map = new HashMap<String, Object>();
			
			TestManagerInstance tmInstance = new TestManagerInstance();
			Project prj = null;
			try {
				prj = projectHandler.hydrateProjectETMDetails(String
						.valueOf(projectId));
				tmInstance = projectHandler
						.hydrateTMCredentialsForProject(prj.getEtmInstance(),
								userId, String.valueOf(projectId));
				/*Modified by paneendra for VAPT FIX starts*/
				/*String decryptedPwd = new Crypto().decrypt(tmInstance
						.getPassword().split("!#!")[1], decodeHex(tmInstance
						.getPassword().split("!#!")[0].toCharArray()));*/
				String decryptedPwd = new CryptoUtilities().decrypt(tmInstance.getPassword());
				/*Modified by paneendra for VAPT FIX ends*/
				TestManager alm = null;
				alm = TestManagerFactory.getTestManagementTools(
						tmInstance.getTool(), tmInstance.getURL(),
						tmInstance.getUsername(), decryptedPwd);
				alm.login();
				alm.setProjectDesc(tmInstance.getTmProjectDomain());
				List<String> tsFields = alm.getTestStep();
				List<String> tcFields = alm.getTestCase();
				alm.logout();
				request.getSession().removeAttribute("SCR_MAP");
				map.put("tcFields", tcFields);
				map.put("tsFields", tsFields);
				map.put("project", prj);
				request.getSession().setAttribute("SCR_MAP", map);
				RequestDispatcher rd = request
						.getRequestDispatcher("maptmattributes.jsp");
				rd.forward(request, response);

			} catch (DatabaseException e) {
				logger.error(
						"could not fetch tc and ts from etm for instance {}",
						prj.getEtmInstance(), e);
			}catch (UnreachableETMException e) {
				logger.error("Could not able to login into TM tool.", e);
			} catch (ETMException e) {

				logger.error("Could not able to login into TM tool.", e);

			} catch (TenjinConfigurationException e) {
				
				logger.error("Could not able to login into TM tool.", e);
			}
		}
		/*Changed by Leelaprasad for TENJINCG-874 ends*/
		else if (txn.equalsIgnoreCase("Fetch_Manager_Projects")){
			String instance = request.getParameter("instance");
			/* Added By Ashiki TENJINCG-986 starts */
			String userId=currentUser.getId();
			/* Added By Ashiki TENJINCG-986 ends */
			String retData="";
			JSONObject rJson=null;
			/* Changed By Ashiki TENJINCG-986 starts */
			JSONArray subSetArray = new JSONArray();
			DefectManagementInstance mngr = new DefectManagementInstance();
			try {
				rJson = new JSONObject();
				/*mngr = new ProjectHandler().hydrateDefectManagerInstance(instance, tjnSession.getUser().getId());
				String url = mngr.getURL();
				String username = mngr.getAdminId();
				String encPwd = mngr.getPassword();
				String password = new Crypto().decrypt(encPwd.split("!#!")[1],
						decodeHex(encPwd.split("!#!")[0].toCharArray()));
				DefectManager dm = null;
				dm = DefectFactory.getDefectTools(mngr.getTool(), url, username, password);
				if(dm.isUrlValid()){
					dm.login(); 
					dttProjectsArray = dm.getAllProjects();
					dm.logout();*/
					
					
				DefectHelper helper = new DefectHelper();
				subSetArray=helper.hydrateSubSets(instance,userId);
				
				rJson.put("subSet", subSetArray);
				rJson.put("status", "SUCCESS");
					
					
					/*rJson.put("projects", dttProjectsArray);*/
					rJson.put("status", "SUCCESS");
				/*}else{
					rJson.put("status", "ERROR");
					
					rJson.put("message", mngr.getTool() + " is not accessible. Please check url and try again.");
				}*/
			
			} /*catch (EDMException e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							e.getMessage());
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
			} catch (UnreachableEDMException e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							e.getMessage());
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
			}*/ /* Added By Ashiki TENJINCG-986 ends */
			catch (JSONException e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occured, Please contact your System Administrator");
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
			} catch (DatabaseException e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message", e.getMessage());
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
			} catch (Exception e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occured, Please contact your System Administrator");
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
				logger.error("Error--------------> during fetching Defect Management projects");
			}
			finally {
				retData = rJson.toString();
			}
			response.getWriter().write(retData);
		} else if (txn.equalsIgnoreCase("edit_dtt_mapping_settings")){
			ProjectHelper prjHelper = new ProjectHelper();
			String retData = "";
			List<DefectManagementInstance> dttManagers = new ArrayList<DefectManagementInstance>();
			try {
				JSONArray jArray = new JSONArray();
				JSONObject json = new JSONObject();
				try {
					logger.debug("Hydrating Defect Management instances to map application to Defect Management Instance and Defect Management project" );
					dttManagers = new ProjectHandler().fetchDefectManagerInstance();
					/*dttManagers = prjHelper.fetchDefectManagerInstance();*/
					logger.info("hydrating of Defect Management instances done");
					Gson gson = new Gson();
					for(DefectManagementInstance t:dttManagers){
						JSONObject tJson = new JSONObject(gson.toJson(t));
						jArray.put(tJson);
					}

					json.put("status", "success");
					json.put("instances", jArray);
				} catch (DatabaseException e) {
					json.put("status", "error");
					json.put("message", e.getMessage());
					logger.error("Error during hydrating instances",e );
				} catch (JSONException e) {
					json.put("status", "error");
					json.put("message", e.getMessage());
					logger.error("Json exception during hydrating instances",e );
				}
				retData = json.toString();
			} catch (JSONException e) {
				logger.error("JSONException caught --> ", e);
				retData = "{status:error,message:An internal error occurred. Please contact Tenjin Support.}";
			}

			response.getWriter().write(retData);
			
			/*Added by Ashiki for Sprint27-9 starts*/
		}else if (txn.equalsIgnoreCase("DTT_UNMAP")) {
			String projectId = request.getParameter("p");
			String aId = request.getParameter("j");
			Project project = new Project();
			ProjectHelper pH = new ProjectHelper();
			JSONObject rJson=null;
			try {
				rJson = new JSONObject();
				project = pH.hydrateProject(Integer.parseInt(projectId));
						project.setInstance(null);
						project.setDefProject(null);
						project.setSubSet(null);
						project.setDttEnable("N");
						project.setDttKey(null);
						project = pH.updateDefMngrProject(project, Integer.parseInt(aId));
					rJson.put("status", "SUCCESS");
					rJson.put("message", "DTT un-mapped from project successfully");
			} catch (Exception e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occured, Please contact your System Administrator");
				} catch (JSONException e1) {
				}
				logger.error("could not unmap DTT from projects to aut and project {}",projectId,e);
			}
			response.getWriter().write(rJson.toString());
		}
		/*Added by Ashiki for Sprint27-9 ends*/
		//Added by prem to restrict unauthorized users starts
		else {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("STATUS", "ERROR");
			map.put("MESSAGE", "User does't have access");
			request.getSession().setAttribute("SCR_MAP", map);
			response.sendRedirect("noAccess.jsp");
		}
		//Added by prem to restrict unauthorized users Ends
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		/*added by shruthi for VAPT FIX to Servlets starts*/
		String txn=request.getParameter("t");
		Authorizer authorizer=new Authorizer();
		if(txn.equalsIgnoreCase("") || txn.equalsIgnoreCase("create") ||  txn.equalsIgnoreCase("update")) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		/*added by shruthi for VAPT FIX to Servlets ends*/
		SessionUtils.loadScreenStateToRequest(request);
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		
		ProjectHandler projectHandler=new ProjectHandler();
		if(txn.equalsIgnoreCase("create")){
			String AccessKey = request.getParameter("AccessKey");
			String AccessSecKey = request.getParameter("AccessSecKey");	
			String domainName=request.getParameter("callback");
			Project project=new Project();
			
			
			project=this.mapAttributes(request);
			project.setCreatedBy(tjnSession.getUser().getId());
			project.setDomain(domainName);
			project.setCreatedDate(new Timestamp(new Date().getTime()));
			try {
				
				project=projectHandler.persistProject(project);
				request.setAttribute("project", project);
				
				SessionUtils.setScreenState("success", "Project.create.success", request);
				//SessionUtils.loadScreenStateToRequest(request);
				/*request.getRequestDispatcher("v2/project_view.jsp").forward(request, response);*/
				response.sendRedirect("ProjectServletNew?t=load_project&paramval=" + project.getId() + "&view_type=ADMIN&refresh_tree=true&select_project=true");
			} catch (RequestValidationException e) {
				
				request.setAttribute("project", project);
				SessionUtils.loadScreenStateToRequest(request);
				SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
				response.sendRedirect("ProjectServletNew?t=project_new&name="+project.getDomain());
			}catch (DatabaseException e) {
				
				logger.error("Error ", e);
			}
			
			
		}else if(txn.equalsIgnoreCase("update")){
			Project project=new Project();
			project.setId(Integer.parseInt(request.getParameter("projectId")));
			project.setName(request.getParameter("name"));
			project.setDescription(request.getParameter("description"));
			project.setType(request.getParameter("type"));
			project.setScreenshotoption(request.getParameter("txtScreenShot"));
			/*Added by Pushpa for TENJINCG-980,981 starts*/
			project.setProjectOwnerEmail(request.getParameter("projectOwnerEmail"));
			if(!request.getParameter("edate").equals("NA")){
			String projectEndDate=request.getParameter("edate");
			 Timestamp ts=null;
			 if(projectEndDate.contains(" ")){
				  ts = java.sql.Timestamp.valueOf( projectEndDate ) ;
			 }else{
			   Date d=new Date(projectEndDate);
			    if(d!=null){
			        ts=new java.sql.Timestamp(d.getTime());
			    }
			 } 
			project.setEndDate(ts);
			}
			/*Added by Pushpa for TENJINCG-980,981 ends*/
			/*Added by Preeti for TENJINCG-969 starts*/
			AuditRecord audit = new AuditRecord();
			audit.setEntityRecordId(project.getId());
			audit.setLastUpdatedBy(tjnSession.getUser().getId());
			audit.setEntityType("project");
			project.setAuditRecord(audit);
			/*Added by Preeti for TENJINCG-969 ends*/
			try {
				projectHandler.updateProject(project);
				SessionUtils.setScreenState("success", "project.update.success", request);
				
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
				SessionUtils.setScreenState("error", "project.update.error", request);
				
			}
			tjnSession.getProject().setScreenshotoption(project.getScreenshotoption());

			response.sendRedirect("ProjectServletNew?t=load_project&paramval="+project.getId());
		}
		else if(txn.equalsIgnoreCase("unMapProjectUsers")){
			String projectId=request.getParameter("projectId");
			String userList=request.getParameter("selectedUsers");
			/*Added by Padmavathi for TNJN27-84 starts*/
			String prjAdminCount=request.getParameter("prjAdminCount");
			String prjType=request.getParameter("prjType");
			/*Added by Padmavathi for TNJN27-84 ends*/
			JSONObject jObj = new JSONObject();
			String retData = "";
			try {
				/*Added by Padmavathi for TNJN27-84 starts*/
				if(prjType.equalsIgnoreCase("Public")&&Integer.parseInt(prjAdminCount)!=0){
					projectHandler.validateProjectUserRoles(Integer.parseInt(projectId), Integer.parseInt(prjAdminCount));
				}
				/*Added by Padmavathi for TNJN27-84 ends*/
				projectHandler.unmapUsersFromProject(Integer.parseInt(projectId),userList);
				jObj.put("status", "SUCCESS");
				jObj.put("message","The selected user(s) have been removed from this project except current user(if selected)");
				
			} catch (Exception e) {
				try {
					jObj.put("status", "FAILURE");
					jObj.put("message", e.getMessage());
				} catch (JSONException e1) {
					
					logger.error("Error ", e1);
				}
		
			} finally{
				retData = jObj.toString();
			}
			response.getWriter().write(retData);
		}else if(txn.equalsIgnoreCase("mapAutToProject")){
			String projectId=request.getParameter("projectId");
			String auList=request.getParameter("selectedAuts");
			JSONObject jObj = new JSONObject();
			String retData = "";
			try {
				projectHandler.mapAutsToProject(Integer.parseInt(projectId),auList);
				jObj.put("status", "SUCCESS");
				jObj.put("message","The selected AUT(S) mapped sucessfully");
			} catch (NumberFormatException e) {
				
				logger.error("Error ", e);
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			} catch (JSONException e) {
					try {
						jObj.put("status", "FAILURE");
						jObj.put("message", e.getMessage());
					} catch (JSONException e1) {
						logger.error("Error ", e1);
					}
			}finally{
				retData = jObj.toString();
			}
			response.getWriter().write(retData);
		} else if (txn.equalsIgnoreCase("unMapProjectAut")) {
			String projectId = request.getParameter("projectId");
			String autList = request.getParameter("autlist");
			JSONObject jObj = new JSONObject();
			String retData = "";
			try {
				new ProjectHandler().unmapAutsFromProject(Integer.parseInt(projectId), autList);
				jObj.put("status", "SUCCESS");
				jObj.put("message",
						"The selected AUT(S) unmapped from this project.");
			} catch (NumberFormatException e) {
				logger.error("Error ", e);
			} catch (DatabaseException e) {
				logger.error("Error ", e);
			} catch (JSONException e) {
				try {
					jObj.put("status", "FAILURE");
					jObj.put("message", e.getMessage());
				} catch (JSONException e1) {
					logger.error("Error ", e1);
				}
				
			}
          /* Added by Ramya for TENJINCG-831 STARTS*/
			catch (RequestValidationException e) {
				try {
					jObj.put("status", "FAILURE");
					jObj.put("message", e.getMessage());
				} catch (JSONException e1) {
					
					logger.error("Error ", e1);
				}
				
			} catch (SQLException e) {
				
				logger.error("Error ", e);
			} 
			/* Added by Ramya for TENJINCG-831 ENDS*/
			
			finally {
				retData = jObj.toString();
			}
			response.getWriter().write(retData);
		}else if(txn.equalsIgnoreCase("updateProjectAuts")){
			String j = request.getParameter("paramval");
			String retData = "";
			String autCallBack="autReload";
			int projectId=0;
			ProjectHelper ph=new ProjectHelper();
			JSONObject r1 = new JSONObject();
			try{
				JSONArray jArray = new JSONArray(j);
				ArrayList<Aut> autList=new ArrayList<Aut>();
				for (int i = 0; i < jArray.length(); i++) {
					JSONObject json = jArray.getJSONObject(i);
					Aut aut=new Aut();
					aut.setId(json.getInt("appId"));
					aut.setURL(json.getString("url"));
					/*commented by Priyanka for Tenj212-21 starts*/
					/*aut.setTestDataPath(json.getString("appPath"));*/
					/*commented by Priyanka for Tenj212-21 ends*/
					projectId=json.getInt("prjId");
					autList.add(aut);
					//ph.updateAutUrl(aut,projectId);	
				}
				new ProjectHandler().updateAutUrl(autList,projectId);
				r1.put("status","success");
				r1.put("autCallBack", autCallBack);
				r1.put("message","AUT(S) Updated Successfully");
				retData = r1.toString();
			}catch(Exception e){
				try {
					r1.put("status", "error");
					r1.put("message","Could not update Tenjin AUT");
					retData = r1.toString();
				} catch (JSONException e1) {

				}
			}
			response.getWriter().write(retData);
		}else if(txn.equalsIgnoreCase("UPDATE_TD_PATHS")){
			int projectId =Integer.parseInt(request.getParameter("projid")) ;
			
			String retData = "";
			String j = request.getParameter("paramval");
			JSONObject rJson = new JSONObject();
			try {
				try {

					JSONArray jArray = new JSONArray(j);
					ArrayList<ProjectTestDataPath> paths = new ArrayList<ProjectTestDataPath>();
					for (int i = 0; i < jArray.length(); i++) {
						JSONObject json = jArray.getJSONObject(i);
						ProjectTestDataPath p = new ProjectTestDataPath();
						p.setProjectId(projectId);
						p.setAppId(json.getInt("appid"));
						p.setFuncCode(json.getString("func"));
						p.setTestDataPath(json.getString("tdp"));
						paths.add(p);
					}

					/*new ProjectHandler().persistTestDataPathsForProject(projectId, paths);*/
					new ProjectHelper().persistTestDataPathsForProject(
							projectId, paths);
					/*Project newProject = new ProjectHelper()
					.hydrateProject(projectId);
					tjnSession = (TenjinSession) request
							.getSession().getAttribute("TJN_SESSION");
					tjnSession.setProject(newProject);*/
					/*request.getSession()
					.setAttribute("TJN_SESSION", tjnSession);*/
					/*
					 * 27-Aug-2015 Bug ID:#18 :Fixed by Mahesh to When we add
					 * the TestData path for the first time to take testdata
					 * path :Ends
					 */

					rJson.put("status", "SUCCESS");
					rJson.put("message", "Test Data paths updated successfully");
				} catch (JSONException e) {
					
					logger.error("Error updating Test Data Paths", e);
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occurred. Please contact your System Administrator");
				} catch (NumberFormatException e) {
					
					logger.error("Error updating Test Data Paths", e);
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occurred. Please contact your System Administrator");
				} catch (DatabaseException e) {
					
					logger.error("Error updating Test Data Paths", e);
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occurred. Please contact your System Administrator");
				} finally {
					retData = rJson.toString();
				}
			} catch (Exception e) {
				logger.error("Error forming JSON for Test data paths update", e);
				retData = "An internal error occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);
		}
		/*Added by Pushpalatha for TJN27-5 starts*/
		else if(txn.equalsIgnoreCase("UPDATE_TDP_FOR_APP")){
			int projectId =Integer.parseInt(request.getParameter("projid")) ;
			int appId=Integer.parseInt(request.getParameter("application"));
			String groupName=request.getParameter("group");
			String path=request.getParameter("path");
			String retData = "";
			JSONObject rJson = new JSONObject();
			ArrayList<ProjectTestDataPath> paths = new ArrayList<ProjectTestDataPath>();
			try {
				try {
					/*Modified by Roshni TENJINCG-1168 starts */
					/*List<Module> module=new ModuleHelper().hydrateModules(appId,groupName);*/
					List<Module> module=new ModuleHandler().getModules(appId,groupName);
					/*Modified by Roshni TENJINCG-1168 ends */
					for (int i = 0; i < module.size(); i++) {
						ProjectTestDataPath p = new ProjectTestDataPath();
						p.setProjectId(projectId);
						p.setAppId(appId);
						p.setFuncCode(module.get(i).getCode());
						p.setTestDataPath(path);
						paths.add(p);
					}
					new ProjectHelper().persistTestDataPathsForProject(
							projectId, paths);
					//new ProjectHandler().updateTDPForApp(projectId,appId,path);
					rJson.put("status", "SUCCESS");
					rJson.put("message", "Test Data paths updated successfully");
				} catch (JSONException e) {
					
					logger.error("Error updating Test Data Paths", e);
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occurred. Please contact your System Administrator");
				} catch (NumberFormatException e) {
					
					logger.error("Error updating Test Data Paths", e);
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occurred. Please contact your System Administrator");
				} catch (DatabaseException e) {
					
					logger.error("Error updating Test Data Paths", e);
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occurred. Please contact your System Administrator");
				} finally {
					retData = rJson.toString();
				}
			} catch (Exception e) {
				logger.error("Error forming JSON for Test data paths update", e);
				retData = "An internal error occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);
		}
		/*Added by Pushpalatha for TJN27-5 ends*/
	}
	private Project mapAttributes(HttpServletRequest request) {
		
		Project project=new Project();
		
		project.setName(request.getParameter("name"));
		project.setDescription(request.getParameter("description"));
		project.setType(request.getParameter("type"));
		/*added by shruthi for TENJINCG-1255 starts*/
		project.setRepoType(request.getParameter("repotype"));
		if(request.getParameter("repotype").equalsIgnoreCase("aws"))
		{
			project.setRepoType("AWS S3");
			project.setRepoRoot(request.getParameter("bucket"));
		}
		else
		{
			project.setRepoType("Native");
			project.setRepoRoot(request.getParameter("rootfolder"));
		}
		/*added by shruthi for TENJINCG-1255 ends*/
		if(request.getParameter("owner").equals("-1")){
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			project.setOwner(tjnSession.getUser().getId());
		}else{
			project.setOwner(request.getParameter("owner"));
		}
		/*Added by Pushpalatha for tENJINCG-980,981 starts*/
		project.setProjectOwnerEmail(request.getParameter("email"));
		  String startDate=request.getParameter("sdate");  
		  String endDate=request.getParameter("edate");
		    Timestamp ts=null;
		   Date d=new Date(startDate);
		    if(d!=null){
		        ts=new java.sql.Timestamp(d.getTime());
		    }
		    project.setStartDate(ts);
		    d=new Date(endDate);
		    if(d!=null){
		        ts=new java.sql.Timestamp(d.getTime()); 
		    }
		
		project.setEndDate(ts);
		/*Added by Pushpalatha for tENJINCG-980,981 ends*/
		return project;
		
	}

}
