/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DomainServletNew.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 15-10-2018		   	Pushpalatha			  	Newly Added 
* 22-06-2019            Padmavathi              V2.8-37 
* 04-12-2019			Preeti					TENJINCG-1174
* 29-05-2020            Leelaprasad             TJN210-30
*/
package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.handler.DomainHandler;
import com.ycs.tenjin.project.Domain;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class DomainServletNew
 */
@WebServlet("/DomainServletNew")
public class DomainServletNew extends HttpServlet {
	
	public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		@Override
		public boolean checkAccess(HttpServletRequest request) {
			
			if (request.getMethod().equalsIgnoreCase("GET")) {
				String txn = request.getParameter("param");
				if(txn.equalsIgnoreCase("domain_new")) {
					if (new AdminAuthorizationRuleImpl().checkAccess(request)) {
						return true;
					}
				}else if(txn.equalsIgnoreCase("domain_view")) {
					if (new AdminAuthorizationRuleImpl().checkAccess(request))
					return true;
				}
			} else if (request.getMethod().equalsIgnoreCase("POST")) {
				if (new AdminAuthorizationRuleImpl().checkAccess(request)) {
					return true;
				}
			}
			return false;
		}
		
	};
	private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(DomainServletNew.class);  
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DomainServletNew() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.debug("Entred doGet()");
		String txn = request.getParameter("param");
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		User user=tjnSession.getUser();
		SessionUtils.loadScreenStateToRequest(request);
		Authorizer authorizer=new Authorizer();
		if(!authorizer.hasAccess(request)){
			response.sendRedirect("noAccess.jsp");
			return;
		}
		
		if(txn.equalsIgnoreCase("domain_new")){
			String domainName=request.getParameter("name");
			request.setAttribute("callback", domainName);
			request.getRequestDispatcher("v2/domain_new.jsp").forward(request, response);
		
		}else if(txn.equalsIgnoreCase("domain_view")){
			
			String domainName = request.getParameter("paramval");
			String folderType = request.getParameter("type");
			
			
			if(domainName.endsWith("_public")){
				domainName = domainName.replace("_public","");
			}
			if(domainName.endsWith("_private")){
				domainName = domainName.replace("_private", "");
			}
			
			if(folderType != null && (folderType.equalsIgnoreCase("public") || folderType.equalsIgnoreCase("Public"))){				
				folderType = "Public";
			}
			else if(folderType != null && (folderType.equalsIgnoreCase("private") || folderType.equalsIgnoreCase("Private"))){
				folderType = "Private";
			}
			else{
				folderType = "";
			}
			
			String userId = user.getId();
			try{
				DomainHandler domainHandler=new DomainHandler();
				Domain domain = new Domain();
				domain = domainHandler.fetchDomain(domainName);
				
				ArrayList<Project> projects = new ArrayList<Project>();
				if(folderType != null && !folderType.equalsIgnoreCase("")){
						if(folderType.equalsIgnoreCase("public") && !user.getRoles().equalsIgnoreCase("tester"))
							projects = domainHandler.getProjectsForDomain(domainName, folderType);
						//public and tester
						if(!folderType.equalsIgnoreCase("public"))
							projects = domainHandler.getProjectsForDomain(domainName, folderType);
					}else{
					if(user.getRoles().equalsIgnoreCase("tester"))
						projects = domainHandler.getProjectsForDomainForCurrentUser(domainName, userId);
					else
						projects = domainHandler.getProjectsForDomain(domainName);
					}
				if(folderType != null){				
					request.setAttribute("PROJTYPE", folderType);
				}
				domain.setProjects(projects);
				request.setAttribute("current_user", user);
				request.setAttribute("domain", domain);
				
				request.setAttribute("projects", domain.getProjects());
			}catch(DatabaseException e){
				request.setAttribute("domain", "");
			}
			/*Added by Padmavathi for V2.8-37 starts*/
			request.setAttribute("refresh_tree", "true");
			/*Added by Padmavathi for V2.8-37 ends*/
			RequestDispatcher dispatcher = request.getRequestDispatcher("v2/domain_view.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SessionUtils.loadScreenStateToRequest(request);
		TenjinSession tjnSession = SessionUtils.getTenjinSession(request);
		String domainName = request.getParameter("txtDomainName");
		
		Authorizer authorizer=new Authorizer();
		if(!authorizer.hasAccess(request)){
			response.sendRedirect("noAccess.jsp");
			return;
		}
		
		Domain domain = new Domain();
		try{
			
			DomainHandler domainHandler=new DomainHandler();
			/* Changed by leelaprasad for the issue TJN210-30 starts */
			/* Changed by leelaprasad for the issue TJN210-30 ends */
			domain.setName(Utilities.trim(domainName));
			domain.setOwner(tjnSession.getUser().getId());
			domain.setCreatedDate(new Timestamp(new Date().getTime()));
			domainHandler.persistDomain(domain);
			SessionUtils.setScreenState("success", "domain.create.success", domain.getName(), request);
			response.sendRedirect("DomainServletNew?param=domain_view&paramval=" + domain.getName());
		}catch(DuplicateRecordException e){
			SessionUtils.loadScreenStateToRequest(request);
			SessionUtils.setScreenState("error", "duplicate.domain.name", request);
			response.sendRedirect("DomainServletNew?param=domain_new&name="+domain.getName());
		} catch (DatabaseException e) {
			
			logger.error("Error ", e);
		}
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
