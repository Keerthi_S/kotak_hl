/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UIValidationServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 24-Jul-2017		Sriram Sridharan		Newly added for TENJINCG-283 Epic
 * 25-Jul-2017		Sriram Sridharan		TENJINCG-285
 * 25-Jul-2017		Sriram Sridharan		TENJINCG-309
 * 28-Jul-2017		Pushpalatha				TENJINCG-316
 * 23-Mar-2018      Padmavathi              TENJINCG-613(Rewritten)
 */

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.project.UIValidation;
import com.ycs.tenjin.bridge.pojo.project.UIValidationType;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.TestStepHandler;
import com.ycs.tenjin.handler.UIValidationHandler;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Utilities;


/**
 * Servlet implementation class UIValidationServlet
 */
public class UIValidationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(UIValidationServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UIValidationServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SessionUtils.loadScreenStateToRequest(request);
		logger.debug("Entered doGet()");
		
		UIValidationHandler uiValHandler=new UIValidationHandler();
		TestStepHandler stepHandler=new TestStepHandler();
		String txn = Utilities.trim( request.getParameter("param"));
		if(txn.equalsIgnoreCase("tstep_ui_vals_list")) {
			String tstepRecId = request.getParameter("srecid");
		 
			try {
				List<UIValidation> vals =uiValHandler.getUIValidationsForStep(Integer.parseInt(tstepRecId));
				TestStep step = stepHandler.getTestStep(Integer.parseInt(tstepRecId));
				request.setAttribute("vals", vals);
				request.setAttribute("step", step);
			} catch (NumberFormatException e) {
				logger.error("ERROR while fetchig UI Validations for step", e);
				throw new ServletException("Could not fetch UI Validations for step. Please contact Tenjin Support.");
			} catch (DatabaseException e) {
				throw new ServletException(e.getMessage());
			}
			RequestDispatcher disp = request.getRequestDispatcher("v2/ui_val_list.jsp");
			request.setAttribute("stepRecId", tstepRecId);
			disp.forward(request, response);
		}
		else if(txn.equalsIgnoreCase("new_ui_val")) {
			String tstepRecId = request.getParameter("srecid");
			request.setAttribute("stepRecId", tstepRecId);
			try {
				TestStep step = stepHandler.getTestStep(Integer.parseInt(tstepRecId));
				request.setAttribute("testStep", step);
				request.getRequestDispatcher("v2/ui_val_new.jsp").forward(request,response);
				
			} catch (NumberFormatException e) {
				logger.error("ERROR hydrating test step", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR hydrating test step", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
			
		}
		
		else if(txn.equalsIgnoreCase("ajax_fetch_all_val_types")) {
			String ret = "";
			
			JsonObject retJson = new JsonObject();
			try {
				List<UIValidationType> valTypes = uiValHandler.getAllUIValidationTypes();
				JsonParser jParser = new JsonParser();
				JsonElement jElement = jParser.parse(new Gson().toJson(valTypes));
				JsonArray valTypesArray =  jElement.getAsJsonArray();
				retJson.addProperty("status", "success");
				retJson.add("valtypes", valTypesArray);
			}catch(DatabaseException e) {
				retJson.addProperty("status", "error");
				retJson.addProperty("message", "Could not retrieve list of Validation Types. Please contact Tenjin Support.");
			}catch(Exception e) {
				logger.error("ERROR while fetching all UI Validation types", e);
				retJson.addProperty("status", "error");
				retJson.addProperty("message", "Could not retrieve list of Validation Types. Please contact Tenjin Support.");
			} finally {
				ret = retJson.toString();
				System.err.println(ret);
			}
			
			response.getWriter().write(ret);
		}
		
		else if(txn.equalsIgnoreCase("ajax_fetch_locations")) {
			String ret = "";
			int appId = Integer.parseInt(request.getParameter("app"));
			String functionCode = request.getParameter("func");
			JsonObject retJson = new JsonObject();
			try {
				List<Location> metadata = uiValHandler.hydrateMetadata(appId, functionCode, false, false);
				String mJsonArray = new Gson().toJson(metadata);
				
				JsonParser parser = new JsonParser();
				JsonElement jElement = parser.parse(mJsonArray);
				retJson.addProperty("status", "success");
				retJson.add("locations", jElement.getAsJsonArray());
			} catch (DatabaseException e) {
				
				logger.error("ERROR fetching page areas", e);
				retJson.addProperty("status", "error");
				retJson.addProperty("message", "Could not fetch page areas due to an internal error. Please contact Tenjin Support.");
			} finally {
				ret = retJson.toString();
				response.getWriter().write(ret);
			}
		}
		
		else if(txn.equalsIgnoreCase("ajax_fetch_fields")) {
			String ret = "";
			int appId = Integer.parseInt(request.getParameter("app"));
			String functionCode = request.getParameter("func");
			String locationName = request.getParameter("loc");
			JsonObject retJson = new JsonObject();
			try {
				List<TestObject> metadata = uiValHandler.hydrateTestObjectsOnPage(functionCode, appId, locationName);
				String mJsonArray = new Gson().toJson(metadata);
				
				JsonParser parser = new JsonParser();
				JsonElement jElement = parser.parse(mJsonArray);
				retJson.addProperty("status", "success");
				retJson.add("locations", jElement.getAsJsonArray());
			} catch (DatabaseException e) {
				
				logger.error("ERROR fetching page areas", e);
				retJson.addProperty("status", "error");
				retJson.addProperty("message", "Could not fetch fields due to an internal error. Please contact Tenjin Support.");
			} finally {
				ret = retJson.toString();
				response.getWriter().write(ret);
			}
		}
		
		else if(txn.equalsIgnoreCase("view_ui_val_step")) {
			String vId = request.getParameter("valrecid");
			String message = request.getParameter("message");
			try {
				UIValidation val = uiValHandler.getUIValidationStep(Integer.parseInt(vId));
				TestStep step =stepHandler.getTestStep(val.getStepRecordId());
				List<TestObject> fields = uiValHandler.hydrateTestObjectsOnPage(step.getModuleCode(), step.getAppId(), val.getPageArea());
				request.setAttribute("val", val);
				request.setAttribute("testStep", step);
				request.setAttribute("fields", fields);
				SessionUtils.setScreenState("success", message,vId, request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("v2/ui_val_view.jsp").forward(request, response);
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid Number", e);
				throw new ServletException("Could not fetch UI Validation Record. Please contact Tenjin Support.");
			} catch (DatabaseException e) {
				throw new ServletException("Could not fetch UI Validation Record. Please contact Tenjin Support.", e);
			}
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		String action = Utilities.trim(request.getParameter("action"));
		String delFlag = Utilities.trim(request.getParameter("del"));
		if(delFlag.equalsIgnoreCase("true")) {
			this.doDelete(request, response);
		}
		else{
			if("create".equalsIgnoreCase(action)) {
				JsonObject retJson = new JsonObject();
				try {
					int valRecId =new UIValidationHandler().createUIValidationRecord(request);
					retJson.addProperty("status", "success");
					retJson.addProperty("recid", valRecId);
					retJson.addProperty("message", "ui.step.create.success");
					SessionUtils.loadScreenStateToRequest(request);
				} catch (TenjinServletException e) {
					retJson.addProperty("status", "error");
					retJson.addProperty("message", e.getMessage());
				} finally {
					response.getWriter().write(retJson.toString());
				}
			
			}else if("update".equalsIgnoreCase(action)) {
				JsonObject retJson = new JsonObject();
				try {
					new UIValidationHandler().updateValidationRecord(request);
					retJson.addProperty("status", "success");
				} catch (TenjinServletException e) {
					retJson.addProperty("status", "error");
					retJson.addProperty("message", e.getMessage());
				} finally {
					response.getWriter().write(retJson.toString());
				}	
			}
		}
	}
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] uivalRecIds = request.getParameterValues("selectedUiValCodes");
		    String stepRecId = request.getParameter("stepRecId");
		
		try {
			new UIValidationHandler().deleteUIValidationStep(uivalRecIds);
			if(uivalRecIds.length>1) {
				SessionUtils.setScreenState("success", "ui.step.delete.multiple.success", request);
			}else {
				SessionUtils.setScreenState("success", "ui.step.delete.success",Arrays.toString(uivalRecIds),request);
			}
			response.sendRedirect("UIValidationServlet?param=tstep_ui_vals_list&srecid="+stepRecId);
		} catch (NumberFormatException e) {
			logger.error("ERROR - Invalid ui val record ID {}", uivalRecIds, e);
			SessionUtils.setScreenState("error", "generic.error", request);
			response.sendRedirect("UIValidationServlet?param=tstep_ui_vals_list&srecid="+stepRecId);
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			SessionUtils.setScreenState("error", "generic.db.error", request);
			response.sendRedirect("UIValidationServlet?param=tstep_ui_vals_list&srecid="+stepRecId);
		}
	}
	
}
