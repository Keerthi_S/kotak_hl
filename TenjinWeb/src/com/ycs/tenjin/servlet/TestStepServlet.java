
/**Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: TestStepServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 

-->

<!--

*//*****************************************
* CHANGE HISTORY
* ==============
*
 * DATE                 CHANGED BY              DESCRIPTION
 * 05-03-2018           Padmavathi              Newly added for TENJINCG-612
 * 20-03-2018           Padmavathi              for TENJINCG-612
 * 22-03-2018           Padmavathi              for TENJINCG-612
 * 09-05-2018           Padmavathi              TENJINCG-645
 * 14-05-2018           Padmavathi              TENJINCG-645
 * 16-05-2018           Padmavathi              TENJINCG-645
 * 19-06-2018			Preeti					T251IT-67
 * 25-09-2018           Padmavathi              TENJINCG-737
 * 12-10-2018           Padmavathi              TENJINCG-847
 * 24-10-2018           Padmavathi              TENJINCG-847
 * 20-12-2018           Padmavathi              TENJINCG-911
 * 19-02-2019			Preeti					TENJINCG-969
 * 28-02-2019           Padmavathi              TENJINCG-942
 * 11-03-2019           Padmavathi              TENJINCG-997
 * 20-03-2019			Preeti					TENJINCG-1020
 * 29-03-2019			Preeti					TENJINCG-1003
 * 10-04-2019			Ashiki					TENJINCG-1029
 * 02-05-2019			Roshni					TENJINCG-1046
 * 03-05-2019           Padmavathi              TENJINCG-1050
 * 29-05-2020			Ashiki					Tenj210-5
   19-11-2020           Priyanka                TENJINCG-1231
 * 15-06-2021			Ashiki					TENJINCG-1275 
*/




package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.ApiLearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.DependencyRulesHandler;
import com.ycs.tenjin.handler.ModuleHandler;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.handler.ProjectMailHandler;
import com.ycs.tenjin.handler.TenjinMailHandler;
import com.ycs.tenjin.handler.TestCaseHandler;
import com.ycs.tenjin.handler.TestStepHandler;
import com.ycs.tenjin.handler.UIValidationHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class TestStepServlet
 */
@WebServlet("/TestStepServlet")
public class TestStepServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(TestStepServlet.class); 
	/*Added by Padmavathi for TENJINCG-737 starts*/
	private String callback=null; 
	int setId=0;
	/*Added by Padmavathi for TENJINCG-737 ends*/
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestStepServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		
		Project project=tjnSession.getProject();

		SessionUtils.loadScreenStateToRequest(request);
		TestStepHandler handler=new TestStepHandler();
		String task = Utilities.trim(request.getParameter("t"));
		String mode=Utilities.trim(request.getParameter("mode"));
		String tcRecId=Utilities.trim(request.getParameter("tcRecId"));
		
		/*Added by Priyanka for TENJINCG-1231 starts*/
		int projectId=project.getId();
		try {
			project=new ProjectHandler().hydrateProject(projectId);
		} catch (DatabaseException e3) {
			
			e3.printStackTrace();
		}
		/*Added by Priyanka for TENJINCG-1231 ends*/
		if(task.equalsIgnoreCase("view")) {
			TestStep teststep=null;
			try {
				int tstepRecId = Integer.parseInt(request.getParameter("key"));
				/*Added by Padmavathi for TENJINCG-737 starts*/
				String callbackParam=request.getParameter("callback");
				if(callbackParam!=null && callbackParam.equalsIgnoreCase("testset")){
					callback=request.getParameter("callback");
					if(request.getParameter("setId")!=null){
						setId=Integer.parseInt(request.getParameter("setId"));
					}
				}else if(callbackParam!=null && callbackParam.equalsIgnoreCase("testcase")){
					callback="testcase";
				}
				/*Added by Padmavathi for TENJINCG-737 ends*/
				teststep=handler.getTestStep(tstepRecId);
				/*Modified by Padmavathi for TENJINCG-997 starts*/
				/*Added by Padmavathi for TENJINCG-645 starts*/
				/*Added by Padmavathi for TENJINCG-645 ends*/
				/*Modified by Padmavathi for TENJINCG-997 ends*/
				request.setAttribute("teststep", teststep);
				/*Added by Padmavathi for TENJINCG-737 starts*/
				request.setAttribute("callback", callback);
				request.setAttribute("setId", setId);
				/*Added by Padmavathi for TENJINCG-737 ends*/
				
				/* Added by Priyanka for TENJINCG-1231 starts*/
              SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				
				if(project.getEndDate()==null){
					request.setAttribute("edate","NA");
				}else{
				String edate=sdf.format(project.getEndDate());
				
				request.setAttribute("edate",edate);
				}
				/* Added by Priyanka for TENJINCG-1231 ends*/
				request.getRequestDispatcher("v2/teststep_view.jsp").forward(request, response);
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid Step ID [{}]", request.getParameter(teststep.getId()));
				request.setAttribute("teststep", teststep);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching Teststep information", e);
				request.setAttribute("teststep", teststep);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
			
		}
		else if(task.equalsIgnoreCase("new")){
			TestStep testStep=new TestStep();
			testStep.setTxnMode(mode);
			String stepId=request.getParameter("stepId");
			String summary=request.getParameter("summary");
			try{
			if(!stepId.equals(null)&& !stepId.equals("")){
				testStep.setId(request.getParameter("stepId"));
			}}catch(NullPointerException e){
				testStep.setId("");
			}
			try{
			if(!summary.equals(null)&& !summary.equals("")){
				testStep.setShortDescription(request.getParameter("summary"));
			}}catch(NullPointerException e){
				testStep.setShortDescription("");
			}
			/*Added by shruthi for TENJINCG-1177 starts*/
			if(mode.equals("API")){
				testStep.setTxnMode("API");
			}
			else if (mode.equals("GUI")){
				testStep.setTxnMode("GUI");
			}else {/*Added by Ashiki for TENJINCG-1275 starts*/
				testStep.setTxnMode("MSG");
				/*Added by Ashiki for TENJINCG-1275 end*/
			}
			/*Added by shruthi for TENJINCG-1177 ends*/
			
			String presetRule=request.getParameter("presetRule");
			String totalSteps=request.getParameter("totalSteps");
			
			String tcId = request.getParameter("tcId");
			
			if(tcRecId!=null){
				testStep.setTestCaseRecordId(Integer.parseInt( tcRecId));
				request.setAttribute("tcRecId", tcRecId);
			}
			
			request.setAttribute("totalSteps", totalSteps);
			request.setAttribute("presetRule1", presetRule);
			
			if(request.getParameter("aut")!=null)
			testStep.setAppId(Integer.parseInt(request.getParameter("aut")));
			
			testStep.setCustomRules(presetRule);
			
			request.setAttribute("teststep", testStep);
			
			request.setAttribute("aut", request.getParameter("aut"));
			request.setAttribute("tcId", tcId);
		    request.getRequestDispatcher("v2/teststep_new.jsp").forward(request, response);
				
		}
		else if(task.equalsIgnoreCase("reorder_steps")) {
			String testCaseRecId = request.getParameter("tcrecid");
			String jsonString = request.getParameter("order");
			
			Map<Integer, Integer> sequenceMap = new HashMap<Integer, Integer>();
			String returnMessage = "";
			JSONObject returnJson = new JSONObject();
			try {
				JSONArray jsonArray = new JSONArray(jsonString);
				for(int i=0; i<jsonArray.length(); i++) {
					JSONObject json = jsonArray.getJSONObject(i);
					sequenceMap.put(json.getInt("stepRecId"), json.getInt("sequence"));
				}
				try {
					handler.reOrderTestCases(Integer.parseInt(testCaseRecId), sequenceMap);
					returnJson.put("status", "success");
				} catch(Exception e) {
					logger.error("ERROR - Could not re-order steps", e);
					returnJson.put("status", "error");
					returnJson.put("message", "Could not re-order steps. Please contact Tenjin Support");
				}
				
				returnMessage = returnJson.toString();
				
			} catch (JSONException e) {
				logger.error("ERROR generating sequence map",e );
			}
			
			response.getWriter().write(returnMessage);
			/*Added by Padmavathi for TENJINCG-645 starts*/
		}else if(task.equalsIgnoreCase("dependecny_reorder_steps")) {
			String testCaseRecId = request.getParameter("tcrecid");
			String jsonString = request.getParameter("order");
			String jsonPrevString = request.getParameter("oldOrder");
			String dependencyJsonstr = request.getParameter("dependencyJson");
			String preserveRules=null;
			String selectedStep=null;
			String ruleType=null;
			Map<Integer, Integer> sequenceMap = new HashMap<Integer, Integer>();
			/*Added by Padmavathi for TENJINCG-997 starts*/
			Map<Integer, Integer> dependencyIdsMap = new HashMap<Integer, Integer>();
			/*Added by Padmavathi for TENJINCG-997 ends*/
			Map<Integer, Integer> oldSequenceMap = new HashMap<Integer, Integer>();
			String returnMessage = "";
			JSONObject returnJson = new JSONObject();
			try {
				
				JSONObject dependencyJson=new JSONObject(dependencyJsonstr);
				String execStatus =null;
				if(dependencyJson.getString("presetRules").equalsIgnoreCase("Y")){
					execStatus=dependencyJson.getString("execStatus");
				}
				preserveRules=dependencyJson.getString("preserveRules");
				/*Added by Padmavathi for TENJINCG-997 starts*/
				ruleType=dependencyJson.getString("ruleType");
				/*Added by Padmavathi for TENJINCG-997 ends*/
				selectedStep=dependencyJson.getString("selectedStep");
				
				JSONArray jsonArray = new JSONArray(jsonString);
				for(int i=0; i<jsonArray.length(); i++) {
					JSONObject json = jsonArray.getJSONObject(i);
					sequenceMap.put(json.getInt("stepRecId"), json.getInt("sequence"));
					/*Added by Padmavathi for TENJINCG-997 starts*/
					dependencyIdsMap.put(json.getInt("stepRecId"), json.getInt("dependentStepRecId"));
					/*Added by Padmavathi for TENJINCG-997 ends*/
				}
				JSONArray prevJsonArray = new JSONArray(jsonPrevString);
				for(int i=0; i<prevJsonArray.length(); i++) {
					JSONObject json = prevJsonArray.getJSONObject(i);
					oldSequenceMap.put(json.getInt("stepRecId"), json.getInt("oldSeq"));
					
				}
				try {
					handler.reOrderTestCases(Integer.parseInt(testCaseRecId), sequenceMap);
					if(ruleType!=null&& ruleType.equalsIgnoreCase("TestCase") ){
						new DependencyRulesHandler().UpdateDependencyRules(Integer.parseInt(testCaseRecId), "TestCase", execStatus);
					}else{
						if(preserveRules.equalsIgnoreCase("No"))
							new DependencyRulesHandler().reOrderStepRule(Integer.parseInt(testCaseRecId), oldSequenceMap,sequenceMap,Integer.parseInt(selectedStep),dependencyIdsMap);
						else
							new DependencyRulesHandler().reOrderStepRule(Integer.parseInt(testCaseRecId), oldSequenceMap,sequenceMap,0,dependencyIdsMap);
						
					}
					returnJson.put("status", "success");
				} catch(Exception e) {
					logger.error("ERROR - Could not re-order steps", e);
					returnJson.put("status", "error");
					returnJson.put("message", "Could not re-order steps. Please contact Tenjin Support");
				}
				
				returnMessage = returnJson.toString();
				
			} catch (JSONException e) {
				logger.error("ERROR generating sequence map",e );
			}
			
			response.getWriter().write(returnMessage);
		}else if(task.equalsIgnoreCase("learnCheck")){
			String returnMessage = "";
			String functionCode=request.getParameter("functionCode");
			int appId=Integer.parseInt(request.getParameter("appId"));
			String txnmode=request.getParameter("mode");
			JSONObject returnJson = new JSONObject();
			List<LearnerResultBean> lrnrRuns;
			ArrayList<ApiLearnerResultBean>  lrnrRunsApi;
			try {
				if(txnmode.equalsIgnoreCase("GUI")){
					lrnrRuns = new ModuleHandler().getLearningHistory(appId,functionCode);
					if(lrnrRuns.size()==0 && (!txnmode.equalsIgnoreCase("API"))){
						try{
							returnJson.put("status", "WARN");
							returnJson.put("message", "Function selected for the test step was not learnt, execution of this step may result in error.");
						}catch(Exception e) {
				         logger.error(e.getMessage());
						}	
					}/*Added by Ashiki for TENJINCG-1275 starts*/
				}else if(txnmode.equalsIgnoreCase("MSG")){
					lrnrRuns = new ModuleHandler().getLearningHistory(appId,functionCode);
					if(lrnrRuns.size()==0 && (!txnmode.equalsIgnoreCase("API"))){
						try{
							returnJson.put("status", "WARN");
							returnJson.put("message", "Function selected for the test step was not learnt, execution of this step may result in error.");
						}catch(Exception e) {
				         logger.error(e.getMessage());
						}	
					}
				}/*Added by Ashiki for TENJINCG-1275 ends*/
				else{
					lrnrRunsApi=handler.getLearningHistoryforApi(appId, functionCode);
					if(lrnrRunsApi.size()==0)
					try{
						returnJson.put("status", "WARN");
						returnJson.put("message", "API selected for the test step was not learnt, execution of this step may result in error.");
					}catch(Exception e) {
						 logger.error(e.getMessage());
					}	
				}
				returnMessage = returnJson.toString();
			} catch (DatabaseException e) {
				logger.error("error while checking learn status",e);
			}
			response.getWriter().write(returnMessage);
		}else if(task.equalsIgnoreCase("manualMapping")){
			Map<String,Object> map=new HashMap<String,Object>();
			TestStep testStep=new TestStep();
			if(tcRecId!=null){
				testStep.setTestCaseRecordId(Integer.parseInt( tcRecId));
			}
			try {
				testStep=handler.getTestStep(Integer.parseInt( tcRecId));
				map.put("TEST_STEP", testStep);
				request.getSession().setAttribute("SCR_MAP", map);
			    request.getRequestDispatcher("manualMapping.jsp").forward(request, response);
			} catch (NumberFormatException | DatabaseException e) {
				logger.error(e.getMessage(),e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
			
		}else if(task.equalsIgnoreCase("dependencyRules")){
			String returnMessage = "";
			String testCaseRecId=request.getParameter("testCaseRecId");
			JSONObject returnJson = new JSONObject();
			try {
				String ruleType=new DependencyRulesHandler().checkDependencyRules(Integer.parseInt(testCaseRecId));
				returnJson.put("status", "success");
				returnJson.put("ruleType",ruleType);
				returnMessage = returnJson.toString();
			} catch (DatabaseException e) {
				logger.error("error while checking dependency Rules",e);
			} catch (JSONException e) {
				logger.error(e.getMessage());
			}
			response.getWriter().write(returnMessage);
			/*Added by Padmavathi for TENJINCG-645 ends*/
		}
		/*Added by Padmavathi for TENJINCG-847 Starts*/
		else if(task.equalsIgnoreCase("copyTestStep")){
			TestStep testStep=null;
			String stepRecId=request.getParameter("stepRecId");
			String prjId=request.getParameter("prjId");
			List<TestCase> testCases=null;
			try {
				testStep=handler.getTestStep(Integer.parseInt(stepRecId));
				request.setAttribute("sourceStepId", testStep.getId());
				testCases=new TestCaseHandler().getAllTestCases1(Integer.parseInt(prjId));
				request.setAttribute("testCases", testCases);
				request.setAttribute("stepCopy", "true");
			} catch (NumberFormatException | DatabaseException e) {
				logger.error("ERROR fetching Teststep information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
			request.setAttribute("teststep", testStep);
			request.getRequestDispatcher("v2/teststep_new.jsp").forward(request, response);
			
		}
		/*Added by Padmavathi for TENJINCG-847 ends*/
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		SessionUtils.loadScreenStateToRequest(request);
		String type = Utilities.trim(request.getParameter("transactionType"));
		TestStepHandler handler=new TestStepHandler();
		if(type.equalsIgnoreCase("true")) {
			this.doDelete(request, response);
		}
		else{
			int projectId= Integer.parseInt(request.getParameter("prjId"));
			TestStep testStep=null;
			if(type.equalsIgnoreCase("update")) {
				try {
					testStep=this.mapAttributes(request);
					/*Added by Preeti for TENJINCG-969 starts*/
					AuditRecord audit = new AuditRecord();
					audit.setEntityRecordId(testStep.getRecordId());
					audit.setLastUpdatedBy(tjnSession.getUser().getId());
					audit.setEntityType("teststep");
					testStep.setAuditRecord(audit);
					/*Added by Preeti for TENJINCG-969 ends*/
					handler.update(testStep,projectId);
					/* Added by Roshni for TENJINCG-1046 starts */
					User tsOwner=new UserHandler().getUser(new TestCaseHandler().hydrateTestCase(projectId, testStep.getTestCaseRecordId()).getTcCreatedBy());
					
					TenjinMailHandler tmhandler=new TenjinMailHandler();
					/*Added by Padmavathi for TENJINCG-1050 starts*/
					if(!new ProjectMailHandler().isUserOptOutFromMail(projectId,"TestPlanEdit",tsOwner.getId())){
						/*Added by Padmavathi for TENJINCG-1050 ends*/
						JSONObject mailContent=tmhandler.getMailContent(String.valueOf(testStep.getRecordId()), testStep.getDataId(), "Test Step", tjnSession.getUser().getFullName(),
								"update", new Timestamp(new Date().getTime()), tsOwner.getEmail(), testStep.getTestCaseName());
						try {
							tmhandler.sendEmailNotification(mailContent);
						} catch (TenjinConfigurationException e) {
							logger.error("Error ", e);
						}
					}
					/* Added by Roshni for TENJINCG-1046 ends */
					SessionUtils.setScreenState("success", "Teststep.update.success", request);
					response.sendRedirect("TestStepServlet?t=view&key=" +testStep.getRecordId());
				} catch (DatabaseException e) {
					logger.error(e.getMessage(), e);
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					try {
						/*Modified by Preeti for T251IT-67 starts*/
						String desc=testStep.getDescription();
						String expResult=testStep.getExpectedResult();
						testStep=handler.getTestStep(testStep.getRecordId());
						testStep.setDescription(desc);
						testStep.setExpectedResult(expResult);
						/*Modified by Preeti for T251IT-67 ends*/
					} catch (DatabaseException e1) {}
					request.setAttribute("teststep", testStep);
					/*Added by Padmavathi for TENJINCG-737 starts*/
					request.setAttribute("callback", callback);
					request.setAttribute("setId", setId);
					/*Added by Padmavathi for TENJINCG-737 ends*/
					request.getRequestDispatcher("v2/teststep_view.jsp").forward(request, response);
				} catch (RequestValidationException e) {
					logger.error(e.getMessage(), e);
					SessionUtils.setScreenState("error", e.getMessage(),e.getTargetFields(), request);
					SessionUtils.loadScreenStateToRequest(request);
					try {
						/*Modified by Preeti for T251IT-67 starts*/
						String desc=testStep.getDescription();
						String expResult=testStep.getExpectedResult();
						testStep=handler.getTestStep(testStep.getRecordId());
						testStep.setDescription(desc);
						testStep.setExpectedResult(expResult);
						/*Modified by Preeti for T251IT-67 ends*/
					} catch (DatabaseException e1) {}
					request.setAttribute("teststep", testStep);
					/*Added by Padmavathi for TENJINCG-737 starts*/
					request.setAttribute("callback", callback);
					request.setAttribute("setId", setId);
					/*Added by Padmavathi for TENJINCG-737 ends*/
					request.getRequestDispatcher("v2/teststep_view.jsp").forward(request, response);
				}
			}else{
				 testStep=this.mapAttributes(request);
				 /*Added by Padmavathi for TENJINCG-847 Starts*/
				 String isCopystep=request.getParameter("stepCopy");
				 String isCopyChildEntity= request.getParameter("childEntityCopy");
				 String sourceStepId=request.getParameter("sourceStepId");
				 /*Added by Padmavathi for TENJINCG-847 ends*/
				 /*Added by Preeti for TENJINCG-969 starts*/
				 testStep.setCreatedBy(tjnSession.getUser().getId());
				 /*Added by Preeti for TENJINCG-969 ends*/
			try {
				try {
				  int sourceStepRecId=testStep.getRecordId();
				  int targetStepRecId=handler.persistTestStep(testStep,projectId);
				  /*Added by Padmavathi for TENJINCG-847 Starts*/
				  if(isCopystep!=null && isCopystep.equalsIgnoreCase("true") && testStep.getTxnMode().equalsIgnoreCase("GUI") && isCopyChildEntity.equalsIgnoreCase("true")){
					  String sourceTcRecId=request.getParameter("sourceTcRecTd");
					  if(testStep.getValidationType().equalsIgnoreCase("UI")){
						 new UIValidationHandler().copyUIValidationSteps(targetStepRecId,sourceStepRecId);
					 }
					 handler.copyManualFieldMap(Integer.parseInt(sourceTcRecId), sourceStepId,testStep);
					 }
					/*Added by Padmavathi for TENJINCG-847 ends*/
					SessionUtils.setScreenState("success", "teststep.create.success", request);
					response.sendRedirect("TestStepServlet?t=view&key=" +testStep.getRecordId());
				} catch (DatabaseException e) {
					logger.error(e.getMessage(), e); 
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("teststep", testStep);
					/*Added by Padmavathi for TENJINCG-847 Starts*/
					if(isCopystep!=null && isCopystep.equalsIgnoreCase("true")){
						List<TestCase> testCases=null;
						try {
							testCases=new TestCaseHandler().getAllTestCases(projectId,testStep.getTxnMode());
							request.setAttribute("testCases", testCases);
							request.setAttribute("stepCopy", "true");
							request.setAttribute("sourceStepId", sourceStepId);
						} catch (NumberFormatException | DatabaseException e1) {
							logger.error("ERROR fetching Teststep information", e1);
						}
					}
					/*Added by Padmavathi for TENJINCG-847 ends*/
					request.getRequestDispatcher("v2/teststep_new.jsp").forward(request, response);
				}
			} catch (RequestValidationException e) {
				logger.error(e.getMessage(), e);
				SessionUtils.setScreenState("error", e.getMessage(),e.getTargetFields(), request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("teststep", testStep);
				/*Added by Padmavathi for TENJINCG-847 Starts*/
				if(isCopystep!=null && isCopystep.equalsIgnoreCase("true")){
					List<TestCase> testCases=null;
					try {
						/*Added by Ashiki for Tenj210-5 starts*/
						/*testCases=new TestCaseHandler().getAllTestCases(projectId,testStep.getTxnMode());*/
						testCases=new TestCaseHandler().getAllTestCases1(projectId);
						/*Added by Ashiki for Tenj210-5 ends*/
						request.setAttribute("testCases", testCases);
						request.setAttribute("stepCopy", "true");
						request.setAttribute("sourceStepId", sourceStepId);
					} catch (NumberFormatException | DatabaseException e1) {
						logger.error("ERROR fetching Teststep information", e1);
					}
				}
				/*Added by Padmavathi for TENJINCG-847 ends*/
				request.getRequestDispatcher("v2/teststep_new.jsp").forward(request, response);
			}}
		}
		
	
		
	}
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] testStepRecIds = request.getParameterValues("TestStepRecId");
		int tcRecId= Integer.parseInt(request.getParameter("txtTcRecId"));
		int projectId= Integer.parseInt(request.getParameter("projectId"));
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		TestStepHandler handler = new TestStepHandler();
		/* Added by Roshni for TENJINCG-1046 starts */
		List<JSONObject> contents=new ArrayList<>();
		/* Added by Roshni for TENJINCG-1046 ends */
		try {
			/* Added by Roshni for TENJINCG-1046 starts */
			TestCase tcase=new TestCaseHandler().hydrateTestCase(projectId, tcRecId);
			User tcOwner=new UserHandler().getUser(tcase.getTcCreatedBy());
			TenjinMailHandler tmhandler=new TenjinMailHandler();
			
			for(String tstepId:testStepRecIds){
				
				JSONObject mailContent=new JSONObject();
				TestStep step=handler.getTestStep(tcRecId, Integer.parseInt(tstepId), projectId);
				/*Added by Padmavathi for TENJINCG-1050 starts*/
				if(!new ProjectMailHandler().isUserOptOutFromMail(projectId,"TestPlanRemove",tcOwner.getId())){
					/*Added by Padmavathi for TENJINCG-1050 ends*/
					mailContent=tmhandler.getMailContent(String.valueOf(Integer.parseInt(tstepId)), step.getId(), "Test Step", tjnSession.getUser().getFullName(),
							"delete", new Timestamp(new Date().getTime()), tcOwner.getEmail(), tcase.getTcId());
			
					contents.add(mailContent);
				}
			}
			/* Added by Roshni for TENJINCG-1046 ends */
			/*Modified by Preeti for TENJINCG-1003 starts*/
			/*handler.deleteTestStep(testStepRecIds,tcRecId,projectId );*/
			handler.deleteTestStep(testStepRecIds,tcRecId,projectId,tjnSession.getUser().getId());
			/* Added by Roshni for TENJINCG-1046 starts */
			for(JSONObject content:contents)
			{
				try {
					new TenjinMailHandler().sendEmailNotification(content);
				} catch (TenjinConfigurationException e) {
					logger.error("Error ", e);
				}
			}
			/* Added by Roshni for TENJINCG-1046 ends */
			/*Modified by Preeti for TENJINCG-1003 ends*/
			if(testStepRecIds.length>1) {
				SessionUtils.setScreenState("success", "testStep.delete.multiple.success", request);
			}else {
				SessionUtils.setScreenState("success", "testStep.delete.success",request);
			}
			response.sendRedirect("TestCaseServletNew?t=testCase_View&paramval="+tcRecId+"&pid="+projectId);
		} catch (NumberFormatException e) {
			logger.error("ERROR - Invalid Step ID {}", testStepRecIds, e);
			SessionUtils.setScreenState("error", "generic.error", request);
			response.sendRedirect("TestCaseServletNew?t=testCase_View&paramval="+tcRecId+"&pid="+projectId);
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			SessionUtils.setScreenState("error", "generic.db.error", request);
			response.sendRedirect("TestCaseServletNew?t=testCase_View&paramval="+tcRecId+"&pid="+projectId);
		} catch (RequestValidationException e) {
			logger.error(e.getMessage(),e);
			SessionUtils.setScreenState("error", e.getMessage(),e.getTargetFields(), request);
			response.sendRedirect("TestCaseServletNew?t=testCase_View&paramval="+tcRecId+"&pid="+projectId);
		}
	}

	private TestStep mapAttributes(HttpServletRequest request) {
 		TestStep testStep =new TestStep();
		if(request.getParameter("txtRecordId")!=null)
		testStep.setRecordId(Integer.parseInt(request.getParameter("txtRecordId")));
		testStep.setTestCaseRecordId(Integer.parseInt(request.getParameter("txnTcRecTd")));
		testStep.setId(Utilities.trim(request.getParameter("id")));
		testStep.setShortDescription(Utilities.trim(request.getParameter("shortDescription")));
		testStep.setValidationType(Utilities.trim(request.getParameter("validationType")));
		if(!request.getParameter("aut").equals("-1")&&request.getParameter("aut")!=null)
		testStep.setAppId(Integer.parseInt(request.getParameter("aut")));
		testStep.setModuleCode(Utilities.trim(request.getParameter("moduleCode")));
		testStep.setOperation(Utilities.trim(request.getParameter("operation")));
		testStep.setAutLoginType(Utilities.trim(request.getParameter("autLoginType")));
		if(request.getParameter("responseType")!=null&&!request.getParameter("responseType").equals(" "))
		testStep.setResponseType(Integer.parseInt(request.getParameter("responseType")));
		testStep.setType(Utilities.trim(request.getParameter("type")));
		testStep.setDataId(Utilities.trim(request.getParameter("dataId")));
		testStep.setTxnMode(Utilities.trim(request.getParameter("txnMode")));
		if(request.getParameter("rowsToExecute")!=null&&!request.getParameter("rowsToExecute").equals(""))
		testStep.setRowsToExecute(Integer.parseInt(Utilities.trim(request.getParameter("rowsToExecute"))));
		testStep.setDescription(Utilities.trim(request.getParameter("description")));
		testStep.setExpectedResult(Utilities.trim(request.getParameter("expectedResult")));
		/*Added by Ashiki for TENJINCG-1275 starts*/
		if(testStep.getTxnMode()!=null && testStep.getTxnMode().equalsIgnoreCase("MSG")){
		testStep.setActualResult(Utilities.trim(request.getParameter("msgFormat")));
		testStep.setAutLoginType("Maker");
		testStep.setFileName(Utilities.trim(request.getParameter("fileName")));
		testStep.setFilePath(Utilities.trim(request.getParameter("filePath")));
		/*Added by Ashiki for TENJINCG-1275 ends*/
	}  
		/*Modified by Padmavathi for TENJINCG-997 starts*/
		/*Added by Padmavathi for TENJINCG-645 starts*/
		testStep.setCustomRules(Utilities.trim(request.getParameter("dependencyRules")));
		/*Added by Padmavathi for TENJINCG-645 ends*/
		/*Modified by Padmavathi for TENJINCG-997 ends*/
		
		/*Added by Padmavathi for TENJINCG-911 starts*/
		if(Utilities.trim(request.getParameter("rerunStep")).equalsIgnoreCase("Y")){
			testStep.setRerunStep("Y");
		}else{
			testStep.setRerunStep("N");
		}
		/*Added by Padmavathi for TENJINCG-911 ends*/
		return testStep;
	}

}
