/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  FunctionServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                	 CHANGED BY                DESCRIPTION
 * 24-Oct-2017           Sriram Sridharan          Newly Added For TENJINCG-399
 * 08-11-2017            Padmavathi                TENJINCG-426
 * 13-11-2017			 Preeti					   TENJINCG-411 
 * 02-02-2018			 Pushpalatha			   TENJINCG-568
 * 02-11-2018			 Sriram SRidharan		   TENJINCG-894
 * 29-05-2019			 Ashiki					   V2.8-83
 * 14-06-2019            Padmavathi                V2.8-79
 * 09-10-2019			 Pushpalatha			   TJN252-7
 * 04-12-2019			 Preeti					   TENJINCG-1174
 */

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.aut.Group;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ExtractorResultBean;
import com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.handler.ModuleHandler;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Utilities;



/**
 * Servlet implementation class FunctionServlet
 */
@WebServlet("/FunctionServlet")
public class FunctionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(FunctionServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FunctionServlet() {
        super();
        
    }

    public static AuthorizationRule authorizationRule = new AuthorizationRule() {
        
        @Override
        public boolean checkAccess(HttpServletRequest request) {
        	if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
                    return true;
            }
            return false;
        }  
    };
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		ModuleHandler handler = new ModuleHandler();
		String task = Utilities.trim(request.getParameter("t"));
		Authorizer authorizer=new Authorizer();
		if(task.equalsIgnoreCase("") || task.equalsIgnoreCase("view") || task.equalsIgnoreCase("new") || task.equalsIgnoreCase("learningHistory") || task.equalsIgnoreCase("extractHistory") || task.equalsIgnoreCase("metadata") || task.equalsIgnoreCase("download_options") || task.equalsIgnoreCase("upload_function") || task.equalsIgnoreCase("download_function")  ) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		if(task.equalsIgnoreCase("view")) {
			try {
				String moduleCode = Utilities.trim(request.getParameter("key"));
				int appId = Integer.parseInt(request.getParameter("appId"));
				
				Aut aut = new AutHandler().getApplication(appId);
				Module module = handler.getModule(appId, moduleCode);
				request.setAttribute("module", module);
				request.setAttribute("aut", aut);
				request.getRequestDispatcher("v2/function_view.jsp").forward(request, response);
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid App ID [{}]", request.getParameter("appId"));
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching function information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
			
		}else if(task.equalsIgnoreCase("new")){
			try {
				int appId = Utilities.trim(request.getParameter("appId")).length() > 0 ? Integer.parseInt(request.getParameter("appId")) : 0;
				if(appId > 0) {
					Aut aut = new AutHandler().getApplication(appId);
					request.setAttribute("aut", aut);
				}
				request.getRequestDispatcher("v2/function_new.jsp").forward(request, response);
				
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid App ID [{}]", request.getParameter("appId"));
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching function information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}else if(task.equalsIgnoreCase("learningHistory")){
			try {
				int appId = Utilities.trim(request.getParameter("appId")).length() > 0 ? Integer.parseInt(request.getParameter("appId")) : 0;
				if(appId > 0) {
					Aut aut = new AutHandler().getApplication(appId);
					request.setAttribute("aut", aut);
				}else{
					throw new RecordNotFoundException("aut.not.found");
				}
				String moduleCode = Utilities.trim(request.getParameter("function"));
				ModuleHandler mHandler = new ModuleHandler();
				Module module = mHandler.getModule(appId, moduleCode);
				if(module == null) {
					throw new RecordNotFoundException("function.not.found");
				}
				
				List<LearnerResultBean> results = mHandler.getLearningHistory(appId, moduleCode);
				/* 	Added by Preeti for TENJINCG-411 starts */
				if(results.size()==0){
				SessionUtils.setScreenState("error", "function.not.learnt", request);
				SessionUtils.loadScreenStateToRequest(request);
				}
				/* 	Added by Preeti for TENJINCG-411 ends */
				request.setAttribute("module", module);
				request.setAttribute("results", results);
				request.getRequestDispatcher("v2/function_lrnr_history.jsp").forward(request, response);
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid App ID [{}]", request.getParameter("appId"));
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching learning history", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}else if(task.equalsIgnoreCase("extractHistory")){
			try {
				int appId = Utilities.trim(request.getParameter("appId")).length() > 0 ? Integer.parseInt(request.getParameter("appId")) : 0;
				if(appId > 0) {
					Aut aut = new AutHandler().getApplication(appId);
					request.setAttribute("aut", aut);
				}else{
					throw new RecordNotFoundException("aut.not.found");
				}
				String moduleCode = Utilities.trim(request.getParameter("function"));
				ModuleHandler mHandler = new ModuleHandler();
				Module module = mHandler.getModule(appId, moduleCode);
				if(module == null) {
					throw new RecordNotFoundException("function.not.found");
				}
				
				List<ExtractorResultBean> results = mHandler.getExtractionHistory(appId, moduleCode);
				/* 	Added by Preeti for TENJINCG-411 starts */
				if(results.size()==0){
					SessionUtils.setScreenState("error", "function.not.extracted", request);
					SessionUtils.loadScreenStateToRequest(request);
				}
				/* 	Added by Preeti for TENJINCG-411 ends */
				request.setAttribute("module", module);
				request.setAttribute("results", results);
				request.getRequestDispatcher("v2/function_extr_history.jsp").forward(request, response);
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid App ID [{}]", request.getParameter("appId"));
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching extraction history", request.getParameter("appId"));
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}
		//TENJINCG-894 (Sriram)
		else if(task.equalsIgnoreCase("metadata")) {
			int appId = 0;
			String functionCode = Utilities.trim(request.getParameter("func"));
			
			ModuleHandler mHandler = new ModuleHandler();
			AutHandler aHandler = new AutHandler();
			
			try {
				appId = Integer.parseInt(Utilities.trim(request.getParameter("app")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid App ID [{}]", request.getParameter("app"));
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}
			
			try {
				Aut aut = aHandler.getApplication(appId);
				if(aut == null) {
					logger.error("Error - AUT [{}] does not exist", appId);
					request.getRequestDispatcher("error.jsp").forward(request, response);
					return;
				}
				
				request.setAttribute("aut", aut);
			} catch (DatabaseException e) {
				logger.error(e.getMessage(),e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}
			try {
				Module module = mHandler.getModule(appId, functionCode);
				if(module == null) {
					logger.error("Module [{}] does not exist for app [{}]", functionCode, appId);
					request.getRequestDispatcher("error.jsp").forward(request, response);
					return;
				}
				
				request.setAttribute("module", module);
			} catch (DatabaseException e) {
				logger.error(e.getMessage(),e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}
			
			request.getRequestDispatcher("v2/metadata.jsp").forward(request, response);
		}
		//TENJINCG-894 (Sriram) ends
		else if(task.equalsIgnoreCase("upload_function")) {

			JSONObject returnJson = new JSONObject();
			String returnMessage = "";
			try {
				returnJson.put("status", "SUCCESS");
			} catch (JSONException e) {
				
				logger.error(e.getMessage(), e);
			}
			returnMessage = returnJson.toString();
			response.getWriter().write(returnMessage);
			
		}
		
		else if(task.equalsIgnoreCase("download_options")) {
			JSONObject returnJson = new JSONObject();
			String returnMessage = "";
			try {
				returnJson.put("status", "SUCCESS");
			} catch (JSONException e) {
				
				logger.error(e.getMessage(), e);
			}
			returnMessage = returnJson.toString();
			response.getWriter().write(returnMessage);
		}else if(task.equalsIgnoreCase("download_function")) {

			JSONObject returnJson = new JSONObject();
			String returnMessage = "";
			try {
				returnJson.put("status", "SUCCESS");
			} catch (JSONException e) {
				
				logger.error(e.getMessage(), e);
			}
			returnMessage = returnJson.toString();
			response.getWriter().write(returnMessage);
		
		}
		else {
			
			try {
				String a = Utilities.trim(request.getParameter("appId"));
				/* Added by Padmavathi for V2.8-79 starts */
				//Modified by Priyanka For File content message starts
				if(request.getParameter("msg")!=null && request.getParameter("msg")!=""&& request.getParameter("status").equalsIgnoreCase("SUCCESS"))
					//Modified by Priyanka For File content message ends
				{
					SessionUtils.setScreenState("success", request.getParameter("msg"), request);
					SessionUtils.loadScreenStateToRequest(request);
				}
				/*Added by Pushpalatha for TJN252-7 starts*/
				if(request.getParameter("status")!=null && request.getParameter("status").equalsIgnoreCase("ERROR"))
				{
					//Modified by Priyanka For File content message starts
					SessionUtils.setScreenState("error", request.getParameter("message"), request);
					//Modified by Priyanka For File content message ends
					SessionUtils.loadScreenStateToRequest(request);
				}
				/*Added by Pushpalatha for TJN252-7 ends*/
				if(request.getParameter("downloadPath")!=null && request.getParameter("downloadPath")!="")
				{
					request.setAttribute("downloadPath", request.getParameter("downloadPath"));
				}
				/* Added by Padmavathi for V2.8-79 ends */
				if(a.length() > 0) {
					try {
						int appId = Integer.parseInt(request.getParameter("appId"));
						List<Module> modules = handler.getModules(appId, request.getParameter("group"));
						request.setAttribute("modules", modules);
					} catch (NumberFormatException e) {
						logger.error("ERROR - Invalid App ID [{}]", request.getParameter("appId"));
						request.getRequestDispatcher("error.jsp").forward(request, response);
					}
				}
				
				request.setAttribute("applicationId", Utilities.trim(request.getParameter("appId")));
				request.setAttribute("selectedGroup", Utilities.trim(request.getParameter("group")));
				request.getRequestDispatcher("v2/function_list.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching function information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SessionUtils.loadScreenStateToRequest(request);
		
		logger.info("entered doPost()");
		//TenjinSession tjnSession = SessionUtils.getTenjinSession(request);
		Module module = this.mapAttributes(request);
		/*Added by Pushpalatha for TENJINCG-568 starts*/
		Group grp = new Group();
		Aut aut = null;
		 /*Added by Pushpalatha for TENJINCG-568 ends*/
		ModuleHandler moduleHandler = new ModuleHandler();
		String delFlag = Utilities.trim(request.getParameter("del"));
		Authorizer authorizer=new Authorizer();
		if(delFlag.equalsIgnoreCase("") || delFlag.equalsIgnoreCase("true") ) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		
		if(delFlag.equalsIgnoreCase("true")) {
			this.doDelete(request, response);
		}else {
			String type = Utilities.trim(request.getParameter("transactionType"));
			
			try {
				aut = new AutHandler().getApplication(Integer.parseInt(Utilities.trim(request.getParameter("aut"))));
			} catch (NumberFormatException e1) {
				logger.error("ERROR - Application ID [{}] is invalid", Utilities.trim(request.getParameter("aut")));
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			} catch (DatabaseException e1) {
				logger.error(e1.getMessage(),e1);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}
			
			if(type.equalsIgnoreCase("update")) {
				try {
					moduleHandler.update(module.getCode(), module, module.getAut());
					SessionUtils.setScreenState("success", "function.update.success", request);
					response.sendRedirect("FunctionServlet?t=view&key=" + module.getCode() + "&appId=" + module.getAut());
				} catch (DatabaseException e) {
					logger.error(e.getMessage(), e);
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("module", module);
					request.setAttribute("aut", aut);
					request.getRequestDispatcher("v2/function_view.jsp").forward(request, response);
				} catch (RequestValidationException e) {
					logger.error(e.getMessage(), e);
					/*Changed by Padmavathi for TENJINCG-426 starts*/
				/*	SessionUtils.setScreenState("error", e.getMessage(), request);*/
					SessionUtils.setScreenState("error", e.getMessage(),e.getTargetFields(), request);
					/*Changed by Padmavathi for TENJINCG-426 ends*/
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("module", module);
					request.setAttribute("aut", aut);
					request.getRequestDispatcher("v2/function_view.jsp").forward(request, response);
				}
			}else {
				try {
					 /*Added by Pushpalatha for TENJINCG-568 starts*/
					aut = new AutHandler().getApplication(Integer.parseInt(Utilities.trim(request.getParameter("aut"))));
					 /*Added by Pushpalatha for TENJINCG-568 ends*/
					moduleHandler.persist(module, module.getAut());
					 /*Added by Pushpalatha for TENJINCG-568 starts*/
					grp.setAut(module.getAut());
					grp.setAppName(aut.getName());
					grp.setGroupName(module.getGroup());
					grp = new ModulesHelper().persistGroup(grp);
					 /*Added by Pushpalatha for TENJINCG-568 ends*/
					SessionUtils.setScreenState("success", "function.create.success", request);
					response.sendRedirect("FunctionServlet?t=view&key=" + module.getCode() + "&appId=" + module.getAut());
				} catch (DatabaseException e) {
					logger.error(e.getMessage(), e);
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("module", module);
					request.setAttribute("aut", aut);
					request.getRequestDispatcher("v2/function_new.jsp").forward(request, response);
				} catch (RequestValidationException e) {
					logger.error(e.getMessage(), e);
					/*Changed by Padmavathi for TENJINCG-426 starts*/
				/*	SessionUtils.setScreenState("error", e.getMessage(), request);*/
					SessionUtils.setScreenState("error", e.getMessage(),e.getTargetFields(), request);
					/*Changed by Padmavathi for TENJINCG-426 ends*/
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("module", module);
					request.setAttribute("aut", aut);
					request.getRequestDispatcher("v2/function_new.jsp").forward(request, response);
				}
			}
		}
	}

	
	private Module mapAttributes(HttpServletRequest request) {
		Module module = new Module();
		/*Changed by Ashiki for V2.8-83 starts*/
		/*module.setCode(Utilities.trim(request.getParameter("functionCode")));*/
		module.setCode(Utilities.trim(request.getParameter("functionCodeValue")));
		/*Changed by Ashiki for V2.8-83 ends*/
		module.setName(Utilities.trim(request.getParameter("functionName")));
		module.setMenuContainer(Utilities.trim(request.getParameter("menu")));
		module.setDateFormat(Utilities.trim(request.getParameter("dateFormat")));
		module.setGroup(Utilities.trim(request.getParameter("group")));
		try {
			module.setAut(Integer.parseInt(Utilities.trim(request.getParameter("aut"))));
			
		} catch (NumberFormatException e) {
			logger.error("ERROR - Application ID [{}] is invalid", Utilities.trim(request.getParameter("aut")));
			module.setAut(0);
		}
		 /*Added by Pushpalatha for TENJINCG-568 starts*/
		module.setAppName(Utilities.trim(request.getParameter("appName")));
		 /*Added by Pushpalatha for TENJINCG-568 ends*/
		return module;
	}
	
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] selectedModuleCodes = request.getParameterValues("selectedFunctionCodes");
		String appId = Utilities.trim(request.getParameter("application")).length() > 0 ? Utilities.trim(request.getParameter("application")) : Utilities.trim(request.getParameter("aut"));
		String group = Utilities.trim(request.getParameter("group"));
		String functionCode = Utilities.trim(request.getParameter("functionCode"));
		boolean requestServedFromListPage = true;
		if(functionCode.length() > 0) {
			requestServedFromListPage = false;
		}
		
		ModuleHandler handler = new ModuleHandler();
		try {
			handler.deleteModules(Integer.parseInt(appId), selectedModuleCodes);
			if(selectedModuleCodes.length>1) {
				SessionUtils.setScreenState("success", "function.delete.multiple.success", request);
			}else {
				SessionUtils.setScreenState("success", "function.delete.success", functionCode, request);
			}
			response.sendRedirect("FunctionServlet?appId=" + appId + "&group=" + group);
		} catch (NumberFormatException e) {
			logger.error("ERROR - Invalid Application ID {}", appId, e);
			SessionUtils.setScreenState("error", "generic.error", request);
			//SessionUtils.loadScreenStateToRequest(request);
			if(requestServedFromListPage) {
				response.sendRedirect("FunctionServlet?appId=" + appId + "&group=" + group);
			}else {
				response.sendRedirect("FunctionServlet?t=view&key=" + functionCode + "&appId=" + appId);
			}
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			SessionUtils.setScreenState("error", "generic.db.error", request);
			if(requestServedFromListPage) {
				response.sendRedirect("FunctionServlet?appId=" + appId + "&group=" + group);
			}else {
				response.sendRedirect("FunctionServlet?t=view&key=" + functionCode + "&appId=" + appId);
			}
		} catch (ResourceConflictException e) {
			/*Changed by Padmavathi for TENJINCG-426 starts*/
			SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(),request);
			/*Changed by Padmavathi for TENJINCG-426 ends*/
			if(requestServedFromListPage) {
				response.sendRedirect("FunctionServlet?appId=" + appId + "&group=" + group);
			}else {
				response.sendRedirect("FunctionServlet?t=view&key=" + functionCode + "&appId=" + appId);
			}
		}
	}
	
}
