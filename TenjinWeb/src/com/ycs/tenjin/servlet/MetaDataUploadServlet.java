


/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MetaDataUploadServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              	DESCRIPTION
 * 23-04-2018           Padmavathi       	        for TENJINCG-617
 */

package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.google.common.collect.Multimap;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.aut.MetadataAudit;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.utils.AnnotationUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.MetaDataHelper;
import com.ycs.tenjin.util.MetaDataUtils;

public class MetaDataUploadServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(MetaDataUploadServlet.class);

	MetaDataUtils util = new MetaDataUtils();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String param = req.getParameter("param");
		
		if(param.equalsIgnoreCase("import_home")){
			req.getSession().setAttribute("METADATA_MAP", null);
			resp.sendRedirect("metadata_import.jsp");
		}else if (param.equalsIgnoreCase("get_func")) {
			Map<String, Object> metadataMap = (Map<String, Object>) req.getSession().getAttribute("METADATA_MAP");
			TenjinSession tjnSession = (TenjinSession)req.getSession().getAttribute("TJN_SESSION");
			String userId = tjnSession.getUser().getId();
			String applicationNodeName = "";
			JSONObject json = (JSONObject) metadataMap.get("json");
			String appName = (String)req.getParameter("appName");
			String functionNodeName = (String)metadataMap.get("functionNodeName");
			String functionNameNodeName = (String)metadataMap.get("functionNameNodeName");

			try {
				applicationNodeName = AnnotationUtils.getApplicationNodeName(Aut.class);
			} catch (Exception e1) {
				logger.error("Error ", e1);
			}
			
			
			String retData = "";
			try {
				JSONArray auts = json.getJSONArray("auts");
				//JSONArray info = json.getJSONArray("info");
				for(int i = 0; i < auts.length(); i++){
				JSONObject aut = (JSONObject) auts.get(i);
				JSONObject autJson = aut.getJSONObject("aut");
					if (appName.equalsIgnoreCase(autJson.getString(applicationNodeName))) {
						autJson.put("userId", userId);
						metadataMap.put("autfunc", autJson);
						autJson.put("functionNodeName", functionNodeName);
						autJson.put("functionNameNodeName", functionNameNodeName);
						retData = autJson.toString();
						
					}
				}
			} catch (JSONException e) {
				logger.error("Error ", e);
			}
			resp.getWriter().write(retData);
		}else if(param.equalsIgnoreCase("import")){
			TenjinSession tjnSession = (TenjinSession)req.getSession().getAttribute("TJN_SESSION");
			Map<String, Object> metadataMap = (Map<String, Object>) req.getSession().getAttribute("METADATA_MAP");
			String data = req.getParameter("data");
			String appName = req.getParameter("appName");
			String importOrCheck = req.getParameter("importorcheck");
			String retData = "";
			
			boolean importFlag = false;
			if (importOrCheck.equalsIgnoreCase("import")) {
				importFlag = true;
			}else if(importOrCheck.equalsIgnoreCase("check")){
				importFlag = false;
			}
			
			JSONObject autJson = (JSONObject) metadataMap.get("autfunc");
			String data1 = data.substring(0, data.length()-1);
			String[] dataArray = data1.split(";", 3);
			String type = dataArray[0];
			String modules = null;
			String apps = null;
			if(type.equalsIgnoreCase("func")){
				apps = appName;
				if(dataArray.length >= 3){
					modules = dataArray[2];
					};
			}
			else if(type.equalsIgnoreCase("app")){
				if(dataArray.length >= 3){
					apps = dataArray[2];
					};
			}
			String selector = dataArray[1];
			
			List<String> funcList = new ArrayList<String>();
			if(modules!=null)
			funcList = (List<String>) Arrays.asList(modules.split(";"));
			
			String[] autList = null;
			if(apps!=null)
			autList = apps.split(";");
			JSONObject json = new JSONObject();
			try {
				/*Changed by Padmavathi for TENJINCG-617 Starts*/
				Multimap<String, JSONObject> result1 = util.selectedData(selector, funcList, autList, autJson, importFlag,"IMPORTED","MetaDataUpload");
				/*Changed by Padmavathi for TENJINCG-617 ends*/
				int count = 0;
				
				count = result1.get("Existing").size();
				if(count > 0){
					metadataMap.put("overrideMap", result1);
					
					json.put("url", "metadata_override.jsp");
				}else{
					json.put("url", "");
					json.put("status", "SUCCESS");
					json.put("message", "Import Successful");
					if (importFlag) {
						MetadataAudit audit = new MetadataAudit();
						audit.setUser(tjnSession.getUser().getId());
						audit.setStatus("SUCCESS");
						audit.setType("IMPORT");
						new MetaDataHelper().persistExportAudit(audit);
					}
					
				}
				
				util = new MetaDataUtils();
				metadataMap.put("appName", appName);
				req.getSession().setAttribute("METADATA_MAP", metadataMap);
			} catch (Exception e) {
				try {
					MetadataAudit audit = new MetadataAudit();
					audit.setUser(tjnSession.getUser().getId());
					audit.setStatus("FAILURE");
					audit.setType("IMPORT");
					
					new MetaDataHelper().persistExportAudit(audit);
					json.put("url", "");
					json.put("status", "ERROR");
					json.put("message", e.getMessage());
				} catch (JSONException | DatabaseException | SQLException e1) {
					logger.error("Error ", e1);
				}
			}
			retData = json.toString();
			resp.getWriter().write(retData);
		}else if(param.equalsIgnoreCase("progress_bar")){
			
			int total = util.getTotalFunctionCount();
			int current = util.metaDataHelper.getCurrentFunctionCount();
			
			JSONObject j = new JSONObject();
			String retData = "";
			try{
				j.put("status","success");
				j.put("counter", current);
				j.put("total", total);
				retData = j.toString();
			}catch(Exception e){
				retData = "{status:error, message:An ineternal error occurred}";
			}
			resp.getWriter().write(retData);
		}else if(param.equals("override")){
			TenjinSession tjnSession = (TenjinSession)req.getSession().getAttribute("TJN_SESSION");
			String retData ="";
			JSONObject output = new JSONObject();
			String data = req.getParameter("data");
			String appName = req.getParameter("app");
			Map<String, Object> metadataMap = (Map<String, Object>) req.getSession().getAttribute("METADATA_MAP");
			
			JSONObject autJson = (JSONObject)metadataMap.get("autfunc");
			Multimap<String, JSONObject> overrideMap = (Multimap<String, JSONObject>)metadataMap.get("overrideMap");
			List<String> dataMap = new ArrayList<String>();
			
			String[] dataArray = data.split(";");
			for (String value : dataArray) {
				dataMap.add(value);
			}
			try {
				/*Changed by Padmavathi for TENJINCG-617 starts*/
				util.overrideFunctions(dataMap, autJson, overrideMap, appName,"IMPORTED","MetaDataUpload");
				/*Changed by Padmavathi for TENJINCG-617 ends*/
				MetadataAudit audit = new MetadataAudit();
				audit.setUser(tjnSession.getUser().getId());
				audit.setStatus("SUCCESS");
				audit.setType("IMPORT");
				
				new MetaDataHelper().persistExportAudit(audit);
				metadataMap.put("message", "Import Completed");
				output.put("status", "SUCCESS");
			} catch (Exception e) {
				logger.debug("Error while overriding the function : " + e.getMessage());
				try {
					MetadataAudit audit = new MetadataAudit();
					audit.setUser(tjnSession.getUser().getId());
					audit.setStatus("FAILURE");
					audit.setType("IMPORT");
					
					new MetaDataHelper().persistExportAudit(audit);
					
					output.put("status", "ERROR");
					output.put("message", e.getMessage());
				} catch (Exception e1) {
				
				}
			}
			util = new MetaDataUtils();
			retData = output.toString();
			resp.getWriter().write(retData);
		}else if (param.equalsIgnoreCase("upload")) {
			Map<String, Object> metadataMap = (Map<String, Object>)req.getSession().getAttribute("METADATA_MAP");
			String fileName = (String) metadataMap.get("filename");
			String folderPath = req.getSession().getServletContext().getRealPath("/") + "\\Uploads\\";
			Map<String, Object> map = new HashMap<String, Object>();
			MetaDataUtils metadata = new MetaDataUtils();
			try{
			JSONObject json = metadata.parseImportedFile(folderPath+fileName);
			
			String appNodeName = AnnotationUtils.getApplicationNodeName(Aut.class);
			String functionNameNodeName = AnnotationUtils.getFunctionNameNodeName(Module.class);
			String functionNodeName = AnnotationUtils.getFunctionNodeName(Module.class);
			map.put("json", json);
			map.put("status", "SUCCESS");
			JSONArray missingAnno = json.getJSONArray("missing-anno");
			String missed = "";
			if(missingAnno.length() > 0){
				for (int i = 0; i < missingAnno.length(); i++) {
					missed += missingAnno.get(i)+",";
				}
				map.put("message", "File Uploaded Successfully. WARNING : "+missed.substring(0, missed.length()-1)+" : These fields will be missed to import.");
			}else{
				map.put("message", "File Uploaded Successfully");
			}
			
			
			map.put("filename", fileName);
			
			map.put("functionNameNodeName", functionNameNodeName);
			map.put("functionNodeName", functionNodeName);
			map.put("appNodeName", appNodeName);
			}catch(Exception e){
				map.put("status", "ERROR");
				map.put("message", "Error While Parsing the import file. Please select appropriate file");
				map.put("filename", fileName);
			}
			req.getSession().setAttribute("METADATA_MAP", map);
			resp.sendRedirect("metadata_import.jsp");
		}else if(param.equalsIgnoreCase("clear")){
			req.getSession().setAttribute("METADATA_MAP", null);
			resp.sendRedirect("metadata_import.jsp");
		}else if(param.equalsIgnoreCase("import_confirm")){
			String value = req.getParameter("confirmVal");
			req.getSession().setAttribute("confirm_val", value);
			}
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);

			File dir = new File(request.getSession().getServletContext()
					.getRealPath("/")
					+ "\\Uploads");
			if (!dir.exists()) {
				dir.mkdir();
			}
			List items = upload.parseRequest(request);
			Iterator iterator = items.iterator();
			String parent = "";
			String fileName = "";
			while (iterator.hasNext()) {
				FileItem xlFile = (FileItem) iterator.next();
				if (!xlFile.isFormField()) {
					fileName = xlFile.getName();
					File uploadedFileRP = new File(request.getSession()
							.getServletContext().getRealPath("/")
							+ "\\Uploads\\" + fileName);
					xlFile.write(uploadedFileRP);
				} else {
					parent = xlFile.getString();
				}
			}

			map.put("status", "SUCCESS");
			map.put("message", "");
			map.put("filename", fileName);
			
		} catch (Exception e) {
			map.put("status", "ERROR");
			map.put("message", "An exception occured while uploading functions");
		}
		request.getSession().setAttribute("METADATA_MAP", map);
		response.sendRedirect("metadata_import.jsp");
	}
}
