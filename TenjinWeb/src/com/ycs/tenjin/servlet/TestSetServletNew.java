/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestSetServletNew.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 10-04-2018		    Pushpalatha			   	Newly Added 
* 17-04-2018			Preeti					TENJINCG-638
* 28-May-2018	        Pushpa					TENJINCG-386
* 31-May-2018			Pushpa					TENJINCG-675
* 18-06-2018			Preeti					T251IT-72
* 20-06-2018			Preeti					T251IT-92
* 27-09-2018			Pushpa					TENJINCG-740
* 22-10-2018			Sriram					TENJINCG-876
* 23-10-2018			Preeti					TENJINCG-848
* 07-11-2018            Padmavathi              TENJINCG-893
* 19-02-2019			Preeti					TENJINCG-969
* 29-03-2019			Preeti					TENJINCG-1003
* 10-04-2019			Ashiki					TENJINCG-1029
* 02-05-2019			Roshni					TENJINCG-1046
* 03-05-2019            Padmavathi              TENJINCG-1050
* 28-05-2019			Ashiki					V2.8-70
* 10-10-2019            Padmavathi              TJN27-47
* 19-11-2020            Priyanka                TENJINCG-1231 
* */

package com.ycs.tenjin.servlet;



import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.handler.ProjectMailHandler;
import com.ycs.tenjin.handler.TenjinMailHandler;
import com.ycs.tenjin.handler.TestSetHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.ExcelHandler;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class TestSetServletNew
 */
@WebServlet("/TestSetServletNew")
public class TestSetServletNew extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(TestSetServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestSetServletNew() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SessionUtils.loadScreenStateToRequest(request);
		String task = Utilities.trim(request.getParameter("t"));
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		User user=tjnSession.getUser();
		Project project=tjnSession.getProject();
		int projectId=project.getId();
		String currentUser=user.getId();
		/*Added by Priyanka for TENJINCG-1231 starts*/
		try {
			project=new ProjectHandler().hydrateProject(projectId);
		} catch (DatabaseException e3) {
			
			e3.printStackTrace();
		}
		/*Added by Priyanka for TENJINCG-1231 ends*/
		if(task.equalsIgnoreCase("init_new_testset")){
			
			request.setAttribute("user", currentUser);
			request.setAttribute("project", project);
			request.getRequestDispatcher("v2/testset_new.jsp").forward(request, response);
		
		}
		/*Added by Preeti for TENJINCG-848 starts*/
		else if(task.equalsIgnoreCase("testset_copy")){
			String recId = request.getParameter("recId");
			TestSetHandler handler=new TestSetHandler();
			try{
				TestSet testset=handler.hydrateTestSetBasicDetails(projectId,Integer.parseInt(recId));
				request.setAttribute("testset", testset);
				request.setAttribute("testSetId", testset.getId());
				List<TestCase> testcase=testset.getTests();
				request.setAttribute("testcasecount", testcase.size());
				request.setAttribute("user", currentUser);
				request.setAttribute("project", project);
				request.setAttribute("flag", "yes");
				request.getRequestDispatcher("v2/testset_new.jsp").forward(request, response);
			} catch (NumberFormatException e) {
			logger.error("ERROR fetching Testset information", e);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			logger.error("Error ", e);
		} catch (DatabaseException e) {
			logger.error("ERROR fetching Testset information", e);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			logger.error("Error ", e);
		}
		
		}
		/*Added by Preeti for TENJINCG-848 ends*/
		else if(task.equalsIgnoreCase("list")){
			try {
				if(request.getParameter("msg")!=null && request.getParameter("msg")!="")
				{
					SessionUtils.setScreenState("success", request.getParameter("msg"), request);
					SessionUtils.loadScreenStateToRequest(request);
					
				} 
				/* Added by Padmavathi for TENJINCG-893 starts */
				if(request.getParameter("downloadPath")!=null && request.getParameter("downloadPath")!="")
				{
					request.setAttribute("downloadPath", request.getParameter("downloadPath"));
				}
				 /* Added by Padmavathi for TENJINCG-893 ends */
				request.setAttribute("user", currentUser);
				request.setAttribute("currentUser", user);
				request.setAttribute("project", project);
				/* Added by Priyanka for TENJINCG-1231 starts*/
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				if(project.getEndDate()==null){
					request.setAttribute("edate","NA");
				}else{
				String edate=sdf.format(project.getEndDate());
				
				request.setAttribute("edate",edate);
				}
				/* Added by Priyanka for TENJINCG-1231 ends*/
				request.getRequestDispatcher("v2/testset_list.jsp").forward(request, response);
			} catch (NumberFormatException e) {
				logger.error("ERROR fetching TestSet information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			} 
			
		}else if(task.equalsIgnoreCase("testSet_View")){
			String recId=request.getParameter("paramval");
			TestSetHandler handler=new TestSetHandler();
			try {
				TestSet testset=handler.hydrateTestSet(projectId,Integer.parseInt(recId));
				
				List<TestCase> testcase=testset.getTests();
				int stepCount=handler.getStepCount(testcase);
				request.setAttribute("testset", testset);
				request.setAttribute("testcase", testcase);
				/*Added by Pushpalatha for TENJINCG-675 starts*/
				request.getSession().setAttribute("testcase", testcase);
				request.getSession().setAttribute("createdOn", testset.getCreatedOn());
				request.getSession().setAttribute("createdOn", testset.getCreatedOn());
				/*Added by Pushpalatha for TENJINCG-675 ends*/
				request.setAttribute("tcCount", testcase.size());
				request.setAttribute("teststepCount", stepCount);
				request.setAttribute("project", project);
				request.setAttribute("user", currentUser);
				if(request.getParameter("msg")!=null && request.getParameter("msg")!="")
				{
					SessionUtils.setScreenState("success", request.getParameter("msg"), request);
					SessionUtils.loadScreenStateToRequest(request);
				}
				/*Added By Ashiki for V2.8-70 starts*/
				if(request.getParameter("downloadPath")!=null && request.getParameter("downloadPath")!="")
				{
					request.setAttribute("downloadPath", request.getParameter("downloadPath"));
				}
				/*Added By Ashiki for V2.8-70 ends*/
				
				/*   Added by Priyanka for TENJINCG-1231 starts */
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				
				if(project.getEndDate()==null){
					request.setAttribute("edate","NA");
				}else{
				String edate=sdf.format(project.getEndDate());
				
				request.setAttribute("edate",edate);
				}
				/*   Added by Priyanka for TENJINCG-1231 starts */
				request.getRequestDispatcher("v2/testset_view.jsp").forward(request,response);
			} catch (NumberFormatException e) {
				logger.error("ERROR fetching Testset information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching Testset information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			}
		
		}
		
		/*Added by Preeti for TENJINCG-638 starts*/
		else if(task.equalsIgnoreCase("testset_execution_history")){
			TestSetHandler handler=new TestSetHandler();
			String recId = request.getParameter("recid");
			try{
				TestSet testset=handler.hydrateTestSet(projectId,Integer.parseInt(recId));
				
				//Change by Sriram for TENJINCG-876 (Automaticall redirect to test case execution history for ad-hoc test set)
				if(Utilities.trim(testset.getRecordType()).equalsIgnoreCase("tsa")) {
					if(testset.getTests() != null) {
						response.sendRedirect("TestCaseServletNew?t=testcase_execution_history&recid=" + testset.getTests().get(0).getTcRecId());
						return;
					}
				}//Change by Sriram for TENJINCG-876 (Automaticall redirect to test case execution history for ad-hoc test set) ends
				
				request.setAttribute("testset", testset);
				List<TestRun> runs = handler.hydrateTestSetExecutionHistory(projectId,Integer.parseInt(recId));
				request.setAttribute("project", project);
				request.setAttribute("runs", runs);
				request.getRequestDispatcher("v2/testset_execution_history.jsp").forward(request, response);
			}catch(Exception e){
				logger.error("Could not fetch execution summary for test case",recId, e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			}
		}
		
		else if(task.equalsIgnoreCase("download_tc_for_ts")){
			String retData = "";
			int testSetId=Integer.parseInt(request.getParameter("txtTestSetId"));
			try{
				JSONObject json = new JSONObject();
				try{
					/*modified by paneendra for VAPT fix starts*/
					String folderPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
					File downloadPath= new File(folderPath+File.separator+"Downloads");
					if(!downloadPath.exists()){
						downloadPath.mkdirs();
					}
					/*modified by paneendra for VAPT fix ends*/
					String filename="TestSet.xlsx";
					ExcelHandler e=new ExcelHandler();
					long startMillis = System.currentTimeMillis();
					e.downloadTestCasesForSet(tjnSession.getProject().getId(), folderPath + File.separator+"Downloads", filename,testSetId);
					logger.info("Test Cases downloaded in {}ms", Utilities.calculateElapsedTime(startMillis, System.currentTimeMillis()));
					json.put("status", "SUCCESS");
					json.put("message","Test Set data downloaded successfully");
					json.put("path","Downloads"+File.separator +filename);
				}catch(Exception e){
					logger.error("An exception occured while deleteing test steps",e);
					json.put("status", "ERROR");
					json.put("message","An Internal Error Occurred. Please try again");
				}finally{
					retData = json.toString();
				}
			}catch(Exception e){
				retData = "An Internal Error Occurred. Please contact your system administrator";
			}

			response.getWriter().write(retData);
		}
		/*Added by Preeti for TENJINCG-638 ends*/
		/*Added by Pushpa for TENJINCG-740 Starts*/
		else if(task.equalsIgnoreCase("download_ts")){
			String retData = "";
			String testSetIds=request.getParameter("txtTestSetId");
			String[] tsetIds=testSetIds.split(",");
			ArrayList<TestSet> testset=null;
			try{
				JSONObject json = new JSONObject();
				try{
					testset=new TestSetHandler().hydrateTestSets(projectId,tsetIds);
					/*modified by paneendra for VAPT fix starts*/
					String folderPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
					File downloadPath= new File(folderPath+File.separator+"Downloads");
					if(!downloadPath.exists()){
						downloadPath.mkdirs();
					}
					/*modified by paneendra for VAPT fix ends*/
					String filename="TestSet"+".xlsx";
					ExcelHandler e=new ExcelHandler();
					long startMillis = System.currentTimeMillis();
					e.downloadTestSet(tjnSession.getProject().getId(), folderPath + File.separator+"Downloads", filename,testset);
					logger.info("Test Cases downloaded in {}ms", Utilities.calculateElapsedTime(startMillis, System.currentTimeMillis()));
					json.put("status", "SUCCESS");
					json.put("message","Test Set(s) and Test Case(s) downloaded successfully");
					json.put("path","Downloads"+File.separator +filename);
				}catch(Exception e){
					logger.error("An exception occured while deleteing test steps",e);
					json.put("status", "ERROR");
					json.put("message","An Internal Error Occurred. Please try again");
				}finally{
					retData = json.toString();
				}
			}catch(Exception e){
				retData = "An Internal Error Occurred. Please contact your system administrator";
			}
			response.getWriter().write(retData);
		}
		/*Added by Pushpa	for TENJINCG-740 Ends*/
		
		else if(task.equalsIgnoreCase("fetch_unmapped_tcs")){
			String tsId = request.getParameter("testset");
			String mode=request.getParameter("mode");
			int tsRecID=Integer.parseInt(tsId.trim());
			TestSetHandler testsetHandler=new TestSetHandler();
			Project project1=tjnSession.getProject();
			TestSet testset=null;
			try {
				testset = testsetHandler.hydrateTestSet( tjnSession.getProject().getId(), tsRecID);
			} catch (DatabaseException e2) {
				e2.printStackTrace();
			}
			
			try {
				ArrayList<TestCase> unmappedTC=testsetHandler.getUnmappedTc(tsRecID, tjnSession.getProject().getId(), mode);
				ArrayList<TestCase> mappedTC=testset.getTests();
				request.setAttribute("tcs", unmappedTC);
				request.setAttribute("mappedTC", mappedTC);
				request.setAttribute("testset", testset);
				request.setAttribute("priority", null);
				request.setAttribute("project", project1);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("v2/tsetmap_new.jsp").forward(request, response);
			} catch (DatabaseException e1) {
				
				request.setAttribute("testset", testset);
				SessionUtils.setScreenState("error", e1.getMessage(), request);
				SessionUtils.loadScreenStateToRequest(request);
				response.sendRedirect("v2/tsetmap_new.jsp");
		
			} catch (RequestValidationException e) {
				
				request.setAttribute("testset", testset);
				SessionUtils.setScreenState("error", e.getMessage(), request);
				SessionUtils.loadScreenStateToRequest(request);
				response.sendRedirect("v2/tsetmap_new.jsp");
			}
			
		}else if(task.equalsIgnoreCase("fetch_priority_testcase")){
			String tsId = request.getParameter("testset");
			String mode=request.getParameter("mode");
			String priority=request.getParameter("priority");
			int tsRecID=Integer.parseInt(tsId.trim());
			TestSetHandler testsetHandler=new TestSetHandler();
			Project project1=tjnSession.getProject();
			TestSet testset=null;
			ArrayList<TestCase> unmappedTC=null;
			ArrayList<TestCase> mappedTC=null;
			try {
				testset = testsetHandler.hydrateTestSet( tjnSession.getProject().getId(), tsRecID);
			} catch (DatabaseException e2) {
				
				e2.printStackTrace();
			}
			try {
				unmappedTC=testsetHandler.getPriorityTc(tsRecID, tjnSession.getProject().getId(), mode,priority);
				mappedTC=testset.getTests();
				request.setAttribute("tcs", unmappedTC);
				request.setAttribute("mappedTC", mappedTC);
				request.setAttribute("testset", testset);
				request.setAttribute("priority", priority);
				request.setAttribute("project", project1);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("v2/tsetmap_new.jsp").forward(request, response);
			} catch (DatabaseException e1) {
				
				request.setAttribute("testset", testset);
				/*Modified by Preeti for T251IT-92 starts*/
				mappedTC=testset.getTests();
				request.setAttribute("tcs", unmappedTC);
				request.setAttribute("mappedTC", mappedTC);
				request.setAttribute("priority", priority);
				SessionUtils.setScreenState("error", e1.getMessage(), request);
				SessionUtils.loadScreenStateToRequest(request);
				/*response.sendRedirect("v2/tsetmap_new.jsp");*/
				request.getRequestDispatcher("v2/tsetmap_new.jsp").forward(request, response);
				/*Modified by Preeti for T251IT-92 ends*/
			} catch (RequestValidationException e) {
				
				request.setAttribute("testset", testset);
				/*Modified by Preeti for T251IT-92 starts*/
				mappedTC=testset.getTests();
				request.setAttribute("tcs", unmappedTC);
				request.setAttribute("mappedTC", mappedTC);
				request.setAttribute("priority", priority);
				SessionUtils.setScreenState("error", e.getMessage(), request);
				SessionUtils.loadScreenStateToRequest(request);
				/*response.sendRedirect("v2/tsetmap_new.jsp");*/
				request.getRequestDispatcher("v2/tsetmap_new.jsp").forward(request, response);
				/*Modified by Preeti for T251IT-92 ends*/
			}
		}
		/*Added by pushpalatha for TENJINCG-386 starts */
		else if(task.equalsIgnoreCase("validate_mobility")){
			Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
			String retData = "";
			TestSet ts= (TestSet)map.get("TEST_SET");
			try{
			JSONObject json = new JSONObject();
			try {
				
				Boolean mobFlag=new TestSetHandler().validateMobile(ts);
			request.getSession().setAttribute("mobFlag",mobFlag);
				json.put("status", "SUCCESS");
				json.put("message", "");
				json.put("mobFlag", mobFlag);
				
			} catch (NumberFormatException e) {
				
				json.put("status", "ERROR");
				json.put("message", "An internal error occurred while validating testset for mobility. Please Try again");
				logger.error("Error ", e);
			} catch (DatabaseException e) {
				
				json.put("status", "ERROR");
				json.put("message", "An internal error occurred while validating testset for mobility. Please Try again");
				logger.error("Error ", e);
			}finally{
				retData = json.toString();
			}
			}catch(Exception e){
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			
			response.getWriter().write(retData);
			
		}
		/*Added by pushpalatha for TENJINCG-386 ends */
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Project project=tjnSession.getProject();
		User user=tjnSession.getUser();
		
		/*Added by Priyanka for TENJINCG-1231 starts*/
		int projectId=project.getId();
		try {
			project=new ProjectHandler().hydrateProject(projectId);
		} catch (DatabaseException e3) {
			
			e3.printStackTrace();
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		if(project.getEndDate()==null){
			request.setAttribute("edate","NA");
		}else{
		String edate=sdf.format(project.getEndDate());
		
		request.setAttribute("edate",edate);
		}
		/*Added by Priyanka for TENJINCG-1231 ends*/
		
		logger.info("entered doPost()");
		String delFlag = Utilities.trim(request.getParameter("del"));
		TestSetHandler handler=new TestSetHandler();
		TestSet testset=new TestSet();
		if(delFlag.equalsIgnoreCase("true")) {
			this.doDelete(request, response);
		}else if(delFlag.equalsIgnoreCase("updateTestSet")){
			String recid=request.getParameter("id");
			String createdBy=request.getParameter("txtCreatedBy");
			testset=this.mapAttributes(request);
			testset.setId(Integer.parseInt(recid));
			/*Added by Preeti for TENJINCG-969 starts*/
			AuditRecord audit = new AuditRecord();
			audit.setEntityRecordId(testset.getId());
			audit.setLastUpdatedBy(tjnSession.getUser().getId());
			audit.setEntityType("testset");
			testset.setAuditRecord(audit);
			/*Added by Preeti for TENJINCG-969 ends*/
			try {
				
				handler.updateTestSet(testset);
				testset=handler.hydrateTestSet(projectId,Integer.parseInt(recid));
				List<TestCase> testcase=testset.getTests();
				request.setAttribute("testset", testset);
				request.setAttribute("testcase", testcase);
				request.setAttribute("project", project);
				request.setAttribute("user", createdBy);
				/*Added by Preeti for T251IT-72 starts*/
				request.setAttribute("tcCount", testcase.size());
				int stepCount=handler.getStepCount(testcase);
				request.setAttribute("teststepCount", stepCount);
				/*Added by Preeti for T251IT-72 ends*/
				SessionUtils.setScreenState("success", "testSet.update.success", request);
				SessionUtils.loadScreenStateToRequest(request);
				/* Added by Roshni for TENJINCG-1046 starts */
				TenjinMailHandler tmhandler=new TenjinMailHandler();
				User tsOwner=new UserHandler().getUser(testset.getCreatedBy());
				/*Added by Padmavathi for TENJINCG-1050 starts*/
				if(!new ProjectMailHandler().isUserOptOutFromMail(projectId,"TestPlanEdit",tsOwner.getId())){
					/*Added by Padmavathi for TENJINCG-1050 ends*/
					JSONObject mailContent=tmhandler.getMailContent(String.valueOf(testset.getId()), testset.getName(), "Test Set", tjnSession.getUser().getFullName(),
						"update", new Timestamp(new Date().getTime()), tsOwner.getEmail(), "");
				
					try {
						new TenjinMailHandler().sendEmailNotification(mailContent);
					} catch (TenjinConfigurationException e) {
						logger.error("Error ", e);
					}
				}
				
				/* Added by Roshni for TENJINCG-1046 ends */
				request.getRequestDispatcher("v2/testset_view.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching TestSet information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			} catch (RequestValidationException e) {
				SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
				request.setAttribute("testset", testset);
				/*Added by Pushpalatha for TENJINCG-675 starts*/
				request.getSession().getAttribute("testcase");
				request.getSession().getAttribute("createdOn");
				/*Added by Pushpalatha for TENJINCG-675 ends*/
				request.setAttribute("project", project);
				request.setAttribute("user",createdBy);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("v2/testset_view.jsp").forward(request, response);
				logger.error("Error ", e);
			}
			
		}
		/*Added by Pushpalatha for TENJINCG-637 starts*/
		else if(delFlag.equalsIgnoreCase("map_Testcase")){
			
			String tsId=request.getParameter("id");
			String mode=request.getParameter("mode");
			String unMappedTc=request.getParameter("UnMappedTC");
			String mappedTc=request.getParameter("MappedTC");
		
			try {
				handler.persistTestSetunMap(Integer.parseInt(tsId), unMappedTc, tjnSession.getProject().getId());
				handler.persistTestSetMap(Integer.parseInt(tsId), mappedTc, tjnSession.getProject().getId(), true);
				SessionUtils.setScreenState("success", "testSet.map.success", request);
				response.sendRedirect("TestSetServletNew?t=fetch_unmapped_tcs&testset="+tsId+"&mode="+mode);
			} catch (NumberFormatException e) {
				logger.error("ERROR fetching TestCase information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching TestSet information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			}
		}
		/*Added by Pushpalatha for TENJINCG-637 ends*/
		else{
			/*Modified by Preeti for TENJINCG-848 starts*/
			testset=this.mapAttributes(request);
			String flag = "";
			String oldTestSetId = request.getParameter("testSetId");
			try {
				flag = request.getParameter("copyFlag");
					if(flag!=null && flag.equalsIgnoreCase("yes")){
						if(testset.getMode()=="")
							testset.setMode(request.getParameter("selMode"));
						handler.persistTestSet(testset,projectId);
						handler.copyTestSetMap(testset.getId(), Integer.parseInt(oldTestSetId),  testset.getProject());
					}
					else
						handler.persistTestSet(testset,projectId);
				/*Modified by Preeti for TENJINCG-848 ends*/
				SessionUtils.setScreenState("success", "testset.create.success", request);
				response.sendRedirect("TestSetServletNew?t=testSet_View&paramval=" +testset.getId());
			}
			catch(DuplicateRecordException e){
				logger.error(e.getMessage(), e);
				SessionUtils.setScreenState("error", e.getMessage(),e.getTargetFields(), request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("testset", testset);
				/*Added by Preeti for TENJINCG-848 starts*/
				if(flag!=null)
					if(flag.equalsIgnoreCase("yes")){
						try {
							testset = handler.hydrateTestSetBasicDetails(projectId,Integer.parseInt(oldTestSetId));
							List<TestCase> testcase=testset.getTests();
							request.setAttribute("testcasecount", testcase.size());
							request.setAttribute("flag", flag);
							request.setAttribute("testSetId", testset.getId());
						} catch (DatabaseException e1) {
							
							logger.error("Error ", e1);
						}
						
				}
				/*Added by Preeti for TENJINCG-848 ends*/
				request.setAttribute("user", user.getId());
				request.setAttribute("project", project);
				request.getRequestDispatcher("v2/testset_new.jsp").forward(request, response);
			}
			catch (DatabaseException e) {
				logger.error(e.getMessage(), e);
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("testset", testset);
				/*Added by Preeti for TENJINCG-848 starts*/
				if(flag!=null)
					if(flag.equalsIgnoreCase("yes")){
						try {
							testset = handler.hydrateTestSetBasicDetails(projectId,Integer.parseInt(oldTestSetId));
							List<TestCase> testcase=testset.getTests();
							request.setAttribute("testcasecount", testcase.size());
							request.setAttribute("flag", flag);
							request.setAttribute("testSetId", testset.getId());
						} catch (DatabaseException e1) {
							
							logger.error("Error ", e1);
						}
						
				}
				/*Added by Preeti for TENJINCG-848 ends*/
				request.setAttribute("user", user.getId());
				request.setAttribute("project", project);
				request.getRequestDispatcher("v2/testset_new.jsp").forward(request, response);
			}
			catch (RequestValidationException e) {
				logger.error(e.getMessage(), e);
				SessionUtils.setScreenState("error", e.getMessage(),e.getTargetFields(), request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("testset", testset);
				/*Added by Preeti for TENJINCG-848 starts*/
				if(flag!=null)
					if(flag.equalsIgnoreCase("yes")){
						try {
							testset = handler.hydrateTestSetBasicDetails(projectId,Integer.parseInt(oldTestSetId));
							List<TestCase> testcase=testset.getTests();
							request.setAttribute("testcasecount", testcase.size());
							request.setAttribute("flag", flag);
							request.setAttribute("testSetId", testset.getId());
						} catch (DatabaseException e1) {
							
							logger.error("Error ", e1);
						}
						
				}
				/*Added by Preeti for TENJINCG-848 ends*/
				request.setAttribute("project", project);
				request.setAttribute("user", user.getId());
				request.getRequestDispatcher("v2/testset_new.jsp").forward(request, response);
			}
		}

	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] testSetRecIds = request.getParameterValues("selectedTestSet");
		
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Project project=tjnSession.getProject();
		int projectId=project.getId();
		TestSetHandler handler = new TestSetHandler();
		/* Added by Roshni for TENJINCG-1046 starts */
		List<JSONObject> contents=new ArrayList<>();
		try {
			TenjinMailHandler tmhandler=new TenjinMailHandler();
			for(String tsId:testSetRecIds){
				JSONObject mailContent=new JSONObject();
				int testSetId=Integer.parseInt(tsId);
				TestSet set=new TestSet();
				set=handler.hydrateTestSet(projectId, testSetId);
				User tsOwner=new UserHandler().getUser(set.getCreatedBy());
				/*Added by Padmavathi for TENJINCG-1050 starts*/
				if(!new ProjectMailHandler().isUserOptOutFromMail(projectId,"TestPlanRemove",tsOwner.getId())){
					/*Added by Padmavathi for TENJINCG-1050 ends*/	
					mailContent=tmhandler.getMailContent(String.valueOf(set.getId()), set.getName(), "Test Set", tjnSession.getUser().getFullName(),
						"delete", new Timestamp(new Date().getTime()), tsOwner.getEmail(), "");
					contents.add(mailContent);
				}
			}
			/* Added by Roshni for TENJINCG-1046 ends */
			
			/*Modified by Preeti for TENJINCG-1003 starts*/
			/*handler.deleteTestSet(testSetRecIds,projectId );*/
			handler.deleteTestSet(testSetRecIds,projectId,tjnSession.getUser().getId() );
			/* Added by Roshni for TENJINCG-1046 starts */
			for(JSONObject content:contents){
				try {
					tmhandler.sendEmailNotification(content);
				} catch (TenjinConfigurationException e) {
					logger.error("Error ", e);
				}
			}
			/* Added by Roshni for TENJINCG-1046 ends */
			/*Modified by Preeti for TENJINCG-1003 ends*/
			if(testSetRecIds.length>1) {
				SessionUtils.setScreenState("success", "testset.delete.multiple.success", request);
			}else {
				SessionUtils.setScreenState("success", "testSet.delete.success",request);
			}
			response.sendRedirect("TestSetServletNew?t=list");
		} catch (NumberFormatException e) {
			logger.error("ERROR - Invalid test set ID {}", testSetRecIds, e);
			SessionUtils.setScreenState("error", "generic.error", request);
			response.sendRedirect("TestSetServletNew?t=list");
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			SessionUtils.setScreenState("error", "generic.db.error", request);
			response.sendRedirect("TestSetServletNew?t=list");
		} catch (RequestValidationException e) {
			logger.error(e.getMessage(),e);
			SessionUtils.setScreenState("error", e.getMessage(),e.getTargetFields(), request);
			response.sendRedirect("TestSetServletNew?t=list");
		} catch (ResourceConflictException e) {
			logger.error(e.getMessage(),e);
			SessionUtils.setScreenState("error",e.getMessage(), request);
			response.sendRedirect("TestSetServletNew?t=list");
			
		}
	}
	
	
	private TestSet mapAttributes(HttpServletRequest request) {
		TestSet testSet=new TestSet();
		
		testSet.setName(Utilities.trim(request.getParameter("name")));
		testSet.setDescription(Utilities.trim(request.getParameter("description")));
		testSet.setType(Utilities.trim(request.getParameter("type")));
		testSet.setPriority(Utilities.trim(request.getParameter("priority")));
		testSet.setCreatedBy(Utilities.trim(request.getParameter("createdBy")));
		
		testSet.setProject(Integer.parseInt((request.getParameter("projectId"))));
		testSet.setRecordType("TS");
		testSet.setMode(Utilities.trim(request.getParameter("mode")));
		
		return testSet;
	}


}
