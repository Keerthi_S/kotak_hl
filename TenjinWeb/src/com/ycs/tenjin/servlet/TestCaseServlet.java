/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCaseServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 27-10-2016            Parveen                Defect #637
 * 28-10-2016			Sriram					Defect #629
 * 3-Nov-2016			Sriram					Fix for Defect#701
  18-Nov-2016           Nagareddy			    display tree for manual fields map and save the manual field mapped
  06-12-2016            Leelaprasad             Req#TJN_243_13
17-12-2016            	Leelaprasad             Req#TEN-105
19-12-2016          	Leelaprasad             DEFECT FIX#TEN-95
19-12-2016          	nagareddy              	DEFECT FIX#TEN-42
10-Jan-2016 			Sahana					TENJINCG-1(Search fields in Test Case search must be made case insensitive)
19-01-2017		   		Manish				   	DEFECT(TENJINCG-40)
24-01-2017				Manish					TENJINCG-54
13-06-2017				Sriram					TENJINCG-189
19-06-2017				Roshni					TENJINCG-198
27-06-2017         	 	Leelaprasad             TENJINCG-245
24-Jul-2017				Sriram					TENJINCG-284
28-07-2017      		Leelaprasad             TENJINCG-319
03-08-2017      		Gangadhar Badagi        TENJINCG-323
08-08-2017      		Padmavathi              for T25IT-40
16-08-2017      		Leelaprasad             for T25IT-141
17-08-2017        		Leelaprasad             T25IT-171
18-08-2017        		Leelaprasad             T25IT-132
19-Aug-2017         	Manish		            T25IT-236 
24-08-2017         		Padmavathi              T25IT-280
29-Aug-2017				Manish					fetch etm project and domain for a perticular tenjin project,
												check if test step already exist under a test case before persisting
 30-08-2017           	Manish	                T25IT-284
 31-08-2017            	Padmavathi              T25IT-412
 01-09-2017			   	Pushpalatha 			T25IT-356
 17-Oct-2017			Manish		 			TENJINCG-357
 27-10-2017		  	    Preeti				    TENJINCG-365
 28-11-2017            	Padmavathi              validating respCode
 18-01-2018				Pushpalatha				TENJINCG-567
 30-01-2018				Preeti					TENJINCG-503
 24-04-2018				Pushpa					TENJINCG-654
 19-06-2018             Padmavathi              T251IT-80
 15-06-2018             Padmavathi              T251IT-107
 25-06-2018				Preeti					T251IT-140
 24-10-2018             Padmavathi              TENJINCG-849
 04-01-2019				Ashiki					for TM user credentials
 06-03-2019				Pushpa					TENJINCG-978,979
 27-03-2018             Padmavathi              TENJINCG-1001
 08-04-2019             Padmavathi              TNJN27-68& TNJN27-69
 22-05-2019				Ashiki					V2.8-53,V2.8-55,V2.8-62
 03-06-2019				Prem					V2.8-84
 07-06-2019				Ashiki					V2.8-118 
 12-06-2019				Pushpa					v2.8-123
 04-12-2019				Preeti					TENJINCG-1174
 09-01-2020				Prem					script fix
 */

package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
/*27-Mar-2015 R2.1: for Defect #1317-By Babu: Upload TestCases and Teststeps is not working properly: Starts*/
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
/*27-Mar-2015 R2.1: for Defect #1317-By Babu: Upload TestCases and Teststeps is not working properly: Ends*/
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.run.ResultValidation;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.db.ResultsHelper;
import com.ycs.tenjin.db.TestCaseHelper;
import com.ycs.tenjin.db.TestSetHelper;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.TestCaseHandler;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.testmanager.ETMException;
import com.ycs.tenjin.testmanager.TestManager;
import com.ycs.tenjin.testmanager.TestManagerFactory;
import com.ycs.tenjin.testmanager.TestManagerInstance;
import com.ycs.tenjin.testmanager.UnreachableETMException;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.ExcelHandler;
import com.ycs.tenjin.util.Utilities;

/*27-Mar-2015 R2.1: for Defect #1317-By Babu: Upload TestCases and Teststeps is not working properly: Starts*/
/*27-Mar-2015 R2.1: for Defect #1317-By Babu: Upload TestCases and Teststeps is not working properly: Ends*/
/**
 * Servlet implementation class TestCaseServlet
 */
public class TestCaseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(TestCaseServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TestCaseServlet() {
		super();
		
	}
	
	public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		@Override
		public boolean checkAccess(HttpServletRequest request) {
			User loggedInUser = SessionUtils.getTenjinSession(request).getUser();
			
			if (request.getMethod().equalsIgnoreCase("GET")) {
				String task = Utilities.trim(request.getParameter("param"));
				if (Utilities.trim(task).equalsIgnoreCase("check_tm_user_name")) {
					String userId = Utilities.trim(request.getParameter("userId"));
					if (Utilities.trim(userId).equalsIgnoreCase(loggedInUser.getId())) {
						return true;
					}
					return false;
				} else if(Utilities.trim(task).equalsIgnoreCase("PERSIST_TM_USER_CREDENTIALS")){
					String jstring = request.getParameter("paramval");
					try {
						JSONObject json = new JSONObject(jstring);
						String userId = json.getString("userId");
							if (Utilities.trim(userId).equalsIgnoreCase(loggedInUser.getId())) {
								return true;
							}
							return false;
					} catch (JSONException e) {
							logger.error("Error during mapping TM user with Tenjin user", e);
					} 
				}
				else {
					return true;
				}
			}else if (request.getMethod().equalsIgnoreCase("POST")) {
				return true;
			}
			return false;
		}

	};

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String txn = request.getParameter("param");
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		Project project = tjnSession.getProject();
		User user = tjnSession.getUser();
		if (txn.equalsIgnoreCase("map_res_val")) {
			String testStepRecId = request.getParameter("s");
			String rvId = request.getParameter("r");
			String retData = "";
			try {
				JSONObject json = new JSONObject();
				ResultValidation val = new ResultsHelper().hydrateResultValidation(Integer.parseInt(rvId));
				try {
					new ResultsHelper().persistResultValidationMapping(Integer.parseInt(rvId),
							Integer.parseInt(testStepRecId));
					json.put("status", "SUCCESS");
					json.put("name", val.getName());
				} catch (DatabaseException e) {
					json.put("status", "ERROR");
					json.put("message", e.getMessage());
				} catch (Exception e) {
					json.put("status", "ERROR");
					json.put("message", "An Internal Error occurred. Please try again");
				} finally {
					retData = json.toString();
				}

			} catch (Exception e) {
				logger.error("An error occurred", e);
				retData = "An internal error occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);
		}
		if(txn.equalsIgnoreCase("FETCH_TCS")){


			String retData = "";

			try{
				JSONObject json = new JSONObject();
				try{
					ArrayList<TestCase> tList = new TestCaseHelper().hydrateAllTestCases(project.getId());
					if(tList != null && tList.size() > 0){
						JSONArray jArray = new JSONArray();
						for(TestCase t:tList){
							JSONObject j = new JSONObject();
							j.put("recid", t.getTcRecId());
							j.put("rectype", t.getRecordType());
							j.put("id", t.getTcId());
							j.put("name", Utilities.escapeHtml(t.getTcName()));
							j.put("project", t.getProjectId());
							j.put("type", t.getTcType());
							j.put("owner", t.getTcCreatedBy());
							j.put("createddate", t.getTcCreatedOn());
							j.put("priority", t.getTcPriority());
							j.put("reviewed", t.getTcReviewed());
							j.put("status", t.getTcStatus());
							/*Changed by Leelaprasad for the defect T25IT-132 starts*/
							j.put("mode", t.getMode());
							/*Changed by Leelaprasad for the defect T25IT-132 ends*/
							jArray.put(j);
						}

						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("tcs", jArray);
					}else{
						json.put("status", "FAILURE");
						json.put("message", "No Test Cases were available.");
					}
				}catch(Exception e){
					logger.error("Error while fetching test cases",e);
					json.put("status", "ERROR");
					json.put("message", "An Internal error occurred while fetching test cases. Please try again");
				}finally{
					retData = json.toString();
				}
			}catch(Exception e){
				logger.error("Error while creating JSON",e);
				retData = "An Internal Error Occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);
		}
		else if (txn.equalsIgnoreCase("unmap_res_val")) {
			String testStepRecId = request.getParameter("s");
			String rvId = request.getParameter("r");
			String retData = "";
			try {
				JSONObject json = new JSONObject();
				try {
					new ResultsHelper().removeResultValidationMapping(Integer.parseInt(rvId),
							Integer.parseInt(testStepRecId));
					json.put("status", "SUCCESS");
				} catch (DatabaseException e) {
					json.put("status", "ERROR");
					json.put("message", e.getMessage());
				} catch (Exception e) {
					json.put("status", "ERROR");
					json.put("message", "An Internal Error occurred. Please try again");
				} finally {
					retData = json.toString();
				}

			} catch (Exception e) {
				logger.error("An error occurred", e);
				retData = "An internal error occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);
		}else if(txn.equalsIgnoreCase("downloadtestcases")){
			String retData = "";
			try{
				JSONObject json = new JSONObject();
				try{
					
					/*
					 Change by Nagababu to changed the file download file on 25-Mar-2015 Ends
					 */
					/*Changed by Pushpalatha for TENJINCG-567 starts*/
					/*modified by paneendra for VAPT fix starts*/
					String folderPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
					File downloadPath= new File(folderPath+File.separator+"Downloads");
					if(!downloadPath.exists()){
						downloadPath.mkdirs();
					}
					//String file = folderPath + File.separator+"Downloads";
					String filename="TestCases.xlsx";
					//FileOutputStream fileOutputStream = new FileOutputStream(file+File.separator+filename);
					/*modified by paneendra for VAPT fix ends*/
					
					/*Changed by Pushpalatha for TENJINCG-567 ends*/
					/*
					 Change by Nagababu to changed the file download file on 20-11-2014 Ends
					 */
					ExcelHandler e=new ExcelHandler();

					/******************************************************************
					 * Changed by Sriram for performance tuning of Bulk Test Cases download (17-10-2015)
					 */
					/*e.downloadtestcases(fileOutputStream,tc_folder_id);*/
					long startMillis = System.currentTimeMillis();
					/*Changed by Pushpalatha for TENJINCG-567 starts*/
					e.downloadTestCases(tjnSession.getProject().getId(), folderPath + File.separator+"Downloads", filename);
					/*Changed by Pushpalatha for TENJINCG-567 ends*/
					logger.info("Test Cases downloaded in {}ms", Utilities.calculateElapsedTime(startMillis, System.currentTimeMillis()));
					/******************************************************************
					 * Changed by Sriram for performance tuning of Bulk Test Cases download (17-10-2015) ends
					 */
					/*
					 Change by Nagababu to changed the file download file on 20-11-2014 Ends
					 */
					json.put("status", "SUCCESS");
					/*Changed by Prem for V2.8-60 starts*/
					/*json.put("message","Test Case and Test Set downloaded successfully");*/
					/*Changed by Prem for V2.8-84 starts*/
					/*json.put("message","Test Case(s) and Test Set(s) are downloaded successfully");*/
					json.put("message","Test Case(s) and Test Step(s) are downloaded successfully");
					/*Changed by Prem for V2.8-84 Ends*/
					/*Changed by Ashiki for V2.8-60 ends*/
					/*Changed by Pushpalatha for TENJINCG-567 starts*/
					/*modified by paneendra for VAPT fix starts*/
					/*json.put("path","Downloads"+File.separator +filename);*/
					json.put("path",File.separator+"Downloads"+File.separator +filename);
					/*modified by paneendra for VAPT fix ends*/
					/*Changed by Pushpalatha for TENJINCG-567 ends*/

				}catch(Exception e){
					logger.error("An exception occured while deleteing test steps",e);
					json.put("status", "ERROR");
					json.put("message","An Internal Error Occurred. Please try again");
				}finally{
					retData = json.toString();
				}
			}catch(Exception e){
				retData = "An Internal Error Occurred. Please contact your system administrator";
			}

			response.getWriter().write(retData);
		}
		else if(txn.equalsIgnoreCase("downloadtestcases_selected")){
			String tcList = request.getParameter("tclist");
			String[] array = tcList.split(",");
			ArrayList<Integer> tcs = new ArrayList<Integer>();
			for(String s:array){
				tcs.add(Integer.parseInt(s));
			}
			String retData = "";
			try{
				JSONObject json = new JSONObject();
				try{
					
					/*
					 Change by Nagababu to changed the file download file on 25-Mar-2015 Ends
					 */
					/*Changed by Pushpalatha for TENJINCG-567 starts*/
					/*modified by paneendra for VAPT fix starts*/
					//String file = folderPath +File.separator+ "Downloads";
					String folderPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
					File downloadPath= new File(folderPath+File.separator+"Downloads");
					if(!downloadPath.exists()){
						downloadPath.mkdirs();
					}
					String filename="TestCases.xlsx";
					/*modified by paneendra for VAPT fix ends*/
					/*Changed by Pushpalatha for TENJINCG-567 ends*/
					/*
					 Change by Nagababu to changed the file download file on 20-11-2014 Ends
					 */
					ExcelHandler e=new ExcelHandler();

					/******************************************************************
					 * Changed by Sriram for performance tuning of Bulk Test Cases download (17-10-2015)
					 */
					/*e.downloadtestcases(fileOutputStream,tc_folder_id);*/
					long startMillis = System.currentTimeMillis();
					/*Changed by Pushpalatha for TENJINCG-567 starts*/
					e.downloadTestCases(tcs, tjnSession.getProject().getId(), folderPath + File.separator+"Downloads", filename);
					/*Changed by Pushpalatha for TENJINCG-567 ends*/
					logger.info("Test Cases downloaded in {}ms", Utilities.calculateElapsedTime(startMillis, System.currentTimeMillis()));
					/******************************************************************
					 * Changed by Sriram for performance tuning of Bulk Test Cases download (17-10-2015) ends
					 */
					/*
					 Change by Nagababu to changed the file download file on 20-11-2014 Ends
					 */
					json.put("status", "SUCCESS");
					/*Changed by Ashiki for V2.8-53,V2.8-55,V2.8-84 starts*/
					/*json.put("message","TestCases and TestSets Data Downloaded successfully");*/
					json.put("message","Test Case(s) and Test Steps(s) are downloaded successfully");
					/*Changed by Ashiki for V2.8-53,V2.8-55,V2.8-84 ends*/
					/*Changed by Pushpalatha for TENJINCG-567 starts*/
					/*modified by paneendra for VAPT fix starts*/
					/*json.put("path","Downloads"+File.separator +filename);*/
					json.put("path",File.separator+"Downloads"+File.separator +filename);
					/*modified by paneendra for VAPT fix ends*/
					/*Changed by Pushpalatha for TENJINCG-567 ends*/

				}catch(Exception e){
					logger.error("An exception occured while deleteing test steps",e);
					json.put("status", "ERROR");
					json.put("message","An Internal Error Occurred. Please try again");
				}finally{
					retData = json.toString();
				}
			}catch(Exception e){
				retData = "An Internal Error Occurred. Please contact your system administrator";
			}

			response.getWriter().write(retData);
		}
		/***************************************************************
		 * Added by Sriram to download individual / only a few test cases (Tenjin v2.2) 19-10-2015 ends
		 */
		/*
		 Change by Nagababu to download excel file for all testcases and teststeps on 18-11-2014 Ends
		 */


		else if (txn.equalsIgnoreCase("tree")) {
			Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
			TestStep step = (TestStep) map.get("TEST_STEP");
			String finalString = "";
			try {
				logger.info("Getting Current Module sheet");
				Map<String, List<String>> currentSheetMap = new TestCaseHelper().fetchModulePageField(step.getAppId(),
						step.getModuleCode());

				logger.info("Setting map to session");
				/////////////////////////////

				JSONArray jArray = new JSONArray();
				int i = 0;
				for (Iterator<Map.Entry<String, List<String>>> entries = currentSheetMap.entrySet().iterator(); entries
						.hasNext();) {
					Map.Entry<String, List<String>> entry = entries.next();

					String sName = entry.getKey().toString();

					logger.info("Processing node for sheet {}", sName);
					String sheetId = "sheet_" + (i + 1);
					boolean mapSuccess = false;

					JSONObject json = new JSONObject();
					json.put("id", sheetId);
					json.put("text", sName);
					JSONObject liAttrs = new JSONObject();
					liAttrs.put("nodetype", "sheet");
					liAttrs.put("nodeid", sheetId);
					json.put("li_attr", liAttrs);

					List<String> fieldNodes = entry.getValue();
					logger.debug("{} field nodes found under this sheet node", fieldNodes.size());
					JSONArray fieldsArray = new JSONArray();
					for (int j = 0; j < fieldNodes.size(); j++) {
						String fieldNode = fieldNodes.get(j);

						String fName = fieldNode;
						boolean mapStatus = false;

						JSONObject field = new JSONObject();
						String fieldId = sheetId + "_field_" + (j + 1);
						field.put("id", fieldId);
						field.put("text", fName);

						JSONObject lAttrs = new JSONObject();
						lAttrs.put("nodetype", "field");
						lAttrs.put("nodeid", fieldId);
						List<String> manualMapFields = new TestCaseHelper().fetchManualMap(step.getAppId(),
								step.getModuleCode(), step.getId(), sName, step.getTestCaseRecordId());
						if (manualMapFields.size() > 0) {
							for (String fieldsName : manualMapFields) {
								if (fieldsName.equals(fName)) {
									mapStatus = true;
									break;
								}
							}
						}

						lAttrs.put("mapped", mapStatus);

						lAttrs.put("sourcesheet", sName);
						lAttrs.put("sourcefield", fName);

						field.put("li_attr", lAttrs);

						if (mapStatus) {
							field.put("icon", "jstree/mapped.png");
						} else {
							field.put("icon", "jstree/unmapped.png");
						}

						fieldsArray.put(field);
					}
					
					if (mapSuccess) {
						json.put("icon", "");
					} else {
						json.put("icon", "");
					}
					/* Modified by Padmavathi for TENJINCG-1001 ends */
					json.put("children", fieldsArray);
					jArray.put(json);
					i++;
				}

				finalString = jArray.toString();
				logger.info("Final JSON Generated below");
				logger.info(finalString);

				response.getWriter().write(finalString);
			} catch (Exception e) {
				response.getWriter().write("Internal error");
			}
		} /*********************************************************************
			 * Method added by nagareddy (18-11-2016) for manual input field save
			 */
		else if (txn.equalsIgnoreCase("TEST_STEP_MANUAL")) {
			String j = request.getParameter("json");
			String retData = "";
			try {
				JSONObject retJson = new JSONObject();

				JSONObject json = new JSONObject(j);

				TestStep step = new TestStep();
				step.setId(json.getString("stepid"));
				step.setTestCaseRecordId(json.getInt("recid"));
				step.setTestCaseName(json.getString("tcid"));
				step.setAppId(json.getInt("app"));
				step.setModuleCode(json.getString("module"));
				///////////// manual fields//////////////
				String manualfields = request.getParameter("manual");

				if (!manualfields.isEmpty()) {
					logger.info("manualfields {} ", manualfields);
					try {

						JSONArray manualObj = new JSONArray(manualfields);
						new TestCaseHelper().insertOrUpdateManualField(json.getInt("app"), json.getString("module"),
								json.getString("stepid"), manualObj, step.getTestCaseRecordId());
						
						retJson.put("status", "SUCCESS");
						retJson.put("message", "Details updated successfully");
					} catch (JSONException e) {
						
						retJson.put("status", "ERROR");
						retJson.put("message", "Update Failed due to an internal error");
					}
					////////////////////// end of manual fields//////////////
					finally {
						retData = retJson.toString();
					}
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);
		}
		/* Changed by Naga Reddy to Defect fix TEN-42 on 19-12-2016 starts */
		else if (txn.equalsIgnoreCase("TEST_STEP_UNMAP_MANUAL")) {
			String j = request.getParameter("json");
			String retData = "";
			try {
				JSONObject retJson = new JSONObject();

				JSONObject json = new JSONObject(j);

				TestStep step = new TestStep();
				step.setId(json.getString("stepid"));
				step.setTestCaseRecordId(json.getInt("recid"));
				step.setTestCaseName(json.getString("tcid"));
				step.setAppId(json.getInt("app"));
				step.setModuleCode(json.getString("module"));

				try {

					new TestCaseHelper().unMapManualField(json.getInt("app"), json.getString("module"),
							json.getString("stepid"), json.getString("tcid"), step.getTestCaseRecordId());
					
					retJson.put("status", "SUCCESS");
					retJson.put("message", "Unmapped successfully");
				} catch (JSONException e) {
					
					retJson.put("status", "ERROR");
					retJson.put("message", "Update Failed due to an internal error");
				}
				////////////////////// end of manual fields//////////////
				finally {
					retData = retJson.toString();
				}

			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);
		}

		/* Changed by Naga Reddy to Defect fix TEN-4 on 20-12-2016 starts */
		else if (txn.equalsIgnoreCase("TEST_STEP_UNMAPSINGLE_MANUAL")) {
			String j = request.getParameter("json");
			String unMapSheet = request.getParameter("sheet");
			String unMapField = request.getParameter("field");
			String retData = "";
			try {
				JSONObject retJson = new JSONObject();

				JSONObject json = new JSONObject(j);

				TestStep step = new TestStep();
				step.setId(json.getString("stepid"));
				step.setTestCaseRecordId(json.getInt("recid"));
				step.setTestCaseName(json.getString("tcid"));
				step.setAppId(json.getInt("app"));
				step.setModuleCode(json.getString("module"));

				try {

					new TestCaseHelper().unMapSingleManualField(json.getInt("app"), json.getString("module"),
							json.getString("stepid"), json.getString("tcid"), step.getTestCaseRecordId(), unMapSheet,
							unMapField);
					
					retJson.put("status", "SUCCESS");
					retJson.put("message", "Unmapped successfully for field '" + unMapField + "'");
				} catch (JSONException e) {
					
					retJson.put("status", "ERROR");
					retJson.put("message", "Update Failed due to an internal error");
				}
				////////////////////// end of manual fields//////////////
				finally {
					retData = retJson.toString();
				}

			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);
		}

		/* Changed by Naga Reddy to Defect fix TEN-42 on 20-12-2016 ends */

		/* added by manish for req TENJINCG-54 on 24-01-2017 starts */
		else if (txn.equalsIgnoreCase("FETCH_TM_USER_CREDENTIALS")) {
			List<TestManagerInstance> tms = new ArrayList<TestManagerInstance>();
			TestCaseHelper helper = new TestCaseHelper();
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				tms = helper.hydrateTmUserCredentials(user.getId());
				map.put("TM_USER_CREDENTIALS_LIST", tms);
			} catch (Exception e) {
				logger.error("Error during hydrating TM user credential list", e);
			}
			/* Added by Ashiki for V2.8-118 starts */
			String message = request.getParameter("message");
			if (message != null) {
				map.put("MESSAGE", message);
			} else {
				map.put("MESSAGE", "");
			}
			/* Added by Ashiki for V2.8-118 ends */
			request.getSession().setAttribute("SCR_MAP", map);
			response.sendRedirect("tmuserlist.jsp");

		} else if (txn.equalsIgnoreCase("new_tm_user")) {
			List<String> instances = new ArrayList<String>();
			Map<String, Object> map = new HashMap<String, Object>();
			TestCaseHelper helper = new TestCaseHelper();
			try {
				instances = helper.fetchAllInstances();
				map.put("instances", instances);
			} catch (Exception e) {
				logger.error("Could not hydrate instances for TM", e);
			}
			request.getSession().setAttribute("SCR_MAP", map);
			/* changed by manish for bug T25IT-284 on 30-Aug-2017 starts */
			/* response.sendRedirect("tmuserdetails.jsp"); */
			/* changed by manish for bug T25IT-284 on 30-Aug-2017 ends */

		} else if (txn.equalsIgnoreCase("PERSIST_TM_USER_CREDENTIALS")) {
			String jstring = request.getParameter("paramval");
			JSONObject rJson = new JSONObject();
			String instance = "";
			try {
				JSONObject json = new JSONObject(jstring);
				TestManagerInstance dm = new TestManagerInstance();
				TestCaseHelper tcHelper = new TestCaseHelper();
				/* Added By Ashiki for TM user credentials starts */
				/* String pwd = new Crypto().encrypt(json.getString("loginPwd")); */
				/*Modified by paneendra for VAPT FIX starts*/
				/*String pwd = new Crypto().encrypt(request.getParameter("loginPwd"));*/
				String pwd = new CryptoUtilities().encrypt(request.getParameter("loginPwd"));
				/*Modified by paneendra for VAPT FIX ends*/
				/* Added By Ashiki for TM user credentials starts */
				dm.setUsername(json.getString("loginId"));
				dm.setInstanceName(json.getString("instance"));
				instance = json.getString("instance");
				dm.setTjnUsername(json.getString("userId"));
				dm.setPassword(pwd);

				tcHelper.validateUserTMLoginName(instance, json.getString("userId"));
				tcHelper.persistTmUserCredentials(dm);
				rJson.put("status", "SUCCESS");
				rJson.put("message", "Test Management User created successfully");
			} catch (DatabaseException e) {
				try {
					logger.debug("Duplicate record found for TM");
					rJson.put("status", "ERROR");
					/* Chagned By Padmavathi for T25IT-412 starts */
					rJson.put("message", e.getMessage());
					/* Chagned By Padmavathi for T25IT-412 ends */

				} catch (JSONException e1) {

				}
			} catch (JSONException e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message", "An Internal error occured please contact Administrator");
					logger.error("Error during mapping TM user with Tenjin user", e);
				} catch (JSONException e1) {
				}
			} catch (Exception e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message", "An Internal error occured please contact Administrator");
					logger.error("Error during mapping TM user with Tenjin user", e);
				} catch (JSONException e1) {
				}
			}

			response.getWriter().write(rJson.toString());
		} else if (txn.equalsIgnoreCase("hydrate_tm_user")) {
			String userId = user.getId();
			String instance = request.getParameter("instance");
			TestCaseHelper helper = new TestCaseHelper();
			TestManagerInstance tmInstance = new TestManagerInstance();
			/* Added by Padmavathi for T251IT-107 starts */
			List<String> instances = new ArrayList<String>();
			/* Added by Padmavathi for T251IT-107 ends */
			Map<String, Object> map = new HashMap<String, Object>();

			try {
				tmInstance = helper.hydrateTMUserDetails(userId, instance);
				/*Modified by paneendra for VAPT Fix starts*/
				if(tmInstance.getPassword()!=null && tmInstance.getUsername()!=null ) {
					/* Added by Padmavathi for T251IT-107 starts */
					instances = helper.fetchAllInstances();
					map.put("instances", instances);
					/* Added by Padmavathi for T251IT-107 ends */
					map.put("tmInstance", tmInstance);
					request.getSession().setAttribute("SCR_MAP", map);
					response.sendRedirect("edittmuserdetails.jsp");
				}else {
					RequestDispatcher dispatcher = request.getRequestDispatcher("noAccess.jsp");
					dispatcher.forward(request, response);
				}
			} catch (Exception e) {
				logger.error("Error while fetching details of a TM Instance user credentials", e);
			}
			
			/*Modified by paneendra for VAPT Fix ends*/
		} else if (txn.equalsIgnoreCase("delete_TM_user")) {
			String uids = request.getParameter("userids");
			String instances = request.getParameter("tmInstances");
			String[] userids = uids.split(",");
			String[] tmInstances = instances.split(",");
			JSONObject retJson = new JSONObject();
			String retData = "";
			TestCaseHelper dmHelper = new TestCaseHelper();
			/*Modified by paneendra for VAPT Fix starts*/
			boolean deleteResult=false;
			try {
				try {
					deleteResult=dmHelper.deleteTMUserByUserId(userids, tmInstances, user.getId());
					if(deleteResult==true) {
					retJson.put("status", "SUCCESS");
					/* Changed by Ashiki for V2.8-62 starts */
					retJson.put("message", "Test Management user(s) Deleted Successfully");
					/* Changed by Ashiki for V2.8-62 ends */
					}else {
						RequestDispatcher dispatcher = request.getRequestDispatcher("noAccess.jsp");
						dispatcher.forward(request, response);
					}
					/*Modified by paneendra for VAPT Fix ends*/
				} catch (Exception e) {
					retJson.put("status", "ERROR");
					/* Changed by Ashiki for V2.8-62 starts */
					retJson.put("message", "Could not delete Test Management user(s)");
					/* Changed by Ashiki for V2.8-62 ends */
				} finally {
					retData = retJson.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact System Administrator";
				logger.error("error during deleting Test Management user");
			}
			response.getWriter().write(retData);
		} else if (txn.equalsIgnoreCase("check_tm_user_name")) {
			JSONObject resJSON = new JSONObject();
			String instance = request.getParameter("instance");
			/* Added by Preeti for T251IT-140 starts */
			String oldinstance = request.getParameter("oldinstance");
			/* Added by Preeti for T251IT-140 ends */
			String userId = request.getParameter("userId");
			String retData = "";
			try {
				try {
					TestCaseHelper helper = new TestCaseHelper();
					/* Modified by Preeti for T251IT-140 starts */
					if (!instance.equalsIgnoreCase(oldinstance))
						helper.validateUserTMLoginName(instance, userId);
					/* Modified by Preeti for T251IT-140 ends */
				} catch (Exception e) {
					logger.debug("Duplicate record found");
					resJSON.put("status", "ERROR");
					/* Modified by Padmavathi for T251IT-107 starts */
					resJSON.put("message", "You have already specified credentials for " + instance
							+ ". Please use a different instance and try again.");
					/* Modified by Padmavathi for T251IT-107 ends */
				} finally {
					retData = resJSON.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator ";
			}
			response.getWriter().write(retData);
		} else if (txn.equalsIgnoreCase("update_tm_user_credentials")) {
			String jString = request.getParameter("json");
			TestManagerInstance tmInstance = new TestManagerInstance();
			TestCaseHelper helper = new TestCaseHelper();
			String retData = "";
			JSONObject retJson = new JSONObject();
			boolean updateResult=false;
			try {
				JSONObject json = new JSONObject(jString);
				tmInstance.setTjnUsername(user.getId());
				tmInstance.setInstanceName(json.getString("instance"));
				tmInstance.setUsername(json.getString("tmUser"));
				/* Added By Ashiki for TM user credentials starts */
				/* tmInstance.setPassword(json.getString("tmPwd")); */
				tmInstance.setPassword(request.getParameter("tmPwd"));
				/* Added By Ashiki for TM user credentials ends */
				/* Modified by Padmavathi for T251IT-107 starts */
				String oldInstance = (json.getString("oldInstance"));
				/* Modified by Preeti for T251IT-140 starts */
				if (!tmInstance.getInstanceName().equalsIgnoreCase(oldInstance))
					helper.validateUserTMLoginName(json.getString("instance"), user.getId());
				/* Modified by Preeti for T251IT-140 ends */
				/* helper.updateTMUserCredentials(tmInstance); */
				/*Modified by paneendra for VAPT Fix starts*/
				/*helper.updateTMUserCredentials(tmInstance, oldInstance);*/
				updateResult=helper.updateTMUserCredentialsByUserId(tmInstance, oldInstance);
				if(updateResult==true) {
					/* Modified by Padmavathi for T251IT-107 ends */
					retJson.put("status", "SUCCESS");
					retJson.put("message", "Test Management user details Updated Successfully");
				}else {
					RequestDispatcher dispatcher = request.getRequestDispatcher("noAccess.jsp");
					dispatcher.forward(request, response);
				}
				/*Modified by paneendra for VAPT Fix ends*/
			} catch (Exception e) {
				try {
					retJson.put("status", "ERROR");
					/* Modified by Padmavathi for T251IT-107 starts */
					retJson.put("message", e.getMessage());
					/* Modified by Padmavathi for T251IT-107 ends */
				} catch (JSONException e1) {
					retData = "An internal error occurred. Please contact System Administrator";
				}

			} finally {
				retData = retJson.toString();
			}
			response.getWriter().write(retData);
		}
		/* added by manish for req TENJINCG-54 on 24-01-2017 ends */

		/* added by manish for req TENJINCG-59 on 25-01-2017 starts */
		else if (txn.equalsIgnoreCase("fetch_tm_testcases")) {
			int projectId = tjnSession.getProject().getId();
			String userId = tjnSession.getUser().getId();
			ProjectHelper phelper = new ProjectHelper();
			Map<String, Object> map = new HashMap<String, Object>();
			TestCaseHelper helper = new TestCaseHelper();
			List<com.ycs.tenjin.testmanager.TestStep> allSteps = new ArrayList<com.ycs.tenjin.testmanager.TestStep>();
			TestManagerInstance tmInstance = new TestManagerInstance();
			try {
				Project prj = phelper.hydrateProject(projectId);
				helper.hydrateFilters(prj);
				/*
				 * changed by manish to fetch etm project and domain for a perticular tenjin
				 * project on 29-Aug-2017 starts
				 */
				tmInstance = helper.hydrateTMCredentials(prj.getEtmInstance(), userId, projectId);
				/*
				 * changed by manish to fetch etm project and domain for a perticular tenjin
				 * project on 29-Aug-2017 ends
				 */
				/*Modified by paneendra for VAPT FIX starts*/
				String decryptedPwd =new CryptoUtilities().decrypt(tmInstance.getPassword());
				/*Modified by paneendra for VAPT FIX ends*/
				// Added by sakthi on 07-04-2017 starts
				/*
				 * AlmTmImpl alm=new AlmTmImpl(tmInstance.getURL(), tmInstance.getUsername(),
				 * decryptedPwd);
				 */
				TestManager alm = null;
				alm = TestManagerFactory.getTestManagementTools(tmInstance.getTool(), tmInstance.getURL(),
						tmInstance.getUsername(), decryptedPwd);
				// Added by sakthi on 07-04-2017 ends

				alm.login();
				alm.setProjectDesc(tmInstance.getTmProjectDomain());
				List<com.ycs.tenjin.testmanager.TestCase> tcList = alm.getTestCases(prj.getTcFilter(),
						prj.getTcFilterValue());
				List<com.ycs.tenjin.testmanager.TestCase> tCaseList = new ArrayList<com.ycs.tenjin.testmanager.TestCase>();
				allSteps = alm.getTestStepsWithFilter(prj.getTsFilter(), prj.getTsFilterValue());
				for (com.ycs.tenjin.testmanager.TestCase tCase : tcList) {
					List<com.ycs.tenjin.testmanager.TestStep> tsList = alm.getTestSteps(prj.getTsFilter(),
							prj.getTsFilterValue(), tCase.getTcId());
					if (tsList != null && tsList.size() > 0) {
						JSONArray jArray = new JSONArray();
						for (com.ycs.tenjin.testmanager.TestStep ts : tsList) {
							JSONObject json = new JSONObject();
							json.put("dataId", ts.getDataId());
							json.put("recordId", ts.getRecordId());
							json.put("description", ts.getDescription());
							json.put("type", ts.getType());
							json.put("moduleCode", ts.getModuleCode());
							json.put("moduleName", ts.getModuleName());
							json.put("appName", ts.getAppName());
							json.put("parentTestCaseId", ts.getParentTestCaseId());
							json.put("rowsToExecute", ts.getRowsToExecute());
							jArray.put(json);
						}
						tCase.setTcSteps(tsList);
						tCase.setStepsArray(jArray);
					}

					tCaseList.add(tCase);
				}
				
				alm.logout();
				request.getSession().removeAttribute("SCR_MAP");
				map.put("tcList", tCaseList);
				map.put("tsList", allSteps);
				map.put("STATUS", "success");
				map.put("MESSAGE", "");

			} catch (DatabaseException e) {
				map.put("STATUS", "error");
				map.put("MESSAGE", "An Internal error occored, Please contact tenjin support");
				logger.error("could not fetch filters for project {}", projectId, e);
			}  catch (UnreachableETMException e) {
				
				logger.error("Could not able to login into TM tool.", e);
				
			} catch (ETMException e) {
				
				logger.error("Could not able to login into TM tool.", e);
				
			} catch (JSONException e) {
				logger.error("Error while putting imported test steps into json", e);
			} catch (Exception e) {
				logger.error("Error while putting imported test steps into json", e);
			}
			request.getSession().setAttribute("SCR_MAP", map);
			RequestDispatcher rd = request.getRequestDispatcher("importedtclist.jsp");
			rd.forward(request, response);

		} else if (txn.equalsIgnoreCase("persist_imported_testcases")) {
			String testcases = request.getParameter("tcids");
			String tstepIds = request.getParameter("tsteps");
			String dUrl = "";
			String[] array = testcases.split(",");
			String[] stepArray = tstepIds.split(",");
			boolean exist = false;

			/* added by manish to display exception messages/ imported logs starts */
			List<String> listExceptions = new ArrayList<String>();
			/* added by manish to display exception messages/ imported logs ends */

			TestCaseHelper helper = new TestCaseHelper();
			Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
			List<com.ycs.tenjin.testmanager.TestCase> tcs = (ArrayList<com.ycs.tenjin.testmanager.TestCase>) map
					.get("tcList");
			List<com.ycs.tenjin.testmanager.TestStep> allSteps = (ArrayList<com.ycs.tenjin.testmanager.TestStep>) map
					.get("tsList");
			List<com.ycs.tenjin.testmanager.TestCase> importedTcs = new ArrayList<com.ycs.tenjin.testmanager.TestCase>();
			
			for (com.ycs.tenjin.testmanager.TestCase tc : tcs) {
				for (int i = 0; i < array.length; i++) {
					if (array[i].equalsIgnoreCase(tc.getTcId())) {
						importedTcs.add(tc);
					}
				}
			}

			for (com.ycs.tenjin.testmanager.TestCase tc : importedTcs) {
				TestCase tjnTc = new TestCase();
				tjnTc.setTcId(tc.getTcId());
				tjnTc.setTcName(tc.getTcName());
				tjnTc.setTcDesc(tc.getTcDesc());
				tjnTc.setTcType(tc.getTcType());
				tjnTc.setTcFolderId(tc.getTcFolderId());
				tjnTc.setTcPriority(tc.getTcPriority());
				tjnTc.setTcCreatedBy(tc.getTcCreatedBy());
				tjnTc.setProjectId(tjnSession.getProject().getId());
				tjnTc.setRecordType("TC");
				tjnTc.setStorage("Remote");
				/* changed by manish for bug T25IT-236 on 19-Aug-2017 starts */
				tjnTc.setMode(tc.getMode());
				/* changed by manish for bug T25IT-236 on 19-Aug-2017 ends */
				try {
					exist = helper.checkTestCase(tjnTc.getTcId(), tjnSession.getProject().getId());
					if (!exist) {
						helper.persistTestCaseTuned(tjnTc);
					} else {
						listExceptions.add("Test Case '" + tc.getTcId() + "' already exist.");
						continue;
					}

				} catch (DatabaseException e) {
					logger.error("Could not able to persist imported tc from tm", e);
					/* added by manish to display exception messages/ imported logs starts */
					listExceptions
							.add("Could not import test case '" + tc.getTcId() + "', Please contact Tenjin support");
					continue;
					/* added by manish to display exception messages/ imported logs ends */
				}

			}
			if (allSteps != null && allSteps.size() > 0) {
				List<com.ycs.tenjin.testmanager.TestStep> importedSteps = new ArrayList<com.ycs.tenjin.testmanager.TestStep>();
				for (com.ycs.tenjin.testmanager.TestStep tstep : allSteps) {
					for (int i = 0; i < stepArray.length; i++) {
						if (stepArray[i].equalsIgnoreCase(tstep.getDataId())) {
							importedSteps.add(tstep);
						}
					}
				}
				for (com.ycs.tenjin.testmanager.TestStep tstep : importedSteps) {
					TestStep tjnStep = new TestStep();
					/* added by manish to display exception messages/ imported logs starts */
					TestCase objTestCase = null;
					int appId = 0;
					List<Module> listFunctions = new ArrayList<Module>();
					/* added by manish to display exception messages/ imported logs ends */
					tjnStep.setId(tstep.getId());
					tjnStep.setDataId(tstep.getDataId());
					tjnStep.setDescription(tstep.getDescription());
					/* changed by manish to display exception messages/ imported logs starts */
					
					tjnStep.setAppName(tstep.getAppName());
					try {
						appId = new AutHelper().hydrateAppId(tjnSession.getProject().getId(), tstep.getAppName());
						if (appId > 0) {
							tjnStep.setAppId(appId);
						} else {
							logger.error("app " + tstep.getAppName() + " is not mapped to Tenjin project.");
							listExceptions.add("Could not import test step '" + tstep.getId()
									+ "' because application '" + tstep.getAppName() + "' is not mapped with project '"
									+ tjnSession.getProject().getName() + "'");
							continue;
						}

					} catch (DatabaseException e2) {
						logger.error(e2.getMessage() + "(test step -- " + tstep.getId() + ")");
						listExceptions.add("Could not import test step '" + tstep.getId() + "' because application '"
								+ tstep.getAppName() + "' is not mapped with project '"
								+ tjnSession.getProject().getName() + "'");
						continue;
					}
					boolean moduleFound = false;
					try {
						listFunctions = new AutHelper().hydrateFunctions(appId);
						if (listFunctions.size() <= 0) {
							listExceptions
									.add("Could not import test step '" + tstep.getId() + "' because application '"
											+ tstep.getAppName() + "' does not contain any function");
							continue;
						}

						for (int i = 0; i < listFunctions.size(); i++) {
							if (tstep.getModuleCode().equalsIgnoreCase(listFunctions.get(i).getCode())) {
								tjnStep.setModuleCode(listFunctions.get(i).getCode());
								moduleFound = true;
							}
						}
						if (!moduleFound) {
							listExceptions.add("Could not import test step '" + tstep.getId()
									+ "' because application '" + tstep.getAppName() + "' does not contain function '"
									+ tstep.getModuleCode() + "'");
							continue;
						}

					} catch (DatabaseException e) {
						logger.error(e.getMessage() + "(test step -- " + tstep.getId() + ")");
						listExceptions.add("Could not import test step '" + tstep.getId() + "' because application '"
								+ tstep.getAppName() + "' does not contain function code " + tstep.getModuleCode());
						continue;
					}
					tjnStep.setRowsToExecute(tstep.getRowsToExecute());
					try {
						objTestCase = new TestCaseHelper().hydrateTestCase(tjnSession.getProject().getId(),
								tstep.getParentTestCaseId());
					} catch (DatabaseException e1) {
						logger.error(e1.getMessage() + "(test step -- " + tstep.getId() + ")");
						listExceptions.add(
								"Could not import test step '" + tstep.getId() + "', Please contact Tenjin support");
						continue;
					}
					/* changed by manish to display exception messages/ imported logs ends */
					tjnStep.setTestCaseRecordId(objTestCase.getTcRecId());
					tjnStep.setShortDescription(tstep.getShortDescription());
					tjnStep.setType(tstep.getType());
					tjnStep.setExpectedResult(tstep.getExpectedResult());
					tjnStep.setOperation(tstep.getOperation());
					tjnStep.setTxnMode(tstep.getTxnMode());
					tjnStep.setAutLoginType(tstep.getAutLoginType());
					tjnStep.setTstepStorage("Remote");
					try {
						/*
						 * changed by manish to check if test step already exist or not on 29-Aug-2017
						 * starts
						 */
						exist = helper.checkImportedTestStep(tjnStep, objTestCase.getTcRecId());
						/*
						 * changed by manish to check if test step already exist or not on 29-Aug-2017
						 * starts
						 */
						if (!exist) {
							helper.persistTestStep(tjnStep);
						}

					} catch (DatabaseException e) {
						logger.error("Could not able to persist imported tstep from tm", e);
					}
				}
			}
			/* changed by manish to display exception messages/ imported logs starts */
			for (int i = 0; i < listExceptions.size(); i++) {
				listExceptions.set(i, i + 1 + ")" + listExceptions.get(i));
			}
			String msg = "";

			map.put("STATUS", "SUCCESS");
			map.put("listExceptions", listExceptions);
			/* Changed by Pushpalatha for TenjinCg-611 starts */
			request.setAttribute("listExceptions", listExceptions);
			/* Changed by Pushpalatha for TenjinCg-611 starts */
			if (listExceptions.size() > 0) {
				
				msg = "Test Cases and Test Steps imported successfully, Please click 'Logs' button for more Info";
			} else {
				msg = "Test Cases and Test Steps imported successfully";
			}
			/* changed by manish to display exception messages/ imported logs ends */
			/* Changed by Pushpalatha for TENJINCG-611 starts */
			dUrl = "TestCaseServletNew?t=list&msg=" + msg;
			/* Changed by Pushpalatha for TENJINCG-611 ends */

			request.getSession().removeAttribute("SCR_MAP");
			request.getSession().setAttribute("SCR_MAP", map);
			response.sendRedirect(dUrl);

		}

		/* added by manish for req TENJINCG-59 on 25-01-2017 ends */

		/* added by manish for req TENJINCG-57 and TENJINCG-58 on 25-01-2017 starts */
		else if (txn.equalsIgnoreCase("fetch_tc_and_ts_from_tm")) {
			int projectId = tjnSession.getProject().getId();
			String userId = tjnSession.getUser().getId();
			ProjectHelper phelper = new ProjectHelper();
			Map<String, Object> map = new HashMap<String, Object>();
			TestCaseHelper helper = new TestCaseHelper();
			TestManagerInstance tmInstance = new TestManagerInstance();
			Project prj = null;
			try {
				prj = phelper.hydrateProject(projectId);
				helper.hydrateFilters(prj);
				/*
				 * changed by manish to fetch etm project and domain for a perticular tenjin
				 * project on 29-Aug-2017 starts
				 */
				tmInstance = helper.hydrateTMCredentials(prj.getEtmInstance(), userId, projectId);
				/*
				 * changed by manish to fetch etm project and domain for a perticular tenjin
				 * project on 29-Aug-2017 ends
				 */
				/*Modified by paneendra for VAPT FIX starts*/
				
				String decryptedPwd = new CryptoUtilities().decrypt(tmInstance.getPassword());
				/*Modified by paneendra for VAPT FIX ends*/
				// Added by sakthi on 07-04-2017 starts
				
				TestManager alm = null;
				alm = TestManagerFactory.getTestManagementTools(tmInstance.getTool(), tmInstance.getURL(),
						tmInstance.getUsername(), decryptedPwd);
				/* Review comments end */
				// Added by sakthi on 07-04-2017 ends
				alm.login();
				alm.setProjectDesc(tmInstance.getTmProjectDomain());
				List<String> tsFields = alm.getTestStep();
				List<String> tcFields = alm.getTestCase();
				alm.logout();
				request.getSession().removeAttribute("SCR_MAP");
				map.put("tcFields", tcFields);
				map.put("tsFields", tsFields);
				map.put("project", prj);
				request.getSession().setAttribute("SCR_MAP", map);
				RequestDispatcher rd = request.getRequestDispatcher("maptmattributes.jsp");
				rd.forward(request, response);

			} catch (DatabaseException e) {
				logger.error("could not fetch tc and ts from etm for instance {}", prj.getEtmInstance(), e);
			}  catch (UnreachableETMException e) {
				
				logger.error("Could not able to login into TM tool.", e);
				
			} catch (ETMException e) {
				

				logger.error("Could not able to login into TM tool.", e);

			
			} catch (TenjinConfigurationException e) {
				
				logger.error("Error ", e);
			}
		} else if (txn.equalsIgnoreCase("map_tm_attributes")) {
			JSONObject rJson = new JSONObject();
			TestManagerInstance tmInstance = new TestManagerInstance();
			TestCaseHelper helper = new TestCaseHelper();
			String jstring = request.getParameter("jstring");
			int projectId = Integer.parseInt(request.getParameter("projectId"));
			boolean update = false;
			try {
				JSONArray jArr = new JSONArray(jstring);
				for (int i = 0; i < jArr.length(); i++) {
					JSONObject json = new JSONObject();
					json = jArr.getJSONObject(i);
					tmInstance.setTjnField(json.getString("tjnField"));
					tmInstance.setTmField(json.getString("tmField"));
					tmInstance.setRec_type(json.getString("rec_type"));
					update = helper.tmAlreadyMapped(projectId, json.getString("tjnField"));
					if (update) {
						helper.updateMappedAttributes(projectId, tmInstance);
					} else {
						helper.persistMappedAttributes(projectId, tmInstance);
					}

				}

				rJson.put("status", "SUCCESS");
				rJson.put("message", "Map Attributes updated successfully");

			} catch (JSONException e) {
				logger.error("could not convert tm attribute string list to json object", e);
			} catch (DatabaseException e) {
				try {
					throw new DatabaseException(
							"Could not map fields due to some Internal error, Please contact Tenjin Support", e);
				} catch (DatabaseException e1) {

				}
			}

			response.getWriter().write(rJson.toString());

		}

		/* added by manish for req TENJINCG-57 and TENJINCG-58 on 25-01-2017 ends */

		/* added by manish for req TENJINCG-60 on 3-01-2017 starts */
		else if (txn.equalsIgnoreCase("sync_test_cases")) {
			String tcIds = request.getParameter("paramval");
			int projectId = tjnSession.getProject().getId();
			String userId = tjnSession.getUser().getId();
			List<String> tcs = new ArrayList<String>();
			JSONArray tcId = null;
			List<String> listExceptions = new ArrayList<String>();
			String source = request.getParameter("source");
			
			if (tcIds != null)
				try {
					tcId = new JSONArray(tcIds);
				} catch (JSONException e1) {
					logger.error("Could not fetch details due to " + e1.getMessage());

				}

			for (int i = 0; i < tcId.length(); i++) {
				JSONObject j = new JSONObject();
				try {
					j = tcId.getJSONObject(i);
				
					tcs.add(j.getString("tcid"));
				} catch (JSONException e) {
					logger.error("Could not fetch details due to " + e.getMessage());

				}
			}
			Project prj = null;
			ProjectHelper phelper = new ProjectHelper();
			TestCaseHelper helper = new TestCaseHelper();
			TestManagerInstance tmInstance = new TestManagerInstance();
			Map<String, Object> map = new HashMap<String, Object>();
			String dUrl = "";
			try {
				prj = phelper.hydrateProject(projectId);
				helper.hydrateFilters(prj);
				/*
				 * changed by manish to fetch etm project and domain for a perticular tenjin
				 * project on 29-Aug-2017 starts
				 */
				tmInstance = helper.hydrateTMCredentials(prj.getEtmInstance(), userId, projectId);
				/*
				 * changed by manish to fetch etm project and domain for a perticular tenjin
				 * project on 29-Aug-2017 ends
				 */
				/*Modified by paneendra for VAPT FIX starts*/
				
				String decryptedPwd =new CryptoUtilities().decrypt(tmInstance.getPassword());
				/*Modified by paneendra for VAPT FIX ends*/
				
				/* Added By Ashiki for TJN252-60 starts */
				Map<String, String> mapAttributes = new TreeMap<String, String>();
				mapAttributes = helper.hydrateMappedAttribute(projectId);
				/* Added By Ashiki for TJN252-60 ends */
				TestManager alm = null;
				alm = TestManagerFactory.getTestManagementTools(tmInstance.getTool(), tmInstance.getURL(),
						tmInstance.getUsername(), decryptedPwd);
				/* Review comments end */
				alm.login();
				alm.setProjectDesc(tmInstance.getTmProjectDomain());
				/* changed By Ashiki for TJN252-60 starts */
				
				List<com.ycs.tenjin.testmanager.TestCase> updatedTcs = alm.SynchronizeTestCases(tcs, mapAttributes);
				/* changed By Ashiki for TJN252-60 ends */
				for (com.ycs.tenjin.testmanager.TestCase tc : updatedTcs) {
					TestCase tjnTc = new TestCase();
					for (int i = 0; i < tcId.length(); i++) {
						if (tc.getTcId().equalsIgnoreCase(tcId.getJSONObject(i).getString("tcid"))) {
							tjnTc.setTcRecId(Integer.parseInt(tcId.getJSONObject(i).getString("tcrecid")));
						}
						

					}

					tjnTc = helper.hydrateTestCase(projectId, tjnTc.getTcRecId());

					if (tjnTc.getTcSteps() != null && tjnTc.getTcSteps().size() > 0) {
						for (TestStep tstep : tjnTc.getTcSteps()) {
							helper.deleteTestStep(tstep);
						}
					}

					tjnTc.setTcId(tc.getTcId());
					tjnTc.setTcName(tc.getTcName());
					tjnTc.setTcDesc(tc.getTcDesc());
					tjnTc.setTcType(tc.getTcType());
					tjnTc.setTcFolderId(tc.getTcFolderId());
					tjnTc.setTcPriority(tc.getTcPriority());
					tjnTc.setTcCreatedBy(tc.getTcCreatedBy());
					tjnTc.setProjectId(tjnSession.getProject().getId());
					tjnTc.setRecordType("TC");
					tjnTc.setStorage("Remote");
					tjnTc.setMode(tc.getMode());
					helper.updateTestCase(tjnTc);

					List<com.ycs.tenjin.testmanager.TestStep> listSteps = new ArrayList<com.ycs.tenjin.testmanager.TestStep>();
					/* changed By Ashiki for TJN252-60 starts */
					
					listSteps = alm.getTestSteps(prj.getTsFilter(), prj.getTsFilterValue(), tc.getTcId(),
							mapAttributes);
					/* changed By Ashiki for TJN252-60 ends */
					/* Added by Padmavathi for TNJN27-69 & TNJN27-68 starts */
					if (listSteps != null) {
						/* Added by Padmavathi for TNJN27-69 & TNJN27-68ends */
						for (com.ycs.tenjin.testmanager.TestStep tstep : listSteps) {
							TestStep tjnStep = new TestStep();
							/* added by manish to display exception messages/ imported logs starts */
							int appId = 0;
							List<Module> listFunctions = new ArrayList<Module>();
							/* added by manish to display exception messages/ imported logs ends */
							tjnStep.setId(tstep.getId());
							tjnStep.setDataId(tstep.getDataId());
							tjnStep.setDescription(tstep.getDescription());
							/* changed by manish to display exception messages/ imported logs starts */
							

							tjnStep.setAppName(tstep.getAppName());
							try {
								appId = new AutHelper().hydrateAppId(tjnSession.getProject().getId(),
										tstep.getAppName());
								if (appId > 0) {
									tjnStep.setAppId(appId);
								} else {
									logger.error("app " + tstep.getAppName() + " is not mapped to Tenjin project.");
									listExceptions
											.add("Could not get test step '" + tstep.getId() + "' because application '"
													+ tstep.getAppName() + "' is not mapped with project '"
													+ tjnSession.getProject().getName() + "'");
									continue;
								}

							} catch (DatabaseException e2) {
								logger.error(e2.getMessage() + "(test step -- " + tstep.getId() + ")");
								listExceptions.add("Could not get test step '" + tstep.getId()
										+ "' because application '" + tstep.getAppName()
										+ "' is not mapped with project '" + tjnSession.getProject().getName() + "'");
								continue;
							}
							boolean moduleFound = false;
							try {
								listFunctions = new AutHelper().hydrateFunctions(appId);
								if (listFunctions.size() <= 0) {
									listExceptions
											.add("Could not get test step '" + tstep.getId() + "' because application '"
													+ tstep.getAppName() + "' does not contain any function");
									continue;
								}

								for (int i = 0; i < listFunctions.size(); i++) {
									if (tstep.getModuleCode().equalsIgnoreCase(listFunctions.get(i).getCode())) {
										tjnStep.setModuleCode(listFunctions.get(i).getCode());
										moduleFound = true;
									}
								}
								if (!moduleFound) {
									listExceptions.add("Could not get test step '" + tstep.getId()
											+ "' because application '" + tstep.getAppName()
											+ "' does not contain function '" + tstep.getModuleCode() + "'");
									continue;
								}

							} catch (DatabaseException e) {
								logger.error(e.getMessage() + "(test step -- " + tstep.getId() + ")");
								listExceptions.add("Could not get test step '" + tstep.getId()
										+ "' because application '" + tstep.getAppName()
										+ "' does not contain function " + tstep.getModuleCode());
								continue;
							}
							tjnStep.setRowsToExecute(tstep.getRowsToExecute());

							/* changed by manish to display exception messages/ imported logs ends */
							tjnStep.setTestCaseRecordId(tjnTc.getTcRecId());
							tjnStep.setShortDescription(tstep.getShortDescription());
							tjnStep.setType(tstep.getType());
							tjnStep.setExpectedResult(tstep.getExpectedResult());
							tjnStep.setOperation(tstep.getOperation());
							tjnStep.setTxnMode(tstep.getTxnMode());
							tjnStep.setAutLoginType(tstep.getAutLoginType());
							tjnStep.setTstepStorage("Remote");
							try {
								boolean exist = false;
								exist = helper.checkImportedTestStep(tjnStep, tjnTc.getTcRecId());
								if (!exist) {
									helper.persistTestStep(tjnStep);
								} else {
									helper.updateTestStep(tjnStep, prj.getName(), tjnStep.getId());
								}

							} catch (DatabaseException e) {
								logger.error("Could not able to persist imported tstep from tm", e);
							}
						}
						/* Added by Padmavathi for TNJN27-69 &TNJN27-68 starts */
					} else {
						logger.error("No test steps found for test case " + tc.getTcId()
								+ " ,only test case is synchronized.");
						listExceptions.add("No test steps found for test case " + tc.getTcId()
								+ " ,only test case is synchronized.");
					}
					/* Added by Padmavathi for TNJN27-69 & TNJN27-68 ends */
				}

			} catch (DatabaseException e) {
				logger.error("could not fetch tc and ts from etm for instance {}", prj.getEtmInstance(), e);
			}  catch (UnreachableETMException e) {
				
				logger.error("Could not able to login into TM tool.", e);
				
			} catch (ETMException e) {
				

				logger.error("Could not able to login into TM tool.", e);

				
			} catch (NumberFormatException e) {
				logger.error("Could not convert json to string while updating tm tc", e);
			} catch (JSONException e) {
				logger.error("Could not convert json to string while updating tm tc", e);
			} catch (TenjinConfigurationException e1) {
				
				logger.error("Could not convert json to string while updating tm tc", e1);
			}

			map.put("STATUS", "SUCCESS");
			map.put("listExceptions", listExceptions);
			/* changed by manish for req TENJINCG-357 on 12-Oct-2017 starts */
			String msg = "";
			if (listExceptions.size() > 0) {
				if (source.equalsIgnoreCase("detail")) {
					
					msg = "Test Case synchronized successfully, Please click 'Logs' button for more Information";
				} else {
					
					msg = "Test Cases synchronized successfully, Please click Logs button for more Information";
				}

			} else {
				if (source.equalsIgnoreCase("deatil")) {
					msg = "Test Case synchronized successfully";
				} else {
					msg = "Test Cases synchronized successfully";
				}

			}
			/* Changed by Pushpalatha for TenjinCg-611 starts */
			if (source.equalsIgnoreCase("list")) {
				/* dUrl = "TestCaseServlet?param=INIT_TC_PAGINATION&msg="+msg; */
				dUrl = "TestCaseServletNew?t=list&msg=" + msg;
			} else {
				try {
					
					dUrl = "TestCaseServletNew?t=testCase_View&paramval=" + tcId.getJSONObject(0).getString("tcrecid")
							+ "&msg=" + msg;
				} catch (JSONException e) {
					logger.error("error while redirecting to tc view page");
				}
			}
			/* Changed by Pushpalatha for TenjinCg-611 starts */
			/* changed by manish for req TENJINCG-357 on 12-Oct-2017 ends */

			request.getSession().removeAttribute("SCR_MAP");
			request.getSession().setAttribute("SCR_MAP", map);
			response.sendRedirect(dUrl);
		}

		/* added by manish for req TENJINCG-60 on 3-01-2017 ends */

		/* Added by Sriram for TENJINCG-189 (13 June 2017) */
		else if (txn.equalsIgnoreCase("test_set_selection")) {

			request.getSession().removeAttribute("TSMAP_MODAL_MAP");

			logger.info("Fetching all available test sets in project {}", tjnSession.getProject().getName());
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("SELECTED_TESTS", request.getParameter("tests"));
			try {

				logger.info("Checking if all tests are of the same mode");
				/*Modified by shruthi for TENJINCG-1177 starts*/
				

				List<TestSet> testSets = new TestSetHelper().hydrateAllTestSets1(tjnSession.getProject().getId());
				/*Modified by shruthi for TENJINCG-1177 ends*/

				map.put("TEST_SETS", testSets);
				map.put("STATUS", "SUCCESS");
				
				map.put("MESSAGE", "");
			} catch (DatabaseException e) {
				
				logger.error("ERROR fetching all test sets", e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", e.getMessage());
			} finally {
				request.getSession().setAttribute("TSMAP_MODAL_MAP", map);
				request.getRequestDispatcher("tsmap_modal.jsp").forward(request, response);
			}
		}
		/* Added by Pushpa for TENJINCG-978 starts */
		else if (txn.equalsIgnoreCase("fetchMappedTestCase")) {
			String retData = "";
			int testSetRecordId = Integer.parseInt(request.getParameter("tsid"));
			JSONObject json = new JSONObject();
			try {
				LinkedHashMap<Integer, String> tcSeq = new TestCaseHandler()
						.hydrateMappedTestCases(tjnSession.getProject().getId(), testSetRecordId);
				json.put("status", "SUCCESS");
				json.put("TC_SEQ", tcSeq);
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			} catch (JSONException e) {
				
				logger.error("Error ", e);
			} finally {
				retData = json.toString();
			}
			response.getWriter().write(retData);
		}
		/* Added by Pushpa for TENJINCG-978 ends */
		else if (txn.equalsIgnoreCase("add_cases_to_set")) {
			@SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("TSMAP_MODAL_MAP");
			String testSetType = request.getParameter("type");
			/* Added by Pushpa for TENJINCG-978 starts */
			String seq = request.getParameter("tcSeq");
			int tcSeq = -1;
			if (seq != null) {
				tcSeq = Integer.parseInt(seq);
			}
			/* Added by Pushpa for TENJINCG-978 ends */
			String tests = (String) map.get("SELECTED_TESTS");

			if ("existing".equalsIgnoreCase(testSetType)) {
				/* Added by Pushpa for TENJINCG-978 starts */
				int testSetRecordId = Integer.parseInt(request.getParameter("tsid"));
				/* Added by Pushpa for TENJINCG-978 ends */

				try {
					/* Added by Pushpa for TENJINCG-978 starts */
					new TestSetHelper().persistTestSetMap1(testSetRecordId, tests, tjnSession.getProject().getId(),
							tcSeq);
					/* Added by Pushpa for TENJINCG-978 ends */
					map.put("STATUS", "COMPLETE");
					/* Changed by Padmavathi for T25IT-40 starts */
					/* Modified by Pushpa for v2.8-123 starts */
					map.put("MESSAGE", "The selected Test Case(s) are mapped successfully.");
					/* Modified by Pushpa for v2.8-123 ends */
					/* Changed by Padmavathi for T25IT-40 ends */
				} catch (DatabaseException e) {
					logger.error("ERROR persisting test set map", e);
					map.put("STATUS", "ERROR");
					map.put("MESSAGE", "Could not map test cases due to an internal error.");
				}
				/* Added by Pushpa for TENJINCG-979 starts */
				catch (RequestValidationException e) {
					map.put("STATUS", "ERROR");
					map.put("MESSAGE", e.getMessage());
				}
				/* Added by Pushpa for TENJINCG-979 ends */ catch (SQLException e) {
					
					logger.error("Error ", e);
				}
			} else if ("new".equalsIgnoreCase(testSetType)) {
				String testSetName = request.getParameter("tsname");

				TestSet testSet = new TestSet();
				testSet.setName(testSetName);
				testSet.setCreatedBy(tjnSession.getUser().getId());
				testSet.setDescription("");
				testSet.setMode((String) map.get("MODE"));
				testSet.setParent(-1);
				testSet.setPriority("Medium");
				testSet.setProject(tjnSession.getProject().getId());
				testSet.setRecordType("TS");
				/* Added by Gangadhar Badagi for TENJINCG-323 starts */
				testSet.setType("Functional");
				/* Added by Gangadhar Badagi for TENJINCG-323 ends */
				boolean okToContinue = false;
				logger.info("Creating new test set with name [{}]", testSetName);
				try {
					new TestSetHelper().persistTestSet(testSet, tjnSession.getProject().getId());
					okToContinue = true;
				} catch (DatabaseException e) {
					logger.error("ERROR persisting test set", e);
					map.put("STATUS", "ERROR");
					/* Changed by Pushpa for TENJINCG-654 starts */
					map.put("MESSAGE", e.getMessage());
					/* Changed by Pushpa for TENJINCG-654 ends */
				}

				if (okToContinue) {
					logger.info("Mapping selected test cases to the new test set");
					try {
						new TestSetHelper().persistTestSetMap(testSet.getId(), tests, tjnSession.getProject().getId(),
								true);
						map.put("STATUS", "COMPLETE");
						/* Changed by Padmavathi for T25IT-40 starts */
						/* Modified by Padmavathi for T251IT-80 starts */
						map.put("MESSAGE", "Test set created and Test Case(s) mapped successfully.");
						/* Modified by Padmavathi for T251IT-80 ends */
						/* Changed by Padmavathi for T25IT-40 ends */
					} catch (DatabaseException e) {
						logger.error("ERROR persisting test set map", e);
						map.put("STATUS", "ERROR");
						map.put("MESSAGE", "Could not create test set due to an internal error.");
					}
				}

			}

			request.getSession().removeAttribute("TSMAP_MODAL_MAP");
			request.getSession().setAttribute("TSMAP_MODAL_MAP", map);
			request.getRequestDispatcher("tsmap_modal.jsp").forward(request, response);

		}
		/* Added by Sriram for TENJINCG-189 (13 June 2017) ends */

		/* Added by Roshni for TENJINCG-198 starts */
		else if (txn.equalsIgnoreCase("reorder_steps")) {
			String testCaseRecId = request.getParameter("tcrecid");
			String jsonString = request.getParameter("order");

			Map<Integer, Integer> sequenceMap = new HashMap<Integer, Integer>();
			String returnMessage = "";
			JSONObject returnJson = new JSONObject();
			try {
				JSONArray jsonArray = new JSONArray(jsonString);
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject json = jsonArray.getJSONObject(i);
					sequenceMap.put(json.getInt("stepRecId"), json.getInt("sequence"));
				}

				try {
					new TestCaseHelper().reOrderTestCases(Integer.parseInt(testCaseRecId), sequenceMap);
					returnJson.put("status", "success");
				} catch (Exception e) {
					logger.error("ERROR - Could not re-order steps", e);
					returnJson.put("status", "error");
					returnJson.put("message", "Could not re-order steps. Please contact Tenjin Support");
				}

				returnMessage = returnJson.toString();

			} catch (JSONException e) {
				
				logger.error("ERROR generating sequence map", e);
			}

			response.getWriter().write(returnMessage);
		}
		/* Added by Roshni for TENJINCG-198 ends */
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
}
