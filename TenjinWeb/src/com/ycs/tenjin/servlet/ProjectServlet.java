/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 07-Dec-2016			Manish					DTT Mapping to project and auts under that project
 * 12-Jan-2017			Sahana				TENJINCG-27(Tenjin Project user should be able to override the AUT URL for a specific project)
 *  13-Jan-2017			Sahana				TENJINCG-24(Define and Default Test Data Path at Application Level)
 *  19-Jan-2017			Sahana 				TENJINCG-3(User should be able to select an Application to update test data paths for functions for that Application)
 *   02-02-2017          Parveen                 TENJINCG-55 and TENJINCG-56
 * 01-Aug-2017		    Manish					TENJINCG-74,75
 *  08-08-2017           Padmavathi               for T25IT-40
 * 09-Aug-2017		    Manish					T25IT-47
 * 09-Aug-2017		    Manish					T25IT-113
 * 19-08-2017           Leelaprasad              defect T25IT-222 
 * 18-Oct-2017         Gangadhar Badagi          TENJINCG-392
 * 23-11-2017           Padmavathi               for TENJINCG-528
* 04-Dec-2017		    Manish					TCGST-4
*  18-01-2018            Padmavathi              for TENJINCG-576
 * 19-01-2018           Padmavathi              for TENJINCG-576
 * 13-03-2018			Preeti					TENJINCG-615
 * 02-05-2018			Preeti					TENJINCG-656
 * 13-06-2018			Preeti					T251IT-6
 * 14-06-2018			Preeti					T251IT-28
 * 15-06-2018			Preeti					T251IT-42
 * 21-06-2018			Preeti					T251IT-47
 * 26-06-2018           Padmavathi              T251IT-50 
 * 27-06-2018           Padmavathi              T251IT-54
 * 15-03-2019			Ashiki				  	TENJINCG-986
 */

package com.ycs.tenjin.servlet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DefectHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.defect.DefectManagementInstance;
import com.ycs.tenjin.project.Project;

/**
 * Servlet implementation class ProjectServlet
 */
// @WebServlet("/ProjectServlet")
public class ProjectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory
			.getLogger(ProjectServlet.class);

	// private static final Logger logger =
	// TenjinLogger.getLogger(ProjectServlet.class.getName());
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProjectServlet() {
		super();

		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String txn = request.getParameter("param");
		
		 if (txn.equalsIgnoreCase("Fetch_Manager_Projects")) {
			TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
			/* Added By Ashiki TENJINCG-986 starts */
			String userId=tjnSession.getUser().getId();
			new ProjectHelper();
			String instance = request.getParameter("instance");
			String retData="";
			JSONObject rJson=null;
			JSONArray dttProjectsArray = new JSONArray();
			new DefectManagementInstance();
			try {
			
				rJson = new JSONObject();
				
					
					DefectHelper helper = new DefectHelper();
					dttProjectsArray=helper.hydrateSubSets(instance,userId);
					
					rJson.put("subSet", dttProjectsArray);
					rJson.put("status", "SUCCESS");
					
			}
			catch (JSONException e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occured, Please contact your System Administrator");
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
			} catch (DatabaseException e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message", e.getMessage());
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
			} catch (Exception e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occured, Please contact your System Administrator");
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
				logger.error("Error--------------> during fetching Defect Management projects");
			}
			/*changed by manish for req T25IT-47 on 09 Aug 2017 ends*/
			finally {
				retData = rJson.toString();
			}
			response.getWriter().write(retData);
		
		} /* added By Ashiki TENJINCG-986 starts */
		else if (txn.equalsIgnoreCase("Fetch_Subset_Projects")) {

			String instance = request.getParameter("subSet");
			String retData="";
			JSONObject rJson=null;
			JSONObject dttProjectsJson = new JSONObject();
			try {
				rJson = new JSONObject();
					
					DefectHelper helper = new DefectHelper();
					dttProjectsJson=helper.hydrateSubsetProjects(instance);
					
					rJson.put("projects", dttProjectsJson);
					rJson.put("status", "SUCCESS");
					
			} catch (DatabaseException e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message", e.getMessage());
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
			} catch (Exception e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occured, Please contact your System Administrator");
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
				logger.error("Error--------------> during fetching Defect Management projects");
			}
			finally {
				retData = rJson.toString();
			}
			response.getWriter().write(retData);

		
		}
		/* changed By Ashiki TENJINCG-986 ends */
		else if (txn.equalsIgnoreCase("DTT_MULTIPLE_ENABLE")) {
			String projectId = request.getParameter("p");
			String jString = request.getParameter("j");
			Project project = new Project();
			ProjectHelper pH = new ProjectHelper();
			JSONObject rJson=null;
			try {
				rJson = new JSONObject();
				JSONArray jArr = new JSONArray(jString);
				project = pH.hydrateProject(Integer.parseInt(projectId));
				if(project != null){
					for(int i=0; i<jArr.length();i++){
						JSONObject json = new JSONObject();
						json = jArr.getJSONObject(i);
						project.setInstance(json.getString("defectManager"));
						project.setDefProject(json.getString("dttproject"));
						/* changed By Ashiki TENJINCG-986 starts */
						if(!json.getString("tool").equalsIgnoreCase("alm")) {
						project.setSubSet(json.getString("dttsubset"));
						}else {
							project.setSubSet("NA");
						}
						/* changed By Ashiki TENJINCG-986 ends */
						project.setDttEnable(json.getString("status"));
						project.setDttKey(json.getString("dttkey"));
						String aId = json.getString("app");
						project = pH.updateDefMngrProject(project, Integer.parseInt(aId));
					}

					rJson.put("status", "SUCCESS");
					rJson.put("message", "Project updated successfully");
				}else {
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occured, Please contact your System Administrator");
				}


			} catch (Exception e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occured, Please contact your System Administrator");
				} catch (JSONException e1) {

				}

				logger.error("could not map Defect Management instances and projects to aut and project {}",projectId,e);
			}

			response.getWriter().write(rJson.toString());

		}else if (txn.equalsIgnoreCase("edit_dtt_mapping_settings")) {
			ProjectHelper prjHelper = new ProjectHelper();
			String retData = "";
			List<DefectManagementInstance> dttManagers = new ArrayList<DefectManagementInstance>();
			try {
				JSONArray jArray = new JSONArray();
				JSONObject json = new JSONObject();
				try {
					logger.debug("Hydrating Defect Management instances to map application to Defect Management Instance and Defect Management project" );
					dttManagers = prjHelper.fetchDefectManagerInstance();
					logger.info("hydrating of Defect Management instances done");
					Gson gson = new Gson();
					for(DefectManagementInstance t:dttManagers){
						JSONObject tJson = new JSONObject(gson.toJson(t));
						jArray.put(tJson);
					}

					json.put("status", "success");
					json.put("instances", jArray);
				} catch (DatabaseException e) {
					json.put("status", "error");
					json.put("message", e.getMessage());
					logger.error("Error during hydrating instances",e );
				} catch (JSONException e) {
					json.put("status", "error");
					json.put("message", e.getMessage());
					logger.error("Json exception during hydrating instances",e );
				}
				retData = json.toString();
			} catch (JSONException e) {
				logger.error("JSONException caught --> ", e);
				retData = "{status:error,message:An internal error occurred. Please contact Tenjin Support.}";
			}

			response.getWriter().write(retData);

		}
	}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {}

	
	
}
