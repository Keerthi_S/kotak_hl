/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectMailServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 * DATE                 CHANGED BY              DESCRIPTION
 * 02-05-2018			Preeti					Newly added for TENJINCG-656
 * 03-05-2018           Leelaprasad             TENJINCG-639 
 * 10-05-2018			Pushpalatha				TENJINCG-629
 * 18-06-2018			Preeti					T251IT-77
 * 03-05-2019           Padmavathi             	TENJINCG-1048&1049&1050
 * 07-05-2019			Ashiki					TENJINCG-1051
 * 08-05-2019			Preeti					TENJINCG-1053
 * 06-06-2019			Prem					 V2.8-92
   12-03-2020			Lokanath				TENJINCG-1185
   13-03-2020			Jagadish                TENJINCG-1184
*/
package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.handler.ProjectMailHandler;
import com.ycs.tenjin.handler.TestSetHandler;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.ProjectMail;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class ProjectMailServlet
 */
@WebServlet("/ProjectMailServlet")
public class ProjectMailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory
			.getLogger(ProjectMailServlet.class);   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjectMailServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		/*Added by Preeti for T251IT-77 starts*/
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Project project=tjnSession.getProject();
		/*Added by Preeti for T251IT-77 ends*/
		String task = Utilities.trim(request.getParameter("param"));
		if(task.equalsIgnoreCase("view")){
			/*Added by Pushpalatha for TENJINCG-629 starts*/
			HashMap<String,String> map=new HashMap<String,String>();
			/*Added by Pushpalatha for TENJINCG-629 ends*/
			String projectId = request.getParameter("projectid");
			
			try {
				logger.info("Fetching Details of mail for project " + projectId);
				ProjectMail projectMail = new ProjectMailHandler().hydrateProjectMail(Integer.parseInt(projectId));
				request.setAttribute("projectMail", projectMail);
				/*Added by Preeti for T251IT-77 starts*/
				request.setAttribute("project", project);
				/*Added by Preeti for T251IT-77 ends*/
				/*Added by Pushpalatha for TENJINCG-629 starts*/
				map=new ProjectMailHandler().hydrateEscalationRules();
				request.setAttribute("EscalationRules",map);
				/*Added by Pushpalatha for TENJINCG-629 starts*/
				/*Added By Ashiki for TENJINCG-1051 starts*/
				String escalationMaildesc = projectMail.getPrjEscalationRuleDesc();
				if(escalationMaildesc!=null) {
				ObjectMapper mapper = new ObjectMapper();
				Map<String, String> maildescMap = mapper.readValue(escalationMaildesc, Map.class);
				request.setAttribute("escaltionRuleDescp", maildescMap);
				}
				/*Added By Ashiki for TENJINCG-1051 ends*/
				/*Added by Preeti for TENJINCG-1053 starts*/
				String escalationMailIds = projectMail.getPrjEscalationMails();
				if(escalationMailIds!=null) {
				ObjectMapper mapper1 = new ObjectMapper();
				Map<String, String> mailIdMap = mapper1.readValue(escalationMailIds, Map.class);
				request.setAttribute("escaltionMailId", mailIdMap);
				}
				List<TestSet> testset = new TestSetHandler().getAllTestSets(Integer.parseInt(projectId));
				request.setAttribute("testsets", testset);
				ArrayList<Aut> auts = new ProjectHandler().hydratePrjectAuts(Integer.parseInt(projectId));
				request.setAttribute("auts", auts);
				/*Added by Preeti for TENJINCG-1053 ends*/
			} catch (Exception e) {
				/*Added by prem for  V2.8-92 starts*/
				try {
					List<TestSet> testset;
					testset = new TestSetHandler().getAllTestSets(Integer.parseInt(projectId));
					request.setAttribute("testsets", testset);
					ArrayList<Aut> auts = new ProjectHandler().hydratePrjectAuts(Integer.parseInt(projectId));
					request.setAttribute("auts", auts);
				} catch (NumberFormatException | DatabaseException e1) {
					
					logger.error("Error ", e1);
				}
				/*Added by prem for  V2.8-92 Ends*/
				logger.info("Fetching Details of mail for project " + projectId);
			}
			/*Modified by  Padmavathi TENJINCG-1048&1049&1050 starts*/
            /*request.getRequestDispatcher("prj_mail_setting.jsp").forward(request,response);*/
			request.getRequestDispatcher("v2/project_mail_setting.jsp").forward(request,response);
			/*Modified by  Padmavathi TENJINCG-1048&1049&1050 ends*/
		}
		/*Added by Leelaprasad for TENJINCG-639 starts*/
		else if(task.equalsIgnoreCase("send_mail_manual")){
			String userList = request.getParameter("users");
			int runId=Integer.parseInt(request.getParameter("run"));
			int prjId=Integer.parseInt(request.getParameter("project"));
			JSONObject jObj = new JSONObject();
			String retData = "";
			try{
			try {
				new ProjectMailHandler().sendRunReportManual(runId,prjId,userList);
				jObj.put("status", "SUCCESS");
				jObj.put("message", "Mail sent successfully for run Id "+runId+".");
			} catch (TenjinConfigurationException e) {
				logger.error(e.getMessage());
				jObj.put("status", "ERROR");
				jObj.put("message", e.getMessage());
			}finally{
				retData=jObj.toString();
			}
			
		} catch (Exception e) {
			retData = "An Internal Error Occurred. Please try again";
		}
			response.getWriter().write(retData);
	}
		/*Added by Leelaprasad for TENJINCG-639 ends*/
		
		/*Added by Padmavathi for TENJINCG-1048&1049&1050 starts*/
		else if(task.equalsIgnoreCase("admin_config_action")){
		String action = request.getParameter("action");
		String selectedUsers=request.getParameter("selectedUsers");
		int prjId=Integer.parseInt(request.getParameter("prjId"));
		
		JSONObject jObj = new JSONObject();
		String retData = "";
		try{
			try {
				new ProjectMailHandler().persistProjectMailPreference(prjId,action,selectedUsers);
				jObj.put("status", "SUCCESS");
				jObj.put("message", "Configuration saved successfully.");
			} catch (DatabaseException e) {
				logger.error(e.getMessage());
				jObj.put("status", "ERROR");
				jObj.put("message", e.getMessage());
			}finally{
				retData=jObj.toString();
			}
		
		} catch (Exception e) {
			retData = "An Internal Error Occurred. Please try again";
		}
		response.getWriter().write(retData);
	}else if(task.equalsIgnoreCase("getPrjMailPreference")){
		String action = request.getParameter("action");
		int prjId=Integer.parseInt(request.getParameter("prjId"));
		JSONObject jObj = new JSONObject();
		String retData = "";
		try{
			try {
				String users=new ProjectMailHandler().getPrjMailPreferenceUsers(prjId, action);
				jObj.put("status", "SUCCESS");
				jObj.put("users", users);
			} catch (DatabaseException e) {
				logger.error(e.getMessage());
				jObj.put("status", "ERROR");
				jObj.put("message", e.getMessage());
			}finally{
				retData=jObj.toString();
			}
		
		} catch (Exception e) {
				retData = "An Internal Error Occurred. Please try again";
		}
		response.getWriter().write(retData);
	}else if(task.equalsIgnoreCase("user_config_action")){
		String selectedActions=request.getParameter("selectedActions");
		int prjId=Integer.parseInt(request.getParameter("prjId"));
		JSONObject jObj = new JSONObject();
		String retData = "";
		try{
			try {
				new ProjectMailHandler().updateProjectUserMailPreference(prjId,selectedActions,tjnSession.getUser().getId());
				jObj.put("status", "SUCCESS");
				jObj.put("message", "User Configuration saved successfully.");
			} catch (DatabaseException e) {
				logger.error(e.getMessage());
				jObj.put("status", "ERROR");
				jObj.put("message", e.getMessage());
			}finally{
				retData=jObj.toString();
			}
		
		} catch (Exception e) {
			retData = "An Internal Error Occurred. Please try again";
		}
		response.getWriter().write(retData);
	}else if(task.equalsIgnoreCase("get_user_mail_preferences")){
		int prjId=Integer.parseInt(request.getParameter("prjId"));
		JSONObject jObj = new JSONObject();
		String retData = "";
		try{
			try {
				List<String> actions=new ProjectMailHandler().getProjectMailPreferenceForUsers(prjId,tjnSession.getUser().getId());
				jObj.put("status", "SUCCESS");
				jObj.put("actions", actions);
				jObj.put("message", "Configuration saved successfully.");
			} catch (DatabaseException e) {
				logger.error(e.getMessage());
				jObj.put("status", "ERROR");
				jObj.put("message", e.getMessage());
			}finally{
				retData=jObj.toString();
			}
		
		} catch (Exception e) {
			retData = "An Internal Error Occurred. Please try again";
		}
		response.getWriter().write(retData);
	}
		
		/*Added by Padmavathi for TENJINCG-1048&1049&1050 ends*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("entered doPost()");
		SessionUtils.loadScreenStateToRequest(request);
		/*Added by Preeti for T251IT-77 starts*/
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Project project=tjnSession.getProject();
		/*Added by Preeti for T251IT-77 ends*/
		/*Added by Pushpalatha for TENJINCG-629 starts*/
		LinkedHashMap<String,String> map=new LinkedHashMap<String,String>();
		/*Added by Pushpalatha for TENJINCG-629 ends*/
		ProjectMail projectMail = new ProjectMail();
		ProjectMailHandler handler = new ProjectMailHandler();
		projectMail = this.mapAttributes(request);
		
		
		try {
				projectMail = handler.updateProjectForMail(projectMail);
				request.setAttribute("projectMail", projectMail);
				/*Added by Preeti for T251IT-77 starts*/
				request.setAttribute("project", project);
				/*Added by Preeti for T251IT-77 ends*/
				/*Added by Pushpalatha for TENJINCG-629 starts*/
				map=handler.hydrateEscalationRules();
				request.setAttribute("EscalationRules",map);
				/*Added By Ashiki for TENJINCG-1051 starts*/
				/*Modified by Preeti for TENJINCG-1053 starts*/
				String escalationMaildesc = projectMail.getPrjEscalationRuleDesc();
				ObjectMapper mapper = new ObjectMapper();
				Map<String, String> maildescMap = mapper.readValue(escalationMaildesc, Map.class);
				request.setAttribute("escaltionRuleDescp", maildescMap);
				/*Modified by Preeti for TENJINCG-1053 ends*/
				/*Added By Ashiki for TENJINCG-1051 ends*/
				/*Added by Preeti for TENJINCG-1053 starts*/
				String escalationMailIds = projectMail.getPrjEscalationMails();
				ObjectMapper mapper1 = new ObjectMapper();
				Map<String, String> mailIdMap = mapper1.readValue(escalationMailIds, Map.class);
				request.setAttribute("escaltionMailId", mailIdMap);
				List<TestSet> testset = new TestSetHandler().getAllTestSets(project.getId());
				request.setAttribute("testsets", testset);
				ArrayList<Aut> auts = new ProjectHandler().hydratePrjectAuts(project.getId());
				request.setAttribute("auts", auts);
				/*Added by Preeti for TENJINCG-1053 ends*/
				/*Added by Pushpalatha for TENJINCG-629 ends*/
				SessionUtils.setScreenState("success", "projectMail.update.success", request);
				SessionUtils.loadScreenStateToRequest(request);
				/*Modified by  Padmavathi TENJINCG-1048&1049&1050 starts*/
				/*request.getRequestDispatcher("prj_mail_setting.jsp").forward(request, response);*/
				/*Modified by  Padmavathi TENJINCG-1048&1049&1050 ends*/
				request.getRequestDispatcher("v2/project_mail_setting.jsp").forward(request, response);
				/*Added by prem for  V2.8-92 starts*/
		}catch(NullPointerException e) {
			try {
				List<TestSet> testset = new TestSetHandler().getAllTestSets(project.getId());
				request.setAttribute("testsets", testset);
				ArrayList<Aut> auts = new ProjectHandler().hydratePrjectAuts(project.getId());
				request.setAttribute("auts", auts);
				request.getRequestDispatcher("v2/project_mail_setting.jsp").forward(request, response);
			} catch (NumberFormatException | DatabaseException e1) {
				
				logger.error("Error ", e1);
			}
			/*Added by prem for  V2.8-92 ends*/
			} catch (DatabaseException e) {
				logger.error("ERROR fetching mail information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			} catch (RequestValidationException e) {
				
				logger.error("Error ", e);
			} 
	
	}

	private ProjectMail mapAttributes(HttpServletRequest request) {
		ProjectMail projectMail = new ProjectMail();
		projectMail.setPrjId(Integer.parseInt(request.getParameter("projectid")));
		projectMail.setPrjMailSubject(Utilities.trim(request.getParameter("txtmailsubject")));
		if(!(request.getParameter("txtemail")==null))
			projectMail.setPrjMailNotification("Y");
		else
			projectMail.setPrjMailNotification("N");
		/*Added by Pushpalatha for TENJINCG-629 starts*/
		if(!(request.getParameter("txtEscRule")==null)){
			projectMail.setPrjMailEscalation("Y");
			/*Modified By Ashiki for TENJINCG-1051 starts*/
			/*projectMail.setMailEscalationRuleId(Integer.parseInt(request.getParameter("escalationRules")));*/
			projectMail.setMailEscalationRuleId(request.getParameter("escalationRules"));
			projectMail.setPrjEscalationRuleDesc(request.getParameter("escalationRulesDesc"));
			/*Modified By Ashiki for TENJINCG-1051 starts*/
		}
		else{
			projectMail.setPrjMailEscalation("N");
			/*Changed by Ashiki for TENJINCG-1051 starts*/
			/*projectMail.setMailEscalationRuleId(0);*/
			projectMail.setMailEscalationRuleId(request.getParameter("escalationRules"));
			projectMail.setPrjEscalationRuleDesc(request.getParameter("escalationRulesDesc"));
			/*Changed by Ashiki for TENJINCG-1051 starts*/
		}
		//projectMail.setMailEscalationRuleId(Integer.parseInt(request.getParameter("escalationRules")));
		/*Modified by Preeti for TENJINCG-1053 starts*/
		/*projectMail.setPrjEscalationMails(request.getParameter("txtEscalationMail"));*/
		projectMail.setPrjEscalationMails(request.getParameter("escalationMailIds"));
		/*Modified by Preeti for TENJINCG-1053 ends*/
		/*Added by Pushpalatha for TENJINCG-629 ends*/
		
		/*Added by lokanath for TENJINCG-1185 starts*/
		String mailIds=request.getParameter("addAddditionalMailIds");
			projectMail.setPrjAddAdditionalMails(mailIds);
		/*Added by lokanath for TENJINCG-1185 ends*/
			/*Added by Jagadish for TENJINCG-1184 Strats*/
			projectMail.setPrjMailtype(request.getParameter("selectOperationType"));
			/*Added by Jagadish for TENJINCG-1184 ends*/
		return projectMail;
		
		
	}
	
	

}
