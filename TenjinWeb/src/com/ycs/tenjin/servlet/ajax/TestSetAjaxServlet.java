/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestSetAjaxServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
*10-10-2019            Padmavathi               newly added forTJN27-47  
*/


package com.ycs.tenjin.servlet.ajax;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.GsonBuilder;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.PaginatedRecords;
import com.ycs.tenjin.handler.TestSetHandler;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class TestSetAjaxServlet
 */
@WebServlet("/TestSetAjaxServlet")
public class TestSetAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(TestSetAjaxServlet.class); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestSetAjaxServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		String t = Utilities.trim(request.getParameter("t"));
		if(t.equalsIgnoreCase("search") || t.equalsIgnoreCase("")) {
			String start = request.getParameter("start");
			String length = request.getParameter("length");
			String orderColumn = Utilities.trim(request.getParameter("order[0][column]"));
			String orderDirection = Utilities.trim(request.getParameter("order[0][dir]"));
			String sortColumn = "";
			if(orderColumn.length() > 0) {
				sortColumn = Utilities.trim(request.getParameter("columns[" + orderColumn + "][data]"));
			}
			String searchValue=request.getParameter("search[value]") ;
			int pageNumber = 1;
			int maxRecords = 10;
			int draw=0;
			try {
				maxRecords = Integer.parseInt(length);
				pageNumber = (Integer.parseInt(start) / maxRecords) + 1;
			} catch (NumberFormatException e) {
				logger.warn("Invalid number passed for offset and/or maxrecords", e);
			}
			
			try {
				draw = Integer.parseInt(Utilities.trim(request.getParameter("draw")));
			} catch (NumberFormatException e1) {
				logger.warn("Invalid or no draw parameter passed");
			}
			
			TestSetHandler handler = new TestSetHandler();
			PaginatedRecords<TestSet> testSets = new PaginatedRecords<>();
			
			try {
				
				testSets = handler.getAllTestSets(SessionUtils.getTenjinSession(request).getProject().getId(),searchValue, maxRecords, pageNumber, sortColumn, orderDirection);
			} catch(DatabaseException e ) {
				testSets.setError(e.getMessage());
				logger.error("An error occurred while searching test cases", e);
			} finally {
				testSets.setDraw(draw);
				response.getWriter().write(new GsonBuilder().create().toJson(testSets));
			}
			
		}
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
