/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AssistedLearningAjaxServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 26-09-2018			Preeti					TENJINCG-742
* 04-10-2018			Preeti					TENJINCG-822
*/
package com.ycs.tenjin.servlet.ajax;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.AssistedLearningRecord;
import com.ycs.tenjin.handler.AssistedLearningHandler;

/**
 * Servlet implementation class AssistedLearningAjaxServlet
 */
@WebServlet("/AssistedLearningAjaxServlet")
public class AssistedLearningAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(AssistedLearningAjaxServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AssistedLearningAjaxServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String txn = request.getParameter("param");
		String functionCode = request.getParameter("funcCode");
		String appId = request.getParameter("appId");
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		AssistedLearningHandler handler = new AssistedLearningHandler();
		if(txn.equalsIgnoreCase("PERSIST_DATA")){
			logger.info("PERSIST_DATA  method for assisted learning");
			ArrayList<AssistedLearningRecord> results = new ArrayList<AssistedLearningRecord>();
			String json = request.getParameter("json");
			String retData = "";
			try{
				JSONObject resData = new JSONObject();
				try{
					Type listType = new TypeToken<List<AssistedLearningRecord>>(){}.getType();
					results = new Gson().fromJson(json, listType);
					/*Modified by Preeti for TENJINCG-822 starts*/
					handler.persistAssistedLearningRecords(functionCode, Integer.parseInt(appId), tjnSession.getUser().getId(), results);
					/*Modified by Preeti for TENJINCG-822 ends*/
					resData.put("status", "SUCCESS");
					resData.put("message", "Assisted learning data saved successfully.");
					}catch(Exception e){
						resData.put("status","ERROR");
						resData.put("message",e.getMessage());
						logger.error("An Error occurred while persisting assisted learning data", e);
				}finally{
					retData = resData.toString();
				}
				}
				catch(Exception e){
					retData = "An internal error occurred. Please contact your System Administrator";
				}

				response.getWriter().write(retData);
		}
	}
}
