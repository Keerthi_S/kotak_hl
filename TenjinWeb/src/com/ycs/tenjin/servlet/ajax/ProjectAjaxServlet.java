/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCaseAjaxServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
*19-10-2018			   	Sriram Sridharan		Newly Added for TENJINCG-873
*22-10-2018				Sriram					TENJINCG-876
*24-10-2018				Sriram					Removed Server Side pagination params while fetching project users and AUTs.
*26-10-2017				Sriram					TENJINCG-875
*28-03-2019				Pushpalatha				TENJINCG-1003
*22-05-2019				Prem 					V2.8-40 
*/

package com.ycs.tenjin.servlet.ajax;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ProjectDTTHelper;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.project.Domain;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.ProjectTestDataPath;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.JsTreeUtils;
import com.ycs.tenjin.util.Utilities;

@WebServlet("/ProjectAjaxServlet")
public class ProjectAjaxServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(ProjectAjaxServlet.class);
	public ProjectAjaxServlet() {
		super();
		
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String t = Utilities.trim(request.getParameter("t"));
		String projectId=Utilities.trim(request.getParameter("projectId"));
		if(t.equalsIgnoreCase("search_prj_users") || t.equalsIgnoreCase("")) {
		
			List<User> users = new LinkedList<>();
			try {
				users = new ProjectHandler().getProjectUsers(Integer.parseInt(projectId));
				//Modified by prem to restrict unauthorized users starts
				response.getWriter().write(new GsonBuilder().create().toJson(users));
			} catch (DatabaseException e) {
				logger.error("Could not fetch project users");
			} 
			catch(NumberFormatException ee) {
				JsonObject json = new JsonObject();
				json.addProperty("status", "error");
				json.addProperty("message", "User does't have access");
				response.getWriter().write((json.toString()));
			}
			finally {
				//response.getWriter().write(new GsonBuilder().create().toJson(users));
			}
			//Modified by prem to restrict unauthorized users Ends
		}else if(t.equalsIgnoreCase("search_prj_auts")){
			
			
			ArrayList<Aut> auts=new ArrayList<Aut>();
			try {
				auts=new ProjectHandler().hydratePrjectAuts(Integer.parseInt(projectId));
				
			} catch (DatabaseException e) {
				logger.error("Could not fetch project auts");
			} finally {
				response.getWriter().write(new GsonBuilder().create().toJson(auts));
			}
			
		} else if(t.equalsIgnoreCase("search_prj_TDP")){
			String applicationId=request.getParameter("applicationId");
			String groupName=request.getParameter("groupName");
			
			ArrayList<ProjectTestDataPath> tdp=new ArrayList<ProjectTestDataPath>();
			try {
				tdp=new ProjectHandler().hydrateProjectTDP(Integer.parseInt(projectId),applicationId,groupName);
				
			} catch (DatabaseException e) {
				logger.error("Could not fetch project auts");
			} finally {
				
				response.getWriter().write(new GsonBuilder().create().toJson(tdp));
			}
			
		}//TENJINCG-875 (Sriram)
		else if(t.equalsIgnoreCase("domain_project_tree")) {
			String loggedInUser = SessionUtils.getTenjinSession(request).getUser().getId();
			String userRole = SessionUtils.getTenjinSession(request).getUser().getRoles();
			List<Domain> domains = new LinkedList<>();
			try {
				/*Modified by Pushpalatha for TENJINCG-1003 starts*/
				if(userRole != null && userRole.equalsIgnoreCase("Site Administrator")) {
					/*Modified by Pushpalatha for TENJINCG-1003 starts*/
					domains  = new ProjectHandler().getProjectTree();
				}else {
					domains = new ProjectHandler().getProjectTree(loggedInUser);
				}
				
				String json = new JsTreeUtils().getJsTreeJson(domains);
				response.getWriter().write(json);
			} catch (DatabaseException e) {
				logger.error(e.getMessage(),e);
				response.getWriter().write("An internal error occurred.");
			}
			
		}//TENJINCG-875 (Sriram) ends
		else if(t.equalsIgnoreCase("search_prj_defect")){
			
			ArrayList<Aut> auts=new ArrayList<Aut>();
		 try {
			auts=new ProjectDTTHelper().hydrateProjectDTT(Integer.parseInt(projectId));
		} catch (NumberFormatException e) {
			
			logger.error("Error ", e);
		} catch (SQLException e) {
			
			logger.error("Error ", e);
		} catch (DatabaseException e) {
			
			logger.error("Error ", e);
		} finally {
			response.getWriter().write(new GsonBuilder().create().toJson(auts));
		}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String t = Utilities.trim(request.getParameter("t"));
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Project currentProject=tjnSession.getProject();
		if(t.equalsIgnoreCase("mapUserToProject")) {
			JsonObject json = new JsonObject();
			String projectId=request.getParameter("projectId");
			String selectedUser=request.getParameter("selectedUsers");
			try {
				new ProjectHandler().mapUsersToProject(Integer.parseInt(projectId),selectedUser,currentProject.getType());
				json.addProperty("status", "success");
				/*Modified by Prem form V2.8-40 Start*/
				/*json.addProperty("message", "Users mapped successfully");*/
				json.addProperty("message", "User(s) mapped successfully");
				/*Modified by Prem form V2.8-40 End*/
			} catch (NumberFormatException e) {
				logger.error("Error adding users to project" , e);
				json.addProperty("status", "error");
				json.addProperty("message", "Could not add users to project due to an internal error");
			} catch (DatabaseException e) {
				logger.error("Error adding users to project" , e);
				json.addProperty("status", "error");
				json.addProperty("message", "Could not add users to project due to an internal error");
			} catch (RequestValidationException e) {
				
				json.addProperty("status", "error");
				json.addProperty("message", e.getMessage());
			} finally {
				response.getWriter().write(json.toString());
			}
			
		}
	}


}
