/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RunHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 26-09-2018           Leelaprasad             newly added for TENJINCG-815
*/
package com.ycs.tenjin.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.db.RunHelper;

/**
 * Servlet implementation class RunAbortServlet
 */
/*@WebServlet("/RunAbortServlet")*/
public class RunAbortServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(RunAbortServlet.class);
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RunAbortServlet() {
        super();
        
        
    }
    public void init(ServletConfig config) throws ServletException {
		try{
			String flag=TenjinConfiguration.getProperty("RUN_ABORT_FLAG");
			if(flag.equalsIgnoreCase("Y")) {
				logger.info("abort flag yes.cleaning the hung up runs as part of server start up.");
			new RunHelper().abortAllEndLessRuns();
			}else {
				logger.info("abort flag is no.hung up runs will not be cleaned during server start");
			}
		}catch(Exception e){
			
		}
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
