/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AssistedLearningDataServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION 
* 19-Jan-2018			Pushpalatha				TenjinCG-587
* 26-09-2018			Preeti					TENJINCG-742
* 04-10-2018			Preeti					TENJINCG-823
* 16-10-2018			Preeti					TENJINCG-880
* 07-11-2018            Leelaprasad             TENJINCG-900
* 19-12-2018            Leelaprasad             TJNUN262-31
*/

package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.AssistedLearningRecord;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.utils.AssistedLearningUtils;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.AssistedLearningHandler;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class AssistedLearningDataServlet
 */

public class AssistedLearningDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(AssistedLearningDataServlet.class);   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AssistedLearningDataServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String txn = request.getParameter("param");
		logger.info("Invoking transaction {}", txn);
		
		Authorizer authorizer=new Authorizer();
		if(txn.equalsIgnoreCase("") || txn.equalsIgnoreCase("init") || txn.equalsIgnoreCase("export") || txn.equalsIgnoreCase("delete_all")) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		
		if(Utilities.trim(txn).equalsIgnoreCase("init")){
			String appId = Utilities.trim(request.getParameter("app"));
			String funcCode = Utilities.trim(request.getParameter("func"));
			/*Modified by Preeti for TENJINCG-742 starts*/
			try {
				Aut aut = new AutHandler().getApplication(Integer.parseInt(appId));
				List<AssistedLearningRecord> records = new AssistedLearningHandler().getAssistedLearningRecords(Integer.parseInt(appId),funcCode);
				request.setAttribute("aut", aut);
				request.setAttribute("funcCode", funcCode);
				request.setAttribute("records", records);
				request.getRequestDispatcher("v2/assistedlearning_new.jsp").forward(request, response);
			} catch (NumberFormatException e) {
				logger.error(e.getMessage(), e);
				SessionUtils.setScreenState("error", "generic.error", request);
				SessionUtils.loadScreenStateToRequest(request);
				response.sendRedirect("FunctionServlet?t=view&key=" + funcCode + "&appId=" + appId);
			} catch (DatabaseException e) {
				logger.error(e.getMessage(), e);
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
				response.sendRedirect("FunctionServlet?t=view&key=" + funcCode + "&appId=" + appId);
			}
			
				
		}else if(txn.equalsIgnoreCase("export")){
			JSONObject json = new JSONObject();
			//Export data to excel
			try{
				String a = Utilities.trim(request.getParameter("app"));
				String funcCode = Utilities.trim(request.getParameter("func"));
				/*Changed by Leelaprasad for TENJINCG-900 starts*/
				
				/*Changed by Leelaprasad for TENJINCG-900 ends*/
				logger.info("Checking downloads folder");
				/*Changed by Leelaprasad for TENJINCG-900 starts*/
				
				/*modified by paneendra for VAPT fix starts*/
				String rootPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
				File dir = new File(rootPath + "\\Downloads");
				if(!dir.exists()){
					dir.mkdirs();
				}
				logger.info("Preparing download");
				/*Changed by Pushpalatha for TENJINCG-587 starts*/
				AssistedLearningUtils.exportFromXmlToSpreadsheet(Integer.parseInt(a), funcCode, dir.getPath());
				/*modified by paneendra for VAPT fix ends*/
				/*Changed by Leelaprasad for TENJINCG-900 ends*/
				/*Changed by Pushpalatha for TENJINCG-587 ends*/
				logger.info("Download ready");
				json.put("status", "SUCCESS");
				json.put("message", "");
				/*Changed by Pushpalatha for TENJINCG-587 starts*/
				json.put("path", "Downloads"+File.separator + funcCode + "_assisted_learning.xlsx");
				/*Changed by Pushpalatha for TENJINCG-587 ends*/
				response.getWriter().write(json.toString());
			}catch(Exception e){
				logger.error("An Error occurred while preparing download", e);
				response.getWriter().write("{status:error,message:" +e.getMessage()+ "}");
			}
		}
		/*Changed by Leelaprasad for TJNUN262-31 starts*/
		else if(txn.equalsIgnoreCase("delete_all")){
			String functionCode=request.getParameter("funcCode");
			String appId=request.getParameter("appId");
			
			try{
				Aut aut = new AutHelper().hydrateAut(Integer.parseInt(appId));
				new AssistedLearningHandler().deleteAllRecords(functionCode,Integer.parseInt(appId));
				request.setAttribute("aut", aut);
				request.setAttribute("funcCode", functionCode);
				SessionUtils.setScreenState("success", "Assisted learning data deleted successfully.", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("v2/assistedlearning_new.jsp").forward(request, response);
			}catch(Exception e) {
				logger.error("unable to delete all records", e);
				response.getWriter().write("{status:error,message:" +e.getMessage()+ "}");
			}
			/*Changed by Leelaprasad for TJNUN262-31 ends*/
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		boolean isMultiPart;
		
		isMultiPart = ServletFileUpload.isMultipartContent(request);
		
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		String appId = "";
		String functionCode = "";
		Aut aut = new Aut();
		try{
			List fileItems = upload.parseRequest(request);
			Iterator fileItemsIter = fileItems.iterator();
			//gateway = new ExtractorGateway();
			Map<String, String> formValues = new HashMap<String, String>();
			/*Changed by Pushpalatha for TENJINCG-587 starts*/
			/*File dir = new File(request.getSession().getServletContext().getRealPath("/") + File.separator+"Uploads");*/
			String rootPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
			File dir = new File(rootPath+ File.separator+"Uploads");
			/*Changed by Pushpalatha for TENJINCG-587 ends*/
			if(!dir.exists()){
				dir.mkdirs();
			}
			/*Changed by Pushpalatha for TENJINCG-587 starts*/
			//String filePath = request.getSession().getServletContext().getRealPath("/") + File.separator+"Uploads";
			String filePath = rootPath + File.separator+"Uploads";
			/*Changed by Pushpalatha for TENJINCG-587 ends*/
			String fullFilePath = "";
			while(fileItemsIter.hasNext()){
				FileItem fi = (FileItem)fileItemsIter.next();
				if(fi.isFormField()){
					formValues.put(fi.getFieldName(), fi.getString());
				}else{
					String fileName = fi.getName();
					File file;
					/*Changed by Pushpalatha for TENJINCG-587 starts*/
					if( fileName.lastIndexOf(File.separator) >= 0 ){
						fullFilePath = filePath + File.separator + fileName.substring( fileName.lastIndexOf(File.separator));
					}else{
						fullFilePath = filePath + File.separator + fileName.substring(fileName.lastIndexOf(File.separator)+1);
					}
					/*Changed by Pushpalatha for TENJINCG-587 ends*/
					file = new File(fullFilePath);
					fi.write(file) ;
					logger.info("Input file written to path {}", filePath);
				}
			}
			
			
			appId = formValues.get("appId");
			functionCode = formValues.get("func");
			
			/*Modified by Preeti for TENJINCG-880 starts*/
			
			aut = new AutHelper().hydrateAut(Integer.parseInt(appId));
				TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
				List<AssistedLearningRecord> records = AssistedLearningUtils.getAssistedLearningDataFromExcel(fullFilePath, functionCode, Integer.parseInt(appId));
			/*Modified by Preeti for TENJINCG-880 ends*/
				new AssistedLearningHandler().persistAssistedLearningRecords(functionCode, Integer.parseInt(appId), tjnSession.getUser().getId(), records);
				request.setAttribute("aut", aut);
				request.setAttribute("funcCode", functionCode);
				request.setAttribute("records", records);
				SessionUtils.setScreenState("success", "Assisted learning data saved successfully.", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("v2/assistedlearning_new.jsp").forward(request, response);
		}catch(Exception e){
			logger.error(e.getMessage(), e);
			SessionUtils.setScreenState("error",e.getMessage(), request);
			SessionUtils.loadScreenStateToRequest(request);
			List<AssistedLearningRecord> records =null;
			try {
				records = new AssistedLearningHandler().getAssistedLearningRecords(Integer.parseInt(appId),functionCode);
			} catch (NumberFormatException e1) {
				logger.error(e.getMessage(), e);
				SessionUtils.setScreenState("error", "generic.error", request);
				SessionUtils.loadScreenStateToRequest(request);
			} catch (DatabaseException e1) {
				logger.error(e.getMessage(), e);
				SessionUtils.setScreenState("error", "generic.error", request);
				SessionUtils.loadScreenStateToRequest(request);
			}
			request.setAttribute("aut", aut);
			request.setAttribute("funcCode", functionCode);
			request.setAttribute("records", records);
			request.getRequestDispatcher("v2/assistedlearning_new.jsp").forward(request, response);
		}
		
	
		/*Modified by Preeti for TENJINCG-823 ends*/
	}

}
