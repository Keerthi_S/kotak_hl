/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ModuleServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 05-12-2016           Leelaprasad             TJN_243_09
* 09-01-2017           Leelaprasad              Req#TENJINCG-6
* 20-06-2017           Padmavathi              TENJINCG-195
* 01-08-2017           Leelaprasad             TENJINCG-265
* 31-08-2017           Leelaprasad             T25IT-422
* 08-11-2017           Padmavathi              TENJINCG-421
* 02-02-2018		   Pushpalatha			   TENJINCG-568
* 
*/

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class ModuleServlet
 */
//@WebServlet("/ModuleServlet")
public class ModuleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(ModuleServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModuleServlet() {
        super();
        
    }
    public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		@Override
		public boolean checkAccess(HttpServletRequest request) {
			if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
				return true;
			}
			return false;
		}  
	};
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String txn = request.getParameter("param");
	
		
	      /****************************************************************************************
		 * Method Added by Arvind to accomodate Requirement TJN_22_20 (Group Functionality Logic needs to be changed) 28-08-2015 STARTS
		 */	
		
		Authorizer authorizer=new Authorizer();
		if(txn.equalsIgnoreCase("") || txn.equalsIgnoreCase("field_wait_time") || txn.equalsIgnoreCase("wait_time_tree") || txn.equalsIgnoreCase("field_wait_time_save") || txn.equalsIgnoreCase("field_wait_time_unmap") || txn.equalsIgnoreCase("field_wait_time_unmap_all") ) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		/*Changed by Leelaprasad for the requirement TENJINCG-265 starts*/
			 if (txn.equalsIgnoreCase("field_wait_time")) {
				String appID=request.getParameter("appID");
				String funcCode=request.getParameter("funcCode");
				Map<String, Object> map = new HashMap<String, Object>();
				try{
				Module module=new ModulesHelper().hydrateModuleInfo(Integer.parseInt(Utilities.trim(appID)), funcCode);
				Aut aut=new AutHelper().hydrateAut(Integer.parseInt(Utilities.trim(appID)));
				map.put("MODULE", module);
				map.put("AUT", aut);
				map.put("STATUS", "SUCCESS");
				map.put("MESSAGE", "SUCCESS");
				}catch(Exception e){
					
				}finally{
					request.getSession().setAttribute("SCR_MAP", map);
				}
				response.sendRedirect("field_Wait_Time.jsp");
			}
		
	     	
		else if (txn.equalsIgnoreCase("wait_time_tree")) {

			@SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>) request
					.getSession().getAttribute("SCR_MAP");
			Module module = (Module) map.get("MODULE");
			Aut aut = (Aut) map.get("AUT");

			String finalString = "";
			try {
				logger.info("Getting Current Module sheet");
				Map<String, List<String>> currentSheetMap = new ModulesHelper()
						.fetchModulePageField(aut.getId(), module.getCode());

				logger.info("Setting map to session");
				// ///////////////////////////

				JSONArray jArray = new JSONArray();
				int i = 0;
				for (Iterator<Map.Entry<String, List<String>>> entries = currentSheetMap
						.entrySet().iterator(); entries.hasNext();) {
					Map.Entry<String, List<String>> entry = entries.next();

					String sName = entry.getKey().toString();

					logger.info("Processing node for sheet {}", sName);
					String sheetId = "sheet_" + (i + 1);
					boolean mapSuccess = false;

					JSONObject json = new JSONObject();
					json.put("id", sheetId);
					json.put("text", sName);
					JSONObject liAttrs = new JSONObject();
					liAttrs.put("nodetype", "sheet");
					liAttrs.put("nodeid", sheetId);
					json.put("li_attr", liAttrs);

					List<String> fieldNodes = entry.getValue();
					logger.debug("{} field nodes found under this sheet node",
							fieldNodes.size());
					JSONArray fieldsArray = new JSONArray();

					for (int j = 0; j < fieldNodes.size(); j++) {
						String fieldNode = fieldNodes.get(j);

						String fName = fieldNode;
						boolean mapStatus = false;

						JSONObject field = new JSONObject();
						String fieldId = sheetId + "_field_" + (j + 1);
						field.put("id", fieldId);
						field.put("text", fName);

						JSONObject lAttrs = new JSONObject();
						lAttrs.put("nodetype", "field");
						lAttrs.put("nodeid", fieldId);
						List<String> waitTimeMapFields = new ModulesHelper()
								.fetchWaitTimeMap(aut.getId(),
										module.getCode(), sName);
						String fieldName = "";
						int waittimefield = 0;
						if (waitTimeMapFields.size() > 0) {
							for (String fieldsName : waitTimeMapFields) {
								int waitTime = 0;
								if (fieldsName.contains("<>")) {
									String[] wt = fieldsName.split("<>");
									fieldName = wt[0];
									try {
										waitTime = Integer.parseInt(wt[1]);

									} catch (Exception re) {
									}
								}
								if (fieldName.equals(fName)) {
									mapStatus = true;
									waittimefield = waitTime;
									break;
								}
							}
						}

						lAttrs.put("mapped", mapStatus);

						lAttrs.put("sourcesheet", sName);
						lAttrs.put("sourcefield", fName);
						lAttrs.put("waittime", waittimefield);

						field.put("li_attr", lAttrs);

						if (mapStatus) {
							field.put("icon", "jstree/mapped.png");
						} else {
							field.put("icon", "jstree/unmapped.png");

						}

						fieldsArray.put(field);
					}

					if (mapSuccess) {
						json.put("icon", "jstree/sheetmapped.png");
					} else {
						json.put("icon", "jstree/sheetunmapped.png");
					}

					json.put("children", fieldsArray);
					jArray.put(json);
					i++;
				}

				finalString = jArray.toString();
				logger.info("Final JSON Generated below");
				logger.info(finalString);

				response.getWriter().write(finalString);
			} catch (Exception e) {
				response.getWriter().write("Internal error");
			}

		}
		else if(txn.equalsIgnoreCase("field_wait_time_save")){
			String j = request.getParameter("json");
			String retData = "";
		try{
			JSONObject retJson = new JSONObject();
		
			JSONObject json = new JSONObject(j);
			
					String waitTimeFields = request.getParameter("waittimefield");
					
					if(!waitTimeFields.isEmpty()){			
					logger.info("waittimefields {} ",waitTimeFields);
					try {
						
						JSONArray waitTimeFieldsObj=new JSONArray(waitTimeFields);
						new ModulesHelper().insertOrUpdateWaitTimeField(json.getInt("app"),json.getString("module"),waitTimeFieldsObj);
						retJson.put("status", "SUCCESS");
						retJson.put("message", "Field wait time updated successfully");
					} catch (JSONException e) {
						
						retJson.put("status","ERROR");
						retJson.put("message", "Update Failed due to an internal error");
					}
					//////////////////////end of manual fields//////////////
					finally{
						retData = retJson.toString();
					}
					}
			}catch(Exception e){
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			
			response.getWriter().write(retData);
		}
		else if(txn.equalsIgnoreCase("field_wait_time_unmap")){

			String j = request.getParameter("json");
			String unMapSheet=request.getParameter("sheet");
			String unMapField=request.getParameter("field");
			String retData = "";
		try{
			JSONObject retJson = new JSONObject();
		
			JSONObject json = new JSONObject(j);
					try {
						
					
						new ModulesHelper().unMapSingleWaitTimeField(json.getInt("app"),json.getString("module"),unMapSheet,unMapField);
						retJson.put("status", "SUCCESS");
						retJson.put("message", "Unmapped successfully for field '"+unMapField+"' in sheet '"+unMapSheet+"'");
					} catch (JSONException e) {
						
						retJson.put("status","ERROR");
						retJson.put("message", "Update Failed due to an internal error");
					}
					//////////////////////end of manual fields//////////////
					finally{
						retData = retJson.toString();
					}
					
			}catch(Exception e){
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		
		}
		else if(txn.equalsIgnoreCase("field_wait_time_unmap_all")){

			//unmapall
			String j = request.getParameter("json");
			String retData = "";
		try{
			JSONObject retJson = new JSONObject();
		
			JSONObject json = new JSONObject(j);
			
					try {
						
					
						new ModulesHelper().unMapAllWaitTimeField(json.getInt("app"),json.getString("module"));
						
						retJson.put("status", "SUCCESS");
						retJson.put("message", "Unmapped successfully");
					} catch (JSONException e) {
						
						retJson.put("status","ERROR");
						retJson.put("message", "Update Failed due to an internal error");
					}
					//////////////////////end of manual fields//////////////
					finally{
						retData = retJson.toString();
					}
					
			}catch(Exception e){
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			
			response.getWriter().write(retData);
			
		}
		/*Changed by Leelaprasad for the requirement TENJINCG-265 ends*/

	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

}
