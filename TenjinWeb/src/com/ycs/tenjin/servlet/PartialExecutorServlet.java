/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestSetServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */
/*Added By Roshni For TENJINCG-259*/
/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                	CHANGED BY              DESCRIPTION
 * 19-Oct-2017			Sriram Sridharan		TENJINCG-397
 * 16-01-2018			Preeti					TENJINCG-574
 * 24-04-2018          	Padmavathi              TENJINCG-653
 * 18-05-2018            Padmavathi              TENJINCG-647
 * 30-05-2018            Padmavathi              TENJINCG-673
 * 26-Nov 2018			Shivam	Sharma			 TENJINCG-904
 * 22-03-2019			Preeti					TENJINCG-1018
 */

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ResultsHelper;
import com.ycs.tenjin.db.TestCaseHelper;
import com.ycs.tenjin.handler.TestSetHandler;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.ExecutorGateway;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class PartialExecutorServlet
 */
/*@WebServlet("/PartialExecutorServlet")*/
public class PartialExecutorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(PartialExecutorServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PartialExecutorServlet() {
		super();
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String txn = request.getParameter("param");

		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");

		/*Added by Preeti for TENJINCG-574 starts*/
		if (txn.equalsIgnoreCase("Re-RunTest")) {
			String tsRecId = request.getParameter("ts");
			String pRunId = request.getParameter("run");
			/*  Added by Padmavathi for TENJINCG-653 starts */ 
			String callback = request.getParameter("callback");
			/*  Added by Padmavathi for TENJINCG-653 ends */ 
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("PARENT_RUN_ID", Integer.parseInt(pRunId));
			/*Added by Padmavathi for TENJINCG-647 starts*/
			Map<String, Object> rerunMap =null;
			/*Added by Padmavathi for TENJINCG-647 ends*/
			try {
				/*Modified by Padmavathi for TENJINCG-647 starts*/
				rerunMap = new ResultsHelper().hydrateTestSetDetailsForRerun(tjnSession.getProject().getId(),
						Integer.parseInt(tsRecId), Integer.parseInt(pRunId), "");
				/*Added by Padmavathi for TENJINCG-647 ends*/
				map.put("STATUS", "");
				map.put("MESSAGE", "");
				map.put("TEST_SET", rerunMap.get("TestSet"));
				/*  Added by Padmavathi for TENJINCG-653 starts */ 
				map.put("callback", callback);
				/*  Added by Padmavathi for TENJINCG-653 ends */ 
			} catch (Exception e) {
				logger.error("Could not initiate run", e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "An internal error occurred. Please try again");
			} finally {
				request.getSession().setAttribute("SCR_MAP", map);
				/*Added by Padmavathi for TENJINCG-647 starts*/
				request.getSession().setAttribute("PassedStepMap", rerunMap.get("PassedStepMap"));
				/*Added by Padmavathi for TENJINCG-647 ends*/
			}

			response.sendRedirect("rerunselection.jsp");
		}
		/*	Added by Preeti for TENJINCG-574 ends*/

		if (txn.equalsIgnoreCase("Re-RunTestSet")) {
			String tsRecId = request.getParameter("ts");
			String pRunId = request.getParameter("run");
			/*  Added by Padmavathi for TENJINCG-653 starts */ 
			String callback = request.getParameter("callback");
			String callbackparam = request.getParameter("callbackparam");
			/*  Added by Padmavathi for TENJINCG-653 ends  */ 

			/*Added by Padmavathi for TENJINCG-647 starts*/
			Map<String,Object> PassedStepMap = (Map<String,Object>) request.getSession().getAttribute("PassedStepMap");
			/*Added by Padmavathi for TENJINCG-647 ends*/

			/*Added by Preeti for TENJINCG-574 starts*/
			String tcList = request.getParameter("tcids");
			String[] tcArray = tcList.split(",");
			String listoftc="";

			for(int i=0;i<tcArray.length;i++)
			{
				int count=0;
				for(int j=i+1;j<tcArray.length;j++)
				{
					if(tcArray[i].equals(tcArray[j])){
						count++;
					}
				}
				if(count==0)
					listoftc=listoftc+tcArray[i]+",";
			}

			String[] listoftcs=listoftc.split(",");
			String tsList = request.getParameter("tsids");
			String[] tsArray = tsList.split(",");
			/*Added by Preeti for TENJINCG-574 ends*/

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("PARENT_RUN_ID", Integer.parseInt(pRunId));
			/*map.put("SELECTED_TESTS",array );*/
			try {
				/*Added by Preeti for TENJINCG-574 starts*/
				ArrayList<TestCase> tc=new TestCaseHelper().hydrateAllTestCases(listoftcs,tsArray);
				/*Added by Preeti for TENJINCG-574 ends*/
				/*Modified by Preeti for TENJINCG-574 starts*/
				/*TestSet t = new ResultsHelper().hydrateTestSetForRerun(tjnSession.getProject().getId(),
						Integer.parseInt(tsRecId), Integer.parseInt(pRunId), "");*/
				TestSet t = new ResultsHelper().hydrateTestSetDetailsForRerun(tjnSession.getProject().getId(),
						Integer.parseInt(tsRecId));
				/*Modified by Preeti for TENJINCG-574 ends*/
				t.setTests(tc);
				/* Added by Padmavathi for TENJINCG-673 starts */

				//Checking Re-run Test contains mobility steps or not 
				Boolean mobFlag=new TestSetHandler().validateMobile(t);
				if(mobFlag){
					map.put("MOBILITY_FLAG", "True");
				}
				/* Added by Padmavathi for TENJINCG-673 ends */
				map.put("STATUS", "");
				map.put("MESSAGE", "");
				map.put("TEST_SET", t);
				/*Added by Preeti for TENJINCG-574 starts*/
				map.put("TEST_CASES", tc);
				/*Added by Preeti for TENJINCG-574 ends*/

				/*  Added by Padmavathi for TENJINCG-653 starts */ 
				map.put("callback", callback);
				map.put("callbackparam", callbackparam);
				/*  Added by Padmavathi for TENJINCG-653 ends */ 

				/*Added by Padmavathi for TENJINCG-647 starts*/
				map.put("PassedStepMap",PassedStepMap);
				/*Added by Padmavathi for TENJINCG-647 ends*/
				if(! Utilities.trim(t.getMode()).equalsIgnoreCase("api")) {
					/* For TENJINCG-397 */
					List<RegisteredClient> clients = new ClientHelper().hydrateAllClients();
					/* For TENJINCG-397 ends*/
					map.put("CLIENT_LIST", clients);
				}
			} catch (Exception e) {
				logger.error("Could not initiate run", e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "An internal error occurred. Please try again");
			} finally {
				request.getSession().setAttribute("SCR_MAP", map);
			}

			response.sendRedirect("reruninit.jsp");
		}

		/*Added by Preeti for TENJINCG-574 starts*/
		else if(txn.equalsIgnoreCase("search")){


			String tsRecId = request.getParameter("ts");
			String pRunId = request.getParameter("run");
			String criteria=request.getParameter("criteria");

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("PARENT_RUN_ID", Integer.parseInt(pRunId));

			try {
				TestSet t = new ResultsHelper().hydrateTestSetForRerun(tjnSession.getProject().getId(),
						Integer.parseInt(tsRecId), Integer.parseInt(pRunId), criteria);
				map.put("STATUS", "");
				map.put("MESSAGE", "");
				map.put("TEST_SET", t);
				map.put("CRITERIA",criteria);

			} catch (Exception e) {
				logger.error("Could not initiate run", e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "An internal error occurred. Please try again");
			} finally {
				request.getSession().setAttribute("SCR_MAP", map);
			}

			response.sendRedirect("rerunselection.jsp");

		}
		/*Added by Preeti for TENJINCG-574 ends*/

		else if(txn.equalsIgnoreCase("rerun_result")){
			String runId = request.getParameter("run");

			Map<String,Object> map = new HashMap<String, Object>();
			String dUrl = "";
			try{		
				dUrl = "ResultsServlet?param=run_result&run=" + runId;
			}catch(Exception e){
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "An internal error occurred while collating results");
				map.put("RUN",null);
				dUrl = "error.jsp";
			}finally{
				request.getSession().setAttribute("SCR_MAP", map);
			}

			response.sendRedirect(dUrl);
		} 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String testSetId = request.getParameter("txtTsRecId");
		String browserType = request.getParameter("lstBrowserType");
		/*  Added by Padmavathi for TENJINCG-653 starts */ 
		String callbackParam = request.getParameter("callbackparam");
		String callback = request.getParameter("callback");
		/*  Added by Padmavathi for TENJINCG-653 ends */ 
		String execSpeed = request.getParameter("txtExecutionSpeed");
		if(Utilities.trim(execSpeed).equalsIgnoreCase("")) {
			execSpeed = "0";
		}
		String screenshotOption = request.getParameter("Screen");
		if(Utilities.trim(screenshotOption).equalsIgnoreCase("")){
			screenshotOption = "0";
		}
		String executionMode = request.getParameter("executionMode");
		String parentRunId = request.getParameter("parentRun");
		/* Added by Padmavathi for TENJINCG-673 starts */
		String mobflag=request.getParameter("mobFlag");
		String deviceId=request.getParameter("listDevice");
		/* Added by Padmavathi for TENJINCG-673 ends */
		boolean okToExecute = true;
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		String message = "";
		Map<String,Object> map = (Map<String,Object>) request.getSession().getAttribute("SCR_MAP");
		if(map == null){
			map = new HashMap<String, Object>();
		}
		List<TestStep> stepsToExecute = new ArrayList<TestStep>();//Sriram for Req#TJN_243_11

		/*Added by Padmavathi for TENJINCG-647 starts*/
		Map<Integer, String> passedStepsStatusMap=(Map<Integer, String>)map.get("PassedStepMap");
		/*Added by Padmavathi for TENJINCG-647 ends*/
		TestSet testSet = (TestSet)map.get("TEST_SET");
		if(testSet != null){
			for(TestCase testCase:testSet.getTests()){
				for(TestStep step:testCase.getTcSteps()){
					stepsToExecute.add(step);
				}
			}
		}else{
			logger.error("ERROR creating ad-hoc test set. Test Set is null");
			message = "Could not start run due to an unexpected error. Please contact Tenjin Support.";
			okToExecute = false;
		}
		String videoCaptureFlag = Utilities.trim(request.getParameter("videoCaptureFlag"));
		if(videoCaptureFlag.equalsIgnoreCase("")){
			videoCaptureFlag = "N";
		}
		//added by shivam sharma for  TENJINCG-904 starts
		String clientName = request.getParameter("client");
		//added by shivam sharma for  TENJINCG-904 ends
		String dUrl = "";
		String oem = "";
		String targetIp = "";
		String port = "4444";

		try {
			logger.info("Getting Automation Engine configuration");
			oem = TenjinConfiguration.getProperty("OEM_TOOL");
		} catch (TenjinConfigurationException e) {
			
			logger.error("CONFIG ERROR --> {}", e.getMessage());
			logger.info("Automation Engine will be defaulted to Selenium WebDriver");
			oem = "Selenium";
		}

		logger.info("Automation Engine --> [{}]", oem);

		RegisteredClient client = null;		
		if(! Utilities.trim(executionMode).equalsIgnoreCase("api")) {
			if(okToExecute){
				/* Added by Padmavathi for TENJINCG-673 starts */
				if(mobflag!=null && mobflag.equalsIgnoreCase("true")){
					if(request.getParameter("listDevice").equals("-1")){
						try {
							throw new TenjinServletException("Please select a device");
						} catch (TenjinServletException e) {
							
							logger.error("Error ", e);
							message = e.getMessage();
							okToExecute = false;
						}
					}
				}
				/* Added by Padmavathi for TENJINCG-673 ends */
				try{
					logger.info("Validating Client...");
					client = new ClientHelper().hydrateClient(clientName);
					if(client == null){
						logger.error("Client [{}] not found");
						throw new TenjinServletException("Client not selected. Please choose a client");
					}

					targetIp = client.getHostName();
					port = client.getPort();

				}catch(DatabaseException e){
					logger.error("CLIENT [{}] not found", e);
					message = "Could not verify Client Information. Please contact Tenjin Support";
					okToExecute = false;
				} catch (TenjinServletException e) {
					
					message = e.getMessage();
					okToExecute = false;
				}
			}
		}
		
		//Code change for OAuth 2.0 requirement TENJINCG-1018- Avinash - Starts
		boolean oAuthValidation = false;
		if(executionMode.equalsIgnoreCase("api")){
			oAuthValidation = new ApiHelper().checkForoAuthOperation(Integer.parseInt(testSetId), "oAuth2");
			request.getSession().setAttribute("oAuthFlag", oAuthValidation);
		}
		//Code change for OAuth 2.0 requirement TENJINCG-1018- Avinash - Ends
		
		if(okToExecute){
			logger.info("Initializing Executor Gateway...");
			try {
				int parentRunId1=Integer.parseInt(parentRunId);
				/*Modified by Padmavathi for TENJINCG-647 starts*/
				/* Modified by Padmavathi for TENJINCG-673 starts */
				ExecutorGateway gateway = new ExecutorGateway(tjnSession, testSet, Integer.parseInt(testSetId), browserType, targetIp, Integer.parseInt(port), oem, Integer.parseInt(execSpeed), Integer.parseInt(screenshotOption), videoCaptureFlag, executionMode,parentRunId1,passedStepsStatusMap,deviceId);
				/* Modified by Padmavathi for TENJINCG-673 ends */
				/*Modified by Padmavathi for TENJINCG-647 ends*/
				gateway.setStepsToExecute(stepsToExecute);
				request.getSession().setAttribute("EXEC_GATEWAY", gateway);

				dUrl = "runvalidate.jsp";
			} catch (NumberFormatException e) {
				
				logger.error("NumberFormatException caught while initializing gateway", e);
				message = "An Internal Error occurred. Please contact your System Administrator";
				okToExecute = false;
			} catch (TenjinServletException e) {
				
				okToExecute = false;
				message = e.getMessage();
			}
		}


		if(okToExecute){
			/*  Modified by Padmavathi for TENJINCG-653 starts */ 
			dUrl = "runvalidate.jsp?callbackParam="+callbackParam+"&callback="+callback+"&parentRunId="+parentRunId;
			/*  Modified by Padmavathi for TENJINCG-653 ends */ 
		}else{
			map.put("STATUS", "ERROR");
			map.put("MESSAGE",message);
			request.getSession().setAttribute("SCR_MAP", map);
			/*  Modified by Padmavathi for TENJINCG-653 starts */ 
			dUrl = "reruninit.jsp?callbackParam="+callbackParam+"&callback="+callback;
			/*  Modified by Padmavathi for TENJINCG-653 ends */ 
		}
		
		//Added by Avinash for OAuth 2.0 requirement TENJINCG-1018- Starts
		if(oAuthValidation){
			dUrl+="&oauthval=Y";
		}
		//Added by Avinash for OAuth 2.0 requirement TENJINCG-1018- Ends

		logger.info("Redirecting...");
		response.sendRedirect(dUrl);}

	

}
