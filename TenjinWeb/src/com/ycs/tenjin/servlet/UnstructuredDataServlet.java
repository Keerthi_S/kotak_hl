/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UnstructuredDataServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 08-04-2020			Ashiki			Newly added for TENJINCG-1204
* */

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.ValidationDetails;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.UnstructuredDataHandler;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class UnstructuredDataServlet
 */
@WebServlet("/UnstructuredDataServlet")
public class UnstructuredDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	private static Logger logger = LoggerFactory
			.getLogger(UnstructuredDataServlet.class);
	
    public UnstructuredDataServlet() {
        super();
        
    }
    UnstructuredDataHandler handler=new UnstructuredDataHandler();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SessionUtils.loadScreenStateToRequest(request);
		String task = Utilities.trim(request.getParameter("t"));
		
		if(task.equalsIgnoreCase("Unstructured_Data_List")) {
			String stepRecId= request.getParameter("stepRecId");
			ArrayList<ValidationDetails> UnstructuredData=new ArrayList<ValidationDetails>();
			try {
				UnstructuredData = handler.hydrateUnstructuredData(Integer.parseInt(stepRecId));
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			}
			request.setAttribute("stepRecId", stepRecId);
			request.setAttribute("UnstructuredData", UnstructuredData);
			RequestDispatcher dispatcher = request.getRequestDispatcher("v2/unstructured_data_validation_list.jsp");
			dispatcher.forward(request, response);
		}
		else if(task.equalsIgnoreCase("view")) {
			String recId= request.getParameter("recId");
			String stepRecId= request.getParameter("stepRecId");
			ValidationDetails validation= new ValidationDetails();
			try {
				validation=handler.getValidationdetailsById(Integer.parseInt(recId));
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			}
			request.setAttribute("validationName", validation.getValidationName()); 
			request.setAttribute("stepRecId", stepRecId);
			request.setAttribute("validation", validation);
			RequestDispatcher dispatcher = request.getRequestDispatcher("v2/unstructured_data_validation_view.jsp");
			dispatcher.forward(request, response);
			
		}
		else if(task.equalsIgnoreCase("new")) {
			String stepRecId= request.getParameter("stepRecId");
			request.setAttribute("stepRecId", stepRecId);
			RequestDispatcher dispatcher = request.getRequestDispatcher("v2/unstructured_data_validation_new.jsp");
			dispatcher.forward(request, response);
		}
	}

	
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		String delFlag = Utilities.trim(request.getParameter("del"));
		ValidationDetails validateDetails = (ValidationDetails) SessionUtils.mapAttributes(request, new ValidationDetails().getClass());
		  
		  String stepRecId= request.getParameter("stepRecId");
		  String excelValidation= request.getParameter("excelValidation");
		  String pdfvalidation= request.getParameter("pdfvalidation");
		  validateDetails.setTestStepId(Integer.parseInt(stepRecId));
		  String validationName= request.getParameter("validationName");
		  String dispatchUrl = "";
		if(delFlag.equalsIgnoreCase("true")){
			doDelete(request, response);
			SessionUtils.setScreenState("success", "unstructured.data.delete.success", request);
			response.sendRedirect("UnstructuredDataServlet?t=Unstructured_Data_List&stepRecId="+stepRecId);
		} 
		else if(delFlag.equalsIgnoreCase("update")) {
			String recId= request.getParameter("recId");
			validateDetails.setValidationName(validationName);
			
			JSONObject jsonObj= new JSONObject(validateDetails);
			   String jsonText = jsonObj.toString();
			   validateDetails.setValidationData(jsonText);
			  try {
				handler.updateUnstructuredData(validateDetails,Integer.parseInt(recId) );
				  
				SessionUtils.setScreenState("success", "unstructured.data.update.success", request);
				response.sendRedirect("UnstructuredDataServlet?t=view&stepRecId="+stepRecId+"&recId="+recId);
			} catch (RequestValidationException e) {

				dispatchUrl = "v2/unstructured_data_validation_view.jsp";
				SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("stepRecId", stepRecId);
				request.setAttribute("validationName", validateDetails.getValidationName());
				request.setAttribute("validation", validateDetails);
				request.getRequestDispatcher(dispatchUrl).forward(request, response);
			
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			}
			  
			 
			  
		}
		else {
			 try {
				if(validateDetails.getFileType().equalsIgnoreCase("pdf")) {
					  validateDetails.setValidationName(pdfvalidation);
				 }else if(validateDetails.getFileType().equalsIgnoreCase("excel")) {
					  validateDetails.setValidationName(excelValidation);
				 }
				validateDetails=processValidationData(validateDetails);
				JSONObject jsonObj= new JSONObject(validateDetails);
				String jsonText = jsonObj.toString();
				validateDetails.setValidationData(jsonText);
				  
				handler.persistUnstructuredData(validateDetails);
				request.setAttribute("validateDetails", validateDetails);
				SessionUtils.setScreenState("success", "unstructured.data.save.success", request);
				response.sendRedirect("UnstructuredDataServlet?t=view&recId="+validateDetails.getRecordId()+"&stepRecId="+stepRecId);
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			}catch (RequestValidationException e) {
		        dispatchUrl = "v2/unstructured_data_validation_new.jsp";
				SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("stepRecId", stepRecId);
				request.setAttribute("validationName", validateDetails.getValidationName());
				request.setAttribute("validation", validateDetails);
				request.getRequestDispatcher(dispatchUrl).forward(request, response);
			}
			 
		}

		}
	private ValidationDetails processValidationData(ValidationDetails validateDetails) {
		if(validateDetails.getSheetName().equalsIgnoreCase("")) {
			validateDetails.setSheetName(null);
		}
		if(validateDetails.getCellReference().equalsIgnoreCase("")) {
			validateDetails.setCellReference(null);
		}
		if(validateDetails.getCellRange().equalsIgnoreCase("")) {
			validateDetails.setCellRange(null);
		}
		if(validateDetails.getPassword().equalsIgnoreCase("")) {
			validateDetails.setPassword(null);
		}
		
		if(validateDetails.getSearchText().equalsIgnoreCase("")) {
			validateDetails.setSearchText(null);
		}
		return validateDetails;
		
	}




	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] selectedRecIds = request.getParameterValues("recId");
		
		try {
			handler.deleteUnstructuredData(selectedRecIds);
			
		} catch (DatabaseException e) {
			
			logger.error("Error ", e);
		}
	}
}
