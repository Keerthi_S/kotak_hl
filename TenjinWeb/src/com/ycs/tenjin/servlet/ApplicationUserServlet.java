/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApplicationUserServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 27-10-2018			Ashiki			Newly added for TENJINCG-846
* 22-03-2019			Preeti					TENJINCG-1018
* */
package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.ApplicationUser;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.AutsHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.ApplicationUserHandler;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class ApplicationUserServlet
 */
@WebServlet("/ApplicationUserServlet")
public class ApplicationUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory	.getLogger(ApplicationUserServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApplicationUserServlet() {
        super();
        
    }
    
    private int appId = 0;
    private int uacId = 0;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		String userId = "";
		if(tjnSession != null){
			User cUser=tjnSession.getUser();
			userId=cUser.getId();
			request.setAttribute("userId", userId);
		}
		
		String task = Utilities.trim(request.getParameter("t"));
		
		ApplicationUserHandler handler = new ApplicationUserHandler();
		if(task.length() < 1){
				ArrayList<ApplicationUser> applicationUsers=new ArrayList<ApplicationUser>();
				try {
					applicationUsers = new ApplicationUserHandler().getAllApplicationUsers(userId);
					request.setAttribute("applicationUsers", applicationUsers);
					RequestDispatcher dispatcher = request.getRequestDispatcher("v2/applicationuser_list.jsp");
					dispatcher.forward(request, response);
				} catch (DatabaseException e) {
					logger.error("ERROR fetching application user information", e);
					request.getRequestDispatcher("error.jsp").forward(request, response);
				}
		}
		if(task.equalsIgnoreCase("new")){
			RequestDispatcher dispatcher = request.getRequestDispatcher("v2/applicationuser_new.jsp");
			dispatcher.forward(request, response);		
		}
		if(task.equalsIgnoreCase("view")){
			String uacId = Utilities.trim(request.getParameter("key"));
			try {
				/*Changes by Sunitha for TENJINCG-1018 starts*/
				ApplicationUser applicationUser = handler.getApplicationUser(Integer.parseInt(uacId));
				AutHandler oAuthHandler = new AutHandler();
				if(oAuthHandler.oAuthRecordExists(applicationUser.getAppId()))
					request.setAttribute("tokenExist", "yes");
				/*Changes by Sunitha for TENJINCG-1018 ends*/
				request.setAttribute("applicationUser", applicationUser);
				RequestDispatcher dispatcher = request.getRequestDispatcher("v2/applicationuser_view.jsp");
				dispatcher.forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching Application User information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}
		/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Starts*/
		if(task.equalsIgnoreCase("accessToken")){
			ApplicationUser oAuth = new ApplicationUser();
			
			String uacId = request.getParameter("uacId");
			String caller = request.getParameter("caller");
			try {
				//Changes by Avinash for OAuth 2.0 requirement - Starts
				oAuth = handler.getApplicationUser(Integer.parseInt(uacId));
				Aut aut = new AutHandler().getOAuthRecord(oAuth.getAppId());
				//Changes by Avinash for OAuth 2.0 requirement - Ends
				request.setAttribute("uacId", uacId);
				request.setAttribute("grantType", aut.getGrantType());
				request.setAttribute("oauth", oAuth);
			} catch (NumberFormatException e) {
				logger.error("ERROR while fetching oAuth2 information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR while fetching oAuth2 information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
			request.getRequestDispatcher("v2/accesstoken_details.jsp?caller="+caller).forward(request, response);
		}
		/* Added by Sunitha for OAuth 2.0 requirement Ends*/
		else if(task.equalsIgnoreCase("getauthcode")){
			String uacIdString = request.getParameter("uacId");
			String appIdString = request.getParameter("appId");
			String caller = request.getParameter("caller");
			
			JSONObject responseMsg = new JSONObject();
			Aut aut = null;
			ApplicationUser user = null;
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				appId = Integer.parseInt(appIdString);
				uacId = Integer.parseInt(uacIdString);
				aut = new AutsHelper().hydrateOAuthRecord(appId);
				
				String url =""; 
				String type = "";
				
				user = handler.getAccessTokenRecord(uacId);
				if(Utilities.trim(user.getAccessToken()).isEmpty() || (caller != null && "refresh".equalsIgnoreCase(caller)) || "client_credentials".equalsIgnoreCase(aut.getGrantType()) || "implicit".equalsIgnoreCase(aut.getGrantType())){
					if("authorization_code".equalsIgnoreCase(aut.getGrantType())){
						url= aut.getAuthUrl()+"?response_type=code&client_id="+aut.getClientId()+"&client_secret="+aut.getClientSecret()+"&redirect_uri="+aut.getCallBackUrl()+"&scope="+user.getLoginUserType();
						type = "window";
					}else if("password".equalsIgnoreCase(aut.getGrantType()))
						url= aut.getAccessTokenUrl()+"?grant_type="+aut.getGrantType()+"&scope="+user.getLoginUserType()+"&username="+user.getLoginName()+"&password="+user.getPassword();
					else if("client_credentials".equalsIgnoreCase(aut.getGrantType()))
						url= aut.getAccessTokenUrl()+"?grant_type="+aut.getGrantType()+"&scope="+user.getLoginUserType();
					else if("implicit".equalsIgnoreCase(aut.getGrantType())){
						url = aut.getAuthUrl()+"?client_id="+aut.getClientId()+"&response_type=token&scope="+user.getLoginUserType()+"&redirect_uri="+aut.getCallBackUrl();
						type = "window";
					}
				}else{
					url= "ApplicationUserServlet?t=refreshtoken&&appId="+appId+"&userType="+user.getLoginUserType()+"&uacId="+uacId;
					type = "refresh";
				}
				
				if(type.isEmpty()){
					try {
						handler.processRequest(url, aut, user, false, aut.getGrantType());
						url = "ApplicationUserServlet?t=accessToken&uacId="+uacId;
						SessionUtils.setScreenState("success", "accesstoken.gettoken.new.success", request);
					} catch (Exception e) {
						logger.debug("Could not get access token. Message : {}.", e.getMessage());
						throw new RequestValidationException(e.getMessage());
					}
				}
				
				try {
					responseMsg.put("status", "SUCCESS");
					responseMsg.put("type", type);
					responseMsg.put("url", url);
				} catch (JSONException e) {}
			} catch (NumberFormatException e) {
				logger.debug("Error Message : {}.", e.getMessage());
				try {
					responseMsg.put("status", "error");
					responseMsg.put("message", "Invalid Application.");
				} catch (JSONException ignore) {}
			}catch(RequestValidationException e){
				logger.debug("Error Message : {}.", e.getMessage());
				try {
					responseMsg.put("status", "error");
					responseMsg.put("message", "Could not get Access Token.");
				} catch (JSONException ignore) {}
			} catch (Exception e) {
				logger.debug("Error Message : {}.", e.getMessage());
				try {
					responseMsg.put("status", "error");
					responseMsg.put("message", "An internal error occurred. Please contact Tenjin Support.");
				} catch (JSONException ignore) {}
			}finally{
				map.put("OAUTH_AUT_DETAILS", aut);
				map.put("OAUTH_USER_DETAILS", user);
				request.getSession().setAttribute("OAUTH_MAP", map);
				response.getWriter().write(responseMsg.toString());
			}
		}
		//This will be our call back url
		else if(task.equalsIgnoreCase("getaccesstoken")){
			Aut autDetails = null;
			ApplicationUser user = new ApplicationUser();
			try {
				String code = request.getParameter("code");
				autDetails = new AutsHelper().hydrateOAuthRecord(appId);
				
				String url = autDetails.getAccessTokenUrl()+"?code="+code+"&grant_type="+autDetails.getGrantType()+"&redirect_uri="+autDetails.getCallBackUrl();
				user.setUacId(uacId);
				JSONObject tokenJson = handler.processRequest(url, autDetails, user, false, autDetails.getGrantType());
				
				if(handler.checkAuthResponse(tokenJson)){
					SessionUtils.setScreenState("success", "accesstoken.gettoken.new.success", request);
				}else{
					List<String> keys = Arrays.asList(JSONObject.getNames(tokenJson));
					if(keys.contains("error")){
						logger.debug(tokenJson.getString("error") +" : "+ tokenJson.getString("error_description"));
						throw new Exception("Could not get access token.");
					}
				}
			} catch (Exception e) {
				logger.debug("Error while generating the token.");
				SessionUtils.setScreenState("error", "accesstoken.gettoken.failure", request);
			}
			request.setAttribute("closeFlag", "true");
			request.setAttribute("oauth", user);
			request.getRequestDispatcher("v2/accesstoken_details.jsp").forward(request, response);
		}else if(task.equalsIgnoreCase("refreshtoken")){
			String redirectUrl = null;
			Aut autDetails = null;
			ApplicationUser userDetails = null;
			try {
				@SuppressWarnings("unchecked")
				Map<String, Object> map = (Map<String, Object>)request.getSession().getAttribute("OAUTH_MAP");
				autDetails = (Aut)map.get("OAUTH_AUT_DETAILS");
				userDetails = (ApplicationUser)map.get("OAUTH_USER_DETAILS");
				String url = "";
				String grantType = autDetails.getGrantType();
				if(grantType.equalsIgnoreCase("password") || grantType.equalsIgnoreCase("authorization_code")){
					//Incase of Authorization code and Password
					url = autDetails.getAccessTokenUrl()+"?grant_type=refresh_token&refresh_token="+userDetails.getRefreshToken();	
				}
				
				JSONObject tokenJson = handler.processRequest(url, autDetails, userDetails, false, autDetails.getGrantType());
				
				if(handler.checkAuthResponse(tokenJson)){
					redirectUrl = "ApplicationUserServlet?t=accessToken&uacId="+userDetails.getUacId();
					SessionUtils.setScreenState("success", "accesstoken.gettoken.refresh.success", request);
				}else{
					List<String> keys = Arrays.asList(JSONObject.getNames(tokenJson));
					if(keys.contains("status")){
						SessionUtils.setScreenState("error", "accesstoken.gettoken.failure", request);
						redirectUrl = "ApplicationUserServlet?t=accessToken&uacId="+userDetails.getUacId();
					}else{
						logger.debug("Failure to refresh the token.  -> {}", tokenJson.getString("error_description"));
						redirectUrl = "ApplicationUserServlet?t=accessToken&caller=refresh&uacId="+userDetails.getUacId();
					}
				}
			} catch (Exception e) {
				logger.debug("Error while generating the token.");
				SessionUtils.setScreenState("error", "accesstoken.gettoken.failure", request);
				redirectUrl = "ApplicationUserServlet?t=accessToken&uacId="+userDetails.getUacId();
			}
			request.getSession().removeAttribute("OAUTH_MAP");
			request.getRequestDispatcher(redirectUrl).forward(request, response);
		}else if(task.equalsIgnoreCase("extract_token")){
			Date tokenGenTime = new Date();
			Map<String, String[]> paramMap = request.getParameterMap();
			JSONObject json = new JSONObject();
			ApplicationUser user = new ApplicationUser();
			try {
				for (String paramKey : paramMap.keySet()) {
					String[] array = paramMap.get(paramKey);
					if(array.length > 0){
						json.put(paramKey, array[0]);
					}
				}
				json.put("generation_date", new Timestamp(tokenGenTime.getTime()));
				user.setUacId(uacId);
				user = handler.mapJSONtoOAuthDetails(user, json);
				handler.persistAccessToken(user, false, "implicit");
				SessionUtils.setScreenState("success", "accesstoken.gettoken.new.success", request);
			} catch (JSONException e1) {
				logger.debug("Error while generating the token.");
				SessionUtils.setScreenState("error", "accesstoken.gettoken.failure", request);
			} catch (DatabaseException e) {
				logger.debug("Error while generating the token.");
				SessionUtils.setScreenState("error", "accesstoken.gettoken.failure", request);
			} catch (RequestValidationException e) {
				logger.debug("Error while generating the token.");
				SessionUtils.setScreenState("error", "accesstoken.gettoken.failure", request);
			}
			request.getRequestDispatcher("ApplicationUserServlet?t=accessToken&uacId="+user.getUacId()).forward(request, response);
		}
	}
	/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 ends*/
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		TenjinSession tjnSession = SessionUtils.getTenjinSession(request);
		User cUser=tjnSession.getUser();
		String userId=cUser.getId();
		String delFlag = Utilities.trim(request.getParameter("del"));
		request.setAttribute("userId", userId);
		ApplicationUser applicationUser=null;
		if(delFlag.equalsIgnoreCase("true")){
			doDelete(request, response);
		}
		/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Starts*/
		else if(delFlag.equalsIgnoreCase("saveAccessToken")){
			ApplicationUser oAuth = new ApplicationUser();
			String uacId = request.getParameter("uacId");
			String grantType = request.getParameter("grantTypeHide");
			ApplicationUserHandler handler = new ApplicationUserHandler();
			try{
				oAuth = this.mapAttributesAccessToken(request);
				handler.persistAccessToken(oAuth, true, grantType);
				SessionUtils.setScreenState("success", "accesstoken.save.success", request);
				response.sendRedirect("ApplicationUserServlet?t=accessToken&uacId=" +oAuth.getUacId());
			}catch (DatabaseException e) {
				logger.error("ERROR while persiting OAUTH2 information", e.getMessage()); 
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("oauth", oAuth);
				request.setAttribute("appId", oAuth.getAppId());
				request.getRequestDispatcher("v2/accesstoken_details.jsp").forward(request, response);
			}catch (RequestValidationException e) {
				logger.error("ERROR while persiting OAUTH2 information", e.getMessage());
				SessionUtils.setScreenState("error", e.getMessage(),e.getTargetFields(), request);
				request.setAttribute("oauth", oAuth);
				request.setAttribute("appId", oAuth.getAppId());
				response.sendRedirect("ApplicationUserServlet?t=accessToken&uacId=" +uacId);
			}
		}
		/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Ends*/
		else{
			
			String uacId = Utilities.trim(request.getParameter("uacId"));
			applicationUser=mapAttributes(request);
			applicationUser.setUserId(userId);
			
			ApplicationUserHandler handler=new ApplicationUserHandler();
			String dispatchUrl = "";
			if(uacId.length() < 1 ){
				try {
					logger.info("Request to create new Application user");
					handler.persistApplicationUser(applicationUser);
					SessionUtils.setScreenState("success", "applicationUser.create.success", request);
					response.sendRedirect("ApplicationUserServlet?t=view&key=" + applicationUser.getUacId());
				} catch (RequestValidationException e) {
					logger.error(e.getMessage());
					dispatchUrl = "v2/applicationuser_new.jsp";
					SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("applicationUser", applicationUser);
					request.getRequestDispatcher(dispatchUrl).forward(request, response);
				} catch (DatabaseException e) {
					logger.error(e.getMessage(), e);
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("applicationUser", applicationUser);
					request.getRequestDispatcher("v2/applicationuser_new.jsp").forward(request, response);
				} 
				
				}else{
				try {
					applicationUser.setUacId(Integer.parseInt(uacId));
					logger.info("Request to update Application User.");
					handler.updateApplicationUser(applicationUser, userId);
					SessionUtils.setScreenState("success", "applicationUser.update.success", request);
					response.sendRedirect("ApplicationUserServlet?t=view&key=" + Integer.parseInt(uacId));
				} catch (RequestValidationException e) {
					logger.error(e.getMessage());
					dispatchUrl = "v2/applicationuser_view.jsp";
					SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("applicationUser", applicationUser);
					request.getRequestDispatcher(dispatchUrl).forward(request, response);
				} catch (DatabaseException e) {
					logger.error(e.getMessage());
					dispatchUrl = "v2/applicationuser_view.jsp";
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("applicationUser", applicationUser);
					request.getRequestDispatcher(dispatchUrl).forward(request, response);
				}
				} 
		}
		

}
	private ApplicationUser mapAttributes(HttpServletRequest request) {
		ApplicationUser applicationUser=new ApplicationUser();
		applicationUser.setLoginName(Utilities.trim(request.getParameter("loginName")));
		applicationUser.setAppId(Integer.parseInt(request.getParameter("aut")));
		applicationUser.setLoginUserType(Utilities.trim(request.getParameter("autLoginType")));
		applicationUser.setPassword(Utilities.trim(request.getParameter("loginPassword")));
		applicationUser.setTxnPassword(Utilities.trim(request.getParameter("transactionPassword")));
		
		return applicationUser;
	}
	
	/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Starts*/
	@SuppressWarnings("deprecation")
	private ApplicationUser mapAttributesAccessToken(HttpServletRequest request) throws RequestValidationException {
		ApplicationUser oAuth = new ApplicationUser();
		oAuth.setUacId(Integer.parseInt(request.getParameter("uacId")));
		oAuth.setAppId(Integer.parseInt(request.getParameter("appId")));
		oAuth.setAccessToken(request.getParameter("accessToken"));
	    oAuth.setRefreshToken(request.getParameter("refreshToken"));
	    oAuth.setTokenValidity(request.getParameter("tokenValidity"));
	    String tokengentime = request.getParameter("tokenGenTime");
	    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	    try {
			dateFormat.setLenient(false);
	    	Date ts = dateFormat.parse(tokengentime);
			oAuth.setTokenGenTime(new Timestamp(ts.getTime()));
		} catch (ParseException e) {
			throw new RequestValidationException("Invalid date.");
		}
	    
		return oAuth;
	}
	
	/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Ends*/
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String[] selectedUacIds = request.getParameterValues("selectedApplicationUser");
		ApplicationUserHandler handler=new ApplicationUserHandler();
		try {
			handler.deleteApplicationUser(selectedUacIds);
			if(selectedUacIds.length>1){
				SessionUtils.setScreenState("success", "applicationUser.delete.multiple.success", request);
			}else{
				SessionUtils.setScreenState("success", "applicationUser.delete.success",request);
			}
			response.sendRedirect("ApplicationUserServlet");
		} catch (DatabaseException e) {
			SessionUtils.setScreenState("error", "generic.db.error", request);
			SessionUtils.loadScreenStateToRequest(request);
			logger.error("Error ", e);
			response.sendRedirect("ApplicationUserServlet");
		}
		
	}
}
