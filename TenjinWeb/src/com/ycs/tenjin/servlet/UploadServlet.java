/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UploadServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 10-Aug-2017		sriram sridharan		Fix for T25IT-80
 * 26-Oct-2017		  Roshni				   TENJINCG-305
 * 06-Dec-2017         Padmavathi               TCGST-2
 * 07-Dec-2017         Padmavathi               TCGST-2
 * 18-01-2018		  Pushpalatha			   TENJINCG-567
 * 20-06-2018          Padmavathi               T251IT-93
 * 22-06-2018		  Padmavathi			   T251IT-59
  27-09-2018		  Pushpa				   Removed comments
 * 27-09-2018		  Pushpa				   TENJINCG-740
 * 12-12-2018		  Ashiki				   TJNUN262-107
 * 28-05-2019		  Ashiki				   V2.8-70
 * 23-09-2019		  Pushpalatha			   TENJINCG-1102
 * 
 */

/*27-Mar-2015 R2.1: for Defect ##1324-By Babu: Upload TestCases and Teststeps is not uploading at server location: Starts*/
package com.ycs.tenjin.servlet;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.Tika;
import org.slf4j.Logger;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.util.ExcelHandler;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class UploadServlet
 */
/*@WebServlet(name = "/UploadServlet", urlPatterns = { "//UploadServlet" })*/
public class UploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(UploadServlet.class);


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UploadServlet() {
		super();
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		TenjinSession tjnsession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		String dUrl = "";
		String uploadFrom="";
		String mode="";
		String setName="";
		String setId="";
		/*Modified by Pushpa for TENJINCG-740 starts*/
		Map<String, Object> map = new HashMap<String, Object>();
		try{
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			/*modified by paneendra for VAPT fix starts*/
			String uploadpath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
			File uploadFilePath= new File(uploadpath+File.separator+"Uploads");
			/*modified by paneendra for VAPT fix ends*/
			/*Added by Pushpa for TENJINCG-1102 ends*/
			/*added by paneendra for VAPT fix starts*/
			if(!uploadFilePath.exists()){
				uploadFilePath.mkdirs();
			}/*added by paneendra for VAPT fix ends*/
			List items = upload.parseRequest(request);
			Iterator iterator = items.iterator();
			String parent="";
			String fileName="";
			File uploadedFileRP=null;
			while(iterator.hasNext()){
				FileItem xlFile = (FileItem)iterator.next();
				if(!xlFile.isFormField()){
					/*Modified by Pushpa for TENJINCG-1027 starts*/
					fileName=xlFile.getName();
					//	Added  By Priyanka for VAPT Starts
					if(!(fileName.endsWith(".xls") || fileName.endsWith(".xlsx")))
					{
						throw new RequestValidationException("Invalid Excel sheet");
					}
					//	Added  By Priyanka for VAPT ends
					if(fileName.contains("\\")){
						String file[]=null;
						file=fileName.split("\\\\");
						fileName=file[file.length-1];
					}else{

						fileName = StringUtils.deleteWhitespace(xlFile.getName());
					}
					/*Modified by Pushpa for TENJINCG-1027 ends*/
					uploadedFileRP = new File(uploadFilePath.getPath()+File.separator + fileName);
					xlFile.write(uploadedFileRP);
				}
				else if(xlFile.getFieldName().equalsIgnoreCase("testset")){
					uploadFrom=xlFile.getFieldName();
				}
				else if(xlFile.getFieldName().equalsIgnoreCase("mode")){
					mode = xlFile.getString();
				}
				else if(xlFile.getFieldName().equalsIgnoreCase("setName")){
					setName = xlFile.getString();

				} 
				else if(xlFile.getFieldName().equalsIgnoreCase("setId")){
					setId = StringUtils.deleteWhitespace( xlFile.getString());

				} 
				else
				{
					parent=xlFile.getString();
				}
			}
			
			Tika tika = new Tika();
			String Exfile ="";
			try {
				Exfile = tika.detect(uploadedFileRP);
			} catch (IOException e) {
				logger.debug("Unable to Detect the File Content: " + e.getMessage());
				logger.error("Error ", e);
			}



			//Exfile = Files.probeContentType(Paths.get(folderPath, fileName));
			if(!(Exfile.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))){
				throw new RequestValidationException("Invalid Excel sheet");

			}
			/*Added  By Priyanka for VAPT and Tenj212-16 ends*/
			/*Added by paneendra for VAPT fix starts*/
			String folderPath=uploadFilePath.getPath();
			/*Addedd by paneendra for VAPT fix ends*/
			String fileNameWithOutExt = FilenameUtils.removeExtension(fileName);
			String file=fileNameWithOutExt+"-output."+fileName.substring(fileName.lastIndexOf('.') + 1);
			file=StringUtils.deleteWhitespace(file);
			ExcelHandler e=new ExcelHandler();
			long startMillis = System.currentTimeMillis();
			String tc_id="";
			if(uploadFrom.equalsIgnoreCase("Testset")&& !setName.equalsIgnoreCase("null")){

				tc_id = e.processTestCaseUploadSheet(folderPath, fileNameWithOutExt, tjnsession.getProject().getId(),mode,setName,tjnsession.getUser().getId(),setId);
				map.put("message", "TestCases and TestSteps created and mapped to the TestSet successfully.");
				/*Added By Ashiki for V2.8-70 starts*/
				map.put("param", setId);
				/*Added By Ashiki for V2.8-70 end*/
			}else if(uploadFrom.equalsIgnoreCase("Testset")){

				tc_id = e.processTestSetUploadSheet(folderPath, fileNameWithOutExt, tjnsession.getProject().getId(),tjnsession.getUser().getId());
				map.put("message", "Testset(s) and Testcase(s)  uploaded successfully. Please check confirmation sheet for more information.");
				map.put("param", "testset"); 

			}else{
				tc_id = e.processTestCaseUploadSheet(folderPath, fileNameWithOutExt, tjnsession.getProject().getId(), tjnsession.getUser().getId());
				map.put("message", "Testcases and Teststeps uploaded successfully. Please check confirmation sheet for more information.");
			}
			logger.info("TC_UPLOAD_TIMER >> {}", Utilities.calculateElapsedTime(startMillis, System.currentTimeMillis()));

			logger.info("Upload finished with status {}", tc_id);

			/*commented by paneendra for VAPT fix starts*/
			/*String targetpath=request.getSession().getServletContext().getRealPath("/") +File.separator+"Uploads"+File.separator;*/
			/*commented by paneendra for VAPT fix ends*/



			map.put("status", "SUCCESS");
			/*modified by paneendra for download file in aws starts*/
			//map.put("path","Uploads"+File.separator+File.separator+file);
			map.put("path",File.separator+"Uploads"+File.separator+File.separator+file);
			/*modified by paneendra for download file in aws ends*/
			map.put("tc_id", "");
		}
		/*Added by Ashiki for TJNUN262-107 starts*/
		catch (RequestValidationException e) {
			map.put("status", "ERROR");
			map.put("message","Please Upload Valid Excel Sheet");
		}
		/*Added by Ashiki for TJNUN262-107 ends*/
		catch(Exception e){
			logger.error("An exception occured while deleteing test steps",e);
			map.put("status", "ERROR");
			map.put("message","An Internal Error Occurred. Please try again");
		}
		request.getSession().setAttribute("SCR_MAP", map);
		if(uploadFrom.equalsIgnoreCase("Testset")&& !setName.equalsIgnoreCase("null")){
			RequestDispatcher rs=request.getRequestDispatcher("uploadFromTestSet.jsp");
			rs.forward(request, response);
		}else
			if(uploadFrom.equalsIgnoreCase("testset")){
				dUrl = "uploadtemp.jsp";
				response.sendRedirect(dUrl);
			}
			else{
				dUrl = "uploadtemp.jsp";
				response.sendRedirect(dUrl);

			}

	}
	/*Modified by Pushpa for TENJINCG-740 ends*/
}

