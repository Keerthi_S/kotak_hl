/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ProjectRunServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 28-July-2017			Gangadhar Badagi		TENJINCG-315
*/

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ExecutionHelper;
import com.ycs.tenjin.db.UserHelper;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.user.User;

/**
 * Servlet implementation class ProjectRunServlet
 */
//@WebServlet("/ProjectRunServlet")
public class ProjectRunServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjectRunServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action = "";
		
		//If our action is PROJECT_RUNS, then fetch all runs for this project and display
		action = request.getParameter("param");
		if(action != null && action.equalsIgnoreCase("PROJECT_RUNS")){
			String p = request.getParameter("pid");
			/*Changed by Gangadhar Badagi for TENJINCG-315 starts*/
			String userId = request.getParameter("uid");
			String status = request.getParameter("status");
			/*Changed by Gangadhar Badagi for TENJINCG-315 ends*/
			int projectId = Integer.parseInt(p);
			ExecutionHelper helper = new ExecutionHelper();
			Map<String, Object> map = new HashMap<String, Object>();
			/*Changed by Gangadhar Badagi for TENJINCG-315 starts*/
			List<TestRun> runs=new ArrayList<TestRun>();
			try {
				
				runs = helper.hydrateRunsForProject(projectId,userId,status);
				/* To Include Test Set name in the run details - Arvind */
				List<User> currentUsers=new UserHelper().hydrateCurrentUsersInProject(Integer.valueOf(p));
				/*Changed by Gangadhar Badagi for TENJINCG-315 ends*/				
				map.put("STATUS", "SUCCESS");
				map.put("ALL_RUNS", runs);
				/*Changed by Gangadhar Badagi for TENJINCG-315 starts*/
				map.put("PRJ_USERS", currentUsers);
				/*Changed by Gangadhar Badagi for TENJINCG-315 ends*/
			} catch (DatabaseException e) {
				
				map.put("STATUS","ERROR");
				map.put("MESSAGE", e.getMessage());
			}finally{
				request.getSession().setAttribute("PRJ_RUN_MAP", map);
			}
			
			response.sendRedirect("projectruns.jsp");
		}
		
		//If the action is anything else, then do specific task
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int projectId = 0;
		int runId = 0;
		String runUser = "";
		String machineIp = "";
		String status = "";
		
		String p = request.getParameter("pid");
		projectId = Integer.parseInt(p);
		String r = request.getParameter("runId");
		runId = Integer.parseInt(r);
		runUser = request.getParameter("runUser");
		machineIp = request.getParameter("machineIp");
		status = request.getParameter("status");
		ArrayList<TestRun> runs = null;
		ExecutionHelper helper = new ExecutionHelper();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			runs = helper.filterProjectRuns(projectId, runId, runUser, machineIp, status);
			map.put("STATUS", "SUCCESS");
			map.put("ALL_RUNS", runs);
		} catch (DatabaseException e) {
			
			map.put("STATUS","ERROR");
			map.put("MESSAGE", e.getMessage());
		}finally{
			request.getSession().setAttribute("PRJ_RUN_MAP", map);
		}
		
		response.sendRedirect("projectruns.jsp");
		
	}

}
