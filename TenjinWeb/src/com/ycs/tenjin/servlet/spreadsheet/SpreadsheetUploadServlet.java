package com.ycs.tenjin.servlet.spreadsheet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.spreadsheet.GenericSpreadsheetHandler;
import com.ycs.tenjin.spreadsheet.aut.AutSpreadsheetHandler;
import com.ycs.tenjin.spreadsheet.exception.GenericSpreadsheetException;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class GenericSpreadsheetServlet
 */
//@WebServlet("/GenericSpreadsheetServlet")
public class SpreadsheetUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(SpreadsheetUploadServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SpreadsheetUploadServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		boolean isMultiPart;
		isMultiPart = ServletFileUpload.isMultipartContent(request);
		JsonObject json = new JsonObject();
		if(isMultiPart){
			try {
				logger.debug("Form is multipart");
				DiskFileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				List<FileItem> fileItems = upload.parseRequest(request);
				
				Iterator<FileItem> fileItemsIter = fileItems.iterator();
				
				Map<String, String> formValues = new HashMap<String, String>();
				List<FileItem> uploadedFiles = new ArrayList<FileItem>();
				while(fileItemsIter.hasNext()){
					FileItem fi = (FileItem)fileItemsIter.next();
					if(fi.isFormField()){
						formValues.put(fi.getFieldName(), fi.getString());
					}else{
						uploadedFiles.add(fi);
					}
				}
				
				String requestType = Utilities.trim(formValues.get("requesttype"));
				if(requestType.equalsIgnoreCase("metadata")) {
					String appId = Utilities.trim(formValues.get("aut-selection"));
					String functionCode = Utilities.trim(formValues.get("func-selection"));
					if(uploadedFiles.size() > 0) {
						FileItem uploadedFile = uploadedFiles.get(0);
						String tempFolderPath = GenericSpreadsheetHandler.generateTempFolder();
						File tempFile = new File(tempFolderPath + File.separator + uploadedFile.getName());
						uploadedFile.write(tempFile);
						
						
						AutSpreadsheetHandler aHandler = new AutSpreadsheetHandler(tjnSession.getUser());
						aHandler.importMetadata(tempFolderPath + File.separator + uploadedFile.getName(), Integer.parseInt(appId), functionCode);
						json.addProperty("status", "success");
						json.addProperty("message", "Metadata Uploaded successfully!");
					}else {
						json.addProperty("status", "error");
						json.addProperty("message", "No file was uploaded. Please upload a valid file and try again.");
					}
					
					
				}
			} catch (FileUploadException e) {
				logger.error("ERROR occurred while uploading file(s)", e);
				json.addProperty("status", "error");
				json.addProperty("message", "An internal error occurred. Please contact Tenjin Support.");
			} catch (GenericSpreadsheetException e) {
				logger.error("ERROR occurred while uploading file(s)", e);
				json.addProperty("status", "error");
				json.addProperty("message", e.getMessage());
			}catch (Exception e) {
				logger.error("ERROR occurred while uploading file(s)", e);
				json.addProperty("status", "error");
				json.addProperty("message", "An internal error occurred. Please contact Tenjin Support.");
			}
		}else {
			logger.error("This servlet accepts only multi-part form post.");
			json.addProperty("status", "error");
			json.addProperty("message", "An internal error occurred. Please contact Tenjin Support.");
		}
		
		
		response.getWriter().write(json.toString());
	}
	
	

}
