/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExtractorServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION 

19-Jan-2018			Pushpalatha				TenjinCG-581
*/



package com.ycs.tenjin.servlet.spreadsheet;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.spreadsheet.GenericSpreadsheetHandler;
import com.ycs.tenjin.spreadsheet.aut.AutSpreadsheetHandler;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class SpreadsheetDownloadServlet
 */
//@WebServlet("/SpreadsheetDownloadServlet")
public class SpreadsheetDownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(SpreadsheetDownloadServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SpreadsheetDownloadServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Modified By Prem for Tenj212-31 Start*/
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		String requestType = Utilities.trim(request.getParameter("requesttype"));
		 if("metadata".equalsIgnoreCase(requestType)) {
			String aut = request.getParameter("aut-selection");
			String functionCodes = request.getParameter("func-selection");
			JsonObject json = new JsonObject();
			try {
				AutSpreadsheetHandler autSpreadsheetHandler = new AutSpreadsheetHandler(tjnSession.getUser());
				
				File file = autSpreadsheetHandler.downloadMetadata(Integer.parseInt(aut), functionCodes.split(","));
				String fileName = this.prepareFileForDownload(file, request);
				
				if(Utilities.trim(fileName).length() > 0) {
					json.addProperty("status", "success");
					json.addProperty("message", "Export completed successfully!");
					json.addProperty("path", "Downloads"+File.separator + fileName);
				}else {
					json.addProperty("status", "error");
					json.addProperty("message", "Could not complete export due to an internal error. Please contact Tenjin Support.");
				}
			} catch (Exception e) {
				logger.error("ERROR while generating spreadsheet", e);
				json.addProperty("status", "error");
				json.addProperty("message", "Could not complete export due to an internal error. Please contact Tenjin Support.");
			} finally {
				response.getWriter().write(json.toString());
			}
		}
		 else {
			 /* Modified By Prem for Tenj212-31 End*/
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		String requestType = Utilities.trim(request.getParameter("requesttype"));
		
		if("aut".equalsIgnoreCase(requestType)) {
			String exportTypeAut = request.getParameter("aut-selection");
			String exportFunctionCodes = request.getParameter("func-selection");
			String includeMetadata = request.getParameter("metadata");
			
			boolean iMetadata = false;
			if(Utilities.trim(includeMetadata).equalsIgnoreCase("y") || Utilities.trim(includeMetadata).equalsIgnoreCase("yes")) {
				iMetadata = true;
			}
			
			AutSpreadsheetHandler autSpreadsheetHandler = new AutSpreadsheetHandler();
			File file = autSpreadsheetHandler.export(exportTypeAut, exportFunctionCodes, iMetadata);
			String fileName = this.prepareFileForDownload(file, request);
			JsonObject json = new JsonObject();
			if(Utilities.trim(fileName).length() > 0) {
				json.addProperty("status", "success");
				json.addProperty("message", "Export completed successfully!");
				json.addProperty("path", "Downloads/" + fileName);
			}else {
				json.addProperty("status", "error");
				json.addProperty("message", "Could not complete export due to an internal error. Please contact Tenjin Support.");
			}
			
			response.getWriter().write(json.toString());
		}/*
		Modified By Prem for Tenj212-31 Start
else if("metadata".equalsIgnoreCase(requestType)) {
			String aut = request.getParameter("aut-selection");
			String functionCodes = request.getParameter("func-selection");
			JsonObject json = new JsonObject();
			try {
				AutSpreadsheetHandler autSpreadsheetHandler = new AutSpreadsheetHandler(tjnSession.getUser());
				
				File file = autSpreadsheetHandler.downloadMetadata(Integer.parseInt(aut), functionCodes.split(","));
				String fileName = this.prepareFileForDownload(file, request);
				
				if(Utilities.trim(fileName).length() > 0) {
					json.addProperty("status", "success");
					json.addProperty("message", "Export completed successfully!");
					Changed by Pushpalatha for TENJINCG-582 starts
					json.addProperty("path", "Downloads"+File.separator + fileName);
					Changed by Pushpalatha for TENJINCG-582 ends
				}else {
					json.addProperty("status", "error");
					json.addProperty("message", "Could not complete export due to an internal error. Please contact Tenjin Support.");
				}
			} catch (Exception e) {
				logger.error("ERROR while generating spreadsheet", e);
				json.addProperty("status", "error");
				json.addProperty("message", "Could not complete export due to an internal error. Please contact Tenjin Support.");
			} finally {
				response.getWriter().write(json.toString());
			}
		}
Modified By Prem for Tenj212-31 End
*/
 	}

	
	private String prepareFileForDownload(File fileToDownload, HttpServletRequest request) {
		
		if(fileToDownload == null) {
			logger.warn("No File to download!");
			return null;
		}
		
		String downloadDir = request.getServletContext().getRealPath("/");
		
		Utilities.checkForDownloadsFolder(downloadDir);
		downloadDir += File.separator + "Downloads";
		
		File downloadFile = new File(downloadDir + File.separator + fileToDownload.getName());
		try {
			FileUtils.copyFile(fileToDownload, downloadFile);
			GenericSpreadsheetHandler.cleanUpTemp(fileToDownload.getParent());
			return downloadFile.getName();
		} catch (IOException e) {
			
			logger.error("ERROR preparing file for download", e);
			return null;
		}
		
		
	}
}
