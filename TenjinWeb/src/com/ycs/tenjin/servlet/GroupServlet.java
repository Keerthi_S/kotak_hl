/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Handler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 19-02-2018		   Pushpalatha			   Newly Added 
* 19-06-2018			Preeti					T251IT-79
* 04-12-2019			Preeti					TENJINCG-1174
*/
package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.aut.Group;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.handler.GroupHandler;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class GroupServlet
 */
@WebServlet("/GroupServlet")
public class GroupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory	.getLogger(GroupServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GroupServlet() {
        super();
       
    }
    
    public static AuthorizationRule authorizationRule = new AuthorizationRule() {
        
        @Override
        public boolean checkAccess(HttpServletRequest request) {
        	if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
                    return true;
            }
            return false;
        }  
    };

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SessionUtils.loadScreenStateToRequest(request);
		
		String task = Utilities.trim(request.getParameter("t"));
		
		Authorizer authorizer=new Authorizer();
		if(task.equalsIgnoreCase("new") || task.equalsIgnoreCase("view") || task.equalsIgnoreCase("list") || task.equalsIgnoreCase("")) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		if(task.length() < 1 || task.equalsIgnoreCase("list")) {
			try {
				List<Group> groups = new GroupHandler().getAllGroups();
				request.setAttribute("groups", groups);
				request.getRequestDispatcher("v2/group_list.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching all Groups", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
			
		}else if(task.equalsIgnoreCase("new")) {		
						
			request.getRequestDispatcher("v2/group_new.jsp").forward(request, response);
			
		}if(task.equalsIgnoreCase("view")) {
			
			String groupName=request.getParameter("paramval");
			int appId=Integer.parseInt(request.getParameter("a"));
			String type=request.getParameter("type");
			GroupHandler handler=new GroupHandler();
			 
			if(type.equalsIgnoreCase("function")){
				try {
					Group group = new GroupHandler().hydrateGroup(appId,groupName);
					request.setAttribute("type", type);
					request.setAttribute("group", group);
					ArrayList<ModuleBean> mapedMod = handler.hydrateModules(appId, groupName);
					request.setAttribute("MAPPEDMODULE", mapedMod);
					ArrayList<ModuleBean> unMapedMod = handler.hydrateModules(appId, "Ungrouped");	
					request.setAttribute("UNMAPPEDMODULE", unMapedMod);
					SessionUtils.loadScreenStateToRequest(request);
					request.getRequestDispatcher("v2/group_view.jsp").forward(request, response);
				} catch (NumberFormatException e) {
					logger.error("Invalid App ID {}", appId);
					request.getRequestDispatcher("error.jsp").forward(request, response);
					logger.error("Error ", e);
				} catch (DatabaseException e) {
					logger.error("ERROR fetching group information", e);
					request.getRequestDispatcher("error.jsp").forward(request, response);
				} catch (SQLException e) {
					logger.error("ERROR fetching group information", e);
					request.getRequestDispatcher("error.jsp").forward(request, response);
					logger.error("Error ", e);
			}
			}
			else{
				
				try {
					Group group = new GroupHandler().hydrateGroup(appId,groupName);
					request.setAttribute("type", type);
					request.setAttribute("group", group);
					ArrayList<Api> mapedMod = handler.hydrateApi(appId, groupName);
					request.setAttribute("MAPPEDAPI", mapedMod);
					ArrayList<Api> unMapedMod = handler.hydrateApi(appId, "Ungrouped");	
					request.setAttribute("UNMAPPEDAPI", unMapedMod);
					
					request.getRequestDispatcher("v2/group_view.jsp").forward(request, response);
				} catch (NumberFormatException e) {
					logger.error("Invalid App ID {}", appId);
					request.getRequestDispatcher("error.jsp").forward(request, response);
					logger.error("Error ", e);
				} catch (DatabaseException e) {
					logger.error("ERROR fetching group information", e);
					request.getRequestDispatcher("error.jsp").forward(request, response);
				} catch (SQLException e) {
					
					logger.error("Error ", e);
				}
			}
		}		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SessionUtils.loadScreenStateToRequest(request);
		
		logger.info("entered doPost()");
		String delFlag = Utilities.trim(request.getParameter("del"));
		
		 Authorizer authorizer=new Authorizer();
		 if(delFlag.equalsIgnoreCase("true")||delFlag.equalsIgnoreCase("functionMap")||delFlag.equalsIgnoreCase("apiMap")||delFlag.equalsIgnoreCase("new")||delFlag.equalsIgnoreCase("")) {
				if(!authorizer.hasAccess(request)){
					response.sendRedirect("noAccess.jsp");
					return;
				}
			}
		
		if(delFlag.equalsIgnoreCase("true")) {
			this.doDelete(request, response);
		}else if(delFlag.equalsIgnoreCase("functionMap")){
			/*Added by Preeti for T251IT-79 starts*/
			String type=request.getParameter("type");
			/*Added by Preeti for T251IT-79 ends*/
			String funcodes = request.getParameter("paramval");
			String funcodes1 = request.getParameter("paramval1");
			String groupName = request.getParameter("group");
			String appId = request.getParameter("appid");
			String groupDesc=request.getParameter("groupDesc");
			String[] record = funcodes.split(",");
			String[] record1 = funcodes1.split(",");
			GroupHandler handler=new GroupHandler();
			Group group=new Group();
			group.setGroupDesc(groupDesc);
			group.setAut(Integer.parseInt(appId));
			group.setGroupName(groupName);
			/*Added by Preeti for T251IT-79 starts*/
			Aut aut =null;
			ArrayList<ModuleBean> mapedMod = null;
			ArrayList<ModuleBean> unMapedMod = null;
			try {
				aut = new AutHandler().getApplication(Integer.parseInt(appId));
				group.setAppName(aut.getName());
				handler.unMapFunction(Integer.parseInt(appId),"Ungrouped",record1);
				handler.mapFunction(Integer.parseInt(appId),groupName,record);
				mapedMod = handler.hydrateModules(Integer.parseInt(appId), groupName);
				unMapedMod = handler.hydrateModules(Integer.parseInt(appId), "Ungrouped");
				/*Added by Preeti for T251IT-79 ends*/
				handler.updateGroup(group);
				SessionUtils.setScreenState("success","group.save.success",request);
				response.sendRedirect("GroupServlet?t=view&a="+appId+"&paramval="+groupName+"&type=function");
			} catch (NumberFormatException e) {
				
				logger.error("Invalid App ID {}", appId);
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
				
			} catch (DatabaseException e) {
				logger.error("ERROR fetching group information", e);
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			/*Added by Preeti for T251IT-79 starts*/
			} catch (RequestValidationException e) {
				logger.error(e.getMessage());
				SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("group", group);
				request.setAttribute("MAPPEDMODULE", mapedMod);	
				request.setAttribute("UNMAPPEDMODULE", unMapedMod);
				request.setAttribute("type", type);
				request.getRequestDispatcher("v2/group_view.jsp").forward(request, response);
				logger.error("Error ", e);
			} catch (SQLException e) {
				logger.error("ERROR fetching group information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			/*Added by Preeti for T251IT-79 ends*/
			}
		}else if (delFlag.equalsIgnoreCase("apiMap")) {
			/*Added by Preeti for T251IT-79 starts*/
			String type=request.getParameter("type");
			/*Added by Preeti for T251IT-79 ends*/
			String funcodes = request.getParameter("paramval");
			String funcodes1=request.getParameter("paramval1");
			String groupName = request.getParameter("group");
			String groupDesc=request.getParameter("groupDesc");
			String appId = request.getParameter("appid");
			String[] record = funcodes.split(",");
			String[] record1 = funcodes1.split(",");
			GroupHandler handler=new GroupHandler();
			Group group=new Group();
			group.setGroupDesc(groupDesc);
			group.setAut(Integer.parseInt(appId));
			group.setGroupName(groupName);
			/*Added by Preeti for T251IT-79 starts*/
			Aut aut =null;
			ArrayList<Api> mapedMod = null;
			ArrayList<Api> unMapedMod = null;
			try {
				aut = new AutHandler().getApplication(Integer.parseInt(appId));
				group.setAppName(aut.getName());	
				handler.unMapApi(Integer.parseInt(appId),"Ungrouped",record1);
				handler.mapApi(Integer.parseInt(appId),groupName,record);
				mapedMod = handler.hydrateApi(Integer.parseInt(appId), groupName);
				unMapedMod = handler.hydrateApi(Integer.parseInt(appId), "Ungrouped");
				/*Added by Preeti for T251IT-79 ends*/
				handler.updateGroup(group);
				SessionUtils.setScreenState("success","group.save.success",request);
				response.sendRedirect("GroupServlet?t=view&a="+appId+"&paramval="+groupName+"&type=Api");
				
			} catch (NumberFormatException e) {
				
				logger.error("Invalid App ID {}", appId);
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
				
			} catch (DatabaseException e) {
				
				logger.error("ERROR fetching group information", e);
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			/*Added by Preeti for T251IT-79 starts*/
			} catch (RequestValidationException e) {
				logger.error(e.getMessage());
				SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("group", group);
				request.setAttribute("MAPPEDAPI", mapedMod);
				request.setAttribute("UNMAPPEDAPI", unMapedMod);
				request.setAttribute("type", type);
				request.getRequestDispatcher("v2/group_view.jsp").forward(request, response);
				logger.error("Error ", e);
			} catch (SQLException e) {
				logger.error("ERROR fetching group information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			/*Added by Preeti for T251IT-79 ends*/
			}
		} else if(delFlag.equalsIgnoreCase("new")){
			
			Group group=new Group();
			Aut aut=null;
			try {
				if(request.getParameter("aut").equals("-1")){
					throw new RequestValidationException("field.mandatory", "Application");
				}
				aut = new AutHandler().getApplication(Integer.parseInt(request.getParameter("aut")));
							
				group.setAut(Integer.parseInt(request.getParameter("aut")));
				group.setAppName(aut.getName());
				group.setGroupName(Utilities.trim(request.getParameter("groupName")));
				group.setGroupDesc(Utilities.trim(request.getParameter("groupDesc")));
				
				GroupHandler groupHandler=new GroupHandler();
				group=groupHandler.persistGroup(group);
				if(group==null){
					SessionUtils.setScreenState("error", "Group.already.exist", request);
					response.sendRedirect("GroupServlet?t=new");
				} else{
				SessionUtils.setScreenState("success", "Group.create.success", request);
				response.sendRedirect("GroupServlet?t=list");
				}
			} catch (DatabaseException e) {
				logger.error(e.getMessage(), e);
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("group", group);
				request.setAttribute("aut", aut);
				request.getRequestDispatcher("v2/group_new.jsp").forward(request, response);
			} catch (RequestValidationException e) {
				logger.error(e.getMessage());
				SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("group", group);
				request.setAttribute("aut", aut);
				request.getRequestDispatcher("v2/group_new.jsp").forward(request, response);
			}
		}
	}	
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] selectedGroups = request.getParameterValues("selectedGroups");
		GroupHandler handler=new GroupHandler();
		try {
			handler.deleteGroup(selectedGroups);
			if(selectedGroups.length>1){
				SessionUtils.setScreenState("success", "group.delete.multiple.success", request);
			}else{
				SessionUtils.setScreenState("success", "group.delete.success",request);
			}
			response.sendRedirect("GroupServlet");
		} catch (DatabaseException e) {
			SessionUtils.setScreenState("error", "generic.db.error", request);
			SessionUtils.loadScreenStateToRequest(request);
			logger.error("Error ", e);
			response.sendRedirect("GroupServlet");
		}
		
		
	}

}
