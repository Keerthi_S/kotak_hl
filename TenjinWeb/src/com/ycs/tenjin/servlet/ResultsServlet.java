/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ResultsServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 3-Nov-16				Sriram				Fix by Sriram for Defect#688
 * 12-01-2017			Manish					TENJINCG-8
 * 17-01-2017			Manish					TENJINCG-8
 * * 17-01-2017           Leelaprasad             TENJINCG-26
 * 18-01-2017           Leelaprasad            TENJINCG-42
 * 18-01-2017            Leelaprasad             TENJINCG-43
 * 15-02-2017			Manish					TENJINCG-121
 * 17-02-2017            Abhilash K N            TENJINCG-129
 * 16-06-2017			Sriram					TENJINCG-187
 * 27-Jul-2017			Sriram Sridharan		TENJINCG-312
 * 16-Aug-2017			Sriram Sridharan		T25IT-123
 * 17-Aug-2017			Sriram Sridharan		T25IT-212
 * 02-02-2018			Preeti					TENJINCG-101
 * 12-04-2018            Padmavathi              TENJINCG-633
 * 19-04-2018            Padmavathi              TENJINCG-630
 * 24-04-2018          	Padmavathi              TENJINCG-653
 * 03-05-2018            Leelaprasad              TENJINCG-639
 * 22-Dec-2017			Sahana					Mobility
 * 30-08-2018			Preeti					TENJINCG-729
 * 22-10-2018			Sriram					File Optimized for TENJINCG-876
 * 24-10-2018			Sriram					TENJINCG-866
 * 30-10-2018			Sriram Sridharan		TENJINCG-886
 * 26-Nov 2018			Shivam	Sharma			 TENJINCG-904
 * 25-Jan-2019			Sahana					pCloudy
 * 18-20-2019			Roshni					TJN252-8
 * 10-03-2020			Sriram					TENJINCG-1175 (TENJINCG-1194)
 * 13-03-2020			Sriram					TENJINCG-1196
 * 12-06-2020			Ashiki					Tenj210-87
 * 16-09-2020 			Pushpa					TENJINCG-1223
 * 17-09-2020           shruthi                 TENJINCG-1226
 * 25-09-2020            shruthi                  TENJINCG-1224
 */

/*Change History
17-Nov-2014 Adding delete functionality (Sneha): code changes are made
 */
package com.ycs.tenjin.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.pojo.project.UIValidation;
import com.ycs.tenjin.bridge.pojo.run.ResultValidation;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.RuntimeScreenshot;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.db.CoreDefectHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.DefectHelper;
import com.ycs.tenjin.db.DeviceHelper;
import com.ycs.tenjin.db.ParametersHelper;
/*17-Nov-2014 Adding delete functionality (Sneha): code changes ends here */
import com.ycs.tenjin.db.ResultsHelper;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.defect.CoreDefectProcessor;
import com.ycs.tenjin.defect.Defect;
import com.ycs.tenjin.device.RegisteredDevice;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.handler.ResultsHandler;
import com.ycs.tenjin.mail.TenjinJavaxMail;
import com.ycs.tenjin.mail.TenjinMailLogs;
import com.ycs.tenjin.pcloudy.Pcloudy;
import com.ycs.tenjin.pcloudy.PcloudyProcessor;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.ExcelHandler;
import com.ycs.tenjin.util.PdfHandler;
import com.ycs.tenjin.util.Utilities;
/**
 * Servlet implementation class ResultsServlet
 */
//@WebServlet("/ResultsServlet")
public class ResultsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(ResultsServlet.class);
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ResultsServlet() {
		super();
		
	}

	

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		String task = Utilities.trim(request.getParameter("param"));

		if(task.equalsIgnoreCase("run_result")) {
			int runId=0;
			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run ID [{}]", request.getParameter("run"));
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}

			ResultsHandler handler = new ResultsHandler();
			String redirect = "";
			TestRun run = null;
			boolean ignoreCallback = false;
			try {
				/*Modified by Pushpalatha for TENJINCG-1106 starts*/
				//run = handler.fetchRunSummary(runId);
				run=handler.getRunJson(runId);
				/*Modified by Pushpalatha for TENJINCG-1106 ends*/				
				//System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(run));
				request.setAttribute("run", run);
				
				
				redirect = "v2/runresult.jsp";
				//Changes for TENJINCG-1175 (TENJINCG-1194) (Sriram) ends

				if(run.getStatus().equalsIgnoreCase("executing") || run.getStatus().equalsIgnoreCase("aborting"))  {
					ignoreCallback = true;
					if(BridgeProcess.APIEXECUTE.equalsIgnoreCase(run.getTaskType())) {
						//TENJINCG-1196
						response.sendRedirect("TestRunServlet?param=run_progress&run=" + run.getId());
						return;
					}else {
						//TENJINCG-1196
						response.sendRedirect("TestRunServlet?param=run_progress&run=" + run.getId());
						//Fix for TENJINCG-866 (Sriram)
						return;
						//Fix for TENJINCG-866 (Sriram) ends
					}
				}
				//added by shivam sharma for  TENJINCG-904 starts
				RegisteredDevice device=null;
				//added by shivam sharma for  TENJINCG-904 ends
				if(run.getDeviceRecId()>0)
				{
					//added by shivam sharma for  TENJINCG-904 starts
					/*Added by Sahana for pCloudy */
					TestRun deviceFarmRun=new RunHelper().hydrateRun(DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ), run.getId());
					//	if(run.getMachine_ip().equalsIgnoreCase("999.999.999.999")){
					if(deviceFarmRun.getDeviceFarmFlag()!=null && deviceFarmRun.getDeviceFarmFlag().equalsIgnoreCase("Y")){
						/*Added by Sahana for pCloudy:Starts*/
						Pcloudy objPcloudy=new PcloudyProcessor().getPcloudyCredentials(deviceFarmRun.getMachine_ip());
						device=new PcloudyProcessor(objPcloudy).deviceById(run.getDeviceRecId());
						/*Added by Sahana for pCloudy:Ends*/
					}else{
						//added by shivam sharma for  TENJINCG-904 ends
						device=new DeviceHelper().hydrateDevice(run.getDeviceRecId());	
					}
					request.setAttribute("device", device);
				}
				request.setAttribute("callback", Utilities.trim(request.getParameter("callback")));
				if(!ignoreCallback) {
					redirect += "?callback=" + Utilities.trim(request.getParameter("callback")) + "&tcRecId=" + Utilities.trim(request.getParameter("tcRecId"));
					logger.info("Forward to {}", redirect);
					request.getRequestDispatcher(redirect).forward(request, response);
				}else {
					logger.info("Redirect to {}", redirect);
					response.sendRedirect(redirect);
				}

			} catch (RecordNotFoundException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error(e.getMessage(), e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (SQLException e) {
				
				logger.error("Error ", e);
			} finally {

			}
		} else if(task.equalsIgnoreCase("api_run_result")) {
			int runId=0;
			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run ID [{}]", request.getParameter("run"));
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}

			ResultsHandler handler = new ResultsHandler();
			String redirect = "";
			TestRun run = null;
			boolean ignoreCallback = false;


			try {
				run = handler.fetchRunSummary(runId, true, true);
				request.setAttribute("run", run);
				if(BridgeProcess.APIEXECUTE.equalsIgnoreCase(run.getTaskType())) {
					redirect = "v2/api_run_result.jsp";
				}else {
					redirect = "v2/runresult.jsp";
				}

				if(run.getStatus().equalsIgnoreCase("executing") || run.getStatus().equalsIgnoreCase("aborting"))  {
					ignoreCallback = true;
					if(BridgeProcess.APIEXECUTE.equalsIgnoreCase(run.getTaskType())) {
						redirect = "ExecutorServlet?param=api_run_progress&paramval=" + run.getId();
					}else {
						redirect = "ExecutorServlet?param=run_progress&paramval=" + run.getId();
					}
				}

				request.setAttribute("callback", Utilities.trim(request.getParameter("callback")));
				if(!ignoreCallback) {
					redirect += "?callback=" + Utilities.trim(request.getParameter("callback")) + "&tcRecId=" + Utilities.trim(request.getParameter("tcRecId"));
					logger.info("Forward to {}", redirect);
					request.getRequestDispatcher(redirect).forward(request, response);
				}else {
					logger.info("Redirect to {}", redirect);
					response.sendRedirect(redirect);
				}
			} catch (RecordNotFoundException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error(e.getMessage(), e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}



		} else if(task.equalsIgnoreCase("TEST_REPORT_GEN")) {
			int runId = 0;

			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("runid")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run Id {}", request.getParameter("runid"));
				JsonObject json = new JsonObject();
				json.addProperty("status", "error");
				json.addProperty("message", "Could not generate report due to an internal error. Please contact Tenjin Support.");
				response.getWriter().write(json.toString());
				return;
			}

			String screenshotOption = Utilities.trim(request.getParameter("screenShotOption"));
			JsonObject json = new JsonObject();
			try {
				String folderPath = request.getServletContext().getRealPath("/");


				Utilities.checkForDownloadsFolder(folderPath);

				folderPath = folderPath + "\\Downloads";
				String fileName =null;
				byte[] bop=null;
				PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), SessionUtils.getTenjinSession(request).getUser().getFullName());
				/* Modified by Padmavathi for TENJINCG-633 starts */
				if(screenshotOption!=null){
					//fileName = pdfHandler.generatePdfReportForRun(runId, SessionUtils.getTenjinSession(request).getProject().getId(),folderPath,screenshotOption);
					bop=pdfHandler.generatePdfReportForRunResult(runId, SessionUtils.getTenjinSession(request).getProject().getId(), folderPath, screenshotOption);				        
				}else{
					//fileName = pdfHandler.generatePdfReportForRun(runId, SessionUtils.getTenjinSession(request).getProject().getId(),folderPath);
					bop=pdfHandler.generatePdfReportForRunResult(runId, SessionUtils.getTenjinSession(request).getProject().getId(),folderPath);
				}
				fileName = "Tenjin_Execution_Report_Run_" + runId + ".pdf";
				response.setContentType("application/pdf");
				response.setHeader("Content-disposition", "attachment;filename=" + fileName);
		        response.setContentLength(bop.length);
		        try (BufferedInputStream bis = new BufferedInputStream(new ByteArrayInputStream(bop));
	                    BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());) {
	                byte[] buffer = new byte[1024];
	                int bytesRead = 0;
	                while ((bytesRead = bis.read(buffer)) != -1) {
	                    bos.write(buffer, 0, bytesRead);
	                }
	                bos.flush();
	            } catch (IOException e) {
	                throw new IOException();
	            }
				
				
			} catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create PDF due to an internal error",e);
			} 

		}else if(task.equalsIgnoreCase("defects")) {
			int runId = 0;
			Map<String, Object> map = new HashMap<>();
			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("runid")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run Id {}", request.getParameter("runid"));
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}

			ResultsHandler handler= new ResultsHandler();
			CoreDefectHelper cHelper = new CoreDefectHelper();
			try {
				TestRun run = handler.fetchRunSummary(runId, false);
				map.put("RUN", run);
				List<Defect> defects = cHelper.hydrateDefects(runId);
				List<Integer> distinctAppIds = new ArrayList<Integer>();
				if(defects != null) {
					for(Defect defect : defects) {
						if(!distinctAppIds.contains(defect.getAppId())){
							distinctAppIds.add(defect.getAppId());
						}
					}
				}


				Map<Integer, List<String>> sevMap = new DefectHelper().getSeverityDefinitions(SessionUtils.getTenjinSession(request).getProject().getId(), distinctAppIds);
				map.put("SEV_MAP", sevMap);
				map.put("DEFECTS", defects);

				map.put("STATUS", "SUCCESS");
				map.put("MESSAGE", "");
				String callback = request.getParameter("callback");
				if(com.ycs.tenjin.util.Utilities.trim(callback).equalsIgnoreCase("")){
					callback="";
				}
				request.getSession().removeAttribute("SCR_MAP");
				request.getSession().setAttribute("SCR_MAP", map);
				request.getRequestDispatcher("rundefectlist.jsp?callback=" + callback).forward(request, response);

			} catch (RecordNotFoundException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (DatabaseException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}else if(task.equalsIgnoreCase("tc_result")) {
			int testCaseRecordId = 0;
			int runId = 0;
			/*added by shruthi for TENJINCG-1226 starts*/
			String elapsedTime = null;
			/*added by shruthi for TENJINCG-1226 ends*/

			try {
				testCaseRecordId = Integer.parseInt(Utilities.trim(request.getParameter("tc")));
				runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
			} catch (NumberFormatException e) {
				logger.error("Error - Run ID({}) / Test Case Record ID({})", request.getParameter("run"), request.getParameter("tc"));
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}

			ResultsHandler handler  = new ResultsHandler();
			try {
				/*Modified by Pushpalatha for TENJINCG-1106 starts*/
				TestCase testCase=handler.getTestCaseResultSummary(runId,testCaseRecordId);
				/*Modified by Pushpalatha for TENJINCG-1106 ends*/
				request.setAttribute("testCase", testCase);
				/*added by shruthi for TENJINCG-1226 starts*/
				List<String> elapsedTimeList = new ArrayList<String>();
				ArrayList<TestStep> tcSteps = testCase.getTcSteps();
				for(TestStep teststep:tcSteps) {
					ArrayList<StepIterationResult> detailedResults = teststep.getDetailedResults();
					for(int i=0;i<detailedResults.size();i++) {
						String elapsedTimePerStep = detailedResults.get(i).getElapsedTime();
						if(elapsedTimePerStep!=null) {
						elapsedTimeList.add(elapsedTimePerStep);
						}   }
				}
				elapsedTime=Utilities.addElapsedTimes(elapsedTimeList);
				
				/*added by shruthi for TENJINCG-1226 ends*/

				String testCaseJson = new GsonBuilder().create().toJson(testCase);
				request.setAttribute("testCaseJson", testCaseJson);

			} catch (RecordNotFoundException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			} catch (DatabaseException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			} catch (SQLException e) {
				
				logger.error("Error ", e);
			}

			try {
				/*Modified by Pushpalatha for TENJINCG-1106 starts*/
				//TestRun run = handler.fetchRunSummary(runId, false);
				TestRun run=handler.getRunJson(runId);
				/*Modified by Pushpalatha for TENJINCG-1106 ends*/
				/*added by shruthi for TENJINCG-1226 starts*/
				run.setElapsedTime(elapsedTime);
				/*added by shruthi for TENJINCG-1226 ends*/
				request.setAttribute("run", run);
			} catch (RecordNotFoundException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			} catch (DatabaseException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			} catch (SQLException e) {
				
				logger.error("Error ", e);
			}
			String forwardTo = "v2/testcaseresult.jsp?callback=" + Utilities.trim(request.getParameter("callback"));
			logger.info("Forwarding to {}", forwardTo);
			request.getRequestDispatcher(forwardTo).forward(request, response);

		}else if(task.equalsIgnoreCase("run_time_values")) {
			int runId=0;
			int testStepRecordId = 0;
			String tdUid = Utilities.trim(request.getParameter("tduid"));

			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
				testStepRecordId = Integer.parseInt(Utilities.trim(request.getParameter("tsrecid")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run ID [{}] or Test Step Record ID [{}]", Utilities.trim(request.getParameter("run")), Utilities.trim(request.getParameter("tsrecid")));
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}

			try {
				List<RuntimeFieldValue> values = new ResultsHandler().fetchRuntimeFieldValues(runId, testStepRecordId, tdUid);
				request.setAttribute("runtimevalues", values);
				request.setAttribute("runId", runId);
				request.setAttribute("testCaseId", Utilities.trim(request.getParameter("tcid")));
				request.setAttribute("testStepId", Utilities.trim(request.getParameter("stepid")));
				request.setAttribute("tduid", tdUid);
				request.getRequestDispatcher("v2/runtimevalues.jsp").forward(request, response);
			} catch (DatabaseException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}


		}else if(task.equalsIgnoreCase("validation_results")) {
			int runId=0;
			int testStepRecordId = 0;
			int iteration = 0;

			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
				testStepRecordId = Integer.parseInt(Utilities.trim(request.getParameter("tsrecid")));
				iteration = Integer.parseInt(Utilities.trim(request.getParameter("txn")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run ID [{}] or Test Step Record ID [{}] or Iteration [{}]", Utilities.trim(request.getParameter("run")), Utilities.trim(request.getParameter("tsrecid")),Utilities.trim(request.getParameter("txn")));
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}

			try {
				StepIterationResult result = new ResultsHandler().fetchValidationResults(runId, testStepRecordId, iteration);
				request.setAttribute("result", result);
				request.setAttribute("runId", runId);
				request.setAttribute("testCaseId", Utilities.trim(request.getParameter("tcid")));
				request.setAttribute("testStepId", Utilities.trim(request.getParameter("stepid")));
				request.setAttribute("tduid", Utilities.trim(request.getParameter("tduid")));
				request.getRequestDispatcher("v2/res_val_results.jsp").forward(request, response);
			} catch (DatabaseException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}

		}else if(task.equalsIgnoreCase("view_screenshots")) {
			String tdUid = Utilities.trim(request.getParameter("tduid"));

			int runId=0;
			int iteration=0;
			int testStepRecordId = 0;

			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
				testStepRecordId = Integer.parseInt(Utilities.trim(request.getParameter("tsrecid")));
				iteration = Integer.parseInt(Utilities.trim(request.getParameter("txn")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run ID [{}] or Test Step Record ID [{}] or Iteration [{}]", Utilities.trim(request.getParameter("run")), Utilities.trim(request.getParameter("tsrecid")),Utilities.trim(request.getParameter("txn")));
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}


			try {
				Map<RuntimeScreenshot, String> imageMap = new ResultsHandler().getScreenshots(runId, testStepRecordId, iteration, tdUid);
				request.setAttribute("imageMap", imageMap);
				request.setAttribute("runId", runId);
				request.getRequestDispatcher("v2/screenshot.jsp").forward(request, response);
			} catch (DatabaseException | TenjinServletException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}else if(task.equalsIgnoreCase("api_request")) {
			String tdUid = Utilities.trim(request.getParameter("tduid"));
			int runId=0;
			int testStepRecordId = 0;

			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
				testStepRecordId = Integer.parseInt(Utilities.trim(request.getParameter("tsrecid")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run ID [{}] or Test Step Record ID [{}] or Iteration [{}]", Utilities.trim(request.getParameter("run")), Utilities.trim(request.getParameter("tsrecid")),Utilities.trim(request.getParameter("txn")));
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}

			try {
				Map<String, String> map = new ResultsHandler().getApiRequest(runId, testStepRecordId, tdUid);
				String contentType = Utilities.trim(map.get("contentType"));
				String fileName = Utilities.trim(map.get("name")) + "." + Utilities.trim(map.get("extn"));

				response.setContentType(contentType);
				response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

				try(OutputStream outputStream = response.getOutputStream()) {
					outputStream.write(map.get("content").getBytes());
					outputStream.flush();
				}
			} catch (DatabaseException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}

		}else if(task.equalsIgnoreCase("api_response")) {
			String tdUid = Utilities.trim(request.getParameter("tduid"));
			int runId=0;
			int testStepRecordId = 0;

			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
				testStepRecordId = Integer.parseInt(Utilities.trim(request.getParameter("tsrecid")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run ID [{}] or Test Step Record ID [{}] or Iteration [{}]", Utilities.trim(request.getParameter("run")), Utilities.trim(request.getParameter("tsrecid")),Utilities.trim(request.getParameter("txn")));
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}

			try {
				Map<String, String> map = new ResultsHandler().getApiResponse(runId, testStepRecordId, tdUid);
				String contentType = Utilities.trim(map.get("contentType"));
				String fileName = Utilities.trim(map.get("name")) + "." + Utilities.trim(map.get("extn"));

				response.setContentType(contentType);
				response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

				try(OutputStream outputStream = response.getOutputStream()) {
					outputStream.write(map.get("content").getBytes());
					outputStream.flush();
				}
			} catch (DatabaseException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}

		}else if(task.equalsIgnoreCase("api_messages")) {
			int runId=0;
			JsonObject resJson = new JsonObject();
			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
			} catch (NumberFormatException e) {
				resJson.addProperty("status", "error");
				resJson.addProperty("message", "Invalid Run ID");
				response.getWriter().write(resJson.toString());
				return;
			}

			try {
				String zipFileName = new ResultsHandler().generateAllTransactionsMessageDownload(runId);
				if(zipFileName != null) {
					resJson.addProperty("status", "success");
					resJson.addProperty("path", "Downloads/" + zipFileName);
				}else {
					resJson.addProperty("status", "success");
					resJson.addProperty("message", "No messages found for this run.");
				}
			} catch (DatabaseException | TenjinServletException e) {
				resJson.addProperty("status", "error");
				resJson.addProperty("message", e.getMessage());
			} finally {
				response.getWriter().write(resJson.toString());
			}
		}else if(task.equalsIgnoreCase("mailLog")) {
			String runId=request.getParameter("runId");
			/*Added by Padmavathi for TENJINCG-653 starts*/
			String callback=request.getParameter("callback");
			/*Added by Padmavathi for TENJINCG-653 ends*/
			ArrayList<TenjinMailLogs> mailInfo=null;
			try {
				mailInfo=new ParametersHelper().hydrateMailLogs(Integer.parseInt(runId));
			} catch (DatabaseException e) {
				logger.info("error occured due to"+e.getMessage());
				logger.error("Error ", e);
			}finally{
				request.setAttribute("mailInfo", mailInfo);
				request.setAttribute("runId", runId);
				/*Added by Padmavathi for TENJINCG-653 starts*/
				request.setAttribute("callback", callback);
				//    									Added by Padmavathi for TENJINCG-653 ends
				request.getRequestDispatcher("v2/run_mail_logs.jsp").forward(request, response);
			}
		}else if(task.equalsIgnoreCase("send_mail")) {
			int runId=0;
			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
				request.setAttribute("runId", runId);
				request.getRequestDispatcher("v2/manual_email_notification.jsp").forward(request, response);
			} catch (NumberFormatException e) {
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
			}
		}else if(task.equalsIgnoreCase("ui_val_result")) {
			int runId = 0;
			int testStepRecordId = 0;
			JsonObject retJson = new JsonObject();
			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("run")));
				testStepRecordId = Integer.parseInt(Utilities.trim(request.getParameter("srecid")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run ID [{}] or Test Step Record ID [{}]", Utilities.trim(request.getParameter("run")), Utilities.trim(request.getParameter("srecid")));
				retJson.addProperty("status", "error");
				retJson.addProperty("message", "An internal error occurred. Please contact Tenjin Support.");
				response.getWriter().write(retJson.toString());
				return;
			}

			try {
				Map<String, Map<String, List<UIValidation>>> map = new ResultsHandler().getUIValidationResults(runId, testStepRecordId);
				retJson.addProperty("status", "success");
				String json = new GsonBuilder().create().toJson(map);
				retJson.add("uiresults", new Gson().fromJson(json, JsonObject.class));
				response.getWriter().write(retJson.toString());
			} catch (DatabaseException e) {
				logger.error(e.getMessage(),e);
				retJson.addProperty("status", "error");
				retJson.addProperty("message", "An internal error occurred. Please contact Tenjin Support.");
				response.getWriter().write(retJson.toString());
			}
		}
		/*Added by Sahana for pCloudy:Starts*/
		else if(task.equalsIgnoreCase("DEVICE_FARM_REPORT")){
			logger.debug("Get Device Farm Report for execution ");
			String pCloudySessionName=Utilities.trim(request.getParameter("sessionName"));
			String targetIp=Utilities.trim(request.getParameter("machineIp"));
			String retData = "";
			try {
				JSONObject json = new JSONObject();
				try {
					Pcloudy objPcloudy=new PcloudyProcessor().getPcloudyCredentials(targetIp);
					JSONObject objPcloudyReport=new PcloudyProcessor(objPcloudy).getPcloudyReport(pCloudySessionName);
					json.put("status", "SUCCESS");
					json.put("pCloudyReport",objPcloudyReport );
				} catch (Exception e) {
					logger.error("An error occurred while fetching Device Farm report", e);
					json.put("status", "ERROR");
					json.put("message","Could not get Device Farm Report");

				} finally {
					retData = json.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);					
		}
		/*Added by Sahana for pCloudy:Ends*/
		
		/*Added by Ashiki for TENJINCG-1192 starts*/
		else if(task.equalsIgnoreCase("RUNTIME_VALUE")){

			int runId = 0;

			try {
				runId = Integer.parseInt(Utilities.trim(request.getParameter("runid")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid Run Id {}", request.getParameter("runid"));
				JsonObject json = new JsonObject();
				json.addProperty("status", "error");
				json.addProperty("message", "Could not generate report due to an internal error. Please contact Tenjin Support.");
				response.getWriter().write(json.toString());
				return;
			}

			JsonObject json = new JsonObject();
			try {
				String folderPath = request.getServletContext().getRealPath("/");
				Utilities.checkForDownloadsFolder(folderPath);
				folderPath = folderPath + "\\Downloads";
				String fileName =null;
				byte[] bop=null;
				PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), SessionUtils.getTenjinSession(request).getUser().getFullName());
				bop=pdfHandler.generatePdfReportForRunTimeValues(runId, SessionUtils.getTenjinSession(request).getProject().getId(), folderPath);	
				fileName = "Tenjin_Execution_Report_RunTimeValues_" + runId + ".pdf";
				response.setContentType("application/pdf");
				response.setHeader("Content-disposition", "attachment;filename=" + fileName);
		        response.setContentLength(bop.length);
		        try (BufferedInputStream bis = new BufferedInputStream(new ByteArrayInputStream(bop));
	                    BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());) {
	                byte[] buffer = new byte[1024];
	                int bytesRead = 0;
	                while ((bytesRead = bis.read(buffer)) != -1) {
	                    bos.write(buffer, 0, bytesRead);
	                }
	                bos.flush();
	            } catch (IOException e) {
	                throw new IOException();
	            }
				
			} catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create PDF due to an internal error",e);
			} 
		}
		/*Added by Ashiki for TENJINCG-1192 ends*/
		/*Added by Pushpa for TENJINCG-1210 starts*/
		else if(task.equalsIgnoreCase("download_runs")) {
			String runList = request.getParameter("runlist");
			String[] runsArray = runList.split(",");
			ArrayList<Integer> runs = new ArrayList<Integer>();
			for(String s:runsArray){
				runs.add(Integer.parseInt(s));
			}
			String retData = "";
			try{
				JSONObject json = new JSONObject();
				
				try{
					String folderPath = request.getServletContext().getRealPath("/");
					Utilities.checkForDownloadsFolder(folderPath);
					String file = folderPath +File.separator+ "Downloads"+File.separator+"RunsData";
					String filename="TestRuns_";
					File runsDir=new File(file);
					
					if (runsDir.isDirectory())
				    {
				        File[] files = runsDir.listFiles();
				        if (files != null && files.length > 0)
				        {
				            for (File aFile : files) 
				            {
				                System.gc();
				                Thread.sleep(2000);
				                FileDeleteStrategy.FORCE.delete(aFile);
				            }
				        }
				        runsDir.delete();
				    } 
					
					ExcelHandler e=new ExcelHandler();
					long startMillis = System.currentTimeMillis();
					e.downloadTestRuns(runs, tjnSession.getProject().getId(), file, filename);
					String zipFilePath=folderPath +File.separator+ "Downloads"+File.separator+"TenjinRuns.zip";
					File f=new File(zipFilePath);
					FileUtils.deleteQuietly(f);
					String zipFile = folderPath +File.separator+ "Downloads"+File.separator+"TenjinRuns.zip";
					FileOutputStream fos = new FileOutputStream(zipFile);
				    ZipOutputStream zos = new ZipOutputStream(fos);
					File dir = new File(file);
				   
				    e.zipDirectory(dir, "TenjinRuns", zos);
				    zos.close();
					logger.info("Test runs downloaded in {}ms", Utilities.calculateElapsedTime(startMillis, System.currentTimeMillis()));
					json.put("status", "SUCCESS");
					json.put("message","Test run(s) downloaded successfully");
					json.put("path","Downloads"+File.separator +"TenjinRuns.zip");

				}catch(Exception e){
					
					logger.error("An exception occured while downloading run",e);
					json.put("status", "ERROR");
					json.put("message","An Internal Error Occurred. Please try again");
					
				}finally{
					retData = json.toString();
				}
				
			}catch(Exception e){
				retData = "An Internal Error Occurred. Please contact your system administrator";
			}

			response.getWriter().write(retData);
		}
		/*Added by Pushpa for TENJINCG-1210 ends*/
		/*Added by Pushpa for TENJINCG-1223 starts*/
		else if(task.equalsIgnoreCase("delete_runs")){
			String runList = request.getParameter("runlist");
			String[] runsArray = runList.split(",");
			/*added by shruthi for TENJINCG-1224 starts*/
			 String preserveDefect =request.getParameter("preserveDefect");
			 /*added by shruthi for TENJINCG-1224 ends*/
			ArrayList<Integer> runs = new ArrayList<Integer>();
			
			try {
				for(String s:runsArray){
					runs.add(Integer.parseInt(s));
				}
				ArrayList<Integer> childRuns=new ResultsHandler().getChildRuns(runs);
				
				runs.addAll(childRuns);
				/*added by shruthi for TENJINCG-1224 starts*/
				new ResultsHandler().deleteTestRuns(runs);
				if(preserveDefect.equals("false"))
				{
				 new ResultsHandler().deleteDefects(runs);
				}
				
				/*added by shruthi for TENJINCG-1224 starts*/
				response.sendRedirect("TestRunServlet?status=success&message=testrun.delete.success");
			} catch (DatabaseException e) {
				
				response.sendRedirect("TestRunServlet?status=error&message=testrun.delete.error");
			} catch (SQLException e) {
				
				response.sendRedirect("TestRunServlet?status=error&message=testrun.delete.error");
			}
		}
		/*Added by Pushpa for TENJINCG-1223 ends*/
		else if(task.equalsIgnoreCase("def_review_progress")){
			int totalCount = (int) request.getSession().getAttribute("DEF_REVIEW_SIZE");
			int currentProgress = (int) request.getSession().getAttribute("DEF_REVIEW_PROGRESS");

			

			try {
				totalCount = (int) request.getSession().getAttribute("DEF_REVIEW_SIZE");
				currentProgress = (int) request.getSession().getAttribute("DEF_REVIEW_PROGRESS");
			} catch (Exception e1) {
				
			}

			double progressPercentage = 0;
			progressPercentage = ((double)currentProgress / (double) totalCount) * 100;
			double roundOff = Math.round(progressPercentage * 100.0) / 100.0;
			int rWithoutDecimals = (int) roundOff;
			String roundOffString = Integer.toString(rWithoutDecimals);
			boolean completed = false;
			if(rWithoutDecimals >=100){
				completed = true;
			}

			JSONObject json = new JSONObject();
			try {
				json.put("total",totalCount);
				json.put("done", currentProgress);
				json.put("progress", roundOffString);
				if(completed){
					json.put("completed", "yes");
				}else{
					json.put("completed", "no");
				}
				response.getWriter().write(json.toString());
			} catch (JSONException e) {
				
				logger.error("JSON error occurred", e);
			}
		}else if(task.equalsIgnoreCase("review_run_defects")){
			request.getSession().removeAttribute("DEF_REVIEW_SIZE");
			request.getSession().removeAttribute("DEF_REVIEW_PROGRESS");
			String rId = request.getParameter("runid");
			String json = request.getParameter("json");
			String userId=tjnSession.getUser().getId();

			try {
				JSONArray jArray = new JSONArray(json);

				request.getSession().setAttribute("DEF_REVIEW_SIZE", jArray.length());
				int processedCount = 0;

				int defectId = 0;

				String action="";
				String severity = "";
				String instanceName = "";

				logger.info("Initializing Defect Processor for Tenjin Core");
				CoreDefectProcessor processor = new CoreDefectProcessor();
				JSONArray errors = new JSONArray();

				for(int i=0; i<jArray.length(); i++){
					JSONObject j = jArray.getJSONObject(i);
					defectId = j.getInt("drecid");
					action = j.getString("action");
					severity = j.getString("severity");
					instanceName = j.getString("dtt_instance");

					try{
						processor.reviewDefect(defectId, severity, action, instanceName,userId);
					}catch(TenjinServletException e){
						JSONObject er = new JSONObject();
						er.put("drecid", defectId);
						er.put("message", e.getMessage());
						errors.put(er);
					}
					processedCount++;
					request.getSession().setAttribute("DEF_REVIEW_PROGRESS", processedCount);
				}

				JSONObject retJson = new JSONObject();
				if(errors.length() > 0 && errors.length() == jArray.length()){
					retJson.put("status", "error");
					retJson.put("detail_messages", errors);
					retJson.put("message", "Defect Review Failed. Please review the error(s) and try again. Contact Tenjin Support if problem persists.");
				}else if(errors.length() > 0 && errors.length() < jArray.length()){
					retJson.put("status", "warning");
					retJson.put("detail_messages", errors);
					retJson.put("message", "Defect Review is complete, but there were some errors. Please review the error(s) and try again. Contact Tenjin Support if problem persists.");
				}else{
					retJson.put("status", "success");
					retJson.put("message", "Defect Review completed successfully!");
				}
				response.getWriter().write(retJson.toString());
				request.getSession().setAttribute("DEF_REVIEW_SIZE", 50);
			} catch (NumberFormatException e) {
				
				logger.error("Invalid Run ID [{}]", rId);
				String ret = "{status:error,message:An internal error occurred. Please contact Tenjin Support}";
				response.getWriter().write(ret);
			} catch (JSONException e) {
				
				logger.error("JSONException caught while reviewing defects", e);
				String ret = "{status:error,message:An internal error occurred. Please contact Tenjin Support}";
				response.getWriter().write(ret);
			} catch (TenjinServletException e) {
				
				String ret = "{status:error,message:" + e.getMessage() + "}";
				response.getWriter().write(ret);
			}

		}
		/*Added by Ashiki for TENJINCG-1211 starts*/
		else if(task.equalsIgnoreCase("consoliated")){
			String currentUser =tjnSession.getUser().getId();
			String projectId = request.getParameter("projectId"); 
			TenjinJavaxMail mail=new TenjinJavaxMail();
			  SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
			  Date date = new Date();  
			  String currentdatetime= formatter.format(date);
			mail.buildConsolidatedMailInformation(currentdatetime, Integer.parseInt(projectId),currentUser);
		}
		/*Added by Ashiki for TENJINCG-1211 ends*/
		
		
		
		
	}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name = request.getParameter("txtName");
		String desc = request.getParameter("txtDesc");
		String type = request.getParameter("lstType");
		String app = request.getParameter("lstApplication");

		ResultValidation rv = new ResultValidation();
		rv.setName(name);
		rv.setDescription(desc);
		rv.setType(type);
		rv.setApplication(Integer.parseInt(app));
		String dUrl = "";
		Map<String, Object> map = new HashMap<String, Object>();
		try{
			//Check for duplicates
			ResultsHelper helper = new ResultsHelper();
			ResultValidation r = helper.hydrateResultValidation(name);

			if(r!=null){
				map.put("STATUS", "FAILURE");
				map.put("MESSAGE", "Record Already Exists. Please use a different name");
				dUrl = "newrvd.jsp";
			}else{
				r = helper.persistResultValidationDefinition(rv);
				map.put("STATUS", "SUCCESS");
				map.put("MESSAGE", "");
				map.put("RVD", r);
				dUrl = "rvddetails.jsp";
			}

		}catch(Exception e){
			map.put("STATUS", "ERROR");
			map.put("MESSAGE", "An Internal Error occurred. Please contact your System Administrator");
			dUrl = "newrvd.jsp";
		}finally{
			request.getSession().setAttribute("SCR_MAP",map);
		}

		response.sendRedirect(dUrl);
	}

	

}
