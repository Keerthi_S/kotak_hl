/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestReportServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 *
 * DATE              	CHANGED BY              DESCRIPTION
 * 23-05-2019			Preeti					TENJINCG-1058,1059,1060,1061,1062
 * 31-05-2019			Pushpa					TENJINCG-1074,1073,1064,1063 
 * 19-08-2019			Preeti					TENJINCG-1065,1072
 * 21-08-2019			Preeti					TENJINCG-1066,1071
 * 19-09-2019			Preeti					TENJINCG-1068,1069
 * 03-02-2021			Paneendra				Tenj210-158
 */

package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.AuditHandler;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.handler.TestCaseHandler;
import com.ycs.tenjin.handler.TestReportHandler;
import com.ycs.tenjin.handler.TestSetHandler;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.ExcelHandler;
import com.ycs.tenjin.util.PdfHandler;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class TestReportServlet
 */
@WebServlet("/TestReportServlet")
public class TestReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(TestReportServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestReportServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Project project = tjnSession.getProject();
		String param = request.getParameter("param");
		TestReportHandler handler = new TestReportHandler();
		AutHelper helper=new AutHelper();
		if(param.equalsIgnoreCase("test_set_report")){
			try{
				/*Modified by Prem for TJN27-203 Starts */
//				List<TestSet> testsets = new TestSetHandler().getAllTestSets(project.getId());
				List<TestSet> testsets = new TestSetHandler().hydrateAllTestSetsForReport(project.getId());
				/*Modified by Prem for TJN27-203 Ends */
				List<User> users = new ProjectHandler().getProjectUsers(project.getId());
				request.setAttribute("testsets", testsets);
				request.setAttribute("users", users);
				request.getRequestDispatcher("v2/test_set_report.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching Test Set information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			}
		}
		else if(param.equalsIgnoreCase("test_case_report")){
			try{
				List<TestCase> testcases = new TestCaseHandler().getAllTestCases(project.getId());
				List<User> users = new ProjectHandler().getProjectUsers(project.getId());
				request.setAttribute("testcases", testcases);
				request.setAttribute("users", users);
				request.getRequestDispatcher("v2/test_case_report.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching Test Case information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			}
		}
		else if(param.equalsIgnoreCase("get_runs")){
			String ts = request.getParameter("ts");
			String tsId = ts.split(":")[0];
			String retData = "";
			try {
				JSONObject json = new JSONObject();
				try {
					ArrayList<Integer> runIds = handler.getRunsForTestSet(Integer.parseInt(tsId), project.getId());
					if (runIds != null && runIds.size() > 0) {
						JSONArray jArray = new JSONArray();
						for (Integer runId : runIds) {
							JSONObject js = new JSONObject();
							js.put("runId",runId);
							jArray.put(js);
						}
						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("runIds", jArray);
					} else {
						json.put("status", "FAILURE");
						json.put("message",	"No runs available for this Test Set");
					}
				} catch (JSONException e) {
					logger.error("An error occurred while processing JSON", e);
					json.put("status", "ERROR");
					json.put("message",	"An error occurred while processing JSON");
				} 
				retData = json.toString();
			} catch (Exception e1) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}/*Added by Paneendra for Tenj210-158 starts*/
		else if(param.equalsIgnoreCase("get_testset_dates")){
			String ts = request.getParameter("ts");
			String tsId = ts.split(":")[0];
			String retData = "";
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");  
			try {
				JSONObject json = new JSONObject();
				ArrayList<Timestamp> runDates = handler.getDatesForTestSet(Integer.parseInt(tsId), project.getId());
				if (runDates != null && runDates.size() > 0) {
					JSONArray jArray = new JSONArray();
					for (Timestamp runDate : runDates) {
						JSONObject js = new JSONObject();
						js.put("runDate",formatter.format(runDate));
						jArray.put(js);
					}
					json.put("status", "SUCCESS");
					json.put("message", "");
					json.put("runDates", jArray);
					retData = json.toString();
				} else {
					json.put("status", "ERROR");
					json.put("message", "Selected TestSet doesn't Executed ");
				retData = json.toString();
				}
			} catch (Exception e1) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}
		else if(param.equalsIgnoreCase("get_testcase_dates")){
			String ts = request.getParameter("tc");
			String tsId = ts.split(":")[0];
			String retData = "";
			/*Modified by Ashiki for TJN27-208 ends*/
			 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");  
			/*Modified by Ashiki for TJN27-208 ends*/
			try {
				JSONObject json = new JSONObject();
				try {
					ArrayList<Timestamp> runDates = handler.getDatesForTestCase(Integer.parseInt(tsId), project.getId());
					if (runDates != null && runDates.size() > 0) {
						JSONArray jArray = new JSONArray();
						for (Timestamp runDate : runDates) {
							JSONObject js = new JSONObject();
							js.put("runDate",formatter.format(runDate));
							jArray.put(js);
						}
						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("runDates", jArray);
					} else {
						json.put("status", "ERROR");
						json.put("message",	"Selected TestCase doesn't Executed ");
					}
				} catch (JSONException e) {
					logger.error("An error occurred while processing JSON", e);
					json.put("status", "ERROR");
					json.put("message",	"An error occurred while processing JSON");
				} 
				retData = json.toString();
			} catch (Exception e1) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}/*Added by Paneendra for Tenj210-158 ends*/
		else if(param.equalsIgnoreCase("TS_RUN_HISTORY_REPORT")){
			JsonObject json = new JsonObject();
			String retData = "";
			String tsName = "";
			Integer tsRecId = 0;
			int projectId = project.getId();
			String ts = request.getParameter("ts");
			if(!ts.equalsIgnoreCase("")){
				tsRecId = Integer.parseInt(ts.split(":")[0]);
				tsName = ts.split(":")[1];
			}
			String filter = request.getParameter("filter");
			String fromRunId = request.getParameter("runFrom");
			String toRunId = request.getParameter("runTo");
			String fromDate = request.getParameter("dateFrom");
			String toDate = request.getParameter("dateTo");
			String reportType = request.getParameter("reportType");
			HashMap<String,String> entityMap = new LinkedHashMap<String,String>();
			ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
			String title="Test Set Run History";
			String subTitle ="Runwise execution report of a Test Set";
			try {
				/*Added by Paneendra for Tenj210-158 starts*/
				entityMap.put("TestSet Id", tsRecId.toString());
				/*Added by Paneendra for Tenj210-158 ends*/
				entityMap.put("TestSet Name", tsName);
				if(filter.equalsIgnoreCase("run")){
					entityMap.put("Run From", fromRunId);
					entityMap.put("Run To", toRunId);
					handler.validateReportdata(entityMap);
					data = handler.getTestSetRunHistoryDataForRun(projectId, tsRecId, Integer.parseInt(fromRunId), Integer.parseInt(toRunId));
				}else if(filter.equalsIgnoreCase("date")){
					entityMap.put("Date From", fromDate);
					entityMap.put("Date To", toDate);
					handler.validateReportdata(entityMap);
					data = handler.getTestSetRunHistoryDataForDate(projectId, tsRecId, fromDate, toDate);
				}
				/*modified by paneendra for VAPT fix starts*/
				String folderPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
				File downloadPath= new File(folderPath+File.separator+"Downloads");
				if(!downloadPath.exists()){
					downloadPath.mkdirs();
				}/*modified by paneendra for VAPT fix ends*/
				folderPath = folderPath + "\\Downloads";
				String fileName =title+"_"+tsName+"_Report";
				ExcelHandler excelHandler=new ExcelHandler();
				fileName = excelHandler.removeSpaces(fileName);
				if(reportType.equalsIgnoreCase("pdf")){
					fileName = fileName + ".pdf";
					PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), SessionUtils.getTenjinSession(request).getUser().getFullName());
					if(data.size()>0)
						pdfHandler.generateEntityReport(title,subTitle,entityMap,data,folderPath,fileName);
				}
				else{
					fileName = fileName + ".xlsx";
					if(data.size()>0)
						excelHandler.generateExcelReport(title, subTitle, entityMap, data, folderPath, fileName);
				}
				if(data.size()>0){
					json.addProperty("status", "SUCCESS");
					json.addProperty("message","");
					json.addProperty("path", "Downloads/" + fileName);
				}
				else{
					json.addProperty("status", "FAILURE");
					json.addProperty("message",	"No data available for this report");
				}
			} catch(RequestValidationException e){
				json.addProperty("status","error");
				json.addProperty("message",SessionUtils.loadMessage(request, e.getMessage(), e.getTargetFields()));
				logger.error("An Error occurred", e);
			}catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create PDF due to an internal error",e);
			} finally{
				retData = json.toString();
				response.getWriter().write(retData);
			}
		}
		else if(param.equalsIgnoreCase("TS_EXECUTION_DETAIL_REPORT")){
			JsonObject json = new JsonObject();
			String retData = "";
			int projectId = project.getId();
			String execDate = request.getParameter("execDate");
			String tsName = "";
			Integer tsRecId = 0;
			String ts = request.getParameter("ts");
			if(!ts.equalsIgnoreCase("")){
				tsRecId = Integer.parseInt(ts.split(":")[0]);
				tsName = ts.split(":")[1];
			}
			String reportType = request.getParameter("reportType");
			HashMap<String,String> entityMap = new LinkedHashMap<String,String>();
			String title="Test Set Execution Detail";
			String subTitle="Test Set Execution Report Case wise";
			try {
				/*Added by Paneendra for Tenj210-158 starts*/
				entityMap.put("TestSet Id", tsRecId.toString());
				/*Added by Paneendra for Tenj210-158 ends*/
				entityMap.put("TestSet Name", tsName);
				entityMap.put("Executed On", execDate);
				handler.validateReportdata(entityMap);
				ArrayList<HashMap<String,Object>> data = handler.getTestSetExecutionDetailData(projectId, tsRecId, execDate);
				
				/*modified by paneendra for VAPT fix starts*/
				String folderPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
				File downloadPath= new File(folderPath+File.separator+"Downloads");
				if(!downloadPath.exists()){
					downloadPath.mkdirs();
				}/*modified by paneendra for VAPT fix ends*/
				folderPath = folderPath + "\\Downloads";
				String fileName =title+"_"+tsName+"_Report";
				ExcelHandler excelHandler=new ExcelHandler();
				fileName = excelHandler.removeSpaces(fileName);
				if(reportType.equalsIgnoreCase("pdf")){
					fileName = fileName + ".pdf";
					PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), SessionUtils.getTenjinSession(request).getUser().getFullName());
					if(data.size()>0)
						pdfHandler.generateEntityReport(title,subTitle,entityMap,data,folderPath,fileName);
				}
				else{
					fileName = fileName + ".xlsx";
					if(data.size()>0)
						excelHandler.generateExcelReport(title, subTitle, entityMap, data, folderPath, fileName);
				}
				if(data.size()>0){
					json.addProperty("status", "SUCCESS");
					json.addProperty("message","");
					json.addProperty("path", "Downloads/" + fileName);
				}
				else{
					json.addProperty("status", "FAILURE");
					json.addProperty("message",	"No data available for this report");
				}
			}catch(RequestValidationException e){
				json.addProperty("status","error");
				json.addProperty("message",SessionUtils.loadMessage(request, e.getMessage(), e.getTargetFields()));
				logger.error("An Error occurred", e);
			} catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create PDF due to an internal error",e);
			} finally{
				retData = json.toString();
				response.getWriter().write(retData);
			}
		}
		else if(param.equalsIgnoreCase("TS_EXECUTION_SUMMARY_REPORT")){
			JsonObject json = new JsonObject();
			String retData = "";
			int projectId = project.getId();
			String userId = request.getParameter("userId");
			String fromDate = request.getParameter("dateFrom");
			String toDate = request.getParameter("dateTo");
			String reportType = request.getParameter("reportType");
			HashMap<String,String> entityMap = new LinkedHashMap<String,String>();
			ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
			String title="Test Set Execution Summary";
			String subTitle ="Test Set execution report";
			try {
				entityMap.put("Date From", fromDate);
				entityMap.put("Date To", toDate);
				entityMap.put("Executed By", userId);
				handler.validateReportdata(entityMap);
				if(userId.equalsIgnoreCase("all"))
					data = handler.getTestSetExecutionSummaryData(projectId, fromDate, toDate);
				else
					data = handler.getTestSetExecutionSummaryData(projectId, fromDate, toDate, userId);
				
				/*modified by paneendra for VAPT fix starts*/
				String folderPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
				File downloadPath= new File(folderPath+File.separator+"Downloads");
				if(!downloadPath.exists()){
					downloadPath.mkdirs();
				}/*modified by paneendra for VAPT fix ends*/
				folderPath = folderPath + "\\Downloads";
				String fileName =title+"_Report";
				ExcelHandler excelHandler=new ExcelHandler();
				fileName = excelHandler.removeSpaces(fileName);
				if(reportType.equalsIgnoreCase("pdf")){
					fileName = fileName + ".pdf";
					PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), SessionUtils.getTenjinSession(request).getUser().getFullName());
					if(data.size()>0)
						pdfHandler.generateEntityReport(title,subTitle,entityMap,data,folderPath,fileName);
				}
				else{
					fileName = fileName + ".xlsx";
					if(data.size()>0)
						excelHandler.generateExcelReport(title, subTitle, entityMap, data, folderPath, fileName);
				}
				if(data.size()>0){
					json.addProperty("status", "SUCCESS");
					json.addProperty("message","");
					json.addProperty("path", "Downloads/" + fileName);
				}
				else{
					json.addProperty("status", "FAILURE");
					json.addProperty("message",	"No data available for this report");
				}
			} catch(RequestValidationException e){
				json.addProperty("status","error");
				json.addProperty("message",SessionUtils.loadMessage(request, e.getMessage(), e.getTargetFields()));
				logger.error("An Error occurred", e);
			}catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create PDF due to an internal error",e);
			} finally{
				retData = json.toString();
				response.getWriter().write(retData);
			}
		}
		else if(param.equalsIgnoreCase("TC_EXECUTION_DETAIL_REPORT")){
			JsonObject json = new JsonObject();
			String retData = "";
			int projectId = project.getId();
			String execDate = request.getParameter("execDate");
			String userId = request.getParameter("userId");
			String tcId = "";
			int tcRecId = 0;
			String tcName ="";
			String tc = request.getParameter("tc");
			if(!tc.equalsIgnoreCase("")){
				tcRecId = Integer.parseInt(tc.split(":")[0]);
				tcId = tc.split(":")[1];
				/*Added by Paneendra for  Tenj210-158 starts*/
				tcName=tc.split(":")[2];
				/*Added by Paneendra for  Tenj210-158 ends*/
			}
			String reportType = request.getParameter("reportType");
			HashMap<String,String> entityMap = new LinkedHashMap<String,String>();
			ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
			String title="Test Case Execution Detail";
			String subTitle="Stepswise execution report of a Test Case";
			try {
				/*Added by Paneendra for  Tenj210-158 starts*/
				entityMap.put("TestCase Id", tcId);
				entityMap.put("TestCase Name", tcName);
				/*Added by Paneendra for  Tenj210-158 ends*/
				entityMap.put("Executed On", execDate);
				entityMap.put("Executed By", userId);
				handler.validateReportdata(entityMap);
				if(userId.equalsIgnoreCase("all"))
					data = handler.getTestCaseExecutionDetailData(projectId, tcRecId, execDate);
				else
					data = handler.getTestCaseExecutionDetailData(projectId, tcRecId, execDate, userId);
				
				/*modified by paneendra for VAPT fix starts*/
				String folderPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
				File downloadPath= new File(folderPath+File.separator+"Downloads");
				if(!downloadPath.exists()){
					downloadPath.mkdirs();
				}/*modified by paneendra for VAPT fix ends*/
				folderPath = folderPath + "\\Downloads";
				String fileName =title+"_"+tcId+"_Report";
				ExcelHandler excelHandler=new ExcelHandler();
				fileName = excelHandler.removeSpaces(fileName);
				if(reportType.equalsIgnoreCase("pdf")){
					fileName = fileName + ".pdf";
					PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), SessionUtils.getTenjinSession(request).getUser().getFullName());
					if(data.size()>0)
						pdfHandler.generateEntityReport(title,subTitle,entityMap,data,folderPath,fileName);
				}
				else{
					fileName = fileName + ".xlsx";
					if(data.size()>0)
						excelHandler.generateExcelReport(title, subTitle, entityMap, data, folderPath, fileName);
				}
				if(data.size()>0){
					json.addProperty("status", "SUCCESS");
					json.addProperty("message","");
					json.addProperty("path", "Downloads/" + fileName);
				}
				else{
					json.addProperty("status", "FAILURE");
					json.addProperty("message",	"No data available for this report");
				}
			}catch(RequestValidationException e){
				json.addProperty("status","error");
				json.addProperty("message",SessionUtils.loadMessage(request, e.getMessage(), e.getTargetFields()));
				logger.error("An Error occurred", e);
			} catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create PDF due to an internal error",e);
			} finally{
				retData = json.toString();
				response.getWriter().write(retData);
			}
		}
		else if(param.equalsIgnoreCase("TC_EXECUTION_HISTORY_REPORT")){
			JsonObject json = new JsonObject();
			String retData = "";
			String tcId = "";
			String tcName ="";
			String tc = request.getParameter("tc");
			/*Added by Ashiki for TJN27-216 starts*/
			int tcRecId=0;
			/*Added by Ashiki for TJN27-216 ends*/
			if(!tc.equalsIgnoreCase("")){
				/*Added by Ashiki for TJN27-216 starts*/
				tcRecId = Integer.parseInt(tc.split(":")[0]);
				/*Added by Ashiki for TJN27-216 ends*/
				tcId = tc.split(":")[1];
				/*Added by Paneendra for  Tenj210-158 starts*/
				tcName=tc.split(":")[2];
				/*Added by Paneendra for  Tenj210-158 starts*/
			}
			int projectId = project.getId();
			String userId = request.getParameter("userId");
			String fromDate = request.getParameter("dateFrom");
			String toDate = request.getParameter("dateTo");
			String reportType = request.getParameter("reportType");
			HashMap<String,String> entityMap = new LinkedHashMap<String,String>();
			ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
			String title="Test Case Execution History";
			String subTitle ="Report of Test Cases executed during a specific period of time";
			try {
				entityMap.put("TestCase Id", tcId);
				/*Added by Paneendra for  Tenj210-158 starts*/
				entityMap.put("TestCase Name", tcName);
				/*Added by Paneendra for  Tenj210-158 ends*/
				entityMap.put("Date From", fromDate);
				entityMap.put("Date To", toDate);
				entityMap.put("Executed By", userId);
				handler.validateReportdata(entityMap);
				if(userId.equalsIgnoreCase("all"))
					/*Modified by Ashiki for TJN27-216 starts*/
					data = handler.getTestCaseExecutionHistoryData(projectId, fromDate, toDate,tcRecId);
				else
					data = handler.getTestCaseExecutionHistoryData(projectId, fromDate, toDate, userId,tcRecId);
				/*Modified by Ashiki for TJN27-216 ends*/
				/*modified by paneendra for VAPT fix starts*/
				String folderPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
				File downloadPath= new File(folderPath+File.separator+"Downloads");
				if(!downloadPath.exists()){
					downloadPath.mkdirs();
				}/*modified by paneendra for VAPT fix ends*/
				folderPath = folderPath + "\\Downloads";
				String fileName =title+"_Report";
				ExcelHandler excelHandler=new ExcelHandler();
				fileName = excelHandler.removeSpaces(fileName);
				if(reportType.equalsIgnoreCase("pdf")){
					fileName = fileName + ".pdf";
					PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), SessionUtils.getTenjinSession(request).getUser().getFullName());
					if(data.size()>0)
						pdfHandler.generateEntityReport(title,subTitle,entityMap,data,folderPath,fileName);
				}
				else{
					fileName = fileName + ".xlsx";
					if(data.size()>0)
						excelHandler.generateExcelReport(title, subTitle, entityMap, data, folderPath, fileName);
				}
				if(data.size()>0){
					json.addProperty("status", "SUCCESS");
					json.addProperty("message","");
					json.addProperty("path", "Downloads/" + fileName);
				}
				else{
					json.addProperty("status", "FAILURE");
					json.addProperty("message",	"No data available for this report");
				}
			}catch(RequestValidationException e){
				json.addProperty("status","error");
				json.addProperty("message",SessionUtils.loadMessage(request, e.getMessage(), e.getTargetFields()));
				logger.error("An Error occurred", e);
			} catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create PDF due to an internal error",e);
			} finally{
				retData = json.toString();
				response.getWriter().write(retData);
			}
		}
		/*Added by Pushpa for TENJINCG-1073,1064 starts*/
		else if(param.equalsIgnoreCase("miscellaneous_report")){
			
			request.getRequestDispatcher("v2/miscellaneous_report.jsp").forward(request, response);
			
		}else if(param.equalsIgnoreCase("APPLICATIONWISE_EXECUTION_REPORT")){
			JsonObject json = new JsonObject();
			String retData = "";
			String appName="";
			String appId=request.getParameter("appId");
			int applicationId=Integer.parseInt(appId);
			String fromDate = request.getParameter("dateFrom");
			String toDate = request.getParameter("dateTo");
			String reportType = request.getParameter("reportType");
			HashMap<String,String> entityMap = new LinkedHashMap<String,String>();
			ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
			String title="Application wise execution report";
			String subTitle ="Application wise execution report";
			/*Added by Ashiki for TJN27-201 starts*/
			int prjId=project.getId();
			/*Added by Ashiki for TJN27-201 ends*/
			
			try {
				appName=helper.getAutName(applicationId);
				/*Added by Paneendra for  Tenj210-158 starts*/
			  //entityMap.put("Application", appId);
				entityMap.put("Application Name", appName);
				/*Added by Paneendra for  Tenj210-158 ends*/
				entityMap.put("Date From", fromDate);
				entityMap.put("Date To", toDate);
				
				handler.validateReportdata(entityMap);
				/*Modified by Ashiki for TJN27-201 starts*/
					data = handler.getApplicationWiseExecutionData(Integer.parseInt(appId), fromDate, toDate,prjId);
				/*Modified by Ashiki for TJN27-201 ends*/
					/*modified by paneendra for VAPT fix starts*/
						String folderPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
						File downloadPath= new File(folderPath+File.separator+"Downloads");
						if(!downloadPath.exists()){
							downloadPath.mkdirs();
						}/*modified by paneendra for VAPT fix ends*/
				folderPath = folderPath + "\\Downloads";
				String fileName =title+"_Report";
				ExcelHandler excelHandler=new ExcelHandler();
				fileName = excelHandler.removeSpaces(fileName);
				if(reportType.equalsIgnoreCase("pdf")){
					fileName = fileName + ".pdf";
					PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), SessionUtils.getTenjinSession(request).getUser().getFullName());
					if(data.size()>0)
						pdfHandler.generateEntityReport(title,subTitle,entityMap,data,folderPath,fileName);
				}
				else{
					fileName = fileName + ".xlsx";
					if(data.size()>0)
						excelHandler.generateExcelReport(title, subTitle, entityMap, data, folderPath, fileName);
				}
				if(data.size()>0){
					json.addProperty("status", "SUCCESS");
					json.addProperty("message","");
					json.addProperty("path", "Downloads/" + fileName);
				}
				else{
					json.addProperty("status", "FAILURE");
					json.addProperty("message",	"No data available for this report");
				}
			}catch(RequestValidationException e){
				json.addProperty("status","error");
				json.addProperty("message",SessionUtils.loadMessage(request, e.getMessage(), e.getTargetFields()));
				logger.error("An Error occurred", e);
			} catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create PDF due to an internal error",e);
			} finally{
				retData = json.toString();
				response.getWriter().write(retData);
			}
		}
		/*Added by Pushpa for TENJINCG-1073,1064 ends*/
		/*Added by Pushpa for TENJINCG-1074,1063 starts*/
		else if(param.equalsIgnoreCase("TESTER_TS_EXECUTION_SUMMARY_REPORT")){
			JsonObject json = new JsonObject();
			String retData = "";
			int projectId = project.getId();
			String userId = request.getParameter("userId");
			String fromDate = request.getParameter("dateFrom");
			String toDate = request.getParameter("dateTo");
			String reportType = request.getParameter("reportType");
			HashMap<String,String> entityMap = new LinkedHashMap<String,String>();
			ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
			String title="Testerwise Test Execution Report";
			String subTitle ="Test Execution Report by Tester";
			try {
				entityMap.put("Executed By", userId);
				entityMap.put("Date From", fromDate);
				entityMap.put("Date To", toDate);
				
				handler.validateReportdata(entityMap);
				
					data = handler.getTesterWiseTestSetExecutionSummary(projectId, fromDate, toDate, userId);
				
					/*modified by paneendra for VAPT fix starts*/
						String folderPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
						File downloadPath= new File(folderPath+File.separator+"Downloads");
						if(!downloadPath.exists()){
							downloadPath.mkdirs();
						}/*modified by paneendra for VAPT fix ends*/
				folderPath = folderPath + "\\Downloads";
				String fileName =title+"_Report";
				ExcelHandler excelHandler=new ExcelHandler();
				fileName = excelHandler.removeSpaces(fileName);
				if(reportType.equalsIgnoreCase("pdf")){
					fileName = fileName + ".pdf";
					PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), SessionUtils.getTenjinSession(request).getUser().getFullName());
					if(data.size()>0)
						pdfHandler.generateEntityReport(title,subTitle,entityMap,data,folderPath,fileName);
				}
				else{
					fileName = fileName + ".xlsx";
					if(data.size()>0)
						excelHandler.generateExcelReport(title, subTitle, entityMap, data, folderPath, fileName);
				}
				if(data.size()>0){
					json.addProperty("status", "SUCCESS");
					json.addProperty("message","");
					json.addProperty("path", "Downloads/" + fileName);
				}
				else{
					json.addProperty("status", "FAILURE");
					json.addProperty("message",	"No data available for this report");
				}
			} catch(RequestValidationException e){
				json.addProperty("status","error");
				json.addProperty("message",SessionUtils.loadMessage(request, e.getMessage(), e.getTargetFields()));
				logger.error("An Error occurred", e);
			}catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create PDF due to an internal error",e);
			} finally{
				retData = json.toString();
				response.getWriter().write(retData);
			}
		}
		/*Added by Pushpa for TENJINCG-1074,1063 ends*/
		/*Added by Preeti for TENJINCG-1065,1072 starts*/
		else if(param.equalsIgnoreCase("get_test_sets")){
			String retData = "";
			try {
				JSONObject json = new JSONObject();
				try {
					/*Modified by Prem for TJN27-197 Starts */
					//List<TestSet> testsets = new TestSetHandler().getAllTestSets(project.getId());
					List<TestSet> testsets = new TestSetHandler().hydrateAllTestSetsForReport(project.getId());
					/*Modified by Prem for TJN27-197 Ends */
					if (testsets != null && testsets.size() > 0) {
						JSONArray jArray = new JSONArray();
						for (TestSet ts : testsets) {
							JSONObject js = new JSONObject();
							js.put("id",ts.getId());
							js.put("name", ts.getName());
							jArray.put(js);
						}
						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("testsets", jArray);
					} else {
						json.put("status", "FAILURE");
						json.put("message",	"No Test Set available");
					}
				} catch (JSONException e) {
					logger.error("An error occurred while processing JSON", e);
					json.put("status", "ERROR");
					json.put("message",	"An error occurred while processing JSON");
				} 
				retData = json.toString();
			} catch (Exception e1) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}
		else if(param.equalsIgnoreCase("SEVERITY_WISE_DEFECT_REPORT")){
			JsonObject json = new JsonObject();
			String retData = "";
			int projectId = project.getId();
			String ts = request.getParameter("ts");
			String[] tsVal = ts.split(":");
			String tsRecId = tsVal[0];
			String tsName = tsVal[1];
			String runFrom = request.getParameter("runFrom");
			String runTo = request.getParameter("runTo");
			String reportType = request.getParameter("reportType");
			HashMap<String,String> entityMap = new LinkedHashMap<String,String>();
			ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
			String title="Severity Wise Defects Report";
			String subTitle ="Severity Wise Defects Report";
			try {
				/*Added by Paneendra for  Tenj210-158 starts*/
				entityMap.put("TestSet Id", tsRecId);
				/*Added by Paneendra for  Tenj210-158 ends*/
				entityMap.put("TestSet Name", tsName);
				entityMap.put("Run From", runFrom);
				entityMap.put("Run To", runTo);
				handler.validateReportdata(entityMap);
				data = handler.getSeverityWiseDefectData(projectId, Integer.parseInt(tsRecId), Integer.parseInt(runFrom), Integer.parseInt(runTo));
				/*modified by paneendra for VAPT fix starts*/
				String folderPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
				File downloadPath= new File(folderPath+File.separator+"Downloads");
				if(!downloadPath.exists()){
					downloadPath.mkdirs();
				}/*modified by paneendra for VAPT fix ends*/
				folderPath = folderPath + "\\Downloads";
				String fileName =title+"_Report";
				ExcelHandler excelHandler=new ExcelHandler();
				fileName = excelHandler.removeSpaces(fileName);
				if(reportType.equalsIgnoreCase("pdf")){
					fileName = fileName + ".pdf";
					PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), SessionUtils.getTenjinSession(request).getUser().getFullName());
					if(data.size()>0)
						pdfHandler.generateEntityReport(title,subTitle,entityMap,data,folderPath,fileName);
				}
				else{
					fileName = fileName + ".xlsx";
					if(data.size()>0)
						excelHandler.generateExcelReport(title, subTitle, entityMap, data, folderPath, fileName);
				}
				if(data.size()>0){
					json.addProperty("status", "SUCCESS");
					json.addProperty("message","");
					json.addProperty("path", "Downloads/" + fileName);
				}
				else{
					json.addProperty("status", "FAILURE");
					json.addProperty("message",	"No data available for this report");
				}
			}catch(RequestValidationException e){
				json.addProperty("status","error");
				json.addProperty("message",SessionUtils.loadMessage(request, e.getMessage(), e.getTargetFields()));
				logger.error("An Error occurred", e);
			} catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create PDF due to an internal error",e);
			} finally{
				retData = json.toString();
				response.getWriter().write(retData);
			}
		}
		/*Added by Preeti for TENJINCG-1065,1072 ends*/
		/*Added by Preeti for TENJINCG-1066,1071 starts*/
		else if(param.equalsIgnoreCase("get_users")){
			String retData = "";
			try {
				JSONObject json = new JSONObject();
				try {
					List<User> users = new ProjectHandler().getProjectUsers(project.getId());
					if (users != null && users.size() > 0) {
						JSONArray jArray = new JSONArray();
						for (User user : users) {
							JSONObject js = new JSONObject();
							js.put("id", user.getId());
							jArray.put(js);
						}
						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("users", jArray);
					} else {
						json.put("status", "FAILURE");
						json.put("message",	"No User available");
					}
				} catch (JSONException e) {
					logger.error("An error occurred while processing JSON", e);
					json.put("status", "ERROR");
					json.put("message",	"An error occurred while processing JSON");
				} 
				retData = json.toString();
			} catch (Exception e1) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}
		else if(param.equalsIgnoreCase("SCHEDULER_REPORT")){
			JsonObject json = new JsonObject();
			String retData = "";
			int projectId = project.getId();
			String taskType = request.getParameter("taskType");
			String taskStatus = request.getParameter("taskStatus");
			String fromDate = request.getParameter("dateFrom");
			String toDate = request.getParameter("dateTo");
			String userId = request.getParameter("userId");
			String schType = request.getParameter("schType");
			String reportType = request.getParameter("reportType");
			HashMap<String,String> entityMap = new LinkedHashMap<String,String>();
			ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
			String title="Scheduler Report";
			String subTitle ="Scheduled Tasks Report";
			try {
				entityMap.put("Task Type", taskType);
				entityMap.put("Task Status", taskStatus);
				entityMap.put("Date From", fromDate);
				entityMap.put("Date To", toDate);
				entityMap.put("Scheduled By", userId);
				entityMap.put("Schedule Type", schType);
				handler.validateReportdata(entityMap);
				if(userId.equalsIgnoreCase("all"))
					data = handler.getSchedulerData(projectId, taskType, taskStatus, fromDate, toDate,schType);
				else
					data = handler.getSchedulerData(projectId, taskType, taskStatus, fromDate, toDate, userId, schType);
				/*modified by paneendra for VAPT fix starts*/
				/*String folderPath = request.getServletContext().getRealPath("/");
				Utilities.checkForDownloadsFolder(folderPath);*/
				String folderPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
				File downloadPath= new File(folderPath+File.separator+"Downloads");
				if(!downloadPath.exists()){
					downloadPath.mkdirs();
				}/*modified by paneendra for VAPT fix ends*/
				folderPath = folderPath + "\\Downloads";
				String fileName =title+"_Report";
				ExcelHandler excelHandler=new ExcelHandler();
				fileName = excelHandler.removeSpaces(fileName);
				if(reportType.equalsIgnoreCase("pdf")){
					fileName = fileName + ".pdf";
					PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), SessionUtils.getTenjinSession(request).getUser().getFullName());
					if(data.size()>0)
						pdfHandler.generateEntityReport(title,subTitle,entityMap,data,folderPath,fileName);
				}
				else{
					fileName = fileName + ".xlsx";
					if(data.size()>0)
						excelHandler.generateExcelReport(title, subTitle, entityMap, data, folderPath, fileName);
				}
				if(data.size()>0){
					json.addProperty("status", "SUCCESS");
					json.addProperty("message","");
					json.addProperty("path", "Downloads/" + fileName);
				}
				else{
					json.addProperty("status", "FAILURE");
					json.addProperty("message",	"No data available for this report");
				}
			}catch(RequestValidationException e){
				json.addProperty("status","error");
				json.addProperty("message",SessionUtils.loadMessage(request, e.getMessage(), e.getTargetFields()));
				logger.error("An Error occurred", e);
			} catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create PDF due to an internal error",e);
			} finally{
				retData = json.toString();
				response.getWriter().write(retData);
			}
		}
		/*Added by Preeti for TENJINCG-1066,1071 ends*/
		/*Added by Preeti for TENJINCG-1068,1069 starts*/
		else if(param.equalsIgnoreCase("CHANGE_HISTORY")){
			String activityArea = request.getParameter("activityArea");
			String entityRecId = request.getParameter("recId");
			String entityId = request.getParameter("id");
			String entityType = request.getParameter("entityType");
			request.setAttribute("activityArea", activityArea);
			request.setAttribute("entityRecId", entityRecId);
			request.setAttribute("entityId", entityId);
			request.setAttribute("entityType", entityType);
			request.getRequestDispatcher("v2/change_history.jsp").forward(request, response);
		}
		else if(param.equalsIgnoreCase("CHANGE_HISTORY_REPORT")){
			JsonObject json = new JsonObject();
			String retData = "";
			String activityArea = request.getParameter("activityArea");
			String entityRecId = request.getParameter("recId");
			String entityId = request.getParameter("id");
			String entityType = request.getParameter("entityType");
			String fromDate = request.getParameter("dateFrom");
			String toDate = request.getParameter("dateTo");
			String reportType = request.getParameter("reportType");
			HashMap<String,String> entityMap = new LinkedHashMap<String,String>();
			String title="Change History Report";
			String subTitle ="Audit log of all the changes";
			try {
				entityMap.put("Activity Area", activityArea);
				entityMap.put("Entity", entityType);
				entityMap.put("Entity Id", entityId);
				entityMap.put("Date From", fromDate);
				entityMap.put("Date To", toDate);
				handler.validateReportdata(entityMap);
				ArrayList<AuditRecord> records = new AuditHandler().getAuditRecord(Integer.parseInt(entityRecId), entityType, fromDate, toDate);
				ArrayList<HashMap<String,Object>> data = handler.getData(records,entityType);
				/*modified by paneendra for VAPT fix starts*/
				String rootPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
				File downloadPath= new File(rootPath+File.separator+"Downloads");
				if(!downloadPath.exists()){
					downloadPath.mkdirs();
				}
				
				String folderPath=downloadPath.getPath();
				/*modified by paneendra for VAPT fix ends*/
				String fileName =title + "_" + entityType + "_Report";
				ExcelHandler excelHandler=new ExcelHandler();
				fileName = excelHandler.removeSpaces(fileName);
				if(reportType.equalsIgnoreCase("pdf")){
					fileName = fileName + ".pdf";
					PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), SessionUtils.getTenjinSession(request).getUser().getFullName());
					if(data.size()>0)
						pdfHandler.generateEntityReport(title,subTitle,entityMap,data,folderPath,fileName);
				}
				else{
					fileName = fileName + ".xlsx";
					if(data.size()>0)
						excelHandler.generateExcelReport(title, subTitle, entityMap, data, folderPath, fileName);
				}
				if(data.size()>0){
					json.addProperty("status", "SUCCESS");
					json.addProperty("message","");
					json.addProperty("path", "Downloads/" + fileName);
				}
				else{
					json.addProperty("status", "FAILURE");
					json.addProperty("message",	"No data available for this report");
				}
			} catch(RequestValidationException e){
				json.addProperty("status","error");
				json.addProperty("message",SessionUtils.loadMessage(request, e.getMessage(), e.getTargetFields()));
				logger.error("An Error occurred", e);
			} catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create PDF due to an internal error",e);
			} finally{
				retData = json.toString();
				response.getWriter().write(retData);
			}
		}
		/*Added by Preeti for TENJINCG-1068,1069 ends*/
		else if(param.equalsIgnoreCase("test_set_execution_report")) {

			try{
				List<TestSet> testSet = new TestSetHandler().getAllTestSets(project.getId());
				List<User> users = new ProjectHandler().getProjectUsers(project.getId());
				request.setAttribute("testSet", testSet);
				request.setAttribute("users", users);
				request.getRequestDispatcher("v2/test_set_execution_report.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching Test Case information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			}
		
			/*Added By Ashiki for TENJINCG-1266 starts*/
		}else if(param.equalsIgnoreCase("TS_RUN_SUMMARY_REPORT")) {

			JsonObject json = new JsonObject();
			String retData = "";
			int projectId = project.getId();
			String exeFromDate = request.getParameter("dateFrom");
			String exeToDate = request.getParameter("dateTo");
			String tsId = "";
			
			HashMap<String,String> entityMap = new LinkedHashMap<String,String>();
			ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
			String title="Test Set Run Summary";
			String subTitle="Test set wise execution report";
			try {
				
				Project prj=new Project();
				prj= new ProjectHelper().hydrateProject(projectId);
				entityMap.put("Domain", prj.getDomain());
				entityMap.put("Project", prj.getName());
				entityMap.put("Executed From", exeFromDate);
				entityMap.put("Executed To", exeToDate);
				data = new TestReportHandler().getTestSetDetailData(projectId,exeFromDate,exeToDate);
				/*String folderPath = request.getServletContext().getRealPath("/");
				Utilities.checkForDownloadsFolder(folderPath);
				folderPath = folderPath + "\\Downloads";*/
				String rootPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
				File downloadPath= new File(rootPath+File.separator+"Downloads");
				if(!downloadPath.exists()){
					downloadPath.mkdirs();
				}
				
				String folderPath=downloadPath.getPath();
				
				String fileName =title+"_"+tsId+"_Report";
				ExcelHandler excelHandler=new ExcelHandler();
				fileName = excelHandler.removeSpaces(fileName);
					fileName = fileName + ".xlsx";
					if(data.size()>0)
						excelHandler.generateExcelReport(title, subTitle, entityMap, data, folderPath, fileName);
				if(data.size()>0){
					json.addProperty("status", "SUCCESS");
					json.addProperty("message","");
					json.addProperty("path", "Downloads/" + fileName);
				}
				else{
					json.addProperty("status", "FAILURE");
					json.addProperty("message",	"No data available for this report");
				}
			}catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create PDF due to an internal error",e);
			} finally{
				retData = json.toString();
				response.getWriter().write(retData);
			}
		
		}/*Added By Ashiki for TENJINCG-1266 ends*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
