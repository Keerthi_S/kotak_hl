/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SessionUtils.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 19-Oct-2017		Sriram Sridharan		Newly added for TENJINCG-396 and TENJINCG-403
* 04-12-2019			Preeti					TENJINCG-1174
* */

package com.ycs.tenjin.servlet.util;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.ScreenState;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.util.Utilities;

public class SessionUtils {
	
	public static final String screenState = "screenState";
	private static final Logger logger = LoggerFactory.getLogger(SessionUtils.class);
	
	public static void setScreenState(String status, String messageKey, HttpServletRequest request) {
		request.getSession().removeAttribute(screenState);
		ScreenState oScreenState = new ScreenState(status, messageKey);
		request.getSession().setAttribute(screenState, oScreenState);
		request.setAttribute(screenState, oScreenState);
	}
	
	public static void setScreenState(String status, String messageKey, String[] parameters, HttpServletRequest request) {
		request.getSession().removeAttribute(screenState);
		ScreenState oScreenState = new ScreenState(status, messageKey, parameters);
		request.getSession().setAttribute(screenState, oScreenState);
		request.setAttribute(screenState, oScreenState);
	}
	
	
	public static void setScreenState(String status, String messageKey, String parameter, HttpServletRequest request) {
		String[] stringParams = {parameter};
		setScreenState(status, messageKey, stringParams, request);
	}
	
	
	
	public static void loadScreenStateToRequest(HttpServletRequest request) {
		
		ScreenState oScreenState = (ScreenState) request.getSession().getAttribute(screenState);
		if(oScreenState != null) {
			Locale locale = null;
			if(getTenjinSession(request) != null && getTenjinSession(request).getLocale() != null) {
				locale = getTenjinSession(request).getLocale();
			}else {
				locale = request.getLocale();
			}
			oScreenState.loadMessage(locale);
			request.setAttribute(screenState, (ScreenState) request.getSession().getAttribute(screenState));
			request.getSession().removeAttribute(screenState);
		}
	}
	
	public static TenjinSession getTenjinSession(HttpServletRequest request) {
		return (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
	}
	
	public static String loadMessage(HttpServletRequest request, String messageKey, Object[] targetFields) {
		logger.info("Loading message for {}", request.getLocale().toString());
		
		Locale locale = null;
		if(getTenjinSession(request) != null && getTenjinSession(request).getLocale() != null) {
			locale = getTenjinSession(request).getLocale();
		}else {
			locale = request.getLocale();
		}
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.tjn_messages", locale);
		String finalMessage = "";
		try {
			String messageFromBundle = resourceBundle.getString(messageKey);
			finalMessage = Utilities.trim(messageFromBundle).length() > 0 ? MessageFormat.format(messageFromBundle, (Object[]) targetFields) : messageKey;
		} catch (Exception e) {
			logger.error("ERROR getting value for Message Key {} from Message bundle", messageKey, e);
			finalMessage = messageKey;
		}
		
		return finalMessage;
	}
	
	public static String loadMessage(HttpServletRequest request, String messageKey) {
		return loadMessage(request, messageKey, new Object[] {});
	}
	
	
	public static Object mapAttributes(HttpServletRequest request, Class sampleClass) {
		Object object = null;
		try {
			object = sampleClass.newInstance();
		} catch (IllegalAccessException exception3) {
			logger.error("you don't have access to this Object", exception3);
		} catch (InstantiationException exception4) {
			logger.error("Failed to instantiate the object", exception4);
		}
		Field[] fields = sampleClass.getDeclaredFields();
		for (Field field : fields) {
			try {
				field.setAccessible(true);
				String type = field.getType().getName();
				String value = request.getParameter(field.getName());
				if (value != null) {
					if (type.toLowerCase().contains("string")) {
						field.set(object, value);
					} else if (type.toLowerCase().contains("int")) {
						int valueIntger = Integer.parseInt(value);
						field.set(object, valueIntger);
					} else if (type.toLowerCase().contains("boolean")) {
						Boolean valueBool = Boolean.valueOf(value);
						field.set(object, valueBool);
					} else if (type.toLowerCase().contains("long")) {
						Long valuLong = Long.valueOf(value);
						field.set(object, valuLong);
					}
				} else {
					continue;
				}

			} catch (IllegalArgumentException exception1) {
				logger.error("the type of value you given for the field is not accepting by field " + field.getName(),
						exception1);
			} catch (IllegalAccessException exception2) {
				logger.error("you don't have access to this field .field may be private " + field.getName(),
						exception2);

			}
		}
		return object;

	}
	/*Added for TENJINCG-1174 starts*/
	public static AuthorizationRule getAuthorizationRule(HttpServletRequest request) {
		if(request == null) return null;
		
		if(!request.getRequestURI().toLowerCase().contains("servlet")) return null;
		
		String contextPath = request.getContextPath();
		String servlet = request.getServletPath().replace(contextPath, "");
		servlet = "com.ycs.tenjin.servlet." + servlet.replace("/", "");
		
		try {
			Class<?> servletClass = Class.forName(servlet);
			Field authorizationRuleField = servletClass.getField("authorizationRule");
			if(authorizationRuleField != null) {
				return (AuthorizationRule) authorizationRuleField.get(null);
			}
		} catch(NoSuchFieldException e) {
			logger.debug("No authorization rule defined for {}", servlet);
			return null;
		} catch (Exception e) {
			logger.error("Error getting Authorization rule for servlet {}", servlet);
			logger.error(e.getMessage());
		} 
		
		return null;
		
	}
	/*Added for TENJINCG-1174 ends*/
}
