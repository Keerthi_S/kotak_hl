/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TenjinSessionServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 12-10-2018        Leelaprasad P         newly added for TENJINCG-872 
* 17-10-2018        Leelaprasad           TENJINCG-827
*/


package com.ycs.tenjin.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.handler.TenjinSessionHandler;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class TenjinSessionServlet
 */

public class TenjinSessionServlet extends HttpServlet {
	
	
	private static Logger logger = LoggerFactory
			.getLogger(TenjinSessionServlet.class);
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TenjinSessionServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String task = Utilities.trim(request.getParameter("t"));
		TenjinSessionHandler handler=new TenjinSessionHandler();
		/*changed by Leelaprasad for TENJINCG-827 starts*/
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		/*changed by Leelaprasad for TENJINCG-827 ends*/
		if (task.equalsIgnoreCase("clear_session")) {
			String userName = request.getParameter("paramval");
             
			try {
				String[] uArray ={userName};
				handler.clearUsers(uArray);
				/*changed by Leelaprasad for TENJINCG-827 starts*/
				if(tjnSession !=null && tjnSession.getProject()!=null &&tjnSession.getUser()!=null){
					new ProjectHandler().endProjectSession(tjnSession.getProject().getId(), tjnSession.getUser().getId());
				}
				/*changed by Leelaprasad for TENJINCG-827 ends*/
				request.getSession().removeAttribute("TJN_SESSION");
				request.getSession().invalidate();
				request.getSession().setAttribute("SCR_MAP",null);
				request.getRequestDispatcher("login.jsp").forward(request, response);
			} catch (Exception e) {
				
			} 
			
	}else if (task.equalsIgnoreCase("login")) {
		/*changed by Leelaprasad for TENJINCG-827 starts*/
			if(tjnSession !=null && tjnSession.getProject()!=null &&tjnSession.getUser()!=null){
				try {
					new ProjectHandler().endProjectSession(tjnSession.getProject().getId(), tjnSession.getUser().getId());
				} catch (DatabaseException e) {
					
					logger.error("Error ", e);
				}
			}
			/*changed by Leelaprasad for TENJINCG-827 ends*/
		request.getRequestDispatcher("login.jsp").forward(request, response);
	}else if (task.equalsIgnoreCase("logout")) {
		

		try {
			if(tjnSession.getUser()!=null){
			String[] users={tjnSession.getUser().getId()};
			new TenjinSessionHandler().clearUsers(users);
			/*changed by Leelaprasad for TENJINCG-827 starts*/
			if(tjnSession.getProject()!=null){
				new ProjectHandler().endProjectSession(tjnSession.getProject().getId(), tjnSession.getUser().getId());
			}
			/*changed by Leelaprasad for TENJINCG-827 ends*/
			}
			
		} catch (DatabaseException e) {
			request.getRequestDispatcher("error.jsp").forward(request, response);
		}

		if (tjnSession != null) {
			tjnSession.setProject(null);
			tjnSession.setUser(null);

		}

		request.getSession().invalidate();
		request.getRequestDispatcher("login.jsp").forward(request, response);
	} 
 
	else {
		request.getRequestDispatcher("error.jsp").forward(request, response);
	}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
