/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ReportServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 *
 * DATE              CHANGED BY                 DESCRIPTION
 * 21-12-2016		SRIRAM					Defect#TEN-24
 * 20-02-2017		SRIRAM					Defect#TENJINCG-132
 * 18-01-2018		Pushpalatha				TENJINCG-583
 * 
 */
package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.ycs.tenjin.TenjinReport;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.db.ReportHelper;
import com.ycs.tenjin.handler.TestCaseHandler;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.ExcelException;
import com.ycs.tenjin.util.ExcelHandler;
import com.ycs.tenjin.util.Utilities;

public class ReportServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory
			.getLogger(ReportServlet.class);
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String param = request.getParameter("param");
		if(param.equalsIgnoreCase("aut_report")){
			/*Added by Pushpa for TENJINCG-557 starts*/
			TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			Project project=tjnSession.getProject();
			/*Added by Pushpa for TENJINCG-557 ends*/
			ReportHelper reportHelper = new ReportHelper();
			Map<String, Object> reportMap = new HashMap<String, Object>();
			ArrayList<Aut> auts = null;
			try {
				/*Changed by Pushpalatha for TENJINCG-557 starts*/
				/*auts = reportHelper.fetchAllAuts();*/
				auts = reportHelper.hydrateProjectAuts(project.getId());
				/*Changed by Pushpalatha for TENJINCG-557 ends*/
				reportMap.put("STATUS", "SUCCESS");
			} catch (DatabaseException e) {
				reportMap.put("STATUS", "ERROR");
				reportMap.put("MESSAGE", "An Internal Error occurred. Please contact your System Administrator");
			}
			reportMap.put("auts", auts);
			request.getSession().setAttribute("REPORTS_MAP", reportMap);
			response.sendRedirect("report.jsp?param=aut");
		}else if(param.equalsIgnoreCase("user_report")){
			ReportHelper reportHelper = new ReportHelper();
			Map<String, Object> reportMap = new HashMap<String, Object>();
			ArrayList<String> userNames = null;
			//Changed by Sriram to fix defect#TEN-24
			
			try {
				TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
				userNames = reportHelper.getUserDetails(tjnSession.getProject().getId());
				reportMap.put("STATUS", "SUCCESS");
			} catch (DatabaseException e) {
				reportMap.put("STATUS", "ERROR");
				reportMap.put("MESSAGE", "An Internal Error occurred. Please contact your System Administrator");
			}
			//Changed by Sriram to fix defect#TEN-24 ends
			
			
			
			reportMap.put("userNames",userNames);
			request.getSession().setAttribute("REPORTS_MAP", reportMap);
			response.sendRedirect("report.jsp?param=user");
		}else if(param.equalsIgnoreCase("trend_report")){
			ReportHelper reportHelper = new ReportHelper();
			Map<String, Object> reportMap = new HashMap<String, Object>();
			ArrayList<Aut> auts = null;
			try {
				auts = reportHelper.fetchAllAuts();
				reportMap.put("STATUS", "SUCCESS");
			} catch (DatabaseException e) {
				reportMap.put("STATUS", "ERROR");
				reportMap.put("MESSAGE", "An Internal Error occurred. Please contact your System Administrator");
			}
			reportMap.put("auts", auts);
			request.getSession().setAttribute("REPORTS_MAP", reportMap);
			response.sendRedirect("report_trend.jsp");
		}else if(param.equalsIgnoreCase("aut_defect_report")){
			ReportHelper reportHelper = new ReportHelper();
			Map<String, Object> reportMap = new HashMap<String, Object>();
			ArrayList<Aut> auts = null;
			try {
				auts = reportHelper.fetchAllAuts();
				reportMap.put("STATUS", "SUCCESS");
			} catch (DatabaseException e) {
				reportMap.put("STATUS", "ERROR");
				reportMap.put("MESSAGE", "An Internal Error occurred. Please contact your System Administrator");
			}
			reportMap.put("auts", auts);
			request.getSession().setAttribute("REPORTS_MAP", reportMap);
			response.sendRedirect("report_aut_defect.jsp");
		}else if(param.equalsIgnoreCase("get_func")){
			String appId = (String)request.getParameter("app_id");
			String retData = "";
			try {
				JSONObject json = new JSONObject();
				Connection conn = null;
				try {
					conn = DatabaseHelper
							.getConnection(Constants.DB_TENJIN_APP);

					ArrayList<ModuleBean> modules = new ModulesHelper()
							.hydrateModules(conn, Integer.parseInt(appId));

					if (modules != null && modules.size() > 0) {
						JSONArray jArray = new JSONArray();
						for (ModuleBean m : modules) {
							JSONObject js = new JSONObject();
							js.put("code", m.getModuleCode());
							js.put("name", m.getModuleName());
							if (m.getMenuContainer() == null) {
								js.put("menu", "");
							} else {
								js.put("menu", m.getMenuContainer());
							}
							jArray.put(js);
						}

						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("modules", jArray);
					} else {
						json.put("status", "FAILURE");
						json.put("message",
								"No Functions were available for this application");
					}
				} catch (JSONException e) {
					logger.error("An error occurred while processing JSON", e);
					json.put("status", "ERROR");
					json.put("message",
							"An error occurred while processing JSON");
				} finally {
					retData = json.toString();
					if (conn != null) {
						try {
							conn.close();
						} catch (Exception e) {
						}
					}
				}
			} catch (Exception e1) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}else if(param.equalsIgnoreCase("criteria")){
			String criterias = (String)request.getParameter("criteria");
			String[] criteriaArray = criterias.split(",");
			String[] criteriaType = new String[criteriaArray.length];
			String[] criteriaValue = new String[criteriaArray.length];
			JSONObject responseJson = new JSONObject();
			int i =0;
			for (String criteria : criteriaArray) {
				String[] temp = criteria.split("\\|");
				criteriaType[i] = temp[0];
				criteriaValue[i] = temp[1];
				i++;
			}
			List<String> arr = Arrays.asList(criteriaType);
			String appId = "";
			String functionCode = "";
			String fromDate = "";
			String toDate = "";
			String userId = "";
			for (String criteriaTyp : arr) {
				switch (criteriaTyp) {
				case "A":
					appId = criteriaValue[arr.indexOf(criteriaTyp)];
					break;
				case "F":
					functionCode = criteriaValue[arr.indexOf(criteriaTyp)];;
					break;
				case "FD":
					fromDate = criteriaValue[arr.indexOf(criteriaTyp)];
					break;
				case "TD":	
					toDate = criteriaValue[arr.indexOf(criteriaTyp)];
					break;
				case "U":
					userId = criteriaValue[arr.indexOf(criteriaTyp)];
					break;
				/*case "T":
					tillDate = criteriaValue[arr.indexOf(criteriaTyp)];
					break;*/
				default:
					logger.debug("ERROR, could not get the criteria");
					break;
				}
			}
			List<TenjinReport> reportList = null;
			
			//Added by Sriram to fix defect#TEN-24
			TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
			//Added by Sriram to fix defect#TEN-24 ends
			
			JSONArray jArray = new JSONArray();
			try {
				if(appId != "" && functionCode == ""){
					//Changed by Sriram to fix defect#TEN-24
					/*reportList = new ReportHelper().generateApplicationWiseExecutionReport(appId, fromDate, toDate);*/
					reportList = new ReportHelper().generateApplicationWiseExecutionReport(appId, functionCode, fromDate, toDate, tjnSession.getProject().getId());
					//Changed by Sriram to fix defect#TEN-24 ends
				}else if(appId != "" && functionCode != ""){
					//Changed by Sriram to fix defect#TEN-24
					/*reportList = new ReportHelper().generateFunctionWiseExecutionReport(appId, functionCode, fromDate, toDate);*/
					reportList = new ReportHelper().generateApplicationWiseExecutionReport(appId, functionCode, fromDate, toDate, tjnSession.getProject().getId());
					//Changed by Sriram to fix defect#TEN-24 ends
				}
				
				if(reportList != null){
					for (TenjinReport report : reportList) {
						JSONObject reportJson = new JSONObject();
							try {
									reportJson.put("application", report.getApplication());	
									reportJson.put("functioncode", report.getModuleCode());	
									reportJson.put("userid", report.getUserId());	
									reportJson.put("totaltc", report.getNoOfTestCases());	
									reportJson.put("executed", report.getExecuted());
									reportJson.put("passedtc", report.getNoOfPassedTestCases());	
									reportJson.put("failedtc", report.getNoOfFailedTestCases());
									reportJson.put("errortc", report.getNoOfErrorTestCases());
									reportJson.put("failduetoverify", report.getNoOfFailedDueToVerification());
									//Changed by Sriram to fix Defect#TEN-22
									/*reportJson.put("defectslogged", report.getDefects());*/
									reportJson.put("defectslogged", report.getDefectCount());
									reportJson.put("appid", report.getAppId());
									//Changed by Sriram to fix Defect#TEN-22 ends
									reportJson.put("percentage", (int)(((double)report.getNoOfPassedTestCases()/report.getNoOfTestCases())*100));
									reportJson.put("fromdate", report.getFromDate());
									reportJson.put("todate", report.getToDate());
									
									//Added by Sriram to fix Defect#TEN-24
									reportJson.put("totalvals", report.getTotalValidations());
									reportJson.put("passvals", report.getPassedValidations());
									reportJson.put("failedvals", report.getFailedValidations());
									reportJson.put("valpercent", report.getValidationPercentage());
									//Added by Sriram to fix Defect#TEN-24 ends
									
									
							} catch (JSONException e) {
								logger.debug("ERROR while preparing the json for the application execution report");
								try {
									reportJson.put("status", "ERROR");
									responseJson.put("message", "Internal Error Occured please contact Administrator");
								} catch (JSONException e1) {
									logger.debug("ERROR while preparing the response json");
								}
							}
						jArray.put(reportJson);
					}
					try {
						responseJson.put("report", jArray);
						responseJson.put("status", "SUCCESS");
					} catch (JSONException e) {
						logger.debug("ERROR while creating the response json ");
						try {
							responseJson.put("report", jArray);
							responseJson.put("status", "ERROR");
							responseJson.put("message", "Internal Error Occured please contact Administrator");
						} catch (JSONException e1) {
							logger.debug("ERROR while preparing the response json");
						}
					}
				}
			} catch (DatabaseException e) {
				logger.debug("ERROR while processing the  application defect report");
				try {
					responseJson.put("report", jArray);
					responseJson.put("status", "ERROR");
					responseJson.put("message", "Internal Error Occured please contact Administrator");
				} catch (JSONException e1) {
					logger.debug("ERROR while preparing the response json");
				}
			}
			String retData = responseJson.toString();
			response.getWriter().write(retData);
		}else if(param.equalsIgnoreCase("dwld_report")){
			
			String json = request.getParameter("json");
			String reportType= request.getParameter("reportType");
			String folderPath = request.getServletContext().getRealPath("/");
			Utilities.checkForDownloadsFolder(folderPath);
			JSONObject responseJson = new JSONObject();
			String ret = "";
			try {
				try {
					/*Changed by Pushpalatha for TENJINCG-567 starts*/
					String fileName = new ExcelHandler().generateReportSpreadsheet(reportType, json, folderPath + File.separator+"Downloads");
					responseJson.put("status", "SUCCESS");
					
					responseJson.put("path", "Downloads"+File.separator + fileName);
					/*Changed by Pushpalatha for TENJINCG-567 ends*/
				} catch (ExcelException e) {
					
					responseJson.put("status", "ERROR");
					responseJson.put("message", "An internal error occurred. Please contact Tenjin Support.");
				}
				
				ret = responseJson.toString();
			} catch (JSONException e) {
				
				logger.error("JSONException occurred --> ", e);
				ret = "{status:ERROR,message:An Internal error occurred. Please contact Tenjin Support.}";
			}
			
			response.getWriter().write(ret);
		}
	
	/*Added by ashiki for TJN27-180 by starts*/
	else if(param.equalsIgnoreCase("TC_EXECUTION_DETAIL_REPORT")){
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Project project=tjnSession.getProject();
		JsonObject json = new JsonObject();
		String retData = "";
		int projectId = project.getId();
		String exeFromDate = request.getParameter("execDate");
		String exeToDate = request.getParameter("exectoDate");
		String tcId = "";
		int tcRecId = 0;
		String tc = request.getParameter("tc");
		if(!tc.equalsIgnoreCase("-1")){
			tcRecId = Integer.parseInt(tc.split(":")[0]);
			tcId = tc.split(":")[1];
		}
		HashMap<String,String> entityMap = new LinkedHashMap<String,String>();
		ArrayList<HashMap<String,Object>> data = new ArrayList<HashMap<String,Object>>();
		String title="Test Case Execution Detail";
		String subTitle="Stepswise execution report of a Test Case";
		try {
			
			Project prj=new Project();
			prj= new ProjectHelper().hydrateProject(projectId);
			entityMap.put("Domain", prj.getDomain());
			entityMap.put("Project", prj.getName());
			entityMap.put("Executed From", exeFromDate);
			entityMap.put("Executed To", exeToDate);
			new TestCaseHandler().validateReportdata(entityMap);
			
			data = new ReportHelper().getTestCaseExecutionDetailData(projectId, tcRecId, exeFromDate,exeToDate);
			String folderPath = request.getServletContext().getRealPath("/");
			Utilities.checkForDownloadsFolder(folderPath);
			folderPath = folderPath + "\\Downloads";
			String fileName =title+"_"+tcId+"_Report";
			ExcelHandler excelHandler=new ExcelHandler();
			fileName = excelHandler.removeSpaces(fileName);
				fileName = fileName + ".xlsx";
				if(data.size()>0)
					excelHandler.generateExcelReport(title, subTitle, entityMap, data, folderPath, fileName);
			if(data.size()>0){
				json.addProperty("status", "SUCCESS");
				json.addProperty("message","");
				json.addProperty("path", "Downloads/" + fileName);
			}
			else{
				json.addProperty("status", "FAILURE");
				json.addProperty("message",	"No data available for this report");
			}
		}catch(Exception e) {
			json.addProperty("status","FAILURE");
			json.addProperty("message","An internal error occurred. Please try again later");
			logger.error("Could not create PDF due to an internal error",e);
		} finally{
			retData = json.toString();
			response.getWriter().write(retData);
		}
	}
	/*Added by ashiki for TJN27-180 by end*/
}
}