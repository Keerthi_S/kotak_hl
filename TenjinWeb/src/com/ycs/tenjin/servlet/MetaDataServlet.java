/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MetaDataServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 14-12-2016           Leelaprasad             Defect#TEN-64
22-Sep-2017			sameer gupta			For new adapter spec
 *18-01-2018				Pushpalatha				TENJINCG-581
 *29-06-2018			Sriram Sridharan		T251IT-101
 *02-11-2018			Sriram SRidharan			TENJINCG-894
 *13-11-2018            Padmavathi              TENJINCG-899
 */

package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.ibm.wsdl.util.IOUtils;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.aut.MetadataAudit;
import com.ycs.tenjin.bridge.oem.gui.datahandler.GuiDataHandlerImpl;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.MetaDataHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.MetaDataUtils;
import com.ycs.tenjin.util.Utilities;

public class MetaDataServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory
			.getLogger(MetaDataServlet.class);

	public static String appId;
	public static String appName;
	MetaDataUtils utils = new MetaDataUtils();
	public MetaDataServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String param = request.getParameter("param");

		if(param.equalsIgnoreCase("export_selection")){
			request.getSession().setAttribute("METADATA_UTIL", utils);
			Map<String, Object> output = new HashMap<String, Object>();
			ArrayList<Aut> auts = null;
			try {
				auts = new AutHelper().hydrateAllAut();
				output.put("auts", auts);
				output.put("STATUS", "SUCCESS");
				output.put("message", "");
			} catch (DatabaseException e) {
				output.put("STATUS", "ERROR");
				output.put("message",
						"An internal error occurred. Please try again.");
			}
			request.getSession().setAttribute("METADATA_MAP", output);
			response.sendRedirect("metadata_export.jsp");
		}
		
		//TENJINCG-894 (Sriram)
		else if(param.equalsIgnoreCase("testObject")) {
			int appId = 0;
			String functionCode = Utilities.trim(request.getParameter("func"));
			String pageAreaName = Utilities.trim(request.getParameter("page"));
			String fieldUniqueName = Utilities.trim(request.getParameter("funame"));
			
			JsonObject json = new JsonObject();
			
			try {
				appId = Integer.parseInt(Utilities.trim(request.getParameter("app")));
			} catch (NumberFormatException e) {
				logger.error("Error - Invalid app ID [{}]", request.getParameter("app"));
				json.addProperty("status", "error");
				json.addProperty("message", "Invalid App ID specified.");
				response.getWriter().write(json.toString());
				return;
			}
			
			try {
				TestObject to = new MetaDataHelper().hydrateTestObject(appId, functionCode, pageAreaName, fieldUniqueName);
				Gson gson = new GsonBuilder().create();
				JsonObject toJson = gson.fromJson(gson.toJson(to), JsonObject.class);
				json.addProperty("status", "success");
				json.add("content", toJson);
				response.getWriter().write(json.toString());
			} catch (DatabaseException e) {
				json.addProperty("status", "error");
				json.addProperty("message", e.getMessage());
				response.getWriter().write(json.toString());
				return;
			}
			
			
		}
		//TENJINCG-894 (Sriram) ends
		
		else if (param.equalsIgnoreCase("get_func")) {
			String appId = (String)request.getParameter("app_id");
			String retData = "";
			try{
				JSONObject json = new JSONObject();
				Connection conn = null;
				try {
					conn = DatabaseHelper
							.getConnection(Constants.DB_TENJIN_APP);

					ArrayList<ModuleBean> modules = new ModulesHelper()
							.hydrateModules(conn, Integer.parseInt(appId));

					if (modules != null && modules.size() > 0) {
						JSONArray jArray = new JSONArray();
						for (ModuleBean m : modules) {
							JSONObject js = new JSONObject();
							js.put("code", m.getModuleCode());
							js.put("name", m.getModuleName());
							if (m.getMenuContainer() == null) {
								js.put("menu", "");
							} else {
								js.put("menu", m.getMenuContainer());
							}
							jArray.put(js);
						}

						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("modules", jArray);
					} else {
						json.put("status", "FAILURE");
						json.put("message",
								"No Functions were available for this application");
					}
				} catch (JSONException e) {
					logger.error("An error occurred while processing JSON", e);
					json.put("status", "ERROR");
					json.put("message",
							"An error occurred while processing JSON");

				} finally {
					retData = json.toString();
					if (conn != null) {
						try {
							conn.close();
						} catch (Exception e) {
						}
					}
				} }catch (Exception e1) {
					retData = "An internal error occurred. Please contact your System Administrator";
				}
			response.getWriter().write(retData);
		}else if(param.equalsIgnoreCase("audit")){
			String type = request.getParameter("type");
			Map<String, Object> map = new HashMap<String, Object>();
			try{
				ArrayList<MetadataAudit> auditMap = new MetaDataHelper().getAuditDetails(type);
				map.put("audit_map", auditMap);
				map.put("status", "SUCCESS");
				map.put("message", "");
			}catch(Exception e){
				map.put("status", "ERROR");
				map.put("message", "Internal Error Occured");
			}
			request.getSession().setAttribute("AUDIT_MAP", map);
			response.sendRedirect("metadata_audit.jsp");
		} else if(param.equalsIgnoreCase("progress_bar")){
			//MetaDataUtils utils1 = (MetaDataUtils) request.getSession().getAttribute("METADATA_UTIL");

			Integer totalCount = utils.getTotalFunctionCount();
			Integer currentCount = utils.getCurrentFunctionCount();
			JSONObject j = new JSONObject();
			String retData = "";
			try{
				j.put("status","success");
				j.put("counter", currentCount);
				j.put("total", totalCount);
				retData = j.toString();
			}catch(Exception e){
				retData = "{status:error, message:An ineternal error occurred}";
			}
			response.getWriter().write(retData);
		}else{
			if (request.getParameter("param1") != null) {
				String txn1 = request.getParameter("param1");
				String[] str = txn1.split("_");
				appId = str[0];
				appName = str[1];
				response.sendRedirect("selectiveFields.jsp");
			}

			if (param != null && param.equalsIgnoreCase("tree")) {
				String jsonString = "";

				String app = request.getParameter("a");
				String func = request.getParameter("f");
				int appId = Integer.parseInt(app);
				//T251IT-101 - Sriram
				/*try {
				jsonString = new MetaDataHelper().getMetadataJSON( appId, func);
			} catch (Exception e) {
				
				logger.error("ERROR generating metadata JSON", e);
			}*/

				String txnMode = request.getParameter("txnMode");
				String operation = request.getParameter("o");
				if(txnMode.equalsIgnoreCase("G")) {
					try {
						jsonString = new MetaDataHelper().getMetadataJSON( appId, func);
					} catch (Exception e) {
						
						logger.error("ERROR generating metadata JSON", e);
					}
				}else {
					try {
						jsonString = new MetaDataHelper().getMetadataJSON( appId, func, operation);
					} catch (Exception e) {
						
						logger.error("ERROR generating metadata JSON", e);
					}
				}
				//T251IT-101 - Sriram ends
				response.getWriter().write(jsonString);
			}
		}
	}



	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String param = request.getParameter("param");
		Authorizer authorizer=new Authorizer();
		if(param.equalsIgnoreCase("update_field_user_info")) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		if(param != null && param.equalsIgnoreCase("selectivedFields")){
			JSONObject jsonStatus = new JSONObject();
			/*Changed by Leelaprasad for the Requirement Defect fix#TEN-64 on 14-12-2016 starts*/
			String jsonString=null;
			/*Changed by Leelaprasad for the Requirement Defect fix#TEN-64 on 14-12-2016 ends*/
			try {
				String txn = request.getParameter("param");
				String app = request.getParameter("a");
				String func = request.getParameter("f");
				int appId = Integer.parseInt(app);

				JSONArray superjson = new JSONArray();
				if (txn != null && txn.equalsIgnoreCase("selectivedFields")) {
					/*Changed by Leelaprasad for the Requirement Defect fix#TEN-64 on 14-12-2016 starts*/
					/*String jsonString = request.getParameter("json");*/
					jsonString = request.getParameter("json");
					/*Changed by Leelaprasad for the Requirement Defect fix#TEN-64 on 14-12-2016 ends*/
					jsonString = jsonString.substring(4, jsonString.length());
					String[] strArrByNextLine = jsonString.split("\r");

					String[] strArrByNextLineSorted = new String[strArrByNextLine.length];
					int tracker = 0;
					for (String parent : strArrByNextLine) {
						String parentChk = "";
						if (!parent.equals("")) {
							parentChk = parent;

							try {
								for (int i = 0; i < strArrByNextLine.length; i++) {
									if (parentChk.equals(strArrByNextLine[i])) {
										strArrByNextLineSorted[tracker] = strArrByNextLine[i];
										strArrByNextLine[i] = "";
										i++;
										strArrByNextLineSorted[++tracker] = strArrByNextLine[i];
										strArrByNextLine[i] = "";
										tracker++;
									} else
										i++;
								}
							} catch (Exception e) {
								logger.error(" Exception occured during selecting fields");
							}
						}
					}
					for (int i = 0; i < strArrByNextLine.length; i++)
						strArrByNextLine[i] = strArrByNextLineSorted[i];

					// ========================================================
					for (int i = 0; i < strArrByNextLine.length; i++) {
						for (int j = i + 1; j < strArrByNextLine.length; j++)
							if (strArrByNextLine[i].equals(strArrByNextLine[j]))
								strArrByNextLine[j] = "";
					}

					String[] strArrByNextLineTemp = new String[strArrByNextLine.length];
					int k = 0;
					for (int i = 0; i < strArrByNextLineTemp.length; i++) {
						if (!strArrByNextLine[i].equals("")) {
							strArrByNextLineTemp[k] = strArrByNextLine[i];
							k++;
						}
					}
					// ========================================================
					int len = strArrByNextLineTemp.length;
					try {

						label1: for (int i = 0; i < len; i++) {
							JSONObject json = new JSONObject();
							String[] strArrByComma;
							strArrByComma = strArrByNextLineTemp[i].split(",");
							if (!strArrByComma[0].equals("page"))
								continue label1;

							json.put("name", strArrByComma[1]);
							json.put("type", strArrByComma[2]);
							json.put("ismrb", strArrByComma[3]);
							JSONArray allPages = new JSONArray();

							try {
								for (int j = i + 1;; j++) {
									strArrByComma = strArrByNextLineTemp[j]
											.split(",");
									if (!strArrByComma[0].equals("field")) {
										i = j - 1;
										json.put("fields", allPages);
										superjson.put(json);
										continue label1;
									}
									JSONObject jsonTemp = new JSONObject();
									jsonTemp.put("name", strArrByComma[1]);
									jsonTemp.put("type", strArrByComma[2]);
									jsonTemp.put("mandatory", strArrByComma[3]);
									jsonTemp.put("default_option", strArrByComma[4]);
									jsonTemp.put("object_class", strArrByComma[5]);
									/*Added by Padmavathi for TENJINCG-899 starts*/
									jsonTemp.put("userRemarks", strArrByComma[6]);
									/*Added by Padmavathi for TENJINCG-899 ends*/
									allPages.put(jsonTemp);
								}
							} catch (Exception e) {
								json.put("fields", allPages);
								superjson.put(json);
								break;
							}

						}
					} catch (JSONException jsone) {
						logger.error("json Exception occured during selecting fields");
					}
				}

				GuiDataHandlerImpl sh = new GuiDataHandlerImpl();
				String folderPath = request.getServletContext().getRealPath("/");

				com.ycs.tenjin.util.Utilities.checkForDownloadsFolder(folderPath);

				folderPath = folderPath + "Downloads";
				sh.generateTemplate(appId, func,
						folderPath, func+"_TTD",
						superjson);

				jsonStatus.put("status", "success");
				jsonStatus.put("message", "File downloaded successfully");
				/*Changed by Pushpalatha for TENJINCG-567 starts*/
				jsonStatus.put("path","Downloads"+File.separator+func+"_TTD.xlsx");
				/*Changed by Pushpalatha for TENJINCG-567 ends*/
			} catch (Exception jsonStatusException1) {
				try {
					jsonStatus.put("status", "failure");
					/*Changed by Leelaprasad for the Requirement Defect fix#TEN-64 on 14-12-2016 starts*/
					if(jsonString==null){
						jsonStatus.put("message", "Please Select fields to generate the Template.");
					}else{
						/*Changed by Leelaprasad for the Requirement Defect fix#TEN-64 on 14-12-2016 ends*/
						jsonStatus.put("message", "An error occurred while generating template. Please contact Tenjin Administrator.");
						/*Changed by Leelaprasad for the Requirement Defect fix#TEN-64 on 14-12-2016 starts*/

					}
					/*Changed by Leelaprasad for the Requirement Defect fix#TEN-64 on 14-12-2016 ends*/
				} catch (JSONException jsonStatusException2) {
					logger.error("json Exception occured during selecting fields");
				}
			}
			response.getWriter().write(jsonStatus.toString());
		}
		//T251IT-101 - Sriram
		else if(param != null && param.equalsIgnoreCase("selectiveFieldsApi")) {
			JSONObject jsonStatus = new JSONObject();
			String app = request.getParameter("a");
			String func = request.getParameter("f");
			String op = request.getParameter("o");
			String superjson = request.getParameter("json");
			GuiDataHandlerImpl impl = new GuiDataHandlerImpl();
			String folderPath = request.getServletContext().getRealPath("/");

			com.ycs.tenjin.util.Utilities.checkForDownloadsFolder(folderPath);

			folderPath = folderPath + "Downloads";
			String res = "";
			try {
				try {
					impl.generateTemplate(Integer.parseInt(app), func,folderPath, func+"_"+op + "_TTD",new JSONArray(superjson));
					jsonStatus.put("status", "success");
					jsonStatus.put("message", "File downloaded successfully");
					jsonStatus.put("path","Downloads"+File.separator+func+"_" + op + "_TTD.xlsx");
				} catch (NumberFormatException e) {
					jsonStatus.put("status", "error");
					jsonStatus.put("message", "An internal error occurred. Please contact Tenjin Support.");
					logger.error("Number format exception --> {}", e.getMessage(), e);
				} finally {
					res = jsonStatus.toString();
				}
			} catch (JSONException e) {
				logger.error(e.getMessage(),e);
				res = "{status:'error',message:'An internal error occurred. Please contact Tenjin Support.'}";
			}
			
			response.getWriter().write(res);
		}
		//T251IT-101 - Sriram ends	
		
		//TENJINCG-894 (Sriram)
		else if(param.equalsIgnoreCase("update_field_user_info")) {
			TestObject t = new TestObject();
			String toJson = IOUtils.getStringFromReader(request.getReader());
			JsonObject json = new JsonObject();
			int appId=0;
			String functionCode = Utilities.trim(request.getParameter("func"));
			
			try {
				appId = Integer.parseInt(Utilities.trim(request.getParameter("app")));
			}catch(NumberFormatException e) {
				logger.error("Error - Invalid application ID [{}]", request.getParameter("app"));
				json.addProperty("status", "error");
				json.addProperty("message", "Invalid Application ID");
				response.getWriter().write(json.toString());
				return;
			}
			
			try {
				t = new GsonBuilder().create().fromJson(toJson, TestObject.class);
				new MetaDataHelper().updateFieldUserInfo(t, appId, functionCode);
				json.addProperty("status", "success");
				json.addProperty("message", "Field updated successfully");
				response.getWriter().write(json.toString());
				return;
			} catch (JsonSyntaxException e) {
				logger.error("An error occurred while parsing JSON to TestObject. Offending JSON is {}", toJson, e);
				json.addProperty("status", "error");
				json.addProperty("message", "An internal error occurred. Please contact Tenjin Support.");
				response.getWriter().write(json.toString());
				return;
			} catch (DatabaseException e) {
				json.addProperty("status", "error");
				json.addProperty("message", e.getMessage());
				response.getWriter().write(json.toString());
				return;
			}
		}
		//TENJINCG-894 (Sriram) Ends
		
		
		else{

			String appId = request.getParameter("app_id");
			String fileName = request.getParameter("fileName");
			String data = request.getParameter("data");
			TenjinSession tjnsession = (TenjinSession) request.getSession()
					.getAttribute("TJN_SESSION");
			JSONObject json = new JSONObject();
			String data1 = data.substring(0, data.length()-1);
			String[] dataArray = data1.split(";", 3);
			String type = dataArray[0];
			String modules = null;
			String apps = null;
			if(type.equalsIgnoreCase("func")){
				apps = appId;
				if(dataArray.length >= 3){
					modules = dataArray[2];
				};
			}else if(type.equalsIgnoreCase("app")){
				if(dataArray.length >= 3){
					apps = dataArray[2];
				};
			}
			String selector = dataArray[1];

			String retData = "";
			try {
				File dir = new File(request.getSession().getServletContext()
						.getRealPath("/")
						+ "\\Downloads");
				if (!dir.exists()) {
					dir.mkdir();
				}
				String folderPath = request.getSession().getServletContext()
						.getRealPath("/")
						+ "\\Downloads\\";

				utils.generateMetadataXML(folderPath, fileName, modules, apps, selector);

				MetadataAudit audit = new MetadataAudit();
				audit.setUser(tjnsession.getUser().getId());
				audit.setDate(com.ycs.tenjin.util.Utilities.getDetailedTimeStamp());
				audit.setStatus("SUCCESS");
				audit.setType("EXPORT");

				new MetaDataHelper().persistExportAudit(audit);
				utils = new MetaDataUtils();
				json.put("message", "Functions Download Successfully");
				json.put("status", "SUCCESS");
				json.put("path", "./Downloads/"+fileName+".xml");
				json.put("tc_id", "");
			} catch (Exception e) {
				logger.error("An exception occured while downloading Metadata", e);
				try {
					json.put("status", "ERROR");
					json.put("message", "An Internal Error Occurred. Please try again");

					MetadataAudit audit = new MetadataAudit();
					audit.setUser(tjnsession.getUser().getId());
					audit.setDate(com.ycs.tenjin.util.Utilities.getDetailedTimeStamp());
					audit.setStatus("FAILURE");
					audit.setType("EXPORT");

					new MetaDataHelper().persistExportAudit(audit);
				} catch (JSONException | DatabaseException | SQLException e1) {
					logger.error("An exception occured while downloading Metadata", e);
				}

			}
			retData = json.toString();
			
			response.getWriter().write(retData);
		}
	}
}
