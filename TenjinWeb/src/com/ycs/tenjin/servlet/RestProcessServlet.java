/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExtractorServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION 

19-Jan-2018			Pushpalatha				TenjinCG-581
08-Feb-2018         Padmavathi              TENJINCG-601
21-Feb-2018         Padmavathi              TENJINCG-601
26-06-2018			Preeti					T251IT-148
10-04-2019			Preeti					TENJINCG-1026
*/


package com.ycs.tenjin.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ycs.tenjin.adapter.api.rest.generic.RestDocumentHandler;
import com.ycs.tenjin.adapter.api.rest.imports.openapi.OpenAPIImportHandler;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ModuleHelper;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class RestProcessServlet
 */
@WebServlet("/RestProcessServlet")
public class RestProcessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory	.getLogger(ClientServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RestProcessServlet() {
        super();
        
    }

    public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		@Override
		public boolean checkAccess(HttpServletRequest request) {
			if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
				return true;
			}
			return false;
		}  
	};
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String txn = request.getParameter("param");
		 Authorizer authorizer=new Authorizer();
			if(txn.equalsIgnoreCase("") || txn.equalsIgnoreCase("view") || txn.equalsIgnoreCase("new") || txn.equalsIgnoreCase("oauth_view") || txn.equalsIgnoreCase("exist_check") || txn.equalsIgnoreCase("download_APK_file") ) {
				if(!authorizer.hasAccess(request)){
					response.sendRedirect("noAccess.jsp");
					return;
				}
			}
		//Changes for TENJINCG-1118 (Sriram)
		if(txn.equalsIgnoreCase("importFromUrl")) {
			
			if(request.getSession().getAttribute("openAPIImportHandler") != null) {
				request.getSession().removeAttribute("openApiImportHandler");
			}
			
			String url = Utilities.trim(request.getParameter("url"));
			JsonObject status = new JsonObject();
			try {
				OpenAPIImportHandler openAPIImportHandler = new OpenAPIImportHandler();
				openAPIImportHandler.initializeFromLocation(url);
				List<Api> listOfApis = openAPIImportHandler.getAllAPIs();
				int appId = Integer.parseInt(request.getParameter("appid"));
				List<Api> existingApis = new ApiHelper().hydrateAllApiWithDetails(appId);
				if((listOfApis != null && listOfApis.size() > 0)) {
					for(Api api : listOfApis) {
						api.setNonExistent(true);
						if ((existingApis != null && existingApis.size() > 0)) {
							String key = api.getOperations().get(0).getMethod().toLowerCase() + "_" + api.getUrl();
							boolean found = false;
							for (Api eApi : existingApis) {
								String eKey = eApi.getOperations().get(0).getMethod().toLowerCase() + "_"
										+ eApi.getUrl();
								if (eKey.equalsIgnoreCase(key)) {
									found = true;
									break;
								}
							}
							if (found) api.setNonExistent(false);
						}
					}
				}
				status.addProperty("status", "SUCCESS");
				JsonArray apisJsonArray = new Gson().fromJson(new Gson().toJson(listOfApis), JsonArray.class);
				status.add("apis", apisJsonArray);
				request.getSession().setAttribute("openAPIImportHandler", openAPIImportHandler);
			} catch(Exception e) {
				status.addProperty("status", "ERROR");
				status.addProperty("message", e.getMessage());
			}
			
			response.getWriter().write(status.toString());
		}//Changes for TENJINCG-1118 (Sriram) ends
		
		/*added by Padmavathi for TENJINCG-602 ends*/
		else{
		ArrayList listOfAPI=(ArrayList) request.getSession().getAttribute("listOfAPI");
	
		ArrayList<Api> newApiList=null;
	
		if(txn.equalsIgnoreCase("fetch_api_list")){
			String appID=request.getParameter("appId");
			String apiCodes = request.getParameter("apiCodes");
			ApiHelper helper=new ApiHelper();
			ArrayList<Api> apilist=new ArrayList<Api>();
			/*Changed by Pushpalatha for TENJINCG-588 starts*/
			try {
				
				String[] array=Utilities.trim(apiCodes).split(",");
				for(String apiCode:array){
					if(Utilities.trim(apiCode).length()<1){
						continue;
					}
					/*Added by Padmavathi for TENJINCG-601 starts*/
					else if(new ModuleHelper().hydrateModule(Integer.parseInt(appID), apiCode)!=null){
						logger.error("ERROR - Module {} already exists for application {}", apiCode, appID);
						continue;
					}
					/*Added by Padmavathi for TENJINCG-601 ends*/
					Api api=new Api();
					api.setCode(apiCode);
					apilist.add(api);
				}
			} catch (Exception e) {
				
				logger.error("Error ", e);
			}
			/*Changed by Pushpalatha for TENJINCG-588 ends*/
			RestDocumentHandler restHandler=new RestDocumentHandler();
			newApiList=restHandler.getApi(apilist, listOfAPI,appID);
			/*Modified by Preeti for T251IT-148 starts*/
			if(newApiList !=null && newApiList.size()>0){
			try {
				helper.persistAPIs(newApiList);
				response.getWriter().append("Served at: ").append(request.getContextPath());
				SessionUtils.setScreenState("success", "API(s) saved successfully.", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("API_LIST", newApiList);
				request.setAttribute("Type", "swagger");
				/*request.setAttribute("API_OPERATION_LIST", newApiOperationList);*/
				request.getRequestDispatcher("aut_api_information.jsp").forward(request	,response);
			} catch (DatabaseException e) {
				
				logger.error("Error ", e);
			}
			}
			else{
				SessionUtils.setScreenState("error", "No API(s) to save.", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("Type", "swagger");
				request.getRequestDispatcher("aut_api_information.jsp").forward(request	,response);
			}
			/*Modified by Preeti for T251IT-148 ends*/	
		}
		
		
		
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Changes for TENJINCG-1118 (Sriram)
		String requestBody = getBody(request);
		JsonArray jsonArray = new Gson().fromJson(requestBody, JsonArray.class);
		List<Api> apis = new ArrayList<Api>();
		
		for(int i=0; i<jsonArray.size(); i++) {
			JsonObject apiJson = jsonArray.get(i).getAsJsonObject();
			apis.add(new Gson().fromJson(apiJson, Api.class));
		}
		
		OpenAPIImportHandler openApiImportHandler = (OpenAPIImportHandler) request.getSession().getAttribute("openAPIImportHandler");
		openApiImportHandler.enrichApis(apis);
		int appId = Integer.parseInt(Utilities.trim(request.getParameter("app")));
		ApiHelper helper=new ApiHelper();
		for(Api api : apis) {
			api.setApplicationId(appId);
		}
		JsonObject jsonObject = new JsonObject();
		try {
			helper.persistAPIs(apis);
			jsonObject.addProperty("status", "SUCCESS");
		} catch (DatabaseException e) {
			jsonObject.addProperty("status", "ERROR");
			jsonObject.addProperty("message", e.getMessage());
		}
		
		response.getWriter().write(jsonObject.toString());
		//Changes for TENJINCG-1118 (Sriram) ends
		
		
	}
	
	
	public static String getBody(HttpServletRequest request) throws IOException {

	    String body = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    BufferedReader bufferedReader = null;

	    try {
	        InputStream inputStream = request.getInputStream();
	        if (inputStream != null) {
	            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	            char[] charBuffer = new char[128];
	            int bytesRead = -1;
	            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
	                stringBuilder.append(charBuffer, 0, bytesRead);
	            }
	        } else {
	            stringBuilder.append("");
	        }
	    } catch (IOException ex) {
	        throw ex;
	    } finally {
	        if (bufferedReader != null) {
	            try {
	                bufferedReader.close();
	            } catch (IOException ex) {
	                throw ex;
	            }
	        }
	    }

	    body = stringBuilder.toString();
	    return body;
	}

}
