/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TenjinUserServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 30-10-2017			Roshni Das				Newly added for TENJINCG-398
* 29-Jan-2018			Preeti					TENJINCG-507
* 24-04-2018			Preeti					Added catch block for getUser(RecordNotFoundException) 
* 18-05-2018			Pushpalatha				TENJINCG-648
* 14-06-2018            Padmavathi              for T251IT-11
* 26-02-2019			Preeti					TENJINCG-988
* 27-02-2019			Preeti					TENJINCG-984
* 29-04-2019			Pushpalatha				TENJINCG-1035
* 07-06-2019			Pushpalatha				TENJINCG-1063,1070
* 24-06-2019            Padmavathi              for license
* 03-12-2019			Pushpalatha				TENJINCG-1170
* 04-12-2019			Preeti					TENJINCG-1174
* */

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.ycs.tenjin.MaxActiveUsersException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.exception.ResourceConflictException;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.ExcelHandler;
import com.ycs.tenjin.util.PdfHandler;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class TenjinUserServlet
 */
@WebServlet("/TenjinUserServlet")
public class TenjinUserServlet extends HttpServlet {

	public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		@Override
		public boolean checkAccess(HttpServletRequest request) {
			User loggedInUser = SessionUtils.getTenjinSession(request).getUser();
			String userId = "";
			if (request.getMethod().equalsIgnoreCase("GET")) {
				String task = Utilities.trim(request.getParameter("t"));
				userId = Utilities.trim(request.getParameter("key"));

				if (Utilities.trim(task).equalsIgnoreCase("view")) {
					if (new AdminAuthorizationRuleImpl().checkAccess(request)
							|| Utilities.trim(userId).equalsIgnoreCase(loggedInUser.getId())) {
						return true;
					}
					return false;
				} else {
					// All other operations, allow only admin
					if (new AdminAuthorizationRuleImpl().checkAccess(request)) {
						return true;
					}

					return false;
				}
			} else if (request.getMethod().equalsIgnoreCase("POST")) {
				String updateStatus = Utilities.trim(request.getParameter("updateStatus"));
				String operation = request.getParameter("operation");
			    userId = request.getParameter("id");
			    if(updateStatus.equalsIgnoreCase("true")) {
			    	
			    	if (new AdminAuthorizationRuleImpl().checkAccess(request)
							&&  !Utilities.trim(userId).equalsIgnoreCase(loggedInUser.getId())) {
						return true;
					}
			    	
			    }
			    else if(operation.equalsIgnoreCase("new")) {
			    	if (new AdminAuthorizationRuleImpl().checkAccess(request)) {
						return true;
					}
			    }else if(operation.equalsIgnoreCase("update")) {
			    	if (new AdminAuthorizationRuleImpl().checkAccess(request)
							|| Utilities.trim(userId).equalsIgnoreCase(loggedInUser.getId())) {
						return true;
					}
			    }
			    return false;
			}
			return false;
		}

	};
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory	.getLogger(TenjinUserServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TenjinUserServlet() {
		super();
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		SessionUtils.loadScreenStateToRequest(request);
		TenjinSession tjnSession = SessionUtils.getTenjinSession(request);
		UserHandler handler = new UserHandler();
		ProjectHandler projectHandler = new ProjectHandler();
		String task = Utilities.trim(request.getParameter("t"));
		/*added by shruthi for VAPT FIX to Servlets starts*/
		Authorizer authorizer=new Authorizer();
		if(task.equalsIgnoreCase("") || task.equalsIgnoreCase("new") || task.equalsIgnoreCase("view") ||task.equalsIgnoreCase("ActiveUser") ) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		/*added by shruthi for VAPT FIX to Servlets ends*/
		String userId = Utilities.trim(request.getParameter("key"));
		if (task.equalsIgnoreCase("new")) {
			User user = new User();
			request.setAttribute("user", user);
			request.getRequestDispatcher("v2/user_new.jsp").forward(request, response);
		} else if (task.equalsIgnoreCase("view")) {
			/*Modified by Pushpa for TENJINCG-1170 starts*/
			User cUser = tjnSession.getUser();
			User user=new User();
			try {
				handler.validateUserUpdate(userId,cUser);
				user = handler.getUser(userId);
				request.setAttribute("user", user);
			/*Modified by Pushpa for TENJINCG-1170 ends*/
				/*Added by Pushpa for TENJINCG-1170 starts*/
				request.setAttribute("loggedInUser", tjnSession.getUser().getId());
				/*Added by Pushpa for TENJINCG-1170 ends*/
				
				/*Added by Preeti for TENJINCG-988 starts*/
				ArrayList<Project> projects = projectHandler.hydrateProjectsForCurrentUser(userId,0);
				request.setAttribute("projects",projects);
				/*Added by Preeti for TENJINCG-988 ends*/
				request.getRequestDispatcher("v2/user_view.jsp").forward(request, response);
			} 
			/*Added by Pushpa for TENJINCG-1163 starts*/
			catch(RequestValidationException e){
				try {
					user = handler.getUser(cUser.getId());
				} catch (RecordNotFoundException | DatabaseException e1) {
					
					logger.error("Error ", e1);
				}
				SessionUtils.setScreenState("error", e.getMessage(), request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("user", user);
				request.getRequestDispatcher("v2/user_view.jsp").forward(request, response);
			
			}
			/*Added by Pushpa for TENJINCG-1163 ends*/
			catch (RecordNotFoundException e) {
				logger.error("Invalid user ID {}", userId);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			}catch (DatabaseException e) {
				logger.error("Invalid user ID {}", userId);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			}
		} else if (task.length() < 1 || task.equalsIgnoreCase("list")) {
			try {
				List<User> users = handler.getAllUsers();
				request.setAttribute("users", users);
				request.getRequestDispatcher("v2/user_list.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching all clients", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		} else if (task.equalsIgnoreCase("ActiveUser")) {
			try {
				String loggedUser = tjnSession.getUser().getId();
				List<User> users = new UserHandler().getActiveUsers(loggedUser);
				/*Added by Preeti for TENJINCG-984 starts*/
				List<String> usersId= new ArrayList<>();
				for(User user : users){
					usersId.add(user.getId());
				}
				Map<String,String> taskType = handler.getCurrentTaskForUser(usersId);
				for(String userid : usersId){
					if(taskType.get(userid) !=null)
						if(taskType.get(userid).equalsIgnoreCase("Learn"))
							taskType.put(userid, "Learning");
						else if(taskType.get(userid).equalsIgnoreCase("Execute"))
							taskType.put(userid, "Executing");
						else if(taskType.get(userid).equalsIgnoreCase("Extract"))
							taskType.put(userid, "Extracting");
						else if(taskType.get(userid).equalsIgnoreCase("Learn (API)"))
							taskType.put(userid, "Learning API");
				}
				request.setAttribute("currentTask", taskType);
				/*Added by Preeti for TENJINCG-984 ends*/
				request.setAttribute("users", users);
				request.getRequestDispatcher("v2/current_users.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching active user", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} catch (SQLException e) {
				logger.error("ERROR fetching active user", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}

		}
		/*Added by Pushpa for TENJINCG-1070,1067 starts */
		else if (task.equalsIgnoreCase("user_activity_report")) {
			JsonObject json = new JsonObject();
			String retData = "";
			String UserForReport=request.getParameter("user");
			String startDate=request.getParameter("startDate");
			String endDate=request.getParameter("endDate");
			String reportType=request.getParameter("reportType");
			String title="User Activity Report";
			String subTitle="";
			try {
				String folderPath = request.getServletContext().getRealPath("/");
				Utilities.checkForDownloadsFolder(folderPath);
				folderPath = folderPath + "\\Downloads";
				String fileName="UserActivity";
				HashMap<String,String> map=new LinkedHashMap<>();
				map.put("User Name",UserForReport);
				map.put("From", startDate);
				map.put("To", endDate);
				logger.info("Fetching user details");
				ArrayList<HashMap<String, Object>> list=handler.getUserActivityReport(UserForReport,startDate,endDate);
				logger.info("Size of report="+list.size());
				if(list.size()>0){
				if(reportType.equalsIgnoreCase("excel")){
					fileName=fileName+".xlsx";
					ExcelHandler excelHandler=new ExcelHandler();
					excelHandler.generateExcelReport(title,subTitle,map,list,folderPath,fileName);
				}else
				{
					fileName=fileName+".pdf";
					PdfHandler pdfHandler = new PdfHandler(request.getServletContext().getRealPath("/"), SessionUtils.getTenjinSession(request).getUser().getFullName());
					pdfHandler.generateEntityReport(title,subTitle,map,list,folderPath,fileName);
				}
				json.addProperty("status", "SUCCESS");
				json.addProperty("message","");
				json.addProperty("path", "Downloads/" + fileName);
				}else{
					json.addProperty("status", "FAILURE");
					json.addProperty("message",	"No data available for this report");
				}
			} catch(Exception e) {
				json.addProperty("status","FAILURE");
				json.addProperty("message","An internal error occurred. Please try again later");
				logger.error("Could not create excel due to an internal error",e);
			} finally{
				retData = json.toString();
				response.getWriter().write(retData);
			}
		}
		/*Added by Pushpa for TENJINCG-1070,1067 starts */
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		/*added by shruthi for VAPT FIX to Servlets starts*/
		String operation = request.getParameter("operation");
		Authorizer authorizer=new Authorizer();
		if(operation.equalsIgnoreCase("") || operation.equalsIgnoreCase("new") ||  operation.equalsIgnoreCase("update")) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		/*added by shruthi for VAPT FIX to Servlets ends*/
		SessionUtils.loadScreenStateToRequest(request);
		TenjinSession tjnSession = SessionUtils.getTenjinSession(request);
		String cUser = tjnSession.getUser().getId();
		logger.info("entered doPost()");
		/*Added by Padmavathi for T251IT-11 starts*/
		String updateStatus = Utilities.trim(request.getParameter("updateStatus"));
		/*Added by Padmavathi for T251IT-11 ends*/
		
		String userId = request.getParameter("id");
		User user = (User) SessionUtils.mapAttributes(request, new User().getClass());
		String dispatchUrl = "";
		UserHandler handler = new UserHandler();
		/*Modified by Padmavathi for T251IT-11 starts*/
		if (updateStatus.equalsIgnoreCase("true")) {
			 String status = request.getParameter("status");
			user.setId(userId);
			/*Modified by Preeti for TENJINCG-507 starts*/
			/*user.setBlockedStatus(status);*/
			user.setEnabledStatus(status);
			/*if (user.getBlockedStatus().equals("Enable"))*/ 
			if (user.getEnabledStatus().equals("Enable"))
			/*Modified by Preeti for TENJINCG-507 ends*/
			{
				try {
					handler.unblockUser(user.getId(), cUser);
					user = handler.getUser(userId);
					SessionUtils.setScreenState("success", "user.enabled.success",userId, request);
					/*commented by Padmavathi for T251IT-11 starts*/
					/*SessionUtils.loadScreenStateToRequest(request);*/
					/*commented by Padmavathi for T251IT-11 ends*/
					response.sendRedirect("TenjinUserServlet?t=view&key=" + user.getId());
				} catch (RecordNotFoundException e) {
					logger.error(e.getMessage());
					dispatchUrl = "v2/user_view.jsp";
					SessionUtils.setScreenState("error", e.getMessage(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("user", user);
				}catch (DatabaseException e) {
					logger.error(e.getMessage());
					dispatchUrl = "v2/user_view.jsp";
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("user", user);
				} catch (ResourceConflictException e) {
					logger.error(e.getMessage());
					dispatchUrl = "v2/user_view.jsp";
					SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("user", user);
				}
			} else {
				try {
					handler.blockUser(user.getId(), cUser);
					user = handler.getUser(userId);
					SessionUtils.setScreenState("success", "user.disable.success", user.getId(), request);
					/*commented by Padmavathi for T251IT-11 starts*/
					/*SessionUtils.loadScreenStateToRequest(request);*/
					/*commented by Padmavathi for T251IT-11 ends*/
					response.sendRedirect("TenjinUserServlet?t=view&key=" + user.getId());

				} catch (DatabaseException e) {
					logger.error(e.getMessage());
					dispatchUrl = "v2/user_view.jsp";
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("user", user);
				} catch (ResourceConflictException e) {
					logger.error(e.getMessage());
					dispatchUrl = "v2/user_view.jsp";
					SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.setAttribute("user", user);
				}
			}
		}else{
				if (operation.equalsIgnoreCase("new")) {
					logger.info("Request to register a new user");
					try {
						/*Modified by Pushpa for TENJINCG-1035 starts*/
						handler.persist(user,cUser);
						/*Modified by Pushpa for TENJINCG-1035 ends*/
						SessionUtils.setScreenState("success", "user.create.success", user.getId(), request);
						response.sendRedirect("TenjinUserServlet?t=view&key=" + user.getId());
					} catch (DatabaseException e) {
						SessionUtils.setScreenState("error", "generic.db.error", request);
						SessionUtils.loadScreenStateToRequest(request);
						request.setAttribute("user", user);
						request.getRequestDispatcher("v2/user_new.jsp").forward(request, response);
					} catch (RequestValidationException  e) {
						dispatchUrl = "v2/user_new.jsp";
						SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
						SessionUtils.loadScreenStateToRequest(request);
						request.setAttribute("user", user);
						request.getRequestDispatcher(dispatchUrl).forward(request, response);
					} catch (TenjinServletException e) {
						logger.error("");
					} catch (MaxActiveUsersException e) {
						dispatchUrl = "v2/user_new.jsp";
						SessionUtils.setScreenState("error", e.getMessage(),request);
						SessionUtils.loadScreenStateToRequest(request);
						request.setAttribute("user", user);
						request.getRequestDispatcher(dispatchUrl).forward(request, response);
					}

				} else if (operation.equalsIgnoreCase("update")) {
					logger.info("Request to update user.");
					try {
						/*Changed by Pushpalatha for TENJINCG-648 starts*/
						handler.update(user,cUser);
					/*Changed by Pushpalatha for TENJINCG-648 ends*/
						SessionUtils.setScreenState("success", "user.update.success", user.getId(), request);
						response.sendRedirect("TenjinUserServlet?t=view&key=" + user.getId());
					} catch (DatabaseException e) {
						logger.error(e.getMessage());
						dispatchUrl = "v2/user_view.jsp";
						SessionUtils.setScreenState("error", "generic.db.error", request);
						SessionUtils.loadScreenStateToRequest(request);
						request.setAttribute("user", user);
						request.getRequestDispatcher(dispatchUrl).forward(request, response);
					} catch (RequestValidationException e) {
						logger.error(e.getMessage());
						dispatchUrl = "v2/user_view.jsp";
						SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
						SessionUtils.loadScreenStateToRequest(request);
						request.setAttribute("user", user);
						request.getRequestDispatcher(dispatchUrl).forward(request, response);
					}
				} 
		
			}
		/*Modified by Padmavathi for T251IT-11 ends*/
	}

}
