/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DefectServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
* 08-Dec-2016			Sriram Sridharan		Changed by Sriram to implement DTT User Mapping functionality
* 13-Dec-2016			Manish					DTT Mapping Screen
* 13-Dec-2016			Manish					Defect TEN-46(1. added new txn-check_dtt_user_name
* 															  2. changed in txn-DTT_TENJIN_USER_MAPPING)
* 22-12-2016			SRIRAM					Defect#TEN-158
* 10-Jan-2017			Manish					TENJINCG-5
* 01-Aug-2017		  	Manish					TENJINCG-74,75
* 30-08-2017            Manish	                T25IT-284
* 09-Oct-2017		  	Manish					using user credentials for sync instead of instance credentials
* 16-May-2018			Pushpalatha				TENJINCG-557
* 15-06-2018            Padmavathi              T251IT-18
* 18-06-2018            Padmavathi              T251IT-48
* 03-07-2018			Preeti					changes for defect priority and severity max length
* 31-12-2018			Ashiki					TJN252-49
* 03-01-2019			Ashiki					TJN252-63 
* 25-02-2019			Ashiki					TENJINCG-985
* 15-03-2019			Ashiki				 	TENJINCG-986
* 10-04-2019            Padmavathi              TNJN27-70
* 02-05-2019			Roshni					TENJINCG-1046
* 06-05-2019            Padmavathi              TENJINCG-1050
* 23-05-2019			Ashiki					V2.8-30,V2.8-29
* 03-06-2019			Prem					V2.8-89
* 06-06-2019			Ashiki					V2.8-110
* 07-06-2019			Ashiki					V2.8-118
* 04-07-2019			Ashiki					TV2.8R2-27
* 30-09-2019			Preeti					For defect posting in JIRA
* 04-12-2019			Preeti					TENJINCG-1174
* 05-06-2020			Ashiki					Tenj210-64
* 25-09-2020            shruthi                  TENJINCG-1224
* 19-11-2020            Priyanka                TENJINCG-1231
* 08-12-2020			Pushpa					TENJINCG-1220
* 11-12-2020			Pushpa					TENJINCG-1221
* 15-12-2020			Pushpa					TENJINCG-1222
* 16-12-2020			Prem					TENJINCG-1236
*/


/*
 * Added File by sahana for Requirement TJN_24_06
 */
package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.CoreDefectHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DefectHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.db.ResultsHelper;
import com.ycs.tenjin.db.RunDefectHelper;
import com.ycs.tenjin.db.UserHelperNew;
import com.ycs.tenjin.defect.Attachment;
import com.ycs.tenjin.defect.CoreDefectProcessor;
import com.ycs.tenjin.defect.Defect;
import com.ycs.tenjin.defect.DefectFactory;
import com.ycs.tenjin.defect.DefectManagementInstance;
import com.ycs.tenjin.defect.DefectManager;
import com.ycs.tenjin.defect.EDMException;
import com.ycs.tenjin.defect.UnreachableEDMException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.handler.ProjectMailHandler;
import com.ycs.tenjin.handler.TenjinMailHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class DefectServlet
 */
//@WebServlet("/DefectServlet")
public class DefectServlet extends HttpServlet {
	private static final Logger logger = LoggerFactory
			.getLogger(DefectServlet.class);
	private static final long serialVersionUID = 1L;
	
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DefectServlet() {
        super();
        
    }
    
    public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		 

        @Override
        public boolean checkAccess(HttpServletRequest request) {
        	TenjinSession tjnSession = (TenjinSession) request.getSession()
                    .getAttribute("TJN_SESSION");
            if (request.getMethod().equalsIgnoreCase("GET")) {
                String txn = request.getParameter("param");
                if(txn.equalsIgnoreCase("FETCH_ALL")||txn.equalsIgnoreCase("NEW_DEF") || txn.equalsIgnoreCase("deleteall") || txn.equalsIgnoreCase("delete") ||txn.equalsIgnoreCase("new_dtt_user")||txn.equalsIgnoreCase("new")) {
                    if (new AdminAuthorizationRuleImpl().checkAccess(request)) {
                        return true;
                    }
                }else if( txn.equalsIgnoreCase("check_dtt_user_name") ) {
                    
                    if(request.getParameter("userId").equalsIgnoreCase(tjnSession.getUser().getId())){
                        return true;
                    }
                }else if(txn.equalsIgnoreCase("DTT_TENJIN_USER_MAPPING")) {
                	String jstring = request.getParameter("paramval");
					try {
						JSONObject json = new JSONObject(jstring);
						String userId = json.getString("userId");
							if (Utilities.trim(userId).equalsIgnoreCase(tjnSession.getUser().getId())) {
								return true;
							}
							return false;
					} catch (JSONException e) {
							logger.error("Error during mapping TM user with Tenjin user", e);
					} 
                }else {
                	return true;
                }
            } else if (request.getMethod().equalsIgnoreCase("POST")) {
                if (new AdminAuthorizationRuleImpl().checkAccess(request)) {
                    return true;
                }
            }else {
            	return true;
            }
            return false;
        }
        
    };

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		TenjinSession tjnSession = (TenjinSession) request.getSession()
				.getAttribute("TJN_SESSION");
		User user = tjnSession.getUser();
		/*Added by Pushpa for TENJINCG-557 starts*/
		Project project=tjnSession.getProject();
		/*Added by Pushpa for TENJINCG-557 ends*/
		String txn = request.getParameter("param");
		
		 Authorizer authorizer=new Authorizer();
			if(txn.equalsIgnoreCase("NEW_DEF") || txn.equalsIgnoreCase("VIEW_DEF") || txn.equalsIgnoreCase("edit_def") || txn.equalsIgnoreCase("FETCH_ALL") || txn.equalsIgnoreCase("new") || txn.equalsIgnoreCase("deleteall") || txn.equalsIgnoreCase("delete")) {
				if(!authorizer.hasAccess(request)){
					response.sendRedirect("noAccess.jsp");
					return;
				}
			}
		
		if (txn.equalsIgnoreCase("FETCH_ALL")) {
			logger.debug("Fetching All Defect Management list");
			ArrayList<DefectManagementInstance> defectList = new ArrayList<DefectManagementInstance>();
			Map<String, Object> map = new HashMap<String, Object>();

			DefectHelper dfHelper = null;

			dfHelper = new DefectHelper();

			if (dfHelper != null) {
				try{
					defectList = dfHelper.hydrateAllDefects();
				}
				catch (DatabaseException e) {
					
					logger.error("Error ", e);
				}
				map.put("DEF_STATUS", "SUCCESS");
				/*Added by Ashiki for V2.8-118 starts*/
				String message = request.getParameter("message");
				if(message!=null) {
					map.put("MESSAGE", message);
				}else {
					map.put("MESSAGE", "");	
				}
				/*Added by Ashiki for V2.8-118 ends*/
				map.put("DEF_LIST", defectList);
			}

			request.getSession().setAttribute("SCR_MAP", map);
			
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("defect_list.jsp");
			dispatcher.forward(request, response);
			
		
		}/*added by manish for filter project defects on 06-Dec-2016*/
		
		else if(txn.equalsIgnoreCase("fetch_project_defects")){
			int projectId = Integer.parseInt(request.getParameter("pid"));
			//Added by prem  for TENJINCG-1231 while testing 2.11 starts
			try {
				project=new ProjectHandler().hydrateProject(projectId);
			} catch (DatabaseException e3) {
				
				e3.printStackTrace();
			}
			//Added by prem  for TENJINCG-1231 while testing 2.11 Ends
			
			/*changed by manish for req TENJINCG-5 on 10-Jan-2016 starts*/
			String callback = request.getParameter("callback");
			/*changed by manish for req TENJINCG-5 on 10-Jan-2016 ends*/
			List<Defect> defects = new ArrayList<Defect>();
			Map<String, Object> map = new HashMap<String, Object>();
			CoreDefectHelper helper = new CoreDefectHelper();
			try {
				defects = helper.hydrateProjectDefects(projectId);
				List<Aut> auts = new AutHelper().hydrateAllAut();
				if(auts == null) auts = new ArrayList<Aut>();
				logger.debug("Got {} AUTs", auts.size());
				map.put("STATUS", "SUCCESS");
				/*changed by manish for req TENJINCG-5 on 10-Jan-2016 starts*/
				/*map.put("message", "");*/
				if(callback!=null && callback.equalsIgnoreCase("deletion")){
					/*Modified by Ashiki for V2.8-110 starts*/
					map.put("message","Selected defect(s) deleted successfully");
					/*Modified by Ashiki for V2.8-110 ends*/
				}
				/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
				else if(callback!=null &&callback.equalsIgnoreCase("fetch_single_defect")){
					if(request.getParameter("message")!=null && request.getParameter("message")!=""){
						map.put("message", request.getParameter("message"));
					}else{
						/*Changed By Ashiki for TENJINCG-985 starts*/
						/*map.put("message", "Defect does not exist, Either someone has deleted the defect or the DTT project.");*/
						map.put("message", "Defect does not exist, Either someone has deleted the defect or the Defect Mangement project.");
						/*Changed By Ashiki for TENJINCG-985 ends*/
					}
					
					map.put("STATUS", "ERROR");
				}
				/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
				else{
					map.put("message", "");
				}
				map.put("defectsCallback", callback);
				/*changed by manish for req TENJINCG-5 on 10-Jan-2016 ends*/
				map.put("defects", defects);
				map.put("auts", auts);
			} catch (DatabaseException e) {
				map.put("STATUS", "ERROR");
				map.put("message", "An internal error occured, please contact Tenjin support.");
				logger.error("Error---------> during hydrating defects for project{}",projectId,e);
			}
			
			request.getSession().removeAttribute("DEFECT_SCR_MAP");
			request.getSession().setAttribute("DEFECT_SCR_MAP", map);
			/*   Added by Priyanka for TENJINCG-1231 starts */
			
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			
			if(project.getEndDate()==null){
				request.setAttribute("edate","NA");
			}else{
			String edate=sdf.format(project.getEndDate());
			
			request.setAttribute("edate",edate);
			}
			
			  request.getRequestDispatcher("projectdefects.jsp").forward(request,response);
		
			/*   Added by Priyanka for TENJINCG-1231 starts */
			//response.sendRedirect("projectdefects.jsp");
			
		}else if(txn.equalsIgnoreCase("filter_project_defects")){
			String recordId = request.getParameter("recordId");
			String functionCode = request.getParameter("functionCode");
			String reporter = request.getParameter("reporter");
			String status = request.getParameter("status");
			String testCaseId = request.getParameter("txtcase");
			String testStepId= request.getParameter("txtstep");
			String appId = request.getParameter("appId");
			String posted = request.getParameter("posted");
			String projectId = request.getParameter("projectId");
			
			
			
			Map<String, Object> map = new HashMap<String, Object>();
			List<Defect> defects = new ArrayList<Defect>();
			JSONObject json = new JSONObject();
			CoreDefectHelper helper = new CoreDefectHelper();
			try {
				
				defects = helper.hydratrFilterProjectDefects(Integer.parseInt(recordId),testCaseId,testStepId,Integer.parseInt(projectId),appId,functionCode,reporter,status,posted);
				logger.info("hydrated defects for project{}------>{}",Integer.parseInt(projectId),defects.size());
				
				List<Aut> auts = new AutHelper().hydrateAllAut();
				if(auts == null) auts = new ArrayList<Aut>();
				logger.debug("Got {} AUTs", auts.size());
				map.put("auts", auts);
				
				map.put("STATUS", "SUCCESS");
				map.put("message", "");
				map.put("defects", defects);
				json.put("status", "SUCCESS");
			} catch (NumberFormatException e) {
				map.put("STATUS", "ERROR");
				map.put("message", "An Internal error occured, Please contact Tenjin support.");
				logger.error("Error-------------> during filtering defects for project{}",projectId,e);
			} catch (DatabaseException e) {
				map.put("STATUS", "ERROR");
				map.put("message", "An Internal error occured, Please contact Tenjin support.");
				logger.error("Error-------------> during filtering defects for project{}",projectId,e);
			} catch (JSONException e) {
				map.put("STATUS", "ERROR");
				map.put("message", "An Internal error occured, Please contact Tenjin support.");
			}
			
			request.getSession().removeAttribute("DEFECT_SCR_MAP");
			request.getSession().setAttribute("DEFECT_SCR_MAP", map);
			response.getWriter().write(json.toString());
			
		}
		/*added by manish for filter project defects on 06-Dec-2016 ends*/
		
		
		else if (txn.equalsIgnoreCase("NEW_DEF")) {
			
				logger.debug("Creating New Defect list");
				
				String jsonString = request.getParameter("json");
				logger.debug("JSON String: " + jsonString);
				
				String retData = "";
				try {
					
					JSONObject json = new JSONObject(jsonString);
					
					DefectManagementInstance dm = new DefectManagementInstance();

					
					dm.setName(json.getString("name"));
					dm.setTool(json.getString("tool"));
					dm.setURL(json.getString("url"));
					dm.setAdminId(json.getString("admin"));
					/*Modified by Preeti for defect posting in JIRA starts*/
					 /*Modified by paneendra for VAPT fix starts*/
					/*dm.setPassword(new Crypto().encrypt(json.getString("password")));*/
					//dm.setPassword(new Crypto().encrypt(request.getParameter("password")));
					String dmPwd=new CryptoUtilities().encrypt(request.getParameter("password"));
					dm.setPassword(dmPwd);
					 /*Modified by paneendra for VAPT fix ends*/
					/*Modified by Preeti for defect posting in JIRA ends*/
					dm.setSeverity(json.getString("Severity"));
					dm.setPriority(json.getString("Priority"));
					/*Added by Pushpalatha for TENJINCG-1220 starts*/
					dm.setOrganization(json.getString("Organization"));
					/*Added by Pushpalatha for TENJINCG-1220 ends*/
					DefectHelper helper = new DefectHelper();
					int checkName=helper.checkName(dm.getName());
					
					/*Added by Preeti for defect priority and severity max length starts*/
					int flag=0;
					String str=dm.getPriority();
					String [] prty={};
					prty=str.split(",");

					for(int i=0;i<prty.length;i++){
						if(prty[i].length()>20){
							JSONObject r = new JSONObject();
							r.put("status", "ERROR");
							r.put("message", "Priority value "+prty[i]+" should not exceed its max length 20.");
							retData = r.toString();
							flag=1;
						}
					}
					String str1=dm.getSeverity();
					String [] svrty={};
					svrty=str1.split(",");

					for(int i=0;i<svrty.length;i++){
						if(svrty[i].length()>20){
							JSONObject r = new JSONObject();
							r.put("status", "ERROR");
							r.put("message", "Severity value "+svrty[i]+" should not exceed its max length 20.");
							retData = r.toString();
							flag=1;
						}
					}
					/*Added by Preeti for defect priority and severity max length ends*/
					if(checkName==0 ){
						if(flag!=1){
							dm = helper.persistDefect(dm);
							JSONObject r1 = new JSONObject();
							r1.put("status","success");
							/*Changed by Ashiki for V2.8-29 starts*/
							/*r1.put("message","Added Defect Successfully");*/
							r1.put("message","Defect Management Instance created Successfully");
							/*Changed by Ashiki for V2.8-29 ends*/
							retData = r1.toString();
							}
						}
				else{
						JSONObject r = new JSONObject();
						r.put("status", "ERROR");
						r.put("message", " User Already Exist AS: "+dm.getName());
						retData = r.toString();
					
				} 
					
									
			}catch (Exception e) {
				
				logger.error("Error ", e);
				JSONObject r = new JSONObject();
						try {
							r.put("status", "ERROR");
							/*Changed by Ashiki for V2.8-29 starts*/
						    /*r.put("message",
								"Could not create defect due to an internal error");*/
							r.put("message","Could not create Defect Management Instance due to an internal error");
						    /*Changed by Ashiki for V2.8-29 ends*/
						retData = r.toString();
					
						} catch (JSONException e1) {
							retData = "An internal error occurred. Please contact your System Administrator ";
							
						}
					}
				response.getWriter().write(retData);
			
		}
		else if (txn.equalsIgnoreCase("CHECK_NAME")) {
			
			JSONObject resJSON = new JSONObject();
			String name = request.getParameter("paramval");
			String retData = "";
			try {
				try {
					DefectHelper helper = new DefectHelper();
					int i = helper.checkName(name);
					if (i >0) {
						resJSON.put("status", "ERROR");
						resJSON.put("message", "The name you entered is already in use. Please use a different name");
					}
				} catch (Exception e) {
					resJSON.put("status", "ERROR");
					resJSON.put("message",
							"Could not update details due to an internal error");
				} finally {
					retData = resJSON.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator ";
			}
			response.getWriter().write(retData);
		}
		else if (txn.equalsIgnoreCase("VIEW_DEF")) {
			logger.debug("View Defect Management Details");
			
			String name = request.getParameter("paramval");

			Map<String, Object> map = new HashMap<String, Object>();
			try {
				DefectHelper helper = new DefectHelper();
				DefectManagementInstance dm1 = helper.hydrateDefects(name);
				/* Added By Ashiki TENJINCG-986 starts */
				ArrayList<DefectManagementInstance> dm2=helper.hydrateDefectManagementProjects(name);
				/* Added By Ashiki TENJINCG-986 ends */
										
				if (dm1 != null) {
					map.put("STATUS","");
					map.put("MESSAGE","");
					map.put("DEF_BEAN", dm1);
					/* Added By Ashiki TENJINCG-986 starts */
					map.put("DEF_PRJ", dm2);
					/* Added By Ashiki TENJINCG-986 ends */
				} else {
					map.put("STATUS", "FAILURE");
					map.put("MESSAGE", "dmedule not found");
				}
			} catch (Exception e) {
				map.put("STATUS", "ERROR");
				map.put("MESSAGE",
						"An Internal Error occurred. Please contact your System Administrator");
			} finally {
				request.getSession().removeAttribute("SCR_MAP");
				request.getSession().setAttribute("SCR_MAP", map);
			}
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("defect_view.jsp");
			dispatcher.forward(request, response);
		} 
		else if (txn.equalsIgnoreCase("edit_def")) {
			logger.debug("Editing Defect Management Details");
			String json = request.getParameter("json");
			String json1 = request.getParameter("json1");
			JSONObject resJSON = new JSONObject();
			String retData = "";
			try {
				
				try {
					JSONObject j = new JSONObject(json);
					DefectManagementInstance dm = new DefectManagementInstance();
					Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
					DefectManagementInstance dm2 = (DefectManagementInstance)map.get("DEF_BEAN");
					
					String exName=dm2.getName();
					
					dm.setName(j.getString("name"));
					dm.setTool(j.getString("tool"));
					dm.setURL(j.getString("url"));
					dm.setAdminId(j.getString("admin"));
					/*Modified by Preeti for defect posting in JIRA starts*/
					/*dm.setPassword(new Crypto().encrypt(j.getString("password")));*/
					/*Modified by Preeti for defect posting in JIRA ends*/
					dm.setSeverity(j.getString("Severity"));
					dm.setPriority(j.getString("Priority"));
					/*Added by Pushpalatha for TENJINCG-1220 starts*/
					/*Added by Prem for Tenj211-24 starts*/
					if(j.getString("name").equalsIgnoreCase("github")) 
						dm.setOrganization(j.getString("Organization"));	
					/*Added by Prem for Tenj211-24 starts*/
					/*Added by Pushpalatha for TENJINCG-1220 ends*/
					/*Modified by Preeti for defect posting in JIRA starts*/
					/*if (j.getString("password").equals(j1.getString("password1")))
						dm.setPassword(j.getString("password"));
					else
						dm.setPassword(new Crypto().encrypt(j.getString("password")));*/
					if (request.getParameter("password").equals(request.getParameter("password1")))
						dm.setPassword(request.getParameter("password"));
					else
						 /*Modified by paneendra for VAPT fix starts*/
						//dm.setPassword(new Crypto().encrypt(request.getParameter("password")));
					      dm.setPassword(new CryptoUtilities().encrypt(request.getParameter("password")));
					     /*Modified by paneendra for VAPT fix ends*/
					/*Modified by Preeti for defect posting in JIRA ends*/	
					/*Added by Preeti for defect priority and severity max length starts*/
					int flag=0;
					String str=dm.getPriority();
					String [] prty={};
					prty=str.split(",");

					for(int i=0;i<prty.length;i++){
						if(prty[i].length()>20){
							resJSON.put("status", "ERROR");
							resJSON.put("message", "Priority value "+prty[i]+" should not exceed its max length 20.");
							retData = resJSON.toString();
							flag=1;
						}
					}
					String str1=dm.getSeverity();
					String [] svrty={};
					svrty=str1.split(",");

					for(int i=0;i<svrty.length;i++){
						if(svrty[i].length()>20){
							resJSON.put("status", "ERROR");
							resJSON.put("message", "Severity value "+svrty[i]+" should not exceed its max length 20.");
							retData = resJSON.toString();
							flag=1;
						}
					}
					/*Added by Preeti for defect priority and severity max length ends*/
					if(flag!=1){
						new DefectHelper().updateDefect(dm,exName);
						resJSON.put("status","success");
						/*Modified by Prem for V2.8-89 Starts*/
						resJSON.put("message"," Defect Management Instance Updated Successfully");
						/*Modified by Prem for V2.8-89 Ends*/
					}
					} catch (Exception e) {
						logger.error("Error ", e);
						resJSON.put("status", "ERROR");
						resJSON.put("message",
								"Could not update defect due to an internal error ");
					} finally {
						retData = resJSON.toString();
					}
				} catch (Exception e) {
					retData = "An internal error occurred. Please contact your System Administrator";
				}

			response.getWriter().write(retData);
				
		}
		else if (txn.equalsIgnoreCase("deleteall")) {
			logger.debug("Delete All Defect Management List");
			DefectHelper helper = null;
			String names = request.getParameter("paramval");
					
			String[] record = names.split(",");
			
			JSONObject retJson = new JSONObject();
			String retData = "";
			try {

				try {
					helper = new DefectHelper();
					helper.clearAll(record);
					
					retJson.put("status", "SUCCESS");
					/*Changed by Ashiki for V2.8-30,TV2.8R2-27 starts*/
					retJson.put("message", "Defect Management Instances deleted successfully");
					/*Changed by Ashiki for V2.8-30,TV2.8R2-27 ends*/
				} catch (DatabaseException e) {
				
					retJson.put("status", "ERROR");
					/*Changed by Ashiki for V2.8-30 starts*/
					retJson.put("message", "Could not delete Defect Management Instances");
					/*Changed by Ashiki for V2.8-30 ends*/
				} finally {
					retData = retJson.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}
		else if (txn.equalsIgnoreCase("delete")) {
			logger.debug("Deleting Defect Management Record");
			String name = request.getParameter("paramval");
			
			
			JSONObject retJson = new JSONObject();
			String retData = "";
			try {

				try {
					DefectHelper helper = new DefectHelper();
					helper.clear(name);
					
					retJson.put("status", "SUCCESS");
					/*Changed by Ashiki for V2.8-30,TV2.8R2-27 starts*/
					retJson.put("message", "Defect Mangement Instance deleted successfully");
					/*Changed by Ashiki for V2.8-30,TV2.8R2-27 ends*/

				} catch (DatabaseException e) {
					retJson.put("status", "ERROR");
					/*Changed by Ashiki for V2.8-30 starts*/
					retJson.put("message", "Could not delete Defect Mangement Instance");
					/*Changed by Ashiki for V2.8-30 ends*/
				} finally {
					retData = retJson.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}
		else  if(txn.equalsIgnoreCase("DTTtool")){
			
			String retData;
			try {

				JSONObject json = new JSONObject();
				try {
					List<String> rc1 = DefectFactory.getAvailableTools();
					
					JSONArray jArray = new JSONArray();
					for (String  reg: rc1) {
						JSONObject j = new JSONObject();
						j.put("dttname", reg);
						jArray.put(j);
					}

					json.put("status", "SUCCESS");
					json.put("message", "");
					json.put("tools", jArray);
				} catch (Exception e) {
					logger.error("An error occurred while processing tools", e);
					json.put("status", "ERROR");
					json.put("message",
							"An internal error occurred. Please try again.");

				} finally {
					retData = json.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}

		/*added by manish for DTT User Credentials on 17-Nov-2016*/
		
		/*changed by manish for defect TEN-46 on 13-12-2016 starts*/
		else if (txn.equalsIgnoreCase("DTT_TENJIN_USER_MAPPING")) {

			String jstring = request.getParameter("paramval");
			JSONObject rJson = new JSONObject();
			String instance = "";
			try {
				JSONObject json = new JSONObject(jstring);
				DefectManagementInstance dm = new DefectManagementInstance();
				DefectHelper dmhHelper = new DefectHelper();
				/*Changed by Ashiki for TJN252-63 starts*/
				/*String pwd = new Crypto().encrypt(json.getString("loginPwd"));*/
				/*Modified by paneendra for VAPT FIX starts*/
				/*String pwd = new Crypto().encrypt(request.getParameter("loginPwd"));*/
				String pwd = new CryptoUtilities().encrypt(request.getParameter("loginPwd"));
				/*Modified by paneendra for VAPT FIX ends*/
				/*Changed by Ashiki for TJN252-63 ends*/
				dm.setAdminId(json.getString("loginId"));
				dm.setInstance(json.getString("instance"));
				instance = json.getString("instance");
				dm.setTjnUserId(json.getString("userId"));//json.getString("userId")
				dm.setDefToolPwd(pwd);/* new Crypto().encrypt(pwd) */
				

				
				CoreDefectHelper helper = new CoreDefectHelper();
				helper.validateUserDttLoginName(instance, json.getString("userId"));
				dmhHelper.createDTTUserMappingData(dm);
				rJson.put("status", "SUCCESS");
				/*Changed By Ashiki for TENJINCG-985 starts*/
				/*rJson.put("message", "DTT User Created Successfully");*/
				rJson.put("message", "Defect Management User Created Successfully");
				/*Changed By Ashiki for TENJINCG-985 ends*/
			}catch(DatabaseException e){
				try {
					logger.debug("Duplicate record found");
					rJson.put("status", "ERROR");
					rJson.put("message", "You have already specified credentials for "+instance+". To create a new one, please delete the existing credential and try again.");
				} catch (JSONException e1) {
					
				}
			} catch (JSONException e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An Internal error occured please contact Administrator");
					logger.error(
							"Error during mapping Defect Management user with Tenjin user", e);
				} catch (JSONException e1) {
				}
			}catch (Exception e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An Internal error occured please contact Administrator");
					logger.error(
							"Error during mapping Defect Management user with Tenjin user", e);
				} catch (JSONException e1) {
				}
			}

			response.getWriter().write(rJson.toString());

		} 
		/*changed by manish for defect TEN-46 on 13-12-2016 ends*/
		else if (txn.equalsIgnoreCase("delete_DTT_user")) {
			String uids = request.getParameter("userids");
			String instances = request.getParameter("dttInstances");
			String[] userids = uids.split(",");
			String[] dttInstances = instances.split(",");
			JSONObject retJson = new JSONObject();
			String retData = "";
			DefectHelper dmHelper = new DefectHelper();
			try {
				try {
					dmHelper.deleteDTTUser(userids, dttInstances);
					retJson.put("status", "SUCCESS");
					retJson.put("message", "Defect Management user(s)  deleted successfully");
				} catch (Exception e) {
					retJson.put("status", "ERROR");
					retJson.put("message", "Could not delete Defect Management user(s)");
				} finally {
					retData = retJson.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact System Administrator";
				logger.error("error during deleting Defect Management user");
			}
			response.getWriter().write(retData);
		}
		else if (txn.equalsIgnoreCase("fetch_def_user_mapping_data")) {
			List<DefectManagementInstance> dms = new ArrayList<DefectManagementInstance>();
			DefectHelper dmHelper = new DefectHelper();
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				dms = dmHelper.fetchTenjinDTTUserMapping(user.getId());
				map.put("USER_DEFECT_MAPPINGS", dms);
			} catch (Exception e) {
				
				logger.error("Error ", e);
			}
			/*Added by Ashiki for V2.8-118 starts*/
			String message = request.getParameter("message");
			if(message!=null) {
				map.put("MESSAGE", message);
			}else {
				map.put("MESSAGE", "");
			}
			/*Added by Ashiki for V2.8-118 ends*/
			request.getSession().setAttribute("SCR_MAP", map);
			response.sendRedirect("userdefectlist.jsp");

		}
		else if(txn.equalsIgnoreCase("fetch_defect_user")){
			String userId = user.getId();
			String instance = request.getParameter("instance");
			DefectHelper dmHelper = new DefectHelper();
			DefectManagementInstance dmInstance = new DefectManagementInstance();
			Map<String,Object> map = new HashMap<String,Object>();
			/*Added by Padmavathi for T251IT-18 starts*/
			List<String> instances=null;
			/*Added by Padmavathi for T251IT-18 ends*/
			try {
				dmInstance=dmHelper.fetchUserDTTDetails(userId,instance);
				/*Added by Padmavathi for T251IT-18 starts*/
				instances = dmHelper.fetchAllInstances();
				map.put("instances", instances);
				/*Added by Padmavathi for T251IT-18 ends*/
				map.put("dmInstance", dmInstance);
			} catch (Exception e) {
				
				logger.error("Error ", e);
			}
			request.getSession().setAttribute("SCR_MAP", map);
			response.sendRedirect("edituserdefectdetails.jsp");
		}
		
		else if(txn.equalsIgnoreCase("update_user_defect_mapping")){
			String jString = request.getParameter("json");
			DefectManagementInstance dmInstance = new DefectManagementInstance();
			DefectHelper dmHelper = new DefectHelper();
			String retData = "";
			JSONObject retJson = new JSONObject();
			try {
				JSONObject json = new JSONObject(jString);
				dmInstance.setTjnUserId(user.getId());
				dmInstance.setInstance(json.getString("instance"));
				dmInstance.setAdminId(json.getString("dttUser"));
				/*Changed by Ashiki for TJN252-63 starts*/
				/*dmInstance.setDefToolPwd(json.getString("dttUserPwd"));*/
				dmInstance.setDefToolPwd(request.getParameter("dttUserPwd"));
				/*Changed by Ashiki for TJN252-63 ends*/
				/*Modified by Padmavathi for T251IT-18 starts*/
			    String oldInstance=json.getString("oldInstance");
			    /*dmHelper.updateDTTUserMappingData(dmInstance);*/
				dmHelper.updateDTTUserMappingData(dmInstance,oldInstance);
				/*Modified by Padmavathi for T251IT-18 ends*/
				retJson.put("status", "SUCCESS");
				/*Modified by Prem for V2.8-89 Starts*/
				/*retJson.put("message", "Records Updated Successfully");*/
				retJson.put("message", " Defect Management User Details Updated Successfully");
				/*Modified by Prem for V2.8-89 ends*/
			} catch (Exception e) {
				try {
					retJson.put("status", "ERROR");
					retJson.put("message", "Could not update user records");
				} catch (JSONException e1) {
					retData = "An internal error occurred. Please contact System Administrator";
				}
				
				logger.error("Error ", e);
			}finally {
				retData = retJson.toString();
			}
			response.getWriter().write(retData);
		}else if (txn.equalsIgnoreCase("new_dtt_user")) {
			List<String> instances = new ArrayList<String>();
			Map<String, Object> map = new HashMap<String, Object>();
			DefectHelper dmHelper = new DefectHelper();
			try {
				instances = dmHelper.fetchAllInstances();
				map.put("instances", instances);
			} catch (Exception e) {
				
				logger.error("Error ", e);
			}
			request.getSession().setAttribute("SCR_MAP", map);
			/*changed by manish for bug T25IT-284 on 30-Aug-2017 starts*/
			/*response.sendRedirect("userdefectdetails.jsp");*/
			/*changed by manish for bug T25IT-284 on 30-Aug-2017 ends*/

		} else if (txn.equalsIgnoreCase("init_new_defect")) {
			request.getSession().removeAttribute("DEFECT_SCR_MAP");
			Map<String, Object> map = new HashMap<String, Object>();
			logger.info("Preparing to load New Defect Screen");
			try {
				
				logger.debug("Getting all runs in project [{}]", tjnSession.getProject().getId());
				List<TestRun> runs = new ResultsHelper().hydrateBasicRunInformation(tjnSession.getProject().getId(), BridgeProcess.EXECUTE);
				if(runs == null) runs = new ArrayList<TestRun>();
				TestRun run = new TestRun();
				run.setId(0);
				TestSet set = new TestSet();
				set.setId(0);
				set.setName("General");
				run.setTestSet(set);
				runs.add(0, run);
				logger.debug("Got {} runs", runs.size());
				
				logger.debug("Getting all available AUTs");
				/*Changed by Pushpalatha for TENJINCG-557 starts*/
				/*List<Aut> auts = new AutHelper().hydrateAllAut();*/
				List<Aut> auts = new ProjectHelper().hydrateProjectAuts(project.getId());
				/*Changed by Pushpalatha for TENJINCG-557 ends*/
				if(auts == null) auts = new ArrayList<Aut>();
				logger.debug("Got {} AUTs", auts.size());
				
				logger.debug("Getting Record ID for new defect");
				RunDefectHelper rdHelper = new RunDefectHelper();
				int defRecId = rdHelper.getMaxRecIdDefect();
				logger.debug("Record ID for new defect would be [{}]", defRecId);
				map.put("DEF_REC_ID", defRecId);
				
				map.put("STATUS", "SUCCESS");
				map.put("RUNS", runs);
				map.put("AUTS", auts);
							
				
			} catch (DatabaseException e) {
				
				logger.error(e.getMessage());
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", e.getMessage());
			}
			
			request.getSession().setAttribute("DEFECT_SCR_MAP", map);
			response.sendRedirect("defect.jsp");
			
		} else if (txn.equalsIgnoreCase("fetch_tcs_for_run")) {
			String retData = "";
			String rId = request.getParameter("run");
			try {
				JSONObject json = new JSONObject();
				JSONArray jArray = new JSONArray();
				try{
					List<TestCase> testCases=new CoreDefectHelper().fetchTestCasesForRun(Integer.parseInt(rId));
					Gson gson = new Gson();
					for(TestCase t:testCases){
						JSONObject tJson = new JSONObject(gson.toJson(t));
						jArray.put(tJson);
					}
					
					
					json.put("status", "success");
					json.put("data", jArray);
					
				}catch(DatabaseException e){
					json.put("status", "error");
					json.put("message", e.getMessage());
				}catch(NumberFormatException e){
					json.put("status", "error");
					json.put("message", "Invalid Run ID");
				}
				
				retData = json.toString();
			}  catch (JSONException e) {
				
				logger.error("JSONException caught --> ", e);
				retData = "{status:error,message:An internal error occurred. Please contact Tenjin Support.}";
			}
			
			response.getWriter().write(retData);
			
		} else if (txn.equalsIgnoreCase("fetch_steps_for_tc")) {
			String retData = "";
			String rId = request.getParameter("tcrecid");
			/*Added by Padmavathi for T251IT-48 starts*/
			String runId = request.getParameter("runId");
			/*Added by Padmavathi for T251IT-48 ends*/
			try {
				JSONObject json = new JSONObject();
				JSONArray jArray = new JSONArray();
				try{
					/*Modified by Padmavathi for T251IT-48 starts*/
					List<TestStep> testSteps=new CoreDefectHelper().fetchTStepsForTestCase(Integer.parseInt(rId),Integer.parseInt(runId));
					/*Modified by Padmavathi for T251IT-48 ends*/
					Gson gson = new Gson();
					for(TestStep t:testSteps){
						JSONObject tJson = new JSONObject(gson.toJson(t));
						jArray.put(tJson);
					}
					
					
					json.put("status", "success");
					json.put("data", jArray);
					
				}catch(DatabaseException e){
					json.put("status", "error");
					json.put("message", e.getMessage());
				}catch(NumberFormatException e){
					json.put("status", "error");
					json.put("message", "Invalid Test Case Record ID");
				}
				
				retData = json.toString();
			}  catch (JSONException e) {
				
				logger.error("JSONException caught --> ", e);
				retData = "{status:error,message:An internal error occurred. Please contact Tenjin Support.}";
			}
			
			response.getWriter().write(retData);
			
		}else if (txn.equalsIgnoreCase("fetch_defect_Instance_prj")) {
			String retData = "";
			
			String appId = request.getParameter("appId");
			JSONObject json = new JSONObject();
			ResultsHelper dHelper = new ResultsHelper ();
			Project prj = new Project();

			try {
				try {
					prj = dHelper.fetchDttInstancePrj(tjnSession.getProject().getId(), Integer.parseInt(appId));
					json.put("status", "success");
					json.put("instance", prj.getInstance());
					json.put("project", prj.getDefProject());
					/*Added by Pushpa for TENJINCG-1221 starts*/
					json.put("subset", prj.getSubSet());
					json.put("owner",prj.getRepoOwner());
					json.put("dttInstance", prj.getDttInstance());
					/*Added by Pushpa for TENJINCG-1221 ends*/
					if(prj.getDttPriorities() != null){
						JSONArray pArray = new JSONArray();
						for(String p:prj.getDttPriorities()){
							JSONObject pJ = new JSONObject();
							pJ.put("code", p);
							pArray.put(pJ);
						}
						json.put("priorities", pArray);
					}else{
						json.put("status", "error");
						/*Changed By Ashiki for TENJINCG-985 starts*/
						/*json.put("message", "Priorities are not defined for this DTT instance. Please contact Tenjin Administrator.");*/
						json.put("message", "Priorities are not defined for this Defect Management instance. Please contact Tenjin Administrator.");
						/*Changed By Ashiki for TENJINCG-985 ends*/
					}
					
					if(prj.getDttSeverities() != null){
						JSONArray pArray = new JSONArray();
						for(String p:prj.getDttSeverities()){
							JSONObject pJ = new JSONObject();
							pJ.put("code", p);
							pArray.put(pJ);
						}
						json.put("severities", pArray);
					}else{
						json.put("status", "error");
						/*Changed By Ashiki for TENJINCG-985 starts*/
						json.put("message", "Severities are not defined for this Defect Management instance. Please contact Tenjin Administrator.");
						/*Changed By Ashiki for TENJINCG-985 ends*/
					}
					
					if(Utilities.trim(prj.getInstance()).equalsIgnoreCase("") || Utilities.trim(prj.getDefProject()).equalsIgnoreCase("")){
						json.put("status", "error");
						/*Changed By Ashiki for TENJINCG-985 starts*/
						json.put("message", "No Defect Management Instance & Project is specified for this Project Application.");
						/*Changed By Ashiki for TENJINCG-985 ends*/
					}
					
					
					
				} catch (DatabaseException e) {
					logger.error("error during fetching Defect Management instance & project", e);
					json.put("status", "error");
					json.put("message", e.getMessage());
				} catch (NumberFormatException e) {
					
					json.put("status", "error");
					json.put("message", "Invalid Application ID");
				}
				
				retData = json.toString();
				response.getWriter().write(retData);
			} catch (JSONException e) {
				
				logger.error("JSONException caught --> ", e);
				retData = "{status:error,message:An internal error occurred. Please contact Tenjin Support.}";
			}
		} else if (txn.equalsIgnoreCase("fetch_application_functions")) {
			String retData = "";
			String appId = request.getParameter("appId");
			JSONObject json = new JSONObject();
			ResultsHelper dHelper = new ResultsHelper ();
			List<TestStep> functions = new ArrayList<TestStep>();

			try {
				functions = dHelper.fetchApplicationFunctions(Integer
						.parseInt(appId));
				json.put("status", "SUCCESS");
				json.put("functions", functions);
				retData = json.toString();
				response.getWriter().write(retData);
			} catch (Exception e) {
				logger.error("error during fetching functions", e);
			}
		} else if (txn.equalsIgnoreCase("fetch_priorities_and_severities")) {
			String instance = request.getParameter("instance");
			String retData = "";
			JSONObject json = new JSONObject();
			ResultsHelper dHelper = new ResultsHelper ();
			List<Defect> severities = new ArrayList<Defect>();
			List<Defect> priorities = new ArrayList<Defect>();
			try {
				severities = dHelper.fetchSeverities(instance);
				priorities = dHelper.fetchPriorities(instance);
				json.put("status", "SUCCESS");
				json.put("severities", severities);
				json.put("priorities", priorities);
				retData = json.toString();
				response.getWriter().write(retData);
			} catch (Exception e) {
				logger.error(
						"error during fetching properties of instance---> severity & priority",
						e);
			}
			
			
		}else if(txn.equalsIgnoreCase("fetch_defect_details")){
			int id = Integer.parseInt(request.getParameter("did"));
			Map<String, Object> map = new HashMap<String, Object>();
			/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
			boolean defectFound = true;
			String messageForward="";
			Defect defect=null;
			/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 ends*/
			
			try {
				CoreDefectProcessor processor = new CoreDefectProcessor();
				/*changed by manish for using user credentials instead of instance credentials on 09-Oct 2017 starts*/
				/*Added by Ashiki for TJN252-49 starts*/
				defect = processor.hydrateDefect(id);
				DefectManagementInstance instance= processor.hydrateinstance(defect, tjnSession.getUser().getId());
				try {
				/*Added by Ashiki for Tenj210-64 starts*/
					/*Modified by Shruthi for TENJINCG-1226 starts*/
					
					boolean isInstanceExist = false;
					try{
					 isInstanceExist = instance.equals(null);
					 isInstanceExist = true;
					}
					catch (NullPointerException e) {
						 isInstanceExist =false;
					}
					if( isInstanceExist)
				/*Modified by Shruthi for TENJINCG-1226 starts*/
				/*Added by Ashiki for Tenj210-64 ends*/
				defect = processor.syncDefectDetails(id, tjnSession.getUser().getId(),instance,defect);
				/*added by shruthi for TENJINCG-1224 starts*/
				if(!processor.validateDefect(defect.getRunId()))
				{
				/*Modified by Pushpa for tENJINCG-1222 starts*/
					if(defect.getRunId()==0){
						 defect.setRunId_status("NA");
					}else{
						defect.setRunId_status("PURGED");
					}
					/*Modified by Pushpa for tENJINCG-1222 ends*/
				}
				/*added by shruthi for TENJINCG-1224 ends*/
				/*Added by Pushpalatha for TENJINCG-1203 starts*/
				/*Added by Pushpa for TENJINCG-1221 starts*/
				defect.setOrganization(instance.getOrganization());
				/*Added by Pushpa for TENJINCG-1221 ends*/
				if(instance.getTool().equalsIgnoreCase("dms")){
					for(Attachment att:defect.getAttachments()){
						
						String url=instance.getURL()+"/def/defectAttachments/"+att.getAttachmentIdentifier()+"/download";
						att.setAttachmentUrl(url);
					}
				}
				}
				/*Added by Ashiki for Tenj210-64 starts*/
				catch (NullPointerException e) {
					logger.error("Error ", e);
				}
				/*Added by Ashiki for Tenj210-64 starts*/	
				/*Added by Pushpalatha for TENJINCG-1203 ends*/
				/*Added by Ashiki for TJN252-49 ends*/
				/*changed by manish for using user credentials instead of instance credentials on 09-Oct 2017 ends*/
				processor.close();
				logger.debug("Getting all runs in project [{}]", tjnSession.getProject().getId());
				List<TestRun> runs = new ResultsHelper().hydrateBasicRunInformation(tjnSession.getProject().getId(), BridgeProcess.EXECUTE);
				if(runs == null) runs = new ArrayList<TestRun>();
				TestRun run = new TestRun();
				run.setId(0);
				TestSet set = new TestSet();
				set.setId(0);
				set.setName("General");
				run.setTestSet(set);
				runs.add(0, run);
				logger.debug("Got {} runs", runs.size());
				
				logger.debug("Getting all available AUTs");
				List<Aut> auts = new AutHelper().hydrateAllAut();
				if(auts == null) auts = new ArrayList<Aut>();
				logger.debug("Got {} AUTs", auts.size());
				
				String callback = request.getParameter("callback");
				
				
				
				
				map.put("STATUS", "SUCCESS");
				map.put("RUNS", runs);
				map.put("AUTS", auts);
				map.put("defect", defect);
				map.put("callback", callback);
				map.put("MESSAGE", "");
				
				
			}catch (DatabaseException e) {
				map.put("status", "error");
				map.put("message", e.getMessage());
				logger.error("Error--------------> during hydrating defect{}",id,e);
			} /*Added by Ashiki for TJN252-49 starts*/
			catch (RequestValidationException e) {
				map.put("status", "error");
				map.put("message", e.getMessage());
				defectFound=true;
				messageForward=e.getMessage();
			} 
			/*Added by Ashiki for TJN252-49 ends*/
			 catch (TenjinServletException e) {
				map.put("status", "error");
				map.put("message", e.getMessage());
				/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
				defectFound=false;
				messageForward=e.getMessage();
				/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
				logger.error("Error--------------> during hydrating defect{}",id,e);
			} 
			request.getSession().removeAttribute("DEFECT_SCR_MAP");
			request.getSession().setAttribute("DEFECT_SCR_MAP", map);
			String callback = request.getParameter("callback");
			if(Utilities.trim(callback).equalsIgnoreCase("")){
				callback="none";
			}
			/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
			if(defectFound){
				/*   Added by Priyanka for TENJINCG-1231 starts */
				
	            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				
				if(project.getEndDate()==null){
					request.setAttribute("edate","NA");
				}else{
				String edate=sdf.format(project.getEndDate());
				
				request.setAttribute("edate",edate);
				}
				RequestDispatcher disp = request.getRequestDispatcher("defect_details.jsp?callback=" + callback);
				disp.forward(request, response);
				/*   Added by Priyanka for TENJINCG-1231 ends */
			}else{
				response.sendRedirect("DefectServlet?param=fetch_project_defects&pid="+tjnSession.getProject().getId()+"&callback=fetch_single_defect&message="+messageForward);
			}
			/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 ends*/
			
		}else if(txn.equalsIgnoreCase("download_attachment")){
			String id = request.getParameter("id");
			JSONObject json = new JSONObject();
			String ret = "";
			try {
				try {
					Attachment att = new CoreDefectHelper().hydrateAttachment(Integer.parseInt(id));
					Utilities.checkForDownloadsFolder(getServletContext().getRealPath("/"));
					String downloadsFolder = getServletContext().getRealPath("/") + "\\Downloads\\" + att.getFileName();
					
					FileUtils.copyFile(new File(att.getFilePath()), new File(downloadsFolder));
					json.put("status", "success");
					json.put("path", "Downloads/" + att.getFileName());
					
				} catch (NumberFormatException e) {
					
					logger.error("Invalid Record ID [{}]", id);
					json.put("status", "error");
					json.put("message", "Could not download file. Please contact Tenjin Support.");
				} catch (DatabaseException e) {
					
					json.put("status", "error");
					json.put("message", e.getMessage());
				}
				
				ret = json.toString();
			} catch (JSONException e) {
				
				logger.error("JSONException occurred", e);
				ret = "{status:error,message:An internal error occurred. Please contact Tenjin Support.}";
			}
			
			response.getWriter().write(ret);
			
		}
		
		
		else if(txn.equalsIgnoreCase("sync_multiple_defects")){
			String ids= request.getParameter("dids");
			JSONObject json = new JSONObject();
			/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
			JSONObject objJson = new JSONObject();
			/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 ends*/
			
			try {
				try {
					CoreDefectProcessor processor = new CoreDefectProcessor();
					/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
					/*changed by manish for using user credentials instead of instance credentials on 09-Oct 2017 starts*/
					objJson = processor.syncMultipleDefects(ids, tjnSession.getUser().getId());
					/*changed by manish for using user credentials instead of instance credentials on 09-Oct 2017 ends*/
					if(objJson.get("syncSuccessful").equals("true")){
						json.put("status", "success");
					}else{
						json.put("status", "warn");
						json.put("listExceptions", objJson.get("listExceptions"));
					}
					/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 ends*/
				} catch (TenjinServletException e) {
					
					json.put("status", "error");
					json.put("message", e.getMessage());
				}
				
				response.getWriter().write(json.toString());
			} catch (JSONException e) {
				
				logger.error("JSONException occurred", e);
				response.getWriter().write("{status:error,message:An internal error occurred. Please contact Tenjin Support.}");
			}
			
		}
		/*added by manish for defect TEN-46 on 13-12-2016 starts*/
		else if(txn.equalsIgnoreCase("check_dtt_user_name")){
			JSONObject resJSON = new JSONObject();
			String instance = request.getParameter("instance");
			String userId = request.getParameter("userId");
			String retData = "";
			try {
				try {
					CoreDefectHelper helper = new CoreDefectHelper();
					/*added by shruthi for TENJINCG-1264 starts*/
					String defecttool =helper.fetchTool(instance,userId);
					resJSON.put("defecttool" , defecttool);
					/*added by shruthi for TENJINCG-1264 ends*/

					helper.validateUserDttLoginName(instance,userId);
				} catch (Exception e) {
					logger.debug("Duplicate record found");
					resJSON.put("status", "ERROR");
					resJSON.put("message", "You have already specified credentials for "+instance+". To create a new one, please delete the existing credential and try again.");
				} finally {
					retData = resJSON.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator ";
			}
			response.getWriter().write(retData);
		}
		/*added by manish for defect TEN-46 on 13-12-2016 ends*/
		
		/*changed by manish for req TENJINCG-5 on 10-Jan-2016 starts*/
		else if(txn.equalsIgnoreCase("inactive_multiple_defects")){
			String ids= request.getParameter("dids");
			JSONObject json = new JSONObject();

			try {
				CoreDefectProcessor processor = new CoreDefectProcessor();
				/* Added by Roshni for TENJINCG-1046 starts */
				TenjinMailHandler tmhandler=new TenjinMailHandler();
				List<JSONObject> contents=new ArrayList<>();
				String[] idList=ids.split(",");
				for(int i=0;i<idList.length;i++){
					JSONObject mailContent=new JSONObject();
					Defect defect=new CoreDefectHelper().hydrateDefect(Integer.parseInt(idList[i]));
					User defOwner=new UserHelperNew().hydrateUser(defect.getCreatedBy());
					/*Added by Padmavathi for TENJINCG-1050 starts*/
					if(!new ProjectMailHandler().isUserOptOutFromMail(tjnSession.getProject().getId(),"DefectDelete",defOwner.getId())){
					/*Added by Padmavathi for TENJINCG-1050 ends*/
					mailContent=tmhandler.getMailContent(idList[i], defect.getSummary(), "Defect", tjnSession.getUser().getFullName(), "delete", new Timestamp(new Date().getTime()) ,
							defOwner.getEmail(), "");
					contents.add(mailContent);
					}
				}
				/* Added by Roshni for TENJINCG-1046 ends */
				processor.deleteDefects(ids);
				/* Added by Roshni for TENJINCG-1046 starts */
				for(JSONObject content:contents){
					try {
						tmhandler.sendEmailNotification(content);
					} catch (TenjinConfigurationException e) {
						logger.error("Error ", e);
					}
				}
				/* Added by Roshni for TENJINCG-1046 ends */
				json.put("status","success");
				json.put("message", "Defects deleted successfully");
			} catch (DatabaseException e) {
				try {
					json.put("status","error");
					json.put("message", "An internal error occured, Please contact Tenjin Support");
				} catch (JSONException e1) {
					
				}
				
			} catch (TenjinServletException e) {
				try {
					json.put("status","error");
					json.put("message", "An internal error occured, Please contact Tenjin Support");
				} catch (JSONException e1) {
					
				}
			} catch (JSONException e) {
				try {
					json.put("status","error");
					json.put("message", "An internal error occured, Please contact Tenjin Support");
				} catch (JSONException e1) {
					
				}
			}
			
			response.getWriter().write(json.toString());
		}
		/*changed by manish for req TENJINCG-5 on 10-Jan-2016 ends*/
		
/* Added By Ashiki TENJINCG-986 starts */
else if(txn.equalsIgnoreCase("Fetch_Manager_Projects")){
			String instance = request.getParameter("instance");
			String retData="";
			JSONObject rJson=null;
			JSONArray dttProjectsArray = new JSONArray();
			DefectManagementInstance mngr = new DefectManagementInstance();
			try {
				rJson = new JSONObject();
				mngr = new DefectHelper().hydrateDefects(instance);
				String url = mngr.getURL();
				String username = mngr.getAdminId();
				String encPwd = mngr.getPassword();
				/*Modified by paneendra for VAPT FIX starts*/
				/*String password = new Crypto().decrypt(encPwd.split("!#!")[1],
						decodeHex(encPwd.split("!#!")[0].toCharArray()));*/
				String password = new CryptoUtilities().decrypt(encPwd);
				/*Modified by paneendra for VAPT FIX ends*/
				DefectManager dm = null;
				dm = DefectFactory.getDefectTools(mngr.getTool(), url, username, password);
				if(dm.isUrlValid()){
					dm.login(); 
					dttProjectsArray = dm.getAllProjects();
					dm.logout();
					/*Modified by Padmavathi for TNJN27-70 starst*/
					JSONArray projectArray=new JSONArray();
					for (int i=0;i<dttProjectsArray.length();i++){ 
						JSONObject projectJson=new JSONObject();
						projectJson.put("pname", dttProjectsArray.getJSONObject(i).getString("pname"));
						projectJson.put("pid", dttProjectsArray.getJSONObject(i).getString("pid"));
						
						projectArray.put(projectJson);
						
					   }
					rJson.put("projectLists", projectArray);
					/*Modified by Padmavathi for TNJN27-70 ends*/
					rJson.put("status", "SUCCESS");
				}else{
					rJson.put("status", "ERROR");
					rJson.put("message", mngr.getTool() + " is not accessible. Please check url and try again.");
				}
			} catch (EDMException e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							e.getMessage());
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
			} catch (UnreachableEDMException e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							e.getMessage());
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
			} catch (JSONException e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occured, Please contact your System Administrator");
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
			} catch (DatabaseException e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message", e.getMessage());
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
			} catch (Exception e) {
				try {
					rJson.put("status", "ERROR");
					rJson.put("message",
							"An internal error occured, Please contact your System Administrator");
					retData = rJson.toString();
				} catch (JSONException e1) {
					logger.error(e1.getMessage());
				}
				logger.error("Error--------------> during fetching Defect Management projects");
			}
			finally {
				retData = rJson.toString();
			}
			response.getWriter().write(retData);

		}
		

else if(txn.equalsIgnoreCase("Persist_Projects")){
	String projects = request.getParameter("paramval");
	String subSetName=request.getParameter("subSetName");
	String instance=request.getParameter("instance");
	String name=request.getParameter("name");
	String userId=user.getId();
	String retData="";
	JSONObject rJson=null;
	rJson = new JSONObject();
	DefectManagementInstance defectMgmntPrj=new DefectManagementInstance();
	defectMgmntPrj.setInstance(instance);
	defectMgmntPrj.setSubSetName(subSetName);
	defectMgmntPrj.setProjects(projects);
	defectMgmntPrj.setName(name);
	defectMgmntPrj.setUserId(userId);
	try {
		defectMgmntPrj=	new DefectHelper().persistDefectManagementProjects(defectMgmntPrj);
		DefectHelper helper = new DefectHelper();
		ArrayList<DefectManagementInstance> dm2=helper.hydrateDefectManagementProjects(name);
		
		rJson.put("projectLists", dm2);
		rJson.put("status", "SUCCESS");
		rJson.put("message", "Subset saved Successfully");
	} catch (DatabaseException e) {

		try {
			rJson.put("status", "ERROR");
			rJson.put("message", e.getMessage());
			retData = rJson.toString();
		} catch (JSONException e1) {
			logger.error(e1.getMessage());
		}
	
				
	} catch (JSONException e) {
		try {
			rJson.put("status", "ERROR");
			rJson.put("message",
					"An internal error occured, Please contact your System Administrator");
			retData = rJson.toString();
		} catch (JSONException e1) {
			logger.error(e1.getMessage());
		}
	}
	finally {
		retData = rJson.toString();
	}
	
	
	response.getWriter().write(retData);
}
		
	}
	
	/* Added By Ashiki TENJINCG-986 ends */
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		boolean isMultiPart;
		isMultiPart = ServletFileUpload.isMultipartContent(request);
		Defect defect = null;
		Map<String, Object> map = (Map<String, Object>) request.getSession().getAttribute("DEFECT_SCR_MAP");
		if(map == null){
			map = new HashMap<String, Object>();
		}
		JSONObject rJson = new JSONObject();
		String txn=	request.getParameter("param");

		Authorizer authorizer=new Authorizer();
		if(txn.equalsIgnoreCase("NEW_DEF")  || txn.equalsIgnoreCase("edit_def") || txn.equalsIgnoreCase(" ")) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}

		if(txn.equalsIgnoreCase("NEW_DEF")) {

			logger.debug("Creating New Defect list");

			String jsonString = request.getParameter("json");
			logger.debug("JSON String: " + jsonString);

			String retData = "";
			try {

				JSONObject json = new JSONObject(jsonString);

				DefectManagementInstance dm = new DefectManagementInstance();


				dm.setName(json.getString("name"));
				dm.setTool(json.getString("tool"));
				dm.setURL(json.getString("url"));
				dm.setAdminId(json.getString("admin"));
				/*Modified by Preeti for defect posting in JIRA starts*/
				/*Modified by paneendra for VAPT fix starts*/
				/*dm.setPassword(new Crypto().encrypt(json.getString("password")));*/
				//dm.setPassword(new Crypto().encrypt(request.getParameter("password")));
				String dmPwd=new CryptoUtilities().encrypt(request.getParameter("password"));
				dm.setPassword(dmPwd);
				/*Modified by paneendra for VAPT fix ends*/
				/*Modified by Preeti for defect posting in JIRA ends*/
				dm.setSeverity(json.getString("Severity"));
				dm.setPriority(json.getString("Priority"));
				/*Added by Pushpalatha for TENJINCG-1220 starts*/
				dm.setOrganization(json.getString("Organization"));
				/*Added by Pushpalatha for TENJINCG-1220 ends*/
				DefectHelper helper = new DefectHelper();
				int checkName=helper.checkName(dm.getName());

				/*Added by Preeti for defect priority and severity max length starts*/
				int flag=0;
				String str=dm.getPriority();
				String [] prty={};
				prty=str.split(",");

				for(int i=0;i<prty.length;i++){
					if(prty[i].length()>20){
						JSONObject r = new JSONObject();
						r.put("status", "ERROR");
						r.put("message", "Priority value "+prty[i]+" should not exceed its max length 20.");
						retData = r.toString();
						flag=1;
					}
				}
				String str1=dm.getSeverity();
				String [] svrty={};
				svrty=str1.split(",");

				for(int i=0;i<svrty.length;i++){
					if(svrty[i].length()>20){
						JSONObject r = new JSONObject();
						r.put("status", "ERROR");
						r.put("message", "Severity value "+svrty[i]+" should not exceed its max length 20.");
						retData = r.toString();
						flag=1;
					}
				}
				/*Added by Preeti for defect priority and severity max length ends*/
				if(checkName==0 ){
					if(flag!=1){
						dm = helper.persistDefect(dm);
						JSONObject r1 = new JSONObject();
						r1.put("status","success");
						/*Changed by Ashiki for V2.8-29 starts*/
						/*r1.put("message","Added Defect Successfully");*/
						r1.put("message","Defect Management Instance created Successfully");
						/*Changed by Ashiki for V2.8-29 ends*/
						retData = r1.toString();
					}
				}
				else{
					JSONObject r = new JSONObject();
					r.put("status", "ERROR");
					r.put("message", " User Already Exist AS: "+dm.getName());
					retData = r.toString();

				} 


			}catch (Exception e) {

				logger.error("Error ", e);
				JSONObject r = new JSONObject();
				try {
					r.put("status", "ERROR");
					/*Changed by Ashiki for V2.8-29 starts*/
					/*r.put("message",
						"Could not create defect due to an internal error");*/
					r.put("message","Could not create Defect Management Instance due to an internal error");
					/*Changed by Ashiki for V2.8-29 ends*/
					retData = r.toString();

				} catch (JSONException e1) {
					retData = "An internal error occurred. Please contact your System Administrator ";

				}
			}
			response.getWriter().write(retData);

		}else if (txn.equalsIgnoreCase("edit_def")) {
			logger.debug("Editing Defect Management Details");
			String json = request.getParameter("json");
			JSONObject resJSON = new JSONObject();
			String retData = "";
			try {
				try {
					JSONObject j = new JSONObject(json);
					DefectManagementInstance dm = new DefectManagementInstance();
					Map<String,Object> scmap = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
					DefectManagementInstance dm2 = (DefectManagementInstance)scmap.get("DEF_BEAN");

					String exName=dm2.getName();

					dm.setName(j.getString("name"));
					dm.setTool(j.getString("tool"));
					dm.setURL(j.getString("url"));
					dm.setAdminId(j.getString("admin"));
					/*Modified by Preeti for defect posting in JIRA starts*/
					/*dm.setPassword(new Crypto().encrypt(j.getString("password")));*/
					/*Modified by Preeti for defect posting in JIRA ends*/
					dm.setSeverity(j.getString("Severity"));
					dm.setPriority(j.getString("Priority"));
					/*Added by Pushpalatha for TENJINCG-1220 starts*/
					/*Added by Prem for Tenj211-24 starts*/
					if(j.getString("name").equalsIgnoreCase("github")) 
						dm.setOrganization(j.getString("Organization"));	
					/*Added by Prem for Tenj211-24 starts*/
					/*Added by Pushpalatha for TENJINCG-1220 ends*/
					/*Modified by Preeti for defect posting in JIRA starts*/
				
					if (request.getParameter("password").equals(request.getParameter("password1")))
						dm.setPassword(request.getParameter("password"));
					else
						/*Modified by paneendra for VAPT fix starts*/
						dm.setPassword(new CryptoUtilities().encrypt(request.getParameter("password")));
					/*Modified by paneendra for VAPT fix ends*/
					/*Modified by Preeti for defect posting in JIRA ends*/	
					/*Added by Preeti for defect priority and severity max length starts*/
					int flag=0;
					String str=dm.getPriority();
					String [] prty={};
					prty=str.split(",");

					for(int i=0;i<prty.length;i++){
						if(prty[i].length()>20){
							resJSON.put("status", "ERROR");
							resJSON.put("message", "Priority value "+prty[i]+" should not exceed its max length 20.");
							retData = resJSON.toString();
							flag=1;
						}
					}
					String str1=dm.getSeverity();
					String [] svrty={};
					svrty=str1.split(",");

					for(int i=0;i<svrty.length;i++){
						if(svrty[i].length()>20){
							resJSON.put("status", "ERROR");
							resJSON.put("message", "Severity value "+svrty[i]+" should not exceed its max length 20.");
							retData = resJSON.toString();
							flag=1;
						}
					}
					/*Added by Preeti for defect priority and severity max length ends*/
					if(flag!=1){
						new DefectHelper().updateDefect(dm,exName);
						resJSON.put("status","success");
						/*Modified by Prem for V2.8-89 Starts*/
						//					resJSON.put("message"," Updated Successfully");
						resJSON.put("message"," Defect Management Instance Updated Successfully");
						/*Modified by Prem for V2.8-89 Ends*/
					}
				} catch (Exception e) {
					logger.error("Error ", e);
					resJSON.put("status", "ERROR");
					resJSON.put("message",
							"Could not update defect due to an internal error ");
				} finally {
					retData = resJSON.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);

		}else {
		try {
			if(isMultiPart){
				//This could be one of two request types (new_defect or add_attachments)
				try {
					logger.debug("Form is multipart");
					DiskFileItemFactory factory = new DiskFileItemFactory();
					ServletFileUpload upload = new ServletFileUpload(factory);
					List fileItems = upload.parseRequest(request);
					
					Iterator fileItemsIter = fileItems.iterator();
					
					Map<String, String> formValues = new HashMap<String, String>();
					List<FileItem> uploadedFiles = new ArrayList<FileItem>();
					while(fileItemsIter.hasNext()){
						FileItem fi = (FileItem)fileItemsIter.next();
						if(fi.isFormField()){
							formValues.put(fi.getFieldName(), fi.getString());
						}else{
							uploadedFiles.add(fi);
						}
					}
					
					String txnType = formValues.get("req_type");
					if(Utilities.trim(txnType).equalsIgnoreCase("new_defect")){
						String formJson = formValues.get("form_data");
						//String dRecId = formValues.get("def_rec_id");
						
						JSONObject json = new JSONObject(formJson);
						defect = new Defect();
						defect.setSummary(json.getString("summary"));
						defect.setTestCaseId(json.getInt("tc"));
						defect.setTestStepId(json.getInt("tstep"));
						defect.setRunId(json.getInt("runId"));
						defect.setAppId(json.getInt("app"));
						defect.setFunctionCode(json.getString("func"));
						defect.setDttInstanceName(json.getString("instance"));
						defect.setEdmProjectName(json.getString("dttproject"));
						defect.setSeverity(json.getString("severity"));
						defect.setPriority(json.getString("priority"));
						defect.setDescription(json.getString("desc"));
						/*Added by Pushpa for TENJINCG-1221 starts*/
						try {
							defect.setRepoOwner(json.getString("owner"));
						} catch (Exception e1) {
							
							logger.error("Error ", e1);
						}
						/*Added by Pushpa for TENJINCG-1221 ends*/
						defect.setCreatedBy(tjnSession.getUser().getId());
						defect.setCreatedOn(new Timestamp(new Date().getTime()));
						String postFlag= formValues.get("post_defect");
						if(Utilities.trim(postFlag).equalsIgnoreCase("y")){
							defect.setTjnStatus("POST");
						}else{
							defect.setTjnStatus("ONHOLD");
						}
						String msg = "";
						CoreDefectProcessor processor;
						try {
							processor = new CoreDefectProcessor();
						} catch (TenjinServletException e) {
							logger.error("ERROR creating new defect --> ", e);
							
							rJson.put("status", "ERROR");
							rJson.put("message", e.getMessage());
							response.getWriter().write(rJson.toString());
							return;
						}
						try {
							processor.saveDefect(defect, tjnSession.getProject().getId());
						} catch (TenjinServletException e) {
							logger.error("ERROR creating new defect --> ", e);
							
							rJson.put("status", "ERROR");
							rJson.put("message", e.getMessage());
							response.getWriter().write(rJson.toString());
							return;
						}
						rJson.put("did", defect.getRecordId());
						msg = "Defect [" + defect.getRecordId() + "] created successfully";
						
						int dRecId = defect.getRecordId();
						logger.debug("Processing attachments for defect [{}]" + dRecId);
						String tempAttachmentFolderName = "";
						
						try {
							tempAttachmentFolderName = TenjinConfiguration.getProperty("MEDIA_REPO_PATH") + "\\Defect";
						} catch (TenjinConfigurationException e) {
							logger.error("ERROR creating new defect attachment--> ", e);
							
							rJson.put("status", "WARN");
							rJson.put("message", "Defect [" + defect.getRecordId() + "] was created successfully, but there was a problem saving attachments. Please contact Tenjin Support.");
							response.getWriter().write(rJson.toString());
							return;
						}
						File dir  = new File(tempAttachmentFolderName);
						if(!dir.exists()){
							dir.mkdir();
						}
						
						tempAttachmentFolderName = tempAttachmentFolderName + "\\" + dRecId;
						dir  = new File(tempAttachmentFolderName);
						if(!dir.exists()){
							dir.mkdir();
						}	
						String fullFilePath = "";
						List<Attachment> attachments = new ArrayList<Attachment>();
						for(FileItem fi:uploadedFiles){
							String fileName = fi.getName();
							if(Utilities.trim(fileName).equalsIgnoreCase("")){
								continue;
							}
							File file;
							String oFileName = "";
							if( fileName.lastIndexOf("\\") >= 0 ){
								oFileName = fileName.substring(fileName.lastIndexOf("\\"));
							}else{
								oFileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
							}
							
							fullFilePath = tempAttachmentFolderName + "\\" + oFileName;
							file = new File(fullFilePath);
							try {
								fi.write(file) ;
							} catch (Exception e) {
								logger.error("ERROR creating new defect attachment--> ", e);
								
								rJson.put("status", "WARN");
								rJson.put("message", "Defect [" + defect.getRecordId() + "] was created successfully, but there was a problem saving attachments. Please contact Tenjin Support.");
								response.getWriter().write(rJson.toString());
								return;
							}
							logger.debug("Attachment file written to path {}", fullFilePath);
							
							Attachment att = new Attachment();
							att.setFileName(oFileName);
							att.setFilePath(fullFilePath);
							/*Added by Prem for TENJINCG-1236 starts*/
							String content = Files.toString(file, StandardCharsets.UTF_8);
							String encodedCredentials = new String(Base64.encodeBase64(content.getBytes()));
							att.setFileContent(encodedCredentials);
							/*Added by Prem for TENJINCG-1236 Ends*/
							attachments.add(att);
						}
						
						defect.setAttachments(attachments);
						
						logger.info("Saving attachments...");
						try {
							processor.saveDefectAttachments(defect);
						} catch (TenjinServletException e) {
							logger.error("ERROR creating new defect attachment--> ", e);
							
							rJson.put("status", "WARN");
							rJson.put("message", "Defect [" + defect.getRecordId() + "] was created successfully, but there was a problem saving attachments. Please contact Tenjin Support.");
							response.getWriter().write(rJson.toString());
							return;
						}
						/* Added by Roshni for TENJINCG-1046 starts */
						List<User> users=null;
						String sendUsers="";
						try {
							users = new ProjectHandler().getProjectUsers(tjnSession.getProject().getId());
								/*Added by Padmavathi for TENJINCG-1050 starts*/
							String mailSendUsers=new ProjectMailHandler().getPrjMailPreferenceUsers(tjnSession.getProject().getId(), "DefectCreation");
							List<String> mailSendUsersList=Arrays.asList(mailSendUsers.split(","));
								/*Added by Padmavathi for TENJINCG-1050 ends*/
							int count=0;
							for(User user:users){
								/*Added by Padmavathi for TENJINCG-1050 starts*/
								if(mailSendUsersList.contains(user.getId())){
									/*Added by Padmavathi for TENJINCG-1050 ends*/
									sendUsers+=user.getEmail();
									if(count<users.size()-1)
										sendUsers+=",";
									count++;
								}
							}
							
						} catch (DatabaseException e1) {
							
							logger.error("Error ", e1);
						}
						TenjinMailHandler tmhandler=new TenjinMailHandler();						
						
						JSONObject mailContent=tmhandler.getMailContent(String.valueOf(defect.getRecordId()), defect.getSummary(), "Defect", tjnSession.getUser().getFullName(), 
								"create", new Timestamp(new Date().getTime()), sendUsers, "");
						try {
							new TenjinMailHandler().sendEmailNotification(mailContent);
						} catch (TenjinConfigurationException e) {
							logger.error("Error ", e);
						}
						/* Added by Roshni for TENJINCG-1046 ends */
						if(Utilities.trim(postFlag).equalsIgnoreCase("y")){
							try {
								/*****************************
								 * Changed by Sriram to implement DTT User Mapping functionality
								 */
								/*processor.postDefectToDTT(defect);*/
								processor.postDefectToDTT(defect, tjnSession.getUser().getId());
								/*****************************
								 * Changed by Sriram to implement DTT User Mapping functionality ends
								 */
								
								/* Added by Roshni for TENJINCG-1046 starts */
								User user=new UserHandler().getUser(defect.getCreatedBy());
								
								JSONObject mailPostContent=tmhandler.getMailContent(String.valueOf(defect.getRecordId()), defect.getSummary(), "Defect", tjnSession.getUser().getFullName(),
										"post", new Timestamp(new Date().getTime()), user.getEmail(), "");
								
								try {
									tmhandler.sendEmailNotification(mailPostContent);
								} catch (TenjinConfigurationException e) {
									logger.error("Error ", e);
								}
								/* Added by Roshni for TENJINCG-1046 ends */
								 /*Changed By Ashiki for TENJINCG-985 starts*/
								/*msg = "Defect [" + defect.getRecordId() + "] has been posted successfully to DTT. Identifier - [" + defect.getIdentifier() + "]";*/
								msg = "Defect [" + defect.getRecordId() + "] has been posted successfully to Defect Management. Identifier - [" + defect.getIdentifier() + "]";
								/*Changed By Ashiki for TENJINCG-985 ends*/
							} catch (TenjinServletException e) {
								logger.error("ERROR posting defect to Defect Management --> ", e);
								
								rJson.put("status", "WARN");
								/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
								rJson.put("message", e.getMessage());
								/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 ends*/
								/*rJson.put("message", "Defect [" + defect.getRecordId() + "] was created successfully, but there was a problem posting it to DTT. Please contact Tenjin Support.");*/
								response.getWriter().write(rJson.toString());
								return;
								/* Added by Roshni for TENJINCG-1046 starts */
							} catch (RecordNotFoundException e1) {
								
								logger.error("Error ", e1);
							} catch (DatabaseException e1) {
								
								logger.error("Error ", e1);
								/* Added by Roshni for TENJINCG-1046 ends */
							}
						}
						
						map.put("STATUS", "SUCCESS");
						map.put("MESSAGE", msg);
						rJson.put("did", defect.getRecordId());
					}else if(Utilities.trim(txnType).equalsIgnoreCase("update_defect")){
						String formJson = formValues.get("form_data");
						String postFlag= formValues.get("post_defect");
						//String dRecId = formValues.get("def_rec_id");
						String msg = "";
						JSONObject json = new JSONObject(formJson);
						defect = new Defect();
						defect.setRecordId(json.getInt("recid"));
						defect.setSummary(json.getString("summary"));
						defect.setSeverity(json.getString("severity"));
						defect.setPriority(json.getString("priority"));
						defect.setDescription(json.getString("desc"));
						defect.setStatus(json.getString("status"));
						defect.setModifiedBy(tjnSession.getUser().getId());
						defect.setDttInstanceName(json.getString("instance"));
						defect.setEdmProjectName(json.getString("dttproject"));
						defect.setCreatedBy(json.getString("createdby"));
						/*Added by Pushpa for TENJINCG-1221 starts*/
						try {
							defect.setRepoOwner(json.getString("owner"));
						}catch(Exception e) {
							
						}
						
						/*Added by Pushpa for TENJINCG-1221 ends*/
						/*added by shruthi for TENJINCG-1224 starts*/
						/*Modified by Prem  start*/
						Object s = json.get("runId");
						if (s.equals("General"))
						defect.setRunId(0);
						else
						defect.setRunId(json.getInt("runId"));
//						defect.setRunId(json.getInt("runId"));
						/*Modified by Prem  End*/
						/*added by shruthi for TENJINCG-1224 ends*/
						
						try{
							defect.setTjnStatus(json.getString("action"));
						}catch(JSONException e){
							logger.warn("ACTION is not specified in json",e );
							defect.setTjnStatus("ONHOLD");
						}
						CoreDefectProcessor processor;
						try {
							processor = new CoreDefectProcessor();
						} catch (TenjinServletException e) {
							logger.error("ERROR creating new defect --> ", e);
							
							rJson.put("status", "ERROR");
							rJson.put("message", e.getMessage());
							response.getWriter().write(rJson.toString());
							return;
						}
						try {
							/*added by shruthi for TENJINCG-1224 starts*/
							if(Utilities.trim(postFlag).equalsIgnoreCase("n")){
								processor.updateDefectDetails(defect);
							}
							else
							{
								
								/*Modified by Prem Start*/
								if (defect.getRunId()==0){
									processor.updateDefectDetails(defect);
								}
							else /*Modified by Prem End*/
								if(processor.validateDefect(defect.getRunId()))
								{
									processor.updateDefectDetails(defect);
								}
								else
								{
									processor.updateDefectDetails(defect);
									rJson.put("status", "ERROR");
									rJson.put("message", "RunId [" + defect.getRunId() + "] purged,So could not able to post.");
									response.getWriter().write(rJson.toString());
									return;
								}
									
							}
							/*added by shruthi for TENJINCG-1224 starts*/
							
						} catch (TenjinServletException e) {
							logger.error("ERROR updating defect --> ", e);
							
							rJson.put("status", "ERROR");
							rJson.put("message", e.getMessage());
							response.getWriter().write(rJson.toString());
							return;
						}
						msg = "Defect [" + defect.getRecordId() + "] updated successfully";
						int dRecId = defect.getRecordId();
						
						logger.debug("Processing attachments for defect [{}]" + dRecId);
						String tempAttachmentFolderName = "";
						
						try {
							tempAttachmentFolderName = TenjinConfiguration.getProperty("MEDIA_REPO_PATH") + "\\Defect";
						} catch (TenjinConfigurationException e) {
							logger.error("ERROR creating new defect attachment--> ", e);
							
							rJson.put("status", "WARN");
							rJson.put("message", "Defect [" + defect.getRecordId() + "] was updated successfully, but there was a problem saving attachments. Please contact Tenjin Support.");
							response.getWriter().write(rJson.toString());
							return;
						}
						File dir  = new File(tempAttachmentFolderName);
						if(!dir.exists()){
							dir.mkdir();
						}
						
						tempAttachmentFolderName = tempAttachmentFolderName + "\\" + dRecId;
						dir  = new File(tempAttachmentFolderName);
						if(!dir.exists()){
							dir.mkdir();
						}	
						String fullFilePath = "";
						List<Attachment> attachments = new ArrayList<Attachment>();
						for(FileItem fi:uploadedFiles){
							String fileName = fi.getName();
							if(Utilities.trim(fileName).equalsIgnoreCase("")){
								continue;
							}
							File file;
							String oFileName = "";
							if( fileName.lastIndexOf("\\") >= 0 ){
								oFileName = fileName.substring(fileName.lastIndexOf("\\"));
							}else{
								oFileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
							}
							
							fullFilePath = tempAttachmentFolderName + "\\" + oFileName;
							file = new File(fullFilePath);
							try {
								fi.write(file) ;
							} catch (Exception e) {
								logger.error("ERROR creating new defect attachment--> ", e);
								
								rJson.put("status", "WARN");
								rJson.put("message", "Defect [" + defect.getRecordId() + "] was updated successfully, but there was a problem saving attachments. Please contact Tenjin Support.");
								response.getWriter().write(rJson.toString());
								return;
							}
							logger.debug("Attachment file written to path {}", fullFilePath);
							
							Attachment att = new Attachment();
							att.setFileName(oFileName);
							att.setFilePath(fullFilePath);
							attachments.add(att);
						}
						
						defect.setAttachments(attachments);
						
						logger.info("Saving attachments...");
						try {
							processor.saveDefectAttachments(defect);
						} catch (TenjinServletException e) {
							logger.error("ERROR creating new defect attachment--> ", e);
							
							rJson.put("status", "WARN");
							rJson.put("message", "Defect [" + defect.getRecordId() + "] was updated successfully, but there was a problem saving attachments. Please contact Tenjin Support.");
							response.getWriter().write(rJson.toString());
							return;
						}
						
						logger.info("Processing attachment removals if any");
						String attachmentRecordIds = formValues.get("remove_files");
						try {
							processor.removeAttachments(defect, attachmentRecordIds);
						} catch (TenjinServletException e) {
							logger.error("ERROR removing defect attachment--> ", e);
							
							rJson.put("status", "WARN");
							rJson.put("message", "Defect [" + defect.getRecordId() + "] was updated successfully, but there was a problem removing attachments. Please contact Tenjin Support.");
							response.getWriter().write(rJson.toString());
							return;
						}
						/* Added by Roshni for TENJINCG-1046 starts */
						TenjinMailHandler tmhandler=new TenjinMailHandler();
						User defectOwner=new UserHandler().getUser(defect.getCreatedBy());
						/*Added by Padmavathi for TENJINCG-1050 starts*/
						if(!new ProjectMailHandler().isUserOptOutFromMail(tjnSession.getProject().getId(),"DefectUpdate",defectOwner.getId())){
						/*Added by Padmavathi for TENJINCG-1050 ends*/
							JSONObject mailContent=tmhandler.getMailContent(String.valueOf(defect.getRecordId()), defect.getSummary(), "Defect", tjnSession.getUser().getFullName(), 
								"update", new Timestamp(new Date().getTime()), defectOwner.getEmail(), "");
							try {
								new TenjinMailHandler().sendEmailNotification(mailContent);
							} catch (TenjinConfigurationException e) {
								logger.error("Error ", e);
							}
						}
						/* Added by Roshni for TENJINCG-1046 ends */
						if(Utilities.trim(postFlag).equalsIgnoreCase("y")){
							try {
								/*****************************
								 * Changed by Sriram to implement DTT User Mapping functionality
								 */
								/*processor.postDefectToDTT(defect);*/
								processor.postDefectToDTT(defect, tjnSession.getUser().getId());
								/*****************************
								 * Changed by Sriram to implement DTT User Mapping functionality ends
								 */
								/*Changed By Ashiki for TENJINCG-985 starts*/
								/*msg = "Defect [" + defect.getRecordId() + "] has been posted successfully to DTT. Identifier - [" + defect.getIdentifier() + "]";*/
								msg = "Defect [" + defect.getRecordId() + "] has been posted successfully to Defect Management. Identifier - [" + defect.getIdentifier() + "]";
								/*Changed By Ashiki for TENJINCG-985 ends*/
								/* Added by Roshni for TENJINCG-1046 starts */
								JSONObject mailPostContent=tmhandler.getMailContent(String.valueOf(defect.getRecordId()), defect.getSummary(), "Defect", tjnSession.getUser().getFullName(), 
										"post", new Timestamp(new Date().getTime()), defectOwner.getEmail(), "");
								try {
									new TenjinMailHandler().sendEmailNotification(mailPostContent);
								} catch (TenjinConfigurationException e) {
									logger.error("Error ", e);
								}
								/* Added by Roshni for TENJINCG-1046 ends */
							} catch (TenjinServletException e) {
								logger.error("ERROR posting defect to Defect Management --> ", e);
								
								rJson.put("status", "WARN");
								/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
								rJson.put("message", e.getMessage());
								//rJson.put("message", "Defect [" + defect.getRecordId() + "] was updated successfully, but there was a problem posting it to DTT. Please contact Tenjin Support.");
								/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 ends*/
								response.getWriter().write(rJson.toString());
								return;
							}
						}
						
						map.put("STATUS", "SUCCESS");
						map.put("MESSAGE", msg);
						
						
					}
				} catch (FileUploadException e) {
					
					logger.error(e.getMessage(),e);
					map.put("STATUS","ERROR");
					map.put("MESSAGE", "An internal error occurred. Please contact Tenjin Support.");
				/* Added by Roshni for TENJINCG-1046 starts */
				} catch (RecordNotFoundException e1) {
					
					logger.error("Error ", e1);
				} catch (DatabaseException e1) {
					
					logger.error("Error ", e1);
				/* Added by Roshni for TENJINCG-1046 ends */
				} 
				
			}else{
				//This has to be update_defect
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "This feature is not implemented yet");
			}
			
			
			rJson.put("status", map.get("STATUS"));
			rJson.put("message", map.get("MESSAGE"));
			response.getWriter().write(rJson.toString());
		} catch (JSONException e) {
			
			logger.error("JSONException occurred", e);
			response.getWriter().write("{stauts:ERROR,message:An internal error occurred. Please contact Tenjin Support.}");
		}
		}
	}
	
	 

}
