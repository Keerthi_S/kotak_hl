/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LicenseServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 19-Nov-2018			Prem					Requiremen#TENJINCG-837
* 26-Nov-2018			Prem					Requiremen#TENJINCG-837
* 28-Nov-2018			Prem					Requiremen#TENJINCG-837
* 01-Dec-2018           Leelaprasad             TJNUN262-43
* 03-Dec-2018           Prem             		Requiremen#TENJINCG-837
* 06-Dec-2018           Prem             		Requiremen#TENJINCG-837
* 06-Dec-2018           Prem					TJNUN262-85 
* 10-Dec-2018			Prem					TJNUN262-108
* 14-Dec-2018			Prem					TJNUN262-57 
* 17-12-2018      		Prem					For java 7 encryption
* 24-06-2019            Padmavathi              for license 
* */

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.LicenseExpiredException;
import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.LicenseHandler;
import com.ycs.tenjin.license.License;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.util.SessionUtils;

/**
 * Servlet implementation class LicesnceServlet
 */
@MultipartConfig
@SuppressWarnings("resource")
public class LicenseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(LicenseServlet.class);
	
	
	public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		@Override
		public boolean checkAccess(HttpServletRequest request) {
			if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
				return true;
			}
			return false;
		}  
	};
	LicenseHandler licenseHandler=new LicenseHandler();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("static-access")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		String param = request.getParameter("param");
		
		Authorizer authorizer=new Authorizer();
		if(param.equalsIgnoreCase("") || param.equalsIgnoreCase("view") ||param.equalsIgnoreCase("modify")||param.equalsIgnoreCase("renew")) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		
		if(param!=null &&param.equalsIgnoreCase("view")){
			try {
				License  license =null;
				license=(License) CacheUtils.getObjectFromRunCache("renewal_licensedetails");
				if(license!=null){
					request.setAttribute("renewedlicense", license);
					request.setAttribute("renewedlicComponents", license.getLicenseComponents());
				}
				license = (License) CacheUtils.getObjectFromRunCache("licensedetails");	
				request.setAttribute("license", license);
				request.setAttribute("licComponents", license.getLicenseComponents());
				request.getRequestDispatcher("license_details.jsp").forward(request, response);
			} catch (TenjinConfigurationException  e) {
				
				logger.error("Error ", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} 
		}else if(param!=null &&param.equalsIgnoreCase("activated")){
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}else if(param!=null &&(param.equalsIgnoreCase("modify")||param.equalsIgnoreCase("renew"))){
			try {
				request.setAttribute("systemId", licenseHandler.generateSystemId());
				request.setAttribute("action", param);
				request.getRequestDispatcher("modify_license.jsp").forward(request, response);
			} catch (TenjinServletException | TenjinConfigurationException e) {
				
				logger.error("Error ", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
			
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("static-access")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		String param = request.getParameter("param");
		String licenseKey=request.getParameter("licenseKey");
		String systemId=request.getParameter("systemId");
		
		 Authorizer authorizer=new Authorizer();
			if(param.equalsIgnoreCase("") || param.equalsIgnoreCase("renewal") || param.equalsIgnoreCase("modify")) {
				if(!authorizer.hasAccess(request)){
					response.sendRedirect("noAccess.jsp");
					return;
				}
			}
		
		if(param!=null && param.equalsIgnoreCase("activateLicense")){
			if(licenseKey!=null){
				try {
					License license=licenseHandler.validateLicenseKey(licenseKey,false);
					licenseHandler.persistLicenseKey(licenseKey);
					licenseHandler.putLicenseInCache(license);
					SessionUtils.setScreenState("success", "license.activated.success", request);
					response.sendRedirect("LicenseServlet?param=activated");
				}  catch (TenjinServletException |LicenseExpiredException|LicenseInactive e ) {
					request.setAttribute("systemId",systemId);
					request.setAttribute("status", "error");
					request.setAttribute("message", e.getMessage());
					request.getRequestDispatcher("license_installation.jsp").forward(request, response);
				}  catch (ParseException |DatabaseException e) {
					request.setAttribute("systemId",systemId);
					request.setAttribute("status", "error");
					request.setAttribute("message", "Internal error occured.Please contact Tenjin Support.");
					request.getRequestDispatcher("license_installation.jsp").forward(request, response);
				} 
			}else{
				request.setAttribute("status", "error");
				request.setAttribute("message", "Invalid License key. Please contact Tenjin support team.");
				request.getRequestDispatcher("license_installation.jsp").forward(request, response);
			}
		}else if(param!=null &&param.equalsIgnoreCase("renewal")){
			if(licenseKey!=null){
				try {
					License license=licenseHandler.validateLicenseKey(licenseKey,true);
					licenseHandler.persistRenewalLicenseKey(licenseKey);
					licenseHandler.putRenewalLicenseInCache(license);
					licenseHandler.sendLicenseNotificationMail(license,"Renewed");
					request.getSession().removeAttribute("LICENSE_NOTIFICATION");
					SessionUtils.setScreenState("success", "license.renewal.success", request);
					response.sendRedirect("LicenseServlet?param=view");
				}  catch (TenjinServletException |LicenseExpiredException|LicenseInactive e ) {
					request.setAttribute("systemId",systemId);
					SessionUtils.setScreenState("error", e.getMessage(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.getRequestDispatcher("modify_license.jsp").forward(request, response);
				}  catch (ParseException |DatabaseException |TenjinConfigurationException e) {
					request.setAttribute("systemId",systemId);
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.getRequestDispatcher("modify_license.jsp").forward(request, response);
				} 
			}else{
				SessionUtils.setScreenState("error", "license.key.invalid", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("modify_license.jsp").forward(request, response);
			}
		}else if(param!=null &&param.equalsIgnoreCase("modify")){
			if(licenseKey!=null){
				try {
					License license=licenseHandler.validateLicenseKey(licenseKey,false);
					licenseHandler.updateLicenseKey(licenseKey);
					licenseHandler.upadateLicenseInCache(license);
					licenseHandler.sendLicenseNotificationMail(license,"Modified");
					SessionUtils.setScreenState("success", "license.modify.success", request);
					response.sendRedirect("LicenseServlet?param=view&systemId="+systemId);
				}  catch (TenjinServletException |LicenseExpiredException|LicenseInactive e ) {
					request.setAttribute("systemId",systemId);
					SessionUtils.setScreenState("error", e.getMessage(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.getRequestDispatcher("modify_license.jsp").forward(request, response);
				}  catch (ParseException |DatabaseException  e) {
					request.setAttribute("systemId",systemId);
					SessionUtils.setScreenState("error", "generic.db.error", request);
					SessionUtils.loadScreenStateToRequest(request);
					request.getRequestDispatcher("modify_license.jsp").forward(request, response);
				} 
			}else{
				SessionUtils.setScreenState("error", "license.key.invalid", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("modify_license.jsp").forward(request, response);
			}
		
		}
		
		
		
}
}