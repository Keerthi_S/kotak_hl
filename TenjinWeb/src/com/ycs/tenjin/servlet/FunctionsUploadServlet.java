/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  FunctionsUploadServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 12-04-2019		   Pushpalatha			   TENJINCG-1030
* 14-06-2019           Padmavathi              V2.8-79
* 18-06-2019		   Ashiki				   V2.8-144
*/

/*27-Mar-2015 R2.1: for Defect ##1324-By Babu: Upload TestCases and Teststeps is not uploading at server location: Starts*/
package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.Tika;
import org.slf4j.Logger;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.util.ExcelHandler;

/**
 * Servlet implementation class UploadServlet
 */
/*@WebServlet(name = "/UploadServlet", urlPatterns = { "//UploadServlet" })*/
public class FunctionsUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(UploadServlet.class);


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FunctionsUploadServlet() {
		super();
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String dUrl = "";
		Map<String, Object> map = new HashMap<String, Object>();
		try{
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);

			/*modified by paneendra for VAPT fix starts*/
			String rootPath=TenjinConfiguration.getProperty("TJN_WORK_PATH");
			File dir = new File(rootPath + "\\Uploads");
			/*modified by paneendra for VAPT fix ends*/
			if(!dir.exists()){
				dir.mkdirs();
			}
			List items = upload.parseRequest(request);
			Iterator iterator = items.iterator();
			String parent="";
			String fileName="";
			while(iterator.hasNext()){
				FileItem xlFile = (FileItem)iterator.next();
				if(!xlFile.isFormField()){
					fileName= xlFile.getName();
					 
                   
					/*Added by padmavathi for V2.8-79 starts*/
					if(fileName.contains("\\")){
						String file[]=null;
						file=fileName.split("\\\\");
						fileName=file[file.length-1];
					}
					/*Added by padmavathi for V2.8-79 ends*/
					/*modified by paneendra for VAPT fix starts*/
					File uploadedFileRP = new File(rootPath+ "\\Uploads\\" + fileName);
					/*modified by paneendra for VAPT fix ends*/
					xlFile.write(uploadedFileRP);
				}
				else
				{
					parent=xlFile.getString();
				}
			}
			/*modified by paneendra for VAPT fix starts*/
			String folderPath = rootPath + "\\Uploads\\";
			/*modified by paneendra for VAPT fix ends*/
			  //	Added  By Priyanka for VAPT Starts
			
			Tika tika = new Tika();
			String Exfile ="";
			try {
			Exfile = tika.detect(folderPath+File.separator+fileName);
			}catch(Exception e) {
				logger.debug("Unable to Detect the File Content: " + e.getMessage());
			}
			   if(!(Exfile.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))){
				   throw new RequestValidationException("Invalid Excel sheet");
				   
			   }
			   //	Added  By Priyanka for VAPT ends
			String fileNameWithOutExt = FilenameUtils.removeExtension(fileName);
			String file=fileNameWithOutExt+"-output."+fileName.substring(fileName.lastIndexOf('.') + 1);
			file=StringUtils.deleteWhitespace(file);
			
			
			ExcelHandler e=new ExcelHandler();
			
			
			
			e.processFunctionsUpload(folderPath, fileNameWithOutExt);
			
			
			/*Modified by Ashiki for V2.8-144 starts*/
			map.put("message", "Function(s) Uploaded Successfully");
			/*Modified by Ashiki for V2.8-144 ends*/
			map.put("status", "SUCCESS");
			/*Modified by Padmavathi for V2.8-79 starts*/
			/*map.put("path","./Uploads/"+file);*/
			map.put("path",File.separator+"Uploads"+File.separator+File.separator+file);
			/*Modified by Padmavathi for V2.8-79 ends*/
			map.put("tc_id", "");
			
		}//	Added  By Priyanka for VAPT starts
		catch (RequestValidationException e) {
			map.put("message", "Please upload a valid Excel sheet");
			map.put("status", "ERROR");
		}
       //Added  By Priyanka for VAPT ends
		catch(Exception e){
			logger.error("An exception occured while uploading functions",e);
			map.put("status", "ERROR");
			map.put("message","An Internal Error Occurred. Please try again");
		}
		request.getSession().setAttribute("SCR_MAP", map);
		dUrl = "uploadtempFunctions.jsp";
		response.sendRedirect(dUrl);
	}

}

