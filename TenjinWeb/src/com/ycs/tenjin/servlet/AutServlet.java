/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AutServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 28-10-2016           Parveen                  Defect #648
 * 3-Nov-2016			Sriram					Fix for Defect#701
 * 4-Nov-2016			Leelaprasad				Fix for Defect#559
 * 08-Nov-2016           Leelaprasad             Fix for Defect#774
 * 10-Nov-2016           Leelaprasad            Fix for Defect#823
 * 15-11-2016            Parveen                 Defect #665
 * 16-Nov-16			    Sriram						Enhancement#665 (Fetch Operations for App)
 * 24-11-2016            Parveen                Enhancement#665
 * 25-11-2016            Leelaprasad             aut Validations
 * 17-12-2016           Leelaprasad            Fix for Defect#TEN-105
 * 19-12-2016           Leelaprasad            Fix for Defect#TEN-105
 * 17-02-2017           Abhilash K N            Fix for Defect#TENJINCG-133
 * 20-06-2017           Padmavathi              TENJINCG-195
 * 09-08-2017           Leelaprasad            defect fix T25IT-48 
 * 11-08-2017			Roshni					T25IT-111
 * 19-08-2017			Sriram					T25IT-102
 * 31-08-2017           Leelaprasad             T25IT-422
22-Sep-2017			sameer gupta				For new adapter spec
 24-Oct-2017			 Sahana					new design change for TENJINCG-375
   02-02-2018			Pushpalatha				TENJINCG-568
   14-06-2018            Padmavathi               T251IT-23
   15-06-2018            Padmavathi               T251IT-25
   15-06-2018			 Preeti					 T251IT-46
   03-07-2018            Padmavathi               T251IT-170
   08-04-2019            Padmavathi               TNJN27-75 
 * */

package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ModuleBean;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

/* Change History
 * 
 * 02-Mar-2015 R2.1 Database agnostic Changes (Babu)
 * 02-Mar-2015 Authentication Changes (Mahesh): User/AUT/Schema Password Encryption/Decryption changes
 *
 */
/**
 * Servlet implementation class AutServlet
 */
// @WebServlet("/AutServlet")
public class AutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(AutServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AutServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		String txn = request.getParameter("param");
		if (txn.equalsIgnoreCase("fetch_operations_for_app")) {
			String aId = request.getParameter("app");
			String ret = "";
			try {
				JSONObject json = new JSONObject();
				try {
					int appId = Integer.parseInt(aId);
					Aut aut = new AutHelper().hydrateAut(appId);

					json.put("status", "SUCCESS");
					json.put("message", "");
					JSONArray opsArray = new JSONArray();
					String ops = aut.getOperation();
					if (!Utilities.trim(ops).equalsIgnoreCase("")) {
						String[] opSplit = ops.split(",");
						for (String op : opSplit) {
							JSONObject o = new JSONObject();
							o.put("code", op);
							opsArray.put(o);
						}

						json.put("operations", opsArray);
					}

					ret = json.toString();
				} catch (NumberFormatException e) {
					
					logger.error("Invalid app id [{}]", aId);
					json.put("status", "ERROR");
					json.put("message", "Invalid Application ID [" + aId + "]. Could not fetch Operations.");
					ret = json.toString();
				} catch (DatabaseException e) {
					
					logger.error("Invalid app id [{}]", aId);
					json.put("status", "ERROR");
					json.put("message", e.getMessage());
					ret = json.toString();
				} catch (Exception e) {
					logger.error("Could not fetch operations", e);
					json.put("status", "ERROR");
					json.put("message", "Could not fetch Operations. Please contact Tenjin Support.");
					ret = json.toString();
				}
			} catch (JSONException e) {
				logger.error("JSONException caught while fetching operations for app {}.", aId, e);
				ret = "{status:ERROR,message:Could not fetch Operations. Please contact Tenjin Support.";
			}
			response.getWriter().write(ret);

		} /******************************
			 * Added by Sriram for Enhancement#665 (Fetch Operations for App) ends
			 */

		/* Added by Parveen for Enhancement#665 on 24-11-2016 starts */
		else if (txn.equalsIgnoreCase("fetch_aut_user_type_for_app")) {
			String aId = request.getParameter("app");
			String ret = "";
			try {
				JSONObject json = new JSONObject();
				try {
					int appId = Integer.parseInt(aId);
					Aut aut = new AutHelper().hydrateAut(appId);

					json.put("status", "SUCCESS");
					json.put("message", "");
					JSONArray userTypeArray = new JSONArray();

					String userType = aut.getLoginUserType();
					if (!Utilities.trim(userType).equalsIgnoreCase("")) {
						String[] userTypeSplit = userType.split(",");
						for (String uType : userTypeSplit) {
							JSONObject user = new JSONObject();
							user.put("code", uType);
							userTypeArray.put(user);
						}

						json.put("appUserTypes", userTypeArray);
					}

					ret = json.toString();
				} catch (NumberFormatException e) {
					
					logger.error("Invalid app id [{}]", aId);
					json.put("status", "ERROR");
					json.put("message", "Invalid Application ID [" + aId + "]. Could not fetch User Types.");
					ret = json.toString();
				} catch (DatabaseException e) {
					
					logger.error("Invalid app id [{}]", aId);
					json.put("status", "ERROR");
					json.put("message", e.getMessage());
					ret = json.toString();
				} catch (Exception e) {
					logger.error("Could not fetch User Types", e);
					json.put("status", "ERROR");
					json.put("message", "Could not fetch User Types. Please contact Tenjin Support.");
					ret = json.toString();
				}
			} catch (JSONException e) {
				logger.error("JSONException caught while fetching User Types for app {}.", aId, e);
				ret = "{status:ERROR,message:Could not fetch User Types. Please contact Tenjin Support.";
			}
			response.getWriter().write(ret);

		}
		/* Added by Pushpalatha for Req#TENJINCG-568 starts */
		else if (txn.equalsIgnoreCase("fetch_aut_groups")) {
			String aId = request.getParameter("app");
			String ret = "";
			try {
				JSONObject json = new JSONObject();
				try {
					int appId = Integer.parseInt(aId);

					ArrayList<String> groups = new ModulesHelper().fetch_all_groups(appId);
					json.put("GROUPS", groups);
					json.put("status", "SUCCESS");
					json.put("message", "");

					ret = json.toString();
				} catch (NumberFormatException e) {
					
					logger.error("Invalid app id [{}]", aId);
					json.put("status", "ERROR");
					json.put("message", "Invalid Application ID [" + aId + "]. Could not fetch Groups.");
					ret = json.toString();
				} catch (DatabaseException e) {
					
					logger.error("Invalid app id [{}]", aId);
					json.put("status", "ERROR");
					json.put("message", e.getMessage());
					ret = json.toString();
				} catch (Exception e) {
					logger.error("Could not fetch User Types", e);
					json.put("status", "ERROR");
					json.put("message", "Could not fetch User Types. Please contact Tenjin Support.");
					ret = json.toString();
				}
			} catch (JSONException e) {
				logger.error("JSONException caught while fetching Groups for app {}.", aId, e);
				ret = "{status:ERROR,message:Could not fetch User Types. Please contact Tenjin Support.";
			}
			response.getWriter().write(ret);

		}
		/* Added by Pushpalatha for Req#TENJINCG-568 ends */
		/* Added by Parveen for Enhancement#665 on 24-11-2016 ends */

		else if (txn.equalsIgnoreCase("fetch_user_aut_for_learning")) {
			String retData = "";
			String appid = request.getParameter("app");

			ArrayList<Aut> userAutCreds = null;
			ArrayList<Aut> otherAutCreds = null;
			JSONObject json = new JSONObject();
			try {
				try {
					userAutCreds = new AutHelper().hydrateAllUserAut(tjnSession.getUser().getId(),
							Integer.parseInt(appid));
					otherAutCreds = new AutHelper().hydrateAllUserAutNotForUser(tjnSession.getUser().getId(),
							Integer.parseInt(appid));

					JSONArray uCreds = new JSONArray();

					for (Aut aut : userAutCreds) {
						JSONObject j = new JSONObject();
						j.put("loginname", aut.getLoginName());
						j.put("loginType", aut.getLoginUserType());
						uCreds.put(j);
					}

					json.put("ucreds", uCreds);

					JSONArray oCreds = new JSONArray();
					for (Aut aut : otherAutCreds) {
						JSONObject j = new JSONObject();
						j.put("loginname", aut.getLoginName());
						j.put("loginType", aut.getLoginUserType());
						oCreds.put(j);
					}

					json.put("ocreds", oCreds);
					json.put("status", "SUCCESS");
					json.put("message", "");
				} catch (NumberFormatException e) {

					json.put("status", "ERROR");
					json.put("message", "Could not fetch Login Credentials for the selected application");
					logger.error("Could not fetch Login credentials for app ID --> " + appid);
					logger.error("", e);
				} catch (DatabaseException e) {

					json.put("status", "ERROR");
					json.put("message", "Could not fetch Login Credentials for the selected application");
					logger.error("Could not fetch Login credentials for app ID --> " + appid);
					logger.error("", e);
				}

				retData = json.toString();
			} catch (JSONException e) {

				retData = "An Internal Error Occurred. Please contact your system administrator";
				logger.error("Could not form User AUT Credentials JSON --> ", e);
			}

			response.getWriter().write(retData);
		}

		else if (txn.equalsIgnoreCase("fetch_prerequisites")) {
			response.sendRedirect("prerequisites.jsp");
		} else if (txn.equalsIgnoreCase("fetch_faq")) {
			response.sendRedirect("faq.jsp");
		}
		/* Added by Leelaprasad for the Requirement Defect#823 on 10-11-2016 ends */
		else if (txn.equalsIgnoreCase("fetch_all_auts")) {
			String retData = "";
			try {
				JSONObject json = new JSONObject();
				try {
					ArrayList<Aut> auts = new AutHelper().hydrateAllAut();
					JSONArray jArray = new JSONArray();
					for (Aut aut : auts) {
						JSONObject j = new JSONObject();
						j.put("id", aut.getId());
						j.put("name", aut.getName());
						j.put("loginreqd", aut.getLoginReqd());
						j.put("loginname", aut.getLoginName());
						j.put("password", aut.getPassword());
						j.put("url", aut.getURL());
						j.put("browser", aut.getBrowser());
						jArray.put(j);
					}

					json.put("status", "SUCCESS");
					json.put("message", "");
					json.put("auts", jArray);
				} catch (Exception e) {
					logger.error("An error occurred while processing AUTs", e);
					json.put("status", "ERROR");
					json.put("message",
							"An internal error occurred. Please try again.");

				} finally {
					retData = json.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);
		}
		else  if (txn.equalsIgnoreCase("fetch_modules_for_app")) {
			String app = request.getParameter("app");
			String retData = "";
			try {
				JSONObject json = new JSONObject();
				Connection conn = null;
				try {
					/******************************************************************
					 * Change by Sriram to fix slow loading of AUT Functions
					 * Make only a single DB Connection for all functions
					 * 08-10-2015 (Tenjin v2.2)
					 */
					conn = DatabaseHelper
							.getConnection(Constants.DB_TENJIN_APP);
					
					ArrayList<ModuleBean> modules = new ModulesHelper()
							.hydrateModules(conn, Integer.parseInt(app));
					if (modules != null && modules.size() > 0) {
						JSONArray jArray = new JSONArray();
						for (ModuleBean m : modules) {
							JSONObject js = new JSONObject();
							js.put("code", m.getModuleCode());
							js.put("name", m.getModuleName());
							if (m.getMenuContainer() == null) {
								js.put("menu", "");
							} else {
								js.put("menu", m.getMenuContainer());
							}
							/* 20-Oct-2014 WS: Starts */
							js.put("wsurl", m.getWSURL());
							js.put("wsop", m.getWSOp());
							/* 20-Oct-2014 WS: Ends */
							/* 20-Oct-2014 WS: Starts */
							// ArrayList<Location> locations =
							// helper.hydrateLocations(m.getModuleCode(),
							// Integer.parseInt(app));
							/******************************************************************
							 * Change by Sriram to fix slow loading of AUT
							 * Functions Make only a single DB Connection for
							 * all functions 08-10-2015 (Tenjin v2.2)
							 */
							
							boolean learnt = new AutHelper().isFunctionLearnt(conn,
									m.getModuleCode(), Integer.parseInt(app));
							if (learnt) {
								js.put("learnt", "YES");
								
								js.put("lastlearnt", m.getLastLearntTimestamp());
							} else {
								js.put("learnt", "NO");
								js.put("lastlearnt", "Not Available");
							}
							/******************************************************************
							 * Change by Sriram to fix slow loading of AUT
							 * Functions Make only a single DB Connection for
							 * all functions 08-10-2015 (Tenjin v2.2) ENDS
							 */

							jArray.put(js);
						}

						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("modules", jArray);
					} else {
						json.put("status", "FAILURE");
						/*Changed by Padmavathi for TNJN27-75 starts*/
						
						json.put("message",
								"No function(s) available for this application");
						/*Changed by Padmavathi for TNJN27-75 ends*/

					}
				} catch (Exception e) {
					logger.error("An error occurred while processing AUTs", e);
					json.put("status", "ERROR");
					json.put("message",
							"An internal error occurred. Please try again.");

				} finally {
					retData = json.toString();
					if (conn != null) {
						try {
							conn.close();

						} catch (Exception e) {

						}
					}
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);
			/* 20-Oct-2014 WS: Starts */
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
