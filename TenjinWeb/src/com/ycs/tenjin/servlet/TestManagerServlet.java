/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestManagerServlet


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 18-02-2019			Ashiki				   	TJN252-60
* 26-03-2019			Roshni					TJN252-54
* 22-05-2019			Ashiki					V2.8-31,V2.8-32
* 06-06-2019			Prem					V2.8-89
* 07-06-2019			Ashiki					V2.8-118
* 04-07-2019            Leelaprasad P           TV2.8R2-30
* 04-12-2019			Preeti					TENJINCG-1174
* 22-06-2021           Paneendra               VAPT FIX
*/	
/*
 * Added File by Parveen for Requirement TENJINCG_53
 */
package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.db.TestManagerHelper;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.testmanager.TestManagerFactory;
import com.ycs.tenjin.testmanager.TestManagerInstance;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class TestManagementServlet
 */
@WebServlet("/TestManagementServlet")
public class TestManagerServlet extends HttpServlet {
	private static final Logger logger = LoggerFactory
			.getLogger(TestManagerServlet.class);
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestManagerServlet() {
        super();
        
    }
    
    public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		@Override
		public boolean checkAccess(HttpServletRequest request) {
			if (request.getMethod().equalsIgnoreCase("GET")) {
				String task = Utilities.trim(request.getParameter("param"));
				
				if (Utilities.trim(task).equalsIgnoreCase("NEW_TM_INSTANCE") || Utilities.trim(task).equalsIgnoreCase("TMtool") 
						|| Utilities.trim(task).equalsIgnoreCase("VIEW_TM") || Utilities.trim(task).equalsIgnoreCase("CHECK_NAME") 
						|| Utilities.trim(task).equalsIgnoreCase("FETCH_ALL") || Utilities.trim(task).equalsIgnoreCase("delete") 
						|| Utilities.trim(task).equalsIgnoreCase("deleteall") || Utilities.trim(task).equalsIgnoreCase("EDIT_TM")
						/*Added by paneendra for VAPT Fix starts*/
						|| Utilities.trim(task).equalsIgnoreCase("new")
						/*Added by paneendra for VAPT Fix ends*/) {
					if (new AdminAuthorizationRuleImpl().checkAccess(request)) {
						return true;
					}
					return false;
				} else {
					return true;
				}
			}else if (request.getMethod().equalsIgnoreCase("POST")) {
				return true;
			}
			return false;
		}

	};

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		TenjinSession tjnSession = (TenjinSession) request.getSession()
				.getAttribute("TJN_SESSION");
		String txn = request.getParameter("param");
		/*Added by paneendra for VAPT Fix starts*/
		Authorizer authorizer=new Authorizer();
		if(txn.equalsIgnoreCase("NEW_TM_INSTANCE") || txn.equalsIgnoreCase("VIEW_TM") || txn.equalsIgnoreCase("EDIT_TM") || txn.equalsIgnoreCase("new") || txn.equalsIgnoreCase("FETCH_ALL") || txn.equalsIgnoreCase("delete") || txn.equalsIgnoreCase("deleteall")) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		/*Added by paneendra for VAPT Fix ends*/
		 if (txn.equalsIgnoreCase("NEW_TM_INSTANCE")) {
			
			logger.debug("Creating New TestManager Instance");
			
			String jsonString = request.getParameter("json");
			logger.debug("JSON String: " + jsonString);
			
			String retData = "";
			try {
				
				JSONObject json = new JSONObject(jsonString);
				
				TestManagerInstance tm = new TestManagerInstance();

				
				tm.setInstanceName(json.getString("name"));
				tm.setTool(json.getString("tool"));
				tm.setURL(json.getString("url"));
				
				TestManagerHelper helper = new TestManagerHelper();
				int checkName=helper.checkName(tm.getInstanceName());
				
				if(checkName==0 ){
					
					tm = helper.persistTestManager(tm);
					
					JSONObject r1 = new JSONObject();
					r1.put("status","success");
					/*Changed by Ashiki for V2.8-31 starts*/
					/*r1.put("message","Added TM instance Successfully");*/
					r1.put("message","Test Management Instance created Successfully");
					/*Changed by Ashiki for V2.8-31 ends*/
					retData = r1.toString();
					}
			else{
					JSONObject r = new JSONObject();
					r.put("status", "ERROR");
					r.put("message", " User Already Exist AS: "+tm.getInstanceName());
					retData = r.toString();
				
			} 
				
								
		}catch (Exception e) {
			
			
			JSONObject r = new JSONObject();
					try {
						r.put("status", "ERROR");
					    r.put("message",
							"Could not create Test Management instance due to an internal error");
					retData = r.toString();
				
					} catch (JSONException e1) {
						retData = "An internal error occurred. Please contact your System Administrator ";
						
					}
				}
			response.getWriter().write(retData);
		
	}
	
			else  if(txn.equalsIgnoreCase("TMtool")){
				
				
				String retData;
				try {

					JSONObject json = new JSONObject();
					try {
						/*ArrayList<String> rc1 = helper.hydrateDttTool();*/
						List<String> rc1 = TestManagerFactory.getAvailableTools();
						
						JSONArray jArray = new JSONArray();
						for (String  reg: rc1) {
							JSONObject j = new JSONObject();
							j.put("tmname", reg);
							jArray.put(j);
						}

						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("tools", jArray);
					} catch (Exception e) {
						logger.error("An error occurred while processing tools", e);
						json.put("status", "ERROR");
						json.put("message",
								"An internal error occurred. Please try again.");

					} finally {
						retData = json.toString();
					}
				} catch (Exception e) {
					retData = "An internal error occurred. Please contact your System Administrator";
				}
				response.getWriter().write(retData);
			}
		 
		 else if (txn.equalsIgnoreCase("VIEW_TM")) {
				logger.debug("View Test MAnager  Details");
				
				String name = request.getParameter("paramval");

				Map<String, Object> map = new HashMap<String, Object>();
				try {
					TestManagerHelper helper = new TestManagerHelper();
					TestManagerInstance tm1 =helper.hydrateTestManagerInstance(name);
											
					if (tm1 != null) {
						map.put("STATUS","");
						map.put("MESSAGE","");
						map.put("TM_BEAN", tm1);
						
					} else {
						map.put("STATUS", "FAILURE");
						map.put("MESSAGE", "tm module not found");
					}
				} catch (Exception e) {
					map.put("STATUS", "ERROR");
					map.put("MESSAGE",
							"An Internal Error occurred. Please contact your System Administrator");
				} finally {
					request.getSession().removeAttribute("SCR_MAP");
					request.getSession().setAttribute("SCR_MAP", map);
				}
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("testmanager_view.jsp");
				dispatcher.forward(request, response);
			} 
			else if (txn.equalsIgnoreCase("CHECK_NAME")) {
				logger.debug("Checking for instance name is already exists....");
				JSONObject resJSON = new JSONObject();
				String name = request.getParameter("paramval");
				String retData = "";
				try {
					try {
						TestManagerHelper helper = new TestManagerHelper();
						int i = helper.checkName(name);
						if (i >0) {
							resJSON.put("status", "ERROR");
							resJSON.put("message", "The name you entered is already in use. Please use a different name");
						}
					} catch (Exception e) {
						resJSON.put("status", "ERROR");
						resJSON.put("message",
								"Could not update details due to an internal error");
					} finally {
						retData = resJSON.toString();
					}
				} catch (Exception e) {
					retData = "An internal error occurred. Please contact your System Administrator ";
				}
				response.getWriter().write(retData);
			}
    
       else  if(txn.equalsIgnoreCase("TmInstances")){
    	   logger.debug("Getting the TM Instances");
    	   String name = request.getParameter("param");
    	   String etmprjtool = request.getParameter("etmprjtool");
    	   
    	   /*Added by Ashiki for TJN252-60 starts*/
    	   Project project=tjnSession.getProject();
    	   Project instance =new Project();
    	   /*Added by Ashiki for TJN252-60 ends*/
    	   /*String etmprjprj = request.getParameter("etmprjprj");
    	   String etmprjenable = request.getParameter("etmprjenable");
    	   String etmprjinstance = request.getParameter("etmprjinstance");*/
    	   
    	   
    	   
    	   TestManagerHelper helper = new TestManagerHelper();
			Map<String,Object> map = new HashMap<String,Object>();
			List<String> instances = new ArrayList<String>();
			/*added by Roshni */
			List<TestManagerInstance> tminstances=new ArrayList<>();
			Project projectForETM =null;
			try{
				instances = helper.fetchTMInstances();
				/*Added by Ashiki for TJN252-60 starts*/
				ProjectHandler projectHandler=new ProjectHandler();
				projectForETM = projectHandler.hydrateProjectETMDetails(Integer.toString(project.getId()));
				/*Added by Ashiki for TJN252-60 ends*/
				/*added by Roshni */
				tminstances=helper.hydrateAllTmInstances(project.getId());
				projectForETM.setTmInstances(tminstances);
				 
			
			}
			catch (DatabaseException e) {
				
				
			}
			
			
			map.put("instances", instances);
			tjnSession.setProject(projectForETM);
            request.getSession().setAttribute("TJN_SESSION", tjnSession);
            request.getSession().setAttribute("SCR_MAP", map);
			request.getRequestDispatcher("testmgr_integration.jsp").forward(request, response);
		}/*Added by Ashiki for TJN252-60 starts*/
		  else  if(txn.equalsIgnoreCase("TEST_MANAGER_INTEGRATION")){

	    	   logger.debug("Getting the TM Instances");
	    	   String etmprjtool = request.getParameter("etmprjtool");
	    	   Project project=tjnSession.getProject();
	    	   Project instance =new Project();
	    	   TestManagerHelper helper = new TestManagerHelper();
				Map<String,Object> map = new HashMap<String,Object>();
				List<String> instances = new ArrayList<String>();
				try{
					instances = helper.fetchTMInstances();
					instance=helper.hydrateInstance(project.getId());
				}
				catch (DatabaseException e) {
					
					
				}
				if(instance!=null){
					/*project.setEtmInstance(projectForETM.getEtmInstance());
					project.setEtmEnable(projectForETM.getEtmEnable());
					project.setEtmProject(projectForETM.getEtmProject());
					project.setTcFilter(projectForETM.getTcFilter());
					project.setTsFilter(projectForETM.getTcFilter());
					project.setTcFilterValue(projectForETM.getTcFilterValue());
					project.setTsFilterValue(projectForETM.getTsFilterValue());*/
				}
				map.put("instances", instances);
				map.put("etmprjtool", etmprjtool);
				map.put("MESSAGE", "Filter values updated successfully");
				//project.setName(projectForETM.getName());
				request.getSession().setAttribute("SCR_MAP", map);
				
	            request.getRequestDispatcher("testmgr_integration.jsp").forward(request, response);
				
			
		}/*Added by Ashiki for TJN252-60 ends*/
			else if (txn.equalsIgnoreCase("FETCH_ALL")) {
				logger.debug("Fetching All TM Management list");
				ArrayList<TestManagerInstance> testManagerList = new ArrayList<TestManagerInstance>();
				Map<String, Object> map = new HashMap<String, Object>();

				TestManagerHelper tmHelper = null;

				tmHelper = new TestManagerHelper();

				if (tmHelper != null) {
					try{
						testManagerList = tmHelper.hydrateAllTestManager();
					}
					catch (DatabaseException e) {
						
						
					}
					map.put("TM_STATUS", "SUCCESS");
					/*Added by Ashiki for V2.8-118 starts*/
					String message = request.getParameter("message");
					if(message!=null) {
						map.put("MESSAGE", message);
					}else {
						map.put("MESSAGE", "");
					}
					/*Added by Ashiki for V2.8-118 ends*/
					map.put("TM_LIST", testManagerList);
				}

				request.getSession().setAttribute("SCR_MAP", map);
				
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("testmanager_list.jsp");
				dispatcher.forward(request, response);
				
			
			}
			else if (txn.equalsIgnoreCase("delete")) {
				logger.debug("Deleting Test Manager Record");
				String name = request.getParameter("paramval");
				
				
				JSONObject retJson = new JSONObject();
				String retData = "";
				try {

					try {
						TestManagerHelper helper = new TestManagerHelper();
						helper.deleteTestManagerInstance(name);
						/*Added by Ashiki for TJN252-60 Starts*/
						helper.updateTmFilters(name);
						/*Added by Ashiki for TJN252-60 ends*/
						helper.deleteInstanceCredentials(name);
						retJson.put("status", "SUCCESS");
						/*Changed by Ashiki for V2.8-32 starts*/
						/*retJson.put("message", "Record Deleted Successfully");*/
//				Changed by Leelaprasad for the issue TV2.8R2-30 starts
						/*retJson.put("message", "Test Mangement Instance Deleted Successfully");
					*/
						retJson.put("message", "Test Management Instance deleted Successfully");
						/*Changed by Leelaprasad for the issue TV2.8R2-30 ends*/
						/*Changed by Ashiki for V2.8-32 end*/

					} catch (DatabaseException e) {
						retJson.put("status", "ERROR");
						/*Changed by Ashiki for V2.8-32 starts*/
						/*retJson.put("message", "Could not delete records");*/
						retJson.put("message", "Could not delete Test Mangement Instance");
						/*Changed by Ashiki for V2.8-32 ends*/
					} finally {
						retData = retJson.toString();
					}
				} catch (Exception e) {
					retData = "An internal error occurred. Please contact your System Administrator";
				}
				response.getWriter().write(retData);
			}
			else if (txn.equalsIgnoreCase("deleteall")) {
				logger.debug("Delete All Test Manager List");
				TestManagerHelper helper = null;
				String names = request.getParameter("paramval");
						
				String[] record = names.split(",");
				
				JSONObject retJson = new JSONObject();
				String retData = "";
				try {

					try {
						helper = new TestManagerHelper();
						helper.clearAllTestManagerInstance(record);
						/*Added by Ashiki for TJN252-60 Starts*/
						helper.updateTmFilters(record);
						/*Added by Ashiki for TJN252-60 ends*/
						helper.deleteMultipleInstanceCredentials(record);
						retJson.put("status", "SUCCESS");
						retJson.put("message", "All Records Deleted Successfully");

					} catch (DatabaseException e) {
					
						retJson.put("status", "ERROR");
						retJson.put("message", "Could not delete test manager instances");
					} finally {
						retData = retJson.toString();
					}
				} catch (Exception e) {
					retData = "An internal error occurred. Please contact your System Administrator";
				}
				response.getWriter().write(retData);
			}
			else if (txn.equalsIgnoreCase("EDIT_TM")) {
				logger.debug("Editing TM Details");
				String json = request.getParameter("json");
				String json1 = request.getParameter("json1");
				JSONObject resJSON = new JSONObject();
				String retData = "";
				try {
					JSONObject j1 = new JSONObject(json1);
					TestManagerInstance tm1 = new TestManagerInstance();
						tm1.setInstanceName(j1.getString("name1"));
						tm1.setTool(j1.getString("tool1"));
						tm1.setURL(j1.getString("url1"));
					try {
						JSONObject j = new JSONObject(json);
						TestManagerInstance tm = new TestManagerInstance();
						Map<String,Object> map = (Map<String,Object>)request.getSession().getAttribute("SCR_MAP");
						TestManagerInstance tm2 = (TestManagerInstance)map.get("TM_BEAN");
						
						String exName=tm2.getInstanceName();
						
						tm.setInstanceName(j.getString("name"));
						tm.setTool(j.getString("tool"));
						tm.setURL(j.getString("url"));
						
						

						new TestManagerHelper().updateTestManager(tm,exName);

						resJSON.put("status","success");
						resJSON.put("message","Test Management Instance Updated Successfully");
						} catch (Exception e) {
							logger.error("Error ", e);
							resJSON.put("status", "ERROR");
							resJSON.put("message",
									"Could not update tm due to an internal error ");
						} finally {
							retData = resJSON.toString();
						}
					} catch (Exception e) {
						retData = "An internal error occurred. Please contact your System Administrator";
					}

				response.getWriter().write(retData);
					
			}else if(txn.equalsIgnoreCase("check_credentials")){
				String tjnUser = request.getParameter("user");
				String instance = request.getParameter("instance");
				ProjectHelper helper = new ProjectHelper();
				JSONObject json = new JSONObject();
				try {
					helper.checkinstanceCredintials(tjnUser, instance);
					try {
						json.put("status", "success");
					} catch (JSONException e) {
						
					}
				} catch (DatabaseException e) {
					try {
						logger.error(e.getMessage(),e);
						json.put("status", "error");
						json.put("message", instance+" does not have credentials. Please provide credentials and try again.");
					} catch (JSONException e1) {
						
					}
					
				}
				
				response.getWriter().write(json.toString());
				
			}
		 /*Added by paneendra for VAPT Fix starts*/
			else if(txn.equalsIgnoreCase("new")){
				RequestDispatcher dispatcher = request.getRequestDispatcher("testmanager_new.jsp");
				dispatcher.forward(request, response);		
			}
			/*Added by paneendra for VAPT Fix ends*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
