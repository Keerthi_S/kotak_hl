/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TenjinSessionListener.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 12-10-2018        Leelaprasad P          TENJINCG-872 re written
*/


package com.ycs.tenjin.servlet.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.user.User;

/**
 * Application Lifecycle Listener implementation class TenjinSessionListener
 *
 */
@WebListener
public class TenjinSessionListener implements HttpSessionListener {

	private static final Logger logger = LoggerFactory.getLogger(TenjinSessionListener.class);
	
    /**
     * Default constructor. 
     */
    public TenjinSessionListener() {
        
    }

	/**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent arg0)  { 
         
    }

	/**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent arg0)  {
   	
    	TenjinSession tjnSession = (TenjinSession) arg0.getSession().getAttribute("TJN_SESSION");
    	if(tjnSession==null){
    		logger.info("Session is null");
    	}else{
    	UserHandler userHandler=new UserHandler(); 
    	try {
    		User user=tjnSession.getUser();
    		if(user!=null){
    			String[] users={tjnSession.getUser().toString()};
    			logger.error("SESSION IS TIMED-OUT");
    			userHandler.clearUsers(users);
    		}
    
		} catch (DatabaseException e) {
			
			logger.error("Error ", e);
		}
    	
    	}
    	
    }
   
}
