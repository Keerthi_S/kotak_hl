/***

 Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCaseServletNew.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
*08-03-2018			   	Pushpalatha			  	Newly Added 
*20-03-2018				Preeti					TENJINCG-611(Test case execution history)
*24-03-2018				Pushpalatha				TENJINCG-652
*09-05-2018             Padmavathi              TENJINCG-646
*04-05-2018				Pushpalatha				TENJINCG-679
*06-05-2018				Padmavathi				TENJINCG-679
*19-06-2018             Padmavathi              T251IT-65
*18-09-2018				Sriram					TENJINCG-735
*28-09-2018				Leelaprasad			    TENJINCG-738
*24-10-2018				Padmavathi				TENJINCG-849
*23-10-2018				Preeti					TENJINCG-850
*02-11-2018				Pushpa					TENJINCG-897
*07-11-2018             Padmavathi              TENJINCG-893
*04-12-2018             Padmavathi              TJNUN262-3
*13-02-2019				Preeti					TENJINCG-970
*18-02-2019				Ashiki					TJN252-60
*19-02-2019				Preeti					TENJINCG-969
*28-02-2019             Padmavathi              TENJINCG-942
*01-03-2019             Padmavathi              TENJINCG-942
*11-03-2019             Padmavathi              TENJINCG-997
*26-03-2019				Roshni					TJN252-54
*29-03-2019				Preeti					TENJINCG-1003
*10-04-2019				Ashiki					TENJINCG-1029
*23-04-2019				Ashiki					TJN252-54
*02-05-2019				Roshni					TENJINCG-1046
*03-05-2019             Padmavathi              TENJINCG-1050
*18-05-2019				Ashiki					TENJINCG_1215,TENJINCG_1214
*27-05-2020				Ashiki					ALM import
*05-06-2020				Ashiki					Tenj210-108
*19-11-2020             Priyanka                TENJINCG-1231
*/

package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.TestCaseHelper;
import com.ycs.tenjin.exception.DuplicateRecordException;
import com.ycs.tenjin.exception.RecordNotFoundException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.handler.ProjectMailHandler;
import com.ycs.tenjin.handler.TenjinMailHandler;
import com.ycs.tenjin.handler.TestCaseHandler;
import com.ycs.tenjin.handler.TestStepHandler;
import com.ycs.tenjin.handler.UserHandler;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.testmanager.ETMException;
import com.ycs.tenjin.testmanager.TestManager;
import com.ycs.tenjin.testmanager.TestManagerFactory;
import com.ycs.tenjin.testmanager.TestManagerInstance;
import com.ycs.tenjin.testmanager.UnreachableETMException;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class TestCaseServletNew
 */
@WebServlet("/TestCaseServletNew")
public class TestCaseServletNew extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(TestCaseServlet.class);   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestCaseServletNew() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SessionUtils.loadScreenStateToRequest(request);
		String task = Utilities.trim(request.getParameter("t"));
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		User user=tjnSession.getUser();
		Project project=tjnSession.getProject();
		int projectId=project.getId();
		String currentUser=user.getId();
		
		/*Added by Priyanka for TENJINCG-1231 ends*/
		try {
			project=new ProjectHandler().hydrateProject(projectId);
		} catch (DatabaseException e3) {
			
			e3.printStackTrace();
		}
		/*Added by Priyanka for TENJINCG-1231 ends*/
		if(task.equalsIgnoreCase("init_new_testcase")){
			/*Added by Padmavathi for TENJINCG-679 starts*/
			TestCase testcase=new TestCase();
			request.setAttribute("testcase", testcase);
			/*Added by Padmavathi for TENJINCG-679 ends*/
			request.setAttribute("user", currentUser);
			request.setAttribute("project", project);
			request.getRequestDispatcher("v2/testcase_new.jsp").forward(request, response);
		
		}else if(task.equalsIgnoreCase("list")){
			try {
			
				
				/*Added by Ashiki for ALM import starts*/
				if(request.getAttribute("msg")!=null && request.getAttribute("msg")!="")
				{
				SessionUtils.setScreenState("success", (String) request.getAttribute("msg"), request);
				SessionUtils.loadScreenStateToRequest(request);
				
				}
				/*Added by Ashiki for ALM import ends*/
				
				if(request.getParameter("msg")!=null && request.getParameter("msg")!="")
				{
					SessionUtils.setScreenState("success", request.getParameter("msg"), request);
					SessionUtils.loadScreenStateToRequest(request);
					
				}
				/* Added by Padmavathi for TENJINCG-893 starts */
				if(request.getParameter("downloadPath")!=null && request.getParameter("downloadPath")!="")
				{
					request.setAttribute("downloadPath", request.getParameter("downloadPath"));
				}
				 /* Added by Padmavathi for TENJINCG-893 ends */
				/*List<TestCase> testcase = new TestCaseHandler().getAllTestCases(projectId);
				request.setAttribute("testcases", testcase);*/
				request.setAttribute("user", currentUser);
				request.setAttribute("currentUser", user);
				request.setAttribute("project", project);
				/*Added by Priyanka for TENJINCG-1231 starts*/
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				if(project.getEndDate()==null){
					request.setAttribute("edate","NA");
				}else{
				String edate=sdf.format(project.getEndDate());
				
				request.setAttribute("edate",edate);
				}
				/*Added by Priyanka for TENJINCG-1231 ends*/
			
				request.getRequestDispatcher("v2/testcase_list.jsp").forward(request, response);
			} catch (NumberFormatException e) {
				logger.error("ERROR fetching TestCase information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			} 
			
		}else if(task.equalsIgnoreCase("testCase_View")){
			String recId=request.getParameter("paramval");
			TestCaseHandler handler=new TestCaseHandler();
			try {
				TestCase testcase=handler.hydrateTestCase(projectId,Integer.parseInt(recId));
				List<TestStep> teststep=testcase.getTcSteps();
				request.setAttribute("testcase", testcase);
				request.setAttribute("teststep", teststep);
				request.setAttribute("project", project);
				request.setAttribute("user", currentUser);
				if(request.getParameter("msg")!=null && request.getParameter("msg")!="")
				{
					SessionUtils.setScreenState("success", request.getParameter("msg"), request);
					SessionUtils.loadScreenStateToRequest(request);
				}
				/*Added by Priyanka for TENJINCG-1231 starts*/
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				if(project.getEndDate()==null){
					request.setAttribute("edate","NA");
				}else{
				String edate=sdf.format(project.getEndDate());
				
				request.setAttribute("edate",edate);
				}
				/*Added by Priyanka for TENJINCG-1231 ends*/
				request.getRequestDispatcher("v2/testcase_view.jsp").forward(request,response);
			} catch (NumberFormatException e) {
				logger.error("ERROR fetching TestCase information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching TestCase information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			}
			
			
		}else if(task.equalsIgnoreCase("tc_search")){
			
			String tcId = request.getParameter("tcid");
			String tcName = request.getParameter("tcname");
			String tcType = request.getParameter("type");
			String tcMode = request.getParameter("mode");
			String prjId=request.getParameter("pid");
			TestCaseHandler handler= new TestCaseHandler();
			try {
				List<TestCase> testcase=
				handler.searchTestcase(tcId,tcName,tcType,tcMode,prjId);
				request.setAttribute("testcases", testcase);
				request.setAttribute("user", currentUser);
				request.setAttribute("project", project);
				request.setAttribute("tcId", tcId);
				request.setAttribute("tcName", tcName);
				request.setAttribute("tcType", tcType);
				request.setAttribute("tcMode", tcMode);
				request.getRequestDispatcher("v2/testcase_list.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching TestCase information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			}			
		}
		
		/*Added by Preeti for TENJINCG-611(Test case execution history) starts*/
		else if(task.equalsIgnoreCase("testcase_execution_history")){
			TestCaseHandler handler=new TestCaseHandler();
			String recId = request.getParameter("recid");
			try{
				TestCase testcase=handler.hydrateTestCase(projectId,Integer.parseInt(recId));
				request.setAttribute("testcase", testcase);
				List<TestRun> runs = handler.hydrateTestCaseExecutionHistory(projectId,Integer.parseInt(recId));
				request.setAttribute("project", project);
				request.setAttribute("runs", runs);
				request.getRequestDispatcher("v2/testcase_execution_history.jsp").forward(request, response);
			}catch(Exception e){
				logger.error("Could not fetch execution summary for test case",recId, e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			}
		}
		/*Added by Preeti for TENJINCG-611(Test case execution history) ends*/
		else if(task.equalsIgnoreCase("fetch_prj_testcases")){
			ProjectHandler handler = new ProjectHandler();
			try {
				ArrayList<Project> projectList = handler.hydrateProjectsForCurrentUser(currentUser,project.getId());
				request.setAttribute("user", currentUser);
				request.setAttribute("project", project);
				request.setAttribute("projectList", projectList);
             /* modified by shruthi for TCGST-51 starts*/
				if(projectList.size()<1)
				{
					SessionUtils.setScreenState("error", "Not mapped to any project.", request);
					request.getRequestDispatcher("TestCaseServletNew?t=list").forward(request, response);
				}
				
				else
				{
				request.getRequestDispatcher("v2/prj_testcase_import.jsp").forward(request, response);
				}
             /* modified by shruthi for TCGST-51 ends*/
			} catch (DatabaseException e) {
				logger.error("ERROR fetching project information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			}
			
			
		}
		else if(task.equalsIgnoreCase("fetch_tm_testcases")){
			
			String userId = tjnSession.getUser().getId();
			
			Map<String,Object> map = new HashMap<String,Object>();
			TestCaseHandler handler=new TestCaseHandler();
			
			List<com.ycs.tenjin.testmanager.TestStep> allSteps = new ArrayList<com.ycs.tenjin.testmanager.TestStep>();
			TestManagerInstance tmInstance = new TestManagerInstance();
			try {
				Project prj = handler.hydrateProject(projectId);
				handler.hydrateFilters(prj);
				
				tmInstance = handler.hydrateTMCredentials(prj.getEtmInstance(), userId, projectId);
				/*Modified by paneendra for VAPT FIX starts*/
				String decryptedPwd = new CryptoUtilities().decrypt(tmInstance.getPassword());
				/*Modified by paneendra for VAPT FIX ends*/
				
				TestManager alm = null;
				alm = TestManagerFactory.getTestManagementTools(tmInstance.getTool(),tmInstance.getURL(), tmInstance.getUsername(), decryptedPwd);
				
				alm.login();
				alm.setProjectDesc(tmInstance.getTmProjectDomain());
				
				/*Modified by Ashiki for TJN252-60 starts*/
				Map<String, String> mapAttributes = new TreeMap<String, String>();
				mapAttributes=handler.hydrateMappedAttribute(projectId);
				List<com.ycs.tenjin.testmanager.TestCase> tcList= alm.getTestCases(prj.getTcFilter(),prj.getTcFilterValue(),mapAttributes);
				List<com.ycs.tenjin.testmanager.TestCase> tCaseList = new ArrayList<com.ycs.tenjin.testmanager.TestCase>();
				allSteps = alm.getTestStepsWithFilter(prj.getTsFilter(), prj.getTsFilterValue(),mapAttributes);
				List<com.ycs.tenjin.testmanager.TestStep> tsList=null;
				for(com.ycs.tenjin.testmanager.TestCase tCase : tcList){
					 tsList=alm.getTestSteps(prj.getTsFilter(),prj.getTsFilterValue(),tCase.getTcId(),mapAttributes);
				/*Modified by Ashiki for TJN252-60 ends*/
					 
					if(tsList!=null && tsList.size()>0){
						JSONArray jArray = new JSONArray();
					for(com.ycs.tenjin.testmanager.TestStep ts: tsList){
							JSONObject json = new JSONObject();
							/*changed by Ashiki for TJN252-54 starts*/
							json.put("dataId",ts.getId());
							/*changed by Ashiki for TJN252-54 ends*/
							json.put("recordId", ts.getRecordId());
							json.put("description", ts.getDescription());
							json.put("type", ts.getType());
							json.put("moduleCode", ts.getModuleCode());
							json.put("moduleName", ts.getModuleName());
							json.put("appName", ts.getAppName());
							json.put("parentTestCaseId", ts.getParentTestCaseId());
							json.put("rowsToExecute", ts.getRowsToExecute());
							jArray.put(json);
						}
					tCase.setTcSteps(tsList);
					tCase.setStepsArray(jArray);	
					}
					
					tCaseList.add(tCase);
				}
				alm.logout();
				request.getSession().removeAttribute("SCR_MAP");
				map.put("tcList", tCaseList);
				request.setAttribute("tcList", tCaseList);
				map.put("tsList", allSteps);
				request.setAttribute("tsList", allSteps);
				map.put("STATUS", "success");
				map.put("MESSAGE", "");
				
				
			} catch (DatabaseException e) {
				map.put("STATUS", "error");
				map.put("MESSAGE", "An Internal error occored, Please contact tenjin support");
				logger.error("could not fetch filters for project {}",projectId,e);
			} catch (UnreachableETMException e) {
				logger.error("Could not able to login into TM tool.",e);
			} catch (ETMException e) {
					logger.error("Could not able to login into TM tool.",e);
			} catch (JSONException e) {
				logger.error("Error while putting imported test steps into json",e);
			}catch (Exception e){
				logger.error("Error while putting imported test steps into json",e);
			}
			request.getSession().setAttribute("SCR_MAP", map);
			RequestDispatcher rd=request.getRequestDispatcher("v2/importedtclistNew.jsp");  
			rd.forward(request, response);
			
		}else if(task.equalsIgnoreCase("persist_imported_testcases")){
			String testcases = request.getParameter("tcids");
			String tstepIds = request.getParameter("tsteps");
			String[] array = testcases.split(",");
			String[] stepArray = tstepIds.split(",");
			boolean exist=false;
			List<String> listExceptions = new ArrayList<String>();
			TestCaseHandler handler=new TestCaseHandler();
			Map<String,Object> map = (Map<String, Object>) request.getSession().getAttribute("SCR_MAP");
			List<com.ycs.tenjin.testmanager.TestCase> tcs = (ArrayList<com.ycs.tenjin.testmanager.TestCase>) map.get("tcList");
			List<com.ycs.tenjin.testmanager.TestStep> allSteps = (ArrayList<com.ycs.tenjin.testmanager.TestStep>) map.get("tsList");
			List<com.ycs.tenjin.testmanager.TestCase> importedTcs = new ArrayList<com.ycs.tenjin.testmanager.TestCase>();
			for(com.ycs.tenjin.testmanager.TestCase tc : tcs){
				for(int i=0;i<array.length;i++){
					if(array[i].equalsIgnoreCase(tc.getTcId())){
						importedTcs.add(tc);
					}
				}
			}
			
			for(com.ycs.tenjin.testmanager.TestCase tc: importedTcs){
				TestCase tjnTc = new TestCase();
				tjnTc.setTcId(tc.getTcId());
				tjnTc.setTcName(tc.getTcName());
				tjnTc.setTcDesc(tc.getTcDesc());
				tjnTc.setTcType(tc.getTcType());
				tjnTc.setTcFolderId(tc.getTcFolderId());
				tjnTc.setTcPriority(tc.getTcPriority());
				tjnTc.setTcCreatedBy(tc.getTcCreatedBy());
				tjnTc.setProjectId(tjnSession.getProject().getId());
				/*Added by Ashiki for TJN252-60 starts*/
				ArrayList<TestStep> list = new ArrayList<TestStep>();
				if(tc.getTcSteps()!=null) {
				for(com.ycs.tenjin.testmanager.TestStep step: tc.getTcSteps()) {
					TestStep tjnStep=new TestStep();
					tjnStep.setId(step.getId());
					tjnStep.setRecordId(step.getRecordId());
					tjnStep.setShortDescription(step.getShortDescription());
					/*changed by Ashiki for TJN252-54 ends*/
					/*tjnStep.setDataId(step.getDataId());*/
					tjnStep.setDataId(step.getId());
					/*changed by Ashiki for TJN252-54 ends*/
					tjnStep.setType(step.getType());
					tjnStep.setDescription(step.getDescription());
					tjnStep.setExpectedResult(step.getExpectedResult());
					tjnStep.setActualResult(step.getActualResult());
					tjnStep.setAppId(step.getAppId());
					tjnStep.setAppName(step.getAppName());
					tjnStep.setModuleCode(step.getModuleCode());
					tjnStep.setId(step.getId());
					tjnStep.setAutLoginType(step.getAutLoginType());
					tjnStep.setOperation(step.getOperation());
					tjnStep.setTxnMode(step.getTxnMode());
					tjnStep.setRowsToExecute(step.getRowsToExecute());
					
					
					list.add(tjnStep);
				}
				}
				tjnTc.setTcSteps(list);
				/*Added by Ashiki for TJN252-60 ends*/
				tjnTc.setRecordType("TC");
				tjnTc.setStorage("Remote");
				tjnTc.setMode(tc.getMode());
				try {
					
					exist=handler.checkTestCase(tjnTc.getTcId(), tjnSession.getProject().getId());
					if(!exist){
					
						handler.persistTestCase(tjnTc);
						/*Added by Ashiki for TJN252-60 Starts*/
						if(tjnTc.getTcSteps()!=null) {
							ArrayList<TestStep> tcsteps=tjnTc.getTcSteps();
							for(TestStep ts: tcsteps) {
								TestCase tcid=new TestCaseHelper().hydrateTestCase(projectId, tjnTc.getTcId());
								ts.setTestCaseRecordId(tcid.getTcRecId());
									String appName=ts.getAppName();
									/*added by Roshni */
									try{
									Aut aut=new AutHandler().getApplication(appName);
									ts.setAppId(aut.getId());
									new TestStepHandler().persistTestStep(ts,tjnSession.getProject().getId());
									}catch(RecordNotFoundException e){
										listExceptions.add(appName +" is not mapped with the project");
									}
							
								
							}
						}
						/*Added by Ashiki for TJN252-60 ends*/
					}else{
						/*Added by Ashiki for Tenj210-108 starts*/
						handler.updateImportedTestCase(tjnTc, tjnSession.getProject().getId());
						
						if(tjnTc.getTcSteps()!=null) {
							ArrayList<TestStep> tcsteps=tjnTc.getTcSteps();
							
							for(TestStep ts: tcsteps) {
								TestCase tcid=new TestCaseHelper().hydrateTestCase(projectId, tjnTc.getTcId());
								tcid.getTcSteps();
								ts.setTestCaseRecordId(tcid.getTcRecId());
									String appName=ts.getAppName();
									try{
									Aut aut=new AutHandler().getApplication(appName);
									ts.setAppId(aut.getId());
									new TestStepHandler().importTestStep(ts,tjnSession.getProject().getId(),aut);
									}catch(RecordNotFoundException e){
										listExceptions.add(appName +" is not mapped with the project");
									}
							
								
							}
						}
						
						/*Added by Ashiki for Tenj210-108 ends*/
					}
					
				} catch (DatabaseException e) {
					logger.error("Could not able to persist imported tc from tm",e);
					listExceptions.add("Could not import test case '"+tc.getTcId()+" "+e.getMessage());
					continue;
				} catch (RequestValidationException e) {
					listExceptions.add("Could not import test case '"+tc.getTcId()+" "+e.getMessage());
					logger.error("Error ", e);
				}
				
				
			}
			if(allSteps!=null && allSteps.size()>0){
				List<com.ycs.tenjin.testmanager.TestStep> importedSteps = new ArrayList<com.ycs.tenjin.testmanager.TestStep>();
				for(com.ycs.tenjin.testmanager.TestStep tstep : allSteps){
					for(int i=0;i<stepArray.length;i++){
						if(stepArray[i].equalsIgnoreCase(tstep.getDataId())){
							importedSteps.add(tstep);
						}
					}
				}
				for(com.ycs.tenjin.testmanager.TestStep tstep: importedSteps){
					TestStep tjnStep = new TestStep();
					TestCase objTestCase = null;
					int appId=0;
					List<Module> listFunctions = new ArrayList<Module>();
					tjnStep.setId(tstep.getId());
					tjnStep.setDataId(tstep.getDataId());
					tjnStep.setDescription(tstep.getDescription());
					
					tjnStep.setAppName(tstep.getAppName());
					
					try {
						
						appId=new AutHandler().hydrateAppId(tjnSession.getProject().getId(),tstep.getAppName());

						if(appId>0){
							tjnStep.setAppId(appId);
						}else{
							logger.error("app "+tstep.getAppName()+" is not mapped to Tenjin project.");
							listExceptions.add("Could not import test step '"+tstep.getId()+"' because application '"+tstep.getAppName()+"' is not mapped with project '"+tjnSession.getProject().getName()+"'");
							continue;
						}

						
					} catch (DatabaseException e2) {
						logger.error(e2.getMessage()+"(test step -- "+tstep.getId()+")");
						listExceptions.add("Could not import test step '"+tstep.getId()+"' because application '"+tstep.getAppName()+"' is not mapped with project '"+tjnSession.getProject().getName()+"'");
						continue;
					}
					boolean moduleFound = false;
					try{
						listFunctions = new AutHelper().hydrateFunctions(appId);
						if( listFunctions.size()<=0){
							listExceptions.add("Could not import test step '"+tstep.getId()+"' because application '"+tstep.getAppName()+"' does not contain any function");
							continue;
						}
						
						for(int i=0;i<listFunctions.size();i++){
							if(tstep.getModuleCode().equalsIgnoreCase(listFunctions.get(i).getCode())){
								tjnStep.setModuleCode(listFunctions.get(i).getCode());
								moduleFound = true;
							}
						}
						if(!moduleFound){
							listExceptions.add("Could not import test step '"+tstep.getId()+"' because application '"+tstep.getAppName()+"' does not contain function '"+tstep.getModuleCode()+"'");
							continue;
						}
						
						
					}catch(DatabaseException e){
						logger.error(e.getMessage()+"(test step -- "+tstep.getId()+")");
						listExceptions.add("Could not import test step '"+tstep.getId()+"' because application '"+tstep.getAppName()+"' does not contain function code "+tstep.getModuleCode());
						continue;
					}
					tjnStep.setRowsToExecute(tstep.getRowsToExecute());
					try {
						objTestCase = new TestCaseHelper().hydrateTestCase(tjnSession.getProject().getId(), tstep.getParentTestCaseId());
					} catch (DatabaseException e1) {
						logger.error(e1.getMessage()+"(test step -- "+tstep.getId()+")");
						listExceptions.add("Could not import test step '"+tstep.getId()+"', Please contact Tenjin support");
						continue;
					}
					tjnStep.setTestCaseRecordId(objTestCase.getTcRecId());
					tjnStep.setShortDescription(tstep.getShortDescription());
					tjnStep.setType(tstep.getType());
					tjnStep.setExpectedResult(tstep.getExpectedResult());
					tjnStep.setOperation(tstep.getOperation());
					tjnStep.setTxnMode(tstep.getTxnMode());
					tjnStep.setAutLoginType(tstep.getAutLoginType());
					tjnStep.setTstepStorage("Remote");
					tjnStep.setValidationType("Functional");
					try {
						exist = handler.checkImportedTestStep(tjnStep,objTestCase.getTcRecId());
						if(!exist){
							
							new TestStepHandler().persistTestStep(tjnStep,tjnSession.getProject().getId());
						}
						
					} catch (DatabaseException e) {
						logger.error("Could not able to persist imported tstep from tm",e);
					} catch (RequestValidationException e) {
						logger.error("Could not able to persist imported tstep from tm",e);
						logger.error("Error ", e);
					}
				}
			}
			for(int i=0;i<listExceptions.size();i++){
				listExceptions.set(i, i+1+")"+listExceptions.get(i));
			}
			String msg="";
			
			map.put("STATUS", "SUCCESS");
			map.put("listExceptions", listExceptions);
			request.setAttribute("listExceptions", listExceptions);
			if(listExceptions.size()>0){
				msg = "Test Cases and Test Steps imported successfully, Please click Logs button for more Info";
			}else{
				msg="Test Cases and Test Steps imported successfully";
			}
			
			/*Added by Ashiki for ALM import starts*/
			request.setAttribute("msg", msg);
			/*Added by Ashiki for ALM import ends*/
			request.getSession().removeAttribute("SCR_MAP");
			request.getSession().setAttribute("SCR_MAP", map);
			/*Modified by Ashiki for ALM import starts*/
			request.getRequestDispatcher("TestCaseServletNew?t=list").forward(request, response);
			/*Modified by Ashiki for ALM import starts*/
		}
		/*Changed by Leelaprasad for the Requirement TENJINCG-738 starts*/
		else if(task.equalsIgnoreCase("autocomplete_tcid")){
			String retData = "";
			String prj_id = request.getParameter("prj_id");
		        response.setContentType("text/html");
		        response.setHeader("Cache-control", "no-cache, no-store");
		        response.setHeader("Pragma", "no-cache");
		        response.setHeader("Expires", "-1");
		       
		        JSONArray arrayObj=new JSONArray();
		        JSONObject retJson = new JSONObject();
		        String query = request.getParameter("term");
		       int projid=Integer.parseInt(prj_id);
		        if(query!=null){
		        try {
					arrayObj= new TestCaseHandler().fetchTestCaseSuggestion(query,projid);
				} catch (DatabaseException e) {
					
					
				}
		        }
		        try {
					retJson.put("status", "SUCCESS");
					retJson.put("tclistIds",arrayObj);
				} catch (JSONException e) {
					
				
				}
		        finally{
					retData = retJson.toString();
				}
		        response.getWriter().write(retData);
		       
			}
		/*Changed by Leelaprasad for the Requirement TENJINCG-738 ends*/
		
		/*Added by Padmavathi for TENJINCG-849 starts*/
		else if(task.equalsIgnoreCase("testCaseCopy")){
			TestCase testcase=null;
			String tcRecId=request.getParameter("tcRecId");
			int stepSize=0;
			try {
				testcase=new TestCaseHandler().hydrateTestCaseBasicDetails(projectId,Integer.parseInt(tcRecId));
				stepSize=new TestStepHandler().getStepsCountForCase(Integer.parseInt(tcRecId));
			} catch (NumberFormatException | RecordNotFoundException | DatabaseException e) {
				logger.error("ERROR fetching Test case information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
			
			request.setAttribute("testcase", testcase);
			request.setAttribute("user", currentUser);
			request.setAttribute("project", project);
			request.setAttribute("stepSize", stepSize);
			request.setAttribute("testCaseCopy", "true");
			request.getRequestDispatcher("v2/testcase_new.jsp").forward(request, response);
		/*Added by Padmavathi for TENJINCG-849 ends*/
		
		/*Added by Ashiki for TENJINCG_1215 starts*/
		}else if(task.equalsIgnoreCase("autocomplete_label")){
			String retData = "";
			String prj_id = request.getParameter("prj_id");
		        response.setContentType("text/html");
		        response.setHeader("Cache-control", "no-cache, no-store");
		        response.setHeader("Pragma", "no-cache");
		        response.setHeader("Expires", "-1");
		       
		        JSONArray arrayObj=new JSONArray();
		        JSONObject retJson = new JSONObject();
		        String query = request.getParameter("term");
		       int projid=Integer.parseInt(prj_id);
		        if(query!=null){
		        try {
					arrayObj= new TestCaseHandler().fetchTestCaseLabelSuggestion(query,projid);
				} catch (DatabaseException e) {
					
					
				}
		        }
		        try {
					retJson.put("status", "SUCCESS");
					retJson.put("tclistIds",arrayObj);
				} catch (JSONException e) {
					
				
				}
		        finally{
					retData = retJson.toString();
				}
		        response.getWriter().write(retData);
		       
			}
		/*Added by Ashiki for TENJINCG_1215 ends*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		Project project=tjnSession.getProject();
	
	
		/*Added by Priyanka for TENJINCG-1231 starts*/
		int projectId=project.getId();
		try {
			project=new ProjectHandler().hydrateProject(projectId);
		} catch (DatabaseException e3) {
			
			e3.printStackTrace();
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		if(project.getEndDate()==null){
			request.setAttribute("edate","NA");
		}else{
		String edate=sdf.format(project.getEndDate());
		
		request.setAttribute("edate",edate);
		}
		/*Added by Priyanka for TENJINCG-1231 ends*/
		
		logger.info("entered doPost()");
		String delFlag = Utilities.trim(request.getParameter("del"));
		if(delFlag.equalsIgnoreCase("true")) {
			this.doDelete(request, response);
		}else if(delFlag.equalsIgnoreCase("updateTestCase")){
			String recid=request.getParameter("txtTcRecId");
			String createdBy=request.getParameter("txtCreatedBy");
			//int projectId=project.getId();
			
			TestCase testcase=new TestCase();
			testcase=this.mapAttributes(request);
			testcase.setTcRecId(Integer.parseInt(recid));
			TestCaseHandler handler=new TestCaseHandler();
			/*Added by Preeti for TENJINCG-969 starts*/
			AuditRecord audit = new AuditRecord();
			audit.setEntityRecordId(testcase.getTcRecId());
			audit.setLastUpdatedBy(tjnSession.getUser().getId());
			audit.setEntityType("testcase");
			testcase.setAuditRecord(audit);
			/*Added by Preeti for TENJINCG-969 ends*/
			try {
				handler.updateTestCase(testcase);
				testcase=handler.hydrateTestCase(projectId,Integer.parseInt(recid));
				/* Added by Roshni for TENJINCG-1046 starts */
				User tcOwner=new UserHandler().getUser(testcase.getTcCreatedBy());
				TenjinMailHandler tmhandler=new TenjinMailHandler();
				/*Added by Padmavathi for TENJINCG-1050 starts*/
				if(!new ProjectMailHandler().isUserOptOutFromMail(projectId,"TestPlanEdit",tcOwner.getId())){
				/*Added by Padmavathi for TENJINCG-1050 ends*/
					JSONObject mailContent=tmhandler.getMailContent(String.valueOf(testcase.getTcRecId()), testcase.getTcName(), "Test Case", tjnSession.getUser().getFullName(),
							"update", new Timestamp(new Date().getTime()), tcOwner.getEmail(), "");
					try {
						new TenjinMailHandler().sendEmailNotification(mailContent);
					} catch (TenjinConfigurationException e) {
						logger.error("Error ", e);
					}
				}
				/* Added by Roshni for TENJINCG-1046 ends */
				List<TestStep> teststep=testcase.getTcSteps();
				request.setAttribute("testcase", testcase);
				request.setAttribute("teststep", teststep);
				request.setAttribute("project", project);
				request.setAttribute("user", createdBy);
				SessionUtils.setScreenState("success", "TestCase.update.success", request);
				SessionUtils.loadScreenStateToRequest(request);
				
				request.getRequestDispatcher("v2/testcase_view.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching TestCase information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
				logger.error("Error ", e);
			} catch (RequestValidationException e) {
				SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
				try {
					String tcName=testcase.getTcName();
					/*Added by Padmavathi for T251IT-65 starts*/
					String tcDesc=testcase.getTcDesc();
					/*Added by Padmavathi for T251IT-65 ends*/
					testcase=handler.hydrateTestCase(projectId, Integer.parseInt(recid));
					testcase.setTcName(tcName);
					/*Added by Padmavathi for T251IT-65 starts*/
					testcase.setTcDesc(tcDesc);
					/*Added by Padmavathi for T251IT-65 ends*/
				} catch (NumberFormatException | RecordNotFoundException | DatabaseException e1) {
					
					logger.error("Error ", e1);
				}
				
				request.setAttribute("testcase", testcase);
				/*Added by Pushpalatha for TENJINCG-679 starts*/
				List<TestStep> teststep=testcase.getTcSteps();
				/*Added by Pushpalatha for TENJINCG-670 ends*/
				request.setAttribute("teststep", teststep);
				request.setAttribute("project", project);
				request.setAttribute("user",createdBy);
				request.setAttribute("testcase", testcase);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("v2/testcase_view.jsp").forward(request, response);
				logger.error("Error ", e);
			}
			
		}else if(delFlag.equalsIgnoreCase("importProjectTestCases")){
			String selectedTestCaseIds = request.getParameter("selectedTestCase");
			String [] sourceTestCasesIds = selectedTestCaseIds.split(",");
			int currentProjectId=project.getId();
			String currentUser=tjnSession.getUser().getId();
			String retData = "";
			TestCaseHandler handler=new TestCaseHandler();
			Connection conn = null;
			/*Added by Preeti for TENJINCG-970 starts*/
			File dir = new File(request.getSession().getServletContext().getRealPath("/") + File.separator+"Uploads");
			if(!dir.exists()){
				dir.mkdir();
			}
			String folderPath = request.getSession().getServletContext().getRealPath("/") + File.separator+"Uploads"+File.separator;
			/*Added by Preeti for TENJINCG-970 ends*/
			try {
				conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				//conn.setAutoCommit(false);
			} catch (DatabaseException e) {
				SessionUtils.setScreenState("error", "generic.db.error", request);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
			if(conn != null){
			try{
				JSONObject resData = new JSONObject();
				try {
					/*Modified by Preeti for TENJINCG-970 starts*/
					handler.importTestCasesForProject(conn,sourceTestCasesIds,currentUser,currentProjectId,folderPath);
					/*Modified by Preeti for TENJINCG-970 ends*/
					//conn.commit();
					resData.put("status", "success");
					/*Added by Preeti for TENJINCG-970 starts*/
					resData.put("path", "Uploads/TestCases-output.xlsx");
					/*Added by Preeti for TENJINCG-970 ends*/
					resData.put("message", "Import completed successfully.");
				}catch(DuplicateRecordException e){
					conn.rollback();
					resData.put("status","error");
					resData.put("message",SessionUtils.loadMessage(request, e.getMessage(), e.getTargetFields()));
					logger.error("An Error occurred", e);
				}catch(RequestValidationException e){
					conn.rollback();
					resData.put("status","error");
					resData.put("message",SessionUtils.loadMessage(request, e.getMessage(), e.getTargetFields()));
					logger.error("An Error occurred", e);
				}catch(DatabaseException e){
					conn.rollback();
					resData.put("status","error");
					resData.put("message","Tenjin is unable to process the records. Please contact Tenjin support for further assistance.");
					logger.error("An Error occurred", e);
				}catch(Exception e){
					conn.rollback();
					resData.put("status","error");
					resData.put("message",SessionUtils.loadMessage(request, e.getMessage()));
					logger.error("An Error occurred", e);
				}finally{
					retData = resData.toString();
					conn.close();
				}
			}
			catch(Exception e){
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			}
			response.getWriter().write(retData);	
		}
		else{
			final String STORAGE="Local";
			String createdBy=request.getParameter("tcCreatedBy");
			
			TestCase test = new TestCase();
			
			test=this.mapAttributes(request);
			test.setStorage(STORAGE);
			
			TestCaseHandler handler=new TestCaseHandler();
			/*Added by Padmavathi for TENJINCG-849 starts*/
			String sourceTcRecId=Utilities.trim(request.getParameter("tcRecId"));
			String isTestCaseCopy=Utilities.trim(request.getParameter("testCaseCopy"));
			int stepSize=0;
			/*Added by Padmavathi for TENJINCG-849 ends*/
			try {
				int targetTcRecId=handler.persistTestCase(test);
				request.setAttribute("testcase", test);
				request.setAttribute("project", project);
				request.setAttribute("user", createdBy);
				
			
				SessionUtils.setScreenState("success", "TestCase.create.success", request);
				/*Modified by Padmavathi for TENJINCG-849 starts*/
				if(isTestCaseCopy.equalsIgnoreCase("true")){
					test.setTcPresetRules(Utilities.trim(request.getParameter("presetRules")));
					handler.copyTestStepsforCase(targetTcRecId, test,Integer.parseInt(sourceTcRecId)); 
					response.sendRedirect("TestCaseServletNew?t=testCase_View&paramval="+targetTcRecId);
				}else{
					SessionUtils.loadScreenStateToRequest(request);
					request.getRequestDispatcher("v2/testcase_view.jsp").forward(request, response);
				}
				/*Modified by Padmavathi for TENJINCG-849 ends*/
			} catch (DatabaseException e) {
				SessionUtils.setScreenState("error", e.getMessage(), request);
				/*Added by Padmavathi for TENJINCG-849 starts*/
				if(isTestCaseCopy.equalsIgnoreCase("true")){
					try {
						stepSize=new TestStepHandler().getStepsCountForCase(Integer.parseInt(sourceTcRecId));
					} catch (NumberFormatException | DatabaseException e1) {
						logger.error("Error ", e1);
					}
					test.setTcRecId(Integer.parseInt(sourceTcRecId));
					request.setAttribute("stepSize", stepSize);
					request.setAttribute("testCaseCopy", "true");
				}
				/*Added by Padmavathi for TENJINCG-849 ends*/
				request.setAttribute("testcase", test);
				request.setAttribute("project", project);
				request.setAttribute("user",createdBy);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("v2/testcase_new.jsp").forward(request, response);
				logger.error("Error ", e);
			} catch (RequestValidationException e) {
				logger.error(e.getMessage());
				SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
				/*Added by Padmavathi for TENJINCG-849 starts*/
				if(isTestCaseCopy.equalsIgnoreCase("true")){
					try {
						stepSize=new TestStepHandler().getStepsCountForCase(Integer.parseInt(sourceTcRecId));
					} catch (NumberFormatException | DatabaseException e1) {
						logger.error("Error ", e1);
					}
					test.setTcRecId(Integer.parseInt(sourceTcRecId));
					request.setAttribute("stepSize", stepSize);
					request.setAttribute("testCaseCopy", "true");
				}
				/*Added by Padmavathi for TENJINCG-849 ends*/
				request.setAttribute("testcase", test);
				request.setAttribute("project", project);
				request.setAttribute("user",createdBy);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher("v2/testcase_new.jsp").forward(request, response);
			}
			
		}

	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*Modified by Padmavathi for TJNUN262-3 starts*/
		/*String[] selectedTestCase = request.getParameterValues("selectedTestCase");*/
		String selectedTestCase = request.getParameter("selectedTestCase");
		/*Modified by Padmavathi for TJNUN262-3 starts*/
		String projectName=Utilities.trim(request.getParameter("projectName"));
		String domain=Utilities.trim(request.getParameter("domain"));
		String projectId=Utilities.trim(request.getParameter("pid"));
		String currentUser=Utilities.trim(request.getParameter("user"));
		
		Project project=new Project();
		project.setId(Integer.parseInt(projectId));
		project.setName(projectName);
		project.setDomain(domain);
		
		TestCaseHandler handler=new TestCaseHandler();
		List<TestCase> testcase=null;
		/* Added by Roshni for TENJINCG-1046 starts */
		List<JSONObject> contents=new ArrayList<>();
		/* Added by Roshni for TENJINCG-1046 ends */
		/*Added by Pushpa for TENJINCG-897 starts*/
		try {
			/*Modified by Preeti for TENJINCG-1003 starts*/
			/* Added by Roshni for TENJINCG-1046 starts */
				TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
			
				TenjinMailHandler tmhandler=new TenjinMailHandler();
				for(String tcId:selectedTestCase.split(",")){
					JSONObject mailContent=new JSONObject();
					int testcaseId=Integer.parseInt(tcId);
					TestCase tcase=handler.hydrateTestCase(Integer.parseInt(projectId), testcaseId);
					/*Added by Ashiki for ALM import starts*/
					if(!tcase.getStorage().equalsIgnoreCase("Remote")) {
					/*Added by Ashiki for ALM import ends*/
					User tcOwner=new UserHandler().getUser(tcase.getTcCreatedBy());
					/*Added by Padmavathi for TENJINCG-1050 starts*/
					if(!new ProjectMailHandler().isUserOptOutFromMail(Integer.parseInt(projectId),"TestPlanRemove",tcOwner.getId())){
						/*Added by Padmavathi for TENJINCG-1050 ends*/
						mailContent=tmhandler.getMailContent(String.valueOf(tcase.getTcRecId()), tcase.getTcName(), "Test Case", tjnSession.getUser().getFullName(),
								"delete", new Timestamp(new Date().getTime()), tcOwner.getEmail(), "");
						contents.add(mailContent);	
					}
					}
				
				}
			/* Added by Roshni for TENJINCG-1046 ends */
			
			handler.deleteTestCase(selectedTestCase,currentUser,Integer.parseInt(projectId));
			/*Modified by Preeti for TENJINCG-1003 ends*/
			
			/* Added by Roshni for TENJINCG-1046 starts */
			for(JSONObject content:contents){
				try {
					tmhandler.sendEmailNotification(content);
				} catch (TenjinConfigurationException e) {
					logger.error("Error ", e);
				}
			}
			/* Added by Roshni for TENJINCG-1046 ends */
			SessionUtils.setScreenState("success","testcase.delete.success", request);
			request.setAttribute("testcases", testcase);
			request.setAttribute("user", currentUser);
			request.setAttribute("project", project);
			SessionUtils.loadScreenStateToRequest(request);
			request.getRequestDispatcher("v2/testcase_list.jsp").forward(request, response);
		} catch (DatabaseException e) {
			SessionUtils.setScreenState("error", "generic.db.error", request);
			SessionUtils.loadScreenStateToRequest(request);
			request.getRequestDispatcher("error.jsp").forward(request, response);
		} catch (SQLException e) {
			SessionUtils.setScreenState("error", "generic.db.error", request);
			SessionUtils.loadScreenStateToRequest(request);
			request.getRequestDispatcher("error.jsp").forward(request, response);
		} catch (RequestValidationException e) {
			request.setAttribute("testcases", testcase);
			request.setAttribute("user", currentUser);
			request.setAttribute("project", project);
			SessionUtils.setScreenState("error",e.getMessage(), request);
			SessionUtils.loadScreenStateToRequest(request);
			request.getRequestDispatcher("v2/testcase_list.jsp").forward(request, response);
		}
		
	}
	
	
	private TestCase mapAttributes(HttpServletRequest request) {
		TestCase testcase=new TestCase();
		testcase.setTcId(request.getParameter("tcId"));
		if(request.getParameter("txtParent")!=null ){
			testcase.setTcFolderId(Integer.parseInt(request.getParameter("txtParent")));
		 }
		testcase.setTcName(Utilities.trim(request.getParameter("tcName")));
		testcase.setTcDesc(Utilities.trim(request.getParameter("tcDesc")));
		testcase.setTcType(Utilities.trim(request.getParameter("tcType")));
		testcase.setTcPriority(Utilities.trim(request.getParameter("tcPriority")));
		testcase.setTcCreatedBy(Utilities.trim(request.getParameter("tcCreatedBy")));
		testcase.setProjectId(Integer.parseInt((request.getParameter("projectId"))));
		testcase.setRecordType(Utilities.trim(request.getParameter("recordType")));
		testcase.setMode(Utilities.trim(request.getParameter("mode")));
		/*Added by Ashiki for TENJINCG-1214 starts*/
		testcase.setLabel(Utilities.trim(request.getParameter("label")));
		/*Added by Ashiki for TENJINCG-1214 ends*/
			/*Added by Padmavathi for TENJINCG-849 starts*/
		if(testcase.getMode()!=null && testcase.getMode().equalsIgnoreCase("")){
			testcase.setMode(Utilities.trim(request.getParameter("lstTcMode")));
		}
			/*Added by Padmavathi for TENJINCG-849 ends*/
			
		/*Modified by Padmavathi for TENJINCG-997 starts*/
		/*Added By Padmavathi for TENJINCG-646 &TENJINCG-645 starts*/
		testcase.setTcPresetRules(Utilities.trim(request.getParameter("dependencyRules")));
		
		/*Added By Padmavathi for TENJINCG-646 &TENJINCG-645 ends*/
		/*Modified by Padmavathi for TENJINCG-997 ends*/
		
		/*Added By Padmavathi for TENJINCG-942 starts*/
		if(request.getParameter("aut")!=null)
			testcase.setAppId(Integer.parseInt(request.getParameter("aut")));
		/*Added By Padmavathi for TENJINCG-942 ends*/
		
		return testcase;
	}

}
