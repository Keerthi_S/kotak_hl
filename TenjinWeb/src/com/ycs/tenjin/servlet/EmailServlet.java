
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: EmailServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 08-01-2018			    Padmavathi		        Newly added for TENJINCG-575
 09-02-2018			    Padmavathi		        for checking Authentication
 22-03-2019             Padmavathi              TENJINCG-1017
 04-12-2019				Preeti					TENJINCG-1174
 * */

package com.ycs.tenjin.servlet;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ParametersHelper;
import com.ycs.tenjin.mail.EmailHandler;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class EmailConfiguration
 */
@WebServlet("/EmailServlet")
public class EmailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(EmailServlet.class);   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmailServlet() {
        super();
        
    }

    public static AuthorizationRule authorizationRule = new AuthorizationRule() {
        
        @Override
        public boolean checkAccess(HttpServletRequest request) {
        	if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
                    return true;
            }
            return false;
        }  
    };
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*replaced by shruthi for VAPT FIX to Servlets starts*/
		String t=request.getParameter("t");
		/*replaced by shruthi for VAPT FIX to Servlets ends*/
		/*added by shruthi for VAPT FIX to Servlets starts*/
		 Authorizer authorizer=new Authorizer();
			if(t.equalsIgnoreCase("") || t.equalsIgnoreCase("configuration") ||t.equalsIgnoreCase("test") ) {
				if(!authorizer.hasAccess(request)){
					response.sendRedirect("noAccess.jsp");
					return;
				}
			}
		/*added by shruthi for VAPT FIX to Servlets ends*/
		SessionUtils.loadScreenStateToRequest(request);
		EmailHandler handler=new EmailHandler();
		ParametersHelper paramHelper=new ParametersHelper();
		if(t.equalsIgnoreCase("configuration")){
			try {
				/*Modified by paneendra for VAPT fix starts*/
				  /*HashMap<String, String> map=new EmailHandler().hydrateEmailConfiguration();*/
					HashMap<String, String> map=paramHelper.hydrateEmailConfiguration();
					map.remove("password");
					/*Modified by paneendra for VAPT fix ends*/
				if(map.size()>0){
					request.setAttribute("delete", "true");
				}
				request.setAttribute("map", map);
				request.getRequestDispatcher("email_configuration.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching email configuration", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}else if(t.equalsIgnoreCase("test")){
					String retData="";
					try{
						String json = request.getParameter("json");
						JSONObject retJson = new JSONObject();
						try {
							JSONObject j = new JSONObject(json);
							handler.TestEmail(j);
							retJson.put("status", "success");
							retJson.put("message", "Test mail sent successfully.");
						}catch (TenjinConfigurationException e) {
							logger.error(e.getMessage(), e);
							retJson.put("status","error");
							retJson.put("message", e.getMessage());
						}  
						catch(Exception e){
							logger.error(e.getMessage(), e);
							retJson.put("status","error");
							retJson.put("message",e.getMessage());
						}finally{
							retData = retJson.toString();
						}
					}catch(Exception e){
						logger.error(e.getMessage(), e);
						retData = "An Internal Error Occurred. Please contact your System Administrator";
					}
					response.getWriter().write(retData);
				}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*replaced by shruthi for VAPT FIX to Servlets starts*/
		String delFlag = Utilities.trim(request.getParameter("del"));
		/*replaced by shruthi for VAPT FIX to Servlets ends*/
		/*added by shruthi for VAPT FIX to Servlets starts*/
		String task = Utilities.trim(request.getParameter("t"));
		 Authorizer authorizer=new Authorizer();
			if(task.equalsIgnoreCase("") || task.equalsIgnoreCase("true")) {
				if(!authorizer.hasAccess(request)){
					response.sendRedirect("noAccess.jsp");
					return;
				}
			}
			/*added by shruthi for VAPT FIX to Servlets ends*/
		
		
		SessionUtils.loadScreenStateToRequest(request);
		EmailHandler handler=new EmailHandler();
		if(delFlag.equalsIgnoreCase("true")) {
			doDelete(request, response);
		}else {
			Map<String,String> map=this.mapAttributes(request);
			try {
				handler.persistEmailConfiguration(map);
				SessionUtils.setScreenState("success", "email.configured.success", request);
				response.sendRedirect("EmailServlet?t=configuration");
			} catch (DatabaseException e) {
				logger.error(e.getMessage(), e);
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("map", map);
				request.getRequestDispatcher("email_configuration.jsp").forward(request, response);
			} catch (TenjinConfigurationException e) {
				logger.error(e.getMessage(), e);
				SessionUtils.setScreenState("error", e.getMessage(), request);
				SessionUtils.loadScreenStateToRequest(request);
				request.setAttribute("map", map);
				request.getRequestDispatcher("email_configuration.jsp").forward(request, response);
			}
		}
	}
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Map<String,String> map=this.mapAttributes(request); 
		try {
			Set<String> keys=map.keySet();
			new EmailHandler().deleteEmailConfiguration(keys);
			SessionUtils.setScreenState("success", "email.configure.delete.success", request);
			SessionUtils.loadScreenStateToRequest(request);
			request.getRequestDispatcher("email_configuration.jsp").forward(request, response);
		} catch (DatabaseException e) {
			logger.error(e.getMessage(), e);
			SessionUtils.setScreenState("error", "generic.db.error", request);
			SessionUtils.loadScreenStateToRequest(request);
			request.setAttribute("map", map);
			request.setAttribute("delete", "true");
			request.getRequestDispatcher("email_configuration.jsp").forward(request, response);
		} 
	}	
	

	private HashMap<String,String> mapAttributes(HttpServletRequest request) {
		
		HashMap<String,String> map=new HashMap<String, String>();
		map.put("smtpServer",Utilities.trim(request.getParameter("SMTPServer")));	
		map.put("port",Utilities.trim( request.getParameter("port")));	
		map.put("authentication",Utilities.trim( request.getParameter("authentication")));
		if(Utilities.trim( request.getParameter("authentication")).equalsIgnoreCase("Y")){
		map.put("userId", Utilities.trim(request.getParameter("userId")));
		map.put("password",Utilities.trim(request.getParameter("password")));
		map.put("userName",Utilities.trim(request.getParameter("userName")));
		}else{
			map.put("userId", "");
			map.put("password","");
			map.put("userName","");
		}
		
		return map;
	}
	
	
}
