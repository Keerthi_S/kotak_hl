/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestSetServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 31-Aug-2016			LATIEF					Requiremen#TJN_23_12
 * 13-Sep-2016			Sriram Sridharan		Added Run Button (for Req#TJN_24_01)
 * 06-Dec-16			Sriram					Requirement#TJN_243_11 (Run Test Step)
 * 05-Nov-2016       	Leelaprasad             Req#tjn_243_13
 * 05-Dec-2016			Sriram					Req#TJN_243_17 (Remove AdHoc Test Sets from Test Set List)
 * 08-Nov-2016       	Leelaprasad             Req#TJN_243_08
 * 01-Dec-2016			Sahana					To overcome null pointer exception
 * 17-Dec-2016       	Leelaprasad             DEFECT#TEN-105
 * 19-Jan-2017			Manish					defect(TENJINCG-48)
 * 25-Jan-2017      	Leelaprasad             TENJINCG-20
 * 30-Jan-2017       	Leelaprasad            	TENJINCG-83
 * 31-Jan-2017       	Leelaprasad            	TENJINCG-52
 *01-Feb-2017			Leelaprasad				defect(TENJINCG-48)
 * 10-May-2017			Sriram					TENJINCG-171
 * 13-06-2017			Sriram					TENJINCG-189
 * 27-06-2017         	Leelaprasad             Step execution error
 * 11-Aug-2017			Roshni					T25IT-120
 16-Aug-2017			Sriram					T25IT-139
 18-08-2017        		Leelaprasad             T25IT-132
 * 19-Oct-2017			Sriram Sridharan		TENJINCG-397
 * 26-Oct-2017		  	Roshni				   	TENJINCG-305 
 * 18-01-2018			Pushpalatha				TENJINCG-580
 * 22-03-2018        	Padmavathi              for navigation of back button in run information page
22-Dec-2017				Sahana					Mobility
26-Nov 2018				Shivam	Sharma			TENJINCG-904	
13-12-2018				Pushpalatha				TJNUN262-63				
06-03-2019				Preeti					TENJINCG-946,947	
02-05-2019				Roshni					TENJINCG-1046
12-06-2019				Roshni					V2.8-96 
25-09-2019				Roshni					TENJINCG-1097
* 06-02-2020			Roshni					TENJINCG-1168
* 11-05-2020			Ashiki					TJN27-81
 */


package com.ycs.tenjin.servlet;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.GsonBuilder;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.TestSetHelper;
import com.ycs.tenjin.handler.RunHandler;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.util.ExcelHandler;
import com.ycs.tenjin.util.TenjinMailThread;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class TestSetServlet
 */
//@WebServlet("/TestSetServlet")
public class TestSetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(TestSetServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TestSetServlet() {
		super();
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String txn = request.getParameter("param");

		TenjinSession tjnSession = (TenjinSession)request.getSession().getAttribute("TJN_SESSION");
		/*added by Roshni TENJINCG-1168 starts */
		RunHandler runHandler=new RunHandler();
		/*added by Roshni TENJINCG-1168 ends */
  if(txn.equalsIgnoreCase("populate_tcs")){
			String retData="";
			try{
				String tsId = request.getParameter("t1");
			JSONObject json = new JSONObject();
			try{
				TestSetHelper h = new TestSetHelper();
				ArrayList<TestCase> mappedTCs = h.hydrateMappedTCs(Integer.parseInt(tsId), tjnSession.getProject().getId());
				json.put("status", "SUCCESS");
				json.put("message", "");
				JSONArray jArray = new JSONArray();
				if(mappedTCs != null){
					for(TestCase t1:mappedTCs){
						JSONObject j = new JSONObject();
						j.put("recid", t1.getTcRecId());
						j.put("id", t1.getTcId());

						j.put("TC_NAME", t1.getTcName());
						j.put("TC_Priority", t1.getTcPriority());
						jArray.put(j);
					}
				}

				json.put("tests", jArray);
			}catch(Exception e){
				json.put("status", "ERROR");
				json.put("message", "An internal error occurred while fetching mapped tests. Please Try again");
			}finally{
				retData = json.toString();
			}
		}catch(Exception e){
			retData = "An internal error occurred. Please contact your System Administrator";
		}

		response.getWriter().write(retData);
		}
		/*Added by Roshni for  V2.8-96  ends*/

		/*  changed by latief for requirement# TJN_23_12 ENDS*/

		/*  changed by sahana for # TJN_24_03 starts*/
		else if(txn.equalsIgnoreCase("fetch_sch_map"))
		{
			logger.debug("fetch Testsets list  ");
			TestSetHelper helper = new TestSetHelper();
			String retData = "";

			try {

				JSONObject json = new JSONObject();
				try {
					/*changed by sahana to overcome null pointer exception: Starts*/
					int projectId=0;
					if(tjnSession.getProject()!=null)
						projectId = tjnSession.getProject().getId();
					if(projectId!=0){
						/*changed by sahana to overcome null pointer exception: ends*/
						ArrayList<TestSet> testSets = null;
						testSets = helper.hydrateAllTestSets(projectId);
						JSONArray jArray = new JSONArray();
						for (TestSet  tset: testSets) {
							JSONObject j = new JSONObject();
							j.put("name", tset.getName());
							j.put("id", tset.getId());
							/*Changed by Leelaprasad for the defect T25IT-132 starts*/
							j.put("mode",tset.getMode());
							/*Changed by Leelaprasad for the defect T25IT-132 ends*/

							jArray.put(j);
						}

						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("tSets", jArray);
						/*changed by sahana to overcome null pointer exception: Starts*/
					}
					else{
						json.put("status", "SUCCESS");
						json.put("message", "");
						json.put("tSets", "");
					}

				} catch (Exception e) {
					logger.error("An error occurred while processing test sets", e);
					/*changed by sahana to overcome null pointer exception: ends*/
					json.put("status", "ERROR");
					json.put("message",
							"An internal error occurred. Please try again.");


				} finally {
					retData = json.toString();
				}
			} catch (Exception e) {
				retData = "An internal error occurred. Please contact your System Administrator";
			}
			response.getWriter().write(retData);
		}
		/*  changed by sahana for # TJN_24_03 ends*/
		else if(txn.equalsIgnoreCase("map_tcs1")){
			String testSetRecId = request.getParameter("ts1");
			String tests = request.getParameter("tcs1");
			String retData = "";
			/*Changed by Leelaprasad for the requirement TJN_243_08 on 08-12-2016 starts*/
			//int tsRecId = Integer.parseInt(testSetRecId);
			int tsRecId = Integer.parseInt(testSetRecId.trim());
			/*Changed by Leelaprasad for the requirement TJN_243_08 on 08-12-2016 ends*/

			try{
				JSONObject json = new JSONObject();
				try{
					TestSetHelper tHelper = new TestSetHelper();
					//Changed by Sriram for TENJINCG-189 (12th June, 2017)
					/*tHelper.persistTestSetMap(tsRecId, tests, tjnSession.getProject().getId());*/
					tHelper.persistTestSetMap(tsRecId, tests, tjnSession.getProject().getId(), true);
					//Changed by Sriram for TENJINCG-189 (12th June, 2017) ends
					json.put("status","SUCCESS");
					/*Changed by Leelaprasad for the requirement TJN_243_08 on 08-12-2016 starts*/
					//json.put("message","");
					/*Changed by Leelaprasad for the requirement TENJINCG-83 starts*/
					/*json.put("message", "Testcases mapped successfully");*/
					json.put("message", "Testcases mapping updated successfully");
					/*Changed by Leelaprasad for the requirement TENJINCG-83 starts*/
					/*Changed by Leelaprasad for the requirement TJN_243_08 on 08-12-2016 ends*/
				}catch(Exception e){
					logger.error("Could not persist test set mmap",e);
					json.put("status", "ERROR");
					json.put("message", e.getMessage());
				}finally{
					retData = json.toString();
				}
			}catch(Exception e){
				retData = "An internal error occurred. Please contact your System Administrator.";
			}
			/* Added by Roshni for TENJINCG-1097 starts */
			Thread t=new Thread(new TenjinMailThread(tjnSession.getProject().getId(),tsRecId,"Test Set","update",tjnSession.getUser()));
			t.setName("Test Set");
			t.start();
			
			response.getWriter().write(retData);
		}

		
		/*  changed by latief for requirement# TJN_23_12*/
		else  if(txn.equalsIgnoreCase("unmapped_tcs")){
			String tsId = request.getParameter("t");

			// Added by sakthi on 07-04-2017 starts
			// Added by sakthi on 07-04-2017 ends
			String retData = "";
			try{
				JSONObject json = new JSONObject();
				try{
					TestSetHelper h = new TestSetHelper();

					// Added by sakthi on 07-04-2017 starts
					/*Modified by shruthi for TENJINCG-1177 starts*/
					/*ArrayList<TestCase> unmappedTCs = h.hydrateUnMappedTCs(Integer.parseInt(tsId), tjnSession.getProject().getId(), mode);*/
					ArrayList<TestCase> unmappedTCs = h.hydrateUnMappedTCs(Integer.parseInt(tsId), tjnSession.getProject().getId());
					// Added by sakthi on 07-04-2017 ends
					/*Modified by shruthi for TENJINCG-1177 ends*/
	
					json.put("status", "SUCCESS");
					json.put("message", "");
					JSONArray jArray1 = new JSONArray();
					if(unmappedTCs != null){
						for(TestCase t:unmappedTCs){
							JSONObject j = new JSONObject();
							j.put("recid", t.getTcRecId());
							j.put("id", t.getTcId());
							j.put("TC_NAME", t.getTcName());
							jArray1.put(j);

						}
					}

					json.put("tests", jArray1);
				}catch(Exception e){
					json.put("status", "ERROR");
					json.put("message", "An internal error occurred while fetching unmapped tests. Please Try again");
				}finally{
					retData = json.toString();
				}
			}catch(Exception e){
				retData = "An internal error occurred. Please contact your System Administrator";
			}

			response.getWriter().write(retData);

		}

		/*  changed by latief for requirement# TJN_23_12 ENDS*/
		else  if(txn.equalsIgnoreCase("mapped_tcs1")){
			String tsId = request.getParameter("t1");
			/*Modified by Preeti for TENJINCG-946,947 starts*/
			ArrayList<TestCase> mappedTCs = new ArrayList<TestCase>();
			try {
				TestSetHelper h = new TestSetHelper();
				mappedTCs = h.hydrateMappedTCs(Integer.parseInt(tsId), tjnSession.getProject().getId());
			} catch (DatabaseException e) {
				logger.error("Could not fetch mapped cases");
			} finally {
				response.getWriter().write(new GsonBuilder().create().toJson(mappedTCs));
			}
			
		}
		else if(txn.equalsIgnoreCase("unmap_tcs")){
			String testSetRecId = request.getParameter("ts");
			String mapTestcase = request.getParameter("tcs");
			String retData = "";
			/*Changed by Leelaprasad for the requirement TJN_243_08 on 08-12-2016 starts*/
			//	int tsRecId = Integer.parseInt(testSetRecId);
			int tsRecId = Integer.parseInt(testSetRecId.trim());
			/*Changed by Leelaprasad for the requirement TJN_243_08 on 08-12-2016 starts*/

			try{
				JSONObject json = new JSONObject();
				try{ 
					
					TestSetHelper tHelper=new TestSetHelper();
					tHelper.persistTestSetunMap(tsRecId, mapTestcase, tjnSession.getProject().getId());
					json.put("status","SUCCESS");
					json.put("message", "Testcases mapping updated successfully");
					/*Added by Ashiki for TJN27-81 ends*/
				}catch(Exception e){
					logger.error("Could not persist test set mmap",e);
					json.put("status", "ERROR");
					json.put("message", e.getMessage());
				}finally{
					retData = json.toString();
				}
			}catch(Exception e){
				retData = "An internal error occurred. Please contact your System Administrator.";
			}
			
			response.getWriter().write(retData);
		}else if(txn.equalsIgnoreCase("initiate_run")){
			Map<String, Object> map = new HashMap<String,Object>();
			String testSetRecId = request.getParameter("ts");
			/* Added by Roshni for T25IT-120 starts */
			String entity_name=request.getParameter("entity_name");
			/* Added by Roshni for T25IT-120 ends */
			/*	added by Padmavathi  for navigation of back button in run information page starts*/
			String callback=request.getParameter("callback");
			/*	added by Padmavathi  for navigation of back button in run information page ends*/
			try{
				/*Modified by Roshni TENJINCG-1168 starts */
				/*TestSet t = new TestSetHelper().hydrateTestSetBasicDetails(Integer.parseInt(testSetRecId), tjnSession.getProject().getId());*/
				TestSet t = runHandler.hydrateTestSetBasicDetails(Integer.parseInt(testSetRecId), tjnSession.getProject().getId());
				/*Modified by Roshni TENJINCG-1168 ends */
				/***********************************************
				 * Added by Sriram to fix Execution Bug in Tenjin v2.3 (12-09-2016)
				 */

				/***********
				 * Changed by Sriram for TENJINCG-165 (11-04-2017)
				 */
				
				if(!Utilities.trim(t.getMode()).equalsIgnoreCase("API")) {
					/* For TENJINCG-397 */
					/*ArrayList<RegisteredClient> clients = new ClientHelper().hydrateAllClients();*/
					/*modified by Roshni TENJINCG-1168 starts */
					/*List<RegisteredClient> clients = new ClientHelper().hydrateAllClients();*/
					List<RegisteredClient> clients = runHandler.hydrateAllClients();
					/*modified by Roshni TENJINCG-1168 ends */
					/* For TENJINCG-397 ends*/
					map.put("CLIENT_LIST", clients);
				}

				map.put("STATUS", "");
				map.put("MESSAGE", "");
				map.put("TEST_SET", t);
				map.put("INVOKED","ts");
				map.put("ID",testSetRecId);
				/*	added by Padmavathi  for navigation of back button in run information page starts*/
				map.put("TEST_SET_ID",testSetRecId);
				map.put("CALL_BACK", callback);
				/*	added by Padmavathi  for navigation of back button in run information page ends*/
				/* Added by Roshni for T25IT-120 starts */
				map.put("ENTITY_ID", testSetRecId);
				map.put("ENTITY_NAME", entity_name);
				/* Added by Roshni for T25IT-120 ends */
				/*changed by sahana for Mobility: Starts*/
				//commented by shivam sharma for  TENJINCG-904 starts
				/*map.put("DEVICE_LIST", devices);*/
				//commented by shivam sharma for  TENJINCG-904 ends
				/*changed by sahana for Mobility: ends*/

			}catch(Exception e){
				logger.error("Could not initiate run",e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "An internal error occurred. Please try again");
			}finally{
				/*Added by paneendra for TENJINCG-1267 starts*/
				runHandler.CloseConnection();	
				/*Added by paneendra for TENJINCG-1267 ends*/
				request.getSession().setAttribute("SCR_MAP", map);
			}

			response.sendRedirect("runinit.jsp");
		}

		/****************************************************************************
		 * Added by Sriram for Req#TJN_24_01 (19-Sep-2016)
		 */
		else if(txn.equalsIgnoreCase("initiate_adhoc_run_tc")){
			Map<String, Object> map = new HashMap<String,Object>();
			String testCaseRecId = request.getParameter("tc");
			/* Added by Roshni for T25IT-120 starts */
			String entity_name=request.getParameter("entity_name");
			/* Added by Roshni for T25IT-120 ends */
			/*	added by Padmavathi  for navigation of back button in run information page starts*/
			String callback=request.getParameter("callback");
			/*	added by Padmavathi  for navigation of back button in run information page ends*/

			try{
				TestSet t = new TestSet();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_hhmmss");
				/*Changed by leelaprasad for TJNUN262-53 starts*/
				/*t.setName("AdHoc_Test" + "_" + sdf.format(new Date()));*/
				t.setName("Test" + "_" + sdf.format(new Date()));
				/*Changed by leelaprasad for TJNUN262-53 ends*/map.put("STATUS", "");
				map.put("MESSAGE", "");
				map.put("INVOKED","tc");
				map.put("ID",testCaseRecId);
				/* Added by Roshni for T25IT-120 starts */
				map.put("ENTITY_ID",testCaseRecId);
				map.put("ENTITY_NAME", entity_name);
				/* Added by Roshni for T25IT-120 ends */
				/*modified by Roshni TENJINCG-1168 starts */
				TestCase testCase =runHandler.hydrateTestCase(tjnSession.getProject().getId(), Integer.parseInt(testCaseRecId));
				//TestCase testCase = new TestCaseHelper().hydrateTestCase(tjnSession.getProject().getId(), Integer.parseInt(testCaseRecId));
				/*modified by Roshni TENJINCG-1168 ends */
				/* Added by Sriram for TENJINCG-171 */
				t.setMode(testCase.getMode());
				/* Added by Sriram for TENJINCG-171 ends*/
				/*	added by Padmavathi  for navigation of back button in run information page starts*/
				map.put("TESTCASE_ID",testCaseRecId);
				map.put("CALL_BACK", callback);
				/*	added by Padmavathi  for navigation of back button in run information page ends*/
				ArrayList<TestCase> tests = new ArrayList<TestCase>();
				tests.add(testCase);
				t.setTests(tests);
				map.put("TEST_SET", t);
				


				/***********************************************
				 * Added by Sriram to fix Execution Bug in Tenjin v2.3 (12-09-2016)
				 */
				/***********
				 * Changed by Sriram for TENJINCG-165 (11-04-2017)
				 */
				/*ArrayList<RegisteredClient> clients = new ClientHelper().hydrateAllClients();
				map.put("CLIENT_LIST", clients);*/
				/*if(!Utilities.trim(testCase.getMode()).equalsIgnoreCase("api")) {*/
					/* For TENJINCG-397 */
					/*ArrayList<RegisteredClient> clients = new ClientHelper().hydrateAllClients();*/
					/*Modified by Roshni TENJINCG-1168 starts */
					/*List<RegisteredClient> clients = new ClientHelper().hydrateAllClients();*/
					List<RegisteredClient> clients = runHandler.hydrateAllClients();
					/*added by Roshni TENJINCG-1168 ends */
					/* For TENJINCG-397 ends*/
					map.put("CLIENT_LIST", clients);
				/*}*/
				/***********
				 * Changed by Sriram for TENJINCG-165 (11-04-2017)
				 */
				/***********************************************
				 * Added by Sriram to fix Execution Bug in Tenjin v2.3 (12-09-2016)
				 */

			}catch(Exception e){
				logger.error("Could not initiate run",e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "An internal error occurred. Please try again");
			}finally{
				/*Added by paneendra for TENJINCG-1267 starts*/
				runHandler.CloseConnection();	
				/*Added by paneendra for TENJINCG-1267 ends*/
				request.getSession().setAttribute("SCR_MAP", map);
			}

			response.sendRedirect("runinit.jsp");
		}
		/****************************************************************************
		 * Added by Sriram for Req#TJN_24_01 (19-Sep-2016) ends
		 */
		/****************************************************************************
		 * Added by Sriram for Req#TJN_243_11 (20-Dec-2016)
		 */
		else if(txn.equalsIgnoreCase("initiate_adhoc_run_ts")){
			Map<String, Object> map = new HashMap<String,Object>();
			String testStepRecId = request.getParameter("ts");
			/* Added by Roshni for T25IT-120 starts */
			String entity_name=request.getParameter("entity_name");
			/* Added by Roshni for T25IT-120 ends */
			/*	added by Padmavathi  for navigation of back button in run information page starts*/
			String callback=request.getParameter("callback");
			/*	added by Padmavathi  for navigation of back button in run information page ends*/

			try{
				TestSet t = new TestSet();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_hhmmss");
				/*Changed by leelaprasad for TJNUN262-53 starts*/
				/*t.setName("AdHoc_Test" + "_" + sdf.format(new Date()));*/
				t.setName("Test" + "_" + sdf.format(new Date()));
				/*Changed by leelaprasad for TJNUN262-53 ends*/
				map.put("STATUS", "");
				map.put("MESSAGE", "");
				map.put("INVOKED","tc");
				map.put("STEP_REC_ID",testStepRecId);
				/*	added by Padmavathi  for navigation of back button in run information page starts*/
				map.put("CALL_BACK", callback);
				/*	added by Padmavathi  for navigation of back button in run information page ends*/
				/*modified by Roshni TENJINCG-1168 starts */
				/*TestCase testCase = new TestCaseHelper().hydrateTestCaseForStep(tjnSession.getProject().getId(), Integer.parseInt(testStepRecId));*/
				TestCase testCase = runHandler.hydrateTestCaseForStep(tjnSession.getProject().getId(), Integer.parseInt(testStepRecId));
				/*modified by Roshni TENJINCG-1168 ends */
				map.put("ID", testCase.getTcRecId());
				ArrayList<TestCase> tests = new ArrayList<TestCase>();
				tests.add(testCase);
				t.setTests(tests);
				/* Fix for T25IT-139 */
				t.setMode(testCase.getMode());
				/* Fix for T25IT-139 ends*/
				/*Added By Leelaprasad for the requirement of step execution error starts*/
				//t.setMode("GUI");
				/*Added By Leelaprasad for the requirement of step execution error ends*/
				map.put("TEST_SET", t);
				/* Added by Roshni for T25IT-120 starts */
				map.put("ENTITY_ID",testStepRecId);
				map.put("ENTITY_NAME", entity_name);
				/* Added by Roshni for T25IT-120 ends */

				/*changed by sahana for Mobility: Starts*/
				//commented by shivam sharma for  TENJINCG-904 starts
				/*List<RegisteredDevice> devices=new DeviceHelper().hydrateAllDevicesWithStatus();
				List<RegisteredDevice> devices= new Pcloudy().getDevices(); 
				map.put("DEVICE_LIST", devices);*/
				//commented by shivam sharma for  TENJINCG-904 ends
				/*changed by sahana for Mobility: ends*/

				/***********************************************
				 * Added by Sriram to fix Execution Bug in Tenjin v2.3 (12-09-2016)
				 */
				/***********
				 * Changed by Sriram for TENJINCG-165 (11-04-2017)
				 */
				/*ArrayList<RegisteredClient> clients = new ClientHelper().hydrateAllClients();
				map.put("CLIENT_LIST", clients);*/
				if(! Utilities.trim(testCase.getMode()).equalsIgnoreCase("api")) {
					/* For TENJINCG-397 */
					/*ArrayList<RegisteredClient> clients = new ClientHelper().hydrateAllClients();*/
					/*modified by Roshni TENJINCG-1168 starts */
					/*List<RegisteredClient> clients = new ClientHelper().hydrateAllClients();*/
					List<RegisteredClient> clients = runHandler.hydrateAllClients();
					/*modified by Roshni TENJINCG-1168 ends */
					/* For TENJINCG-397 ends*/
					map.put("CLIENT_LIST", clients);
				}

				/***********
				 * Changed by Sriram for TENJINCG-165 (11-04-2017)
				 */
				/***********************************************
				 * Added by Sriram to fix Execution Bug in Tenjin v2.3 (12-09-2016)
				 */

			}catch(Exception e){
				logger.error("Could not initiate run",e);
				map.put("STATUS", "ERROR");
				map.put("MESSAGE", "An internal error occurred. Please try again");
			}finally{
				runHandler.CloseConnection();	
				request.getSession().setAttribute("SCR_MAP", map);
			}

			response.sendRedirect("runinit.jsp");
		}
		/****************************************************************************
		 * Added by Sriram for Req#TJN_243_11 (20-Dec-2016) ends
		 */
	
		
		else if(txn.equalsIgnoreCase("fetch_unmapped_tcs")){
			String tsId = request.getParameter("testset");
			String tsName=request.getParameter("testsetname");
			// Added by sakthi on 07-04-2017 starts
			String mode=request.getParameter("mode");
			// Added by sakthi on 07-04-2017 ends
			Map<String, Object> map = new HashMap<String, Object>();
			/*Modified by Preeti for TENJINCG-946,947 starts*/
			map.put("tsRecId", tsId);
			map.put("tsName" ,tsName);
			map.put("mode", mode);
			
			request.getSession().setAttribute("SCR_MAP", map);
			/*Modified by Preeti for TENJINCG-946,947 ends*/
			response.sendRedirect("tsetmap.jsp");
		}
		/*Added by Pushpa for TJNUN262-63 starts*/
		else if(txn.equalsIgnoreCase("fetch_unmapped_tcsList")){
			String tsId = request.getParameter("testset");
			
			int tsRecID=Integer.parseInt(tsId.trim());
			TestSetHelper h = new TestSetHelper();

			ArrayList<TestCase> unmappedTCs=new ArrayList<TestCase>();
			
			try {
				/*Modified by shruthi for TENJINCG-1177 starts*/
				/*unmappedTCs = h.hydrateUnMappedTCs(tsRecID, tjnSession.getProject().getId(), mode);*/
				unmappedTCs = h.hydrateUnMappedTCs(tsRecID, tjnSession.getProject().getId());
				/*Modified by shruthi for TENJINCG-1177 ends*/
			
			} catch (DatabaseException e) {
				logger.error("Could not fetch project users");
			} finally {
				response.getWriter().write(new GsonBuilder().create().toJson(unmappedTCs));
			}
			
		}
		/*Added by Pushpa for TJNUN262-63 ends*/

		/*Changed by Leelaprasad for the requirement TJN_243_08 on 08-12-2016 ends*/
		/*Changed by Leelaprasad for the requirement TENJINCG-20 starts*/
		
		else if(txn.equalsIgnoreCase("download_tc_for_ts")){
			String retData = "";
			int testSetId=Integer.parseInt(request.getParameter("txtTestSetId"));
			try{
				JSONObject json = new JSONObject();
				try{
					String folderPath = request.getServletContext().getRealPath("/");
					Utilities.checkForDownloadsFolder(folderPath);
					/*Changed by Pushpalatha for TENJINCG-567 starts*/
					String filename="TestSet_"+testSetId+".xlsx";
					/*Changed by Pushpalatha for TENJINCG-567 ends*/
					ExcelHandler e=new ExcelHandler();
					long startMillis = System.currentTimeMillis();
					/*Changed by Pushpalatha for TENJINCG-567 starts*/
					e.downloadTestCasesForSet(tjnSession.getProject().getId(), folderPath + File.separator+"Downloads", filename,testSetId);
					/*Changed by Pushpalatha for TENJINCG-567 ends*/
					logger.info("Test Cases downloaded in {}ms", Utilities.calculateElapsedTime(startMillis, System.currentTimeMillis()));
					json.put("status", "SUCCESS");
					json.put("message","TestCase(s) and TestSet(s) Data Downloaded successfully");
					/*Changed by Pushpalatha for TENJINCG-567 starts*/
					json.put("path","Downloads"+File.separator +filename);
					/*Changed by Pushpalatha for TENJINCG-567 ends*/

				}catch(Exception e){
					logger.error("An exception occured while deleteing test steps",e);
					json.put("status", "ERROR");
					json.put("message","An Internal Error Occurred. Please try again");
				}finally{
					retData = json.toString();
				}
			}catch(Exception e){
				retData = "An Internal Error Occurred. Please contact your system administrator";
			}

			response.getWriter().write(retData);
		}
		/* Added by Roshni for TENJINCG-305 ends*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

}
