/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MessageServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                	 CHANGED BY                DESCRIPTION
15-06-2021				Ashiki					Newly adde for TENJINCG-1275
 */

package com.ycs.tenjin.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.message.MessageValidateApplication;
import com.ycs.tenjin.bridge.oem.message.MessageValidateApplicationFactory;
import com.ycs.tenjin.bridge.oem.message.datahandler.MessageValidateTestDataHandler;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.MessageValidate;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.MessageValidateHandler;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.util.Utilities;



/**
 * Servlet implementation class MessageServlet
 */
@WebServlet("/MessageServlet")
@MultipartConfig
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(MessageServlet.class);
	@SuppressWarnings("unused")
	private Object fileItem;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MessageServlet() {
        super();
    }

    public static AuthorizationRule authorizationRule = new AuthorizationRule() {
        
        @Override
        public boolean checkAccess(HttpServletRequest request) {
        	if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
                    return true;
            }
            return false;
        }  
    };
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		String task = Utilities.trim(request.getParameter("param"));
		Authorizer authorizer=new Authorizer();
		if(task.equalsIgnoreCase("") || task.equalsIgnoreCase("new") || task.equalsIgnoreCase("list") || task.equalsIgnoreCase("DOWNLOAD_DATA_TEMPLATE") || task.equalsIgnoreCase("deleteall")) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
	   if(task.equalsIgnoreCase("new")){
			List<String> msgFormat=null;
				try {
					msgFormat = MessageValidateApplicationFactory.getAvailableMessageAdapters();
				} catch (BridgeException e) {
					logger.error("Error ", e);
				}
			
			request.setAttribute("formats", msgFormat);
			request.getRequestDispatcher("v2/messages_new.jsp").forward(request, response);
			
		}else if(task.equalsIgnoreCase("list")){
			List<MessageValidate> messageList=null;
			try {
				messageList=new MessageValidateHandler().hydrateAllMessageFormat();
			} catch (DatabaseException e) {
				logger.error("ERROR fetching message information", e);
			}
			    request.setAttribute("messagelist", messageList);
			    RequestDispatcher dispatcher = request.getRequestDispatcher("v2/messages_list.jsp");
				dispatcher.forward(request, response);
		}else if(task.equalsIgnoreCase("DOWNLOAD_DATA_TEMPLATE")){
			JSONObject json = new JSONObject();
			String retData = "";
			try {
				try {
					String msgId = request.getParameter("msgId");
					String folderPath = request.getServletContext().getRealPath("/");
					Utilities.checkForDownloadsFolder(folderPath);
					folderPath = folderPath + "Downloads";
					String templatePath="";
					try {
						MessageValidate message=new MessageValidateHandler().hydrateXML(Integer.parseInt(msgId));
						byte[] decoded = Base64.decodeBase64(message.getContextType());
						String xmlDecrytedValue = new String(decoded, StandardCharsets.UTF_8);
						MessageValidateApplication adaptersLearner = MessageValidateApplicationFactory.getLeaner(message.getMessageFormat());

						String learnType=xmlDecrytedValue;
						Map<String, String> fieldList = new MessageValidateHandler().hydrateFieldLabels(message.getMessageFormat(),message.getSubType());
                        Map<Location, ArrayList<String>> learntMap=adaptersLearner.learn(message, learnType,fieldList);
						MessageValidateTestDataHandler  template= MessageValidateApplicationFactory.getDataHandler();

						templatePath=template.generateMessageTemplate(folderPath,message.getMessageName(),learntMap);
						
						logger.info("Template generated successfully");
					} catch (Exception e) {
						logger.error("Error ", e);
					}
					json.put("status", "SUCCESS");
					json.put("message", "Dowmloaded Sucessfully");
					json.put("path", "Downloads" + File.separator + templatePath);
				} catch (Exception e) {
					json.put("status", "ERROR");
					json.put("message", e.getMessage());
				} finally {
					retData = json.toString();
				}
			}catch(JSONException e){
				logger.error("ERROR occurred while generating JSON after template generation", e);
				retData = "An internal error occurred. Please contact Tenjin Support";
			}
			response.getWriter().write(retData);
		}
		else if(task.equalsIgnoreCase("view")) {
			
		}
		
		else if(task.equalsIgnoreCase("fetch_messageType_for_app")){
			String app = request.getParameter("app");
			String retData = "";
			try {
				List<MessageValidate> messageType = new MessageValidateHandler().hydrateAllMsgs(app);
				JSONObject json = new JSONObject();
				JSONArray jArray = new JSONArray();
				for(MessageValidate msg:messageType) {
					JSONObject js = new JSONObject();
					js.put("subtype", msg.getSubType());
					js.put("message", msg.getMessageName());
					js.put("type", msg.getMessageFormat());
					jArray.put(js);
				}
				json.put("status", "SUCCESS");
				json.put("message", "");
				json.put("messageType", jArray);
				retData = json.toString();
			    response.getWriter().write(retData);
			} catch (NumberFormatException e) {
				
				logger.error("Error ", e);
			} catch (JSONException e) {
				
				logger.error("Error ", e);
			}
		}
		else if(task.equalsIgnoreCase("fetch_messageType")){

			String app = request.getParameter("app");
			String retData = "";
			try {
				List<MessageValidate> messageType = new MessageValidateHandler().hydrateAllMsgs(app);
				JSONObject json = new JSONObject();
				JSONArray jArray = new JSONArray();
				for(MessageValidate msg:messageType) {
					JSONObject js = new JSONObject();
					js.put("type", msg.getType());
					js.put("message", msg.getName());
					jArray.put(js);
				}
				json.put("status", "SUCCESS");
				json.put("message", "");
				json.put("messageType", jArray);
				retData = json.toString();
			    response.getWriter().write(retData);
			} catch (NumberFormatException e) {
				logger.error("Error ", e);
			} catch (JSONException e) {
				logger.error("Error ", e);
			}
		
		}else if(task.equalsIgnoreCase("deleteall")){
			
			String msgId=request.getParameter("paramval");
			JSONObject retJson = new JSONObject();
			String retData = "";
			String[] messageId=msgId.split(",");
			String values="";
		    for(int i=0;i<messageId.length;i++){
		    	values=messageId[i]+",";
		    	try {
		    	try {
		    		new MessageValidateHandler().deleteMessage(values);
		    		retJson.put("status", "SUCCESS");
					retJson.put("message", "Message(s) deleted successfully");
				} catch (DatabaseException e) {
				
					retJson.put("status", "ERROR");
					retJson.put("message", "Could not delete Message(s)");
				} catch (JSONException e) {
					logger.error("Error ", e);
				}finally {
					retData = retJson.toString();
				}
		    	}
		    	catch (Exception e) {
		    		retData = "An internal error occurred. Please contact your System Administrator";
				}
		    }
		    response.getWriter().write(retData);
		}
	}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		String delFlag = Utilities.trim(request.getParameter("del"));
		Authorizer authorizer=new Authorizer();
		if(delFlag.equalsIgnoreCase("") ) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		if(delFlag.equalsIgnoreCase("true")){
			doDelete(request, response);
		}
		else{
			String dispatchUrl = "";
			List<MessageValidate> messageList=null;
			try {
				MessageValidate message =this.mapAttributes(request);
				new MessageValidateHandler().persistMessage(message);
				messageList=new MessageValidateHandler().hydrateAllMessageFormat();
				request.setAttribute("messagelist", messageList);
				SessionUtils.setScreenState("success", "Message created successfully", request);
			      SessionUtils.loadScreenStateToRequest(request);
		           request.getRequestDispatcher("v2/messages_list.jsp").forward(request, response);	
				}catch (RequestValidationException e) {
					request.setAttribute("messagelist", messageList);
					logger.error(e.getMessage());
					dispatchUrl = "v2/messages_new.jsp";
					SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
					SessionUtils.loadScreenStateToRequest(request);
					request.getRequestDispatcher(dispatchUrl).forward(request, response);
				}  catch (DatabaseException e) {
					logger.error("ERROR in saving Message information", e);
				}
			
			
					
		}
		
	}
	
	
	 private MessageValidate mapAttributes(HttpServletRequest request) throws IOException, ServletException {
		 MessageValidate message = new MessageValidate();
		message.setMessageFormat(request.getParameter("msgFormat"));
		message.setMessageName(request.getParameter("messageName"));
	    message.setApplicationId(Integer.parseInt(request.getParameter("aut")));
		message.setFileName(extractFileName(request.getPart("txtInputFile")));
		
		if(message.getMessageFormat().equalsIgnoreCase("ISO20022")){
			message.setSubType(request.getParameter("txtStepMsgType"));
		}
		else if(message.getMessageFormat().equalsIgnoreCase("ISO8583")){
			message.setSubType(request.getParameter("txtStepMsgType1"));
		}
		else{
			message.setSubType(request.getParameter("txtStepMsgType2"));
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(request.getPart("txtInputFile").getInputStream(), "UTF-8"));
		StringBuilder value = new StringBuilder();
		char[] buffer = new char[1024];
		for (int length = 0; (length = reader.read(buffer)) > 0;) {
		value.append(buffer, 0, length);
		}
		String fileContent= value.toString();
		String encodedContent = new String(Base64.encodeBase64(fileContent.getBytes()));
		message.setFileContent(encodedContent);
		return message;
		
	}

	@SuppressWarnings("unused")
	private String extractFileName(Part part) {
	        String contentDisp = part.getHeader("content-disposition");
	        String contentStore = part.getHeader("StoreLocation");
	        
	        String[] items = contentDisp.split(";");
	        for (String s : items) {
	            if (s.trim().startsWith("filename")) {
	                return s.substring(s.indexOf("=") + 2, s.length() - 1);
	            }
	        }
	        return "";
	    }
	
	 
	
	     
}
