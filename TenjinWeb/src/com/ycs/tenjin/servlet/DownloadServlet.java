/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: DownloadServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 	  CHANGED BY              DESCRIPTION
 * 
 * 
 */

package com.ycs.tenjin.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileDeleteStrategy;
import org.slf4j.Logger;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;


/**
 * Servlet implementation class DownloadServlet
 */
@WebServlet("/DownloadServlet")
public class DownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DownloadServlet.class);
	private static final int DEFAULT_BUFFER_SIZE = 40240;


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DownloadServlet() {
		super();
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String downloadPath=request.getParameter("param");
		String filePath = null;
		try {
			filePath = TenjinConfiguration.getProperty("TJN_WORK_PATH");
		} catch (TenjinConfigurationException e) {
			logger.error("could not load the file from the path:"+filePath);		
		}
		File downloadFile = new File(filePath+File.separator+downloadPath);
		Path rootpath = downloadFile.toPath().getParent();
		File filerootPath = rootpath.toFile();
		if (downloadFile.exists()) {
			String contentType = getServletContext().getMimeType(downloadFile.getName());


			if (contentType == null) {
				contentType = "application/octet-stream";
			}

			response.reset();
			response.setBufferSize(DEFAULT_BUFFER_SIZE);
			response.setContentType(contentType);
			response.setHeader("Content-Length", String.valueOf(downloadFile.length()));
			response.setHeader("Content-Disposition", "attachment; filename=\"" + downloadFile.getName() + "\"");

			BufferedInputStream bufferedinptstream = null;
			BufferedOutputStream bufferedoutptstream = null;

			try {
				bufferedinptstream = new BufferedInputStream(new FileInputStream(downloadFile), DEFAULT_BUFFER_SIZE);
				bufferedoutptstream = new BufferedOutputStream(response.getOutputStream(),DEFAULT_BUFFER_SIZE);
				byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
				int length;
				while ((length = bufferedinptstream.read(buffer)) > 0) {
					bufferedoutptstream.write(buffer, 0, length);
				}
			}catch(Exception fi) {
				logger.error("Error in downloading the file:"+fi);
			}finally {
				close(bufferedoutptstream);
				close(bufferedinptstream);
			}


			String[] myFiles;   
			if (filerootPath.isDirectory()) {
				myFiles = filerootPath.list();
				for (int i = 0; i < myFiles.length; i++) {
					File myFile = new File(filerootPath, myFiles[i]);
					System.gc();
					try {
						Thread.sleep(2000);
					} catch (InterruptedException Ie) {
						logger.error("Error in deleting  the files from the directory:"+Ie);
					}
					FileDeleteStrategy.FORCE.delete(myFile);

				}
			
			
		}
		}else {
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				map.put("MESSAGE","Invalid Request");
				request.getSession().setAttribute("SCR_MAP", map);
				RequestDispatcher rd = request.getRequestDispatcher("noAccess.jsp");
				rd.forward(request, response);
			} catch (Exception e) {
				logger.error(" file not exist");
			}

		}

	}



	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		

	}


	private static void close(Closeable resource) {
		if (resource != null) {
			try {
				resource.close();
			} catch (IOException e) {
				logger.error("error in closing the resource:"+e);
			}
		}
	}
}

