/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DTTSubsetServlet.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*19-09-2019				Ashiki				Newly added by TENJINCG-1101
*06-02-2020				Roshni					TENJINCG-1168
*02-06-2020				Ashiki					Tenj210-54
*08-12-2020				Pushpa					TENJINCG-1220
*11-12-2020				Pushpa					TENJINCG-1221

			
*/


/*
 * Added File by sahana for Requirement TJN_24_06
 */
package com.ycs.tenjin.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.RateLimiter;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.defect.DTTSubset;
import com.ycs.tenjin.defect.DefectFactory;
import com.ycs.tenjin.defect.DefectManagementInstance;
import com.ycs.tenjin.defect.DefectManager;
import com.ycs.tenjin.defect.EDMException;
import com.ycs.tenjin.defect.UnreachableEDMException;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.DTTSubsetHandler;
import com.ycs.tenjin.security.AdminAuthorizationRuleImpl;
import com.ycs.tenjin.security.AuthorizationRule;
import com.ycs.tenjin.security.Authorizer;
import com.ycs.tenjin.servlet.util.SessionUtils;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

/**
 * Servlet implementation class DTTSubsetServlet
 */
@WebServlet("/DTTSubsetServlet")
public class DTTSubsetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory	.getLogger(DTTSubsetServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DTTSubsetServlet() {
        super();
        
    }
    
   public static RateLimiter limiter = RateLimiter.create(1.0);
   
    public static AuthorizationRule authorizationRule = new AuthorizationRule() {

		@Override
		public boolean checkAccess(HttpServletRequest request) {
			if(new AdminAuthorizationRuleImpl().checkAccess(request) ) {
				return true;
			}
			return false;
		}  
	};

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionUtils.loadScreenStateToRequest(request);
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		User user = tjnSession.getUser();
		SessionUtils.loadScreenStateToRequest(request);
		String task = Utilities.trim(request.getParameter("param"));
		String dispatchUrl = "";
		Authorizer authorizer=new Authorizer();
		if(task.equalsIgnoreCase("FETCH_ALL_SUBSET") || task.equalsIgnoreCase("Persist_Projects") || task.equalsIgnoreCase("view") || task.equalsIgnoreCase("Add_Projects") || task.equalsIgnoreCase("Remove_Projects")|| task.equalsIgnoreCase("new") ) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		
		if(task.equalsIgnoreCase("FETCH_ALL_SUBSET")) {
			try {
				Map<String,Object> map = new HashMap<String,Object>();
				String[] projects=null;
				List<DTTSubset> subsetList = new DTTSubsetHandler().getAllDTTSubsets();
				for(DTTSubset subset:subsetList) {
					int projCount =0;
					String project=subset.getProjects();
					if(project!=null) {
						projects=project.split(",");
						projCount=projects.length;
					}
					
					map.put(subset.getSubSetName(),projCount);
				}
				
				request.setAttribute("subsetLists", subsetList);
				request.setAttribute("ProjectCount", map);
				request.getRequestDispatcher("v2/dtt_subset_list.jsp").forward(request, response);
			} catch (DatabaseException e) {
				logger.error("ERROR fetching subset information", e);
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		}
		
		if(task.equalsIgnoreCase("new")) {
			List<String> instances=null;
			try {
				instances =new DTTSubsetHandler().fetchAllInstances();
			} catch (DatabaseException e) {
				logger.error("Error ", e);
			}
			request.setAttribute("instances", instances);
			request.getRequestDispatcher("v2/dtt_subset_new.jsp").forward(request, response);
		}
		
		if(task.equalsIgnoreCase("Fetch_Projects")) {
			String instance = request.getParameter("instance");
			String retData="";
			JSONObject rJson=null;
			JSONArray dttProjectsArray = new JSONArray();
			DefectManagementInstance defectManagement = new DefectManagementInstance();
			 /*Added by paneendra for VAPT fix starts*/
			CryptoUtilities cryptoUtilities=new CryptoUtilities();
			 /*Added by paneendra for VAPT fix ends*/
			try {
				rJson = new JSONObject();
				defectManagement = new DTTSubsetHandler().getDefectInstance(instance);
				String url = defectManagement.getURL();
				String username = defectManagement.getAdminId();
				String encPwd = defectManagement.getPassword();
				/*Added by Pushpa for TENJINCG-1220 starts*/
				String gitOrg=defectManagement.getOrganization();
				/*Added by Pushpa for TENJINCG-1220 ends*/
				/*Modified by paneendra for VAPT fix starts*/
				/*String password = new Crypto().decrypt(encPwd.split("!#!")[1],decodeHex(encPwd.split("!#!")[0].toCharArray()));*/
				String password = cryptoUtilities.decrypt(encPwd);
				/*Modified by paneendra for VAPT fix ends*/
				DefectManager dm = null;
				/*Modified by Pushpa for TENJINCG-1220 starts*/
				if(gitOrg==null){
					dm = DefectFactory.getDefectTools(defectManagement.getTool(), url, username, password);
				}else{
					dm = DefectFactory.getDefectTools(defectManagement.getTool(), url, username, password,gitOrg);
				}
				/*Modified by Pushpa for TENJINCG-1220 ends*/
				if(dm.isUrlValid()){
					dm.login(); 
					dttProjectsArray = dm.getAllProjects();
					dm.logout();
					JSONArray projectArray=new JSONArray();
					for (int i=0;i<dttProjectsArray.length();i++){ 
						JSONObject projectJson=new JSONObject();
						projectJson.put("pname", dttProjectsArray.getJSONObject(i).getString("pname"));
						projectJson.put("pid", dttProjectsArray.getJSONObject(i).getString("pid"));
						/*Added by Pushpalatha for TENJINCG-1221 starts*/
						try{
						projectJson.put("owner", dttProjectsArray.getJSONObject(i).getString("owner"));
						}catch(Exception e){
							projectJson.put("owner", "NA");
						}
						/*Added by Pushpalatha for TENJINCG-1221 ends*/
						projectArray.put(projectJson);
					   }
					
					rJson.put("projectLists", projectArray);
					rJson.put("status", "SUCCESS");
				}
				else{
					rJson.put("status", "ERROR");
					rJson.put("message", defectManagement.getTool() + " is not accessible. Please check url and try again.");
				}
			} catch (DatabaseException e) {
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
			} catch (EDMException e) {
				logger.error("Error ", e);
				/*Added by Ashiki for Tenj210-54 starts*/
				logger.error(e.getMessage());
				try {
					rJson.put("status", "ERROR");
					rJson.put("message", e.getMessage());
				} catch (JSONException e1) {
					logger.error("Error ", e1);
				}
				/*Added by Ashiki for Tenj210-54 starts*/
			} catch (UnreachableEDMException e) {
				logger.error("Error ", e);
			} catch (JSONException e) {
				logger.error("Error ", e);
			}catch (Exception e) {
				logger.error("Error ", e);
				logger.error(e.getMessage());
				dispatchUrl = "DTTSubsetServlet?param=new";
				SessionUtils.setScreenState("error", e.getMessage(), request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher(dispatchUrl).forward(request, response);
			}
			finally {
				retData = rJson.toString();
			}
			response.getWriter().write(retData);
		}
		else if(task.equalsIgnoreCase("Persist_Projects")) {
			String projects = request.getParameter("projects");
			String subSetName=request.getParameter("subSetName");
			String instanceName=request.getParameter("instanceName");
			String userId=user.getId();
			DTTSubset persistSubset=new DTTSubset();
			persistSubset.setProjects(projects);
			persistSubset.setSubSetName(subSetName);
			persistSubset.setInstanceName(instanceName);
			persistSubset.setUserId(userId);
			
			try {
				/*Modified by Roshni TENJINCG-1168 starts */
				DTTSubsetHandler handler=new DTTSubsetHandler();
				persistSubset=handler.persistDttSubSet(instanceName,persistSubset);
				/*Modified by Roshni TENJINCG-1168 ends */
				SessionUtils.setScreenState("success", "subset.create.success", request);
				request.getRequestDispatcher("DTTSubsetServlet?param=view&recId="+persistSubset.getPrjId()).forward(request, response);
				
			} catch (DatabaseException e) {
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
			} catch (RequestValidationException e) {
				logger.error(e.getMessage());
				dispatchUrl = "DTTSubsetServlet?param=new";
				SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher(dispatchUrl).forward(request, response);
			}
			
		}
		else if(task.equalsIgnoreCase("view")) {
			String recId = request.getParameter("recId");
			DTTSubset subset=new DTTSubset();
			try {
				subset=new DTTSubsetHandler().getDTTSubset(Integer.parseInt(recId));
			} catch (NumberFormatException e) {
				logger.error("Error ", e);
			} catch (DatabaseException e) {
				logger.error("Error ", e);
			}
			String mappedProjects="N";
			if(subset.getProjects()!=null) {
				String[] projects=subset.getProjects().split(",");
				request.setAttribute("Projects", projects);
			}
			
			request.setAttribute("Subset", subset);
			request.setAttribute("mappedProjects", mappedProjects);
			request.getRequestDispatcher("v2/dtt_subset_view.jsp").forward(request, response);
		}
		else if(task.equalsIgnoreCase("Add_Projects")) {

			String projects = request.getParameter("projects");
			String subSetName=request.getParameter("subSetName");
			String instanceName=request.getParameter("instanceName");
			String userId=user.getId();
			String recId=request.getParameter("recId");
			DefectManagementInstance defectManagement = new DefectManagementInstance();
			DTTSubset upadateSubset=new DTTSubset();
			upadateSubset.setProjects(projects);
			upadateSubset.setSubSetName(subSetName);
			upadateSubset.setInstanceName(instanceName);
			upadateSubset.setUserId(userId);
			upadateSubset.setPrjId(Integer.parseInt(recId));
			String add="add";
			try {
				defectManagement = new DTTSubsetHandler().getDefectInstance(instanceName);
				upadateSubset.setInstance(defectManagement.getTool());
				upadateSubset=new DTTSubsetHandler().updateProjects(upadateSubset, Integer.parseInt(recId), add);
				SessionUtils.setScreenState("success", "subset.project.add.success", request);
				request.getRequestDispatcher("DTTSubsetServlet?param=view&recId="+upadateSubset.getPrjId()).forward(request, response);
				
			} catch (DatabaseException e) {
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
			}catch (RequestValidationException e) {
				logger.error("Error ", e);
				logger.error(e.getMessage());
				dispatchUrl = "DTTSubsetServlet?param=view&recId="+recId;
				SessionUtils.setScreenState("error", e.getMessage(), e.getTargetFields(), request);
				SessionUtils.loadScreenStateToRequest(request);
				request.getRequestDispatcher(dispatchUrl).forward(request, response);
			}
			catch (Exception e) {
				SessionUtils.setScreenState("error", "Unable to Add projects due to internal Error", request);
				SessionUtils.loadScreenStateToRequest(request);
			}
		}
		else if(task.equalsIgnoreCase("Remove_Projects")) {

			String projects = request.getParameter("projects");
			String subSetName=request.getParameter("subSetName");
			String userId=user.getId();
			DTTSubset upadateSubset=new DTTSubset();
			upadateSubset.setProjects(projects);
			upadateSubset.setSubSetName(subSetName);
			upadateSubset.setUserId(userId);
			String recId=request.getParameter("recId");
			upadateSubset.setPrjId(Integer.parseInt(recId));
			String remove="remove";
			try {
				upadateSubset=new DTTSubsetHandler().updateProjects(upadateSubset, Integer.parseInt(recId),remove);
				SessionUtils.setScreenState("success", "subset.project.delete.success", request);
				request.getRequestDispatcher("DTTSubsetServlet?param=view&recId="+upadateSubset.getPrjId()).forward(request, response);
			} catch (NumberFormatException e) {
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
			} catch (DatabaseException e) {
				SessionUtils.setScreenState("error", "generic.db.error", request);
				SessionUtils.loadScreenStateToRequest(request);
			}catch (Exception e) {
				SessionUtils.setScreenState("error", "Unable to Remove projects due to internal Error", request);
				SessionUtils.loadScreenStateToRequest(request);
			}
			
		}else if(task.equalsIgnoreCase("Fetch_Projects_View")) {

			Map<String, String> map = new HashMap<String, String>();
			String instanceName = request.getParameter("instanceName");
			String recId=request.getParameter("recId");
			String project="Y";
			DTTSubset subset = new DTTSubset();
			try {
				 subset=new DTTSubsetHandler().getDTTSubset(Integer.parseInt(recId));
				map=new DTTSubsetHandler().getProjects(instanceName,subset);
				if(subset.getProjects()!=null) {
					request.setAttribute("PROJECT_MAP", map);
					request.setAttribute("Subset", subset);
					request.setAttribute("Status", project);
					request.getRequestDispatcher("v2/dtt_subset_view.jsp").forward(request, response);
				}
				
			}
			catch (Exception e) {
				SessionUtils.setScreenState("error", "subset.project.error", request);
				SessionUtils.loadScreenStateToRequest(request);
			}
		
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String delFlag = Utilities.trim(request.getParameter("del"));
		TenjinSession tjnSession = (TenjinSession) request.getSession().getAttribute("TJN_SESSION");
		User user = tjnSession.getUser();
		SessionUtils.loadScreenStateToRequest(request);
		Authorizer authorizer=new Authorizer();
		String task = Utilities.trim(request.getParameter("param"));
		if(delFlag.equalsIgnoreCase("true")) {
			if(!authorizer.hasAccess(request)){
				response.sendRedirect("noAccess.jsp");
				return;
			}
		}
		
		if(delFlag.equalsIgnoreCase("true")){
			doDelete(request, response);
		}
		if(task.equalsIgnoreCase("Persist_Projects")) {
			String projects = request.getParameter("projects");
			String subSetName=request.getParameter("subSetName");
			String instanceName=request.getParameter("instanceName");
			String userId=user.getId();
			DTTSubset persistSubset=new DTTSubset();
			persistSubset.setProjects(projects);
			persistSubset.setSubSetName(subSetName);
			persistSubset.setInstanceName(instanceName);
			persistSubset.setUserId(userId);
			String retData = "";
			JSONObject r1=null;
			try {
				/*Modified by Roshni TENJINCG-1168 starts */
				//Max 1 call per sec
				RateLimiter limiter = RateLimiter.create(1.0);
				// boolean tryAcquire = limiter.tryAcquire();
				if(limiter.tryAcquire(60,TimeUnit.SECONDS)){
					DTTSubsetHandler handler=new DTTSubsetHandler();
					persistSubset=handler.persistDttSubSet(instanceName,persistSubset);

				/*persistSubset.setInstance(defectManagement.getTool());
				persistSubset=new DTTSubsetHandler().persistDefectManagementProjects(persistSubset);*/
				/*Modified by Roshni TENJINCG-1168 ends */
				//SessionUtils.setScreenState("success", "subset.create.success", request);
				//request.getRequestDispatcher("DTTSubsetServlet?param=view&recId="+persistSubset.getPrjId()).forward(request, response);
				 r1 = new JSONObject();
				r1.put("status","success");
				r1.put("subsetId",persistSubset.getPrjId());
				r1.put("message","Subset created successfully");
				
				
				}
				else{
					 r1 = new JSONObject();
					r1.put("status","error");
					r1.put("subsetId",persistSubset.getPrjId());
					r1.put("message","TOO MANY REQUEST");	
				//SessionUtils.setScreenState("error", "TOO MANY REQUEST", request);
				//SessionUtils.loadScreenStateToRequest(request);
				}
				//DTTSubsetHandler handler=new DTTSubsetHandler();
				//persistSubset=handler.persistDttSubSet(instanceName,persistSubset);
				
				/*Modified by Roshni TENJINCG-1168 ends */
				
				
				retData = r1.toString();
			}catch(DatabaseException e) {
				JSONObject r = new JSONObject();
				try {
					r.put("status", "ERROR");
					r.put("message","An internal error occurred. Please contact Tenjin Support.");
				retData = r.toString();
			
				} catch (JSONException e1) {
					retData = "An internal error occurred. Please contact your System Administrator ";
					
				}
				
			}catch(RequestValidationException e){
				
				logger.error("Error ", e);
				JSONObject r = new JSONObject();
						try {
							r.put("status", "ERROR");
							r.put("message","An internal error occurred. Please contact Tenjin Support.");
						retData = r.toString();
					
						} catch (JSONException e1) {
							retData = "An internal error occurred. Please contact your System Administrator ";
							
						}
				
			} catch (JSONException e) {
				logger.error("Error ", e);
				JSONObject r = new JSONObject();
						try {
							r.put("status", "ERROR");
							r.put("message","An internal error occurred. Please contact Tenjin Support.");
						retData = r.toString();
					
						} catch (JSONException e1) {
							retData = "An internal error occurred. Please contact your System Administrator ";
							
						}
			}	finally {
				response.getWriter().write(retData);
			}
		
		}else if(task.equalsIgnoreCase("Add_Projects")) {

			String projects = request.getParameter("projects");
			String subSetName=request.getParameter("subSetName");
			String instanceName=request.getParameter("instanceName");
			String userId=user.getId();
			String recId=request.getParameter("recId");
			DefectManagementInstance defectManagement = new DefectManagementInstance();
			DTTSubset upadateSubset=new DTTSubset();
			upadateSubset.setProjects(projects);
			upadateSubset.setSubSetName(subSetName);
			upadateSubset.setInstanceName(instanceName);
			upadateSubset.setUserId(userId);
			upadateSubset.setPrjId(Integer.parseInt(recId));
			String add="add";
			String retData = "";
			try {
				defectManagement = new DTTSubsetHandler().getDefectInstance(instanceName);
				upadateSubset.setInstance(defectManagement.getTool());
				upadateSubset=new DTTSubsetHandler().updateProjects(upadateSubset, Integer.parseInt(recId), add);
				
				
				JSONObject r1 = new JSONObject();
				r1.put("status","success");
				r1.put("subsetId",upadateSubset.getPrjId());
				r1.put("message","Subset updated successfully.");
				retData = r1.toString();
			}catch(DatabaseException e) {
				JSONObject r = new JSONObject();
				try {
					r.put("status", "ERROR");
					r.put("message","An internal error occurred. Please contact Tenjin Support.");
				retData = r.toString();
			
				} catch (JSONException e1) {
					retData = "An internal error occurred. Please contact your System Administrator ";
					
				}
				
			}catch(RequestValidationException e){
				
				logger.error("Error ", e);
				JSONObject r = new JSONObject();
						try {
							r.put("status", "ERROR");
							r.put("message","An internal error occurred. Please contact Tenjin Support.");
						retData = r.toString();
					
						} catch (JSONException e1) {
							retData = "An internal error occurred. Please contact your System Administrator ";
							
						}
				
			} catch (JSONException e) {
				logger.error("Error ", e);
				JSONObject r = new JSONObject();
						try {
							r.put("status", "ERROR");
							r.put("message","An internal error occurred. Please contact Tenjin Support.");
						retData = r.toString();
					
						} catch (JSONException e1) {
							retData = "An internal error occurred. Please contact your System Administrator ";
							
						}
			}	finally {
				response.getWriter().write(retData);
			}
				
				
				
		}
			
	}
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String[] selectedRecIds = request.getParameterValues("selectedSubset");
		String[] subsetName=request.getParameterValues("selectedsubsetName");
		DTTSubsetHandler handler=new DTTSubsetHandler();
		try {
			boolean mappedSubset=handler.checkSubsetMapped(subsetName);
			if(mappedSubset==false) {
				handler.deleteSubset(selectedRecIds);
				if(selectedRecIds.length>1){
					SessionUtils.setScreenState("success", "subset.delete.multiple.success", request);
				}else{
					SessionUtils.setScreenState("success", "subset.delete.success",request);
				}
			}
			else {
				SessionUtils.setScreenState("error", "subset.delete.failure", request);
			}
			
			response.sendRedirect("DTTSubsetServlet?param=FETCH_ALL_SUBSET");
		} catch (DatabaseException e) {
			SessionUtils.setScreenState("error", "generic.db.error", request);
			SessionUtils.loadScreenStateToRequest(request);
			logger.error("Error ", e);
			response.sendRedirect("DTTSubsetServlet?param=FETCH_ALL_SUBSET");
		}catch (Exception e) {
			SessionUtils.setScreenState("error", "Unable to Persist projects due to internal Error", request);
			SessionUtils.loadScreenStateToRequest(request);
		}
		
	}

}
