/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TenjinReport.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/*******************************************************************************
 * CHANGE HISTORY ==============
 * 
 * DATE              CHANGED BY                 DESCRIPTION
 * 21-12-2016		SRIRAM					Defect#TEN-24
* 21-Dec-2016			Sriram					Defect TEN-22
 */

package com.ycs.tenjin;

import java.util.ArrayList;
import java.util.List;

public class TenjinReport {
	private String application;
	private String moduleCode;
	private String userId;
	private int noOfTestCases;
	private int executed;
	private int noOfPassedTestCases;
	private int noOfFailedTestCases;
	private int noOfErrorTestCases;
	private int noOfFailedDueToVerification;
	private String percentage;
	private String fromDate;
	private String toDate;
	
	public int getExecuted() {
		return executed;
	}
	public void setExecuted(int executed) {
		this.executed = executed;
	}
	private List<String> defects = new ArrayList<String>();
	
	public String getApplication() {
		return application;
	}
	public void setApplication(String application) {
		this.application = application;
	}
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getNoOfTestCases() {
		return noOfTestCases;
	}
	public void setNoOfTestCases(int noOfTestCases) {
		this.noOfTestCases = noOfTestCases;
	}
	public int getNoOfPassedTestCases() {
		return noOfPassedTestCases;
	}
	public void setNoOfPassedTestCases(int noOfPassedTestCases) {
		this.noOfPassedTestCases = noOfPassedTestCases;
	}
	public int getNoOfFailedTestCases() {
		return noOfFailedTestCases;
	}
	public void setNoOfFailedTestCases(int noOfFailedTestCases) {
		this.noOfFailedTestCases = noOfFailedTestCases;
	}
	public int getNoOfFailedDueToVerification() {
		return noOfFailedDueToVerification;
	}
	public void setNoOfFailedDueToVerification(int noOfFailedDueToVerification) {
		this.noOfFailedDueToVerification = noOfFailedDueToVerification;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public List<String> getDefects() {
		return defects;
	}
	public void setDefects(List<String> defects) {
		this.defects = defects;
	}
	public int getNoOfErrorTestCases() {
		return noOfErrorTestCases;
	}
	public void setNoOfErrorTestCases(int noOfErrorTestCases) {
		this.noOfErrorTestCases = noOfErrorTestCases;
	}
	
	
	//Added by Sriram to fix defect#TEN-24
	private int totalValidations;
	private int passedValidations;
	private int failedValidations;
	private int errorValidations;
	private String validationPercentage;
	
	public int getTotalValidations() {
		return totalValidations;
	}
	public void setTotalValidations(int totalValidations) {
		this.totalValidations = totalValidations;
	}
	public int getPassedValidations() {
		return passedValidations;
	}
	public void setPassedValidations(int passedValidations) {
		this.passedValidations = passedValidations;
	}
	public int getFailedValidations() {
		return failedValidations;
	}
	public void setFailedValidations(int failedValidations) {
		this.failedValidations = failedValidations;
	}
	public int getErrorValidations() {
		return errorValidations;
	}
	public void setErrorValidations(int errorValidations) {
		this.errorValidations = errorValidations;
	}
	public String getValidationPercentage() {
		return validationPercentage;
	}
	public void setValidationPercentage(String validationPercentage) {
		this.validationPercentage = validationPercentage;
	}
	
	//Added by Sriram to fix defect#TEN-24 ends
	
	//Added by Sriram to fix defect#TEN-22
	private int defectCount;
	private int appId;
	public int getDefectCount() {
		return defectCount;
	}
	public void setDefectCount(int defectCount) {
		this.defectCount = defectCount;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	
	//Added by Sriram to fix defect#TEN-22 ends
	
}
