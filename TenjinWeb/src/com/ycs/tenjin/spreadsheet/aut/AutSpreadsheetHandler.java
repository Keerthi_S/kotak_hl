/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AutSpreadsheetHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 02-Aug-2017              Sriram                Newly added for
 */

package com.ycs.tenjin.spreadsheet.aut;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.pojo.TaskManifest;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.LearningHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.db.UserHelper;
import com.ycs.tenjin.run.TestRun;
import com.ycs.tenjin.spreadsheet.GenericSpreadsheetHandler;
import com.ycs.tenjin.spreadsheet.exception.GenericSpreadsheetException;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.FileCompressor;
import com.ycs.tenjin.util.Utilities;

public class AutSpreadsheetHandler {
	private static final Logger logger = LoggerFactory.getLogger(AutSpreadsheetHandler.class);
	private static String SS_WORK_PATH = "";
	private Connection conn = null;

	private static CellReference CONTROL_APP_NAME_CELL = new CellReference("C5");
	private static CellReference CONTROL_FUNC_CODE_CELL = new CellReference("C6");
	
	private static CellReference CONTROL_EXPORTED_BY_CELL = new CellReference("F5");
	private static CellReference CONTROL_EXPORTED_ON_CELL = new CellReference("F6");
	
	private static SimpleDateFormat CONTROL_DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
	
	
	private User user;
	
	public AutSpreadsheetHandler() {
		try {
			SS_WORK_PATH = TenjinConfiguration.getProperty("TJN_WORK_PATH");
		} catch (TenjinConfigurationException e) {
			
			logger.error("ERROR - Could not get TJN_WORK_PATH from configuration", e);
			SS_WORK_PATH = System.getProperty("user.home");
		}

		SS_WORK_PATH = SS_WORK_PATH + File.separator + "temp" + File.separator + "spreadsheets";

		File ssWorkDir = new File(SS_WORK_PATH);
		if(!ssWorkDir.exists()) {
			ssWorkDir.mkdirs();
		}

	}
	
	public AutSpreadsheetHandler(String userId) {
		try {
			SS_WORK_PATH = TenjinConfiguration.getProperty("TJN_WORK_PATH");
		} catch (TenjinConfigurationException e) {
			
			logger.error("ERROR - Could not get TJN_WORK_PATH from configuration", e);
			SS_WORK_PATH = System.getProperty("user.home");
		}
		
		SS_WORK_PATH = SS_WORK_PATH + File.separator + "temp" + File.separator + "spreadsheets";

		File ssWorkDir = new File(SS_WORK_PATH);
		if(!ssWorkDir.exists()) {
			ssWorkDir.mkdirs();
		}
		
		try {
			this.user = new UserHelper().hydrateUser(userId);
		} catch (DatabaseException e) {
			logger.error("ERROR fetching details of user: {}", userId, e);
		}

	}
	
	public AutSpreadsheetHandler(User user) {
		try {
			SS_WORK_PATH = TenjinConfiguration.getProperty("TJN_WORK_PATH");
		} catch (TenjinConfigurationException e) {
			
			logger.error("ERROR - Could not get TJN_WORK_PATH from configuration", e);
			SS_WORK_PATH = System.getProperty("user.home");
		}

		SS_WORK_PATH = SS_WORK_PATH + File.separator + "temp" + File.separator + "spreadsheets";

		File ssWorkDir = new File(SS_WORK_PATH);
		if(!ssWorkDir.exists()) {
			ssWorkDir.mkdirs();
		}
		
		this.user = user;

	}

	private void getDBConnection() throws DatabaseException {
		this.conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
	}

	private void closeDBConnection() {
		try {
			this.conn.close();
			logger.info("DB Connection closed");
		}catch(Exception ignore) {}
	}

	public File downloadAutInformation(boolean includeMetadata) {
		String tempFolderName = SS_WORK_PATH + File.separator + Utilities.getRawTimeStamp();
		File tempDir = new File(tempFolderName);
		if(!tempDir.exists()) {
			tempDir.mkdirs();
		}
		boolean allOk = false;

		logger.info("Fetching all Applications");
		try {

			this.getDBConnection();

			logger.info("Fetching all applications");
			ArrayList<Aut> auts = new AutHelper().hydrateAllAut(this.conn);
			logger.info("{} applications fetched", auts.size());
			logger.info("Fetching all functions");
			List<Module> functions = new ModulesHelper().hydrateAllModules(this.conn);
			logger.info("{} functions fetched", functions.size());
			GenericSpreadsheetHandler<Aut> autHandler = new GenericSpreadsheetHandler<Aut>();
			GenericSpreadsheetHandler<Module> moduleHandler = new GenericSpreadsheetHandler<Module>();
			Workbook workbook = autHandler.putObjectsToSpreadsheet(auts, "Applications");
			moduleHandler.putObjectsToSpreadsheet(workbook, functions, "Functions");

			moduleHandler.writeWorkbookToFile(workbook, tempFolderName + File.separator + "applications-exported.xlsx");
			GenericSpreadsheetHandler<Location> locHandler = new GenericSpreadsheetHandler<Location>();
			GenericSpreadsheetHandler<TestObject> testObjectHandler = new GenericSpreadsheetHandler<TestObject>();
			if(includeMetadata) {
				Map<Integer, List<Module>> autFunctionsMap = new HashMap<Integer, List<Module>>();
				logger.info("Hashing functions");
				for(Module module : functions) {
					List<Module> modulesForApp = autFunctionsMap.get(module.getAut());
					if(modulesForApp == null) {
						modulesForApp = new ArrayList<Module>();
					}

					modulesForApp.add(module);
					autFunctionsMap.put(module.getAut(), modulesForApp);
				}
				LearningHelper lHelper = new LearningHelper();
				

				for(Aut aut : auts) {
					List<Module> modules = autFunctionsMap.get(aut.getId());
					File autDir = new File(tempFolderName + File.separator + aut.getName());
					String autDirName = tempFolderName + File.separator + aut.getName();
					if(!autDir.exists()) {
						autDir.mkdirs();
					}
					if(modules != null) {
						for(Module module : modules) {
							logger.info("Hydrating metadata for application {}, function {}", aut.getId(), module.getCode());
							List<Location> locations = lHelper.hydrateMetadata(this.conn, aut.getId(), module.getCode(), false, false);
							logger.info("Entering {} locations to worksheet", locations.size());
							Workbook mWorkbook = locHandler.putObjectsToSpreadsheet(locations, "Page Areas");
							List<TestObject> testObjects = new ArrayList<TestObject>();
							for(Location location : locations) {
								testObjects.addAll(location.getTestObjects());
							}

							logger.info("Entering {} test objects to worksheet", testObjects.size());
							testObjectHandler.putObjectsToSpreadsheet(mWorkbook, testObjects, "Fields");
							locHandler.writeWorkbookToFile(mWorkbook, autDirName + File.separator +  module.getCode() + "_Metadata-exported.xlsx");
						}
					}
				}
			}
			allOk = true;

		} catch (DatabaseException e) {
			logger.error("ERROR generating download",e);
		} catch (GenericSpreadsheetException e) {
			logger.error("ERROR generating download",e);
		}
		
		if(allOk) {
			File fileToReturn = null;
			if(includeMetadata) {
				fileToReturn = this.compress(tempFolderName, tempFolderName + File.separator + "applications-with-metadata-exported.zip");
			}else {
				fileToReturn = new File(tempFolderName + File.separator + "applications-exported.xlsx");
			}
			return fileToReturn;
		}else {
			logger.error("Download to spreadsheet completed with ERROR(s)");
			return null;
		}
	}
	
	public File downloadAutInformation(int[] appIds, boolean includeMetadata) {
		String tempFolderName = SS_WORK_PATH + File.separator + Utilities.getRawTimeStamp();
		File tempDir = new File(tempFolderName);
		if(!tempDir.exists()) {
			tempDir.mkdirs();
		}
		boolean allOk = false;

		logger.info("Fetching all Applications");
		try {

			this.getDBConnection();

			logger.info("Fetching all applications");
			ArrayList<Aut> auts = new AutHelper().hydrateAllAut(this.conn, appIds);
			logger.info("{} applications fetched", auts.size());
			logger.info("Fetching all functions");
			List<Module> functions = new ModulesHelper().hydrateAllModules(this.conn);
			logger.info("{} functions fetched", functions.size());
			GenericSpreadsheetHandler<Aut> autHandler = new GenericSpreadsheetHandler<Aut>();
			GenericSpreadsheetHandler<Module> moduleHandler = new GenericSpreadsheetHandler<Module>();
			Workbook workbook = autHandler.putObjectsToSpreadsheet(auts, "Applications");
			moduleHandler.putObjectsToSpreadsheet(workbook, functions, "Functions");

			moduleHandler.writeWorkbookToFile(workbook, tempFolderName + File.separator + "applications-exported.xlsx");
			GenericSpreadsheetHandler<Location> locHandler = new GenericSpreadsheetHandler<Location>();
			GenericSpreadsheetHandler<TestObject> testObjectHandler = new GenericSpreadsheetHandler<TestObject>();
			if(includeMetadata) {
				Map<Integer, List<Module>> autFunctionsMap = new HashMap<Integer, List<Module>>();
				logger.info("Hashing functions");
				for(Module module : functions) {
					List<Module> modulesForApp = autFunctionsMap.get(module.getAut());
					if(modulesForApp == null) {
						modulesForApp = new ArrayList<Module>();
					}

					modulesForApp.add(module);
					autFunctionsMap.put(module.getAut(), modulesForApp);
				}
				LearningHelper lHelper = new LearningHelper();

				for(Aut aut : auts) {
					List<Module> modules = autFunctionsMap.get(aut.getId());
					File autDir = new File(tempFolderName + File.separator + aut.getName());
					String autDirName = tempFolderName + File.separator + aut.getName();
					if(!autDir.exists()) {
						autDir.mkdirs();
					}
					if(modules != null) {
						for(Module module : modules) {
							logger.info("Hydrating metadata for application {}, function {}", aut.getId(), module.getCode());
							List<Location> locations = lHelper.hydrateMetadata(this.conn, aut.getId(), module.getCode(), false, false);
							logger.info("Entering {} locations to worksheet", locations.size());
							Workbook mWorkbook = locHandler.putObjectsToSpreadsheet(locations, "Page Areas");
							List<TestObject> testObjects = new ArrayList<TestObject>();
							for(Location location : locations) {
								testObjects.addAll(location.getTestObjects());
							}

							logger.info("Entering {} test objects to worksheet", testObjects.size());
							testObjectHandler.putObjectsToSpreadsheet(mWorkbook, testObjects, "Fields");
							locHandler.writeWorkbookToFile(mWorkbook, autDirName + File.separator +  module.getCode() + "_Metadata-exported.xlsx");
						}
					}
				}
			}
			allOk = true;

		} catch (DatabaseException e) {
			logger.error("ERROR generating download",e);
		} catch (GenericSpreadsheetException e) {
			logger.error("ERROR generating download",e);
		}
		
		if(allOk) {
			File fileToReturn = null;
			if(includeMetadata) {
				fileToReturn = this.compress(tempFolderName, tempFolderName + File.separator + "applications-with-metadata-exported.zip");
			}else {
				fileToReturn = new File(tempFolderName + File.separator + "applications-exported.xlsx");
			}
			return fileToReturn;
		}else {
			logger.error("Download to spreadsheet completed with ERROR(s)");
			return null;
		}
	}

	/**
	 * Generates a set of Spreadsheets representing information of all functions available under a specific application ID. <br />
	 * <ul>
	 * <li>Generates one workbook which contains information about all functions in the specified application</li>
	 * <li>Generates a separate spreadsheet for metadata of each function. (Only if includeMetadata is set to true)</li>
	 * <li>Compresses all the generated files to a Zip file to be returned. (Only if includeMetadata is set to true)</li>
	 * </ul>
	 * @param appId The application ID
	 * @param includeMetadata a boolean value indicating if metadata has to be exported for each function.
	 * @return A zip file containing all the generated spreadsheets if includeMetadata is set to true. Returns a single spreadsheet if includeMetadata is set to false.
	 */
	public File downloadFunctionInformation(int appId, boolean includeMetadata) {
		String tempFolderName = SS_WORK_PATH + File.separator + Utilities.getRawTimeStamp();
		File tempDir = new File(tempFolderName);
		if(!tempDir.exists()) {
			tempDir.mkdirs();
		}
		boolean allOk = false;
		logger.info("Fetching all functions for Application {}", appId);
		try {
			List<Module> modules = new ModulesHelper().hydrateModulesForApplication(appId);

			logger.info("Writing {} functions to sheet", modules.size());
			GenericSpreadsheetHandler<Module> handler = new GenericSpreadsheetHandler<Module>();
			Workbook workbook = handler.putObjectsToSpreadsheet(modules, "Functions");

			handler.writeWorkbookToFile(workbook, tempFolderName + File.separator + "Functions-exported.xlsx"); 

			if(includeMetadata) {
				GenericSpreadsheetHandler<Location> locHandler = new GenericSpreadsheetHandler<Location>();
				GenericSpreadsheetHandler<TestObject> testObjectHandler = new GenericSpreadsheetHandler<TestObject>();
				LearningHelper learningHelper = new LearningHelper();
				for(Module module : modules) {
					logger.debug("Hydrating metadata info for {}", module.getCode());
					List<Location> locations = learningHelper.hydrateMetadata(this.conn, appId, module.getCode(), false, false);
					//learningHelper.hydrateMetadata(appId, module.getCode(), false, false);
					logger.debug("Entering {} locations to worksheet", locations.size());
					Workbook mWorkbook = locHandler.putObjectsToSpreadsheet(locations, "Page Areas");
					List<TestObject> testObjects = new ArrayList<TestObject>();
					for(Location location : locations) {
						testObjects.addAll(location.getTestObjects());
					}

					logger.debug("Entering {} test objects to worksheet", testObjects.size());
					testObjectHandler.putObjectsToSpreadsheet(mWorkbook, testObjects, "Fields");
					locHandler.writeWorkbookToFile(mWorkbook, tempFolderName + File.separator + module.getCode() + "_Metadata-exported.xlsx");
				}
			}

			allOk = true;

		} catch (DatabaseException e) {
			logger.error("ERROR generating download",e);
		} catch (GenericSpreadsheetException e) {
			logger.error("ERROR generating download",e);
		}


		if(allOk) {
			File fileToReturn = null;
			if(includeMetadata) {
				fileToReturn = this.compress(tempFolderName, tempFolderName + File.separator + "Functions-with-metadata-exported.zip");
			}else {
				fileToReturn = new File(tempFolderName + File.separator + "Functions-exported.xlsx");
			}
			return fileToReturn;
		}else {
			logger.error("Download to spreadsheet completed with ERROR(s)");
			return null;
		}

	}

	/**
	 * Generates a set of Spreadsheets representing information of the specific function codes available under a specific application ID. <br />
	 * <ul>
	 * <li>Generates one workbook which contains information about all functions specified</li>
	 * <li>Generates a separate spreadsheet for metadata of each function. (Only if includeMetadata is set to true)</li>
	 * <li>Compresses all the generated files to a Zip file to be returned. (Only if includeMetadata is set to true)</li>
	 * </ul>
	 * @param appId The application ID
	 * @param functionCodes A String array containing the set of function codes that have to be exported.
	 * @param includeMetadata a boolean value indicating if metadata has to be exported for each function.
	 * @return A zip file containing all the generated spreadsheets if includeMetadata is set to true. Returns a single spreadsheet if includeMetadata is set to false.
	 */
	public File downloadFunctionInformation(int appId, String[] functionCodes, boolean includeMetadata) {

		try {
			this.getDBConnection();
		} catch (DatabaseException e1) {
			
			logger.error(e1.getMessage());
			return null;
		}

		String tempFolderName = SS_WORK_PATH + File.separator + Utilities.getRawTimeStamp();
		File tempDir = new File(tempFolderName);
		if(!tempDir.exists()) {
			tempDir.mkdirs();
		}
		boolean allOk = false;
		logger.info("Fetching all functions for Application {}", appId);
		try {
			List<Module> modules = new ModulesHelper().hydrateModulesForApplication(this.conn, appId, functionCodes);

			logger.info("Writing {} functions to sheet", modules.size());
			GenericSpreadsheetHandler<Module> handler = new GenericSpreadsheetHandler<Module>();
			Workbook workbook = handler.putObjectsToSpreadsheet(modules, "Functions");

			handler.writeWorkbookToFile(workbook, tempFolderName + File.separator + "Functions-exported.xlsx"); 

			if(includeMetadata) {
				GenericSpreadsheetHandler<Location> locHandler = new GenericSpreadsheetHandler<Location>();
				GenericSpreadsheetHandler<TestObject> testObjectHandler = new GenericSpreadsheetHandler<TestObject>();
				LearningHelper learningHelper = new LearningHelper();
				for(Module module : modules) {
					logger.info("Hydrating metadata info for {}", module.getCode());
					List<Location> locations = learningHelper.hydrateMetadata(this.conn, appId, module.getCode(), false, false);
					logger.info("Entering {} locations to worksheet", locations.size());
					Workbook mWorkbook = locHandler.putObjectsToSpreadsheet(locations, "Page Areas");
					List<TestObject> testObjects = new ArrayList<TestObject>();
					for(Location location : locations) {
						testObjects.addAll(location.getTestObjects());
					}

					logger.info("Entering {} test objects to worksheet", testObjects.size());
					testObjectHandler.putObjectsToSpreadsheet(mWorkbook, testObjects, "Fields");
					locHandler.writeWorkbookToFile(mWorkbook, tempFolderName + File.separator + module.getCode() + "_Metadata-exported.xlsx");
				}
			}

			allOk = true;

		} catch (DatabaseException e) {
			logger.error("ERROR generating download",e);
		} catch (GenericSpreadsheetException e) {
			logger.error("ERROR generating download",e);
		}

		this.closeDBConnection();


		if(allOk) {
			File fileToReturn = null;
			if(includeMetadata) {
				//Zip the file
				fileToReturn = this.compress(tempFolderName, tempFolderName + File.separator + "Functions-with-metadata-exported.zip");
			}else {
				fileToReturn = new File(tempFolderName + File.separator + "Functions-exported.xlsx");
			}

			//this.cleanUpTemp(tempFolderName);
			return fileToReturn;
		}else {
			logger.error("Download to spreadsheet completed with ERROR(s)");
			//this.cleanUpTemp(tempFolderName);
			return null;
		}

	}

	public static void main(String[] args) throws IOException {
		AutSpreadsheetHandler s = new AutSpreadsheetHandler("prem");
		
		try {
			s.importMetadata("C:\\Users\\prem.kg\\Downloads\\STDCIF_Metadata-exported.xlsx", 31974, "STDCIF");
		} catch (GenericSpreadsheetException e) {
			
			logger.error("Error ", e);
		}

		System.out.println("Done");
	}

	private File compress(String sourceFolderPath, String destinationZipFilePath) {
		File fileToReturn = null;
		FileCompressor compressor = new FileCompressor(sourceFolderPath, destinationZipFilePath);
		try {
			compressor.compress();
			fileToReturn = new File(destinationZipFilePath);
			return fileToReturn;
		} catch (Exception e) {
			
			logger.error("ERROR occurred while trying to compress files from path {}", sourceFolderPath, e);
			return null;
		}
	}

	public void importMetadata(String workbookPath, int appId, String functionCode) throws GenericSpreadsheetException {
		Date importStartTime = new Date();
		logger.info("Getting control properties...");
		Map<String, String> controlProperties = GenericSpreadsheetHandler.getControlProperties(workbookPath);
	
		try {
			logger.info("Validating application");
			Aut aut = new AutHelper().hydrateAut(appId);
			if(aut == null) {
				logger.error("Invalid application {}", appId);
				throw new GenericSpreadsheetException("Application specified in the Control sheet is invalid. Please review your uploaded file and try again.");
			} else if(!Utilities.trim(aut.getName()).equalsIgnoreCase(Utilities.trim(controlProperties.get("Application")))) {
				logger.error("ERROR - Application specified in control sheet [{}] does not match with the application ID passed [{}]", Utilities.trim(controlProperties.get("Application")), appId);
				throw new GenericSpreadsheetException("Application specified in the Control sheet does not match with the current application. Please review your uploaded file and try again.");
			}
			
			logger.info("Validating function code");
			Module module = new ModulesHelper().hydrateModuleInfo(appId, functionCode);
			if(module == null) {
				logger.error("ERROR - Function code [{}] does not exist", Utilities.trim(controlProperties.get("Function")));
				throw new GenericSpreadsheetException("Function code specified in the control sheet is invalid. Please review the uploaded file and try again.");
			} else if(!Utilities.trim(module.getCode()).equalsIgnoreCase(Utilities.trim(controlProperties.get("Function")))) {
				logger.error("ERROR - Function code speicifed in control sheet [{}] does not match with function code passed [{}]", Utilities.trim(controlProperties.get("Function")), functionCode);
				throw new GenericSpreadsheetException("Function code specified in the control sheet does not match with the current function code. Please review the uploaded file and try again.");
			}
			
			logger.info("All validations passed.");
			logger.info("Loading locations from the uploaded workbook");
			
			GenericSpreadsheetHandler<Location> locHandler = new GenericSpreadsheetHandler<Location>();
			List<Location> locations = locHandler.getObjectsFromSpreadsheet(workbookPath, "Page Areas", Location.class);
			if(locations != null && locations.size() > 0) {
				logger.info("{} locations were found.", locations.size());
			}else {
				logger.warn("No locations found in the sheet. Upload is aborting");
				//throw new GenericSpreadsheetException("Could not find any locations in the uploaded sheet. Please verify the uploaded file and try again.");
			}
			
			logger.info("Loading test objects from the uploaded workbook");
			GenericSpreadsheetHandler<TestObject> testObjectHandler= new GenericSpreadsheetHandler<TestObject>();
			List<TestObject> testObjects = testObjectHandler.getObjectsFromSpreadsheet(workbookPath, "Fields", TestObject.class);
			if(testObjects != null && testObjects.size() > 0) {
				logger.info("{} test objects were found", testObjects.size());
			}else {
				logger.warn("No test objects found in the sheet.");
			}
			
			
			if((locations != null && locations.size() > 0) || (testObjects != null && testObjects.size() > 0)) {
				logger.info("Persisting metadata...");
				new LearningHelper().persistMetadataWithoutProcessing(appId, functionCode, locations, testObjects);
				logger.info("Metadata persisted successfully.");
			}else {
				logger.warn("No locations or fields were found to persist.");
			}
			
			logger.info("Updating Audit");
			this.updateMetadataImportAudit(aut, module, importStartTime, "IMPORTED", "Metadata imported successfully");
			
		} catch (DatabaseException e) {
			
			//this.updateMetadataImportAudit(aut, module, importStartTime, "ERROR", "Internal error while importing metadata.");
			throw new GenericSpreadsheetException("Could not perform upload due to an internal error.. Please contact Tenjin Support.");
		}
		
	}
	
	private void updateMetadataImportAudit(Aut aut, Module module, Date startTime, String status, String message) {
		TestRun run = new TestRun();
		run.setAppId(aut.getId());
		run.setBrowser_type("APPDEFAULT");
		run.setMachine_ip("N/A");
		run.setTargetPort(4444);
		run.setTaskType(BridgeProcess.LEARN);
		run.setUser(user.getId());
		run.setStartTimeStamp(new Timestamp(startTime.getTime()));
		TaskManifest manifest =new TaskManifest();
		List<Module> modules = new ArrayList<Module>();
		modules.add(module);
		manifest.setFunctions(modules);
		manifest.setAut(aut);
		manifest.setTaskType(BridgeProcess.LEARN);
		try {
			new RunHelper().persistNewRun(run, manifest);
			new LearningHelper().beginLearningRun(run.getId(), false);
			new LearningHelper().updateImportAuditTrail(run.getId(), module.getCode(), status, message);
		} catch (DatabaseException e) {
			
			logger.error("ERROR updating import audit trail", e);
		}
		
		
	}

	public File export(String autSelection, String functionSelection, boolean includeMetadata) {
		boolean singleAut = true;
		boolean allFunctions = true;
		int appId = 0;
		if(Utilities.trim(autSelection).contains(",")) {
			singleAut = false;
		}else {
			try {
				appId = Integer.parseInt(autSelection);
			} catch (NumberFormatException e) {
				logger.error("ERROR - Invalid Application ID {}", autSelection, e);
				return null;
			}
		}

		if(Utilities.trim(functionSelection).length() > 0) {
			allFunctions = false;
		}

		if(singleAut && allFunctions) {
			return this.downloadFunctionInformation(appId, includeMetadata);
		}else if(singleAut && !allFunctions) {
			return this.downloadFunctionInformation(appId, Utilities.trim(functionSelection).split(","), includeMetadata);
		}else if(!singleAut) {
			if(Utilities.trim(autSelection).length() > 0) {
				String[] autSplit = Utilities.trim(autSelection).split(",");
				int[] appIds = new int[autSplit.length];
				for(int i=0; i<autSplit.length; i++) {
					appIds[i] = Integer.parseInt(autSplit[i]);
				}
				
				return this.downloadAutInformation(appIds, includeMetadata);
			}else {
				return this.downloadAutInformation(includeMetadata);
			}
		}else {
			logger.error("Not implemented yet!");
			return null;
		}
	}
	
	
	public File downloadMetadata(int appId, String[] functionCodes) {

		try {
			this.getDBConnection();
		} catch (DatabaseException e1) {
			
			logger.error(e1.getMessage());
			return null;
		}

		String tempFolderName = SS_WORK_PATH + File.separator + Utilities.getRawTimeStamp();
		File tempDir = new File(tempFolderName);
		if(!tempDir.exists()) {
			tempDir.mkdirs();
		}
		
		
		
		boolean allOk = false;
		logger.info("Fetching all functions for Application {}", appId);
		try {
			
			Aut aut = new AutHelper().hydrateAut(appId);
			
			GenericSpreadsheetHandler<Location> locHandler = new GenericSpreadsheetHandler<Location>();
			GenericSpreadsheetHandler<TestObject> testObjectHandler = new GenericSpreadsheetHandler<TestObject>();
			LearningHelper learningHelper = new LearningHelper();
			
			for(String moduleCode : functionCodes) {
				//Get the metadata template
				InputStream in = this.getClass().getClassLoader().getResourceAsStream("metadata-template.xlsx");
				if(in == null) {
					logger.error("ERROR - Control template not found for metadata export");
					return null;
				}
				Workbook mWorkbook = GenericSpreadsheetHandler.getWorkbook(in);
				try  {
					in.close();
				}catch(Exception ignore) {logger.warn("Could not close stream for template workbook", ignore);}
				logger.info("Hydrating metadata info for {}", moduleCode);
				List<Location> locations = learningHelper.hydrateMetadata(this.conn, appId, moduleCode, false, false);
				logger.info("Entering {} locations to worksheet", locations.size());
				locHandler.putObjectsToSpreadsheet(mWorkbook, locations, "Page Areas");
				List<TestObject> testObjects = new ArrayList<TestObject>();
				for(Location location : locations) {
					testObjects.addAll(location.getTestObjects());
				}

				logger.info("Entering {} test objects to worksheet", testObjects.size());
				testObjectHandler.putObjectsToSpreadsheet(mWorkbook, testObjects, "Fields");
				
				logger.info("Writing control information");
				Sheet controlSheet = mWorkbook.getSheet("Control_Sheet");
				if(controlSheet != null) {
					Row oRow = controlSheet.getRow(CONTROL_APP_NAME_CELL.getRow()) != null ? controlSheet.getRow(CONTROL_APP_NAME_CELL.getRow()) : controlSheet.createRow(CONTROL_APP_NAME_CELL.getRow());
					Cell oCell = oRow.getCell(CONTROL_APP_NAME_CELL.getCol(), Row.CREATE_NULL_AS_BLANK);
					oCell.setCellType(Cell.CELL_TYPE_STRING);
					oCell.setCellValue(aut.getName());
					
					oRow = controlSheet.getRow(CONTROL_FUNC_CODE_CELL.getRow()) != null ? controlSheet.getRow(CONTROL_FUNC_CODE_CELL.getRow()) : controlSheet.createRow(CONTROL_FUNC_CODE_CELL.getRow());
					oCell = oRow.getCell(CONTROL_FUNC_CODE_CELL.getCol(), Row.CREATE_NULL_AS_BLANK);
					oCell.setCellType(Cell.CELL_TYPE_STRING);
					oCell.setCellValue(moduleCode);
					
					oRow = controlSheet.getRow(CONTROL_EXPORTED_BY_CELL.getRow()) != null ? controlSheet.getRow(CONTROL_EXPORTED_BY_CELL.getRow()) : controlSheet.createRow(CONTROL_EXPORTED_BY_CELL.getRow());
					oCell = oRow.getCell(CONTROL_EXPORTED_BY_CELL.getCol(), Row.CREATE_NULL_AS_BLANK);
					oCell.setCellType(Cell.CELL_TYPE_STRING);
					oCell.setCellValue(this.user.getFullName());
					
					oRow = controlSheet.getRow(CONTROL_EXPORTED_ON_CELL.getRow()) != null ? controlSheet.getRow(CONTROL_EXPORTED_ON_CELL.getRow()) : controlSheet.createRow(CONTROL_EXPORTED_ON_CELL.getRow());
					oCell = oRow.getCell(CONTROL_EXPORTED_ON_CELL.getCol(), Row.CREATE_NULL_AS_BLANK);
					oCell.setCellType(Cell.CELL_TYPE_STRING);
					oCell.setCellValue(CONTROL_DATE_FORMAT.format(new Date()));
					
				}
				
				locHandler.writeWorkbookToFile(mWorkbook, tempFolderName + File.separator + moduleCode + "_Metadata-exported.xlsx");
			}
		

			allOk = true;

		} catch (DatabaseException e) {
			logger.error("ERROR generating download",e);
		} catch (GenericSpreadsheetException e) {
			logger.error("ERROR generating download",e);
		} catch (Exception e) {
			logger.error("ERROR generating download",e);
		}

		this.closeDBConnection();


		if(allOk) {
			File fileToReturn = null;
			if(functionCodes != null && functionCodes.length > 1) {
				fileToReturn = this.compress(tempFolderName, tempFolderName + File.separator + "Functions-with-metadata-exported.zip");
			}else {
				fileToReturn = new File(tempFolderName + File.separator + functionCodes[0] + "_Metadata-exported.xlsx");
			}

			//this.cleanUpTemp(tempFolderName);
			return fileToReturn;
		}else {
			logger.error("Download to spreadsheet completed with ERROR(s)");
			//this.cleanUpTemp(tempFolderName);
			return null;
		}

	}

}
