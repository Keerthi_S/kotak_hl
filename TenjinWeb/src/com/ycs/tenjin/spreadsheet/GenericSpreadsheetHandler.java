/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GenericSpreadsheetHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 02-Aug-2017              Sriram                Newly added for
 */

package com.ycs.tenjin.spreadsheet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.spreadsheet.annotation.Downloadable;
import com.ycs.tenjin.spreadsheet.annotation.Uploadable;
import com.ycs.tenjin.spreadsheet.exception.GenericSpreadsheetException;
import com.ycs.tenjin.util.Utilities;

public class GenericSpreadsheetHandler<E> {
	
	private static final Logger logger = LoggerFactory.getLogger(GenericSpreadsheetHandler.class);
	private static final String CONTROL_SHEET_NAME = "Control_Sheet";
	private static final int CONTROL_PROP_COL = 1;
	private static final int CONTROL_PROP_START_ROW = 4;
	
	public Workbook putObjectsToSpreadsheet(List<E> objects, String sheetName) throws GenericSpreadsheetException {
		Workbook workbook = this.getNewWorkbook();
		this._transferToSpreadsheet(workbook, sheetName, objects);
		return workbook;
	}
	
	
	
	public void putObjectsToSpreadsheet(Workbook workbook, List<E> objects, String sheetName) throws GenericSpreadsheetException {
		this._transferToSpreadsheet(workbook, sheetName, objects);
	}
	

	
	public Workbook getNewWorkbook() {
		return new XSSFWorkbook();
	}
	
	public Workbook getWorkbook(String filePath) throws FileNotFoundException, IOException {
		return new XSSFWorkbook(new FileInputStream(new File(filePath)));
	}
	
	public static Workbook getWorkbook(InputStream inputStream) throws IOException {
		return new XSSFWorkbook(inputStream);
	}
	
	public void writeWorkbookToFile(Workbook workbook, String path) throws GenericSpreadsheetException {
		try {
			OutputStream os = new FileOutputStream(new File(path));
			workbook.write(os);
			os.close();
		} catch (Exception e) {
			logger.error("ERROR while writing workbook to output.", e);
			throw new GenericSpreadsheetException("Could not save workbook to file.", e);
		}
	}
	
	protected void _transferToSpreadsheet(Workbook workbook, String sheetName, List<E> objectsToDownload) throws GenericSpreadsheetException {
		long startMillis = System.currentTimeMillis();
		try {
			Sheet targetSheet =workbook.getSheet(sheetName);
			if(targetSheet != null ) {
				logger.error("Sheet {} already exists in this workbook", sheetName);
				throw new GenericSpreadsheetException("Sheet [" + sheetName +"] is already present. Please use a different sheet name");
			}
			
			targetSheet = workbook.createSheet(sheetName);
			
			if(objectsToDownload == null || objectsToDownload.size() < 1) {
				logger.error("ERROR - objectsToDownload is empty");
				return;
			}
			
			@SuppressWarnings("rawtypes")
			Class clazz = objectsToDownload.get(0).getClass();
			
			int startRow = 1;
			int currentRow = startRow;
			int startCol = 0;
			int currentCol = startCol;
			int headerRow = 0;
			
			
			Field[] downloadableFields = FieldUtils.getFieldsWithAnnotation(clazz, Downloadable.class);
			
			if(downloadableFields.length < 1) {
				throw new GenericSpreadsheetException("No downloadable fields found for class [" + clazz.getName() + "]. Please use @Downloadable to make a field downloadable.");
			}
			
			
			
			CellStyle headerRowStyle = getHeaderRowStyle(workbook);
			String columnName = "";
			Row headerRowObj = targetSheet.createRow(headerRow);
			for(Field dField : downloadableFields) {
				columnName = dField.getAnnotation(Downloadable.class).columnName();
				Cell tCell = headerRowObj.createCell(currentCol);
				tCell.setCellType(Cell.CELL_TYPE_STRING);
				tCell.setCellStyle(headerRowStyle);
				tCell.setCellValue(columnName);
				logger.debug("Added header [{}] in row {}, column {}", columnName, headerRow, currentCol);
				currentCol++;
			}
			
			String getterMethodName = "";
			String fieldValue = "";
			for(Object obj : objectsToDownload) {
				currentCol=startCol;
				Row cRow = targetSheet.createRow(currentRow);
				for(Field dField : downloadableFields) {
					columnName = dField.getAnnotation(Downloadable.class).columnName();
					getterMethodName = getGetterMethod(dField.getName());
					@SuppressWarnings("unchecked")
					Method getterMethod = clazz.getMethod(getterMethodName,new Class[] {});
					Object value = getterMethod.invoke(obj, new Object[] {});
					fieldValue = value != null ? value.toString() : "";
					
					Cell cCell = cRow.createCell(currentCol);
					cCell.setCellType(Cell.CELL_TYPE_STRING);
					cCell.setCellValue(fieldValue);
					
					logger.debug("Entered value on row {} [{} = {}]", currentRow, columnName, fieldValue);
					
					currentCol++;
				}
				
				currentRow++;
			}
			
			
		} catch(GenericSpreadsheetException e) {
			throw e;
		}catch (Exception e) {
			logger.error("ERROR while transferring objects to spreadsheet", e);
			throw new GenericSpreadsheetException("Could not transfer objects to spreadsheet", e);
		} finally {
			long endMillis = System.currentTimeMillis();
			logger.info("{} objects entered into spreadsheet in {}", objectsToDownload.size(), Utilities.calculateElapsedTime(startMillis, endMillis));
		}
	}
	
	private static String getSetterMethod(String fieldName) {
		return "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
	}
	
	
	
	private static String getGetterMethod(String fieldName) {
		return "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
	}
	
	private static CellStyle getHeaderRowStyle(Workbook wb){

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}
	
	public static void cleanUpTemp(String tempFolderPath) {
		try {
			File tempDir = new File(tempFolderPath);
			String[] filesInTemp = tempDir.list();
			for(String s : filesInTemp) {
				File tempFile = new File(tempDir.getPath(), s);
				tempFile.delete();
			}
			
			tempDir.delete();
		} catch (Exception e) {
			
			logger.warn("WARNING - Could not delete temp folder {}", tempFolderPath, e);
		}
	}
	
	public List<E> getObjectsFromSpreadsheet(String filePath, String sheetName, Class<E> cls) throws GenericSpreadsheetException {
		return this.getObjectsFromSpreadsheet(new File(filePath), sheetName, cls);
	}
	
	
	public List<E> getObjectsFromSpreadsheet(File spreadsheetFile, String sheetName, Class<E> clazz) throws GenericSpreadsheetException {
		
		
		try {
			
			Workbook workbook = getWorkbook(spreadsheetFile.getPath());
			List<E> objects = new ArrayList<E>();
			
			Sheet sheet=workbook.getSheet(sheetName);
			
			int startRow = 0;
			int currentRow = 1;
			int col = 0;
			
			List<String> colNames = new ArrayList<>();
			
			/*
			 * Adding all the Excel sheet column's Name in colNames ArrayList
			 */
			while (sheet.getRow(startRow).getCell(col) != null) {
				sheet.getRow(startRow).getCell(col).setCellType(Cell.CELL_TYPE_STRING);
				String value = sheet.getRow(startRow).getCell(col).getStringCellValue();
				colNames.add(value);
				col++;
			}
			int colSize = colNames.size();
			
			Field[] fields = FieldUtils.getFieldsWithAnnotation(clazz, Uploadable.class);
			E obj = null;
			while (sheet.getRow(currentRow) != null) {
				try {
					obj = clazz.newInstance();
				} catch (Exception e) {
					
					logger.error("ERROR initializing object of type {}", clazz.getName(), e);
					throw new GenericSpreadsheetException("Could not load spreadsheet contents due to an internal error.");
				} 
				col = 0;
				while (col < colSize) {
					sheet.getRow(currentRow).getCell(col, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_STRING);
					String value = sheet.getRow(currentRow).getCell(col).getStringCellValue();
					for (int j = 0; j < fields.length; j++) {
						String fCol=fields[j].getAnnotation(Uploadable.class).columnName();
						if (fCol.equalsIgnoreCase(colNames.get(col))) {

							String setterMethodName = getSetterMethod(fields[j].getName()); // setMode
							try {
								@SuppressWarnings("unchecked")
								Method setterMethod = clazz.getMethod(setterMethodName, fields[j].getType());
								try {
									setterMethod.invoke(obj, convertValueToType(value, fields[j].getType()));
								} catch (Exception e) {
									logger.error("Unable to invoke setter method {} for type {}", setterMethodName + "(" + fields[j].getType().getName() + ")",  clazz.getName(), e);
									throw new GenericSpreadsheetException("Could not invoke setter method due to an internal error, please contact tenjin support. ");							
								}
							} catch (SecurityException | NoSuchMethodException e) {

								logger.error("Unable to invoke setter method {} for type {}", setterMethodName + "(" + fields[j].getType().getName() + ")", clazz.getName());
								throw new GenericSpreadsheetException("please contact tenjin support. ");
							}
							break;
						}
					}
					col++;
				}
				
				//Invoke the setUploadRow() method
				//String setUploadRowMethod = "setUploadRow";
				@SuppressWarnings("unchecked")
				Method setUploadRowMethod = clazz.getMethod("setUploadRow", int.class);
				try {
					setUploadRowMethod.invoke(obj, currentRow);
				} catch (Exception e) {
					
					logger.error("Could not invoke setUploadRow method", e);
				}
				
				objects.add(obj);
				currentRow++;
			}
			return objects;
		} catch (Exception e) {
			logger.error("ERROR while reading spreadsheet on sheet {}", sheetName, e);
			throw new GenericSpreadsheetException("Could not load spreadsheet contents due to an internal error.");
		} 
		
		
		
	}
	
	@SuppressWarnings("rawtypes")
	private static Object convertValueToType(String value, Class type) {
		if (type.getName().toLowerCase().contains("int")) {
			// Integer or int
			if(Utilities.trim(value).equalsIgnoreCase("")) {
				return 0;
			}
			return Integer.parseInt(value);
		} else if (type.getName().toLowerCase().contains("long")) {
			// Long or long
			return Long.parseLong(value);
		} else if(type.getName().toLowerCase().contains("boolean")) {
			value = Utilities.trim(value);
			if(value.toLowerCase().equalsIgnoreCase("true") || value.toLowerCase().equalsIgnoreCase("yes") || value.toLowerCase().equals("y")) {
				return true;
			}else {
				return false;
			}
		}else {
			return value;
		}
	}
	
	public static Map<String, String> getControlProperties(String workbookPath) throws GenericSpreadsheetException{
		Map<String, String> controlProperties = new HashMap<String, String> ();
		
		try {
			Workbook workbook = getWorkbook(new FileInputStream(new File(workbookPath)));
			
			Sheet sheet = workbook.getSheet(CONTROL_SHEET_NAME);
			if(sheet == null) {
				throw new GenericSpreadsheetException("The uploaded file is invalid. Tenjin could not find the control sheet. Please try again with a valid file.");
			}
			
			Iterator<Row> rowIter = sheet.iterator();
			while(rowIter.hasNext()) {
				Row row = rowIter.next();
				if(row.getRowNum() < CONTROL_PROP_START_ROW) {
					continue;
				}
				
				Iterator<Cell> cellIter = row.cellIterator();
				String propName = "";
				String propValue = "";
				while(cellIter.hasNext()) {
					Cell cell = cellIter.next();
					if(cell.getColumnIndex() == CONTROL_PROP_COL) {
						cell.setCellType(Cell.CELL_TYPE_STRING);
						propName = Utilities.trim(cell.getStringCellValue());
						
						Cell valueCell = row.getCell(CONTROL_PROP_COL+1, Row.CREATE_NULL_AS_BLANK);
						propValue = Utilities.trim(valueCell.getStringCellValue());
						
						controlProperties.put(propName, propValue);
						break;
					}
				}
			}
		} catch (FileNotFoundException e) {
			
			logger.error("ERROR accessing workbook", e);
			throw new GenericSpreadsheetException("Could not access workbook.");
		} catch (IOException e) {
			
			logger.error("ERROR accessing workbook", e);
			throw new GenericSpreadsheetException("Could not access workbook.");
		}
				
		
		return controlProperties;
	}
	
	public static void main(String[] args) throws GenericSpreadsheetException {
		Map<String, String> controlMap = getControlProperties("C:\\users\\sriram\\desktop\\metadata-template.xlsx");
		for(Entry<String, String> o : controlMap.entrySet()) {
			System.out.println(o.getKey() + "=" + o.getValue());
		}
	}
	
	public static String generateTempFolder() {
		String SS_WORK_PATH = "";
		try {
			SS_WORK_PATH = TenjinConfiguration.getProperty("TJN_WORK_PATH");
		} catch (TenjinConfigurationException e) {
			
			logger.error("ERROR - Could not get TJN_WORK_PATH from configuration", e);
			SS_WORK_PATH = System.getProperty("user.home");
		}
		SS_WORK_PATH = SS_WORK_PATH + File.separator + "temp" + File.separator + "spreadsheets";

		File ssWorkDir = new File(SS_WORK_PATH);
		if(!ssWorkDir.exists()) {
			ssWorkDir.mkdirs();
		}
		
		String tempFolderName = SS_WORK_PATH + File.separator + Utilities.getRawTimeStamp();
		File tempDir = new File(tempFolderName);
		if(!tempDir.exists()) {
			tempDir.mkdirs();
		}
		
		return tempFolderName;
	}
}
