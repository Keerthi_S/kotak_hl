/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  User.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE              CHANGED BY                  DESCRIPTION
* 25-Oct-2016		Sriram						Added @SkipSerialization annotation to some fields (Req#TJN_24_18)
* 26-Aug-2017		Manish						T25IT-312
* 30-10-2017		Roshni						Tenjincg-398
* 10-Nov-2017		Manish						TENJINCG-428
* 29-Jan-2018		Preeti						TENJINCG-507
* 12-10-2018        Leelaprasad P          		TENJINCG-872
* 19-11-2018		Prem						TENJINCG-837
* 28-11-2018		Prem						TENJINCG-837
* 13-12-2018        Padmavathi              	TJNUN262-79
* 28-03-2019		Ashiki						TENJINCG-1023
* 24-06-2019        Padmavathi              for license 
*/

package com.ycs.tenjin.user;

import java.sql.Timestamp;
import java.util.List;

import com.ycs.tenjin.license.ProductLicense;
import com.ycs.tenjin.project.AuditRecord;
import com.ycs.tenjin.rest.SkipSerialization;
import com.ycs.tenjin.util.Utilities;

public class User {
	private String id;
	private String firstName;
	private String lastName;
	private String fullName;
	private String email;
	private String primaryPhone;
	private String secondaryPhone;
	
	@SkipSerialization // Sriram for Req#TJN_24_18
	private String password;
	private String roles;
	
	@SkipSerialization // Sriram for Req#TJN_24_18
	private String lastLogin;
	
	@SkipSerialization // Sriram for Req#TJN_24_18
	private String roleInCurrentProject;
	
	@SkipSerialization // Sriram for Req#TJN_24_18
	private int tenjinSessionId;
	/****************************************************************************************
	 * Added by Sriram for requirement TJN_22_15 (Clear user screen needs to be added) 01-09-2015 starts
	 */
	@SkipSerialization // Sriram for Req#TJN_24_18
	private String currentTerminal;
	
	
	
	public String getCurrentTerminal() {
		return currentTerminal;
	}
	public void setCurrentTerminal(String currentTerminal) {
		this.currentTerminal = currentTerminal;
	}
	/****************************************************************************************
	 * Added by Sriram for requirement TJN_22_15 (Clear user screen needs to be added) 01-09-2015 ends
	 */
	
	 /********************************
		 * changed by manish for req no-TJN_23_02, TJN_23_03
		 */ 
		@SkipSerialization // Sriram for Req#TJN_24_18
		private List<FailedLoginAttempts> failedLoginAttempts;
		//@SkipSerialization // Sriram for Req#TJN_24_18
		/*changed by manish for bug T25IT-312 on 26-Aug-2017 starts*/
		/*private String blockedStatus;*/
		/*Modified by Preeti for TENJINCG-507 starts*/
		/*private String userEnabled;*/
		private String enabledStatus;
		/*Modified by Preeti for TENJINCG-507 ends*/
		/*changed by manish for bug T25IT-312 on 26-Aug-2017 ends*/
		@SkipSerialization // Sriram for Req#TJN_24_18
		private Timestamp timestamp;
		
		
		
		public Timestamp getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(Timestamp timestamp) {
			this.timestamp = timestamp;
		}
		
		/*Modified by Preeti for TENJINCG-507 starts*/
		/*public String getBlockedStatus() {
			return userEnabled;
		}
		public void setBlockedStatus(String blockedStatus) {
			this.userEnabled = blockedStatus;
		}*/
		
		public String getEnabledStatus() {
			return enabledStatus;
		}
		public void setEnabledStatus(String enabledStatus) {
			this.enabledStatus = enabledStatus;
		}
		/*Modified by Preeti for TENJINCG-507 ends*/
		public List<FailedLoginAttempts> getFailedLoginAttempts() {
			return failedLoginAttempts;
		}
		public void setFailedLoginAttempts(List<FailedLoginAttempts> failedLoginAttempts) {
			this.failedLoginAttempts = failedLoginAttempts;
		}
		
		/********************************
		 * changed by manish for req no-TJN_23_02, TJN_23_03 ends
		 */
	
	public int getTenjinSessionId() {
		return tenjinSessionId;
	}
	public void setTenjinSessionId(int tenjinSessionId) {
		this.tenjinSessionId = tenjinSessionId;
	}
	/**
	 * @return the roleInCurrentProject
	 */
	public String getRoleInCurrentProject() {
		return roleInCurrentProject;
	}
	/**
	 * @param roleInCurrentProject the roleInCurrentProject to set
	 */
	public void setRoleInCurrentProject(String roleInCurrentProject) {
		this.roleInCurrentProject = roleInCurrentProject;
	}
	/**
	 * @return the lastLogin
	 */
	public String getLastLogin() {
		return lastLogin;
	}
	/**
	 * @param lastLogin the lastLogin to set
	 */
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}
	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the primaryPhone
	 */
	public String getPrimaryPhone() {
		return primaryPhone;
	}
	/**
	 * @param primaryPhone the primaryPhone to set
	 */
	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}
	/**
	 * @return the secondaryPhone
	 */
	public String getSecondaryPhone() {
		return secondaryPhone;
	}
	/**
	 * @param secondaryPhone the secondaryPhone to set
	 */
	public void setSecondaryPhone(String secondaryPhone) {
		this.secondaryPhone = secondaryPhone;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the roles
	 */
	public String getRoles() {
		return roles;
	}
	/**
	 * @param roles the roles to set
	 */
	public void setRoles(String roles) {
		this.roles = roles;
	}
	
	
	/*Added by Ashiki for TENJINCG-1023 starts*/
	private AuditRecord auditRecord;
	
	public AuditRecord getAuditRecord() {
		return auditRecord;
	}
	public void setAuditRecord(AuditRecord auditRecord) {
		this.auditRecord = auditRecord;
	}
	
	/*Added by Ashiki for TENJINCG-1023 ends*/
	
	public void merge(User user)
	{
		this.firstName = user.getFirstName() != null ? Utilities.trim(user.getFirstName()) : this.firstName;
		this.lastName =  user.getLastName() != null ? Utilities.trim(user.getLastName()) : this.lastName;
		this.primaryPhone =  user.getPrimaryPhone() != null? Utilities.trim(user.getPrimaryPhone()) : this.primaryPhone;
		this.email =  user.getEmail() != null ? Utilities.trim(user.getEmail()) : this.email;
		this.roles =  user.getRoles() != null ? Utilities.trim(user.getRoles()) : this.roles;
		this.password =Utilities.trim(user.getPassword()).length() > 0 ? Utilities.trim(user.getPassword()) : this.password;
		/*added by manish for bug #TENJINCG-428 on 10-Nov-2017 starts*/
		/*Modified by Preeti for TENJINCG-507 starts*/
		/*this.userEnabled = user.getBlockedStatus() != null ? Utilities.trim(user.getBlockedStatus()) :this.userEnabled;*/
		this.enabledStatus = user.getEnabledStatus() != null ? Utilities.trim(user.getEnabledStatus()) :this.enabledStatus;
		/*Modified by Preeti for TENJINCG-507 ends*/
		/*added by manish for bug #TENJINCG-428 on 10-Nov-2017 ends*/
	}
/*Modified by Roshni for TENJINCG-398 ends */
	/*Changed by Leelaprasad for TENJINCG-872 starts*/
	private int failedLoginAttemptsCount;
	private boolean loggedIn;



	public int getFailedLoginAttemptsCount() {
		return failedLoginAttemptsCount;
	}
	public void setFailedLoginAttemptsCount(int failedLoginAttemptsCount) {
		this.failedLoginAttemptsCount = failedLoginAttemptsCount;
	}
	public boolean isLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	/* Uncommented by Padmavathi for license starts */
	public int getMaxNumberUsers() {
		return maxNumberUsers;
	}
	public void setMaxNumberUsers(int maxNumberUsers) {
		this.maxNumberUsers = maxNumberUsers;
	}
/* Uncommented by Padmavathi for license ends */

	/*Changed by Leelaprasad for TENJINCG-872 ends*/
	
	
/* Uncommented by Padmavathi for license starts */
	private int maxNumberUsers;
	
	private ProductLicense productLicense;



	public ProductLicense getProductLicense() {
		return productLicense;
	}
	public void setProductLicense(ProductLicense productLicense) {
		this.productLicense = productLicense;
	}
	/* Uncommented by Padmavathi for license ends */
	/*Added by Padmavathi for TJNUN262-79 starts*/
	private String sessionKey;
	public String getSessionKey() {
		return sessionKey;
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	/*Added by Padmavathi for TJNUN262-79 ends*/
	
}
