/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  CoreDefectProcessor.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 22-Nov-2016           Sriram Sridharan          Newly Added For 
 * 08-Dec-2016			Sriram Sridharan		Changed by Sriram to implement DTT User Mapping functionality
 * 16-Dec-2016			Manish					Defect TEN-86
 * 16-Dec-2016			Manish					Defect TEN-83
 * 20-Dec-2016			Manish					Defect TEN-106
 * 10-Jan-2017			Manish					TENJINCG-5
 * 10-Jan-2017			Manish					TENJINCG-119
 * 15-02-2017			Manish					TENJINCG-121
 * 01-Aug-2017		    Manish					TENJINCG-74,75
 * 09-Oct-2017		    Manish					using user credentials for sync instead of instance credentials
 * 08-Nov-2018			Ashiki					TENJINCG-901
 * 23-11-2018			Ashiki					TENJINCG-903
 * 06-12-2018			Ashiki					For BURGAN Bank ends
 * 31-12-2018			Ashiki					TJN252-49
 * 31-12-2018			Ashiki					Posting Defect from SaveReview
 * 31-12-2018			Ashiki					Posting Defect
 * 25-02-2019			Ashiki					TENJINCG-985
 * 19-04-2019            Padmavathi              TNJNR2-5
 * 12-06-2020			Ashiki					Tenj210-87
 * 11-12-2020			Pushpalatha				TENJINCG-1221
 * 15-12-2020			Pushpa					TENJINCG-1222
 * 16-12-2020			Prem					TENJINCG-1236
 * 16-12-2020			Prem					TENJINCG-1244
 */


package com.ycs.tenjin.defect;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ConnectException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.ProcessingException;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.db.CoreDefectHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.DefectHelper;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

public class CoreDefectProcessor {
	private static final Logger logger = LoggerFactory.getLogger(CoreDefectProcessor.class);
	private Connection conn;
	private Map<String, DefectManagementInstance> dttInstanceMap;

	public CoreDefectProcessor() throws TenjinServletException{
		try {
			this.conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch (DatabaseException e) {
			
			throw new TenjinServletException(e.getMessage());
		}

		this.dttInstanceMap = new HashMap<String, DefectManagementInstance>();
	}
	/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
	/*changed by manish for using user credentials instead of instance credentials on 09-Oct 2017 starts*/
	/*public JSONObject syncMultipleDefects(String defectRecordIdsCSV) throws TenjinServletException{*/
	public JSONObject syncMultipleDefects(String defectRecordIdsCSV, String userId) throws TenjinServletException{
		/*changed by manish for using user credentials instead of instance credentials on 09-Oct 2017 ends*/
		/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
		//This method accepts a comma separated list of defect record IDs (1,2,3,4,5)
		boolean syncSuccessful = true;
		logger.info("Getting details about all defects");
		List<Defect> defects = new ArrayList<Defect>();
		String[] dArray = defectRecordIdsCSV.split(",");
		CoreDefectHelper helper = new CoreDefectHelper();
		/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
		JSONObject objJson = new JSONObject();
		List<String> listExceptions = new ArrayList<String>();
		/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
		for(String d:dArray){
			try {
				logger.debug("Fetching info for defect {}", d);
				Defect defect = helper.hydrateDefect(this.conn, Integer.parseInt(d));
				/*changed by manish for req TENJINCG-5 on 10-Jan-17 starts*/

				if(defect.getPosted() != null && defect.getPosted().equalsIgnoreCase("yes")){
					defects.add(defect);
				}
				/*defects.add(defect);*/
				/*changed by manish for req TENJINCG-5 on 10-Jan-17 ends*/

			} catch (NumberFormatException e) {
				logger.error("Invalid Defect Record ID {}", d, e);
				syncSuccessful = false;
			} catch (DatabaseException e) {
				logger.error(e.getMessage());
				syncSuccessful = false;
			}
		}

		Map<String, DefectManager> defectManagerMap = new HashMap<String, DefectManager>();

		/*changed by manish for req TENJINCG-5 on 10-Jan-17 starts*/

		if(defects!=null && defects.size()>0){

			/*changed by manish for req TENJINCG-5 on 10-Jan-17 ends*/
			for(Defect defect:defects){
				int recordId = defect.getRecordId();
				String instanceName = defect.getDttInstanceName();
				DefectManager dm = defectManagerMap.get(instanceName);
				try {
					/*Added by Pushpalatha for TENJINCG-1203 starts*/
					String toolName="";
					/*Added by Pushpalatha for TENJINCG-1203 ends*/
					if( dm== null){
						logger.info("Loading DM Implementation for [{}]", instanceName);

						/*changed by manish for using user credentials instead of instance credentials on 09-Oct 2017 starts*/
						/*DefectManagementInstance instance = new DefectHelper().hydrateDefectManagerInstance(this.conn, defect.getDttInstanceName());*/
						DefectManagementInstance instance = new DefectHelper().hydrateDefectManagerInstance(this.conn, defect.getDttInstanceName(), userId);
						/*changed by manish for using user credentials instead of instance credentials on 09-Oct 2017 ends*/

						toolName = instance.getTool();
						logger.info("Encrypting Password for Defect Managment");
						/*Modified by paneendra for VAPT FIX starts*/
						/*String decryptedPwd = new Crypto().decrypt(instance.getPassword().split("!#!")[1],  decodeHex(instance.getPassword().split("!#!")[0].toCharArray()));*/
						String decryptedPwd = new CryptoUtilities().decrypt(instance.getPassword());
						/*Modified by paneendra for VAPT FIX ends*/
						logger.info("Getting Implementation for [{}]", toolName);
						dm = DefectFactory.getDefectTools(toolName, instance.getURL(), instance.getAdminId(), decryptedPwd);
						dm.setProjectDesc(defect.getEdmProjectName());
						logger.info("Logging In to [{}]", defect.getDttInstanceName());
						dm.login();

						defectManagerMap.put(instanceName, dm);
					}

					logger.info("Fetching information of defect [{}] from [{}]", defect.getIdentifier(), defect.getDttInstanceName());
					/*Modified by Pushpa for tENJINCG-1222 starts*/
					Defect syncDefect=null;
					if(toolName.equalsIgnoreCase("github")){
						syncDefect = dm.getDefect(defect);
					}else{
						syncDefect = dm.getDefect(defect.getIdentifier());
					}
					/*Modified by Pushpa for tENJINCG-1222 ends*/
					/*Uncommented and modified by Pushpalatha for TENJINCG-1203 starts*/
					/*Commented By Ashiki for BURGAN Bank starts*/
					logger.info("Fetching attachments for defect [{}] from [{}]", defect.getIdentifier(), defect.getDttInstanceName());
					List<Attachment> attachments = dm.getDefectAttachments(defect.getIdentifier(), defect.getEdmProjectName());
					if(attachments !=null && !toolName.equalsIgnoreCase("DMS")){
						String tempAttachmentFolderName = TenjinConfiguration.getProperty("MEDIA_REPO_PATH") + "\\Defect";
						File dir  = new File(tempAttachmentFolderName);
						if(!dir.exists()){
							dir.mkdir();
						}

						tempAttachmentFolderName = tempAttachmentFolderName + "\\" + recordId;
						dir  = new File(tempAttachmentFolderName);
						if(!dir.exists()){
							dir.mkdir();
						}	

						for(Attachment att:attachments){
							File sFile = null;
							/*Modified by Prem TENJINCG-1244 Starts*/
							if(toolName.equalsIgnoreCase("mantis")){
								sFile = new File(att.getFileName());
								FileWriter myWriter = new FileWriter(att.getFileName());
								myWriter.write(att.getFileContent());
								myWriter.close();
							}else {
								sFile = new File(att.getFilePath());
							}
							/*Modified by Prem TENJINCG-1244 End*/
							File dFile = new File(tempAttachmentFolderName + "\\" + att.getFileName());

							logger.debug("Removing existing attachment [{}] from [{}]", att.getFileName(), tempAttachmentFolderName);
							if(dFile.exists()){
								if(dFile.delete()){
									logger.debug("File deleted..");
								}else{
									logger.warn("Could not delete existing attachment [{}] from [{}]", att.getFileName(), tempAttachmentFolderName);
								}
							}

							logger.debug("Copying new attachment [{}] from DTT", att.getFileName());
							FileUtils.copyFile(sFile, dFile);
							logger.debug("Copy successful");

							logger.debug("Clearing temp file from [{}]", att.getFilePath());
							if(sFile.delete()){
								logger.debug("Temp File deleted...");
							}else{
								logger.warn("Could not delete temp attachment from [{}] - Space issues may occur. Manual clean-up required.", att.getFilePath());
							}

							att.setFilePath(tempAttachmentFolderName + "\\" + att.getFileName()); 

						}
					}


					syncDefect.setAttachments(attachments);

					logger.debug("Getting comments for defect with DTT Identifier [{}]", defect.getIdentifier());
					List<Comment> comments = dm.getComments(defect.getIdentifier());
					logger.info("Updating defect data in local DB");
					/*Commented By Ashiki for BURGAN Bank ends*/
					/*Uncommented and modified by Pushpalatha for TENJINCG-1203 ends*/


					syncDefect.setRecordId(recordId);
					helper.syncDefect(this.conn, syncDefect);
					/*Uncommented by Pushpalatha for TENJINCG-1203 starts*/
					/*Commented By Ashiki for BURGAN Bank starts*/
					/*Modified by Pushpa for tENJINCG-1222 starts*/
					if(!toolName.equalsIgnoreCase("github")){
						helper.syncDefectAttachments(this.conn, syncDefect);
					}
					/*Modified by Pushpa for tENJINCG-1222 ends*/
					/*Commented By Ashiki for BURGAN Bank ends*/
					/*Uncommented by Pushpalatha for TENJINCG-1203 ends*/
					logger.info("Done");
					/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
				} catch (UnreachableEDMException e) {
					logger.error("ERROR while syncing defect {}", recordId, e);
					/*Changed By Ashiki for TENJINCG-985 starts*/
					/*listExceptions.add("Tenjin is not able to reach to DTT tool. Please check configuration and try again(defect-"+defect.getRecordId()+").");*/
					listExceptions.add("Tenjin is not able to reach to Defect Management tool. Please check configuration and try again(defect-"+defect.getRecordId()+").");
					/*Changed By Ashiki for TENJINCG-985 ends*/
					syncSuccessful = false;
				} catch (EDMException e) {
					logger.error("ERROR while syncing defect {}", recordId, e);
					syncSuccessful = false;
					listExceptions.add(e.getMessage()+"(defect-"+defect.getRecordId()+").");
				}
				catch (IOException e) {
					logger.error("ERROR while syncing defect {}", recordId, e);
					listExceptions.add("Not able to fetch attachments related to defect-"+defect.getRecordId());
					syncSuccessful = false;
				} catch (DatabaseException e) {
					logger.error("ERROR while syncing defect {}", recordId, e);
					listExceptions.add("An Internal error occured, Please contact Tenjin support(defect-"+defect.getRecordId()+").");
					syncSuccessful = false;
				} /*Added by Ashiki for TJN252-49 starts*/
				catch (ProcessingException e) {
					listExceptions.add("(defect-"+defect.getRecordId()+").Could not synchronise "+defect.getDttInstanceName()+" is Down");
					syncSuccessful = false;
				}
				/*Added by Ashiki for TJN252-49 ends*/catch (Exception e) {
					logger.error("ERROR while syncing defect {}", recordId, e);
					listExceptions.add("An Internal error occured, Please contact Tenjin support(defect-"+defect.getRecordId()+").");
					syncSuccessful = false;
				} 

				/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 ends*/
			}
			/*changed by manish for req TENJINCG-5 on 10-Jan-17 starts*/
		}else{
			syncSuccessful = false;
			logger.info("user has not selected any posted defects");
		}
		for (int i = 0; i < listExceptions.size(); i++) {

			String newvalue = (i + 1) + ") " + listExceptions.get(i) + ".";
			listExceptions.set(i, newvalue);
		}

		/*changed by manish for req TENJINCG-5 on 10-Jan-17 ends*/

		try {
			objJson.put("listExceptions", listExceptions);
			if(syncSuccessful){
				objJson.put("syncSuccessful", "true");
			}else{
				objJson.put("syncSuccessful", "false");
			}
		} catch (JSONException e) {
			logger.error(e.getMessage());
		}


		return objJson;
	}

	/*Added by Ashiki for TJN252-49 starts*/
	public Defect hydrateDefect(int recordId) throws TenjinServletException {
		Defect defect = null;
		CoreDefectHelper helper = new CoreDefectHelper();
		try {
			defect = helper.hydrateDefect(this.conn, recordId);
			if(defect == null){
				logger.debug("Defect for record {} is null", recordId);
				throw new TenjinServletException("Invalid Defect Record ID [" + recordId + "]");
			}

		} catch (DatabaseException e) {
			
			throw new TenjinServletException(e.getMessage());
		}
		return defect;
	}

	public DefectManagementInstance hydrateinstance(Defect defect,String userId) throws  DatabaseException {

		DefectManagementInstance instance=null;
		try {
			instance = new DefectHelper().hydrateDefectManagerInstance(this.conn, defect.getDttInstanceName(), userId);
		}
		catch (NullPointerException e) {
			
		}
		return instance;
	}
	
	public Defect syncDefectDetails(int recordId, String userId,DefectManagementInstance instance,Defect defect) throws TenjinServletException, RequestValidationException, ConnectException{
		
		logger.debug("Defect Posted status --> {}", defect.getPosted());
		if(Utilities.trim(defect.getPosted()).equalsIgnoreCase("y") || Utilities.trim(defect.getPosted()).equalsIgnoreCase("yes")){
			Defect syncDefect = null;
			/*Added by Ashiki for TJN252-49 starts*/
			CoreDefectHelper helper = new CoreDefectHelper();
			/*Added by Ashiki for TJN252-49 ends*/
			try {
				

				String toolName = instance.getTool();
				logger.info("Encrypting Password for Defect managment");
				/*Modified by paneendra for VAPT FIX starts*/
				/*String decryptedPwd = new Crypto().decrypt(instance.getPassword().split("!#!")[1],  decodeHex(instance.getPassword().split("!#!")[0].toCharArray()));*/
				String decryptedPwd = new CryptoUtilities().decrypt(instance.getPassword());
				/*Modified by paneendra for VAPT FIX ends*/
				logger.info("Getting Implementation for [{}]", toolName);
				DefectManager dm = DefectFactory.getDefectTools(toolName, instance.getURL(), instance.getAdminId(), decryptedPwd);
				dm.setProjectDesc(defect.getEdmProjectName());
				logger.info("Logging In to [{}]", defect.getDttInstanceName());
				/*Added by Ashiki for TJN252-49 starts*/
				/*dm.login();*/
				try {
					dm.login();
				}catch(ProcessingException e) {
					logger.error("Error ", e);
				}
				/*Added by Ashiki for TJN252-49 ends*/


				logger.info("Fetching information of defect [{}] from [{}]", defect.getIdentifier(), defect.getDttInstanceName());
				/*Modified by Pushpa for tENJINCG-1222 starts*/
				if(toolName.equalsIgnoreCase("github")){
					syncDefect = dm.getDefect(defect);
				}else{
					syncDefect = dm.getDefect(defect.getIdentifier());
				}
				/*Modified by Pushpa for tENJINCG-1222 ends*/

				logger.info("Done");
				logger.info("Refreshing defect information");

				defect = helper.hydrateDefect(this.conn, recordId);
			} catch (DatabaseException e) {
				
				throw new TenjinServletException(e.getMessage());
			} catch (EDMException e) {
				
				throw new TenjinServletException(e.getMessage());
			} catch (UnreachableEDMException e) {
				
				throw new TenjinServletException(e.getMessage());
			} 
			catch (Exception e) {
				
				logger.error("ERROR while trying to sync defect {}", recordId, e);
				throw new TenjinServletException("Could not synchronize defect information due to an internal error. Please contact Tenjin Support.");
			} 


		}else{
			logger.warn("Tenjin cannot sync this defect as it is not posted to Defect Management");
		}

		return defect;

	}

	public void saveDefect(Defect defect, int projectId) throws TenjinServletException{
		CoreDefectHelper helper = new CoreDefectHelper();

		if(defect == null){
			logger.error("Defect object is null");
			throw new TenjinServletException("Invalid defect");
		}

		logger.info("Saving defect with record ID {} to DB", defect.getRecordId());

		try {
			helper.newDefect(defect, projectId);
			logger.info("Done");
		} catch (DatabaseException e) {
			throw new TenjinServletException(e.getMessage());
		}
	}


	public void updateDefectDetails(Defect defect) throws TenjinServletException{
		CoreDefectHelper helper = new CoreDefectHelper();

		if(defect == null){
			logger.error("Defect object is null");
			throw new TenjinServletException("Invalid defect");
		}

		logger.info("Updating defect with record ID {} to DB", defect.getRecordId());

		try {
			helper.updateDefectInfo(this.conn, defect);
			logger.info("Done");
		} catch (DatabaseException e) {
			throw new TenjinServletException(e.getMessage());
		}
	}

	public void removeAttachments(Defect defect, String attachmentRecordIds) throws TenjinServletException{
		CoreDefectHelper helper = new CoreDefectHelper();

		if(defect == null){
			logger.error("Defect object is null");
			throw new TenjinServletException("Invalid defect");
		}

		logger.info("Updating defect with record ID {} to DB", defect.getRecordId());

		try {
			String[] atts = attachmentRecordIds.split(",");
			helper.removeAttachments(this.conn, defect.getRecordId(), atts);
			logger.info("Done");
		} catch (DatabaseException e) {
			throw new TenjinServletException(e.getMessage());
		}
	}

	public void saveDefectAttachments(Defect defect) throws TenjinServletException{
		CoreDefectHelper helper = new CoreDefectHelper();

		if(defect == null){
			logger.error("Defect object is null");
			throw new TenjinServletException("Invalid defect");
		}

		if(defect.getAttachments() != null && defect.getAttachments().size() > 0){
			logger.info("Saving {} attachments for defect {}", defect.getAttachments().size(), defect.getRecordId());
			try {
				helper.persistDefectAttachments(defect.getRecordId(), defect.getAttachments());
			} catch (DatabaseException e) {
				throw new TenjinServletException(e.getMessage());
			}
		}else{
			logger.warn("No defect attachmets found to save.");
		}
	}

	public void postDefectToDTT(Defect defect, String tenjinUserId) throws TenjinServletException{
		logger.debug("entered postDefectToDTT()");
		CoreDefectHelper helper =new CoreDefectHelper();
		/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
		boolean postFlag = false;
		/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 ends*/

		if(defect == null){
			logger.error("Defect object is null");
			throw new TenjinServletException("Invalid defect");
		}

		if(Utilities.trim(defect.getPosted()).equalsIgnoreCase("y") || Utilities.trim(defect.getPosted()).equalsIgnoreCase("yes")){
			logger.error("Defect with ID [{}] is already posted. Defect Management identifier --> [{}]", defect.getRecordId(), defect.getIdentifier());
			/*Changed By Ashiki for TENJINCG-985 starts*/
			/*throw new TenjinServletException("This defect has already been posted. DTT Identifier is [" + defect.getIdentifier() + "]");*/
			throw new TenjinServletException("This defect has already been posted. Defect Management Identifier is [" + defect.getIdentifier() + "]");
			/*Changed By Ashiki for TENJINCG-985 ends*/
		}

		logger.debug("Validating Defect Managment instance");
		try {
			/*****************************
			 * Changed by Sriram to implement DTT User Mapping functionality
			 */
			/*DefectManagementInstance instance = new DefectHelper().hydrateDefectManagerInstance(this.conn, defect.getDttInstanceName());*/
			DefectManagementInstance instance = new DefectHelper().hydrateDefectManagerInstance(this.conn, defect.getDttInstanceName(), tenjinUserId);
			if(instance == null){
				logger.error("Defect Management Instance is null for instance [{}], Tenjin User [{}].", defect.getDttInstanceName(), tenjinUserId);
				/*Changed By Ashiki for TENJINCG-985 starts*/
				/*throw new TenjinServletException("Could not access DTT Instance [" + defect.getDttInstanceName() + "]. Either the instance is unavailable, or you have not specified credentials to access the instance.");*/
				throw new TenjinServletException("Could not access Defect Management Instance [" + defect.getDttInstanceName() + "]. Either the instance is unavailable, or you have not specified credentials to access the instance.");
				/*Changed By Ashiki for TENJINCG-985 ends*/
			}
			/*****************************
			 * Changed by Sriram to implement DTT User Mapping functionality
			 */

			String toolName = instance.getTool();
			if(Utilities.trim(toolName).equalsIgnoreCase("qc") || Utilities.trim(toolName).equalsIgnoreCase("alm")){
				defect.setCreatedOn(new Timestamp(new Date().getTime()));
				defect.setCreatedBy(instance.getAdminId());
			}
			/*changed by manish for defect TEN-83 on 16-Dec-2016 starts*/
			if(Utilities.trim(toolName).equalsIgnoreCase("jira")){
				String dttkey = helper.hydrateKey(defect.getEdmProjectName(),defect.getDttInstanceName());
				defect.setEdmProjectName(dttkey);
			}
			if(Utilities.trim(toolName).equalsIgnoreCase("dms")){
				String dttkey = helper.hydrateKey(defect.getEdmProjectName(),defect.getDttInstanceName());
				defect.setEdmProjectName(dttkey);
			}
			/*Addded by Pushpa for TENJINCG-1221 starts*/
			if(Utilities.trim(toolName).equalsIgnoreCase("github")){
				String dttkey = helper.hydrateKey(defect.getEdmProjectName(),defect.getDttInstanceName());
				//defect.setEdmProjectName(dttkey);
			}
			/*Addded by Pushpa for TENJINCG-1221 ends*/
			/*Added by Prem for TENJINCG-1236 starts*/
			if(Utilities.trim(toolName).equalsIgnoreCase("mantis")){
				defect.setEdmProjectName(defect.getEdmProjectName());
			}
			/*Added by Prem for TENJINCG-1236 Ends*/
			/*changed by manish for defect TEN-83 on 16-Dec-2016 ends*/

			/*Added by Paneendra for TENJINCG-1262 Starts*/
			if(Utilities.trim(toolName).equalsIgnoreCase("rally")){
				String dttkey = helper.hydrateKey(defect.getEdmProjectName(),defect.getDttInstanceName());
				//defect.setEdmProjectName(dttkey);
				defect.setProjObjId(dttkey);
			}
			/*Added by Paneendra for TENJINCG-1262 Ends*/
			/*Modified by paneendra for VAPT FIX starts*/
			/*String decryptedPwd = new Crypto().decrypt(instance.getPassword().split("!#!")[1],
					  decodeHex(instance.getPassword().split("!#!")[0].toCharArray()));*/
			String decryptedPwd = new CryptoUtilities().decrypt(instance.getPassword());
			/*Modified by paneendra for VAPT FIX ends*/
			logger.info("Getting Implementation for [{}]", toolName);
			/*Modified by Pushpa for TENJINCG-1221 starts*/
			DefectManager dm=null;
			if(instance.getOrganization()==null){
				dm = DefectFactory.getDefectTools(toolName, instance.getURL(), instance.getAdminId(), decryptedPwd);
			}else{
				dm = DefectFactory.getDefectTools(toolName, instance.getURL(), instance.getAdminId(), decryptedPwd,instance.getOrganization());
			}
			/*Modified by Pushpa for TENJINCG-1221 starts*/
			dm.setProjectDesc(defect.getEdmProjectName());
			logger.info("Logging In to [{}]", defect.getDttInstanceName());
			/*changed by manish for defect TEN-106 on 20-Dec-2016 starts*/
			helper.hydrateAttachments(this.conn,defect);
			/*changed by manish for defect TEN-106 on 20-Dec-2016 ends*/
			dm.login();
			/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
			JSONArray dttProjectsArray = new JSONArray();
			if(dm.isUrlValid()){
				dttProjectsArray = dm.getAllProjects();
				for(int i=0; i<dttProjectsArray.length();i++){
					JSONObject objDttProject = new JSONObject();
					objDttProject=dttProjectsArray.getJSONObject(i);
					if(Utilities.trim(toolName).equalsIgnoreCase("qc") || Utilities.trim(toolName).equalsIgnoreCase("alm")){
						/*Added by Ashiki for TENJINCG-901 Starts*/
						if(defect.getStatus()==null){
							defect.setStatus("New");
						}
						/*Added by Ashiki for TENJINCG-901 ends*/
						if(objDttProject.getString("pname").equalsIgnoreCase(defect.getEdmProjectName().split(":")[1])){
							postFlag=true;
						}
					}
					/*Added by Paneendra for TENJINCG-1262 Starts*/
					else if(Utilities.trim(toolName).equalsIgnoreCase("rally")){

						if(objDttProject.getString("pname").equalsIgnoreCase(defect.getProjObjId())||(objDttProject.getString("pid").equalsIgnoreCase(defect.getProjObjId()))){
							postFlag=true;
						}
					}/*Added by Paneendra for TENJINCG-1262 Ends*/

					else{
						if(objDttProject.getString("pname").equalsIgnoreCase(defect.getEdmProjectName()) || objDttProject.getString("pid").equalsIgnoreCase(defect.getEdmProjectName())){
							postFlag=true;
						}
					}


				}
				if(!postFlag){
					logger.info("Defect Management project does not exist.");
					/*Changed By Ashiki for TENJINCG-985 starts*/
					/*throw new TenjinServletException("Dtt project does not exist.Somebody may have deleted the Dtt project.");*/
					throw new TenjinServletException("Defect Mangement project does not exist.Somebody may have deleted the Defect Management project.");
					/*Changed By Ashiki for TENJINCG-985 ends*/
				}
				logger.info("Posting Defect");
				dm.postDefect(defect);

			}else{
				throw new TenjinServletException("Please check the URL and then try again.");
			}
			

			logger.info("Logging out");
			dm.logout();


			helper.updateDefectPostingStatus(this.conn, defect, instance.getName());
			logger.info("Defect Posted successfully with Defect Management ID --> [{}]", defect.getIdentifier());
			/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
		} catch (DatabaseException e) {
			throw new TenjinServletException(e.getMessage());
		}
		/*Added by Padmavathi for TNJNR2-5 starts*/
		catch (NullPointerException e) {
			if(e.getMessage().contains("Please define DTT user credentials for current user")){
				throw new TenjinServletException("Please define Defect Management user credentials for current user");
			}else{
				throw new TenjinServletException("Could not post defect to Defect Mangement due to an internal error. Please contact Tenjin Support.");
			}
		}
		/*Added by Padmavathi for TNJNR2-5 ends*/
		catch (EDMException e) {
			/*throw new TenjinServletException("Could not post defect to DTT due to an internal error. Please contact Tenjin Support.");*/

			throw new TenjinServletException(e.getMessage());
		}  catch (UnreachableEDMException e) {
			logger.error("ERROR posting defect to Defect Management", e);
			throw new TenjinServletException(e.getMessage());
		} catch (JSONException e) {
			logger.error("ERROR posting defect to Defect Management", e);
			throw new TenjinServletException(e.getMessage());
		} catch (Exception e) {
			logger.error("ERROR posting defect to Defect Management", e);
		}
		/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 ends*/
	}

	/******
	 * changed by manish for defect TENJINCG-121 on 15-02-2017 starts
	 */
	/*public void reviewDefect(int defectRecordId, String severity, String action, String dttInstanceName) throws TenjinServletException{*/
	public void reviewDefect(int defectRecordId, String severity, String action, String dttInstanceName,String userId) throws TenjinServletException{
		/******
		 * changed by manish for defect TENJINCG-121 on 15-02-2017 ends
		 */
		logger.debug("Getting information about defect [{}]", defectRecordId);
		CoreDefectHelper helper = new CoreDefectHelper();

		try {
			Defect defect = helper.hydrateDefect(defectRecordId);
			if(defect == null){
				logger.error("Defect with Record ID [{}] is null - Not Found", defectRecordId);
				throw new TenjinServletException("Invalid Defect Record ID [" + defectRecordId + "]");
			}

			action = Utilities.trim(action);
			severity = Utilities.trim(severity);

			logger.info("Saving review information for defect [{}]", defectRecordId);
			if(action.equalsIgnoreCase("") || action.equalsIgnoreCase("-1")){
				action = "Not Reviewed";
			}

			helper.updateDefectReviewInfo(this.conn, defectRecordId, action, severity);
			defect.setSeverity(severity);


			if(action.equalsIgnoreCase("post")){
				logger.info("Validating Defect Management Instance");
				DefectManagementInstance instance = this.dttInstanceMap.get(dttInstanceName);
				if(instance == null){
					/******
					 * changed by manish for defect TENJINCG-121 on 15-02-2017 starts
					 */
					/*instance = new DefectHelper().hydrateDefects(dttInstanceName);*/
					instance = new DefectHelper().hydrateDefectManagerInstance(this.conn, dttInstanceName,userId );
					/******
					 * changed by manish for defect TENJINCG-121 on 15-02-2017 ends
					 */
					this.dttInstanceMap.put(dttInstanceName, instance);
				}

				if(instance == null){
					logger.error("Invalid Defect Management Instance Name [{}]", dttInstanceName);
					/*Changed By Ashiki for TENJINCG-985 starts*/
					/*throw new TenjinServletException("The DTT Instance [" + dttInstanceName + "] is invalid. Please contact your Tenjin Administrator.");*/
					throw new TenjinServletException("The Defect Management Instance [" + dttInstanceName + "] is invalid. Please contact your Tenjin Administrator.");
					/*Changed By Ashiki for TENJINCG-985 ends*/
				}

				/*Added by Ashiki for Posting defect from SaveReview starts*/
				defect.setCreatedBy(instance.getAdminId());
				/*Added by Ashiki for Posting defect from SaveReview ends*/

				logger.info("Getting Defect Management Project Name");
				String dttProjectName = helper.getEDMProjectIdentifier(conn, defect.getAppId(), defectRecordId);
				if(Utilities.trim(dttProjectName).equalsIgnoreCase("")){
					logger.error("Defect Management Project Name is null");
					/*Changed By Ashiki for TENJINCG-985 starts*/
					/*throw new TenjinServletException("This defect cannot be posted to " + dttInstanceName + " as no corresponding DTT Project was specified");*/
					throw new TenjinServletException("This defect cannot be posted to " + dttInstanceName + " as no corresponding Defect Management Project was specified");
					/*Changed By Ashiki for TENJINCG-985 ends*/
				}

				defect.setEdmProjectName(dttProjectName);

				logger.info("Setting appropriate Priority");
				String oPriority="";
				if(!Utilities.trim(instance.getPriority()).equalsIgnoreCase("")){
					String[] split = instance.getPriority().split(",");
					int len = split.length/2;
					oPriority = split[len];
				}

				logger.info("Defect [{}] will be set to Priority [{}]", defectRecordId, oPriority);
				defect.setPriority(oPriority);


				String toolName = instance.getTool();
				

				/*Added by Ashiki for Tenj210-87 starts*/
				if(Utilities.trim(toolName).equalsIgnoreCase("qc") || Utilities.trim(toolName).equalsIgnoreCase("alm")){
					defect.setCreatedOn(new Timestamp(new Date().getTime()));
					defect.setCreatedBy(instance.getAdminId());
				}
				/*changed by manish for defect TEN-83 on 16-Dec-2016 starts*/
				if(Utilities.trim(toolName).equalsIgnoreCase("jira")){
					String dttkey = helper.hydrateKey(defect.getEdmProjectName(),defect.getDttInstanceName());
					defect.setEdmProjectName(dttkey);
				}
				if(Utilities.trim(toolName).equalsIgnoreCase("dms")){
					String dttkey = helper.hydrateKey(defect.getEdmProjectName(),defect.getDttInstanceName());
					defect.setEdmProjectName(dttkey);
				}
				/*Added by Ashiki for Tenj210-87 ends*/

				/*Modified by paneendra for VAPT FIX starts*/
				/*String decryptedPwd = new Crypto().decrypt(instance.getPassword().split("!#!")[1],  decodeHex(instance.getPassword().split("!#!")[0].toCharArray()));*/
				String decryptedPwd = new CryptoUtilities().decrypt(instance.getPassword());
				/*Modified by paneendra for VAPT FIX ends*/
				logger.info("Getting Implementation for [{}]", toolName);
				DefectManager dm = DefectFactory.getDefectTools(toolName, instance.getURL(), instance.getAdminId(), decryptedPwd);
				/*Added by Ashiki for posting Defect starts*/ 
				dm.setProjectDesc(defect.getEdmProjectName());
				/*Added by Ashiki for posting Defect ends*/
				logger.info("Logging In to [{}]", dttInstanceName);
				dm.login();
				/*changed by manish for defect TENJINCG-119 on 14-Feb-2017 starts*/
				/*changed by manish for defect TEN-106 on 20-Dec-2016 starts*/
				helper.hydrateAttachments(this.conn,defect);
				/*changed by manish for defect TEN-106 on 20-Dec-2016 ends*/

				logger.info("Posting Defect");
				dm.postDefect(defect);
				logger.info("Defect Posted successfully with Defect Management ID --> [{}]", defect.getIdentifier());
				logger.info("Logging out");
				dm.logout();


				helper.updateDefectPostingStatus(this.conn, defect, instance.getName());
			}


		} catch (DatabaseException e) {
			
			throw new TenjinServletException(e.getMessage());
		} catch (EDMException e) {
			
			throw new TenjinServletException(e.getMessage());
		} catch (UnreachableEDMException e) {
			
			throw new TenjinServletException(e.getMessage());
		}  catch (Exception e) {
			
			throw new TenjinServletException(e.getMessage());
		}


	}

	/*changed by manish for req TENJINCG-5 on 10-Jan-17 starts*/

	public void deleteDefects(String ids) throws DatabaseException{
		//This method accepts a comma separated list of defect record IDs (1,2,3,4,5)
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		String[] dArray = ids.split(",");
		CoreDefectHelper helper = new CoreDefectHelper();
		for(String id: dArray){
			helper.deleteMultipleDefects(id,conn);
		}
		try {
			conn.close();
		} catch (SQLException e) {

		}

	}

	/*changed by manish for req TENJINCG-5 on 10-Jan-17 ends*/
	/*added by shruthi for TENJINCG-1224 starts*/
	public boolean validateDefect(int runid) 
	{
		boolean  isRunExist=false;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				) {
			PreparedStatement pst = null;
			ResultSet rs = null;

			pst = conn.prepareStatement("SELECT * FROM MASTESTRUNS WHERE RUN_ID=?");
			pst.setInt(1, runid);
			rs=pst.executeQuery();

			while(rs.next())
			{
				isRunExist=true;
			}
		}

		catch (SQLException e) {
			
			logger.error("Error ", e);
		} catch (DatabaseException e) {
			
			logger.error("Error ", e);
		}

		return isRunExist;	
	}
	/*added by shruthi for TENJINCG-1224 ends*/

	public void close(){
		try{
			this.conn.close();
		}catch(Exception e){
			logger.warn("ERROR occurred while closing DB Connection", e);
		}
	}
}
