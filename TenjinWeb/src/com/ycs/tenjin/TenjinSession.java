/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TenjinSession.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 11-01-2017            Leelaprasad            REQ#TENJINCG-11
* 19-Oct-2017			Sriram				   TENJINCG-403
* 28-08-2019			Prem			      TENJINCG-1099
* 
*/

package com.ycs.tenjin;


import java.util.List;
import java.util.Locale;

import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.user.User;

public class TenjinSession {
	private User user;
	private Project project;
	private int tenjinSessionId;
	/*Added by prem for TENJINCG-1099 starts*/
	private boolean urlSession;
	/*Added by prem for TENJINCG-1099 Ends*/
	/*Changed by Leelaprasad for the Requirement TENJINCG-11 starts*/
	private List<User> users;
	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	/*Changed by Leelaprasad for the Requirement TENJINCG-11 ends*/
	public int getTenjinSessionId() {
		return tenjinSessionId;
	}

	public void setTenjinSessionId(int tenjinSessionId) {
		this.tenjinSessionId = tenjinSessionId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
		
	}
	
	/* For TENJINCG-396 */
	private Locale locale;
	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	/* For TENJINCG-396 ends */

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
	/*Added by prem for TENJINCG-1099 starts*/
	public boolean isUrlSession() {
		return urlSession;
	}

	public void setUrlSession(boolean urlSession) {
		this.urlSession = urlSession;
	}
	/*Added by prem for TENJINCG-1099 Ends*/
	
}
