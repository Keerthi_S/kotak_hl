package com.ycs.tenjin.aws;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.exceptions.TestDataException;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.TestDataHelper;
import com.ycs.tenjin.handler.ProjectHandler;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestDataPath;
import com.ycs.tenjin.util.GenericFileUpload;

import software.amazon.awssdk.auth.credentials.SystemPropertyCredentialsProvider;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

public class FileUploadImpl implements GenericFileUpload  {

	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(FileUploadImpl.class);



	@Override
	public void uploadTestDataFile(String rootPath, TestDataPath testdatapath, String fileName, InputStream inputstream,
			String repotype,String tempawsuploaderPath) {
		TestDataHelper testdatahelper=new TestDataHelper();
		if(repotype.equalsIgnoreCase("native")) {
			logger.info("Repo type is :"+repotype);
			String folderPath=rootPath+File.separator+testdatapath.getProjectName()+File.separator+testdatapath.getAppName()+File.separator+testdatapath.getFunction()+File.separator+testdatapath.getUser();

			String status = "FAIL";
			try {

				File file=new File(folderPath);

				if(!file.exists()) {
					file.mkdirs();
					file=new File(folderPath+File.separator+fileName);
					file.createNewFile();
					byte[] bytes = IOUtils.toByteArray(inputstream);
					FileUtils.writeByteArrayToFile(file,bytes);


				}else {
					file=new File(folderPath+File.separator+fileName);
					file.createNewFile();
					byte[] bytes = IOUtils.toByteArray(inputstream);
					FileUtils.writeByteArrayToFile(file,bytes);
				}
				status = "SUCCESS";
				logger.info("File uploaded!");
			}catch(Exception e){
				logger.error("Error in File uploading!");
				logger.error("Error ", e);
			}finally {
				logger.info("Upload status: " + status);
			}
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			testdatapath.setUploadedOn(timestamp);
			try {
				boolean recordExist=new TestDataHelper().hydrateTestDataFileName(testdatapath);
				if(!recordExist) {
					logger.info("filename doesnot exists ,inserting new record");
					testdatahelper.persistTestData(testdatapath);
				}else {
					logger.info("filename already exists ,updating the record");
					testdatahelper.updateTestData(testdatapath);
				}
				//testdatahelper.persistTestData(testdatapath);
			} catch (DatabaseException e) {
				logger.error("couldnot insert testdata record!");
				logger.error("Error ", e);
			} 
		}else {
			logger.info("Repo type is :"+repotype);
			String folderPath=testdatapath.getProjectName()+"/"+testdatapath.getAppName()+"/"+testdatapath.getFunction()+"/"+
					testdatapath.getUser()+"/"+fileName;
			//String path="D://Awstest"+"//"+testdatapath.getUser();
			Region region = Region.AP_SOUTH_1;
			S3Client s3ClientBuild =null;
			try {
				/*modified by paneendra for redundant code in  building s3 client starts*/
				/*System.setProperty("aws.accessKeyId", TenjinConfiguration.getProperty("ACCESS_KEY_ID"));
				System.setProperty("aws.secretAccessKey", TenjinConfiguration.getProperty("ACCESS_SEC_KEY"));

				S3Client s3 = S3Client.builder()
						.region(region)
						.credentialsProvider(SystemPropertyCredentialsProvider.create())
						.build();*/
				s3ClientBuild=this.buildS3Client();
				/*modified by paneendra for redundant code in  building s3 client ends*/
				File file=new File(tempawsuploaderPath); 

				if(!file.exists()) {
					file.mkdirs();
					file=new File(tempawsuploaderPath+File.separator+fileName);
					file.createNewFile();
					byte[] bytes = IOUtils.toByteArray(inputstream);
					FileUtils.writeByteArrayToFile(file,bytes);


				}else {
					file=new File(tempawsuploaderPath+File.separator+fileName);

					file.createNewFile();
					byte[] bytes = IOUtils.toByteArray(inputstream);
					FileUtils.writeByteArrayToFile(file,bytes);
				}

				PutObjectRequest request = PutObjectRequest.builder().bucket(rootPath).key(folderPath).build();
				s3ClientBuild.putObject(request, new File(tempawsuploaderPath+File.separator+fileName).toPath());
				logger.info("File uploaded!");

				try {
					Path locpath = Paths.get(tempawsuploaderPath).getParent(); 
					File delfile= locpath.toFile();
					FileUtils.deleteDirectory(delfile); 
					logger.info("deleted the temporary folder");
				}catch(Exception tempdel) {
					logger.error("Error in deleteing the temporary folder"+tempdel);
				}
			}catch (Exception e) {
				logger.error("Error in File uploading!");
				logger.error("Error ", e);
			}
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			testdatapath.setUploadedOn(timestamp);
			try {
				boolean recordExist=new TestDataHelper().hydrateTestDataFileName(testdatapath);
				if(!recordExist) {
					logger.info("filename doesnot exists ,inserting new record");
					testdatahelper.persistTestData(testdatapath);
				}else {
					logger.info("filename already exists ,updating the record");
					testdatahelper.updateTestData(testdatapath);
				}
				//testdatahelper.persistTestData(testdatapath);
			} catch (DatabaseException e) {
				logger.error("couldnot insert testdata record!");
				logger.error("Error ", e);
			}
		}

	}
	/*modified by shruthi for Tenj212-15 starts*/
	public File downloadTestDataFileFromS3(String reporoot,String awsPath, String folderPath) {
		File file = null;
		S3Client s3ClientBuild =null;
		try {
			/*modified by paneendra for redundant code in  building s3 client starts*/
			/*Region region = Region.AP_SOUTH_1;
			System.setProperty("aws.accessKeyId", TenjinConfiguration.getProperty("ACCESS_KEY_ID"));
			System.setProperty("aws.secretAccessKey", TenjinConfiguration.getProperty("ACCESS_SEC_KEY"));
			S3Client s3 = S3Client.builder().region(region).credentialsProvider(SystemPropertyCredentialsProvider.create()).build();*/
			s3ClientBuild=this.buildS3Client();
			/*modified by paneendra for redundant code in  building s3 client ends*/
			GetObjectRequest request1 = GetObjectRequest.builder().bucket(reporoot).key(awsPath).build();
			file=new File(folderPath);
			ResponseInputStream res = s3ClientBuild.getObject(request1);
			byte[] bytes = IOUtils.toByteArray(res);
			FileUtils.writeByteArrayToFile(file, bytes);
		} catch (Exception e) {
			logger.error("Error ", e);
		}

		return file;
	}
	/*modified by shruthi for Tenj212-15 starts*/

	public void downloadTestDataFile(String rootpath,String folderPath, String fileName)
			throws IOException, TestDataException {
		try {

			File file = new File(folderPath);

			if (!file.exists()) {
				file.mkdirs();
				file = new File(folderPath + File.separator + fileName);
				file.createNewFile();
				BufferedInputStream inputstream = new BufferedInputStream(new URL(rootpath).openStream());
				byte[] bytes = IOUtils.toByteArray(inputstream);
				FileUtils.writeByteArrayToFile(file, bytes);

			} else {
				file = new File(folderPath + File.separator + fileName);
				file.createNewFile();
				BufferedInputStream inputstream = new BufferedInputStream(new URL(rootpath).openStream());
				byte[] bytes = IOUtils.toByteArray(inputstream);
				FileUtils.writeByteArrayToFile(file, bytes);
			}
			/*Added by Ashiki for Tenj212-23 starts*/
		} catch(FileNotFoundException e) {
			throw new TestDataException("Test data template not available in the path");
			}/*Added by Ashiki for Tenj212-23 ends*/
		catch (Exception e) {
			logger.error("Error in File dowloading!");
			logger.error("Error ", e);
		}

	}

	/*Added by paneendra for  building s3 client starts*/
	public S3Client buildS3Client() {

		Region region = Region.AP_SOUTH_1;
		S3Client buildClient=null;
		try {
			ProjectHandler  projectHandler = new ProjectHandler();
			Project project = new Project();
		   project = projectHandler.hydrateAwsDeatils();
			System.setProperty("aws.accessKeyId", project.getAccessKey());
			System.setProperty("aws.secretAccessKey", project.getAccessSecKey());

			buildClient = S3Client.builder()
					.region(region)
					.credentialsProvider(SystemPropertyCredentialsProvider.create())
					.build();
		}catch(Exception e) {
			logger.error("Error in building s3 client"+e);
		}

		return buildClient;

	}
	public S3Client buildS3Client(String accessKey, String accessSecKey) throws DatabaseException, TenjinConfigurationException {
		Region region = Region.AP_SOUTH_1;
		S3Client buildClient=null;
		{  
			Project project = new Project();
			 project.setAccessKey(accessKey);
			 project.setAccessSecKey(accessSecKey);
			try {
				
				System.setProperty("aws.accessKeyId", project.getAccessKey());
				System.setProperty("aws.secretAccessKey", project.getAccessSecKey());
				
				
			
				
			
			buildClient = S3Client.builder()
					.region(region)
					.credentialsProvider(SystemPropertyCredentialsProvider.create())
					.build();
			
		}catch(Exception e) {
			logger.error("Error in building s3 client"+e);
		}
			
		
		return buildClient;
	}

	/*Added by paneendra for  building s3 client ends*/


	}
}
