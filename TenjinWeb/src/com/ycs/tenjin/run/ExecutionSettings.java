/**
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutionSettings.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source fi le is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India



/*CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
*24-10-2018             Ramya                  Newly Added For TENJINCG-832
*26-10-2018             Ramya                  for TENJINCG-832
*07-12-2018				Pushpa				   TENJINCG-909
* 
* */

package com.ycs.tenjin.run;

public class ExecutionSettings {

	private int runId;
	private String client;
	private String browser;
	
	private String scrnShotOption;
	public int getRunId() {
		return runId;
	}
	public void setRunId(int runId) {
		this.runId = runId;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public String getScrnShotOption() {
		return scrnShotOption;
	}
	public void setScrnShotOption(String scrnShotOption) {
		this.scrnShotOption = scrnShotOption;
	}
	
}
