/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestRunSearchRequest.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 22-10-2018		Sriram Sridharan		Added for TENJINCG-866
* 12-12-2018        Padmavathi              for TJNUN262-98
* 13-12-2018	    Padmavathi				for TJNUN262-97
* 20-03-2019        Padmavathi              TENJINCG-1010 
*/

package com.ycs.tenjin.run;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.util.Utilities;

public class TestRunSearchRequest {
	private int runId;
	private String testCaseId;
	private String testStepId;
	private int projectId;
	private int appId;
	private String functionCode;
	private String userId;
	private String date;
	private Date runStartDate;
	private String status;
	
	public String getTestCaseId() {
		return testCaseId;
	}
	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}
	public String getTestStepId() {
		return testStepId;
	}
	public void setTestStepId(String testStepId) {
		this.testStepId = testStepId;
	}
	public int getRunId() {
		return runId;
	}
	public void setRunId(int runId) {
		this.runId = runId;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	public String getFunctionCode() {
		return functionCode;
	}
	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Date getRunStartDate() {
		return runStartDate;
	}
	public void setRunStartDate(Date runStartDate) {
		this.runStartDate = runStartDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	/* Modified by Padmavathi for TENJINCG-1010 starts */
	/*private String baseQuery = "SELECT A.RUN_ID, A.RUN_PRJ_ID, A.RUN_TASK_TYPE, A.RUN_STATUS AS RUN_COMPLETION_STATUS, A.RUN_START_TIME,A.RUN_END_TIME,A.RUN_USER,A.RUN_TS_REC_ID,A.RUN_MACHINE_IP,A.RUN_CL_PORT,A.RUN_BROWSER_TYPE,A.RUN_PRJ_NAME,A.PARENT_RUN_ID,A.RUN_DOMAIN_NAME,A.RUN_DEVICE_REC_ID, B.TOTAL_TCS,B.TOTAL_EXE_TCS,B.TOTAL_PASSED_TCS,B.TOTAL_FAILED_TCS,B.TOTAL_ERROR_TCS,B.RUN_STATUS,C.TS_NAME FROM MASTESTRUNS A, V_RUNRESULTS B,TESTSETS C WHERE C.TS_REC_ID=A.RUN_TS_REC_ID AND B.RUN_ID = A.RUN_ID";*/
	private String baseQuery = "SELECT A.RUN_ID, A.RUN_PRJ_ID, A.RUN_TASK_TYPE, A.RUN_STATUS AS RUN_COMPLETION_STATUS, A.RUN_START_TIME,A.RUN_END_TIME,A.RUN_USER,A.RUN_TS_REC_ID,A.RUN_MACHINE_IP,A.RUN_CL_PORT,A.RUN_BROWSER_TYPE,A.RUN_PRJ_NAME,A.PARENT_RUN_ID,A.RUN_DOMAIN_NAME,A.RUN_DEVICE_REC_ID, B.TOTAL_TCS,B.TOTAL_EXE_TCS,B.TOTAL_PASSED_TCS,B.TOTAL_FAILED_TCS,B.TOTAL_ERROR_TCS,B.RUN_STATUS,C.TS_NAME,C.TS_REC_TYPE FROM MASTESTRUNS A, V_RUNRESULTS B,TESTSETS C WHERE C.TS_REC_ID=A.RUN_TS_REC_ID AND B.RUN_ID = A.RUN_ID";
	/* Modified by Padmavathi for TENJINCG-1010 ends */
	public String buildQuery(String dbVendor) {
		String finalQuery = "";
		
		
		if(this.runId > 0) {
			return this.baseQuery + " AND A.RUN_ID=" + this.runId;
		}
		
		
		if(projectId  > 0){
			/* Modified by Padmavathi for TENJINCG-1010 starts */
			finalQuery = "SELECT * FROM ("
					+ "SELECT mv.*, m.RUN_MACHINE_IP,m.RUN_DEVICE_REC_ID,m.RUN_DOMAIN_NAME,m.RUN_PRJ_NAME,m.PARENT_RUN_ID,m.RUN_PRJ_ID,m.RUN_CL_PORT,m.RUN_BROWSER_TYPE,m.RUN_TASK_TYPE,m.RUN_START_TIME, m.RUN_END_TIME,m.RUN_USER,m.RUN_TS_REC_ID,ts.ts_name,ts.TS_REC_TYPE"
					+ " ,ROW_NUMBER() OVER (PARTITION BY MV.RUN_ID ORDER BY MV.RUN_ID) ROW_NUM"
					+ "	FROM Mastestruns m,v_runresults mv, v_runresults_tc tc,Testcases t,v_runresults_ts tsr,teststeps tsp, testsets ts"
					+ "	WHERE mv.run_id=m.run_id and m.run_id = tc.run_id"
					+ "	AND tc.tc_rec_id = t.tc_rec_id"
					+ " AND m.run_id=tsr.run_id"
					+ "	AND tsr.TSTEP_REC_ID = tsp.TSTEP_REC_ID"
					+ " AND ts.TS_REC_ID=m.run_ts_rec_id"
					+ " AND m.RUN_PRJ_ID=" + projectId + " and(";
		}else{
			finalQuery="SELECT mv.*, m.RUN_MACHINE_IP,m.RUN_DEVICE_REC_ID,m.RUN_DOMAIN_NAME,m.RUN_PRJ_NAME,m.PARENT_RUN_ID,m.RUN_PRJ_ID,m.RUN_CL_PORT,m.RUN_BROWSER_TYPE,m.RUN_TASK_TYPE,m.RUN_START_TIME, m.RUN_END_TIME,m.RUN_USER,m.RUN_TS_REC_ID,ts.ts_name,ts.TS_REC_TYPE FROM Mastestruns m,v_runresults mv, v_runresults_tc tc,Testcases t,v_runresults_ts tsr,teststeps tsp,testsets ts	WHERE mv.run_id=m.run_id and m.run_id = tc.run_id	AND tc.tc_rec_id = t.tc_rec_id AND ts.TS_REC_ID=m.run_ts_rec_id AND m.run_id=tsr.run_id	AND tsr.TSTEP_REC_ID = tsp.TSTEP_REC_ID and(";
		}
		/* Modified by Padmavathi for TENJINCG-1010 ends */
		boolean criteriaSpecified=false;
		
		if(userId!=null && userId != ""){
			finalQuery = finalQuery+"m.run_user = '"+userId+"' AND ";
			criteriaSpecified =true;
		}
					
		
		if(functionCode!=null && functionCode != ""){
			finalQuery = finalQuery+"tsp.FUNC_CODE ='"+functionCode+"' AND ";
			criteriaSpecified =true;
		}
		
		
		if(status!=null && status != ""){
			finalQuery = finalQuery + "mv.run_status ='" + status + "' AND ";
			criteriaSpecified =true;
		}
		
		if(Utilities.trim(testCaseId).length() > 0){
			finalQuery = finalQuery+"tc_id ='"+testCaseId+"' AND ";
			criteriaSpecified =true;
		}
		
		if(testStepId!=null && testStepId != ""){
			finalQuery = finalQuery+"tsp.TSTEP_ID = '"+testStepId+"' AND ";
			criteriaSpecified =true;
		}
		
		if(appId > 0){
			finalQuery = finalQuery+"tsp.app_id ="+appId+" AND ";
			criteriaSpecified =true;
		}
		
		if(date!=null && date != ""){
			if("Oracle".equalsIgnoreCase(dbVendor))
				finalQuery = finalQuery+"to_date(m.RUN_START_TIME,'DD-MM-RR') ='"+date+"' AND ";
			else
				finalQuery = finalQuery+"REPLACE(CONVERT(VARCHAR(11),m.RUN_START_TIME,106),' ','-') ='"+date+"' AND ";
			
			criteriaSpecified =true;
		}
		
		if(criteriaSpecified){
		 int index=finalQuery.lastIndexOf(" ");
		 finalQuery=finalQuery.substring(0,index-3);

			finalQuery = finalQuery+")";
		}else{
			finalQuery = finalQuery+")";
			finalQuery = finalQuery.replace("and()","");
		}
		if("Oracle".equalsIgnoreCase(dbVendor))
			/*Modified by Padmavathi for TJNUN262-97 starts*/
			/*finalQuery = finalQuery+") WHERE ROW_NUM=1 ORDER BY RUN_ID DESC";*/
			finalQuery = finalQuery+") WHERE ROW_NUM=1";
		else
			/*finalQuery = finalQuery+") AS FILTER_RUNS WHERE ROW_NUM=1 ORDER BY RUN_ID DESC";*/
			finalQuery = finalQuery+") AS FILTER_RUNS WHERE ROW_NUM=1";
		/*Modified by Padmavathi for TJNUN262-97 ends*/
		
		
		
		
		
		return finalQuery;
	}
	
	private static final Logger logger = LoggerFactory.getLogger(TestRunSearchRequest.class);
	
	public static TestRunSearchRequest buildFromRequest(HttpServletRequest request) {
		String runId = Utilities.trim(request.getParameter("runid"));
		String startedBy = Utilities.trim(request.getParameter("startedBy"));
		String startedOn = Utilities.trim(request.getParameter("startedOn"));
		String testCaseId = Utilities.trim(request.getParameter("testCase"));
		String application = Utilities.trim(request.getParameter("app"));
		String result= Utilities.trim(request.getParameter("result"));
		String testStep = Utilities.trim(request.getParameter("testStep"));
		String function = Utilities.trim(request.getParameter("function"));
		
		
		try {
			TestRunSearchRequest req = new TestRunSearchRequest();
			req.setRunId(Utilities.isNumeric(runId) ? Integer.parseInt(runId) : 0);
			req.setUserId(startedBy);
			/*req.setRunStartDate(startedOn.length() > 0 ? );*/
			/*Added by Padmavathi for TJNUN262-98 starts*/
			req.setDate(startedOn);
			/*Added by Padmavathi for TJNUN262-98 ends*/
			req.setTestCaseId(testCaseId);
			req.setTestStepId(testStep);
			req.setAppId(Utilities.isNumeric(application) ? Integer.parseInt(application) : 0);
			req.setStatus(result);
			req.setFunctionCode(function);
			
			return req;
		} catch (Exception e) {
			logger.error("Error building search request", e);
			return null;
		}
	}
	
}
