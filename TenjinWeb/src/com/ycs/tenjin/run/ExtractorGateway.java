/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExtractorGateway.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 08-Nov-2016          	Sriram Sridharan        Newly Added For Data Extraction in Tenjin 2.4.1
* 13-Dec-2016          	Leelaprasad             Defect fix#TEN-28
* 22-Sep-2017			sameer gupta			For new adapter spec
* 16-03-2018			Preeti 					TENJINCG-73
* 04-07-2018            Padmavathi              T251IT-177
* 02-01-2019			Prem					For validating adapter license
* 24-06-2019            Padmavathi              for license
* 03-12-2019			Roshni					TENJINCG-1168
*/


package com.ycs.tenjin.run;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.MaxActiveUsersException;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.exceptions.TestDataException;
import com.ycs.tenjin.bridge.oem.gui.datahandler.GuiDataHandlerImpl;
import com.ycs.tenjin.bridge.pojo.TaskManifest;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ExtractionRecord;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.AutsHelper;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.handler.LicenseHandler;
import com.ycs.tenjin.license.License;
import com.ycs.tenjin.license.LicenseComponent;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

public class ExtractorGateway {
	private static final Logger logger = LoggerFactory.getLogger(ExtractorGateway.class);
	
	private Connection conn;
	private int appId;
	private Map<String, String> functionMap;
	private String browserType;
	private String clientName;
	private String autLoginType;
	private String user;
	//private String automationEngine;
	
	private Aut aut;
	private List<Module> functions;
	private RegisteredClient client;
	private TestRun testRun;
	private TaskManifest manifest;
	private List<ExtractionRecord> extractionRecords;
/*Added by Prem for validating adapter license start*/
/* Uncommented by Padmavathi for license starts */
	private String adapter;
	/* Uncommented by Padmavathi for license ends */
/*Added by Prem for validating adapter license End*/
	public ExtractorGateway(int appId, Map<String, String> functionMap, String user) throws TenjinServletException{
		this.appId = appId;
		this.functionMap = functionMap;
		this.user = user;
		this.extractionRecords = new ArrayList<ExtractionRecord>();
		try {
			this.conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		} catch (DatabaseException e) {
			
			throw new TenjinServletException("Could not connect to Tenjin DB. Please contact Tenjin Support");
		}
	}
	/*changed by nagababu:Starts  */
	public ExtractorGateway() throws TenjinServletException{
		try {
			this.conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		} catch (DatabaseException e) {
			
			throw new TenjinServletException("Could not connect to Tenjin DB. Please contact Tenjin Support");
		}
	}
	/*changed by nagababu:Ends  */
	public TestRun getTestRun() {
		return testRun;
	}



	public void setTestRun(TestRun testRun) {
		this.testRun = testRun;
	}



	public TaskManifest getManifest() {
		return manifest;
	}



	public void setManifest(TaskManifest manifest) {
		this.manifest = manifest;
	}



	public String getBrowserType() {
		return browserType;
	}

	public void setBrowserType(String browserType) {
		this.browserType = browserType;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getAutLoginType() {
		return autLoginType;
	}

	public void setAutLoginType(String autLoginType) {
		this.autLoginType = autLoginType;
	}

	public Aut getAut() {
		return aut;
	}

	public List<Module> getFunctions() {
		return functions;
	}
	
	public void performInitialValidations() throws TenjinServletException{
		/************************************************************************
		 *commented by sameer for New Adapter Spec
		 */
		//this.validateAutomationEngine();
		this.validateAut();
		this.validateFunctionsAndInputFiles();
	}
	
	public void performFinalValidations() throws TenjinServletException{
		this.validateClient();
		this.validateUserAutCredentials();
	}

	public void performAllValidations() throws TenjinServletException{
		this.performInitialValidations();
		this.performFinalValidations();
	}
	
	
	
	private void validateAut() throws TenjinServletException{
		
		
		logger.info("Validating AUT Information");
		
		try {
			/*Modified by Preeti for TENJINCG-73 starts*/
			/*this.aut = new AutHelper().hydrateAut(this.appId);*/
			/* modified by Roshni for TENJINCG-1168 starts */
			/*this.aut = new AutsHelper().hydrateAut(this.appId);*/
			this.aut = new AutHandler().getApplication(this.appId);
			/* modified by Roshni for TENJINCG-1168 ends */
			/*Modified by Preeti for TENJINCG-73 ends*/
			if(this.aut == null){
				logger.error("AUT is null");
				throw new TenjinServletException("Could not verify Application Information. Please contact Tenjin Support.");
			}
		} catch (DatabaseException e) {
			
			logger.error("ERROR fetching details of application with ID [{}]", this.appId, e);
			throw new TenjinServletException("Could not verify Application Information. Please contact Tenjin Support.");
		} catch(TenjinServletException e){
			throw e;
		}
		
		logger.info("Application [{}] validated.", this.aut.getName());
	}
	
	private void validateFunctionsAndInputFiles() throws TenjinServletException{
		logger.info("Validating Functions to Learn");
		
		this.functions = new ArrayList<Module>();
		ModulesHelper helper = new ModulesHelper();
		String message = "";
		for(String functionCode:this.functionMap.keySet()){
			try {
				Module module = helper.hydrateModuleInfo(this.aut.getId(), functionCode);
				this.extractionRecords = new ArrayList<ExtractionRecord>();
				if(module == null){
					logger.error("Invalid function code [{}]", functionCode);
					message = "The function [" + functionCode + "] is Invalid. Please review your selection and try again";
					break;
				}
				
				String inputFilePath = this.functionMap.get(functionCode);
				File file = new File(inputFilePath);
				if(!file.exists()){
					logger.error("Could not find input file for function [{}] at path [{}]", functionCode, inputFilePath);
					message = "There was a problem accessing the input file for function [" + functionCode +  "]. Please contact Tenjin Support.";
					break;
				}else{
					module.setExtractorInputFile(inputFilePath);
				}
				
				List<String> eManifest = new GuiDataHandlerImpl().getDataExtractionInputRecordIDs(inputFilePath, this.aut.getId(), functionCode);
				if(eManifest != null){
					Map<Integer, String> headerMap = new HashMap<Integer, String>();
					String header = eManifest.get(0);
					String[] hSplit = header.split(";;");
					if(hSplit.length < 3){
						logger.error("Invalid input file. Manifest does not have required number of values");
						logger.error("Header Manifest --> [{}]", header);
						throw new TenjinServletException("Could not parse input file. Please contact Tenjin Support.");
					}
					
					for(int i=3; i<hSplit.length; i++){
						headerMap.put(i, hSplit[i]);
					}
					
					for(int i=1; i<eManifest.size(); i++){
						String m = eManifest.get(i);
						ExtractionRecord record = new ExtractionRecord();
						
						String[] mSplit = m.split(";;");
						
						int rowNum=0;
						String tdGid = "";
						String tdUid = "";
						
						if(mSplit.length < 3){
							logger.error("Invalid input file. Manifest does not have required number of values");
							logger.error("Manifest --> [{}]", header);
							throw new TenjinServletException("Could not parse input file. Please contact Tenjin Support.");
						}
						
						rowNum = Integer.parseInt(mSplit[0]);
						tdGid = mSplit[1];
						tdUid = mSplit[2];
						
						Map<String, String> qMap = new HashMap<String, String>();
						for(int j=3; j<mSplit.length; j++){
							qMap.put(headerMap.get(j), mSplit[j]);
						}
						
						record.setQueryFields(qMap);
						record.setTdUid(tdUid);
						record.setSpreadsheetRowNum(rowNum);
						record.setTdGid(tdGid);
						
						this.extractionRecords.add(record);
					}
					
				}
				
				module.setExtractionRecords(this.extractionRecords);
				this.functions.add(module);
			} catch (DatabaseException e) {
				
				logger.error("ERROR while validating function [{}]", functionCode, e);
				message = "Could not validate function [" + functionCode + "]. Please contact Tenjin support.";
				break;
			} catch (TestDataException e) {
				
				logger.error("ERROR while validating extraction input file for function [{}]", functionCode, e);
				message = "Could not validate input file for function [" + functionCode + "]. Please contact Tenjin support.";
				break;
			}
		}
		
		if(message.length() > 0){
			throw new TenjinServletException(message);
		}
		
		
		logger.info("All functions validated successfully");
	}
	
	private void validateUserAutCredentials() throws TenjinServletException{
		logger.info("Validating User-AUT Credentials");
		/*Added by Padmavathi for T251IT-177 starts*/
		String autName=null;
		/*Added by Padmavathi for T251IT-177 ends*/
		try {
			/*Modified by Preeti for TENJINCG-73 starts*/
			/*Added by Padmavathi for T251IT-177 starts*/
			autName=this.aut.getName();
			/*Added by Padmavathi for T251IT-177 ends*/
			/*this.aut = new AutHelper().hydrateAutForDriverLaunch(this.conn, this.aut.getName(), this.user, this.autLoginType);*/
			this.aut = new AutsHelper().hydrateAutForDriverLaunch(this.conn, this.aut.getName(), this.user, this.autLoginType);
			/*Modified by Preeti for TENJINCG-73 ends*/
			if(this.aut == null){
				logger.error("Aut User credentials in valid");
				/*Modified by Padmavathi for T251IT-177 starts*/
				/*throw new TenjinServletException("The credentials you have specified for " + this.aut.getName() + " are invalid. Please provide valid credentials.");*/
				throw new TenjinServletException("The credentials you have specified for " + autName + " are invalid. Please provide valid credentials.");
				/*Modified by Padmavathi for T251IT-177 ends*/
			}
			
			logger.debug("Decrypting password");
			/*Modified by paneendra for VAPT FIX starts*/
			/*String dPass = new Crypto().decrypt(this.aut.getPassword().split("!#!")[1],
					  decodeHex(this.aut.getPassword().split("!#!")[0].toCharArray()));*/
			String dPass = new CryptoUtilities().decrypt(this.aut.getPassword());
			/*Modified by paneendra for VAPT FIX ends*/
			this.aut.setPassword(dPass);
		} catch (DatabaseException e) {
			
			/*Modified by Padmavathi for T251IT-177 starts*/
			/*logger.error("ERROR validating user aut credentials for [{}]", this.aut.getName(), e);
			throw new TenjinServletException("Could not validate credentials for [" + this.aut.getName() + "]. Please contact Tenjin Support");*/
			logger.error("ERROR validating user aut credentials for [{}]", autName, e);
			throw new TenjinServletException("Could not validate credentials for [" + autName + "]. Please contact Tenjin Support");
			/*Modified by Padmavathi for T251IT-177 ends*/
		} catch(Exception e){
			/*Modified by Padmavathi for T251IT-177 starts*/
			/*logger.error("ERROR decrypting AUT login password", this.aut.getName(), e);
			throw new TenjinServletException("Could not validate credentials for [" + this.aut.getName() + "]. Please contact Tenjin Support");*/
			logger.error("ERROR decrypting AUT login password",autName, e);
			throw new TenjinServletException("Could not validate credentials for [" + autName + "]. Please contact Tenjin Support");
			/*Modified by Padmavathi for T251IT-177 ends*/
		}
		
		logger.info("Credentials validated successfully");
		
		if(this.functions != null && this.functions.size() >0){
			for(Module module:this.functions){
				module.setAutUserType(this.autLoginType);
			}
		}
	}
	
	private void validateClient() throws TenjinServletException{
		logger.info("Validating Client Information");
		
		
		try{
			this.client = new ClientHelper().hydrateClient(this.clientName);
			
			if(this.client == null){
				logger.error("Invalid Client [{}]", this.clientName);
				throw new TenjinServletException("The client [" + this.clientName + "] is invalid. Please choose a different client and try again.");
			}
			
		} catch(DatabaseException e){
			logger.error(e.getMessage());
			throw new TenjinServletException("Could not validate client information. Please contact Tenjin Support");
		}
		
		logger.info("Client information validated successfully");
	}
	
	public void createRun() throws TenjinServletException {
		
		logger.info("Creating a new Run");
		this.testRun = new TestRun();
		this.testRun.setTaskType(BridgeProcess.EXTRACT);
		this.testRun.setUser(this.user);
		this.testRun.setStartTimeStamp(new Timestamp(new Date().getTime()));
		this.testRun.setMachine_ip(this.client.getHostName());
		this.testRun.setTargetPort(Integer.parseInt(this.client.getPort()));
		this.testRun.setBrowser_type(this.browserType);
		this.testRun.setAppId(this.appId);
		
		logger.info("Creating Task Manifest");
		this.manifest = new TaskManifest();
		this.manifest.setAut(this.aut);
		this.manifest.setFunctions(this.functions);
		if(Utilities.trim(this.browserType).equalsIgnoreCase("APPDEFAULT")){
			this.manifest.setBrowser(this.aut.getBrowser());
		}else{
			this.manifest.setBrowser(this.browserType);
		}
		this.manifest.setClientIp(this.client.getHostName());
		this.manifest.setClientPort(Integer.parseInt(this.client.getPort()));
		/************************************************************************
		 *commented by sameer for New Adapter Spec
		 */
		//this.manifest.setAutomationEngine(this.automationEngine);
		this.manifest.setTaskType(BridgeProcess.EXTRACT);
		
		/* Uncommented by Padmavathi for license starts */
		this.manifest.setAdapterName(this.adapter);
		/* Uncommented by Padmavathi for license ends */
		/*Added by Prem for validating adapter license End	*/	
		logger.info("Persisting new Run");
		try {
			new RunHelper().persistNewRun(this.conn, this.testRun, this.manifest);
			this.manifest.setRunId(this.testRun.getId());
		} catch (DatabaseException e) {
			
			logger.error("ERROR creating new run");
			throw new TenjinServletException("Could not initiate Extraction Run due to an internal error. Please contact Tenjin Support.");
		}
		
		try{
			this.conn.close();
		}catch(Exception ignore){}
		
	}
	/*Changed by Nagababu:Starts*/
	public void loadRun(int runId, String clientName,String browserType) throws TenjinServletException{
		
		logger.info("Fetching details of run [{}]", runId);
		try {
			this.testRun = new RunHelper().hydrateRun(this.conn, runId);
		} catch (DatabaseException e) {
			
			throw new TenjinServletException(e.getMessage());
		}
		
		if(this.testRun.getFunctions() == null || this.testRun.getFunctions().size() < 1){
			throw new TenjinServletException("No functions were found to extract as part of this run. Please review your job. Contact Tenjin Support for further assistance.");
		}
		
		logger.info("Getting Application ID");
		this.appId = this.testRun.getAppId();
		
		logger.info("Getting all function codes");
		//this.functions=new ArrayList<Module>();
		this.functionMap=new HashMap<String,String>();
		//this.functionCodes = new ArrayList<String>();
		for(Module module: this.testRun.getFunctions()){
			//this.functions.add(module);
			this.functionMap.put(module.getCode(), module.getExtractorInputFile());
			this.autLoginType = module.getAutUserType();
		}
		
		logger.info("Getting AUT Login Type --> [{}]", this.autLoginType);
		logger.info("Target Machine --> [{}], Target Port --> [{}]", this.testRun.getMachine_ip(), this.testRun.getTargetPort());
		if(Utilities.trim(this.user).equalsIgnoreCase("")){
			this.user = this.testRun.getUser();
		}else{
			this.testRun.setUser(this.user);
		}
		logger.info("User --> [{}]", this.user);
		this.clientName = clientName;
		this.browserType=browserType;
		
		this.performAllValidations();
		
		for(Module function:this.functions){
			for(Module rFunction:this.testRun.getFunctions()){
				if(rFunction.getCode().equalsIgnoreCase(function.getCode())){
					function.setLastSuccessfulLearningResult(rFunction.getLastSuccessfulLearningResult());
					break;
				}
			}
		}
		this.testRun.setMachine_ip(this.client.getHostName());
		try {
			this.testRun.setTargetPort(Integer.parseInt(this.client.getPort()));
		} catch (NumberFormatException e) {
			
			logger.error("Invalid Port specified --> [{}]", this.client.getPort());
			logger.warn("Default port 4444 will be used");
			this.testRun.setTargetPort(4444);
		}
		
		logger.info("Updating run information");
		this.testRun.setStartTimeStamp(new Timestamp(new Date().getTime()));
		try{
			new RunHelper().updateRunInfo(this.conn, this.testRun.getId(), this.testRun.getStartTimeStamp().getTime(), this.testRun.getBrowser_type(), this.testRun.getMachine_ip(), this.testRun.getTargetPort(), this.testRun.getUser());
		}catch(Exception e){
			logger.error("ERROR updating Run Information", e);
			throw new TenjinServletException("Could not load run due to an internal error. Please contact Tenjin Support.");
		}
		
		logger.info("Creating Manifest");
		this.manifest = new TaskManifest();
		this.manifest.setAut(this.aut);
		this.manifest.setFunctions(this.functions);
		if(Utilities.trim(this.testRun.getBrowser_type()).equalsIgnoreCase("APPDEFAULT")){
			this.manifest.setBrowser(this.aut.getBrowser());
		}else{
			this.manifest.setBrowser(this.testRun.getBrowser_type());
		}
		if(this.manifest.getBrowser()==null){
			this.manifest.setBrowser(this.browserType);
		}
		this.manifest.setClientIp(this.client.getHostName());
		this.manifest.setClientPort(Integer.parseInt(this.client.getPort()));
		/************************************************************************
		 *commented by sameer for New Adapter Spec
		 */
		//this.manifest.setAutomationEngine(this.automationEngine);
		this.manifest.setTaskType(BridgeProcess.EXTRACT);
		this.manifest.setRunId(this.testRun.getId());
		
		
		logger.info("Run [{}] loaded successfully", runId);
		
		try{
			this.conn.close();
		}catch(Exception ignore){}
	}
	/*Changed by Nagababu:Ends*/
	/* Changed by Leelaprasad for the Requirement of defect fix#TEN-28 on 13-12-2016 starts*/
	public Boolean validateExcelSheet(String filePath){
		  Workbook workbook=null;
		  Boolean validate=true;
		try {
		    workbook = WorkbookFactory.create(new File(filePath));
			Sheet sheet = workbook.getSheetAt(0);
			  Row header = sheet.getRow(4);
			
			if(header==null){
				validate=false;
				
			}else{
				validate=true;
			}
			
		} catch (InvalidFormatException | IOException e) {
			
			logger.error("Error ", e);
		}
		return validate;
		
	}
	/* Changed by Leelaprasad for the Requirement of defect fix#TEN-28 on 13-12-2016 ends*/
	/*Added by Padmavathi for license starts*/
	public void validateAdapterLicense() throws MaxActiveUsersException, LicenseInactive{
		try {
			int limit = 0;
			int count = 0;
			List<String> adpternameLice = new ArrayList<>();
			Map<String, String> map = new ConcurrentHashMap<String, String>();
			Map<String, Integer> countMap = new ConcurrentHashMap<String, Integer>();	
			License license = (License) CacheUtils.getObjectFromRunCache("licensedetails");	
			for (LicenseComponent licenseComponent :license.getLicenseComponents()) {
				String adapterName=licenseComponent.getProductComponent().getName();
					if (adapterName.equalsIgnoreCase(this.aut.getAdapterPackage())) {
						new LicenseHandler().licenseComponentActiveValidation(licenseComponent);
						CacheUtils.initializeRunCache();
						try {
							if (CacheUtils.getObjectFromRunCache(this.aut.getAdapterPackage()).equals(null)) {
							} else {
								count = (int) CacheUtils.getObjectFromRunCache(this.aut.getAdapterPackage());
								limit = licenseComponent.getLimit();
								if (count <= limit) {
									CacheUtils.putObjectInRunCache(this.aut.getAdapterPackage(), count + 1);
								}
							}
							if (count > limit) {
								logger.error("Max number of Tenjin User for Application {}", this.aut.getAdapterPackage());
							}
							adpternameLice.add(adapterName);
							countMap.put(adapterName, limit);
						} catch (NullPointerException e) {
							count = 1;
							CacheUtils.putObjectInRunCache(this.aut.getAdapterPackage(), count);
							adpternameLice.add(adapterName);
							limit = licenseComponent.getLimit();
							countMap.put(adapterName, limit);
							if (count > limit) {
								logger.error("Max numeber of Tenjin User for Application {}", this.aut.getAdapterPackage());
								map.put(this.aut.getAdapterPackage(), "You have reached the maximum limit for ["+this.aut.getName()+"] application");
							}
						}
						this.adapter = adpternameLice.toString();
					} else {
						logger.error("No adapter License availabel{} ", this.aut.getAdapterPackage());
						map.put(adapterName, "You need a license for ["+adapterName+"] application to perform this operation. Please contact Tenjin Support.");
					}
			}
			List<String> st = adpternameLice;
			if(adpternameLice.isEmpty()) {
				logger.error("No adapter License availabel for {}",this.aut.getAdapterPackage());
				throw new MaxActiveUsersException("You need a license for ["+ this.aut.getName()+"] application to perform this operation. Please contact Tenjin Support.");
			}
			for (String s : st) {
				count = (int) CacheUtils.getObjectFromRunCache(s);
				map.containsKey(s);
				if (countMap.containsKey(s)) {
					map.remove(s);
					Integer se = countMap.get(s);
					if (count > se) {
						logger.error("Max numeber of Tenjin User for {}",this.aut.getAdapterPackage());
						CacheUtils.putObjectInRunCache(s, count - 1);
						throw new MaxActiveUsersException("You have reached the maximum limit for ["+this.aut.getName()+"] application.");
					}
				} 
			}
		}
		catch (TenjinConfigurationException e) {
			
			logger.error("Error ", e);
		}
	}
	/*Added by Padmavathi for license ends*/
}
