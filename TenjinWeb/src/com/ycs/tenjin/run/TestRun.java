/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestRun.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 21-Sep-2016           Sriram Sridharan        Changed by Sriram for Req#TJN_24_05
* 12-01-2017			Manish					TENJINCG-8
* 03-Jul-2017			Roshni					TENJINCG-259
 *18-Aug-2017			Manish					T25IT-105
 *22-10-2018			Sriram					TENJINCG-876
 *20-03-2019            Padmavathi              TENJINCG-1010 
 *08-03-2019			Sahana					pCloudy
 *18-11-2020			Ashiki					TENJINCG-1213
*/

package com.ycs.tenjin.run;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.rest.SkipSerialization;

public class TestRun {
	private int id;
	/* Fix for timestamp (Changing the type)- Arvind */
	private java.sql.Timestamp startTimeStamp;
	private java.sql.Timestamp endTimeStamp;
	/* Fix for timestamp (Changing the type)- Arvind */
	private String elapsedTime;
	private String user;
	private String status;
	private TestSet testSet;
	private int projectId;
	private int executedTests;
	private int passedTests;
	private int failedTests;
	private int erroredTests;
	private String machine_ip;
	private String browser_type;
	/**************************************************************
	 * Added by Sriram for Execution Screen overhaul (Tenjin v2.2) - 22-09-2015 begins
	 */
	private int targetPort;
	private String domainName;
	private String projectName;
	
	/*changed by manish for req TENJINCG-8 on 12-Jan-2017 starts*/
	
	private TestCase testCase;
	private String tcRecId;
	/* Added by Roshni for TENJINCG-259 starts*/
	private int parentRunId;
	public int getParentRunId() {
		return parentRunId;
	}
	public void setParentRunId(int parentRunId) {
		this.parentRunId = parentRunId;
	}
	/* Added by Roshni for TENJINCG-259 ends*/
	public TestCase getTestCase() {
		return testCase;
	}
	public void setTestCase(TestCase testCase) {
		this.testCase = testCase;
	}
	public String getTcRecId() {
		return tcRecId;
	}
	public void setTcRecId(String tcRecId) {
		this.tcRecId = tcRecId;
	}
	
	/*changed by manish for req TENJINCG-8 on 12-Jan-2017 ends*/
	
	
	
	/**
	 * @return the domainName
	 */
	public String getDomainName() {
		return domainName;
	}
	/**
	 * @param domainName the domainName to set
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}
	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	/**
	 * @return the targetPort
	 */
	public int getTargetPort() {
		return targetPort;
	}
	/**
	 * @param targetPort the targetPort to set
	 */
	public void setTargetPort(int targetPort) {
		this.targetPort = targetPort;
	}
	/**************************************************************
	 * Added by Sriram for Execution Screen overhaul (Tenjin v2.2) - 22-09-2015 endss
	 */
	public String getMachine_ip() {
		return machine_ip;
	}
	public void setMachine_ip(String machine_ip) {
		this.machine_ip = machine_ip;
	}
	public String getBrowser_type() {
		return browser_type;
	}
	public void setBrowser_type(String browser_type) {
		this.browser_type = browser_type;
	}
	/********************************
	 * Change by Sriram for Performance tuning of Execution History (for Tenjin v2.1)
	 */
	private int testSetRecordId;
	private int totalTests;
	
	public int getTotalTests() {
		return totalTests;
	}
	public void setTotalTests(int totalTests) {
		this.totalTests = totalTests;
	}
	public int getTestSetRecordId() {
		return testSetRecordId;
	}
	public void setTestSetRecordId(int testSetRecordId) {
		this.testSetRecordId = testSetRecordId;
	}
	/********************************
	 * Change by Sriram for Performance tuning of Execution History (for Tenjin v2.1) ends
	 */
	/**
	 * @return the executedTests
	 */
	public int getExecutedTests() {
		return executedTests;
	}
	/**
	 * @param executedTests the executedTests to set
	 */
	public void setExecutedTests(int executedTests) {
		this.executedTests = executedTests;
	}
	/**
	 * @return the passedTests
	 */
	public int getPassedTests() {
		return passedTests;
	}
	/**
	 * @param passedTests the passedTests to set
	 */
	public void setPassedTests(int passedTests) {
		this.passedTests = passedTests;
	}
	/**
	 * @return the failedTests
	 */
	public int getFailedTests() {
		return failedTests;
	}
	/**
	 * @param failedTests the failedTests to set
	 */
	public void setFailedTests(int failedTests) {
		this.failedTests = failedTests;
	}
	/**
	 * @return the erroredTests
	 */
	public int getErroredTests() {
		return erroredTests;
	}
	/**
	 * @param erroredTests the erroredTests to set
	 */
	public void setErroredTests(int erroredTests) {
		this.erroredTests = erroredTests;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public TestSet getTestSet() {
		return testSet;
	}
	public void setTestSet(TestSet testSet) {
		this.testSet = testSet;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public Timestamp getStartTimeStamp() {
		return startTimeStamp;
	}
	public void setStartTimeStamp(Date startTimeStamp) {
		this.startTimeStamp = new Timestamp(startTimeStamp.getTime());
	}
	public Timestamp getEndTimeStamp() {
		return endTimeStamp;
	}
	public void setEndTimeStamp(Date endTimeStamp) {
		this.endTimeStamp = (Timestamp) endTimeStamp;
	}
	public String getElapsedTime() {
		return elapsedTime;
	}
	public void setElapsedTime(String elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
	
	
	/*************************************************
	 * Added by Sriram for Req#TJN_24_05
	 */
	private List<Module> functions;
	/*changed by manish for bug T25IT-105 on 18-Aug-2017 starts*/
	@SkipSerialization
	private int appId;
	/*changed by manish for bug T25IT-105 on 18-Aug-2017 ends*/
	private int screenshotOption;
	private String completionStatus;
	
	
	
	public String getCompletionStatus() {
		return completionStatus;
	}
	public void setCompletionStatus(String completionStatus) {
		this.completionStatus = completionStatus;
	}
	public int getScreenshotOption() {
		return screenshotOption;
	}
	public void setScreenshotOption(int screenshotOption) {
		this.screenshotOption = screenshotOption;
	}
	public List<Module> getFunctions() {
		return functions;
	}
	public void setFunctions(List<Module> functions) {
		this.functions = functions;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	
	/*************************************************
	 * Added by Sriram for Req#TJN_24_05 ends
	 */
	
	
	/*************************************************
	 * Added by Sriram for Req#TJN_23_18
	 */
	private String taskType;
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	/*************************************************
	 * Added by Sriram for Req#TJN_23_18 ends
	 */
	
	/******************************
	 * Added by Sriram for TENJINCG-169 (Schedule API Learning)
	 */
	private Api api;
	/*Added by Ashiki for TENJINCG-1213 start*/
	private List<Api> apis;
	/*Added by Ashiki for TENJINCG-1213 end*/
	private List<ApiOperation> apiOperations;

	public Api getApi() {
		return api;
	}
	public void setApi(Api api) {
		this.api = api;
	}
	public List<ApiOperation> getApiOperations() {
		return apiOperations;
	}
	public void setApiOperations(List<ApiOperation> apiOperations) {
		this.apiOperations = apiOperations;
	}
	
	/******************************
	 * Added by Sriram for TENJINCG-169 (Schedule API Learning) ends
	 */
	/* Added by Roshni for TENJINCG-259 starts*/
	private int pureRunId;
	private List<Integer> childRunIds;
	private List<Integer> reRunIds;
	private int currentRunId;
	
	/*changed by manish for bug T25IT-105 on 18-Aug-2017 starts*/
	@SkipSerialization
	private List<Integer>  allParentRunIds;
	/*changed by manish for bug T25IT-105 on 18-Aug-2017 ends*/
	
	public int getPureRunId() {
		return pureRunId;
	}
	public void setPureRunId(int pureRunId) {
		this.pureRunId = pureRunId;
	}
	public List<Integer> getChildRunIds() {
		return childRunIds;
	}
	public void setChildRunIds(List<Integer> childRunIds) {
		this.childRunIds = childRunIds;
	}
	public List<Integer> getReRunIds() {
		return reRunIds;
	}
	public void setReRunIds(List<Integer> reRunIds) {
		this.reRunIds = reRunIds;
	}
	public int getCurrentRunId() {
		return currentRunId;
	}
	public void setCurrentRunId(int currentRunId) {
		this.currentRunId = currentRunId;
	}
	public List<Integer> getAllParentRunIds() {
		return allParentRunIds;
	}
	public void setAllParentRunIds(List<Integer> allParentRunIds) {
		this.allParentRunIds = allParentRunIds;
	}
	/* Added by Roshni for TENJINCG-259 ends*/
	
	/*changed by sahana for Mobility: Starts*/
	private int deviceRecId;
	public int getDeviceRecId() {
		return deviceRecId;
	}
	public void setDeviceRecId(int deviceRecId) {
		this.deviceRecId = deviceRecId;
	}
	/*changed by sahana for Mobility: Starts*/
	//TENJINCG-876 - Sriram
	private String testSetName;
	public String getTestSetName() {
		return testSetName;
	}
	public void setTestSetName(String testSetName) {
		this.testSetName = testSetName;
	}
	//TENJINCG-876 - Sriram ends
	/* Added by Padmavathi for TENJINCG-1010 starts */
	private String executedLevel;
	public String getExecutedLevel() {
		return executedLevel;
	}
	public void setExecutedLevel(String executedLevel) {
		this.executedLevel = executedLevel;
	}
	/* Added by Padmavathi for TENJINCG-1010 ends */
	/*Added by Sahana for pCloudy: Starts*/
	private String deviceFarmFlag;
	public String getDeviceFarmFlag() {
		return deviceFarmFlag;
	}
	public void setDeviceFarmFlag(String deviceFarmFlag) {
		this.deviceFarmFlag = deviceFarmFlag;
	}
	/*Added by Sahana for pCloudy: Ends*/
	/*Added by Ashiki for TENJINCG-1213 start*/
	public List<Api> getApis() {
		return apis;
	}
	public void setApis(List<Api> apis) {
		this.apis = apis;
	}
	/*Added by Ashiki for TENJINCG-1213 end*/
}
