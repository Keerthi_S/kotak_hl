/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LearnerGateway.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                	CHANGED BY              DESCRIPTION
* 22-Sep-2016           Sriram Sridharan        Newly Added For TJN_23_18
* 27-Sep-2016			Sriram Sridharan		Tenjin Core changes (Load an existing run) for scheduling and remote adapter
* 16-03-2018			Preeti 					TENJINCG-73
* 22-Dec-2017			Sahana					  Mobility	
* 31-08-2018            Padmavathi              To check deviceRecId is not null
* 02-01-2019			Prem					For validating adapter license
* 25-01-2019			Sahana 					pCloudy
* 11-04-2019			Ashiki					TENJINCG-1032
* 24-06-2019            Padmavathi              for license
* 03-12-2019			Roshni					TENJINCG-1168
* 19-03-2020			Pushpalatha				TJN252-7
* 11-09-2020            Poojalakshmi            Tenj210-144 
* 18-11-2020			Ashiki					TENJINCG-1213	
*/

package com.ycs.tenjin.run;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.MaxActiveUsersException;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.pojo.TaskManifest;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.AutsHelper;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.exception.RequestValidationException;
import com.ycs.tenjin.handler.AutHandler;
import com.ycs.tenjin.handler.LicenseHandler;
import com.ycs.tenjin.handler.ModuleHandler;
import com.ycs.tenjin.license.License;
import com.ycs.tenjin.license.LicenseComponent;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

public class LearnerGateway {
	private static final Logger logger = LoggerFactory.getLogger(LearnerGateway.class);
	
	private Aut aut;
	private List<Module> functions;
	private String user;
	private String autLoginType; 
	private String regClientName;
	private String browserSelection;
	private int appId;
	private List<String> functionCodes;
	private RegisteredClient client;
	private TestRun testRun;
	private TaskManifest manifest;
	/*Added by Prem for validating adapter license start*/
	/* Uncommented by Padmavathi for license starts */
	private String adapter;
	/* Uncommented by Padmavathi for license ends */
	/*Added by Prem for validating adapter license End*/
	/* Added by Sriram for TENJINCG-152 - API Testing Enablement */
	private String learningMode;
	/* Added by Sriram for TENJINCG-152 - API Testing Enablement ends*/
	
	private Connection conn = null;
	
	public LearnerGateway() throws TenjinServletException{
		try {
			this.conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		} catch (DatabaseException e) {
			
			throw new TenjinServletException("Could not connect to Tenjin DB. Please contact Tenjin Support");
		}
		
		/* Added by Sriram for TENJINCG-152 - API Testing Enablement */
		this.learningMode = "GUI";
		/* Added by Sriram for TENJINCG-152 - API Testing Enablement ends*/
	}
	
	public LearnerGateway(boolean connectToDatabase) throws TenjinServletException {
		if(connectToDatabase) {
			try {
				this.conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			} catch (DatabaseException e) {
				
				throw new TenjinServletException("Could not connect to Tenjin DB. Please contact Tenjin Support");
			}
		}
		
		/* Added by Sriram for TENJINCG-152 - API Testing Enablement */
		this.learningMode = "GUI";
		/* Added by Sriram for TENJINCG-152 - API Testing Enablement ends*/
	}
	
	public LearnerGateway(int appId, List<String> functionCodes, String userId) throws TenjinServletException{
		this.user =userId;
		this.appId = appId;
		this.functionCodes = functionCodes;
		try {
			this.conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		} catch (DatabaseException e) {
			
			throw new TenjinServletException("Could not connect to Tenjin DB. Please contact Tenjin Support");
		}
		
		/* Added by Sriram for TENJINCG-152 - API Testing Enablement */
		this.learningMode = "GUI";
		/* Added by Sriram for TENJINCG-152 - API Testing Enablement ends*/
	}
	
	/* Added by Sriram for TENJINCG-152 - API Testing Enablement */
	
	private Api api;
	private String apiCode;
	private String apiLearnType;
	private List<ApiOperation> operationsToLearn;
	/*Added by Ashiki for TENJINCG-1213 start*/
	private List<Api>  api_learn;
	/*Added by Ashiki for TENJINCG-1213 end*/
	
	public String getLearningMode() {
		return learningMode;
	}

	public Api getApi() {
		return api;
	}

	public String getApiCode() {
		return apiCode;
	}

	public String getApiLearnType() {
		return apiLearnType;
	}

	public List<ApiOperation> getOperationsToLearn() {
		return operationsToLearn;
	}

	public LearnerGateway(int appId, String apiCode, List<ApiOperation> operationsToLearn, String userId) throws TenjinServletException {
		this.appId = appId;
		/*this.apiLearnType = apiLearnType;*/
		this.apiCode = apiCode;
		this.learningMode = "API";
		this.user = userId;
		this.operationsToLearn = operationsToLearn;
		try {
			this.conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		} catch (DatabaseException e) {
			
			throw new TenjinServletException("Could not connect to Tenjin DB. Please contact Tenjin Support");
		}
	}	
	/*Added by Ashiki for TENJINCG-1213 starts*/
	public LearnerGateway(int appId, String apiCode, String userId) throws TenjinServletException {
		this.appId = appId;
		this.learningMode = "API_CODE";
		this.user = userId;
		this.apiCode=apiCode;
		try {
			this.conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		} catch (DatabaseException e) {
			
			throw new TenjinServletException("Could not connect to Tenjin DB. Please contact Tenjin Support");
		}
	}
	/*Added by Ashiki for TENJINCG-1213 end*/
	
	public void setApiLearnType(String apiLearnType) {
		this.apiLearnType = apiLearnType;
	}
	
	/* Added by Sriram for TENJINCG-152 - API Testing Enablement ends*/
	
	public void performInitialValidation() throws TenjinServletException{
		/************************************************************************
		 *commented by sameer for New Adapter Spec
		 */
		//this.validateAutomationEngine();
		this.validateAut();
		this.validateFunctions();
	}
	
	public void performFinalValidation() throws TenjinServletException{	
		this.validateClient();
		this.validateUserAutCredentials();
	}
	
	public void performAllValidations() throws TenjinServletException{
		this.performInitialValidation();
		this.performFinalValidation();
	}
	
	/* Added by Sriram for TENJINCG-152 - API Testing Enablement */
	public void performAllValidationsForApiLearning() throws TenjinServletException {
		this.validateAut();
		this.validateApi();
		this.validateOperationsToLearn();
	}
	/* Added by Sriram for TENJINCG-152 - API Testing Enablement ends*/
	
	
	public TestRun getTestRun() {
		return testRun;
	}


	public void setTestRun(TestRun testRun) {
		this.testRun = testRun;
	}


	public TaskManifest getManifest() {
		return manifest;
	}


	public void setManifest(TaskManifest manifest) {
		this.manifest = manifest;
	}


	public void setAutLoginType(String autLoginType) {
		this.autLoginType = autLoginType;
	}


	public void setRegClientName(String regClientName) {
		this.regClientName = regClientName;
	}


	public void setBrowserSelection(String browserSelection) {
		this.browserSelection = browserSelection;
	}
	
	
	public Aut getAut() {
		return aut;
	}

	

	public List<Module> getFunctions() {
		return functions;
	}
	
	//TENJINCG-731 - By Sameer - Mobility - Start
	/*changed by sahana for Mobility: Starts*/
	private String deviceRecId;
	/*Added by Ashiki for TENJINCG-1213 start*/
	private String apiType;
	/*Added by Ashiki for TENJINCG-1213 end*/
	public String getDeviceRecId() {
		return deviceRecId;
	}
	public void setDeviceRecId(String deviceRecId) {
		this.deviceRecId = deviceRecId;
	}
	/*changed by sahana for Mobility: ends*/ 
	//TENJINCG-731 - By Sameer - Mobility - End
	
	/******************************
	 * Added by Sriram for TENJINCG-169 (Schedule API Learning)
	 */
	public void loadApiRun(int runId, String apiLearnType) throws TenjinServletException{
		
		logger.info("Fetching details of run [{}]", runId);
		try {
			this.testRun = new RunHelper().hydrateRun(this.conn, runId);
		} catch (DatabaseException e) {
			
			throw new TenjinServletException(e.getMessage());
		}
		
		if(this.testRun.getApi() == null || this.testRun.getApiOperations() == null || this.testRun.getApiOperations().size() < 1){
			throw new TenjinServletException("No API Operations were found to learn as part of this run. Please review your job. Contact Tenjin Support for further assistance.");
		}
		
		this.apiCode = this.testRun.getApi().getCode();
		
		logger.info("Getting Application ID");
		this.appId = this.testRun.getAppId();
		
		
		logger.info("Getting all API Operations");
		this.operationsToLearn = new ArrayList<ApiOperation>();
		for(ApiOperation op: this.testRun.getApiOperations()) {
			this.operationsToLearn.add(op);
		}
		
		if(Utilities.trim(this.user).equalsIgnoreCase("")){
			this.user = this.testRun.getUser();
		}else{
			this.testRun.setUser(this.user);
		}
		logger.info("User --> [{}]", this.user);
		
		this.performAllValidationsForApiLearning();
		
		
		
		for(ApiOperation op: this.api.getOperations()) {
			for(ApiOperation rOp : this.testRun.getApiOperations()) {
				if(rOp.getName().equalsIgnoreCase(op.getName())){
					op.setLastSuccessfulLearningResult(rOp.getLastSuccessfulLearningResult());
				}
			}
		}
		
		this.testRun.setMachine_ip("N/A");
		
		this.testRun.setTargetPort(0);
		
		logger.info("Updating run information");
		this.testRun.setStartTimeStamp(new Timestamp(new Date().getTime()));
		try{
			new RunHelper().updateRunInfo(this.conn, this.testRun.getId(), this.testRun.getStartTimeStamp().getTime(), this.testRun.getBrowser_type(), this.testRun.getMachine_ip(), this.testRun.getTargetPort(), this.testRun.getUser());
		}catch(Exception e){
			logger.error("ERROR updating Run Information", e);
			throw new TenjinServletException("Could not load run due to an internal error. Please contact Tenjin Support.");
		}
		
		logger.info("Creating Manifest");
		this.manifest = new TaskManifest();
		this.manifest.setAut(this.aut);
		
		
		this.manifest.setApi(this.api);
		this.manifest.setApiLearnType(this.apiLearnType);
		this.manifest.setTaskType(this.testRun.getTaskType());
		this.manifest.setRunId(this.testRun.getId());
		this.manifest.setApiLearnType(apiLearnType);
		
		logger.info("Run [{}] loaded successfully", runId);
		
		try{
			this.conn.close();
		}catch(Exception ignore){}
	}
	/******************************
	 * Added by Sriram for TENJINCG-169 (Schedule API Learning) ends
	 */
	/******************************
	 * Added by Sriram for Tenjin Core changes for Scheduling and Remote Adapter (Load an existing run)
	 */
	public void loadRun(int runId, String clientName) throws TenjinServletException{
		
		logger.info("Fetching details of run [{}]", runId);
		try {
			this.testRun = new RunHelper().hydrateRun(this.conn, runId);
		} catch (DatabaseException e) {
			
			throw new TenjinServletException(e.getMessage());
		}
		
		if(this.testRun.getFunctions() == null || this.testRun.getFunctions().size() < 1){
			throw new TenjinServletException("No functions were found to learn as part of this run. Please review your job. Contact Tenjin Support for further assistance.");
		}
		
		logger.info("Getting Application ID");
		this.appId = this.testRun.getAppId();
		
		logger.info("Getting all function codes");
		this.functionCodes = new ArrayList<String>();
		for(Module module: this.testRun.getFunctions()){
			this.functionCodes.add(module.getCode());
			this.autLoginType = module.getAutUserType();
		}
		
		logger.info("Getting AUT Login Type --> [{}]", this.autLoginType);
		logger.info("Target Machine --> [{}], Target Port --> [{}]", this.testRun.getMachine_ip(), this.testRun.getTargetPort());
		if(Utilities.trim(this.user).equalsIgnoreCase("")){
			this.user = this.testRun.getUser();
		}else{
			this.testRun.setUser(this.user);
		}
		logger.info("User --> [{}]", this.user);
		this.regClientName = clientName;
		
		this.performAllValidations();
		
		for(Module function:this.functions){
			for(Module rFunction:this.testRun.getFunctions()){
				if(rFunction.getCode().equalsIgnoreCase(function.getCode())){
					function.setLastSuccessfulLearningResult(rFunction.getLastSuccessfulLearningResult());
					break;
				}
			}
		}
		this.testRun.setMachine_ip(this.client.getHostName());
		try {
			this.testRun.setTargetPort(Integer.parseInt(this.client.getPort()));
		} catch (NumberFormatException e) {
			
			logger.error("Invalid Port specified --> [{}]", this.client.getPort());
			logger.warn("Default port 4444 will be used");
			this.testRun.setTargetPort(4444);
		}
		
		logger.info("Updating run information");
		this.testRun.setStartTimeStamp(new Timestamp(new Date().getTime()));
		try{
			new RunHelper().updateRunInfo(this.conn, this.testRun.getId(), this.testRun.getStartTimeStamp().getTime(), this.testRun.getBrowser_type(), this.testRun.getMachine_ip(), this.testRun.getTargetPort(), this.testRun.getUser());
		}catch(Exception e){
			logger.error("ERROR updating Run Information", e);
			throw new TenjinServletException("Could not load run due to an internal error. Please contact Tenjin Support.");
		}
		
		logger.info("Creating Manifest");
		this.manifest = new TaskManifest();
		this.manifest.setAut(this.aut);
		this.manifest.setFunctions(this.functions);
		if(Utilities.trim(this.testRun.getBrowser_type()).equalsIgnoreCase("APPDEFAULT")){
			this.manifest.setBrowser(this.aut.getBrowser());
		}else{
			this.manifest.setBrowser(this.testRun.getBrowser_type());
		}
		this.manifest.setClientIp(this.client.getHostName());
		this.manifest.setClientPort(Integer.parseInt(this.client.getPort())); 
		//TENJINCG-731 - By Sameer - Mobility - Start 
		this.manifest.setDeviceId(this.deviceRecId);
		//TENJINCG-731 - By Sameer - Mobility - End
		/************************************************************************
		 *commented by sameer for New Adapter Spec
		 */
		//this.manifest.setAutomationEngine(this.automationEngine);
		this.manifest.setTaskType(BridgeProcess.LEARN);
		this.manifest.setRunId(this.testRun.getId());
		
		
		logger.info("Run [{}] loaded successfully", runId);
		
		try{
			this.conn.close();
		}catch(Exception ignore){}
	}
	/******************************
	 * Added by Sriram for Tenjin Core changes for Scheduling and Remote Adapter (Load an existing run)
	 * @throws MaxActiveUsersException 
	 */

	public void createRun() throws TenjinServletException{
		logger.info("Creating a new Run");
		this.testRun = new TestRun();
		/* Changed by Sriram for TENJINCG-152 - API Testing Enablement */
		/*this.testRun.setTaskType(AdapterTask.LEARN);*/
		if("api".equalsIgnoreCase(this.learningMode)){
			this.testRun.setTaskType(BridgeProcess.APILEARN);
			this.testRun.setApi(this.api);
			this.testRun.setApiOperations(this.api.getOperations());
		}/*Added by Ashiki for TENJINCG-1213 start*/
		else if("API_CODE".equalsIgnoreCase(this.learningMode)) {
			this.testRun.setTaskType(BridgeProcess.APICODELEARN);
			this.testRun.setApis(this.api_learn);
			/*Added by Ashiki for TENJINCG-1213 end*/
		}else{
			this.testRun.setTaskType(BridgeProcess.LEARN);
		}
		/* Changed by Sriram for TENJINCG-152 - API Testing Enablement  ends*/
		this.testRun.setUser(this.user);
		this.testRun.setStartTimeStamp(new Timestamp(new Date().getTime()));
		
		/* Changed by Sriram for TENJINCG-152 - API Testing Enablement */
		/*this.testRun.setMachine_ip(this.client.getHostName());
		this.testRun.setTargetPort(Integer.parseInt(this.client.getPort()));
		this.testRun.setBrowser_type(this.browserSelection);*/
		/*Modified by Ashiki for TENJINCG-1213 end*/
		if(!"api".equalsIgnoreCase(this.learningMode)&& !"API_CODE".equalsIgnoreCase(this.learningMode)) {
		/*Modified by Ashiki for TENJINCG-1213 end*/	
			this.testRun.setMachine_ip(this.client.getHostName());
			this.testRun.setTargetPort(Integer.parseInt(this.client.getPort()));
			this.testRun.setBrowser_type(this.browserSelection);
		}
		/* Changed by Sriram for TENJINCG-152 - API Testing Enablement ends*/
		
		
		this.testRun.setAppId(this.appId);
		//TENJINCG-731 - By Sameer - Mobility - Start
		/*changed by sahana for Mobility: Starts*/
		/*Added by Padmavathi to check deviceRecId is not null starts*/
		if(this.deviceRecId!=null)
		/*Added by Padmavathi to check deviceRecId is not null ends*/
		this.testRun.setDeviceRecId(Integer.parseInt(this.deviceRecId));
		/*changed by sahana for Mobility: ends*/
		//TENJINCG-731 - By Sameer - Mobility - End
		/* Added by Sahana for pCloudy:starts */
		if(this.client!=null && this.client.getDeviceFarmCheck()!=null && this.client.getDeviceFarmCheck().equalsIgnoreCase("Y"))
			this.testRun.setDeviceFarmFlag(this.client.getDeviceFarmCheck());
		else
			this.testRun.setDeviceFarmFlag("N");
		/* Added by Sahana for pCloudy:ends */
		logger.info("Creating Task Manifest");
		this.manifest = new TaskManifest();
		this.manifest.setAut(this.aut);
		
		/* Changed by Sriram for TENJINCG-152 - API Testing Enablement */
		/*this.manifest.setFunctions(this.functions);*/
		if("api".equalsIgnoreCase(this.learningMode)) {
			this.manifest.setApi(this.api);
			this.manifest.setApiLearnType(this.apiLearnType);
			/*Added by Ashiki for TENJINCG-1213 start*/
		}else if("API_CODE".equalsIgnoreCase(this.learningMode)) {
			this.manifest.setApis(this.api_learn);
			this.manifest.setApiLearnType(this.apiLearnType);
			this.manifest.setApiType(this.apiType);
			/*Added by Ashiki for TENJINCG-1213 end*/
		}else{
			this.manifest.setFunctions(this.functions);
			if(Utilities.trim(this.browserSelection).equalsIgnoreCase("APPDEFAULT")){
				this.manifest.setBrowser(this.aut.getBrowser());
			}else{
				this.manifest.setBrowser(this.browserSelection);
			}
			this.manifest.setClientIp(this.client.getHostName());
			this.manifest.setClientPort(Integer.parseInt(this.client.getPort())); 
			/*Added by Ashiki for TENJINCG-1032 starts*/
			this.manifest.setRegClientName(this.client.getName());
			this.manifest.setAutLoginType(this.autLoginType);
			/*Added by Ashiki for TENJINCG-1032 ends*/
			//TENJINCG-731 - By Sameer - Mobility - Start
			this.manifest.setDeviceId(this.deviceRecId);
			//TENJINCG-731 - By Sameer - Mobility - End
			/************************************************************************
			 *commented by sameer for New Adapter Spec
			 */
			//this.manifest.setAutomationEngine(this.automationEngine);
		}
		
		this.manifest.setTaskType(this.testRun.getTaskType());
		/* Uncommented by Padmavathi for license starts */
		this.manifest.setAdapterName(this.adapter);
		/* Uncommented by Padmavathi for license ends */
		
		logger.info("Persisting new Run");
		try {
			new RunHelper().persistNewRun(this.conn, this.testRun, this.manifest);
			this.manifest.setRunId(this.testRun.getId());
		} catch (DatabaseException e) {
			
			logger.error("ERROR creating new run");
			throw new TenjinServletException("Could not initiate Learning Run due to an internal error. Please contact Tenjin Support.");
		}
		
		try{
			this.conn.close();
		}catch(Exception ignore){}
		
	}
	
	/************************************************************************
	 *commented by sameer for New Adapter Spec
	 * @throws MaxActiveUsersException 
	 * @throws LicenseInactive 
	 */

	/*Added by Padmavathi for license starts*/
	public void validateAdapterLicense() throws MaxActiveUsersException, LicenseInactive{
		try {
			int limit = 0;
			int count = 0;
			List<String> adpternameLice = new ArrayList<>();
			Map<String, String> map = new ConcurrentHashMap<String, String>();
			Map<String, Integer> countMap = new ConcurrentHashMap<String, Integer>();	
			License license = (License) CacheUtils.getObjectFromRunCache("licensedetails");	
			for (LicenseComponent licenseComponent :license.getLicenseComponents()) {
				String adapterName=licenseComponent.getProductComponent().getName();
					if (adapterName.equalsIgnoreCase(this.aut.getAdapterPackage())) {
						
						new LicenseHandler().licenseComponentActiveValidation(licenseComponent);
						CacheUtils.initializeRunCache();
						try {
							if (CacheUtils.getObjectFromRunCache(this.aut.getAdapterPackage()).equals(null)) {
							} else {
								count = (int) CacheUtils.getObjectFromRunCache(this.aut.getAdapterPackage());
								limit = licenseComponent.getLimit();
								if (count <= limit) {
									CacheUtils.putObjectInRunCache(this.aut.getAdapterPackage(), count + 1);
								}
							}
							if (count > limit) {
								logger.error("Max numeber of Tenjin User for Application {}", this.aut.getAdapterPackage());
							}
							adpternameLice.add(adapterName);
							countMap.put(adapterName, limit);
						} catch (NullPointerException e) {
							count = 1;
							CacheUtils.putObjectInRunCache(this.aut.getAdapterPackage(), count);
							adpternameLice.add(adapterName);
							limit = licenseComponent.getLimit();
							countMap.put(adapterName, limit);
							if (count > limit) {
								//logger.error("Max numeber of Tenjin User for Application {}", this.aut.getAdapterPackage());
								map.put(this.aut.getAdapterPackage(), "You have reached the maximum limit for ["+this.aut.getName()+"] application");
							}
						}
						this.adapter = adpternameLice.toString();
					} else {
						//logger.error("No adapter License availabel{} ", this.aut.getAdapterPackage());
						map.put(adapterName, "You need a license for ["+adapterName+"] application to perform this operation. Please contact Tenjin Support.");
					}
			}
			List<String> st = adpternameLice;
			if(adpternameLice.isEmpty()) {
				logger.error("No adapter License availabel for {}",this.aut.getName());
				throw new MaxActiveUsersException("You need a license for ["+ this.aut.getName()+"] application to perform this operation. Please contact Tenjin Support.");
			}
			for (String s : st) {
			/*Added by Poojalakshmi Tenj210-144 starts*/
			 Object adapter=CacheUtils.getObjectFromRunCache(s);
			 try {
				if(adapter.equals(null)) throw new MaxActiveUsersException("Invalid adapter name ["+this.aut.getAdapterPackage()+"].Please contact Tenjin Support.");
            }
			 catch (NullPointerException e) {
				 logger.error("Invalid adapter name , Check the DB ",this.aut.getAdapterPackage() );
				  throw new MaxActiveUsersException("Invalid adapter name ["+this.aut.getAdapterPackage()+"].Please contact Tenjin Support.");
			}
			 /*Added by Poojalakshmi Tenj210-144 ends*/
			  count = (int) CacheUtils.getObjectFromRunCache(s);
			  map.containsKey(s);
				if (countMap.containsKey(s)) {
					map.remove(s);
					Integer se = countMap.get(s);
					if (count > se) {
						logger.error("Max number of Tenjin User for {}",this.aut.getName());
						CacheUtils.putObjectInRunCache(s, count - 1);
						throw new MaxActiveUsersException("You have reached the maximum limit for ["+this.aut.getName()+"] application");
					}
				} 
			}
		}
		catch (TenjinConfigurationException e) {
			
			logger.error("Error ", e);
		} 
	}
	/*Added by Padmavathi for license ends*/
	private void validateAut() throws TenjinServletException{
		
		
		logger.info("Validating AUT Information");
		
		try {
			/*Modified by Preeti for TENJINCG-73 starts*/
			/*this.aut = new AutHelper().hydrateAut(this.appId);*/
			/* modified by Roshni for TENJINCG-1168 starts */
			/*this.aut = new AutsHelper().hydrateAut(this.appId);*/
			/*this.aut = new AutHandler().getApplication(this.appId);*/
			this.aut = new AutHandler().getApplication(this.appId,this.conn);
			/* modified by Roshni for TENJINCG-1168 ends */
			/*Modified by Preeti for TENJINCG-73 ends*/
			if(this.aut == null){
				logger.error("AUT is null");
				throw new TenjinServletException("Could not verify Application Information. Please contact Tenjin Support.");
			}
		} catch (DatabaseException e) {
			
			logger.error("ERROR fetching details of application with ID [{}]", this.appId, e);
			throw new TenjinServletException("Could not verify Application Information. Please contact Tenjin Support.");
		} catch(TenjinServletException e){
			throw e;
		}
		
		logger.info("Application [{}] validated.", this.aut.getName());
	}
	
	
	private void validateFunctions() throws TenjinServletException{
		logger.info("Validating Functions to Learn");
		
		this.functions = new ArrayList<Module>();
		ModulesHelper helper = new ModulesHelper();
		String message = "";
		for(String functionCode:this.functionCodes){
			try {
			    /*Modified by Roshni TENJINCG-1168 starts */
				/*Module module = helper.hydrateModuleInfo(this.aut.getId(), functionCode);*/
				Module module = helper.hydrateModuleInfo(this.aut.getId(), functionCode,this.conn);
				  /*Modified by Roshni TENJINCG-1168 ends */
				if(module == null){
					logger.error("Invalid function code [{}]", functionCode);
					message = "The function [" + functionCode + "] is Invalid. Please review your selection and try again";
					break;
				}
				
				this.functions.add(module);
			} catch (DatabaseException e) {
				
				logger.error("ERROR while validating function [{}]", functionCode, e);
				message = "Could not validate function [" + functionCode + "]. Please contact Tenjin support.";
				break;
			}
		}
		
		if(message.length() > 0){
			throw new TenjinServletException(message);
		}
		
		
		logger.info("All functions validated successfully");
	}
	
	/* Added by Sriram for TENJINCG-152 - API Testing Enablement */
	private void validateApi() throws TenjinServletException {
		logger.info("Validating API [{}]", this.apiCode);
		
		
		try {
			this.api = new ApiHelper().hydrateApi(this.appId, this.apiCode);
		} catch (DatabaseException e) {
			
			throw new TenjinServletException("Could not validate API due to an internal error. Please contact Tenjin Support.");
		}
		
		if(this.api == null) {
			logger.error("Invalid API Code [{}]", this.apiCode);
			throw new TenjinServletException("The API [" + this.apiCode + "] is invalid." );
		}
		
		logger.info("API [{}] validated successfully.", this.apiCode);
		
	}
	
	private void validateOperationsToLearn() throws TenjinServletException {
		ApiHelper helper = new ApiHelper();
		if(this.operationsToLearn != null && this.operationsToLearn.size() > 0) {
			for(ApiOperation op:this.operationsToLearn) {
				logger.info("Validating operation [{}]", op.getName());
				try {
					ApiOperation operation = helper.hydrateApiOperation(this.conn, this.appId, this.apiCode, op.getName());
					if(operation == null) {
						logger.error("Invalid operation [{}] for API [{}] in application [{}]", op.getName(), this.apiCode, this.appId);
						throw new TenjinServletException("Invalid Operation [" + op.getName() + "]");
					}else {
						
						
						/* Added by paneendra for TENJINCG-1239  starts*/
						op.setListHeaderRepresentation(operation.getListHeaderRepresentation());
						/* Added by paneendra for TENJINCG-1239  ends*/
						op.setListRequestRepresentation(operation.getListRequestRepresentation());
						op.setListResponseRepresentation(operation.getListResponseRepresentation());
						op.setResourceParameters(operation.getResourceParameters());
						//changes for API_405 ends
					}
				} catch (DatabaseException e) {
					
					throw new TenjinServletException("Could not validate operations for this API due to an unexpected error. Please contact Tenjin Support.");
				} 
			}
		}else{
			logger.warn("No Operations are selected for this API to validate");
		}
		
		this.api.setOperations(this.operationsToLearn);
		
	}
	
	/* Added by Sriram for TENJINCG-152 - API Testing Enablement ends*/
	
	private void validateClient() throws TenjinServletException{
		logger.info("Validating Client Information");
		
		
		try{
			this.client = new ClientHelper().hydrateClient(this.regClientName);
			
			if(this.client == null){
				logger.error("Invalid Client [{}]", this.regClientName);
				throw new TenjinServletException("The client [" + this.regClientName + "] is invalid. Please choose a different client and try again.");
			}
			
		} catch(DatabaseException e){
			logger.error(e.getMessage());
			throw new TenjinServletException("Could not validate client information. Please contact Tenjin Support");
		}
		
		logger.info("Client information validated successfully");
	}
	
	private void validateUserAutCredentials() throws TenjinServletException{
		logger.info("Validating User-AUT Credentials");
		/*changed by sahana  to over come NullPointerException...starts*/
		String autName=null;
		/*changed by sahana  to over come NullPointerException...ends*/
		try {
			autName=this.aut.getName();
			/*Modified by Preeti for TENJINCG-73 starts*/
			/*this.aut = new AutHelper().hydrateAutForDriverLaunch(this.conn, this.aut.getName(), this.user, this.autLoginType);*/
			this.aut = new AutsHelper().hydrateAutForDriverLaunch(this.conn, this.aut.getName(), this.user, this.autLoginType);
			/*Modified by Preeti for TENJINCG-73 ends*/
			if(this.aut == null){
				logger.error("Aut User credentials in valid");
				/*throw new TenjinServletException("The credentials you have specified for " + this.aut.getName() + " are invalid. Please provide valid credentials.");*/
				/*changed by sahana  to over come NullPointerException...starts*/
				throw new TenjinServletException("The credentials you have specified for " + autName + " are invalid. Please provide valid credentials.");
				/*changed by sahana  to over come NullPointerException...ends*/
			}
			
			logger.debug("Decrypting password");
			 /*Modified by paneendra for VAPT fix starts*/
			CryptoUtilities cryptoUtilities=new CryptoUtilities();
			/*String dPass = new Crypto().decrypt(this.aut.getPassword().split("!#!")[1],
					  decodeHex(this.aut.getPassword().split("!#!")[0].toCharArray()));*/
			
			String dPass = cryptoUtilities.decrypt(this.aut.getPassword());
			 /*Modified by paneendra for VAPT fix ends*/
			this.aut.setPassword(dPass);
		} catch (DatabaseException e) {
			/*changed by sahana  to over come NullPointerException...starts*/
			/*logger.error("ERROR validating user aut credentials for [{}]", this.aut.getName(), e);
			throw new TenjinServletException("Could not validate credentials for [" + this.aut.getName() + "]. Please contact Tenjin Support");*/
			logger.error("ERROR validating user aut credentials for [{}]", autName, e);
			throw new TenjinServletException("Could not validate credentials for [" + autName + "]. Please contact Tenjin Support");
			/*changed by sahana  to over come NullPointerException...ends*/
		} catch(Exception e){
			/*changed by sahana  to over come NullPointerException...starts*/
			/*logger.error("ERROR decrypting AUT login password", this.aut.getName(), e);
			throw new TenjinServletException("Could not validate credentials for [" + this.aut.getName() + "]. Please contact Tenjin Support");*/
			logger.error("ERROR decrypting AUT login password", autName, e);
			throw new TenjinServletException("Could not validate credentials for [" + autName + "]. Please contact Tenjin Support");
			/*changed by sahana  to over come NullPointerException...ends*/
		}
		
		logger.info("Credentials validated successfully");
		
		if(this.functions != null && this.functions.size() >0){
			for(Module module:this.functions){
				module.setAutUserType(this.autLoginType);
			}
		}
	}
	
	/*Added by Pushpalatha for TJN252-7 starts*/
	public synchronized void createRun1(List<String> funcList,int appId) throws TenjinServletException, DatabaseException, RequestValidationException{
			
			new ModuleHandler().validateFunc(funcList,appId,this.conn);
			logger.info("Creating a new Run");
			this.testRun = new TestRun();
			if("api".equalsIgnoreCase(this.learningMode)){
				this.testRun.setTaskType(BridgeProcess.APILEARN);
				this.testRun.setApi(this.api);
				this.testRun.setApiOperations(this.api.getOperations());
			}else{
				this.testRun.setTaskType(BridgeProcess.LEARN);
			}
			this.testRun.setUser(this.user);
			this.testRun.setStartTimeStamp(new Timestamp(new Date().getTime()));
			
			
			if(!"api".equalsIgnoreCase(this.learningMode)) {
				this.testRun.setMachine_ip(this.client.getHostName());
				this.testRun.setTargetPort(Integer.parseInt(this.client.getPort()));
				this.testRun.setBrowser_type(this.browserSelection);
			}
			
			
			this.testRun.setAppId(this.appId);
			if(this.deviceRecId!=null)
			this.testRun.setDeviceRecId(Integer.parseInt(this.deviceRecId));
			
			logger.info("Creating Task Manifest");
			this.manifest = new TaskManifest();
			this.manifest.setAut(this.aut);
			/*Added by Prem for handling null while removeing adapter count from cache starts */
			this.manifest.setAdapterName(this.aut.getAdapterPackage());
			/*Added by Prem for handling null while removeing adapter count from cache Ends */
			if("api".equalsIgnoreCase(this.learningMode)) {
				this.manifest.setApi(this.api);
				this.manifest.setApiLearnType(this.apiLearnType);
			}else{
				this.manifest.setFunctions(this.functions);
				if(Utilities.trim(this.browserSelection).equalsIgnoreCase("APPDEFAULT")){
					this.manifest.setBrowser(this.aut.getBrowser());
				}else{
					this.manifest.setBrowser(this.browserSelection);
				}
				this.manifest.setClientIp(this.client.getHostName());
				this.manifest.setClientPort(Integer.parseInt(this.client.getPort())); 
				this.manifest.setDeviceId(this.deviceRecId);
				
			}
			
			this.manifest.setTaskType(this.testRun.getTaskType());
			
			
			
			logger.info("Persisting new Run");
			try {
				new RunHelper().persistNewRun(this.conn, this.testRun, this.manifest);
				this.manifest.setRunId(this.testRun.getId());
			} catch (DatabaseException e) {
				logger.error("ERROR creating new run");
				throw new TenjinServletException("Could not initiate Learning Run due to an internal error. Please contact Tenjin Support.");
			}
			
			try{
				this.conn.close();
			}catch(Exception ignore){}
			
		}
		/*Added by Pushpalatha for TJN252-7 ends*/

	
	/*Added by Ashiki for TENJINCG-1213 starts*/
	public void performApiValidations() throws TenjinServletException, DatabaseException {
	this.validateAut();
	this.validateApiCode();
	}
	/*Added by Ashiki for TENJINCG-1213 end*/

	/*Added by Ashiki for TENJINCG-1213 starts*/
	private void validateApiCode() throws TenjinServletException {
		String apiType="";

		logger.info("Validating API [{}]", this.apiCode);
		List<Api> code=new ArrayList<Api>();
		String[] apicodes= this.apiCode.split(",");
		
		try {
			for (String apiCode : apicodes) {
				Api api = new ApiHelper().hydrateApi(this.appId, apiCode);
				if(apiType.equalsIgnoreCase(""))
				apiType=api.getType();
				else {
					if(!apiType.equalsIgnoreCase(api.getType())) {
						throw new TenjinServletException("API code type should of type.");
					}
				}
				ApiHelper helper = new ApiHelper();
				logger.info("Validating operation ");
				List<ApiOperation> operation=	helper.hydrateApiOperation(this.conn, this.appId, apiCode);
				if(operation == null) {
					logger.error("Invalid operation [{}] for API [{}] in application [{}]", operation.get(0).getName(), this.apiCode, this.appId);
					throw new TenjinServletException("Invalid Operation [" + operation.get(0).getName() + "]");
				} 
				api.setOperations(operation);
				
				code.add(api);
				
			}
			this.api_learn=code;
			this.setApiType(apiType);
		} catch (DatabaseException e) {
			
			throw new TenjinServletException("Could not validate API due to an internal error. Please contact Tenjin Support.");
		}
		
		
		logger.info("API [{}] validated successfully.", this.apiCode);
		
	
		
	
		
	}
	/*Added by Ashiki for TENJINCG-1213 end*/



	public String getApiType() {
		return apiType;
	}

	public void setApiType(String apiType) {
		this.apiType = apiType;
	}

	/*Added by Ashiki for TENJINCG-1213 start*/
	public List<Api> getApi_learn() {
		return api_learn;
	}

	public void setApi_learn(List<Api> api_learn) {
		this.api_learn = api_learn;
	}
	/*Added by Ashiki for TENJINCG-1213 end*/
}