/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCaseExecutionProgress.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright &copy 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 13-03-2020			Sriram					TENJINCG-1196
 */

package com.ycs.tenjin.run;

import java.util.List;

import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.util.HatsConstants;

public class TestCaseExecutionProgress {
	private List<TestCase> testCases;
	private TestCase currentTestCase;
	private TestStep currentTestStep;
	private int runId;
	public List<TestCase> getTestCases() {
		return testCases;
	}
	public void setTestCases(List<TestCase> testCases) {
		this.testCases = testCases;
		if(this.testCases != null) {
			for(TestCase testCase : this.testCases) {
				if(testCase.getTcStatus() != null && testCase.getTcStatus().equalsIgnoreCase(HatsConstants.SCRIPT_EXECUTING)) {
					setCurrentTestCase(testCase);
				}
			}
		}
	}
	public TestCase getCurrentTestCase() {
		return currentTestCase;
	}
	public void setCurrentTestCase(TestCase currentTestCase) {
		this.currentTestCase = currentTestCase;
	}
	public TestStep getCurrentTestStep() {
		return currentTestStep;
	}
	public void setCurrentTestStep(TestStep currentTestStep) {
		this.currentTestStep = currentTestStep;
	}
	public int getRunId() {
		return runId;
	}
	public void setRunId(int runId) {
		this.runId = runId;
	}
	
}
