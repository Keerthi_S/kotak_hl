/***


Yethi Consulting Private Ltd. CONFIDENTIAL

Name of this file:  ExecutorGateway.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright &copy 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 26-Sep-2016           Sriram Sridharan        Newly Added For 
* 27-Sep-2016			Sriram					Added by Sriram for Req#TJN_23_18
* 06-Dec-16				Sriram					Requirement#TJN_243_11 (Run Test Step)
* 19-12-2016        	Leelaprasad             DEFECT FIX#TEN-95
* 03-Jul-2017			Roshni					TENJINCG-259
* 27-Jul-2017			Sriram Sridharan		TENJINCG-311
* 16-Aug-2017			Sriram Sridharan		T25IT-162
* 29-08-2017          	Leelaprasad            defcet T25IT-299
* 22-Sep-2017			sameer gupta			For new adapter spec
* 27-09-2017          	Leelaprasad             Demo API testing
* 29-Jan-2018			Manish/Arvind			TENJINCG-545/SATJSUP-9/SATJSUP-8
* 30-01-2017			Preeti					TENJINCG-503
* 02-Feb-2018			Sriram					Added File.separator to enable MacOS Compatability
* 15-05-2018            Padmavathi              TENJINCG-647
* 18-05-2018            Padmavathi              TENJINCG-647
* 22-Dec-2017			Sahana					Mobility
* 30-05-2018            Padmavathi              TENJINCG-673
* 24-08-2018            Padmavathi              commented unused connection
* 25-10-2018            Ramya                   TENJINCG-832
* 26-10-2018            Ramya                   TENJINCG-832
* 19-11-2018			Prem					TENJINCG-843
* 26-11-2018			Prem					TENJINCG-843
* 07-12-2018			Pushpa					TENJINCG-909
* 27-12-2018			Preeti					TJN252-57
* 22-03-2019			Preeti					TENJINCG-1018
* 25-01-2019			Sahana 					pCloudy
* 11-04-2019            Padmavathi              TNJN27-87
* 14-04-2019			Ashiki					TJN252-97
* 24-04-2019			Roshni					TENJINCG-1038
* 22-06-2019			Preeti					V2.8-161
* 24-06-2019            Padmavathi              for license
* 08-07-2019            Padmavathi              TV2.8R2-20 & 22
* 25-02-2020			Sriram 					TENJINCG-1175 (TENJINCG-1178) 
* 11-02-2021			Paneendra               TENJINCG-1257
*/

package com.ycs.tenjin.run;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.LicenseInactive;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.TenjinSession;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.BridgeProcessor;
import com.ycs.tenjin.bridge.constants.BrowserType;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.oem.gui.datahandler.GuiDataHandler;
import com.ycs.tenjin.bridge.oem.gui.datahandler.GuiDataHandlerImpl;
import com.ycs.tenjin.bridge.pojo.TaskManifest;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.project.UIValidation;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.AutHelper;
import com.ycs.tenjin.db.ClientHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.ExecutionHelper;
import com.ycs.tenjin.db.LearningHelper;
import com.ycs.tenjin.db.ModulesHelper;
import com.ycs.tenjin.db.ProjectHelper;
import com.ycs.tenjin.db.ResultsHelper;
import com.ycs.tenjin.db.RunHelper;
import com.ycs.tenjin.db.TestDataHelper;
import com.ycs.tenjin.db.TestSetHelper;
import com.ycs.tenjin.db.UIValidationHelper;
import com.ycs.tenjin.db.UserHelperNew;
import com.ycs.tenjin.handler.LicenseHandler;
import com.ycs.tenjin.license.License;
import com.ycs.tenjin.license.LicenseComponent;
import com.ycs.tenjin.project.Project;
import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestDataPath;
import com.ycs.tenjin.project.TestSet;
import com.ycs.tenjin.project.TestStep;
import com.ycs.tenjin.servlet.TenjinServletException;
import com.ycs.tenjin.user.User;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.ExcelException;
import com.ycs.tenjin.util.ExcelHandler;
import com.ycs.tenjin.util.Utilities;

public class ExecutorGateway {
	private static final Logger logger = LoggerFactory.getLogger(ExecutorGateway.class);
	private TaskManifest manifest;
	private TenjinSession tjnSession;
	private int testSetRecordId;
	private String targetIp;
	private int targetPort;
	private TestSet testSet;
	private TestRun testRun;
	private Map<String, String> testDataFilePaths;
	private String browserType;
	private Map<Integer, List<String>> tdUidMap;
	private int execSpeed;
	/* Added by Ramya for TENJINCG-832 starts */
	private int runId;
	/* Added by Ramya for TENJINCG-832 ends */
//	private String oem;
	private int screenshotOption;
	private Connection projConn;
	/* commented by Padmavathi for unused connection starts */
	/* private Connection appConn; */
	/* commented by Padmavathi for unused connection ends */
	private Map<String, Map<String, Aut>> autsMap;
	private List<ExecutionStep> steps;
	private Map<String, Module> inScopeFunctions;
	/* Added by Ashiki for TENJINCG-1275 starts */
	private String msgTaskType="";
	/* Added by Ashiki for TENJINCG-1275 ends */
	// Parveen for TJN_24_11
	private String videoCaptureFlag;
	
	// Parveen for TJN_24_11 ends

	// Sriram TJN_243_11
	private List<TestStep> stepsToExecute;

	/********************************
	 * Added by Sriram for TENJINCG-168 (API Execution)
	 */
	private String executionMode;
	/********************************
	 * Added by Sriram for TENJINCG-168 (API Execution) ends
	 */
	/* Added by Roshni for TENJINCG-259 */
	private int parentId;

	/* changed by sahana for Mobility: Starts */
	private String deviceRecId;
	/* changed by sahana for Mobility: ends */

	/* Added by Padmavathi for TENJINCG-647 starts */
	private Map<Integer, String> passedStepsStatusMap;
	/* Added by Padmavathi for TENJINCG-647 ends */

	/* For TENJINCG-311 */
	private Map<Integer, String> uiValidationMap;

	/* For TENJINCG-311 ends */
	public List<TestStep> getStepsToExecute() {
		return stepsToExecute;
	}

	public void setStepsToExecute(List<TestStep> stepsToExecute) {
		this.stepsToExecute = stepsToExecute;
	}

	// Sriram TJN_243_11 ends
	/* Added By Prem for TENJINCG-843 */
	/* Uncommented by Padmavathi for license starts */
	private String adapter;
/* Uncommented by Padmavathi for license ends */
	public ExecutorGateway() throws TenjinServletException {
		try {
			this.projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		} catch (Exception e) {
			logger.error("ERROR getting connection to TJNPROJ", e);
			throw new TenjinServletException("Could not connect to Tenjin Database");
		}
		this.testDataFilePaths = new HashMap<String, String>();
		logger.info("ExecutorGateway Initialization --> Done");
	}

	/********************************
	 * Changed by Sriram for TENJINCG-168 (API Execution)
	 */

	public ExecutorGateway(TenjinSession tjnSession, int testSetRecordId, String browserType, String targetIp,
			int targetPort, String oem, int execSpeed, int screenshotOption, String videoCaptureFlag)
			throws TenjinServletException {
		if (tjnSession != null && tjnSession.getUser() != null && tjnSession.getProject() != null) {
			this.tjnSession = tjnSession;
		} else {
			logger.error(
					"ExecutionGateway Error --> Invalid Tenjin session. Either user or project or TenjinSession is null");
			throw new TenjinServletException("Could not initiate execution");
		}

		this.testSetRecordId = testSetRecordId;
		this.targetIp = targetIp;
		this.targetPort = targetPort;
		// Added here ...
		this.execSpeed = execSpeed;
		this.tdUidMap = new TreeMap<Integer, List<String>>();

		this.testDataFilePaths = new HashMap<String, String>();
		this.browserType = browserType;
		this.screenshotOption = screenshotOption;
		this.videoCaptureFlag = videoCaptureFlag;
		try {
			this.projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		} catch (Exception e) {
			logger.error("ERROR getting connection to TJNPROJ", e);
			throw new TenjinServletException("Could not connect to Tenjin Database");
		}
		logger.info("ExecutorGateway Initialization --> Done");
	}

	/* changed by sahana for Mobility: Starts */
	
	public ExecutorGateway(TenjinSession tjnSession, int testSetRecordId, String browserType, String targetIp,
			int targetPort, String oem, int execSpeed, int screenshotOption, String videoCaptureFlag,
			String executionMode, String deviceId, String adapter) throws TenjinServletException {
		/* changed by sahana for Mobility: ends */
		if (tjnSession != null && tjnSession.getUser() != null && tjnSession.getProject() != null) {
			this.tjnSession = tjnSession;
		} else {
			logger.error(
					"ExecutionGateway Error --> Invalid Tenjin session. Either user or project or TenjinSession is null");
			throw new TenjinServletException("Could not initiate execution");
		}

		this.testSetRecordId = testSetRecordId;
		this.targetIp = targetIp;
		this.targetPort = targetPort;
		// Added here ...
		this.execSpeed = execSpeed;
		this.tdUidMap = new TreeMap<Integer, List<String>>();
//		this.oem = oem;

		this.testDataFilePaths = new HashMap<String, String>();
		this.browserType = browserType;
		this.screenshotOption = screenshotOption;
		this.videoCaptureFlag = videoCaptureFlag;
		/* changed by sahana for Mobility: Starts */
		this.deviceRecId = deviceId;
		/* changed by sahana for Mobility:ends */
	/* Uncommented by Padmavathi for license starts */
		this.adapter = adapter;
		/* Uncommented by Padmavathi for license ends */
		try {
			this.projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		} catch (Exception e) {
			logger.error("ERROR getting connection to TJNPROJ", e);
			throw new TenjinServletException("Could not connect to Tenjin Database");
		}
		
		if (Utilities.trim(executionMode).length() > 0) {
			this.executionMode = executionMode;
		} else {
			this.executionMode = "GUI";
		}

		logger.info("ExecutorGateway Initialization --> Done");
	}

	/********************************
	 * Changed by Sriram for TENJINCG-168 (API Execution)
	 */

	/* Added by Roshni for TENJINCG-259 starts */
	/* Modified by Padmavathi TENJINCG-673 starts */
	public ExecutorGateway(TenjinSession tjnSession, TestSet testSet, int testSetRecordId, String browserType,
			String targetIp, int targetPort, String oem, int execSpeed, int screenshotOption, String videoCaptureFlag,
			String executionMode, int parentId, Map<Integer, String> passedStepsStatusMap, String deviceId)
			throws TenjinServletException {
		/* Modified by Padmavathi TENJINCG-673 ends */
		if (tjnSession != null && tjnSession.getUser() != null && tjnSession.getProject() != null) {
			this.tjnSession = tjnSession;
		} else {
			logger.error(
					"ExecutionGateway Error --> Invalid Tenjin session. Either user or project or TenjinSession is null");
			throw new TenjinServletException("Could not initiate execution");
		}
		this.testSet = testSet;
		this.testSetRecordId = testSetRecordId;
		this.targetIp = targetIp;
		this.targetPort = targetPort;
		// Added here ...
		this.execSpeed = execSpeed;
		this.tdUidMap = new TreeMap<Integer, List<String>>();

		this.testDataFilePaths = new HashMap<String, String>();
		this.browserType = browserType;
		this.screenshotOption = screenshotOption;
		this.videoCaptureFlag = videoCaptureFlag;
		this.parentId = parentId;
		/* Added by Padmavathi for TENJINCG-647 starts */
		this.passedStepsStatusMap = passedStepsStatusMap;
		/* Added by Padmavathi for TENJINCG-647 ends */

		/* Added by Padmavathi for TENJINCG-673 starts */
		this.deviceRecId = deviceId;
		/* Added by Padmavathi for TENJINCG-673 ends */
		try {
			this.projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		} catch (Exception e) {
			logger.error("ERROR getting connection to TJNPROJ", e);
			throw new TenjinServletException("Could not connect to Tenjin Database");
		}
		
		if (Utilities.trim(executionMode).length() > 0) {
			this.executionMode = executionMode;
		} else {
			this.executionMode = "GUI";
		}

		logger.info("ExecutorGateway Initialization --> Done");
	}

	/* Added by Roshni for TENJINCG-259 ends */
	public void performAllValidations() throws TenjinServletException {
		logger.info("Initializing Executor");
		this.initExecution();

		logger.info("Validating Test Data Paths");
		/*modified by paneendra for TENJINCG-1268 starts */
		TreeMap<String, String> failedMap = this.validateUploadedTestDataPaths();
		/*modified by paneendra for TENJINCG-1268 ends */
		if (failedMap != null && failedMap.size() > 0) {
			String msg = "";
			for (String key : failedMap.keySet()) {
				/*Modified by Padmavathi for TV2.8R2-20 for starts*/
				if(!key.equalsIgnoreCase("Status")){
					String[] msgs=key.split(",");
					msg = msg + failedMap.get(key) +" for the function "+msgs[1] +" under the AUT "+ msgs[0]+" \n";
				}
				/*Modified by Padmavathi for TV2.8R2-20 for ends*/
				
			}

			logger.error("Test Data Paths validation failed\n [{}]", msg);
			throw new TenjinServletException(msg);
		}

		logger.info("Verifying Test Data");
		Map<String, List<String>> map = this.getTestDataRowUids();

		String msg = "";

		for (String key : map.keySet()) {
			String[] split = key.split(",");
			String tc = split[0];
			String s = split[1];
			List<String> uids = map.get(key);

			int iterCount = 0;
			if (uids != null) {
				iterCount = uids.size();
			}

			if (iterCount < 1) {
				/*Modified by Padmavathi for TV2.8R2-22 for starts*/
				/*msg = msg + "No Test data was found for Test Case " + tc + ", Step " + s + "\n";*/
				msg = msg + "No Test data found for test step "+ s +" which is under the test case " + tc +" \n";
				/*Modified by Padmavathi for TV2.8R2-22 for ends*/
			}
		}

		if (Utilities.trim(msg).length() > 0) {
			logger.error("Test Data Verification failed\n[{}]", msg);
			throw new TenjinServletException(msg);
		}

		logger.info("Validating AUT Credentials");
		Map<String, String> autMap = this.validateUserAutCredentials();
		if (autMap != null && autMap.size() > 0) {
			msg = "";
			for (String key : autMap.keySet()) {
				msg = msg + key + " - " + autMap.get(key) + "\n";
			}

			logger.error("User AUT Credentials Validation failed\n [{}]", msg);
			throw new TenjinServletException(msg);
		}


		logger.info("Execution Validated successfully");
	}

	public void initExecution() throws TenjinServletException {
		try {
			this.testSet = new TestSetHelper().hydrateTestSetForRun(this.testSetRecordId,
					this.tjnSession.getProject().getId());
			logger.info("Hydrate Test Set --> Done");

			logger.info("Setting Execution Mode to [{}]", this.testSet.getMode());
			this.executionMode = this.testSet.getMode();

			if (Utilities.trim(this.executionMode).equalsIgnoreCase("")) {
				logger.info("Setting execution mode to GUI by default");
				this.executionMode = "GUI";
			}
		} catch (DatabaseException e) {
			logger.error("ERROR hydrating test set", e);
			throw new TenjinServletException("Could not fetch Test Set Information. Please contact Tenjin Support.");
		} catch (Exception e) {
			logger.error("ERROR hydrating test set", e);
			throw new TenjinServletException("Could not fetch Test Set Information. Please contact Tenjin Support.");
		}

		String lockUser = null;

		try {
			lockUser = new ExecutionHelper().isTestSetBeingExecuted(this.projConn, this.tjnSession.getProject().getId(),
					this.testSetRecordId);
			if (lockUser == null) {
				logger.info("TestSet concurrency check --> Done");
			} else {
				logger.error("TestSet Concurrency Check --> Failed -- Testset already being executed by {}", lockUser);
				throw new TenjinServletException(
						"This test set is currently being executed. Please wait until the current run is complete");
			}
		} catch (TenjinServletException e) {
			throw e;
		} catch (DatabaseException e) {
			logger.error("ERROR Test Set concurrency check failed", e);
			throw new TenjinServletException("An error occurred while fetching information about this test set");
		} catch (Exception e) {
			logger.error("ERROR Test Set concurrency check failed", e);
			throw new TenjinServletException("An error occurred while fetching information about this test set");
		}

		/****************************************************************************
		 * Added by Sriram for Req#TJN_243_11 (20-Dec-2016)
		 */
		logger.info("Filtering Test Steps if required");
		this.filterTestSteps();
		logger.info("Filtering done");
		/****************************************************************************
		 * Added by Sriram for Req#TJN_243_11 (20-Dec-2016) ends
		 */

	}

	/****************************************************************************
	 * Added by Sriram for Req#TJN_243_11 (20-Dec-2016)
	 */
	private boolean stepNeedsToBeExecuted(int stepRecordId) {
		if (this.stepsToExecute != null && this.stepsToExecute.size() > 0) {
			for (TestStep step : this.stepsToExecute) {
				if (step.getRecordId() == stepRecordId) {
					return true;
				}
			}
		}
		return false;
	}

	private void filterTestSteps() {
		ArrayList<TestCase> nTestCases = new ArrayList<TestCase>();

		for (TestCase tc : this.testSet.getTests()) {
			int eStepCount = 0;
			ArrayList<TestStep> steps = new ArrayList<TestStep>();
			for (TestStep step : tc.getTcSteps()) {
				if (this.stepNeedsToBeExecuted(step.getRecordId())) {
					steps.add(step);
					eStepCount++;
				} else {
					logger.warn("Step [{}] of Test Case [{}] will not be executed as it is filtered.", step.getId(),
							tc.getTcId());
				}
			}

			if (eStepCount > 0) {
				tc.setTcSteps(steps);
				nTestCases.add(tc);
			}
		}

		if (nTestCases.size() > 0) {
			this.testSet.setTests(nTestCases);
		}
	}

	/****************************************************************************
	 * Added by Sriram for Req#TJN_243_11 (20-Dec-2016) ends
	 */

	// Added by Sriram for Req#TJN_23_18
	public void loadRun(int runId, int projectId, String userName, String clientName, int execSpeed,
			String videoCaptureFlag, int screenshotOption, String browserType) throws TenjinServletException {

		logger.info("Fetching information about run [{}]", runId);
		try {
			this.testRun = new ResultsHelper().hydrateRun(runId, projectId);
			this.testRun.setProjectId(projectId);
		} catch (DatabaseException e) {
			
			logger.error("ERROR fetching run information");
			throw new TenjinServletException("Could not fetch run information. Please contact Tenjin Support.");
		}

		this.testSetRecordId = this.testRun.getTestSetRecordId();
		if (Utilities.trim(browserType).equalsIgnoreCase("")) {
			this.browserType = this.testRun.getBrowser_type();
		} else {
			this.testRun.setBrowser_type(browserType);
		}

		if (!Utilities.trim(userName).equalsIgnoreCase("")) {
			this.testRun.setUser(userName);
		}

		this.tjnSession = new TenjinSession();
		try {
			logger.info("Validating User");
			User user = new UserHelperNew().hydrateUser(this.testRun.getUser());
			tjnSession.setUser(user);
		} catch (DatabaseException e) {
			
			logger.error(e.getMessage());
			throw new TenjinServletException(
					"Could not validate user [" + this.testRun.getUser() + "]. Please contact Tenjin Support.");
		}

		try {

			logger.info("Validating Project");
			Project project = new ProjectHelper().hydrateProject(this.testRun.getProjectId());
			tjnSession.setProject(project);
		} catch (DatabaseException e) {
			
			logger.error(e.getMessage());
			throw new TenjinServletException("Could not validate project with ID [" + this.testRun.getProjectId()
					+ "]. Please contact Tenjin Support.");
		}
		/* Changed by Leelaparasad for defect T25IT-299 starts */
		/* if(!Utilities.trim(clientName).equalsIgnoreCase("")){ */
		if (!Utilities.trim(clientName).equalsIgnoreCase("") && !(Utilities.trim(clientName).equalsIgnoreCase("NA"))) {
			/* Changed by Leelaparasad for defect T25IT-299 ends */
			logger.info("Validating Client");
			try {
				RegisteredClient client = new ClientHelper().hydrateClient(clientName);

				if (client == null) {
					logger.error("Client [{}] is invalid", clientName);
					throw new TenjinServletException("The Client [" + clientName
							+ "] is invalid or may have been removed. Please choose a different client");
				}

				this.testRun.setMachine_ip(client.getHostName());
				this.testRun.setTargetPort(Integer.parseInt(client.getPort()));
			} catch (NumberFormatException e) {
				
				throw new TenjinServletException(
						"The specified client port was invalid. Please check client information and try again");
			} catch (DatabaseException e) {
				
				throw new TenjinServletException(
						"Could not validate Client Information. Please contact Tenjin Support.");
			}
		}
		/* Changed by Leelaparasad for defect T25IT-299 starts */
		else if (Utilities.trim(clientName).equalsIgnoreCase("NA")) {

			this.testRun.setMachine_ip("NA");
			this.testRun.setTargetPort(0);
		}
		/* Changed by Leelaparasad for defect T25IT-299 ends */
		else {
			try {
				RegisteredClient client = new ClientHelper().hydrateClient(this.testRun.getMachine_ip(),
						this.testRun.getTargetPort());
				this.testRun.setMachine_ip(client.getHostName());
				this.testRun.setTargetPort(Integer.parseInt(client.getPort()));
			} catch (NumberFormatException e) {
				
				throw new TenjinServletException(
						"The specified client port was invalid. Please check client information and try again");
			} catch (DatabaseException e) {
				
				throw new TenjinServletException(
						"Could not validate Client Information. Please contact Tenjin Support.");
			}
		}

		/************************************************************************
		 * commented by sameer for New Adapter Spec
		 */


		if (screenshotOption == 0) {
			this.screenshotOption = this.testRun.getScreenshotOption();
		} else {
			this.screenshotOption = screenshotOption;
		}
		this.videoCaptureFlag = videoCaptureFlag;
		this.execSpeed = execSpeed;

		this.performAllValidations();
		/*Added by Padmavathi for license starts*/
		Set<String> al =null;
		String msg = "";
		try {
			Map<String, String> adapterLicenseMap = this.validateAdapterCredentials(al);
			if (adapterLicenseMap != null && adapterLicenseMap.size() > 0) {
				msg = "";
				for (String key : adapterLicenseMap.keySet()) {
					msg = msg + key + " - " + adapterLicenseMap.get(key) + "\n";
				}
				logger.error("Adapter license Validation failed\n [{}]", msg);
				throw new TenjinServletException(msg);
			}
		} catch (TenjinConfigurationException e) {
			throw new TenjinServletException(e.getMessage());
		}
		/*Added by Padmavathi for license ends*/
		logger.info("Updating Run Information");
		try {
			this.testRun.setTestSet(this.testSet);
			this.testRun.setStartTimeStamp(new Timestamp(new Date().getTime()));
			new RunHelper().updateRunInfo(this.projConn, this.testRun, this.tdUidMap);
		} catch (DatabaseException e) {
			
			logger.error("Could not update run information", e);
			throw new TenjinServletException(
					"Could to load run due to an internal error. Please contact Tenjin Support.");
		}

		logger.info("Creating Task Manifest");
		this.steps = new ArrayList<ExecutionStep>();
		logger.info("Preparing Execution Steps...");
		for (TestCase tc : this.testSet.getTests()) {
			for (TestStep step : tc.getTcSteps()) {
				logger.info("Preparing Test Case [{}], Step [{}]", tc.getTcId(), step.getId());
				ExecutionStep eStep = step.getExecutionStep();
				/*********************************************
				 * Changed by Sriram for TENJINCG-170 (Schedule API Execution)
				 */
				
				/***
				 * Changed for TENJINCG-1175 (TENJINCG-1178) Avinash
				 */
				//if (!"api".equalsIgnoreCase(this.testSet.getMode())) {
				if (!"api".equalsIgnoreCase(step.getTxnMode())) {
					/***
					 * Changed for TENJINCG-1175 (TENJINCG-1178) Avinash - Ends
					 */
					eStep.setFunctionMenu(this.inScopeFunctions.get(step.getModuleCode()).getMenuContainer());
				}
				eStep.setExecutionMode(step.getTxnMode());
				/* Modified by Preeti for TENJINCG-503 starts */
				/* eStep.setApiCode(step.getApiCode()); */
				eStep.setApiCode(step.getModuleCode());
				/* Modified by Preeti for TENJINCG-503 ends */
				/*********************************************
				 * Changed by Sriram for TENJINCG-170 (Schedule API Execution)
				 */
				 /* Added by Ashiki for TENJINCG-1275 starts */
				eStep.setMsgVCode(step.getModuleCode());
				/* Added by Ashiki for TENJINCG-1275 ends */
				eStep.setTestCaseRecordId(tc.getTcRecId());
				this.steps.add(eStep);
			}
		}

		this.manifest = new TaskManifest();
		this.manifest.setRunId(this.testRun.getId());
		this.manifest.setAutMap(this.autsMap); // Need to populate
		this.manifest.setSteps(this.steps); // Need to Populate
		this.manifest.setTdUidMap(this.tdUidMap);
		this.manifest.setTestDataPaths(this.testDataFilePaths);
		this.manifest.setClientIp(this.testRun.getMachine_ip());
		this.manifest.setClientPort(this.testRun.getTargetPort());
		// TENJINCG-731 - By Sameer - Mobility - Start
		this.manifest.setDeviceId(this.deviceRecId);
		// TENJINCG-731 - By Sameer - Mobility - End
		this.manifest.setScreenshotOption(this.screenshotOption);
		/* changed by sharath for defect TENJINCG-115 on 14-02-2017 starts */
		this.manifest.setFieldTimeout(this.execSpeed);
		/* changed by sharath for defect TENJINCG-115 on 14-02-2017 ends */
		this.manifest.setBrowser(this.browserType);
		/************************************************************************
		 * commented by sameer for New Adapter Spec
		 */
		// this.manifest.setAutomationEngine(this.oem);
		// Parveen for TJN_24_11
		this.manifest.setVideoCaptureFlag(this.videoCaptureFlag);
		/* changed by nagababu for defect TENJINCG-120 on 14-02-2017 starts */
		this.manifest.setUserId(this.testRun.getUser());
		/* changed by nagababu for defect TENJINCG-120 on 14-02-2017 ends */

		// Parveen for TJN_24_11 ends
		/*********************************************
		 * Changed by Sriram for TENJINCG-170 (Schedule API Execution)
		 */
		/* this.manifest.setTaskType(AdapterTask.EXECUTE); */
		this.manifest.setTaskType(this.testRun.getTaskType());
		/*********************************************
		 * Changed by Sriram for TENJINCG-170 (Schedule API Execution) ends
		 */

		logger.info("Run [{}] loaded successfully", this.testRun.getId());

	}// Added by Sriram for Req#TJN_23_18 ends

	/************************************************************************
	 * Validates test data paths for a given functions executed in the specific run
	 * 
	 * @throws TenjinServletException
	 */
	
	
	/*modified by paneendra for TENJINCG-1268 starts */
	public TreeMap<String, String> validateUploadedTestDataPaths() throws TenjinServletException {
	/*modified by paneendra for TENJINCG-1268 ends */
		TreeMap<String, String> map = new TreeMap<String, String>();
		this.inScopeFunctions = new HashMap<String, Module>();
		Hashtable<String, String> projectTDPs = null;
		String scannedFunctions = "";

		try {
			projectTDPs = new ProjectHelper().hydrateUploadedTestDataMapForProject(this.projConn,
					this.tjnSession.getProject().getId());
			logger.debug("Hydrate Test data paths for project --> Done");
		} catch (Exception e) {
			logger.error("ERROR getting test data paths for project", e);
			throw new TenjinServletException("Could not retrieve test data paths for this project");
		}

		ExcelHandler excelHandler = new ExcelHandler();
		boolean validationPassed = true;
		for (TestCase tc : this.testSet.getTests()) {
			for (TestStep step : tc.getTcSteps()) {

				if (scannedFunctions == null || ("api".equalsIgnoreCase(step.getTxnMode()))
						|| !scannedFunctions.contains("!##!" + step.getModuleCode() + step.getAppId())) {
				
					int a = step.getAppId();
					String appId = Integer.toString(a);
					String tdp = projectTDPs.get(a + "|" + step.getModuleCode());
					
					scannedFunctions = scannedFunctions + "!##!" + step.getModuleCode() + step.getAppId();
				
					try {
						
						String fileName = step.getModuleCode() + "_TTD";
						
						if ("api".equalsIgnoreCase(step.getTxnMode())) {
							
							 //fileName = step.getApiCode() + "_" + step.getOperation() + "_TTD"; 
							fileName = step.getModuleCode() + "_" + step.getOperation() + "_TTD";
						}
			    /* modified by shruthi for 2.12 Test data path fix starts*/
				TestDataPath testdatapath=new TestDataHelper().hydrateAllTestDataPaths(this.tjnSession.getProject().getId(),step.getModuleCode(),this.tjnSession.getUser().getId());
				/* modified by shruthi for 2.12 Test data path fix ends*/
				Project project=new TestDataHelper().hydrateTestDataPath(this.tjnSession.getProject().getId());
				
				/*added by shruthi for TCGST-45 starts*/
				if(testdatapath == null ){
					logger.error("Test data path is null for {}", step.getModuleCode());
					map.put(step.getAppName() + "," + step.getModuleCode(),
						"No Test Data File is uploaded");
					validationPassed = false;
					continue;
				}
				/*	added by shruthi for TCGST-45 ends*/		
				String newtestdatpath=tdp.concat(File.separator)+testdatapath.getProjectName()+
								 File.separator+testdatapath.getAppName()+File.separator+testdatapath.getFunction()+File.separator+testdatapath.getUser();
						
			  /*String p = excelHandler.loadExcelToTempFolder(
								tdp.concat(File.separator).concat(this.tjnSession.getUser().getId()), fileName,Integer.parseInt(appId),this.tjnSession.getUser().getId());*/
						
				/*Modified by paneendra for TENJINCG-1268 starts*/
				/*String p = excelHandler.loadUploadedExcelToTempFolder(
								newtestdatpath, fileName,Integer.parseInt(appId),this.tjnSession.getUser().getId(),project,testdatapath,tempawsPath);*/
						
				String p = excelHandler.loadUploadedExcelToTempFolder(
						newtestdatpath, fileName,Integer.parseInt(appId),this.tjnSession.getUser().getId(),project,testdatapath);
				/*Modified by paneendra for TENJINCG-1268 ends*/
						logger.info("Test Data File Found for {} at path {}", step.getModuleCode(), p);
						
						if ("api".equalsIgnoreCase(step.getTxnMode())) {
							
							this.testDataFilePaths.put(step.getModuleCode() + "_" + step.getOperation(), p);
						} else {
							
							/* this.testDataFilePaths.put(step.getModuleCode(),p); */
							this.testDataFilePaths.put(step.getModuleCode() + step.getAppId(), p);
							
						}
						
					} catch (ExcelException e) {
						
						logger.error(e.getMessage(), e);
						
						map.put(step.getAppName() + "," + step.getModuleCode(),
								e.getMessage());
						
						validationPassed = false;
						continue;
					} catch (IOException e) {
						logger.error(e.getMessage(), e);
						map.put(step.getAppName() + "," + step.getModuleCode(),
								"The test data file was not found at path " + tdp);
						validationPassed = false;
						continue;
					} catch (DatabaseException e) {
						
						logger.error("Error ", e);
					}

				String mode="GUI";
					if (mode.equalsIgnoreCase(step.getTxnMode())) {
						
						logger.debug("Validating function [{}]", step.getModuleCode());
						try {
							Module module = new ModulesHelper().hydrateModuleInfo(a, step.getModuleCode());
							if (module == null) {
								logger.error("Function [{}] is invalid");
								map.put(step.getAppName() + "," + step.getModuleCode(),
										"The Function [" + step.getModuleCode() + "] is invalid.");
								validationPassed = false;
								continue;
							}
							
							ArrayList<LearnerResultBean> lrnrRuns = new ModulesHelper()
									.hydrateSuccessfulLearningHistory(step.getModuleCode(), a);
							if (lrnrRuns.size() == 0) {
								logger.error("Function [{}] Not learnt");
								map.put(step.getAppName() + "," + step.getModuleCode(),
										"The Function [" + step.getModuleCode() + "] is not learnt.");
								validationPassed = false;
								continue;
							}
							
							this.inScopeFunctions.put(module.getCode(), module);
						} catch (Exception e) {
							logger.error(e.getMessage(), e);
							map.put(step.getAppName() + "," + step.getModuleCode(),
									"Internal Error. Could not load test data file from " + tdp);
							validationPassed = false;
							continue;
						}
					}

					
				}
			}
		}

		if (!validationPassed) {
			map.put("STATUS", TenjinConstants.ACTION_RESULT_FAILURE);
		}

		return map;
	}

	
	

	/************************************************************************
	 * Validates test data files to check if there is any data provided for the test
	 * step.
	 * 
	 * @throws TenjinServletException
	 */
	public Map<String, List<String>> getTestDataRowUids() throws TenjinServletException {
		Map<Integer, List<String>> map = new TreeMap<Integer, List<String>>();
		Map<String, List<String>> mapToReturn = new TreeMap<String, List<String>>();
		if (this.testDataFilePaths.size() < 1) {
			throw new TenjinServletException("No Test data files available");
		}

		GuiDataHandler handler = new GuiDataHandlerImpl();

		// boolean validationPassed = true;
		ArrayList<TestCase> newTestCases = new ArrayList<TestCase>();
		for (TestCase tc : this.testSet.getTests()) {
			ArrayList<TestStep> newSteps = new ArrayList<TestStep>();
			for (TestStep step : tc.getTcSteps()) {
				try {
					/*****************************************************************************************
					 * Changed by Sriram for Bug 2219 (Tenjin v2.2) starts
					 */
					List<String> uids = null;
					/***
					 * Changed for TENJINCG-1175 (TENJINCG-1178) Avinash
					 */
					//if ("api".equalsIgnoreCase(this.testSet.getMode())) {
					if ("api".equalsIgnoreCase(step.getTxnMode())) {
						/***
						 * Changed for TENJINCG-1175 (TENJINCG-1178) Avinash - Ends
						 */
						uids = handler.getAllTdUidsInRange(
								this.testDataFilePaths.get(step.getModuleCode() + "_" + step.getOperation()),
								step.getDataId(), step.getRowsToExecute());
					}else {
						uids = handler.getAllTdUidsInRange(
								this.testDataFilePaths.get(step.getModuleCode() + step.getAppId()), step.getDataId(),
								step.getRowsToExecute());
					}
					/* Changed by Leelaprasad for API testing Demo on 27-09-2017 ends */
					/*****************************************************************************************
					 * Changed by Sriram for Bug 2219 (Tenjin v2.2) ends
					 */

					if (uids == null || uids.size() < 1) {
						logger.error("No Test data specified for Step [{}] in Test Case [{}]", step.getId(),
								tc.getTcId());
						mapToReturn.put(tc.getTcId() + ", " + step.getId(), null);
						continue;
					}

					map.put(step.getRecordId(), uids);
					step.setTotalTransactions(uids.size());
					newSteps.add(step);
					logger.debug("Iteration count for TDGID {} --> {}", step.getDataId(), step.getTotalTransactions());

				} catch (Exception e) {
					map.put(step.getRecordId(), null);
					mapToReturn.put(tc.getTcId() + ", " + step.getId(), null);
				}
				// map.put(step.getId(), Integer.toString(iterCount));
				/* For TENJINCG-311 */
				if (Utilities.trim(step.getValidationType()).equalsIgnoreCase("ui")) {
					logger.debug("Getting UI Validations for this step");
					try {
						Map<String, Map<String, List<UIValidation>>> validationsMap = new UIValidationHelper()
								.hydrateUIValidationsForStep(step.getRecordId(), "page");
						if (validationsMap != null) {
							JSONObject masterValJson = new JSONObject();
							for (String page : validationsMap.keySet()) {
								JSONArray valJsonArray = new JSONArray();
								Map<String, List<UIValidation>> pageMap = validationsMap.get(page);
								if (pageMap != null) {
									for (String type : pageMap.keySet()) {
										List<UIValidation> vals = pageMap.get(type);
										if (vals != null) {
											for (UIValidation val : vals) {
												JSONObject valJson = new JSONObject();

												// Validate Pre action fields
												String preSteps = val.getPreSteps();
												try {
													JSONArray preStepsJsonArray = this.validateUIValidationSteps(
															preSteps, step.getAppId(), step.getModuleCode(), page,
															true);
													JSONArray stepsJsonArray = this.validateUIValidationSteps(
															val.getSteps(), step.getAppId(), step.getModuleCode(), page,
															false);

													valJson.put("REC_ID", val.getRecordId());
													valJson.put("VAL_ID", val.getType().getId());
													valJson.put("PRE_STEPS", preStepsJsonArray);
													valJson.put("VAL_STEPS", stepsJsonArray);

													valJsonArray.put(valJson);
												} catch (Exception e) {
													
													List<String> retMsg = new ArrayList<String>();
													if (e instanceof TenjinServletException) {
														retMsg.add(e.getMessage());
														mapToReturn.put(tc.getTcId() + ", " + step.getId(), retMsg);
													} else {
														retMsg.add(
																"Could not verify UI Validation steps for this Test Step. Please contact Tenjin Support.");
														mapToReturn.put(tc.getTcId() + ", " + step.getId(), retMsg);
													}
												}
											}
										}
									}
								}

								masterValJson.put(page, valJsonArray);
							}

							if (this.uiValidationMap == null) {
								this.uiValidationMap = new TreeMap<Integer, String>();
							}
							this.uiValidationMap.put(step.getRecordId(), masterValJson.toString());
						}
					} catch (DatabaseException e) {
						
						List<String> retMsg = new ArrayList<String>();
						retMsg.add(
								"Could not verify UI Validation steps for this Test Step. Please contact Tenjin Support.");
						mapToReturn.put(tc.getTcId() + ", " + step.getId(), retMsg);
					} catch (JSONException e) {
						
						List<String> retMsg = new ArrayList<String>();
						retMsg.add(
								"Could not verify UI Validation steps for this Test Step. Please contact Tenjin Support.");
						mapToReturn.put(tc.getTcId() + ", " + step.getId(), retMsg);
					}
				}
				/* For TENJINCG-311 ends */
			}
			tc.setTcSteps(newSteps);
			newTestCases.add(tc);
		}
		this.testSet.setTests(newTestCases);
		this.tdUidMap = map;
		return mapToReturn;
	}

	String adapterName = null;
	/*List<String> adaptersName = null;*/
	Set<String> adaptersName = null;
	public Map<String, String> validateUserAutCredentials() throws TenjinServletException {
		Map<String, String> map = new HashMap<String, String>();
		this.autsMap = new HashMap<String, Map<String, Aut>>();
		adaptersName = new CopyOnWriteArraySet<String>();
		boolean validationPassed = true;
		String processedAuts = "";
		AutHelper helper = new AutHelper();
		for (TestCase tc : this.testSet.getTests()) {
			for (TestStep step : tc.getTcSteps()) {
				if (processedAuts != null && !processedAuts.contains("!##!" + step.getAppId())) {
					try {
						
						Map<String, Aut> aMap = this.autsMap.get(step.getAppName());

						if (aMap == null) {
							/*Modified by Ashiki for TJN252-97 starts*/
							ArrayList<Aut> allCreds = helper.hydrateAllUserAut(this.projConn,
									this.tjnSession.getUser().getId(), step.getAppId(),this.tjnSession.getProject().getId());
							/*Modified by Ashiki for TJN252-97 ends*/
							aMap = new HashMap<String, Aut>();
							boolean matchFound = false;
							 /*Modified by paneendra for VAPT fix starts*/
							CryptoUtilities cryptoUtilities=new CryptoUtilities();
							for (Aut aut : allCreds) {
								String dPass =cryptoUtilities.decrypt(aut.getPassword());
								/*String dPass = new Crypto().decrypt(aut.getPassword().split("!#!")[1],
										decodeHex(aut.getPassword().split("!#!")[0].toCharArray()));*/
								 /*Modified by paneendra for VAPT fix ends*/
								aut.setPassword(dPass);
								aMap.put(aut.getLoginUserType(), aut);

								adapterName = aut.getAdapterPackage();

								if (aut.getLoginUserType().equalsIgnoreCase(step.getAutLoginType())) {
									matchFound = true;
								}
							}

							this.autsMap.put(step.getAppName(), aMap);
							if (!matchFound) {
								logger.error(
										"User AUT Credentials not defined for Tenjin User {} for Application {}, Type {}",
										tjnSession.getUser().getId(), step.getAppName(), step.getAutLoginType());
								map.put(step.getAppName(), "No [" + step.getAutLoginType()
										+ "] credentials have been defined for this application under test");
								validationPassed = false;
							}

						} else {
							Aut aut = aMap.get(step.getAutLoginType());
							if (aut == null) {
								logger.error(
										"User AUT Credentials not defined for Tenjin User {} for Application {}, Type {}",
										tjnSession.getUser().getId(), step.getAppName(), step.getAutLoginType());
								map.put(step.getAppName(), "No [" + step.getAutLoginType()
										+ "] credentials have been defined for this application under test");
								validationPassed = false;
							}
						}
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						map.put(step.getAppName(), "No credentials have been defined for this application under test");
						validationPassed = false;
					}
				}
				/* Modified by Padmavathi for license starts */
				if(adapterName!=null){
					adaptersName.add(adapterName);
				}
				adapterName=null;
				/* Modified by Padmavathi for license ends */
			}
		}

		if (!validationPassed) {
			map.put("STATUS", TenjinConstants.ACTION_RESULT_FAILURE);
		}
		return map;
	}
	/* Uncommented by Padmavathi for license starts */
	/*Added by Prem for TENJINCG-843 */
	public Map<String, String> validateAdapterCredentials(Set<String> al) throws TenjinConfigurationException {
		Map<String, String> map = new ConcurrentHashMap<String, String>();
		Map<String, Integer> countMap = new ConcurrentHashMap<String, Integer>();
		// List<String> adaptersName = new ArrayList<String>();
		int limit = 0;
		int count = 0;
		al = adaptersName;
		String adpterVal = null;
		List<String> adpternameLice = new ArrayList<>();
		License license = (License) CacheUtils.getObjectFromRunCache("licensedetails");
		for (String string : al) {
			adpterVal = string;
			for (LicenseComponent licenseComponent :license.getLicenseComponents()) {
				adpterVal.replace("[", "").replace("]", "").replace(" ", "").split(",");
				/*if (licenseComponent.getType().equalsIgnoreCase("ADAPTER")) {*/
				String adapterName=licenseComponent.getProductComponent().getName();
					if (adapterName.equalsIgnoreCase(adpterVal)) {
						try {
							new LicenseHandler().licenseComponentActiveValidation(licenseComponent);
						} catch (LicenseInactive e1) {
							logger.error("license component is Inactive {}", adapter);
							map.put(adpterVal,e1.getMessage());
							continue;
						}
						CacheUtils.initializeRunCache();
						try {
							if (CacheUtils.getObjectFromRunCache(adpterVal).equals(null)) {
							} else {
								count = (int) CacheUtils.getObjectFromRunCache(adpterVal);
								limit = licenseComponent.getLimit();
								if (count <= limit) {
									CacheUtils.putObjectInRunCache(adpterVal, count + 1);
								}
							}
							if (count > limit) {
								logger.error("Max number of Tenjin User for Application {}", adapter);
								map.put(adpterVal, "You have reached the maximum limit for ["+adpterVal+"] adapter.");
							}
							adpternameLice.add(adapterName);
							countMap.put(adapterName, limit);
						} catch (NullPointerException e) {
							count = 1;
							CacheUtils.putObjectInRunCache(adpterVal, count);
							adpternameLice.add(adapterName);
							limit = licenseComponent.getLimit();
							countMap.put(adapterName, limit);
							if (count > limit) {
								logger.error("Max number of Tenjin User for Application {}", adapter);
								map.put(adpterVal, "You have reached the maximum limit for ["+adpterVal+"] adapter.");
							}
						}

						this.adapter = adpternameLice.toString();
					} else {
						logger.error("No adapter License availabel{}", adapterName);
						map.put(adpterVal, "You need a license for ["+adpterVal+"] adapter to perform this operation. Please contact Tenjin Support.");
					}
			}

		}
		List<String> st = adpternameLice;
		List<String> removeAdaterList=new ArrayList<String>();
		for (String s : st) {
			count = (int) CacheUtils.getObjectFromRunCache(s);
			map.containsKey(s);
			if (countMap.containsKey(s)) {
				Integer se = countMap.get(s);
				if (count > se) {
					logger.error("Max number of Tenjin User for Application {}", adapter);
					map.put(s, "You have reached the maximum limit for ["+s+"] adapter");
					//count = (int) CacheUtils.getObjectFromRunCache(s);
					CacheUtils.putObjectInRunCache(s, count - 1);
				}else{
					map.remove(s);
					removeAdaterList.add(s);
				}

			} 

		}
		/*Added by Padmavathi for license starts*/
		if(map != null && map.size() > 0){
			for(String s : removeAdaterList){
				count = (int) CacheUtils.getObjectFromRunCache(s);
				CacheUtils.putObjectInRunCache(s, count - 1);
			}
		}
/*Added by Padmavathi for license ends*/
		return map;
	}
/* Uncommented by Padmavathi for license ends */
	public void createRun() throws TenjinServletException {

		if (this.testSet == null || this.testSet.getTests() == null || this.testSet.getTests() == null) {
			throw new TenjinServletException("Could not start run. Please contact Tenjin Support");
		}

		
		
		logger.info("Preparing Execution Steps...");
		/*Changes by Avinash for TENJINCG-1018 starts*/
		populateExecutionSteps();
		/*Changes by Avinash for TENJINCG-1018 ends*/
		this.testRun = new TestRun();
		this.testRun.setTestSet(this.testSet);
		this.testRun.setUser(this.tjnSession.getUser().getId());
		this.testRun.setStatus("Not Started");
		this.testRun.setStartTimeStamp(new Timestamp(new Date().getTime()));
		this.testRun.setProjectId(this.tjnSession.getProject().getId());
		this.testRun.setMachine_ip(this.targetIp);
		this.testRun.setBrowser_type(this.browserType);
		this.testRun.setTargetPort(this.targetPort);
		this.testRun.setProjectId(this.tjnSession.getProject().getId());
		this.testRun.setProjectName(this.tjnSession.getProject().getName());
		this.testRun.setDomainName(this.tjnSession.getProject().getDomain());

		/* Added by Roshni for TENJINCG-259 */
		this.testRun.setParentRunId(this.parentId);
		
		/****
		 * Changed by Sriram for TENJINCG-1175 (TENJINCG-1178)
		
		/* Added by Ashiki for TENJINCG-1275 starts */
		try {
			if(msgTaskType.equalsIgnoreCase("MSG")) {
				testRun.setTaskType(BridgeProcess.MSGEXECUTE);
			}else {
				testRun.setTaskType(BridgeProcess.EXECUTE);
			}	
		}catch (NullPointerException e) {
			 testRun.setTaskType(BridgeProcess.EXECUTE);
		}
		/* Added by Ashiki for TENJINCG-1275 ends */
		if(!StringUtils.isEmpty(deviceRecId)) {
			testRun.setDeviceRecId(Integer.parseInt(deviceRecId));
		}
		
		/****
		 * Changed by Sriram for TENJINCG-1175 (TENJINCG-1178) ends
		 */ 
		this.testRun.setScreenshotOption(this.screenshotOption);

		try {
			/* Added by Sahana for pCloudy:starts */
			RegisteredClient objRegClient= new ClientHelper().hydrateClientByHostName(this.targetIp);
			if(objRegClient != null && objRegClient.getDeviceFarmCheck()!=null && objRegClient.getDeviceFarmCheck().equalsIgnoreCase("Y"))
				this.testRun.setDeviceFarmFlag(objRegClient.getDeviceFarmCheck());
			else
				this.testRun.setDeviceFarmFlag("N");
			/* Added by Sahana for pCloudy:ends */
			/* Added by Ramya for TENJINCG-832 starts */
			/* new RunHelper().persistNewRun(this.projConn,this.testRun, this.tdUidMap); */
			runId = new RunHelper().persistNewRun(this.projConn, this.testRun, this.tdUidMap);
			/*Changed by Pushpalatha for TENJINCG-909 starts*/
			/*new RunHelper().insertExecutionSettings(runId, this.testRun.getMachine_ip(), this.browserType,
					this.execSpeed, this.screenshotOption);*/
			/*Modified by Preeti for V2.8-161 starts*/
			/*new RunHelper().insertExecutionSettings(runId, this.testRun.getMachine_ip(), this.browserType,
					 this.screenshotOption);*/
			/*Modified by Padmavathi for scheduling API */
			new RunHelper().insertExecutionSettings(runId,this.testRun.getTestSet(), this.testRun.getMachine_ip(), this.browserType,
					 this.screenshotOption,this.testRun.getTaskType());
			/*Modified by Padmavathi for scheduling API  ends*/
			/*Modified by Preeti for V2.8-161 ends*/
			/*Changed by Pushpa for TENJINCG-909 ends*/
			/* Added by Ramya for TENJINCG-832 ends */

		} catch (DatabaseException e) {
			logger.error("ERROR persisting run", e);
			throw new TenjinServletException("Could not start the test run due to an internal error");
		} catch (SQLException e) {
			
			logger.error("Error ", e);
		}

		this.manifest = new TaskManifest();
		this.manifest.setRunId(this.testRun.getId());
		this.manifest.setAutMap(this.autsMap); // Need to populate
		this.manifest.setSteps(this.steps); // Need to Populate
		this.manifest.setTdUidMap(this.tdUidMap);
		this.manifest.setTestDataPaths(this.testDataFilePaths);
		this.manifest.setClientIp(this.targetIp);
		this.manifest.setClientPort(this.targetPort);
		// TENJINCG-731 - By Sameer - Mobility - Start
		this.manifest.setDeviceId(this.deviceRecId);
		// TENJINCG-731 - By Sameer - Mobility - End
		this.manifest.setScreenshotOption(this.screenshotOption);
		/* changed by sharath for defect TENJINCG-115 on 14-02-2017 starts */
		this.manifest.setFieldTimeout(this.execSpeed);
		/* changed by sharath for defect TENJINCG-115 on 14-02-2017 ends */
		this.manifest.setBrowser(this.browserType);
		/************************************************************************
		 * commented by sameer for New Adapter Spec
		 */
		// this.manifest.setAutomationEngine(this.oem);
		// Parveen for TJN_24_11
		this.manifest.setVideoCaptureFlag(this.videoCaptureFlag);
		// Parveen for TJN_24_11 ends

		this.manifest.setProjectId(this.tjnSession.getProject().getId());
		this.manifest.setUserId(this.tjnSession.getUser().getId());

		/********************************
		 * Added by Sriram for TENJINCG-168 (API Execution)
		 */
		this.manifest.setTaskType(this.testRun.getTaskType());
		/********************************
		 * Added by Sriram for TENJINCG-168 (API Execution) ends
		 */
		/* Added by Padmavathi for TENJINCG-647 starts */
		this.manifest.setPassedStepsStatusMap(this.passedStepsStatusMap);
		/* Added by Padmavathi for TENJINCG-647 ends */
		/* Added By Prem for TENJINCG-843 */
		/* Uncommented by Padmavathi for license starts */
		this.manifest.setAdapterName(this.adapter);
		/* Uncommented by Padmavathi for license ends */
	}
	/*Changes by Avinash for TENJINCG-1018 starts*/
	private void populateExecutionSteps() {
		int stepcount = 0;
		if(this.steps == null){
			this.steps = new ArrayList<ExecutionStep>();
			for (TestCase tc : this.testSet.getTests()) {
				for (TestStep step : tc.getTcSteps()) {
					logger.info("Preparing Test Case [{}], Step [{}]", tc.getTcId(), step.getId());
					ExecutionStep eStep = step.getExecutionStep();
					/********************************
					 * Changed by Sriram for TENJINCG-168 (API Execution)
					 */
					/***
					 * Changed for TENJINCG-1175 (TENJINCG-1178) Avinash
					 */
					/* Added by Ashiki for TENJINCG-1275 starts */
					if("msg".equalsIgnoreCase(step.getTxnMode())){
						eStep.setFileName(step.getFileName());
						eStep.setFilePath(step.getFilePath());
						msgTaskType="MSG";
						
					}
					/* Added by Ashiki for TENJINCG-1275 ends */
					else if (!"api".equalsIgnoreCase(step.getTxnMode())) {
						/***
						 * Changed for TENJINCG-1175 (TENJINCG-1178) Avinash - Ends
						 */
						eStep.setFunctionMenu(this.inScopeFunctions.get(step.getModuleCode()).getMenuContainer());
					}
					/********************************
					 * Changed by Sriram for TENJINCG-168 (API Execution) ends
					 */
					eStep.setTestCaseRecordId(tc.getTcRecId());
					/********************************
					 * Added by Sriram for TENJINCG-168 (API Execution)
					 */
					eStep.setExecutionMode(step.getTxnMode());
					/* Modified by Preeti for TENJINCG-503 starts */
					/* eStep.setApiCode(step.getApiCode()); */
					eStep.setApiCode(step.getModuleCode());
					/* Modified by Preeti for TENJINCG-503 ends */
	
					/* Added by Padmavathi for TENJINCG-647 (Dependency Rules) starts */
	
					// if executing single step not setting dependency rules
	
					if (this.parentId != 0 || stepcount != 0) {
						eStep.setDependencyRule(step.getCustomRules());
						eStep.setDependentStepId(step.getDependentStepRecId());
					}
					/* Added by Padmavathi for TENJINCG-647 (Dependency Rules) ends */
	
					/********************************
					 * Added by Sriram for TENJINCG-168 (API Execution) ends
					 */
					/* For TENJINCG-311 */
					if (this.uiValidationMap != null) {
						String uiValString = Utilities.trim(this.uiValidationMap.get(step.getRecordId()));
						if (uiValString.length() > 0) {
							eStep.setUiValidationString(uiValString);
						}
					}
					/* For TENJINCG-311 ends */
	
					this.steps.add(eStep);
					stepcount++;
				}
			}
		}
	}
	/*Changes by Avinash for TENJINCG-1018 ends*/
	public TaskManifest getTaskManifest() {
		return this.manifest;
	}

	public TestRun getRun() {
		return this.testRun;
	}

	public static void main(String[] args) throws TenjinServletException, DatabaseException, SQLException {

		ExecutorGateway gateway = new ExecutorGateway();
		gateway.loadRun(1, 1, "sriram", "Test Machine", 0, "N", 2, BrowserType.CHROME);

		Connection conn = null;
		if (conn == null) {
			logger.info("No DB Connection. Creating one");
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		}

		logger.info("Running task");
		new ExecutionHelper().updateRunStatus(conn, TenjinConstants.SCRIPT_EXECUTING, gateway.getRun().getId());
		BridgeProcessor handler = new BridgeProcessor(gateway.getTaskManifest());
		handler.createTask();
		handler.runTask();

		try {
			conn.close();
		} catch (Exception ignore) {
		}
	}

	/* Added by Roshni for TENJINCG-259 starts */
	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	/* Added by Roshni for TENJINCG-259 ends */
	/* For TENJINCG-311 */
	private JSONArray validateUIValidationSteps(String validationString, int appId, String moduleCode,
			String pageAreaName, boolean preSteps) throws TenjinServletException {
		validationString = Utilities.trim(validationString);
		if (validationString.length() < 1) {
			return new JSONArray();
		}

		JSONArray retArray = new JSONArray();

		try {
			JSONArray jArray = new JSONArray(validationString);
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject field = jArray.getJSONObject(i);

				String label = field.getString("FLD_LABEL");
				try {
					TestObject t = null;

					if (Utilities.trim(label).equalsIgnoreCase("button")) {
						t = new TestObject();
						t.setLabel(label);
					} else {
						/* Fix for T25IT-162 */
						/* t = new LearningHelper().hydrateTestObject(moduleCode, appId, label); */
						t = new LearningHelper().hydrateTestObjectForExecution(moduleCode, pageAreaName, appId, label);
						/* Fix for T25IT-162 ends */
					}

					if (t != null) {
						JSONObject fJson = t.toJson();
						if (preSteps) {
							fJson.put("ACTION", "INPUT");
						} else {
							fJson.put("ACTION", "FUNCTION");
						}
						fJson.put("DATA", field.getString("DATA"));
						retArray.put(fJson);
					} else {
						throw new TenjinServletException(
								"Failed to validate UI Validation steps for this Test Step. Field [" + label
										+ "] on page [" + pageAreaName
										+ "] is invalid. Please re-learn this function.");
					}
				} catch (Exception e) {
					
					logger.error("ERROR validating UI Validation step for field {} on page {}", label, pageAreaName, e);
					throw new TenjinServletException(
							"Could not validate UI Validation steps due to an internal error. Please contact Tenjin Support");
				} 
			}

		} catch (JSONException e) {
			
			logger.error("ERROR validating UI Steps", e);
			throw new TenjinServletException(
					"Could not validate UI Validation steps due to an internal error. Please contact Tenjin Support");
		}

		return retArray;
	}
	/* For TENJINCG-311 ends */

	
	
	//Code changes for OAuth 2.0 requirement TENJINCG-1018- Avinash - Starts
	public Map<String, Map<String, String>> validateoAuthTokens() {
		Map<String, Map<String, String>> returnMap = new HashMap<String, Map<String, String>>();
		populateExecutionSteps();
		for(String appName : this.autsMap.keySet()){
			Map<String, String> strMap = new HashMap<String, String>();
			for (ExecutionStep step : steps) {
				if(step.getAppName().equalsIgnoreCase(appName)){
					Map<String, Aut> userTypes = this.autsMap.get(appName);
					for(String userType : userTypes.keySet()){
						if(userType.equalsIgnoreCase(step.getAutLoginType())){
							Aut aut = userTypes.get(userType);
							if(oAuthDetailsAvail(aut)){
								if(aut.getAccessToken() == null){
									strMap.put(userType, "Access Token not yet generated.");
									returnMap.put(appName, strMap);
								}else{
									long secs = (new Date().getTime() - aut.getTokenGenerationTime().getTime())/1000;
									if(Integer.valueOf(aut.getTokenValidity()) < secs ){
										logger.debug("Access Token is invalid.");
										strMap.put(userType, "Invalid Access Token. Please generate to proceed with execution.");
										returnMap.put(appName, strMap);
									}
								}
							}
						}
					}
				}
			}
		}
		return returnMap;
	}
	
	private boolean oAuthDetailsAvail(Aut aut) {
		/*Added by Padmavathi for  TNJN27-87 starts*/
		if(aut.getGrantType().equalsIgnoreCase("implicit")&& aut.getClientId() != null){
			return true;
		}
		/*Added by Padmavathi for  TNJN27-87 ends*/
		else  if(aut.getAccessTokenUrl() != null && aut.getClientId() != null & aut.getClientSecret() != null)
			return true;
		
		
		return false;
	}
	//Code changes for OAuth 2.0 requirement TENJINCG-1018- Avinash - Ends
/* Added by Ashiki for TENJINCG-1275 starts */
	public String getMsgTaskType() {
		return msgTaskType;
	}

	public void setMsgTaskType(String msgTaskType) {
		this.msgTaskType = msgTaskType;
	}
	/* Added by Ashiki for TENJINCG-1275 ends */
}
