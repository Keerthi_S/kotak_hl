/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExtractorProgressView.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 10-Nov-2016           Sriram Sridharan          Newly Added For 
*/


package com.ycs.tenjin.run;

import com.ycs.tenjin.bridge.pojo.aut.ExtractionRecord;
import com.ycs.tenjin.bridge.pojo.aut.Module;

public class ExtractorProgressView {
	private TestRun run;
	private String progressPercentage;
	private Module currentFunction;
	private ExtractionRecord currentRecord;
	private String screenStatus;
	private String message;
	
	
	public String getScreenStatus() {
		return screenStatus;
	}
	public void setScreenStatus(String screenStatus) {
		this.screenStatus = screenStatus;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public TestRun getRun() {
		return run;
	}
	public void setRun(TestRun run) {
		this.run = run;
	}
	public String getProgressPercentage() {
		return progressPercentage;
	}
	public void setProgressPercentage(String progressPercentage) {
		this.progressPercentage = progressPercentage;
	}
	public Module getCurrentFunction() {
		return currentFunction;
	}
	public void setCurrentFunction(Module currentFunction) {
		this.currentFunction = currentFunction;
	}
	public ExtractionRecord getCurrentRecord() {
		return currentRecord;
	}
	public void setCurrentRecord(ExtractionRecord currentRecord) {
		this.currentRecord = currentRecord;
	}
	
	
}
