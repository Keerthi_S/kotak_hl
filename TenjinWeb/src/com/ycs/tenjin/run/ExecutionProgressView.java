/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutionProgressView.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 16-Nov-2017		Sriram Sridharan		Fix to remove decimals in Execution Percentage
*/

package com.ycs.tenjin.run;

import com.ycs.tenjin.project.TestCase;
import com.ycs.tenjin.project.TestStep;

public class ExecutionProgressView {
	private TestRun run;
	private TestCase currentTestCase;
	private TestStep currentTestStep;
	private String currentIterationUid;
	private int currentIteration;
	private int totalIterations;
	private int totalCompletedIterations;
	private int totalRemainingIterations;
	private int totalPass;
	private int totalFail;
	private int totalError;
	/* Fixed by Sriram to remove decimals in execution progress percentage */
	/*private double executionProgress;*/
	int executionProgress;
	/* Fixed by Sriram to remove decimals in execution progress percentage ends*/
	private String autoRefreshFlag;
	private String refreshTimeout;
	
	
	/**
	 * @return the autoRefreshFlag
	 */
	public String getAutoRefreshFlag() {
		return autoRefreshFlag;
	}
	/**
	 * @param autoRefreshFlag the autoRefreshFlag to set
	 */
	public void setAutoRefreshFlag(String autoRefreshFlag) {
		this.autoRefreshFlag = autoRefreshFlag;
	}
	/**
	 * @return the refreshTimeout
	 */
	public String getRefreshTimeout() {
		return refreshTimeout;
	}
	/**
	 * @param refreshTimeout the refreshTimeout to set
	 */
	public void setRefreshTimeout(String refreshTimeout) {
		this.refreshTimeout = refreshTimeout;
	}
	/**
	 * @return the run
	 */
	public TestRun getRun() {
		return run;
	}
	/**
	 * @return the totalRemainingIterations
	 */
	public int getTotalRemainingIterations() {
		return totalRemainingIterations;
	}
	/**
	 * @param totalRemainingIterations the totalRemainingIterations to set
	 */
	public void setTotalRemainingIterations(int totalRemainingIterations) {
		this.totalRemainingIterations = totalRemainingIterations;
	}
	/**
	 * @return the totalPass
	 */
	public int getTotalPass() {
		return totalPass;
	}
	/**
	 * @param totalPass the totalPass to set
	 */
	public void setTotalPass(int totalPass) {
		this.totalPass = totalPass;
	}
	/**
	 * @return the totalFail
	 */
	public int getTotalFail() {
		return totalFail;
	}
	/**
	 * @param totalFail the totalFail to set
	 */
	public void setTotalFail(int totalFail) {
		this.totalFail = totalFail;
	}
	/**
	 * @return the totalError
	 */
	public int getTotalError() {
		return totalError;
	}
	/**
	 * @param totalError the totalError to set
	 */
	public void setTotalError(int totalError) {
		this.totalError = totalError;
	}
	/**
	 * @param run the run to set
	 */
	public void setRun(TestRun run) {
		this.run = run;
	}
	/**
	 * @return the currentTestCase
	 */
	public TestCase getCurrentTestCase() {
		return currentTestCase;
	}
	/**
	 * @param currentTestCase the currentTestCase to set
	 */
	public void setCurrentTestCase(TestCase currentTestCase) {
		this.currentTestCase = currentTestCase;
	}
	/**
	 * @return the currentTestStep
	 */
	public TestStep getCurrentTestStep() {
		return currentTestStep;
	}
	/**
	 * @param currentTestStep the currentTestStep to set
	 */
	public void setCurrentTestStep(TestStep currentTestStep) {
		this.currentTestStep = currentTestStep;
	}
	/**
	 * @return the currentIterationUid
	 */
	public String getCurrentIterationUid() {
		return currentIterationUid;
	}
	/**
	 * @param currentIterationUid the currentIterationUid to set
	 */
	public void setCurrentIterationUid(String currentIterationUid) {
		this.currentIterationUid = currentIterationUid;
	}
	/**
	 * @return the currentIteration
	 */
	public int getCurrentIteration() {
		return currentIteration;
	}
	/**
	 * @param currentIteration the currentIteration to set
	 */
	public void setCurrentIteration(int currentIteration) {
		this.currentIteration = currentIteration;
	}
	/**
	 * @return the totalIterations
	 */
	public int getTotalIterations() {
		return totalIterations;
	}
	/**
	 * @param totalIterations the totalIterations to set
	 */
	public void setTotalIterations(int totalIterations) {
		this.totalIterations = totalIterations;
	}
	/**
	 * @return the totalCompletedIterations
	 */
	public int getTotalCompletedIterations() {
		return totalCompletedIterations;
	}
	/**
	 * @param totalCompletedIterations the totalCompletedIterations to set
	 */
	public void setTotalCompletedIterations(int totalCompletedIterations) {
		this.totalCompletedIterations = totalCompletedIterations;
	}
	/**
	 * @return the executionProgress
	 */
	/* Fixed by Sriram to remove decimals in execution progress percentage */
	public int getExecutionProgress() {
		return executionProgress;
	}
	/* Fixed by Sriram to remove decimals in execution progress percentage ends*/
	/**
	 * @param executionProgress the executionProgress to set
	 */
	
	/* Fixed by Sriram to remove decimals in execution progress percentage */
	
	public void setExecutionProgress(int executionProgress) {
		this.executionProgress = executionProgress;
	}
	
	/* Fixed by Sriram to remove decimals in execution progress percentage ends*/
}
