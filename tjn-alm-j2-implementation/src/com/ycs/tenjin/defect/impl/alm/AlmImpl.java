/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: AlmImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY                         DESCRIPTION
 * 18-Nov-2016          Gangadhar Badagi                   Newly added for defect
* 01-Aug-2017		    Manish								TENJINCG-74,75
* 14-11-2018			Ashiki							  TENJINCG-901
 *  
 */



package com.ycs.tenjin.defect.impl.alm;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.alm.bean.DataList;
import com.ycs.alm.bean.Fields;
import com.ycs.alm.client.SetterRequest;
import com.ycs.alm.services.AlmService;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.defect.Attachment;
import com.ycs.tenjin.defect.Comment;
import com.ycs.tenjin.defect.Defect;
import com.ycs.tenjin.defect.DefectManager;
import com.ycs.tenjin.defect.EDMException;
import com.ycs.tenjin.defect.UnreachableEDMException;
import com.ycs.tenjin.defect.impl.GenericEDMImpl;


/*********Implementing Defect Manager and Genericimpl***************/

public class AlmImpl extends GenericEDMImpl implements DefectManager {
	
	private static final Logger logger=LoggerFactory.getLogger(AlmImpl.class);
	AlmService service=new AlmService();
	String cookieToken="";
	SetterRequest setterRequest=new SetterRequest();
	String domain="";
	String project="";
	
	
	public AlmImpl(String baseUrl, String userId, String password) {
		super(baseUrl, userId, password);
	}
	
	/*******************Logging into HP-ALM*********************/
	@Override
	public String login() throws UnreachableEDMException, EDMException {
		this.cookieToken = service.login(this.baseUrl, this.userId,
				this.password);
		return this.cookieToken;
	}
	
	/*************Setting DOMAIN and PROJECTS***************/
		@Override
	public void setProjectDesc(String projectDescription) {
		String[] domproject = projectDescription.split(":");
		this.domain = domproject[0];
		this.project = domproject[1];
		logger.info("----domain and project------[{},{}]", domproject[0],
				domproject[1]);
	}

		/****************Validating url*******************/
	@Override
	public boolean isUrlValid() throws UnreachableEDMException {
		@SuppressWarnings("unused")
		boolean value = false;
		try {
			value = service.isUrlValid(this.baseUrl);
			return true;
		} catch (Exception e) {
			logger.error("Entered url is wrong,Please check the url");
			return false;
		}
	}

	/****************Posting multiple defects**********************/
	@Override
	public List<Defect> postDefects(List<Defect> defects)
			throws UnreachableEDMException, EDMException {
		List<Defect> defectList = new ArrayList<Defect>();
		defectList.addAll(defects);
		for (Defect defect : defectList) {
			DataList postdefectdata = setterRequest.PostDefectObj(defect);
			DataList postRes = service.postDefect(postdefectdata, this.baseUrl,
					this.domain, this.project, this.cookieToken);
			defect.setIdentifier(String.valueOf(postRes.getData().get(0)
					.getId()));

		}

		return defectList;
	}

	/*******************Getting details of a defect***************/
	@Override
	public Defect getDefect(String defectIdentifier) {
		Defect defect = null;
		try {
			Fields defectRes = service.getDefect(this.baseUrl,
					defectIdentifier, this.domain, this.project,
					this.cookieToken);
			defect = new Defect();
			defect.setIdentifier(String.valueOf(defectRes.getId()));
			defect.setEdmProjectName(String.valueOf(defectRes.getProject()));
			/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
			/*Added by Ashiki for req TENJINCG-901 starts*/
			if(defectRes.getDescription()!=null){
			/*Added by Ashiki for req TENJINCG-901 ends*/
			if(htmlToText(defectRes.getDescription())!=null){
				defect.setDescription(htmlToText(defectRes.getDescription()));
			}
			}
			/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 ends*/
			try {
				defect.setModifiedOn(getStringToTimeStamp(defectRes
						.getLast_modified()));
				defect.setCreatedOn(getStringToTimestampDateOnly(defectRes
						.getCreation_time()));
			} catch (Exception e) {
				throw new EDMException("Defect Id " + defectIdentifier
						+ " is not existed");
			}
			defect.setPriority(defectRes.getPriority());
			defect.setSeverity(defectRes.getSeverity());
			defect.setStatus(defectRes.getStatus());
			defect.setSummary(defectRes.getName());

		} catch (NullPointerException e) {
			logger.error("Error"+e);
			try {
				throw new EDMException("Defect Id " + defectIdentifier
						+ " is not existed");
			} catch (EDMException e1) {
				e1.printStackTrace();
			}
		} catch (Exception e) {
			logger.error("Internal error occured while fetching the details for defect");
		}
		return defect;

	}
	
	/******************Getting details of multiple defects*******************/
	@Override
	public List<Defect> getDefects(String[] defectIdentifiers)
			throws UnreachableEDMException, EDMException {
		List<Defect> listDefect = new ArrayList<Defect>();
		for (String id : defectIdentifiers) {
			Defect defect = getDefect(id);
			listDefect.add(defect);
		}

		return listDefect;
	}
	
	/*************Getting details of multiple defects*************/
	@Override
	public List<Defect> getDefects(List<String> defectIdentifiers)
			throws UnreachableEDMException, EDMException {
		List<Defect> listDefect = new ArrayList<Defect>();
		for (String id : defectIdentifiers) {
			Defect defect = getDefect(id);
			listDefect.add(defect);
		}
		return listDefect;
	}

	/******************Posting the attachments for a defect*************/
	@Override
	public void postDefectAttachment(Defect defect, Attachment attachment)
			throws UnreachableEDMException, EDMException {
		try {
			Fields fields = service.postAttachment(this.baseUrl, this.domain,
					this.project, attachment, defect.getIdentifier(),
					this.cookieToken);
			logger.info("----Attachment Id for defect Id [{}] is [{}]", fields
					.getEntity().getId(), fields.getId());
			attachment.setAttachmentIdentifier(String.valueOf(fields.getId()));
		} catch (ClientProtocolException e) {
			logger.error("Internal error occured while posting the attachments for defect");
		} catch (IOException e) {
			logger.error("Internal error occured while posting the attachments for defect");
		}
	}
	/***************Posting the attachments for defect******************/
	@Override
	public void postDefectAttachment(String defectIdentifier,
			String projectIdentifer, Attachment attachment)
			throws UnreachableEDMException, EDMException {
		try {
			Fields fields = service.postAttachment(this.baseUrl, this.domain,
					this.project, attachment, defectIdentifier,
					this.cookieToken);
			logger.info("----Attachment Id for defect Id [{}] is [{}]", fields
					.getEntity().getId(), fields.getId());
			attachment.setAttachmentIdentifier(String.valueOf(fields.getId()));
		} catch (ClientProtocolException e) {
			logger.error("Internal error occured while posting the attachments for defect");
		} catch (IOException e) {
			logger.error("Internal error occured while posting the attachments for defect");
		}

	}
	
	/**************Posting the comments for specific defect***********/
	@Override
	public void postComment(Defect defect, Comment comment)
			throws UnreachableEDMException, EDMException {
		try {
			Fields fld = new Fields();
			fld.setDev_comments(comment.getText());
			service.postComment(this.baseUrl,
					defect.getIdentifier(), fld, this.domain, this.project,
					this.userId, this.cookieToken);
		} catch (Exception e) {
			logger.error("Unable to post the comment for {}",
					defect.getIdentifier());
		}

	}

	/**
	 *  Get list of defect attachment.
	 **/
	@Override
	public List<Attachment> getDefectAttachments(String defectIdentifier,
			String projectIdentifier) throws UnreachableEDMException,
			EDMException {
		List<Attachment> attachmelist = service.getAttachments(this.baseUrl,
				this.domain, this.project, defectIdentifier, this.cookieToken);
		try {
			logger.info("Find your attachments in "
					+ TenjinConfiguration.getProperty("MEDIA_REPO_PATH"));
		} catch (TenjinConfigurationException e) {
			logger.error("Internal error occured while getting the attachments for defect");
		}
		// logger.info("Find your attachments in "+Properties.path);
		return attachmelist;
	}

	/**************Getting attachmnets for defect*************************/
	@Override
	public Attachment getDefectAttachment(String defectIdentifier,
			String projectIdentifier, String attachmentIdentifier)
			throws UnreachableEDMException, EDMException {
		Attachment attachment = service.getAttachments(this.baseUrl,
				attachmentIdentifier, this.domain, this.project,
				defectIdentifier, this.cookieToken);

		try {
			logger.info("Find your attachments in "
					+ TenjinConfiguration.getProperty("MEDIA_REPO_PATH"));
		} catch (TenjinConfigurationException e) {
			logger.error("Internal error occured while getting the attachments for defect");
		}
		// logger.info("Find your attachments in "+Properties.path);
		return attachment;
	}

	/****************Getting the attachments for multiple attachment Ids*************************/
	@Override
	public List<Attachment> getDefectAttachments(String defectIdentifier,
			String projectIdentifier, List<String> attachmentIdentifiers)
			throws UnreachableEDMException, EDMException {

		List<Attachment> list = new ArrayList<Attachment>();
		try {
			for (String attachmentIdentifier : attachmentIdentifiers) {
				Attachment attachment = service.getAttachments(this.baseUrl,
						attachmentIdentifier, this.domain, this.project,
						defectIdentifier, this.cookieToken);
				list.add(attachment);
			}
		} catch (Exception e) {
			logger.error("Error while fetching arttachmnets");
		}
		try {
			logger.info("Find your attachments in "
					+ TenjinConfiguration.getProperty("MEDIA_REPO_PATH"));
		} catch (TenjinConfigurationException e) {
			logger.error("Internal error occured while getting the attachments for defect");
		}
		// logger.info("Find your attachments in "+Properties.path);
		return list;
	}
	
	/*****************Fetching list of projects*****************/
	@Override
	public JSONArray getAllProjects() throws UnreachableEDMException,
			EDMException {
		String allProjects = service.getProjects(this.baseUrl, this.domain,
				this.cookieToken);
		JSONArray finalJsonArray = null;
		try {
			JSONObject allProjectsJson = new JSONObject(allProjects);
			JSONArray jsonArray = allProjectsJson.getJSONArray("results");
			/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 starts*/
			finalJsonArray = new JSONArray();
			/*changed by manish for req TENJINCG-74,75 on 01 Aug 2017 ends*/
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				JSONObject jsonNewArray = new JSONObject();
				jsonNewArray.put("pname", jsonObject.get("name"));
				logger.info("----Project name is----[{}]",
						jsonNewArray.getString("pname"));
				finalJsonArray.put(jsonNewArray);
			}
		} catch (JSONException e1) {
			logger.error("Json parse exception while fetching projects");
			// e1.printStackTrace();
		}
		return finalJsonArray;
	}

	/***********Logging out from HP-ALM*******************/
	@Override
	public void logout() throws UnreachableEDMException, EDMException {

		try {
			service.logout(this.baseUrl, this.cookieToken);
			logger.info("You are successfully logged out");
		} catch (Exception e) {
			logger.error("Unable to logout due to internal error");
		}
	}
	
	/**************Getting the comment details for s defect*************/
	@Override
	public List<Comment> getComments(String defectIdentifier)
			throws UnreachableEDMException, EDMException {

		List<Comment> listCommts = new ArrayList<Comment>();
		try {
			String commentOut = service.getComments(this.baseUrl,
					defectIdentifier, this.domain, this.project,
					this.cookieToken);

			JSONObject allProjectsJson;

			allProjectsJson = new JSONObject(commentOut);
			Comment comObj = new Comment();
			comObj.setIdentifier(String.valueOf(allProjectsJson.getInt("id")));
			String text = htmlToText(allProjectsJson.getString("dev-comments"));
			comObj.setText(text.replace(
					"________________________________________", ""));
			listCommts.add(comObj);
			logger.info("-----Your comment details are----[{}]",
					"Your comment--->" + comObj.getText() + ","
							+ "Your defect Id --->" + comObj.getIdentifier());
		} catch (JSONException e) {
			logger.error("Json parse exception while feching comments");
		} catch (Exception e) {
			/*
			 * throw new EDMException(
			 * "Unable to get comment details,Please check the defect Id or Network connection"
			 * );
			 */
			logger.error("Unable to get comment details,Please check the defect Id or Network connection");
		}
		return listCommts;
	}
	
	/****************Posting a defect*****************/
	@Override
	public void postDefectDetails(Defect defect)
			throws UnreachableEDMException, EDMException {

		DataList postdefectdata = setterRequest.PostDefectObj(defect);
		try {
			DataList postRes = service.postDefect(postdefectdata, this.baseUrl,
					this.domain, this.project, this.cookieToken);
			defect.setIdentifier(String.valueOf(postRes.getData().get(0)
					.getId()));
		} catch (NullPointerException e1) {
			logger.error("Internal error occured while posting the defect");
			throw new EDMException("Could not post defect due to an internal error. Please contact Tenjin Support.");
		} catch (Exception e) {
			logger.error("Internal error occured while posting the defect");
			throw new EDMException("Could not post defect due to an internal error. Please contact Tenjin Support.");
		}

	}

	/******************Converting String to Time stamp*************/
	public Timestamp getStringToTimeStamp(String time) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		java.util.Date parsedDate = null;
		try {
			parsedDate = dateFormat.parse(time);
		} catch (ParseException e) {

		}
		Timestamp ts = new java.sql.Timestamp(parsedDate.getTime());
		return ts;
	}

	/***********Converting String to Date***************/
	public Timestamp getStringToTimestampDateOnly(String time) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date parsedDate = null;
		try {
			parsedDate = dateFormat.parse(time);
		} catch (ParseException e) {

		}
		Timestamp ts = new java.sql.Timestamp(parsedDate.getTime());
		return ts;
	}
	
	/*******************Getting text content from html**********************/
	public static String htmlToText(String html) {
		String text = Jsoup.parse(html).body().text();
		return text;
	}

	@Override
	public Defect getDefect(Defect defect) throws EDMException {
		// TODO Auto-generated method stub
		return null;
	}
}
