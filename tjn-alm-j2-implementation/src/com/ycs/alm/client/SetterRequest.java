
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SetterRequest.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 12-Oct-2016          Gangadhar Badagi        Newly added for defect
 * 11-Nov-2016           Abhilash K N            Changes made in post setter request,commented few parameters.
 */

package com.ycs.alm.client;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.alm.bean.DataList;
import com.ycs.alm.bean.Fields;
import com.ycs.alm.util.Properties;
import com.ycs.tenjin.defect.Defect;

public class SetterRequest {

	private static final Logger logger=LoggerFactory.getLogger(SetterRequest.class);
	
	
	/*************Defect Response conversion*************/
	public void getDefectReq(Fields defectRes) {
		Defect defect = new Defect();
		defect.setIdentifier(String.valueOf(defectRes.getId()));
		defect.setDescription(defectRes.getDescription());
		defect.setModifiedOn(getStringToTimeStamp(defectRes.getLast_modified()));
		defect.setCreatedOn(getStringToTimeStamp(defectRes.getCreation_time()));
		defect.setPriority(defectRes.getPriority());
		// defect.setComments(comments);
		defect.setSummary(defectRes.getName());
		defect.setStatus(defectRes.getStatus());
	}

	/********Converting from String to Timestamp*******/
	public Timestamp getStringToTimeStamp(String time) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		java.util.Date parsedDate = null;
		try {
			parsedDate = dateFormat.parse(time);
		} catch (ParseException e) {

		}
		Timestamp ts = new java.sql.Timestamp(parsedDate.getTime());
		return ts;
	}

	/********Converting from Timestamp to String*******/
	public Timestamp getStringToTimestampDateOnly(String time) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date parsedDate = null;
		try {
			parsedDate = dateFormat.parse(time);
		} catch (ParseException e) {

		}
		Timestamp ts = new java.sql.Timestamp(parsedDate.getTime());
		return ts;
	}

	/************************Setting the details for posting a defect******************/
	public DataList PostDefectObj(Defect defect) {
		DataList dlist = new DataList();
		try {
			Fields d = new Fields();
			d.setType(Properties.TYPE);
			d.setName(defect.getSummary());
			d.setProject(defect.getEdmProjectName());
			d.setDescription(defect.getDescription());
			d.setPriority(defect.getPriority());
			d.setSeverity(defect.getSeverity());
			d.setDetected_by(defect.getCreatedBy());

			String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(defect
					.getCreatedOn());
			d.setCreation_time(timeStamp);
			/*
			 * d.setUser_01(defect.getAppName());
			 * d.setUser_02(defect.getDefectType());
			 * d.setUser_03(defect.getFunctionCode());
			 * d.setUser_04(defect.getTestCaseId());
			 * d.setUser_05(defect.getTestStepId());
			 * d.setUser_06(defect.getRunId());
			 */
			d.setStatus(defect.getStatus());
			// d.setOwner(defect.getAssignedTo());
			// d.setDev_comments(defect.getComments().get(0).getText());
			List<Fields> flist = new ArrayList<Fields>();
			flist.add(d);

			dlist.setData(flist);
		} catch (NullPointerException e) {
			logger.error("Error"+e);
		} catch (Exception e) {
			logger.error("Error"+e);
		}
		return dlist;

	}

}
