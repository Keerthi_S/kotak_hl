
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Fields.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 14-Oct-2016          Gangadhar Badagi & Abhilash Kn       Newly added for defect
 * 
 */

package com.ycs.alm.bean;

import com.google.gson.annotations.SerializedName;

public class Fields {
	private String type;
	@SerializedName("detection-version")
	private String detection_version;
	private String subject;
	private String description;
	@SerializedName("target-rcyc")
	private Target_Rcyc target_rcyc;
	private String project;
	@SerializedName("has-linkage")
	private String has_linkage;
	@SerializedName("last-modified")
	private String last_modified;
	@SerializedName("has-others-linkage")
	private String has_others_linkage;
	private String attachment;
	@SerializedName("cycle-id")
	private String cycle_id;
	@SerializedName("request-type")
	private String request_type;
	@SerializedName("creation-time")
	private String creation_time;
	@SerializedName("actual-fix-time")
	private String actual_fix_time;
	private Integer id;
	@SerializedName("run-reference")
	private String run_reference;
	@SerializedName("request-note")
	private String request_note;
	@SerializedName("request-server")
	private String request_server;
	private String severity;
	@SerializedName("to-mail")
	private String to_mail;
	private String owner;
	@SerializedName("detected-by")
	private String detected_by;
	@SerializedName("step-reference")
	private String step_reference;
	@SerializedName("estimated-fix-time")
	private String estimated_fix_time;
	private String reproducible;
	@SerializedName("ver-stamp")
	private String ver_stamp;
	@SerializedName("request-id")
	private Integer request_id;
	private String priority;
	@SerializedName("cycle-reference")
	private String cycle_reference;
	@SerializedName("user-06")
	private String user_06;
	private String environment;
	@SerializedName("target-rel")
	private Target_Rel target_rel;
	@SerializedName("test-reference")
	private String test_reference;
	@SerializedName("planned-closing-ver")
	private String planned_closing_ver;
	@SerializedName("extended-reference")
	private String extended_reference;
	@SerializedName("dev-comments")
	private String dev_comments;
	@SerializedName("detected-in-rcyc")
	private Detected_In_Rcyc detected_in_rcyc;
	@SerializedName("closing-version")
	private String closing_version;
	private String name;
	@SerializedName("has-change")
	private String has_change;
	@SerializedName("user-01")
	private String user_01;
	@SerializedName("user-02")
	private String user_02;
	@SerializedName("user-03")
	private String user_03;
	@SerializedName("user-04")
	private String user_04;
	@SerializedName("user-05")
	private String user_05;
	@SerializedName("detected-in-rel")
	private Detected_In_Rel detected_in_rel;
	private String status;
	@SerializedName("closing-date")
	private String closing_date;
	@SerializedName("vc-cur-ver")
	private String vc_cur_ver;
	@SerializedName("vc-user-name")
	private String vc_user_name;
	@SerializedName("file-size")
	private String file_size;
	@SerializedName("ref-subtype")
	private String ref_subtype;
	@SerializedName("ref-type")
	private String ref_type;
	private Detected_In_Rel entity;


	public String getVc_cur_ver() {
		return vc_cur_ver;
	}
	public void setVc_cur_ver(String vc_cur_ver) {
		this.vc_cur_ver = vc_cur_ver;
	}
	public String getVc_user_name() {
		return vc_user_name;
	}
	public void setVc_user_name(String vc_user_name) {
		this.vc_user_name = vc_user_name;
	}
	public String getFile_size() {
		return file_size;
	}
	public void setFile_size(String file_size) {
		this.file_size = file_size;
	}
	public String getRef_subtype() {
		return ref_subtype;
	}
	public void setRef_subtype(String ref_subtype) {
		this.ref_subtype = ref_subtype;
	}
	public String getRef_type() {
		return ref_type;
	}
	public void setRef_type(String ref_type) {
		this.ref_type = ref_type;
	}
	public Detected_In_Rel getEntity() {
		return entity;
	}
	public void setEntity(Detected_In_Rel entity) {
		this.entity = entity;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getVer_stamp() {
		return ver_stamp;
	}
	public void setVer_stamp(String ver_stamp) {
		this.ver_stamp = ver_stamp;
	}
	public String getDetection_version() {
		return detection_version;
	}
	public void setDetection_version(String detection_version) {
		this.detection_version = detection_version;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getHas_linkage() {
		return has_linkage;
	}
	public void setHas_linkage(String has_linkage) {
		this.has_linkage = has_linkage;
	}
	public String getLast_modified() {
		return last_modified;
	}
	public void setLast_modified(String last_modified) {
		this.last_modified = last_modified;
	}
	public String getHas_others_linkage() {
		return has_others_linkage;
	}
	public void setHas_others_linkage(String has_others_linkage) {
		this.has_others_linkage = has_others_linkage;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getCycle_id() {
		return cycle_id;
	}
	public void setCycle_id(String cycle_id) {
		this.cycle_id = cycle_id;
	}
	public String getRequest_type() {
		return request_type;
	}
	public void setRequest_type(String request_type) {
		this.request_type = request_type;
	}
	public String getCreation_time() {
		return creation_time;
	}
	public void setCreation_time(String creation_time) {
		this.creation_time = creation_time;
	}
	public String getActual_fix_time() {
		return actual_fix_time;
	}
	public void setActual_fix_time(String actual_fix_time) {
		this.actual_fix_time = actual_fix_time;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRun_reference() {
		return run_reference;
	}
	public void setRun_reference(String run_reference) {
		this.run_reference = run_reference;
	}
	public String getRequest_note() {
		return request_note;
	}
	public void setRequest_note(String request_note) {
		this.request_note = request_note;
	}
	public String getRequest_server() {
		return request_server;
	}
	public void setRequest_server(String request_server) {
		this.request_server = request_server;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getTo_mail() {
		return to_mail;
	}
	public void setTo_mail(String to_mail) {
		this.to_mail = to_mail;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getDetected_by() {
		return detected_by;
	}
	public void setDetected_by(String detected_by) {
		this.detected_by = detected_by;
	}
	public String getStep_reference() {
		return step_reference;
	}
	public void setStep_reference(String step_reference) {
		this.step_reference = step_reference;
	}
	public String getEstimated_fix_time() {
		return estimated_fix_time;
	}
	public void setEstimated_fix_time(String estimated_fix_time) {
		this.estimated_fix_time = estimated_fix_time;
	}
	public String getReproducible() {
		return reproducible;
	}
	public void setReproducible(String reproducible) {
		this.reproducible = reproducible;
	}

	public Integer getRequest_id() {
		return request_id;
	}
	public void setRequest_id(Integer request_id) {
		this.request_id = request_id;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getCycle_reference() {
		return cycle_reference;
	}
	public void setCycle_reference(String cycle_reference) {
		this.cycle_reference = cycle_reference;
	}
	public String getUser_06() {
		return user_06;
	}
	public void setUser_06(String user_06) {
		this.user_06 = user_06;
	}
	public String getEnvironment() {
		return environment;
	}
	public void setEnvironment(String environment) {
		this.environment = environment;
	}


	public String getTest_reference() {
		return test_reference;
	}
	public void setTest_reference(String test_reference) {
		this.test_reference = test_reference;
	}
	public String getPlanned_closing_ver() {
		return planned_closing_ver;
	}
	public void setPlanned_closing_ver(String planned_closing_ver) {
		this.planned_closing_ver = planned_closing_ver;
	}
	public String getExtended_reference() {
		return extended_reference;
	}
	public void setExtended_reference(String extended_reference) {
		this.extended_reference = extended_reference;
	}
	public String getDev_comments() {
		return dev_comments;
	}
	public void setDev_comments(String dev_comments) {
		this.dev_comments = dev_comments;
	}


	public String getClosing_version() {
		return closing_version;
	}
	public void setClosing_version(String closing_version) {
		this.closing_version = closing_version;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHas_change() {
		return has_change;
	}
	public void setHas_change(String has_change) {
		this.has_change = has_change;
	}
	public String getUser_01() {
		return user_01;
	}
	public void setUser_01(String user_01) {
		this.user_01 = user_01;
	}
	public String getUser_02() {
		return user_02;
	}
	public void setUser_02(String user_02) {
		this.user_02 = user_02;
	}
	public String getUser_03() {
		return user_03;
	}
	public void setUser_03(String user_03) {
		this.user_03 = user_03;
	}
	public String getUser_04() {
		return user_04;
	}
	public void setUser_04(String user_04) {
		this.user_04 = user_04;
	}
	public String getUser_05() {
		return user_05;
	}
	public void setUser_05(String user_05) {
		this.user_05 = user_05;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getClosing_date() {
		return closing_date;
	}
	public void setClosing_date(String closing_date) {
		this.closing_date = closing_date;
	}
	public Detected_In_Rel getDetected_in_rel() {
		return detected_in_rel;
	}
	public void setDetected_in_rel(Detected_In_Rel detected_in_rel) {
		this.detected_in_rel = detected_in_rel;
	}
	public Target_Rcyc getTarget_rcyc() {
		return target_rcyc;
	}
	public void setTarget_rcyc(Target_Rcyc target_rcyc) {
		this.target_rcyc = target_rcyc;
	}
	public Target_Rel getTarget_rel() {
		return target_rel;
	}
	public void setTarget_rel(Target_Rel target_rel) {
		this.target_rel = target_rel;
	}
	public Detected_In_Rcyc getDetected_in_rcyc() {
		return detected_in_rcyc;
	}
	public void setDetected_in_rcyc(Detected_In_Rcyc detected_in_rcyc) {
		this.detected_in_rcyc = detected_in_rcyc;
	}



}
