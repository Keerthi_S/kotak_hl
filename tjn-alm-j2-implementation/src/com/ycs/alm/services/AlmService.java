
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AlmService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 18-Nov-2016          Gangadhar Badagi        Newly added
*/

package com.ycs.alm.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.alm.bean.DataList;
import com.ycs.alm.bean.Fields;
import com.ycs.alm.util.Properties;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.defect.Attachment;
import com.ycs.tenjin.defect.Comment;
import com.ycs.tenjin.defect.EDMException;


/*******************Invoking services for specific function******************/

public class AlmService {
	
	private static final Logger logger=LoggerFactory.getLogger(AlmService.class);
	
	private static ClientConfig config = new ClientConfig();
	
	
	
	/*********Validating Url***********/
	
	public boolean isUrlValid(String baseUrl) {
		logger.info("Calling --> GET {}", baseUrl);

		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(baseUrl);
		Response response = target.request(MediaType.APPLICATION_JSON).get();

		logger.info("Response from server recieved");
		int status = response.getStatus();

		if (status == 200 || status == 201) {
			logger.info("Status - [{}]", status);
			logger.info("Url is validated");
			return true;
		} else {
			logger.error("Status - [{}]", status);
			logger.error("Entered  url is incorrect,please check the url");
			return false;
		}
	}
		
	/**
	 *  Token generation for login.
	 **/
	public static String getToken(String username, String password) {
		String NME_PSWD = username + ":" + password;
		System.out.println(NME_PSWD);
		byte[] encode = Base64.encodeBase64(NME_PSWD.getBytes());
		return new String(encode);
	}
   
	/*******Logging into Application and Generating CookieToken************/
	public String login(String baseUrl, String username, String password)
			throws EDMException {
		String token = getToken(username, password);
		String targetUrl = baseUrl + "api/authentication/sign-in";
		String cookie = "";

		logger.info("Calling --> GET {}", targetUrl);
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(targetUrl);

		Response response = target.request()
				.header(HttpHeaders.AUTHORIZATION, "Basic " + token).get();

		Map<String, NewCookie> cookieValues = response.getCookies();
		for (Entry<String, NewCookie> value : cookieValues.entrySet()) {
			NewCookie newCookie = value.getValue();
			cookie = cookie + newCookie.getName() + "=" + newCookie.getValue()
					+ ";";

		}
		logger.info("Response from server recieved");
		int status = response.getStatus();
		String message = response.readEntity(String.class);

		if (status == 200 || status == 201) {
			logger.info("Status - [{}]", status);
			logger.info("Message - [{}]", message);
			return cookie;
		} else {
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			throw new EDMException(status + " - " + message);
		}
	}
	
	/***************** Posting a Defect ******************/
	public DataList postDefect(DataList postdefectdata, String baseUrl,
			String domain, String project, String cookieToken)
			throws EDMException {

		Gson gson = new Gson();
		String input = gson.toJson(postdefectdata);
		DataList dataList = null;
		String targetUrl = baseUrl + "api/domains/" + domain + "/projects/"
				+ project + "/defects";

		String out = GenericAlmServices.post(targetUrl, cookieToken, input);
		dataList = gson.fromJson(out, DataList.class);
		return dataList;
	}
	
	/*******Posting comment  for a defect****************/
	public String postComment(String baseUrl, String defectId, Fields field,
			String domain, String project, String userId, String cookieToken)
			throws EDMException {
		Gson gson = new Gson();
		String out = "";
		String input = "";
		String targetUrl = baseUrl + "/api/domains/" + domain + "/projects/"
				+ project + "/defects/" + defectId;

		String com = getComments(baseUrl, defectId, domain, project,
				cookieToken);

		List<Comment> listCommts = new ArrayList<Comment>();
		JSONObject allProjectsJson;
		Comment comObj = new Comment();
		String preCmt = "";
		try {
			allProjectsJson = new JSONObject(com);
			comObj.setIdentifier(String.valueOf(allProjectsJson.getInt("id")));
			comObj.setText(allProjectsJson.getString("dev-comments"));
			listCommts.add(comObj);
			preCmt = comObj.getText();
		} catch (Exception e) {
			logger.error("Error"+e);
		}
		try {
			if (!preCmt.equalsIgnoreCase("null")) {
				String fld = field.getDev_comments();
				String formatedDate = dateConversion();
				field.setDev_comments("<html><font color=\"blue\"></font>"
						+ preCmt
						+ "<br/>"
						+ "<html>________________________________________</html>"
						+ "<br/>" + "<font color=\"blue\">" + userId + ", "
						+ formatedDate + ":</font> " + fld + "</html>");
				input = gson.toJson(field);
				System.out.println("---Out---" + input);
			} else {
				String formatedDate = dateConversion();
				field.setDev_comments("<html>________________________________________</html>"
						+ "<br/>"
						+ "<html><font color=\"blue\">"
						+ userId
						+ ", "
						+ formatedDate
						+ ":</font> "
						+ field.getDev_comments());
				input = gson.toJson(field);
			}
			out = GenericAlmServices.put(targetUrl, cookieToken, input);
			return out;
		} catch (Exception e) {
			logger.error("Error occured while fetching the comments for a defect");
		}
		return out;
	}

	/********Posting Attachment for defect***********/
	public Fields postAttachment(String baseUrl, String domain, String project,
			Attachment attachment, String defectIdentifier, String cookieToken)
			throws ClientProtocolException, IOException {

		Gson gson = new Gson();
		Fields fields = new Fields();
		String path = attachment.getFilePath();
		String lastValue = attachment.getFileName();
		File fileUpload = new File(path);
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost postRequest = new org.apache.http.client.methods.HttpPost(
				baseUrl + "/api/domains/" + domain + "/projects/" + project
						+ "/attachments/");

		postRequest.setHeader(HttpHeaders.COOKIE, cookieToken);
		MultipartEntityBuilder entity = MultipartEntityBuilder.create();
		entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		entity.addTextBody("entity.type", Properties.TYPE,
				ContentType.TEXT_PLAIN);
		entity.addTextBody("entity.id", defectIdentifier,
				ContentType.TEXT_PLAIN);
		entity.addTextBody("filename", lastValue, ContentType.TEXT_PLAIN);
		entity.addBinaryBody("content", fileUpload);
		postRequest.setEntity(entity.build());
		org.apache.http.HttpResponse response = httpClient.execute(postRequest);
		String json = EntityUtils.toString(response.getEntity());
		fields = gson.fromJson(json, Fields.class);

		return fields;
	}
	
	/********Getting defect details**************/
	public Fields getDefect(String baseUrl, String defectId, String domain,
			String project, String cookieToken) throws EDMException {
		Gson gson = new Gson();
		Fields defectRes = null;
		String targetUrl = baseUrl + "/api/domains/" + domain + "/projects/"
				+ project + "/defects/" + defectId;

		String out = GenericAlmServices.get(targetUrl, cookieToken);
		defectRes = gson.fromJson(out, Fields.class);
		return defectRes;
	}
	
	/*****************Fetching list of projects************/
	public String getProjects(String baseUrl, String domain, String cookieToken)
			throws EDMException {
		String targetUrl = baseUrl + "api/domains/" + domain + "/projects";
		String out = GenericAlmServices.get(targetUrl, cookieToken);
		return out;
	}

	/***********Getting comments of defect*************/
	public String getComments(String baseUrl, String defectId, String domain,
			String project, String cookieToken) throws EDMException {
		String targetUrl = baseUrl + "/api/domains/" + domain + "/projects/"
				+ project + "/defects/" + defectId + "?fields=dev-comments";
		String out = GenericAlmServices.get(targetUrl, cookieToken);
		return out;
	}
	
	/***************** To get List of Attachments********************/
	public List<Attachment> getAttachments(String baseUrl, String domain,
			String project, String defectId, String cookieToken)
			throws EDMException {

		JSONArray jarray = null;
		List<Attachment> atchList = new ArrayList<Attachment>();
		// try {
		String targetUrl = baseUrl + "/api/domains/" + domain + "/projects/"
				+ project + "/attachments";
		String out = GenericAlmServices.get(targetUrl, cookieToken);
		System.out.println("--Output----" + out);
		JSONObject jobject;
		try {
			jobject = new JSONObject(out);
			jobject.getString("data");
			jarray = new JSONArray(jobject.getString("data"));
			for (int i = 0; i < jarray.length(); i++) {
				String url = null;
				JSONObject jsonobj = jarray.getJSONObject(i);
				JSONObject jjobject = new JSONObject(
						jsonobj.getString("entity"));
				if ((jjobject.getString("id")).equalsIgnoreCase(defectId)) {
					Attachment attach = new Attachment();
					String fileName = jsonobj.getString("name");
					String attachment_Id = jsonobj.getString("id");
					attach.setAttachmentIdentifier(attachment_Id);
					attach.setFileName(fileName);
					url = targetUrl + "/" + attachment_Id;
					InputStream input = getInputStream(url, cookieToken);
					// Path
					// filedirectory=Paths.get(Properties.path+defectId+"\\"+attachment_Id);
					Path filedirectory = null;
					try {
						filedirectory = Paths.get(TenjinConfiguration
								.getProperty("MEDIA_REPO_PATH")
								+ "\\Defects\\"
								+ defectId + "\\" + attachment_Id);
					} catch (TenjinConfigurationException e1) {
						logger.error("Internal error occured while creating the file directory");
					}
					if (!Files.exists(filedirectory)) {
						try {
							Files.createDirectories(filedirectory);
							FileOutputStream output = new FileOutputStream(
									new File(filedirectory + "\\" + fileName));
							IOUtils.copy(input, output);
							input.close();
							output.close();
							attach.setFilePath(filedirectory + "\\" + fileName);
							atchList.add(attach);
							logger.info(
									"Fetching attachment {} with file name {} from ALM is successfull!",
									attachment_Id, fileName);
						} catch (IOException e) {
							logger.error("Error"+e);
						}
					}
				}
			}
		} catch (JSONException e) {
			logger.error("Json parse exception while fetching the attachments");
		}
		return atchList;
	}
	
	/***************** Getting  attachments for particular defect ********************/
	public Attachment getAttachments(String baseUrl, String attachmentId,
			String domain, String project, String defectIdentifier,
			String cookieToken) {

		Attachment attachment = new Attachment();

		String targetUrl = baseUrl + "/api/domains/" + domain + "/projects/"
				+ project + "/attachments/" + attachmentId;

		String response = null;
		try {
			response = GenericAlmServices.get(targetUrl, cookieToken);
		} catch (EDMException e1) {
			logger.error("Internal error occured while fetching the attachments");
		}

		try {
			JSONObject jsonobj = new JSONObject(response);
			String fileName = jsonobj.getString("name");
			String attachment_Id = jsonobj.getString("id");
			attachment.setAttachmentIdentifier(attachment_Id);
			attachment.setFileName(fileName);
			InputStream input = getInputStream(targetUrl, cookieToken);

			// Path
			// filedirectory=Paths.get(Properties.path+defectIdentifier+"\\"+attachment_Id);
			Path filedirectory = Paths.get(TenjinConfiguration
					.getProperty("MEDIA_REPO_PATH")
					+ "\\Defects\\"
					+ defectIdentifier + "\\" + attachment_Id);
			if (!Files.exists(filedirectory)) {
				Files.createDirectories(filedirectory);
			}
			FileOutputStream output = new FileOutputStream(new File(
					filedirectory + "\\" + fileName));
			IOUtils.copy(input, output);
			input.close();
			output.close();

		} catch (FileNotFoundException e) {
			logger.error("File is not found");
		} catch (JSONException e) {
			logger.error("JSON parse exception while fetching attachmnents");
		} catch (IOException e) {
			logger.error("Unable to write and read the file");
		} catch (Exception e) {
			logger.error("Unable to get details for attachmnet");
		}
		return attachment;
	}
	
	/*******************Getting input stream for fetching the attachments***********************/
	public InputStream getInputStream(String url, String cookieToken)
			throws EDMException {
		logger.info("Calling --> GET {}", url);
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(url);

		Response response = target.request()
				.accept(MediaType.APPLICATION_OCTET_STREAM_TYPE)
				.header(HttpHeaders.COOKIE, cookieToken).get();
		logger.info("Response from server recieved");
		int status = response.getStatus();
		if (status == 200 || status == 201) {
			logger.info("Status - [{}]", status);
			InputStream message = response.readEntity(InputStream.class);
			return message;
		} else {
			logger.error("Status - [{}]", status);
			String message = response.readEntity(String.class);
			logger.error("Message - [{}]", message);
			throw new EDMException(status + " - " + message);
		}
	}
	
	/************Logging out from HP-ALM*******************/
	@SuppressWarnings("unused")
	public void logout(String baseUrl, String cookieToken) throws EDMException {
		String targetUrl = baseUrl + "/api/authentication/sign-out";
	}
	
	/********Date conversion to (dd-MM-yyyy)  format***********/
	public String dateConversion() throws ParseException {
		java.util.Date fecha = new java.util.Date();
		DateFormat formatter = new SimpleDateFormat(
				"EEE MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);
		Date date;
		date = (Date) formatter.parse(fecha.toString());

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		String formatedDate = cal.get(Calendar.DATE) + "-"
				+ (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);
		return formatedDate;
	}
}