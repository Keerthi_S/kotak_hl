/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GenericRallyServices.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 */
package com.ycs.rally.services;


import java.io.InputStream;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.defect.EDMException;

public class GenericRallyServices {
	private static final Logger logger=LoggerFactory.getLogger(GenericRallyServices.class);
	private static ClientConfig config = new ClientConfig();

	public String get(String url, String username,String password) throws EDMException {

		logger.info("Calling --> GET {}", url);

		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(url);

		Response response = target.request()
				.header(username,password).get();

		logger.info("Response from server recieved");
		int status = response.getStatus();
		String message = response.readEntity(String.class);

		if (status == 200 || status == 201) {
			logger.info("Status - [{}]", status);
			logger.info("Message - [{}]", message);
			return message;
		}

		else if(status == 401){
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			throw new EDMException("Authentication failed. Please check user credentials and try again");
		}else {
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			throw new EDMException("Defect does not exist, Either someone has deleted the defect or Defect Management project.");

		}
	}

	public String post(String targetUrl, String input,String username,String password) throws EDMException {

		logger.info("Calling --> POST {}", targetUrl);

		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(targetUrl);

		Response response = target
				.request(MediaType.APPLICATION_JSON)
				//.header(HttpHeaders.AUTHORIZATION,"Basic "+ rallyToken)
				.header(username,password)
				.post(Entity.entity(input, MediaType.APPLICATION_JSON),
						Response.class);

		logger.info("Response from server recieved");
		int status = response.getStatus();
		String message = response.readEntity(String.class);

		if (status == 200 || status == 201) {
			logger.info("Status - [{}]", status);
			logger.info("Message - [{}]", message);
			return message;
		}
		else if(status == 401){
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			throw new EDMException("Authentication failed. Please check user credentials and try again");
		}
		else if(status == 400){
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			if(message.contains("priority")){
				throw new EDMException("Priority value is not valid");
			}
			else if(message.contains("customfield_10131")){
				throw new EDMException("Severity value is not valid");
			}
			else
				throw new EDMException("Could not post defect due to an Internal error, Please contact Tenjin support");
		} 
		else {
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			throw new EDMException("Could not post defect due to an Internal error, Please contact Tenjin support");
		}
	}

	public InputStream getInputStream(String fileUrl, String username,String password) throws EDMException {
		logger.info("Calling --> GET {}", fileUrl);

		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(fileUrl);

		Response response = target.request().header(username,password).get();

		logger.info("Response from server recieved");
		int status = response.getStatus();

		if(status == 200 || status == 201){
			logger.info("Status - [{}]", status);
			return response.readEntity(InputStream.class);
		}
		else if(status == 401){
			logger.error("Status - [{}]", status);
			throw new EDMException("Authentication failed. Please check user credentials and try again");
		} else{
			String message = response.readEntity(String.class);
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			throw new EDMException(status + " - " + message);
		}
	}

}
