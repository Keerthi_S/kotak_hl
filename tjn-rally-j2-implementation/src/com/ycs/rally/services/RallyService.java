/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RallyService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 */
package com.ycs.rally.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.rallydev.rest.RallyRestApi;
import com.rallydev.rest.request.CreateRequest;
import com.rallydev.rest.request.QueryRequest;
import com.rallydev.rest.response.CreateResponse;
import com.rallydev.rest.response.QueryResponse;
import com.rallydev.rest.util.Fetch;
import com.rallydev.rest.util.QueryFilter;
import com.ycs.rally.util.Properties;
import com.ycs.tenjin.defect.Attachment;
import com.ycs.tenjin.defect.Defect;
import com.ycs.tenjin.defect.EDMException;
import com.ycs.tenjin.defect.UnreachableEDMException;

public class RallyService {

	private static final Logger logger = LoggerFactory
			.getLogger(RallyService.class);
	GenericRallyServices genericService=new GenericRallyServices();
	private static ClientConfig config = new ClientConfig();
	String userId="ZSESSIONID";
	public JSONArray getAllProjects(String baseUrl, String username,String password) throws UnreachableEDMException, EDMException {

		String targetUrl = baseUrl + "/slm/webservice/v2.0/projects";
		JSONArray finalArray = new JSONArray();
		String jsonString = "";
		try {
			jsonString = genericService.get(targetUrl,this.userId,password);
			JSONObject mainObject = new JSONObject(jsonString);
			mainObject= (JSONObject) mainObject.get("QueryResult");
			Object obj=mainObject.get("Results");

			jsonString=obj.toString();
			JSONArray array = new JSONArray(jsonString);
			for (int i = 0; i < array.length(); i++) {
				JSONObject rallyObject = array.getJSONObject(i);
				JSONObject project = new JSONObject();
				project.put("pid", rallyObject.get("_refObjectUUID"));
				project.put("pname", rallyObject.get("_refObjectName"));
				finalArray.put(project);
			}
		} catch (JSONException e) {
			throw new UnreachableEDMException(
					"Unable to fetch projects due to json parse error.");
		}
		return finalArray;
	}
	public Integer urlvalidation(String baseUrl) {

		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(baseUrl);

		Response response = target.request().get();
		return response.getStatus();
	}
	public String getToken(String username, String password) {
		String NME_PSWD = this.userId + ":" + password;
		byte[] encode = Base64.encodeBase64(NME_PSWD.getBytes());
		return new String(encode);
	}

	public void getTokens(String username, String password) {

	}

	public Map<String, Object> createIssuse(String input, String baseUrl,String username,String password,Defect defect) throws EDMException {

		String targetUrl = baseUrl + "/slm/webservice/v2.0/defect/create";
		Gson gson = new Gson();
		String jsonString = genericService.post(targetUrl, input,this.userId,password);
		JSONObject mainObject;
		Object obj = null;
		try {
			mainObject = new JSONObject(jsonString);
			mainObject= (JSONObject) mainObject.get("CreateResult");
			obj=mainObject.get("Object");
		} catch (JSONException e) {
			logger.error("Error in Posting the defect:"+e);
			
		}

		@SuppressWarnings("unchecked")
		Map<String,Object> json = gson.fromJson(obj.toString(), Map.class);
		return json;
	}



	public List<Attachment> addattachment(String baseUrl, Defect defect,
			Attachment fullfilename,String apiKey) throws EDMException, URISyntaxException, IOException {

		List<Attachment> resobj = null;
		String path = fullfilename.getFilePath();
		URI uri=new URI(baseUrl);

		File f = new File(path);
		if (!f.exists() && !f.isDirectory()) {

			logger.error("File not exists");
		}
		String imageFileName=fullfilename.getFileName();
		String fullImageFile = path;
		String imageBase64String;
		long attachmentSize;
		RandomAccessFile myImageFileHandle = new RandomAccessFile(fullImageFile, "r");
		RallyRestApi restApi=null;
		CreateResponse attachmentResponse=null;
		try {
			restApi = new RallyRestApi(uri,apiKey);
			long longLength = myImageFileHandle.length();
			long maxLength = 5000000;
			if (longLength >= maxLength) {		
				restApi.close();
				throw new IOException("File size >= 5 MB Upper limit for Rally.");	
				}
			int fileLength = (int) longLength;            

			// Read file and return data
			byte[] fileBytes = new byte[fileLength];
			myImageFileHandle.readFully(fileBytes);
			imageBase64String = Base64.encodeBase64String(fileBytes);
			attachmentSize = fileLength;



			// First create AttachmentContent from image string
			JsonObject myAttachmentContent = new JsonObject();
			//myAttachmentContent.addProperty("Project","/project/"+defect.getProjObjId());
			myAttachmentContent.addProperty("Content", imageBase64String);
			CreateRequest attachmentContentCreateRequest = new CreateRequest("AttachmentContent", myAttachmentContent);
			CreateResponse attachmentContentResponse = restApi.create(attachmentContentCreateRequest);
			String myAttachmentContentRef = attachmentContentResponse.getObject().get("_ref").getAsString();
			System.out.println("Attachment Content created: " + myAttachmentContentRef);        	

			// Now create the Attachment itself
			JsonObject myAttachment = new JsonObject();
			myAttachment.addProperty("Artifact", defect.getDefObjId());
			myAttachment.addProperty("Content", myAttachmentContentRef);
			myAttachment.addProperty("Name", imageFileName);
			myAttachment.addProperty("ContentType","application/octetstream");
			myAttachment.addProperty("Size", attachmentSize);

			CreateRequest attachmentCreateRequest = new CreateRequest("Attachment", myAttachment);
			attachmentResponse = restApi.create(attachmentCreateRequest);
			String myAttachmentRef = attachmentResponse.getObject().get("_ref").getAsString();
			System.out.println("Attachment  created: " + myAttachmentRef);  

		
		if (attachmentResponse.wasSuccessful()) {
			logger.info("Post attachment for defect:" + defect.getIdentifier() + " {}",
					Properties.RALLY_SUCCESS);
			Gson gson = new Gson();
			resobj = gson
					.fromJson(
							attachmentResponse.getObject(),
							new TypeToken<ArrayList<Attachment>>() {
							}.getType());
		} else {
			String[] attachmentContentErrors;
			attachmentContentErrors = attachmentResponse.getErrors();
			String message = attachmentContentErrors.toString(); 
			logger.error("Message - [{}]", message);
			throw new EDMException(message);
		}
		
		}catch (Exception e) {
			logger.error("Error occurred while attempting to create Content and/or Attachment:" , e.getMessage());
			        	
		}
		finally {
			restApi.close();
			myImageFileHandle.close();
		}
 
		return resobj;
	}

	public JsonObject getDefectDetailsandAttachments(String BaseUrl,String password,String defectId) {
		JsonObject JsonObj=null;
		RallyRestApi	restApi = null;
		try {
			URI uri=new URI(BaseUrl);
			restApi = new RallyRestApi(uri,password);


			QueryRequest  existUserStoryRequest = new QueryRequest("Defects");
			existUserStoryRequest.setFetch(new Fetch("FormattedID","Name","Description","Priority","Severity","_refObjectUUID","State"));
			existUserStoryRequest.setQueryFilter(new QueryFilter("FormattedID", "=", defectId));
			QueryResponse userStoryQueryResponse = restApi.query(existUserStoryRequest);

			JsonObj = userStoryQueryResponse.getResults().get(0).getAsJsonObject();
		}catch(Exception e) {
			logger.error("Error in fetching the Defect details: "+e);
			
		}

		return JsonObj;
	}

	public Defect getIssuedata(String BaseUrl,String password,
			String getdefectId) throws UnreachableEDMException, EDMException, JSONException, URISyntaxException {

		JsonObject JsonObj=this.getDefectDetailsandAttachments(BaseUrl, password, getdefectId);

		Gson gson = new Gson();
		@SuppressWarnings("unchecked")
		Map<String,String> json = gson.fromJson(JsonObj.toString(), Map.class);

		Defect defectobj = new Defect();

		defectobj.setSummary(json.get("Name"));
		defectobj.setDescription(json.get("Description"));
		defectobj.setPriority(json.get("Priority"));
		defectobj.setSeverity(json.get("Severity"));
		defectobj.setStatus(json.get("State"));

		return defectobj;

	}


	public String getlistattachment(String baseUrl, String username,String password, String defectIdentifier) throws EDMException, IOException {

		String  jsonString="";

		try {
			JsonObject JsonObj=this.getDefectDetailsandAttachments(baseUrl, password, defectIdentifier);

			String defecturl=JsonObj.get("_ref").toString().replaceAll("^[\"']+|[\"']+$", "");
			String targetUrl =defecturl+"/attachments";
			jsonString = genericService.get(targetUrl,this.userId,password);


		}catch(Exception e) {
			logger.error("Error in fetching the defects attachments:"+e);
			
		}

		return jsonString;

	}


	public String downloadAttachmentFile(String targeturl, String username,String password,Attachment attachment) throws EDMException {
		String fileName = attachment.getFileName();


		Path filedirectory = Paths.get(Properties.RALLY_ATTACHMENT_DOWNLOAD_PATH+ "\\" + attachment.getAttachmentIdentifier());
		try {
			if (!Files.exists(filedirectory)) {
				Files.createDirectories(filedirectory);
			}
			InputStream inputStream = genericService.getInputStream(targeturl, this.userId,password);

			FileOutputStream outputStream = new FileOutputStream(new File(
					filedirectory + "\\" + fileName));
			IOUtils.copy(inputStream, outputStream);
			inputStream.close();
			outputStream.close();
			logger.info("Attachment downloaded to path {}", filedirectory);
			return filedirectory + "\\" + fileName;
		} catch (Exception e) {
			throw new EDMException("Failed to download attachment for defect "
					+ attachment.getAttachmentIdentifier());
		}

	}


}

