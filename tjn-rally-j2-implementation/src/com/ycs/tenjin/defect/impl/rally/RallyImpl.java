/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RallyImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 08-12-2020			Pushpalatha				Newly Added for TENJINCG-1220
 */
package com.ycs.tenjin.defect.impl.rally;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.rally.services.GenericRallyServices;
import com.ycs.rally.services.RallyService;
import com.ycs.tenjin.defect.Attachment;
import com.ycs.tenjin.defect.Comment;
import com.ycs.tenjin.defect.Defect;
import com.ycs.tenjin.defect.DefectManager;
import com.ycs.tenjin.defect.EDMException;
import com.ycs.tenjin.defect.UnreachableEDMException;
import com.ycs.tenjin.defect.impl.GenericEDMImpl;




public class RallyImpl extends GenericEDMImpl implements DefectManager{
	private static final Logger logger=LoggerFactory.getLogger(RallyImpl.class);

	RallyService rallyService =new RallyService();
	GenericRallyServices genericService=new GenericRallyServices();
	SetterRequest setterrequest =new SetterRequest();
	String userId="ZSESSIONID";
	public RallyImpl(String baseUrl, String userId, String password) {
		super(baseUrl, userId, password);
		
	}
	/*public RallyImpl(String baseUrl, String userId, String password,String organization) {
		super(baseUrl, userId, password, organization);
		
	}*/

	@Override
	public String login() throws UnreachableEDMException, EDMException {
		rallyService.getTokens(this.userId,this.password);
		return "";

	}

	@Override
	public boolean isUrlValid() throws UnreachableEDMException {
		Integer UrlvaidateStatus=rallyService.urlvalidation(this.baseUrl);
		if(UrlvaidateStatus==200)
			return true;
		else{
			return false;
		}
	}

	@Override
	public JSONArray getAllProjects() throws UnreachableEDMException, EDMException {
		JSONArray allProjects = rallyService.getAllProjects(this.baseUrl, this.userId,this.password);
		return allProjects;
	}



	@Override
	public void postDefectDetails(Defect defect) throws UnreachableEDMException, EDMException {	
		String input=setterrequest.getObjCreateIssue(defect,this.userId);
		Map<String,Object> map = rallyService.createIssuse(input,this.baseUrl,this.userId,this.password,defect);
		String identifier=map.get("FormattedID").toString();
		String refId = map.get("_ref").toString();
		defect.setIdentifier(identifier);
		defect.setDefObjId(refId);

	}
	@Override
	public void postDefectAttachment(Defect defect, Attachment attachment)
			throws UnreachableEDMException, EDMException {
		try {
			List<Attachment>  attachmentdata= rallyService.addattachment(this.baseUrl,defect,attachment,this.password);
			attachment.setAttachmentIdentifier(attachmentdata.get(0).getAttachmentIdentifier());
			attachment.setFileName(attachmentdata.get(0).getFileName());
		} catch (EDMException e) {
			throw new EDMException("An internal error occurred while fetching posting attachment to DMS. Please contact support");
		}catch (Exception e) {
			logger.error("Error occured while posting the attachment ", e);
		}

	}


	@Override
	public Defect getDefect(String defectIdentifier) throws UnreachableEDMException, EDMException {
		Defect defect=new Defect();
		try {
			defect = rallyService.getIssuedata(this.baseUrl,this.password,defectIdentifier);
		} catch (JSONException e) {
			logger.error("Error occured while geting defect ", e);
		} catch (URISyntaxException e) {
			logger.error("Error occured ", e);
		}
		return defect;
	}


	@Override
	public List<Attachment> getDefectAttachments(String defectIdentifier, String projectIdentifier)
			throws UnreachableEDMException, EDMException {
		String listAttachment ="";
		try {
			listAttachment = rallyService.getlistattachment(this.baseUrl,this.userId,this.password,defectIdentifier);
		} catch (IOException e) {
			logger.error("Error While getting the attachemnts ", e);
		}
		List<Attachment> attachmentList=new ArrayList<Attachment>();
		JSONArray attachmentsJsonArray=null;
		JSONObject  mainObject=null;
		Object obj=null;
		Gson gson = new Gson();
		try {

			mainObject = new JSONObject(listAttachment);

			mainObject= (JSONObject) mainObject.get("QueryResult");
			obj=mainObject.get("Results");
			attachmentsJsonArray= new JSONArray(obj.toString());

			for(int i=0; i<attachmentsJsonArray.length(); i++)
			{
				JSONObject json=attachmentsJsonArray.getJSONObject(i);
				Attachment attachment=new Attachment();
				attachment.setAttachmentIdentifier(json.getString("ObjectID"));
				attachment.setFileName(json.getString("Name"));
				String contentObject=json.getString("Content");
				Map<String,String> contentjson = gson.fromJson(contentObject, Map.class);
				String targetUrl=contentjson.get("_ref");
				genericService.get(targetUrl,this.userId,this.password);

				String path = rallyService.downloadAttachmentFile(targetUrl,this.userId,this.password,attachment);
				attachment.setFilePath(path);
				attachmentList.add(attachment);
			}

		} catch (Exception att) {
			logger.error("Error" , att);
		}
		return attachmentList ;
	}



	@Override
	public List<Defect> postDefects(List<Defect> defects) throws UnreachableEDMException, EDMException {
		
		return null;
	}



	@Override
	public List<Defect> getDefects(String[] defectIdentifiers) throws UnreachableEDMException, EDMException {
		
		return null;
	}

	@Override
	public List<Defect> getDefects(List<String> defectIdentifiers) throws UnreachableEDMException, EDMException {
		
		return null;
	}



	@Override
	public void postDefectAttachment(String defectIdentifier, String projectIdentifer, Attachment attachment)
			throws UnreachableEDMException, EDMException {
		

	}

	@Override
	public void postComment(Defect defect, Comment comment) throws UnreachableEDMException, EDMException {
		

	}


	@Override
	public Attachment getDefectAttachment(String defectIdentifier, String projectIdentifier,
			String attachmentIdentifier) throws UnreachableEDMException, EDMException {
		
		return null;
	}

	@Override
	public List<Attachment> getDefectAttachments(String defectIdentifier, String projectIdentifier,
			List<String> attachmentIdentifiers) throws UnreachableEDMException, EDMException {
		
		return null;
	}


	@Override
	public void logout() throws UnreachableEDMException, EDMException {
		

	}

	@Override
	public List<Comment> getComments(String defectIdentifier) throws UnreachableEDMException, EDMException {
		
		return null;
	}

	@Override
	public void setProjectDesc(String projectDescription) {
		

	}


	public Defect getDefect(Defect defect) throws EDMException {


		return null;
	}
}
