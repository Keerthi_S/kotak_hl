package com.ycs.tenjin.defect.impl.rally;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.defect.Defect;

public class SetterRequest {

	private static final org.slf4j.Logger logger=LoggerFactory.getLogger(SetterRequest.class);
	/**
	 * Conversion for create issue.
	 */


	public String getObjCreateIssue(Defect defect,String userId) {
		JSONObject defectobj = new JSONObject();
		JSONObject defectdetails = new JSONObject();

		try {
			defectdetails.put("Name",defect.getSummary());

			defectdetails.put("Project","/project/"+defect.getProjObjId());
			defectdetails.put("Priority",defect.getPriority());
			defectdetails.put("Severity",defect.getSeverity());
			defectdetails.put("Description",defect.getDescription());
			defectobj.put("Defects", defectdetails);
		} catch (JSONException e) {
			logger.error("Error in Attaching the defect data:"+e);
		}

		return  defectobj.toString();
	}

}
