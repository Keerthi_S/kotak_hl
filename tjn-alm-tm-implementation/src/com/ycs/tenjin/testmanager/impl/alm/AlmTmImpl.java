package com.ycs.tenjin.testmanager.impl.alm;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.alm.tm.services.AlmTmService;
import com.ycs.tenjin.testmanager.ETMException;
import com.ycs.tenjin.testmanager.TestCase;
import com.ycs.tenjin.testmanager.TestManager;
import com.ycs.tenjin.testmanager.TestStep;
import com.ycs.tenjin.testmanager.UnreachableETMException;
import com.ycs.tenjin.testmanager.impl.GenericETMImpl;

public class AlmTmImpl extends GenericETMImpl implements TestManager {

	AlmTmService service = new AlmTmService();
	String cookieToken = "";
	String domain = "";
	String project = "";

	public AlmTmImpl(String baseUrl, String userId, String password) {
		super(baseUrl, userId, password);

	}

	private static final Logger logger = LoggerFactory
			.getLogger(AlmTmImpl.class);

	/******************* Logging into HP-ALM *********************/
	@Override
	public String login() throws UnreachableETMException, ETMException {
		this.cookieToken = service.login(this.baseUrl, this.userId,
				this.password);
		return this.cookieToken;
	}

	/************* Setting DOMAIN and PROJECTS ***************/
	@Override
	public void setProjectDesc(String projectDescription) {
		String[] domproject = projectDescription.split(":");
		this.domain = domproject[0];
		this.project = domproject[1];
		logger.info("----domain and project------[{},{}]", domproject[0],
				domproject[1]);
	}

	/******************* Getting Single TestStep ***************/
	@Override
	public List<String> getTestStep() throws UnreachableETMException,
			ETMException {
		List<String> ts = service.getSingleTestStep(this.baseUrl, this.domain,
				this.project, this.cookieToken);
		return ts;
	}

	/******************* Getting Single TestCase ***************/
	@Override
	public List<String> getTestCase() throws UnreachableETMException,
			ETMException {
		List<String> tc = service.getSingleTestCase(this.baseUrl, this.domain,
				this.project, this.cookieToken);
		return tc;
	}

	/******************* Getting Multiple TestCases Based on Filter Search ***************/
	@Override
	public List<TestCase> getTestCases(String fieldKey, String fieldValue)
			throws UnreachableETMException, ETMException {
		List<TestCase> listTc = null;
		try {
			listTc = service.getTestCasesByFiltering(this.baseUrl, fieldKey,
					fieldValue, this.domain, this.project, this.cookieToken);
		} catch (Exception e) {
			logger.error("Unable to fetch testCases from ALm.");
			throw new ETMException(e.getMessage());
		}
		return listTc;
	}
	
	/*Added By Ashiki for TJN252-60 starts*/
	@Override
	public List<TestCase> getTestCases(String fieldKey, String fieldValue,
			Map<String, String> mapAttributes)
			throws UnreachableETMException, ETMException {
		List<TestCase> listTc = null;
		try {
			listTc = service.getTestCasesByFiltering(this.baseUrl, fieldKey,
					fieldValue, this.domain, this.project, this.cookieToken,mapAttributes);
		} catch (Exception e) {
			logger.error("Unable to fetch testCases from ALm.");
			throw new ETMException(e.getMessage());
		}
		return listTc;
	}
	/*Added By Ashiki for TJN252-60 ends*/
	/******************* Getting Multiple TestSteps Based on Filter Search ***************/
	@Override
	public List<TestStep> getTestStepsWithFilter(String fieldKey, String fieldValue)
			throws UnreachableETMException, ETMException {
		List<TestStep> listTs=null;
		try {
			listTs=service.getTestStepsByFiltering(this.baseUrl, fieldKey,
					fieldValue, this.domain, this.project,
					this.cookieToken);
		} catch (Exception e) {
			logger.error("Unable to fetch testSteps from ALm.");
		}
		return listTs;
	}
	/*Added By Ashiki for TJN252-60 starts*/
	@Override
	public List<TestStep> getTestStepsWithFilter(String fieldKey, String fieldValue,
			Map<String,String> mapAttributes)
			throws UnreachableETMException, ETMException {
		List<TestStep> listTs=null;
		try {
			listTs=service.getTestStepsByFiltering(this.baseUrl, fieldKey,
					fieldValue, this.domain, this.project,
					this.cookieToken,mapAttributes);
		} catch (Exception e) {
			logger.error("Unable to fetch testSteps from ALm.");
		}
		return listTs;
	}
	/*Added By Ashiki for TJN252-60 ends*/
	/******************* Getting Multiple TestSteps Based on Filter Search with tcId ***************/
	@Override
	public List<TestStep> getTestSteps(String fieldKey, String fieldValue,
			String tcRecordId) throws UnreachableETMException, ETMException {

	List<TestStep> listTs = null;
	try {
		listTs = service.getTestStepsByFilteringByTcId(this.baseUrl, fieldKey,
				fieldValue, tcRecordId, this.domain, this.project,
				this.cookieToken);
	} catch (Exception e) {
		logger.error("Unable to fetch testSteps from ALm.");
	}
	return listTs;

}
	/*Added By Ashiki for TJN252-60 starts*/
	public List<TestStep> getTestSteps(String fieldKey, String fieldValue,
			String tcRecordId,Map<String, String> mapAttributes) throws UnreachableETMException, ETMException {
		List<TestStep> listTs = null;
		try {
			listTs = service.getTestStepsByFilteringByTcId(this.baseUrl, fieldKey,
					fieldValue, tcRecordId, this.domain, this.project,
					this.cookieToken,mapAttributes);
		} catch (Exception e) {
			logger.error("Unable to fetch testSteps from ALm.");
		}
		return listTs;
	}
	/*Added By Ashiki for TJN252-60 ends*/
	
	
	/******************* Synchronization of TestCases Based on Ids ***************/
	@Override
	public List<TestCase> SynchronizeTestCases(List<String> tcRecIds)
			throws UnreachableETMException, ETMException {
		List<TestCase> listTc = null;
		try {
			listTc = service.syncTestCases(this.baseUrl, this.domain,
					this.project, this.cookieToken, tcRecIds);
		} catch (Exception e) {
			
			logger.error("Unable to fetch testCases from ALm.");
			throw new ETMException(e.getMessage());
		}
		return listTc;
	}
	/*Added By Ashiki for TJN252-60 starts*/
	@Override
	public List<TestCase> SynchronizeTestCases(List<String> tcRecIds,Map<String,String> mapAttributes)
			throws UnreachableETMException, ETMException {
		List<TestCase> listTc = null;
		try {
			listTc = service.syncTestCases(this.baseUrl, this.domain,
					this.project, this.cookieToken, tcRecIds,mapAttributes);
		} catch (Exception e) {
			
			logger.error("Unable to fetch testCases from ALm.");
			throw new ETMException(e.getMessage());
		}
		return listTc;
	}
	/*Added By Ashiki for TJN252-60 ends*/
	/******************* Synchronization of TestSteps Based on Ids ***************/
	@Override
	public List<TestStep> SynchronizeTestSteps(List<String> tcStepIds,Map<String,String> mapAttributes)
			throws UnreachableETMException, ETMException {
		List<TestStep> listTs = null;
		try {
			listTs = service.syncTestSteps(this.baseUrl, this.domain,
					this.project, this.cookieToken, tcStepIds,mapAttributes);
		} catch (Exception e) {
			logger.error("Unable to fetch testSteps from ALm.");
			throw new ETMException(e.getMessage());
		}
		return listTs;
	}

	
	/*Added By Ashiki for TJN252-60 starts*/
	@Override
	public List<TestStep> SynchronizeTestSteps(List<String> tcStepIds)
			throws UnreachableETMException, ETMException {
		List<TestStep> listTs = null;
		try {
			listTs = service.syncTestSteps(this.baseUrl, this.domain,
					this.project, this.cookieToken, tcStepIds);
		} catch (Exception e) {
			logger.error("Unable to fetch testSteps from ALm.");
			throw new ETMException(e.getMessage());
		}
		return listTs;
	}
	/*Added By Ashiki for TJN252-60 ends*/
	/******************* Logout ***************/
	@Override
	public void logout() throws UnreachableETMException, ETMException {
		try {
			service.logout(this.baseUrl, this.cookieToken);
			logger.info("You are successfully logged out");
		} catch (Exception e) {
			logger.error("Unable to logout due to internal error");
		}
	}





}
