/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AlmTmService.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 23-JAN-2017         Abhilash K N       Newly added
 * 19-Aug-2017         Manish		            T25IT-236 
 * 29-Aug-2017         Manish		            T25IT-309 
 * 18-02-2019		   Ashiki					TJN252-60
 * 26-03-2019			Roshni					TJN252-54
 * 27-05-2020		   Ashiki					ALM import
 */
package com.ycs.tenjin.alm.tm.services;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.testmanager.ETMException;
import com.ycs.tenjin.testmanager.TestCase;
import com.ycs.tenjin.testmanager.TestStep;

public class AlmTmService {
	private static final Logger logger = LoggerFactory
			.getLogger(AlmTmService.class);

	private static ClientConfig config = new ClientConfig();

	/******
	 * Generic Get method
	 * ****************/
	public  String get(String url, String cookieToken)
			throws ETMException {
		logger.info("Calling --> GET {}", url);

		Client client = ClientBuilder.newClient(config);

		WebTarget target = client.target(url);
		Response response = target
				.request(MediaType.APPLICATION_FORM_URLENCODED,
						MediaType.APPLICATION_JSON)
				.header(HttpHeaders.COOKIE, cookieToken).get();

		logger.info("Response from server recieved");
		int status = response.getStatus();
		String message = response.readEntity(String.class);

		if (status == 200 || status == 201) {
			logger.info("Status - [{}]", status);
			logger.info("Message - [{}]", message);
			return message;
		}/*changed by manish for displaying proper message for wrong credentials starts*/
		else if (status == 401) {
			logger.info("Status - [{}]", status);
			logger.info("Message - [{}]", message);
			throw new ETMException("Authentication failed. Please check ETM credentials and try again");
		}
		/*changed by manish for displaying proper message for wrong credentials ends*/
		else {
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			/*changed by manish for displaying proper message for wrong credentials starts*/
			/*throw new ETMException(status + " - " + message);*/
			throw new ETMException("An Internal error occured, please contact Tenjin support");
			/*changed by manish for displaying proper message for wrong credentials ends*/
		}
	}

	/*******
	 * Logging into Application and Generating CookieToken
	 ************/
	public String login(String baseUrl, String username, String password)
			throws ETMException {
		String token = getToken(username, password);
		String targetUrl = baseUrl + "/api/authentication/sign-in";
		String cookie = "";

		logger.info("Calling --> GET {}", targetUrl);
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(targetUrl);

		Response response = target.request()
				.header(HttpHeaders.AUTHORIZATION, "Basic " + token).get();

		Map<String, NewCookie> cookieValues = response.getCookies();
		for (Entry<String, NewCookie> value : cookieValues.entrySet()) {
			NewCookie newCookie = value.getValue();
			cookie = cookie + newCookie.getName() + "=" + newCookie.getValue()
					+ ";";

		}
		logger.info("Response from server recieved");
		int status = response.getStatus();
		String message = response.readEntity(String.class);

		if (status == 200 || status == 201) {
			logger.info("Status - [{}]", status);
			logger.info("Message - [{}]", message);
			return cookie;
		}
		/*changed by manish for displaying proper message for wrong credentials starts*/
		else if (status == 401) {
			logger.info("Status - [{}]", status);
			logger.info("Message - [{}]", message);
			throw new ETMException("Authentication failed. Please check ETM credentials and try again");
		}
		/*changed by manish for displaying proper message for wrong credentials ends*/
		else {
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			/*changed by manish for displaying proper message for wrong credentials starts*/
			/*throw new ETMException(status + " - " + message);*/
			throw new ETMException("An Internal error occured, please contact Tenjin support");
			/*changed by manish for displaying proper message for wrong credentials ends*/
		}
	}

	/********
	 * Getting TestStep details
	 * **************/
	public List<String> getSingleTestStep(String baseUrl, String domain,
			String project, String cookieToken) throws ETMException {
		String targetUrl = baseUrl + "/rest/domains/" + domain + "/projects/"
				+ project + "/customization/entities/design-step/fields";

		String out = this.get(targetUrl, cookieToken);
		JSONObject rootObj = null;
		// Map<String,String> map=new HashMap<String, String>();
		List<String> fieldKeys = new ArrayList<String>();
		// System.out.println("---------length "+rootObj.getJSONArray("Fields").length());
		try {
			rootObj = new JSONObject(out);
			//rootObj.getString("entities");
			rootObj=new JSONObject(rootObj.getString("Fields"));
			//JSONArray jarray = new JSONArray(rootObj.getString("entities"));
			JSONArray jarray = new JSONArray(rootObj.getString("Field"));
		/*	jarray.getJSONObject(0).getJSONArray("Fields").length();
			JSONArray needArray = new JSONArray(jarray.getJSONObject(0)
					.getString("Fields"));*/
			JSONArray needArray = jarray;
			for (int i = 0; i < needArray.length(); i++) {
				try {

					/*
					 * map.put(needArray.getJSONObject(i).getString("Name"),
					 * needArray
					 * .getJSONObject(i).getJSONArray("values").getJSONObject
					 * (0).getString("value"));
					 */
					String displayName=needArray.getJSONObject(i).getString("label").toString();
					String value=needArray.getJSONObject(i).getString("name").toString();
					String finalString=displayName+"+"+value;
					/*fieldKeys.add(needArray.getJSONObject(i).getString("Name")
							.toString());*/
					fieldKeys.add(finalString);

				} catch (Exception e) {
					
					logger.error("Error occured while fetching TestStep {} ",
							e.getMessage());
				}

			}
			/*
			 * for(Map.Entry m:map.entrySet()){
			 * System.out.println(m.getKey()+" :"+m.getValue()); }
			 */
			/*
			 * tStep=new TestStep(); tStep.setAutLoginType(map.get("user-05"));
			 * tStep.setAppName(map.get("user-02"));
			 * tStep.setDescription(Jsoup.parse
			 * (map.get("description")).body().text());
			 * tStep.setModuleCode(map.get("user-03"));
			 * tStep.setOperation(map.get("user-06"));
			 * tStep.setRowsToExecute(Integer.parseInt(map.get("user-08")));
			 * //tStep
			 * .setTestCaseRecordId(Integer.parseInt(map.get("parent-id")));
			 * tStep.setTestCaseRecordId(8);
			 * tStep.setTxnMode(map.get("user-09"));
			 * tStep.setType(map.get("user-06"));
			 * tStep.setDataId(map.get("name"));
			 * tStep.setId(map.get("user-07"));
			 * tStep.setExpectedResult(Jsoup.parse
			 * (map.get("expected")).body().text());
			 * tStep.setShortDescription(map.get("user-11"));
			 */
			/* ts = gson.fromJson(out, TestStep.class); */
		} catch (JSONException e) {
			
			logger.error("Json parseException {}", e.getMessage());
		}
		return fieldKeys;
	}

	/**
	 * Token generation for login.
	 **/
	public static String getToken(String username, String password) {
		String NME_PSWD = username + ":" + password;
		System.out.println(NME_PSWD);
		byte[] encode = Base64.encodeBase64(NME_PSWD.getBytes());
		return new String(encode);
	}


	/********
	 * Getting Single TestCase details
	 * **************/
	public List<String> getSingleTestCase(String baseUrl, String domain,
			String project, String cookieToken) throws ETMException {
		String targetUrl = baseUrl + "/rest/domains/" + domain + "/projects/"
				+ project + "/customization/entities/test/fields";

		String out = this.get(targetUrl, cookieToken);
		JSONObject rootObj = null;
		// Map<String,String> map=new HashMap<String, String>();
		JSONArray needArray = null;
		List<String> fieldKeys = new ArrayList<String>();
		try {
			rootObj = new JSONObject(out);
			rootObj=new JSONObject(rootObj.getString("Fields"));
			//rootObj.getString("entities");
			JSONArray jarray = new JSONArray(rootObj.getString("Field"));
			//jarray.getJSONObject(0).getJSONArray("Fields").length();
			needArray = jarray;/*new JSONArray(jarray.getJSONObject(0).getString("Fields"));*/
		} catch (JSONException e1) {

			logger.error("Json parseException { }", e1.getMessage());
		}
		for (int i = 0; i < needArray.length(); i++) {
			try {
				String displayName=needArray.getJSONObject(i).getString("label").toString();
				String value=needArray.getJSONObject(i).getString("name").toString();
				String finalString=displayName+"+"+value;
				fieldKeys.add(finalString);
				
				/*fieldKeys.add(needArray.getJSONObject(i).getString("name")
						.toString());*/
				/*
				 * map.put(needArray.getJSONObject(i).getString("Name"),
				 * needArray
				 * .getJSONObject(i).getJSONArray("values").getJSONObject
				 * (0).getString("value"));
				 */
			} catch (Exception e) {
				
				logger.error(
						"Error occured while fetching Single TestCase {} ",
						e.getMessage());
			}

		}

		// fieldKeys.add(paramE)
		/*
		 * for(Map.Entry m:map.entrySet()){ //
		 * System.out.println(m.getKey()+" :"+m.getValue());
		 * fieldKeys.add(m.getKey().toString()); }
		 */
		// extra.......
		// tCase.getTcModifiedOn(map.get("last-modified"));
		return fieldKeys;
	}

	/*********
	 * Getting TestCases details on filter Search
	 * **************/
	public ArrayList<TestCase> getTestCasesByFiltering(String baseUrl,
			String fieldKey, String fieldValue, String domain, String project,
			String cookieToken) throws ETMException {
		JSONArray needArray = null;
		Map<String, String> map = new HashMap<String, String>();
		TestCase tCase = null;
		ArrayList<TestCase> listTc = new ArrayList<TestCase>();
		String targetUrl = null;

		try {
			targetUrl = baseUrl + "/rest/domains/" + domain + "/projects/"
					+ project + "/tests?query="
					+ URLEncoder.encode("{", "UTF-8") + fieldKey
					+ URLEncoder.encode("[", "UTF-8") + this.trim(fieldValue)
					+ URLEncoder.encode("]}", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Url Encoding problem {}", e.getMessage());
		}
		String out = this.get(targetUrl, cookieToken);

		try {
			JSONObject rootObj = new JSONObject(out);
			if (Integer.parseInt(rootObj.getString("TotalResults")) == 0
					|| rootObj.getJSONArray("entities").isNull(0)) {
				logger.info("No result found based on the filter search value");

			}
			rootObj.getString("entities");
			JSONArray jarray = new JSONArray(rootObj.getString("entities"));
			jarray.getJSONObject(0).getJSONArray("Fields").length();
			for (int j = 0; j < jarray.length(); j++) {
				needArray = new JSONArray(jarray.getJSONObject(j).getString(
						"Fields"));

				for (int i = 0; i < needArray.length(); i++) {
					try {

						map.put(needArray.getJSONObject(i).getString("Name"),
								needArray.getJSONObject(i)
										.getJSONArray("values")
										.getJSONObject(0).getString("value"));
					} catch (Exception e) {
						logger.error(
								"Error occured while fetching TestCase {} ",
								e.getMessage());
					}

				}
				/*
				 * for(Map.Entry m:map.entrySet()){
				 * System.out.println(m.getKey()+" :"+m.getValue());
				 * 
				 * }
				 */
				tCase = new TestCase();

				try {
					tCase.setTcDesc(Jsoup.parse(map.get("description")).body()
							.text());
				} catch (NullPointerException e1) {
					 logger.error("NullPointerException { }", e1.getMessage());
				}
				tCase.setTcId(map.get("id"));
				tCase.setTcName(map.get("name"));
				tCase.setTcType(map.get("subtype-id"));
				tCase.setTcPriority(map.get("user-01"));
				tCase.setTcCreatedBy(map.get("owner"));
				/*changed by manish for bug T25IT-236 on 19-Aug-2017 starts*/
				if(map.get("user-02")!=null){
					tCase.setMode(map.get("user-02"));
				}else{
					tCase.setMode("GUI");
				}
				/*changed by manish for bug T25IT-236 on 19-Aug-2017 ends*/

				try {
					/*changed by manish for bug T25IT-309 on 29-Aug-2017 starts*/
					/*tCase.setTcCreatedOn(new SimpleDateFormat("dd-MM-YYYY",
							Locale.ENGLISH).parse(map.get("creation-time")));*/
					tCase.setTcCreatedOn(new SimpleDateFormat("yyyy-MM-dd").parse(map.get("creation-time")));
					/*changed by manish for bug T25IT-309 on 29-Aug-2017 ends*/
				} catch (ParseException e) {
					logger.error("Date conversion Exception {}", e.getMessage());
				}
				listTc.add(tCase);

			}
		}catch (Exception e) {
			throw new ETMException(
					"Unable to fetch TestCase based on filter search value "
							+ fieldValue + " " + e.getMessage());
		}

		return listTc;
	
}
	/*Added By Ashiki for TJN252-60 starts*/
	public ArrayList<TestCase> getTestCasesByFiltering(String baseUrl,
			String fieldKey, String fieldValue, String domain, String project,
			String cookieToken,Map<String, String> mapAttributes) throws ETMException {
		
		JSONArray needArray = null;
		Map<String, String> map = new HashMap<String, String>();
		TestCase tCase = null;
		ArrayList<TestCase> listTc = new ArrayList<TestCase>();
		String targetUrl = null;

		try {
			targetUrl = baseUrl + "/rest/domains/" + domain + "/projects/"
					+ project + "/tests?query="
					+ URLEncoder.encode("{", "UTF-8") + fieldKey
					+ URLEncoder.encode("[", "UTF-8") + this.trim(fieldValue)
					+ URLEncoder.encode("]}", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Url Encoding problem {}", e.getMessage());
		}
		String out = this.get(targetUrl, cookieToken);

		try {
			JSONObject rootObj = new JSONObject(out);
			if (Integer.parseInt(rootObj.getString("TotalResults")) == 0
					|| rootObj.getJSONArray("entities").isNull(0)) {
				logger.info("No result found based on the filter search value");

			}
			rootObj.getString("entities");
			JSONArray jarray = new JSONArray(rootObj.getString("entities"));
			jarray.getJSONObject(0).getJSONArray("Fields").length();
			for (int j = 0; j < jarray.length(); j++) {
				needArray = new JSONArray(jarray.getJSONObject(j).getString(
						"Fields"));

				for (int i = 0; i < needArray.length(); i++) {
					try {

						map.put(needArray.getJSONObject(i).getString("Name"),
								needArray.getJSONObject(i)
										.getJSONArray("values")
										.getJSONObject(0).getString("value"));
					} catch (Exception e) {
						logger.error(
								"Error occured while fetching TestCase {} ",
								e.getMessage());
					}

				}
			
				tCase = new TestCase();

				try {
					tCase.setTcDesc(Jsoup.parse(map.get("description")).body()
							.text());
				} catch (NullPointerException e1) {
					 logger.error("NullPointerException { }", e1.getMessage());
				}
				tCase.setTcId(map.get(mapAttributes.get("Test Case Id")));
				tCase.setTcName(map.get("name"));
				
				if(map.get(mapAttributes.get("Test Case Type"))!=null){
					tCase.setTcType("Acceptance");
				}
				tCase.setTcPriority(map.get("user-07"));
				if(map.get("user-07")==null) {
					tCase.setTcPriority("Medium");
				}
				tCase.setTcCreatedBy(map.get("owner"));
				tCase.setMode("GUI");

				try {
					tCase.setTcCreatedOn(new SimpleDateFormat("yyyy-MM-dd").parse(map.get("creation-time")));
				} catch (ParseException e) {
					logger.error("Date conversion Exception {}", e.getMessage());
				}
				listTc.add(tCase);

			}
		}catch (Exception e) {
			throw new ETMException(
					"Unable to fetch TestCase based on filter search value "
							+ fieldValue + " " + e.getMessage());
		}

		return listTc;
	}
	/*Added By Ashiki for TJN252-60 ends*/
	/*********
	 * Getting TestSteps details on filter Search
	 * **************/
	public List<TestStep> getTestStepsByFiltering(String baseUrl,
			String fieldKey, String fieldValue, String domain, String project,
			String cookieToken) throws ETMException {
		JSONArray needArray = null;
		Map<String, String> map = new HashMap<String, String>();
		 
		ArrayList<TestStep> listTs = new ArrayList<TestStep>();
		String targetUrl = null;

		logger.info("In AlmTmService before building url");
		targetUrl = baseUrl + "/rest/domains/" + domain + "/projects/"
				+ project + "/design-steps";
		logger.info("target url -------->>"+targetUrl);
		String out = this.get(targetUrl, cookieToken);
		logger.info("In AlmTmService after getting output, out--->>"+out);
		try {
			JSONObject rootObj = new JSONObject(out);
			if (Integer.parseInt(rootObj.getString("TotalResults")) == 0
					|| rootObj.getJSONArray("entities").isNull(0)) {
				logger.info("No result found based on the filter search value");

			}
			rootObj.getString("entities");
			logger.info("entities--->>"+rootObj.getString("entities"));
			JSONArray jarray = new JSONArray(rootObj.getString("entities"));
			jarray.getJSONObject(0).getJSONArray("Fields").length();
			for (int j = 0; j < jarray.length(); j++) {
				needArray = new JSONArray(jarray.getJSONObject(j).getString(
						"Fields"));

				for (int i = 0; i < needArray.length(); i++) {
					try {

						map.put(needArray.getJSONObject(i).getString("Name"),
								needArray.getJSONObject(i)
										.getJSONArray("values")
										.getJSONObject(0).getString("value"));
					} catch (Exception e) {
						logger.error(
								"Error occured while fetching TestSteps {} ",
								e.getMessage());
					}

				}
				
				TestStep tStep = new TestStep();
				/*tStep.setAutLoginType(map.get("user-08"));*/
				tStep.setAutLoginType(map.get("user-06"));
				tStep.setAppName(map.get("user-02"));
				try {
					tStep.setDescription(Jsoup.parse(map.get("description")).body()
							.text());
				} catch (NullPointerException e1) {
					
					 logger.error("NullPointerException { }", e1.getMessage());
				}
				/*tStep.setModuleCode(map.get("user-04"));*/
				tStep.setModuleCode(map.get("user-03"));
				/*tStep.setOperation(map.get("user-06"));*/
				tStep.setOperation(map.get("user-04"));
				/*tStep.setRowsToExecute(Integer.parseInt(map.get("user-09")));*/
				tStep.setRowsToExecute(Integer.parseInt(map.get("user-05")));
				tStep.setParentTestCaseId(map.get("parent-id"));
				// tStep.setTestCaseRecordId(8);
				//tStep.setTxnMode(map.get("user-07"));
				/*tStep.setType(map.get("user-03"));*/
				tStep.setType(map.get("user-07"));
				/*tStep.setDataId(map.get("user-05"));*/
				tStep.setDataId(map.get("user-08"));
				tStep.setId(map.get("id"));
				try {
					tStep.setExpectedResult(Jsoup.parse(map.get("expected")).body()
							.text());
				} catch (NullPointerException e) {
					
					 logger.error("NullPointerException { }", e.getMessage());
				}
				tStep.setShortDescription(map.get("name"));
				//tStep.setShortDescription(map.get("user-01"));
				logger.info("before adding step to list");
				listTs.add(tStep);

			}
		} /*
		 * catch (JSONException e) {
		 * 
		 * logger.error("Json parseException { }", e.getMessage()); }
		 */catch (Exception e) {
			throw new ETMException(
					"Unable to fetech TestStep based on filter search value "
							+ fieldValue + " " + e.getMessage());
		}

		logger.info("before returning list from AlmTmService");
		return listTs;
	}
	/*Added By Ashiki for TJN252-60 starts*/
	public List<TestStep> getTestStepsByFiltering(String baseUrl,
			String fieldKey, String fieldValue, String domain, String project,
			String cookieToken,Map<String,String> mapAttributes) throws ETMException {

		JSONArray needArray = null;
		Map<String, String> map = new HashMap<String, String>();
		 
		ArrayList<TestStep> listTs = new ArrayList<TestStep>();
		String targetUrl = null;

		logger.info("In AlmTmService before building url");
		targetUrl = baseUrl + "/rest/domains/" + domain + "/projects/"
				+ project + "/design-steps";
		logger.info("target url -------->>"+targetUrl);
		String out = this.get(targetUrl, cookieToken);
		logger.info("In AlmTmService after getting output, out--->>"+out);
		try {
			JSONObject rootObj = new JSONObject(out);
			if (Integer.parseInt(rootObj.getString("TotalResults")) == 0
					|| rootObj.getJSONArray("entities").isNull(0)) {
				logger.info("No result found based on the filter search value");

			}
			rootObj.getString("entities");
			logger.info("entities--->>"+rootObj.getString("entities"));
			JSONArray jarray = new JSONArray(rootObj.getString("entities"));
			jarray.getJSONObject(0).getJSONArray("Fields").length();
			for (int j = 0; j < jarray.length(); j++) {
				needArray = new JSONArray(jarray.getJSONObject(j).getString(
						"Fields"));

				for (int i = 0; i < needArray.length(); i++) {
					try {

						map.put(needArray.getJSONObject(i).getString("Name"),
								needArray.getJSONObject(i)
										.getJSONArray("values")
										.getJSONObject(0).getString("value"));
					} catch (Exception e) {
						logger.error(
								"Error occured while fetching TestSteps {} ",
								e.getMessage());
					}

				}
				
				TestStep tStep = new TestStep();
				
				tStep.setAutLoginType(map.get(mapAttributes.get("AUT Login Type")));
				tStep.setAppName(map.get("user-02"));
				try {
					tStep.setDescription(Jsoup.parse(map.get("description")).body()
							.text());
				} catch (NullPointerException e1) {
					 logger.error("NullPointerException { }", e1.getMessage());
				}
				tStep.setModuleCode(map.get(mapAttributes.get("Function Code")));
				tStep.setOperation(map.get(mapAttributes.get("Operation")));
				tStep.setRowsToExecute(Integer.parseInt(map.get("user-05")));
				tStep.setParentTestCaseId(map.get("parent-id"));
				tStep.setType(map.get(mapAttributes.get("Step Type")));
				tStep.setDataId(map.get("user-08"));
				tStep.setId(map.get("id"));
				try {
					tStep.setExpectedResult(Jsoup.parse(map.get("expected")).body()
							.text());
				} catch (NullPointerException e) {
					 logger.error("NullPointerException { }", e.getMessage());
				}
				tStep.setShortDescription(map.get("name"));
				/*Added by Ashiki for ALM import starts*/
				tStep.setTxnMode(map.get("user-10"));
				/*Added by Ashiki for ALM import ends*/
				logger.info("before adding step to list");
				listTs.add(tStep);
				
			}
		} catch (Exception e) {
			throw new ETMException(
					"Unable to fetech TestStep based on filter search value "
							+ fieldValue + " " + e.getMessage());
		}

		logger.info("before returning list from AlmTmService");
		return listTs;
	
	}
	/*Added By Ashiki for TJN252-60 ends*/
	/********
	 * Getting TestSteps details on filter Search
	 * 
	 * @return
	 * @throws JSONException
	 **************/
	public ArrayList<TestStep> getTestStepsByFilteringByTcId(String baseUrl,
			String fieldKey, String fieldValue, String tcRecordId,
			String domain, String project, String cookieToken)
			throws ETMException {

		
		JSONArray needArray = null;
		Map<String, String> map = new HashMap<String, String>();
		
		ArrayList<TestStep> listTs = new ArrayList<TestStep>();
		String targetUrl = null;

		try {
			targetUrl = baseUrl + "/rest/domains/" + domain + "/projects/"
					+ project + "/design-steps?query="
					+ URLEncoder.encode("{", "UTF-8") + fieldKey
					+ URLEncoder.encode("[", "UTF-8") + this.trim(fieldValue)
					+ URLEncoder.encode("]", "UTF-8") + ";parent-id"
					+ URLEncoder.encode("[", "UTF-8") + tcRecordId
					+ URLEncoder.encode("]}", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Url Encoding problem {}", e.getMessage());
		}
		String out = this.get(targetUrl, cookieToken);

		try {
			JSONObject rootObj = new JSONObject(out);
			if (Integer.parseInt(rootObj.getString("TotalResults")) == 0
					|| rootObj.getJSONArray("entities").isNull(0)) {
				logger.info("No result found based on the filter search value");

			}
			rootObj.getString("entities");
			JSONArray jarray = new JSONArray(rootObj.getString("entities"));
			jarray.getJSONObject(0).getJSONArray("Fields").length();
			for (int j = 0; j < jarray.length(); j++) {
				needArray = new JSONArray(jarray.getJSONObject(j).getString(
						"Fields"));

				for (int i = 0; i < needArray.length(); i++) {
					try {

						map.put(needArray.getJSONObject(i).getString("Name"),
								needArray.getJSONObject(i)
										.getJSONArray("values")
										.getJSONObject(0).getString("value"));
					} catch (Exception e) {
						logger.error(
								"Error occured while fetching TestCase {} ",
								e.getMessage());
					}

				}
				/*
				 * for(Map.Entry m:map.entrySet()){
				 * System.out.println(m.getKey()+" :"+m.getValue());
				 * 
				 * }
				 */
				TestStep tStep = new TestStep();
				tStep.setAutLoginType(map.get("user-08"));
				tStep.setAppName(map.get("user-02"));
				
				try {
					tStep.setDescription(Jsoup.parse(map.get("description")).body()
							.text());
				} catch (NullPointerException e1) {
					
					 logger.error("NullPointerException { }", e1.getMessage());
				}
				tStep.setModuleCode(map.get("user-04"));
				tStep.setOperation(map.get("user-06"));
				tStep.setRowsToExecute(Integer.parseInt(map.get("user-09")));
				tStep.setParentTestCaseId(map.get("parent-id"));
				// tStep.setTestCaseRecordId(8);
				tStep.setTxnMode(map.get("user-07"));
				tStep.setType(map.get("user-03"));
				tStep.setDataId(map.get("user-05"));
				tStep.setId(map.get("id"));
				try {
					tStep.setExpectedResult(Jsoup.parse(map.get("expected")).body()
							.text());
				}catch (NullPointerException e) {
					 
					 logger.error("NullPointerException { }", e.getMessage());
					 }
				tStep.setShortDescription(map.get("user-01"));
				listTs.add(tStep);

			}
		} 
		  
		 catch (Exception e) {
			throw new ETMException(
					"Unable to fetech TestStep based on filter search value "
							+ fieldValue + " " + e.getMessage(),e);
		}
         
		return listTs;

	
}
	
/*Added By Ashiki for TJN252-60 starts*/
	public ArrayList<TestStep> getTestStepsByFilteringByTcId(String baseUrl,
			String fieldKey, String fieldValue, String tcRecordId,
			String domain, String project, String cookieToken,Map<String, String> mapAttributes)
			throws ETMException {
		
		JSONArray needArray = null;
		Map<String, String> map = new HashMap<String, String>();
		
		ArrayList<TestStep> listTs = new ArrayList<TestStep>();
		String targetUrl = null;

		try {
			targetUrl = baseUrl + "/rest/domains/" + domain + "/projects/"
					+ project + "/design-steps?query="
					+ URLEncoder.encode("{", "UTF-8") + fieldKey
					+ URLEncoder.encode("[", "UTF-8") + this.trim(fieldValue)
					+ URLEncoder.encode("]", "UTF-8") + ";parent-id"
					+ URLEncoder.encode("[", "UTF-8") + tcRecordId
					+ URLEncoder.encode("]}", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Url Encoding problem {}", e.getMessage());
		}
		String out = this.get(targetUrl, cookieToken);

		try {
			JSONObject rootObj = new JSONObject(out);
			if (Integer.parseInt(rootObj.getString("TotalResults")) == 0
					|| rootObj.getJSONArray("entities").isNull(0)) {
				logger.info("No result found based on the filter search value");

			}
			rootObj.getString("entities");
			JSONArray jarray = new JSONArray(rootObj.getString("entities"));
			jarray.getJSONObject(0).getJSONArray("Fields").length();
			for (int j = 0; j < jarray.length(); j++) {
				needArray = new JSONArray(jarray.getJSONObject(j).getString(
						"Fields"));

				for (int i = 0; i < needArray.length(); i++) {
					try {

						map.put(needArray.getJSONObject(i).getString("Name"),
								needArray.getJSONObject(i)
										.getJSONArray("values")
										.getJSONObject(0).getString("value"));
					} catch (Exception e) {
						logger.error(
								"Error occured while fetching TestCase {} ",
								e.getMessage());
					}

				}
				TestStep tStep = new TestStep();
				tStep.setAutLoginType(map.get(mapAttributes.get("AUT Login Type")));
				tStep.setAppName(map.get("user-02"));
				try {
					tStep.setDescription(Jsoup.parse(map.get("description")).body()
							.text());
				} catch (NullPointerException e1) {
					 logger.error("NullPointerException { }", e1.getMessage());
				}
				tStep.setModuleCode(map.get(mapAttributes.get("Function Code")));
				tStep.setOperation(map.get(mapAttributes.get("Operation")));
				tStep.setRowsToExecute(Integer.parseInt(map.get("user-05")));
				tStep.setParentTestCaseId(map.get("parent-id"));
				tStep.setType(map.get(mapAttributes.get("Step Type")));
				tStep.setDataId(map.get("user-08"));
				tStep.setId(map.get("id"));
				try {
					tStep.setExpectedResult(Jsoup.parse(map.get("expected")).body()
							.text());
				} catch (NullPointerException e) {
					 logger.error("NullPointerException { }", e.getMessage());
				}
				tStep.setShortDescription(map.get("name"));
				/*Added by Ashiki for ALM import starts*/
				tStep.setTxnMode(map.get("user-10"));
				/*Added by Ashiki for ALM import ends*/
				listTs.add(tStep);

			}
		} 
		  
		 catch (Exception e) {
			throw new ETMException(
					"Unable to fetech TestStep based on filter search value "
							+ fieldValue + " " + e.getMessage(),e);
		}
         
		return listTs;

	}
	/*Added By Ashiki for TJN252-60 ends*/
	/***********
	 * Synchronize of TestCases
	 * 
	 * @return
	 * @throws ETMException
	 ******************/
	public ArrayList<TestCase> syncTestCases(String baseUrl, String domain,
			String project, String cookieToken, List<String> tcRecIds)
			throws ETMException {
		

		JSONArray needArray = null;
		Map<String, String> map = new HashMap<String, String>();
		TestCase tCase = null;
		ArrayList<TestCase> listTc = new ArrayList<TestCase>();
		String targetUrl = null;

		String tcIds = "";
		for (String tcId : tcRecIds) {
			tcIds += tcId + "+" + "OR" + "+";
		}
		tcIds = tcIds.substring(0, tcIds.length() - 4);
		// logger.debug("--------------------- final tcIds {}",tcIds);
		try {
			targetUrl = baseUrl + "/rest/domains/" + domain + "/projects/"
					+ project + "/tests?query="
					+ URLEncoder.encode("{", "UTF-8") + "id"
					+ URLEncoder.encode("[", "UTF-8") + tcIds
					+ URLEncoder.encode("]}", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Url Encoding problem {}", e.getMessage());
		}

		String out = this.get(targetUrl, cookieToken);
		try {
			JSONObject rootObj = new JSONObject(out);
			if (Integer.parseInt(rootObj.getString("TotalResults")) == 0
					|| rootObj.getJSONArray("entities").isNull(0)) {
				logger.info("No result found based on the filter search value");

			}
			rootObj.getString("entities");
			JSONArray jarray = new JSONArray(rootObj.getString("entities"));
			jarray.getJSONObject(0).getJSONArray("Fields").length();
			for (int j = 0; j < jarray.length(); j++) {
				needArray = new JSONArray(jarray.getJSONObject(j).getString(
						"Fields"));

				for (int i = 0; i < needArray.length(); i++) {
					try {

						map.put(needArray.getJSONObject(i).getString("Name"),
								needArray.getJSONObject(i)
										.getJSONArray("values")
										.getJSONObject(0).getString("value"));
					} catch (Exception e) {
						logger.error(
								"Error occured while fetching TestCase {} ",
								e.getMessage());
					}

				}
				/*
				 * for(Map.Entry m:map.entrySet()){
				 * System.out.println(m.getKey()+" :"+m.getValue());
				 * 
				 * }
				 */
				tCase = new TestCase();

				try {
					tCase.setTcDesc(Jsoup.parse(map.get("description")).body()
							.text());
				} catch (NullPointerException e1) {
					
					 logger.error("NullPointerException { }", e1.getMessage());
				}
				tCase.setTcId(map.get("id"));
				tCase.setTcName(map.get("name"));
				tCase.setTcType(map.get("subtype-id"));
				tCase.setTcPriority(map.get("user-01"));
				tCase.setTcCreatedBy(map.get("owner"));
				/*changed by manish for bug T25IT-236 on 19-Aug-2017 starts*/
				if(map.get("user-02")!=null){
					tCase.setMode(map.get("user-02"));
				}else{
					tCase.setMode("GUI");
				}
				/*changed by manish for bug T25IT-236 on 19-Aug-2017 ends*/

				try {
					/*changed by manish for bug T25IT-309 on 29-Aug-2017 starts*/
					/*tCase.setTcCreatedOn(new SimpleDateFormat("dd-MM-YYYY",
							Locale.ENGLISH).parse(map.get("creation-time")));*/
					tCase.setTcCreatedOn(new SimpleDateFormat("yyyy-MM-dd",
							Locale.ENGLISH).parse(map.get("creation-time")));
					/*changed by manish for bug T25IT-309 on 29-Aug-2017 ends*/
				} catch (ParseException e) {
					logger.error("Date conversion Exception {}", e.getMessage());
				}
				listTc.add(tCase);

			}
		}catch (Exception e) {
			throw new ETMException("Unable to fetech TestCases"
					+ e.getMessage());
		}

		return listTc;
	}
	
	/*Added By Ashiki for TJN252-60 starts*/
	public ArrayList<TestCase> syncTestCases(String baseUrl, String domain,
			String project, String cookieToken, List<String> tcRecIds,Map<String,String> mapAttributes)
			throws ETMException {

		

		JSONArray needArray = null;
		Map<String, String> map = new HashMap<String, String>();
		TestCase tCase = null;
		ArrayList<TestCase> listTc = new ArrayList<TestCase>();
		String targetUrl = null;

		String tcIds = "";
		for (String tcId : tcRecIds) {
			tcIds += tcId + "+" + "OR" + "+";
		}
		tcIds = tcIds.substring(0, tcIds.length() - 4);
		// logger.debug("--------------------- final tcIds {}",tcIds);
		try {
			targetUrl = baseUrl + "/rest/domains/" + domain + "/projects/"
					+ project + "/tests?query="
					+ URLEncoder.encode("{", "UTF-8") + "id"
					+ URLEncoder.encode("[", "UTF-8") + tcIds
					+ URLEncoder.encode("]}", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Url Encoding problem {}", e.getMessage());
		}

		String out = this.get(targetUrl, cookieToken);
		try {
			JSONObject rootObj = new JSONObject(out);
			if (Integer.parseInt(rootObj.getString("TotalResults")) == 0
					|| rootObj.getJSONArray("entities").isNull(0)) {
				logger.info("No result found based on the filter search value");

			}
			rootObj.getString("entities");
			JSONArray jarray = new JSONArray(rootObj.getString("entities"));
			jarray.getJSONObject(0).getJSONArray("Fields").length();
			for (int j = 0; j < jarray.length(); j++) {
				needArray = new JSONArray(jarray.getJSONObject(j).getString(
						"Fields"));

				for (int i = 0; i < needArray.length(); i++) {
					try {

						map.put(needArray.getJSONObject(i).getString("Name"),
								needArray.getJSONObject(i)
										.getJSONArray("values")
										.getJSONObject(0).getString("value"));
					} catch (Exception e) {
						logger.error(
								"Error occured while fetching TestCase {} ",
								e.getMessage());
					}

				}
				tCase = new TestCase();

				try {
					tCase.setTcDesc(Jsoup.parse(map.get("description")).body()
							.text());
				} catch (NullPointerException e1) {
					 logger.error("NullPointerException { }", e1.getMessage());
				}
				tCase.setTcId(map.get(mapAttributes.get("Test Case Id")));
				tCase.setTcName(map.get("name"));
				
				if(map.get(mapAttributes.get("Test Case Type"))!=null){
					tCase.setTcType("Acceptance");
				}
				tCase.setTcPriority(map.get("user-01"));
				tCase.setTcCreatedBy(map.get("owner"));
				tCase.setMode("GUI");

				try {
					tCase.setTcCreatedOn(new SimpleDateFormat("yyyy-MM-dd",
							Locale.ENGLISH).parse(map.get("creation-time")));
				} catch (ParseException e) {
					logger.error("Date conversion Exception {}", e.getMessage());
				}
				listTc.add(tCase);

			}
		}catch (Exception e) {
			throw new ETMException("Unable to fetech TestCases"
					+ e.getMessage());
		}

		return listTc;
	
	
	}
	/*Added By Ashiki for TJN252-60 ends*/
	/***********
	 * Synchronize of TestSteps
	 * 
	 * @return
	 * @return
	 * @throws ETMException
	 ******************/
	public ArrayList<TestStep> syncTestSteps(String baseUrl, String domain,
			String project, String cookieToken, List<String> tcStepIds)
			throws ETMException {
		
		JSONArray needArray = null;
		Map<String, String> map = new HashMap<String, String>();
		
		ArrayList<TestStep> listTs = new ArrayList<TestStep>();
		String targetUrl = null;

		String tsIds = "";
		for (String tsId : tcStepIds) {
			tsIds += tsId + "+" + "OR" + "+";
		}
		tsIds = tsIds.substring(0, tsIds.length() - 4);
		//  logger.debug("--------------------- final tsIds {}",tsIds);
		try {
			targetUrl = baseUrl + "/rest/domains/" + domain + "/projects/"
					+ project + "/design-steps?query="
					+ URLEncoder.encode("{", "UTF-8") + "id"
					+ URLEncoder.encode("[", "UTF-8") + tsIds
					+ URLEncoder.encode("]}", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Url Encoding problem {}", e.getMessage());
		}

		String out = this.get(targetUrl, cookieToken);

		try {
			JSONObject rootObj = new JSONObject(out);
			if (Integer.parseInt(rootObj.getString("TotalResults")) == 0
					|| rootObj.getJSONArray("entities").isNull(0)) {
				logger.info("No result found based on the filter search value");

			}
			rootObj.getString("entities");
			JSONArray jarray = new JSONArray(rootObj.getString("entities"));
			jarray.getJSONObject(0).getJSONArray("Fields").length();
			for (int j = 0; j < jarray.length(); j++) {
				needArray = new JSONArray(jarray.getJSONObject(j).getString(
						"Fields"));

				for (int i = 0; i < needArray.length(); i++) {
					try {

						map.put(needArray.getJSONObject(i).getString("Name"),
								needArray.getJSONObject(i)
										.getJSONArray("values")
										.getJSONObject(0).getString("value"));
					} catch (Exception e) {
						logger.error(
								"Error occured while fetching TestCase {} ");
					}

				}
				TestStep tStep = new TestStep();
				tStep.setAutLoginType(map.get("user-05"));
				tStep.setAppName(map.get("user-02"));
				try {
					tStep.setDescription(Jsoup.parse(map.get("description")).body()
							.text());
				} catch (NullPointerException e) {
					 logger.error("NullPointerException { }", e.getMessage());
				
				}
				tStep.setModuleCode(map.get("user-03"));
				tStep.setOperation(map.get("user-06"));
				tStep.setRowsToExecute(Integer.parseInt(map.get("user-08")));
				tStep.setTestCaseRecordId(Integer.parseInt(map.get("parent-id")));
				tStep.setTxnMode(map.get("user-09"));
				tStep.setType(map.get("user-06"));
				tStep.setDataId(map.get("id")+"-"+map.get("name"));
				tStep.setId(map.get("user-07"));
				try {
					tStep.setExpectedResult(Jsoup.parse(map.get("expected")).body()
							.text());
				} catch (NullPointerException e) {
					 logger.error("NullPointerException { }", e.getMessage());
				}
				tStep.setShortDescription(map.get("user-11"));

				listTs.add(tStep);

			}
		} catch (Exception e) {
			throw new ETMException("Unable to fetech TestSteps"
					+ e.getMessage());
		}

		return listTs;
	}
	
	/*Added By Ashiki for TJN252-60 starts*/
	public ArrayList<TestStep> syncTestSteps(String baseUrl, String domain,
			String project, String cookieToken, List<String> tcStepIds,Map<String,String> mapAttributes)
			throws ETMException {

		
		JSONArray needArray = null;
		Map<String, String> map = new HashMap<String, String>();
		ArrayList<TestStep> listTs = new ArrayList<TestStep>();
		String targetUrl = null;

		String tsIds = "";
		for (String tsId : tcStepIds) {
			tsIds += tsId + "+" + "OR" + "+";
		}
		tsIds = tsIds.substring(0, tsIds.length() - 4);
		try {
			targetUrl = baseUrl + "/rest/domains/" + domain + "/projects/"
					+ project + "/design-steps?query="
					+ URLEncoder.encode("{", "UTF-8") + "id"
					+ URLEncoder.encode("[", "UTF-8") + tsIds
					+ URLEncoder.encode("]}", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Url Encoding problem {}", e.getMessage());
		}

		String out = this.get(targetUrl, cookieToken);

		try {
			JSONObject rootObj = new JSONObject(out);
			if (Integer.parseInt(rootObj.getString("TotalResults")) == 0
					|| rootObj.getJSONArray("entities").isNull(0)) {
				logger.info("No result found based on the filter search value");

			}
			rootObj.getString("entities");
			JSONArray jarray = new JSONArray(rootObj.getString("entities"));
			jarray.getJSONObject(0).getJSONArray("Fields").length();
			for (int j = 0; j < jarray.length(); j++) {
				needArray = new JSONArray(jarray.getJSONObject(j).getString(
						"Fields"));

				for (int i = 0; i < needArray.length(); i++) {
					try {

						map.put(needArray.getJSONObject(i).getString("Name"),
								needArray.getJSONObject(i)
										.getJSONArray("values")
										.getJSONObject(0).getString("value"));
					} catch (Exception e) {
						logger.error(
								"Error occured while fetching TestCase {} ");
					}

				}
				
				TestStep tStep = new TestStep();
				tStep.setAutLoginType(map.get(mapAttributes.get("AUT Login Type")));
				tStep.setAppName(map.get("user-02"));
				try {
					tStep.setDescription(Jsoup.parse(map.get("description")).body()
							.text());
				} catch (NullPointerException e1) {
					 logger.error("NullPointerException { }", e1.getMessage());
				}
				tStep.setModuleCode(map.get(mapAttributes.get("Function Code")));
				tStep.setOperation(map.get(mapAttributes.get("Operation")));
				tStep.setRowsToExecute(Integer.parseInt(map.get("user-05")));
				tStep.setParentTestCaseId(map.get("parent-id"));
				tStep.setType(map.get(mapAttributes.get("Step Type")));
				tStep.setDataId(map.get("user-08"));
				tStep.setId(map.get("id"));
				tStep.setTxnMode(map.get("user-09"));
				try {
					tStep.setExpectedResult(Jsoup.parse(map.get("expected")).body()
							.text());
				} catch (NullPointerException e) {
					 logger.error("NullPointerException { }", e.getMessage());
				}
				tStep.setShortDescription(map.get("name"));
			
				listTs.add(tStep);

			}
		} catch (Exception e) {
			throw new ETMException("Unable to fetech TestSteps"
					+ e.getMessage());
		}

		return listTs;
	
	}
	/*Added By Ashiki for TJN252-60 ends*/
	
	/*********** Logging out from HP-ALM *******************/
	public void logout(String baseUrl, String cookieToken) throws ETMException {
		String targetUrl = baseUrl + "/api/authentication/sign-out";
		 this.get(targetUrl, cookieToken);

	}

	/*********** Removing empty space from input textbox ******************/
	public  String trim(String str) {
		if (str == null) {
			return "";
		}
		return str.replace(String.valueOf((char) 160), " ").trim();
	}

	

}
