/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AppiumExecutionTaskHandlerImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Oct 27, 2017         Sameer Gupta          	Newly Added For  
 */

package com.ycs.tenjin.bridge.appium.taskhandler;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.appium.AppiumApplicationImpl;
import com.ycs.tenjin.bridge.appium.taskhandler.task.ExecutorTask;
import com.ycs.tenjin.bridge.appium.taskhandler.task.TaskFactory;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.RunAbortedException;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationAbstract;
import com.ycs.tenjin.bridge.oem.gui.taskhandler.ExecutionTaskHandler;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.IterationHelper;

public class AppiumExecutionTaskHandlerImpl implements ExecutionTaskHandler {

	protected static final Logger logger = LoggerFactory
			.getLogger(AppiumExecutionTaskHandlerImpl.class);

	protected AppiumApplicationImpl oApplication;

	public AppiumExecutionTaskHandlerImpl(GuiApplicationAbstract oApplication) {
		this.oApplication = (AppiumApplicationImpl) oApplication;
	}

	public IterationStatus executeFunctionImpl(ExecutionStep step,
			int iteration, JSONObject iterationData, int screenshotOption,
			int runId, int fieldTimeout) throws RunAbortedException {

		IterationHelper helper = new IterationHelper();

		logger.info("Initiating Executor");

		ExecutorTask executor = null;
		IterationStatus iStatus = null;
		try {
			executor = TaskFactory.getExecutor(this.oApplication, runId, step,
					screenshotOption, fieldTimeout);

			iStatus = executor.executeIteration(iterationData,
					this.oApplication, iteration);
			logger.debug("Thread for Run {} is still alive", runId);

		} catch (BridgeException e) {
			logger.error(
					"Executor invocation failed for step [{}] - Record ID [{}]",
					step.getId(), step.getRecordId());
			try {
				logger.info(
						"Setting results for all transactions in Step [{}] - Record ID[{}] to E",
						step.getId(), step.getRecordId());
				helper.updateRunStatusForAllTransactions(runId,
						step.getRecordId(), "E", e.getMessage());
			} catch (DatabaseException e1) {
				logger.error(e1.getMessage());
			}
			try {
				logger.info("Destroying Driver");
				oApplication.destroy();
			} catch (Exception ignore) {
			}
		}

		return iStatus;

	}

}
