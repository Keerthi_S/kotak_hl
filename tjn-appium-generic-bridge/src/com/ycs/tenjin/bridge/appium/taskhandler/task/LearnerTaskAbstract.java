/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LearnerTaskAbstract.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Oct 27, 2017         Sameer Gupta          	Newly Added For 
 * 26-09-2018			Preeti					TENJINCG-742 
 */

package com.ycs.tenjin.bridge.appium.taskhandler.task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.bridge.appium.AppiumApplicationImpl;
import com.ycs.tenjin.bridge.appium.generic.DriverContext;
import com.ycs.tenjin.bridge.appium.generic.MobileGenericMethodsImpl;
import com.ycs.tenjin.bridge.appium.mobile.CommonMethods;
import com.ycs.tenjin.bridge.appium.mobile.ExecutionMethods;
import com.ycs.tenjin.bridge.appium.mobile.LearningMethods;
import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.enums.PageType;
import com.ycs.tenjin.bridge.exceptions.AssistedLearningException;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Metadata;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.utils.AssistedLearningUtils;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

public abstract class LearnerTaskAbstract implements LearnerTask {

	protected static final Logger logger = LoggerFactory
			.getLogger(LearnerTaskAbstract.class);

	// Constructor Variables
	protected AppiumApplicationImpl oApplication;
	protected Module function;
	protected DriverContext driverContext;
	protected CommonMethods commonMethods;
	protected LearningMethods learningMethods;
	protected ExecutionMethods executionMethods;
	protected MobileGenericMethodsImpl mobileGenericMethodsImpl;

	// Result/Output Variables
	protected Multimap<String, Location> pageAreas = ArrayListMultimap.create();
	protected ArrayList<TreeMap<String, TestObject>> testObjectMap = new ArrayList<TreeMap<String, TestObject>>();
	protected HashMap<String, ArrayList<TestObject>> testObjectByLocation = new HashMap<String, ArrayList<TestObject>>();

	// global variable
	protected int pageSequence = 0;
	protected int fieldSequence = 0;
	protected int tabOrder = 0;
	protected Multimap<String, TestObject> assistedLearningData = null;

	public LearnerTaskAbstract(AppiumApplicationImpl oApplication,
			Module function) {

		logger.info("LearnerTaskAbstract");
		this.oApplication = oApplication;
		this.function = function;
		this.driverContext = oApplication.getDriverContext();
		this.mobileGenericMethodsImpl = new MobileGenericMethodsImpl(this.driverContext); 
	}

	@Override
	final public Metadata learn() throws LearnerException {

		logger.info("Loading assisted learning data");
		// Get the Assisted Learning data
		try {
			/*Modified by Preeti for TENJINCG-742 starts*/
			/*this.assistedLearningData = AssistedLearningUtils
					.getAssistedLearningData(this.function.getCode(),
							this.oApplication.getAppName(), null);*/
			this.assistedLearningData = AssistedLearningUtils.getAssistedLearningData(this.function.getCode(), this.oApplication.getAppId());
			/*Modified by Preeti for TENJINCG-742 ends*/
		} catch (AssistedLearningException e) {
			logger.error(
					"ERROR loading assisted learning data for function {}",
					this.function.getCode(), e);
		}

		this.preLearning();

		Metadata metadata = new Metadata();

		logger.info("Beginning learning of function {}",
				this.function.getCode());

		// At this point, we will be in the landing page of a given function.
		// Update the location as such.
		Location location = new Location();
		location.setLandingPage(true);
		location.setLocationType(PageType.MAIN.toString());
		location.setParent("N-A");
		location.setSequence(this.pageSequence++);
		location.setWayIn("Navigate to Function");
		location.setWayOut("Exit");
		location.setLabel(this.getScreenTitle(location));

		try {
			this.addLocationToMap(location);
		} catch (Exception e) {
			throw new LearnerException(e.getMessage(), e);
		}

		this.navigateAndLearn(location);

		this.postLearning();

		metadata.setPageAreas(this.pageAreas);
		metadata.setTestObjectMap(this.testObjectMap);
		return metadata;
	}

	final public void navigateAndLearn(Location currentLocation) {

		this.prePageLearning(currentLocation);

		this.performAssistedLearning(currentLocation, "pre_");

		this.learnScreenElements(currentLocation);

		this.performAssistedLearning(currentLocation, "");

		try {

			ArrayList<Location> listLocationsToTraverse = this
					.getAllPageAreas(currentLocation);

			for (Location objLocation : listLocationsToTraverse) {

				this.commonMethods.navigateToPage(objLocation);

				String screenTitle = this.getScreenTitle(objLocation);

				if (objLocation.getLocationType() == null
						|| objLocation.getLocationType().equals("")) {
					throw new LearnerException(
							"Location Type not set in adapter.");
				} else if (objLocation.getWayIn() == null
						|| objLocation.getWayIn().equals("")) {
					throw new LearnerException("Way in not set in adapter.");
				}

				Location location = new Location();
				location.setLabel(screenTitle);
				location.setLandingPage(false);
				location.setLocationType(objLocation.getLocationType());
				location.setParent(currentLocation.getLocationName());
				location.setSequence(this.pageSequence++);
				location.setWayIn(objLocation.getWayIn());
				// TODO Way Out
				location.setWayOut("NA");

				try {
					this.addLocationToMap(location);
				} catch (Exception e) {
					throw new LearnerException(e.getMessage(), e);
				}

				this.navigateAndLearn(location);

			}

		} catch (Exception e) {
			logger.debug("No page to navigate or could not navigate");

		}

		this.postPageLearning(currentLocation);

	}

	final public void learnScreenElements(Location currentLocation) {

		logger.info("Learning screen {}", currentLocation);
		TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();
		ArrayList<TestObject> testObjects = null;

		List<MobileElement> elements = null;
		try {
			this.tabOrder = 0;
			elements = this.driverContext.getDriver().findElements(
					MobileBy.xpath(this.oApplication.getCompositeXpath()));
			testObjects = this.getAllVisibleElement(currentLocation, elements);
		} catch (Exception e) {
			logger.error("Could not get all visible elements on {}",
					currentLocation);
		}

		for (TestObject t : testObjects) {
			pMap.put(t.getUniqueId(), t);
		}

		this.testObjectByLocation.put(currentLocation.getLocationName(), testObjects);
		this.testObjectMap.add(pMap);

	}

	private ArrayList<TestObject> getAllTestObjectsForPage(
			Location currentLocation) {
		ArrayList<TestObject> testObjects = null;

		try {
			List<MobileElement> elements = this.driverContext.getDriver()
					.findElements(
							By.xpath(this.oApplication.getCompositeXpath()));
			testObjects = this.getAllVisibleElement(currentLocation, elements);

		} catch (Exception e) {
			logger.error("Could not get all visible elements on {}",
					currentLocation);
		}
		return testObjects;
	}

	final public void addLocationToMap(Location location)
			throws LearnerException {
		try {

			// Set Location Screen Title
			location.setScreenTitle(location.getLabel());

			// Set Location Name
			String locationName = location.getLabel();
			// changed by shivam for increasing count of existing page area - starts
			
			int count = 0;
			for (String key : this.pageAreas.keys()) {
				Location loc = (Location) (this.pageAreas.get(key).toArray()[0]);

				if (loc.getLabel().equalsIgnoreCase(location.getLabel())) {
					count = count + 1;
				}
			}

			logger.debug("Scanning map for existing pages with name " + locationName);
			if (count > 0) {
				logger.debug("There is already " + count + " page(s) with name " + locationName);
				int nc = count + 1;

				String newLocName = "00" + Integer.toString(nc);
				newLocName = newLocName.substring(newLocName.length() - 2);
				locationName = locationName + " " + newLocName;
				logger.debug("New Location Name will be " + newLocName);

			} else {
				logger.debug("There are no existing pages with name " + locationName);
			}

			// changed by shivam for increasing count of existing page area - ends
			location.setLocationName(locationName);

			if (location.getLocationType().equals(PageType.TABLE.toString())) {
				location.setMultiRecordBlock(true);
			}

			// Validate Location
			this.validateLocation(location);

			this.pageAreas.put(location.getLocationName(), location);
			logger.info("New Location --> {}", location.toString());
		} catch (Exception e) {
			throw new LearnerException("Could not resolve location name for " + location.getLocationName(), e);
		}
	}

	final public TestObject getTestObject(Location currentLocation,
			FieldType type, String label, String identifiedBy, String uniqueId,
			Integer index, boolean mandatory, String dropdownValues) {

		TestObject t = new TestObject();

		t.setTabOrder(this.tabOrder++);
		t.setSequence(this.fieldSequence++);
		// No need to set instance here as setting up of instance will be taken
		// care before persist
		// t.setInstance(instance);

		t.setLabel(label);
		// No need to set name here as setting up of name will be taken care
		// before persist
		// t.setName(name);

		t.setObjectClass(type.toString());
		t.setIdentifiedBy(identifiedBy);
		t.setUniqueId(uniqueId);
		t.setIndex(index);

		t.setDefaultOptions(dropdownValues);
		if (mandatory) {
			t.setMandatory("YES");
		} else {
			t.setMandatory("NO");
		}

		t.setLocation(currentLocation.getLocationName());
		if (currentLocation.isMultiRecordBlock()) {
			t.setIsMultiRecord("Y");
		} else {
			t.setIsMultiRecord("N");
		}

		// Set default values. Depending on application these values may be
		// required. So if required setup in adapter.
		t.setViewmode("Y");
		t.setLovAvailable("N");
		t.setIsFooterElement("N");
		t.setAutoLovAvailable("N");

		return t;
	}

	protected void performAssistedLearning(Location currentLocation,
			String stage) throws LearnerException {

		this.preAssistedLearning();

		logger.debug("Entering Assisted Learning Input for screen --> "
				+ currentLocation);

		if (this.assistedLearningData != null) {

			Collection<TestObject> fields = this.assistedLearningData.get(stage
					+ currentLocation.getLocationName());

			logger.debug("Found {} fields to input on this screen",
					fields.size());

			ArrayList<TestObject> testObjects = null;
			boolean findTestObjectsFirstTime = true;

			for (TestObject field : fields) {

				System.out.println(field);
				try {

					if (field.getIdentifiedBy() == null
							|| field.getIdentifiedBy().equals("")
							|| field.getIdentifiedBy().equals("id")) {

						TestObject targetField = null;
						for (int i = 0; i < 2; i++) {

							if (findTestObjectsFirstTime) {
								//changed by Sahana, handling post asst learning:Starts
								if (stage.equalsIgnoreCase("")) {
									//changed by Sahana, handling post asst learning:Starts
									if (this.testObjectByLocation
											.containsKey(currentLocation
													.getLocationName())) {
										testObjects = this.testObjectByLocation
												.get(currentLocation
														.getLocationName());
									}
								} else {
									testObjects = this
											.getAllTestObjectsForPage(currentLocation);
								}
								findTestObjectsFirstTime = false;
							}

							for (TestObject t : testObjects) {
								if (field.getIdentifiedBy() == null
										|| field.getIdentifiedBy().equals("")) {
									if (t.getLabel().trim()
											.equalsIgnoreCase(field.getLabel())) {
										targetField = t;
										break;
									}
								} else if (field.getIdentifiedBy().equals("id")) {
									if (t.getUniqueId().trim()
											.equalsIgnoreCase(field.getLabel())) {
										targetField = t;
										break;
									}
								}
							}

							if (targetField != null) {
								break;
							} else {
								//changed by Sahana, handling pre asst learning:Starts
								if (stage.equalsIgnoreCase("pre_") && i == 0) {
									//changed by Sahana, handling pre asst learning:Ends
									testObjects = this
											.getAllTestObjectsForPage(currentLocation);
								} else {
									break;
								}
							}
						}

						// Operate on the field
						if (targetField != null) {
							logger.debug("Found target field");

							MobileElement targetele = this.executionMethods
									.locateElement(targetField);

							try {
								this.executionMethods.performInput(targetele,
										targetField, field.getData());
							} catch (AutException e) {
								throw new LearnerException(e.getMessage());
							}
						} else {
							throw new LearnerException(
									"Field for assisted learning not found.");
						}

					} else if (field.getIdentifiedBy().toLowerCase()
							.startsWith("button")) {

						logger.debug("Selecting navigation button {}",
								field.getLabel());

						try {
							TestObject t = new TestObject();
							t.setObjectClass(FieldType.BUTTON.toString());
							t.setData(field.getLabel());
							MobileElement element = this.executionMethods
									.locateElement(t);
							this.executionMethods.performInput(element, t, "");

						} catch (Exception e) {
							throw new LearnerException(e.getMessage());
						}
					} else if (field.getIdentifiedBy().toLowerCase()
							.startsWith("wait")) {

						logger.debug("Wait for element {}", field.getLabel());

						try {
							int seconds = 1;
							if (field.getData() != null
									&& !field.getData().equals("")) {
								seconds = Integer.parseInt(field.getData());
							}
							this.mobileGenericMethodsImpl.explicitWait(seconds);
						} catch (Exception e) {
							throw new LearnerException(e.getMessage());
						}
					} else if (field.getIdentifiedBy()
							.equalsIgnoreCase("xpath")) {
						MobileElement element = this.driverContext.getDriver()
								.findElement(MobileBy.xpath(field.getLabel()));
						if (element != null && field.getData() != null
								&& !field.getData().isEmpty()) {
							element.setValue(field.getData());
						}
					}

					this.performAssistedLearningExtended(currentLocation,
							stage, field);

				} catch (Exception ex) {
					logger.error("Could not perform assisted learning for "
							+ field.getLabel());
				}

			}

		}

		this.postAssistedLearning();

	}

	// Abstract Methods
	// /////////////////////////////////////////////////////////////////////

	/**
	 * Add Required Logic before starting with learning.
	 */
	public abstract void preLearning() throws LearnerException;

	/**
	 * Add Required Logic after completing learning.
	 */
	public abstract void postLearning() throws LearnerException;

	/**
	 * Add Required Logic before learning the page.
	 */
	public abstract void prePageLearning(Location currentLocation)
			throws LearnerException;

	/**
	 * Add Required Logic after learning the page.
	 */
	public abstract void postPageLearning(Location currentLocation)
			throws LearnerException;

	/**
	 * Add Required Logic before starting with Assisted learning.
	 */
	public abstract void preAssistedLearning() throws LearnerException;

	/**
	 * Add Required Logic after completing Assisted learning.
	 */
	public abstract void postAssistedLearning() throws LearnerException;

	/**
	 * Add Required logic to get all visible elements that are required to be
	 * added for current page. <br>
	 * <br>
	 * By default list of elements are coming from composite path, if you don't
	 * want to use that composite path and add new logic to get list of
	 * elements, override 'elements' parameter.
	 */
	public abstract ArrayList<TestObject> getAllVisibleElement(
			Location currentLocation, List<MobileElement> elements)
					throws LearnerException;

	/**
	 * Get list of all pages in current location. <br>
	 * <br>
	 * Set location type and way in here for all locations.
	 */
	public abstract ArrayList<Location> getAllPageAreas(Location currentLocation)
			throws LearnerException;

	/**
	 * This method is called just before adding the location to map, so if you
	 * need to validate or change any property of location do it here.<br>
	 * <br>
	 * Mostly name and labels are validated here.
	 */
	public abstract void validateLocation(Location location)
			throws LearnerException;

	/**
	 * Get title of current page.
	 */
	public abstract String getScreenTitle(Location objLocation)
			throws LearnerException;

	/**
	 * This method can be used to enhance the functionality of assisted
	 * learning.
	 */
	public abstract Map<String, Object> performAssistedLearningExtended(
			Location currentLocation, String stage, TestObject field);

}
