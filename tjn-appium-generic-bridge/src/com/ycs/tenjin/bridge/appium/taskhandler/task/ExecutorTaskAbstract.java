/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutorTaskAbstract.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Oct 27, 2017         Sameer Gupta          	Newly Added For  
 * 04-06-2018			Preeti					TENJINCG-595
 * 09-01-2019			Pushpa					TJN252-64
 */

package com.ycs.tenjin.bridge.appium.taskhandler.task;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.bridge.appium.AppiumApplicationImpl;
import com.ycs.tenjin.bridge.appium.generic.DriverContext;
import com.ycs.tenjin.bridge.appium.generic.MobileGenericMethodsImpl;
import com.ycs.tenjin.bridge.appium.mobile.CommonMethods;
import com.ycs.tenjin.bridge.appium.mobile.ExecutionMethods;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.exceptions.RunAbortedException;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.RuntimeScreenshot;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.util.Utilities;

import io.appium.java_client.MobileElement;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public abstract class ExecutorTaskAbstract implements ExecutorTask {

	private static final Logger logger = LoggerFactory
			.getLogger(ExecutorTaskAbstract.class);

	// Constructor Variables
	protected int runId;
	protected ExecutionStep step;
	protected int screenshotOption;
	protected int fieldTimeout;

	// Result/Output Variables
	protected IterationStatus iStatus = null;
	protected List<ValidationResult> results = null;
	protected List<RuntimeScreenshot> runtimeScreenshots = null;
	protected List<RuntimeFieldValue> runtimeValues = null;

	// Main Method Variables
	protected AppiumApplicationImpl oApplication;
	protected int iteration;
	protected DriverContext driverContext;
	protected ExecutionMethods executionMethods;
	protected CommonMethods commonMethods;
	protected MobileGenericMethodsImpl mobileGenericMethods;

	// global variable
	/*Changed by Pushpa for  TJN252-64 starts*/
	protected int screenshotIndex = 1;
	/*Changed by Pushpa for  TJN252-64 ends*/
	protected String tdUid;
	
	//added by shivam for execution speed starts
	// Parameter Variable 
	public int waitTimeForExecutionSpeedNormal = 0;
	public int waitTimeForExecutionSpeedMedium = 2;
	public int waitTimeForExecutionSpeedSlow = 5;
	//added by shivam for execution speed ends

	public ExecutorTaskAbstract(int runId, ExecutionStep step,
			int screenshotOption, int fieldTimeout) {
		logger.info("ExecutorTaskAbstract");
		this.runId = runId;
		this.step = step;
		this.screenshotOption = screenshotOption;
		this.fieldTimeout = fieldTimeout;

	}

	@Override
	final public IterationStatus executeIteration(JSONObject dataSet,
			AppiumApplicationImpl oApplication, int iteration)
			throws RunAbortedException {

		this.iStatus = new IterationStatus();
		this.results = new ArrayList<ValidationResult>();
		this.runtimeScreenshots = new ArrayList<RuntimeScreenshot>();
		this.runtimeValues = new ArrayList<RuntimeFieldValue>();

		this.oApplication = oApplication;
		this.iteration = iteration;
		this.driverContext = this.oApplication.getDriverContext();

		this.mobileGenericMethods = new MobileGenericMethodsImpl(
				this.driverContext);

		logger.debug("entered executeIteration()");
		logger.debug(
				"Dataset for Iteration {}, Step {}, Record ID {} is as follows",
				iteration, this.step.getId(), this.step.getRecordId());
		logger.debug(dataSet.toString());

		try {

			tdUid = dataSet.getString("TDUID");

			JSONArray pageAreas = dataSet.getJSONArray("PAGEAREAS");
			int pageAreaCount = pageAreas.length();
			logger.info(
					"Retrieved Pageareas to traverse. This step needs to traverse through {} pageareas",
					pageAreaCount);

			/* Added by Preeti for TENJINCG-595 starts */
			logger.debug("Checking if thread for run {} is alive", this.runId);
			if (CacheUtils.isRunAborted(this.runId)) {
				logger.debug("Thread for Run {} is interrupted.", this.runId);
				throw new RunAbortedException();
			}

			logger.debug("Thread for Run {} is still alive", this.runId);
			/* Added by Preeti for TENJINCG-595 ends */

			// /////////////////////////////////////////////////////////////////////

			try {

				this.initialize();

				this.preExecution();
				// Operation Related Code
				this.processApplicationOperation();
				// All Page Processing Starts here
				this.processAllPageAreas(pageAreas);

				// Handle Iteration Status
				this.processIterationStatus();

				if (this.iStatus.getResult().equalsIgnoreCase("S")) {
					// Capture Screenshot
					this.capturePageScreenshot(true);
				} else {
					this.captureFailureScreenshot();
				}
				// Once all pages are completed, commit the operation
				this.postExecution();

			} catch (ExecutorException e) {

				String errorMessage = this.checkForErrorMessage();

				if (errorMessage == null || errorMessage.isEmpty()) {
					this.iStatus.setResult("E");
					this.iStatus.setMessage(e.getMessage());
				} else {
					this.iStatus.setResult("F");
					this.iStatus.setMessage(errorMessage);
				}

				this.captureFailureScreenshot();
				return this.iStatus;
			} catch (RunAbortedException e) {
				logger.error("Run aborted", e);
				this.iStatus.setResult("E");
				this.iStatus.setMessage(e.getMessage());
				this.captureFailureScreenshot();
				return this.iStatus;
			} catch (Exception e) {
				logger.error("ERROR processing page areas", e);
				this.iStatus.setResult("E");
				this.iStatus
						.setMessage("Execution aborted abnormally due to an unknown error");
				this.captureFailureScreenshot();
				return this.iStatus;
			}

			// /////////////////////////////////////////////////////////////////////

		} catch (JSONException e) {
			logger.error("ERROR in Test Data JSON --> {}", e.getMessage(), e);
			this.iStatus.setResult("E");
			this.iStatus
					.setMessage("Could not process Test data for this step due to an internal error");
		} finally {
			this.iStatus.setRuntimeFieldValues(this.runtimeValues);
			this.iStatus.setRuntimeScreenshots(this.runtimeScreenshots);
			this.iStatus.setValidationResults(this.results);
		}

		return this.iStatus;

	}

	final public void processAllPageAreas(JSONArray pageAreas)
			throws ExecutorException, JSONException, RunAbortedException {

		/* Added by Preeti for TENJINCG-595 starts */
		logger.debug("Checking if thread for run {} is alive", this.runId);
		if (CacheUtils.isRunAborted(this.runId)) {
			logger.debug("Thread for Run {} is interrupted.", this.runId);
			throw new RunAbortedException();
		}

		logger.debug("Thread for Run {} is still alive", this.runId);
		/* Added by Preeti for TENJINCG-595 ends */

		for (int i = 0; i < pageAreas.length(); i++) {

			JSONObject pageArea = pageAreas.getJSONObject(i);

			Location location = Location.loadFromJson(pageArea);

			JSONArray detailRecords = pageArea.getJSONArray("DETAILRECORDS");
			if (detailRecords == null) {
				logger.error("DETAILRECORDS is null for page area {}",
						location.getLocationName());
				return;
			}

			if (i > 0) {
				logger.info("Navigating to page {}", location.getLocationName());
				try {
					this.capturePageScreenshot(false);
					/* Added by Preeti for TENJINCG-595 starts */
					logger.debug("Checking if thread for run {} is alive",
							this.runId);
					if (CacheUtils.isRunAborted(this.runId)) {
						logger.debug("Thread for Run {} is interrupted.",
								this.runId);
						throw new RunAbortedException();
					}

					logger.debug("Thread for Run {} is still alive", this.runId);
					/* Added by Preeti for TENJINCG-595 ends */
					this.commonMethods.navigateToPage(location);
				} catch (Exception e) {
					throw new ExecutorException("Fail to navigate to page '"
							+ location.getLocationName() + "'.", e);
				}
			}

			this.prePageExecution(location);

			/* Added by Preeti for TENJINCG-595 starts */
			logger.debug("Checking if thread for run {} is alive", this.runId);
			if (CacheUtils.isRunAborted(this.runId)) {
				logger.debug("Thread for Run {} is interrupted.", this.runId);
				throw new RunAbortedException();
			}

			logger.debug("Thread for Run {} is still alive", this.runId);
			/* Added by Preeti for TENJINCG-595 ends */

			if (!location.isMultiRecordBlock()) {
				this.preSingleRecordPageAreaExecution(location);
				// Handle Scenario for non MRB page area
				this.processSingleRecordPageArea(detailRecords, location);
				this.postSingleRecordPageAreaExecution(location);
			} else {
				this.preMultiRecordPageAreaExecution(location);
				// Handle Scenario for MRB page area
				this.processMultiRecordPageArea(detailRecords, location);
				this.postMultiRecordPageAreaExecution(location);
			}

			/* Added by Preeti for TENJINCG-595 starts */
			logger.debug("Checking if thread for run {} is alive", this.runId);
			if (CacheUtils.isRunAborted(this.runId)) {
				logger.debug("Thread for Run {} is interrupted.", this.runId);
				throw new RunAbortedException();
			}

			logger.debug("Thread for Run {} is still alive", this.runId);
			/* Added by Preeti for TENJINCG-595 ends */

			this.postPageExecution(location);

		}
	}

	final public void processSingleRecordPageArea(JSONArray detailRecords,
			Location location) throws ExecutorException {
		try {

			logger.info("{} detail records are present for page {}",
					detailRecords.length(), location.getLocationName());
			logger.debug("Getting master record at index {}", 0);

			JSONObject detail = detailRecords.getJSONObject(0);
			String dId = detail.getString("DR_ID");

			logger.info("Executing --> Page [{}], Detail Record [{}]",
					location.getLocationName(), dId);
			logger.debug("Getting all fields JSONArray for detail record {}",
					dId);

			JSONArray fields = detail.getJSONArray("FIELDS");
			for (int i = 0; i < fields.length(); i++) {

				logger.debug("Getting field information at index {}", i);
				JSONObject field = fields.getJSONObject(i);

				logger.debug("Loading test object");
				TestObject t = TestObject.loadFromJson(field);

				if (t == null) {
					logger.error("Could not load TestObject from JSON");
					continue;
				}
				logger.debug("Processing I/O --> Field [{}], Page [{}]",
						t.getLabel(), location.getLocationName());

				t.setLocation(location.getLocationName());

				this.processField(location, t);
				
				//added by shivam for execution speed starts
				this.waitAsPerExecutionSpeed();
				//added by shivam for execution speed ends

			}
		} catch (JSONException e) {
			throw new ExecutorException(e.getMessage(), e);
		}
	}

	// TODO handle Multi Record Page Area
	final public void processMultiRecordPageArea(JSONArray detailRecords,
			Location location) throws ExecutorException {

	}

	final public void processField(Location location, TestObject t) {

		this.preFieldExecution(location, t);

		MobileElement element = null;
		try {

			logger.debug("Locating element {} on page {}", t.getLabel(),
					location.getLocationName());
			try {
				element = this.executionMethods.locateElement(t);
			} catch (Exception e) {
			}

			if (element == null) {
				System.err.println();
			}
			if (t.getAction().equalsIgnoreCase("MANUAL")
					|| t.getData().equalsIgnoreCase("@@MANUAL")) {
				logger.info("performing Manual on element");
				CacheManager cm = CacheManager.getInstance();
				try {
					cm.addCache("cacheStore");
				} catch (Exception e) {
				}
				Cache cache = cm.getCache("cacheStore");

				t.setData("false");
				t.setLocation(location.getLocationName());
				cache.put(new Element(this.runId, t));
				Element ele = cache.get(this.runId);
				try {
					String execConfigPath = TenjinConfiguration.getProperty(
							"MANUAL_TIME").trim();
					if (execConfigPath != null
							&& !execConfigPath.equalsIgnoreCase("")) {
						int idletimetolive = Integer.parseInt(execConfigPath);
						int seconds = 60;
						ele.setTimeToLive(idletimetolive * seconds);
					}
				} catch (Exception e) {
				}

				TestObject output = (TestObject) (ele == null ? null : ele
						.getObjectValue());
				System.out.println("In Manual " + t.getLabel() + " "
						+ output.getData());

			} else if (t.getAction().equalsIgnoreCase("input")) {
				logger.debug("Performing Input/Output");

				if (Utilities.trim(t.getData()).equalsIgnoreCase("@@OUTPUT")) {
					String output = this.executionMethods.performOutput(
							element, t);
					RuntimeFieldValue r = this.getRuntimeFieldValueObject(
							t.getLabel(), output, 1);
					this.runtimeValues.add(r);
				} else {
					logger.debug("Performing input");
					this.executionMethods.performInput(element, t, t.getData());
					logger.info(
							"Performed Input -- Field {}, Page {}, Data {}",
							t.getLabel(), location.getLocationName(),
							t.getData());
				}
			} else if (t.getAction().equalsIgnoreCase("verify")) {
				String output = this.executionMethods.performOutput(element, t);
				ValidationResult valRes = this.getValidationResultObject(
						location.getLocationName(), t.getLabel(), t.getData(),
						output, "1");
				this.results.add(valRes);
			}
		} catch (AutException e) {
			throw new ExecutorException(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(
					"UNKNOWN ERROR occurred while processing field [{}] on page [{}]",
					t.getLabel(), location.getLocationName(), e);
			throw new ExecutorException("Could not process field ["
					+ t.getLabel() + "] on page [" + location.getLocationName()
					+ "]. Please contact Tenjin Support.");
		}

		this.postFieldExecution(location, t);
	}

	// Above this all methods are flow related
	// Create Objects
	final public ValidationResult getValidationResultObject(String page,
			String field, String expectedValue, String actualValue,
			String detailRecordNo) {
		ValidationResult r = new ValidationResult();
		actualValue = Utilities.trim(actualValue);
		r.setActualValue(actualValue);
		r.setExpectedValue(expectedValue);
		r.setDetailRecordNo(detailRecordNo);
		r.setField(field);
		r.setPage(page);
		r.setIteration(this.iteration);
		r.setResValId(0);
		r.setRunId(this.runId);
		r.setTestStepRecordId(this.step.getRecordId());

		if (Utilities.isNumeric(actualValue)
				&& Utilities.isNumeric(expectedValue)) {
			if (Utilities.isNumericallyEqual(actualValue, expectedValue)) {
				r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
			} else {
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		} else if (actualValue.equalsIgnoreCase(expectedValue)) {
			/*************
			 * Fix by Sriram for comparision of numeric values Ends
			 */

			r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
		} else {

			if (Utilities.isNumeric(expectedValue)
					&& Utilities.isNumeric(actualValue)) {
				if (Double.parseDouble(expectedValue) == Double
						.parseDouble(actualValue)) {
					r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
				} else {
					r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
				}
			} else {
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		}
		logger.info(r.toString());
		return r;
	}

	final public RuntimeFieldValue getRuntimeFieldValueObject(String field,
			String value, int detailRecordNo) {
		RuntimeFieldValue r = new RuntimeFieldValue();

		r.setDetailRecordNo(detailRecordNo);
		r.setField(field);
		r.setIteration(this.iteration);
		r.setRunId(this.runId);
		r.setTdGid(this.step.getTdGid());
		r.setTdUid(tdUid);
		r.setTestStepRecordId(step.getRecordId());
		r.setValue(value);

		logger.info(r.toString());
		return r;
	}

	final public RuntimeScreenshot getRuntimeScreenshotObject(
			String screenshotType) {

		RuntimeScreenshot ras = new RuntimeScreenshot();

		ras.setIteration(this.iteration);
		ras.setRunId(this.runId);
		ras.setSequence(this.screenshotIndex++);
		ras.setTdUid(tdUid);
		ras.setTdGid(this.step.getTdGid());
		ras.setTestStepRecordId(this.step.getRecordId());
		ras.setTimestamp(new Timestamp(new Date().getTime()));
		ras.setType(screenshotType);
		try {
			ras.setScreenshot(this.mobileGenericMethods
					.captureScreenshotAsFile());
		} catch (Exception e) {
			logger.error("ERROR capturing {} screenshot", screenshotType, e);
		}

		return ras;
	}

	final public void capturePageScreenshot(boolean lastPage)
			throws ExecutorException {

		boolean takeScreenshot = false;

		if (this.screenshotOption == 2 && lastPage) {
			takeScreenshot = true;
		} else if (this.screenshotOption == 3) {
			takeScreenshot = true;
		}

		if (takeScreenshot) {

			RuntimeScreenshot ras = this
					.getRuntimeScreenshotObject("End Of Page");
			this.runtimeScreenshots.add(ras);
		}
	}

	final public void captureFailureScreenshot() throws ExecutorException {

		if (this.screenshotOption > 0) {
			RuntimeScreenshot ras = this
					.getRuntimeScreenshotObject("Failure/Error");
			this.runtimeScreenshots.add(ras);
		}
	}
	
	//Added by shivam for Execution Speed starts
		final public void waitAsPerExecutionSpeed() throws ExecutorException {
			try {

				switch (fieldTimeout) {
				case 0:
					if (this.waitTimeForExecutionSpeedNormal > 0)
						Thread.sleep(this.waitTimeForExecutionSpeedNormal * 1000);
					break;
				case 1:
					if (this.waitTimeForExecutionSpeedMedium > 0)
						Thread.sleep(this.waitTimeForExecutionSpeedMedium * 1000);
					break;
				case 3:
					if (this.waitTimeForExecutionSpeedSlow > 0)
						Thread.sleep(this.waitTimeForExecutionSpeedSlow * 1000);
					break;
				default:
					break;
				}
			} catch (Exception ignore) {
			}
		}

		//Added by shivam for Execution Speed ends

	// Abstract Methods
	// /////////////////////////////////////////////////////////////////////

	/**
	 * Initialize the classes here. There is difference here as for execution
	 * application value doesn't come while constructor but during method call.
	 * Following classes needs to be initialized here:-<br>
	 * this.commonMethods = new CommonMethodsImpl(driverContext);<br>
	 * this.executionMethods = new IosExecutionMethodsImpl(this.driverContext); <br>
	 */
	public abstract void initialize() throws ExecutorException;

	/**
	 * Add Required Logic before starting with execution.
	 */
	public abstract void preExecution() throws ExecutorException;

	/**
	 * Add Required Logic after completing execution.
	 */
	public abstract void postExecution() throws ExecutorException;

	/**
	 * Add Application Operation specific Logic here.
	 */
	public abstract void processApplicationOperation() throws ExecutorException;

	/**
	 * Add Required Logic before starting the execution for the page.
	 */
	public abstract void prePageExecution(Location location)
			throws ExecutorException;

	/**
	 * Add Required Logic after the execution for the page is completed.
	 */
	public abstract void postPageExecution(Location location)
			throws ExecutorException;

	/**
	 * Add Required Logic before starting the execution for the Single Record
	 * page.<br>
	 * <br>
	 * Only logic specific to Single Record Page should be added here.
	 */
	public abstract void preSingleRecordPageAreaExecution(Location location)
			throws ExecutorException;

	/**
	 * Add Required Logic after the execution for the Single Record page is
	 * completed.<br>
	 * <br>
	 * Only logic specific to Single Record Page should be added here.
	 */
	public abstract void postSingleRecordPageAreaExecution(Location location)
			throws ExecutorException;

	/**
	 * Add Required Logic before starting the execution for the Multi Record
	 * page.<br>
	 * <br>
	 * Only logic specific to Multi Record Page should be added here.
	 */
	public abstract void preMultiRecordPageAreaExecution(Location location)
			throws ExecutorException;

	/**
	 * Add Required Logic after the execution for the Multi Record page is
	 * completed.<br>
	 * <br>
	 * Only logic specific to Multi Record Page should be added here.
	 */
	public abstract void postMultiRecordPageAreaExecution(Location location)
			throws ExecutorException;

	/**
	 * Add Required Logic before starting the execution for the field.
	 */
	public abstract void preFieldExecution(Location location, TestObject t)
			throws ExecutorException;

	/**
	 * Add Required Logic after the execution for the field is completed.
	 */
	public abstract void postFieldExecution(Location location, TestObject t)
			throws ExecutorException;

	/**
	 * Build the Iteration Status here. Following things are mandatory to be
	 * setup here:-<br>
	 * this.iStatus.setMessage(value);<br>
	 * this.iStatus.setResult("S/F");<br>
	 * this.runtimeValues.add(r);<br>
	 */
	public abstract void processIterationStatus() throws ExecutorException;

	/**
	 * This method will check that if the exception occurred is because of any
	 * valid error message. And get that if there.
	 */
	public abstract String checkForErrorMessage();

}
