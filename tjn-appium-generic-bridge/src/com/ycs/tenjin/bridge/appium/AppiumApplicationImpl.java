/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AppiumApplicationImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Oct 27, 2017         Sameer Gupta          	Newly Added For  
 * 26-Nov-2018			Shivam	Sharma			TENJINCG-904
 * 25-Jan-2019			Sahana					pCloudy
 */

package com.ycs.tenjin.bridge.appium;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ssts.pcloudy.Connector;
import com.ssts.pcloudy.appium.PCloudyAppiumSession;
import com.ssts.pcloudy.dto.appium.booking.BookingDtoDevice;
import com.ssts.pcloudy.dto.file.PDriveFileDTO;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.bridge.appium.generic.DriverContext;
import com.ycs.tenjin.bridge.appium.generic.Launcher;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.DriverException;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationAbstract;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.client.RegisteredClient;
import com.ycs.tenjin.db.DeviceHelper;
import com.ycs.tenjin.device.RegisteredDevice;
import com.ycs.tenjin.util.CryptoUtilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public abstract class AppiumApplicationImpl extends GuiApplicationAbstract {

	private static final Logger logger = LoggerFactory.getLogger(AppiumApplicationImpl.class);

	// /////////////////////////////////////////////////
	// Variables
	// /////////////////////////////////////////////////
	protected DriverContext driverContext;
	protected int OBJECT_IDENTIFICATION_TIMEOUT = 0;
	protected String compositeXpathAndroid = "//android.widget.EditText|.//android.widget.RadioGroup|.//android.widget.Spinner|.//android.widget.DatePicker|.//android.widget.CheckBox";
	protected String compositeXpathIos = ".//XCUIElementTypePickerWheel|.//XCUIElementTypeTextField|.//XCUIElementTypeSwitch";
	protected String compositeXpath = "";

	// /////////////////////////////////////////////////
	// Constructor
	// /////////////////////////////////////////////////
	/*********************************
	 * TENJINCG-731 - By Sameer - Mobility -
	 */
	public AppiumApplicationImpl(Aut aut, String clientIp, int port, String browser, String deviceId) {

		super(aut, clientIp, port, browser, deviceId);

		if (this.getApplicationType().equals(3)) {
			this.compositeXpath = this.compositeXpathAndroid;
		} else if (this.getApplicationType().equals(4)) {
			this.compositeXpath = this.compositeXpathIos;
		}

		OBJECT_IDENTIFICATION_TIMEOUT = 15;
	}

	// /////////////////////////////////////////////////
	// Getters
	// /////////////////////////////////////////////////

	public String getCompositeXpath() {
		return compositeXpath;
	}

	public DriverContext getDriverContext() {
		return driverContext;
	}

	// /////////////////////////////////////////////////
	// Overriding Methods
	// /////////////////////////////////////////////////
	// added by shivam sharma for TENJINCG-904 starts
	String authToken = "";
	PCloudyAppiumSession pCloudySession = null;
	Connector con = null;
	// added by shivam to kill the session
	BookingDtoDevice aDevice;
	boolean deviceFarmFlag;

	// added by shivam sharma for TENJINCG-904 ends
	/*********************************
	 * TENJINCG-731 - By Sameer - Mobility -
	 */
	public void launch1(String url) throws AutException, DriverException {

	}

	@Override
	public void destroy() {
		try {
			// added by shivam sharma for TENJINCG-904 starts
			if (deviceFarmFlag) {
				logger.info("clearning pCloudy session");
				pCloudySession.releaseSessionNow();
			} else {
				this.driverContext.getDriver().quit();
			}
			// added by shivam sharma for TENJINCG-904 ends
		} catch (Exception ignore) {
			logger.error("Error ", ignore);
		}
	}

	// added by shivam sharma for TENJINCG-904 starts
	public void launch(String url) throws AutException {
		/*Added by Sahana for pCloudy: Starts*/
		String HelperClassName="com.ycs.tenjin.db.ClientHelper";
		Object helperobj=null;
		RegisteredClient client=null;
		
		try {
			Class<?> cls=Class.forName(HelperClassName);
			helperobj=cls.newInstance();
			Method method = helperobj.getClass().getMethod("hydrateClientByHostName", String.class);
			Object objClient = method.invoke(helperobj,this.clientIp);
			client=(RegisteredClient) objClient;
		} catch (Exception e) {
			logger.error("Error while hydrating registered client from class "+HelperClassName);
		} 
		try {
			deviceFarmFlag=client.getDeviceFarmCheck().equalsIgnoreCase("Y");
			if (deviceFarmFlag) {
				String userName=client.getDeviceFarmUsrName();
				/*String password=new Crypto().decrypt(client.getDeviceFarmPwd().split("!#!")[1],
					decodeHex(client.getDeviceFarmPwd().split("!#!")[0].toCharArray()));*/
				/*Modified by paneendra for VAPT fix starts*/
				/*String key=new Crypto().decrypt(client.getDeviceFarmKey().split("!#!")[1],
						decodeHex(client.getDeviceFarmKey().split("!#!")[0].toCharArray()));*/
				String key=new CryptoUtilities().decrypt(client.getDeviceFarmKey());
				/*Modified by paneendra for VAPT fix ends*/
				launchPcloudy(userName,key);
				/*	Added by Sahana for pCloudy: Ends*/
			} else {
				launchLocalDevice();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	private void launchLocalDevice() throws AutException {
		// added by shivam sharma for TENJINCG-904 ends
		try {
			logger.info("Launching Application");

			RegisteredDevice device = new DeviceHelper().hydrateDevice(Integer.parseInt(this.deviceId));

			DesiredCapabilities capabilities = new DesiredCapabilities();

			if (this.getApplicationType().equals(3)) {

				capabilities.setCapability("platformName", "Android");
				capabilities.setCapability("appPackage", this.getApplicationPackage());
				capabilities.setCapability("appActivity", this.getApplicationActivity());
				// added by sahana, for choosing a device from multiple
				// devices connected:Starts
				capabilities.setCapability("udid", device.getDeviceId());
				// added by sahana, for choosing a device from multiple
				// devices connected:Ends
				capabilities.setCapability("deviceName", device.getDeviceName());
				// capabilities.setCapability(CapabilityType.VERSION,
				// "4.4.2");
				// added by sahana, for handling alerts and granting
				// permissions:Starts
				capabilities.setCapability("autoAcceptAlerts", true);
				capabilities.setCapability("autoGrantPermissions", true);
				// added by sahana, for handling alerts and granting
				// permissions:Ends
				capabilities.setCapability("disableAndroidWatchers", true);
			} else if (this.getApplicationType().equals(4)) {

				capabilities.setCapability("platformName", "iOS");
				capabilities.setCapability("bundleId", this.getApplicationPackage());
				capabilities.setCapability("automationName", "XCUITest");
				capabilities.setCapability("deviceName", device.getDeviceName());
				capabilities.setCapability("udid", device.getDeviceId());
				capabilities.setCapability("useNewWDA", true);

			} else {
				throw new AutException("Wrong type of application, Please check your adapter.");
			}

			String appiumClient = "http://" + this.clientIp + ":" + this.port + "/wd/hub";

			AppiumDriver<MobileElement> driver = Launcher.getDriver(appiumClient, capabilities,
					this.getApplicationType());

			this.driverContext = new DriverContext();
			this.driverContext.setDriver(driver);

		} catch (Exception e) {
			logger.error("Could not launch application", e);
			throw new AutException("Could not launch AUT", e);
		}
	}

	// added by shivam sharma for TENJINCG-904 starts
	private void launchPcloudy(String userName, String key) throws AutException {
		// added by shivam sharma for pCloudy -starts
		String mediaRepoPath = null;
		File fileToBeUploaded = null;
		try {
			mediaRepoPath = TenjinConfiguration.getProperty("APP_FILE_PATH");
		} catch (Exception e1) {
			throw new AutException("Could not launch AUT", e1);
		}
		String finalPath = mediaRepoPath + appId + File.separator + this.appFileName;
		fileToBeUploaded = new File(finalPath);
		if (!fileToBeUploaded.exists())
			throw new AutException("Could not find .apk/.ipa");
		// added by shivam sharma for pCloudy -ends
		logger.info("Launching Application");
		String taskHandlerType = null;
		if (Thread.currentThread().getStackTrace()[3].getClassName().toString().toLowerCase().contains("learning")) {
			taskHandlerType = "Learning";
		} else if (Thread.currentThread().getStackTrace()[3].getClassName().toString().toLowerCase()
				.contains("execution")) {
			taskHandlerType = "Execution";
		} else {
			taskHandlerType = "Extraction";
		}
		try {
			int deviceBookDurationTime = 15;
			// Build Connection
			con = new Connector("https://device.pcloudy.com/api/");
			/*Changed by Sahana for pCloudy */
			authToken = con.authenticateUser(userName,key);

			/*commented by Sahana, to book devices by Id:Starts*/
			// Get Selected device
			/*ArrayList<MobileDevice> availableDevices = new ArrayList<MobileDevice>();
			availableDevices
					.addAll(con.chooseDevices(authToken, "android", new Version("7.0.*"), new Version("9.*.*"), 400));
			availableDevices.addAll(con.chooseDevices(authToken, "ios", new Version("9.*"), new Version("11.*"), 400));

			boolean deviceFound = false;
			for (MobileDevice mobileDevice : availableDevices) {
				if (mobileDevice.id.toString().equals(deviceId)) {
					selectedDevices.add(mobileDevice);
					deviceFound = true;
					break;
				}
			}*/

			/*if (!deviceFound) {
				throw new AutException("Device no longer available on Cloud.");
			}*/
			/*commented by Sahana, to book devices by Id:Ends*/
			ArrayList<Integer> selectedDevices = new ArrayList<Integer>();
			selectedDevices.add(Integer.parseInt(deviceId));
			int appType=this.getApplicationType();
			String platform="";
			if(appType==3)
				platform="Android";
			else if(appType==4)
				platform="ios";

			// Define session name
			String runId = Thread.currentThread().getName().replace("Tenjin Run", "").trim();
			String sessionName = taskHandlerType +"(RunId:"+ runId +")";

			// Book the selected devices in pCloudy
			/*Added by Sahana, to book devices by Id:Starts*/
			BookingDtoDevice[] bookedDevices =	con.AppiumApis().bookDevicesForAppium(authToken, selectedDevices, platform, deviceBookDurationTime, sessionName,false);
			/*	BookingDtoDevice[] bookedDevices = con.AppiumApis().bookDevicesForAppium(authToken, selectedDevices,
					deviceBookDurationTime, sessionName);*/
			/*Added by Sahana, to book devices by Id:Ends*/
			logger.info("Devices booked successfully");

			// Upload APK in pCloudy Cloud Drive
			// commented by shivam sharma for pCloudy -starts
			/*
			 * String mediaRepoPath =
			 * TenjinConfiguration.getProperty("APP_FILE_PATH"); String
			 * finalPath = mediaRepoPath + appId + File.separator +
			 * this.appFileName; File fileToBeUploaded = new File(finalPath);
			 */
			// commented by shivam sharma for pCloudy -ends
			PDriveFileDTO alreadyUploadedApp = con.getAvailableAppIfUploaded(authToken, fileToBeUploaded.getName());
			if (alreadyUploadedApp == null) {
				System.out.println("Uploading App: " + fileToBeUploaded.getAbsolutePath());
				PDriveFileDTO uploadedApp = con.uploadApp(authToken, fileToBeUploaded, false);
				System.out.println("App uploaded");
				alreadyUploadedApp = new PDriveFileDTO();
				alreadyUploadedApp.file = uploadedApp.file;
			} else {
				System.out.println(" App already present. Not uploading... ");
			}

			// Initiate Appium Connection
			con.AppiumApis().initAppiumHubForApp(authToken, alreadyUploadedApp);
			URL endpoint = con.AppiumApis().getAppiumEndpoint(authToken);
			System.out.println("Appium Endpoint: " + endpoint);
			this.aDevice = bookedDevices[0];
			pCloudySession = new PCloudyAppiumSession(con, authToken, aDevice);

			// Set Capabilities
			AppiumDriver<MobileElement> driver = null;
			DesiredCapabilities capabilities = new DesiredCapabilities();

			capabilities.setCapability("newCommandTimeout", 600);
			capabilities.setCapability("launchTimeout", 90000);
			capabilities.setCapability("deviceName", aDevice.capabilities.deviceName);
			/*capabilities.setCapability("browserName", aDevice.capabilities.deviceName);*/

			if(this.getApplicationType().equals(3))
			{
				// for hiding keyboard - starts
				capabilities.setCapability("unicodeKeyboard", true);
				capabilities.setCapability("resetKeyboard", true);
				// for hiding keyboard - ends
				capabilities.setCapability("platformName", "Android");
				capabilities.setCapability("appPackage", this.getApplicationPackage());
				capabilities.setCapability("appActivity", this.getApplicationActivity());
				driver = new AndroidDriver<MobileElement>(endpoint, capabilities);
			}
			else if(this.getApplicationType().equals(4))
			{
				capabilities.setCapability("platformName", "iOS");
				capabilities.setCapability("automationName", "XCUITest");
				capabilities.setCapability("bundleId", this.getApplicationPackage());
				driver = new IOSDriver<MobileElement>(endpoint, capabilities);
			}		
			// Set Driver Context
			this.driverContext = new DriverContext();
			this.driverContext.setDriver(driver);

		} catch (Exception e) {
			logger.error("Error ", e);
			logger.error("Could not launch application", e);
			throw new AutException("Could not launch AUT", e);
		}

	}

	// added by shivam sharma for TENJINCG-904 ends
}
