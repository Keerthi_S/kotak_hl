package com.ycs.tenjin.bridge.appium.generic;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.appium.mobile.AndroidLearningMethodsAbstract;

public class MobileGenericMethodsImpl {

	private static final Logger logger = LoggerFactory
			.getLogger(AndroidLearningMethodsAbstract.class);
	protected DriverContext driverContext;

	public MobileGenericMethodsImpl(DriverContext driverContext) {
		this.driverContext = driverContext;
	}

	public File captureScreenshotAsFile() {
		try {
			/*
			 * added by Shivam Sharma for the requirement of resizing screenshot
			 * - starts
			 * 
			 * return ((TakesScreenshot)
			 * this.driverContext.getDriver()).getScreenshotAs(OutputType.FILE);
			 */
			float actualHieght = this.driverContext.getDriver().manage().window().getSize().getHeight();
			float actualWidth = this.driverContext.getDriver().manage().window().getSize().getWidth();
			float initialHeight = 800;
			if(actualHieght > initialHeight){
				actualWidth = (initialHeight/actualHieght)*actualWidth;
				actualHieght = initialHeight;
			}
			
			File inputFile = ((TakesScreenshot) this.driverContext.getDriver())
					.getScreenshotAs(OutputType.FILE);

			BufferedImage inputImage = ImageIO.read(inputFile);
			BufferedImage bufferedImage = new BufferedImage(Math.round(actualWidth) ,Math.round(actualHieght), 
					inputImage.getType());
			Graphics2D g2d = bufferedImage.createGraphics();
			g2d.drawImage(inputImage, 0, 0, Math.round(actualWidth) ,Math.round(actualHieght), null);
			g2d.dispose();
			ImageIO.write(bufferedImage, "png", inputFile);
			return inputFile;

			/*
			 * added by Shivam Sharma for the requirement of resizing screenshot
			 * - ends
			 */

		} catch (Exception e) {
			logger.error("Could not capture screenshot of application", e);
			return null;
		}
	}

	public void explicitWait(long seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (Exception ignore) {
		}
	}
}
