package com.ycs.tenjin.bridge.appium.generic;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.DriverException;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public class Launcher {
	public static AppiumDriver<MobileElement> getDriver(String appiumClient,
			DesiredCapabilities capabilities, Integer applicationType)
					throws AutException, DriverException {

		capabilities.setCapability("newCommandTimeout", 60 * 5);
		/*commented by Sahana, these capabilities refers to Android only:Starts*/
		/*capabilities.setCapability("unicodeKeyboard", true);
		capabilities.setCapability("resetKeyboard", true);*/
		/*commented by Sahana, these capabilities refers to Android only:ends*/

		try {
			AppiumDriver<MobileElement> driver = null;
			if (applicationType.equals(3)) {
				//added by Sahana:starts
				capabilities.setCapability("unicodeKeyboard", true);
				capabilities.setCapability("resetKeyboard", true);
				//added by Sahana:ends
				driver = new AndroidDriver<MobileElement>(
						new URL(appiumClient), capabilities);
			} else if (applicationType.equals(4)) {
				driver = new IOSDriver<MobileElement>(new URL(appiumClient),
						capabilities);
			}
			return driver;
		} catch (MalformedURLException e) {
			throw new DriverException(
					"Could not launch application on Device or Emulator.");
		}
	}

}
