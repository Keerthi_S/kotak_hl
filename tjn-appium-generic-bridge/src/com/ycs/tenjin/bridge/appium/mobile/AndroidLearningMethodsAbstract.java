package com.ycs.tenjin.bridge.appium.mobile;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.appium.generic.DriverContext;
import com.ycs.tenjin.bridge.enums.FieldType;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

public abstract class AndroidLearningMethodsAbstract implements LearningMethods {

	public static final Logger logger = LoggerFactory
			.getLogger(AndroidLearningMethodsAbstract.class);
	public DriverContext driverContext;

	public AndroidLearningMethodsAbstract(DriverContext driverContext) {
		logger.info("AndroidLearningMethodsAbstract");
		this.driverContext = driverContext;
	}

	// Get Type Of Element
	// //////////////////////////////////////////////////////////////////////////////
	@Override
	public FieldType getTypeOfElement(MobileElement element) {
		FieldType type = null;
		String className = element.getAttribute("className").toLowerCase();
		if (className.contains("spinner"))
			type = FieldType.LIST;
		else if (className.contains("radio"))
			type = FieldType.RADIO;
		else if (className.contains("edittext"))
			type = FieldType.TEXTBOX;
		else if (className.contains("button"))
			type = FieldType.BUTTON;
		else if (className.contains("check"))
			type = FieldType.CHECKBOX;

		FieldType extendType = this.getTypeOfElementExtend(element);
		if (extendType != null) {
			type = extendType;
		}
		return type;
	}

	public abstract FieldType getTypeOfElementExtend(MobileElement element);

	// Get Labels
	// //////////////////////////////////////////////////////////////////////////////
	@Override
	public String getLabelOfElement(FieldType type, MobileElement element) {
		String label = null;

		String extendLabel = this.getLabelOfElementExtend(type, element);
		if (extendLabel != null && !extendLabel.equals("")) {
			label = extendLabel;
		}
		return label;
	}

	public abstract String getLabelOfElementExtend(FieldType type,
			MobileElement element);

	// Dropdown Values
	// //////////////////////////////////////////////////////////////////////////////
	@Override
	public String getDropdownValuesOfElement(FieldType type,
			MobileElement element) {

		String dropValues = "";
		if (type.toString().equalsIgnoreCase(FieldType.LIST.toString())) {
			dropValues = getDropdownValuesOfList(element);
		} else if (type.toString().equalsIgnoreCase(FieldType.RADIO.toString())) {
			dropValues = getDropdownValuesOfRadioGroup(element);
		} else if (type.toString().equalsIgnoreCase(
				FieldType.CHECKBOX.toString())) {
			dropValues = "Check;Uncheck";
		}
		String extendDropValues = this.getDropdownValuesOfElementExtend(type,
				element);
		if (extendDropValues != null && !extendDropValues.equals("")) {
			dropValues = extendDropValues;
		}
		return dropValues;
	}

	public abstract String getDropdownValuesOfElementExtend(FieldType type,
			MobileElement element);

	public String getDropdownValuesOfRadioGroup(MobileElement element) {
		String dropValues = "";
		List<MobileElement> dropElements = element.findElements(MobileBy
				.xpath("//android.widget.RadioButton"));
		if (dropElements != null) {
			for (int k = 0; k < dropElements.size(); k++) {
				dropValues += dropElements.get(k).getText() + ";";
			}
		}
		return dropValues;
	}

	public String getDropdownValuesOfList(MobileElement element) {
		String dropValues = "";
		element.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			logger.error("Error ", e);
		}
		List<MobileElement> dropElements = this.driverContext.getDriver()
				.findElements(MobileBy.xpath(".//android.widget.CheckedTextView|.//android.widget.TextView"));
		if (dropElements != null) {
			for (int k = 0; k < dropElements.size(); k++) {
				dropValues += dropElements.get(k).getText() + ";";
			}
		}

		this.driverContext.getDriver().navigate().back();
		return dropValues;
	}

}
