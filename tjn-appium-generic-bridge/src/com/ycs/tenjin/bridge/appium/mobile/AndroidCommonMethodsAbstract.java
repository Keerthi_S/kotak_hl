package com.ycs.tenjin.bridge.appium.mobile;

import com.ycs.tenjin.bridge.appium.generic.DriverContext;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.pojo.aut.Location;

public abstract class AndroidCommonMethodsAbstract implements CommonMethods {

	protected DriverContext driverContext;

	public AndroidCommonMethodsAbstract(DriverContext driverContext) {
		this.driverContext = driverContext;
	}

	// Navigate To Page
	// /////////////////////////////////////////////////////////////////////////////// 
	public void navigateToPage(Location location) throws BridgeException {
		this.navigateToPageExtend(location);
	}

	public abstract void navigateToPageExtend(Location location)
			throws BridgeException;
}
