package com.ycs.tenjin.bridge.appium.mobile;

import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;

import io.appium.java_client.MobileElement;

public interface ExecutionMethods {

	public MobileElement locateElement(TestObject t) throws AutException;

	public void performInput(MobileElement element, TestObject t, String data)
			throws AutException;

	public String performOutput(MobileElement element, TestObject t)
			throws AutException;

}
