package com.ycs.tenjin.bridge.appium.mobile;

import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.pojo.aut.Location;

public interface CommonMethods {

	public void navigateToPage(Location location) throws BridgeException;

}
