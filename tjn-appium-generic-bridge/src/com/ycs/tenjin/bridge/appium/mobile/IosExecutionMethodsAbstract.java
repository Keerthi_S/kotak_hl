package com.ycs.tenjin.bridge.appium.mobile;

import org.openqa.selenium.StaleElementReferenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.appium.generic.DriverContext;
import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;

import io.appium.java_client.MobileElement;

public abstract class IosExecutionMethodsAbstract implements ExecutionMethods {

	public static final Logger logger = LoggerFactory
			.getLogger(IosExecutionMethodsAbstract.class);
	public DriverContext driverContext;

	public IosExecutionMethodsAbstract(DriverContext driverContext) {
		this.driverContext = driverContext;
	}

	// Locate Element
	// //////////////////////////////////////////////////////////////////////////////
	@Override
	public MobileElement locateElement(TestObject t) throws AutException {

		MobileElement element = null;

		MobileElement extendElement = this.locateElementExtend(t);
		if (extendElement != null) {
			element = extendElement;
		}

		return element;
	}

	public abstract MobileElement locateElementExtend(TestObject t)
			throws AutException;

	// Inputs
	// //////////////////////////////////////////////////////////////////////////////
	public void performInput(MobileElement element, TestObject t, String data)
			throws AutException {

		String elemType = t.getObjectClass();

		try {
			if (FieldType.TEXTAREA.toString().equalsIgnoreCase(elemType)
					|| FieldType.TEXTBOX.toString().equalsIgnoreCase(elemType)
					|| FieldType.PASSWORD.toString().equalsIgnoreCase(elemType)) {
				this.textBoxInput(element, data);
			} else if (FieldType.CHECKBOX.toString().equalsIgnoreCase(elemType)) {
				this.checkBoxInput(element, data);
			} else if (FieldType.LIST.toString().equalsIgnoreCase(elemType)) {
				this.listInput(element, data);
			}
			this.performInputExtend(element, t, data);
		} catch (ExecutorException e) {

			if (e.getCause() != null
					&& e.getCause() instanceof StaleElementReferenceException) {
				logger.error("STALE ELEMENT >> PAGE DOM HAS REFRESHED");
				throw new ExecutorException(
						"Could not perform I/O as page DOM has refreshed",
						e.getCause());
			}

			logger.error("Could not input {} to field {} on page {}", data,
					t.getLabel(), t.getLocation());
			throw new AutException("Could not input " + data + " to field "
					+ t.getLabel() + " on page " + t.getLocation());
		} catch (Exception e) {
			logger.error("Could not input {} to field {} on page {}", data,
					t.getLabel(), t.getLocation());
			throw new AutException("Could not input " + data + " to field "
					+ t.getLabel() + " on page " + t.getLocation());
		}
	}

	public abstract void performInputExtend(MobileElement element,
			TestObject t, String data) throws AutException;

	public void textBoxInput(MobileElement element, String data)
			throws ExecutorException, StaleElementReferenceException {
		try {
			/*element.clear();*/
			element.setValue(data);
		} catch (Exception e) {
			logger.error(
					"An exception occurred while setting value to text field",
					e);
			throw new ExecutorException("Could not set value " + data
					+ " in text field", e);
		}
	}

	public void checkBoxInput(MobileElement element, String data) {
		String checkboxData = "0";
		if(data.equalsIgnoreCase("check")){
			checkboxData = "1";
		}
		try {
			
			if (!element.getAttribute("value").equalsIgnoreCase(checkboxData))
				element.click();
		} catch (Exception e) {
			logger.debug("could not perform input in checkbox");
		}
	}

	public void listInput(MobileElement element, String data) {

		try {
			element.setValue(data);
		} catch (Exception e) {
			logger.error(
					"An exception occurred while setting value to text field",
					e);
			throw new ExecutorException("Could not set value " + data
					+ " in text field", e);
		}

	}

	// Outputs
	// /////////////////////////////////////////////////////////
	public String performOutput(MobileElement element, TestObject t)
			throws AutException {

		String elemType = t.getObjectClass();

		String retVal = "";

		try {
			if (FieldType.TEXTBOX.toString().equalsIgnoreCase(elemType)
					|| FieldType.PASSWORD.toString().equalsIgnoreCase(elemType)) {
				retVal = this.textBoxOutput(element);
			} else if (FieldType.CHECKBOX.toString().equalsIgnoreCase(elemType)) {
				retVal = this.checkBoxOutput(element);
			} else if (FieldType.LIST.toString().equalsIgnoreCase(elemType)) {
				retVal = this.listOutput(element);
			}

			String extendRetVal = this.performOutputExtend(element, t);
			if (extendRetVal != null && !extendRetVal.equals("")) {
				retVal = extendRetVal;
			}
		} catch (ExecutorException e) {
			logger.error("Could not get value of field {} on page {}",
					t.getLabel(), t.getLocation());
			throw new AutException("Could not get value of field "
					+ t.getLabel() + " on page " + t.getLocation());
		} catch (Exception e) {
			logger.error("Could not get value of field {} on page {}",
					t.getLabel(), t.getLocation());
			throw new AutException("Could not get value of field "
					+ t.getLabel() + " on page " + t.getLocation());
		}

		return retVal;
	}

	public abstract String performOutputExtend(MobileElement element,
			TestObject t) throws AutException;

	public String textBoxOutput(MobileElement element) {
		return element.getText();
	}

	public String checkBoxOutput(MobileElement element) {
		return null;
	}

	public String listOutput(MobileElement element) {
		return null;
	}

}
