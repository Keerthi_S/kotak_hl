package com.ycs.tenjin.bridge.appium.mobile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.appium.generic.DriverContext;
import com.ycs.tenjin.bridge.enums.FieldType;

import io.appium.java_client.MobileElement;

public abstract class IosLearningMethodsAbstract implements LearningMethods {

	public static final Logger logger = LoggerFactory
			.getLogger(IosLearningMethodsAbstract.class);
	public DriverContext driverContext;

	public IosLearningMethodsAbstract(DriverContext driverContext) {
		logger.info("IosLearningMethodsImpl");
		this.driverContext = driverContext;
	}

	// Get Type Of Element
	// //////////////////////////////////////////////////////////////////////////////
	@Override
	public FieldType getTypeOfElement(MobileElement element) {
		FieldType type = null;
		String tagname = element.getTagName().toLowerCase();
		if (tagname.contains("wheel"))
			type = FieldType.LIST;
		else if (tagname.contains("textfield"))
			type = FieldType.TEXTBOX;
		else if (tagname.contains("switch"))
			type = FieldType.CHECKBOX;

		FieldType extendType = this.getTypeOfElementExtend(element);
		if (extendType != null) {
			type = extendType;
		}
		return type;
	}

	public abstract FieldType getTypeOfElementExtend(MobileElement element);

	// Get Labels
	// //////////////////////////////////////////////////////////////////////////////
	@Override
	public String getLabelOfElement(FieldType type, MobileElement element) {
		String label = null;
		String extendLabel = this.getLabelOfElementExtend(type, element);
		if (extendLabel != null && !extendLabel.equals("")) {
			label = extendLabel;
		}
		return label;
	}

	public abstract String getLabelOfElementExtend(FieldType type,
			MobileElement element);

	// Dropdown Values
	// //////////////////////////////////////////////////////////////////////////////
	@Override
	public String getDropdownValuesOfElement(FieldType type,
			MobileElement element) {

		String dropValues = "";
		if (type.toString().equalsIgnoreCase(FieldType.LIST.toString())) {
			dropValues = this.getDropdownValuesOfList(element);
		} else if (type.toString().equalsIgnoreCase(
				FieldType.CHECKBOX.toString())) {
			dropValues = "check;uncheck";
		}

		String extendDropValues = this.getDropdownValuesOfElementExtend(type,
				element);
		if (extendDropValues != null && !extendDropValues.equals("")) {
			dropValues = extendDropValues;
		}
		return dropValues;
	}

	public abstract String getDropdownValuesOfElementExtend(FieldType type,
			MobileElement element);

	public String getDropdownValuesOfList(MobileElement element) {
		String dropValues = "";
		return dropValues;

	}
}
