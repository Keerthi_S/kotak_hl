package com.ycs.tenjin.bridge.appium.mobile;

import com.ycs.tenjin.bridge.enums.FieldType;

import io.appium.java_client.MobileElement;

public interface LearningMethods {

	public FieldType getTypeOfElement(MobileElement element);

	public String getLabelOfElement(FieldType type, MobileElement element);

	public String getDropdownValuesOfElement(FieldType type,
			MobileElement element);
}
