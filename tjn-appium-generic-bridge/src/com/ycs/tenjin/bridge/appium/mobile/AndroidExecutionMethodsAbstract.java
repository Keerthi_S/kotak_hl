package com.ycs.tenjin.bridge.appium.mobile;

import java.util.List;

import org.openqa.selenium.StaleElementReferenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.appium.generic.DriverContext;
import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

public abstract class AndroidExecutionMethodsAbstract implements
ExecutionMethods {

	public static final Logger logger = LoggerFactory
			.getLogger(AndroidExecutionMethodsAbstract.class);
	public DriverContext driverContext;

	public AndroidExecutionMethodsAbstract(DriverContext driverContext) {
		this.driverContext = driverContext;
	}

	// Locate Element
	// //////////////////////////////////////////////////////////////////////////////
	@Override
	public MobileElement locateElement(TestObject t) throws AutException {

		MobileElement element = null;

		if (t.getObjectClass().equalsIgnoreCase(FieldType.BUTTON.toString())) {
			element = locateButton(t);
		}

		MobileElement extendElement = this.locateElementExtend(t);
		if (extendElement != null) {
			element = extendElement;
		}

		return element;
	}

	private MobileElement locateButton(TestObject t) {
		MobileElement element = null;
		try {
			element = this.driverContext.getDriver().findElement(
					MobileBy.xpath(".//android.widget.Button[@text='"
							+ t.getData()
							+ "']|.//android.widget.TextView[@text='"
							+ t.getData() + "']"));
		} catch (Exception e) {
			logger.debug("Could not find the element " + t.getData() + ".");
		}
		return element;
	}

	public abstract MobileElement locateElementExtend(TestObject t)
			throws AutException;

	// Inputs
	// //////////////////////////////////////////////////////////////////////////////
	public void performInput(MobileElement element, TestObject t, String data)
			throws AutException {

		String elemType = t.getObjectClass();

		try {
			if (FieldType.TEXTAREA.toString().equalsIgnoreCase(elemType)
					|| FieldType.TEXTBOX.toString().equalsIgnoreCase(elemType)
					|| FieldType.PASSWORD.toString().equalsIgnoreCase(elemType)) {
				this.textBoxInput(element, data);
			} else if (FieldType.CHECKBOX.toString().equalsIgnoreCase(elemType)) {
				this.checkBoxInput(element, data);
			} else if (FieldType.RADIO.toString().equalsIgnoreCase(elemType)) {
				this.radioButtonInput(element, data);
			} else if (FieldType.LIST.toString().equalsIgnoreCase(elemType)) {
				this.listInput(element, data);
			} else if (FieldType.BUTTON.toString().equalsIgnoreCase(elemType)) {
				this.buttonInput(element);
			}

			this.performInputExtend(element, t, data);
		} catch (ExecutorException e) {

			if (e.getCause() != null
					&& e.getCause() instanceof StaleElementReferenceException) {
				logger.error("STALE ELEMENT >> PAGE DOM HAS REFRESHED");
				throw new ExecutorException(
						"Could not perform I/O as page DOM has refreshed",
						e.getCause());
			}

			logger.error("Could not input {} to field {} on page {}", data,
					t.getLabel(), t.getLocation());
			throw new AutException("Could not input " + data + " to field "
					+ t.getLabel() + " on page " + t.getLocation());
		} catch (Exception e) {
			logger.error("Could not input {} to field {} on page {}", data,
					t.getLabel(), t.getLocation());
			throw new AutException("Could not input " + data + " to field "
					+ t.getLabel() + " on page " + t.getLocation());
		}
	}

	public abstract void performInputExtend(MobileElement element,
			TestObject t, String data) throws AutException;

	public void textBoxInput(MobileElement element, String data)
			throws ExecutorException, StaleElementReferenceException {

		try {
			//commented by Sahana, as it takes 4 seconds to add input to field:starts
			//element.clear();
			//commented by Sahana, as it takes 4 seconds to add input to field:ends
			element.setValue(data);
		} catch (Exception e) {
			logger.error(
					"An exception occurred while setting value to text field",
					e);
			throw new ExecutorException("Could not set value " + data
					+ " in text field", e);
		}
	}

	public void listInput(MobileElement element, String data)
			throws ExecutorException {
		try {

			element.click();

			//			Thread.sleep(500);
			//
			//			this.driverContext
			//					.getDriver()
			//					.findElement(
			//							MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView("
			//									+ "new UiSelector().text(\""
			//									+ data
			//									+ "\"));")).click();


			Thread.sleep(5000);

			List<MobileElement> dropElements = this.driverContext
					.getDriver()
					.findElements(
							MobileBy.xpath(".//android.widget.CheckedTextView|.//android.widget.TextView"));
			if (dropElements != null) {
				for (int k = 0; k < dropElements.size(); k++) {
					MobileElement dropValue = dropElements.get(k);
					if (dropValue.getText().equalsIgnoreCase(data)) {
						dropValue.click();
						break;
					}
				}
			}
		} catch (Exception e) {
			logger.error(
					"An exception occurred while selecting value from list box",
					e);
			throw new ExecutorException("Could not select " + data
					+ " from list ");
		}
	}

	public void radioButtonInput(MobileElement element, String data)
			throws ExecutorException {
		try {

			List<MobileElement> dropElements = element.findElements(MobileBy
					.xpath("//android.widget.RadioButton"));

			for (MobileElement radioElement : dropElements) {
				if (radioElement.getText().equalsIgnoreCase(data))
					radioElement.click();
			}

		} catch (ExecutorException e) {
			throw new ExecutorException(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(
					"An exception occurred while setting value to checkbox/radio button",
					e);
			throw new ExecutorException("Could not input " + data
					+ " to field ", e);
		}
	}

	public void checkBoxInput(MobileElement element, String data)
			throws ExecutorException {
		try {
			if (data.equalsIgnoreCase("check")) {
				if (!element.isSelected()) {
					element.click();
				}
			} else if (data.equalsIgnoreCase("uncheck")) {
				if (element.isSelected()) {
					element.click();
				}
			} else {
				throw new ExecutorException("Could not input " + data
						+ " in checkbox");
			}
		} catch (Exception e) {
			logger.error(
					"An exception occurred while setting value to checkbox/radio button",
					e);
			throw new ExecutorException("Could not input " + data
					+ " in checkbox");
		}
	}

	public void buttonInput(MobileElement element)
			throws ExecutorException {

		try {
			element.click();
		} catch (Exception e) {
			logger.error(
					"An exception occurred while clicking button.",
					e);
			throw new ExecutorException("Could not click the button.", e);
		}

	}

	// Outputs
	// /////////////////////////////////////////////////////////
	public String performOutput(MobileElement element, TestObject t)
			throws AutException {

		String elemType = t.getObjectClass();

		String retVal = "";

		try {
			if (FieldType.TEXTAREA.toString().equalsIgnoreCase(elemType)
					|| FieldType.TEXTBOX.toString().equalsIgnoreCase(elemType)
					|| FieldType.PASSWORD.toString().equalsIgnoreCase(elemType)) {
				retVal = this.textBoxOutput(element);
			} else if (FieldType.CHECKBOX.toString().equalsIgnoreCase(elemType)) {
				retVal = this.checkBoxOutput(element);
			} else if (FieldType.RADIO.toString().equalsIgnoreCase(elemType)) {
				retVal = this.radioButtonOutput(element, t);
			} else if (FieldType.LIST.toString().equalsIgnoreCase(elemType)) {
				retVal = this.listOutput(element);
			}

			String extendRetVal = this.performOutputExtend(element, t);
			if (extendRetVal != null && !extendRetVal.equals("")) {
				retVal = extendRetVal;
			}
		} catch (ExecutorException e) {
			logger.error("Could not get value of field {} on page {}",
					t.getLabel(), t.getLocation());
			throw new AutException("Could not get value of field "
					+ t.getLabel() + " on page " + t.getLocation());
		} catch (Exception e) {
			logger.error("Could not get value of field {} on page {}",
					t.getLabel(), t.getLocation());
			throw new AutException("Could not get value of field "
					+ t.getLabel() + " on page " + t.getLocation());
		}

		return retVal;
	}

	public abstract String performOutputExtend(MobileElement element,
			TestObject t) throws AutException;

	public String textBoxOutput(MobileElement element) throws ExecutorException {
		try {
			return element.getText();
		} catch (Exception e) {
			logger.error("ERROR getting value from text box", e);
			throw new ExecutorException("Could not get value of text box");
		}

	}

	public String listOutput(MobileElement element) throws ExecutorException {

		try {
			return element.getText();
		} catch (Exception e) {
			throw new ExecutorException("Could not get value of list ", e);
		}

	}

	public String radioButtonOutput(MobileElement element, TestObject t)
			throws ExecutorException {
		String retVal = "";
		try {

			List<MobileElement> dropElements = element.findElements(MobileBy
					.xpath("//android.widget.RadioButton"));

			for (MobileElement radioElement : dropElements) {
				if (radioElement.isSelected())
					retVal = radioElement.getText();
			}

		} catch (Exception e) {
			logger.error("Could not get value of radio button {}",
					t.getLabel(), e);
			throw new ExecutorException("Could not get value of radio button "
					+ t.getLabel());
		}
		return retVal;

	}

	public String checkBoxOutput(MobileElement element)
			throws ExecutorException {
		try {
			if (element.isSelected()) {
				return "YES";
			} else {
				return "NO";
			}
		} catch (Exception e) {
			throw new ExecutorException("Could not get value of checkbox");
		}
	}

}
