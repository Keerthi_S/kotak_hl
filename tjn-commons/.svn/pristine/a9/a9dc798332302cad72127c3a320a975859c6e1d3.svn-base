/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ValidationDetails.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 27-10-2018			Ashiki			Newly added for TENJINCG-1204
* */

package com.ycs.tenjin;



public class ValidationDetails {
	private ValidationType validationType;
	private String baseFilePath;
	private String inputFilePath;
	private String fileType;
	private boolean isPasswordProtected;
	private String password;
	private String searchText;
	private int expectedOccurance = -1;
	private Object result;
	private String validationName;
	private int testStepId;
	private int recordId;
	private String validationData;
	private String actualValue;
	//Excel
	private String sheetName;
	private String cellRange;
	private String cellReference;
	
	//PDF
	private int searchPage = -1;
	private boolean isPDFFormat;
	private int pageNumber;
	private String pdfValidationType; //static/ dynamic
	
	public ValidationType getValidationType() {
		return validationType;
	}
	public void setValidationType(ValidationType validationType) {
		this.validationType = validationType;
	}
	public String getBaseFilePath() {
		return baseFilePath;
	}
	public void setBaseFilePath(String baseFilePath) {
		this.baseFilePath = baseFilePath;
	}
	public String getInputFilePath() {
		return inputFilePath;
	}
	public void setInputFilePath(String inputFilePath) {
		this.inputFilePath = inputFilePath;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public int getExpectedOccurance() {
		return expectedOccurance;
	}
	public void setExpectedOccurance(int expectedOccurance) {
		this.expectedOccurance = expectedOccurance;
	}
	public String getSheetName() {
		return sheetName;
	}
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	public String getCellRange() {
		return cellRange;
	}
	public void setCellRange(String cellRange) {
		this.cellRange = cellRange;
	}
	public String getCellReference() {
		return cellReference;
	}
	public void setCellReference(String cellReference) {
		this.cellReference = cellReference;
	}
	public int getSearchPage() {
		return searchPage;
	}
	public void setSearchPage(int searchPage) {
		this.searchPage = searchPage;
	}
	public String getPdfValidationType() {
		return pdfValidationType;
	}
	public void setPdfValidationType(String pdfValidationType) {
		this.pdfValidationType = pdfValidationType;
	}
	public boolean isPDFFormat() {
		return isPDFFormat;
	}
	public void setPDFFormat(boolean isPDFFormat) {
		this.isPDFFormat = isPDFFormat;
	}
	public boolean isPasswordProtected() {
		return isPasswordProtected;
	}
	public void setPasswordProtected(boolean isPasswordProtected) {
		this.isPasswordProtected = isPasswordProtected;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}
	public String getValidationName() {
		return validationName;
	}
	public void setValidationName(String validationName) {
		this.validationName = validationName;
	}
	public int getTestStepId() {
		return testStepId;
	}
	public void setTestStepId(int testStepId) {
		this.testStepId = testStepId;
	}
	public int getRecordId() {
		return recordId;
	}
	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}
	public String getValidationData() {
		return validationData;
	}
	public void setValidationData(String validationData) {
		this.validationData = validationData;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getActualValue() {
		return actualValue;
	}
	public void setActualValue(String actualValue) {
		this.actualValue = actualValue;
	}
}