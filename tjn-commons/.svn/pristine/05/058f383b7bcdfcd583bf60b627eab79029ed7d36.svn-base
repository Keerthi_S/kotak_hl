# 
# 
# Yethi Consulting Private Ltd. CONFIDENTIAL
# 
# 
# Name of this file:  tjn_messages_fr_FR.properties
# 
# 
# Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE
# 
# 
# Copyright � 2016-17 by Yethi Consulting Private Ltd.
# 
# 
# This source file is part of the TENJIN Software Product and System 
# and is copyrighted by Yethi Consulting Private Ltd.
# 
# All rights reserved.  No part of this work may be reproduced, copied, 
# duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
# system, transmitted in any form or by any means, electronic, 
# mechanical, photographic, graphic, optic recording or otherwise, translated 
# in any language or computer language, sold, rented, leased without the prior 
# written permission of Yethi Consulting Services Private Ltd.
# 
# Notice: All information and source code contained in this file is, and remains 
# the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
# The intellectual and technical concepts contained herein are proprietary to Yethi 
# Consulting Services Private Ltd., and its suppliers and may be covered under patents 
# and patents in process and are protected by trade secret or copyright laws. Dissemination 
# of this information or reproduction of this material is strictly forbidden unless prior 
# written permission is obtained from Yethi Consulting Services Private Ltd. 


# Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
# HAL 3rd Stage, Bangalore - 560 075,
# Karnataka-560075,India
# 
# 
# ******************************************
# CHANGE HISTORY
# ==============
# 
# DATE                 CHANGED BY              DESCRIPTION
# 19-Oct-2017			sriram					TENJINCG-403
# 
# 
######################################################
# INSTRUCTIONS
# ------------
# Replace all Single Quotes (') with Two Single Quotes ('')
######################################################


#GENERIC
generic.db.error=Une erreur interne a eu lieu. Veuillez contacter le support de Tenjin.
generic.error=Une erreur interne a eu lieu. Veuillez contacter le support de Tenjin.
field.mandatory={0} est obligatoire.
field.value.invalid=La valeur {0} sp�cifi�e pour {1} n''est pas valide.
record.not.found=L''enregistrement que vous essayez de voir n''a pas �t� trouv�. Cela aurait pu �tre supprim�. Veuillez contacter votre administrateur Tenjin.
illegal.field.update=La valeur de {0} ne peut pas �tre mise � jour.
aut.selection.required=S�lectionnez une application.
no.records.selected=Veuillez choisir au moins un enregistrement pour continuer.


#REGISTERED CLIENTS - SUCCESS
client.create.success=Client successfully created with key {0}
client.delete.success=Client {0} deleted successfully.
client.delete.multiple.success=The selected client(s) were deleted successfully.
client.update.success=Client {0} updated successfully.
client.list.success={0} clients found.
client.found=Client details fetched successfully.
#REGISTERED CLIENTS - FAILURES
client.already.exists=This client name is already used. Please choose a different name.
client.delete.error.in.use=This client cannot be deleted now as it is currently in use.
client.delete.error.multiple.in.use=The delete operation could not be completed now as some of the clients are currently in use.
client.list.not.found=No registered clients found.
client.not.found=The specified Client is not registered with Tenjin.

#APPLICATIONS - SUCCESS

#APPLICATIONS - FAILURES
aut.not.found=The specified Application does not exist.

#FUNCTIONS - SUCCESS
function.create.success=Function created successfully.
function.update.success=Function details updated successfully.
function.delete.success=Function {0} deleted successfully.
function.delete.multiple.success=The selected function(s) were deleted successfully.
#FUNCTIONS - FALURES
function.not.found=The specified Function does not exist.
function.already.exists=Function {0} already exists for Application {1}. Please use a different Function Code.
function.delete.metadata.conflict=Function {0} cannot be deleted as it contains Metadata.
function.delete.step.conflict=Function {0} cannot be deleted as it is already mapped to a Test Step.
function.delete.learning.conflit=Function {0} cannot be deleted as it is currently being learnt.
function.delete.metadata.conflict.multiple=Bulk Deletion Failed - Function {0} cannot be deleted as it contains Metadata.
function.delete.step.conflict.multiple=Bulk Deletion Failed - Function {0} cannot be deleted as it is already mapped to a Test Step.
function.delete.learning.conflit.multiple=Bulk Deletion Failed - Function {0} cannot be deleted as it is currently being learnt.

#USERS - CONFIRMATION
confirm.user.enable=�tes-vous s�r de vouloir activer cet utilisateur?
confirm.user.disable=�tes-vous s�r de vouloir d�sactiver cet utilisateur? Une fois d�sactiv�, cet utilisateur ne pourra pas vous connecter � Tenjin.
#USERS - SUCCESS
user.create.success=L''utilisateur {0} cr�� avec succ�s.
user.update.success=L''utilisateur {0} a �t� mis � jour avec succ�s
user.password.update.success=Le mot de passe a �t� chang� avec succ�s
user.enabled.success=L''utilisateur {0} est maintenant activ�.
user.disable.success=L''utilisateur {0} est maintenant d�sactiv�

#USERS - FAILURES
user.not.found=User was not found. It could have been removed. Please contact your Tenjin Administrator.
user.already.exists=This User ID is already used. Please use a different User ID and try again.
password.length.fail=Password must have alteast 8 characters.