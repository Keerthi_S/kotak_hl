/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  PreparedStatementBuilder.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
*14-09-2018			   	Sriram Sridharan			  	Newly Added for TENJINCG-735
*/

package com.ycs.tenjin.db;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;

public class PreparedStatementBuilder {
	public static PreparedStatement build(Connection connection, String query, Object...parameters) throws SQLException, DatabaseException {
		PreparedStatement pst = connection.prepareStatement(query);
		setParametersToPreparedStatement(pst, parameters);
		return pst;
	}
	
	
	
	private static void setParametersToPreparedStatement(PreparedStatement pst, Object...parameters) throws SQLException, DatabaseException{
		int parameterCount = pst.getParameterMetaData().getParameterCount();
		if(parameterCount > 0) {
			if(parameters == null || parameters.length < 1) {
				throw new DatabaseException("Missing statement parameter values");
			}else if(parameters != null && parameters.length < parameterCount) {
				throw new DatabaseException("Insufficient parameter values specified. Expected " + parameterCount + ", got " + parameters.length);
			}
			
			for(int i=0; i<parameterCount; i++) {
				setObject(pst, i+1, parameters[i]);
			}
		}
	}
	
	private static void setObject(PreparedStatement pst, int parameterIndex, Object parameterObj) throws SQLException, DatabaseException {
		 try {
			if (parameterObj instanceof Byte) {
			     pst.setInt(parameterIndex, ((Byte) parameterObj).intValue());
			 } else if (parameterObj instanceof String) {
			     pst.setString(parameterIndex, (String) parameterObj);
			 } else if (parameterObj instanceof BigDecimal) {
			     pst.setBigDecimal(parameterIndex, (BigDecimal) parameterObj);
			 } else if (parameterObj instanceof Short) {
			     pst.setShort(parameterIndex, ((Short) parameterObj).shortValue());
			 } else if (parameterObj instanceof Integer) {
			     pst.setInt(parameterIndex, ((Integer) parameterObj).intValue());
			 } else if (parameterObj instanceof Long) {
			     pst.setLong(parameterIndex, ((Long) parameterObj).longValue());
			 } else if (parameterObj instanceof Float) {
			     pst.setFloat(parameterIndex, ((Float) parameterObj).floatValue());
			 } else if (parameterObj instanceof Double) {
			     pst.setDouble(parameterIndex, ((Double) parameterObj).doubleValue());
			 } else if (parameterObj instanceof byte[]) {
			     pst.setBytes(parameterIndex, (byte[]) parameterObj);
			 } else if (parameterObj instanceof java.sql.Date) {
			     pst.setDate(parameterIndex, (java.sql.Date) parameterObj);
			 } else if (parameterObj instanceof Time) {
			     pst.setTime(parameterIndex, (Time) parameterObj);
			 } else if (parameterObj instanceof Timestamp) {
			     pst.setTimestamp(parameterIndex, (Timestamp) parameterObj);
			 } else if (parameterObj instanceof Boolean) {
			     pst.setBoolean(parameterIndex, ((Boolean) parameterObj).booleanValue());
			 } else if (parameterObj instanceof InputStream) {
			     pst.setBinaryStream(parameterIndex, (InputStream) parameterObj, -1);
			 } else if (parameterObj instanceof java.sql.Blob) {
			     pst.setBlob(parameterIndex, (java.sql.Blob) parameterObj);
			 } else if (parameterObj instanceof java.sql.Clob) {
			     pst.setClob(parameterIndex, (java.sql.Clob) parameterObj);
			 } else if (parameterObj instanceof java.util.Date) {
			     pst.setTimestamp(parameterIndex, new Timestamp(((java.util.Date) parameterObj).getTime()));
			 } else if (parameterObj instanceof BigInteger) {
			     pst.setString(parameterIndex, parameterObj.toString());
			 } else {
			     pst.setObject(parameterIndex, parameterObj);
			 }
		} catch (SQLException e) {
			throw e;
		} catch (Exception e) {
			throw new DatabaseException("Error setting parameters to statement", e);
		}
	}
}
