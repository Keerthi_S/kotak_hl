/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SchMapping.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/




package com.ycs.tenjin.scheduler;



public class SchMapping {
	private String schID;
	private int seqNo;
	private String project;
	private String app_id;
	private int testStepId;
	
	public int getTestStepId() {
		return testStepId;
	}
	public void setTestStepId(int testStepId) {
		this.testStepId = testStepId;
	}
	private int runId;
	public int getRunId() {
		return runId;
	}
	public void setRunId(int runId) {
		this.runId = runId;
	}
	public String getApp_id() {
		return app_id;
	}
	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}
	private String func_Code;
	private String tset_Id;
	private String ext_file_path;
	public int getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(int i) {
		this.seqNo = i;
	}
	
	public String getSchID() {
		return schID;
	}
	public void setSchID(String schID) {
		this.schID = schID;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getFunc_Code() {
		return func_Code;
	}
	public void setFunc_Code(String func_Code) {
		this.func_Code = func_Code;
	}
	public String getTset_Id() {
		return tset_Id;
	}
	
	public String getExt_file_path() {
		return ext_file_path;
	}
	public void setExt_file_path(String ext_file_path) {
		this.ext_file_path = ext_file_path;
	}
	
	private String apiOperation;

	public String getApiOperation() {
		return apiOperation;
	}
	public void setApiOperation(String apiOperation) {
		this.apiOperation = apiOperation;
	}
	

}
