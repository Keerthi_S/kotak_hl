/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Scheduler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 05-12-2016			sahana				   Req#TJN_243_03
 * 16-Dec-2016			Sahana					Req#TEN-72
 * 27-Jan-2017			Sahana					TENJINCG-61(A Tenjin user should be able to specify Screenshot Option while Scheduling an Execution task)
 * 05-07-2017			Gangadhar Badagi		TENJINCG-263
 * 18-07-2017			Gangadhar Badagi		added for Learn API
 * 21-07-2017			Gangadhar Badagi		TENJINCG-292
 * 21-09-2017           Gangadhar Badagi		TENJINCG-348
 */



package com.ycs.tenjin.scheduler;


import java.util.ArrayList;
import java.util.List;

public class Scheduler {

	private int schedule_Id;
	private String sch_date;
	private String sch_time;
	private String action;

	private String reg_client;
	private String status;
	private String taskName;

	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getBrowserType() {
		return browserType;
	}
	public void setBrowserType(String browserType) {
		this.browserType = browserType;
	}
	public String getAutLoginTYpe() {
		return autLoginType;
	}
	public void setAutLoginTYpe(String autLoginTYpe) {
		this.autLoginType = autLoginTYpe;
	}
	private String created_by;
	private String created_on;
	private int runId;
	private String browserType;
	private String autLoginType;
	private String frequency;
	private int recurCycles;
	private String recurDays;
	private String endDate;
	private String schRecur;
	
	
	public String getSchRecur() {
		return schRecur;
	}
	public void setSchRecur(String schRecur) {
		this.schRecur = schRecur;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public int getRecurCycles() {
		return recurCycles;
	}
	public void setRecurCycles(int recurCycles) {
		this.recurCycles = recurCycles;
	}
	public String getRecurDays() {
		return recurDays;
	}
	public void setRecurDays(String recurDays) {
		this.recurDays = recurDays;
	}
	private int screenShotOption ;
	public int getScreenShotOption() {
		return screenShotOption;
	}
	public void setScreenShotOption(int screenShotOption) {
		this.screenShotOption = screenShotOption;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	private int projectId;

	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public int getRun_id() {
		return runId;
	}
	public void setRun_id(int run_id) {
		this.runId = run_id;
	}
	private ArrayList<SchMapping> schMap;

	public ArrayList<SchMapping> getSchMap() {
		return schMap;
	}
	public void setSchMap(ArrayList<SchMapping> schMap) {
		this.schMap = schMap;
	}

	public int getSchedule_Id() {
		return schedule_Id;
	}
	public void setSchedule_Id(int schedule_Id) {
		this.schedule_Id = schedule_Id;
	}
	public String getSch_date() {
		return sch_date;
	}
	public void setSch_date(String string) {
		this.sch_date = string;
	}
	public String getSch_time() {
		return sch_time;
	}
	public void setSch_time(String sch_time) {
		this.sch_time = sch_time;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getReg_client() {
		return reg_client;
	}
	public void setReg_client(String reg_client) {
		this.reg_client = reg_client;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getCreated_on() {
		return created_on;
	}
	public void setCreated_on(String string) {
		this.created_on = string;
	}

	
	private String appName;
	private List<String> functions;
	private String testSetName;
	private List<String> testDataPath;
	private String type;
	private String testCaseName;
	private String apiCode;
	public String getApiCode() {
		return apiCode;
	}
	public void setApiCode(String apiCode) {
		this.apiCode = apiCode;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public List<String> getFunctions() {
		return functions;
	}
	public void setFunctions(List<String> functions) {
		this.functions = functions;
	}
	
	public String getTestSetName() {
		return testSetName;
	}
	public void setTestSetName(String testSetName) {
		this.testSetName = testSetName;
	}
	public List<String> getTestDataPath() {
		return testDataPath;
	}
	public void setTestDataPath(List<String> testDataPath) {
		this.testDataPath = testDataPath;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTestCaseName() {
		return testCaseName;
	}
	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}
	
	private String apiLearnType;

	
	public String getApiLearnType() {
		return apiLearnType;
	}
	public void setApiLearnType(String apiLearnType) {
		this.apiLearnType = apiLearnType;
	}
	
	private String message;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	


}
