/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Paginator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
*14-09-2018			   	Sriram Sridharan		Newly Added for TENJINCG-735
*05-10-2018				Sriram Sridharan		Fix for TENJINCG-836
*24-10-2018				Sriram Sridharan		Fix for TENJINCG-866
*29-10-2018				Sriram Sridharan		Fix for TENJINCG-852
*20-12-2018				Pushpalatha				TJN262R2-21,TJN262R2-25
*20-12-2018				Pushpalatha				TJN262R2-49
*24-12-2018			Sriram Sridharan		TJN262R2-48
*/

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;


public class Paginator implements AutoCloseable{
	private static final Logger logger = LoggerFactory.getLogger(Paginator.class);
	
	private PreparedStatement preparedStatement;
	private ResultSet resultSet;
	private Connection connection;
	
	private static final int DEFAULT_OFFSET = 1;
	private static final int DEFAULT_MAX_ROW_COUNT = 10;
	
	private int maxRecords;
	private int offset;
	private int startRow;
	private int endRow;
	private boolean connectEagerly;
	public ResultSet getResultSet() {
		return resultSet;
	}
	public Connection getConnection() {
		return connection;
	}



	public Paginator(int maxRecords, int pageNumber) {
		this.maxRecords = maxRecords;
		this.offset = pageNumber;
		this.init();
	}
	public Paginator(int maxRecords, int pageNumber, boolean connectEagerly) throws DatabaseException {
		this.maxRecords = maxRecords;
		this.offset = pageNumber;
		this.connectEagerly = connectEagerly;
		this.initEager();
	}
	public void executeQuery(String entity,String query, Object...parameters) throws SQLException, DatabaseException {
		if(!this.connectEagerly || this.connection == null) {
			this.connection = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		}
		this.executeQuery(entity,this.connection, query, parameters);
	}
	
	public void executeQuery(String entity,Connection conn, String query, Object...parameters) throws SQLException, DatabaseException {
		String dbProductName = conn.getMetaData().getDatabaseProductName();
		
		String orderByClause = null;
		if(!"oracle".equalsIgnoreCase(dbProductName)) {
			orderByClause = this.stripLastOrderByClause(query);
		}
		
		String paginatedQuery = this.getRecordCountQuery(query, dbProductName);
		
		
		
		paginatedQuery = this.getPaginationQuery(paginatedQuery, dbProductName);
		
		if(paginatedQuery.contains("<<ORDER_BY_CLAUSE")) {
			paginatedQuery = paginatedQuery.replaceAll("<<ORDER_BY_CLAUSE>>", orderByClause);
		}
		Object[] paramsArray = null;
		if(parameters != null) {
			paramsArray = new Object[parameters.length + 2];
		}else {
			paramsArray = new Object[2];
		}
		
		for(int i=0; i<parameters.length; i++) {
			paramsArray[i] = parameters[i];
		}
		
		paramsArray[paramsArray.length-2] = this.startRow-1;
		paramsArray[paramsArray.length-1] = this.endRow;
		
		this.preparedStatement = PreparedStatementBuilder.build(conn, paginatedQuery, paramsArray);
		this.resultSet = this.preparedStatement.executeQuery();
	}
	
	
	private String getPaginationQuery(String query, String dbProductName) {
		String paginatedQuery = "";
		if(dbProductName.equalsIgnoreCase("oracle")) {
			paginatedQuery = "SELECT C.* FROM (SELECT ROWNUM AS R_NUM, B.* FROM ( " + query + ") B ) C WHERE R_NUM > ? AND R_NUM < ?" ;
		}else {
			if(query.toLowerCase().startsWith("select ")) {
				query = "Select top 100 percent " + query.substring(6);
			}
			paginatedQuery = "SELECT C.* FROM ( SELECT B.*, ROW_NUMBER() OVER ( ORDER BY <<ORDER_BY_CLAUSE>> ) AS R_NUM FROM ( " + query + " ) B ) C WHERE R_NUM > ? AND R_NUM < ?";
		}
		logger.debug("Generated pagination query: [{}]", paginatedQuery);
		return paginatedQuery;
	}
	
	private String getRecordCountQuery(String query, String dbProductName) {
		String rQuery = "";
		if("oracle".equalsIgnoreCase(dbProductName)) {
			rQuery = "WITH BASE AS (" + query + ") select (select count(*) from base) P_REC_COUNT, base.* from BASE";
		}else {
			if(query.toLowerCase().startsWith("select ")) {
				query = "Select top 100 percent " + query.substring(6);
			}
			rQuery = "SELECT ( SELECT COUNT( * ) FROM ( " + query + " ) A ) P_REC_COUNT, BASE.* FROM ( " + query + " ) BASE ";
		}
		logger.debug("Generated record count query: [{}]", rQuery);
		return rQuery;
	}
	
	private void init() {
		if(this.offset < 1) {
			logger.warn("Invalid offset [{}] specified. Using default offset [{}]", this.offset, DEFAULT_OFFSET);
			this.offset = DEFAULT_OFFSET;
		}
		
		if(this.maxRecords < 1) {
			logger.warn("Invalid Max reocrds [{}] specified. Using default value [{}]", this.maxRecords, DEFAULT_MAX_ROW_COUNT);;
			this.maxRecords = DEFAULT_MAX_ROW_COUNT;
		}
		
		if(this.offset == 1) {
			this.startRow = 1;
			this.endRow = this.maxRecords+1;
		} else {
			this.endRow = (this.offset * this.maxRecords) + 1;
			
			this.startRow = this.endRow - this.maxRecords;
		}
		
		
	}
	private void initEager() throws DatabaseException {
		this.init();
		
		if(this.connectEagerly) {
			logger.info("Establishing connection to the database");
			this.connection = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		}
	}
	

	@Override
	public void close() throws Exception {
		if(this.resultSet != null) {
			logger.debug("Closing ResultSet");
			this.resultSet.close();
		}
		
		if(this.preparedStatement != null) {
			logger.debug("Closing PreparedStatement");
			this.preparedStatement.close();
		}
		
		if(this.connection != null) {
			logger.debug("Closing Connection");
			this.connection.close();
		}
	}
	private String stripLastOrderByClause(String query) {
		String orderByClause=null;
		if(query.toLowerCase().contains("order by")) {
			String[] qSplit = query.toLowerCase().split("order by");
			if(qSplit.length > 1) {
				orderByClause = qSplit[qSplit.length-1]; 
			}
		}
		
		if(Utilities.trim(orderByClause).length() > 0) {
			query = query.toLowerCase().replace("order by " + orderByClause, "");
		}
		
		
		return orderByClause;
	}
}
