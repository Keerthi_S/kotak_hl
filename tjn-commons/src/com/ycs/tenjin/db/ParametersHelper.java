
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ParametersHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 03-11-2017            Padmavathi              Newly added for TENJINCG-575
 * 19-04-2018            Padmavathi              TENJINCG-630
 */



package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.mail.TenjinMailLogs;
import com.ycs.tenjin.util.Constants;

public class ParametersHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(ParametersHelper.class);
	
	public void pesistsEmailConfiguration(Map<String, String> map) throws DatabaseException{
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		logger.info("persisting emailconfiguration");
		try{
		  pst=conn.prepareStatement("INSERT INTO TJN_PARAMS(PARAM_NAME,PARAM_VALUE) VALUES(?,?)");
		 	for (Entry<String, String> entry : map.entrySet()) {
				pst.setString(1,  entry.getKey());
				pst.setString(2,  entry.getValue());
				pst.addBatch();
			 }
			 pst.executeBatch(); 
			 logger.info("persisting emailconfiguration completed");
		}catch(Exception e){
			logger.error("error occured while persisting emailconfiguration");
			throw new DatabaseException("Could not configure email");
		}finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);	
		}
		
	}
	public void deleteEmailConfiguration(Set<String> keys) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		try{
			logger.info("deleting emailconfiguration");
			  pst=conn.prepareStatement("DELETE FROM TJN_PARAMS WHERE PARAM_NAME=?");
			  for(String key:keys){
				  pst.setString(1,  key);
				  pst.addBatch();
			  }
				 pst.executeBatch(); 
				 logger.info("deleting emailconfiguration completed");
			}catch(Exception e){
				logger.error("error occured while deleting emailconfiguration");
				throw new DatabaseException("Could not delete email");
			}finally{
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);	
			}
	}
	
	public HashMap<String, String> hydrateEmailConfiguration() throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		Statement st=null;
		ResultSet rs=null;
		HashMap<String,String> map=new HashMap<String,String>();
		logger.info("hydrating emailconfiguration");
		try{
			 st = conn.createStatement();
			 rs = st.executeQuery("SELECT * FROM TJN_PARAMS");
			while(rs.next()){
				map.put(rs.getString("PARAM_NAME"), rs.getString("PARAM_VALUE"));
		
			}
			logger.info("hydrating emailconfiguration completed");
			}catch(Exception e){
				logger.info("error occured hydrating emailconfiguration ");
				throw new DatabaseException("Could not delete email");
			}finally{
				DatabaseHelper.close(st);
				DatabaseHelper.close(rs);
				DatabaseHelper.close(conn);	
			}
		return map;
	}
	public void persistMailLogs(TenjinMailLogs mailLogs) throws DatabaseException{
		Connection conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		PreparedStatement pst=null;
		try{
			logger.info("Persisting mailLogs");
			pst=conn.prepareStatement("INSERT INTO TJN_MAIL_LOGS(RUN_ID,MAIL_SENDER_ID,MAIL_RECEIPIENT_LIST,MAIL_SENT_TIME,MAIL_CC_LIST,SENT_STATUS)VALUES(?,?,?,?,?,?)");
			
			pst.setInt(1, mailLogs.getRunId());
			pst.setString(2, mailLogs.getSenderId());
			pst.setString(3, mailLogs.getRecipientIds());
			pst.setTimestamp(4, mailLogs.getSentTime());
			pst.setString(5, mailLogs.getCcList());
			pst.setString(6, mailLogs.getSentStatus());
			
			pst.execute();
		}catch(Exception e){
			logger.error("error occured while persisting mailLogs");
			throw new DatabaseException("Could not persists mailLogs");
		}finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);	
		}
		
	}
	
	public ArrayList<TenjinMailLogs> hydrateMailLogs(int runId) throws DatabaseException{
		Connection conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		PreparedStatement pst = null;
		ResultSet rs=null;
		TenjinMailLogs mailInfo=null;
		ArrayList<TenjinMailLogs> mailLogs=new ArrayList<TenjinMailLogs>();
		try{
			logger.info("Hydrating mailLogs");
			pst = conn.prepareStatement("SELECT * FROM TJN_MAIL_LOGS WHERE RUN_ID=?");
			pst.setInt(1, runId);
			rs = pst.executeQuery();
			while(rs.next()){
				mailInfo=this.mailLogsMapFields(rs);
				mailLogs.add(mailInfo);
			}
		}catch(Exception e){
			throw new DatabaseException("Could not fetch mailLogs", e);
		}finally {
		DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);
	}
		return mailLogs;
		
	}
	
	private TenjinMailLogs mailLogsMapFields(ResultSet rs) throws SQLException{
		TenjinMailLogs mailLogs=new TenjinMailLogs();
		
		mailLogs.setRunId(rs.getInt("RUN_ID"));
		mailLogs.setSenderId(rs.getString("MAIL_SENDER_ID"));
		mailLogs.setRecipientIds(rs.getString("MAIL_RECEIPIENT_LIST"));
		mailLogs.setSentStatus(rs.getString("SENT_STATUS"));
		mailLogs.setCcList(rs.getString("MAIL_CC_LIST"));
		mailLogs.setSentTime(rs.getTimestamp("MAIL_SENT_TIME"));
		
		return mailLogs;
	}
 
}
