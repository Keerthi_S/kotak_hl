/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DatabaseHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 19-Oct-2017			sriram					TENJINCG-396
* 20-04-2018			Preeti					TENJINCG-616
* 11-07-2018            Padmavathi              for closing connections
*/

package com.ycs.tenjin.db;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;

public class DatabaseHelper {
	private static Connection conn;
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DatabaseHelper.class);
	
	public static Connection getConnection(String dbName) throws DatabaseException{
		
		
		
		String connType = "";
		
		try{
			connType = TenjinConfiguration.getProperty("JDBC_CONN_TYPE");
		}catch(TenjinConfigurationException e){
			logger.error("ERROR --> Could not fetch JDBC Connection Type from properties file.",e);
			throw new DatabaseException("Could not connect to database due to an internal error");
		}
		
		if(connType != null && connType.equals("pool")){
			try {
				conn = getConnectionFromPool(dbName);
			} catch (TenjinConfigurationException e) {
				
				logger.error(e.getMessage(),e);
				throw new DatabaseException("Could not connect to database due to an internal error");
			}
		}else if(connType != null && connType.equalsIgnoreCase("jdbc")){
			try {
				conn = getJdbcConnection(dbName);
			} catch (TenjinConfigurationException e) {
				
				logger.error(e.getMessage(),e);
				throw new DatabaseException("Could not connect to database due to an internal error");
			}
		}else{
			logger.error("ERROR --> Invalid value specified for JDBC_CONN_TYPE. Please check your properties file");
			throw new DatabaseException("Could not connect to database due to an internal error");
		}
	
		
		return conn;
		
	}
	
	
	
	private static Connection getConnectionFromPool(String dbName) throws DatabaseException, TenjinConfigurationException{
		Connection conn = null;
		
		String jndiName = TenjinConfiguration.getProperty("DB_JNDI");
		
		Context initContext=null;
		DataSource ds = null;
		
		try{
			initContext = new InitialContext();
			
			ds = (DataSource)initContext.lookup(jndiName);
		}catch (NamingException e) {
			
			logger.error("Could not create datasouce",e);
			throw new DatabaseException("Could not connect to database due to an internal error");
		}
		
		try{
			conn = ds.getConnection();
		}catch(Exception e){
			logger.error("ERROR getting connection from DataSource",e);
			throw new DatabaseException("Could not connect to database due to an internal error");
		}
		
		return conn;
	}
	
	private static Connection getJdbcConnection(String dbName) throws DatabaseException, TenjinConfigurationException{
		Connection conn = null;
		
		String db = "";
		String dbServer = "";
		String dbPort = "";
		String dbUid = "";
		String dbPwd = "";
		String dbSid = "";
		String databaseName = "";
		db = TenjinConfiguration.getProperty("DB_ENGINE");
		dbServer = TenjinConfiguration.getProperty("DB_SERVER");
		dbPort = TenjinConfiguration.getProperty("DB_PORT");
		if(db != null && db.equalsIgnoreCase("oracle")){
			dbSid = TenjinConfiguration.getProperty("DB_SID");
		}else if(db != null && db.equalsIgnoreCase("sqlserver")){
			databaseName = TenjinConfiguration.getProperty(dbName.toUpperCase() + "_DBNAME");
		}
		
		dbUid = TenjinConfiguration.getProperty(dbName.toUpperCase() + "_UID");
		dbPwd = TenjinConfiguration.getProperty(dbName.toUpperCase() + "_PWD");
		
		if(db != null && db.equalsIgnoreCase("oracle")){
			String connectionURL = "jdbc:oracle:thin:@" + dbServer + ":" + dbPort + ":" + dbSid;
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				conn = DriverManager.getConnection(connectionURL,dbUid,dbPwd);
			} catch (ClassNotFoundException e) {
				
				logger.error(e.getMessage(),e);
				throw new DatabaseException("Could not connect to database due to an internal error",e);
			} catch (SQLException e) {
				
				logger.error(e.getMessage(),e);
				throw new DatabaseException("Could not connect to " + dbName,e);
			}
		}
		else if(db != null && db.equalsIgnoreCase("sqlserver")){
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");	
				conn = DriverManager.getConnection("jdbc:sqlserver://"+dbServer+":"+dbPort+";user="+dbUid+";password="+dbPwd+";database="+databaseName.toLowerCase());
			} catch (ClassNotFoundException e) {
				
				logger.error(e.getMessage(),e);
				throw new DatabaseException("Could not connect to database due to an internal error",e);
			} catch (SQLException e) {
				
				logger.error(e.getMessage(),e);
				throw new DatabaseException("Could not connect to " + dbName,e);
			}
		}
		else{
			logger.error("Database " + db + " is not supported yet by Tenjin");
			throw new DatabaseException("Database " + db + " is not supported yet by Tenjin");
		}
		
		return conn;
	}
	
	
	public static int getGlobalSequenceNumber(Connection conn) throws DatabaseException{
		PreparedStatement pst=null;
		ResultSet rs =null;
		try {
			DatabaseMetaData meta = conn.getMetaData(); 
			if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				pst = conn.prepareStatement("select SQ_GLOBAL.nextval maxVal from dual");
			else
				pst = conn.prepareStatement("select next value for SQ_GLOBAL as maxVal");
			rs = pst.executeQuery();
			int seq=0;
			while(rs.next()) {
				seq=rs.getInt("maxVal");
			}
			
			rs.close();pst.close();
			return seq;
		} catch (SQLException e) {
			logger.error("ERROR getting Global Sequence value", e);
			throw new DatabaseException("Could not get global sequence", e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
	}
	
	public static void closeConnection(Connection conn) {
		try {
			conn.close();
		}catch(Exception ignore) {
			logger.warn("WARNING - There was an error closing the Database Connection.", ignore);
		}
	}
	
	public static void commit(Connection conn) {
		try {
			conn.commit();
		}catch(Exception e) {
			logger.error("ERROR committing transaction", e);
		}
	}
	
	public static void rollback(Connection conn) {
		try {
			conn.rollback();
		}catch(Exception e) {
			logger.error("ERROR rolling back inserts", e);
		}
	}
	
	/*public static void setAutoCommit(Connection conn, boolean autoCommit) {
		try {
			conn.setAutoCommit(autoCommit);
		} catch (SQLException e) {
			logger.error("ERROR setting auto-commit to {}", autoCommit, e);
		}
	}*/
	


    public static void close(ResultSet resultSet) {
          if (resultSet == null){
                return;
          }
          else{
                try {resultSet.close();} catch (SQLException e) {}
          }     
    }
    
    public static void close(Statement statement) {
          if (statement == null){
                return;
          }
          else{
                try {statement.close();} catch (SQLException e) {}
          }     
    }
    
    public static void close(Connection connection) {
          if (connection == null){
                return;
          }
          else{
                try {connection.close();} catch (SQLException e) {}
          }     
    }

}
