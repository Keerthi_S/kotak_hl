/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DefectHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 *03-12-2016            Parveen                	REQ #TJN_243_04 Changing insert query
* 08-Dec-2016			Sriram Sridharan		Changed by Sriram to implement DTT User Mapping functionality
* 10-Aug-2017		    Manish					T25IT-113
* 04-Dec-2017		    Manish					TCGST-4
* 20-04-2018			Preeti					TENJINCG-616
* 15-06-2018            Padmavathi              T251IT-18
* 11-07-2018            Padmavathi              for closing connections
* 31-12-2018			Ashiki					TJN252-49
* 31-12-2018			Ashiki					Deleting DDTUsers for SQL server
* 15-03-2019			Pushpa					TENJINCG-956
* 15-03-2019			Ashiki				    TENJINCG-986
* * 06-02-2020			Roshni					TENJINCG-1168
 */



/*
 * Added File by sahana for Requirement TJN_24_06
 */
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ycs.tenjin.defect.DefectManagementInstance;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;

public class DefectHelper {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DefectHelper.class);
	public ArrayList<DefectManagementInstance> hydrateAllDefects()  throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_CORE);
		Statement st =null;
		PreparedStatement st1 =null;
		ResultSet rs =null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<DefectManagementInstance> dm = new ArrayList<DefectManagementInstance>();

		try {
			 st = conn.createStatement();

			 rs = st.executeQuery("SELECT * FROM TJN_DEF_TOOL_MASTER ");
			while (rs.next()) {
				st1=conn.prepareStatement("SELECT * FROM TJN_PRJ_AUTS WHERE PRJ_DEF_MANAGER=?");
				st1.setString(1, rs.getString("INSTANCE_NAME"));
				ResultSet rs1=st1.executeQuery();
				DefectManagementInstance dm1 = new DefectManagementInstance();

				dm1.setName(rs.getString("INSTANCE_NAME"));
				dm1.setTool(rs.getString("TOOL_NAME"));
				dm1.setURL(rs.getString("URL"));
				dm1.setAdminId(rs.getString("ADMIN_ID"));
				dm1.setPassword(rs.getString("ADMIN_PWD"));
				if(rs1.next()){
					dm1.setStatus("Mapped");
				}else{
					dm1.setStatus("Not Mapped");
				}
				dm.add(dm1);


			}
			

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch records", e);
		} finally {
		
			DatabaseHelper.close(rs);
			DatabaseHelper.close(st);
			DatabaseHelper.close(conn);
		}

		return dm;

	}

	public int checkName(String name) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement p = null;
		ResultSet rs =null;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		int chName=0;
		try {

			p = conn.prepareStatement("SELECT 1 FROM TJN_DEF_TOOL_MASTER where upper(INSTANCE_NAME)=?");
			p.setString(1, name.toUpperCase());
			rs = p.executeQuery();
			if(rs.next()) {

				chName=rs.getInt(1);
			}
			if(chName>1)
			{
				throw new DatabaseException("The name you entered is already in use. Please use a different name");	
			}
			
		}catch (Exception e) {
			logger.error("Error ", e);

			throw new DatabaseException("Could not create client", e);
		}finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(p);
			DatabaseHelper.close(conn);
		}
		return chName;

	}

	public DefectManagementInstance persistDefect(DefectManagementInstance dm) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}

		try {
			pst = conn.prepareStatement("INSERT INTO TJN_DEF_TOOL_MASTER (INSTANCE_NAME,TOOL_NAME,URL,ADMIN_ID,ADMIN_PWD) VALUES (?,?,?,?,?)");
			pst.setString(1, dm.getName());
			pst.setString(2, dm.getTool());
			pst.setString(3, dm.getURL());
			pst.setString(4, dm.getAdminId());
			pst.setString(5, dm.getPassword());

			pst.execute();

			Map<String,Integer> svrtymap=	this.severityDtt(dm.getSeverity());
			Map<String,Integer> prty=this.severityDtt(dm.getPriority());
			Map<String ,Map<String,Integer>> both = new  LinkedHashMap<String, Map<String,Integer>>();
			both.put("Priority", prty);
			both.put("Severity", svrtymap);
			/*Modified by Roshni TENJINCG-1168 starts */
			/*this.persistprtyDtt(dm.getName(),both);*/
			this.persistprtyDtt(dm.getName(),both,conn);
			/*Modified by Roshni TENJINCG-1168 ends */

		}catch (Exception e) {
			logger.error("Error ", e);
			throw new DatabaseException("Could not create defect", e);
		} finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return dm;
	}
	/*Modified by Roshni TENJINCG-1168 starts */
	private void persistprtyDtt(String name, Map<String ,Map<String,Integer>> serPrir,Connection conn) throws DatabaseException {
	/*private void persistprtyDtt(String name, Map<String ,Map<String,Integer>> serPrir) throws DatabaseException {*/
		//Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		/*Modified by Roshni TENJINCG-1168 ends */
		PreparedStatement pst =null;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}

		try {
			
			for(Entry<String, Integer> mp: serPrir.get("Severity").entrySet()){
				pst = conn.prepareStatement("INSERT INTO TJN_DTT_PROPERTIES(INSTANCE_NAME,PROPERTY_NAME,PROPERTY_SCORE,PROPERTY_VALUE) VALUES (?,?,?,?)");
				pst.setString(1,name);
				pst.setString(2, "Severity");
				pst.setInt(3, mp.getValue());
				pst.setString(4,mp.getKey());

				pst.execute();
				DatabaseHelper.close(pst);
			}

			pst = null;
			for(Entry<String, Integer> mp : serPrir.get("Priority").entrySet())
			{
				pst = conn.prepareStatement("INSERT INTO TJN_DTT_PROPERTIES(INSTANCE_NAME,PROPERTY_NAME,PROPERTY_SCORE,PROPERTY_VALUE) VALUES (?,?,?,?)");
				pst.setString(1,name);
				pst.setString(2, "Priority");
				pst.setInt(3,  mp.getValue() );
				pst.setString(4,mp.getKey());

				pst.execute();
				DatabaseHelper.close(pst);
			}

		}	

		catch (Exception e) {
			logger.error("Error ", e);
			throw new DatabaseException("Could not create defect", e);
		} finally {
			/*Commented by Roshni TENJINCG-1168 */
			/*DatabaseHelper.close(conn);*/
		}
	}

	private Map<String,Integer> severityDtt(String value) {
		String str=value;

		String [] svrty={};
		svrty=str.split(",");

		Map<String,Integer> map=new LinkedHashMap<String, Integer>();
		for(int i=0;i<svrty.length;i++)
			map.put(svrty[i],svrty.length-i);

		return map;
	}

	
	public DefectManagementInstance hydrateDefects(String name) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		DefectManagementInstance rc = null;
		try {

			pst = conn
					.prepareStatement("SELECT * FROM TJN_DEF_TOOL_MASTER WHERE INSTANCE_NAME =?");
			pst.setString(1,name );
			System.out.println("------instance name------"+name);
			rs = pst.executeQuery();

			while (rs.next()) {
				rc = new DefectManagementInstance();
				rc.setName(rs.getString("INSTANCE_NAME"));
				rc.setTool(rs.getString("TOOL_NAME"));
				rc.setURL(rs.getString("URL"));
				rc.setAdminId(rs.getString("ADMIN_ID"));
				rc.setPassword(rs.getString("ADMIN_PWD"));
			}


			
			String svrty=this.hydratedtt(name,"Severity");
			String prty=this.hydratedtt(name,"Priority");

			rc.setPriority(prty);
			rc.setSeverity(svrty);

		} catch (Exception e) {
			logger.error("Error ", e);
			throw new DatabaseException("Could not fetch record ", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return rc;

	}
	/*Added by Roshni TENJINCG-1168 starts */
	public DefectManagementInstance hydrateDefects(Connection conn,String name) throws DatabaseException {

		PreparedStatement pst =null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		DefectManagementInstance rc = null;
		try {

			pst = conn
					.prepareStatement("SELECT * FROM TJN_DEF_TOOL_MASTER WHERE INSTANCE_NAME =?");
			pst.setString(1,name );
			System.out.println("------instance name------"+name);
			rs = pst.executeQuery();

			while (rs.next()) {
				rc = new DefectManagementInstance();
				rc.setName(rs.getString("INSTANCE_NAME"));
				rc.setTool(rs.getString("TOOL_NAME"));
				rc.setURL(rs.getString("URL"));
				rc.setAdminId(rs.getString("ADMIN_ID"));
				rc.setPassword(rs.getString("ADMIN_PWD"));
			}


			
			String svrty=this.hydratedtt(name,"Severity");
			String prty=this.hydratedtt(name,"Priority");

			rc.setPriority(prty);
			rc.setSeverity(svrty);

		} catch (Exception e) {
			logger.error("Error ", e);
			throw new DatabaseException("Could not fetch record ", e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return rc;

	}
	/*Modified by Roshni TENJINCG-1168 ends */
	public DefectManagementInstance hydrateDefectManagerInstance(Connection conn, String name) throws DatabaseException {

		//Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		DefectManagementInstance rc = null;
		try {

			pst = conn
					.prepareStatement("SELECT * FROM TJN_DEF_TOOL_MASTER WHERE INSTANCE_NAME =?");
			pst.setString(1,name );
			System.out.println("------instance name------"+name);
			rs = pst.executeQuery();

			while (rs.next()) {
				rc = new DefectManagementInstance();
				rc.setName(rs.getString("INSTANCE_NAME"));
				rc.setTool(rs.getString("TOOL_NAME"));
				rc.setURL(rs.getString("URL"));
				rc.setAdminId(rs.getString("ADMIN_ID"));
				rc.setPassword(rs.getString("ADMIN_PWD"));
			}


			/*rs.close();
			pst.close();*/
			String svrty=this.hydratedtt(name,"Severity");
			String prty=this.hydratedtt(name,"Priority");

			rc.setPriority(prty);
			rc.setSeverity(svrty);

		} catch (Exception e) {
			logger.error("Could not fetch DTT instance", e);
			throw new DatabaseException("Could not fetch DTT Instance information. Please contact Tenjin Support", e);
		} finally {
			/*try {
				conn.close();
			} catch (Exception e) {
				logger.error("Error ", e);
			}*/
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return rc;

	}
	/*changed by manish for bug T25IT-113 on 10 Aug 2017 starts*/
	public DefectManagementInstance _hydrateDefectManagerInstance(String instanceName, String tenjinUserId) throws DatabaseException {
		Connection conn=null;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			return this.hydrateDefectManagerInstance(conn, instanceName,tenjinUserId);
		} finally  {
			DatabaseHelper.close(conn);
		}
	}
	
	public DefectManagementInstance hydrateDefectManagerInstance(Connection conn, String name, String tenjinUserId) throws DatabaseException {

		PreparedStatement pst =null;
		ResultSet rs = null;

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		DefectManagementInstance rc = null;
		try {
			
			
			pst = conn.prepareStatement("SELECT A.*,B.DEF_TOOL_USERID,B.DEF_TOOL_PWD FROM TJN_DEF_TOOL_MASTER a, TJN_DEF_USER_MAPING B WHERE B.TJN_INSTANCE_NAME=A.INSTANCE_NAME AND B.TJN_USERID=? and a.INSTANCE_NAME = ?");
			
			pst.setString(1, tenjinUserId);
			pst.setString(2,name );
			System.out.println("------instance name------"+name);
			rs = pst.executeQuery();

			while (rs.next()) {
				rc = new DefectManagementInstance();
				rc.setName(rs.getString("INSTANCE_NAME"));
				rc.setTool(rs.getString("TOOL_NAME"));
				rc.setURL(rs.getString("URL"));
				rc.setAdminId(rs.getString("DEF_TOOL_USERID"));
				rc.setPassword(rs.getString("DEF_TOOL_PWD"));
			}


			String svrty=this.hydratedtt(name,"Severity");
			String prty=this.hydratedtt(name,"Priority");
			
			rc.setPriority(prty);
			rc.setSeverity(svrty);
		}catch (NullPointerException e) {
			throw new NullPointerException("Please define DTT user credentials for current user");
		} catch (Exception e) {
			logger.error("Could not fetch DTT instance", e);
			
			throw new DatabaseException(e.getMessage());
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return rc;

	}
	
	public String hydratedtt(  String name,String property) throws DatabaseException
	{
		List<String> svrty=new ArrayList<String>();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst =null;
		ResultSet rs = null;
		String str="";
		try {

			pst = conn
					.prepareStatement("SELECT * FROM TJN_DTT_PROPERTIES WHERE INSTANCE_NAME =? AND PROPERTY_NAME=? ORDER BY PROPERTY_SCORE DESC");
			pst.setString(1,name );
			pst.setString(2,property);
			rs = pst.executeQuery();

			while (rs.next()) {
				svrty.add(rs.getString("PROPERTY_VALUE"));	
			}
			

			for(int i=0;i<svrty.size();i++)
			{
				if(i!=svrty.size()-1)
					str=str+svrty.get(i).concat(",");
				else
					str=str+svrty.get(i);

			}
		} catch (Exception e) {
			logger.error("Error ", e);
			throw new DatabaseException("Could not fetch record ", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return str;

	}


	public void updateDefect(DefectManagementInstance dm, String exName) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);


		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst =null;
		try {


			pst = conn.prepareStatement("UPDATE TJN_DEF_TOOL_MASTER SET INSTANCE_NAME=?,TOOL_NAME =?, URL = ?,ADMIN_ID=?, ADMIN_PWD=? WHERE INSTANCE_NAME=?");

			pst.setString(1, dm.getName());
			pst.setString(2, dm.getTool());
			pst.setString(3, dm.getURL());
			pst.setString(4, dm.getAdminId());
			pst.setString(5, dm.getPassword());
			pst.setString(6,exName );

			pst.execute();
			Map<String,Integer> severity= this.severityDtt(dm.getSeverity());
			Map<String,Integer> priority= this.severityDtt(dm.getPriority());
			/*Modified by Roshni TENJINCG-1168 starts */
			/*this.updateDtt(dm,priority,severity, "Priority");
			this.updateDtt(dm,priority, severity, "Severity");*/
			this.updateDtt(dm,priority,severity, "Priority",conn);
			this.updateDtt(dm,priority, severity, "Severity",conn);
			/*Modified by Roshni TENJINCG-1168 ends */
		} catch (Exception e) {
			System.out.println(" the error in update  helper  is ");
			logger.error("Error ", e);
			throw new DatabaseException(
					"Could not update defect because of an internal error ");
		}finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}
	/*Modified by Roshni TENJINCG-1168 starts */
	/*public void updateDtt( DefectManagementInstance dm, Map<String ,Integer> prty ,Map<String ,Integer> map, String property) throws DatabaseException {*/
	public void updateDtt( DefectManagementInstance dm, Map<String ,Integer> prty ,Map<String ,Integer> map, String property,Connection conn) throws DatabaseException {
	/*Modified by Roshni TENJINCG-1168 ends */
		
		try {		
			this.clearDtt(dm.getName());
			Map<String ,Map<String,Integer>> both = new  LinkedHashMap<String, Map<String,Integer>>();
			both.put("Priority", prty);
			both.put("Severity", map);
			/*Modified by Roshni TENJINCG-1168 starts */
			/*this.persistprtyDtt(dm.getName(),both);*/
			this.persistprtyDtt(dm.getName(),both,conn);
			/*Modified by Roshni TENJINCG-1168 ends */
		}
		catch (Exception e) {
			System.out.println(" the error in update  helper  is ");
			logger.error("Error ", e);
			throw new DatabaseException(
					"Could not update defect because of an internal error ");
		}
	}


	public void clearAll(String[] record) throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement prest =null;
		PreparedStatement pst =null;
		try {
			for( int i = 0 ; i < record.length; i++ )
			{
				/*Modified by Ashiki VAPT fixes starts */
				prest = conn.prepareStatement("DELETE FROM TJN_DEF_TOOL_MASTER WHERE INSTANCE_NAME=?");
				pst = conn.prepareStatement("DELETE FROM TJN_DTT_PROPERTIES WHERE INSTANCE_NAME=?");
				prest.setString(1,record[i]);
				pst.setString(1,record[i]);

				/*Modified by Ashiki VAPT fixes ends */
				prest.executeUpdate();
				pst.executeUpdate();
				prest.close();
				pst.close();
				
			}
		} catch (Exception e) {
			throw new DatabaseException("Could not delete all records", e);
		}

		finally {
			
			/*DatabaseHelper.close(pst);
			DatabaseHelper.close(prest);*/
			DatabaseHelper.close(conn);
		}




	}

	public void clear(String name) throws DatabaseException {

		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst =null;
		try {

			pst = conn
					.prepareStatement("DELETE FROM TJN_DEF_TOOL_MASTER WHERE INSTANCE_NAME =?");
			pst.setString(1, name);
			pst.executeUpdate();
			this.clearDtt(name);
		} catch (Exception e) {
			throw new DatabaseException("Could not delete record", e);
		} finally {
			
		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);
		}

	}


	public void clearDtt(String exname) throws DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst =null;
		try {

			pst = conn
					.prepareStatement("DELETE FROM TJN_DTT_PROPERTIES WHERE INSTANCE_NAME =?");
			pst.setString(1, exname);
			pst.executeUpdate();

		} catch (Exception e) {
			throw new DatabaseException("Could not delete record", e);
		} finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}


	}	
	
	public List<DefectManagementInstance> fetchTenjinDTTUserMapping(
			String userId) throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs =null;
		List<DefectManagementInstance> dms = new ArrayList<DefectManagementInstance>();
		try {
			/*modified by shruthi for sql injection starts*/
			pst = conn
					.prepareStatement("select * from tjn_def_user_maping where tjn_userid=?");
			pst.setString(1, userId);
			/*modified by shruthi for sql injection ends*/
			rs = pst.executeQuery();
			while (rs.next()) {
				DefectManagementInstance dmInstance = new DefectManagementInstance();
				dmInstance.setInstance(rs.getString("TJN_INSTANCE_NAME"));
				dmInstance.setDefToolUserId(rs.getString("DEF_TOOL_USERID"));
				dmInstance.setAdminId(rs.getString("DEF_TOOL_USERID"));
				dmInstance.setTjnUserId(rs.getString("TJN_USERID"));
				dms.add(dmInstance);
			}
		} catch (Exception e) {
			
			logger.error("Error ", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return dms;

	}
	
	public DefectManagementInstance fetchUserDTTDetails(String userId, String instance)throws DatabaseException{
		DefectManagementInstance dmInstance = new DefectManagementInstance();
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			/*modified by shruthi for sql injection starts*/
			pst = conn.prepareStatement("select * from TJN_DEF_USER_MAPING where TJN_USERID=? and TJN_INSTANCE_NAME=?");
			pst.setString(1, userId);
			pst.setString(2, instance);
			rs = pst.executeQuery();
			/*modified by shruthi for sql injection ends*/
			while(rs.next()){
				dmInstance.setTjnUserId(rs.getString("TJN_USERID"));
				dmInstance.setInstance(rs.getString("TJN_INSTANCE_NAME"));
				dmInstance.setAdminId(rs.getString("DEF_TOOL_USERID"));
				String encPwd = rs.getString("DEF_TOOL_PWD");
				/*Modified by paneendra for VAPT FIX starts*/
				String pwd = new CryptoUtilities().decrypt(encPwd);
				/*Modified by paneendra for VAPT FIX ends*/
				dmInstance.setDefToolPwd(pwd);
			}
		} catch (Exception e) {
			
			logger.error("Error ", e);
		}finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return dmInstance;
	}
	
	public void updateDTTUserMappingData(DefectManagementInstance dm,String oldInstance)
			throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		 dm.getTjnUserId();
		try {
			pst = conn.prepareStatement("UPDATE TJN_DEF_USER_MAPING SET DEF_TOOL_USERID=?,DEF_TOOL_PWD=?,TJN_INSTANCE_NAME=? WHERE TJN_USERID=? AND TJN_INSTANCE_NAME=?");
			String pwd = dm.getDefToolPwd();
			/*Modified by paneendra for VAPT FIX starts*/
			/*String encPwd = new Crypto().encrypt(pwd);*/
			String encPwd = new CryptoUtilities().encrypt(pwd);
			/*Modified by paneendra for VAPT FIX ends*/
			pst.setString(1, dm.getAdminId());
			pst.setString(2, encPwd);
			pst.setString(3, dm.getInstance());
			pst.setString(4, dm.getTjnUserId());
			pst.setString(5, oldInstance);
			rs = pst.executeQuery();
		} catch (Exception e) {
			
			logger.error("Error ", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}
	
	public void deleteDTTUser(String[] userIds, String[] dttInstances)
			throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		try {
			for (int i = 0; i < userIds.length; i++) {

				/*modified by shruthi for sql injection starts*/
				pst = conn.prepareStatement("DELETE FROM TJN_DEF_USER_MAPING WHERE DEF_TOOL_USERID=? AND TJN_INSTANCE_NAME=?");
				pst.setString(1, userIds[i]);
				pst.setString(2, dttInstances[i]);
				pst.execute();
				DatabaseHelper.close(pst);
				/*modified by shruthi for sql injection ends*/

			}
		} catch (SQLException e) {
			throw new DatabaseException("Could not delete record", e);
		} finally {
			
			DatabaseHelper.close(conn);
		}

	}
	
	/*Added by paneendra for VAPT fix starts*/
	public boolean deleteDTTUserByUserId(String[] userIds, String[] dttInstances,String userId)
			throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		boolean result=false;
		try {
			for (int i = 0; i < userIds.length; i++) {
				/*modified by shruthi for sql injection starts*/
				pst = conn.prepareStatement("DELETE FROM TJN_DEF_USER_MAPING WHERE DEF_TOOL_USERID=? AND TJN_INSTANCE_NAME=?  AND TJN_USERID=?");
				pst.setString(1, userIds[i]);
				pst.setString(2, dttInstances[i]);
				pst.setString(3, userId);
				/*modified by shruthi for sql injection ends*/
				/*Modified  by Priyanka for VAPT Sanity Test starts*/
				//result=pst.execute();
				if(pst.executeUpdate()>=1) {
					result = true;
				}
				/*Modified  by Priyanka for VAPT Sanity Test ends*/
				DatabaseHelper.close(pst);

			}
		} catch (SQLException e) {
			throw new DatabaseException("Could not delete record", e);
		} finally {
			
			DatabaseHelper.close(conn);
		}
		return result;

	}/*Added by paneendra for VAPT fix ends*/
	
	public void createDTTUserMappingData(DefectManagementInstance dm)
			throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		dm.getTjnUserId();
		try {
			
			pst = conn
					.prepareStatement("INSERT INTO TJN_DEF_USER_MAPING (TJN_USERID,TJN_INSTANCE_NAME,DEF_TOOL_USERID,DEF_TOOL_PWD) VALUES (?,?,?,?)");
			pst.setString(1, dm.getTjnUserId());
			pst.setString(2, dm.getInstance());
			pst.setString(3, dm.getAdminId());
			pst.setString(4, dm.getDefToolPwd());
			pst.execute();
		} catch (Exception e) {
			logger.error("Error ", e);
		} finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}
	public List<String> fetchAllInstances() throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs =null;
		List<String> instances = new ArrayList<String>();
		String instance = "";
		try {
			pst = conn
					.prepareStatement("select INSTANCE_NAME from tjn_def_tool_master");
			rs = pst.executeQuery();
			while (rs.next()) {
				instance = rs.getString("INSTANCE_NAME");
				instances.add(instance);
			}
		} catch (Exception e) {
			
			logger.error("Error ", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return instances;
	}
	
	
	public Map<Integer, List<String>> getSeverityDefinitions(int projectId, List<Integer> appIds) throws DatabaseException{
		
		Map<Integer, List<String>> map = new HashMap<Integer, List<String>>();
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			if (appIds != null) {
				for (int appId : appIds) {
					try {
						pst = conn.prepareStatement(
								"SELECT INSTANCE_NAME, PROPERTY_VALUE FROM TJN_DTT_PROPERTIES WHERE INSTANCE_NAME IN (SELECT PRJ_DEF_MANAGER FROM TJN_PRJ_AUTS WHERE PRJ_ID=? AND APP_ID=?) and property_name='Severity' order by property_score desc");
						pst.setInt(1, projectId);
						pst.setInt(2, appId);

						rs = pst.executeQuery();
						List<String> severities = new ArrayList<String>();
						String instanceName = "";
						while (rs.next()) {
							instanceName = rs.getString("INSTANCE_NAME");
							severities.add(rs.getString("PROPERTY_VALUE"));
						}

						severities.add(0, instanceName);

						map.put(appId, severities);

						DatabaseHelper.close(rs);
						DatabaseHelper.close(pst);
					} catch (SQLException e) {
						
						logger.error("ERROR getting severities for project {} app {}", projectId, appId, e);
					}

				}
			} 
		} finally {
			DatabaseHelper.close(conn);
		}
		
		
		
		return map;
	}
	
	
public DefectManagementInstance persistDefectManagementProjects(DefectManagementInstance defectManagerProjects) throws DatabaseException {
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("INSERT INTO TJN_DTT_PROJECTS (PRJ_ID,PROJECT_SET_NAME,INSTANCE,PROJECTS,INSTANCE_NAME,USER_ID)  VALUES(?,?,?,?,?,?)");)
		{
			int prjId=DatabaseHelper.getGlobalSequenceNumber(conn);
			pst.setInt(1, prjId);
			pst.setString(2,defectManagerProjects.getSubSetName());
			pst.setString(3, defectManagerProjects.getInstance());
			pst.setString(4,defectManagerProjects.getProjects() );
			pst.setString(5,defectManagerProjects.getName());
			pst.setString(6,defectManagerProjects.getUserId());
			pst.execute();
			defectManagerProjects.setPrjId(prjId);
		}catch(DatabaseException e){
			logger.error("ERROR Defect Managements Projects", e);
			throw e;
		}catch(Exception e){
			logger.error("Could not Create Defect Managements Project user",e);
			throw new DatabaseException("Could not insert Defect Managements Projects due to an internal error",e);
		}
		return defectManagerProjects;
	}

	public ArrayList<DefectManagementInstance> hydrateDefectManagementProjects(String name) throws DatabaseException{
		ArrayList<DefectManagementInstance> projectsLists = new ArrayList<DefectManagementInstance>();
		DefectManagementInstance projects= null;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT * FROM TJN_DTT_PROJECTS where INSTANCE_NAME=?");)
		{
			pst.setString(1, name);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					projects=new DefectManagementInstance() ;
					projects.setPrjId(rs.getInt("PRJ_ID"));
					projects.setSubSetName(rs.getString("PROJECT_SET_NAME"));
					projects.setInstance(rs.getString("INSTANCE"));
					projects.setProjects(rs.getString("PROJECTS"));
					projectsLists.add(projects);
				}
			}
			
			
		}catch(DatabaseException e){
			logger.error("ERROR Defect Managements Projects", e);
			throw e;
		}catch(Exception e){
			logger.error("Could not hydrate Defect Managements Projects",e);
			throw new DatabaseException("Could not hydrate Defect Managements Projects due to an internal error",e);
		}
		return projectsLists;
		
	}
	
	
	public JSONArray  hydrateSubSets(String name, String userId) throws DatabaseException{
		JSONArray array=new JSONArray();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT PROJECT_SET_NAME FROM TJN_DTT_PROJECTS where INSTANCE_NAME=? and USER_ID=? ");)
		{
			pst.setString(1, name);
			pst.setString(2, userId);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					JSONObject json=new JSONObject();
					json.put("subSet", rs.getString("PROJECT_SET_NAME"));
					array.put(json);
				}
			}
			
			
		}catch(DatabaseException e){
			logger.error("ERROR Defect Managements Projects", e);
			throw e;
		}catch(Exception e){
			logger.error("Could not hydrate Defect Managements Projects",e);
			throw new DatabaseException("Could not hydrate Defect Managements Projects due to an internal error",e);
		}
		return array;
		
	
	}
	
	
	public JSONObject  hydrateSubsetProjects(String subSet) throws DatabaseException{
		JSONObject projectJson=new JSONObject();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT PROJECTS FROM TJN_DTT_PROJECTS WHERE PROJECT_SET_NAME =? ");)
		{
			pst.setString(1, subSet);
			
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					projectJson.put("project", rs.getString("PROJECTS"));
				}
			}
			
			
		}catch(DatabaseException e){
			logger.error("ERROR Defect Managements Projects", e);
			throw e;
		}catch(Exception e){
			logger.error("Could not hydrate Defect Managements Projects",e);
			throw new DatabaseException("Could not hydrate Defect Managements Projects due to an internal error",e);
		}
		return projectJson;
		
	
	}

}