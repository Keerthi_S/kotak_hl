/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Utilities.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* Sep 1, 2016           Sriram Sridharan          Newly Added For 
*/

package com.ycs.tenjin.util;

import java.io.File;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class Utilities {
	
	private static Logger logger = LoggerFactory
			.getLogger(Utilities.class);
	
	public static void checkForDownloadsFolder(String contextRoot){
		String folderPath = contextRoot + "\\Downloads";
		File dir = new File(folderPath);
		if(!dir.exists()){
			dir.mkdir();
		}else{
			
		}
	}
	
	
	public static String trim(String str){
		if(str == null){return "";}
		return str.replace(String.valueOf((char) 160), " ").trim();
	}
	
	
	public static String calculateElapsedTime(long startMillis, long endMillis){
		long millis = endMillis - startMillis;
		String eTime = String.format(
				"%02d:%02d:%02d",
				TimeUnit.MILLISECONDS.toHours(millis),
				TimeUnit.MILLISECONDS.toMinutes(millis)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
								.toHours(millis)),
				TimeUnit.MILLISECONDS.toSeconds(millis)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
								.toMinutes(millis)));
		if(eTime.equalsIgnoreCase("00:00:00")){
			eTime = Long.toString(millis) + "ms";
		}
		
		return eTime;
	}
	public static boolean isNumericallyEqual(String exp, String act){
		try{
			String e = exp.replace(",", "");
			String a = act.replace(",", "");
			if(Double.parseDouble(e) == Double.parseDouble(a)){
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			return false;
		}
	}
	
	public static boolean isNumeric(String str){
		try{
			
			String s = str.replace(",", "");
			Double.parseDouble(s);
			
			return true;
		}catch(Exception e){
			return false;
		}
	}
	

	
	public static boolean isLocalHost(String hostName){
		try{
			return isLocalHost(InetAddress.getByName(hostName));
		}catch(UnknownHostException e){
			return false;
		}
	}
	public static boolean isLocalHost(InetAddress addr){
		if(addr.isAnyLocalAddress() || addr.isLoopbackAddress()){
			return true;
		}
		
		try{
			return NetworkInterface.getByInetAddress(addr) != null;
		}catch(Exception e){
			return false;
		}
	}
	
	
	
	public static String calculateElapsedTimeInMillis(long startMillis, long endMillis){
		long millis = endMillis - startMillis;
		return Long.toString(millis);
	}
	
	
	/*Added by Padmavathi for TENJINCG-726 starts*/
	public static String addElapsedTimes(List<String> elapsedTimeList){
		SimpleDateFormat st= new SimpleDateFormat("HH:mm:ss");
		long milliseconds = 0;
		for (final String t : elapsedTimeList) {
		  try {
			  long startingMS = st.parse("00:00:00").getTime();
			  milliseconds = milliseconds + (st.parse(t).getTime() - startingMS);
		  } catch (ParseException e) {
			logger.error("Error ", e);
		  }
		}
		String eTime = String.format(
				"%02d:%02d:%02d",
				TimeUnit.MILLISECONDS.toHours(milliseconds),
				TimeUnit.MILLISECONDS.toMinutes(milliseconds)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
								.toHours(milliseconds)),
				TimeUnit.MILLISECONDS.toSeconds(milliseconds)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
								.toMinutes(milliseconds)));
		return eTime;
		
	}
	
	
	
	public static void checkForUploadsFolder(String contextRoot){
		//Changed by sriram to support cross-platform file path separator
		String folderPath = contextRoot + File.separator + "uploads";
		File dir = new File(folderPath);
		if(!dir.exists()){
			dir.mkdir();
		}else{
			
		}
	}
	
	
	/****************
	 * Fix by Sriram to create report path
	 * *************************/
	
	public static String createSessionReportFolder(String contextRoot, String userId){
		String folderPath = "";
		folderPath = contextRoot + "\\userdata";
		File dir = new File(folderPath);
		if(!dir.exists()){
			dir.mkdir();
		}
		
		folderPath = folderPath + "\\" + userId;
		dir = new File(folderPath);
		if(!dir.exists()){
			dir.mkdir();
		}
		
		folderPath = folderPath + "\\reports";
		dir = new File(folderPath);
		if(!dir.exists()){
			dir.mkdir();
		}
		
		return folderPath;
	}
	/****************
	 * Fix by Sriram to create report path ends
	 * *************************/
	
	
	public static String createSessionSnapshotFolder(String contextRoot, String userId){
		String folderPath = "";
		folderPath = contextRoot + "\\userdata";
		File dir = new File(folderPath);
		if(!dir.exists()){
			dir.mkdir();
		}
		
		folderPath = folderPath + "\\" + userId;
		dir = new File(folderPath);
		if(!dir.exists()){
			dir.mkdir();
		}
		
		folderPath = folderPath + "\\snapshots";
		dir = new File(folderPath);
		if(!dir.exists()){
			dir.mkdir();
		}
		
		return folderPath;
	}
	
	public static String getDetailedTimeStamp(){
		SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy hh:mm:ss a");
		String date = sdf.format(new Date());
		return date;
	}
	
	/************************
	 * Method added by Sriram to get Raw timestamp like this: 20121224133012
	 */
	public static String getRawTimeStamp(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		String date = sdf.format(new Date());
		return date;
	}
	/************************
	 * Method added by Sriram to get Raw timestamp like this: 20121224133012 ends
	 */
	
		
	public String xmlTest(){
		String xml = "";
		//DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		try {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl", ClassLoader.getSystemClassLoader());
		/*Added by Paneendra for VAPT fix starts*/
		docFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		docFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		//docFactory.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, "true");
		docFactory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
		/*Added by Paneendra for VAPT fix ends*/
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("company");
			doc.appendChild(rootElement);
			
			Element staff = doc.createElement("Staff");
			rootElement.appendChild(staff);
	 
			// set attribute to staff element
			Attr attr = doc.createAttribute("id");
			attr.setValue("1");
			staff.setAttributeNode(attr);
	 
			// shorten way
			// staff.setAttribute("id", "1");
	 
			// firstname elements
			Element firstname = doc.createElement("firstname");
			firstname.appendChild(doc.createTextNode("yong"));
			staff.appendChild(firstname);
	 
			// lastname elements
			Element lastname = doc.createElement("lastname");
			lastname.appendChild(doc.createTextNode("mook kim"));
			staff.appendChild(lastname);
	 
			// nickname elements
			Element nickname = doc.createElement("nickname");
			nickname.appendChild(doc.createTextNode("mkyong"));
			staff.appendChild(nickname);
	 
			// salary elements
			Element salary = doc.createElement("salary");
			salary.appendChild(doc.createTextNode("100000"));
			staff.appendChild(salary);
			
			
	 
		} catch (ParserConfigurationException e) {
			
			logger.error("Error ", e);
		}
		
		return xml;
	}
	
	//Uncommented by Sriram for CD/CI demo
	public static JSONObject stringToJSON(String stringToConvert){
		JSONObject j = new JSONObject();
		
		String s = stringToConvert;
		try{
			s = s.replace("{", "");
			s = s.replace("}", "");
			
			String[] split = s.split(",");
			
			for(String st:split){
				String[] d = st.split(":");
				if(d.length > 1){
					j.put(d[0], d[1]);
				}
			}
		}catch(Exception e){
			System.err.println("Could not convert string ot json");
			System.err.print(e.getMessage());
			logger.error("Error ", e);
		}
		return j;
	}
	
	//comment
	
	//TENJINCG-876 - Sriram
	public static boolean isJson(String str) {
		str = trim(str);
        try {
            new JSONObject(str);
        } catch (JSONException ex) {
            try {
                new JSONArray(str);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
	
	public static boolean isXML(String str) {
		str = trim(str);
		
		if(str.startsWith("<"))
			return true;
		else
			return false;
	}//TENJINCG-876 - Sriram ends
	
	public static String escapeHtml(String str) {
		return StringEscapeUtils.escapeHtml4(StringUtils.trim(str));
	}
	
	//Added for Tenjincg-1166 by Roshni starts
	public static String escapeXml(String str) {
		return StringEscapeUtils.escapeXml11(StringUtils.trim(str));
	}
	
	public static String sanitizeString(String str){
		return Utilities.escapeHtml(Utilities.escapeXml(str));
	}
	//Added for Tenjincg-1166 by Roshni ends
	
}
