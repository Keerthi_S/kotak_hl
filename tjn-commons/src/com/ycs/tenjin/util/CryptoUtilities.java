/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Utilities.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Sep 1, 2016           Sriram Sridharan          Newly Added For 
 */

package com.ycs.tenjin.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;



public class CryptoUtilities {

	

	private static final Logger logger = LoggerFactory.getLogger(CryptoUtilities.class);
	public  Key readPublicKey() throws IOException {
		File file = new File(CryptoUtilities.class.getClassLoader().getResource("resources/public.key").getFile().replace("%20", " "));
		Key key = null;
		InputStream inputStream = new FileInputStream(file);
		ObjectInputStream objectInputStream = new ObjectInputStream(new BufferedInputStream(inputStream));
		try {
			BigInteger modulus = (BigInteger) objectInputStream.readObject();
			BigInteger exponent = (BigInteger) objectInputStream.readObject();
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			key = keyFactory.generatePublic(new RSAPublicKeySpec(modulus, exponent));
		} catch (Exception e) {
			logger.error("Error loading public key", e);
		} finally {
			objectInputStream.close();
		}
		return key;
	}

	public  Key readPrivateKey() throws IOException {
		File file = new File(CryptoUtilities.class.getClassLoader().getResource("resources/private.key").getFile().replace("%20", " "));
		Key key = null;
		InputStream inputStream = new FileInputStream(file);
		ObjectInputStream objectInputStream = new ObjectInputStream(new BufferedInputStream(inputStream));
		try {
			BigInteger modulus = (BigInteger) objectInputStream.readObject();
			BigInteger exponent = (BigInteger) objectInputStream.readObject();
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			key = keyFactory.generatePrivate(new RSAPrivateKeySpec(modulus, exponent));

		} catch (Exception e) {
			logger.error("Error loading private key", e);
		} finally {
			objectInputStream.close();
		}
		return key;
	}
	
	public  String encrypt(String plainText) throws TenjinConfigurationException {
		try {
			byte[] cipherTextArray = encryptAsBytes(plainText);
			return Base64.getEncoder().encodeToString(cipherTextArray);
		} catch (Exception e) {
			throw new TenjinConfigurationException("Could not encrypt plain text", e);
		}
	}

	public  byte[] encryptAsBytes(String plainText)  throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		Key publicKey = readPublicKey();
		Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-512ANDMGF1PADDING");
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		return cipher.doFinal(plainText.getBytes());

	}
	
	public  String decrypt(String encryptedText) throws TenjinConfigurationException {
		try {
			return decrypt(Base64.getDecoder().decode(encryptedText));
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | IOException e) {
			throw new TenjinConfigurationException("Could not decrypt plain text", e);
		}		
	}

	public  String decrypt(byte[] cipherTextArray) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Key privateKey = readPrivateKey();
		Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-512ANDMGF1PADDING");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		byte[] decryptedTextArray = cipher.doFinal(cipherTextArray);
		return new String(decryptedTextArray);
	}
	
}
