/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  PatternMatch.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 09-Aug-2018          Sameer Gupta	        Newly Added For Pattern Matching(TENJINCG-783)
 *   
 */

package com.ycs.tenjin.util;

public class PatternMatch {


	public boolean matchString(String stringToMatch, String pattern) {
		return matchString(stringToMatch, pattern, false);
	}

	public boolean matchString(String stringToMatch, String pattern,
			boolean ignoreCase) {

		if (pattern.equals("%")) {
			return true;
		}
		if (ignoreCase) {
			stringToMatch = stringToMatch.toLowerCase();
			pattern = pattern.toLowerCase();
		}

		String[] arrayPattern = pattern.split("%");

		if (pattern.endsWith("%")) {
			arrayPattern = addEmptyElementToArray(arrayPattern);
		}

		boolean result = true;

		for (int i = 0; i < arrayPattern.length; i++) {

			String currentPatter = arrayPattern[i];

			if (i != arrayPattern.length - 1) {

				if (stringToMatch.startsWith(currentPatter)) {
					currentPatter = currentPatter.replace("[", "\\[");
					currentPatter = currentPatter.replace("]", "\\]");
					stringToMatch = stringToMatch.replaceFirst(currentPatter,
							"");

					String nextPattern = arrayPattern[i + 1];
					if (!nextPattern.equals("")) {
						if (stringToMatch.contains(nextPattern)) {
							int indexOfPattern = stringToMatch
									.indexOf(nextPattern);
							stringToMatch = stringToMatch
									.substring(indexOfPattern);
						} else {
							result = false;
							break;
						}
					}

				} else {
					result = false;
					break;
				}
			} else {
				if (!pattern.endsWith("%")) {
					if (!stringToMatch.equals(currentPatter)) {
						result = false;
						break;
					}
				}
			}

		}

		return result;

	}

	private String[] addEmptyElementToArray(String[] arrayPattern) {
		int length = arrayPattern.length;
		String[] tempArray = new String[length + 1];

		for (int i = 0; i < arrayPattern.length; i++) {
			tempArray[i] = arrayPattern[i];
		}
		tempArray[length] = "";

		arrayPattern = tempArray;
		return arrayPattern;
	}
}
