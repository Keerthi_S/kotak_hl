/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DefectManager.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 15-03-2019		   Pushpalatha			   TENJINCG-956
* 15-03-2019			Ashiki				   TENJINCG-986
*/



package com.ycs.tenjin.defect;

public class DefectManagementInstance {
	
	private String name;
	private String tool;
	private String URL;
	private String adminId;
	private String password;
	private String severity;
	private String instance;
	private String defToolUserId;
	private String defToolPwd;
	private String tjnUserId;
	private String priority;
	private String status;
	/*Added by Pushpalatha for TENJINCG-1220 starts*/
	private String organization;
	
	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}
	/*Added by Pushpalatha for TENJINCG-1220 ends*/
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	/**
	 * @return the tjnUserId
	 */
	public String getTjnUserId() {
		return tjnUserId;
	}

	/**
	 * @param tjnUserId the tjnUserId to set
	 */
	public void setTjnUserId(String tjnUserId) {
		this.tjnUserId = tjnUserId;
	}

	/**
	 * @return the instance
	 */
	public String getInstance() {
		return instance;
	}

	/**
	 * @param instance the instance to set
	 */
	public void setInstance(String instance) {
		this.instance = instance;
	}

	/**
	 * @return the defToolUserId
	 */
	public String getDefToolUserId() {
		return defToolUserId;
	}

	/**
	 * @param defToolUserId the defToolUserId to set
	 */
	public void setDefToolUserId(String defToolUserId) {
		this.defToolUserId = defToolUserId;
	}

	/**
	 * @return the defToolPwd
	 */
	public String getDefToolPwd() {
		return defToolPwd;
	}

	/**
	 * @param defToolPwd the defToolPwd to set
	 */
	public void setDefToolPwd(String defToolPwd) {
		this.defToolPwd = defToolPwd;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTool() {
		return tool;
	}

	public void setTool(String tool) {
		this.tool = tool;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	private String subSetName;
	private int prjId;
	private String projects;
	private String userId;
	
	public String getSubSetName() {
		return subSetName;
	}

	public void setSubSetName(String subSetName) {
		this.subSetName = subSetName;
	}

	public int getPrjId() {
		return prjId;
	}

	public void setPrjId(int prjId) {
		this.prjId = prjId;
	}

	public String getProjects() {
		return projects;
	}

	public void setProjects(String projects) {
		this.projects = projects;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	

}
