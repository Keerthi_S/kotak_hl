/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RegisteredClient.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                  CHANGED BY              DESCRIPTION
 * 19-Aug-2017			sriram					T25IT-119
 * 19-Oct-2017			sriram					TENJINCG-397
 * 08-11-2017			Preeti Singh			TENJINCG-410
 * 17-11-2017			Preeti Singh			TENJINCG-477
 * 28-02-2019           Leelaprasad P           TENJINCG-983
 * 08-03-2019			Sahana					pCloudy
 */

/* New RegisteredClient java Class Created By Parveen For The requirment TJN_23_22 */

package com.ycs.tenjin.client;


import java.util.Comparator;
import java.util.Date;

import com.ycs.tenjin.util.Utilities;

public class RegisteredClient {
	
	private String name;
	private String port;
	private String hostName;
	private String status;
	

	public static final Comparator<RegisteredClient> clientComp = new Comparator<RegisteredClient>() {
		public int compare(RegisteredClient o1,RegisteredClient o2) {
			String s1 = o1.getName();
			   String s2 = o2.getName();
			   return s1.compareTo(s2);
		}
		};
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public void merge(RegisteredClient newClient) {
		
		this.name = newClient.getName() != null ? Utilities.trim(newClient.getName()) : this.name;
		this.hostName = newClient.getHostName() !=null ? Utilities.trim(newClient.getHostName()) : this.hostName;
		this.port = newClient.getPort() !=null ? Utilities.trim(newClient.getPort()) : this.port;
		this.osType=newClient.getPort() !=null ? Utilities.trim(newClient.getOsType()) : this.osType;

		
		this.deviceFarmCheck = newClient.getDeviceFarmCheck()!= null ? Utilities.trim(newClient.getDeviceFarmCheck()) : this.deviceFarmCheck;
		this.deviceFarmUsrName = newClient.getDeviceFarmUsrName()!=null ? Utilities.trim(newClient.getDeviceFarmUsrName()) : this.deviceFarmUsrName;
		this.deviceFarmPwd = newClient.getDeviceFarmPwd() !=null ? Utilities.trim(newClient.getDeviceFarmPwd()) : this.deviceFarmPwd;
		this.deviceFarmKey = newClient.getDeviceFarmKey() !=null ? Utilities.trim(newClient.getDeviceFarmKey()) : this.deviceFarmKey;
		

		
	}

	private int recordId;
	private String createdBy;
	private Date createdOn;
	public int getRecordId() {
		return recordId;
	}
	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	private String osType;
	public String getOsType() {
		return osType;
	}
	public void setOsType(String osType) {
		this.osType = osType;
	}
	
	private String deviceFarmCheck;
	private String deviceFarmUsrName;
	private String deviceFarmPwd;
	private String deviceFarmKey;

	public String getDeviceFarmCheck() {
		return deviceFarmCheck;
	}
	public void setDeviceFarmCheck(String deviceFarmCheck) {
		this.deviceFarmCheck = deviceFarmCheck;
	}
	public String getDeviceFarmUsrName() {
		return deviceFarmUsrName;
	}
	public void setDeviceFarmUsrName(String deviceFarmUsrName) {
		this.deviceFarmUsrName = deviceFarmUsrName;
	}
	public String getDeviceFarmPwd() {
		return deviceFarmPwd;
	}
	public void setDeviceFarmPwd(String deviceFarmPwd) {
		this.deviceFarmPwd = deviceFarmPwd;
	}
	public String getDeviceFarmKey() {
		return deviceFarmKey;
	}
	public void setDeviceFarmKey(String deviceFarmKey) {
		this.deviceFarmKey = deviceFarmKey;
	}
	
}
