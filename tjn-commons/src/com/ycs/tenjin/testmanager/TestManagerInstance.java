/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestManagerInstance.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
24-01-2017			Manish						TENJINCG-54
15-03-2019			Pushpalatha					TENJINCG-956

 */

package com.ycs.tenjin.testmanager;

public class TestManagerInstance {
	
	private String instanceName;
	private String URL;
	private String password;
	private String tool;
	private String username;
	private String tjnUsername;
	private String tmProjectDomain;
	private String tmTcId;
	private String tmTcType;
	private String tmAppId;
	private String tmFuncCode;
	private String tmOperation;
	private String tmAutLoginType;
	private String tmStepType;
	private String tjnField;
	private String tmField;
	/*Added by Pushpalatha for TENJINCG-956 starts*/
	public String getTmStatus() {
		return tmStatus;
	}
	public void setTmStatus(String tmStatus) {
		this.tmStatus = tmStatus;
	}
	/*Added by Pushpalatha for TENJINCG-956 ends*/
	int tmRecId;
	private String tmStatus;


	public int getTmRecId() {
		return tmRecId;
	}
	public void setTmRecId(int tmRecId) {
		this.tmRecId = tmRecId;
	}
	public String getTjnField() {
		return tjnField;
	}
	public void setTjnField(String tjnField) {
		this.tjnField = tjnField;
	}
	public String getTmField() {
		return tmField;
	}
	public void setTmField(String tmField) {
		this.tmField = tmField;
	}
	public String getRec_type() {
		return rec_type;
	}
	public void setRec_type(String rec_type) {
		this.rec_type = rec_type;
	}
	private String rec_type;
	
	
	public String getTmTcId() {
		return tmTcId;
	}
	public void setTmTcId(String tmTcId) {
		this.tmTcId = tmTcId;
	}
	public String getTmTcType() {
		return tmTcType;
	}
	public void setTmTcType(String tmTcType) {
		this.tmTcType = tmTcType;
	}
	public String getTmAppId() {
		return tmAppId;
	}
	public void setTmAppId(String tmAppId) {
		this.tmAppId = tmAppId;
	}
	public String getTmFuncCode() {
		return tmFuncCode;
	}
	public void setTmFuncCode(String tmFuncCode) {
		this.tmFuncCode = tmFuncCode;
	}
	public String getTmOperation() {
		return tmOperation;
	}
	public void setTmOperation(String tmOperation) {
		this.tmOperation = tmOperation;
	}
	public String getTmAutLoginType() {
		return tmAutLoginType;
	}
	public void setTmAutLoginType(String tmAutLoginType) {
		this.tmAutLoginType = tmAutLoginType;
	}
	public String getTmStepType() {
		return tmStepType;
	}
	public void setTmStepType(String tmStepType) {
		this.tmStepType = tmStepType;
	}
	public String getTmProjectDomain() {
		return tmProjectDomain;
	}
	public void setTmProjectDomain(String tmProjectDomain) {
		this.tmProjectDomain = tmProjectDomain;
	}
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTool() {
		return tool;
	}
	public void setTool(String tool) {
		this.tool = tool;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTjnUsername() {
		return tjnUsername;
	}
	public void setTjnUsername(String tjnUsername) {
		this.tjnUsername = tjnUsername;
	}
	
	

}
