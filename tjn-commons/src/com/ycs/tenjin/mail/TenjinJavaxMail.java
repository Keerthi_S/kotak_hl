/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: TenjinJavaxMail.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 23-01-2018			    Padmavathi		        Newly added for TENJINCG-579
 05-02-2018             Padmavathi              for attaching pdf in mail
 20-02-2018             Padmavathi              for checking duplicate recipientMails
 21-02-2018             Padmavathi              for checking duplicate recipientMails  
 01-03-2018             Padmavathi              for displaying correct exception messages.
 13-03-2018				Preeti					TENJINCG-615
 02-05-2018				Preeti					TENJINCG-656
 03-05-2018            	Leelaprasad             TENJINCG-639
 14-05-2018				Pushpalatha				TENJINCG-629
 18-06-2018				Preeti					T251IT-63
 15-03-2019             padmavathi              TENJINCG-948
 22-03-2019             Padmavathi              TENJINCG-1017
 18-04-2019             Padmavathi              TNJNR2-4
 02-05-2019				Roshni					TENJINCG-1046
 06-05-2019             Padmavathi              TENJINCG-1050
 06-05-2019				Roshni					TENJINCG-1036
 07-05-2019				Ashiki					TENJINCG-1051
 08-05-2019				Preeti					TENJINCG-1053
 04-07-2019             Padmavathi              TV2.8R2-25,26,29,23
 12-03-2020				Lokanath				TENJINCG-1185
 17-03-2020				Lokanath				TENJINCG-1184
 * */




package com.ycs.tenjin.mail;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.AuthenticationFailedException;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ParametersHelper;
import com.ycs.tenjin.util.Utilities;


public class TenjinJavaxMail implements Mailer{
	private static final Logger logger = LoggerFactory.getLogger(TenjinJavaxMail.class);
	
	private int runID;
	private String senderAliasName=null;
	
	@Override
	public void sendEmail(Multipart multipart,TenjinMail mail) throws TenjinConfigurationException, UnsupportedEncodingException {
		String[] recipientMails =null;
		String[] ccMails=null;
		
			try{
			   Properties properties = System.getProperties();
			   logger.info("setting properties");
			   properties.put("mail.smtp.starttls.enable", "true");
			   properties.put("mail.smtp.host", mail.getSmtpServer());
			   properties.put("mail.smtp.password", mail.getPassword());
			   properties.put("mail.smtp.port", mail.getPort());
			   properties.put("mail.smtp.auth", mail.getAuthentication());
			   properties.put("mail.smtp.ssl.trust", mail.getSmtpServer());
			   logger.info("setting properties are completed");
			   
			   logger.info("initializing session object");
			   SMTPAuthenticator authenticator = null;
			   if(Utilities.trim(mail.getSenderName()).length()>0){
				    authenticator = new SMTPAuthenticator(mail.getSenderName(), mail.getPassword());
			   }else{
				    authenticator = new SMTPAuthenticator(mail.getSenderMail(), mail.getPassword());
			   }
			   properties.put("mail.smtp.submitter", authenticator.getPasswordAuthentication().getUserName());
			   Session session = Session.getInstance(properties, authenticator);
			   
			   logger.info("initializing Mimemessage object");
			   Message message = new MimeMessage(session);
			   logger.info("Setting sender and receiver mail");
			   if(senderAliasName!=null)
				   message.setFrom(new InternetAddress(mail.getSenderMail(),senderAliasName));
			   else
				   message.setFrom(new InternetAddress(mail.getSenderMail())); 
			   recipientMails =this.checkDuplicateUsersMails(mail.getRecipientMails());
			   if(mail.getCcMails()!=null && !mail.getCcMails().equals("")){
				   ccMails=this.checkDuplicateUsersMails(mail.getCcMails());
				   recipientMails=this.getRecipients(recipientMails,ccMails);
				  
 				   InternetAddress[] ccRecipientAddress = new InternetAddress[ccMails.length];
			   int counter1=0;
			   for (String ccRecipient : ccMails) {
				   ccRecipientAddress[counter1] = new InternetAddress(ccRecipient.trim());
			       counter1++;
			   }
			   message.addRecipients(Message.RecipientType.CC, ccRecipientAddress);
			   }
			   
			   InternetAddress[] recipientAddress = new InternetAddress[recipientMails.length];
			   int counter = 0;
			   for (String recipient : recipientMails) {
			       recipientAddress[counter] = new InternetAddress(recipient.trim());
			       counter++;
			   }
			   message.setRecipients(Message.RecipientType.TO, recipientAddress);
			   logger.info("Setting text and subject line");
			   message.setSubject(mail.getSubject());
			   message.setContent(multipart,"text/html; charset=utf-8");
			   
			   logger.info("initializing Transport object");
			   Transport transport = session.getTransport("smtp");
			    logger.info("connecting to the mail service...");
			   transport.connect(mail.getSmtpServer(), mail.getSenderMail(), mail.getPassword());
			   logger.info("sending mail...");
			   transport.sendMessage(message, message.getAllRecipients());
			   transport.close();
			   this.persistMailLogs(runID, mail.getSenderMail(),Arrays.toString(recipientMails), "Pass", Arrays.toString(ccMails));
		      logger.info("Sent mail successfully.");
		   	}catch (AuthenticationFailedException mex) {
		   		this.persistMailLogs(runID, mail.getSenderMail(),Arrays.toString(recipientMails), "Fail", Arrays.toString(ccMails));
		   		logger.error("Incorrect username or password.");
		     throw new TenjinConfigurationException("Incorrect userID or password.");
		   } catch (javax.mail.SendFailedException mex) {
			   this.persistMailLogs(runID, mail.getSenderMail(),Arrays.toString(recipientMails), "Fail", Arrays.toString(ccMails));
		   		logger.error("Incorrect Recipient E-mail.");
		     throw new TenjinConfigurationException("Incorrect Recipient E-mail.");
		   }catch (javax.mail.MessagingException mex) {
			   this.persistMailLogs(runID, mail.getSenderMail(),Arrays.toString(recipientMails), "Fail", Arrays.toString(ccMails));
			   if(mex.getMessage().contains("Could not connect to SMTP host")){
				   logger.error(mex.getMessage());
				     throw new TenjinConfigurationException("Invalid port.");
			   }else if(mex.getMessage().contains("Unknown SMTP host")){
				   logger.error(mex.getMessage());
				     throw new TenjinConfigurationException("Invalid SMTP Server.");
			   }
			   else{
				   logger.error(mex.getMessage());
				   throw new TenjinConfigurationException("An internal error occurred. Please contact Tenjin Support.");
			   }
		   		
		   }
		}
	private String[] getRecipients(String[] recipientMails, String[] ccMails) {
		List<String> recMails = new ArrayList<String>(Arrays.asList(recipientMails));
		List<String> ccRecMails = new ArrayList<String>(Arrays.asList(ccMails));
		
		recMails.removeAll(ccRecMails);
		String [] recMail = recMails.toArray(new String[recMails.size()]);
		return recMail;
	}
	@SuppressWarnings("unchecked")
	public String buildTenjinEscalationMails(JSONObject jsonProject,JSONObject jsonRun,JSONObject jsonMail,JSONArray jsonExecHistory) throws JSONException, JsonParseException, JsonMappingException, IOException{
		String escMailIds="";
		JSONObject jsonSet=jsonRun.getJSONObject("testSet");
		JSONArray jsonCase=jsonSet.getJSONArray("tests");
		
		JSONObject jsonStep=null;
		float totalExecutedStep=0;
		float totalErroredStep=0;
		float totalFailedStep=0;
		String tsId = jsonSet.getString("id");
		ArrayList<String> autIds = new ArrayList<String>();
		JSONObject step=null;
		for(int i=0;i<jsonCase.length();i++){
			jsonStep=jsonCase.getJSONObject(i);
			totalExecutedStep=totalExecutedStep+Float.parseFloat(jsonStep.getString("totalExecutedSteps"));
			totalErroredStep=totalErroredStep+Float.parseFloat(jsonStep.getString("totalErrorredSteps"));
			totalFailedStep=totalFailedStep+Float.parseFloat(jsonStep.getString("totalFailedSteps"));
			JSONArray jsonSteps = jsonStep.getJSONArray("tcSteps");
			for(int j=0;j<jsonSteps.length();j++){
				step=jsonSteps.getJSONObject(j);
				autIds.add(step.getString("appId"));
			}
	}
		float failedPercentage=(totalFailedStep/totalExecutedStep)*100;
		float ErroredPercentage=(totalErroredStep/totalExecutedStep)*100;
		String ruleDesc=jsonMail.getString("prjEscalationRuleDesc");
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> descMap = mapper.readValue(ruleDesc, Map.class);
		
		String mailIds = jsonMail.getString("prjEscalationMails");
		ObjectMapper mapper1 = new ObjectMapper();
		Map<String, String> idsMap = mapper1.readValue(mailIds, Map.class);
		 if(descMap.containsKey("101")){
			if(failedPercentage>Integer.parseInt(descMap.get("101"))){
				escMailIds = escMailIds + "," + idsMap.get("101");
			}
		} if(descMap.containsKey("102")){
			if(ErroredPercentage>Integer.parseInt(descMap.get("102"))){
				escMailIds = escMailIds + "," + idsMap.get("102");
			}
		}
		 if(descMap.containsKey("103")){
			if(failedPercentage>Integer.parseInt(descMap.get("103"))||ErroredPercentage>Integer.parseInt(descMap.get("103"))){
				escMailIds = escMailIds + "," + idsMap.get("103");
			}
		} if(descMap.containsKey("104")){
			int errorRun=0;
			int failedRun=0;
			String status="";
			
			for(int i=0; i<Integer.parseInt(descMap.get("104")); i++){
				if(i<jsonExecHistory.length()){
				JSONObject objects = jsonExecHistory.optJSONObject(i);
				status=objects.getString("status");
				if(status.equalsIgnoreCase("Error")){
					errorRun++;
				}
				if(status.equalsIgnoreCase("Fail")){
					failedRun++;
				}
				}
			}
			
			if(failedRun>Integer.parseInt(descMap.get("104"))||errorRun>Integer.parseInt(descMap.get("104"))){
				escMailIds = escMailIds + "," + idsMap.get("104");
			}
			
		}
		if(descMap.containsKey("105")){
			if(descMap.get("105").equalsIgnoreCase(tsId)){
				String status=jsonRun.getString("status");
				if(status.equalsIgnoreCase("Error") || status.equalsIgnoreCase("Fail"))
					escMailIds = escMailIds + "," + idsMap.get("105");
			}
		}
		if(descMap.containsKey("106")){
			if(autIds.contains(descMap.get("106"))){
				String status=jsonRun.getString("status");
				if(status.equalsIgnoreCase("Error") || status.equalsIgnoreCase("Fail"))
					escMailIds = escMailIds + "," + idsMap.get("106");
			}
		}
		escMailIds = escMailIds.substring(1);
		return escMailIds;
		
	}
	public void buildMailInformation(int runId,int projectId) {
		/*Added by lokanath for TENJINCG-1184 starts*/
		boolean sendMailStatus=false;
		TenjinReflection tenjinReflection;
		JSONObject jsonMail;
		String mailType="";
		try {
			tenjinReflection = new TenjinReflection();
			TenjinMail mail=new TenjinMail();
			String projectMail=tenjinReflection.getProjectMail(projectId);
			jsonMail = new JSONObject(projectMail);
			 mailType=jsonMail.getString("prjMailtype");
			 String runTypeStatus=tenjinReflection.getRunIdType(runId);
			 if(mailType.toLowerCase().contains("schedule run")&&runTypeStatus.equals("true")){
				 sendMailStatus=true;
			 }
			 if(mailType.toLowerCase().contains("mail to all")){
				 sendMailStatus=true;
			 }
			 if(sendMailStatus){
				 String project=tenjinReflection.getProject(projectId);
					String users="";
					JSONObject jsonProject=new JSONObject(project);
					String runs =tenjinReflection.getRunJson(runId, projectId);
					JSONObject jsonRun = new JSONObject(runs);
					int setId=Integer.parseInt(jsonRun.getString("testSetRecordId"));
					
					String exexutinHist=tenjinReflection.getExecuitionHistoryJson(projectId,setId );
					JSONArray jsonExexutinHist = new JSONArray(exexutinHist);
					if(jsonMail.getString("prjMailNotification").equalsIgnoreCase("Y")){
						//getting project users
						JSONArray array=jsonProject.getJSONArray("users");
						runID=runId;
						/*Added by lokanath for TENJINCG-1185 starts*/
						String additionalMails="";
						try {
							additionalMails = jsonMail.getString("prjAddAdditionalMails");
						} catch (JSONException e ) {
							
							logger.error("Error ", e);
						}
						/*Added by lokanath for TENJINCG-1185 ends*/
						/*Modified by lokanath for TENJINCG-1185 starts*/
						users=this.getPrjUserTosendMail(array,projectId,tenjinReflection,jsonRun,additionalMails);
						/*Modified by lokanath for TENJINCG-1185 ends*/
						mail.setRecipientMails(users);
						try {
							if(jsonMail.has("prjMailEscalation")&&jsonMail.has("prjEscalationRuleDesc")){
							String ccMails=this.buildTenjinEscalationMails(jsonProject,jsonRun,jsonMail,jsonExexutinHist);
							mail.setCcMails(ccMails);
							}
						}
						catch (Exception e) {
							logger.error("Error ", e);
						}
						
						HashMap<String, String> hashMap = new EmailHandler().hydrateEmailConfiguration();
						if(hashMap.size()>0){
							mail.setSmtpServer(hashMap.get("smtpServer"));
							mail.setPassword(hashMap.get("password"));
							mail.setSenderMail(hashMap.get("userId"));
							mail.setSenderName(hashMap.get("userName"));
							mail.setAuthentication(hashMap.get("authentication"));
							if(hashMap.get("port")!=null){
								mail.setPort(Integer.parseInt(hashMap.get("port")));
							}
							
							String mailSubject=buildMailSubject(jsonProject,jsonRun,jsonMail);
							mail.setSubject(mailSubject);
							String runJson=	tenjinReflection.getRunJson(runId,projectId);
							senderAliasName=new JSONObject(runJson).getString("user");
							String context=new TenjinMailcontext().contextBuilder(runJson);
							 String workPath = TenjinConfiguration.getProperty("MEDIA_REPO_PATH");
							String fileName=this.fetchReport(runId, projectId,runJson,workPath);
						     Multipart multipart = new MimeMultipart();
						     BodyPart testExecutiontbody = new MimeBodyPart();
						     BodyPart reportBody = new MimeBodyPart();
						     testExecutiontbody.setContent(context,"text/html; charset=utf-8");
						     multipart.addBodyPart(testExecutiontbody);
						     FileDataSource source = new FileDataSource(workPath + fileName);
						     reportBody.setDataHandler(new DataHandler(source));
						     reportBody.setFileName(source.getName());
						     multipart.addBodyPart(reportBody);
						     
						     this.sendEmail(multipart, mail);
						}else{
							logger.info("email notification not configured");
						}
					}else{
						logger.info("email notification is N");
					}
					
			 }
		} catch (Exception e1) {
			logger.error("Error ", e1);
		}
	     /*Added by lokanath for TENJINCG-1184 ends*/   
		
		
		/*try{
		logger.info("checking email notification");
		//commented by lokanath
			//TenjinMail mail=new TenjinMail();
		
		//	TenjinReflection tenjinReflection =new TenjinReflection();
			String project=tenjinReflection.getProject(projectId);
			String users="";
			JSONObject jsonProject=new JSONObject(project);
			String runs =tenjinReflection.getRunJson(runId, projectId);
			JSONObject jsonRun = new JSONObject(runs);
			int setId=Integer.parseInt(jsonRun.getString("testSetRecordId"));
			
			String exexutinHist=tenjinReflection.getExecuitionHistoryJson(projectId,setId );
			JSONArray jsonExexutinHist = new JSONArray(exexutinHist);
			//commented by lokanath
			//getting mail id's for project preference
			//String projectMail=tenjinReflection.getProjectMail(projectId);
			//JSONObject jsonMail = new JSONObject(projectMail);
			
			if(jsonMail.getString("prjMailNotification").equalsIgnoreCase("Y")){
				//getting project users
				JSONArray array=jsonProject.getJSONArray("users");
				runID=runId;
				Added by lokanath for TENJINCG-1185 starts
				String additionalMails=jsonMail.getString("prjAddAdditionalMails");
				Added by lokanath for TENJINCG-1185 ends
				Modified by lokanath for TENJINCG-1185 starts
				users=this.getPrjUserTosendMail(array,projectId,tenjinReflection,jsonRun,additionalMails);
				Modified by lokanath for TENJINCG-1185 ends
				mail.setRecipientMails(users);
				try {
					if(jsonMail.has("prjMailEscalation")&&jsonMail.has("prjEscalationRuleDesc")){
					String ccMails=this.buildTenjinEscalationMails(jsonProject,jsonRun,jsonMail,jsonExexutinHist);
					mail.setCcMails(ccMails);
					}
				}
				catch (Exception e) {
					
					logger.error("Error ", e);
				}
				
				HashMap<String, String> hashMap = new EmailHandler().hydrateEmailConfiguration();
				if(hashMap.size()>0){
					mail.setSmtpServer(hashMap.get("smtpServer"));
					mail.setPassword(hashMap.get("password"));
					mail.setSenderMail(hashMap.get("userId"));
					mail.setSenderName(hashMap.get("userName"));
					mail.setAuthentication(hashMap.get("authentication"));
					if(hashMap.get("port")!=null){
						mail.setPort(Integer.parseInt(hashMap.get("port")));
					}
					
					String mailSubject=buildMailSubject(jsonProject,jsonRun,jsonMail);
					mail.setSubject(mailSubject);
					String runJson=	tenjinReflection.getRunJson(runId,projectId);
					senderAliasName=new JSONObject(runJson).getString("user");
					String context=new TenjinMailcontext().contextBuilder(runJson);
					 String workPath = TenjinConfiguration.getProperty("MEDIA_REPO_PATH");
					String fileName=this.fetchReport(runId, projectId,runJson,workPath);
				     Multipart multipart = new MimeMultipart();
				     BodyPart testExecutiontbody = new MimeBodyPart();
				     BodyPart reportBody = new MimeBodyPart();
				     testExecutiontbody.setContent(context,"text/html; charset=utf-8");
				     multipart.addBodyPart(testExecutiontbody);
				     FileDataSource source = new FileDataSource(workPath + fileName);
				     reportBody.setDataHandler(new DataHandler(source));
				     reportBody.setFileName(source.getName());
				     multipart.addBodyPart(reportBody);
				     
				     this.sendEmail(multipart, mail);
				}else{
					logger.info("email notification not configured");
				}
			}else{
				logger.info("email notification is N");
			}
			
		}catch(Exception e){
			logger.error(e.getMessage());
		}*/
	}
	
	public String buildMailSubject(JSONObject jsonProject, JSONObject jsonRun, JSONObject jsonMail)
	{
		String keys="";
		String subjectMail="";
		try{
			if(jsonMail.has("prjMailSubject"))
			{
			keys = jsonMail.getString("prjMailSubject");
				subjectMail = keys;
				subjectMail = removePunctuations(keys);
				String[] subject=subjectMail.split(" ");
				for(int i=0; i<subject.length; i++){
					if(subject[i].startsWith("$") && subject[i].endsWith("$")){
						if(subject[i].equalsIgnoreCase("$domain_name$")){
							keys=keys.replace(subject[i], jsonProject.getString("domain"));
						}
						if(subject[i].equalsIgnoreCase("$project_name$")){
							keys=keys.replace(subject[i], jsonProject.getString("name"));
						}
						if(subject[i].equalsIgnoreCase("$run_id$")){
							keys=keys.replace(subject[i], jsonRun.getString("id"));
						}
						if(subject[i].equalsIgnoreCase("$run_status$")){
							keys=keys.replace(subject[i], jsonRun.getString("status"));
						}
						if(subject[i].equalsIgnoreCase("$user_name$")){
							keys=keys.replace(subject[i], jsonRun.getString("user"));
						}
						if(subject[i].equalsIgnoreCase("$test_set$")){
							keys=keys.replace(subject[i], (String) jsonRun.getJSONObject("testSet").get("name"));
						}
					}
				}
			}
			else
				keys="Execution report for Run Id : "+ jsonRun.getString("id");
		}
		catch(Exception e){
			logger.error("Error ", e);
		}
		return keys;
	}
	
	public String removePunctuations(String s) {
	    String res = "";
	    for (Character c : s.toCharArray()) {
	        if(Character.isLetterOrDigit(c) || Character.isWhitespace(c) || c=='$' || c=='_')
	            res += c;
	        else
	        	res+=' ';
	    }
	    return res;
	}
	
	public String fetchReport(int runId,int projectId,String runJson,String workPath ) throws TenjinConfigurationException, JSONException{
		String fileName=new TenjinReflection().GetReportPath(runId, projectId, workPath,runJson);
		fileName=fileName.substring(1,fileName.length()-1);
		return fileName;
		
	
	}
	private String[] checkDuplicateUsersMails(String usermails){
		String[] arr=usermails.split(",");
		if(arr.length==1){
			return arr;
		}else{
	     List<String> arrList = new ArrayList<String>();
	     int cnt= 0;
	          for(int i=0;i<arr.length;i++){
	        	  for(int j=i+1;j<arr.length;j++){
	        		  if(Utilities.trim(arr[i]).equalsIgnoreCase(Utilities.trim((arr[j])))){
	        			  cnt+=1;
	        		  }                
	        	  }
	        	  if(cnt<1){
	        		  arrList.add(Utilities.trim((arr[i])));
	        	  }
	          cnt=0;
	        }
	          String[] recipientList = new String[arrList.size()];
	          recipientList = arrList.toArray(recipientList);
		
		return recipientList;
		}
		
	}
	
	public void persistMailLogs(int runId,String senderId,String receipientList,String status,String ccList){
		TenjinMailLogs mailInfo=	new TenjinMailLogs();
		
		mailInfo.setSentTime(new Timestamp((new Date().getTime())));
		mailInfo.setRunId(runId);
		mailInfo.setSenderId(senderId);
		mailInfo.setRecipientIds(receipientList.substring(1,receipientList.length()-1));
		mailInfo.setSentStatus(status);
		if(!ccList.equalsIgnoreCase("null")){
		mailInfo.setCcList(ccList.substring(1,ccList.length()-1));
		}
		try {
			new ParametersHelper().persistMailLogs(mailInfo);
		} catch (DatabaseException e) {
			
			logger.error("Error ", e);
		}
		
	}
public void buildMailInformationForManualSentMail(int runId,int projectId,String userList) throws TenjinConfigurationException {
		
		try{
		    String [] userIds=userList.split(";");
			TenjinReflection tenjinReflection =new TenjinReflection();
			String project=tenjinReflection.getProject(projectId);
			String usermails="";
			JSONObject jsonProject=new JSONObject(project);
				JSONArray array=jsonProject.getJSONArray("users");
				runID=runId;
				for(int i=0;i<array.length();i++){
					JSONObject objects = array.optJSONObject(i);
					String userIdFromJson=objects.getString("id");
					for(String str:userIds){
						if(str.equalsIgnoreCase(userIdFromJson)){
							usermails+=objects.getString("email");
							if(i<array.length()-1){
								usermails+=",";
							}
							break;
						}
						
					}
					
				}
				this.buildAndSendMail(runId, projectId, usermails, jsonProject);
		}catch(Exception e){
			logger.error(e.getMessage());
			throw new TenjinConfigurationException(e.getMessage());
		}
	}
public void buildAndSendMail(int runId,int projectId,String usermails,	JSONObject jsonProject ) throws TenjinConfigurationException, IOException{
	
try{
	TenjinMail mail=new TenjinMail();
	TenjinReflection tenjinReflection =new TenjinReflection();
	String runs =tenjinReflection.getRunJson(runId, projectId);
	JSONObject jsonRun = new JSONObject(runs);
	
	String projectMail=tenjinReflection.getProjectMail(projectId);
	JSONObject jsonMail = new JSONObject(projectMail);
	mail.setRecipientMails(usermails);
	HashMap<String, String> hashMap = new EmailHandler().hydrateEmailConfiguration();
	if(hashMap.size()>0){
		mail.setSmtpServer(hashMap.get("smtpServer"));
		mail.setPassword(hashMap.get("password"));
		mail.setSenderMail(hashMap.get("userId"));
		mail.setSenderName(hashMap.get("userName"));
		mail.setAuthentication(hashMap.get("authentication"));
		if(hashMap.get("port")!=null){
			mail.setPort(Integer.parseInt(hashMap.get("port")));
		}
		
		String mailSubject=buildMailSubject(jsonProject,jsonRun,jsonMail);
		mail.setSubject(mailSubject);
		String runJson=	tenjinReflection.getRunJson(runId,projectId);
		senderAliasName=new JSONObject(runJson).getString("user");
		String context=new TenjinMailcontext().contextBuilder(runJson);
		 String workPath = TenjinConfiguration.getProperty("MEDIA_REPO_PATH");
		String fileName=this.fetchReport(runId, projectId,runJson,workPath);
	     Multipart multipart = new MimeMultipart();
	     BodyPart testExecutiontbody = new MimeBodyPart();
	     BodyPart reportBody = new MimeBodyPart();
	     testExecutiontbody.setContent(context,"text/html; charset=utf-8");
	     multipart.addBodyPart(testExecutiontbody);
	     FileDataSource source = new FileDataSource(workPath + fileName);
	     reportBody.setDataHandler(new DataHandler(source));
	     reportBody.setFileName(source.getName());
	     multipart.addBodyPart(reportBody);
	     this.sendEmail(multipart, mail);
}else{
	logger.info("E-mail notification not configured.");
	throw new TenjinConfigurationException("E-mail notification settings are not configured.");
}
}
/*Added by Ruksar for TENJINCG-753 starts*/
catch(IOException ie){	
	logger.error(ie.getMessage());
	throw new TenjinConfigurationException("File path error.");
	}
/*Added by Ruksar for TENJINCG-753 ends*/
catch(Exception e){
	logger.error(e.getMessage());
	throw new TenjinConfigurationException(e.getMessage());
}
 
}

public void buildTenjinMailInformation(JSONObject mailDetails) {

	try{
	logger.info("checking email notification");
		TenjinMail mail=new TenjinMail();
		mail.setRecipientMails(mailDetails.getString("userEmailId"));
		HashMap<String, String> hashMap = new EmailHandler().hydrateEmailConfiguration();
		if(hashMap.size()>0){
			mail.setSmtpServer(hashMap.get("smtpServer"));
			mail.setPassword(hashMap.get("password"));
			mail.setSenderMail(hashMap.get("userId"));
			mail.setSenderName(hashMap.get("userName"));
			mail.setAuthentication(hashMap.get("authentication"));
			if(hashMap.get("port")!=null){
				mail.setPort(Integer.parseInt(hashMap.get("port")));
				String mailSubject=this.buildTenjinMailSubject(mailDetails);
				mail.setSubject(mailSubject);
				String context=new TenjinMailcontext().genericContextBuilder(mailDetails);		
				this.sendEmail(context, mail);
			}}else{
				logger.info("email notification not configured");
			}
			
		}catch(Exception e){
		logger.error(e.getMessage());
	}

}
	private String buildTenjinMailSubject(JSONObject mailContent) throws JSONException {
	
		String entityType=mailContent.getString("entityType");
		String entityName=mailContent.getString("entityName");
		String userName=mailContent.getString("userName");
		String operation=mailContent.getString("operation");
		String operationString="";		
		String subjectLine="";

		if(operation.equalsIgnoreCase("update")){
			operationString="Updated";
		}
		else if(operation.equalsIgnoreCase("delete")){
			operationString="Deleted";
		}
		else if(operation.equalsIgnoreCase("create")){
			operationString="Created";
		}
		else if(operation.equalsIgnoreCase("post")){
			operationString="Posted";
		}
		else if(operation.equalsIgnoreCase("cancel")){
			operationString="Cancelled";
		}
		else if(operation.equalsIgnoreCase("remove")){
			operationString="Removed";
		}
		if(entityType.toLowerCase().contains("schedule") && !entityType.equalsIgnoreCase("scheduled task")){
			operationString=operation+" initiated with scheduler "+entityName;
			subjectLine=operationString;
		}
		else if(entityType.equalsIgnoreCase("project user")){
			operationString="You have been removed from Project: "+entityName+" by "+userName;
			subjectLine=operationString;
		}
		if(subjectLine.equalsIgnoreCase("")){
		
			subjectLine=entityType+" "+operationString+" by "+userName;
		}
		
		return subjectLine;
		
		
	}
private void sendEmail(String context, TenjinMail mail)throws TenjinConfigurationException, UnsupportedEncodingException {

	String[] recipientMails =null;
	String[] ccMails=null;
		try{
		   Properties properties = System.getProperties();
		   logger.info("setting properties");
		   properties.put("mail.smtp.starttls.enable", "true");
		   properties.put("mail.smtp.host", mail.getSmtpServer());
		   properties.put("mail.smtp.password", mail.getPassword());
		   properties.put("mail.smtp.port", mail.getPort());
		   properties.put("mail.smtp.auth", mail.getAuthentication());
		   properties.put("mail.smtp.ssl.trust", mail.getSmtpServer());
		   logger.info("setting properties are completed");
		   
		   logger.info("initializing session object");
		   SMTPAuthenticator authenticator = null;
		   if(Utilities.trim(mail.getSenderName()).length()>0){
			   authenticator = new SMTPAuthenticator(mail.getSenderName(), mail.getPassword());
			}else{
				authenticator = new SMTPAuthenticator(mail.getSenderMail(), mail.getPassword());
			 }
		   properties.put("mail.smtp.submitter", authenticator.getPasswordAuthentication().getUserName());
		   Session session = Session.getInstance(properties, authenticator);
		   
		   logger.info("initializing Mimemessage object");
		   Message message = new MimeMessage(session);
		   logger.info("Setting sender and receiver mail");
		   if(senderAliasName!=null)
			   message.setFrom(new InternetAddress(mail.getSenderMail(),senderAliasName));
		   else
			   message.setFrom(new InternetAddress(mail.getSenderMail())); 
		   recipientMails =this.checkDuplicateUsersMails(mail.getRecipientMails());
		   if(mail.getCcMails()!=null && !mail.getCcMails().equals("")){
			   ccMails=this.checkDuplicateUsersMails(mail.getCcMails());
			   recipientMails=this.getRecipients(recipientMails,ccMails);
			  
				   InternetAddress[] ccRecipientAddress = new InternetAddress[ccMails.length];
		   int counter1=0;
		   for (String ccRecipient : ccMails) {
			   ccRecipientAddress[counter1] = new InternetAddress(ccRecipient.trim());
		       counter1++;
		   }
		   message.addRecipients(Message.RecipientType.CC, ccRecipientAddress);
		   }
		   
		   InternetAddress[] recipientAddress = new InternetAddress[recipientMails.length];
		   int counter = 0;
		   for (String recipient : recipientMails) {
		       recipientAddress[counter] = new InternetAddress(recipient.trim());
		       counter++;
		   }
		   message.setRecipients(Message.RecipientType.TO, recipientAddress);
		   logger.info("Setting text and subject line");
		   message.setSubject(mail.getSubject());
		   message.setContent(context,"text/html; charset=utf-8");
		   
		   logger.info("initializing Transport object");
		   Transport transport = session.getTransport("smtp");
		    logger.info("connecting to the mail service...");
		   transport.connect(mail.getSmtpServer(), mail.getSenderMail(), mail.getPassword());
		   logger.info("sending mail...");
		   transport.sendMessage(message, message.getAllRecipients());
		   transport.close();
		   this.persistMailLogs(runID, mail.getSenderMail(),Arrays.toString(recipientMails), "Pass", Arrays.toString(ccMails));
	      logger.info("Sent mail successfully.");
	   	}catch (AuthenticationFailedException mex) {
	   		this.persistMailLogs(runID, mail.getSenderMail(),Arrays.toString(recipientMails), "Fail", Arrays.toString(ccMails));
	   		logger.error("Incorrect username or password.");
	     throw new TenjinConfigurationException("Incorrect userID or password.");
	   } catch (javax.mail.SendFailedException mex) {
		   this.persistMailLogs(runID, mail.getSenderMail(),Arrays.toString(recipientMails), "Fail", Arrays.toString(ccMails));
	   		logger.error("Incorrect Recipient E-mail.");
	     throw new TenjinConfigurationException("Incorrect Recipient E-mail.");
	   }catch (javax.mail.MessagingException mex) {
		   this.persistMailLogs(runID, mail.getSenderMail(),Arrays.toString(recipientMails), "Fail", Arrays.toString(ccMails));
		   if(mex.getMessage().contains("Could not connect to SMTP host")){
			   logger.error(mex.getMessage());
			     throw new TenjinConfigurationException("Invalid port.");
		   }else if(mex.getMessage().contains("Unknown SMTP host")){
			   logger.error(mex.getMessage());
			     throw new TenjinConfigurationException("Invalid SMTP Server.");
		   }
		   else{
			   logger.error(mex.getMessage());
			   throw new TenjinConfigurationException("An internal error occurred. Please contact Tenjin Support.");
		   }
	   		
	   }
	
	
}

private String getPrjUserTosendMail(JSONArray prjUsersArray,int projectId ,TenjinReflection tenjinReflection,JSONObject runJson,String additionalMails) throws JSONException{
	String users="";
	String mailSendUsers=tenjinReflection.hydrateProjectMailPreferenceUsers(projectId,"Execution");
	mailSendUsers=mailSendUsers.substring(1, mailSendUsers.length()-1);
	List<String> mailSendUsersList=new ArrayList<String>();
	/*Added by lokanath for TENJINCG-1185 starts*/
	List<String> additionalMailList=new ArrayList<String>();
	if(!mailSendUsers.equals("")){
		mailSendUsersList=Arrays.asList(mailSendUsers.split(","));
	}
	if(!additionalMails.equals("")){
		additionalMailList=Arrays.asList(additionalMails.split(","));
	}
	if(mailSendUsersList.size()>0){
		for(int i=0;i<prjUsersArray.length();i++){
			JSONObject objects = prjUsersArray.optJSONObject(i);
			if(mailSendUsersList.contains(objects.getString("id"))){
				users+=objects.getString("email")+",";
			}
		}
		
	}else{
			for(int i=0;i<prjUsersArray.length();i++){
				JSONObject objects = prjUsersArray.optJSONObject(i);
				users+=objects.getString("email")+",";
			}
	}
	if(additionalMailList.size()>0){
		for(String s:additionalMailList){
			if(!users.contains(s)){
				users+=s+",";
			}
		}
		
	}
	
	users=users.substring(0,users.length()-1);
	/*Modified by lokanath for TENJINCG-1185 starts*/
	return users;
}

public void buildTenjinPwdResetMailInformation(JSONObject mailDetails) throws TenjinConfigurationException {
	try{
		logger.info("checking email notification");
			TenjinMail mail=new TenjinMail();
			mail.setRecipientMails(mailDetails.getString("userEmailId"));
			HashMap<String, String> hashMap = new EmailHandler().hydrateEmailConfiguration();
			if(hashMap.size()>0){
				mail.setSmtpServer(hashMap.get("smtpServer"));
				mail.setPassword(hashMap.get("password"));
				mail.setSenderMail(hashMap.get("userId"));
				mail.setSenderName(hashMap.get("userName"));
				mail.setAuthentication(hashMap.get("authentication"));
				if(hashMap.get("port")!=null){
					mail.setPort(Integer.parseInt(hashMap.get("port")));
					String mailSubject="Tenjin password reset link";
					mail.setSubject(mailSubject);
					String context=new TenjinMailcontext().passwordResetCotext(mailDetails);		
					this.sendEmail(context, mail);
				}}else{
					logger.info("email notification not configured");
					throw new TenjinConfigurationException("Email notification is not configured. Please contact to Administrator.");
				}
				
			}catch(Exception e){
			logger.error(e.getMessage());
			throw new TenjinConfigurationException(e.getMessage());
		}
	}
/*Added by Ashiki for TENJINCG-1211 starts*/
public void buildConsolidatedMailInformation(String currentdatetime, int projectId, String currentUser) {
	TenjinReflection tenjinReflection;
	try {
		tenjinReflection = new TenjinReflection();
		TenjinMail mail=new TenjinMail();
		String project=tenjinReflection.getProject(projectId);
		String usermails="";
		JSONObject jsonProject=new JSONObject(project);
			JSONArray array=jsonProject.getJSONArray("users");
			for(int i=0;i<array.length();i++){
				JSONObject objects = array.optJSONObject(i);
				usermails+=objects.getString("email");
				if(i<array.length()-1){
					usermails+=",";
				}
				
			}
			mail.setRecipientMails(usermails);
			HashMap<String, String> hashMap = new EmailHandler().hydrateEmailConfiguration();
			if(hashMap.size()>0){
				mail.setSmtpServer(hashMap.get("smtpServer"));
				mail.setPassword(hashMap.get("password"));
				mail.setSenderMail(hashMap.get("userId"));
				mail.setSenderName(hashMap.get("userName"));
				mail.setAuthentication(hashMap.get("authentication"));
				if(hashMap.get("port")!=null){
					mail.setPort(Integer.parseInt(hashMap.get("port")));
				}
				
				mail.setSubject("Today Executed Runs");
				String Project=	tenjinReflection.getProject(projectId);
				String context=new TenjinMailcontext().consolidatedcontextBuilder(Project,currentUser,jsonProject);
				 String workPath = TenjinConfiguration.getProperty("MEDIA_REPO_PATH");
				 String fileName=new TenjinReflection().GetConsolidatedReportPath(projectId,workPath,currentdatetime,currentUser);
				 fileName=fileName.substring(1,fileName.length()-1);
			     Multipart multipart = new MimeMultipart();
			     BodyPart testExecutiontbody = new MimeBodyPart();
			     BodyPart reportBody = new MimeBodyPart();
			     testExecutiontbody.setContent(context,"text/html; charset=utf-8");
			     multipart.addBodyPart(testExecutiontbody);
			     FileDataSource source = new FileDataSource(workPath + fileName);
			     reportBody.setDataHandler(new DataHandler(source));
			     reportBody.setFileName(source.getName());
			     multipart.addBodyPart(reportBody);
			     this.sendEmail(multipart, mail);
	}
	}
			catch (Exception e1) {
		logger.error("Error ", e1);
	}
}

/*Added by Ashiki for TENJINCG-1211 end*/
}
