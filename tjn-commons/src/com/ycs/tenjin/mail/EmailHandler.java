
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  EmailHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 03-11-2017           Padmavathi             	Newly added for TENJINCG-575
 * 13-06-2018           Padmavathi             	T251IT-5
 * 19-06-2018			Preeti					T251IT-66
 * 03-07-2018           Padmavathi              T251IT-168
 * 24-06-2019           Padmavathi              for license 
 * 13-04-2020			Lokanath				TENJINCG-1140
 */

package com.ycs.tenjin.mail;




import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ParametersHelper;
import com.ycs.tenjin.license.License;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

public class EmailHandler {
	private static final Logger logger = LoggerFactory.getLogger(EmailHandler.class);
	 
	ParametersHelper helper=new ParametersHelper();
	public void persistEmailConfiguration(Map<String, String> map) throws DatabaseException, TenjinConfigurationException   {
		
		this.validateEmailConfiguration(map,false);
		HashMap<String, String> hashMap=this.hydrateEmailConfiguration();
		if(hashMap.size()>0){
	    Set<String> keys=map.keySet();
		this.deleteEmailConfiguration(keys);
		}
		
		try {
			/*Modified by paneendra for VAPT FIX starts*/
			/*map.put("password", (new Crypto().encrypt(map.get("password"))));*/
			map.put("password", (new CryptoUtilities().encrypt(map.get("password"))));
			/*Modified by paneendra for VAPT FIX ends*/
		}  catch (Exception e) {
			logger.error("error occured while encrypting password");
		}

		helper.pesistsEmailConfiguration(map);
		
	}
	public HashMap<String, String>  hydrateEmailConfiguration() throws DatabaseException{
		HashMap<String, String>  map=helper.hydrateEmailConfiguration();
	    String decpwd=map.get("password");
		if(decpwd!=null){
		try {
			/*Modified by paneendra for VAPT FIX starts*/
			/*String pwd = new Crypto().decrypt(decpwd.split("!#!")[1],
					  decodeHex(decpwd.split("!#!")[0].toCharArray()));*/
			String pwd = new CryptoUtilities().decrypt(decpwd);
			/*Modified by paneendra for VAPT FIX ends*/
			map.put("password", pwd);
		} catch (Exception e) {
			logger.error("could not decrypt password",e);
		}
		}
		return map;
	}
	public void  deleteEmailConfiguration(Set<String> keys) throws DatabaseException{
		 helper.deleteEmailConfiguration(keys);
		
	}
	public void TestEmail(JSONObject json) throws  JSONException, TenjinConfigurationException, UnsupportedEncodingException, MessagingException{
		Map<String, String> map = new HashMap<String, String>();
		@SuppressWarnings("unchecked")
	    Iterator<String> keysItr = json.keys();
	    while(keysItr.hasNext()) {
	        String key = keysItr.next();
	        Object value = json.get(key);
	        map.put(key, (String) value);
	        
	    }
	    this.validateEmailConfiguration(map,true);
	    	if(Utilities.trim(map.get("email")).length() < 1) {
				logger.error("ERROR - email is blank");
				throw new TenjinConfigurationException("Recipient E-mail is required.");
			}
	    	else if((!Utilities.trim(map.get("email")).matches("\\s*\\S+\\s*"))){
				logger.error("ERROR - Recipient E-mail can not contain spaces.");
				throw new TenjinConfigurationException("Recipient E-mail can not contain spaces.");
			}
	   TenjinMail mail=new TenjinMail();
	   mail.setSmtpServer(Utilities.trim(json.getString("smtpServer")));
	   mail.setPort(Integer.parseInt(Utilities.trim(json.getString("port"))));
	   mail.setSenderMail(Utilities.trim(json.getString("userId")));
	   mail.setPassword(Utilities.trim(json.getString("password")));
	   mail.setRecipientMails(Utilities.trim( json.getString("email")));
	   mail.setSenderName(Utilities.trim( json.getString("userName")));
	  
	   if(Utilities.trim(json.getString("authentication")).equalsIgnoreCase("Y")){
		   mail.setAuthentication("true");
	   }else{
		   mail.setAuthentication("false");
	   }
	   mail.setSubject("Test mail from Tenjin.");
	   Multipart multipart = new MimeMultipart();
	   BodyPart msgBody = new MimeBodyPart();
	   msgBody.setText("Test mail received from Tenjin, configuration settings are validated successfully.");
       multipart.addBodyPart(msgBody);
	   new TenjinJavaxMail().sendEmail(multipart,mail);
	  
	}
	
	private void validateEmailConfiguration(Map<String, String> map,boolean flag) throws  TenjinConfigurationException {
		if(map.size()>0){
			if(Utilities.trim(map.get("smtpServer")).length() < 1) {
				logger.error("ERROR - SMTP Server is blank");
				throw new TenjinConfigurationException("SMTP Server is required. ");
			}
			if(Utilities.trim(map.get("port")).length() < 1) {
				logger.error("ERROR - Port is blank");
				throw new TenjinConfigurationException( "Port is required.");
			}
			if((Utilities.trim(map.get("authentication")).length()<1 &&!(Utilities.trim(map.get("authentication")).equalsIgnoreCase("Y")))||(Utilities.trim(map.get("authentication")).equalsIgnoreCase("N")))
			{
			    map.put("authentication", "N");
			    if(flag){
			    	throw new TenjinConfigurationException( "Authentication is required.");
					 }
			}else{
				if(Utilities.trim(map.get("userId")).length() < 1) {
					logger.error("ERROR - UserId is blank");
					throw new TenjinConfigurationException("E-mail is required.");
				}
				else if((!Utilities.trim(map.get("userId")).matches("\\s*\\S+\\s*"))){
						logger.error("ERROR - User Id can not contain spaces.");
						throw new TenjinConfigurationException("User ID can not contain spaces.");
					}
				/*Added by lokanath for TENJINCG-1140 starts*/
				else if((Utilities.trim(map.get("userId")).length()>1)){
					String email=map.get("userId");
					 int atstr = email.indexOf("@");
					    int dotstr = email.lastIndexOf(".");
					    	if (atstr<1 || dotstr<atstr+2 || dotstr+2>=email.length()) {
					    		logger.error("ERROR - User Id must be valid Mail Id.");
								throw new TenjinConfigurationException("Email Id is not Valid.");
					    	}
				}
				/*Added by lokanath for TENJINCG-1140 ends*/
				if(Utilities.trim(map.get("password")).length() < 1) {
					logger.error("ERROR - password is blank");
					throw new TenjinConfigurationException( "Password is required.");
				}
			}
			
			if(Utilities.trim(map.get("smtpServer")).length() > 255) {
				logger.error("ERROR - SMTP Server value is long");
				throw new TenjinConfigurationException("SMTP Server should not exceed its max length 255. ");
			}
			if(Utilities.trim(map.get("port")).length() > 255) {
				logger.error("ERROR - Port value is long");
				throw new TenjinConfigurationException("Port should not exceed its max length 255. ");
			}
			if(Utilities.trim(map.get("userId")).length() > 255) {
				logger.error("ERROR - User Id value is long");
				throw new TenjinConfigurationException("User Id should not exceed its max length 255. ");
			}
			if(Utilities.trim(map.get("password")).length() > 50) {
				logger.error("ERROR - Password value is long");
				throw new TenjinConfigurationException("Password should not exceed its max length 50. ");
			}
			try{
				Integer.parseInt(Utilities.trim(map.get("port")));
			}catch(NumberFormatException e){
				logger.error("ERROR - port is not a number");
				throw new TenjinConfigurationException("Please enter only number for Port.");
			}
		}
	}
	 public void sendLicenseNotificationMail(License license,String action,String recipientMails){
	  	   try {
	  		 TenjinMail mail=new TenjinMail();
	  		 logger.info("Checking mail configuration settings...");
	  		 HashMap<String, String> hashMap = new EmailHandler().hydrateEmailConfiguration();
	  		 if(hashMap.size()>0){
	  			mail.setSmtpServer(hashMap.get("smtpServer"));
	  			mail.setPassword(hashMap.get("password"));
	  			mail.setSenderMail(hashMap.get("userId"));
	  			mail.setSenderName(hashMap.get("userName"));
	  			mail.setAuthentication(hashMap.get("authentication"));
	  			if(hashMap.get("port")!=null){
	  				mail.setPort(Integer.parseInt(hashMap.get("port")));
	  			}
	    	   mail.setSubject("License notification from Tenjin.");
	    	   if(action!=null&&(action.equalsIgnoreCase("Modified")||action.equalsIgnoreCase("Renewed"))){
	    		   mail.setCcMails(license.getLicSupportMails());
	    	   }
	    	   mail.setRecipientMails(recipientMails);
	    	   Multipart multipart = new MimeMultipart();
	    	   BodyPart msgBody = new MimeBodyPart();
	    	   String context=new TenjinMailcontext().buildLicenseMailContext(license, action);
	    	   msgBody.setContent(context,"text/html; charset=utf-8"); 
	    	   multipart.addBodyPart(msgBody);
		  	   new TenjinJavaxMail().sendEmail(multipart,mail);
	  		}else{
	  			 logger.info("Mail configuration settings are not configured.");
	  		}
	  	   } catch (MessagingException |UnsupportedEncodingException |TenjinConfigurationException | DatabaseException e) {
	  		   
	  		   logger.error(e.getMessage());
	  		   logger.error("Error ", e);
	  	   } 
	     }
}
