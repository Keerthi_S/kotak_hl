/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: TenjinReflection.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 31-01-2018			    Padmavathi		        Newly added for TENJINCG-579
 05-02-2018             Padmavathi              for attaching pdf in mail
 02-05-2018				Preeti					TENJINCG-656
 14-05-2018				Pushpalatha				TENJINCG-629
 06-03-2019             Leelaprasad             TENJINCG-993
  06-05-2019            Padmavathi              TENJINCG-1050
 17-03-2020             Lokanath				TENJINCG-1184
 * */



package com.ycs.tenjin.mail;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.GsonBuilder;
import com.ycs.tenjin.TenjinConfigurationException;

public class TenjinReflection {
	private static final Logger logger = LoggerFactory.getLogger(TenjinReflection.class);

	public  String getRunJson(int runId,int projectId) {
		String json =null;

		try {
			
			Object helperObj = getHelperObject("com.ycs.tenjin.db.ResultsHelper");
			
			Method method = helperObj.getClass().getMethod("hydrateRun", int.class, int.class);
			Object runObj = method.invoke(helperObj,runId, projectId);
			
			GsonBuilder gBuilder = new GsonBuilder();
			json = gBuilder.setPrettyPrinting().create().toJson(runObj);
			
		} catch (SecurityException e) {
			logger.error("error while hydrating TestRun object");
		} catch (IllegalArgumentException e) {
			logger.error("error while hydrating TestRun object");
		} catch (IllegalAccessException e) {
			logger.error("error while hydrating TestRun object");
		} catch (NoSuchMethodException e) {
			logger.error("error while hydrating TestRun object");
		} catch (InvocationTargetException e) {
			logger.error("error while hydrating TestRun object");
		}
		return json;
		
		
	}
	/* Added by Pushpalatha for TENJINCG-629 starts*/
	public  String getExecuitionHistoryJson(int tsetRecId,int projectId) {
		String json =null;

		try {
			
			Object helperObj = getHelperObject("com.ycs.tenjin.db.TestSetHelperNew");
			
			Method method = helperObj.getClass().getMethod("hydrateTestSetExecutionHistoryForEscMail", int.class, int.class);
			Object runObj = method.invoke(helperObj,tsetRecId, projectId);
			
			GsonBuilder gBuilder = new GsonBuilder();
			json = gBuilder.setPrettyPrinting().create().toJson(runObj);
			
		} catch (SecurityException e) {
			logger.error("error while hydrating TestRun object");
		} catch (IllegalArgumentException e) {
			logger.error("error while hydrating TestRun object");
		} catch (IllegalAccessException e) {
			logger.error("error while hydrating TestRun object");
		} catch (NoSuchMethodException e) {
			logger.error("error while hydrating TestRun object");
		} catch (InvocationTargetException e) {
			logger.error("error while hydrating TestRun object");
		}
		return json;
		
		
	}
	/* Added by Pushpalatha for TENJINCG-629 ends*/
	public String getProject(int projectId){
		    String json=null;
			Object helperobj=getHelperObject("com.ycs.tenjin.db.ProjectHelper");
		    Method method;
			try {
				method = helperobj.getClass().getMethod("hydrateProjectMailNotification", int.class);
				Object userObj=  method.invoke(helperobj, projectId);
			    GsonBuilder gBuilder = new GsonBuilder();
				json = gBuilder.setPrettyPrinting().create().toJson(userObj);
				
			} catch (SecurityException e) {
			logger.error("error while hydrating project");
			} catch (NoSuchMethodException e) {
				logger.error("error while hydrating project");
			} catch (IllegalArgumentException e) {
				logger.error("error while hydrating project");
			} catch (IllegalAccessException e) {
				logger.error("error while hydrating project");
			} catch (InvocationTargetException e) {
				logger.error("error while hydrating project");
			}
		    
		return json;
		
	}
	/*Added by Preeti for TENJINCG-656 starts*/
	public String getProjectMail(int projectId){
	    String json=null;
		Object helperobj=getHelperObject("com.ycs.tenjin.db.ProjectMailHelper");
	    Method method;
		try {
			method = helperobj.getClass().getMethod("hydrateProjectMail", int.class);
			Object userObj=  method.invoke(helperobj, projectId);
		    GsonBuilder gBuilder = new GsonBuilder();
			json = gBuilder.setPrettyPrinting().create().toJson(userObj);
			
		} catch (SecurityException e) {
		logger.error("error while hydrating project");
		} catch (NoSuchMethodException e) {
			logger.error("error while hydrating project");
		} catch (IllegalArgumentException e) {
			logger.error("error while hydrating project");
		} catch (IllegalAccessException e) {
			logger.error("error while hydrating project");
		} catch (InvocationTargetException e) {
			logger.error("error while hydrating project");
		}
	    
	return json;
	
}
	/*Added by Padmavathi for  TENJINCG-1050 starts*/
	public String hydrateProjectMailPreferenceUsers(int projectId,String action){
	    String json=null;
		Object helperobj=getHelperObject("com.ycs.tenjin.db.ProjectMailHelper");
	    Method method;
		try {
			method = helperobj.getClass().getMethod("hydrateProjectMailPreferenceUsers", int.class,String.class);
			Object userObj=  method.invoke(helperobj, projectId,action);
		    GsonBuilder gBuilder = new GsonBuilder();
			json = gBuilder.setPrettyPrinting().create().toJson(userObj);
			
		} catch (SecurityException e) {
		logger.error("error while hydrating project");
		} catch (NoSuchMethodException e) {
			logger.error("error while hydrating project");
		} catch (IllegalArgumentException e) {
			logger.error("error while hydrating project");
		} catch (IllegalAccessException e) {
			logger.error("error while hydrating project");
		} catch (InvocationTargetException e) {
			logger.error("error while hydrating project");
		}
	    
	return json;
	
}
	/*Added by Padmavathi for  TENJINCG-1050 ends*/
	/*Added by Preeti for TENJINCG-656 ends*/
	
	private Object getHelperObject(String className){
		String HelperClassName=className;
		Object helperobj=null;
	      try {
	    	  Class<?> cls=Class.forName(HelperClassName);
			  helperobj=cls.newInstance();
			} catch (InstantiationException e) {
				logger.error("error while instantiation class "+HelperClassName);
			} catch (IllegalAccessException e) {
				logger.error("error while instantiation class "+HelperClassName);
			} catch (ClassNotFoundException e) {
				logger.error("error while getting class "+HelperClassName);
			}
		    
		return helperobj;
		
	}
	
	
	public String GetReportPath(int runId,int projectId,String folderPath,String runJson) throws JSONException, TenjinConfigurationException{
		String fileName=null;
		Method method;
	      try {
	    	  JSONObject json=new JSONObject(runJson);
	    	  Class<?> cls=Class.forName("com.ycs.tenjin.util.PdfHandler");
	    	  Constructor<?> constructor=cls.getConstructor(String.class,String.class);
	    	  String userName=json.getString("user");
	    	  Object handler= constructor.newInstance(folderPath,userName);
	    	  method = handler.getClass().getMethod("generatePdfReportForRun", int.class,int.class,String.class);
			  Object reportObj=  method.invoke(handler,runId, projectId,folderPath);
			  GsonBuilder gBuilder = new GsonBuilder();
			  fileName = gBuilder.setPrettyPrinting().create().toJson(reportObj);
			} catch (InstantiationException e) {
				logger.error("error while instantiation class PdfHandler");
			} catch (IllegalAccessException e) {
				logger.error("error while instantiation class PdfHandler");
			} catch (ClassNotFoundException e) {
				logger.error("error while getting class PdfHandler ");
			} catch (NoSuchMethodException e) {
				logger.error("error while invoking method generatePdfReportForRun");
			}   catch (InvocationTargetException e) {
				logger.error("Error ", e);
				logger.error("error while instantiation class PdfHandler");
			}
	    
		return fileName;
		
	}
	public String getDeafultAppBrowser(int appId) {
		String json =null;

		try {
			
			Object helperObj = getHelperObject("com.ycs.tenjin.db.TestStepHelper");
			
			Method method = helperObj.getClass().getMethod("getAppDefaultBrowser", int.class);
			Object runObj = method.invoke(helperObj,appId);
			
			GsonBuilder gBuilder = new GsonBuilder();
			json = gBuilder.create().toJson(runObj).toString();
			
		} catch (SecurityException e) {
			logger.error("error while Fetching deafalut browser for application");
		} catch (IllegalArgumentException e) {
			logger.error("error while Fetching deafalut browser for application");
		} catch (IllegalAccessException e) {
			logger.error("error while Fetching deafalut browser for application");
		} catch (NoSuchMethodException e) {
			logger.error("error while Fetching deafalut browser for application");
		} catch (InvocationTargetException e) {
			logger.error("error while Fetching deafalut browser for application");
		}
		return json;
		
		
	}
	
	/*Added by lokanath for TENJINCG-1184 starts*/
	public  String getRunIdType(int runId) {
		
		String json =null;
        try {
			
			Object helperObj = getHelperObject("com.ycs.tenjin.db.SchedulerHelper");
			
			Method method = helperObj.getClass().getMethod("checkRunIdType", int.class);
			Object runObj = method.invoke(helperObj,runId);
			
			GsonBuilder gBuilder = new GsonBuilder();
			json = gBuilder.setPrettyPrinting().create().toJson(runObj);
			
		} catch (SecurityException e) {
			logger.error("error while hydrating TestRun object");
		} catch (IllegalArgumentException e) {
			logger.error("error while hydrating TestRun object");
		} catch (IllegalAccessException e) {
			logger.error("error while hydrating TestRun object");
		} catch (NoSuchMethodException e) {
			logger.error("error while hydrating TestRun object");
		} catch (InvocationTargetException e) {
			logger.error("error while hydrating TestRun object");
		}
		return json;
	}
	/*Added by lokanath for TENJINCG-1184 ends*/
	
	/*Added by Pushpalatha for 1106 starts*/
	public String hydrateRunForAudit(int runId,int prjId){
	    String json=null;
		Object helperobj=getHelperObject("com.ycs.tenjin.db.ResultsHelper");
	    Method method;
		try {
			method = helperobj.getClass().getMethod("hydrateRunForAudit", int.class,int.class);
			Object runObj=  method.invoke(helperobj, runId,prjId);
		    GsonBuilder gBuilder = new GsonBuilder();
			json = gBuilder.setPrettyPrinting().create().toJson(runObj);
			
		} catch (SecurityException e) {
		logger.error("error while hydrating project");
		} catch (NoSuchMethodException e) {
			logger.error("error while hydrating project");
		} catch (IllegalArgumentException e) {
			logger.error("error while hydrating project");
		} catch (IllegalAccessException e) {
			logger.error("error while hydrating project");
		} catch (InvocationTargetException e) {
			logger.error("error while hydrating project");
		}
	    
	return json;
	
}
	
	
	public String persisteExecAudit(int runId,int prjId){
	    String json=null;
		Object helperobj=getHelperObject("com.ycs.tenjin.db.IterationHelper");
	    Method method;
		try {
			method = helperobj.getClass().getMethod("persisteExecAudit", int.class,int.class);
			Object runObj=  method.invoke(helperobj, runId,prjId);
		    GsonBuilder gBuilder = new GsonBuilder();
			json = gBuilder.setPrettyPrinting().create().toJson(runObj);
			
		} catch (SecurityException e) {
		logger.error("error while hydrating project");
		} catch (NoSuchMethodException e) {
			logger.error("error while hydrating project");
		} catch (IllegalArgumentException e) {
			logger.error("error while hydrating project");
		} catch (IllegalAccessException e) {
			logger.error("error while hydrating project");
		} catch (InvocationTargetException e) {
			logger.error("error while hydrating project");
		}
	    
	return json;
	
}
	/*Added by Pushpalatha for 1106 ends*/

/*Added by Ashiki for TENJINCG-1211 starts*/
public String getConsolidatedRuns(String currentdatetime, int projectId ) {

    String json=null;
	Object helperobj=getHelperObject("com.ycs.tenjin.db.ResultsHelper");
    Method method;
	try {
		method = helperobj.getClass().getMethod("hydrateConsolidatedRun", String.class,int.class);
		Object userObj=  method.invoke(helperobj,currentdatetime, projectId);
	    GsonBuilder gBuilder = new GsonBuilder();
		json = gBuilder.setPrettyPrinting().create().toJson(userObj);
		
	} catch (SecurityException e) {
	logger.error("error while hydrating project");
	} catch (NoSuchMethodException e) {
		logger.error("error while hydrating project");
	} catch (IllegalArgumentException e) {
		logger.error("error while hydrating project");
	} catch (IllegalAccessException e) {
		logger.error("error while hydrating project");
	} catch (InvocationTargetException e) {
		logger.error("error while hydrating project");
	}
    
return json;


	
}
public String GetConsolidatedReportPath(int projectId, String folderPath, String currentdatetime, String currentUser) throws JSONException {
	String fileName=null;
	Method method;
	try {
    	  Class<?> cls=Class.forName("com.ycs.tenjin.util.PdfHandler");
    	  Constructor<?> constructor=cls.getConstructor(String.class,String.class);
    	  Object handler= constructor.newInstance(folderPath,currentUser);
    	  method = handler.getClass().getMethod("generatePdfReportConsolitatedRun", int.class,String.class,String.class);
		  Object reportObj=  method.invoke(handler,projectId,folderPath,currentdatetime);
		  GsonBuilder gBuilder = new GsonBuilder();
		  fileName = gBuilder.setPrettyPrinting().create().toJson(reportObj);
		} catch (InstantiationException e) {
			logger.error("error while instantiation class PdfHandler");
		} catch (IllegalAccessException e) {
			logger.error("error while instantiation class PdfHandler");
		} catch (ClassNotFoundException e) {
			logger.error("error while getting class PdfHandler ");
		} catch (NoSuchMethodException e) {
			logger.error("error while invoking method generatePdfReportForRun");
		}   catch (InvocationTargetException e) {
			logger.error("Error ", e);
			logger.error("error while instantiation class PdfHandler");
		}
	
	return fileName;
	

}
/*Added by Ashiki for TENJINCG-1211 end*/
}

