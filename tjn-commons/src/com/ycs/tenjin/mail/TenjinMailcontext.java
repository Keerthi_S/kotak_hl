/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: TenjinMailcontext.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 31-01-2018			    Padmavathi		        Newly added for TENJINCG-579
 11-04-2018				Preeti					TENJINCG-628
 13-04-2018				Preeti					Modified for TENJINCG-628
 13-04-2018             Padmavathi              TENJINCG-627
 20-04-2018             Padmavathi              TENJINCG-627
 25-09-2018             Padmavathi              To handle API execution
 12-02-2019				Pushpalatha  			TENJINCG-930
 06-03-2019             Leelaprasad             TENJINCG-993
 15-03-2019             padmavathi              TENJINCG-948
 28-03-2019             Padmavathi              TNJN27-18
 02-05-2019				Roshni					TENJINCG-1046
 06-05-2019				Roshni					TENJINCG-1036
 24-06-2019             Padmavathi              for license
 21-08-2019				Prem					TENJINCG-1100
 02-06-2020				Ruksar					tenj210-47
 * */



package com.ycs.tenjin.mail;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.license.License;
import com.ycs.tenjin.license.LicenseComponent;

public class TenjinMailcontext {
	private static final Logger logger = LoggerFactory.getLogger(TenjinMailcontext.class);

	public String contextBuilder(String run) throws JSONException{
		logger.info("preparing content for mail");
		JSONObject json=new JSONObject(run);
		/*Changed by leelaprasad for TENJINCG-993 starts*/
		JSONObject jsonSet=json.getJSONObject("testSet");
		JSONArray  jsonTcArray=jsonSet.getJSONArray("tests");
		/*Changed by leelaprasad for TENJINCG-993 ends*/
		try{ 
	     json.getString("browser_type");
	     /*Changed by leelaprasad for TENJINCG-993 starts*/
	     String browser="";
	     if(json.getString("browser_type").equalsIgnoreCase("APPDEFAULT")) {
	    	 browser=this.buildBrowser(jsonTcArray);
	     }
	     /*Added by Padmavathi for TNJN27-18 starts*/
	     else{
	    	 browser=json.getString("browser_type");
	     }
	     /*Added by Padmavathi for TNJN27-18 ends*/
	     json.put("browser_type",browser);
	     /*Changed by leelaprasad for TENJINCG-993 ends*/
	     /* To handle Api execution starts*/
	     json.getString("machine_ip");
	     /* To handle Api execution ends*/
	    }catch(Exception e){
		json.put("browser_type", "N/A");
		json.put("machine_ip","N/A");
		}
	String content=	"<html>"+
				"    <head>"+
				"        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"+
				"        <title>Tenjin Test Execution Report</title>"+
				""+
				""+
				"    </head>"+
				"    <body style=\"font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; color: #7b7b7b;\">"+
				"        <table id=\"mail-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color: #f7f7f7; border-radius: 5px; padding: 20px; border: 1px solid #ddd;\" bgcolor=\"#f7f7f7\">"+
				"            <tr>"+
				"              <td>"+
				"                  <table id=\"mail-header\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">"+
				"                      <tr>"+
				"                          <td class=\"muted\" style=\"border-bottom-width: 1px; border-bottom-color: #ccc; border-bottom-style: solid;\">"+
				 								/*Modified by Padmavathi for TENJINCG-948 starts*/
			/*	"                              <span class=\"title-muted\" style=\"color: #7b7b7b; text-transform: uppercase;\">Tenjin Test Execution Report </span>"+*/
				"                              <span class=\"title-muted\" style=\"color: #7b7b7b; text-transform: uppercase;\">Tenjin Test Execution Report by</span>"+
				"                              <span class=\"title-muted\" style=\"text-transform: uppercase;\">"+json.getString("user")+ "</span>"+						
												/*Modified by Padmavathi for TENJINCG-948 ends*/
				"                          </td>"+
				"                      </tr>"+
				"                      <tr>"+
				"                          <td style=\"padding-top: 10px;\">"+
				"                              <span class=\"title-normal\" style=\"text-transform: capitalize; font-weight: bold; color: #3b73af; font-size: 1.3em;\">"+jsonSet.getString("name")+"</span>";
	
								if(json.getString("status")!=null&&json.getString("status").equalsIgnoreCase("Error")){
									content+=" <span class=\"label label-error\" style=\"display: inline; font-size: 75%; font-weight: 700; line-height: 1; color: #000; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em; text-transform: uppercase; position: relative; bottom: 1px; background-color: #FFAB00; padding: 0.3em 0.6em;\">"+json.getString("status")+"</span>";
		
								}else if(json.getString("status")!=null&&json.getString("status").equalsIgnoreCase("Pass")){
									content+=	"<span class=\"label label-pass\" style=\"display: inline; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em; text-transform: uppercase; position: relative; bottom: 1px; background-color: #36B37E; padding: 0.3em 0.6em;\">"+json.getString("status")+"</span>";}
								else if(json.getString("status")!=null&&json.getString("status").equalsIgnoreCase("Fail")){
									content+=	"<span class=\"label label-fail\" style=\"display: inline; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em; text-transform: uppercase; position: relative; bottom: 1px; background-color: #FF5630; padding: 0.3em 0.6em;\">"+json.getString("status")+"</span>";}
	
		
	
	
								content+=	"                          </td>"+
				"                        </tr>"+
							/*Commented by Padmavathi for TENJINCG-948 starts*/	
				/*"                        <tr>"+
				"                            <td>"+
				"                                <span>Executed by </span><span class=\"text-focus\" style=\"font-weight: bold;\">"+json.getString("user")+"</span> <span> on </span> <span class=\"text-focus\" style=\"font-weight: bold;\">"+json.getString("startTimeStamp")+"</span>"+
				"                            </td>"+
				"                        </tr>"+*/
							/*Commented by Padmavathi for TENJINCG-948 ends*/	
				"                  </table>"+
				"              </td>"+
				"            </tr>"+
				"            <tr>"+
				"                <td class=\"nopadding\">"+
				"                    <hr style=\"margin-top: 5px; margin-bottom: 5px; border-top-style: solid; border-top-color: #eeeeee; border-bottom-style: solid; border-bottom-color: #ffffff; border-width: 1px 0;\">"+
				"                </td>"+
				"            </tr>"+
				"            <tr>"+
				"                <td>"+
				"                    <table id=\"mail-body\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color: #fff; border-radius: 5px; padding: 10px; border: 1px solid #ddd;\" bgcolor=\"#fff\">"+
				"                        <tr>"+
				"                            <td>"+
				"                                <table class=\"data-grid\" id=\"run-summary-table\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-radius: 3px; background-color: #fff; margin-top: 10px; border: 1px solid #ccc;\" bgcolor=\"#fff\">"+
				"                                    <tr>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Run ID</th>"+
				"                                        <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+json.getString("id")+"</td>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Status</th>"+
				"                                        <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+json.getString("status")+"</td>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Total Tests</th>"+
				"                                        <td class=\"focused default\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; color: #fff; background-color: #A5ADBA; padding: 5px;\" align=\"center\" bgcolor=\"#A5ADBA\">"+json.getString("totalTests")+"</td>"+
				"                                    </tr>"+
				"                                    <tr>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Project</th>"+
				"                                        <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+json.getString("projectName")+"</td>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Browser</th>"+
				"                                        <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+json.getString("browser_type")+"</td>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Passed Tests</th>"+
				"                                        <td class=\"focused pass\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; color: #fff; background-color: #36B37E; padding: 5px;\" align=\"center\" bgcolor=\"#36B37E\">"+json.getString("passedTests")+"</td>"+
				"                                    </tr>"+
				"                                    <tr>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Domain</th>"+
				"                                        <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+json.getString("domainName")+"</td>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Host</th>"+
				"                                        <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+json.getString("machine_ip")+"</td>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Failed Tests</th>"+
				"                                        <td class=\"focused fail\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; color: #fff; background-color: #FF5630; padding: 5px;\" align=\"center\" bgcolor=\"#FF5630\">"+json.getString("failedTests")+"</td>"+
				"                                    </tr>"+
				"                                    <tr>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Start Time</th>"+
				"                                        <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+json.getString("startTimeStamp")+"</td>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">End Time</th>"+
				                                            /*Modified by Ruksar for Tenj210-47 starts*/
				"                                        <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+json.getString("endTimeStamp")+"</td>"+
				                                             /*Modified by Ruksar for Tenj210-47 ends*/
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Error Tests</th>"+
				"                                        <td class=\"focused error\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; color: #000; background-color: #FFAB00; padding: 5px;\" align=\"center\" bgcolor=\"#FFAB00\">"+json.getString("erroredTests")+"</td>"+
				"                                    </tr>"+
				"                                    <tr>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Elapsed Time</th>"+
				"                                        <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+json.getString("elapsedTime")+"</td>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Executed By</th>"+
				"                                        <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+json.getString("user")+"</td>"+
				"                                        <th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\"></th>"+
				"                                        <td class=\"focused error\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; color: #000; background-color: #ddd; padding: 5px;\" align=\"center\" bgcolor=\"#FFAB00\"></td>"+
				"                                    </tr>"+
				"                                </table>"+
				"                            </td>"+
				"                        </tr>"+
				"                        <tr>"+
				"                            <td>"+
				"                                <table id=\"tc-summary-title\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">"+
				"                                    <tr>"+
				"                                        <td class=\"section-title-holder\" style=\"padding-top: 30px;\"><span class=\"section-title\" style=\"text-transform: uppercase; font-weight: bold; color: #3b73af;\">Summary of Test Cases</span></td>"+
				"                                    </tr>"+
				"                                </table>"+
				"                            </td>"+
				"                        </tr>"+
				"                        <tr>"+
				"                            <td>"+
				"                                <table class=\"data-grid\" id=\"tc-summary\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-radius: 3px; background-color: #fff; margin-top: 10px; border: 1px solid #ccc;\" bgcolor=\"#fff\">"+
				"                                    <thead>"+
				"                                        <tr>"+
				"                                            <th style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-color: #ccc; border-right-width: 1px; padding: 5px; border-right-style: solid; background-color: #ddd; color: #000;\" bgcolor=\"#ddd\">Test Case ID</th>"+
				"                                            <th style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-color: #ccc; border-right-width: 1px; padding: 5px; border-right-style: solid; background-color: #ddd; color: #000;\" bgcolor=\"#ddd\">Name</th>"+
				"                                            <th style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-color: #ccc; border-right-width: 1px; padding: 5px; border-right-style: solid; background-color: #ddd; color: #000;\" bgcolor=\"#ddd\">Total Steps</th>"+
				"                                            <th style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-color: #ccc; border-right-width: 1px; padding: 5px; border-right-style: solid; background-color: #ddd; color: #000;\" bgcolor=\"#ddd\">Passed</th>"+
				"                                            <th style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-color: #ccc; border-right-width: 1px; padding: 5px; border-right-style: solid; background-color: #ddd; color: #000;\" bgcolor=\"#ddd\">Failed</th>"+
				"                                            <th style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-color: #ccc; border-right-width: 1px; padding: 5px; border-right-style: solid; background-color: #ddd; color: #000;\" bgcolor=\"#ddd\">Error</th>"+
				"                                            <th style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-color: #ccc; border-right-width: 1px; padding: 5px; border-right-style: solid; background-color: #ddd; color: #000;\" bgcolor=\"#ddd\">Status</th>"+
				"                                        </tr>"+
				"                                    <thead>"+
				"                                    </thead>"+
				"</thead>"+
				"<tbody>";
				
								for(int i=0;i<jsonTcArray.length();i++){
									JSONObject tcJson=jsonTcArray.getJSONObject(i);
									int stepSize=tcJson.getJSONArray("tcSteps").length();
									 
				
					content+=     "	<tr>";
												
						
					content+=	" <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;text-align: center;\">"+tcJson.getString("tcId")+"</td>"+
				"							 <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px; text-align: center;\">"+tcJson.getString("tcName")+"</td>"+
				"							 <td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\">"+stepSize+"</td>"+
				"							 <td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\">"+tcJson.getString("totalPassedSteps")+"</td>"+
				"							 <td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\">"+tcJson.getString("totalFailedSteps")+"</td>"+
				"							 <td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\">"+tcJson.getString("totalErrorredSteps")+"</td>";
				
				if(tcJson.getString("tcStatus")!=null&&tcJson.getString("tcStatus").equalsIgnoreCase("Error")){
					content+="<td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\"><span class=\"label label-pass\" style=\"display: inline; font-size: 75%; font-weight: 700; line-height: 1; color: #000; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em; text-transform: uppercase; position: relative; bottom: 1px; background-color: #FFAB00; padding: 0.3em 0.6em;\">"+tcJson.getString("tcStatus")+"</span></td>";}
				else if(tcJson.getString("tcStatus")!=null&&tcJson.getString("tcStatus").equalsIgnoreCase("Pass")){
					content+=	"<td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\"><span class=\"label label-pass\" style=\"display: inline; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em; text-transform: uppercase; position: relative; bottom: 1px; background-color: #36B37E; padding: 0.3em 0.6em;\">"+tcJson.getString("tcStatus")+"</span></td>";}
				else if(tcJson.getString("tcStatus")!=null&&tcJson.getString("tcStatus").equalsIgnoreCase("Fail")){
					content+=	"<td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\"><span class=\"label label-fail\" style=\"display: inline; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em; text-transform: uppercase; position: relative; bottom: 1px; background-color: #FF5630; padding: 0.3em 0.6em;\">"+tcJson.getString("tcStatus")+"</span></td>";}
				content+="						</tr>";
				}
				
											content+= "</tbody>"+
				"                                </table>"+
				"                        <tr>"+
				"                            <td>"+
				"                                <table id=\"tc-summary-title\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">"+
				"                                    <tr>"+
				"                                        <td class=\"section-title-holder\" style=\"padding-top: 30px;\"><span class=\"section-title\" style=\"text-transform: uppercase; font-weight: bold; color: #3b73af;\">Summary of Validations</span></td>"+
				"                                    </tr>"+
				"                                </table>"+
				"                            </td>"+
				"                        </tr>"+
				"                        <tr>"+
				"                            <td>"+
				"                                <table class=\"data-grid\" id=\"tc-summary\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-radius: 3px; background-color: #fff; margin-top: 10px; border: 1px solid #ccc;\" bgcolor=\"#fff\">"+
				"                                    <thead>"+
				"                                        <tr>"+
				"                                            <th style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-color: #ccc; border-right-width: 1px; padding: 5px; border-right-style: solid; background-color: #ddd; color: #000;\" bgcolor=\"#ddd\">Test Case ID</th>"+
				"                                            <th style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-color: #ccc; border-right-width: 1px; padding: 5px; border-right-style: solid; background-color: #ddd; color: #000;\" bgcolor=\"#ddd\">Name</th>"+
				"                                            <th style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-color: #ccc; border-right-width: 1px; padding: 5px; border-right-style: solid; background-color: #ddd; color: #000;\" bgcolor=\"#ddd\">Total Validations</th>"+
				"                                            <th style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-color: #ccc; border-right-width: 1px; padding: 5px; border-right-style: solid; background-color: #ddd; color: #000;\" bgcolor=\"#ddd\">Passed</th>"+
				"                                            <th style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-color: #ccc; border-right-width: 1px; padding: 5px; border-right-style: solid; background-color: #ddd; color: #000;\" bgcolor=\"#ddd\">Failed</th>"+
				"                                            <th style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-color: #ccc; border-right-width: 1px; padding: 5px; border-right-style: solid; background-color: #ddd; color: #000;\" bgcolor=\"#ddd\">Status</th>"+
				"                                        </tr>"+
				"                                    <thead>"+
				"                                    </thead>"+
				"</thead>"+
				"<tbody>";
				boolean flag=true;
				for(int i=0;i<jsonTcArray.length();i++){
					JSONObject tcJson=jsonTcArray.getJSONObject(i);
					JSONArray jsonTstepArray=tcJson.getJSONArray("tcSteps");
					
						Map<String,String> map=this.validationResults(jsonTstepArray);
						if(Integer.parseInt(map.get("validationResultCount"))!=0){
							flag=false;
							content+=     "	<tr>";
														
							/*if(map.get("status")!=null&&map.get("status").equalsIgnoreCase("Pass")){
								content+=	"<td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\"><span class=\"label label-pass\" style=\"display: inline; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em; text-transform: uppercase; position: relative; bottom: 1px; background-color: #36B37E; padding: 0.3em 0.6em;\">Pass</span></td>";}*/
						/*	else if(map.get("status")!=null&&map.get("status").equalsIgnoreCase("Fail")){
								content+=	"<td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\"><span class=\"label label-fail\" style=\"display: inline; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em; text-transform: uppercase; position: relative; bottom: 1px; background-color: #FF5630; padding: 0.3em 0.6em;\">Fail</span></td>";}	*/
								content+=	" <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px; text-align: center;\">"+tcJson.getString("tcId")+"</td>"+
								"							 <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px; text-align: center;\">"+tcJson.getString("tcName")+"</td>"+
								"							 <td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\">"+map.get("validationResultCount")+"</td>"+
								"							 <td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\">"+map.get("passCount")+"</td>"+
								"							 <td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\">"+map.get("failCount")+"</td>";
								if(map.get("status")!=null&&map.get("status").equalsIgnoreCase("Pass")){
									content+=	"<td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\"><span class=\"label label-pass\" style=\"display: inline; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em; text-transform: uppercase; position: relative; bottom: 1px; background-color: #36B37E; padding: 0.3em 0.6em;\">Pass</span></td>";}
								else if(map.get("status")!=null&&map.get("status").equalsIgnoreCase("Fail")){
									content+=	"<td class=\"focused\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\"><span class=\"label label-fail\" style=\"display: inline; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em; text-transform: uppercase; position: relative; bottom: 1px; background-color: #FF5630; padding: 0.3em 0.6em;\">Fail</span></td>";}
								content+="						</tr>";
																		
																	
							}
						
					}
					if(flag){
						content+= "                    </table>"+
								" <table  width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"padding-top:10px;\">"+
								" <tr>"+
								" <td>"+
									" <span class=\"text-focus\" style=\"border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-width: 1px; border-right-color: #ccc; border-right-style: solid; text-align: center; font-weight: bold; padding: 5px;\" align=\"center\"\">No validations for this run</span>"+
									" </td>"+
									" </tr>"+
									"</table>";
					}
					content+="</tbody>";
														
					content+="                           </table>"+
							"                            </td>"+
							"                        </tr>"+
							"                    </table>"+
							"					 <table id=\"mail-footer\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"padding-top: 40px;\">"+
							"           			<tr>"+
							"               			<td class=\"muted\" style=\"border-bottom-width: 1px; border-bottom-color: #ccc; border-bottom-style: solid;\">"+
							"                   			<span class=\"title-muted\" style=\"color: #2F4F4F;font-size: 13px;\">&copy Copyright "+ new java.text.SimpleDateFormat("yyyy").format(new java.util.Date()) +"<a href='http://www.yethi.in' target='_blank'> Yethi Consulting Pvt. Ltd.</a> All Rights Reserved</span>"+
							"                    		</td>"+
							"              			</tr>"+
							"					</table>"+
							"                </td>"+
							"            </tr>"+
							"        </table>"+
							"    </body>"+
							"</html>";
		
		return content;
	}
	/*Changed by leelaprasad for TENJINCG-993 starts*/
	private String buildBrowser(JSONArray jsonTcArray) {
		String browser="";
		List<String> browsers=new ArrayList<String>();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
			try {
				for(int i=0;i<jsonTcArray.length();i++){
			    JSONObject tcJson=jsonTcArray.getJSONObject(i);
				JSONArray jsonTstepArray=tcJson.getJSONArray("tcSteps");
			
				for(int j=0;j<jsonTstepArray.length();j++){
					JSONObject stepJson=jsonTstepArray.getJSONObject(j);
					int appId=stepJson.getInt("appId");
					TenjinReflection tenjinReflection =new TenjinReflection();
					String defBrowserName=tenjinReflection.getDeafultAppBrowser(appId);
					defBrowserName=defBrowserName.trim().substring(1, defBrowserName.length()-1);
					if(!(browsers.contains(defBrowserName))){
						browsers.add(defBrowserName);
					}
					
				}
				
				}
				if(browsers.size()>1) {
				
					for(String browserName:browsers) {
						if(browser.equalsIgnoreCase("")) {
							browser=browserName;
						}else {
							browser=browser+","+browserName;
						}
						
					}
				}else {
					browser=browsers.get(0);
				}
			} catch (JSONException e) {
				
				logger.error("Error ", e);
			
		}
		
		return browser;
	}
	/*Changed by leelaprasad for TENJINCG-993 ends*/
	/*Added by Padmavathi for TENJINCG-627 starts*/
	public Map<String,String> validationResults(JSONArray jsonTstepArray) throws JSONException{
		int passCount=0;
		int failCount=0;
		String status="Pass";
		int validationResultCount=0;
		Map<String,String> map=new HashMap<String,String>();
		for(int i=0;i<jsonTstepArray.length();i++){
			JSONObject stepJson=jsonTstepArray.getJSONObject(i);
			JSONArray detailedStepResults=stepJson.getJSONArray("detailedResults");
				for(int j=0;j<detailedStepResults.length();j++){
					JSONObject detailedStepResult=detailedStepResults.getJSONObject(j);
					JSONArray validationResults=detailedStepResult.getJSONArray("validationResults");
				     validationResultCount+=detailedStepResult.getJSONArray("validationResults").length();
				     for(int k=0;k<validationResults.length();k++){
				    	 JSONObject validationResult=validationResults.getJSONObject(k);
				    	 if(validationResult.getString("status").equalsIgnoreCase("Pass")){
				    		 passCount++;
				    	 }else if(validationResult.getString("status").equalsIgnoreCase("Fail")){
				    		 failCount++;
				    		 }
				     }
				}
		}
				map.put("validationResultCount",String.valueOf(validationResultCount));
				map.put("passCount", String.valueOf(passCount));
				map.put("failCount", String.valueOf(failCount));
				if(failCount>=1){
					status="Fail";
				}
				map.put("status",status);
		return map;
		
	}
	/*Added by Padmavathi for TENJINCG-627 ends*/
	/* Added by Roshni for TENJINCG-1046 starts */
	public String genericContextBuilder(JSONObject mailContent) throws JSONException{
		String entityType=mailContent.getString("entityType");
		String entityId=mailContent.getString("entityId");
		String entityName=mailContent.getString("entityName");
		String userName=mailContent.getString("userName");
		String operation=mailContent.getString("operation");
		String dateTime=mailContent.getString("dateTime");
		String operationString="",color="";
		String parentId=mailContent.getString("parentId");
		
		if(operation.equalsIgnoreCase("update")){
			operationString="Updated";
			color="#FFAB00";
		}
		else if(operation.equalsIgnoreCase("delete")){
			operationString="Deleted";
			color="#FF5630";
		}
		else if(operation.equalsIgnoreCase("cancel")){
			operationString="Cancelled";
			color="#FF5630";
		}
		else if(operation.equalsIgnoreCase("remove")){
			operationString="Removed";
			color="#FF5630";
			
		}
		else if(operation.equalsIgnoreCase("create")){
			operationString="Created";
			color="#36B37E";
		}
		else if(operation.equalsIgnoreCase("post")){
			operationString="Posted";
			color="#36B37E";
		}
		else if(entityType.toLowerCase().contains("schedule")&&!entityType.equalsIgnoreCase("scheduled task")){
			operationString=operation+" initiated with scheduler "+entityName;
			color="#36B37E";
		}
		String content=
		"			<html>"+
		"				<body style=\"font-family:'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; color: #7b7b7b;\">"+
		"					<table id=\"mail-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color: #f7f7f7; border-radius: 5px; padding: 20px; border: 1px solid #ddd;\" bgcolor=\"#f7f7f7\">"+
		"						<tr>"+
		"							<td>"+
		"								<table id=\"mail-header\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">"+
        "            						<tr>"+
        "                						<td style=\"padding-top: 10px;\">"+
		"                    						<span class=\"title-normal\" style=\"text-transform: capitalize; font-weight: bold; color: #3b73af; font-size: 1.3em;\">Tenjin Entity Status</span> "+
		"											<span class=\"label \" style=\"display: inline; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em; text-transform: uppercase; position: relative; bottom: 1px; background-color: "+color+"; padding: 0.3em 0.6em;\">"+operationString+"</span>"+
        "                						</td>"+
        "           						</tr>"+
        "        						</table>"+
        "    						</td>"+
        "						</tr>"+
        "						<tr>"+
        "   						<td class=\"nopadding\">"+
        "        						<hr style=\"margin-top: 5px; margin-bottom: 5px; border-top-style: solid; border-top-color: #eeeeee; border-bottom-style: solid; border-bottom-color: #ffffff; border-width: 1px 0;\"> </td>"+
        "        				</tr>"+
        "        				<tr>"+
        "        					<td>"+
        "        						<table id=\"mail-body\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color: #fff; border-radius: 5px; padding: 10px; border: 1px solid #ddd;\" bgcolor=\"#fff\">"+
        "        							<tr>"+
        "        								<td>"+
        "                    						<table class=\"data-grid\" id=\"run-summary-table\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-radius: 3px; background-color: #fff; margin-top: 10px; border: 1px solid #ccc;\" bgcolor=\"#fff\">"+
        "                        						<tr>"+
        "                            						<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Entity Type</th>"+
        "                            						<td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+entityType+"</td>"+
        "                       						</tr>"+
        "                        						<tr>"+
        "                            						<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Entity Id</th>"+
        "                            						<td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+entityId+"</td>"+
        "                        						</tr>";
														if(parentId!=null && !parentId.equalsIgnoreCase("") && !parentId.equalsIgnoreCase("null")){
															content+="<tr>"+
        "                            						<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Parent Id</th>"+
        "                            						<td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+parentId+"</td>"+
        "                        						</tr>";}
                              							content+="<tr>"+
        "                            						<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Entity Name</th>"+
        "                            						<td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+entityName+"</td>"+
        "                        						</tr>"+
        "                        						<tr>"+
        "                            						<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Operation</th>"+
        "                            						<td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+operation+"</td>"+
        "                        						</tr>";

														if(userName!=null && !userName.equalsIgnoreCase("") && !userName.equalsIgnoreCase("null")){
                                							content+="<tr>"+
		"                             						<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">"+operationString+" By</th>"+
		"                             						<td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+userName+"</td>"+
		"                      							</tr>";}
														content+="<tr>"+
		"                             						<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">"+operationString+" On</th>"+
		"                             						<td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+dateTime+"</td>"+
		"                      							</tr>"+
        "                    					</table>"+
        "                					</td>"+
        "            					</tr>"+
		"							</table>"+
		"							<table id=\"mail-footer\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"padding-top: 40px;\">"+
		"           					<tr>"+
		"               					<td class=\"muted\" style=\"border-bottom-width: 1px; border-bottom-color: #ccc; border-bottom-style: solid;\">"+
		"                   					<span class=\"title-muted\" style=\"color: #2F4F4F;font-size: 13px;\">&copy Copyright "+ new java.text.SimpleDateFormat("yyyy").format(new java.util.Date()) +"<a href='http://www.yethi.in' target='_blank'> Yethi Consulting Pvt. Ltd.</a> All Rights Reserved</span>"+
		"                    				</td>"+
		"              					</tr>"+
		"							</table>"+
        "   	 				</td>"+
		"					</tr>"+
		"				</table>"+
		"			</body>"+
		"		</html>";
		
		return content;
	}
	/* Added by Roshni for TENJINCG-1046 ends */
	
	/* Added by Roshni for TENJINCG-1036 starts */
	public String passwordResetCotext(JSONObject mailContent) throws JSONException {
		
		String entityId=mailContent.getString("entityId");
		String entityName=mailContent.getString("entityName");
		String userName=mailContent.getString("userName");
//		 Calendar calendar = Calendar.getInstance();
		Date today = new Date();
		
		
		String content=
						"			<html>"+
						"				<body style=\"font-family:'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 11px; color: #7b7b7b;\">"+
						"					<table id=\"mail-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color: #f7f7f7; border-radius: 5px; padding: 20px; border: 1px solid #ddd;\" bgcolor=\"#f7f7f7\">"+
						"						<tr>"+
						"							<td>"+
						"								<table id=\"mail-header\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">"+
				        "            						<tr>"+
				        "                						<td style=\"padding-top: 10px;\">"+
						"                    						<span class=\"title-normal\" style=\"text-transform: capitalize;color: #3b73af; font-size: 1.3em;\">Hello "+userName+","+
						"											<br><br>We received a request to reset your Tenjin password."+
						/*Added by Prem for TENJINCG-1100 start*/
						/*"											<br>Please click <a style=\"color:red;\" href=\""+entityName+"TenjinPasswordResetServlet?param=resetpwdscreen&userid="+entityId>here</a> to reset your password </span> "+*/
						"											<br>Please click <a style=\"color:red;\" href=\""+entityName+"TenjinPasswordResetServlet?param=resetpwdscreen&userid="+entityId+"&username="+userName+"&time="+today+" \">here</a> to reset your password <br> This link will expiry in 24 hours</span> "+							
						/*Added by Prem for TENJINCG-1100 ends*/
						"                						</td>"+
				        "           						</tr>"+
				        "        						</table>"+
				        "    						</td>"+
				        "						</tr>"+
				        "					<tr><td><table id=\"mail-footer\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"padding-top: 40px;\">"+
				        "           					<tr>"+
				        "               					<td class=\"muted\" style=\"border-bottom-width: 1px; border-bottom-color: #ccc; border-bottom-style: solid;\">"+
				        "                  					<span class=\"title-muted\" style=\"color: #2F4F4F;font-size: 13px;\">&copy Copyright "+ new java.text.SimpleDateFormat("yyyy").format(new java.util.Date()) +"<a href='http://www.yethi.in' target='_blank'> Yethi Consulting Pvt. Ltd.</a> All Rights Reserved</span>"+
				        "                    				</td>"+
				        "              					</tr>"+
				        "							</table></td></tr>"+
				        "					</table>"+
				        "				</body>"+
						"			</html>";
		return content;
	}
	/* Added by Roshni for TENJINCG-1036 ends */
	
	/*Added by Padmavathi for license starts*/
	public String buildLicenseMailContext(License license,String action){
		SimpleDateFormat formatter=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String content=
				"			<html>"+
				"				<body style=\"font-family:'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; color: #7b7b7b;\">"+
				"					<table id=\"mail-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color: #f7f7f7; border-radius: 5px; padding: 20px; border: 1px solid #ddd;\" bgcolor=\"#f7f7f7\">"+
				"						<tr>"+
				"							<td>";
							if(action!=null&&(action.equalsIgnoreCase("Modified")||action.equalsIgnoreCase("Renewed"))){
									content+="<table id=\"mail-header\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">"+
		        "            						<tr>"+
		        "                						<td style=\"padding-top: 10px;\">"+
				"                    						<span class=\"title-normal\" style=\"text-transform: capitalize; font-weight: bold; color: #3b73af; font-size: 1.3em;\">License "+action+" Successfully</span> "+
		        "                						</td>"+
		        "           						</tr>"+
		        "            						<tr>"+
		        "                						<td style=\"padding-top:9px;\">"+
				"                    						<span class=\"title-normal\" style=\"text-transform: capitalize;  font-size: 1.2em;\">License details</span> "+
		        "                						</td>"+
		        "           						</tr>"+
		        "        						</table>";
		        }else if(action!=null && action.equalsIgnoreCase("LicenseWillExpire")){
		        	content+="<table id=\"mail-header\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">"+
		    		        "            						<tr>"+
		    		        "                						<td style=\"padding-top: 10px;\">"+
		    				"                    						<span class=\"title-normal\" style=\" font-size: 1.1em;\">Your license will expire on "+formatter.format(license.getEndDate()) + ". Please renew your license.</span> "+
		    		        "                						</td>"+
		    		        "           						</tr>"+
		    		        "            						<tr>"+
		    		        "                						<td style=\"padding-top:9px;\">"+
		    				"                    						<span class=\"title-normal\" style=\"text-transform: capitalize;  font-size: 1em;\">License details</span> "+
		    		        "                						</td>"+
		    		        "           						</tr>"+
		    		        "        						</table>";
		        }else if(action!=null && action.equalsIgnoreCase("LicenseExpired")){
		        	content+="<table id=\"mail-header\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">"+
		    		        "            						<tr>"+
		    		        "                						<td style=\"padding-top: 10px;\">"+
		    				"                    						<span class=\"title-normal\" style=\"  font-size: 1.1em;\">Your License has been expired. You have " + license.getGracePeriod() + " day(s) to renew your license.</span> "+
		    		        "                						</td>"+
		    		        "           						</tr>"+
		    		        "            						<tr>"+
		    		        "                						<td style=\"padding-top:9px;\">"+
		    				"                    						<span class=\"title-normal\" style=\"text-transform: capitalize;  font-size: 1em;\">License details</span> "+
		    		        "                						</td>"+
		    		        "           						</tr>"+
		    		        "        						</table>";
		        }
							content+="</td>"+
		        "						</tr>"+
		        "						<tr>"+
		        "   						<td class=\"nopadding\">"+
		        "        						<hr style=\"border-top-style: solid; border-top-color: #eeeeee; border-bottom-style: solid; border-bottom-color: #ffffff; border-width: 1px 0;\"> </td>"+
		        "        				</tr>"+
		        "        				<tr>"+
		        "        					<td>"+
		        "        						<table id=\"mail-body\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color: #fff; border-radius: 5px; padding: 10px; border: 1px solid #ddd;\" bgcolor=\"#fff\">"+
		        "        							<tr>"+
		        "        								<td>"+
		        "                    						<table class=\"data-grid\" id=\"run-summary-table\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-radius: 3px; background-color: #fff; margin-top: 10px; border: 1px solid #ccc;\" bgcolor=\"#fff\">"+
		        "                        						<tr>"+
		        "                            						<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Company Name</th>"+
		        "                            						<td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+license.getCustomer().getName()+"</td>"+
		        "                       						</tr>"+
		        "                        						<tr>"+
		        "                            						<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Start Date</th>"+
		        "                            						<td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+formatter.format(license.getStartDate())+"</td>"+
		        "                        						</tr>"+
																"<tr>"+
		        "                            						<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">End Date</th>"+
		        "                            						<td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+formatter.format(license.getEndDate())+"</td>"+
		        "                        						</tr>"+
		                              							"<tr>"+
		        "                            						<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Grace Period</th>"+
		        "                            						<td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+license.getGracePeriod()+"</td>"+
		        "                        						</tr>"+
		        "                        						<tr>"+
		        "                            						<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Core Limit</th>"+
		        "                            						<td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+license.getCoreLimit()+"</td>"+
		        "                        						</tr>";

														for(LicenseComponent component:license.getLicenseComponents() ){
															content+="<tr>"+
			    "                             							<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">"+component.getProductComponent().getName()+" Adapter</th>"+
			    "                             							<td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+component.getLimit()+"</td>"+
			    "                      								</tr>";
														}
										content+=  " </table>"+
		        "                					</td>"+
		        "            					</tr>"+
				"							</table>"+
				"							<table id=\"mail-footer\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"padding-top: 40px;\">"+
				"           					<tr>"+
				"               					<td class=\"muted\" style=\"border-bottom-width: 1px; border-bottom-color: #ccc; border-bottom-style: solid;\">"+
				"                   					<span class=\"title-muted\" style=\"color: #2F4F4F;font-size: 13px;\">&copy Copyright "+ new java.text.SimpleDateFormat("yyyy").format(new java.util.Date()) +"<a href='http://www.yethi.in' target='_blank'> Yethi Consulting Pvt. Ltd.</a> All Rights Reserved</span>"+
				"                    				</td>"+
				"              					</tr>"+
				"							</table>"+
		        "   	 				</td>"+
				"					</tr>"+
				"				</table>"+
				"			</body>"+
				"		</html>";
		return content;
	}
	/*Added by Padmavathi for license ends*/
	/*Added by Ashiki for TENJINCG-1211 starts*/
	public String consolidatedcontextBuilder(String runJson, String currentUser, JSONObject jsonProject) throws JSONException {
		
		String content=	"<html>"+
				"    <head>"+
				"        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"+
				"        <title>Tenjin Test Execution Report</title>"+
				""+
				""+
				"    </head>"+
				"    <body style=\"font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; color: #7b7b7b;\">"+
				"        <table id=\"mail-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color: #f7f7f7; border-radius: 5px; padding: 20px; border: 1px solid #ddd;\" bgcolor=\"#f7f7f7\">"+
				"            <tr>"+
				"              <td>"+
				"						<table id=\"mail-header\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">"+
				"                      		<tr>"+
				"                          		<td class=\"muted\" style=\"border-bottom-width: 1px; border-bottom-color: #ccc; border-bottom-style: solid;\">"+
				"                              		<span class=\"title-muted\" style=\"color: #7b7b7b; text-transform: uppercase;\">Tenjin Test Execution Report by</span>"+
				"                              		<span class=\"title-muted\" style=\"text-transform: uppercase;\">"+currentUser+ "</span>"+						
				"                          		</td>"+
				"                      		</tr>"+
				"				</td>"+
				"			 </tr>"+
				"            <tr>"+
				"                <td>"+
				"                    <table id=\"mail-body\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color: #fff; border-radius: 5px; padding: 10px; border: 1px solid #ddd;\" bgcolor=\"#fff\">"+
				"                        <tr>"+
				"                            <td>"+
				"                                <table class=\"data-grid\" id=\"run-summary-table\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-radius: 3px; background-color: #fff; margin-top: 10px; border: 1px solid #ccc;\" bgcolor=\"#fff\">"+
				"                                    <tr>"+
				"										<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 3px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Domain</th>"+
				"                                        <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+jsonProject.getString("domain")+"</td>"+
				"									 </tr>"+
				"								 </table>"+
				"								 <table class=\"data-grid\" id=\"run-summary-table\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-radius: 3px; background-color: #fff; margin-top: 10px; border: 1px solid #ccc;\" bgcolor=\"#fff\">"+
				"                                    <tr>"+
				"										<th class=\"inline-th\" style=\"border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ccc; border-right-width: 1px; border-right-color: #ccc; padding: 5px; border-right-style: solid; text-align: left; background-color: #ddd; color: #000;\" align=\"left\" bgcolor=\"#ddd\">Project</th>"+
				"                                       <td style=\"padding: 5px; border-bottom-style: solid; border-bottom-color: #ccc; border-bottom-width: 1px; border-right-style: solid; border-right-color: #ccc; border-right-width: 1px;\">"+jsonProject.getString("name")+"</td>"+
				"									 </tr>"+
						"								 </table>"+
				"						     </td>"+
				"						 </tr>"+
				"					  </table>"+
				"				</td>"+
				"			</tr>"+
				"		</table>"+
				"	</body>" + 
				"</html>" ;
				
				
				return content;
	}
	/*Added by Ashiki for TENJINCG-1211 end*/
}