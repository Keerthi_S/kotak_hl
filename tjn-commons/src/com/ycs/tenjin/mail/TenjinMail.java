
/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file: TenjinMail.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 23-01-2018			    Padmavathi		        Newly added for TENJINCG-579
 14-05-2018				Pushpalatha				TENJINCG-629
 22-03-2019             Padmavathi              TENJINCG-1017
 * */



package com.ycs.tenjin.mail;

public class TenjinMail {

  private String smtpServer;
  private int port;
  private String senderMail;
  private String password;
  private String recipientMails;
  private String authentication;
  private String subject;
  private String senderName;
  private String ccMails;
  public String getCcMails() {
	return ccMails;
  }
  public void setCcMails(String ccMails) {
	this.ccMails = ccMails;
  }
  public String getSmtpServer() {
	return smtpServer;
  }
  public String getRecipientMails() {
	return recipientMails;
  }
  public void setRecipientMails(String recipientMails) {
	this.recipientMails = recipientMails;
  }
  public String getSubject() {
	return subject;
  }
  public void setSubject(String subject) {
	this.subject = subject;
  }
  public void setSmtpServer(String smtpServer) {
	this.smtpServer = smtpServer;
  }	
  public int getPort() {
	return port;
  }
  public void setPort(int port) {
	this.port = port;
  }
 
  public String getPassword() {
	return password;
  }
  public void setPassword(String password) {
	this.password = password;
  }
  public String getSenderMail() {
	return senderMail;
  }
  public void setSenderMail(String senderMail) {
	this.senderMail = senderMail;
  }

  public String getAuthentication() {
	return authentication;
  }
  public void setAuthentication(String authentication) {
	this.authentication = authentication;
  }
public String getSenderName() {
	return senderName;
}
public void setSenderName(String senderName) {
	this.senderName = senderName;
}
  
}
