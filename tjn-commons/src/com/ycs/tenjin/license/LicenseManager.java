/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LicenseManager.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 29-Sep-2016           Sriram Sridharan          Newly Added For 
*/

package com.ycs.tenjin.license;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

public class LicenseManager {
	private static final Logger logger = LoggerFactory.getLogger(LicenseManager.class);
	
	public static License getLicense() throws TenjinConfigurationException{
		
		logger.info("Loading license...");
		String	encryptedLisence1="";
		encryptedLisence1 = TenjinConfiguration.getProperty("TJN_LICENSE");
		
		License license = new License();
		
		try{
			logger.debug("Reading License Information");
			encryptedLisence1 = encryptedLisence1.replace("!##r!", "\r");
			encryptedLisence1 = encryptedLisence1.replace("!##n!", "\n");
			
			
			 /*Modified by paneendra for VAPT fix starts*/
			CryptoUtilities cryptoUtilities=new CryptoUtilities();
			/*String decryptedLicense= new Crypto().decrypt(encryptedLisence1.split("!#!")[1],decodeHex(encryptedLisence1.split("!#!")[0].toCharArray()));*/
			
			String decryptedLicense = cryptoUtilities.decrypt(encryptedLisence1);
			 /*Modified by paneendra for VAPT fix ends*/
			int lic=decryptedLicense.length();
	        String s2= Utilities.trim(decryptedLicense.substring(40,lic));
	        String clientName = Utilities.trim(decryptedLicense.substring(0, 39));
	        
	        license.setClientCount(Integer.parseInt(s2));
	        license.setLicensee(clientName);
	        
	        
		}catch(Exception e){
			logger.error("ERROR processing license", e);
			throw new TenjinConfigurationException("Could not validate License. Please contact Tenjin Support.");
		}
		return license;
	}
	
	public static void main(String[] args) throws TenjinConfigurationException {
		License license = LicenseManager.getLicense();
		System.err.println("Licensee --> " + license.getLicensee());
		System.err.println("Number of Clients --> " + license.getClientCount());
	}
	
	
}
