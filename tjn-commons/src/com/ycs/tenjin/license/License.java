/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  License.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 29-Sep-2016           Sriram Sridharan          Newly Added For 
* 24-06-2019            Padmavathi              for License
*/

package com.ycs.tenjin.license;

import java.util.Date;
import java.util.List;

public class License {
	
	private Long id;
	private Customer customer;
	private ProductVersion productVersion;
	private LicenseType type;
	private Integer coreLimit;
	private Date startDate;
	private Date endDate;
	private Integer gracePeriod;
	private Date relationshipSince;
	private LicenseStatus status;
	private LicenseHost host;
	private String licensee;
	private int clientCount;
	private List<LicenseComponent> licComponents; 
	private String licSupportMails;
	
	public List<LicenseComponent> getLicenseComponents() {
		return licComponents;
	}

	public void setLicenseComponents(List<LicenseComponent> licenseComponents) {
		this.licComponents = licenseComponents;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public ProductVersion getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(ProductVersion productVersion) {
		this.productVersion = productVersion;
	}

	public LicenseType getType() {
		return type;
	}

	public void setType(LicenseType type) {
		this.type = type;
	}

	public Integer getCoreLimit() {
		return coreLimit;
	}

	public void setCoreLimit(Integer coreLimit) {
		this.coreLimit = coreLimit;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getGracePeriod() {
		return gracePeriod;
	}

	public void setGracePeriod(Integer gracePeriod) {
		this.gracePeriod = gracePeriod;
	}

	public Date getRelationshipSince() {
		return relationshipSince;
	}

	public void setRelationshipSince(Date relationshipSince) {
		this.relationshipSince = relationshipSince;
	}

	public LicenseStatus getStatus() {
		return status;
	}

	public void setStatus(LicenseStatus status) {
		this.status = status;
	}

	public LicenseHost getHost() {
		return host;
	}

	public void setHost(LicenseHost host) {
		this.host = host;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		
		return this.id;
	}

	public String getLicensee() {
		return licensee;
	}

	public void setLicensee(String licensee) {
		this.licensee = licensee;
	}

	public int getClientCount() {
		return clientCount;
	}

	public void setClientCount(int clientCount) {
		this.clientCount = clientCount;
	}

	public List<LicenseComponent> getLicComponents() {
		return licComponents;
	}

	public void setLicComponents(List<LicenseComponent> licComponents) {
		this.licComponents = licComponents;
	}

	public String getLicSupportMails() {
		return licSupportMails;
	}

	public void setLicSupportMails(String licSupportMails) {
		this.licSupportMails = licSupportMails;
	}

}
