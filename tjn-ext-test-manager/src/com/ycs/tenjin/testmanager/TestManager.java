/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestManager.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 23-Jan-2017           Jyoti Ranjan          Newly Added For
 * 17-02-2019			 Ashiki					TJN252-60
 */

package com.ycs.tenjin.testmanager;

import java.util.List;
import java.util.Map;

public interface TestManager {
	// Login
	public String login() throws UnreachableETMException, ETMException;

	// Getting single TestCase
	public List<String> getTestCase() throws UnreachableETMException,
			ETMException;

	// Getting single TestStep
	public List<String> getTestStep() throws UnreachableETMException,
			ETMException;

	// Getting Multiple TestCase Based on filter.
	public List<TestCase> getTestCases(String fieldKey, String fieldValue)
			throws UnreachableETMException, ETMException;

	// Getting Multiple TestStep Based on filter.
	public List<TestStep> getTestStepsWithFilter(String fieldKey, String fieldValue)
			throws UnreachableETMException, ETMException;

	// Getting Multiple TestCase Based on filter associated to Testcase.
	public List<TestStep> getTestSteps(String fieldKey, String fieldValue,
			String tcRecordId) throws UnreachableETMException, ETMException;

	// Synchronization of TestCases Based on their Id.
	public List<TestCase> SynchronizeTestCases(List<String> tcRecIds)
			throws UnreachableETMException, ETMException;

	// Synchronization of TestSteps Based on their Id.
	public List<TestStep> SynchronizeTestSteps(List<String> tcStepIds)
			throws UnreachableETMException, ETMException;

	// Logout
	public void logout() throws UnreachableETMException, ETMException;

	// Project Description
	public void setProjectDesc(String projectDescription);
	
	/*Added By Ashiki for TJN252-60 starts*/
	public List<TestStep> getTestStepsWithFilter(String tsFilter, String tsFilterValue,
			Map<String, String> mapAttributes) throws UnreachableETMException, ETMException;

	public List<TestCase> getTestCases(String tcFilter, String tcFilterValue,
			Map<String, String> mapAttributes) throws UnreachableETMException, ETMException;

	List<TestStep> getTestSteps(String fieldKey, String fieldValue, String tcRecordId,
			Map<String, String> mapAttributes) throws UnreachableETMException, ETMException;


	List<TestCase> SynchronizeTestCases(List<String> tcRecIds, Map<String, String> mapAttributes)
			throws UnreachableETMException, ETMException;

	List<TestStep> SynchronizeTestSteps(List<String> tcStepIds, Map<String, String> mapAttributes)
			throws UnreachableETMException, ETMException;
	/*Added By Ashiki for TJN252-60 ends*/
}
