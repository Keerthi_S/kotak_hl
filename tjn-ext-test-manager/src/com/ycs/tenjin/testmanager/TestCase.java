/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestCase.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 23-Jan-2017         Jyoti Ranjan             Newly Added For 
 * 19-Aug-2017         Manish		            T25IT-236 
 */

package com.ycs.tenjin.testmanager;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;

public class TestCase {
	int tcRecId;
	String tcId;
	int tcFolderId;
	String tcFolderName;
	String tcName;
	String tcDesc;
	int tcTsetId;
	String tcType;
	String tcPriority;
	String tcReviewed;
	String tcStatus;
	Date tcCreatedOn;
	Date tcModifiedOn;
	String tcCreatedBy;
	String tcModifiedBy;
	int projectId;
	String recordType;
	private List<TestStep> tcSteps;
	// private TestCaseResult result;
	private int uploadRow;
	private Collection<TestStep> stepCollection;

	/* changed by manish for req TENJINCG-32 on 11-Jan-2017 starts */
	// this field is added to differentiate test cases created locally by tenjin
	// or imported from outside tenjin
	private String storage;

	public void setStorage(String storage) {
		this.storage = storage;
	}

	/* changed by manish for req TENJINCG-32 on 11-Jan-2017 ends */
	/*added by manish for req TENJINCG-59 on 03-03-2017 starts*/
	private JSONArray stepsArray;
	
	/*changed by manish for bug T25IT-236 on 19-Aug-2017 starts*/
	private String mode;

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
	/*changed by manish for bug T25IT-236 on 19-Aug-2017 ends*/
	
	public String getStorage() {
		return storage;
	}
	

	public JSONArray getStepsArray() {
		return stepsArray;
	}

	public void setStepsArray(JSONArray stepsArray) {
		this.stepsArray = stepsArray;
	}

	/*added by manish for req TENJINCG-59 on 03-03-2017 ends*/
	public Collection<TestStep> getStepCollection() {
		return stepCollection;
	}

	public void setStepCollection(Collection<TestStep> stepCollection) {
		this.stepCollection = stepCollection;
	}

	public int getUploadRow() {
		return uploadRow;
	}

	public void setUploadRow(int uploadRow) {
		this.uploadRow = uploadRow;
	}

	/*********************************************************
	 * Added by Sriram for Tenjin v2.1 (For Results Summary Display)
	 */
	private int totalSteps;
	private int totalPassedSteps;
	private int totalFailedSteps;
	private int totalExecutedSteps;
	private int totalErrorredSteps;

	public int getTotalSteps() {
		return totalSteps;
	}

	public void setTotalSteps(int totalSteps) {
		this.totalSteps = totalSteps;
	}

	public int getTotalPassedSteps() {
		return totalPassedSteps;
	}

	public void setTotalPassedSteps(int totalPassedSteps) {
		this.totalPassedSteps = totalPassedSteps;
	}

	public int getTotalFailedSteps() {
		return totalFailedSteps;
	}

	public void setTotalFailedSteps(int totalFailedSteps) {
		this.totalFailedSteps = totalFailedSteps;
	}

	public int getTotalExecutedSteps() {
		return totalExecutedSteps;
	}

	public void setTotalExecutedSteps(int totalExecutedSteps) {
		this.totalExecutedSteps = totalExecutedSteps;
	}

	public int getTotalErrorredSteps() {
		return totalErrorredSteps;
	}

	public void setTotalErrorredSteps(int totalErrorredSteps) {
		this.totalErrorredSteps = totalErrorredSteps;
	}

	/*********************************************************
	 * Added by Sriram for Tenjin v2.1 (For Results Summary Display) ends
	 */
	/*
	 * public TestCaseResult getResult() { return result; } public void
	 * setResult(TestCaseResult result) { this.result = result; }
	 */

	public int getProjectId() {
		return projectId;
	}

	public List<TestStep> getTcSteps() {
		return tcSteps;
	}

	public void setTcSteps(List<TestStep> tcSteps) {
		this.tcSteps = tcSteps;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getTcCreatedBy() {
		return tcCreatedBy;
	}

	public void setTcCreatedBy(String tcCreatedBy) {
		this.tcCreatedBy = tcCreatedBy;
	}

	public String getTcModifiedBy() {
		return tcModifiedBy;
	}

	public void setTcModifiedBy(String tcModifiedBy) {
		this.tcModifiedBy = tcModifiedBy;
	}

	public int getTcRecId() {
		return tcRecId;
	}

	public void setTcRecId(int tcRecId) {
		this.tcRecId = tcRecId;
	}

	public String getTcId() {
		return tcId;
	}

	public void setTcId(String tcId) {
		this.tcId = tcId;
	}

	public int getTcFolderId() {
		return tcFolderId;
	}

	public void setTcFolderId(int tcFolderId) {
		this.tcFolderId = tcFolderId;
	}

	public String getTcFolderName() {
		return tcFolderName;
	}

	public void setTcFolderName(String tcFolderName) {
		this.tcFolderName = tcFolderName;
	}

	public String getTcName() {
		return tcName;
	}

	public void setTcName(String tcName) {
		this.tcName = tcName;
	}

	public String getTcDesc() {
		return tcDesc;
	}

	public void setTcDesc(String tcDesc) {
		this.tcDesc = tcDesc;
	}

	public int getTcTsetId() {
		return tcTsetId;
	}

	public void setTcTsetId(int tcTsetId) {
		this.tcTsetId = tcTsetId;
	}

	public String getTcType() {
		return tcType;
	}

	public void setTcType(String tcType) {
		this.tcType = tcType;
	}

	public String getTcPriority() {
		return tcPriority;
	}

	public void setTcPriority(String tcPriority) {
		this.tcPriority = tcPriority;
	}

	public String getTcReviewed() {
		return tcReviewed;
	}

	public void setTcReviewed(String tcReviewed) {
		this.tcReviewed = tcReviewed;
	}

	public String getTcStatus() {
		return tcStatus;
	}

	public void setTcStatus(String tcStatus) {
		this.tcStatus = tcStatus;
	}

	public Date getTcCreatedOn() {
		return tcCreatedOn;
	}

	public void setTcCreatedOn(Date tcCreatedOn) {
		this.tcCreatedOn = tcCreatedOn;
	}

	public Date getTcModifiedOn() {
		return tcModifiedOn;
	}

	public void setTcModifiedOn(Date tcModifiedOn) {
		this.tcModifiedOn = tcModifiedOn;
	}

}
