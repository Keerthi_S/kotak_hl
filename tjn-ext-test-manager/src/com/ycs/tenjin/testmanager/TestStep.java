/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestStep.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 23-Jan-2017          Abhilash K N         Newly Added 
*/

package com.ycs.tenjin.testmanager;


public class TestStep {

	private int recordId;
	private int appId;
	private int testCaseRecordId;
	/*private HighLevelStepResult highLevelResult;*/
	private String shortDescription;
	private String dataId;
	private String type;
	private String description;
	private String expectedResult;
	private String actualResult;
	private String moduleCode;
	private String transactionContext;
	private String executionContext;
	private String txnMode;	/*20-Oct-2014 WS*/
	/*private HighLevelStepResult resultSummary;
	private ArrayList<StepIterationResult> detailedResults;
	private ArrayList<ResultValidation> validations;*/
	private String id;
	private String appName;
	private String parentTestCaseId;
	private int uploadRow;
	private String testCaseName;
	private int rowsToExecute;

	
	
	public String getTestCaseName() {
		return testCaseName;
	}
	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}
	public int getRowsToExecute() {
		return rowsToExecute;
	}
	public void setRowsToExecute(int rowsToExecute) {
		this.rowsToExecute = rowsToExecute;
	}
	public int getUploadRow() {
		return uploadRow;
	}
	public void setUploadRow(int uploadRow) {
		this.uploadRow = uploadRow;
	}
	public String getParentTestCaseId() {
		return parentTestCaseId;
	}
	public void setParentTestCaseId(String parentTestCaseId) {
		this.parentTestCaseId = parentTestCaseId;
	}

	/*********************************************************
	 * Added by Sriram for Tenjin v2.1 (For Results Summary Display)
	 */
	private int totalTransactions;
	private int totalExecutedTxns;
	private int totalPassedTxns;
	private int totalFailedTxns;
	private int totalErroredTxns;
	private String status;
	
	
	
	public int getTotalTransactions() {
		return totalTransactions;
	}
	public void setTotalTransactions(int totalTransactions) {
		this.totalTransactions = totalTransactions;
	}
	public int getTotalExecutedTxns() {
		return totalExecutedTxns;
	}
	public void setTotalExecutedTxns(int totalExecutedTxns) {
		this.totalExecutedTxns = totalExecutedTxns;
	}
	public int getTotalPassedTxns() {
		return totalPassedTxns;
	}
	public void setTotalPassedTxns(int totalPassedTxns) {
		this.totalPassedTxns = totalPassedTxns;
	}
	public int getTotalFailedTxns() {
		return totalFailedTxns;
	}
	public void setTotalFailedTxns(int totalFailedTxns) {
		this.totalFailedTxns = totalFailedTxns;
	}
	public int getTotalErroredTxns() {
		return totalErroredTxns;
	}
	public void setTotalErroredTxns(int totalErroredTxns) {
		this.totalErroredTxns = totalErroredTxns;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	/*********************************************************
	 * Added by Sriram for Tenjin v2.1 (For Results Summary Display) ends
	 */
	
	/**************************************************************************
	 * Fix by Sriram to add AUT Login Type Attribute (1-Dec-2014)
	 */
	private String autLoginType;
	public String getAutLoginType() {
		return autLoginType;
	}
	public void setAutLoginType(String autLoginType) {
		this.autLoginType = autLoginType;
	}
	/**************************************************************************
	 * Fix by Sriram to add AUT Login Type Attribute (1-Dec-2014) ends
	 */
	
	/*************
	 * Fix by Sriram to add Test Step Operation Attribute
	 */
	private String operation;
	
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	/*************
	 * Fix by Sriram to add Test Step Operation Attribute Ends
	 */
	
	/**
	 * @return the appName
	 */
	public String getAppName() {
		return appName;
	}
	/**
	 * @param appName the appName to set
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/*20-Oct-2014 WS: Starts*/
	public String getTxnMode() {
		return txnMode;
	}
	public void setTxnMode(String m) {
		this.txnMode = m == null ? "G" : m;
	}
	/*20-Oct-2014 WS: Ends*/
	
	
	/*public ArrayList<ResultValidation> getValidations() {
		return validations;
	}
	public void setValidations(ArrayList<ResultValidation> validations) {
		this.validations = validations;
	}
	public HighLevelStepResult getResultSummary() {
		return resultSummary;
	}
	public void setResultSummary(HighLevelStepResult resultSummary) {
		this.resultSummary = resultSummary;
	}
	public ArrayList<StepIterationResult> getDetailedResults() {
		return detailedResults;
	}
	public void setDetailedResults(ArrayList<StepIterationResult> detailedResults) {
		this.detailedResults = detailedResults;
	}*/
	public int getRecordId() {
		return recordId;
	}
	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	public int getTestCaseRecordId() {
		return testCaseRecordId;
	}
	public void setTestCaseRecordId(int testCaseRecordId) {
		this.testCaseRecordId = testCaseRecordId;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getDataId() {
		return dataId;
	}
	public void setDataId(String dataId) {
		this.dataId = dataId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getExpectedResult() {
		return expectedResult;
	}
	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}
	public String getActualResult() {
		return actualResult;
	}
	public void setActualResult(String actualResult) {
		this.actualResult = actualResult;
	}
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getTransactionContext() {
		return transactionContext;
	}
	public void setTransactionContext(String transactionContext) {
		this.transactionContext = transactionContext;
	}
	public String getExecutionContext() {
		return executionContext;
	}
	public void setExecutionContext(String executionContext) {
		this.executionContext = executionContext;
	}
	/*public HighLevelStepResult getHighLevelResult() {
		return highLevelResult;
	}
	public void setHighLevelResult(HighLevelStepResult highLevelResult) {
		this.highLevelResult = highLevelResult;
	}
	*/
	//Added by Sriram to store function name (For Defect creation)
	private String moduleName;
	
	
	
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	/********************
	 * Added by Sriram for Requirement TJN_24_05
	 */
	/*public ExecutionStep getExecutionStep(){
		
		ExecutionStep step = new ExecutionStep();
		step.setRecordId(this.recordId);
		step.setAppId(this.getAppId());
		step.setAppName(this.appName);
		step.setFunctionCode(this.moduleCode);
		step.setTdGid(this.dataId);
		step.setTransactionLimit(this.totalTransactions);
		step.setOperation(this.operation);
		step.setId(this.id);
		step.setAutLoginType(this.autLoginType);
		*//******************************
		 * Added by Sriram for Def#789
		 *//*
		step.setType(this.type);
		step.setExpectedResult(this.expectedResult);
		*//******************************
		 * Added by Sriram for Def#789 ends
		 *//*
		return step;
	}*/

}
