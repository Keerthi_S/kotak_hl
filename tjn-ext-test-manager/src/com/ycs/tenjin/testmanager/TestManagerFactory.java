/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestManagerFactory.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 23-Jan-2017          Jyoti Ranjan          Newly Added For 
 * 01-09-2017		    Padmavathi				T25IT-403
 */


package com.ycs.tenjin.testmanager;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/****  This class generate instance of the subclass interface dynamically. ****/
public class TestManagerFactory {
	private static final Logger logger = LoggerFactory.getLogger(TestManagerFactory.class);

	public static TestManager getTestManagementTools(String toolName,String baseUrl, String userId, String password) throws ETMException{
		String expPackageName = "com.ycs.tenjin.testmanager.impl." + toolName;
		Reflections reflections = new Reflections(expPackageName);
		Set<Class<? extends TestManager>> subTypes = reflections.getSubTypesOf(TestManager.class);

		for(Class<? extends TestManager> type:subTypes){
			try {
				Constructor<?> constructor = type.getConstructor(String.class,String.class,String.class);
				return (TestManager) constructor.newInstance(baseUrl,userId,password);
			} catch (Exception e) {
				logger.error("Could not initiate test management toll for {}", toolName, e);
				throw new ETMException("Could not initiate the test management toll for " + toolName);
			} 
		}

		logger.error("ERROR initializing test manager for {}. Test management implementation was not found");
		throw new ETMException("Could not initialize test manager for " + toolName + ". Please contact Tenjin Support");


	}
	
	public static List<String> getAvailableTools() throws ETMException{
		List<String> adapters = new ArrayList<String>();
		
		String expPackageName = "com.ycs.tenjin.testmanager.impl";
		Reflections reflections = new Reflections(expPackageName);
		
		Set<Class<? extends TestManager>> subTypes = reflections.getSubTypesOf(TestManager.class);
		for(Class<? extends TestManager> type:subTypes){
			String packageName = type.getPackage().getName();
			/*Changed by Padmavathi for T25IT-403 starts*/
			/*if(packageName.toLowerCase().contains("genericetmimpl")){
				continue;
			}*/
			if(type.toString().toLowerCase().contains("genericetmimpl")){
				continue;
			}
			/*Changed by Padmavathi for T25IT-403 ends*/
			packageName = packageName.replace(expPackageName + ".", "");
			adapters.add(packageName);
		}
		
		return adapters;
	}
}
