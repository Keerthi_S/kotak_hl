package com.ycs.tenjin.unstructure.abstraction;

import com.ycs.tenjin.ValidationDetails;

public interface UnstructureValidation {
	public ValidationDetails validate();
}