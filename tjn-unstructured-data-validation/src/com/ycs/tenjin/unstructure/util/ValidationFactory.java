package com.ycs.tenjin.unstructure.util;

import java.util.Set;

import org.reflections.Reflections;

import com.ycs.tenjin.ValidationDetails;
import com.ycs.tenjin.unstructure.abstraction.UnstructureValidation;
import com.ycs.tenjin.unstructure.abstraction.UnstructureValidationAbstract;

public class ValidationFactory {
	
	public static UnstructureValidation getValidationObject(ValidationDetails validation) throws Exception{
		String packageName = "com.ycs.tenjin.unstructure.validation."+validation.getFileType();
		Reflections reflection = new Reflections(packageName);
		Set<Class<? extends UnstructureValidationAbstract>> subTypes = reflection.getSubTypesOf(UnstructureValidationAbstract.class);
		for (Class<? extends UnstructureValidation> subType : subTypes) {
			return (UnstructureValidation) subType.getDeclaredConstructor(ValidationDetails.class).newInstance(validation);
		}
		return null;
	}
}