package com.ycs.tenjin.unstructure.validation.pdf.pojo;

import java.util.List;

public class PDFValidationDetails {
	private List<TextProperties> textProperties;
	private List<ImageProperties> imageProperties;
	private List<SignatureProperties> signatureProperties;
	public List<TextProperties> getTextProperties() {
		return textProperties;
	}
	public void setTextProperties(List<TextProperties> textProperties) {
		this.textProperties = textProperties;
	}
	public List<ImageProperties> getImageProperties() {
		return imageProperties;
	}
	public void setImageProperties(List<ImageProperties> imageProperties) {
		this.imageProperties = imageProperties;
	}
	public List<SignatureProperties> getSignatureProperties() {
		return signatureProperties;
	}
	public void setSignatureProperties(List<SignatureProperties> signatureProperties) {
		this.signatureProperties = signatureProperties;
	}
}