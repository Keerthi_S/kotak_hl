package com.ycs.tenjin.unstructure.validation.pdf.text;

import java.awt.geom.Rectangle2D;
import java.util.Comparator;

public class Rectangle2DExtend implements Comparator<Rectangle2D>{

	@Override
	public int compare(Rectangle2D rec1, Rectangle2D rec2) {
		
		int xAxis = compareXAxis(rec1.getX(), rec2.getX());
		int yAxis = compareXAxis(rec1.getY(), rec2.getY());
		
				if(xAxis == 0 && yAxis == 0){
			return 0;
		}else if(xAxis == -1 && yAxis == 0){
			return -1;
		}else if(xAxis == 1 && yAxis == 0){
			return 1;
		}else if(xAxis == 0 && yAxis == -1){
			return -1;
		}else if(xAxis == -1 && yAxis == -1){
			return -1;
		}else if(xAxis == 1 && yAxis == -1){
			return -1;
		}else if(xAxis == 0 && yAxis == 1){
			return 1;
		}else if(xAxis == -1 && yAxis == 1){
			return 1;
		}else if(xAxis == 1 && yAxis == 1){
			return 1;
		}
		
		return compareXAxis(xAxis, yAxis);
	}
	
	private int compareXAxis(double rec1, double rec2){
		if(rec1 == rec2){
			return 0;
		}else if(rec1 < rec2){
			return -1;
		}else if(rec1 > rec2){
			return 1;
		}
		return 0;
	}
}