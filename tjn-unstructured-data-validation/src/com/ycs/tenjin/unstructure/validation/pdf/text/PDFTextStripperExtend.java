package com.ycs.tenjin.unstructure.validation.pdf.text;

import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;

import com.ycs.tenjin.unstructure.validation.pdf.pojo.FontDetails;
import com.ycs.tenjin.unstructure.validation.pdf.pojo.TextProperties;

public class PDFTextStripperExtend extends PDFTextStripper{
	private List<TextProperties> contentDetails = new ArrayList<TextProperties>();
	public PDFTextStripperExtend() throws IOException {
		super();
	}

	@Override
     protected void writeString(String string, List<TextPosition> textPositions) throws IOException {
         String wordSeparator = getWordSeparator();
         List<TextPosition> word = new ArrayList<>();
         textPositions.forEach(a -> {
        	 String currChar = a.getUnicode();
        	 if(currChar != null && !currChar.isEmpty()){
        		 if (!currChar.equals(wordSeparator)) {
                     word.add(a);
                 } else if (!word.isEmpty()) {
                     getCoordinates(word);
                     word.clear();
                 }
        	 }
        });
        
         if (!word.isEmpty()) {
        	 getCoordinates(word);
             word.clear();
         }
     }

     private void getCoordinates(List<TextPosition> word) {
    	 Rectangle2D boundingBox = null;
    	 TextProperties textProperties = new TextProperties();
    	 List<FontDetails> fontDetails = new ArrayList<FontDetails>();
         StringBuilder builder = new StringBuilder();
         for (TextPosition text : word) {
             Rectangle2D box = new Rectangle2D.Float(text.getXDirAdj(), text.getYDirAdj(), text.getWidthDirAdj(), text.getHeightDir());
             if (boundingBox == null)
                 boundingBox = box;
             else
                 boundingBox.add(box);
             builder.append(text.getUnicode());
             fontDetails.add(new FontDetails(getFontStyle(text.getFont()), text.getFont().getName(), text.getFontSize()));
         }
         
    	 textProperties.setValue(builder.toString());
    	 textProperties.setFontDetails(fontDetails);
    	 textProperties.setCoordinates(boundingBox);
    	 
         this.contentDetails.add(textProperties);
     }
     
     private String getFontStyle(PDFont font){
    	 String style = "";
    	 if(font.toString().toLowerCase().contains("bold")){
    		 style = "BOLD";
    	 }else if(font.toString().toLowerCase().contains("italic")){
    		 style = "ITALIC";
    	 }else{
    		 style = "REGULAR";
    	 }
    	 return style;
     }
     
     public List<TextProperties> getContentDetails(){
    	 return this.contentDetails;
     }
}