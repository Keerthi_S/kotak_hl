package com.ycs.tenjin.unstructure.validation.pdf.pojo;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.util.Matrix;

public class ImageProperties {
	
	private Matrix coordinates;
	private int imageHeight;
	private int imageWidth;
	private String desc;
	private BufferedImage image;
	private File imageFile;
	private String imageDesc;
	private List<String> messages = new ArrayList<String>();
	
	public Matrix getCoordinates() {
		return coordinates;
	}
	
	public void setCoordinates(Matrix coordinates) {
		this.coordinates = coordinates;
	}
	
	public BufferedImage getImage() {
		return image;
	}
	

	public void setImage(BufferedImage image) {
		this.image = image;
	}
	

	public int getImageHeight() {
		return imageHeight;
	}
	
	public void setImageHeight(int imageHeight) {
		this.imageHeight = imageHeight;
	}
	
	public int getImageWidth() {
		return imageWidth;
	}
	
	public void setImageWidth(int imageWidth) {
		this.imageWidth = imageWidth;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "ImageProperties [coordinates=" + coordinates + ", imageHeight=" + imageHeight + ", imageWidth="
				+ imageWidth + ", desc=" + desc + ", image=" + image + "]";
	}

	public String getImageDesc() {
		return imageDesc;
	}

	public void setImageDesc(String imageDesc) {
		this.imageDesc = imageDesc;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public File getImageFile() {
		return imageFile;
	}

	public void setImageFile(File imageFile) {
		this.imageFile = imageFile;
	}
	
}
