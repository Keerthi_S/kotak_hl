package com.ycs.tenjin.unstructure.validation.pdf.signature;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.io.RandomAccessBufferedFileInputStream;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.encryption.SecurityProvider;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.apache.pdfbox.util.Charsets;
import org.apache.pdfbox.util.Hex;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.CMSAttributes;
import org.bouncycastle.asn1.cms.Time;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationVerifier;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.util.CollectionStore;
import org.bouncycastle.util.Selector;
import org.bouncycastle.util.Store;

import com.ycs.tenjin.unstructure.validation.pdf.pojo.SignatureProperties;

public class PDFSignatureValidation {
	private final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

	public PDFSignatureValidation() {}
	
	public static void main(String[] args) throws Exception {
		Security.addProvider(SecurityProvider.getProvider());

		PDFSignatureValidation show = new PDFSignatureValidation();
		List<SignatureProperties> signatures = show.showSignature("C:\\Users\\avinash.muthukumar\\Downloads\\2018-19.pdf");
		for (SignatureProperties pdfSignature : signatures) {
			System.out.println(pdfSignature);
		}
	}
	
	public List<SignatureProperties> validateSignatures(String baseFilePath, String inputFilePath) throws Exception{
		List<SignatureProperties> baseSignature = showSignature(baseFilePath);
		List<SignatureProperties> inputSignature = showSignature(inputFilePath);
		
		for (SignatureProperties signatureProperties : baseSignature) {
			System.out.println(signatureProperties);
		}
		
		for (SignatureProperties signatureProperties : inputSignature) {
			System.out.println(signatureProperties);
		}
		return inputSignature;
	}

	public List<SignatureProperties> showSignature(String str) throws Exception {
		File infile = new File(str);
		PDDocument document = null;
		List<SignatureProperties> signatures = new ArrayList<SignatureProperties>();
		try {
			RandomAccessBufferedFileInputStream raFile = new RandomAccessBufferedFileInputStream(infile);
			PDFParser parser = new PDFParser(raFile);
			parser.setLenient(false);
			parser.parse();
			document = parser.getPDDocument();
			for (PDSignature sig : document.getSignatureDictionaries()) {
				COSDictionary sigDict = sig.getCOSObject();
				COSString contents = (COSString) sigDict.getDictionaryObject(COSName.CONTENTS);
				SignatureProperties signature = new SignatureProperties();
				FileInputStream fis = new FileInputStream(infile);
				byte[] buf = null;
				try {
					buf = sig.getSignedContent(fis);
				} finally {
					fis.close();
				}

				if (sig.getName() != null) {
					//System.out.println("Name:     " + sig.getName());
				}
				if (sig.getSignDate() != null) {
					signature.setSignDate(sdf.format(sig.getSignDate().getTime()));
					//System.out.println("Modified: "	+ sdf.format(sig.getSignDate().getTime()));
				}
				String subFilter = sig.getSubFilter();
				if (subFilter != null) {
					if (subFilter.equals("adbe.pkcs7.detached")	|| subFilter.equals("ETSI.CAdES.detached")) {
						verifyPKCS7(buf, contents, sig, signature);
					} else if (subFilter.equals("adbe.pkcs7.sha1")) {
						/*replaced forBy Prem  security fox of Weak Cryptography Strat*/	
					//	byte[] hash = MessageDigest.getInstance("SHA1").digest(buf);
						byte[] hash = MessageDigest.getInstance("SHA-512").digest(buf); 
						/*replaced forBy Prem  security fox of Weak Cryptography Ends*/
						verifyPKCS7(hash, contents, sig, signature);
					} else if (subFilter.equals("adbe.x509.rsa_sha1")) {
						COSString certString = (COSString) sigDict.getDictionaryObject(COSName.CERT);
						if (certString == null) {
							//System.err.println("The /Cert certificate string is missing in the signature dictionary");
							return signatures;
						}
						byte[] certData = certString.getBytes();
						CertificateFactory factory = CertificateFactory.getInstance("X.509");
						ByteArrayInputStream certStream = new ByteArrayInputStream(certData);
						Collection<? extends Certificate> certs = factory.generateCertificates(certStream);
						System.out.println("certs=" + certs);

						X509Certificate cert = (X509Certificate) certs.iterator().next();

						try {
							if (sig.getSignDate() != null) {
								cert.checkValidity(sig.getSignDate().getTime());
								System.out.println("Certificate valid at signing time");
							} else {
								System.err.println("Certificate cannot be verified without signing time");
							}
						} catch (CertificateExpiredException ex) {
							System.err.println("Certificate expired at signing time");
						} catch (CertificateNotYetValidException ex) {
							System.err.println("Certificate not yet valid at signing time");
						}
						if (CertificateVerifier.isSelfSigned(cert)) {
							System.err.println("Certificate is self-signed, LOL!");
						} else {
							//System.out.println("Certificate is not self-signed");

							if (sig.getSignDate() != null) {
								@SuppressWarnings("unchecked")
								Store<X509CertificateHolder> store = new JcaCertStore(certs);
								verifyCertificateChain(store, cert, sig.getSignDate().getTime());
							}
						}
					} else if (subFilter.equals("ETSI.RFC3161")) {
						verifyETSIdotRFC3161(buf, contents);
					} else {
						System.err.println("Unknown certificate type: "+ subFilter);
					}
				} else {
					throw new IOException("Missing subfilter for cert dictionary");
				}

				int[] byteRange = sig.getByteRange();
				if (byteRange.length != 4) {
					System.err.println("Signature byteRange must have 4 items");
				} else {
					/*long fileLen = infile.length();
					long rangeMax = byteRange[2] + (long) byteRange[3];
					int contentLen = contents.getString().length() * 2 + 2;*/
					/*if (fileLen != rangeMax || byteRange[0] != 0 || byteRange[1] + contentLen != byteRange[2]) {
						//System.out.println("Signature does not cover whole document");
					} else {
						//System.out.println("Signature covers whole document");
					}*/
					checkContentValueWithFile(infile, byteRange, contents);
				}
				signatures.add(signature);
			}
			analyseDSS(document);
		} catch (CMSException ex) {
			throw new IOException(ex);
		} catch (OperatorCreationException ex) {
			throw new IOException(ex);
		} finally {
			if (document != null) {
				document.close();
			}
		}
		//System.out.println("Analyzed: " + str);
		return signatures;
	}

	private void checkContentValueWithFile(File file, int[] byteRange,
			COSString contents) throws IOException {
		RandomAccessBufferedFileInputStream raf = new RandomAccessBufferedFileInputStream(file);
		raf.seek(byteRange[1]);
		int c = raf.read();
		if (c != '<') {
			System.err.println("'<' expected at offset " + byteRange[1] + ", but got " + (char) c);
		}
		byte[] contentFromFile = raf.readFully(byteRange[2] - byteRange[1] - 2);
		byte[] contentAsHex = Hex.getString(contents.getBytes()).getBytes(Charsets.US_ASCII);
		if (contentFromFile.length != contentAsHex.length) {
			System.err.println("Raw content length from file is "+ contentFromFile.length+ ", but internal content string in hex has length "+ contentAsHex.length);
		}
		for (int i = 0; i < contentFromFile.length; ++i) {
			try {
				if (Integer.parseInt(String.valueOf((char) contentFromFile[i]),16) != Integer.parseInt(String.valueOf((char) contentAsHex[i]), 16)) {
					System.err.println("Possible manipulation at file offset " + (byteRange[1] + i + 1) + " in signature content");
					break;
				}
			} catch (NumberFormatException ex) {
				System.err.println("Incorrect hex value");
				System.err.println("Possible manipulation at file offset " + (byteRange[1] + i + 1) + " in signature content");
				break;
			}
		}
		c = raf.read();
		if (c != '>') {
			System.err.println("'>' expected at offset " + byteRange[2] + ", but got " + (char) c);
		}
		raf.close();
	}

	private void verifyETSIdotRFC3161(byte[] buf, COSString contents) throws Exception {
		TimeStampToken timeStampToken = new TimeStampToken(new CMSSignedData(contents.getBytes()));
		System.out.println("Time stamp gen time: " + timeStampToken.getTimeStampInfo().getGenTime());
		System.out.println("Time stamp tsa name: " + timeStampToken.getTimeStampInfo().getTsa().getName());

		CertificateFactory factory = CertificateFactory.getInstance("X.509");
		ByteArrayInputStream certStream = new ByteArrayInputStream(contents.getBytes());
		Collection<? extends Certificate> certs = factory.generateCertificates(certStream);
		System.out.println("certs=" + certs);

		String hashAlgorithm = timeStampToken.getTimeStampInfo().getMessageImprintAlgOID().getId();
		if (Arrays.equals(MessageDigest.getInstance(hashAlgorithm).digest(buf),timeStampToken.getTimeStampInfo().getMessageImprintDigest())) {
			System.out.println("ETSI.RFC3161 timestamp signature verified");
		} else {
			System.err.println("ETSI.RFC3161 timestamp signature verification failed");
		}

		X509Certificate certFromTimeStamp = (X509Certificate) certs.iterator().next();
		SigUtils.checkTimeStampCertificateUsage(certFromTimeStamp);
		validateTimestampToken(timeStampToken);
		verifyCertificateChain(timeStampToken.getCertificates(),certFromTimeStamp, timeStampToken.getTimeStampInfo().getGenTime());
	}

	private void verifyPKCS7(byte[] byteArray, COSString contents, PDSignature sig, SignatureProperties pdfSignature) throws Exception {
		CMSProcessable signedContent = new CMSProcessableByteArray(byteArray);
		CMSSignedData signedData = new CMSSignedData(signedContent,	contents.getBytes());
		Store<X509CertificateHolder> certificatesStore = signedData.getCertificates();
		if (certificatesStore.getMatches(null).isEmpty()) {
			throw new IOException("No certificates in signature");
		}
		Collection<SignerInformation> signers = signedData.getSignerInfos().getSigners();
		if (signers.isEmpty()) {
			throw new IOException("No signers in signature");
		}
		SignerInformation signerInformation = signers.iterator().next();
		@SuppressWarnings("unchecked")
		Collection<X509CertificateHolder> matches = certificatesStore.getMatches((Selector<X509CertificateHolder>) signerInformation.getSID());
		if (matches.isEmpty()) {
			throw new IOException("Signer '"+ signerInformation.getSID().getIssuer() + ", serial# "+ signerInformation.getSID().getSerialNumber()+ " does not match any certificates");
		}
		X509CertificateHolder certificateHolder = matches.iterator().next();
		X509Certificate certFromSignedData = new JcaX509CertificateConverter().getCertificate(certificateHolder);
		List<Object> list = new ArrayList<Object>();
		certFromSignedData.getSubjectAlternativeNames().stream().forEach(a -> a.stream().forEach(b -> list.add(b)));
		pdfSignature.setAlgorithm(certFromSignedData.getSigAlgName());
		pdfSignature.setPublicKey(certFromSignedData.getPublicKey().toString());
		pdfSignature.setSubjectAlternativeNames(list);
		pdfSignature.setSubjectName(certFromSignedData.getSubjectX500Principal().getName());
		//System.out.println("certFromSignedData: "  + certFromSignedData);

		SigUtils.checkCertificateUsage(certFromSignedData);

		TimeStampToken timeStampToken = extractTimeStampTokenFromSignerInformation(signerInformation);
		if (timeStampToken != null) {
			validateTimestampToken(timeStampToken);
			@SuppressWarnings("unchecked")
			Collection<X509CertificateHolder> tstMatches = timeStampToken.getCertificates().getMatches((Selector<X509CertificateHolder>) timeStampToken.getSID());
			X509CertificateHolder tstCertHolder = tstMatches.iterator().next();
			X509Certificate certFromTimeStamp = new JcaX509CertificateConverter().getCertificate(tstCertHolder);
			HashSet<X509CertificateHolder> certificateHolderSet = new HashSet<X509CertificateHolder>();
			certificateHolderSet.addAll(certificatesStore.getMatches(null));
			certificateHolderSet.addAll(timeStampToken.getCertificates().getMatches(null));
			verifyCertificateChain(new CollectionStore<X509CertificateHolder>(certificateHolderSet), certFromTimeStamp, timeStampToken.getTimeStampInfo().getGenTime());
			SigUtils.checkTimeStampCertificateUsage(certFromTimeStamp);
		}

		try {
			if (sig.getSignDate() != null) {
				certFromSignedData.checkValidity(sig.getSignDate().getTime());
				//System.out.println("Certificate valid at signing time");
			} else {
				System.err.println("Certificate cannot be verified without signing time");
			}
		} catch (CertificateExpiredException ex) {
			System.err.println("Certificate expired at signing time");
		} catch (CertificateNotYetValidException ex) {
			System.err.println("Certificate not yet valid at signing time");
		}

		if (signerInformation.getSignedAttributes() != null) {
			Attribute signingTime = signerInformation.getSignedAttributes().get(CMSAttributes.signingTime);
			if (signingTime != null) {
				Time timeInstance = Time.getInstance(signingTime.getAttrValues().getObjectAt(0));
				try {
					certFromSignedData.checkValidity(timeInstance.getDate());
					//System.out.println("Certificate valid at signing time: "+ timeInstance.getDate());
				} catch (CertificateExpiredException ex) {
					System.err.println("Certificate expired at signing time");
				} catch (CertificateNotYetValidException ex) {
					System.err.println("Certificate not yet valid at signing time");
				}
			}
		}

		if (signerInformation.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider(SecurityProvider.getProvider()).build(certFromSignedData))) {
			//System.out.println("Signature verified");
		} else {
			System.out.println("Signature verification failed");
		}

	}

	private TimeStampToken extractTimeStampTokenFromSignerInformation(SignerInformation signerInformation) throws CMSException,IOException, TSPException {
		if (signerInformation.getUnsignedAttributes() == null) {
			return null;
		}
		AttributeTable unsignedAttributes = signerInformation.getUnsignedAttributes();
		Attribute attribute = unsignedAttributes.get(PKCSObjectIdentifiers.id_aa_signatureTimeStampToken);
		if (attribute == null) {
			return null;
		}
		ASN1Object obj = (ASN1Object) attribute.getAttrValues().getObjectAt(0);
		CMSSignedData signedTSTData = new CMSSignedData(obj.getEncoded());
		return new TimeStampToken(signedTSTData);
	}

	private void verifyCertificateChain(Store<X509CertificateHolder> certificatesStore,	X509Certificate certFromSignedData, Date signDate) throws Exception {
		Collection<X509CertificateHolder> certificateHolders = certificatesStore.getMatches(null);
		Set<X509Certificate> additionalCerts = new HashSet<X509Certificate>();
		JcaX509CertificateConverter certificateConverter = new JcaX509CertificateConverter();
		for (X509CertificateHolder certHolder : certificateHolders) {
			X509Certificate certificate = certificateConverter.getCertificate(certHolder);
			if (!certificate.equals(certFromSignedData)) {
				additionalCerts.add(certificate);
			}
		}
		CertificateVerifier.verifyCertificate(certFromSignedData,additionalCerts, true, signDate);
	}

	private void validateTimestampToken(TimeStampToken timeStampToken) throws IOException, CertificateException, TSPException, OperatorCreationException {
		@SuppressWarnings("unchecked")
		Collection<X509CertificateHolder> tstMatches = timeStampToken.getCertificates().getMatches((Selector<X509CertificateHolder>) timeStampToken.getSID());
		X509CertificateHolder holder = tstMatches.iterator().next();
		X509Certificate tstCert = new JcaX509CertificateConverter().getCertificate(holder);
		SignerInformationVerifier siv = new JcaSimpleSignerInfoVerifierBuilder().setProvider(SecurityProvider.getProvider()).build(tstCert);
		timeStampToken.validate(siv);
		System.out.println("TimeStampToken validated");
	}

	private void analyseDSS(PDDocument document) throws IOException {
		PDDocumentCatalog catalog = document.getDocumentCatalog();
		COSBase dssElement = catalog.getCOSObject().getDictionaryObject("DSS");

		if (dssElement instanceof COSDictionary) {
			COSDictionary dss = (COSDictionary) dssElement;
			System.out.println("DSS Dictionary: " + dss);
			COSBase certsElement = dss.getDictionaryObject("Certs");
			if (certsElement instanceof COSArray) {
				printStreamsFromArray((COSArray) certsElement, "Cert");
			}
			COSBase ocspsElement = dss.getDictionaryObject("OCSPs");
			if (ocspsElement instanceof COSArray) {
				printStreamsFromArray((COSArray) ocspsElement, "Ocsp");
			}
			COSBase crlElement = dss.getDictionaryObject("CRLs");
			if (crlElement instanceof COSArray) {
				printStreamsFromArray((COSArray) crlElement, "CRL");
			}
		}
	}

	private void printStreamsFromArray(COSArray elements, String description) throws IOException {
		for (COSBase baseElem : elements) {
			COSObject streamObj = (COSObject) baseElem;
			if (streamObj.getObject() instanceof COSStream) {
				COSStream cosStream = (COSStream) streamObj.getObject();

				InputStream input = cosStream.createInputStream();
				 IOUtils.toByteArray(input);
				input.close();

			}
		}
	}
}