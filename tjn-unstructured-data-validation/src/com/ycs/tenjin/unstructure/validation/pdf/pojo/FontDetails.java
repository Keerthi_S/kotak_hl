package com.ycs.tenjin.unstructure.validation.pdf.pojo;

public class FontDetails {
	private String fontStyle;
	private String fontName;
	private float fontSize;
	
	public FontDetails(String fontStyle, String fontName, float fontSize) {
		this.fontStyle = fontStyle;
		this.fontName = fontName;
		this.fontSize = fontSize;
	}
	
	public String getFontStyle() {
		return fontStyle;
	}
	public void setFontStyle(String fontStyle) {
		this.fontStyle = fontStyle;
	}
	public String getFontName() {
		return fontName;
	}
	public void setFontName(String fontName) {
		this.fontName = fontName;
	}
	public float getFontSize() {
		return fontSize;
	}
	public void setFontSize(float fontSize) {
		this.fontSize = fontSize;
	}
	
	@Override
	public boolean equals(Object arg0) {
		FontDetails details = (FontDetails) arg0;
		if(!details.getFontName().equalsIgnoreCase(this.getFontName()) || details.getFontSize() != this.getFontSize() || !details.getFontStyle().equalsIgnoreCase(this.getFontStyle())){
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "FontDetails [fontStyle=" + fontStyle + ", fontName=" + fontName
				+ ", fontSize=" + fontSize + "]";
	}
}