package com.ycs.tenjin.unstructure.validation.pdf;

import java.io.File;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.ycs.tenjin.ValidationDetails;
import com.ycs.tenjin.ValidationType;
import com.ycs.tenjin.unstructure.abstraction.UnstructureValidationAbstract;
import com.ycs.tenjin.unstructure.validation.pdf.image.PDFImageValidation;
import com.ycs.tenjin.unstructure.validation.pdf.pojo.ImageProperties;
import com.ycs.tenjin.unstructure.validation.pdf.pojo.PDFValidationDetails;
import com.ycs.tenjin.unstructure.validation.pdf.pojo.SignatureProperties;
import com.ycs.tenjin.unstructure.validation.pdf.pojo.TextProperties;
import com.ycs.tenjin.unstructure.validation.pdf.signature.PDFSignatureValidation;
import com.ycs.tenjin.unstructure.validation.pdf.text.PDFTextValidation;

public class PDFValidation extends UnstructureValidationAbstract {
	
	private static Logger logger = LoggerFactory
			.getLogger(PDFValidation.class);
	private ValidationDetails validationDetails;
	public PDFValidation(ValidationDetails validationDetails) {
		super(validationDetails);
		this.validationDetails = validationDetails;
	}

	@Override
	public ValidationDetails validate() {
		System.out.println("PDF Validation");
		if(this.validationDetails.getValidationType().equals(ValidationType.PDFFORMAT)){
			pdfFormatValidation();
		}else{
			pdfSimpleValidation();
		}
		return this.validationDetails;
	}
	
	private void pdfFormatValidation(){
		PDFValidationDetails pdfValidationDetails = null;
		try (
			PDDocument baseFileDoc = PDDocument.load(new File(this.validationDetails.getBaseFilePath()));
			PDDocument inputFileDoc = PDDocument.load(new File(this.validationDetails.getInputFilePath()));){
			
			//Text and Format validation
			List<TextProperties> textProperties = PDFTextValidation.validateTextandFormat(baseFileDoc, inputFileDoc, this.validationDetails.getPdfValidationType());
			
			//Image validation
			List<ImageProperties> imageProperties = PDFImageValidation.validateImage(baseFileDoc, inputFileDoc);
			
			//Signature Validation
			List<SignatureProperties> signatureProperties = new PDFSignatureValidation().validateSignatures(this.validationDetails.getBaseFilePath(), this.validationDetails.getInputFilePath());
			
			pdfValidationDetails = new PDFValidationDetails();
			pdfValidationDetails.setImageProperties(imageProperties);
			pdfValidationDetails.setSignatureProperties(signatureProperties);
			pdfValidationDetails.setTextProperties(textProperties);
		}catch(Exception e){
			logger.error("Error ", e);
		}
		this.validationDetails.setResult(pdfValidationDetails);
	}
	
	private void pdfSimpleValidation(){
		try {
			PdfReader pdf = null;
			if(this.validationDetails.isPasswordProtected()){
				pdf = new PdfReader(this.validationDetails.getBaseFilePath(), this.validationDetails.getPassword().getBytes());
			}else{
				pdf = new PdfReader(this.validationDetails.getBaseFilePath());
			}

			if(this.validationDetails.getSearchPage() > -1){
				String text = PdfTextExtractor.getTextFromPage(pdf, this.validationDetails.getSearchPage());
				int count = StringUtils.countMatches(text, this.validationDetails.getSearchText());
				if(this.validationDetails.getExpectedOccurance() > -1){
					if(count == this.validationDetails.getExpectedOccurance()){
						this.validationDetails.setResult(true);
					}else{
						this.validationDetails.setResult(false);
					}
				}else{
					String returnData = count > 0 ? count + " match(s) found on page "  + this.validationDetails.getSearchPage() + "\n" : "Match not found on page " +this.validationDetails.getSearchPage();
					this.validationDetails.setResult(returnData);
				}
			}else{
				String stringOutput = "";
				String pageText = "";
				for (int i = 1; i <= pdf.getNumberOfPages(); i++) {
					pageText += PdfTextExtractor.getTextFromPage(pdf, i);
					if(this.validationDetails.getExpectedOccurance() == -1){
						int count = StringUtils.countMatches(pageText, this.validationDetails.getSearchText());
						stringOutput += count + " match(s) found on page "  + i + "\n";
						pageText = "";
					}
				}
				
				if(this.validationDetails.getExpectedOccurance() > -1){
					int count = StringUtils.countMatches(pageText, this.validationDetails.getSearchText());
					if(count == this.validationDetails.getExpectedOccurance()){
						this.validationDetails.setResult(true);
					}else{
						this.validationDetails.setResult(false);
					}
				}else{
					this.validationDetails.setResult(stringOutput);
				}
			}
		} catch (Exception e) {
			logger.error("Error ", e);
			System.out.println("could not process the PDF file");
		}
	}
}