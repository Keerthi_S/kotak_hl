package com.ycs.tenjin.unstructure.validation.pdf.pojo;

import java.util.List;

public class SignatureProperties {
	private String signDate;
	private String algorithm;
	private String publicKey;
	private List<Object> subjectAlternativeNames;
	private String subjectName;
	
	public String getSignDate() {
		return signDate;
	}
	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}
	public String getAlgorithm() {
		return algorithm;
	}
	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}
	public String getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	public List<Object> getSubjectAlternativeNames() {
		return subjectAlternativeNames;
	}
	public void setSubjectAlternativeNames(List<Object> subjectAlternativeNames) {
		this.subjectAlternativeNames = subjectAlternativeNames;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	@Override
	public String toString() {
		return "PDFSignature [signDate=" + signDate + ", algorithm="
				+ algorithm + ", publicKey=" + publicKey
				+ ", subjectAlternativeNames=" + subjectAlternativeNames
				+ ", subjectName=" + subjectName + "]";
	}
}
