package com.ycs.tenjin.unstructure.validation.pdf.signature;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.security.cert.X509Extension;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.encryption.SecurityProvider;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.cert.ocsp.BasicOCSPResp;
import org.bouncycastle.cert.ocsp.OCSPException;
import org.bouncycastle.cert.ocsp.OCSPResp;

public final class CertificateVerifier {
	private static final Log LOG = LogFactory.getLog(CertificateVerifier.class);

	private CertificateVerifier() {}

	public static PKIXCertPathBuilderResult verifyCertificate(X509Certificate cert, Set<X509Certificate> additionalCerts,boolean verifySelfSignedCert, Date signDate) throws CertificateVerificationException {
		try {

			Set<X509Certificate> certSet = CertificateVerifier.downloadExtraCertificates(cert);
			int downloadSize = certSet.size();
			certSet.addAll(additionalCerts);
			if (downloadSize > 0) {
				LOG.info("CA issuers: "	+ (certSet.size() - additionalCerts.size())	+ " downloaded certificate(s) are new");
			}

			Set<X509Certificate> intermediateCerts = new HashSet<X509Certificate>();
			Set<TrustAnchor> trustAnchors = new HashSet<TrustAnchor>();
			for (X509Certificate additionalCert : certSet) {
				if (isSelfSigned(additionalCert)) {
					trustAnchors.add(new TrustAnchor(additionalCert, null));
				} else {
					intermediateCerts.add(additionalCert);
				}
			}

			if (trustAnchors.isEmpty()) {
				throw new CertificateVerificationException("No root certificate in the chain");
			}

			PKIXCertPathBuilderResult verifiedCertChain = verifyCertificate(cert, trustAnchors, intermediateCerts, signDate);

			LOG.info("Certification chain verified successfully");

			checkRevocations(cert, certSet, signDate);

			return verifiedCertChain;
		} catch (CertPathBuilderException certPathEx) {
			throw new CertificateVerificationException("Error building certification path: "+ cert.getSubjectX500Principal(), certPathEx);
		} catch (CertificateVerificationException cvex) {
			throw cvex;
		} catch (Exception ex) {
			throw new CertificateVerificationException("Error verifying the certificate: "+ cert.getSubjectX500Principal(), ex);
		}
	}

	private static void checkRevocations(X509Certificate cert, Set<X509Certificate> additionalCerts, Date signDate)	throws IOException, CertificateVerificationException, OCSPException, RevokedCertificateException,GeneralSecurityException {
		if (isSelfSigned(cert)) {
			return;
		}
		X509Certificate issuerCert = null;
		for (X509Certificate additionalCert : additionalCerts) {
			if (cert.getIssuerX500Principal().equals(additionalCert.getSubjectX500Principal())) {
				issuerCert = additionalCert;
				break;
			}
		}
		String ocspURL = extractOCSPURL(cert);
		if (ocspURL != null) {
			OcspHelper ocspHelper = new OcspHelper(cert, signDate, issuerCert,additionalCerts, ocspURL);
			try {
				verifyOCSP(ocspHelper, additionalCerts);
			} catch (IOException ex) {
				LOG.warn("IOException trying OCSP, will try CRL", ex);
				CRLVerifier.verifyCertificateCRLs(cert, signDate,additionalCerts);
			}
		} else {
			LOG.info("OCSP not available, will try CRL");
			CRLVerifier.verifyCertificateCRLs(cert, signDate, additionalCerts);
		}

		checkRevocations(issuerCert, additionalCerts, signDate);
	}

	public static boolean isSelfSigned(X509Certificate cert)
			throws GeneralSecurityException {
		try {
			PublicKey key = cert.getPublicKey();
			cert.verify(key, SecurityProvider.getProvider().getName());
			return true;
		} catch (SignatureException ex) {
			LOG.debug("Couldn't get signature information - returning false",ex);
			return false;
		} catch (InvalidKeyException ex) {
			LOG.debug("Couldn't get signature information - returning false", ex);
			return false;
		} catch (IOException ex) {
			LOG.debug("Couldn't get signature information - returning false",ex);
			return false;
		}
	}

	public static Set<X509Certificate> downloadExtraCertificates(X509Extension ext) {
		Set<X509Certificate> resultSet = new HashSet<X509Certificate>();
		byte[] authorityExtensionValue = ext.getExtensionValue(Extension.authorityInfoAccess.getId());
		if (authorityExtensionValue == null) {
			return resultSet;
		}
		ASN1Primitive asn1Prim;
		try {
			asn1Prim = JcaX509ExtensionUtils.parseExtensionValue(authorityExtensionValue);
		} catch (IOException ex) {
			LOG.warn(ex.getMessage(), ex);
			return resultSet;
		}
		if (!(asn1Prim instanceof ASN1Sequence)) {
			LOG.warn("ASN1Sequence expected, got " + asn1Prim.getClass().getSimpleName());
			return resultSet;
		}
		ASN1Sequence asn1Seq = (ASN1Sequence) asn1Prim;
		Enumeration<?> objects = asn1Seq.getObjects();
		while (objects.hasMoreElements()) {
			ASN1Sequence obj = (ASN1Sequence) objects.nextElement();
			ASN1ObjectIdentifier oid = (ASN1ObjectIdentifier) obj.getObjectAt(0);
			if (!oid.equals(X509ObjectIdentifiers.id_ad_caIssuers)) {
				continue;
			}
			DERTaggedObject location = (DERTaggedObject) obj.getObjectAt(1);
			DEROctetString uri = (DEROctetString) location.getObject();
			InputStream in = null;
			try {
				String urlString = new String(uri.getOctets());
				LOG.info("CA issuers URL: " + urlString);
				in = new URL(urlString).openStream();
				CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
				Collection<? extends Certificate> altCerts = certFactory.generateCertificates(in);
				for (Certificate altCert : altCerts) {
					resultSet.add((X509Certificate) altCert);
				}
				LOG.info("CA issuers URL: " + altCerts.size()+ " certificate(s) downloaded");
			} catch (IOException ex) {
				LOG.warn(ex.getMessage(), ex);
			} catch (CertificateException ex) {
				LOG.warn(ex.getMessage(), ex);
			} finally {
				IOUtils.closeQuietly(in);
			}
		}
		LOG.info("CA issuers: Downloaded " + resultSet.size() + " certificate(s) total");
		return resultSet;
	}

	private static PKIXCertPathBuilderResult verifyCertificate(X509Certificate cert, Set<TrustAnchor> trustAnchors,	Set<X509Certificate> intermediateCerts, Date signDate) throws GeneralSecurityException {
		X509CertSelector selector = new X509CertSelector();
		selector.setCertificate(cert);

		PKIXBuilderParameters pkixParams = new PKIXBuilderParameters(trustAnchors, selector);

		pkixParams.setRevocationEnabled(false);

		pkixParams.setPolicyQualifiersRejected(false);

		pkixParams.setDate(signDate);

		CertStore intermediateCertStore = CertStore.getInstance("Collection",new CollectionCertStoreParameters(intermediateCerts));
		pkixParams.addCertStore(intermediateCertStore);

		CertPathBuilder builder = CertPathBuilder.getInstance("PKIX");
		return (PKIXCertPathBuilderResult) builder.build(pkixParams);
	}

	private static String extractOCSPURL(X509Certificate cert)throws IOException {
		byte[] authorityExtensionValue = cert.getExtensionValue(Extension.authorityInfoAccess.getId());
		if (authorityExtensionValue != null) {
			ASN1Sequence asn1Seq = (ASN1Sequence) JcaX509ExtensionUtils.parseExtensionValue(authorityExtensionValue);
			Enumeration<?> objects = asn1Seq.getObjects();
			while (objects.hasMoreElements()) {
				ASN1Sequence obj = (ASN1Sequence) objects.nextElement();
				ASN1ObjectIdentifier oid = (ASN1ObjectIdentifier) obj.getObjectAt(0);
				DERTaggedObject location = (DERTaggedObject) obj.getObjectAt(1);
				if (oid.equals(X509ObjectIdentifiers.id_ad_ocsp) && location.getTagNo() == GeneralName.uniformResourceIdentifier) {
					DEROctetString url = (DEROctetString) location.getObject();
					String ocspURL = new String(url.getOctets());
					LOG.info("OCSP URL: " + ocspURL);
					return ocspURL;
				}
			}
		}
		return null;
	}

	private static void verifyOCSP(OcspHelper ocspHelper,Set<X509Certificate> additionalCerts)	throws RevokedCertificateException, IOException, OCSPException,	CertificateVerificationException {
		Date now = Calendar.getInstance().getTime();
		OCSPResp ocspResponse;
		ocspResponse = ocspHelper.getResponseOcsp();
		if (ocspResponse.getStatus() != OCSPResp.SUCCESSFUL) {
			throw new CertificateVerificationException("OCSP check not successful, status: "+ ocspResponse.getStatus());
		}
		LOG.info("OCSP check successful");

		BasicOCSPResp basicResponse = (BasicOCSPResp) ocspResponse.getResponseObject();
		X509Certificate ocspResponderCertificate = ocspHelper.getOcspResponderCertificate();
		if (ocspResponderCertificate.getExtensionValue(OCSPObjectIdentifiers.id_pkix_ocsp_nocheck.getId()) != null) {
			LOG.info("Revocation check of OCSP responder certificate skipped (id-pkix-ocsp-nocheck is set)");
			return;
		}

		LOG.info("Check of OCSP responder certificate");
		Set<X509Certificate> additionalCerts2 = new HashSet<X509Certificate>(additionalCerts);
		JcaX509CertificateConverter certificateConverter = new JcaX509CertificateConverter();
		for (X509CertificateHolder certHolder : basicResponse.getCerts()) {
			try {
				X509Certificate cert = certificateConverter.getCertificate(certHolder);
				if (!ocspResponderCertificate.equals(cert)) {
					additionalCerts2.add(cert);
				}
			} catch (CertificateException ex) {
				LOG.error(ex, ex);
			}
		}
		CertificateVerifier.verifyCertificate(ocspResponderCertificate,additionalCerts2, true, now);
		LOG.info("Check of OCSP responder certificate done");
	}
}