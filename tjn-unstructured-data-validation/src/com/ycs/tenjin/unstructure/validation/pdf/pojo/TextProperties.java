package com.ycs.tenjin.unstructure.validation.pdf.pojo;

import java.awt.geom.Rectangle2D;
import java.util.List;

public class TextProperties {
	private String value;
	private List<FontDetails> fontDetails;
	private Rectangle2D coordinates;
	private String desc = "";
	
	public TextProperties(){}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	public List<FontDetails> getFontDetails() {
		return fontDetails;
	}

	public void setFontDetails(List<FontDetails> fontDetails) {
		this.fontDetails = fontDetails;
	}

	public Rectangle2D getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Rectangle2D coordinates) {
		this.coordinates = coordinates;
	}
	
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public boolean valueEquals(TextProperties prop){
		if(this.getValue().equals(prop.getValue())){
			return true;
		}else {
			for(int i = 0; i < this.getFontDetails().size(); i++){
			if(!this.fontDetails.get(i).equals(prop.getFontDetails().get(i))){
				if(this.desc.isEmpty() && prop.desc.isEmpty()){
					this.setDesc("Fonts Mismatch.");
					prop.setDesc("Fonts Mismatch.");
				}
				return false;
			}
		}
	}
		return false;
	}
	
	@Override
	public boolean equals(Object arg0) {
		TextProperties prop = (TextProperties)arg0;
		
		
		if((this.getCoordinates().getY() == prop.getCoordinates().getY()) && this.getValue().equalsIgnoreCase(prop.getValue())){
			return true;
		}
		return false;
	}

	public boolean fontEquals(TextProperties prop){
		for(int i = 0; i < this.getFontDetails().size(); i++){
			if(!this.fontDetails.get(i).equals(prop.getFontDetails().get(i))){
				return false;
			}
		}
		return true;
	}
	
	public boolean staticEquals(Object arg0) {
		TextProperties prop = (TextProperties)arg0;
		if(prop.getFontDetails().size() != this.getFontDetails().size()){
			return false;
		}else{
			for(int i = 0; i < this.getFontDetails().size(); i++){
				if(!this.fontDetails.get(i).equals(prop.getFontDetails().get(i))){
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return "TextProperties [value=" + value + ", coordinates=" + coordinates + ", desc="
				+ desc + "]";
	}
}
