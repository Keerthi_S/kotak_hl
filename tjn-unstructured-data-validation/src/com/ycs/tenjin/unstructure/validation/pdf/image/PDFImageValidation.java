package com.ycs.tenjin.unstructure.validation.pdf.image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.unstructure.validation.pdf.pojo.ImageProperties;

public class PDFImageValidation {
	private static Logger logger = LoggerFactory
			.getLogger(PDFImageValidation.class);
	

	public static List<ImageProperties> validateImage(PDDocument baseDoc, PDDocument inputDoc) throws IOException {
		List<ImageProperties> imageProperties = new ArrayList<ImageProperties>();
		try{
			GetImageLocationsAndSize baseImageContent = getImageStripper(baseDoc);
			GetImageLocationsAndSize inputImageContent = getImageStripper(inputDoc);
			
			for(int i = 0; i < baseImageContent.getContentDetails().size(); i++){
				ImageProperties baseImageProperties = baseImageContent.getContentDetails().get(i);
				ImageProperties inputImageProperties = inputImageContent.getContentDetails().get(i);
				
				if(!((baseImageProperties.getCoordinates().getTranslateX() == inputImageProperties.getCoordinates().getTranslateX()) && (baseImageProperties.getCoordinates().getTranslateY() == inputImageProperties.getCoordinates().getTranslateY()))){
					baseImageProperties.getMessages().add("Image co-ordinates are different. On base document [" + baseImageProperties.getCoordinates().getTranslateX() +","+ baseImageProperties.getCoordinates().getTranslateY() +"], input document ["+inputImageProperties.getCoordinates().getTranslateX()+"],"+inputImageProperties.getCoordinates().getTranslateY()+"]");
				}
				compareImages(baseImageProperties, inputImageProperties);
			}
		}catch(Exception e){
			logger.error("Error ", e);
		}
		return imageProperties;
	}
	
	private static void compareImages(ImageProperties baseImageProperties, ImageProperties inputImageProperties) throws Exception{
		LinkedList<Rect> rectangles = null;
		BufferedImage inputImage = inputImageProperties.getImage();
		BufferedImage baseImage   = baseImageProperties.getImage();

		int xOffset = 0;
		int yOffset = 0;
		
		int widthArraySize = inputImage.getWidth();
		int hightArraySize = inputImage.getHeight();
		
		if(baseImage.getWidth() > inputImage.getWidth()){
			widthArraySize = baseImage.getWidth();
		}
		
		if(baseImage.getHeight() > inputImage.getHeight()){
			hightArraySize = baseImage.getHeight();
		}
		
		// Creating 2 Two Dimensional Arrays for Image Processing
		int[][] baseFrame = new int[hightArraySize][widthArraySize];
		int[][] inputFrame = new int[hightArraySize][widthArraySize];

		// Creating 2 Single Dimensional Arrays to retrieve the Pixel Values
		int baseImagePixelLength = baseImage.getWidth() * baseImage.getHeight();
		int inputImagePixelLength = inputImage.getWidth() * inputImage.getHeight();
		
		int imagePixelLength = baseImagePixelLength;
		
		if(baseImagePixelLength < inputImagePixelLength){
			imagePixelLength = inputImagePixelLength;
		}
		
		int[] baseImagePixel   = new int[imagePixelLength];
		int[] inputImagePixel  = new int[imagePixelLength];

		// Reading the Pixels from the Buffered Image
		baseImage.getRGB(0, 0, baseImage.getWidth(), baseImage.getHeight(), baseImagePixel, 0, baseImage.getWidth());
		inputImage.getRGB(0, 0, inputImage.getWidth(), inputImage.getHeight(), inputImagePixel, 0, inputImage.getWidth());

		// Transporting the Single Dimensional Arrays to Two Dimensional Array
		for(int row = 0; row < baseImage.getHeight(); row++) {
			System.arraycopy(baseImagePixel, (row * baseImage.getWidth()), baseFrame[row], 0, baseImage.getWidth());
			System.arraycopy(inputImagePixel, (row * inputImage.getWidth()), inputFrame[row], 0, inputImage.getWidth());
		}

		// Finding Differences between the Base Image and ScreenShot Image
		ImageDifference imDiff = new ImageDifference();
		rectangles = imDiff.differenceImage(baseFrame, inputFrame, xOffset, yOffset, baseImage.getWidth(), baseImage.getHeight());

		// Displaying the rectangles found
		for(Rect rect : rectangles) {
			System.out.println("Difference Identified : " + inputImageProperties.getImageDesc());
			for(int i = rect.y; i < rect.y + rect.w; i++) {
				inputImagePixel[ ( rect.x               * inputImage.getWidth()) + i ] = 0xFF0000FF;
				inputImagePixel[ ((rect.x + rect.h - 1) * inputImage.getWidth()) + i ] = 0xFF0000FF;
			}

			for(int j = rect.x; j < rect.x + rect.h; j++) {
				inputImagePixel[ (j * inputImage.getWidth()) + rect.y                ] = 0xFF0000FF;
				inputImagePixel[ (j * inputImage.getWidth()) + (rect.y + rect.w - 1) ] = 0xFF0000FF;
			}
			inputImage.setRGB(0, 0, inputImage.getWidth(), inputImage.getHeight(), inputImagePixel, 0, inputImage.getWidth());
			File file = File.createTempFile("Tenjin_"+ GetImageLocationsAndSize.getRawTimeStamp(), ".png");
			ImageIO.write(inputImage, "PNG", file);
			inputImageProperties.setImageFile(file);
		}
	}

	private static GetImageLocationsAndSize getImageStripper(PDDocument doc){
		try {
			GetImageLocationsAndSize imageStripper = new GetImageLocationsAndSize();
			for(PDPage bpage : doc.getPages()){
				imageStripper.processPage(bpage);
			}
			return imageStripper;
		} catch (IOException e) {}
		return null;
	}
}