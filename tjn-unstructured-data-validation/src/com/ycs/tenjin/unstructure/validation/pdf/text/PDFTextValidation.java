package com.ycs.tenjin.unstructure.validation.pdf.text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.unstructure.validation.pdf.pojo.TextProperties;

public class PDFTextValidation {
	private static Logger logger = LoggerFactory
			.getLogger(PDFTextValidation.class);
	
	public static List<TextProperties> validateTextandFormat(PDDocument baseDoc, PDDocument inputDoc, String check) throws Exception{
		List<TextProperties> textProperties = new ArrayList<TextProperties>();
		try {
			PDFTextStripperExtend baseStripper = getTextStripper(baseDoc);
			PDFTextStripperExtend inputStripper = getTextStripper(inputDoc);
			List<TextProperties> inputTextProperties= new ArrayList<TextProperties>();
			
			if(check.equalsIgnoreCase("dynamic")) {
				for(TextProperties baseTextProperty : baseStripper.getContentDetails()){
					for(TextProperties inputTextProperty : inputStripper.getContentDetails()){
						if(baseTextProperty.equals(inputTextProperty)){
							inputTextProperties.add(inputTextProperty);
							break;
						}
					}
				}
				inputStripper.getContentDetails().removeAll(inputTextProperties);
				getDifferenceMap(inputStripper.getContentDetails()).forEach((k, v) -> v.stream().forEach(a -> textProperties.add(a)));
			} else if(check.equalsIgnoreCase("static")) {
				for(TextProperties baseTextProperty : baseStripper.getContentDetails()){
					for(TextProperties inputTextProperty : inputStripper.getContentDetails()){
						if(baseTextProperty.equals(inputTextProperty) && !baseTextProperty.fontEquals(inputTextProperty)){
							inputTextProperties.add(inputTextProperty);
							break;
						}
					}
				}
				getDifferenceMap(inputTextProperties).forEach((k, v) -> v.stream().forEach(a -> textProperties.add(a)));
			}
			textProperties.stream().forEach(a -> System.out.println(a));
		} catch(Exception e){
			logger.error("Error ", e);
		}
		return textProperties;
	}
	
	private static Map<Double, List<TextProperties>> getDifferenceMap(List<TextProperties> list){
		Map<Double, List<TextProperties>> differenceMap = new TreeMap<Double, List<TextProperties>>();
		for(TextProperties text : list){
			List<TextProperties> str = null;
			if(differenceMap.containsKey(text.getCoordinates().getY())){
				str = differenceMap.get(text.getCoordinates().getY());
			}else{
				str = new ArrayList<TextProperties>();
			}
			str.add(text);
			differenceMap.put(text.getCoordinates().getY(), str);
		}
		return differenceMap;
	}
	
	private static PDFTextStripperExtend getTextStripper(PDDocument doc){
		try {
			PDFTextStripperExtend inputStripper = new PDFTextStripperExtend();
			inputStripper.setSortByPosition(true);
			inputStripper.setStartPage(0);
			inputStripper.setEndPage(doc.getNumberOfPages());
			inputStripper.setWordSeparator(" ");
			Writer inputWriter = new OutputStreamWriter(new ByteArrayOutputStream());
			inputStripper.writeText(doc, inputWriter);
			return inputStripper;
		} catch (IOException e) {}
		return null;
	}
}
