package com.ycs.tenjin.unstructure.validation.pdf.image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import javax.imageio.ImageIO;

public class ImageDifference {
	long start = 0, end = 0;

	public LinkedList<Rect> differenceImage(int[][] baseFrame,
			int[][] inputFrame, int xOffset, int yOffset, int width, int height) {
		// Code starts here
		int xRover = 0;
		int yRover = 0;
		int index = 0;
		int limit = 0;
		int rover = 0;

		boolean isRectChanged = false;
		boolean shouldSkip = false;

		LinkedList<Rect> rectangles = new LinkedList<Rect>();
		Rect rect = null;

		start = System.nanoTime();

		// xRover - Rovers over the height of 2D Array
		// yRover - Rovers over the width of 2D Array
		int verticalLimit = xOffset + height;
		int horizontalLimit = yOffset + width;

		for (xRover = xOffset; xRover < verticalLimit; xRover += 1) {
			for (yRover = yOffset; yRover < horizontalLimit; yRover += 1) {

				if (baseFrame[xRover][yRover] != inputFrame[xRover][yRover]) {
					// Skip over the already processed Rectangles
					for (Rect itrRect : rectangles) {
						if (((xRover < itrRect.x + itrRect.h) && (xRover >= itrRect.x))
								&& ((yRover < itrRect.y + itrRect.w) && (yRover >= itrRect.y))) {
							shouldSkip = true;
							yRover = itrRect.y + itrRect.w - 1;
							break;
						} 
					} 

					if (shouldSkip) {
						shouldSkip = false;
						continue;
					} // End if(shouldSkip)

					rect = new Rect();

					rect.x = ((xRover - 1) < xOffset) ? xOffset : (xRover - 1);
					rect.y = ((yRover - 1) < yOffset) ? yOffset : (yRover - 1);
					rect.w = 2;
					rect.h = 2;

					/*
					 * Boolean variable used to re-scan the currently found
					 * rectangle for any change due to previous scanning of
					 * boundaries
					 */
					isRectChanged = true;

					while (isRectChanged) {
						isRectChanged = false;
						index = 0;

						/* I */
						/* Scanning of left-side boundary of rectangle */
						index = rect.x;
						limit = rect.x + rect.h;
						while (index < limit && rect.y != yOffset) {
							if (baseFrame[index][rect.y] != inputFrame[index][rect.y]) {
								isRectChanged = true;
								rect.y = rect.y - 1;
								rect.w = rect.w + 1;
								index = rect.x;
								continue;
							} // End if(baseFrame[index][rect.y] !=
								// screenShot[index][rect.y])

							index = index + 1;
							;
						} // End while(index < limit && rect.y != yOffset)

						/* II */
						/* Scanning of bottom boundary of rectangle */
						index = rect.y;
						limit = rect.y + rect.w;
						while ((index < limit)
								&& (rect.x + rect.h != verticalLimit)) {
							rover = rect.x + rect.h - 1;
							if (baseFrame[rover][index] != inputFrame[rover][index]) {
								isRectChanged = true;
								rect.h = rect.h + 1;
								index = rect.y;
								continue;
							} // End if(baseFrame[rover][index] !=
								// screenShot[rover][index])

							index = index + 1;
						} // End while( (index < limit) && (rect.x + rect.h !=
							// verticalLimit) )

						/* III */
						/* Scanning of right-side boundary of rectangle */
						index = rect.x;
						limit = rect.x + rect.h;
						while ((index < limit)
								&& (rect.y + rect.w != horizontalLimit)) {
							rover = rect.y + rect.w - 1;
							if (baseFrame[index][rover] != inputFrame[index][rover]) {
								isRectChanged = true;
								rect.w = rect.w + 1;
								index = rect.x;
								continue;
							} // End if(baseFrame[index][rover] !=
								// screenShot[index][rover])

							index = index + 1;
						} // End while( (index < limit) && (rect.y + rect.w !=
							// horizontalLimit) )

					} // while(isRectChanged)

					// Remove those rectangles that come inside "rect"
					// rectangle.
					int idx = 0;
					while (idx < rectangles.size()) {
						Rect r = rectangles.get(idx);
						if (((rect.x <= r.x) && (rect.x + rect.h >= r.x + r.h))
								&& ((rect.y <= r.y) && (rect.y + rect.w >= r.y
										+ r.w))) {
							rectangles.remove(r);
						} else {
							idx += 1;
						} // End if( ( (rect.x <= r.x) && (rect.x + rect.h >=
							// r.x + r.h) ) && ( (rect.y <= r.y) && (rect.y +
							// rect.w >= r.y + r.w) ) )
					} // End while(idx < rectangles.size())

					// Giving a head start to the yRover when a rectangle is
					// found
					rectangles.addFirst(rect);

					yRover = rect.y + rect.w - 1;
					rect = null;

				} // End if(baseFrame[xRover][yRover] !=
					// screenShot[xRover][yRover])
			} // End for(yRover = yOffset; yRover < horizontalLimit; yRover +=
				// 1)
		} // End for(xRover = xOffset; xRover < verticalLimit; xRover += 1)

		end = System.nanoTime();
		return rectangles;
	}

	public static void main(String[] args) throws IOException {
		LinkedList<Rect> rectangles = null;

		// Buffering the Base image and Screen Shot Image
		BufferedImage screenShotImg = ImageIO.read(new File(
				"C:\\Users\\Yethi\\Desktop\\INPUT2.PNG"));
		BufferedImage baseImg = ImageIO.read(new File(
				"C:\\Users\\Yethi\\Desktop\\INPUT1.PNG"));

		int width = baseImg.getWidth();
		int height = baseImg.getHeight();
		int xOffset = 0;
		int yOffset = 0;
		int length = baseImg.getWidth() * baseImg.getHeight();

		// Creating 2 Two Dimensional Arrays for Image Processing
		int[][] baseFrame = new int[height][width];
		int[][] screenShot = new int[screenShotImg.getHeight()][screenShotImg
				.getWidth()];

		// Creating 2 Single Dimensional Arrays to retrieve the Pixel Values
		int[] baseImgPix = new int[length];
		int[] screenShotImgPix = new int[screenShotImg.getWidth()
				* screenShotImg.getHeight()];

		// Reading the Pixels from the Buffered Image
		baseImg.getRGB(0, 0, baseImg.getWidth(), baseImg.getHeight(),
				baseImgPix, 0, baseImg.getWidth());
		screenShotImg.getRGB(0, 0, screenShotImg.getWidth(),
				screenShotImg.getHeight(), screenShotImgPix, 0,
				screenShotImg.getWidth());

		// Transporting the Single Dimensional Arrays to Two Dimensional Array
		long start = System.nanoTime();

		for (int row = 0; row < height; row++) {
			System.arraycopy(baseImgPix, (row * width), baseFrame[row], 0,
					width);
			System.arraycopy(screenShotImgPix,
					(row * screenShotImg.getWidth()), screenShot[row], 0,
					screenShotImg.getWidth());
		}

		long end = System.nanoTime();
		System.out
				.println("Array Copy : " + ((double) (end - start) / 1000000));

		// Finding Differences between the Base Image and ScreenShot Image
		ImageDifference imDiff = new ImageDifference();
		rectangles = imDiff.differenceImage(baseFrame, screenShot, xOffset,
				yOffset, width, height);

		// Displaying the rectangles found
		int index = 0;
		for (Rect rect : rectangles) {
			System.out.println("\nRect info : " + (++index));
			System.out.println("X : " + rect.x);
			System.out.println("Y : " + rect.y);
			System.out.println("W : " + rect.w);
			System.out.println("H : " + rect.h);

			// Creating Bounding Box
			for (int i = rect.y; i < rect.y + rect.w; i++) {
				screenShotImgPix[(rect.x * screenShotImg.getWidth()) + i] = 0xFF0000FF;
				screenShotImgPix[((rect.x + rect.h - 1) * screenShotImg
						.getWidth()) + i] = 0xFF0000FF;
			}

			for (int j = rect.x; j < rect.x + rect.h; j++) {
				screenShotImgPix[(j * screenShotImg.getWidth()) + rect.y] = 0xFF0000FF;
				screenShotImgPix[(j * screenShotImg.getWidth())
						+ (rect.y + rect.w - 1)] = 0xFF0000FF;
			}

		}

		// Creating the Resultant Image
		screenShotImg.setRGB(0, 0, screenShotImg.getWidth(), height,
				screenShotImgPix, 0, screenShotImg.getWidth());
		ImageIO.write(screenShotImg, "PNG", new File("D:\\result.png"));

		double d = ((double) (imDiff.end - imDiff.start) / 1000000);
		System.out.println("\nTotal Time : " + d + " ms" + "  Array Copy : "
				+ ((double) (end - start) / 1000000) + " ms");

	}
}