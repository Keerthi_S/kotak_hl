package com.ycs.tenjin.unstructure.validation.pdf.image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.pdfbox.contentstream.PDFStreamEngine;
import org.apache.pdfbox.contentstream.operator.DrawObject;
import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.contentstream.operator.OperatorName;
import org.apache.pdfbox.contentstream.operator.state.Concatenate;
import org.apache.pdfbox.contentstream.operator.state.Restore;
import org.apache.pdfbox.contentstream.operator.state.Save;
import org.apache.pdfbox.contentstream.operator.state.SetGraphicsStateParameters;
import org.apache.pdfbox.contentstream.operator.state.SetMatrix;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.util.Matrix;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.unstructure.validation.pdf.pojo.ImageProperties;

public class GetImageLocationsAndSize extends PDFStreamEngine {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(GetImageLocationsAndSize.class);
	private List<ImageProperties> contentDetails = new ArrayList<ImageProperties>();
	private int imageCount = 0;
	public GetImageLocationsAndSize() throws IOException{  
		addOperator(new Concatenate());  
		addOperator(new DrawObject());  
		addOperator(new SetGraphicsStateParameters());  
		addOperator(new Save());  
		addOperator(new Restore());  
		addOperator(new SetMatrix());  
	}

	public static String getRawTimeStamp(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		String date = sdf.format(new Date());
		return date;
	}
	
	protected void processOperator(Operator operator, List<COSBase>operands)
			throws IOException {
		ImageProperties imgProperties = new ImageProperties();
		Matrix ctmNew = null;
		int imageWidth = 0;
		int imageHeight = 0;
		File file = null;
		String operation = operator.getName();
		BufferedImage image1 = null;
		if(OperatorName.DRAW_OBJECT.equals(operation)){
			COSName objectName = (COSName) operands.get( 0 );
			PDXObject xobject = getResources().getXObject( objectName );

			if(xobject instanceof PDImageXObject){
				PDImageXObject image = (PDImageXObject)xobject;
				image1 = image.getImage();
				//file = File.createTempFile("Tenjin_"+getRawTimeStamp(), ".png");
				String directories = System.getProperty("java.io.tmpdir")+File.separator+"TenjinImage_"+File.separator+GetImageLocationsAndSize.getRawTimeStamp()+".png";
				file = new File(directories);
				file.getParentFile().mkdirs();
				if(file.createNewFile()) {
					ImageIO.write(image1, "png", file);
					
					imageWidth = image.getWidth();
					imageHeight = image.getHeight();

					ctmNew = getGraphicsState().getCurrentTransformationMatrix();
					
					imgProperties.setImageDesc("Image " + ++imageCount);
					imgProperties.setCoordinates(ctmNew);
					imgProperties.setImage(image1);
					imgProperties.setImageHeight(imageHeight);
					imgProperties.setImageWidth(imageWidth);
					imgProperties.setImageFile(file);
					this.contentDetails.add(imgProperties);
		            }
		            else {
		                logger.error("File is not created ");
		            }
				
				
			}else if(xobject instanceof PDFormXObject)  {
				PDFormXObject form = (PDFormXObject)xobject;
				showForm(form);
			}
		}else{
			super.processOperator( operator, operands );
		}
	}
	
	public List<ImageProperties> getContentDetails(){
		return this.contentDetails;
    }
}