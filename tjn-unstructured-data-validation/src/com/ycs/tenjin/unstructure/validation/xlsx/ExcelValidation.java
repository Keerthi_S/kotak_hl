package com.ycs.tenjin.unstructure.validation.xlsx;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.record.RecordFormatException;
import org.apache.poi.hssf.record.crypto.Biff8EncryptionKey;
import org.apache.poi.poifs.crypt.Decryptor;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;

import com.ycs.tenjin.ValidationDetails;
import com.ycs.tenjin.unstructure.abstraction.UnstructureValidationAbstract;

public class ExcelValidation extends UnstructureValidationAbstract {
	private ValidationDetails validationInput = null;
	public ExcelValidation(ValidationDetails validationInput) {
		super(validationInput);
		this.validationInput = validationInput;
	}

	@Override
	public ValidationDetails validate() {
		try {
			Workbook workBook = createWorkBook(validationInput.getBaseFilePath(), validationInput.getPassword());
			Sheet sheet = workBook.getSheet(validationInput.getSheetName());
			
			if (sheet == null)
				throw new Exception("Could not find sheet with given name : " + validationInput.getSheetName());

			if(validationInput.getCellReference()!=null){
				this.validationInput.setResult(Boolean.class);
				CellReference ref = new CellReference(validationInput.getCellReference());
				Row row = sheet.getRow(ref.getRow());
				if(row == null)
					throw new Exception("Given Row reference is incorrect.");
				
				Cell cell = row.getCell(ref.getCol());
				if(cell == null)
					throw new Exception("Given Cell reference is incorrect.");
				String cellValue = getCellValue(cell);
				if (validationInput.getSearchText().equalsIgnoreCase(cellValue))
					this.validationInput.setResult(true);
				else
					this.validationInput.setResult(false);
			}else if(validationInput.getCellRange() != null){
				CellRangeAddress cells = CellRangeAddress.valueOf(validationInput.getCellRange());
				processRowData(sheet, cells);
			}else{
				processRowData(sheet, null);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("could not process the EXCEL file");
		}
		return this.validationInput;
	}
	
	private static Workbook createWorkBook(String filePath, String password) throws Exception {
		Workbook workBook = null;
		try {
			try {
				workBook = WorkbookFactory.create(new File(filePath));
			} catch (Exception e) {
				if(e instanceof RecordFormatException && e.getMessage().contains("Unknown encryption")){
					Biff8EncryptionKey.setCurrentUserPassword(password);
					NPOIFSFileSystem fileSystem = new NPOIFSFileSystem(new File(filePath), true);
					workBook = WorkbookFactory.create(fileSystem);
				}else if(e instanceof EncryptedDocumentException){
					POIFSFileSystem fileSystem = new POIFSFileSystem(new FileInputStream(new File(filePath)));
					EncryptionInfo info = new EncryptionInfo(fileSystem);
					Decryptor decryptor = Decryptor.getInstance(info);
					decryptor.verifyPassword(password);
					InputStream dataStream = decryptor.getDataStream(fileSystem);
					workBook = WorkbookFactory.create(dataStream);
				}
			}
		} catch (Exception e) {
			throw e;
		}
		return workBook;
	}
	
	private void processRowData(Sheet sheet, CellRangeAddress cellRangeAddress){
		StringBuilder cellReferences = new StringBuilder();
		int count = 0, startRow, lastRow, startColumn, lastColumn = 0;
		if(cellRangeAddress != null){
			startRow = cellRangeAddress.getFirstRow();
			lastRow = cellRangeAddress.getLastRow();
		}else{
			startRow = sheet.getFirstRowNum();
			lastRow = sheet.getLastRowNum();
		}
		 
        for (int i = startRow; i <= lastRow; i++) {
            Row row = sheet.getRow(i);
            if(row == null)
                continue;
            
            if(cellRangeAddress != null){
    			startColumn = cellRangeAddress.getFirstColumn();
    			lastColumn = cellRangeAddress.getLastColumn();
    		}else{
    			startColumn= row.getFirstCellNum();
    			lastColumn = row.getLastCellNum();
    		}
            
            for (int j = startColumn; j <= lastColumn; j++) {
                Cell cell = row.getCell(j);
                if(cell != null){
                    String cellValue = getCellValue(cell);
                    if (this.validationInput.getSearchText().equalsIgnoreCase(cellValue)) {
                    	if(this.validationInput.getExpectedOccurance() > -1){
                    		count++;
                    	}else{
                    		cellReferences.append(new CellReference(cell).formatAsString()+", ");
                    	}
					}
                }
            }
        }
        if(this.validationInput.getExpectedOccurance() > 0){
        	if(this.validationInput.getExpectedOccurance() == count){
        		this.validationInput.setResult(true);
        	}else{
        		this.validationInput.setResult(false);
        	}
        }else{
        	if(cellReferences.toString().trim().isEmpty())
        		this.validationInput.setResult("Match not found on given " + (cellRangeAddress != null ? "cell range [" + cellRangeAddress.formatAsString() : "sheet [" + sheet.getSheetName()) + "].");
        	else
        		this.validationInput.setResult("Value identified on " + (cellRangeAddress != null ? "cell range [" + cellRangeAddress.formatAsString() : "sheet [" + sheet.getSheetName()) + "] on cells [" + cellReferences.toString().trim().substring(0, cellReferences.length()-2).trim() + "].");
        }
	}
	
	private static String getCellValue(Cell cell) {
		if(cell.getCellType() == (Cell.CELL_TYPE_STRING)){
			return cell.getStringCellValue();
		}else if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
			return String.valueOf(cell.getNumericCellValue());
		}else /*if(cell.getCellType() == Cell.CELL_TYPE_BLANK || cell.getCellType() == Cell.CELL_TYPE_FORMULA || cell.getCellType() == Cell.CELL_TYPE_BOOLEAN)*/{
			return "";
		}
	}
}