package com.ycs.tenjin.unstructure;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.ValidationDetails;
import com.ycs.tenjin.ValidationType;
import com.ycs.tenjin.unstructure.abstraction.UnstructureValidationAbstract;
import com.ycs.tenjin.unstructure.util.ValidationFactory;
import com.ycs.tenjin.unstructure.validation.pdf.pojo.PDFValidationDetails;

public class UnstructureValidation {
	
	private static Logger logger = LoggerFactory
			.getLogger(UnstructureValidation.class);
	
	
	public ValidationDetails getValidationInput(String dataValue){
		ValidationDetails validationInput = new ValidationDetails();
		try {
			String[] dataArray = dataValue.split(";;");
			validationInput.setBaseFilePath(dataArray[1]);
			
			if(dataArray[0].toLowerCase().contains("format")){
				validationInput.setPDFFormat(true);
			}

			String fileType = validationInput.getBaseFilePath().substring(validationInput.getBaseFilePath().lastIndexOf(".") + 1, validationInput.getBaseFilePath().length());
			validationInput.setFileType(fileType);
			validationInput.setValidationType(ValidationType.getValidationType(validationInput, fileType));
			
			File file = new File(validationInput.getBaseFilePath());
			if (!file.exists())
				throw new Exception("File not present in the given path. " + validationInput.getBaseFilePath());

			if(validationInput.isPDFFormat() && !validationInput.getFileType().equalsIgnoreCase("pdf")){
				throw new Exception("Formation validation is only for PDF Document.");
			}else if(validationInput.isPDFFormat()){
				if(!dataArray[2].substring(dataArray[2].lastIndexOf(".") + 1,	dataArray[2].length()).equalsIgnoreCase("pdf")){
					throw new Exception("Input file for PDF validation has to be PDF Format.");
				}
			}

			if (validationInput.getValidationType().equals(ValidationType.EXCEL)) {
				if (dataArray.length < 4 || dataArray.length > 7)
					throw new Exception("Arguments mismatch for EXCEL validation.");
				getExcelValidationInputs(validationInput, dataValue);
			}else if(validationInput.getValidationType().equals(ValidationType.PDFFORMAT)){
				if (dataArray.length != 4)
					throw new Exception("Arguments mismatch for PDF Format validation.");
				getPDFFormatValidationInputs(validationInput, dataValue);
			}else if (validationInput.getValidationType().equals(ValidationType.PDF)) {
				if (dataArray.length < 3 || dataArray.length > 6)
					throw new Exception("Arguments mismatch for PDF validation.");
				getPDFValidationInputs(validationInput, dataValue);
			}else {
				throw new Exception("Invalid file format. Tenjin can handle EXCEL and PDF file.");
			}
			
			UnstructureValidationAbstract unstructureValidation = (UnstructureValidationAbstract) ValidationFactory.getValidationObject(validationInput);
			unstructureValidation.validate();
			
		} catch (Exception e) {
			logger.error("Error ", e);
		}
		
		if(validationInput.getResult() instanceof Boolean)
			System.out.println((Boolean)validationInput.getResult());
		else if(validationInput.getResult() instanceof String)
			System.out.println((String)validationInput.getResult());
		else if(validationInput.getResult() instanceof PDFValidationDetails)
			System.out.println((PDFValidationDetails)validationInput.getResult());
		
		return validationInput;
	}
	
	private ValidationDetails getExcelValidationInputs(ValidationDetails validationInput, String dataValue){
		String[] dataArray = dataValue.split(";;");
		int length = dataArray.length;
		
		if(dataArray[0].toLowerCase().contains("password")){
			validationInput.setPasswordProtected(true);
			validationInput.setPassword(dataArray[2]);
			validationInput.setSheetName(dataArray[3]);
		}else{
			validationInput.setSheetName(dataArray[2]);
		}
		
		switch(length){
		case 4:
			validationInput.setSearchText(dataArray[3]);
			break;
		case 5:
			if(validationInput.isPasswordProtected()){
				validationInput.setSearchText(dataArray[4]);
			}else{
				try {
					int expectedOccurance = Integer.parseInt(dataArray[4]);
					validationInput.setSearchText(dataArray[3]);
					validationInput.setExpectedOccurance(expectedOccurance);
				} catch (NumberFormatException e) {
					if(dataArray[3].contains(":")){
						validationInput.setCellRange(dataArray[3]);
					}else{
						validationInput.setCellReference(dataArray[3]);
					}
					validationInput.setSearchText(dataArray[4]);
				}
			}
			break;
		case 6:
			if(validationInput.isPasswordProtected()){
				try {
					int expectedOccurance = Integer.parseInt(dataArray[5]);
					validationInput.setSearchText(dataArray[4]);
					validationInput.setExpectedOccurance(expectedOccurance);
				} catch (NumberFormatException e) {
					if(dataArray[4].contains(":")){
						validationInput.setCellRange(dataArray[4]);
					}else{
						validationInput.setCellReference(dataArray[4]);
					}
					validationInput.setSearchText(dataArray[5]);
				}
			}else{
				validationInput.setCellRange(dataArray[3]);
				validationInput.setSearchText(dataArray[4]);
				validationInput.setExpectedOccurance(Integer.parseInt(dataArray[5]));
			}
			break;
		case 7:
			validationInput.setCellRange(dataArray[4]);
			validationInput.setSearchText(dataArray[5]);
			validationInput.setExpectedOccurance(Integer.parseInt(dataArray[6]));
			break;
		}
		return validationInput;
	}
	
	private ValidationDetails getPDFFormatValidationInputs(ValidationDetails validationInput, String dataValue){
		String[] dataArray = dataValue.split(";;");
		validationInput.setInputFilePath(dataArray[2]);
		validationInput.setPdfValidationType(dataArray[3]);
		return validationInput;
	}
	
	private ValidationDetails getPDFValidationInputs(ValidationDetails validationInput, String dataValue) {
		
		String[] dataArray = dataValue.split(";;");
		int length = dataArray.length;
		
		if(dataArray[0].toLowerCase().contains("password")){
			validationInput.setPasswordProtected(true);
			validationInput.setPassword(dataArray[2]);
		}
		
		switch(length){
			case 3:
				validationInput.setSearchText(dataArray[2]);
				break;
			case 4:
				if(validationInput.isPasswordProtected()){
					validationInput.setSearchText(dataArray[3]);
				}else{
					try {
						int searchPage = Integer.parseInt(dataArray[2]);
						validationInput.setSearchPage(searchPage);
						validationInput.setSearchText(dataArray[3]);
					} catch (NumberFormatException e) {
						validationInput.setSearchText(dataArray[2]);
						validationInput.setExpectedOccurance(Integer.parseInt(dataArray[3]));
					}
				}
				break;
			case 5:
				if(validationInput.isPasswordProtected()){
					try {
						int searchPage = Integer.parseInt(dataArray[3]);
						validationInput.setSearchPage(searchPage);
						validationInput.setSearchText(dataArray[4]);
					} catch (NumberFormatException e) {
						validationInput.setSearchText(dataArray[3]);
						validationInput.setExpectedOccurance(Integer.parseInt(dataArray[4]));
					}
				}else{
					validationInput.setSearchPage(Integer.parseInt(dataArray[2]));
					validationInput.setSearchText(dataArray[3]);
					validationInput.setExpectedOccurance(Integer.parseInt(dataArray[4]));
				}
				break;
			case 6:
				validationInput.setSearchPage(Integer.parseInt(dataArray[3]));
				validationInput.setSearchText(dataArray[4]);
				validationInput.setExpectedOccurance(Integer.parseInt(dataArray[5]));
				break;
		}
		return validationInput;
	}
}