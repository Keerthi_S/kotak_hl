/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  WebMethodsImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Oct 27, 2017         Sameer Gupta          	Newly Added For  
 */

package com.ycs.tenjin.bridge.selenium.asv2.generic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.selenium.asv2.web.LearningMethodsAbstract;
import com.ycs.tenjin.util.PatternMatch;

public class WebMethodsImpl {

	private static final Logger logger = LoggerFactory
			.getLogger(LearningMethodsAbstract.class);
	protected DriverContext driverContext;

	public WebMethodsImpl(DriverContext driverContext) {
		this.driverContext = driverContext;
	}

	// Generic Methods
	// //////////////////////////////////////////////////////////////////////////////
	public void explicitWait(long seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (Exception ignore) {
		}
	}
	
	public String getTaskHandlerType() {
		
		String taskHandlerType = null;
		int len = Thread.currentThread().getStackTrace().length - 4;
		StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
		while (len > 3) {
			String stacktrace1 = stacktrace[len].getClassName().toString()
					.toLowerCase();
			if (stacktrace1.contains("learning")) {
				taskHandlerType = "learning";
				break;
			} else if (stacktrace1.contains("execution")) {
				taskHandlerType = "execution";
				break;
			} else if (stacktrace1.contains("extraction")) {
				taskHandlerType = "extraction";
				break;
			}
			len--;
		}
		return taskHandlerType;
	}

	// @@Messages Methods
	// //////////////////////////////////////////////////////////////////////////////
	private HashMap<String, ArrayList<String>> mapOfMessages = new HashMap<String, ArrayList<String>>();
	
	public HashMap<String, ArrayList<String>> getMapOfMessages() {
		return mapOfMessages;
	}

	public void setMapOfMessages(HashMap<String, ArrayList<String>> messages) {
		this.mapOfMessages.clear();
		this.mapOfMessages.putAll(messages);
	}

	public void addMessageToList(String message, String action) {
		this.addMessageToList(message, action, null);
	}

	public void addMessageToList(String message, String action,
			HashMap<String, ArrayList<String>> mapOfMessages) {
		if (message != null) {
			message = message.trim();
			if (!message.equals("")) {

				HashMap<String, ArrayList<String>> mapOfMessagesLocal = null;
				if (mapOfMessages!=null) {
					mapOfMessagesLocal = mapOfMessages;
				} else {
					mapOfMessagesLocal = this.mapOfMessages;
				}

				if (action == null || action.equals("")) {
					action = "ignore";
				} else {
					action = action.trim().toLowerCase();
				}

				if (action.equals("ignore")) {
					if (message.contains("%")) {
						action = "ignore_pattern";
					}
				}

				ArrayList<String> listMessages;
				if (mapOfMessagesLocal.containsKey(action)) {
					listMessages = mapOfMessagesLocal.get(action);
				} else {
					listMessages = new ArrayList<String>();
				}
				listMessages.add(message);
				mapOfMessagesLocal.put(action, listMessages);
			}
		}
	}
	
	public boolean validateMessage(String message) throws AutException {

		boolean validMessage = false;
		if (this.getMapOfMessages() != null) {

			ArrayList<String> listMessages = null;

			if (this.getMapOfMessages().containsKey("ignore")) {
				listMessages = this.getMapOfMessages().get("ignore");
				if (listMessages.contains(message)) {
					validMessage = true;
				}
			}

			if (!validMessage
					&& this.getMapOfMessages().containsKey("contains")) {
				listMessages = this.getMapOfMessages().get("contains");
				for (String messageToCheck : listMessages) {
					if (message.contains(messageToCheck)) {
						validMessage = true;
						break;
					}
				}
			}

			if (!validMessage
					&& this.getMapOfMessages().containsKey("ignore_pattern")) {
				listMessages = this.getMapOfMessages().get("ignore_pattern");
				for (String pattern : listMessages) {
					PatternMatch objPatternMatch = new PatternMatch();
					if (objPatternMatch.matchString(message, pattern, true)) {
						validMessage = true;
						break;
					}
				}
			}
		}
		return validMessage;
	}
	
	// Switch window methods
	// //////////////////////////////////////////////////////////////////////////////
	private String currentWindow = null;

	public void switchDriverToLatestWindow() {

		String lastWindow = null;

		for (String window : this.driverContext.getDriver().getWindowHandles()) {
			lastWindow = window;
		}

		if (lastWindow != null) {

			if (!(currentWindow != null && currentWindow.equals(lastWindow))) {
				this.currentWindow = lastWindow;
				this.driverContext.getDriver().switchTo().window(lastWindow);
				try {
					Thread.sleep(1000);

					this.driverContext.getDriver().manage().window().maximize();
				} catch (Exception e) {
				}
			}
		}
	}

	public void switchDriverToInitialWindow() {

		String firstWindowName = null;
		boolean firstWindow = true;
		for (String window : this.driverContext.getDriver().getWindowHandles()) {
			if (firstWindow) {
				firstWindowName = window;
				firstWindow = false;
			} else {
				this.driverContext.getDriver().switchTo().window(window);

				this.driverContext.getDriver().close();
			}
		}

		if (firstWindowName != null) {
			this.driverContext.getDriver().switchTo().window(firstWindowName);
		}
	}

	// Locate Element
	// //////////////////////////////////////////////////////////////////////////////
	public WebElement locateElement(By by) throws AutException {
		return locateElement(by, null, 30, false);
	}

	public WebElement locateElement(By by, int waitTimeInSec) throws AutException {
		return locateElement(by, null, waitTimeInSec, false);
	}

	public WebElement locateElement(By by, WebElement wrapper) throws AutException {
		return locateElement(by, wrapper, 30, false);
	}

	public WebElement locateElement(By by, WebElement wrapper, int waitTimeInSec) throws AutException {
		return locateElement(by, wrapper, waitTimeInSec, false);
	}

	public WebElement locateVisibleElement(By by) throws AutException {
		return locateElement(by, null, 30, true);
	}

	public WebElement locateVisibleElement(By by, int waitTimeInSec) throws AutException {
		return locateElement(by, null, waitTimeInSec, true);
	}

	public WebElement locateVisibleElement(By by, WebElement wrapper) throws AutException {
		return locateElement(by, wrapper, 30, true);
	}

	public WebElement locateVisibleElement(By by, WebElement wrapper, int waitTimeInSec) throws AutException {
		return locateElement(by, wrapper, waitTimeInSec, true);
	}

	private WebElement locateElement(By by, WebElement wrapper, int waitTimeInSec, boolean onlyVisible)
			throws AutException {

		WebElement element = null;
		int secCounter = 0;

		while (element == null && secCounter <= waitTimeInSec) {
			try {
				List<WebElement> listElements = null;
				if (wrapper == null) {
					listElements = this.driverContext.getDriver().findElements(by);
				} else {
					listElements = wrapper.findElements(by);
				}

				if (listElements != null) {
					for (WebElement ele : listElements) {

						if (onlyVisible) {
							if (ele.isDisplayed() && ele.isEnabled()) {
								element = ele;
								break;
							}
						} else {
							element = ele;
							break;
						}
					}
				}

			} catch (Exception e) {

			}

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
			}
			secCounter++;
		}

		return element;
	}

	// Retry to find element and click- For scenarios where element is found but
	// click fails, due to element not ready for click event.
	// //////////////////////////////////////////////////////////////////////////////
	public boolean retryToClickElement(By by, int maxTries) {
		return retryToClickElement(by, null, maxTries);
	}

	public boolean retryToClickElement(By by, WebElement wrapper, int maxTries) {

		logger.info("retryToClickElement - trying for - " + by.toString());
		boolean clicked = false;
		int noOfTries = 0;
		while (!clicked && noOfTries < maxTries) {
			try {
				WebElement element = null;
				if (wrapper == null) {
					element = this.driverContext.getDriver().findElement(by);
				} else {
					element = wrapper.findElement(by);
				}
				element.click();
				clicked = true;
			} catch (Exception ex) {
			}
			this.explicitWait(1);
			noOfTries = noOfTries + 1;
			logger.info("retryToClickElement - try - " + noOfTries);
		}
		return clicked;
	}

	// Scroll Methods
	// //////////////////////////////////////////////////////////////////////////////
	public void scrollToElement(WebElement element) {
		((JavascriptExecutor) this.driverContext.getDriver()).executeScript(
				"arguments[0].scrollIntoView(true);", element);
	}

	public void scrollToElementAndClick(WebElement element) {
		this.scrollToElement(element);
		try {
			element.click();
		} catch (Exception ex) {
			// If element is hiding in top bar, scroll down
			((JavascriptExecutor) this.driverContext.getDriver())
					.executeScript("window.scrollBy(0, -100)");
			element.click();
		}
	}
}
