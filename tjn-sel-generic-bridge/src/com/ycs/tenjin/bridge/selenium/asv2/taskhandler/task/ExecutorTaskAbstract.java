/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutorTaskAbstract.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Oct 27, 2017         Sameer Gupta          	Newly Added For  
 * 26-10-2018           Padmavathi               TENJINCG-783
 * 09-01-2019			Pushpa					 TJN252-64
 */

package com.ycs.tenjin.bridge.selenium.asv2.taskhandler.task;

import java.io.File;
import java.net.Inet4Address;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.exceptions.RunAbortedException;
import com.ycs.tenjin.bridge.pojo.PrintScreenObject;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.RuntimeScreenshot;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.bridge.selenium.asv2.SeleniumApplicationImpl;
import com.ycs.tenjin.bridge.selenium.asv2.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv2.generic.WebMethodsImpl;
import com.ycs.tenjin.bridge.selenium.asv2.web.CommonMethods;
import com.ycs.tenjin.bridge.selenium.asv2.web.ExecutionMethods;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.util.PatternMatch;
import com.ycs.tenjin.util.Utilities;

public abstract class ExecutorTaskAbstract implements ExecutorTask {

	private static final Logger logger = LoggerFactory.getLogger(ExecutorTaskAbstract.class);

	// Constructor Variables
	protected int runId;
	protected ExecutionStep step;
	protected int screenshotOption;
	protected int fieldTimeout;

	// Result/Output Variables
	protected IterationStatus iStatus = null;
	protected List<ValidationResult> results = null;
	protected List<RuntimeScreenshot> runtimeScreenshots = null;
	protected List<RuntimeFieldValue> runtimeValues = null;

	// Main Method Variables
	protected SeleniumApplicationImpl oApplication;
	protected int iteration;
	protected int detailRecordNo = 0;
	protected DriverContext driverContext;
	protected ExecutionMethods executionMethods;
	protected CommonMethods commonMethods;

	// global variable
	protected WebMethodsImpl webGenericMethods;
	/*Changed by Pushpa for  TJN252-64 starts*/
	protected int screenshotIndex = 1;
	/*Changed by Pushpa for  TJN252-64 ends*/
	protected String tdUid;
	
	// Parameter Variable 
	public int waitTimeForExecutionSpeedNormal = 0;
	public int waitTimeForExecutionSpeedMedium = 2;
	public int waitTimeForExecutionSpeedSlow = 5;

	public ExecutorTaskAbstract(int runId, ExecutionStep step, int screenshotOption, int fieldTimeout) {
		logger.info("ExecutorTaskAbstract");
		this.runId = runId;
		this.step = step;
		this.screenshotOption = screenshotOption;
		this.fieldTimeout = fieldTimeout; 
	}

	@Override
	final public IterationStatus executeIteration(JSONObject dataSet, SeleniumApplicationImpl oApplication,
			int iteration) {

		this.iStatus = new IterationStatus();
		this.results = new ArrayList<ValidationResult>();
		this.runtimeScreenshots = new ArrayList<RuntimeScreenshot>();
		this.runtimeValues = new ArrayList<RuntimeFieldValue>();

		this.oApplication = oApplication;
		this.iteration = iteration;
		this.driverContext = this.oApplication.getDriverContext();

		this.webGenericMethods = new WebMethodsImpl(this.driverContext);

		logger.debug("entered executeIteration()");
		logger.debug("Dataset for Iteration {}, Step {}, Record ID {} is as follows", iteration, this.step.getId(),
				this.step.getRecordId());
		logger.debug(dataSet.toString());

		try {

			this.iStatus.setValidateAllMessagesWithExpectedMessage(true);
			if(this.step.getExpectedResult()==null){
				this.step.setExpectedResult("");
			}
			this.step.setExpectedResult(this.step.getExpectedResult().trim());
			if(this.step.getExpectedResult().equals("")){
				this.iStatus.setExpectedMessageFound(true);
			}
			
			
			tdUid = dataSet.getString("TDUID");

			JSONArray pageAreas = dataSet.getJSONArray("PAGEAREAS");
			int pageAreaCount = pageAreas.length();
			logger.info("Retrieved Pageareas to traverse. This step needs to traverse through {} pageareas",
					pageAreaCount);

			// /////////////////////////////////////////////////////////////////////

			try {

				this.checkForAbort();
				
				this.initialize();

				this.webGenericMethods.switchDriverToLatestWindow();
				
				this.preExecution();
				
				// Operation Related Code 
				this.preProcessAllPageAreas();

				// All Page Processing Starts here
				this.processAllPageAreas(pageAreas);
 
				this.postProcessAllPageAreas();

				// Handle Iteration Status
				this.processIterationStatus();

				if (this.iStatus.getResult().equalsIgnoreCase("S")) {
					// Capture Screenshot
					this.capturePageScreenshot(true);
				} else {
					this.captureFailureScreenshot();
				}
				// Once all pages are completed, commit the operation
				this.postExecution();

			} catch (ExecutorException e) {
				this.iStatus.setResult("F");
				this.iStatus.setMessage(e.getMessage());
				this.captureFailureScreenshot();
				return this.iStatus;
			} catch (Exception e) {
				logger.error("ERROR processing page areas", e);
				this.iStatus.setResult("E");
				this.iStatus.setMessage("Execution aborted abnormally due to an unknown error");
				this.captureFailureScreenshot();
				return this.iStatus;
			}

			// /////////////////////////////////////////////////////////////////////

		} catch (JSONException e) {
			logger.error("ERROR in Test Data JSON --> {}", e.getMessage(), e);
			this.iStatus.setResult("E");
			this.iStatus.setMessage("Could not process Test data for this step due to an internal error");
		} finally {
			this.iStatus.setRuntimeFieldValues(this.runtimeValues);
			this.iStatus.setRuntimeScreenshots(this.runtimeScreenshots);
			this.iStatus.setValidationResults(this.results);
		}

		return this.iStatus;

	}

	final public void processAllPageAreas(JSONArray pageAreas) throws ExecutorException, JSONException, RunAbortedException {

		for (int i = 0; i < pageAreas.length(); i++) {

			this.checkForAbort();
			
			JSONObject pageArea = pageAreas.getJSONObject(i);

			Location location = Location.loadFromJson(pageArea);
			location.setWrapper(this.getWrapper(location));

			JSONArray detailRecords = null;
			try {
				detailRecords = pageArea.getJSONArray("DETAILRECORDS");
			} catch (Exception e) {
				detailRecords = new JSONArray();
			}

			this.capturePageScreenshot(false);

			if (i > 0) {
				logger.info("Navigating to page {}", location.getLocationName());
				try {
					this.commonMethods.navigateToPage(location);
				} catch (BridgeException e) {
					throw new ExecutorException(e.getMessage(), e);
				} catch (Exception e) {
					throw new ExecutorException("Fail to navigate to "
							+ location.getLocationName() + ".", e);
				}
			}

			boolean fieldsExistsToExecute = true;
			if (detailRecords.length() < 1) {
				fieldsExistsToExecute = false;
			}

			this.prePageExecution(location);

			if (!location.isMultiRecordBlock()) {
				this.preSingleRecordPageAreaExecution(location);
			} else {
				this.preMultiRecordPageAreaExecution(location);
			}
			
			if (fieldsExistsToExecute) {
				for (int k = 0; k < detailRecords.length(); k++) {
					JSONObject detailRecord = detailRecords.getJSONObject(k);
					this.processPageArea(detailRecord, location);
				}
			}

			if (!location.isMultiRecordBlock()) {
				this.postSingleRecordPageAreaExecution(location);
			} else {
				this.postMultiRecordPageAreaExecution(location);
			}
			
			this.postPageExecution(location);
			this.detailRecordNo = 0;
		}
	}


	/**
	 * Use this method only when you specifically want to set wrapper for
	 * execution. As observed in most of scenarios you don't need to set wrapper
	 * during execution. In case required override the method.
	 */
	public WebElement getWrapper(Location location) throws LearnerException {
		return null;
	}

	final public void processPageArea(JSONObject detail, Location location) throws ExecutorException {

		String TDDRID = "";
		try {
			logger.info("detail records are present for page {}", location.getLocationName());
			// JSONObject detail = detailRecords.getJSONObject(0);
			TDDRID = detail.getString("DR_ID");
			this.detailRecordNo = Integer.parseInt(TDDRID);
		} catch (Exception ex) {
			throw new ExecutorException("Invalid TDDRID - " + TDDRID, ex);
		}

		try {			
			JSONArray fields = detail.getJSONArray("FIELDS");
			WebElement targetRowForMRB = null;
			if(location.isMultiRecordBlock()){
				targetRowForMRB = this.handleMRBAction(location, fields);
			}
			 
			logger.info("Executing --> Page [{}], Detail Record [{}]", location.getLocationName(), TDDRID);
			logger.debug("Getting all fields JSONArray for detail record {}", TDDRID);

			for (int i = 0; i < fields.length(); i++) {

				logger.debug("Getting field information at index {}", i);
				JSONObject field = fields.getJSONObject(i);

				logger.debug("Loading test object");
				TestObject t = TestObject.loadFromJson(field);

				if (t == null) {
					logger.error("Could not load TestObject from JSON");
					continue;
				}
				logger.debug("Processing I/O --> Field [{}], Page [{}]", t.getLabel(), location.getLocationName());

				t.setLocation(location.getLocationName());

				this.processField(location, t,targetRowForMRB);
				 
				this.waitAsPerExecutionSpeed();
			}
		} catch (JSONException e) {
			throw new ExecutorException(e.getMessage(), e);
		}
	}

	private WebElement handleMRBAction(Location location, JSONArray fields)
			throws ExecutorException {

		boolean queryNeeded = false;
		boolean addRowNeeded = false;
		boolean deleteRowNeeded = false;
		JSONArray drQueryFields = new JSONArray();
		try {
			for (int drf = 0; drf < fields.length(); drf++) {
				JSONObject drField;

				drField = fields.getJSONObject(drf);

				if (drField.getString("ACTION").equalsIgnoreCase("QUERY")) {
					drQueryFields.put(drField);
					queryNeeded = true;
				} else if (drField.getString("ACTION").equalsIgnoreCase(
						"ADDROW")) {
					drField.remove("ACTION");
					drField.put("ACTION", "INPUT");
					addRowNeeded = true;
				} else if (drField.getString("ACTION").equalsIgnoreCase(
						"DELETEROW")) {
					drQueryFields.put(drField);
					deleteRowNeeded = true;
				}
			}
		} catch (JSONException e) {

		}

		logger.debug("{} query fields were found for this detail record",
				drQueryFields.length());

		WebElement targetRow = null;
		try {
			if (queryNeeded) {
				targetRow = this.queryRowMRB(location, drQueryFields);
				if (targetRow == null) {
					logger.error("Could not query the table for row with your search criteria");
					throw new ExecutorException(
							"Could not locate the row containing your data in the table");
				}
			}

			if (addRowNeeded) {
				targetRow = this.addRowMRB(location, targetRow);
			}

			if (deleteRowNeeded) {
				targetRow = this.deleteRowMRB(location, targetRow);
			}
			
		} catch (ExecutorException e) {
			throw new ExecutorException(e.getMessage());
		}
		return targetRow;
	}

	final public void processField(Location location, TestObject t,WebElement targetRowForMRB) {

		if (!location.isMultiRecordBlock()) { 
			this.preFieldExecution(location, t);
		}else{ 
			this.preMRBFieldExecution(location, t);
		}

		WebElement element = null;
		try {

			logger.debug("Locating element {} on page {}", t.getLabel(), location.getLocationName());
			try {
				if (!location.isMultiRecordBlock()) { 
					element = this.executionMethods.locateElement(t, location.getWrapper(),
							this.oApplication.getObjectIdentificationTimeout());
				}else{ 
					element = this.executionMethods.locateMRBElement(t, targetRowForMRB,
							this.oApplication.getObjectIdentificationTimeout());
				}
			} catch (Exception e) {
			}

			if (element == null) {
				System.err.println();
			}
			if (t.getAction().equalsIgnoreCase("MANUAL") || t.getData().equalsIgnoreCase("@@MANUAL")) {
				logger.info("performing Manual on element");
				CacheManager cm = CacheManager.getInstance();
				try {
					cm.addCache("cacheStore");
				} catch (Exception e) {
				}
				Cache cache = cm.getCache("cacheStore");

				t.setData("false");
				t.setLocation(location.getLocationName());
				cache.put(new Element(this.runId, t));
				Element ele = cache.get(this.runId);
				try {
					String execConfigPath = TenjinConfiguration.getProperty("MANUAL_TIME").trim();
					if (execConfigPath != null && !execConfigPath.equalsIgnoreCase("")) {
						int idletimetolive = Integer.parseInt(execConfigPath);
						int seconds = 60;
						ele.setTimeToLive(idletimetolive * seconds);
					}
				} catch (Exception e) {
				}

				TestObject output = (TestObject) (ele == null ? null : ele.getObjectValue());
				System.out.println("In Manual " + t.getLabel() + " " + output.getData());

			} else if (t.getAction().equalsIgnoreCase("input")) {
				logger.debug("Performing Input/Output");

				if (Utilities.trim(t.getData()).equalsIgnoreCase("@@OUTPUT")) {
					String output = "";
					if (!location.isMultiRecordBlock()) { 
						output = this.executionMethods.performOutput(element, t);
					}else{ 
						output = this.executionMethods.performMRBOutput(element, t);
					}
					RuntimeFieldValue r = this.getRuntimeFieldValueObject(t.getLabel(), output);
					r.setLocationName(location.getLocationName());
					this.runtimeValues.add(r);
				} else if (t.getData().trim().toUpperCase().startsWith("@@WAIT")) {
					logger.debug("Wait for element {}", t.getLabel());

					try {
						
						String[] waitParams = t.getData().split(",");
						int seconds = 1;
						if (waitParams.length==2) {
							seconds = Integer.parseInt(waitParams[1].trim());
						}
						this.webGenericMethods.explicitWait(seconds);
					} catch (Exception e) {
						throw new ExecutorException(
								"Invalid value for @@wait for field "
										+ t.getLabel() + ".", e);
					}
				} else {
					
					if(FieldType.BUTTON.toString().equalsIgnoreCase(t.getObjectClass())){
						this.capturePageScreenshot(false);
					}
					
					logger.debug("Performing input");
					if (!location.isMultiRecordBlock()) { 
						this.executionMethods.performInput(element, t, t.getData());
					}else{ 
						this.executionMethods.performMRBInput(element, t, t.getData());
					}
					logger.info("Performed Input -- Field {}, Page {}, Data {}", t.getLabel(),
							location.getLocationName(), t.getData());
					
					if(FieldType.BUTTON.toString().equalsIgnoreCase(t.getObjectClass())){
						this.capturePageScreenshot(false);
					}
				}
			} else if (t.getAction().equalsIgnoreCase("verify")) {

				String output = "";
				if (!location.isMultiRecordBlock()) { 
					output = this.executionMethods.performOutput(element, t);
				}else{ 
					output = this.executionMethods.performMRBOutput(element, t);
				}
				ValidationResult valRes = this.getValidationResultObject(location.getLocationName(), t.getLabel(),
						t.getData(), output, "1");
				this.results.add(valRes);
			}
		} catch (AutException e) {
			throw new ExecutorException(e.getMessage(), e);
		} catch (Exception e) {
			logger.error("UNKNOWN ERROR occurred while processing field [{}] on page [{}]", t.getLabel(),
					location.getLocationName(), e);
			throw new ExecutorException("Could not process field [" + t.getLabel() + "] on page ["
					+ location.getLocationName() + "]. Please contact Tenjin Support.");
		}

		if (!location.isMultiRecordBlock()) { 
			this.postFieldExecution(location, t);
		}else{ 
			this.postMRBFieldExecution(location, t);
		}

	}


	// Result generation Methods
	// /////////////////////////////////////////////////////////////////////
	
	final public ValidationResult getValidationResultObject(String page, String field, String expectedValue,
			String actualValue, String detailRecordNo) {
		ValidationResult r = new ValidationResult();
		actualValue = Utilities.trim(actualValue);
		r.setActualValue(actualValue);
		r.setExpectedValue(expectedValue);
		r.setDetailRecordNo(detailRecordNo);
		r.setField(field);
		r.setPage(page);
		r.setIteration(this.iteration);
		r.setResValId(0);
		r.setRunId(this.runId);
		r.setTestStepRecordId(this.step.getRecordId());

		if (Utilities.isNumeric(actualValue) && Utilities.isNumeric(expectedValue)) {
			if (Utilities.isNumericallyEqual(actualValue, expectedValue)) {
				r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
			} else {
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		} else if (actualValue.equalsIgnoreCase(expectedValue)) {
			/*************
			 * Fix by Sriram for comparision of numeric values Ends
			 */

			r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
		} else {

			if (Utilities.isNumeric(expectedValue) && Utilities.isNumeric(actualValue)) {
				if (Double.parseDouble(expectedValue) == Double.parseDouble(actualValue)) {
					r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
				} else {
					r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
				}
			} else {
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		}
		logger.info(r.toString());
		return r;
	}

	final public RuntimeFieldValue getRuntimeFieldValueObject(String field, String value) {
		RuntimeFieldValue r = new RuntimeFieldValue();

		r.setDetailRecordNo(this.detailRecordNo);
		r.setField(field);
		r.setIteration(this.iteration);
		r.setRunId(this.runId);
		r.setTdGid(this.step.getTdGid());
		r.setTdUid(tdUid);
		r.setTestStepRecordId(step.getRecordId());
		r.setValue(value);

		logger.info(r.toString());
		return r;
	}

	final public RuntimeScreenshot getRuntimeScreenshotObject(String screenshotType) {

		RuntimeScreenshot ras = new RuntimeScreenshot();

		ras.setIteration(this.iteration);
		ras.setRunId(this.runId);
		ras.setSequence(this.screenshotIndex++);
		ras.setTdUid(tdUid);
		ras.setTdGid(this.step.getTdGid());
		ras.setTestStepRecordId(this.step.getRecordId());
		ras.setTimestamp(new Timestamp(new Date().getTime()));
		ras.setType(screenshotType);
		try {
			ras.setScreenshot(this.captureScreenshotAsFile(this.oApplication.clientIp));
		} catch (Exception e) {
			logger.error("ERROR capturing {} screenshot", screenshotType, e);
		}

		return ras;
	}
 
	private File captureScreenshotAsFile(String ipAddress) {

		File screenshot = null;
		try {
			String printScreenFlag = TenjinConfiguration
					.getProperty("ENABLE_PRINTSCREEN");
			if (printScreenFlag != null
					&& printScreenFlag.equalsIgnoreCase("Y")) {

				if (Utilities.isLocalHost(ipAddress)) {
					ipAddress = Inet4Address.getLocalHost().getHostAddress();
				}

				PrintScreenObject prntScreenObject = (PrintScreenObject) CacheUtils
						.getObjectFromRunCache("screenshot" + ipAddress);
				prntScreenObject.setScreenshotInProgress(true);
				prntScreenObject.setScreenshot(null);
				CacheUtils.putObjectInRunCache("screenshot" + ipAddress,
						prntScreenObject);

				boolean isScreenshotReceived = false;

				while (!isScreenshotReceived) {
					prntScreenObject = (PrintScreenObject) CacheUtils
							.getObjectFromRunCache("screenshot" + ipAddress);
					if (!prntScreenObject.isScreenshotInProgress()) {
						isScreenshotReceived = true;
						prntScreenObject.setScreenshotInProgress(false);
						screenshot = prntScreenObject.getScreenshot();
						CacheUtils.putObjectInRunCache(
								"screenshot" + ipAddress, prntScreenObject);
					}
				}
			} else {
				screenshot = ((TakesScreenshot) this.driverContext.getDriver())
						.getScreenshotAs(OutputType.FILE);
			}
		} catch (Exception e) {
			logger.debug("Could not capture screenshot of application", e);
			screenshot = ((TakesScreenshot) this.driverContext.getDriver())
					.getScreenshotAs(OutputType.FILE);
		}
		return screenshot;
	}

	// General Methods
	// /////////////////////////////////////////////////////////////////////
	
	private void checkForAbort() throws RunAbortedException {
		logger.debug("Checking if thread for run {} is alive", this.runId);
		if(CacheUtils.isRunAborted(this.runId)) {
			logger.debug("Thread for Run {} is interrupted.", this.runId);
			throw new RunAbortedException();
		}
	}

	final public void capturePageScreenshot(boolean lastPage) throws ExecutorException {

		boolean takeScreenshot = false;

		if (this.screenshotOption == 2 && lastPage) {
			takeScreenshot = true;
		} else if (this.screenshotOption == 3) {
			takeScreenshot = true;
		}

		if (takeScreenshot) {

			RuntimeScreenshot ras = this.getRuntimeScreenshotObject("End Of Page");
			this.runtimeScreenshots.add(ras);
		}
	}

	final public void captureFailureScreenshot() throws ExecutorException {

		if (this.screenshotOption > 0) {
			RuntimeScreenshot ras = this.getRuntimeScreenshotObject("Failure/Error");
			this.runtimeScreenshots.add(ras);
		}
	}

	final public void waitAsPerExecutionSpeed() throws ExecutorException {
		try {

			switch (fieldTimeout) {
			case 0:
				if (this.waitTimeForExecutionSpeedNormal > 0)
					Thread.sleep(this.waitTimeForExecutionSpeedNormal * 1000);
				break;
			case 1:
				if (this.waitTimeForExecutionSpeedMedium > 0)
					Thread.sleep(this.waitTimeForExecutionSpeedMedium * 1000);
				break;
			case 3:
				if (this.waitTimeForExecutionSpeedSlow > 0)
					Thread.sleep(this.waitTimeForExecutionSpeedSlow * 1000);
				break;
			default:
				break;
			}
		} catch (Exception ignore) {
		}
	}
	
	final public void validateWithExpectedMessage(String message)
			throws ExecutorException {
		try {

			if (!this.step.getExpectedResult().equals("")
					&& !message.equals("")) {

				String expectedMessage = this.step.getExpectedResult();
				if (expectedMessage.contains("%")) {
					PatternMatch objPatternMatch = new PatternMatch();
					if (objPatternMatch.matchString(message, expectedMessage,
							true)) {
						this.iStatus.setExpectedMessageFound(true);
					}
				} else {
					if (expectedMessage.equalsIgnoreCase(message.trim())) {
						this.iStatus.setExpectedMessageFound(true);
					}
				}
			}

		} catch (Exception ignore) {
		}
	}
	
	// Abstract Methods
	// /////////////////////////////////////////////////////////////////////

	/**
	 * Initialize the classes here. There is difference here as for execution
	 * application value doesn't come while constructor but during method call.
	 * Following classes needs to be initialized here:-<br>
	 * this.commonMethods = new CommonMethodsImpl(driverContext);<br>
	 * this.executionMethods = new IosExecutionMethodsImpl(this.driverContext);
	 * <br>
	 */
	public abstract void initialize() throws ExecutorException;

	/**
	 * Add Required Logic before starting with execution.
	 */
	public abstract void preExecution() throws ExecutorException;

	/**
	 * Add Required Logic after completing execution.
	 */
	public abstract void postExecution() throws ExecutorException;

	/**
	 * Add Application Operation specific Logic here.
	 */
	public abstract void preProcessAllPageAreas() throws ExecutorException;

	/**
	 * Add Application Operation specific Logic here.
	 */
	public abstract void postProcessAllPageAreas() throws ExecutorException;

	/**
	 * Add Required Logic before starting the execution for the page.
	 */
	public abstract void prePageExecution(Location location) throws ExecutorException;

	/**
	 * Add Required Logic after the execution for the page is completed.
	 */
	public abstract void postPageExecution(Location location) throws ExecutorException;

	/**
	 * Add Required Logic before starting the execution for the Single Record
	 * page.<br>
	 * <br>
	 * Only logic specific to Single Record Page should be added here.
	 */
	public abstract void preSingleRecordPageAreaExecution(Location location) throws ExecutorException;

	/**
	 * Add Required Logic after the execution for the Single Record page is
	 * completed.<br>
	 * <br>
	 * Only logic specific to Single Record Page should be added here.
	 */
	public abstract void postSingleRecordPageAreaExecution(Location location) throws ExecutorException;

	/**
	 * Add Required Logic before starting the execution for the Multi Record
	 * page.<br>
	 * <br>
	 * Only logic specific to Multi Record Page should be added here.
	 */
	public abstract void preMultiRecordPageAreaExecution(Location location) throws ExecutorException;

	/**
	 * Add Required Logic after the execution for the Multi Record page is
	 * completed.<br>
	 * <br>
	 * Only logic specific to Multi Record Page should be added here.
	 */
	public abstract void postMultiRecordPageAreaExecution(Location location) throws ExecutorException;

	/**
	 * To find the row on which execution will happen.
	 */
	public abstract WebElement queryRowMRB(Location location,
			JSONArray drQueryFields) throws ExecutorException;
	
	/**
	 * To add the row on which execution will happen.
	 */
	public abstract WebElement addRowMRB(Location location,
			WebElement targetRow) throws ExecutorException;
	
	/**
	 * To delete the row in MRB.
	 */
	public abstract WebElement deleteRowMRB(Location location,
			WebElement targetRow) throws ExecutorException;
	
	/**
	 * Add Required Logic before starting the execution for the field.
	 */
	public abstract void preFieldExecution(Location location, TestObject t) throws ExecutorException;

	/**
	 * Add Required Logic after the execution for the field is completed.
	 */
	public abstract void postFieldExecution(Location location, TestObject t) throws ExecutorException;

	/**
	 * Add Required Logic before starting the execution for the field.
	 */
	public abstract void preMRBFieldExecution(Location location, TestObject t) throws ExecutorException;

	/**
	 * Add Required Logic after the execution for the field is completed.
	 */
	public abstract void postMRBFieldExecution(Location location, TestObject t) throws ExecutorException;
	
	/**
	 * Build the Iteration Status here. Following things are mandatory to be
	 * setup here:- this.iStatus.setMessage(value);
	 * this.iStatus.setResult("S/F"); this.runtimeValues.add(r);
	 */
	public abstract void processIterationStatus() throws ExecutorException;
}
