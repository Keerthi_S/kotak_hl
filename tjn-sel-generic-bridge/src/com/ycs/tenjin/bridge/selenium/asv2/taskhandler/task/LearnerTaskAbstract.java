/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LearnerTaskAbstract.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Oct 27, 2017         Sameer Gupta          	Newly Added For  
 */

package com.ycs.tenjin.bridge.selenium.asv2.taskhandler.task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.enums.PageType;
import com.ycs.tenjin.bridge.exceptions.AssistedLearningException;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Metadata;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.selenium.asv2.SeleniumApplicationImpl;
import com.ycs.tenjin.bridge.selenium.asv2.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv2.generic.WebMethodsImpl;
import com.ycs.tenjin.bridge.selenium.asv2.web.CommonMethods;
import com.ycs.tenjin.bridge.selenium.asv2.web.ExecutionMethods;
import com.ycs.tenjin.bridge.selenium.asv2.web.LearningMethods;
import com.ycs.tenjin.bridge.utils.AssistedLearningUtils;
import com.ycs.tenjin.util.Utilities;

public abstract class LearnerTaskAbstract implements LearnerTask {

	protected static final Logger logger = LoggerFactory.getLogger(LearnerTaskAbstract.class);

	// Constructor Variables
	protected SeleniumApplicationImpl oApplication;
	protected Module function;
	protected DriverContext driverContext;
	protected CommonMethods commonMethods;
	protected LearningMethods learningMethods;
	protected ExecutionMethods executionMethods;
	protected WebMethodsImpl webGenericMethods;

	// Result/Output Variables
	protected Multimap<String, Location> pageAreas = ArrayListMultimap.create();
	protected HashMap<String, ArrayList<TestObject>> testObjectByLocation = new HashMap<String, ArrayList<TestObject>>();
	
	// global variable
	protected int pageSequence = 0;
	protected int fieldSequence = 0;
	protected int tabOrder = 0;
	protected Multimap<String, TestObject> learningData = null;
	protected boolean learnEOL;

	public LearnerTaskAbstract(SeleniumApplicationImpl oApplication, Module function) {

		logger.info("LearnerTaskAbstract");
		this.oApplication = oApplication;
		this.function = function;
		this.driverContext = oApplication.getDriverContext(); 
		this.webGenericMethods = new WebMethodsImpl(this.driverContext);
	}

	@Override
	final public Metadata learn() throws LearnerException {

		logger.info("Loading assisted learning data");
		// Get the Assisted Learning data
		try {
			this.learningData = AssistedLearningUtils.getAssistedLearningData(this.function.getCode(),
					this.oApplication.getAppId());

		} catch (AssistedLearningException e) {
			logger.error("ERROR loading assisted learning data for function {}", this.function.getCode(), e);
		}

		this.webGenericMethods.switchDriverToLatestWindow();

		this.preLearning();

		Metadata metadata = new Metadata();

		logger.info("Beginning learning of function {}", this.function.getCode());

		// At this point, we will be in the landing page of a given function.
		// Update the location as such.
		Location location = new Location();
		location.setLandingPage(true);
		location.setLocationType(PageType.MAIN.toString());
		location.setParent("NA");
		location.setSequence(this.pageSequence++);
		location.setWayIn("Navigate to Function");
		location.setWayOut("Exit");
		location.setWrapper(this.getWrapper(location));
		location.setLabel(this.getScreenTitle(location));

		try {
			this.addLocationToMap(location);
		} catch (Exception e) {
			throw new LearnerException(e.getMessage(), e);
		}

		this.navigateAndLearn(location);

		this.performAssistedLearningEOL();

		this.postLearning();

		metadata.setPageAreas(this.pageAreas);
		
		ArrayList<TreeMap<String, TestObject>> testObjectMap = new ArrayList<TreeMap<String, TestObject>>();
		for(String locationName:testObjectByLocation.keySet()){
			ArrayList<TestObject> testObjects = testObjectByLocation.get(locationName);
			TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();
			for (TestObject t : testObjects) {
				pMap.put(t.getUniqueId(), t);
			}
			testObjectMap.add(pMap);
		} 
		metadata.setTestObjectMap(testObjectMap);
		
		return metadata;
	}

	final public void navigateAndLearn(Location currentLocation) {

		this.prePageLearning(currentLocation);

		this.performAssistedLearning(currentLocation, "pre_");

		this.learnScreenElements(currentLocation);

		this.performAssistedLearning(currentLocation, "");

		try {

			ArrayList<Location> listLocationsToTraverse = new ArrayList<Location>();

			if (!currentLocation.getMultiRecordBlock()) {
				listLocationsToTraverse = this.getAllPageAreas(currentLocation);
			}else{
				listLocationsToTraverse = this.getAllPageAreasMRBs(currentLocation);
			}
			
			if (listLocationsToTraverse != null) {

				for (Location objLocation : listLocationsToTraverse) {

					// ADDED BY SHIVAM TO SKIP ALERTS LINK-STARTS
					// this.commonMethods.navigateToPage(objLocation);
					try {
						this.commonMethods.navigateToPage(objLocation);
					} catch (BridgeException e) {
						if(e.getMessage().trim().toLowerCase().endsWith("continue")){
							continue;
						}else{
							throw new LearnerException(e.getMessage(), e);
						}
					}
					// ADDED BY SHIVAM TO SKIP ALERTS LINK-STARTS

					this.webGenericMethods.switchDriverToLatestWindow();

					WebElement wrapper = this.getWrapper(objLocation);
					if (wrapper == null) {
						new LearnerException("Wrapper not found for location "
								+ currentLocation.getLocationName());
					}
					objLocation.setWrapper(wrapper);

					String screenTitle = this.getScreenTitle(objLocation);
					//Need to remove special chars as not accepted by excel
					screenTitle = screenTitle.replaceAll(":", "").replaceAll("'", "").replaceAll("/", "");

					String wayOut = this.getWayout(objLocation);

					if (screenTitle == null || screenTitle.equals("")) {
						throw new LearnerException("Screen Title not found.");
					} else if (objLocation.getLocationType() == null
							|| objLocation.getLocationType().equals("")) {
						throw new LearnerException(
								"Location Type not found for location "
										+ screenTitle);
					} else if (objLocation.getWayIn() == null
							|| objLocation.getWayIn().equals("")) {
						throw new LearnerException(
								"Way in not found for location " + screenTitle);
					} else if (wayOut == null) {
						throw new LearnerException(
								"Way out not found for location " + screenTitle);
					}

					Location location = new Location();
					location.setLabel(screenTitle);
					location.setLandingPage(false);
					location.setLocationType(objLocation.getLocationType());
					location.setParent(currentLocation.getLocationName());
					location.setSequence(this.pageSequence++);
					location.setWayIn(objLocation.getWayIn());
					location.setWayOut(wayOut);
					location.setWrapper(objLocation.getWrapper());
					// added by shivam- starts
					location.setScreenTitle(screenTitle);
					// added by shivam - ends
					try {
						this.addLocationToMap(location);
					} catch (Exception e) {
						throw new LearnerException(e.getMessage(), e);
					}

					this.navigateAndLearn(location);

				}

			} 
		} catch (LearnerException e) {
			throw new LearnerException(e.getMessage(), e);
		}

		this.postPageLearning(currentLocation);

		if (!currentLocation.getWayOut().equals("")) {
			this.webGenericMethods.switchDriverToLatestWindow();
		}

	}

	final public void learnScreenElements(Location currentLocation) {
		// added by shivam for frames

		logger.info("Learning screen {}", currentLocation);

		this.tabOrder = 0;

//		TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();

		ArrayList<TestObject> testObjects = this.getAllTestObjectsForPage(currentLocation);

//		for (TestObject t : testObjects) {
//			pMap.put(t.getUniqueId(), t);
//		}

		this.testObjectByLocation.put(currentLocation.getLocationName(), testObjects);
//		this.testObjectMap.add(pMap);

	}

	private ArrayList<TestObject> getAllTestObjectsForPage(Location currentLocation) {
		// ArrayList<TestObject> testObjects = null;
		// added by shivam - starts
		ArrayList<TestObject> testObjects = new ArrayList<TestObject>();
		// added by shivam - ends
		try {
			if (!currentLocation.getMultiRecordBlock()) {
				List<WebElement> elements = currentLocation.getWrapper()
						.findElements(By.xpath(this.oApplication.getCompositeXpath()));
				testObjects = this.getAllVisibleElement(currentLocation, elements);
			} else {
				testObjects = this.getAllVisibleElementForMRB(currentLocation);
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Could not get all visible elements on {}", currentLocation);
		}
		return testObjects;
	}

	final public void addLocationToMap(Location location) throws LearnerException {
		try {

			// Set Location Screen Title
			location.setScreenTitle(location.getLabel());

			// Set Location Name
			String locationName = location.getLabel();

			int count = 0;
			for (String key : this.pageAreas.keys()) {
				// changed by shivam
				Location loc = (Location) (this.pageAreas.get(key).toArray()[0]);

				if (loc.getLabel().equalsIgnoreCase(location.getLabel())) {
					count = count + 1;
				}
			}

			logger.debug("Scanning map for existing pages with name " + locationName);
			if (count > 0) {
				logger.debug("There is already " + count + " page(s) with name " + locationName);
				int nc = count + 1;

				String newLocName = "00" + Integer.toString(nc);
				newLocName = newLocName.substring(newLocName.length() - 2);
				locationName = locationName + " " + newLocName;
				logger.debug("New Location Name will be " + newLocName);

			} else {
				logger.debug("There are no existing pages with name " + locationName);
			}
			location.setLocationName(locationName);

			if (location.getLocationType().equals(PageType.TABLE.toString())) {
				location.setMultiRecordBlock(true);
			}

			// Validate Location
			this.validateLocation(location);

			this.pageAreas.put(location.getLocationName(), location);
			logger.info("New Location --> {}", location.toString());
		} catch (Exception e) {
			throw new LearnerException("Could not resolve location name for " + location.getLocationName(), e);
		}
	}

	final public TestObject getTestObject(Location currentLocation, FieldType type, String label, String identifiedBy,
			String uniqueId, boolean mandatory, String dropdownValues, boolean hasLov, boolean hasAutoLov,
			String fieldGroup) {

		TestObject t = new TestObject();

		t.setTabOrder(this.tabOrder++);
		t.setSequence(this.fieldSequence++);
		// No need to set instance here as setting up of instance will be taken
		// care before persist
		// t.setInstance(instance);

		t.setLabel(label);
		// No need to set name here as setting up of name will be taken care
		// before persist
		// t.setName(name);

		t.setObjectClass(type.toString());
		t.setIdentifiedBy(identifiedBy);
		t.setUniqueId(uniqueId);

		t.setDefaultOptions(dropdownValues);
		if (mandatory) {
			t.setMandatory("YES");
		} else {
			t.setMandatory("NO");
		}

		t.setLocation(currentLocation.getLocationName());
		if (currentLocation.isMultiRecordBlock()) {
			t.setIsMultiRecord("Y");
		} else {
			t.setIsMultiRecord("N");
		}

		if (Utilities.trim(fieldGroup).length() > 0) {
			t.setGroup(fieldGroup);
		} else {
			t.setGroup("");
		}

		if (hasLov) {
			t.setLovAvailable("Y");
		} else {
			t.setLovAvailable("N");
		}

		if (hasAutoLov) {
			t.setAutoLovAvailable("Y");
		} else {
			t.setAutoLovAvailable("N");
		}

		// Set default values. Depending on application these values may be
		// required. So if required setup in adapter.
		t.setViewmode("Y");
		t.setIsFooterElement("N");

		return t;
	}

	protected void performAssistedLearningEOL() throws LearnerException {

		List<String> eolPages = new LinkedList<String>();
		Collection<TestObject> fields = null;
		// find all enteris in assistedLearningData which starts with EOL
		// add unique pages of those enteries in eolPages
		for (String location : this.pageAreas.keySet()) {
			try {
				fields = this.learningData.get("eol_" + location);
			} catch (Exception e) {
			}
			if (fields != null && !fields.isEmpty()){
				eolPages.add(location);
				this.learnEOL = true;
			}
		}

		for (String page : eolPages) {
			if (this.pageAreas.containsKey(page)) {
				Location eolLocation = (Location) this.pageAreas.get(page).toArray()[0];
				// added by shivam to handle tab and links in EOL assisted
				// learning - starts
//				if (eolLocation.getLocationType().equalsIgnoreCase(PageType.LINK.toString())
//						|| eolLocation.getLocationType().equalsIgnoreCase(PageType.TAB.toString())) {
					try {
						this.commonMethods.navigateToPage(eolLocation);
					} catch (BridgeException e) {
						if(e.getMessage().trim().toLowerCase().endsWith("continue")){
							continue;
						}else{
							throw new LearnerException(e.getMessage(), e);
						}
					}
//				}
				// added by shivam to handle tab and links in EOL assisted
				// learning - ends
				this.performAssistedLearning(eolLocation, "eol_");
			} else {
				logger.error("Could not perform assisted learning for page - " + page);
			}
		}

		// navigate and learn
	}

	protected void performAssistedLearning(Location currentLocation, String stage) throws LearnerException {

		logger.debug("Entering Assisted Learning Input for screen --> " + currentLocation);

		if (this.learningData != null) {

			Collection<TestObject> fields = this.learningData.get(stage + currentLocation.getLocationName());
 
			boolean firstField = true;
			ArrayList<TestObject> testObjects = null;
			
			logger.debug("Found {} fields to input on this screen", fields.size());

			for (TestObject field : fields) {

				try {

					if (field.getIdentifiedBy() == null || field.getIdentifiedBy().equals("")
							|| field.getIdentifiedBy().equalsIgnoreCase("id")|| field.getIdentifiedBy().equalsIgnoreCase("button")) {

						if (firstField && stage.equalsIgnoreCase("pre_")) {
							if(field.getIdentifiedBy() == null || field.getIdentifiedBy().trim().equals("")){
								testObjects = this.getAllTestObjectsForPage(currentLocation);
								firstField = false;
							}
						}
						
						TestObject targetField = this.getTargetField(
								currentLocation, field,testObjects);

						// Operate on the field
						if (targetField != null) {
							logger.debug("Found target field");

							WebElement targetele = this.executionMethods.locateElement(targetField,
									currentLocation.getWrapper(), 10);

							try {
								this.executionMethods.performInput(targetele, targetField, field.getData());
							} catch (AutException e) {
								throw new LearnerException(e.getMessage());
							}
						} else {
							throw new LearnerException("Field for assisted learning not found.");
						}

					} else if (field.getIdentifiedBy().toLowerCase().startsWith("@@wait")) {

						logger.debug("Wait for element {}", field.getLabel());

						try {
							int seconds = 1;
							if (field.getData() != null && !field.getData().equals("")) {
								seconds = Integer.parseInt(field.getData());
							}
							this.webGenericMethods.explicitWait(seconds);
						} catch (Exception e) {
							throw new LearnerException(e.getMessage());
						}
					}else if (field.getIdentifiedBy().equalsIgnoreCase("@@LEARN")) {
                        
//                        TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();
                        ArrayList<TestObject> testobjectsListOld = this.testObjectByLocation
                                .get(currentLocation.getLocationName());
                        ArrayList<TestObject> testobjectsListNew = this
                                .getAllTestObjectsForPage(currentLocation);
 
                        HashMap<String, TestObject> mapTestObjectsOld = new HashMap<String, TestObject>();
                        if(testobjectsListOld!=null){
                            for (TestObject testobj1 : testobjectsListOld) {
                                mapTestObjectsOld.put(testobj1.getUniqueId(),
                                        testobj1);
                            }
                        }
 
                        for (int j = 0; j < testobjectsListNew.size(); j++) {
                            TestObject testobj2 = testobjectsListNew.get(j);
                            if (!mapTestObjectsOld.containsKey(testobj2
                                    .getUniqueId())) {
                                testobjectsListOld.add(testobj2);
                            }
                        } 
                        
                        this.testObjectByLocation.put(
                                currentLocation.getLocationName(), testobjectsListOld); 
                    }else{
    					this.performAssistedLearningExtended(currentLocation, stage, field);
                    }


				} catch (Exception ex) {
					logger.error("Could not perform assisted learning for "
							+ field.getLabel());
					throw new LearnerException(
							"Could not perform assisted learning for "
									+ field.getLabel() + " on page "
									+ currentLocation.getLocationName());
				}

			}

		}
	}

	/**
	 * This method can be used where you want to find the element in current
	 * list of objects.
	 */
	final protected TestObject getTargetField(Location currentLocation,
			TestObject field,ArrayList<TestObject> testObjects) {

		TestObject targetField = null;

		if (field.getIdentifiedBy() == null || field.getIdentifiedBy().equals("")) {
			
			if(testObjects==null){
				if (this.testObjectByLocation.containsKey(currentLocation
						.getLocationName())) {
					testObjects = this.testObjectByLocation.get(currentLocation
							.getLocationName());
				}
			}

			for (TestObject t : testObjects) {
				if (Utilities.trim(t.getLabel()).equalsIgnoreCase(field.getLabel())) {
					targetField = t;
					break;
				}
			}
		} else if (field.getIdentifiedBy().equals("id")) {
			targetField = new TestObject();
			targetField.setObjectClass("id");
			targetField.setIdentifiedBy("id");
			targetField.setUniqueId(field.getLabel());
		}else if (field.getIdentifiedBy().equalsIgnoreCase("button")) { 
			targetField = new TestObject();
			targetField.setObjectClass("button");
			targetField.setIdentifiedBy("button");
			targetField.setLabel(field.getLabel());
			targetField.setData(field.getData());
		}

		return targetField;
	}

	/**
	 * Add Required logic to get all visible elements that are required to be
	 * added for current page. <br>
	 * <br>
	 * By default list of elements are coming from composite path, if you don't
	 * want to use that composite path and add new logic to get list of
	 * elements, override 'elements' parameter.
	 */
	protected ArrayList<TestObject> getAllVisibleElement(Location currentLocation, List<WebElement> elements)
			throws LearnerException {

		ArrayList<TestObject> testObjects = new ArrayList<TestObject>();

		this.preGetAllVisibleElements(currentLocation, elements, testObjects);

		if (elements != null) {

			int counter = 0;

			//To get list of test objects if already learnt.
			ArrayList<TestObject> testobjectsListOld = this.testObjectByLocation
					.get(currentLocation.getLocationName());
			HashMap<String, TestObject> mapTestObjectsOld = new HashMap<String, TestObject>();
			if (testobjectsListOld != null) {
				for (TestObject testobj1 : testobjectsListOld) {
					mapTestObjectsOld.put(testobj1.getUniqueId(), testobj1);
				}
			}
			
			for (int i = 0; i < elements.size(); i++) {
				counter++;
				logger.debug("Processing element {}", counter);
				WebElement element = elements.get(i);

				logger.warn("Element {} --------> " + element.getAttribute("title"), counter);
				if (!validateElement(element)) {
					logger.warn("Element {} is hidden. Will be skipped", counter);
					continue;
				}

				Map<String, String> attributesMap = this.getHTMLAttributesOfElement(element);
				logger.debug("All element attributes --> {}", attributesMap.get("all"));

				FieldType type = this.learningMethods.getTypeOfElement(attributesMap);
				logger.debug("Element Type --> {}", type);
				// addded by shivam to handle NullPointerException - starts
				if (type == null) {
					logger.warn("Could not get type element {}. This element will be skipped", counter);
					continue;
				}
				// added by shivam to handle NullPointerException - ends
				Map<String, String> uidMap = this.learningMethods.getUniqueIdOfElement(attributesMap, element);
				logger.debug("UID Map --> {}", uidMap);

				if (uidMap == null || Utilities.trim(uidMap.get("uid")).equalsIgnoreCase("")) {
					logger.warn("Could not get Unique ID for element {}. This element will be skipped", counter);
					continue;
				}

				String identifiedBy = uidMap.get("uidType");
				String uid = uidMap.get("uid");

				//Ignore elements which are already learnt
				if (mapTestObjectsOld.containsKey(uid)) {
					continue;
				}

				String label = this.learningMethods.getLabelOfElement(type, element,attributesMap);
				logger.debug("Label --> {}", label);
				if (Utilities.trim(label).equalsIgnoreCase("")) {
					logger.warn("Label empty. So using UID as label");
					label = uid;
					logger.debug("New Label --> {}", label);
				}

				String dropValues = this.learningMethods.getDropdownValuesOfElement(type, element, attributesMap);
				logger.debug("Default Options --> {}", dropValues);

				boolean mandatory = this.learningMethods.isMandatory(attributesMap, element);
				logger.debug("Mandatory --> {}", mandatory);
				boolean hasLov = this.learningMethods.elementHasLOV(attributesMap, element);
				logger.debug("Has LOV --> {}", hasLov);
				boolean hasAutoLov = this.learningMethods.elementHasAutoLOV(attributesMap, element);
				logger.debug("Has AutoLOV --> {}", hasAutoLov);
				String fieldGroup = this.learningMethods.getFieldGroup(attributesMap, element);
				logger.debug("Field Group --> {}", fieldGroup);

				TestObject t = this.getTestObject(currentLocation, type, label, identifiedBy, uid, mandatory,
						dropValues, hasLov, hasAutoLov, fieldGroup);
				testObjects.add(t);
			}
		}

		this.postGetAllVisibleElements(currentLocation, elements, testObjects);

		return testObjects;
	}

	protected Map<String, String> getHTMLAttributesOfElement(WebElement element) {

		try {
			@SuppressWarnings("unchecked")
			ArrayList<String> parentAttributes = (ArrayList<String>) ((JavascriptExecutor) this.driverContext
					.getDriver()).executeScript(
							"var s = []; var attrs = arguments[0].attributes; for (var l = 0; l < attrs.length; ++l) { var a = attrs[l]; s.push(a.name + ':' + a.value); } ; return s;",
							element);

			Map<String, String> attributesMap = new HashMap<String, String>();
			String allAttrs = "";
			String aValue = null;
			for (String s : parentAttributes) {
				String[] split = s.split(":");
				if (split.length >= 2) {
					if (split.length == 2) {
						attributesMap.put(split[0], split[1]);
					} else {
						aValue = "";
						for (int i = 1; i < split.length; i++) {
							if (i < split.length - 1) {
								aValue = aValue + split[i] + ":";
							} else {
								aValue = aValue + split[i];
							}
						}

						attributesMap.put(split[0], aValue);
					}
				} else {
					attributesMap.put(split[0], null);
				}

				allAttrs = allAttrs + s + ",";
			}

			attributesMap.put("all", allAttrs);
			attributesMap.put("tag", element.getTagName());

			return attributesMap;
		} catch (Exception e) {
			logger.error("ERROR while getting HTML attributes of element", e);
			return null;
		}
	}

	/**
	 * This method is called before processing, so if you need to validate any
	 * element do it here.<br>
	 * 
	 */
	public boolean validateElement(WebElement element) throws LearnerException{
		boolean validElement = true;
		if (!element.isDisplayed()) { 
			validElement = false;
		}
		return validElement;
	}
	
	// Abstract Methods - General
	// /////////////////////////////////////////////////////////////////////

	/**
	 * Get wrapper for page to learn. This is the section containing elements to
	 * learn excluding sub-locations(eg tabs).
	 */
	public abstract WebElement getWrapper(Location objLocation) throws LearnerException;

	/**
	 * Get title of current page.
	 */
	public abstract String getScreenTitle(Location objLocation) throws LearnerException;

	/**
	 * Get list of all pages in current location. <br>
	 * <br>
	 * Set location type and way in here for all locations.
	 */
	public abstract ArrayList<Location> getAllPageAreas(Location currentLocation) throws LearnerException;

	/**
	 * This method is called just before adding the location to map, so if you
	 * need to validate or change any property of location do it here.<br>
	 * <br>
	 * Mostly name and labels are validated here.
	 */
	public abstract void validateLocation(Location location) throws LearnerException;

	/**
	 * This is to set value of way out from the page.
	 */
	public abstract String getWayout(Location objLocation) throws LearnerException;

	/**
	 * Get list of test objects for MRB.
	 */
	public abstract ArrayList<TestObject> getAllVisibleElementForMRB(Location currentLocation) throws LearnerException;

	// Abstract Methods - Pre And Post
	// /////////////////////////////////////////////////////////////////////

	/**
	 * Add Required Logic before starting with learning.
	 */
	public abstract void preLearning() throws LearnerException;

	/**
	 * Add Required Logic after completing learning.
	 */
	public abstract void postLearning() throws LearnerException;

	/**
	 * Add Required Logic before learning the page.
	 */
	public abstract void prePageLearning(Location currentLocation) throws LearnerException;

	/**
	 * Add Required Logic after learning the page.
	 */
	public abstract void postPageLearning(Location currentLocation) throws LearnerException;

	/**
	 * This method can be used to enhance the functionality of assisted
	 * learning.
	 */
	public abstract Map<String, Object> performAssistedLearningExtended(Location currentLocation, String stage,
			TestObject field);

	/**
	 * Add Required Logic before starting with GetAllVisibleElements.
	 */
	public abstract void preGetAllVisibleElements(Location currentLocation, List<WebElement> elements,
			ArrayList<TestObject> testObjects) throws LearnerException;

	/**
	 * Add Required Logic after completing GetAllVisibleElements.
	 */
	public abstract void postGetAllVisibleElements(Location currentLocation, List<WebElement> elements,
			ArrayList<TestObject> testObjects) throws LearnerException;

	/**
	 * Add Required Logic for get All MRB page areas.
	 */
	public abstract ArrayList<Location> getAllPageAreasMRBs(Location currentLocation)
                            throws LearnerException ;

}
