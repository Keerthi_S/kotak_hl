/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutionMethodsAbstract.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Oct 27, 2017         Sameer Gupta          	Newly Added For  
 */
 
 package com.ycs.tenjin.bridge.selenium.asv2.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.selenium.asv2.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv2.generic.WebMethodsImpl;
import com.ycs.tenjin.bridge.selenium.asv2.taskhandler.task.ExecutorTaskAbstract;

public abstract class ExecutionMethodsAbstract implements ExecutionMethods {

	public static final Logger logger = LoggerFactory
			.getLogger(ExecutionMethodsAbstract.class);
	public DriverContext driverContext;
	public ExecutorTaskAbstract executorTaskAbstract;
	protected WebMethodsImpl webGenericMethods;
	 
	public void setExecutorTaskAbstract(ExecutorTaskAbstract executorTaskAbstract) {
		this.executorTaskAbstract = executorTaskAbstract;
	}

	public ExecutionMethodsAbstract(DriverContext driverContext,WebMethodsImpl webGenericMethods) {
		this.driverContext = driverContext;
		this.webGenericMethods = webGenericMethods;
		this.intializeMessages();
	}

	private static HashMap<String, HashMap<String, ArrayList<String>>> mapOfMessagesStatic = new HashMap<String, HashMap<String, ArrayList<String>>>();

	public void intializeMessages() {
		try {

			String packageName = getClass().getPackage().toString();

			if (!mapOfMessagesStatic.containsKey(packageName)) {
				Scanner scanner = new Scanner(getClass().getResourceAsStream(
						"messages.txt"));

				HashMap<String, ArrayList<String>> mapOfMessages = new HashMap<String, ArrayList<String>>();

				while (scanner.hasNextLine()) {
					String line = scanner.nextLine().trim();

					String data[] = line.split(",");
					if (data.length == 1) {
						this.webGenericMethods.addMessageToList(data[0], "",
								mapOfMessages);
					} else if (data.length == 2) {
						this.webGenericMethods.addMessageToList(data[0],
								data[1], mapOfMessages);
					} else {
					}
				}
				scanner.close();

				mapOfMessagesStatic.put(packageName, mapOfMessages);
				this.webGenericMethods.setMapOfMessages(mapOfMessages);
			} else {
				this.webGenericMethods.setMapOfMessages(mapOfMessagesStatic
						.get(packageName));
			}

		} catch (Exception ex) {
		}
	}
	
	// Locate Element
	// //////////////////////////////////////////////////////////////////////////////
	
	@Override
	public WebElement locateElement(TestObject t, WebElement wrapper,
			int objectIdentificationTimeout) throws AutException {
		
		WebElement element = null;

		if (t.getObjectClass().equalsIgnoreCase("button")) {
			if (t.getData() != null
					&& t.getData().trim().toLowerCase().startsWith("@@")) {
				return processButtonHandlers(t, wrapper);
			}
		}
		
		WebElement extendElement = this.locateElementExtend(t);
		if (extendElement != null) {
			element = extendElement;
		} else {

			List<WebElement> elementList = null;
			int secCounter = 0;

			String uniqueId = t.getUniqueId();
			String idType = t.getIdentifiedBy();

			By by;
			if ("xpath".equalsIgnoreCase(idType)) {
				by = By.xpath(uniqueId);
			} else if ("tagName".equalsIgnoreCase(idType)) {
				by = By.tagName(uniqueId);
			} else if ("button".equalsIgnoreCase(idType)) {
				by = By.xpath(".//button[@value='" + t.getData()
						+ "'] | .//input[@value='" + t.getData()
						+ "' and @type='button']");
			} else {
				by = By.xpath(".//*[@" + idType + "='" + uniqueId + "']");
			}

			while (element == null && secCounter <= objectIdentificationTimeout) {
				secCounter++;
				logger.info("locateElement - Try no --> "+secCounter);
				try {
					if (wrapper == null) {
						elementList = this.driverContext.getDriver()
								.findElements(by);
					} else {
						elementList = wrapper.findElements(by);
					}
					boolean eleFound = false;
					for (WebElement ele : elementList) {
						if (ele.isDisplayed()){
							element = ele;
							eleFound = true;
							break;
						}
					}
					if (!eleFound) {
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
					}
				}
			}
		}

		return element;
	}

	private final WebElement processButtonHandlers(TestObject t, WebElement wrapper)
			throws AutException {
		WebElement element;
		if (t.getData() != null
				&& t.getData().trim().toLowerCase().startsWith("@@message")) {
			
			t.setObjectClass("na");
			element = this.driverContext.getDriver().findElement(
					By.xpath(".//html"));
			
			String data[] = t.getData().split(",");

			if (data.length == 2) {
				this.webGenericMethods.addMessageToList(data[1], "");
			} else if (data.length == 3) {
				this.webGenericMethods.addMessageToList(data[1], data[2]);
			} else {
				new AutException("Wrong number of arguments for @@message in "
						+ t.getData());
			}
		} else {
			element = this.processButtonHandlersExtended(wrapper, t);

			if (element == null) {
				t.setObjectClass("na");
				element = this.driverContext.getDriver().findElement(
						By.xpath(".//html"));
			}
		}
		return element;
	}

	public abstract WebElement locateElementExtend(TestObject t)
			throws AutException;

	public abstract WebElement processButtonHandlersExtended(WebElement wrapper, TestObject t)
			throws AutException;
	
	// Inputs
	// //////////////////////////////////////////////////////////////////////////////
	public void performInput(WebElement element, TestObject t, String data)
			throws AutException {

		String elemType = t.getObjectClass();

		try {

			this.prePerformInput(element, t, data);
			
			boolean inputPerformed = this.performInputExtend(element, t, data);
			if (inputPerformed) {
				//Input handled in extended so nothing needs to be done
			} else {
				if (FieldType.TEXTAREA.toString().equalsIgnoreCase(elemType)
						|| FieldType.TEXTBOX.toString().equalsIgnoreCase(elemType)
						|| FieldType.PASSWORD.toString().equalsIgnoreCase(elemType)) {
					this.textBoxInput(t, element, data);
				} else if (FieldType.CHECKBOX.toString().equalsIgnoreCase(elemType)) {
					this.checkBoxInput(t, element, data);
				} else if (FieldType.RADIO.toString().equalsIgnoreCase(elemType)) {
					this.radioButtonInput(t, element, data);
				} else if (FieldType.LIST.toString().equalsIgnoreCase(elemType)) {
					this.listInput(t, element, data);
				} else if (FieldType.BUTTON.toString().equalsIgnoreCase(elemType)) {
					this.buttonInput(t, element);
				}
			}
			
			this.postPerformInput(element, t, data);
 
		} catch (ExecutorException e) {

			if (e.getCause() != null
					&& e.getCause() instanceof StaleElementReferenceException) {
				logger.error("STALE ELEMENT >> PAGE DOM HAS REFRESHED");
				throw new ExecutorException(
						"Could not perform I/O as page DOM has refreshed",
						e.getCause());
			}

			if(e.getMessage()!=null&&!e.getMessage().equals("")){
				logger.error(e.getMessage());
				throw new AutException(e.getMessage());
			}else{
				logger.error("Could not input {} to field {} on page {}", data,
						t.getLabel(), t.getLocation());
				throw new AutException("Could not input " + data + " to field "
						+ t.getLabel() + " on page " + t.getLocation());
			}
		} catch (AutException e) {
			
			if(e.getMessage()!=null&&!e.getMessage().equals("")){
				logger.error(e.getMessage());
				throw new AutException(e.getMessage());
			}else{
				logger.error("Could not input {} to field {} on page {}", data,
						t.getLabel(), t.getLocation());
				throw new AutException("Could not input " + data + " to field "
						+ t.getLabel() + " on page " + t.getLocation());
			}
		}  catch (Exception e) {
			logger.error("Could not input {} to field {} on page {}", data,
					t.getLabel(), t.getLocation());
			throw new AutException("Could not input " + data + " to field "
					+ t.getLabel() + " on page " + t.getLocation());
		}
	}

	public abstract boolean performInputExtend(WebElement element, TestObject t,
			String data) throws AutException;

	public abstract void prePerformInput(WebElement element, TestObject t,
			String data) throws AutException;

	public abstract void postPerformInput(WebElement element, TestObject t,
			String data) throws AutException;

	public void textBoxInput(TestObject t, WebElement element, String data)
			throws ExecutorException, StaleElementReferenceException {	
		try {
			element.clear();
			if (data != null && !data.equalsIgnoreCase("@@CLEAR")) {
				element.sendKeys(data); 
			}
		} catch (Exception e) {
				logger.error(
						"An exception occurred while setting value to text field",
						e);
				throw new ExecutorException("Could not set value " + data
						+ " in text field", e);
			
		}
	}

	public void listInput(TestObject t, WebElement element, String data)
			throws ExecutorException {
		try {
			Select select = new Select(element);
			select.selectByVisibleText(data);
		} catch (Exception e) {
			logger.error(
					"An exception occurred while selecting value from list box",
					e);
			throw new ExecutorException("Could not select " + data
					+ " from list ");
		}
	}

	public void radioButtonInput(TestObject t, WebElement element, String data)
			throws ExecutorException {
		try {
			/*
			 * if(data.equalsIgnoreCase("yes")){ element.click(); }
			 */
			boolean targetRadioFound = false;
			List<WebElement> radios = this.driverContext.getDriver()
					.findElements(By.name(t.getUniqueId()));
			if (radios != null && radios.size() > 0) {
				for (WebElement radio : radios) {
					String rLabel = radio.getAttribute("value");
					if (rLabel != null && rLabel.equalsIgnoreCase(data)) {
						targetRadioFound = true;
						radio.click();
						break;
					}
				}
			}

			if (!targetRadioFound) {
				throw new ExecutorException("Could not find Option " + data
						+ " in " + t.getLabel());
			}

		} catch (ExecutorException e) {
			throw new ExecutorException(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(
					"An exception occurred while setting value to checkbox/radio button",
					e);
			throw new ExecutorException("Could not input " + data
					+ " to field " + t.getLabel(), e);
		}
	}

	public void checkBoxInput(TestObject t, WebElement element, String data)
			throws ExecutorException {
		try {
			if (data.equalsIgnoreCase("yes")) {
				if (!element.isSelected()) {
					element.click();
				}
			} else if (data.equalsIgnoreCase("no")) {
				if (element.isSelected()) {
					element.click();
				}
			} else {
				throw new ExecutorException("Could not input " + data
						+ " in checkbox");
			}
		} catch (Exception e) {
			logger.error(
					"An exception occurred while setting value to checkbox/radio button",
					e);
			throw new ExecutorException("Could not input " + data
					+ " in checkbox");
		}
	}

	public void buttonInput(TestObject t, WebElement element) throws ExecutorException {
		try {

			element.click();

			Thread.sleep(500);

		} catch (Exception e) {
			logger.error("An exception occurred while clicking the button", e);
			throw new ExecutorException("Could not click the button.");
		}
	}

	// Outputs
	// /////////////////////////////////////////////////////////
	public String performOutput(WebElement element, TestObject t)
			throws AutException {

		String elemType = t.getObjectClass();

		String retVal = "";

		try {
			
			this.prePerformOutput(element, t);
			
			String extendRetVal = this.performOutputExtend(element, t);
			if (extendRetVal != null && !extendRetVal.equals("")) {
				retVal = extendRetVal;
			} else { 
				if (FieldType.TEXTBOX.toString().equalsIgnoreCase(elemType)
						|| FieldType.PASSWORD.toString().equalsIgnoreCase(
								elemType)) {
					retVal = this.textBoxOutput(element, t);
				} else if (FieldType.TEXTAREA.toString().equalsIgnoreCase(
						elemType)) {
					retVal = this.textAreaOutput(element, t);
				} else if (FieldType.CHECKBOX.toString().equalsIgnoreCase(
						elemType)) {
					retVal = this.checkBoxOutput(element, t);
				} else if (FieldType.RADIO.toString()
						.equalsIgnoreCase(elemType)) {
					retVal = this.radioButtonOutput(element, t);
				} else if (FieldType.LIST.toString().equalsIgnoreCase(elemType)) {
					retVal = this.listOutput(element, t);
				}
			}
			
			this.postPerformOutput(element, t);

		} catch (ExecutorException e) {
			if(e.getMessage()!=null&&!e.getMessage().equals("")){
				logger.error(e.getMessage());
				throw new AutException(e.getMessage());
			}else{
				logger.error("Could not get value of field {} on page {}",
						t.getLabel(), t.getLocation());
				throw new AutException("Could not get value of field "
						+ t.getLabel() + " on page " + t.getLocation());
			}
		} catch (AutException e) {
			if(e.getMessage()!=null&&!e.getMessage().equals("")){
				logger.error(e.getMessage());
				throw new AutException(e.getMessage());
			}else{
				logger.error("Could not get value of field {} on page {}",
						t.getLabel(), t.getLocation());
				throw new AutException("Could not get value of field "
						+ t.getLabel() + " on page " + t.getLocation());
			}
		} catch (Exception e) {
			logger.error("Could not get value of field {} on page {}",
					t.getLabel(), t.getLocation());
			throw new AutException("Could not get value of field "
					+ t.getLabel() + " on page " + t.getLocation());
		}

		return retVal;
	}

	public abstract String performOutputExtend(WebElement element, TestObject t)
			throws AutException;

	public abstract void prePerformOutput(WebElement element, TestObject t) throws AutException;

	public abstract void postPerformOutput(WebElement element, TestObject t) throws AutException;

	public String textBoxOutput(WebElement element, TestObject t) throws ExecutorException {
		try {
			return element.getAttribute("value");
		} catch (Exception e) {
			logger.error("ERROR getting value from text box", e);
			throw new ExecutorException("Could not get value of text box");
		}
	}

	protected String textAreaOutput(WebElement element, TestObject t)
			throws ExecutorException {
		try {
			return element.getText();
		} catch (Exception e) {
			logger.error("ERROR getting value from textarea", e);
			throw new ExecutorException("Could not get value of text area");
		}
	}

	public String listOutput(WebElement element, TestObject t) throws ExecutorException {

		try {
			Select select = new Select(element);
			WebElement option = select.getFirstSelectedOption();

			return option.getText();
		} catch (Exception e) {
			throw new ExecutorException("Could not get value of list ", e);
		}

	}

	public String radioButtonOutput(WebElement element, TestObject t)
			throws ExecutorException {
		String retVal = "";
		try {
			List<WebElement> radios = this.driverContext.getDriver()
					.findElements(By.name(t.getUniqueId()));
			if (radios != null && radios.size() > 0) {
				for (WebElement radio : radios) {
					String rLabel = radio.getAttribute("value");
					if (element.isSelected()) {
						retVal = rLabel;
						break;
					}
				}
			}
		} catch (Exception e) {
			logger.error("Could not get value of radio button {}",
					t.getLabel(), e);
			throw new ExecutorException("Could not get value of radio button "
					+ t.getLabel());
		}

		return retVal;

	}

	public String checkBoxOutput(WebElement element, TestObject t) throws ExecutorException {
		try {
			if (element.isSelected()) {
				return "YES";
			} else {
				return "NO";
			}
		} catch (Exception e) {
			throw new ExecutorException("Could not get value of checkbox");
		}
	}

}
