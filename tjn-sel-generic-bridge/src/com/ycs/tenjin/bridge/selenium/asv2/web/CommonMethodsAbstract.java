package com.ycs.tenjin.bridge.selenium.asv2.web;

import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.selenium.asv2.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv2.generic.WebMethodsImpl;

public abstract class CommonMethodsAbstract implements CommonMethods {

	protected DriverContext driverContext;
	protected WebMethodsImpl webGenericMethods;

	public CommonMethodsAbstract(DriverContext driverContext,WebMethodsImpl webGenericMethods) {
		this.driverContext = driverContext;
		this.webGenericMethods = webGenericMethods;
	}

	// Navigate To Page
	 /////////////////////////////////////////////////////////////////////////////
	public void navigateToPage(Location location) throws BridgeException {
		this.navigateToPageExtend(location);
	}

	public abstract void navigateToPageExtend(Location location) throws BridgeException;

	public abstract void navigateToButton(String buttonText) throws BridgeException;

	public abstract void navigateToLink(String buttonText) throws BridgeException;
	
}
