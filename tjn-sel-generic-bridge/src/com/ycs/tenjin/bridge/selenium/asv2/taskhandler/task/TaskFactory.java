/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TaskFactory.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Oct 27, 2017         Sameer Gupta          	Newly Added For  
 */

package com.ycs.tenjin.bridge.selenium.asv2.taskhandler.task;

import java.lang.reflect.Constructor;
import java.util.Set;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.selenium.asv2.SeleniumApplicationImpl;

public class TaskFactory {

	private static final Logger logger = LoggerFactory
			.getLogger(TaskFactory.class);

	public static LearnerTaskAbstract getLearner(SeleniumApplicationImpl application,
			Module module) throws BridgeException {
//changed by shivam temp start
		/*String expPackageName = "com.ycs.tenjin.adapter.appium.+ application.getAdapterPrefix();"*/
		String expPackageName = "com.ycs.tenjin.adapter.selenium."
				+ application.getAdapterPrefix();
		logger.debug("Loading Learner from adapter package {}", expPackageName);
		Reflections reflections = new Reflections(expPackageName);

		Set<Class<? extends LearnerTaskAbstract>> subTypes = reflections
				.getSubTypesOf(LearnerTaskAbstract.class);
		for (Class<? extends LearnerTaskAbstract> type : subTypes) {
			try {
				Constructor<?> constructor = type.getConstructor(
						SeleniumApplicationImpl.class, Module.class);
				return (LearnerTaskAbstract) constructor.newInstance(application,
						module);
			} catch (Exception e) {
				logger.error("Could not initiate adapter for {}",
						application.getAppName(), e);
				throw new BridgeException("Could not initiate the learner for "
						+ application.getAppName());
			}
		}
		logger.error("Could not initiate learner for {}",
				application.getAppName());
		return null;

	}

	public static ExecutorTaskAbstract getExecutor(SeleniumApplicationImpl application,
			int runId, ExecutionStep step, int screenshotOption,
			int fieldTimeout) throws BridgeException {
		String expPackageName = "com.ycs.tenjin.adapter.selenium."
				+ application.getAdapterPrefix();
		logger.debug("Loading Executor from adapter package {}", expPackageName);

		Reflections reflections = new Reflections(expPackageName);

		Set<Class<? extends ExecutorTaskAbstract>> subTypes = reflections
				.getSubTypesOf(ExecutorTaskAbstract.class);
		for (Class<? extends ExecutorTaskAbstract> type : subTypes) {
			try {
				Constructor<?> constructor = type.getConstructor(int.class,
						ExecutionStep.class, int.class, int.class);
				return (ExecutorTaskAbstract) constructor.newInstance(runId, step,
						screenshotOption, fieldTimeout);
			} catch (Exception e) {
				logger.error("Could not initiate executor for {}",
						application.getAppName(), e);
				throw new BridgeException("Could not initiate Executor for "
						+ application.getAppName());
			}

		}
		logger.error("Could not initiate Executor for {}",
				application.getAppName());
		return null;
	}

}
