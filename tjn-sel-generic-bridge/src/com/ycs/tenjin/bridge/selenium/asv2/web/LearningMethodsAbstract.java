/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LearningMethodsAbstract.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Oct 27, 2017         Sameer Gupta          	Newly Added For  
 */

package com.ycs.tenjin.bridge.selenium.asv2.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.selenium.asv2.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv2.generic.WebMethodsImpl;
import com.ycs.tenjin.util.Utilities;

public abstract class LearningMethodsAbstract implements LearningMethods {

	public static final Logger logger = LoggerFactory
			.getLogger(LearningMethodsAbstract.class);
	public DriverContext driverContext;
	protected WebMethodsImpl webGenericMethods;

	public LearningMethodsAbstract(DriverContext driverContext,WebMethodsImpl webGenericMethods) {
		logger.info("AndroidLearningMethodsAbstract");
		this.driverContext = driverContext;
		this.webGenericMethods = webGenericMethods;
	}

	// Get Type Of Element
	// //////////////////////////////////////////////////////////////////////////////
	@Override
	public FieldType getTypeOfElement(Map<String, String> attributesMap) {
		FieldType elementDesc = null;

		FieldType extendDesc = this.getTypeOfElementExtend(attributesMap);
		if (extendDesc != null && !extendDesc.equals("")) {
			elementDesc = extendDesc;
		} else {

			String type = "";
			type = attributesMap.get("type");

			if (type == null || type.equalsIgnoreCase("")) {
				type = attributesMap.get("tag");
			}

			if (type != null && type.equalsIgnoreCase("text")) {
				elementDesc = FieldType.TEXTBOX;
			} else if (type != null && type.equalsIgnoreCase("password")) {
				elementDesc = FieldType.PASSWORD;
			} else if (type != null && type.equalsIgnoreCase("checkbox")) {
				elementDesc = FieldType.CHECKBOX;
			} else if (type != null && type.toLowerCase().contains("radio")) {
				elementDesc = FieldType.RADIO;
			} else if (type != null && type.equalsIgnoreCase("button")) {
				elementDesc = FieldType.BUTTON;
			} else if (type != null && type.equalsIgnoreCase("submit")) {
				elementDesc = FieldType.BUTTON;
			} else if (type != null && type.equalsIgnoreCase("file")) {
				elementDesc = FieldType.FILE;
			} else if (type != null && type.equalsIgnoreCase("select")) {
				elementDesc = FieldType.LIST;
			} else if (type != null && type.equalsIgnoreCase("a")) {
				elementDesc = FieldType.LINK;
			} else if (type != null && type.equalsIgnoreCase("table")) {
				elementDesc = FieldType.TABLE;
			} else if (type != null && type.equalsIgnoreCase("button")) {
				elementDesc = FieldType.BUTTON;
			} else if (type != null && type.equalsIgnoreCase("textarea")) {
				elementDesc = FieldType.TEXTAREA;
			} else if (type != null && type.equalsIgnoreCase("me")) {
				elementDesc = FieldType.TABLE;
			} else {
				logger.error("Element type not found");
			}
		}

		if (elementDesc != null) {
			attributesMap.put("elem_type", elementDesc.toString());
		}

		return elementDesc;

	}
	
	public abstract FieldType getTypeOfElementExtend(
			Map<String, String> attributesMap);

	// Get UniqueIdOfElement
	// //////////////////////////////////////////////////////////////////////////////
	@Override
	public Map<String, String> getUniqueIdOfElement(Map<String, String> attributesMap, WebElement element)
			throws LearnerException {
		Map<String, String> uidMap = new HashMap<String, String>();

		Map<String, String> extendUidMap = this.getUniqueIdOfElementExtend(
				attributesMap, element);
		if (extendUidMap != null) {
			uidMap = extendUidMap;
		} else {

			String uid = "";
			String identifiedBy = "";
			if (!attributesMap.get("elem_type").equalsIgnoreCase(
					FieldType.RADIO.toString())) {
				uid = Utilities.trim(attributesMap.get("id"));
				if (uid.equalsIgnoreCase("")) {
					uid = Utilities.trim(attributesMap.get("name"));
					if (!uid.equalsIgnoreCase("")) {
						identifiedBy = "name";
					} else {
						logger.error("Could not identify UID of element. ID or Name not found");
						logger.error("All attrs --> {}",
								attributesMap.get("all"));
					}
				} else {
					identifiedBy = "id";
				}
			} else {
				uid = Utilities.trim(attributesMap.get("name"));
				identifiedBy = "name";

				if (uid.equalsIgnoreCase("")) {
					logger.error("Could not identify UID for RADIO button. NAME attribute not defined");
				}
			}

			uidMap.put("uid", uid);
			uidMap.put("uidType", identifiedBy);
		}

		return uidMap;
	}
	
	public abstract Map<String, String> getUniqueIdOfElementExtend(
			Map<String, String> attributesMap, WebElement element)
			throws LearnerException;

	// Get Labels
	// //////////////////////////////////////////////////////////////////////////////
	@Override
	public String getLabelOfElement(FieldType type, WebElement element,
			Map<String, String> attributesMap) {
		String label = null;

		String extendLabel = this.getLabelOfElementExtend(type, element,
				attributesMap);
		if (extendLabel != null && !extendLabel.equals("")) {
			label = extendLabel;
		}
		if (label != null) {
			attributesMap.put("elem_label", label);
		}
		return label;
	}

	public abstract String getLabelOfElementExtend(FieldType type,
			WebElement element, Map<String, String> attributesMap);

	// Dropdown Values
	// //////////////////////////////////////////////////////////////////////////////
	@Override
	public String getDropdownValuesOfElement(FieldType type,
			WebElement element, Map<String, String> attributesMap)
			throws LearnerException {

		String dropValues = "";

		String extendDropValues = this.getDropdownValuesOfElementExtend(type,
				element, attributesMap);
		if (extendDropValues != null && !extendDropValues.equals("")) {
			dropValues = extendDropValues;
		} else {
			if (type.toString().equalsIgnoreCase(FieldType.LIST.toString())) {
				dropValues = getDropdownValuesOfList(element);
			} else if (type.toString().equalsIgnoreCase(
					FieldType.RADIO.toString())) {
				dropValues = getDropdownValuesOfRadioGroup(element,
						attributesMap);
			} else if (type.toString().equalsIgnoreCase(
					FieldType.CHECKBOX.toString())) {
				dropValues = "YES;NO";
			}
		}
		return dropValues;
	}

	public abstract String getDropdownValuesOfElementExtend(FieldType type,
			WebElement element, Map<String, String> attributesMap);

	public String getDropdownValuesOfRadioGroup(WebElement element,
			Map<String, String> attributesMap) throws LearnerException {
		String dropValues = "";
		List<WebElement> radios = null;
		try {
			String radioName = attributesMap.get("name");
			radios = this.driverContext.getDriver().findElements(
					By.name(radioName));
		} catch (Exception e) {
			logger.error("ERROR in locateElemenets(By by)", e);
			throw new LearnerException(
					"Could not identify elements with the specified criteria",
					e);
		}
		for (WebElement radio : radios) {
			dropValues = dropValues + "," + radio.getAttribute("value");
		}
		return dropValues;
	}

	public String getDropdownValuesOfList(WebElement element)
			throws LearnerException {
		String dropValues = "";
		try {
			String innerHtml = element.getAttribute("innerHTML");
			innerHtml = innerHtml.replace("< /", "</");
			innerHtml = innerHtml.replace("<  /", "</");
			innerHtml = innerHtml.replace("<   /", "</");
			innerHtml = innerHtml.replace("<    /", "</");
			innerHtml = innerHtml.replace("<     /", "</");
			String[] options = innerHtml.split("</");
			for (int i = 0; i < options.length - 1; i++) {
				String[] values = options[i].split(">");
				String value = "";
				if (values.length > 1) {
					value = values[values.length - 1];
				}
				dropValues = dropValues + value + ";";
			}
			if (!dropValues.equals(""))
				dropValues = dropValues.substring(0, dropValues.length() - 1);
		} catch (Exception e) {
			logger.error("Error to get options for : "
					+ element.getAttribute("innerHTML"));
		}

		return dropValues;
	}

	// Additional
	// ///////////////////////////////////////////
	public abstract boolean isMandatory(Map<String, String> attributesMap,
			WebElement element) throws LearnerException;

	public boolean elementHasLOV(Map<String, String> attributesMap,
			WebElement element) throws LearnerException {
		return false;
	}

	public boolean elementHasAutoLOV(Map<String, String> attributesMap,
			WebElement element) {
		return false;
	}

	public String getFieldGroup(Map<String, String> attributesMap,
			WebElement element) throws LearnerException {
		return null;
	}

}
