package com.ycs.tenjin.bridge.selenium.asv2.web;

import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.pojo.aut.Location;

public interface CommonMethods {

	public void navigateToPage(Location location) throws BridgeException;

	public abstract void navigateToButton(String buttonText) throws BridgeException;

	public abstract void navigateToLink(String buttonText) throws BridgeException; 
	
}
