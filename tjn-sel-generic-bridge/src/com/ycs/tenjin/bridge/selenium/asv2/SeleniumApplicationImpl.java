/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SeleniumApplicationImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Oct 27, 2017         Sameer Gupta          	Newly Added For  
 */

package com.ycs.tenjin.bridge.selenium.asv2;

import org.openqa.selenium.WebDriver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.DriverException;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationAbstract;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.selenium.asv2.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv2.generic.WebMethodsImpl;
import com.ycs.tenjin.util.Utilities;

public abstract class SeleniumApplicationImpl extends GuiApplicationAbstract {

	private static final Logger logger = LoggerFactory.getLogger(SeleniumApplicationImpl.class);

	// /////////////////////////////////////////////////
	// Variables
	// /////////////////////////////////////////////////
	protected DriverContext driverContext;
	protected int OBJECT_IDENTIFICATION_TIMEOUT = 0;
	protected String compositeXpath = ".//input[@type='text' or @type='Text' or @type='TEXT']|.//textarea|.//input[@type='password' or @type='Password' or @type='PASSWORD' or @type='passWord' or @type='PassWord']|.//button|.//input[@type='button' or @type='submit' or @type='Button' or @type='Submit' or @type='BUTTON' or @type='SUBMIT']|.//select|.//input[@type='radio' or @type='Radio' or @type='RADIO']|.//input[@type='Checkbox' or @type='CHECKBOX' or @type='checkbox' or @type='checkBox']";

	protected WebMethodsImpl webGenericMethods;
	public String clientIp = "" ;
	
	// /////////////////////////////////////////////////
	// Constructor
	// /////////////////////////////////////////////////
	public SeleniumApplicationImpl(Aut aut, String clientIp, int port,
			String browser) {
		super(aut, clientIp, port, browser, null);
		this.clientIp = clientIp;
		/*added by Prem for  v210Reg-30 start*/
		this.browser = aut.getBrowser();
		/*added by Prem for  v210Reg-30 start*/
		this.adapterPackageOrTool=this.adapterPackageOrTool+".asv2";
		OBJECT_IDENTIFICATION_TIMEOUT = 15;
	}

	// /////////////////////////////////////////////////
	// Getters
	// /////////////////////////////////////////////////

	public String getCompositeXpath() {
		return compositeXpath;
	}

	public DriverContext getDriverContext() {
		return driverContext;
	}

	public int getObjectIdentificationTimeout() {
		return OBJECT_IDENTIFICATION_TIMEOUT;
	}

	// /////////////////////////////////////////////////
	// Overriding Methods
	// /////////////////////////////////////////////////

	public void launch(String url) throws AutException, DriverException {

		try {
			WebDriver driver = com.ycs.tenjin.bridge.selenium.asv1.generic.Launcher.getDriver(this.clientIp, this.port,
					this.browser);

			driver.get(url);
			this.driverContext = new DriverContext();
			this.driverContext.setDriver(driver); 
			
			this.webGenericMethods = new WebMethodsImpl(this.driverContext);
		} catch (Exception e) {
			logger.error("Could not launch application", e);
			throw new AutException("Could not launch AUT", e);
		}
	}

	@Override
	public void destroy() {
		try {
			this.driverContext.getDriver().quit();
		} catch (Exception ignore) {
		}
	}
	
	@Override
	public String toString() {
		return "New Spec";
	}

	@Override
	//This method is deprecated in new adapter spec
	public void explicitWait(long seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (Exception e) {
		}
		 
	}
}
