/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SOAPApiBridgeImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 03-Apr-2017           Sriram Sridharan          Newly Added For
* 17-Aug-2017			Sriram Sridharan		T25IT-192
* 22-Feb-2018			Sriram Sridharan		TENJINCG-610
* 04-June-2018          Leelaprasad             TENJINCG-662
* 12-12-2018			Sriram Sridharan		TENJINCG-913
* 17-01-2019			Preeti					TJN252-70
* 18-12-2020            Paneendra               TENJINCG-1239
*/


package com.ycs.tenjin.adapter.api.soap.generic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.eviware.soapui.impl.wsdl.WsdlInterface;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.support.wsdl.WsdlImporter;
import com.eviware.soapui.model.iface.Operation;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.oem.api.ApiApplication;
import com.ycs.tenjin.bridge.oem.api.generic.ApiLearnType;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Metadata;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.util.Utilities;

/*Changed by leelaprasad for the defect TENJINCG-662 starts*/
/*public abstract class SOAPApiBridgeImpl implements ApiApplication {*/
public class SOAPApiBridgeImpl implements ApiApplication {
	/*Changed by leelaprasad for the defect TENJINCG-662 ends*/
	
	private static final Logger logger = LoggerFactory.getLogger(SOAPApiBridgeImpl.class);
	
	private List<TestObject> testObjects = new ArrayList<TestObject>();
	private ArrayList<TreeMap<String, TestObject>> testObjectMap;
	private Multimap<String, Location> pageAreas;
	
	private int pageSequence;
	private int fieldSequence;
	private int tabOrder;
	private String uniqueLocationName;
	
	public String tdUid;
	private int iteration;
	private int runId;
	private ExecutionStep step;

	
	public SOAPApiBridgeImpl() {
		
		this.initialize();
	}
	
	public SOAPApiBridgeImpl(int runId, ExecutionStep step) {
		this.runId = runId;
		this.step = step;
	}

	@Override
	public void initialize() {
		
		this.testObjectMap = new ArrayList<TreeMap<String, TestObject>>();
		this.pageAreas = ArrayListMultimap.create();
		this.pageSequence=-1;
		this.fieldSequence=-1;
		this.tabOrder = -1;
		this.uniqueLocationName = "";
	}

	@Override
	public String getAdapterName() {
		
		return null;
	}

	@Override
	//changes done by parveen, Leelaprasad(need to be work on Multiple Response) starts
/*	public Metadata learn(Api api, ApiOperation operation, String learnType, String entityType)
			throws BridgeException {*/
	public ArrayList<Metadata> learn(Api api, ApiOperation operation, String learnType, String entityType)
			throws BridgeException {
		//need to work on soap 
		ArrayList<Metadata> metaDataList=new ArrayList<Metadata>();
		//changes done by parveen, Leelaprasad(need to be work on Multiple Response) ends	
		
		/* Fix for T25IT-192 */
		if(Utilities.trim(learnType).equalsIgnoreCase("")) {
			learnType = ApiLearnType.URL.toString();
		}
		/* Fix for T25IT-192 ends*/
		
		
		String rDescriptor = "";
		if(Utilities.trim(learnType).equalsIgnoreCase(ApiLearnType.URL.toString())){
			if(Utilities.trim(entityType).equalsIgnoreCase("request")) {
				rDescriptor = GenericSOAPUtils.getRequestXml(api.getUrl(), operation.getName());
			} else if(Utilities.trim(entityType).equalsIgnoreCase("response")) {
				rDescriptor = GenericSOAPUtils.getResponseXml(api.getUrl(), operation.getName());
			}
		}else if(Utilities.trim(learnType).equalsIgnoreCase(ApiLearnType.REQUEST_XML.toString())){
			if(Utilities.trim(entityType).equalsIgnoreCase("request")) {
				//changes done by parveen, Leelaprasad(need to be work on Multiple Response) starts
					if(operation.getListRequestRepresentation().size()>0){
						rDescriptor = operation.getListRequestRepresentation().get(0).getRepresentationDescription();
					}
					
			} else if(Utilities.trim(entityType).equalsIgnoreCase("response")) {
			//	rDescriptor = operation.getResponseDescriptor();
				if(operation.getListResponseRepresentation().size()>0){
					/*Modified by Preeti for TJN252-70 starts*/
					/*rDescriptor = operation.getListResponseRepresentation().get(0).getRepresentationDescription();*/
					rDescriptor = operation.getListResponseRepresentation().get(0).getRepresentationDescription().trim();
					/*Modified by Preeti for TJN252-70 ends*/
				}
				//changes done by parveen, Leelaprasad(need to be work on Multiple Response) ends
			}
		}
		
		Metadata metadata=null;
		
		try {
			/*Modified by Preeti for TJN252-70 starts*/
			if(!rDescriptor.equals(""))
				metadata = this.learnXml(rDescriptor);
			/*Modified by Preeti for TJN252-70 ends*/
		} catch (Exception e) {
			
			logger.error("Could not parse {} descriptor for this operation.", entityType, e);
			throw new BridgeException("Could not parse " + entityType + " descriptor for this operation.");
		}
		
		if(metadata != null) {
			metadata.setDescriptor(rDescriptor);
		}
		//changes done by parveen, Leelaprasad(need to be work on Multiple Response) starts	
	//	return metadata;
		/*Modified by Preeti for TJN252-70 starts*/
		if(metadata != null) 
			metaDataList.add(metadata);
		/*Modified by Preeti for TJN252-70 ends*/
		return metaDataList;
		//changes done by parveen, Leelaprasad(need to be work on Multiple Response) ends
	}

	@Override
	public Metadata learn(Api api, ApiOperation operation, String entityType) throws BridgeException {
		
		return null;
	}

	@Override
	public Metadata learn(Api api, String entityType) throws BridgeException {
		
		return null;
	}

	@Override
	public List<ApiOperation> getAllOperations(String url) throws BridgeException {
		
		return null;
	}

	@Override
	public void getAllOperations(Api api) throws BridgeException {
		
		api.setOperations(new ArrayList<ApiOperation>());
		try {
			WsdlProject project = new WsdlProject();
			WsdlInterface[] wsdls = WsdlImporter.importWsdl(project,api.getUrl());
			
			logger.debug("WSDLs Length for URL [{}] is ", api.getUrl(), wsdls.length);

			int index=0;
			for(WsdlInterface wsInterface: wsdls) {
				index++;

				logger.debug("Scanning WSDL " + index);
				Operation[] operations = wsInterface.getAllOperations();

				logger.debug("This WSDL has " + operations.length + " operations.");

				for(Operation op:operations) {
					logger.debug("Operation [" + op.getName() + "]");
					ApiOperation operation = new ApiOperation();
					operation.setName(op.getName());
					api.getOperations().add(operation);
				}
			}
		} catch (Exception e) {
			
			logger.error("ERROR fetching operations for URL [{}]", api.getUrl(), e);
			throw new BridgeException("Could not fetch available operations. Please contact Tenjin Support.");
		}
	}
	
	
	private Metadata learnXml(String sourceXml) throws LearnerException{
		
		logger.info("Loading XML document");
		Document document = GenericSOAPUtils.loadDocument(sourceXml, true);
		
		logger.debug("Getting root element");
		Element rootElement = document.getDocumentElement();
		String rootElementNamespaceURI = rootElement.getNamespaceURI();
		
		logger.debug("Root Element [{}], Namespace URI [{}]", rootElement.getLocalName(), rootElementNamespaceURI);
		NodeList soapBodyNodeList = rootElement.getElementsByTagNameNS(rootElementNamespaceURI, "Body");
		
		if(soapBodyNodeList == null || soapBodyNodeList.getLength() < 1) {
			logger.error("ERROR - SOAP Body Element not found");
			throw new LearnerException("SOAP Body Element was not found. Please ensure the XML is valid.");
		}
		
		//For TENJINCG-610
		/*this.scanNode(soapBodyNodeList.item(0));*/
		this.scanNode(soapBodyNodeList.item(0), null);
		//For TENJINCG-610 ends
		
		Metadata metadata = new Metadata();
		
		metadata.setPageAreas(this.pageAreas);
		metadata.setTestObjectMap(this.testObjectMap);
		return metadata;
		
	}
	
	//For TENJINCG-610
	/*private void scanNode(Node node) throws LearnerException{*/
	private void scanNode(Node node, String uniqueNameForThisNode) throws LearnerException{
		//For TENJINCG-610 ends
		try {
			String nodeName = node.getNodeName();
			//For TENJINCG-610
			if(uniqueNameForThisNode == null)
				uniqueNameForThisNode = nodeName;
			//For TENJINCG-610 ends
			
			logger.debug("Scanning node {}", nodeName);
			NodeList childNodes = node.getChildNodes();
			logger.debug("This node has [{}] child nodes", childNodes.getLength());
			
			for(int i=0; i<childNodes.getLength(); i++) {
				Node childNode = childNodes.item(i);
				
				if(childNode.getNodeType() != Node.ELEMENT_NODE) {
					continue;
				}
				
				String childNodeLocalName = childNode.getLocalName();
				
				
				if(GenericSOAPUtils.hasChildNodes(childNode)) {
					logger.debug("Creating page area for [{}]", childNodeLocalName);
					//For TENJINCG-610
					String uniqueName = this.createPageAreaObject(childNode);
					this.scanNode(childNode, uniqueName);
					//For TENJINCG-610 ends
				} else {
					//For TENJINCG-610
					/*this.createTestObject(childNode);*/
					this.createTestObject(childNode, uniqueNameForThisNode);
					//For TENJINCG-610 ends
				}

			}
			
		} catch(LearnerException e) {
			throw e;
		} catch(Exception e) {
			logger.error("ERROR scanning node", e);
			throw new LearnerException("Could not learn XML due to an internal error.");
		}
		
		TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();
		for(TestObject t:this.testObjects){
			pMap.put(t.getUniqueId(), t);
		}
		this.testObjects.clear();
		this.testObjectMap.add(pMap);
		this.tabOrder = -1;
		
	}
	
	//For TENJINCG-610
	/*private void createPageAreaObject(Node node) throws LearnerException{*/
	private String createPageAreaObject(Node node) throws LearnerException{
		//For TENJINCG-610 ends
		boolean landingPage = false;
		this.pageSequence++;
		if(this.pageSequence < 1) {
			landingPage = true;
		}
		
		try{
			Location location = new Location();
			location.setLabel(node.getLocalName());
			location.setLocationType("XMLNODE");
			location.setLandingPage(landingPage);
			if(node.getParentNode() != null) {
				location.setParent(node.getParentNode().getLocalName());
			}else{
				location.setParent("N/A");
			}
			
			location.setPath(GenericSOAPUtils.getPath(node));
			location.setScreenTitle(node.getNodeName());
			location.setSequence(this.pageSequence);
			location.setWayIn("N/A");
			location.setWayOut("N/A");
			
			this.addToMap(location);
			
			this.uniqueLocationName = location.getLocationName();
			//For TENJINCG-610
			return this.uniqueLocationName;
			//For TENJINCG-610 ends
		} catch (Exception e) {
			logger.error("ERROR occurred while creating location", e);
			throw new LearnerException("Could not create location");
		}
	}
	
	private void addToMap(Location location) throws LearnerException{
		try{
			String locationName = location.getLabel();
			logger.debug("Scanning map for existing pages with name " + locationName);
			Collection<Location> locs = this.pageAreas.get(locationName);
			if(locs != null && locs.size() > 0){
				logger.debug("There is already " + locs.size() + " page(s) with name " + locationName);
				int nc = locs.size() + 1;
				String newLocName = "00" + Integer.toString(nc);
				newLocName = newLocName.substring(newLocName.length() -2);
				newLocName = locationName + " " + newLocName;
				logger.debug("New Location Name will be " + newLocName);
				location.setLocationName(newLocName);
			}else{
				logger.debug("There are no existing pages with name " + locationName);
				location.setLocationName(locationName);
			}

			this.pageAreas.put(locationName, location);
			logger.info("New Location --> {}", location.toString());
		}catch(Exception e){
			throw new LearnerException("Could not resolve location name for " + location.getLocationName() ,e);
		}
	}
	
	//For TENJINCG-610
	/*private void createTestObject(Node node) {*/
	private void createTestObject(Node node, String parentLocationName) {
		//For TENJINCG-610 ends
		this.fieldSequence++;
		this.tabOrder++;
		
		TestObject t = new TestObject();
		t.setLabel(node.getLocalName());
		t.setUniqueId(node.getNodeName());
		t.setIdentifiedBy("label");
		t.setObjectClass(String.valueOf(node.getNodeType()));
		t.setDefaultOptions(null);
		t.setMandatory("NO");
		t.setLovAvailable("N");
		t.setAutoLovAvailable("N");
		t.setViewmode("Y");
		t.setTabOrder(this.tabOrder);
		t.setSequence(this.fieldSequence);
		//For TENJINCG-610
		/*t.setLocation(this.uniqueLocationName);*/
		t.setLocation(parentLocationName);
		//For TENJINCG-610 ends
		t.setIsMultiRecord("N");
		t.setGroup("");
		this.testObjects.add(t);
	}

	@Override
	public IterationStatus executeIteration(JSONObject dataSet, int iteration, int expectedResponseCode /* Parameter added by Sriram for TENJINCG-913*/ ) throws BridgeException {
		
		
		this.tdUid = "";
		IterationStatus iStatus = new IterationStatus();
		iStatus.setRuntimeFieldValues(new ArrayList<RuntimeFieldValue>());
		this.iteration = iteration;
		
		String baselineDescriptor = "";
		String apiUrl = "";
		
		if(step.getApi() != null) {
			apiUrl = step.getApi().getUrl();
		}else {
			logger.error("Step API is null");
			iStatus.setResult("E");
			iStatus.setMessage("An internal error occurred. Please contact Tenjin Support.");
			return iStatus;
		}
		//changes done by parveen, Leelaprasad(need to be work on Multiple Response) starts
		/*if(this.step.getApiOperation() != null && this.step.getApiOperation().getRequestDescriptor() != null) {
			baselineDescriptor = this.step.getApiOperation().getRequestDescriptor();
		}else{
			iStatus.setResult("E");
			iStatus.setMessage("No Baseline Request descriptor found for Operation [" + this.step.getApiOperation().getName() + "]. Please ensure this operation is learnt successfull.");
			return iStatus;
		}*/
		
		if(this.step.getApiOperation() != null && (!this.step.getApiOperation().getListRequestRepresentation().isEmpty())) {
			baselineDescriptor = this.step.getApiOperation().getListRequestRepresentation().get(0).getRepresentationDescription();
		}else{
			iStatus.setResult("E");
			iStatus.setMessage("No Baseline Request descriptor found for Operation [" + this.step.getApiOperation().getName() + "]. Please ensure this operation is learnt successfull.");
			return iStatus;
		}
		//changes done by parveen, Leelaprasad(need to be work on Multiple Response) ends
		
		try {
			this.tdUid = dataSet.getString("TDUID");
			
			logger.info("Beginning execution of Transaction [{}]", tdUid);
			logger.info("Building request...");
			
			GenericSOAPUtils util = new GenericSOAPUtils();
			JSONArray pageAreas = dataSet.getJSONArray("PAGEAREAS");
			
			String requestXml = util.buildXMLRequest(tdUid, pageAreas, baselineDescriptor);
			iStatus.setApiRequestDescriptor(requestXml);
			
			logger.info("Calling SOAP Service");
			Map<String, String> resultMap = util.sendSOAPRequest(apiUrl, requestXml);
			
			if(resultMap.get("status").equalsIgnoreCase(TenjinConstants.ACTION_RESULT_SUCCESS)) {
				this.getPostExecutionResult(iStatus, resultMap.get("response"));
			} else {
				iStatus.setResult("F");
				iStatus.setMessage(resultMap.get("message"));
			}
			
			iStatus.setApiResponseDescriptor(resultMap.get("response"));
			
		} catch(JSONException e) {
			logger.error("JSONException caught", e);
			iStatus.setResult("E");
			iStatus.setMessage("An internal error occurred which aborted the execution of this transaction. Please contact Tenjin Support.");
		} catch(BridgeException e) {
			iStatus.setResult("E");
			iStatus.setMessage(e.getMessage());
		}
		
		return iStatus;
	}
	
	protected void getPostExecutionResult(IterationStatus iStatus, String responseXML) throws BridgeException {
		iStatus.setResult("S");
		iStatus.setMessage("Execution Passed.");
	}
	
	protected RuntimeFieldValue getRuntimeFieldValueObject(String field, String value, int detailRecordNo, String tdUid){
		RuntimeFieldValue r = new RuntimeFieldValue();

		r.setDetailRecordNo(1);
		r.setField(field);
		r.setIteration(this.iteration);
		r.setRunId(this.runId);
		r.setTdGid(this.step.getTdGid());
		r.setTdUid(tdUid);
		r.setTestStepRecordId(step.getRecordId());
		r.setValue(value);

		logger.info(r.toString());
		return r;
	}

	/*	Added By Paneendra for TENJINCG-1239 starts */
	@Override
	public IterationStatus executeIteration(JSONObject dataSet, JSONObject headerdataset, int iteration,
			int expectedResponseCode) throws BridgeException {
		
		return null;
	}
	/*	Added By Paneendra for TENJINCG-1239 starts */
}
