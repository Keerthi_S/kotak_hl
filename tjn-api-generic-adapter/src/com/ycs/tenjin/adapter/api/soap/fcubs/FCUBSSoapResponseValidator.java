package com.ycs.tenjin.adapter.api.soap.fcubs;

import com.ycs.tenjin.adapter.api.soap.generic.GenericSOAPResponseValidator;
import com.ycs.tenjin.bridge.oem.api.generic.ApiResponseValidator;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;

public class FCUBSSoapResponseValidator extends GenericSOAPResponseValidator implements ApiResponseValidator {

	public FCUBSSoapResponseValidator(int runId, ExecutionStep step, int iteration) {
		super(runId, step, iteration);
		
	}

}
