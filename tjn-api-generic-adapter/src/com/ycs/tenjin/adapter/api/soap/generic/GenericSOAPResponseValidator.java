/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GenericSOAPResponseValidator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 25-01-2019			Preeti					TJN252-80
 */

package com.ycs.tenjin.adapter.api.soap.generic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.oem.api.generic.ApiResponseValidator;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.util.Utilities;

public class GenericSOAPResponseValidator implements ApiResponseValidator {

	private static final Logger logger = LoggerFactory.getLogger(GenericSOAPResponseValidator.class);

	private int runId;
	private ExecutionStep step;
	private int iteration;
	
	public GenericSOAPResponseValidator(int runId, ExecutionStep step, int iteration) {
		this.runId = runId;
		this.step = step;
		this.iteration = iteration;
	}
	
	//@Override
	public List<ValidationResult> validateResponse2(JSONObject expectedResponseData, String actualResponseDescriptor) {
		
		List<ValidationResult> results = new ArrayList<ValidationResult>();
		
		
		
		Document document = null;
		
		logger.info("Loading Response XML");
		try{
			document = GenericSOAPUtils.loadDocument(actualResponseDescriptor, true);
		} catch(LearnerException e) {
			logger.error("Could not load Response XML", e);
			return results;
		}
		
		if(document == null) {
			logger.error("ERROR --> Empty document loaded from Response XML");
			logger.error("Response XML is invalid --> {}", actualResponseDescriptor);
			return null;
		}
		
		try {
			JSONArray pageAreas = expectedResponseData.getJSONArray("PAGEAREAS");
			
			for(int i=0; i<pageAreas.length(); i++) {
				JSONObject pageArea = pageAreas.getJSONObject(i);
				
				String pageName = pageArea.getString("PA_NAME");
				String path = pageArea.getString("PA_PATH");
				
				logger.debug("Validating Page [{}]", pageName);
				
				
				Node targetNode = new GenericSOAPUtils().getNode(document, pageName, path);
				
				if(targetNode == null) {
					logger.error("ERROR --> Could not find page [{}] in response XML.", pageName);
					continue;
				}
				
				JSONArray detailRecords = pageArea.getJSONArray("DETAILRECORDS");
				for(int dIndex = 0; dIndex < detailRecords.length(); dIndex ++) {
					JSONObject detailRecord = detailRecords.getJSONObject(dIndex);
					String detailId = detailRecord.getString("DR_ID");
					logger.debug("Validating detail ID [{}]", detailId);
					JSONArray fields = detailRecord.getJSONArray("FIELDS");
					
					for(int fIndex=0; fIndex < fields.length(); fIndex++) {
						JSONObject field = fields.getJSONObject(fIndex);
						/*Modified by Preeti for TJN252-80 starts*/
						/*String label = field.getString("FLD_LABEL_2");*/
						String label = field.getString("FLD_LABEL");
						/*Modified by Preeti for TJN252-80 ends*/
						String expectedFieldValue = field.getString("DATA");
						Node fieldNode = getNode(targetNode, label);
						
						if(fieldNode == null) {
							ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, "Could not locate this field in response XML.", detailId);
							results.add(r);
							continue;
						}
						
						String actualFieldValue = fieldNode.getTextContent();
						if(actualFieldValue == null) {
							actualFieldValue = "";
						}
						
						logger.debug("Field [{}], Expected [{}], Actual [{}]", label, expectedFieldValue, actualFieldValue);
						
						ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, actualFieldValue, detailId);
						results.add(r);
						
					}
					
				}
			}
		} catch (JSONException e) {
			
			logger.error("ERROR - JSONException caught", e);
			return results;
		}
		
		
		
		
		
		return results;
	}
	
	private Node getNode(Node parentNode, String childNodeName) {
		String childNodeLocalName = "";
		if(Utilities.trim(childNodeName).contains(":")) {
			String[] split = childNodeName.split(":");
			childNodeLocalName = split[1];
		}else{
			childNodeLocalName = childNodeName;
		}
		
		NodeList nList =((Element) parentNode).getElementsByTagNameNS("*", childNodeLocalName);
		
		if(nList.getLength() > 0) {
			return nList.item(0);
		} else{
			return null;
		}
	}
	
	private ValidationResult getValidationResultObject(String page, String field, String expectedValue, String actualValue, String detailRecordNo) {
		ValidationResult r = new ValidationResult();
		actualValue = Utilities.trim(actualValue);
		r.setActualValue(actualValue);
		r.setExpectedValue(expectedValue);
		r.setDetailRecordNo(detailRecordNo);
		r.setField(field);
		r.setPage(page);
		r.setIteration(this.iteration);
		r.setResValId(0);
		r.setRunId(this.runId);
		r.setTestStepRecordId(step.getRecordId());

		if(Utilities.isNumeric(actualValue) && Utilities.isNumeric(expectedValue)){
			if(Utilities.isNumericallyEqual(actualValue, expectedValue)){
				r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
			}else{
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		}else if(actualValue.equalsIgnoreCase(expectedValue)){
			/*************
			 * Fix by Sriram for comparision of numeric values Ends
			 */

			r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
		}else{

			if(Utilities.isNumeric(expectedValue) && Utilities.isNumeric(actualValue)){
				if(Double.parseDouble(expectedValue) == Double.parseDouble(actualValue)){
					r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
				}else{
					r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
				}
			}else{
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		}

		return r;

	}

	@SuppressWarnings("unused")
	@Override
	public Map<String, Object> validateResponse(JSONObject expectedResponseData, String actualResponseDescriptor) {
		
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		List<ValidationResult> results = new ArrayList<ValidationResult>();
		List<RuntimeFieldValue> runtimeValues = new ArrayList<RuntimeFieldValue>();
		
		
		GenericSOAPUtils soapUtils = new GenericSOAPUtils();
		
		Document document = null;
		Element rootElement = null;
		
		logger.info("Loading Response XML");
		try{
			document = GenericSOAPUtils.loadDocument(actualResponseDescriptor, true);
		} catch(LearnerException e) {
			logger.error("Could not load Response XML", e);
			return resultMap;
		}
		
		if(document == null) {
			logger.error("ERROR --> Empty document loaded from Response XML");
			logger.error("Response XML is invalid --> {}", actualResponseDescriptor);
			return null;
		}
		
		try {
			rootElement = document.getDocumentElement();
			JSONArray pageAreas = expectedResponseData.getJSONArray("PAGEAREAS");
			String tdUid = expectedResponseData.getString("TDUID");
			for(int i=0; i<pageAreas.length(); i++) {
				JSONObject pageArea = pageAreas.getJSONObject(i);
				
				String pageName = pageArea.getString("PA_NAME");
				String path = pageArea.getString("PA_PATH");
				
				logger.debug("Validating Page [{}]", pageName);
				
				Node targetNode = soapUtils.getNode(document, pageName, path);
				
				if(targetNode == null) {
					logger.error("ERROR --> Could not find page [{}] in response XML.", pageName);
					continue;
				}
				
				JSONArray detailRecords = pageArea.getJSONArray("DETAILRECORDS");
				for(int dIndex = 0; dIndex < detailRecords.length(); dIndex ++) {
					JSONObject detailRecord = detailRecords.getJSONObject(dIndex);
					String detailId = detailRecord.getString("DR_ID");
					logger.debug("Validating detail ID [{}]", detailId);
					JSONArray fields = detailRecord.getJSONArray("FIELDS");
					
					for(int fIndex=0; fIndex < fields.length(); fIndex++) {
						JSONObject field = fields.getJSONObject(fIndex);
						/*Modified by Preeti for TJN252-80 starts*/
						/*String label = field.getString("FLD_LABEL_2");*/
						String label = field.getString("FLD_LABEL");
						/*Modified by Preeti for TJN252-80 ends*/
						String expectedFieldValue = field.getString("DATA");
						Node fieldNode = getNode(targetNode, label);
						
						if(!"@@OUTPUT".equalsIgnoreCase(expectedFieldValue)) {
							if(fieldNode == null) {
								ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, "Could not locate this field in response XML.", detailId);
								results.add(r);
								continue;
							}
							
							String actualFieldValue = fieldNode.getTextContent();
							if(actualFieldValue == null) {
								actualFieldValue = "";
							}
							
							logger.debug("Field [{}], Expected [{}], Actual [{}]", label, expectedFieldValue, actualFieldValue);
							
							ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, actualFieldValue, detailId);
							results.add(r);
						}else{
							
							if(fieldNode != null) {
								String actualFieldValue = fieldNode.getTextContent();
								if(actualFieldValue == null) {
									actualFieldValue = "";
								}
								RuntimeFieldValue rfv = this.getRuntimeFieldValueObject(label, actualFieldValue,  tdUid);
								runtimeValues.add(rfv);
							}
							
						}
						
					}
					
				}
			}
		} catch (JSONException e) {
			
			logger.error("ERROR - JSONException caught", e);
			/*return results;*/
		}
		
		resultMap.put("RUNTIME_VALUES", runtimeValues);
		resultMap.put("VALIDATIONS", results);
		return resultMap;
	}
	
	protected RuntimeFieldValue getRuntimeFieldValueObject(String field, String value, String tdUid){
		RuntimeFieldValue r = new RuntimeFieldValue();

		r.setDetailRecordNo(1);
		r.setField(field);
		r.setIteration(this.iteration);
		r.setRunId(this.runId);
		r.setTdGid(this.step.getTdGid());
		r.setTdUid(tdUid);
		r.setTestStepRecordId(step.getRecordId());
		r.setValue(value);

		logger.info(r.toString());
		return r;
	}
}
