package com.ycs.tenjin.adapter.api.soap.fcubs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ycs.tenjin.adapter.api.soap.generic.GenericSOAPUtils;
import com.ycs.tenjin.adapter.api.soap.generic.SOAPApiBridgeImpl;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.api.ApiApplication;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.util.Utilities;

public class FCUBSSoapAPIBridgeImpl extends SOAPApiBridgeImpl implements ApiApplication {

	private static final Logger logger = LoggerFactory.getLogger(FCUBSSoapAPIBridgeImpl.class);
	
	public FCUBSSoapAPIBridgeImpl() {
		
		super();
	}
	
	public FCUBSSoapAPIBridgeImpl(int runId, ExecutionStep step) {
		super(runId, step);
	}
	
	@Override
	protected void getPostExecutionResult(IterationStatus iStatus, String responseXML) throws BridgeException {
		
		try {
			
			Document document = GenericSOAPUtils.loadDocument(responseXML, true);
			
			logger.debug("Getting FCUBS_HEADER node");
			Node headerNode = document.getDocumentElement().getElementsByTagNameNS("*", "FCUBS_HEADER").item(0);
			
			Element headerElement = (Element) headerNode;
			
			Node msgStatNode = headerElement.getElementsByTagNameNS("*", "MSGSTAT").item(0);
			String msgStat = Utilities.trim(msgStatNode.getTextContent());
			
			if(msgStat.equalsIgnoreCase("FAILURE")){
				iStatus.setResult("F");
				
				Element bodyElement = (Element) document.getDocumentElement().getElementsByTagNameNS("*", "FCUBS_BODY").item(0);
				Element errorResponseElement = (Element) bodyElement.getElementsByTagNameNS("*", "FCUBS_ERROR_RESP").item(0);
				NodeList errorNodes = errorResponseElement.getElementsByTagNameNS("*", "ERROR");
				String errorCode = "";
				String error = "";
				for(int i=0; i<errorNodes.getLength(); i++) {
					Node errorNode = errorNodes.item(i);
					
					
					NodeList errorChildren = errorNode.getChildNodes();
					for(int j=0; j<errorChildren.getLength(); j++) {
						Node eNode = errorChildren.item(j);
						if(GenericSOAPUtils.getNodeNameWithoutPrefix(eNode.getNodeName()).equalsIgnoreCase("ECODE")) {
							errorCode = Utilities.trim(eNode.getTextContent());
						}else if(GenericSOAPUtils.getNodeNameWithoutPrefix(eNode.getNodeName()).equalsIgnoreCase("EDESC")){
							error = Utilities.trim(eNode.getTextContent());
						}
					}
					
					RuntimeFieldValue rfv = this.getRuntimeFieldValueObject("WSERROR ("+ (i+1) +")", errorCode + " - " + error,1, this.tdUid);
					iStatus.getRuntimeFieldValues().add(rfv);
				}
				
				iStatus.setMessage(errorCode + " - " + error);
				
			}else {
				iStatus.setResult("S");
				
				Element bodyElement = (Element) document.getDocumentElement().getElementsByTagNameNS("*", "FCUBS_BODY").item(0);
				Element warningResponseElement = (Element) bodyElement.getElementsByTagNameNS("*", "FCUBS_WARNING_RESP").item(0);
				NodeList warningNodes = warningResponseElement.getElementsByTagNameNS("*", "WARNING");
				String wCode = "";
				String wDesc = "";
				for(int i=0; i<warningNodes.getLength(); i++) {
					Node warningNode = warningNodes.item(i);
					
					
					NodeList warningChildren = warningNode.getChildNodes();
					for(int j=0; j<warningChildren.getLength(); j++) {
						Node eNode = warningChildren.item(j);
						if(GenericSOAPUtils.getNodeNameWithoutPrefix(eNode.getNodeName()).equalsIgnoreCase("WCODE")) {
							wCode = Utilities.trim(eNode.getTextContent());
						}else if(GenericSOAPUtils.getNodeNameWithoutPrefix(eNode.getNodeName()).equalsIgnoreCase("WDESC")){
							wDesc = Utilities.trim(eNode.getTextContent());
						}
					}
					
					RuntimeFieldValue rfv = this.getRuntimeFieldValueObject("WS MESSAGE ("+ (i+1) +")", wCode + " - " + wDesc,1, this.tdUid);
					iStatus.getRuntimeFieldValues().add(rfv);
				}
				
				iStatus.setMessage(wCode + " - " + wDesc);
			}
			
			
		} catch(Exception e ) {
			logger.error("ERROR in getPostExecutionResult", e);
			iStatus.setResult("E");
			iStatus.setMessage("Could not determine the result of this transaction due to an internal error. Please contact Tenjin Support.");
		}
	}

}
