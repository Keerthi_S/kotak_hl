/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GenericSOAPUtils.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 25-01-2019			Preeti					TJN252-80
 * 10-11-2021			Prem					VAPT FIX
 */

package com.ycs.tenjin.adapter.api.soap.generic;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.eviware.soapui.impl.wsdl.WsdlInterface;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.support.wsdl.WsdlImporter;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.util.Utilities;

public class GenericSOAPUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(GenericSOAPUtils.class);
	
	@SuppressWarnings("unused")
	private String tdUid;

	private Document requestDocument;
	private Document baselineDocument;
	
	
	
	public static Document getNewXMLDocument(boolean namespaceAware) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			docFactory.setNamespaceAware(namespaceAware);
			
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			return doc;
		} catch(Exception e) {
			logger.error("ERROR creating new document", e);
			return null;
		}
	}
	
	public static Document loadDocument(File xmlFile, boolean namespaceAware) {
		
		if(xmlFile == null) {
			logger.error("ERROR loading document - input file is null");
			return null;
		}
		
		logger.debug("Loading XML Document from path {}", xmlFile.getAbsolutePath());
		
		try {
			//DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			/*Added by Prem for VAPT fix starts*/
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl", ClassLoader.getSystemClassLoader());
			//factory.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, "true");
			factory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
			factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			/*Added by Prem for VAPT fix ends*/
			factory.setNamespaceAware(true);
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			return builder.parse(new FileInputStream(xmlFile));
		} catch (FileNotFoundException e) {
			
			logger.error("ERROR loading XML document from file at path {}", xmlFile.getAbsolutePath(), e);
			return null;
		} catch (ParserConfigurationException e) {
			
			logger.error("ERROR parsing document", e);
			return null;
		} catch (SAXException e) {
			
			logger.error("ERROR parsing document", e);
			return null;
		} catch (IOException e) {
			
			logger.error("ERROR loading XML document from file at path {}", xmlFile.getAbsolutePath(), e);
			return null;
		}
	}
	
	public static Document loadDocument(String xmlString, boolean namespaceAware) {
		
		if(xmlString == null) {
			logger.error("ERROR loading document - input xmlString is null");
			return null;
		}
		
		logger.debug("Loading XML Document from String {}", xmlString);
		
		try {
			//DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl", ClassLoader.getSystemClassLoader());
			/*Added by prem for VAPT fix starts*/
			factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		//	factory.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, "true");
			factory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
			/*Added by prem for VAPT fix ends*/
			factory.setNamespaceAware(true);
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			return builder.parse(new InputSource(new StringReader(xmlString)));
		} catch (FileNotFoundException e) {
			
			logger.error("ERROR loading XML document from string {}", xmlString, e);
			return null;
		} catch (ParserConfigurationException e) {
			
			logger.error("ERROR parsing document", e);
			return null;
		} catch (SAXException e) {
			
			logger.error("ERROR parsing document", e);
			return null;
		} catch (IOException e) {
			
			logger.error("ERROR loading XML document from string {}", xmlString, e);
			return null;
		}
	}
	
	
	public static boolean hasChildNodes(Node node) {
		boolean hasChildNodes = false;

		NodeList childNodes = node.getChildNodes();
		for(int i=0; i< childNodes.getLength(); i++ ) {
			Node childNode = childNodes.item(i);
			if(childNode.getNodeType() == Node.ELEMENT_NODE) {
				hasChildNodes = true;
				break;
			}
		}

		return hasChildNodes;
	}
	
	public static String getPath(Node node) {
		
		boolean reachedTop = false;
		String path = "";
		List<String> paths = new ArrayList<String>();
		try{
			while(!reachedTop) {
				Node parent = node.getParentNode();
				if(parent != null) {
					paths.add(node.getNodeName());
					node = parent;
				}else  {
					reachedTop = true;
				}
			}
		} catch(Exception e) {
			logger.error("ERROR", e);
		}
		
		for(int i= (paths.size()-1); i>=0; i--) {
			path = path + paths.get(i);
			if(i > 0) {
				path = path  + "-->";
			}
		}
		
		return path;
	}
	
	
	public static Document getSoapXMLTemplateDocument(String baselineRequestXML) {
		Document document = loadDocument(baselineRequestXML, true);
		
		Document newDoc = getNewXMLDocument(true);
		
		Element rootElement = document.getDocumentElement();
		Element newRoot = newDoc.createElementNS(rootElement.getNamespaceURI(), rootElement.getLocalName());
		newDoc.appendChild(newRoot);
		
		Element oHeader = (Element) document.getElementsByTagNameNS(rootElement.getNamespaceURI(), "Header").item(0);
		Element oBody = (Element) document.getElementsByTagNameNS(rootElement.getNamespaceURI(), "Body").item(0);
		
		Element nHeader = newDoc.createElementNS(rootElement.getNamespaceURI(), oHeader.getLocalName());
		newRoot.appendChild(nHeader);
		
		Element nBody = newDoc.createElementNS(rootElement.getNamespaceURI(), oBody.getLocalName());
		newRoot.appendChild(nBody);
		
		return newDoc;
		
	}
	
	public static Document getSoapXMLTemplateDocument(File baselineRequestXML) {
		Document document = loadDocument(baselineRequestXML, true);
		
		Document newDoc = getNewXMLDocument(true);
		
		Element rootElement = document.getDocumentElement();
		Element newRoot = newDoc.createElementNS(rootElement.getNamespaceURI(), rootElement.getLocalName());
		newDoc.appendChild(newRoot);
		
		Element oHeader = (Element) document.getElementsByTagNameNS(rootElement.getNamespaceURI(), "Header").item(0);
		Element oBody = (Element) document.getElementsByTagNameNS(rootElement.getNamespaceURI(), "Body").item(0);
		
		Element nHeader = newDoc.createElementNS(rootElement.getNamespaceURI(), oHeader.getLocalName());
		newRoot.appendChild(nHeader);
		
		Element nBody = newDoc.createElementNS(rootElement.getNamespaceURI(), oBody.getLocalName());
		newRoot.appendChild(nBody);
		
		return newDoc;
		
	}
	
	
	
	public String buildXMLRequest(JSONObject jsonData, File baselineRequestXML) {
		
		/*this.requestXML = "";
		this.requestDocument = getSoapXMLTemplateDocument(baselineRequestXML);
		this.baselineDocument = loadDocument(baselineRequestXML, true);
		
		try {
			JSONArray pageAreas = jsonData.getJSONArray("PAGEAREAS");
			logger.info("This test has data across {} page areas", pageAreas.length());
			
			
			
			this.tdUid = jsonData.getString("TDUID");
		} catch(JSONException e) {
			
		}
		
		
		return requestXML;*/
		
		this.requestDocument = getSoapXMLTemplateDocument(baselineRequestXML);
		this.baselineDocument = loadDocument(baselineRequestXML, true);
		
		try {
			JSONArray pageAreas = jsonData.getJSONArray("PAGEAREAS");
			logger.info("This test has data across {} page areas", pageAreas.length());
			
			for(int i=0; i<pageAreas.length(); i++) {
				this.scanPageArea(pageAreas.getJSONObject(i));
			}
			
			
			this.tdUid = jsonData.getString("TDUID");
		} catch(JSONException e) {
			
		}
		
		
		return this.getStringFromDocument(this.requestDocument);
	}
	
	public String buildXMLRequest(String tdUid, JSONArray pageAreas, String baselineRequestXML) {
		
		this.requestDocument = getSoapXMLTemplateDocument(baselineRequestXML);
		this.baselineDocument = loadDocument(baselineRequestXML, true);
		this.tdUid =tdUid;
		
		try {
			logger.info("This test has data across {} page areas", pageAreas.length());
			
			for(int i=0; i<pageAreas.length(); i++) {
				this.scanPageArea(pageAreas.getJSONObject(i));
			}
		} catch(JSONException e) {
			
		}
		
		
		return this.getStringFromDocument(this.requestDocument);
	}
	
	
	
	
	private void scanPageArea(JSONObject pageArea) {
		
		try {
			JSONArray detailRecords = pageArea.getJSONArray("DETAILRECORDS");
			String pageName = pageArea.getString("PA_NAME");
			String pagePath = pageArea.getString("PA_PATH");
			
			
			if(detailRecords.length() > 0) {
				
				
				for(int d=0; d < detailRecords.length(); d++ ){
					
					if("child".equalsIgnoreCase(pageName)) {
						System.err.println();
					}
					
					Node pageNode;
					if(detailRecords.length() > 1){
						pageNode = this.createPageNode(pageName, pagePath, true);
					}else{
						pageNode = this.createPageNode(pageName, pagePath, false);
					}
					
					JSONObject detailRecord = detailRecords.getJSONObject(d);
					JSONArray fields = detailRecord.getJSONArray("FIELDS");
					
					for(int f=0; f< fields.length(); f++) {
						JSONObject field = fields.getJSONObject(f);
						/*Modified by Preeti for TJN252-80 starts*/
						/*String label = field.getString("FLD_LABEL_2");*/
						String label = field.getString("FLD_LABEL");
						/*Modified by Preeti for TJN252-80 ends*/
						String data = field.getString("DATA");
						
						if(!Utilities.trim(data).equalsIgnoreCase("@@OUTPUT")) {
							this.createFieldNode(pageNode, label, data);
						}
					}
				}
				
			}else{
				logger.warn("No data found for page [{}]", pageName);
			}
		} catch (JSONException e) {
			
			logger.error("JSONException occurred", e);
		}
		
	}
	
	
	private void createFieldNode(Node pageNode, String nodeName, String nodeValue) {
		this.createNode(pageNode, nodeName, pageNode.getNamespaceURI(),nodeValue);
	}
	
	private Node getNodeFromBaselineDocument(String nodeName, String nodePath) {
		if(Utilities.trim(nodePath).equalsIgnoreCase("")) {
			return null;
		}
		
		String[] pathArray = nodePath.split("-->");
		
		Node targetNode = (Node) this.baselineDocument.getDocumentElement();
		for(int i=0; i<pathArray.length; i++) {
			
			if( i== 0) {
				if(this.isRootNodeValid(this.getNodeNameWithoutNamespace(pathArray[i]))) {
					continue;
				}else{
					logger.error("Root Node is invalid");
					return null;
				}
			}
			
			String nodeLocalName = this.getNodeNameWithoutNamespace(pathArray[i]);
			targetNode = this.getNode(targetNode, nodeLocalName);
		}
		
		return targetNode;
	}
	
	public Node getNode(Document document, String nodeName, String nodePath) {
		if(Utilities.trim(nodePath).equalsIgnoreCase("")) {
			return null;
		}
		
		String[] pathArray = nodePath.split("-->");
		
		Node targetNode = (Node) document.getDocumentElement();
		for(int i=0; i<pathArray.length; i++) {
			
			if( i== 0) {
				if(this.isRootNodeValid(document, this.getNodeNameWithoutNamespace(pathArray[i]))) {
					continue;
				}else{
					logger.error("Root Node is invalid");
					return null;
				}
			}
			
			String nodeLocalName = this.getNodeNameWithoutNamespace(pathArray[i]);
			targetNode = this.getNode(targetNode, nodeLocalName);
		}
		
		return targetNode;
	}
	
	private Node createPageNode(String nodeName, String nodePath, boolean ignoreExisting) {
		
		if(Utilities.trim(nodePath).equalsIgnoreCase("")) {
			return null;
		}
		
		logger.debug("Loading node from baseline document for page [{}]", nodeName);
		Node pageNode = this.getNodeFromBaselineDocument(nodeName, nodePath);
		
		String[] pathArray = nodePath.split("-->");

		Node targetNode = (Node) this.requestDocument.getDocumentElement();
		for(int i=0; i<pathArray.length; i++) {
			
			if( i== 0) {
				if(this.isRootNodeValid(this.getNodeNameWithoutNamespace(pathArray[i]))) {
					continue;
				}else{
					logger.error("Root Node is invalid");
					return null;
				}
			}
			
			String nodeLocalName = this.getNodeNameWithoutNamespace(pathArray[i]);
			if(ignoreExisting) {
				if(i == (pathArray.length -1)){
					targetNode = this.createNode(targetNode, nodeLocalName, pageNode.getNamespaceURI(), true);
				}else{
					targetNode = this.createNode(targetNode, nodeLocalName, pageNode.getNamespaceURI(), false);
				}
			}else{
				targetNode = this.createNode(targetNode, nodeLocalName, pageNode.getNamespaceURI(), false);
			}
		}
		
		return targetNode;
		
	}
	
	private Node getNode(Node parentNode, String childNodeName) {
		String childNodeLocalName = childNodeName;
		
		NodeList nList =((Element) parentNode).getElementsByTagNameNS("*", childNodeLocalName);
		
		if(nList.getLength() > 0) {
			return nList.item(0);
		} else{
			return null;
		}
	}
	
	private String getNodeNameWithoutNamespace(String name) {
		if(Utilities.trim(name).equalsIgnoreCase("")) {
			return "";
		}
		
		
		String[] array = name.split(":");
		
		if(array.length > 1) {
			return array[1];
		}else {
			return array[0];
		}
	}
	
	public static String getNodeNameWithoutPrefix(String name) {
		if(Utilities.trim(name).equalsIgnoreCase("")) {
			return "";
		}
		
		
		String[] array = name.split(":");
		
		if(array.length > 1) {
			return array[1];
		}else {
			return array[0];
		}
	}
	
	private boolean isRootNodeValid(String rootNodeLocalName) {
		try {
			
			Element rootElement = this.baselineDocument.getDocumentElement();
			if(rootNodeLocalName.equalsIgnoreCase(rootElement.getLocalName())) {
				return true;
			}else {
				return false;
			}
			
		} catch(Exception e) {
			logger.error("ERROR occurred while validating baseline root node name", e);
			return false;
		}
	}
	
	private boolean isRootNodeValid(Document document, String rootNodeLocalName) {
		try {
			
			Element rootElement = document.getDocumentElement();
			if(rootNodeLocalName.equalsIgnoreCase(rootElement.getLocalName())) {
				return true;
			}else {
				return false;
			}
			
		} catch(Exception e) {
			logger.error("ERROR occurred while validating baseline root node name", e);
			return false;
		}
	}
	
	public String getStringFromDocument(Document doc)
	{
		try {
			DOMSource domSource = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			/*Added by prem for VAPT fix starts*/
			tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
			/*Added by prem for VAPT fix ends*/
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			return writer.toString();
		} catch (Exception e) {
			logger.error("ERROR in getStringFromDocument(doc)", e);
			return "";
		}
	}
	
	
	public Node createNode(Node parentNode, String childNodeLocalName, String namespaceURI, boolean ignoreExisting) {
		String parentNodeLocalName = parentNode.getLocalName();
		NodeList children = parentNode.getChildNodes();
		Node targetNode = null;
		boolean nodeFound = false;
		
		if(ignoreExisting) {
			Element nElement ;
			if(!Utilities.trim(namespaceURI).equalsIgnoreCase("")) {
				nElement = this.requestDocument.createElementNS(namespaceURI, childNodeLocalName);
			}else{
				nElement = this.requestDocument.createElement(childNodeLocalName);
			}
			parentNode.appendChild(nElement);
			targetNode = (Node) nElement;
		}else{
			for(int i=0; i<children.getLength(); i++ ) {
				Node child = children.item(i);
				
				if(this.getNodeNameWithoutNamespace(child.getNodeName()).equalsIgnoreCase(childNodeLocalName)) {
					logger.debug("Node [{}] exists under parent [{}]", childNodeLocalName, parentNodeLocalName);
					nodeFound = true;
					targetNode = child;
					break;
				}
			}
			
			if(!nodeFound) {
				Element nElement ;
				if(!Utilities.trim(namespaceURI).equalsIgnoreCase("")) {
					nElement = this.requestDocument.createElementNS(namespaceURI, childNodeLocalName);
				}else{
					nElement = this.requestDocument.createElement(childNodeLocalName);
				}
				parentNode.appendChild(nElement);
				targetNode = (Node) nElement;
			}
		}
		
		return targetNode;
		
	}
	
	public Node createNode(Node parentNode, String childNodeLocalName, String namespaceURI, String textContent) {
		String parentNodeLocalName = parentNode.getLocalName();
		NodeList children = parentNode.getChildNodes();
		Node targetNode = null;
		boolean nodeFound = false;
		
		for(int i=0; i<children.getLength(); i++ ) {
			Node child = children.item(i);
			
			if(this.getNodeNameWithoutNamespace(child.getNodeName()).equalsIgnoreCase(childNodeLocalName)) {
				logger.debug("Node [{}] exists under parent [{}]", parentNodeLocalName);
				nodeFound = true;
				targetNode = child;
				break;
			}
		}
		
		if(!nodeFound) {
			Element nElement ;
			if(!Utilities.trim(namespaceURI).equalsIgnoreCase("")) {
				nElement = this.requestDocument.createElementNS(namespaceURI, childNodeLocalName);
			}else{
				nElement = this.requestDocument.createElement(childNodeLocalName);
			}
			parentNode.appendChild(nElement);
			targetNode = (Node) nElement;
		}
		
		if(targetNode != null) {
			targetNode.setTextContent(textContent);
		}
		
		return targetNode;
		
	}
	
	public static String getRequestXml(String url, String operation) throws BridgeException {
		try {
			WsdlProject project = new WsdlProject();
			WsdlInterface[] wsdls = WsdlImporter.importWsdl(project,url);
			WsdlInterface wsdl = wsdls[0];
			String reqSoapStr = wsdl.getOperationByName(operation).createRequest(true);
			return reqSoapStr;
		} catch (Exception e) {
			
			logger.error("ERROR getting Request XML for URL [{}], Operation [{}]", url, operation, e);
			throw new BridgeException("Could not get Request Descriptor for the selected operation. Please contact Tenjin Support.");
		}
	}
	
	public static String getResponseXml(String url, String operation) throws BridgeException {
		try {
			WsdlProject project = new WsdlProject();
			WsdlInterface[] wsdls = WsdlImporter.importWsdl(project,url);
			WsdlInterface wsdl = wsdls[0];
			String reqSoapStr = wsdl.getOperationByName(operation).createResponse(true);
			return reqSoapStr;
		} catch (Exception e) {
			
			logger.error("ERROR getting Response XML for URL [{}], Operation [{}]", url, operation, e);
			throw new BridgeException("Could not get Response Descriptor for the selected operation. Please contact Tenjin Support.");
		}
	}
	
	
	public Map<String, String> sendSOAPRequest(String wsUrl, String requestXML) throws BridgeException {
		
		Map<String, String> resultMap = new HashMap<String, String>();
		
		if(Utilities.trim(wsUrl).equalsIgnoreCase("")) {
			throw new BridgeException("Invalid API URL. Please verify your API Information.");
		}
		
		if(Utilities.trim(requestXML).equalsIgnoreCase("")) {
			throw new BridgeException("Invalid Request XML. Please verify your test data.");
		}
		
		try{
			logger.debug("Building SOAP Message");
			InputStream is = new ByteArrayInputStream(requestXML.getBytes());
			SOAPMessage soapMessage = MessageFactory.newInstance().createMessage(null, is);
			ByteArrayOutputStream rq = new ByteArrayOutputStream();
			soapMessage.writeTo(rq);
			
			logger.debug("Creating SOAP Connection");
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			
			
			logger.debug("Calling SOAP Service at URL [{}]", wsUrl);
			logger.debug("Sending request [{}]", requestXML);
			SOAPMessage soapResponse = soapConnection.call(soapMessage, wsUrl);
			logger.debug("Request Sent...");
			
			ByteArrayOutputStream rs = new ByteArrayOutputStream();
			soapResponse.writeTo(rs);		
			String resMsg = new String(rs.toByteArray());
			
			logger.debug("Got SOAP Response --> [{}]", resMsg);
			
			SOAPBody soapBody = soapResponse.getSOAPBody();
			SOAPFault fault = soapBody.getFault();
			
			if(fault != null) {
				logger.error("SOAP Fault Found");
				logger.error("Fault Code --> [{}]", fault.getFaultCode());
				logger.error("Fault String --> [{}]", fault.getFaultString());
				resultMap.put("status", TenjinConstants.ACTION_RESULT_ERROR);
				resultMap.put("message", fault.getFaultCode() + " - " + fault.getFaultString());
			} else  {
				logger.debug("No Faults!");
				resultMap.put("status", TenjinConstants.ACTION_RESULT_SUCCESS);
				resultMap.put("message", "");
			}
			
			resultMap.put("response", resMsg);
			
		} catch(SOAPException e) {
			
			logger.error("SOAPException occurred while invoking API", e);
			throw new BridgeException("Could not send SOAP Request due to an unexpected error. Please contact Tenjin support.");
			
		} catch (IOException e) {
			
			logger.error("IOException occurred while invoking API", e);
			throw new BridgeException("Could not send SOAP Request due to an unexpected error. Please contact Tenjin support.");
		}
		
		return resultMap;
	}
	
	//TODO Temporary code. Consider removing.
	
	
}
