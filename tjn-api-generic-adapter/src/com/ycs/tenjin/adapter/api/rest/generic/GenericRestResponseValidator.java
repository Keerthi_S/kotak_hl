/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GenericRestResponseValidator.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 03-Nov-2017		Sriram Sridharan		Fixes for TENJINCG-414
 * 27-Nov-2017		Sriram Sridharan		For TENJINCG-514
 * 16-01-2018				Sriarm				TENJINCG-416
 * 25-01-2019			Preeti					TJN252-80
 */

package com.ycs.tenjin.adapter.api.rest.generic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ycs.tenjin.adapter.api.soap.generic.GenericSOAPUtils;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.oem.api.generic.ApiResponseValidator;
import com.ycs.tenjin.bridge.pojo.aut.Representation;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.util.Utilities;

public class GenericRestResponseValidator implements ApiResponseValidator {

	private static final Logger logger = LoggerFactory.getLogger(GenericRestResponseValidator.class);

	private int runId;
	private ExecutionStep step;
	private int iteration;
	
	private JsonObject actualResponseJson;
	
	public GenericRestResponseValidator(int runId, ExecutionStep step, int iteration) {
		this.runId = runId;
		this.step = step;
		this.iteration = iteration;
	}
	
	
	@Override
	public Map<String, Object> validateResponse(JSONObject expectedResponseData, String actualResponseDescriptor) {
		
		
		
		Representation responseRepresentation = null;
		/*commented by Leelaprasad for the requirement need to change this as 405 406 407 408 */
		/*if(step.getApiOperation() != null && step.getApiOperation().getResponseRepresentation() != null) {
			responseRepresentation = step.getApiOperation().getResponseRepresentation();
		}*/
		if(step.getApiOperation() != null && !step.getApiOperation().getListResponseRepresentation().isEmpty()){
			responseRepresentation= new ApiHelper().hydrateRepresentationforResponseType(step.getAppId(),step.getApiCode(),step.getApiOperation().getName(),"RESPONSE",step.getResponseType());
		}
		/*commented by Leelaprasad for the requirement need to change this as 405 406 407 408 */
		if(responseRepresentation == null) {
			logger.warn("Response representation is not set. Validation cannot continue");
			return null;
		}
		
		if(Utilities.trim(responseRepresentation.getMediaType()).equalsIgnoreCase("")) {
			logger.warn("Response media type is not set. Assuming it to be application/json");
			responseRepresentation.setMediaType("application/json");
		}
		
		logger.debug("Response Media Type --> {}", responseRepresentation.getMediaType());
		
		if("application/json".equalsIgnoreCase(responseRepresentation.getMediaType())){
			return this.validateJsonResponse(expectedResponseData.toString(), actualResponseDescriptor);
		}if("application/xml".equalsIgnoreCase(responseRepresentation.getMediaType())) {
			return this.validateXmlResponse(expectedResponseData, actualResponseDescriptor);
		}else{
			logger.error("Media Type [{}] is not supported yet.", responseRepresentation.getMediaType());
			return null;
		}
		
		
	}
	private Map<String, Object> validateXmlResponse(JSONObject expectedResponseData, String actualResponseDescriptor) {
		
		Map<String, Object> resultMap = new HashMap<String, Object>();

		List<ValidationResult> results = new ArrayList<ValidationResult>();
		List<RuntimeFieldValue> runtimeValues = new ArrayList<RuntimeFieldValue>();

		GenericSOAPUtils soapUtils = new GenericSOAPUtils();

		Document document = null;
		Element rootElement = null;

		logger.info("Loading Response XML");
		try {
			document = GenericSOAPUtils.loadDocument(actualResponseDescriptor, true);
		} catch (LearnerException e) {
			logger.error("Could not load Response XML", e);
			return resultMap;
		}

		if (document == null) {
			logger.error("ERROR --> Empty document loaded from Response XML");
			logger.error("Response XML is invalid --> {}", actualResponseDescriptor);
			return null;
		}

		try {
			rootElement = document.getDocumentElement();
			JSONArray pageAreas = expectedResponseData.getJSONArray("PAGEAREAS");
			String tdUid = expectedResponseData.getString("TDUID");
			for (int i = 0; i < pageAreas.length(); i++) {
				JSONObject pageArea = pageAreas.getJSONObject(i);

				JSONArray detailRecords = pageArea.getJSONArray("DETAILRECORDS");
				for (int dIndex = 0; dIndex < detailRecords.length(); dIndex++) {
					JSONObject detailRecord = detailRecords.getJSONObject(dIndex);
					String detailId = detailRecord.getString("DR_ID");
					logger.debug("Validating detail ID [{}]", detailId);
					JSONArray fields = detailRecord.getJSONArray("FIELDS");

					for (int fIndex = 0; fIndex < fields.length(); fIndex++) {
						JSONObject field = fields.getJSONObject(fIndex);
						/* Modified by Preeti for TJN252-80 starts */
						/* String label = field.getString("FLD_LABEL_2"); */
						String label = field.getString("FLD_LABEL");
						/* Modified by Preeti for TJN252-80 ends */
						String expectedFieldValue = field.getString("DATA");
						String pageName = field.getString("FLD_PAGE_AREA");
						String path = field.getString("FLD_PARENT_PATH");

						logger.debug("Validating Page [{}]", pageName);

						Node targetNode = soapUtils.getNode(document, pageName, path);

						if (targetNode == null) {
							logger.error("ERROR --> Could not find page [{}] in response XML.", pageName);
							ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue,
									"Could not find page " + pageName + "in response XML.", detailId);
							results.add(r);
							continue;
						}
						Node fieldNode = getNode(targetNode, label);

						if (!"@@OUTPUT".equalsIgnoreCase(expectedFieldValue)) {
							if (fieldNode == null) {
								ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue,
										"Could not locate this field in response XML.", detailId);
								results.add(r);
								continue;
							}

							String actualFieldValue = fieldNode.getTextContent();
							if (actualFieldValue == null || actualFieldValue.length()>255) {
								actualFieldValue = "";
							}

							logger.debug("Field [{}], Expected [{}], Actual [{}]", label, expectedFieldValue,
									actualFieldValue);

							ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue,
									actualFieldValue, detailId);
							results.add(r);
						} else {

							if (fieldNode != null) {
								String actualFieldValue = fieldNode.getTextContent();
								if (actualFieldValue == null) {
									actualFieldValue = "";
								}
								RuntimeFieldValue rfv = this.getRuntimeFieldValueObject(label, actualFieldValue, tdUid);
								runtimeValues.add(rfv);
							}

						}
					}
				}
			}
		} catch (JSONException e) {

			logger.error("ERROR - JSONException caught", e);
			/* return results; */
		}

		resultMap.put("RUNTIME_VALUES", runtimeValues);
		resultMap.put("VALIDATIONS", results);
		return resultMap;

		
		/*Map<String, Object> resultMap = new HashMap<String, Object>();
		
		List<ValidationResult> results = new ArrayList<ValidationResult>();
		List<RuntimeFieldValue> runtimeValues = new ArrayList<RuntimeFieldValue>();
		
		
		GenericSOAPUtils soapUtils = new GenericSOAPUtils();
		
		Document document = null;
		Element rootElement = null;
		
		logger.info("Loading Response XML");
		try{
			document = GenericSOAPUtils.loadDocument(actualResponseDescriptor, true);
		} catch(LearnerException e) {
			logger.error("Could not load Response XML", e);
			return resultMap;
		}
		
		if(document == null) {
			logger.error("ERROR --> Empty document loaded from Response XML");
			logger.error("Response XML is invalid --> {}", actualResponseDescriptor);
			return null;
		}
		
		try {
			rootElement = document.getDocumentElement();
			JSONArray pageAreas = expectedResponseData.getJSONArray("PAGEAREAS");
			String tdUid = expectedResponseData.getString("TDUID");
			for(int i=0; i<pageAreas.length(); i++) {
				JSONObject pageArea = pageAreas.getJSONObject(i);
				
				String pageName = pageArea.getString("PA_NAME");
				String path = pageArea.getString("PA_PATH");
				
				logger.debug("Validating Page [{}]", pageName);
				
				Node targetNode = soapUtils.getNode(document, pageName, path);
				
				if(targetNode == null) {
					logger.error("ERROR --> Could not find page [{}] in response XML.", pageName);
					continue;
				}
				
				JSONArray detailRecords = pageArea.getJSONArray("DETAILRECORDS");
				for(int dIndex = 0; dIndex < detailRecords.length(); dIndex ++) {
					JSONObject detailRecord = detailRecords.getJSONObject(dIndex);
					String detailId = detailRecord.getString("DR_ID");
					logger.debug("Validating detail ID [{}]", detailId);
					JSONArray fields = detailRecord.getJSONArray("FIELDS");
					
					for(int fIndex=0; fIndex < fields.length(); fIndex++) {
						JSONObject field = fields.getJSONObject(fIndex);
						/*Modified by Preeti for TJN252-80 starts*/
						/*String label = field.getString("FLD_LABEL_2");*/
						//String label = field.getString("FLD_LABEL");
						/*Modified by Preeti for TJN252-80 ends
						String expectedFieldValue = field.getString("DATA");
						Node fieldNode = getNode(targetNode, label);
						
						if(!"@@OUTPUT".equalsIgnoreCase(expectedFieldValue)) {
							if(fieldNode == null) {
								ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, "Could not locate this field in response XML.", detailId);
								results.add(r);
								continue;
							}
							
							String actualFieldValue = fieldNode.getTextContent();
							if(actualFieldValue == null) {
								actualFieldValue = "";
							}
							
							logger.debug("Field [{}], Expected [{}], Actual [{}]", label, expectedFieldValue, actualFieldValue);
							
							ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, actualFieldValue, detailId);
							results.add(r);
						}else{
							
							if(fieldNode != null) {
								String actualFieldValue = fieldNode.getTextContent();
								if(actualFieldValue == null) {
									actualFieldValue = "";
								}
								RuntimeFieldValue rfv = this.getRuntimeFieldValueObject(label, actualFieldValue,  tdUid);
								runtimeValues.add(rfv);
							}
							
						}
					}
					
				}
			}
		} catch (JSONException e) {
			
			logger.error("ERROR - JSONException caught", e);
			//return results;
		}
		
		resultMap.put("RUNTIME_VALUES", runtimeValues);
		resultMap.put("VALIDATIONS", results);
		return resultMap;*/
	}
	
	private Node getNode(Node parentNode, String childNodeName) {

		String childNodeLocalName = "";
		if(Utilities.trim(childNodeName).contains(":")) {
			String[] split = childNodeName.split(":");
			childNodeLocalName = split[1];
		}else{
			childNodeLocalName = childNodeName;
		}
		
		NodeList nList =((Element) parentNode).getElementsByTagNameNS("*", childNodeLocalName);
		
		if(nList.getLength() > 0) {
			return nList.item(0);
		} else{
			return null;
		}
	
	}
	/* TENJINCG-414 */
	/*private Map<String, Object> validateJsonResponse(String expectedResponseData, String actualResponseDescriptor) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		List<ValidationResult> results = new ArrayList<ValidationResult>();
		List<RuntimeFieldValue> runtimeValues = new ArrayList<RuntimeFieldValue>();
		
		try{
			JsonParser parser = new JsonParser();
			JsonObject eResponseData = parser.parse(expectedResponseData).getAsJsonObject();
			
			JsonObject responseJson = parser.parse(actualResponseDescriptor).getAsJsonObject();
			this.actualResponseJson = new JsonObject();
			this.actualResponseJson.add("Root", responseJson);
			
			String tdUid = eResponseData.get("TDUID").getAsString();
			JsonArray pageAreas = eResponseData.getAsJsonArray("PAGEAREAS");
			for(JsonElement pageAreaElement : pageAreas) {
				JsonObject pageArea = pageAreaElement.getAsJsonObject();
				
				String pageName = pageArea.get("PA_NAME").getAsString();
				String path = pageArea.get("PA_PATH").getAsString();
				String isMrb = pageArea.get("PA_IS_MRB").getAsString();
				
				logger.debug("Validating Page [{}]", pageName);
				
				JsonElement elementAtPath = this.getElementAtPath(path);
				
				if(elementAtPath == null) {
					logger.error("ERROR --> Could not find page [{}] in response JSON.", pageName);
					continue;
				}
				
				JsonArray detailRecords = pageArea.get("DETAILRECORDS").getAsJsonArray();
				
				if("N".equalsIgnoreCase(isMrb)) {
					if(detailRecords != null && detailRecords.size() > 0) {
						JsonElement detailRecordElement = detailRecords.get(0);
						JsonObject fieldNode = elementAtPath.getAsJsonObject();
						JsonObject detailRecord = detailRecordElement.getAsJsonObject();
						String detailId = detailRecord.get("DR_ID").getAsString();
						JsonArray fields = detailRecord.getAsJsonArray("FIELDS");
						for(JsonElement fieldElement : fields) {
							JsonObject field = fieldElement.getAsJsonObject();
							String label = field.get("FLD_LABEL_2").getAsString();
							String expectedFieldValue = field.get("DATA").getAsString();
							if(!"@@OUTPUT".equalsIgnoreCase(expectedFieldValue)) {
								if(fieldNode == null) {
									ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, "Could not locate this field in response XML.", detailId);
									results.add(r);
									continue;
								}
								
								String actualFieldValue = fieldNode.get(label).getAsString();
								if(actualFieldValue == null) {
									actualFieldValue = "";
								}
								
								logger.debug("Field [{}], Expected [{}], Actual [{}]", label, expectedFieldValue, actualFieldValue);
								
								ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, actualFieldValue, detailId);
								results.add(r);
							}else{
								
								if(fieldNode != null) {
									String actualFieldValue = fieldNode.get(label).getAsString();
									if(actualFieldValue == null) {
										actualFieldValue = "";
									}
									RuntimeFieldValue rfv = this.getRuntimeFieldValueObject(label, actualFieldValue,  tdUid);
									runtimeValues.add(rfv);
								}
								
							}
						}
					}
				}else{
					if(!elementAtPath.isJsonArray()) {
						logger.warn("Element at path is expected to be a JSONArray for a multi record page, but it is not. Validation cannot continue");
						continue;
					}
					
					JsonArray actualDetailRecords = elementAtPath.getAsJsonArray();
					
					if(detailRecords != null && detailRecords.size() > 0) {
						int dIndex=0;
						for(JsonElement detailRecordElement : detailRecords) {
							JsonObject detailRecord = detailRecordElement.getAsJsonObject();
							String detailId = detailRecord.get("DR_ID").getAsString();
							JsonArray fields = detailRecord.getAsJsonArray("FIELDS");
							
							JsonObject fieldNode = null;
							
							try{
								fieldNode = actualDetailRecords.get(dIndex).getAsJsonObject();
							}catch(Exception e){
								logger.error("Could not get field node from JSON For detail record {}, [{}]", dIndex, detailId);
								continue;
							}
							
							if(fieldNode == null) {
								logger.error("Could not get field node from JSON For detail record {}, [{}]", dIndex, detailId);
								continue;
							}
							
							for(JsonElement fieldElement : fields) {
								JsonObject field = fieldElement.getAsJsonObject();
								String label = field.get("FLD_LABEL_2").getAsString();
								String expectedFieldValue = field.get("DATA").getAsString();
								if(!"@@OUTPUT".equalsIgnoreCase(expectedFieldValue)) {
									if(fieldNode == null) {
										ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, "Could not locate this field in response XML.", detailId);
										results.add(r);
										continue;
									}
									
									String actualFieldValue = fieldNode.get(label).getAsString();
									if(actualFieldValue == null) {
										actualFieldValue = "";
									}
									
									logger.debug("Field [{}], Expected [{}], Actual [{}]", label, expectedFieldValue, actualFieldValue);
									
									ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, actualFieldValue, detailId);
									results.add(r);
								}else{
									
									if(fieldNode != null) {
										String actualFieldValue = fieldNode.get(label).getAsString();
										if(actualFieldValue == null) {
											actualFieldValue = "";
										}
										RuntimeFieldValue rfv = this.getRuntimeFieldValueObject(label, actualFieldValue,  tdUid);
										runtimeValues.add(rfv);
									}
									
								}
							}
						}
					}
				}
			}
		}catch(Exception e) {
			logger.error("ERROR validating JSON response", e);
		}
		resultMap.put("RUNTIME_VALUES", runtimeValues);
		resultMap.put("VALIDATIONS", results);
		return resultMap;
	}*/
	String tdUid;
	List<ValidationResult> results = new ArrayList<ValidationResult>();
	List<RuntimeFieldValue> runtimeValues = new ArrayList<RuntimeFieldValue>();
	private Map<String, Object> validateJsonResponse(String expectedResponseData, String actualResponseDescriptor) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		
		
		try {
			JsonParser parser = new JsonParser();
			JsonObject eResponseData = parser.parse(expectedResponseData).getAsJsonObject();
			
			JsonObject responseJson = parser.parse(actualResponseDescriptor).getAsJsonObject();
			this.actualResponseJson = new JsonObject();
			this.actualResponseJson.add("Root", responseJson);
			
			tdUid = eResponseData.get("TDUID").getAsString();
			JsonArray pageAreas = eResponseData.getAsJsonArray("PAGEAREAS");
			
			for(JsonElement pageAreaElement : pageAreas) {
				this.validatePageArea(pageAreaElement, null, null);
			}
			
		} catch(Exception e) {
			logger.error("ERROR validating JSON response", e);
		}
		
		
		resultMap.put("RUNTIME_VALUES", runtimeValues);
		resultMap.put("VALIDATIONS", results);
		return resultMap;
		
	}
	
	private void validatePageArea(JsonElement pageAreaElement, JsonElement parentPageArea, JsonObject parentNodeFromActualResponse) {

		JsonObject pageArea = pageAreaElement.getAsJsonObject();
		
		String pageName = pageArea.get("PA_NAME").getAsString();
		String path = pageArea.get("PA_PATH").getAsString();
		String isMrb = pageArea.get("PA_IS_MRB").getAsString();
		
		String parentPath = parentPageArea == null ? "" : parentPageArea.getAsJsonObject().get("PA_PATH").getAsString();
		String relativePath = path;
		logger.debug("Validating Page [{}]", pageName);
		
		JsonElement elementAtPath = null;
		if(parentPageArea == null) {
			//This is the first page we need to validate OR this is the root of the JSON. So let us resolve the path from the top of theJSON
			elementAtPath = this.getElementAtPath(relativePath);
		}else{
			//This means we are in the middle of traversing the document, so we are going to search relatively.
			relativePath = path.replace(Utilities.trim(parentPath+ "-->"), "");
			elementAtPath = this.getElementAtPath(parentNodeFromActualResponse, relativePath);
		}
		if(elementAtPath == null) {
			logger.error("ERROR --> Could not find page [{}] in response JSON.", pageName);
			return;
		}
		
		JsonArray detailRecords = pageArea.get("DETAILRECORDS").getAsJsonArray();
		if("N".equalsIgnoreCase(isMrb)) {
			if(detailRecords != null && detailRecords.size() > 0) {
				JsonElement detailRecordElement = detailRecords.get(0);
				JsonObject fieldNode = elementAtPath.getAsJsonObject();
				JsonObject detailRecord = detailRecordElement.getAsJsonObject();
				String detailId = detailRecord.get("DR_ID").getAsString();
				JsonArray fields = detailRecord.getAsJsonArray("FIELDS");
				for(JsonElement fieldElement : fields) {
					JsonObject field = fieldElement.getAsJsonObject();
					/*Modified by Preeti for TJN252-80 starts*/
					/*String label = field.get("FLD_LABEL_2").getAsString();*/
					String label = field.get("FLD_LABEL").getAsString();
					/*Modified by Preeti for TJN252-80 ends*/
					String expectedFieldValue = field.get("DATA").getAsString();
					if(!"@@OUTPUT".equalsIgnoreCase(expectedFieldValue)) {
						if(fieldNode == null) {
							ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, "Could not locate this field in response XML.", detailId);
							results.add(r);
							continue;
						}
							//TENJINCG-514
						String actualFieldValue = "";
						/*if(fieldNode.get(label) != null) {
							actualFieldValue = fieldNode.get(label).getAsString();
						}*/
						try {
							actualFieldValue = fieldNode.get(label).getAsString();
						}catch(UnsupportedOperationException e) {
							logger.warn("Could not find value of {}", e.getMessage());
						}
						if(actualFieldValue == null) {
							actualFieldValue = "";
						}
						
						logger.debug("Field [{}], Expected [{}], Actual [{}]", label, expectedFieldValue, actualFieldValue);
						
						ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, actualFieldValue, detailId);
						results.add(r);
					}else{
						
						if(fieldNode != null) {
							//TENJINCG-514
							String actualFieldValue = null;
							/*if(fieldNode.get(label) != null) {
							actualFieldValue = fieldNode.get(label).getAsString();
						}*/
							try {
								actualFieldValue = fieldNode.get(label).getAsString();
							}catch(UnsupportedOperationException e) {
								logger.warn("Could not find value of {}", e.getMessage());
							}
							if(actualFieldValue == null) {
								actualFieldValue = "";
							}
							RuntimeFieldValue rfv = this.getRuntimeFieldValueObject(label, actualFieldValue,  tdUid);
							runtimeValues.add(rfv);
						}
						
					}
				}
					//TENJINCG-514
				JsonArray children = detailRecord.getAsJsonArray("CHILDREN");
				if(children != null && children.size() > 0) {
					for(JsonElement childPageArea : children) {
						this.validatePageArea(childPageArea, pageArea, fieldNode);
					}
				}	//TENJINCG-514 ends
			}
		}else{
			//elementAtPath is expected to be a JSONArray for MRBs
			if(!elementAtPath.isJsonArray()) {
				logger.warn("Element at path is expected to be a JSONArray for a multi record page, but it is not. Validation cannot continue");
				return;
			}
			
			JsonArray actualDetailRecords = elementAtPath.getAsJsonArray();
			
			if(detailRecords != null && detailRecords.size() > 0) {
				int dIndex=-1;
				for(JsonElement detailRecordElement : detailRecords) {
					JsonObject detailRecord = detailRecordElement.getAsJsonObject();
					String detailId = detailRecord.get("DR_ID").getAsString();
					JsonArray fields = detailRecord.getAsJsonArray("FIELDS");
					JsonObject fieldNode = null;
					boolean queryFieldsAvailable = false;
					
					//
					//TENJINCG-416
					JsonArray queryFields = new JsonArray();
					for(JsonElement field : fields){
						JsonObject fieldObj = field.getAsJsonObject();
						if(Utilities.trim(fieldObj.get("ACTION").getAsString()).equalsIgnoreCase("QUERY")) {
							queryFieldsAvailable = true;
							queryFields.add(fieldObj);
						}
					}
					//TENJINCG-416 Ends
					
					int indexForCurrentDetailRecord = getLastDetailIndex(detailId);
					
					if(queryFieldsAvailable) {
						//TENJINCG-416
						//The objective here is to iterate through the queryFields, and find the fieldNode that matches the query criteria.
						for(JsonElement actualDetailRecord : actualDetailRecords) {
							JsonObject aRecordObject = actualDetailRecord.getAsJsonObject();
							boolean match = true;
							for(JsonElement qField : queryFields) {
								JsonObject qFieldObject = qField.getAsJsonObject();
								/*Modified by Preeti for TJN252-80 starts*/
								/*String qFieldName = qFieldObject.get("FLD_LABEL_2").getAsString();*/
								String qFieldName = qFieldObject.get("FLD_LABEL").getAsString();
								/*Modified by Preeti for TJN252-80 ends*/
								String qFieldValue = qFieldObject.get("DATA").getAsString();
								
								String aFieldValue = aRecordObject.get(qFieldName) != null ? aRecordObject.get(qFieldName).getAsString() : "";
								if(!Utilities.trim(qFieldValue).equalsIgnoreCase(Utilities.trim(aFieldValue))){
									match = false;
									break;
								}
							}
							
							if(match) {
								fieldNode = aRecordObject;
								break;
							}
						}
						//TENJINCG-416 ends
					}else{
						try{
							fieldNode = actualDetailRecords.get(indexForCurrentDetailRecord > 0 ? indexForCurrentDetailRecord-1 : 0).getAsJsonObject();
						}catch(Exception e){
							logger.error("Could not get field node from JSON For detail record {}, [{}]", dIndex, detailId, e);
						}
					}
					
					if(fieldNode == null) {
						logger.error("Could not get field node from Json for detail record [{}]", detailId);
						continue;
					}
					
					for(JsonElement fieldElement : fields) {
						JsonObject field = fieldElement.getAsJsonObject();
						/*Modified by Preeti for TJN252-80 starts*/
						/*String label = field.get("FLD_LABEL_2").getAsString();*/
						String label = field.get("FLD_LABEL").getAsString();
						/*Modified by Preeti for TJN252-80 ends*/
						String expectedFieldValue = field.get("DATA").getAsString();
						
						if(!"@@OUTPUT".equalsIgnoreCase(expectedFieldValue)) {
							if(fieldNode == null) {
								ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, "Could not locate this field in response XML.", detailId);
								results.add(r);
								continue;
							}
							
							/*String actualFieldValue = fieldNode.get(label).getAsString();*/
							/*String actualFieldValue = fieldNode.get(label) != null ? fieldNode.get(label).getAsString() : "";*/
							String actualFieldValue = "";
							try {
								actualFieldValue = fieldNode.get(label).getAsString();
							} catch(UnsupportedOperationException e) {
								logger.error("ERROR - Could not get actual value from JsonObject for key {}", label, e);
							}
							if(actualFieldValue == null) {
								actualFieldValue = "";
							}
							
							logger.debug("Field [{}], Expected [{}], Actual [{}]", label, expectedFieldValue, actualFieldValue);
							
							ValidationResult r = getValidationResultObject(pageName, label, expectedFieldValue, actualFieldValue, detailId);
							results.add(r);
						}else{
							
							if(fieldNode != null) {
								String actualFieldValue = fieldNode.get(label).getAsString();
								if(actualFieldValue == null) {
									actualFieldValue = "";
								}
								RuntimeFieldValue rfv = this.getRuntimeFieldValueObject(label, actualFieldValue,  tdUid);
								runtimeValues.add(rfv);
							}
							
						}
					}
					
					JsonArray children = detailRecord.getAsJsonArray("CHILDREN");
					if(children != null && children.size() > 0) {
						for(JsonElement childPageArea : children) {
							this.validatePageArea(childPageArea, pageArea, fieldNode);
						}
					}
				}
			}
		}
	
	}
	
	private static int getLastDetailIndex(String drId) {
		
		if(Utilities.trim(drId).length()<1) {
			return 0;
		}
		int index=0;
		
		String[] indexArray = Utilities.trim(drId).split("\\.");
		
		index = indexArray.length > 0 ? Integer.parseInt(indexArray[indexArray.length-1]) : 0;
		
		return index;
	}

	private JsonElement getElementAtPath(JsonObject parentElement, String pagePath) {
		pagePath = Utilities.trim(pagePath);
		boolean found = true;
		String[] pathArray = pagePath.split("-->");
		JsonElement elementAtPath = null;
		for(String path : pathArray) {
			if(elementAtPath == null) {
				elementAtPath = parentElement.get(path);
			}else{
				JsonElement tempElement = elementAtPath.getAsJsonObject().get(path);
				elementAtPath = tempElement;
			}
			
			if(elementAtPath == null) {
				logger.error("Path {} was not found in response json", path);
				found = false;
				break;
			}
			
		}
		
		if(found) {
			return elementAtPath;
		}else{
			return null;
		}
	}
	/* TENJINCG-414 Ends */
	private JsonElement getElementAtPath(String pagePath) {
		pagePath = Utilities.trim(pagePath);
		boolean found = true;
		String[] pathArray = pagePath.split("-->");
		JsonElement elementAtPath = null;
		for(String path : pathArray) {
			if(elementAtPath == null) {
				elementAtPath = this.actualResponseJson.get(path);
			}else{
				JsonElement tempElement = elementAtPath.getAsJsonObject().get(path);
				elementAtPath = tempElement;
			}
			
			if(elementAtPath == null) {
				logger.error("Path {} was not found in response json", path);
				found = false;
				break;
			}
			
		}
		
		if(found) {
			return elementAtPath;
		}else{
			return null;
		}
	}
	
	private ValidationResult getValidationResultObject(String page, String field, String expectedValue, String actualValue, String detailRecordNo) {
		ValidationResult r = new ValidationResult();
		actualValue = Utilities.trim(actualValue);
		r.setActualValue(actualValue);
		r.setExpectedValue(expectedValue);
		r.setDetailRecordNo(detailRecordNo);
		r.setField(field);
		r.setPage(page);
		r.setIteration(this.iteration);
		r.setResValId(0);
		r.setRunId(this.runId);
		r.setTestStepRecordId(step.getRecordId());

		if(Utilities.isNumeric(actualValue) && Utilities.isNumeric(expectedValue)){
			if(Utilities.isNumericallyEqual(actualValue, expectedValue)){
				r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
			}else{
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		}else if(actualValue.equalsIgnoreCase(expectedValue)){
			/*************
			 * Fix by Sriram for comparision of numeric values Ends
			 */

			r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
		}else{

			if(Utilities.isNumeric(expectedValue) && Utilities.isNumeric(actualValue)){
				if(Double.parseDouble(expectedValue) == Double.parseDouble(actualValue)){
					r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
				}else{
					r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
				}
			}else{
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		}

		return r;

	}

	protected RuntimeFieldValue getRuntimeFieldValueObject(String field, String value, String tdUid){
		RuntimeFieldValue r = new RuntimeFieldValue();

		r.setDetailRecordNo(1);
		r.setField(field);
		r.setIteration(this.iteration);
		r.setRunId(this.runId);
		r.setTdGid(this.step.getTdGid());
		r.setTdUid(tdUid);
		r.setTestStepRecordId(step.getRecordId());
		r.setValue(value);

		logger.debug(r.toString());
		return r;
	}
}
