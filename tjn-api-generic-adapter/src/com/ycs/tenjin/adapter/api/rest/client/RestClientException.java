package com.ycs.tenjin.adapter.api.rest.client;

public class RestClientException extends Exception {
	private static final long serialVersionUID = 6193918139490967683L;

	public RestClientException(String message) {
		super(message);
	}
	
	public RestClientException(String message, Throwable cause) {
		super(message, cause);
	}
}
