/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  OpenAPIReader.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 23-Oct-2019		Sriram Sridharan		Added as part of TENJINCG-1118
 */

package com.ycs.tenjin.adapter.api.rest.imports.openapi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.swagger.models.Model;
import io.swagger.models.Swagger;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.Property;
import io.swagger.models.properties.RefProperty;
import io.swagger.parser.SwaggerParser;

public class OpenAPIReader {
	private static final Logger logger = LoggerFactory.getLogger(OpenAPIReader.class);
	
	private Swagger swagger;
	private String documentationUrl;
	
	private Map<String, JsonElement> plainPropertiesMap = new HashMap<String, JsonElement>();
	
	public void readFrom(String url) throws Exception {
		this.documentationUrl = StringUtils.trim(url);
		
		try {
			logger.info("Reading documentation from [{}]", this.documentationUrl);
			this.swagger = new SwaggerParser().read(this.documentationUrl);
		} catch(Exception e) {
			throw e;
		}
	}
	
	public String getExampleRequest(String definition) {
		plainPropertiesMap = new HashMap<String, JsonElement>();
		JsonElement jsonElement = this.extractJsonForModel(definition);
		if(jsonElement != null) {
			return new GsonBuilder().setPrettyPrinting().create().toJson(jsonElement);
		}else {
			return null;
		}
	}
	
	private JsonElement extractJson(Property property) {
		
		if(property == null) {
			logger.warn("Cannot extract json for a null property");
			return new JsonObject();
		}
		
		
		
		RefProperty targetRefProperty = null;
		boolean array = false;
		
		if(property instanceof RefProperty) {
			targetRefProperty = (RefProperty) property;
		}else if(property instanceof ArrayProperty) {
			ArrayProperty arrayProperty = (ArrayProperty) property;
			array = true;
			if(arrayProperty.getItems() != null && arrayProperty.getItems() instanceof RefProperty) {
				targetRefProperty = (RefProperty) arrayProperty.getItems();
			}else if(arrayProperty.getItems() != null) {
				//Just for logging to see what all types we can get
				logger.warn("WARNING - ArrayProperty.getItems() is of an unknown type [{}]", arrayProperty.getItems().getClass().getName());
			}else {
				//Just for logging to see what all types we can get
				logger.warn("WARNING - arrayProperty.getItems() is null. Json extraction may not be perfect");
			}
		}
		
		if(targetRefProperty != null) {
			String reference = getDefinitionReference(targetRefProperty.get$ref());
			JsonElement element = null;
			if(this.plainPropertiesMap.containsKey(reference)) {
				element = this.plainPropertiesMap.get(reference);
			}else {
				element =  this.extractJsonForModel(getDefinitionReference(reference));
			}
			
			if(array) {
				JsonArray jsonArray = new JsonArray();
				jsonArray.add(element);
				return jsonArray;
			}else {
				return element;
			}
		}else {
			return new JsonObject();
		}
	}
	
	public JsonElement extractJsonForModel(String model) {
		if(this.swagger != null && this.swagger.getDefinitions() != null) {
			Model schema = this.swagger.getDefinitions().get(model);
			if(schema == null) {
				logger.error("Error - No matching definition found for model [{}]", model);
				return null;
			}
			
			return this.parseModel(schema, model);
		}else {
			logger.error("Error - Swagger definitions object is null");
			return null;
		}
	}
	
	public JsonElement extractJsonForModel(String model, Swagger swagger) {
		this.swagger = swagger;
		return this.extractJsonForModel(model);
	}

	
	private JsonElement parseModel(Model model, String modelName) {
		JsonObject root = new JsonObject();
		logger.info("Processing model [{}]", modelName);
		Set<String> uniqueProperties = new HashSet<String>();
		
		List<Property> embeddedProperties = new ArrayList<Property>();
		
		if(modelName.equalsIgnoreCase("AssetType")) {
			System.err.println();
		}
		
		for(Entry<String, Property> entry : model.getProperties().entrySet()) {
			String propertyName = entry.getKey();
			
			Property property = entry.getValue();
			property.setName(propertyName);
			uniqueProperties.add(property.getClass().getSimpleName());
			
			
			Object value = this.getPropertyValue(property, propertyName);
			if(property instanceof ArrayProperty || property instanceof RefProperty) {
				embeddedProperties.add(property);
				continue;
			}
			
			logger.debug("Property [{}.{}]- Primitive", modelName, propertyName);
			if(value != null) {
				if(value instanceof String) {
					root.addProperty(propertyName, value.toString());
				}else if(value instanceof Number) {
					root.addProperty(propertyName, (Number) value);
				}else if(JsonElement.class.isAssignableFrom(value.getClass())) {
					root.add(propertyName, (JsonElement) value); 
				}else {
					root.addProperty(propertyName, value.toString());
				}
			}
		}
		
		this.plainPropertiesMap.put(modelName, this.deepCopy(root));
		for(Property property : embeddedProperties) {
			logger.debug("Property [{}.{}]- Embedded", modelName, property.getName());
			JsonElement valueElement = this.extractJson(property);
			root.add(property.getName(), valueElement);
			
			
		}
		
		return root;
	}
	
	private JsonObject deepCopy(JsonObject jsonObject) {
		try {
			Gson gson = new Gson();
			return gson.fromJson(gson.toJson(jsonObject), JsonObject.class);
		}catch(Exception e) {
			logger.error("Deep copy of JsonObject Failed",e );
			return null;
		}
	}
	
	@SuppressWarnings("rawtypes")
	private Object getPropertyValue(Property property, String propertyName) {
		Class propertyClass = property.getClass();
		switch(propertyClass.getSimpleName()) {
		case "IntegerProperty" :
			return (Number) 0;
		case "LongProperty":
			return (Number) 0;
		case "BaseIntegerProperty":
			return (Number) 0;
		case "DecimalProperty":
			return (Number) 0.0;
		case "DoubleProperty":
			return (Number) 0.0;
		case "FloatProperty":
			return (Number) 0.0;
		case "StringProperty":
			return "";
		case "UUIDProperty":
			return "";
		case "EmailProperty":
			return "email@hostname.com";
		case "DateProperty":
			return this.getNowAsISO();
		case "DateTimeProperty":
			return this.getNowAsISO();
		case "MapProperty":
			return new JsonObject();
		case "ObjectProperty":
			return new JsonObject();
		case "ArrayProperty":
			return null;
		case "RefProperty":
			
			return null;
		default:
			return "";
			
		}
	}
	
	private String getNowAsISO() {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
		df.setTimeZone(tz);
		String nowAsISO = df.format(new Date());
		return nowAsISO;
	}
	
	public static String getDefinitionReference(String reference) {
		if(!StringUtils.isEmpty(reference)) {
			reference = StringUtils.trim(reference).replace("#/definitions", "");
			
			String[] split = reference.split("/");
			return (split[split.length-1]);
			
		}else {
			return null;
		}
	}
	
	public static void main(String[] args) throws Exception {
		OpenAPIReader reader = new OpenAPIReader();
		reader.readFrom("http://localhost:8080/assets/v2/api-docs");
		String exampleRequest = reader.getExampleRequest("Asset");
		System.out.println(exampleRequest);
		
	}
}
