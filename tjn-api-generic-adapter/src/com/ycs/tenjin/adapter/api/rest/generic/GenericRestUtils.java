package com.ycs.tenjin.adapter.api.rest.generic;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


import com.ycs.tenjin.util.Utilities;


public class GenericRestUtils {

	private static final Logger logger = LoggerFactory.getLogger(GenericRestUtils.class);

	private Document requestDocument;
	private Document baselineDocument;



	public String tdUid;
	
	public static Document loadDocument(String xmlString, boolean namespaceAware) {

		if(xmlString == null) {
			logger.error("ERROR loading document - input xmlString is null");
			return null;
		}

		logger.debug("Loading XML Document from String {}", xmlString);

		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);

			DocumentBuilder builder = factory.newDocumentBuilder();

			return builder.parse(new InputSource(new StringReader(xmlString)));
		} catch (FileNotFoundException e) {
			logger.error("ERROR loading XML document from string {}", xmlString, e);
			return null;
		} catch (ParserConfigurationException e) {
			logger.error("ERROR parsing document", e);
			return null;
		} catch (SAXException e) {
			logger.error("ERROR parsing document", e);
			return null;
		} catch (IOException e) {
			logger.error("ERROR loading XML document from string {}", xmlString, e);
			return null;
		}
	}



	/*public  void scanNode(Node node, String uniqueNameForThisNode) throws LearnerException{
		//For TENJINCG-610 ends

		//Changes for RBL - Saranya - Deekshitha
		String nodeName = "";
		boolean loactionExist=false;
		try {

			nodeName = node.getLocalName();
			//For TENJINCG-610
			if(uniqueNameForThisNode == null)
				uniqueNameForThisNode = nodeName;
			//For TENJINCG-610 ends

			logger.debug("Scanning node {}", nodeName);
			NodeList childNodes = node.getChildNodes();
			logger.debug("This node has [{}] child nodes", childNodes.getLength());

			for(int i=0; i<childNodes.getLength(); i++) {
				Node childNode = childNodes.item(i);

				if(childNode.getNodeType() != Node.ELEMENT_NODE) {
					continue;
				}
				

				String childNodeLocalName = childNode.getLocalName();


				if(hasChildNodes(childNode)) {
					logger.debug("Creating page area for [{}]", childNodeLocalName);
					//For TENJINCG-610
					String uniqueName = this.createPageAreaObject(childNode);
					this.scanNode(childNode, uniqueName);
					//For TENJINCG-610 ends
				} else {
					
					//For TENJINCG-610
					this.createTestObject(childNode);
					if(!loactionExist){
						String uniqueName = this.createPageAreaObject(node);
						loactionExist=true;
						}
					this.createTestObject(childNode, uniqueNameForThisNode);
					//For TENJINCG-610 ends
				}

			}

		} catch(LearnerException e) {
			throw e;
		} catch(Exception e) {
			logger.error("ERROR scanning node", e);
			throw new LearnerException("Could not learn XML due to an internal error.");
		}

		TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();
		for(TestObject t:this.testObjects){
			if(t!=null && t.getUniqueId()!=null) {
			pMap.put(t.getUniqueId(), t);
			}
		}
		
		for(int i=0;i<this.testObjects.size();i++){
			TestObject t=this.testObjects.get(i);
			pMap.put(t.getUniqueId(), t);
			}
		this.testObjects.clear();
		if(this.testObjectMap==null) {
			
		}
		this.testObjectMap.add(pMap);
		this.tabOrder = -1;

	}

	private String createPageAreaObject(Node node) throws LearnerException{
		//For TENJINCG-610 ends
		boolean landingPage = false;
		this.pageSequence++;
		if(this.pageSequence < 1) {
			landingPage = true;
		}

		try{
			Location location = new Location();
			location.setLabel(node.getLocalName());
			location.setLocationType("XMLNODE");
			location.setLandingPage(landingPage);
			if(node.getParentNode() != null) {
				location.setParent(node.getParentNode().getLocalName());
			}else{
				location.setParent("N/A");
			}

			location.setPath(getPath(node));
			location.setScreenTitle(node.getNodeName());
			location.setSequence(this.pageSequence);
			location.setWayIn("N/A");
			location.setWayOut("N/A");

			this.addToMap(location);

			this.uniqueLocationName = location.getLocationName();
			//For TENJINCG-610
			return this.uniqueLocationName;
			//For TENJINCG-610 ends
		} catch (Exception e) {
			logger.error("ERROR occurred while creating location", e);
			throw new LearnerException("Could not create location");
		}
	}

public  String getPath(Node node) {
		
		boolean reachedTop = false;
		String path = "";
		List<String> paths = new ArrayList<String>();
		try{
			while(!reachedTop) {
				Node parent = node.getParentNode();
				if(parent != null) {
					paths.add(node.getNodeName());
					node = parent;
				}else  {
					reachedTop = true;
				}
			}
		} catch(Exception e) {
			logger.error("ERROR", e);
		}
		
		for(int i= (paths.size()-1); i>=0; i--) {
			path = path + paths.get(i);
			if(i > 0) {
				path = path  + "-->";
			}
		}
		
		return path;
	}

	private void addToMap(Location location) throws LearnerException{
		try{
			String locationName = location.getLabel();
			logger.debug("Scanning map for existing pages with name " + locationName);
			if(location.getSequence()==1) {
				this.pageAreas=ArrayListMultimap.create();
			}
			
			//if(this.pageAreas!=null) {
			Collection<Location> locs = this.pageAreas.get(locationName);
			if(locs != null && locs.size() > 0){
				logger.debug("There is already " + locs.size() + " page(s) with name " + locationName);
				int nc = locs.size() + 1;
				String newLocName = "00" + Integer.toString(nc);
				newLocName = newLocName.substring(newLocName.length() -2);
				newLocName = locationName + " " + newLocName;
				logger.debug("New Location Name will be " + newLocName);
				location.setLocationName(newLocName);
			}
			//}
		else{
				logger.debug("There are no existing pages with name " + locationName);
				location.setLocationName(locationName);
			}

			this.pageAreas.put(locationName, location);
			logger.info("New Location --> {}", location.toString());
		}catch(Exception e){
			throw new LearnerException("Could not resolve location name for " + location.getLocationName() ,e);
		}
	}*/

	//For TENJINCG-610
	/*private void createTestObject(Node node) {*/
	

	public String buildXMLRequest(String tdUid, JSONArray pageAreas, String baselineRequestXML) {

		this.requestDocument = getSoapXMLTemplateDocument(baselineRequestXML);
		this.baselineDocument = loadDocument(baselineRequestXML, true);
		this.tdUid =tdUid;

		try {
			logger.info("This test has data across {} page areas", pageAreas.length());
			for(int i=0; i<pageAreas.length(); i++) {
				this.scanPageArea(pageAreas.getJSONObject(i));
			}
		} catch(JSONException e) {

		}


		return this.getStringFromDocument(this.requestDocument);
	}



	public  Document getSoapXMLTemplateDocument(String baselineRequestXML) {
		Document document = loadDocument(baselineRequestXML, true);

		Document newDoc = getNewXMLDocument(true);

		Element rootElement = document.getDocumentElement();
		Element newRoot = newDoc.createElementNS(rootElement.getNamespaceURI(), rootElement.getLocalName());
		newDoc.appendChild(newRoot);
		/*Changes for RBL - Deekshitha-Saranya 
		 * Element oHeader = (Element)
		 * document.getElementsByTagNameNS(rootElement.getNamespaceURI(),
		 * "Header").item(0);
		 * 
		 * Element oBody = (Element)
		 * document.getElementsByTagNameNS(rootElement.getNamespaceURI(),
		 * "Body").item(0);
		 * 
		 * if(oBody==null){ oBody = (Element)
		 * document.getElementsByTagNameNS(rootElement.getNamespaceURI(),
		 * "body").item(0); } Element
		 * nHeader=(Element)newDoc.getElementsByTagNameNS(rootElement.getNamespaceURI(),
		 * "RequestHeader").item(0);
		 * 
		 * Element nHeader = newDoc.createElementNS(rootElement.getNamespaceURI(),
		 * oHeader.getLocalName()); newRoot.appendChild(nHeader);
		 * 
		 * Element nBody = newDoc.createElementNS(rootElement.getNamespaceURI(),
		 * oBody.getLocalName()); newRoot.appendChild(nBody);
		 */

		return newDoc;

	}
	private void scanPageArea(JSONObject pageArea) {

		try {
			/*modified by paneendra for Tenj212-37 starts*/
			//JSONArray detailRecords = pageArea.getJSONArray("DETAILRECORDS");
			JSONObject xmldetailRecords = pageArea.getJSONObject("DETAILRECORDS");
			/*String pageName = pageArea.getString("PA_NAME");
			String pagePath = pageArea.getString("PA_PATH");*/
			/*String pageName = " ";
			String pagePath = " ";*/


			if(xmldetailRecords.length() > 0) {
				//TODO Create Page element

              
				for(int d=0; d < xmldetailRecords.length(); d++ ){

					/*if("child".equalsIgnoreCase(pageName)) {
						System.err.println();
					}*/

					Node pageNode;
					

					//JSONObject detailRecord = detailRecords.getJSONObject(d);
					JSONArray fields = xmldetailRecords.getJSONArray("FIELDS");
					for(int f=0; f< fields.length(); f++) {
						JSONObject field = fields.getJSONObject(f);
						/*Modified by Preeti for TJN252-80 starts*/
						/*String label = field.getString("FLD_LABEL_2");*/
						String label = field.getString("FLD_LABEL");
						/*Modified by Preeti for TJN252-80 ends*/
						String data = field.getString("DATA");
						String pageName = field.getString("FLD_PAGE_AREA");
						String pagePath = field.getString("FLD_PARENT_PATH");
						/*modified by paneendra for Tenj212-37 ends*/
						if(xmldetailRecords.length() > 1){
							pageNode = this.createPageNode(pageName, pagePath, true);
						}else{
							pageNode = this.createPageNode(pageName, pagePath, false);
						}
						if(!Utilities.trim(data).equalsIgnoreCase("@@OUTPUT")) {
							
							this.createFieldNode(pageNode, label, data);
						}
					}
				}

			}else{
				logger.warn("No data found for page [{}]");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			logger.error("JSONException occurred", e);
		}

	}
	private void createFieldNode(Node pageNode, String nodeName, String nodeValue) {
		this.createNode(pageNode, nodeName, pageNode.getNamespaceURI(),nodeValue);
	}

	private Node createPageNode(String nodeName, String nodePath, boolean ignoreExisting) {

		if(Utilities.trim(nodePath).equalsIgnoreCase("")) {
			return null;
		}

		logger.debug("Loading node from baseline document for page [{}]", nodeName);
		Node pageNode = this.getNodeFromBaselineDocument(nodeName, nodePath);

		String[] pathArray = nodePath.split("-->");

		Node targetNode = (Node) this.requestDocument.getDocumentElement();
		for(int i=0; i<pathArray.length; i++) {

			if( i== 0) {
				if(this.isRootNodeValid(this.getNodeNameWithoutNamespace(pathArray[i]))) {
					continue;
				}else{
					logger.error("Root Node is invalid");
					return null;
				}
			}

			String nodeLocalName = this.getNodeNameWithoutNamespace(pathArray[i]);
			if(ignoreExisting) {
				if(i == (pathArray.length -1)){
					targetNode = this.createNode(targetNode, nodeLocalName, pageNode.getNamespaceURI(), true);
				}else{
					targetNode = this.createNode(targetNode, nodeLocalName, pageNode.getNamespaceURI(), false);
				}
			}else{
				targetNode = this.createNode(targetNode, nodeLocalName, pageNode.getNamespaceURI(), false);
			}
		}

		return targetNode;

	}
	public Node createNode(Node parentNode, String childNodeLocalName, String namespaceURI, boolean ignoreExisting) {
		String parentNodeLocalName = parentNode.getLocalName();
		NodeList children = parentNode.getChildNodes();
		Node targetNode = null;
		boolean nodeFound = false;

		if(ignoreExisting) {
			Element nElement ;
			if(!Utilities.trim(namespaceURI).equalsIgnoreCase("")) {
				nElement = this.requestDocument.createElementNS(namespaceURI, childNodeLocalName);
			}else{
				nElement = this.requestDocument.createElement(childNodeLocalName);
			}
			parentNode.appendChild(nElement);
			targetNode = (Node) nElement;
		}else{
			for(int i=0; i<children.getLength(); i++ ) {
				Node child = children.item(i);

				if(this.getNodeNameWithoutNamespace(child.getNodeName()).equalsIgnoreCase(childNodeLocalName)) {
					logger.debug("Node [{}] exists under parent [{}]", childNodeLocalName, parentNodeLocalName);
					nodeFound = true;
					targetNode = child;
					break;
				}
			}

			if(!nodeFound) {
				Element nElement ;
				if(!Utilities.trim(namespaceURI).equalsIgnoreCase("")) {
					nElement = this.requestDocument.createElementNS(namespaceURI, childNodeLocalName);
				}else{
					nElement = this.requestDocument.createElement(childNodeLocalName);
				}
				
				
				parentNode.appendChild(nElement);
				targetNode = (Node) nElement;
			}
		}

		return targetNode;

	}
	public Node createNode(Node parentNode, String childNodeLocalName, String namespaceURI, String textContent) {
		String parentNodeLocalName = parentNode.getLocalName();
		NodeList children = parentNode.getChildNodes();
		Node targetNode = null;
		boolean nodeFound = false;

		for(int i=0; i<children.getLength(); i++ ) {
			Node child = children.item(i);

			if(this.getNodeNameWithoutNamespace(child.getNodeName()).equalsIgnoreCase(childNodeLocalName)) {
				logger.debug("Node [{}] exists under parent [{}]", parentNodeLocalName);
				nodeFound = true;
				targetNode = child;
				break;
			}
		}

		if(!nodeFound) {
			Element nElement ;
			if(!Utilities.trim(namespaceURI).equalsIgnoreCase("")) {
				nElement = this.requestDocument.createElementNS(namespaceURI, childNodeLocalName);
			}else{
				nElement = this.requestDocument.createElement(childNodeLocalName);
			}
			parentNode.appendChild(nElement);
			targetNode = (Node) nElement;
		}

		if(targetNode != null) {
			targetNode.setTextContent(textContent);
		}

		return targetNode;

	}
	public  Document getNewXMLDocument(boolean namespaceAware) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			docFactory.setNamespaceAware(namespaceAware);

			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			return doc;
		} catch(Exception e) {
			logger.error("ERROR creating new document", e);
			return null;
		}
	}

	public String getStringFromDocument(Document doc)	{
		try {
			DOMSource domSource = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			String xmlString = result.getWriter().toString();
			System.out.println(xmlString);
			return xmlString;

		} catch (Exception e) {
			logger.error("ERROR in getStringFromDocument(doc)", e);
			return "";
		}
	}



	private Node getNodeFromBaselineDocument(String nodeName, String nodePath) {
		if(Utilities.trim(nodePath).equalsIgnoreCase("")) {
			return null;
		}

		String[] pathArray = nodePath.split("-->");

		Node targetNode = (Node) this.baselineDocument.getDocumentElement();
		for(int i=0; i<pathArray.length; i++) {

			if( i== 0) {
				if(this.isRootNodeValid(this.getNodeNameWithoutNamespace(pathArray[i]))) {
					continue;
				}else{
					logger.error("Root Node is invalid");
					return null;
				}
			}

			String nodeLocalName = this.getNodeNameWithoutNamespace(pathArray[i]);
			targetNode = this.getNode(targetNode, nodeLocalName);
		}

		return targetNode;
	}


	private Node getNode(Node parentNode, String childNodeName) {
		String childNodeLocalName = childNodeName;
		/*if(Utilities.trim(childNodeName).contains(":")) {
		String[] split = childNodeName.split(":");
		childNodeLocalName = split[1];
	}else{
		childNodeLocalName = childNodeName;
	}*/

		NodeList nList =((Element) parentNode).getElementsByTagNameNS("*", childNodeLocalName);

		if(nList.getLength() > 0) {
			return nList.item(0);
		} else{
			return null;
		}
	}

	private String getNodeNameWithoutNamespace(String name) {
		if(Utilities.trim(name).equalsIgnoreCase("")) {
			return "";
		}


		String[] array = name.split(":");

		if(array.length > 1) {
			return array[1];
		}else {
			return array[0];
		}
	}

	public static String getNodeNameWithoutPrefix(String name) {
		if(Utilities.trim(name).equalsIgnoreCase("")) {
			return "";
		}


		String[] array = name.split(":");

		if(array.length > 1) {
			return array[1];
		}else {
			return array[0];
		}
	}

	private boolean isRootNodeValid(String rootNodeLocalName) {
		try {

			Element rootElement = this.baselineDocument.getDocumentElement();
			if(rootNodeLocalName.equalsIgnoreCase(rootElement.getLocalName())) {
				return true;
			}else {
				return false;
			}

		} catch(Exception e) {
			logger.error("ERROR occurred while validating baseline root node name", e);
			return false;
		}
	}

	public static boolean hasChildNodes(Node node) {
		boolean hasChildNodes = false;

		NodeList childNodes = node.getChildNodes();
		for(int i=0; i< childNodes.getLength(); i++ ) {
			Node childNode = childNodes.item(i);
			if(childNode.getNodeType() == Node.ELEMENT_NODE) {
				hasChildNodes = true;
				break;
			}
		}

		return hasChildNodes;
	}
	
	private boolean isRootNodeValid(Document document, String rootNodeLocalName) {
		try {

			Element rootElement = document.getDocumentElement();
			if(rootNodeLocalName.equalsIgnoreCase(rootElement.getLocalName())) {
				return true;
			}else {
				return false;
			}

		} catch(Exception e) {
			logger.error("ERROR occurred while validating baseline root node name", e);
			return false;
		}
	}

}
