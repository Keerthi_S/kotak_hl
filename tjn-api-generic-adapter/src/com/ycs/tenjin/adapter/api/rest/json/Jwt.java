package com.ycs.tenjin.adapter.api.rest.json;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import io.jsonwebtoken.Jwts;




public class Jwt {
	
	private static final Logger logger=LoggerFactory.getLogger(Jwt.class);
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		
		String payload = "{" + " \"doFundsTransfer\": {" + " \"arg0\": {" + " \"bankCode\": \"08\","
				+ " \"channel\": \"APIGW\"," + " \"externalReferenceNo\": \"2507201805\","
				+ " \"transactingPartyCode\": \"50000010\"," + " \"transactionBranch\": \"089999\","
				+ " \"userId\": \"DevUser01\"" + " }," + " \"arg1\": {" + " \"additionalBrokerFillerDTO\": \"\","
				+ " \"brokerDetailsDTO\": {" + " \"acctNbr\": \"50100100796920\"," + " \"agencyId\": \"123\","
				+ " \"amtTxnTcy\": \"5.0\"," + " \"custId\": \"50197740\"," + " \"flagRepeat\": \"N\","
				+ " \"narrative\": \"TXN_NARR\"," + " \"refSysTrAudNbr\": \"2507201806\","
				+ " \"transactionId\": \"7005\"" + " }," + " \"flgRelHold\": \"N\"," + " \"namCurrencyShrt\": \"INR\","
				+ " \"toAccountNumber\": \"50100100705820\"," + " \"toCustId\": \"50156042\","
				+ " \"valueDateXML\": \"20180725\"" + " }" + " }" + " }" + "";
		
		try {
			PrivateKey privateKey = loadPrivateKey();
			String jwtToken = Jwts.builder()
					.setHeaderParam("typ", "JWT")
					.setClaims(new Gson().fromJson(payload, Map.class))
					.signWith(privateKey).compact();
			
			jwtToken=new Jwt().getJWTString(payload,privateKey);
			
			System.out.println(jwtToken);
			String encryptedRequestBody = CryptoUtils.encrypt(CryptoUtils.INIT_VECTOR + jwtToken, true);
			String encryptedSymmetricKey = CryptoUtils.encryptSymmetricKey();

			JsonObject finalRequest = new JsonObject();
			finalRequest.addProperty("RequestSignatureEncryptedValue", encryptedRequestBody);
			finalRequest.addProperty("SymmetricKeyEncryptedValue", encryptedSymmetricKey);
			finalRequest.addProperty("Scope", "OOB");
			finalRequest.addProperty("TransactionId", UUID.randomUUID().toString());
			finalRequest.addProperty("OAuthTokenValue", "");
			
			System.out.println(finalRequest.toString());
			
			/*try {
				Jws<Claims> claims = Jwts.parserBuilder().setSigningKey(getPublicKey()).build().parseClaimsJws(jwtToken);
				System.out.println(claims);
			} catch (SignatureException e) {
				logger.error("Error"+e);
			} catch (ExpiredJwtException e) {
				logger.error("Error"+e);
			} catch (UnsupportedJwtException e) {
				logger.error("Error"+e);
			} catch (MalformedJwtException e) {
				logger.error("Error"+e);
			} catch (IllegalArgumentException e) {
				logger.error("Error"+e);
			} catch (InvalidKeySpecException e) {
					logger.error("Error"+e);
			}*/
			
		} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException | CertificateException
				| IOException e) {
			logger.error("Error"+e);
		}

	}
	
	public String getJWTString(String payload,PrivateKey privateKey) {
		
		String jwtToken = Jwts.builder()
				.setHeaderParam("typ", "JWT")
				.setClaims(new Gson().fromJson(payload, Map.class))
				.signWith(privateKey).compact();
		return jwtToken;
	}
	
	public static PublicKey getPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
	    String rsaPublicKey = "-----BEGIN CERTIFICATE-----"
	    		+ "MIIC5DCCAcygAwIBAgIIWP5F/i/mMEQwDQYJKoZIhvcNAQEMBQAwEDEOMAwGA1UE"
	    		+ "AxMFeWV0aGkwHhcNMjEwNTA1MDI1ODIzWhcNMjEwNjA0MDI1ODIzWjAQMQ4wDAYD"
	    		+ "VQQDEwV5ZXRoaTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKe910gh"
	    		+ "BZms5zs+d+wl6gRbJNBm/+OxcHk1m2ojN4uB0XMZW26Ge9eEkJQlbu6IUcU1ox4s"
	    		+ "NHGR7BvuGXDFQJ7272MhS1Dwv2q67iP4oDm81xj4v9G7Rkg45CSMnNcbwgllQt5t"
	    		+ "k6pdUNyLmRkYqmRUUxW5Gkyt7xlRvX7spDA1GwQTq1TgrGLSUdLN0wCMFrIVK+Xg"
	    		+ "9yFJmeLeKvOZWRBDgX44BmZJBWSQJx26vHIifiVTL4Iz6e2hkXHezd2zr+maJkQY"
	    		+ "Rp9iOGFF2YIY1PGPEtpiHInr0OzYHX6epQ4NgZ/JzfflmU/MNXlRKJYIjI5yFTee"
	    		+ "C/PJZvxph36plt0CAwEAAaNCMEAwHQYDVR0OBBYEFAenMXZ7/sXOYikNGrRtwhFF"
	    		+ "06puMB8GA1UdIwQYMBaAFAenMXZ7/sXOYikNGrRtwhFF06puMA0GCSqGSIb3DQEB"
	    		+ "DAUAA4IBAQCH98uY2tBw4YeJcg9oqfL9Z6WocsHvqGkqWlaRRcGm2KaUyyN2B2wn"
	    		+ "YQgEjrO0D2fObkoDxSiZAW6ONg5Nmh3Uo5dLp1YK2tEks7jDbg65xnbJUBoJnAEE"
	    		+ "EJ/1zfw0asbt21yXQQAXZFxHOIW8BBR0QtGuzRxfLvjnklPDtZVHqSEVS2eRjycR"
	    		+ "oY1oVyygrbt4SIGlxgaMdvOqtO0WCKB9TrwW4IHP1reFZQURECHIQ/8/tzExvs6H"
	    		+ "ksqex1Pd9EMiPy9uRvR34pxazkhSg99CgOg3H0gVKPFSPCw136k9H4SYluZDRyur"
	    		+ "UF9VCbpYzXTx2B5GXxooVBx4y805XxS0"
	    		+ "-----END CERTIFICATE-----";
	    rsaPublicKey = rsaPublicKey.replace("-----BEGIN PUBLIC KEY-----", "");
	    rsaPublicKey = rsaPublicKey.replace("-----END PUBLIC KEY-----", "");
	    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.decodeBase64(rsaPublicKey));
	    KeyFactory kf = KeyFactory.getInstance("RSA");
	    PublicKey publicKey = kf.generatePublic(keySpec);
	    return publicKey;
	}

	public static PrivateKey loadPrivateKey() throws UnrecoverableKeyException, KeyStoreException,
			NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException {
		String pfxPassword = "12345";
		KeyStore store = KeyStore.getInstance("PKCS12");
		store.load(new FileInputStream("D:\\HDFC\\yethiuat.p12"), pfxPassword.toCharArray());
		PrivateKey key = (PrivateKey) store.getKey("yethi", pfxPassword.toCharArray());
		return key;
	}
}
