package com.ycs.tenjin.adapter.api.rest.client;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;

public class RestClientBuilder {
	
	private static final Logger logger=LoggerFactory.getLogger(RestClientBuilder.class);
	
	/*private static final String PKCS_12_CERTIFICATE_PATH = "D:\\HDFC\\yethiuat.p12"; // path to yethi_SSL.p12
	private static final String KEYSTORE_PASS = "12345";*/
	
	public Client initClient() throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext ctx = SSLContext.getInstance("SSL");
        ctx.init(null, certs, new SecureRandom());

        return ClientBuilder.newBuilder()
                .hostnameVerifier(new TrustAllHostNameVerifier())
                .sslContext(ctx)
                .build();
    }

	private SSLContext getContext() {
		SSLContext ctx;
		String pfxPassword =" ";
		String uploadpath=" ";
		try {
			KeyStore keyStore = KeyStore.getInstance("pkcs12");
			 uploadpath=TenjinConfiguration.getProperty("HDFC_PATH");
			 pfxPassword=TenjinConfiguration.getProperty("PFX_PASSWORD");
			keyStore.load(new FileInputStream(uploadpath), pfxPassword.toCharArray());
			KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			kmf.init(keyStore, pfxPassword.toCharArray());
			ctx = SSLContext.getInstance("TLS");
			ctx.init(kmf.getKeyManagers(), null, null);
			return ctx;
		} catch (UnrecoverableKeyException | KeyManagementException | KeyStoreException | NoSuchAlgorithmException
				| CertificateException | IOException e) {
             logger.error(" Error in retreiving the context "+e);
			
		} catch (TenjinConfigurationException e) {
			logger.error("Error"+e);
		}
		
		return null;
		
	}
	
    TrustManager[] certs = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                	  return new X509Certificate[] {};
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }

                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }
            }
    };

    public static class TrustAllHostNameVerifier implements HostnameVerifier {

        public boolean verify(String hostname, SSLSession session) {
            return true;
        }

    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	public Client initClient() throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext ctx = SSLContext.getInstance("TLSv1.2");
        ctx.init(null, certs, new SecureRandom());

        return ClientBuilder.newBuilder()
                .hostnameVerifier(new TrustAllHostNameVerifier())
                .sslContext(ctx)
                .build();
    }

    TrustManager[] certs = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                	  return new X509Certificate[] {};
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }

                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }
            }
    };

    public static class TrustAllHostNameVerifier implements HostnameVerifier {

        public boolean verify(String hostname, SSLSession session) {
            return true;
        }

    }
*/}
