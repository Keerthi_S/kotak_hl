package com.ycs.tenjin.adapter.api.rest.client;

public class RestResponse {
	private int status;
	private String message;
	private Object responseData;
	
	/*Modified By Paneendra for TENJINCG-1239 starts */
	private String finalUrl;
	public String getFinalUrl() {
		return finalUrl;
	}
	public void setFinalUrl(String finalUrl) {
		this.finalUrl = finalUrl;
	}
	/*Modified By Paneendra for TENJINCG-1239 Ends */
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getResponseData() {
		return responseData;
	}
	public void setResponseData(Object responseData) {
		this.responseData = responseData;
	}
	
	public static RestResponse create(int status, String message, Object entity) {
		RestResponse response = new RestResponse();
		response.setMessage(message);
		response.setResponseData(entity);
		response.setStatus(status);
		
		return response;
	}
}
