/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RestDocumentHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION 

14-Feb-2018         Padmavathi              TENJINCG-602
20-Feb-2018         Leelaprasad             to set deafult group when uplaoding from swagger
17-Dec-2018			Sriram Sridharan		TENJINCG-914
*/

package com.ycs.tenjin.adapter.api.rest.generic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Parameter;
import com.ycs.tenjin.bridge.pojo.aut.Representation;

public class RestDocumentHandler {

	private static Logger logger = LoggerFactory
			.getLogger(RestDocumentHandler.class);
	
	public JSONArray swaggerFileUpload(String uploadedFile){


		String data="";
		JSONArray apiArray=null;
		JSONObject masterJSON ;
		/*Modified by Padmavathi for TENJINCG-602 starts*/
		File file=new File(uploadedFile);
		if (file.exists()) {
			Scanner content;
			try {
				content = new Scanner(file);
				while (content.hasNextLine()) {
					data=data+content.nextLine(); 

				}

				masterJSON = new JSONObject(data);
				apiArray=buildApiArray(masterJSON);
			}catch (FileNotFoundException e) {
				
				logger.error("Error ", e);
			}catch (JSONException e) {
				
				logger.error("Error ", e);

			}

			}
		return apiArray;
		}
	@SuppressWarnings("unchecked")
	public JSONArray buildApiArray(JSONObject masterJSON){
		
		
		JSONObject paths;

		JSONArray jsonArrayList=new JSONArray();
		String requestDescriptor=null;
		String responseDescriptor=null;
		String requestMediaType="";
		String responceMediaType="";
		List<String> parameterList= null;
		String parameter="";
try{
				paths=masterJSON.getJSONObject("paths"); 

				Iterator<String> urlKeys = paths.keys();
				while(urlKeys.hasNext()){
					String urlKey = urlKeys.next();

					try{
						JSONObject operationObject = paths.getJSONObject(urlKey);
						Iterator<String> OperationKeys = operationObject.keys();
						//list of get,post

						while(OperationKeys.hasNext()){
							JSONObject requestJSON = new JSONObject();
							JSONObject responseJSON = new JSONObject();
							String opKey = OperationKeys.next();
							JSONObject singleObjectJson = operationObject.getJSONObject(opKey);
							//get tags,produce,parameters

							// String tagObject =singleObjectJson.getJSONArray("tags").get(0).toString();
							if(singleObjectJson.has("produces")){
								requestMediaType=singleObjectJson.getJSONArray("produces").get(0).toString();
							}
							if(singleObjectJson.has("consumes")){
								responceMediaType=singleObjectJson.getJSONArray("consumes").get(0).toString();
							}
							String operationIDObject =singleObjectJson.getString("operationId");

							JSONArray parameterObject=singleObjectJson.getJSONArray("parameters");
							if(parameterObject.length()>0){
								parameterList=new ArrayList<String>();
 								for(int i=0;i<parameterObject.length();i++){
									
									JSONObject singleParameterObject=parameterObject.getJSONObject(i);
									if(singleParameterObject.getString("in").equalsIgnoreCase("body")){
										JSONObject schemaObject=singleParameterObject.getJSONObject("schema");
										String definition="";
										if(schemaObject.has("$ref")){
											definition=schemaObject.getString("$ref");  
										}else if(schemaObject.has("items")){
											JSONObject itemsOfSchemaObject=schemaObject.getJSONObject("items"); 
											definition=itemsOfSchemaObject.getString("$ref");
										}

										if(definition!=""){
											definition=definition.substring(14);


											enrichRequest(definition,masterJSON,requestJSON,definition,"");
											requestDescriptor=requestJSON.toString();
											
										}

									}
									else if(singleParameterObject.getString("in").equalsIgnoreCase("path")){
										/*parameterList=new ArrayList<String>();*/
										parameter=parameter+singleParameterObject.getString("name")+";"+singleParameterObject.getString("type")+"";
										parameterList.add(parameter);
										parameter="";
									}

								}  
							}
							List<Representation> responseRepresentation=new ArrayList<Representation>();
							if(singleObjectJson.has("responses")){
								JSONObject responceObject=singleObjectJson.getJSONObject("responses");
								Iterator<String> responceObjectKeys = responceObject.keys();
								while(responceObjectKeys.hasNext()){

									Representation representation=new Representation();
									String responseKey = responceObjectKeys.next();
									JSONObject successObject=responceObject.getJSONObject(responseKey);
									if(successObject.has("schema")){
										JSONObject schemaObject=successObject.getJSONObject("schema");
										String definition="";
										if(schemaObject.has("$ref")){
											definition=schemaObject.getString("$ref");  
										}else if(schemaObject.has("items")){
											JSONObject itemsOfSchemaObject=schemaObject.getJSONObject("items"); 
											definition=itemsOfSchemaObject.getString("$ref");
										}

										if(definition!=""){
											definition=definition.substring(14);


											enrichRequest(definition,masterJSON,responseJSON,definition,"");
											responseDescriptor=responseJSON.toString();

										}
									}
									representation.setRepresentationDescription(responseDescriptor);
									representation.setResponseCode(Integer.valueOf(responseKey));
									responseRepresentation.add(representation);
									responseDescriptor="";
								}


							}



							JSONObject  API_OP_JSONOBJ= new JSONObject();
							
							API_OP_JSONOBJ.put("API_CODE", operationIDObject);
							API_OP_JSONOBJ.put("API_NAME", operationIDObject);
							API_OP_JSONOBJ.put("API_TYPE", "rest.generic");
							API_OP_JSONOBJ.put("API_URL", urlKey);
							API_OP_JSONOBJ.put("OP_NAME", operationIDObject);
							API_OP_JSONOBJ.put("OP_METHOD_TYPE", opKey.toUpperCase());
							API_OP_JSONOBJ.put("MEDIA_TYPE_CUM", requestMediaType);
							API_OP_JSONOBJ.put("MEDIA_TYPE_PRO", responceMediaType);
							if(requestDescriptor==null){
								API_OP_JSONOBJ.put("API_OP_REQ",""); 
							}else{
								API_OP_JSONOBJ.put("API_OP_REQ",requestDescriptor);
							}
							if(responseDescriptor==null){
								API_OP_JSONOBJ.put("API_OP_RESP",""); 
							}else{
								API_OP_JSONOBJ.put("API_OP_RESP",responseRepresentation);
							}
						

							API_OP_JSONOBJ.put("PARAMETER_STR", parameter);
							API_OP_JSONOBJ.put("PARAMETER_LIST", parameterList);
							jsonArrayList.put(API_OP_JSONOBJ);
							parameter="";
							requestDescriptor=null;
							responseDescriptor=null;
							parameterList=null;
						}
					}catch(Exception e){
                     logger.error("Error ", e);
					}

				}    

			}catch (JSONException e) {
				
				logger.error("Error ", e);

			} /*catch (FileNotFoundException e) {
				
				logger.error("Error ", e);
			}
*/

		

		return jsonArrayList;

	}
	/*Modified by Padmavathi for TENJINCG-602 ends*/
	public ArrayList<Api> getApi(ArrayList<Api> selectedApiList,ArrayList<Api> oldApiList,String appId){
		ArrayList<Api> newApiList=new ArrayList<Api>();
		for(Api selectedAPI:selectedApiList){
			for(Api oldAPI:oldApiList){
				if(selectedAPI.getCode().equalsIgnoreCase(oldAPI.getCode())){
					oldAPI.setApplicationId(Integer.parseInt(appId));
					newApiList.add(oldAPI);
					break;
				}
			}
		}
		return newApiList;

	}

	static int x=0;
	static int y=1;
	@SuppressWarnings("rawtypes")
	public static JSONObject enrichRequest(String definition,
			JSONObject masterJsonObject,JSONObject requestJSON,String defKey ,String childDataType) {
		/*if(definition.equalsIgnoreCase("ClientRestResponse") || definition.equalsIgnoreCase("ClientListRestResponse")){
			System.out.println("debug");
			
		}*/
		JSONObject paramJson=new JSONObject();
		JSONObject propertiesObject=new JSONObject();
		try {
			JSONObject buildJson=new JSONObject();
			try{
                if(childDataType.equalsIgnoreCase("array")){
				buildJson=requestJSON.getJSONArray(defKey).getJSONObject(0);
                }else{
                	buildJson=requestJSON.getJSONObject(defKey);
                }
				
			}catch(JSONException je){

			}
			JSONObject definitionObject = masterJsonObject.getJSONObject(
					"definitions").getJSONObject(definition);
			if(definitionObject.has("properties")){
				propertiesObject = definitionObject
						.getJSONObject("properties");
			}

			Iterator keys = propertiesObject.keys();
			while(keys.hasNext()) {
                String childType="";
				String key = (String)keys.next();
				JSONObject value = (JSONObject) propertiesObject.get(key);
				String newDefinition="";
				if(value.has("$ref")){
					newDefinition=value.getString("$ref");  
					childType="object";
				}else if(value.has("items")){
					JSONObject itemsOfSchemaObject=value.getJSONObject("items"); 
					if(itemsOfSchemaObject.has("$ref")){
						newDefinition=itemsOfSchemaObject.getString("$ref");
						
					}
					if(value.has("type")){
						childType=value.getString("type");
					}
				}

				String valueResp =buildValueForRequest(definitionObject, value, key);
				if (newDefinition.equalsIgnoreCase("")) {

					if (x != 0) {

					buildJson.put(key, valueResp);	
					} else {

						requestJSON.putOpt(key, valueResp);
					}

				}else{
					newDefinition=newDefinition.substring(14);

					
					
					JSONArray childArray=new JSONArray();
					JSONObject insideObject=new JSONObject();
					JSONObject childObject=new JSONObject();
					

					childArray.put(insideObject);
					if(y!=1){
						if(childType.equalsIgnoreCase("array")){
						requestJSON.getJSONArray(defKey).getJSONObject(0).put(key, childArray);
						}else{
							requestJSON.put(key,childObject);
						}
					}else{
						if(childType.equalsIgnoreCase("array")){
						requestJSON.putOpt(key, childArray);
						}else{
							requestJSON.putOpt(key,childObject);
						}
					}

					if(y==1){
						paramJson=requestJSON;
					}else{
						paramJson=requestJSON.getJSONArray(defKey).getJSONObject(0);
					}
					y--;
					x++;
					enrichRequest(newDefinition,masterJsonObject,paramJson,key,childType);
					y++;
					x--; 


				}
				
			}




		} catch (JSONException e) {
			
			logger.error("Error ", e);
		}
		return requestJSON;

	}


	

	public static  List<Api> buildAPI(JSONArray jsonArrayList){
		List<Api> lstAPI=new ArrayList<Api>();
		try{
			for(int i=0;i<jsonArrayList.length();i++){
				Api api=new Api();
				JSONObject jsonObject=jsonArrayList.getJSONObject(i);
				api.setCode(jsonObject.getString("API_CODE"));
				api.setName(jsonObject.getString("API_NAME"));
				api.setType(jsonObject.getString("API_TYPE"));
				api.setUrl(jsonObject.getString("API_URL"));
				/*Changed by leelaprasad to set default group starts*/
				api.setGroup("Ungrouped");
				/*Changed by leelaprasad to set default group ends*/
				ArrayList<ApiOperation> apiOperations=buildOperations(jsonObject);
				api.setOperations(apiOperations);
				lstAPI.add(api);
			}
		}catch(Exception e){

		}
		System.out.println("Total API's:    "+lstAPI.size());
		return lstAPI;

	}

	public static ArrayList<ApiOperation> buildOperations(JSONObject jsonObject){


		ArrayList<ApiOperation> lstOperations=new ArrayList<ApiOperation>();
		try{
			
			ApiOperation operation=new ApiOperation();
			
			operation.setName(jsonObject.getString("OP_NAME"));
			System.out.println("OPERATION_NAME:===="+jsonObject.getString("OP_NAME"));
			operation.setMethod(jsonObject.getString("OP_METHOD_TYPE"));
			


			operation.setApiCode(jsonObject.getString("API_CODE"));
			Representation requestRepresentation = new Representation();
			requestRepresentation.setMediaType(jsonObject.getString("MEDIA_TYPE_CUM"));
			if(jsonObject.getString("API_OP_REQ")!=""){

				requestRepresentation.setRepresentationDescription(jsonObject.getString("API_OP_REQ"));
			}else{
				requestRepresentation.setRepresentationDescription(null);
			}
			requestRepresentation.setResponseCode(0);
			ArrayList<Representation> reqRepresentationList=new ArrayList<Representation>();
			reqRepresentationList.add(requestRepresentation);


			


			operation.setListRequestRepresentation(reqRepresentationList);
		
			
			JSONArray responseArray=(JSONArray) jsonObject.get("API_OP_RESP");
			
			ArrayList<Representation> respRepresentationList=new ArrayList<Representation>();
			
			for (int j = 0; j < responseArray.length(); j++) {
				Representation representation=(Representation) responseArray.get(j);
				representation.setMediaType(jsonObject.getString("MEDIA_TYPE_PRO"));
				respRepresentationList.add(representation);
				
			}
			
			operation.setListResponseRepresentation(respRepresentationList);
			List<Parameter> resourceParameters = new ArrayList<Parameter>();
			if(jsonObject.getString("PARAMETER_LIST").length()>0){
				JSONArray paramArray=new JSONArray(jsonObject.getString("PARAMETER_LIST"));

				for (int j = 0; j < paramArray.length(); j++) {
					Parameter p = new Parameter();
					/*Modified by Padmavathi for TENJINCG-602 starts*/
					String[] parameterList=paramArray.get(j).toString().split(",");
					for(String parameter:parameterList){
						String[] parameterLists=parameter.split(";");
					p.setName(parameterLists[0]);
					p.setType(parameterLists[1]);
					p.setStyle("Template");
					/*Modified by Padmavathi for TENJINCG-602 ends*/
					resourceParameters.add(p);
					}
				}
				operation.setResourceParameters(resourceParameters);
			}else{
				operation.setResourceParameters(null);
			}
			lstOperations.add(operation);
			/*}*/
		}catch(Exception e){
			logger.error("Error ", e);

		}

		System.out.println("Total Operations:    "+lstOperations.size());
		return lstOperations;

	}
	
	
	public static String buildValueForRequest(JSONObject definitionObject,JSONObject value,String key){
		String valueResp="";
		Boolean readOnly = false;
		JSONArray defEnum=new JSONArray();
		try {
			if (value.has("readOnly")) {
				String defString ="";
				readOnly = (Boolean) value.get("readOnly");
				if(value.has("enum")){
				defEnum =(JSONArray) value.get("enum");
				defString = returnString(defEnum);
				}
				if (readOnly) {
					if(defString.equalsIgnoreCase("")){
						valueResp = "read-Only";
					}else{
						valueResp = "read-Only" + ";" + defString;
					}
				} else {
					valueResp = ""+";"+defString;
				}
			} 
			else if(definitionObject.has("required")){
				String defString ="";
				 JSONArray jArray=(JSONArray) definitionObject.get("required");
				 Boolean requiredFlag=fetchRequiredStatus(jArray ,key);
				 if(value.has("enum")){
						defEnum =(JSONArray) value.get("enum");
						defString = returnString(defEnum);
				 }
				 if(requiredFlag){
					 if(defString.equalsIgnoreCase("")){
						 valueResp = "required";
					 }else{
					 valueResp = "required" + ";" + defString;
					 }
				 }else{
					 valueResp = "" + ";" + defString;
				 }
			}
			else if (!(value.has("readOnly")) && (definitionObject.has("required")) &&  value.has("enum")) {
				
			  
				 defEnum =(JSONArray) value.get("enum");
				String defString = returnString(defEnum);

				if (defString.length() > 0) {
					valueResp = ""+";"+defString;
				} else {
					valueResp = "";
				}

			}
		} catch (JSONException e) {
			
			logger.error("Error ", e);
		}
		return valueResp;
	}
	public static String returnString(JSONArray jsonArray){
		String str="";
		int i=0;
		try {
			
			for(int j=0;j<jsonArray.length();j++){
				if(i==0){
				  str=(String) jsonArray.get(j);
				}else{
				   str=str+","+(String) jsonArray.get(j);
				}
			i++;
		}
		} catch (JSONException e) {
			
			logger.error("Error ", e);
		}
		
		return str;
		
	}
	private static Boolean fetchRequiredStatus(JSONArray jsonArray, String key) {
		Boolean flag=false;
		try {
		for(int j=0;j<jsonArray.length();j++){
			String str=(String) jsonArray.get(j);
			if(key.equalsIgnoreCase(str)){
				flag=true;
				break;
				
			}
		}
		} catch (JSONException e) {
			
			logger.error("Error ", e);
		}
		return flag;
	}
	/*added by Padmavathi for TENJINCG-602 starts*/
	public JSONArray buildJsonFromUrl(String url)throws IOException, JSONException {
		    InputStream is = new URL(url).openStream();
		    BufferedReader rd =null;
		    try {
		     rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		      String jsonText = readAll(rd);
		      JSONObject json = new JSONObject(jsonText);
		      JSONArray jsonArray=this.buildApiArray(json);
		      return jsonArray;
		    } finally {
		    	rd.close();
		        is.close();
		    }
		  }
	private  String readAll(Reader rd) throws IOException {
		    StringBuilder sb = new StringBuilder();
		    int cp;
		    while ((cp = rd.read()) != -1) {
		      sb.append((char) cp);
		    }
		    return sb.toString();
		  }
	/*added by Padmavathi for TENJINCG-602 ends*/
	
	
}
