package com.ycs.tenjin.adapter.api.rest.json;

public class JsonParserException extends Exception {
	
	private static final long serialVersionUID = -524120918497109075L;

	public JsonParserException(String message) {
		super(message);
	}
	
	public JsonParserException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
