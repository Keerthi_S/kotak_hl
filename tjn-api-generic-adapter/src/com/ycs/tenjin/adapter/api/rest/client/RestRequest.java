/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RestRequest.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 23-Nov-2017           Sriram Sridharan          TENJINCG-527
 * 17-Nov-2020			Pushpalatha 			TENJINCG-1228

 */

package com.ycs.tenjin.adapter.api.rest.client;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.util.Utilities;


public class RestRequest {
	private static final Logger logger=LoggerFactory.getLogger(RestRequest.class);
	
	private static ClientConfig config = new ClientConfig();
	
	private Map<String, String> headers = new HashMap<String, String>();
	private String baseUrl;
	private Map<String, String> pathParameters = new HashMap<String, String> ();
	private String path;
	private String method;
	private String finalUrl;
	private String requestData;
	private String requestMediaType;
	/*Added by Pushpa for TENJINCG-1228 starts*/
	private String requestUrl;
	
	public String getRequestUrl() {
		return requestUrl;
	}
	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}
	/*Added by Pushpa for TENJINCG-1228 ends*/
	
	
	public String getRequestData() {
		return requestData;
	}
	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}
	public String getRequestMediaType() {
		return requestMediaType;
	}
	public void setRequestMediaType(String requestMediaType) {
		this.requestMediaType = requestMediaType;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public Map<String, String> getHeaders() {
		return headers;
	}
	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}
	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	public Map<String, String> getPathParameters() {
		return pathParameters;
	}

	public void setPathParameters(Map<String, String> pathParameters) {
		this.pathParameters = pathParameters;
	}
	
	
	public RestResponse sendRequest() throws RestClientException,JSONException {
		this.mergePathParameters();
		
		RestResponse response =null;
		
		if("get".equalsIgnoreCase(this.method)) {
			response = this.get(this.finalUrl,this.requestData,this.requestMediaType);
		}else if("post".equalsIgnoreCase(this.method)) {
			response = this.post(this.finalUrl, this.requestData, this.requestMediaType);
		}else if("delete".equalsIgnoreCase(this.method)) {
			response = this.delete(this.finalUrl);
		}else if("put".equalsIgnoreCase(this.method)) {
			response = this.put(this.finalUrl, this.requestData, this.requestMediaType);
		}else{
			throw new RestClientException("Operation "+method+" is not supported yet.");
		}
		
		if(response == null) {
			logger.error("RESPONSE is null");
			throw new RestClientException("Could not get a response from the server.");
		}
		
		logger.debug("Response recieved from server.");
		logger.info("Status Code --> [{}]", response.getStatus());
		
		return response;
	}

	private RestResponse get(String url,String postData,String requestMediaType) throws RestClientException, JSONException {
		logger.info("Calling --> GET {}", url);
		
		try {
			RestClientBuilder restClientBuilder = new RestClientBuilder();
			Client client = restClientBuilder.initClient();
			
			WebTarget target = client.target(url);
			
			Builder builder = target.request();
			if(this.headers != null && this.headers.size() > 0) {
				for(String key : this.headers.keySet()) {
					builder.header(key, this.headers.get(key));
				}
			}
			
			//Response response = builder.get();
			Response response = builder.post(Entity.entity(postData, requestMediaType), Response.class);
		
			//Response response = builder.get(Entity.entity(entity, mediaType));
			
			logger.info("Response from server recieved");
			int status = response.getStatus();
			String message = response.readEntity(String.class);
			/*Modified By Paneendra for TENJINCG-1239 starts */
			//return RestResponse.create(status, null, message);
			
			RestResponse restResponse=RestResponse.create(status, null, message);
			restResponse.setFinalUrl(url);
			return restResponse;
			/*Modified By Paneendra for TENJINCG-1239 Ends */
		} catch (KeyManagementException e) {
			logger.error("ERROR accessing secured web service",e );
			throw new RestClientException("Invocation of service failed.", e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("ERROR accessing secured web service",e );
			throw new RestClientException("Invocation of service failed.", e);
		}
	}
	
	private RestResponse post(String url, String postData, String requestMediaType) throws RestClientException, JSONException {
		logger.info("Calling --> POST {}", url);
		
		try {
			RestClientBuilder restClientBuilder = new RestClientBuilder();
			Client client = restClientBuilder.initClient();
			WebTarget target = client.target(url);
			
			Builder builder = target.request();
			if(this.headers != null && this.headers.size() > 0) {
				for(String key : this.headers.keySet()) {
					builder.header(key, this.headers.get(key));
				}
			}
			
			/*Modified by Pushpalatha for Flexiloans api starts*/
			Response response=null;
			Form form = new Form();
			if(requestMediaType.equalsIgnoreCase("application/x-www-form-urlencoded")) {
				
				JSONObject jsoObject = new JSONObject(postData);
				Iterator<String> keys = jsoObject.keys();

				while(keys.hasNext()) {
				    String key = keys.next();
				    
				    	form.param(key, (String) jsoObject.get(key));
						 
				  
				}
				
				response = builder.post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED), Response.class);
			}else {
				response = builder.post(Entity.entity(postData, requestMediaType), Response.class);
			}
			/*Modified by Pushpalatha for Flexiloans api ends*/
			
			//Response response = builder.post(Entity.entity(postData, requestMediaType), Response.class);
			
			logger.info("Response from server recieved");
			int status = response.getStatus();
			String message = response.readEntity(String.class);
			/*Modified By Paneendra for TENJINCG-1239 starts */
			//return RestResponse.create(status, null, message);
			
			RestResponse restResponse=RestResponse.create(status, null, message);
			restResponse.setFinalUrl(url);
			return restResponse;
			/*Modified By Paneendra for TENJINCG-1239 Ends */
		} catch (KeyManagementException e) {
			logger.error("ERROR accessing secured web service",e );
			throw new RestClientException("Invocation of service failed.", e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("ERROR accessing secured web service",e );
			throw new RestClientException("Invocation of service failed.", e);
		}
	}
	
	private RestResponse put(String url, String postData, String requestMediaType) throws RestClientException {
		logger.info("Calling --> PUT {}", url);
		
		try {
			RestClientBuilder restClientBuilder = new RestClientBuilder();
			Client client = restClientBuilder.initClient();
			WebTarget target = client.target(url);
			
			Builder builder = target.request();
			if(this.headers != null && this.headers.size() > 0) {
				for(String key : this.headers.keySet()) {
					builder.header(key, this.headers.get(key));
				}
			}
			
			Response response = builder.put(Entity.entity(postData, requestMediaType), Response.class);
			
			logger.info("Response from server recieved");
			int status = response.getStatus();
			String message = response.readEntity(String.class);
			
			/*Modified By Paneendra for TENJINCG-1239 starts */
			//return RestResponse.create(status, null, message);
			
			RestResponse restResponse=RestResponse.create(status, null, message);
			restResponse.setFinalUrl(url);
			return restResponse;
			/*Modified By Paneendra for TENJINCG-1239 Ends */
		}catch (KeyManagementException e) {
			logger.error("ERROR accessing secured web service",e );
			throw new RestClientException("Invocation of service failed.", e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("ERROR accessing secured web service",e );
			throw new RestClientException("Invocation of service failed.", e);
		}
	}
	
	private RestResponse delete(String url) throws RestClientException {
		logger.info("Calling --> DELETE {}", url);
		
		try {
			RestClientBuilder restClientBuilder = new RestClientBuilder();
			Client client = restClientBuilder.initClient();
			WebTarget target = client.target(url);
			
			Builder builder = target.request();
			if(this.headers != null && this.headers.size() > 0) {
				for(String key : this.headers.keySet()) {
					builder.header(key, this.headers.get(key));
				}
			}
			
			Response response = builder.delete();
			
			logger.info("Response from server recieved");
			int status = response.getStatus();
			String message = response.readEntity(String.class);
			
			/*Modified By Paneendra for TENJINCG-1239 starts */
			//return RestResponse.create(status, null, message);
			
			RestResponse restResponse=RestResponse.create(status, null, message);
			restResponse.setFinalUrl(url);
			return restResponse;
			/*Modified By Paneendra for TENJINCG-1239 Ends */
		} catch (KeyManagementException e) {
			logger.error("ERROR accessing secured web service",e );
			throw new RestClientException("Invocation of service failed.", e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("ERROR accessing secured web service",e );
			throw new RestClientException("Invocation of service failed.", e);
		}
	}
	
	private String mergePathParameters() {
		
		if(this.baseUrl.endsWith("/")) {
			this.baseUrl = this.baseUrl.substring(0, this.baseUrl.length()-1);
		}
		
		if(this.path.startsWith("/")) {
			this.path = this.path.substring(1, this.path.length());
		}
		
		/*Modified by Pushpa for TJN27-169 starts*/
		if(!this.baseUrl.equalsIgnoreCase(this.path)){
		this.finalUrl = this.baseUrl + "/" + this.path;
		}else{
			this.finalUrl=this.path;
		}
		/*Modified by Pushpa for TJN27-169 ends*/
		
		String[] pathParams = extractPathParameters(this.finalUrl);
		for(String pathParam : pathParams) {
			String paramValue = Utilities.trim(this.pathParameters.get(pathParam));
			this.finalUrl = this.finalUrl.replace("{" + pathParam + "}", paramValue);
			//Remove the pathParam key in this.pathParameters
			this.pathParameters.remove(pathParam); //TENJINCG-527
		}
		
		//Any additional parameters in pathParameters shall be treated as query parameters - TENJINCG-527
		String queryString = "";
		for(String key : this.pathParameters.keySet()) {
			String paramValue = Utilities.trim(this.pathParameters.get(key));
			 if(queryString.length() < 1 ){
				 queryString += key + "=" + paramValue;
			 }else{
				 queryString += "&" + key + "=" + paramValue;
			 }
		}
		
		if(queryString.length() > 0) {
			this.finalUrl  += "?" + queryString;
		}
		//Any additional parameters in pathParameters shall be treated as query parameters - TENJINCG-527 ends
		
		return this.finalUrl;
	}

	private static String[] extractPathParameters(String url) {
		url = Utilities.trim(url);
		String params = "";
		String[] split = url.split("\\{");
		for(String s : split){
			if(s.contains("}")){
				String[] subSplit = s.split("\\}");
				if(subSplit.length > 0) {
					params += subSplit[0] +",";
				}
			}
		}
		if(params.endsWith(",")) {
			params = params.substring(0, params.length()-1);
		}

		return params.split(",");
	}
	
	
	
}
