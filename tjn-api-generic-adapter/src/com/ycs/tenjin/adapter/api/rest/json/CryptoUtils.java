package com.ycs.tenjin.adapter.api.rest.json;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Enumeration;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;

public class CryptoUtils {
	
	private static final Logger logger=LoggerFactory.getLogger(CryptoUtils.class);
	
	public static final String AES_ALGORITHM = "AES";
	public static final String CRYPTO_ALGORITHM = "AES/CBC/PKCS5PADDING";
	public static final String CRYPTO_RSA_ALGORITHM = "RSA/ECB/PKCS1Padding";
	public static final String SYMMETRIC_KEY = "zPI4n2wGFuu2DGx4RkcPAdwisqUQ3GCt";
	public static final String INIT_VECTOR = "zPI4n2wGFuu2DGx4";
	/*private static final String PUBLIC_KEY_PATH = "D:\\public_der.der";*/
	
	private static SecretKeySpec getKeySpec() {
		return getKeySpec(SYMMETRIC_KEY);
	}
	
	private static SecretKeySpec getKeySpec(String key) {
		return new SecretKeySpec(key.getBytes(), AES_ALGORITHM);
	}
	
	private static Cipher getCipher() throws NoSuchAlgorithmException, NoSuchPaddingException {
		return Cipher.getInstance(CRYPTO_ALGORITHM);
	}
	
	private static IvParameterSpec getIvParameterSpec() {
		return new IvParameterSpec(INIT_VECTOR.getBytes());
	}
	
	private static IvParameterSpec getDefaultIvParameterSpec() {
		return new IvParameterSpec(new byte[16]);
	}
	
	public static String encrypt(String raw, boolean withIv) {
		try {
			Cipher cipher = getCipher();
			SecretKeySpec secretKeySpec = getKeySpec();
			
			if(withIv) {
				IvParameterSpec iv = getIvParameterSpec();
				cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, iv);
			}else {
				cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, getDefaultIvParameterSpec());
			}
			
			byte[] encrypted = cipher.doFinal(raw.getBytes());
			return Base64.getEncoder().encodeToString(encrypted);
		} catch (InvalidKeyException e) {
			logger.error("Error"+e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("Error"+e);
		} catch (NoSuchPaddingException e) {
			logger.error("Error"+e);
		} catch (InvalidAlgorithmParameterException e) {
			logger.error("Error"+e);
		} catch (IllegalBlockSizeException e) {
			logger.error("Error"+e);
		} catch (BadPaddingException e) {
			logger.error("Error"+e);
		}
		return null;
	}
	
	public static byte[] readFileBytes(String filename) throws IOException
	{
	    Path path = Paths.get(filename);
	    return Files.readAllBytes(path);        
	}

	public static PublicKey readPublicKey(String filename) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException
	{
	    X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(readFileBytes(filename));
	    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	    return keyFactory.generatePublic(publicSpec);       
	}
	
	public static byte[] rsaEncrypt(PublicKey key, byte[] plaintext) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException
	{
	    Cipher cipher = Cipher.getInstance(CRYPTO_RSA_ALGORITHM);   
	    cipher.init(Cipher.ENCRYPT_MODE, key);  
	    return cipher.doFinal(plaintext);
	}
	
	public static String encryptSymmetricKey() {
		String publicKeyPath="";
		try {
			publicKeyPath=TenjinConfiguration.getProperty("PUBLIC_KEY_PATH");
			PublicKey publicKey = readPublicKey(publicKeyPath);
			byte[] messageBytes = SYMMETRIC_KEY.getBytes("UTF-8");
			byte[] secret = rsaEncrypt(publicKey, messageBytes);
			return Base64.getEncoder().encodeToString(secret);
		} catch (InvalidKeyException e) {
			logger.error("Error"+e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("Error"+e);
		} catch (InvalidKeySpecException e) {
			logger.error("Error"+e);
		} catch (UnsupportedEncodingException e) {
			logger.error("Error"+e);
		} catch (NoSuchPaddingException e) {
			logger.error("Error"+e);
		} catch (IllegalBlockSizeException e) {
			logger.error("Error"+e);
		} catch (BadPaddingException e) {
			logger.error("Error"+e);
		} catch (IOException e) {
			logger.error("Error"+e);
		} catch (InvalidAlgorithmParameterException e) {
			logger.error("Error"+e);
		} catch (TenjinConfigurationException e) {
			logger.error("Error"+e);
		}
		
		return null;
	}
	
	public static String decryptSymmetricKey(String encryptedKey) {
		String pfxPassword =" ";
		String uploadpath=" ";
		try {
			KeyStore store= KeyStore.getInstance("PKCS12");
			 uploadpath=TenjinConfiguration.getProperty("HDFC_PATH");
			 pfxPassword=TenjinConfiguration.getProperty("PFX_PASSWORD");
			store.load(new FileInputStream(uploadpath), pfxPassword.toCharArray());
			
			Enumeration<?> aliases = store.aliases();
			StringBuilder sb=new StringBuilder();
			String keyAlias = "";
			while (aliases.hasMoreElements()) {
				sb.append(aliases.nextElement()).append(",");
			}
			
			keyAlias=sb.toString().split(",")[0];
			//PrivateKey key = (PrivateKey)store.getKey("yethi", pfxPassword.toCharArray());
			PrivateKey key = (PrivateKey)store.getKey(keyAlias, pfxPassword.toCharArray());
			Cipher cipher = Cipher.getInstance(key.getAlgorithm());
			cipher.init(Cipher.DECRYPT_MODE, key);
			return new String(cipher.doFinal(Base64.getDecoder().decode(encryptedKey.getBytes())));
		} catch (UnrecoverableKeyException | InvalidKeyException | KeyStoreException | NoSuchAlgorithmException
				| CertificateException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException
				| IOException e) {
			logger.error("Error"+e);
			
		} catch (TenjinConfigurationException e) {
			logger.error("Error"+e);
		}
		
		return null;
	}
	
	public static String decryptResponse(String rawResponse, String decryptedKey) {
		SecretKey key = new SecretKeySpec(decryptedKey.getBytes(),AES_ALGORITHM);
		IvParameterSpec iv = getIvParameterSpec();
		
		try {
			return decrypt(CRYPTO_ALGORITHM, rawResponse, key, iv);
		} catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException
				| InvalidAlgorithmParameterException | BadPaddingException | IllegalBlockSizeException e) {
			logger.error("Error"+e);
			return null;
		}
	}
	
	public static String decrypt(String algorithm, String cipherText, SecretKey key,
		    IvParameterSpec iv) throws NoSuchPaddingException, NoSuchAlgorithmException,
		    InvalidAlgorithmParameterException, InvalidKeyException,
		    BadPaddingException, IllegalBlockSizeException {
		    
		    Cipher cipher = Cipher.getInstance(algorithm);
		    cipher.init(Cipher.DECRYPT_MODE, key, iv);
		    byte[] plainText = cipher.doFinal( Base64.getMimeDecoder().decode(cipherText.getBytes()));
		    String decryptedRes=new String(plainText);
		    String[] chunks = decryptedRes.split("\\.");
			java.util.Base64.Decoder decoder = Base64.getDecoder();

			 

			String payload = new String(Base64.getMimeDecoder().decode(chunks[1]));
			System.out.println("Payload : " + payload);
		    
		    return payload;
		}
	
public static String decryptForAPI(String value,String symmetricKey ) {
        
        
        try {
            IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(symmetricKey.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec,iv);
            byte[] original = cipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(value));
            
            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
            
        }
        return null;



    }
}
