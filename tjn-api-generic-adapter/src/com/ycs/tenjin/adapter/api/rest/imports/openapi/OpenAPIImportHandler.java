/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  OpenAPIImportHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 23-Oct-2019		Sriram Sridharan		Added as part of TENJINCG-1118
 */

package com.ycs.tenjin.adapter.api.rest.imports.openapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.ycs.tenjin.adapter.api.rest.imports.exception.ApiImportException;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Parameter;
import com.ycs.tenjin.bridge.pojo.aut.Representation;
import com.ycs.tenjin.util.Utilities;

import io.swagger.models.HttpMethod;
import io.swagger.models.Model;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Response;
import io.swagger.models.Swagger;
import io.swagger.models.auth.AuthorizationValue;
import io.swagger.models.parameters.BodyParameter;
import io.swagger.parser.SwaggerParser;

public class OpenAPIImportHandler {
	private static final Logger logger = LoggerFactory.getLogger(OpenAPIImportHandler.class);
	
	private Swagger swagger;
	List<Api> apis = new ArrayList<Api>();
	
	
	public OpenAPIImportHandler() {
	
	}
	
	public static void main(String[] args) {
		OpenAPIImportHandler handler = new OpenAPIImportHandler();
		handler.initializeFromLocation("http://localhost:8080/assets/v2/api-docs");
		
		List<Api> apis = handler.getAllAPIs();
		Api api = null;
		
		for(Api a : apis) {
			if(a.getOperations().get(0).getMethod().equalsIgnoreCase("post")) {
				api = a;
				break;
			}
		}
		
		handler.enrichApi(api);
		
		System.err.println(new GsonBuilder().setPrettyPrinting().create().toJson(api));
	}
	
	public void initializeFromLocation(String url) {
		this.initializeFromLocation(url, null);
	}
	
	public void initializeFromLocation(String url, List<AuthorizationValue> authorizationValues) throws ApiImportException {
		logger.info("Importing APIs from URL {}", url);
		try {
			this.swagger = new SwaggerParser().read(url, authorizationValues, false);
		} catch (Exception e) {
			logger.error("Error loading from Open API URL: {}", url, e);
			throw new ApiImportException(e.getMessage());
		}
		
		if(swagger == null) {
			logger.error("Error loading from Open API URL: {}", url);
			throw new ApiImportException("Could not establish a connection to " + url + ".");
		}
		
		
		logger.info("Connection established");
	}
	
	public List<Api> getAllAPIs() {
		if(this.swagger == null) {
			logger.error("Error - OpenAPIImportHandler not initialized");
			throw new ApiImportException();
		}
		
		if(this.swagger.getPaths() != null) {
			for(Entry<String, Path> entry : this.swagger.getPaths().entrySet()) {
				apis.addAll(this.buildApisFromPath(entry.getKey(), entry.getValue()));
			}
		}
		
		return apis;
	}
	
	public List<Api> enrichApis() {
		return apis;
	}
	
	public void enrichApis(List<Api> apis) {
		if(apis != null && apis.size() > 0) {
			for(Api api : apis) {
				this.enrichApi(api);
			}
		}
	}
	
	public void enrichApi(Api api) {
		if(this.swagger == null) {
			logger.error("Error - OpenAPIImportHandler not initialized");
			throw new ApiImportException();
		}
		
		if(api.getOperations() == null || api.getOperations().size() < 1) {
			logger.error("Error - No operations found for API {}", api.getCode());
			return;
		}
		
		ApiOperation apiOperation = api.getOperations().get(0);
		
		logger.info("Enriching API {} [{} {}]", apiOperation.getName(), apiOperation.getMethod(), api.getUrl());
		
		Operation operation = this.getOperationFromDefinition(apiOperation, api.getUrl());
		if(operation == null){
			logger.error("Error - Could not find a valid operation for {} {}", apiOperation.getMethod(), api.getUrl());
			return;
		}
		
		if(operation.getParameters() != null) {
			List<Parameter> tenjinPathParameters = new ArrayList<>();
			List<Parameter> tenjinQueryParameters = new ArrayList<>();
			io.swagger.models.parameters.Parameter bodyParam = null;
			for(io.swagger.models.parameters.Parameter parameter : operation.getParameters()) {
				Parameter p = new Parameter();
				p.setName(parameter.getName());
				p.setRequired(parameter.getRequired());
				p.setType(parameter.getPattern());
				
				switch(Utilities.trim(parameter.getIn().toLowerCase())) {
				case "path":
					tenjinPathParameters.add(p);
					break;
				case "query":
					tenjinQueryParameters.add(p);
					break;
				case "body":
					bodyParam = parameter;
					break;
				}
			}
			
			apiOperation.setResourceParameters(tenjinPathParameters);
			//TODO: Add query parameters to apiOperation here
			
			if(bodyParam != null) {
				
				OpenAPIReader openApiReader = new OpenAPIReader();
				
				Representation representation = new Representation();
				
				if(operation.getConsumes() != null && operation.getConsumes().size() > 0) {
					representation.setMediaType(operation.getConsumes().get(0));
				}
				
				representation.setParameters(tenjinQueryParameters);
				
				String schemaReference = ((BodyParameter) bodyParam).getSchema().getReference();
				schemaReference = OpenAPIReader.getDefinitionReference(schemaReference);
				
				JsonElement jsonElement = new OpenAPIReader().extractJsonForModel(schemaReference, swagger);
				if(jsonElement != null) {
					representation.setRepresentationDescription(new GsonBuilder().setPrettyPrinting().create().toJson(jsonElement));
				}
				
				ArrayList<Representation> reqReps = new ArrayList<Representation>();
				reqReps.add(representation);
				apiOperation.setListRequestRepresentation(reqReps);
				
				
				ArrayList<Representation> resReps = new ArrayList<Representation>();
				if(operation.getResponses() != null) {
					for(Entry<String, Response> entry : operation.getResponses().entrySet()) {
						Response response = entry.getValue();
						Model responseSchema = response.getResponseSchema();
						
						if(responseSchema == null) continue;
						
						
						JsonElement responseContent = openApiReader.extractJsonForModel(OpenAPIReader.getDefinitionReference(responseSchema.getReference()), swagger);
						
						Representation rep= new Representation();
						rep.setResponseCode(Integer.parseInt(entry.getKey()));
						rep.setRepresentationDescription(new GsonBuilder().setPrettyPrinting().create().toJson(responseContent));
						rep.setMediaType(operation.getProduces().get(0));
						resReps.add(rep);
					}
				}
				
				apiOperation.setListResponseRepresentation(resReps);
			}
		}
		
	}
	
	private Operation getOperationFromDefinition(ApiOperation apiOperation, String apiUrl) {
		String method = apiOperation.getMethod();
		
		Path path = this.swagger.getPath(apiUrl);
		if(path != null) {
			Operation operation = path.getOperationMap().get(HttpMethod.valueOf(method));
			return operation;
		}else {
			logger.error("No definition available for path {}", apiUrl);
		}
		
		return null;
	}
	
	private List<Api> buildApisFromPath(String pathToResource, Path path) {
		List<Api> apis = new ArrayList<>();
		
		
		logger.debug("Processing path: {}", pathToResource);
		
		if(path.getOperationMap() != null) {
			for(Entry<HttpMethod, Operation> entry : path.getOperationMap().entrySet()) {
				Api api = new Api();
				api.setUrl(pathToResource);
				api.setType("rest.generic");
				api.setCode(entry.getValue().getOperationId());
				api.setName(api.getCode());
				if(!StringUtils.isEmpty(entry.getValue().getSummary()) ) {
					api.setName(entry.getValue().getSummary());
				}
				ApiOperation operation = new ApiOperation();
				operation.setApiCode(api.getCode());
				List<ApiOperation> ops = new ArrayList<ApiOperation>();
				operation.setName(entry.getValue().getOperationId());
				operation.setMethod(entry.getKey().name());
				ops.add(operation);
				api.setOperations(ops);
				
				if(entry.getValue().getTags() != null && entry.getValue().getTags().size() > 0) {
					api.setGroup(entry.getValue().getTags().get(0));
				}
				
				apis.add(api);
			}
		}
		return apis;
	}
}
