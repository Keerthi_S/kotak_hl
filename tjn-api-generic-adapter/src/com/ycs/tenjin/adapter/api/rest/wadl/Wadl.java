/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Wadl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 23-11-2017		Sriram Sridharan			TENJINCG-517
 * 10-11-2021          Prem               VAPT FIX
   
 * 
 */

package com.ycs.tenjin.adapter.api.rest.wadl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Parameter;
import com.ycs.tenjin.bridge.pojo.aut.Representation;
import com.ycs.tenjin.util.Utilities;


public class Wadl {
	
	private static final Logger logger = LoggerFactory.getLogger(Wadl.class);
	
	
	/*private File wadl;*/
	private Document document;
	private String resourcesBasePath;
	List<Api> apis = new ArrayList<Api>();
	
	public Wadl(File wadl) {
/*		this.wadl = wadl;*/
		logger.info("Loading WADL");
		this.document = loadDocument(wadl, true);
	}
	

 
	public Wadl() {
		// TODO Auto-generated constructor stub
	}



	public List<Api> parseWadl() {
		
		try {
			Element rootElement = this.document.getDocumentElement();
			
			NodeList resourcesNodeList = rootElement.getElementsByTagNameNS("*", "resources");
			if(resourcesNodeList == null || resourcesNodeList.getLength() < 1) {
				logger.error("Invalid WADL file. <resources> node not found");
				throw new WadlException("Could not parse WADL. Resources node was not found. Please verify you have supplied the correct WADL and try again.");
			}
			
			Element resourcesElement = (Element) resourcesNodeList.item(0);
			this.resourcesBasePath = resourcesElement.getAttribute("base");
			logger.info("Resources Base path is {}", this.resourcesBasePath);
			NodeList resourceNodes = resourcesElement.getChildNodes();
			for(int i=0; i<resourceNodes.getLength(); i++) {
				Node resourceNode = resourceNodes.item(i);
				if(resourceNode.getNodeType() == Node.ELEMENT_NODE && resourceNode.getLocalName().equalsIgnoreCase("resource")) {
					this.parseResourceNode(resourceNode);
				}
			}
			
			
		} catch(Exception e) {
			logger.error("An unexpected error occurred while parsing WADL file", e);
			throw new WadlException("Could not parse WADL. Please contact Tenjin Support");
		}
		
		return this.apis;
	}
	
	private void parseResourceNode(Node resourceNode) {
		try {
			
			String path = this.getResourcePath(resourceNode);
			if(path.equalsIgnoreCase("/domains/{dName}/projects/{pName}/testcases/search")) 
				System.err.println();
			/*Changed by Leelaprasad for the requirement of resource parameters missing wadl*/
			List<Parameter> params = this.parseParameterNodes(resourceNode);
			/*List<Parameter> params = this.getParameterNodesTemp(path);*/
			/*Changed by Leelaprasad for the requirement of resource parameters missing wadl*/
			NodeList childrenNodes = resourceNode.getChildNodes();
			for(int i=0; i<childrenNodes.getLength(); i++) {
				Node childNode = childrenNodes.item(i);
				
				if(childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getLocalName().equalsIgnoreCase("method")) {
					ApiOperation operation = this.parseMethodNode(childNode);
					if(params != null) {
						operation.getResourceParameters().addAll(params);
					}
					Api api = new Api();
					api.setName(operation.getName());
					api.setUrl(path);
					api.setType("rest");
					api.setCode(operation.getName());
					api.getOperations().add(operation);
					this.apis.add(api);
				} else if(childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getLocalName().equalsIgnoreCase("resource")) {
					this.parseResourceNode(childNode);
				}
			}
			
		} catch(Exception e) {
			logger.error("ERROR occurred while parsing resource node");
			throw new WadlException("Could not parse resource");
		}
	}
	/*Changed by Leelaprasad for the requirement of resource parameters missing  in wadl*/
	 /*Using to populate the resource parameters in new Operation page*/
	public List<Parameter> getParameterNodesTemp(String path) {
		List<Parameter> parameters = new ArrayList<Parameter>();
		String [] paramArray=path.split("/");
		
		for(String str:paramArray){
			if(str.startsWith("{") && str.endsWith("}")){
				Parameter param = new Parameter();
				String paramName=str.substring(1, str.length()-1);
				param.setName(paramName);
				if(str.toLowerCase().contains("id")){
					param.setStyle("xs:int");
				}else{
				param.setStyle("xs:string");
				}
				param.setType("template");
				parameters.add(param);
			}
		}
		
		return parameters;
	}
	/*Changed by Leelaprasad for the requirement of resource parameters missing wadl*/
	private List<Parameter> parseParameterNodes(Node resourceOrMethodNode) {
		try {
			
			List<Parameter> parameters = new ArrayList<Parameter>();
			NodeList childrenNodes = resourceOrMethodNode.getChildNodes();
			for(int i=0; i<childrenNodes.getLength(); i++) {
				Node childNode = childrenNodes.item(i);
				if(childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getLocalName().equalsIgnoreCase("param")) {
					Parameter p = this.parseParameterNode(childNode);
					if(p != null) {
						parameters.add(p);
					}
				}
			}
			
			return parameters;
		}catch(Exception e) {
			logger.error("ERROR while parsing parameter nodes", e);
			throw new WadlException("Could not parse parameter nodes", e);
		}
	}
	
	private ApiOperation parseMethodNode(Node methodNode) {
		try{
			Element methodElement = (Element) methodNode;
			String opName = methodElement.getAttribute("id");
			String methodName = methodElement.getAttribute("name");
			
			List<Representation> requestRepresentations = new ArrayList<Representation>();
			List<Representation> responseRepresentations = new ArrayList<Representation>();
			//Fix to parse Parameters in Method request node - TENJINCG-517
			List<Parameter> methodParameters = new ArrayList<Parameter>();
			//Fix to parse Parameters in Method request node - TENJINCG-517 ends
			NodeList childrenNodes = methodElement.getChildNodes();
			for(int i=0 ; i<childrenNodes.getLength(); i++) {
				Node childNode = childrenNodes.item(i);
				
				if(Utilities.trim(childNode.getLocalName()).equalsIgnoreCase("request")) {
					//Fix to parse Parameters in Method request node - TENJINCG-517
					methodParameters = this.parseParameterNodes(childNode);
					//Fix to parse Parameters in Method request node - TENJINCG-517 ends
					NodeList representationNodes = ((Element) childNode).getElementsByTagNameNS("*", "representation");
					if(representationNodes != null && representationNodes.getLength() > 0) {
						Node representationNode = representationNodes.item(0);
						Representation rep = this.parseRepresentationNode(representationNode);
						if(rep != null) {
							requestRepresentations.add(rep);
						}
					}
					
				}else if(Utilities.trim(childNode.getLocalName()).equalsIgnoreCase("response")){
					NodeList representationNodes = ((Element) childNode).getElementsByTagNameNS("*", "representation");
					if(representationNodes != null && representationNodes.getLength() > 0) {
						Node representationNode = representationNodes.item(0);
						Representation rep = this.parseRepresentationNode(representationNode);
						if(rep != null) {
							responseRepresentations.add(rep);
						}
					}
				}
			}
			
			ApiOperation operation = new ApiOperation();
			operation.setName(opName);
			operation.setMethod(methodName);
			//Fix to parse Parameters in Method request node - TENJINCG-517
			operation.setResourceParameters(methodParameters);
			//Fix to parse Parameters in Method request node - TENJINCG-517 ends
			/*Chnaged by Leelaprasad for TENJINCG 404 starts*/
			/*if(requestRepresentations.size() > 0) {
				operation.setRequestRepresentation(requestRepresentations.get(0));
			}
			
			if(responseRepresentations.size() > 0) {
				operation.setResponseRepresentation(responseRepresentations.get(0));
			}*/
			
			ArrayList<Representation> reqRepresentationList=new ArrayList<Representation>();
			if(requestRepresentations.size() > 0) {
				for(Representation reqRepresentation:requestRepresentations){
					reqRepresentationList.add(reqRepresentation);
				}
				operation.setListRequestRepresentation(reqRepresentationList);
				
			}
			ArrayList<Representation> respRepresentationList=new ArrayList<Representation>();
			if(responseRepresentations.size() > 0) {
                 for(Representation responseRepresentation:responseRepresentations){
                	 respRepresentationList.add(responseRepresentation);
				}
				
                 operation.setListResponseRepresentation(respRepresentationList);
			}
			/*Chnaged by Leelaprasad for TENJINCG 404 starts*/
			return operation;
		}catch(Exception e) {
			logger.error("ERROR parsing resource method", e);
			throw new WadlException("Could not parse method due to an internal error. Please contact Tenjin support.");
		}
		
		
	}
	
	private Representation parseRepresentationNode(Node representationNode) {
		try{
			Representation representation = new Representation();
			representation.setMediaType(((Element) representationNode).getAttribute("mediaType"));
			List<Parameter> parameters = new ArrayList<Parameter> ();
			NodeList parameterNodes = ((Element) representationNode).getElementsByTagNameNS("*", "param");
			for(int p=0; p<parameterNodes.getLength(); p++) {
				Parameter param = this.parseParameterNode(parameterNodes.item(p));
				if(param != null) {
					parameters.add(param);
				}
			}
			
			representation.setParameters(parameters);
			
			return representation;
		}catch(Exception e) {
			logger.error("ERROR parsing representation node");
			throw new WadlException("Could not parse representation node.");
		}
	}
	
	private Parameter parseParameterNode(Node parameterNode) {
		try {
			Parameter param = new Parameter();
			
			Element parameterElement = (Element) parameterNode;
			param.setName(parameterElement.getAttribute("name"));
			param.setStyle(parameterElement.getAttribute("style"));
			param.setType(parameterElement.getAttribute("type"));
			
			return param;
		} catch(Exception e) {
			logger.error("ERROR parsing parameter node", e);
			throw new WadlException("Could not parse parameter node.");
		}
	}
	
	/*private String getMethodPath(Node methodNode) {
		String path = "";
		
		boolean reachedTop = false;
		
		Node currentNode = methodNode;
		
		List<String> paths = new ArrayList<String>();
		
		while(!reachedTop) {
			Node parentNode = currentNode.getParentNode();
			if(!Utilities.trim(parentNode.getLocalName()).equalsIgnoreCase("resources")) {
				if(Utilities.trim(parentNode.getLocalName()).equalsIgnoreCase("resource")) {
					paths.add(((Element) parentNode).getAttribute("path"));
				}
			}else {
				reachedTop = true;
			}
			
			currentNode = parentNode;
		}
		
		for(int i=(paths.size()-1); i >=0; i--) {
			path += paths.get(i);
		}
		
		return path;
	}*/
	
	private String getResourcePath(Node resourceNode) {
		String path = "";
		
		boolean reachedTop = false;
		
		Node currentNode = resourceNode;
		
		List<String> paths = new ArrayList<String>();
		paths.add(((Element) resourceNode).getAttribute("path"));
		while(!reachedTop) {
			Node parentNode = currentNode.getParentNode();
			if(!Utilities.trim(parentNode.getLocalName()).equalsIgnoreCase("resources")) {
				if(Utilities.trim(parentNode.getLocalName()).equalsIgnoreCase("resource")) {
					paths.add(((Element) parentNode).getAttribute("path"));
				}
			}else {
				reachedTop = true;
			}
			
			currentNode = parentNode;
		}
		
		for(int i=(paths.size()-1); i >=0; i--) {
			path += paths.get(i);
		}
		
		return path;
	}
	
	private static Document loadDocument(File xmlFile, boolean namespaceAware) {
		
		if(xmlFile == null) {
			logger.error("ERROR loading document - input file is null");
			return null;
		}
		
		logger.debug("Loading XML Document from path {}", xmlFile.getAbsolutePath());
		
		try {
			//DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance("com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl", ClassLoader.getSystemClassLoader());
			/*Added by Prem for VAPT fix starts*/
			factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			//factory.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, "true");
			factory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
			/*Added by Prem for VAPT fix ends*/
			factory.setNamespaceAware(true);
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			return builder.parse(new FileInputStream(xmlFile));
		} catch (FileNotFoundException e) {
			
			logger.error("ERROR loading XML document from file at path {}", xmlFile.getAbsolutePath(), e);
			return null;
		} catch (ParserConfigurationException e) {
			
			logger.error("ERROR parsing document", e);
			return null;
		} catch (SAXException e) {
			
			logger.error("ERROR parsing document", e);
			return null;
		} catch (IOException e) {
			
			logger.error("ERROR loading XML document from file at path {}", xmlFile.getAbsolutePath(), e);
			return null;
		}
	}
	
}
