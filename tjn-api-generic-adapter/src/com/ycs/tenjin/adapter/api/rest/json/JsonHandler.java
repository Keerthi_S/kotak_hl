/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  JsonHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 14-Nov-2017		Sriram Sridharan		Fixes for TENJINCG-430
   27-Nov-2017		Sriram Sridharan		Fixes for TENJINCG-514
   25-01-2019			Preeti					TJN252-80
 */
package com.ycs.tenjin.adapter.api.rest.json;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.ycs.tenjin.util.Utilities;

public class JsonHandler {

	private String baseJsonString;
	private String outputJsonString;
	private JsonObject baseJson;

	private File baseJsonFile;
	private boolean readFromFile;

	private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
	private static final Logger logger = LoggerFactory.getLogger(JsonHandler.class);

	public JsonHandler(String baseJsonString) {
		this.baseJsonString = baseJsonString;
		this.readFromFile = false;
	}

	public JsonHandler(File baseJsonFile) {
		this.readFromFile = true;
		this.baseJsonFile = baseJsonFile;
	}

	public static void main(String[] args) throws JsonParserException {
		JsonHandler json = new JsonHandler(new File("D:" + File.separator + "hierarchical_json.json"));
		String jsonRequest = json.buildJsonRequest();
		System.out.println(jsonRequest);

	}

	public Map<String, String> getResourceParameters() throws JsonParserException {
		Map<String, String> resourceParams = new HashMap<String, String>();

		logger.debug("Parsing base json");
		if (this.readFromFile) {
			this.baseJson = this.getJsonObject(this.baseJsonFile);
		} else {
			this.baseJson = this.getJsonObject(this.baseJsonString);
		}

		JsonArray resourceParamsArray = this.baseJson.getAsJsonArray("RESOURCE_PARAMS");
		if (resourceParamsArray != null && resourceParamsArray.size() > 0) {
			JsonObject resourceParamsObject = resourceParamsArray.get(0).getAsJsonObject();
			JsonArray fields = resourceParamsObject.get("FIELDS").getAsJsonArray();
			if (fields != null && fields.size() > 0) {
				for (JsonElement field : fields) {
					JsonObject fieldObj = field.getAsJsonObject();
					/* Modified by Preeti for TJN252-80 starts */
					/*
					 * resourceParams.put(fieldObj.get("FLD_LABEL_2").getAsString(),
					 * fieldObj.get("DATA").getAsString());
					 */
					resourceParams.put(fieldObj.get("FLD_LABEL").getAsString(), fieldObj.get("DATA").getAsString());
					/* Modified by Preeti for TJN252-80 ends */
				}
			}
		}
		return resourceParams;
	}

	private JsonObject getJsonObject(String jsonString) throws JsonParserException {
		try {
			JsonParser parser = new JsonParser();
			return parser.parse(jsonString).getAsJsonObject();
		} catch (JsonSyntaxException e) {

			logger.error("Could not parse JSON String", e);
			throw new JsonParserException("Could not parse JSON String");
		}
	}

	private JsonObject getJsonObject(File file) throws JsonParserException {
		try {
			JsonParser parser = new JsonParser();
			Reader reader = new FileReader(file);
			return parser.parse(reader).getAsJsonObject();
		} catch (JsonSyntaxException e) {

			logger.error("Could not parse JSON String", e);
			throw new JsonParserException("Could not parse JSON String");
		} catch (FileNotFoundException e) {

			logger.error("Could not parse JSON String", e);
			throw new JsonParserException("Could not parse JSON String");
		}
	}

	/**************
	 * Returns a Json Node representing the page specified by the path. Creates the
	 * page and all intermediate pages if they don't exist.
	 * 
	 * Algorithm: ---------- Split the path with "-->" Start scanning from the root
	 * Json object for each string within the path, 1. Check if the node exists. 1.1
	 * If the node doesn't exist, create a new node with key as the path string,
	 * value as new JsonObject() if isMrb is N, and new JsonArray() if isMrb is Y.
	 * 1.2 If the node exists 1.2.1 Get the type of node. Check if it is of
	 * JsonPrimitive, JsonObject or JsonArray. 1.2.2 If it is JsonPrimitive, convert
	 * it to JsonObject if isMrb is N and JsonArray if isMrb is Y 1.2.3 If it is
	 * JsonArray, then check if the path element is the last element in the array
	 * (have we reached our target node). 1.2.3.1 If yes, return the node as is.
	 * 1.2.3.2 If no, return the first element of the array.
	 * 
	 * 
	 * @param pagePath - Path of the page
	 * @param isMrb    - Is the page a single record page(N) or a multi record
	 *                 page(Y)
	 * @return JsonElement (JsonObject or a JsonArray representing the page).
	 */

	private JsonObject buildJsonObjectForFields(JsonArray fieldsTestDataArray) {
		JsonObject json = new JsonObject();
		for (JsonElement testDataFieldElement : fieldsTestDataArray) {
			JsonObject testDataField = testDataFieldElement.getAsJsonObject();
			/* Modified by Preeti for TJN252-80 starts */
			/*
			 * json.addProperty(testDataField.get("FLD_LABEL_2").getAsString(),
			 * testDataField.get("DATA").getAsString());
			 */
			json.addProperty(testDataField.get("FLD_LABEL").getAsString(), testDataField.get("DATA").getAsString());
			/* Modified by Preeti for TJN252-80 ends */
		}
		return json;
	}

	public JsonElement scanPageArea(JsonObject pageAreaTestDataObject) {
		JsonElement requestNode = null;

		String isMrb = pageAreaTestDataObject.get("PA_IS_MRB").getAsString();

		if (Utilities.trim(isMrb).length() < 1) {
			isMrb = "N";
		}
		JsonArray detailRecords = pageAreaTestDataObject.getAsJsonArray("DETAILRECORDS");
		// Create the Page Area Node
		if (isMrb.equalsIgnoreCase("Y")) {
			requestNode = this.buildJsonArrayForDetailRecords(detailRecords);
		} else {
			requestNode = this.buildJsonObjectForDetailRecord(detailRecords);
		}

		return requestNode;
	}

	private JsonObject buildJsonObjectForDetailRecord(JsonArray detailRecordsTestDataArray) {
		JsonObject detailRecordTestDataObject = detailRecordsTestDataArray.get(0).getAsJsonObject();
		JsonArray fields = detailRecordTestDataObject.getAsJsonArray("FIELDS");
		JsonObject reqFieldsJsonObject = this.buildJsonObjectForFields(fields);

		JsonArray childrenTestDataJsonArray = detailRecordTestDataObject.get("CHILDREN").getAsJsonArray();
		if (childrenTestDataJsonArray != null && childrenTestDataJsonArray.size() > 0) {
			for (JsonElement childTestDataElement : childrenTestDataJsonArray) {
				JsonObject childTestDataObject = childTestDataElement.getAsJsonObject();
				JsonElement reqChild = this.scanPageArea(childTestDataObject);
				String childPageName = childTestDataObject.get("PA_NAME").getAsString();
				reqFieldsJsonObject.add(childPageName, reqChild);
			}
		}

		return reqFieldsJsonObject;
	}

	private JsonArray buildJsonArrayForDetailRecords(JsonArray detailRecordsTestDataArray) {
		JsonArray array = new JsonArray();
		for (JsonElement detailRecord : detailRecordsTestDataArray) {
			JsonObject detailRecordTestDataObject = detailRecord.getAsJsonObject();
			JsonArray fields = detailRecordTestDataObject.getAsJsonArray("FIELDS");
			JsonObject reqFieldsJsonObject = this.buildJsonObjectForFields(fields);
			JsonArray childrenTestDataJsonArray = detailRecordTestDataObject.get("CHILDREN").getAsJsonArray();
			if (childrenTestDataJsonArray != null && childrenTestDataJsonArray.size() > 0) {
				for (JsonElement childTestDataElement : childrenTestDataJsonArray) {
					JsonObject childTestDataObject = childTestDataElement.getAsJsonObject();
					JsonElement reqChild = this.scanPageArea(childTestDataObject);
					String childPageName = childTestDataObject.get("PA_NAME").getAsString();
					reqFieldsJsonObject.add(childPageName, reqChild);
				}
			}
			array.add(reqFieldsJsonObject);
		}

		return array;

	}

	/*
	 * public String buildJsonRequest() throws JsonParserException{
	 * 
	 * logger.debug("Parsing base json"); if(this.readFromFile) { this.baseJson =
	 * this.getJsonObject(this.baseJsonFile); }else{ this.baseJson =
	 * this.getJsonObject(this.baseJsonString); }
	 * 
	 * JsonArray pageAreas = this.baseJson.getAsJsonArray("PAGEAREAS");
	 * 
	 * JsonElement data = this.scanPageArea(pageAreas.get(0).getAsJsonObject());
	 * if(data != null) { TENJINCG-430 this.outputJsonString =
	 * data.getAsJsonObject().toString(); if(data.isJsonArray()) {
	 * this.outputJsonString = data.getAsJsonArray().toString(); }else{
	 * this.outputJsonString = data.getAsJsonObject().toString(); } TENJINCG-430
	 * ends }
	 * 
	 * return this.outputJsonString; }
	 */
	// TENJINCG-514 ends

	public String getResponse(String encryptedJson) {
		encryptedJson = encryptedJson.replaceAll("\\s", "");
		CryptoUtils crypto = new CryptoUtils();
		JSONObject json = null;
		try {
			json = new JSONObject(encryptedJson);
			if (json.has("ResponseEncryptedValue")) {
				String encryptedSymmetricVal = json.getString("GWSymmetricKeyEncryptedValue");
				String decryptedSymmetricKey = crypto.decryptSymmetricKey(encryptedSymmetricVal);
				String encryptedRes = json.getString("ResponseEncryptedValue");
				String decryptedRes = crypto.decryptForAPI(encryptedRes, decryptedSymmetricKey);
				decryptedRes = decryptedRes.substring(decryptedRes.indexOf('{'), decryptedRes.length());
				return decryptedRes;
			} else if (json.has("ResponseSignatureEncryptedValue")) {

				String encryptedSymmetricVal = json.getString("GWSymmetricKeyEncryptedValue");
				String decryptedSymmetricKey = crypto.decryptSymmetricKey(encryptedSymmetricVal);
				String encryptedRes = json.getString("ResponseSignatureEncryptedValue");
				String decryptedRes = CryptoUtils.decryptResponse(encryptedRes, decryptedSymmetricKey);

				// String
				// decryptedRes=crypto.decrypt("CRYPTO_ALGORITHM",decryptedSymmetricKey,"",CryptoUtils.INIT_VECTOR);
				// decryptedRes=decryptedRes.substring(decryptedRes.indexOf('{'),decryptedRes.length());

				return decryptedRes;

				/*
				 * { "ResponseSignatureEncryptedValue": "", "GWSymmetricKeyEncryptedValue": "",
				 * "Scope": "", "TransactionId": "", "Status": "" }
				 */

			} else {
				return encryptedJson.toString();
			}
		} catch (JSONException e) {
			logger.error("Error" + e);
		}

		return null;
	}

	public String buildJsonRequest() throws JsonParserException {

		logger.debug("Parsing base json");
		if (this.readFromFile) {
			this.baseJson = this.getJsonObject(this.baseJsonFile);
		} else {
			this.baseJson = this.getJsonObject(this.baseJsonString);
		}

		JsonArray pageAreas = this.baseJson.getAsJsonArray("PAGEAREAS");

		JsonElement data = this.scanPageArea(pageAreas.get(0).getAsJsonObject());
		if (data != null) {
			/* TENJINCG-430 */
			/* this.outputJsonString = data.getAsJsonObject().toString(); */
			if (data.isJsonArray()) {
				this.outputJsonString = data.getAsJsonArray().toString();
			} else {
				this.outputJsonString = data.getAsJsonObject().toString();
				try {
					JsonObject json = new JsonObject();

					// String valToEncrypt=json.get("RequestEncryptedValue").toString();
					String encryptedValue = "";
					String encryptedSymmetricKey = "";
					String value = "";
					try {
						CryptoUtils crypto = new CryptoUtils();

						encryptedValue = crypto.encrypt(CryptoUtils.INIT_VECTOR + this.outputJsonString, true);
						encryptedSymmetricKey = crypto.encryptSymmetricKey();
						// String symmetricKey = CryptoUtils.encryptSymmetricKey();
						logger.debug("encryptedValue=" + encryptedValue);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.error("Error" + e);
					}

					json.addProperty("RequestEncryptedValue", encryptedValue);

					json.addProperty("SymmetricKeyEncryptedValue", encryptedSymmetricKey);
					json.addProperty("Scope", "ABC");
					json.addProperty("TransactionId", "12605202112ww");
					json.addProperty("OAuthTokenValue", "");
					// neena
					// this.outputJsonString = json.toString();
					JSONObject jsonObject = new JSONObject(this.outputJsonString);
					Iterator<String> keys = jsonObject.keys();

					while (keys.hasNext()) {
						String key = keys.next();

						try {
							value = jsonObject.getString(key);
						} catch (JSONException e) {
							logger.error("Error ", e);
						}
						json.addProperty(key, value);
						this.outputJsonString = json.toString();

					}
				} catch (Exception e) {

				}
			}
			/* TENJINCG-430 ends */
		}
		return this.outputJsonString;
	}

	public String buildJsonRequestForJWT(String accessToken) throws JsonParserException {

		logger.debug("Parsing base json");
		if (this.readFromFile) {
			this.baseJson = this.getJsonObject(this.baseJsonFile);
		} else {
			this.baseJson = this.getJsonObject(this.baseJsonString);
		}

		JsonArray pageAreas = this.baseJson.getAsJsonArray("PAGEAREAS");

		JsonElement data = this.scanPageArea(pageAreas.get(0).getAsJsonObject());
		if (data != null) {
			/* TENJINCG-430 */
			/* this.outputJsonString = data.getAsJsonObject().toString(); */
			if (data.isJsonArray()) {
				this.outputJsonString = data.getAsJsonArray().toString();
			} else {
				this.outputJsonString = data.getAsJsonObject().toString();
				CryptoUtils crypto = new CryptoUtils();
				try {
					JsonObject json = new JsonObject();
					Jwt jwt = new Jwt();
					PrivateKey privateKey = jwt.loadPrivateKey();
					/*
					 * String jwtToken = Jwts.builder() .setHeaderParam("typ", "JWT") .setClaims(new
					 * Gson().fromJson(this.outputJsonString, Map.class))
					 * .signWith(privateKey).compact();
					 */
					String jwtToken = jwt.getJWTString(this.outputJsonString, privateKey);

					System.out.println(jwtToken);

					// String valToEncrypt=json.get("RequestEncryptedValue").toString();
					String encryptedRequestBody = CryptoUtils.encrypt(CryptoUtils.INIT_VECTOR + jwtToken, true);
					String encryptedSymmetricKey = CryptoUtils.encryptSymmetricKey();

					JsonObject finalRequest = new JsonObject();
					finalRequest.addProperty("RequestSignatureEncryptedValue", encryptedRequestBody);
					finalRequest.addProperty("SymmetricKeyEncryptedValue", encryptedSymmetricKey);
					finalRequest.addProperty("Scope", "ABC");
					finalRequest.addProperty("TransactionId", "12345");
					finalRequest.addProperty("OAuthTokenValue", accessToken);
					this.outputJsonString = finalRequest.toString();
					System.out.println(finalRequest.toString());
					logger.debug("encryptedValue=" + encryptedRequestBody);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Error" + e);
				}

			}
			/* TENJINCG-430 ends */
		}
		return this.outputJsonString;
	}

	public String buildJsonRequest1() throws JsonParserException {
		logger.debug("Parsing base json");
		if (this.readFromFile) {
			this.baseJson = this.getJsonObject(this.baseJsonFile);
		} else {
			this.baseJson = this.getJsonObject(this.baseJsonString);
		}

		JsonArray pageAreas = this.baseJson.getAsJsonArray("PAGEAREAS");
		JsonElement data = null;

		JsonObject parentJson = new JsonObject();
		JsonObject requestJson = new JsonObject();

		
		String parentName = pageAreas.get(0).getAsJsonObject().get("PA_PARENT").getAsString();
		for (int i = 0; i < pageAreas.size(); i++) {
			data = this.scanPageArea(pageAreas.get(i).getAsJsonObject());
			requestJson=data.getAsJsonObject();
			//commented by Sumit
			String parentChild = pageAreas.get(i).getAsJsonObject().get("PA_NAME").getAsString();
			// checking for the parent paths if other than Root is present
			String pathParents = pageAreas.get(i).getAsJsonObject().get("PA_PARENT").getAsString();
			if(pathParents.equalsIgnoreCase("N/A")) {
				this.outputJsonString = requestJson.toString();
			   return this.outputJsonString;
			}
			else {
			JsonObject parentpath = new JsonObject();
			if ((!pathParents.equals("Root")) && !parentChild.isEmpty()) {
				if (!parentChild.equals("Root")) {
					parentpath.add(parentChild, data);
				}
				//String parentPath = pageAreas.get(i).getAsJsonObject().get("PA_PARENT").getAsString();
				parentJson.add(parentChild,data);
			} else {
				// checking if the page directly having path as root
				if (pathParents.equals("Root")) {
					parentJson.add(parentChild, data);
				} else if (!parentChild.equals("Root")) {
					requestJson.add(parentChild, data);
				}
			}
		
		if (!parentName.equals("Root") && !parentJson.has(parentName)) {
			requestJson.add(parentName, parentJson);
		} else {
			requestJson = parentJson;
		}


		if (requestJson.entrySet().isEmpty()) {
			if (data != null) {
				/* TENJINCG-430 */
				if (data.isJsonArray()) {
					this.outputJsonString = data.getAsJsonArray().toString();
				} else {
					this.outputJsonString = data.getAsJsonObject().toString();
				} /* TENJINCG-430 ends */
			}
		} else {
			this.outputJsonString = requestJson.toString();
		}
			}
		}

		return this.outputJsonString;
		/*
		 * logger.debug("Parsing base json"); if (this.readFromFile) { this.baseJson =
		 * this.getJsonObject(this.baseJsonFile); } else { this.baseJson =
		 * this.getJsonObject(this.baseJsonString); }
		 * 
		 * JsonArray pageAreas = this.baseJson.getAsJsonArray("PAGEAREAS");
		 * 
		 * for(JsonElement pageAreaElement : pageAreas) { JsonObject pageArea =
		 * pageAreaElement.getAsJsonObject(); String pageName =
		 * pageArea.get("PA_NAME").getAsString(); String rawPageName = pageName;
		 * if(!RESOURCE_PARAMETERS.equalsIgnoreCase(pageName)){
		 * this.scanPageArea2(pageArea); } }
		 * 
		 * 
		 * JsonElement data = this.scanPageArea(pageAreas.get(0).getAsJsonObject()); if
		 * (data != null) { TENJINCG-430 this.outputJsonString =
		 * data.getAsJsonObject().toString(); if (data.isJsonArray()) {
		 * this.outputJsonString = data.getAsJsonArray().toString(); } else {
		 * this.outputJsonString = data.getAsJsonObject().toString(); } TENJINCG-430
		 * ends }
		 * 
		 * return this.outputJsonString;
		 */
	}

	// TENJINCG-514 ends
	public String encrypt(String key, String xml) throws GeneralSecurityException, UnsupportedEncodingException {
		try {
			// String key = TenjinConfiguration.getProperty("ENCRYPTION_KEY");
			/* String key = "6fa5b77373589af97d9eeb9e99edd1c8"; */

			/*
			 * String xml = null; logger.debug("Parsing base json"); if (this.readFromFile)
			 * { this.baseJson = this.getJsonObject(this.baseJsonFile); } else {
			 * this.baseJson = this.getJsonObject(this.baseJsonString); }
			 * 
			 * JsonArray pageAreas = this.baseJson.getAsJsonArray("PAGEAREAS");
			 */

			// JsonElement data = this.scanPageArea(pageAreas.get(0).getAsJsonObject());

			// if (data != null) {

			//// JsonObject jsonObject = data.getAsJsonObject();
			/// String xml1 = XML.toString(new JSONObject(jsonObject.toString()));
			// xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<CreateLeadReq>" + xml1 +
			//// "</CreateLeadReq>";
			// xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + xml1;
			/*
			 * xml="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + "	<Header>\r\n" +
			 * "		<SrcAppCd>CRMNext</SrcAppCd>\r\n" +
			 * "		<RequestID>62729</RequestID>\r\n" + "	</Header>\r\n" +
			 * "	<CreateLead>\r\n" + "		<City>Pune</City>\r\n" +
			 * "		<Custom>\r\n" +
			 * "			<Address_Line_1_Le>508  Paritosh Appt Asthavinayak Chowk Balewadi</Address_Line_1_Le>\r\n"
			 * + "			<Address_Line_P1_Le></Address_Line_P1_Le>\r\n" +
			 * "			<Remarks_Le>TEST</Remarks_Le>\r\n" +
			 * "			<Address_Type_Le>Branch</Address_Type_Le>\r\n" +
			 * "			<Source_SystemID>101</Source_SystemID>\r\n" +
			 * "			<Process_Type>N</Process_Type>\r\n" +
			 * "			<Existing_Customer>No</Existing_Customer>\r\n" +
			 * "			<Resident_Type>R</Resident_Type>\r\n" +
			 * "			<Sub_Source>DSA</Sub_Source>\r\n" +
			 * "			<Individual>I</Individual>\r\n" +
			 * "			<property_Le>Residential</property_Le>\r\n" +
			 * "			<LEA_Prospect>Lead</LEA_Prospect>\r\n" + "		</Custom>\r\n" +
			 * "		<Email>as@test.com</Email>\r\n" +
			 * "		<FirstName>ANay</FirstName>\r\n" +
			 * "		<LastName>Seth</LastName>\r\n" +
			 * "		<LayoutKey>111206</LayoutKey>\r\n" +
			 * "		<LeadAmount>25000</LeadAmount>\r\n" +
			 * "		<LeadSourceKey>25</LeadSourceKey>\r\n" +
			 * "		<MobilePhone>9393833737</MobilePhone>\r\n" +
			 * "		<ProductKey>126</ProductKey>\r\n" +
			 * "		<RatingKey>2</RatingKey>\r\n" +
			 * "		<StatusCodeKey>100012</StatusCodeKey>\r\n" + "	</CreateLead>\r\n";
			 */
			/*
			 * if (xml1 == null || key == null) { throw new
			 * IllegalArgumentException("text to be encrypted and key should not be null");
			 * }
			 */

			Cipher cipher = Cipher.getInstance(ALGORITHM);
			byte[] messageArr = xml.getBytes();
			byte[] keyparam = key.getBytes();
			SecretKeySpec keySpec = new SecretKeySpec(keyparam, "AES");
			byte[] ivParams = new byte[16];
			byte[] encoded = new byte[messageArr.length + 16];
			System.arraycopy(ivParams, 0, encoded, 0, 16);
			System.arraycopy(messageArr, 0, encoded, 16, messageArr.length);
			cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(ivParams));
			byte[] encryptedBytes = cipher.doFinal(encoded);
			// encryptedBytes = Base64.getEncoder().encode(encryptedBytes);
			encryptedBytes = Base64.encodeBase64(encryptedBytes);
			return new String(encryptedBytes);
			// }
		} catch (Exception e) {
		}
		return null;
	}

	public String decrypt(String encryptedStr, String key)
			throws GeneralSecurityException, UnsupportedEncodingException {
		try {
			// String key = TenjinConfiguration.getProperty("ENCRYPTION_KEY");
			/* String key = "6fa5b77373589af97d9eeb9e99edd1c8"; */
			if (encryptedStr == null || key == null) {
				throw new IllegalArgumentException("text to be decrypted and key should not be null");
			}
			if (encryptedStr.contains("HTTP Response Code :: ")) {
				return encryptedStr;
			}
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			byte[] keyparam = key.getBytes();
			SecretKeySpec keySpec = new SecretKeySpec(keyparam, "AES");
			byte[] encoded = encryptedStr.getBytes();
			// encoded = Base64.getDecoder().decode(encoded);
			encoded = Base64.decodeBase64(encoded);
			byte[] decodedEncrypted = new byte[encoded.length - 16];
			System.arraycopy(encoded, 16, decodedEncrypted, 0, encoded.length - 16);
			byte[] ivParams = new byte[16];
			System.arraycopy(encoded, 0, ivParams, 0, ivParams.length);
			cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(ivParams));
			byte[] decryptedBytes = cipher.doFinal(decodedEncrypted);
			return new String(decryptedBytes);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
