/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RestApiBridgeImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 14-Nov-2017		Sriram Sridharan		Fixes for TENJINCG-430
 * 20-Nov-2017		Gangadhar Badagi		    TENJINCG-478
 * 02-Feb-2018			Sriram					TENJINCG-415
 * 12-12-2018		Sriram Sridharan			TENJINCG-913
 * 22-03-2019			Preeti					TENJINCG-1018
 * 17-11-2020		Pushpalatha					TENJINCG-1228
 * 18-11-2020       Paneendra					TENJINCG-1239
 */


package com.ycs.tenjin.adapter.api.rest.generic;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.ycs.tenjin.adapter.api.rest.client.RestClientException;
import com.ycs.tenjin.adapter.api.rest.client.RestRequest;
import com.ycs.tenjin.adapter.api.rest.client.RestResponse;
import com.ycs.tenjin.adapter.api.rest.json.JsonHandler;
import com.ycs.tenjin.adapter.api.soap.generic.GenericSOAPUtils;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.oem.api.ApiApplication;
import com.ycs.tenjin.bridge.oem.api.generic.ApiLearnType;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.ApplicationUser;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Metadata;
import com.ycs.tenjin.bridge.pojo.aut.Parameter;
import com.ycs.tenjin.bridge.pojo.aut.Representation;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.utils.ApiUtilities;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.OAuthHelper;
import com.ycs.tenjin.util.Utilities;

public class RestApiBridgeImpl implements ApiApplication {

	private static final Logger logger = LoggerFactory.getLogger(RestApiBridgeImpl.class);
	private static Set<Class<?>> WRAPPER_TYPES;
	private static final String ROOT_LOCATION_NAME = "Root";

	private String authenticationType = "Basic";

	private List<TestObject> testObjects;
	private ArrayList<TreeMap<String, TestObject>> testObjectMap;
	private Multimap<String, Location> pageAreas;

	private int pageSequence;
	private int fieldSequence;
	private int tabOrder;

	public String tdUid;
	@SuppressWarnings("unused")
	private int iteration;
	private int runId;
	private ExecutionStep step;
	private String uniqueLocationName;
	public static boolean parentFound=false;

	/*	Added By Paneendra for TENJINCG-1239 starts */
	Map<String, String> headers = new HashMap<String, String>();
	/*	Added By Paneendra for TENJINCG-1239 ends */



	@Override
	public void initialize() {

		this.testObjectMap = new ArrayList<TreeMap<String, TestObject>>();
		this.pageAreas = ArrayListMultimap.create();
		this.testObjects = new ArrayList<TestObject> ();
		this.pageSequence=-1;
		this.fieldSequence=-1;
		this.tabOrder = -1;
		WRAPPER_TYPES = getWrapperTypes();
	}

	public RestApiBridgeImpl() {

		this.initialize();
	}

	public RestApiBridgeImpl(int runId, ExecutionStep step) {
		this.runId = runId;
		this.step = step;
	}

	@Override
	public String getAdapterName() {

		return null;
	}

	/*@Override
	public Metadata learn(Api api, ApiOperation operation, String learnType, String entityType)
			throws BridgeException {

		String rDescriptor = "";
		if(Utilities.trim(learnType).equalsIgnoreCase(ApiLearnType.URL.toString())){
			throw new BridgeException("Unsupported Learning Type for REST APIs");
		}else if(Utilities.trim(learnType).equalsIgnoreCase(ApiLearnType.REQUEST_XML.toString())){
			if(Utilities.trim(entityType).equalsIgnoreCase("request")) {
				rDescriptor = operation.getRequestDescriptor();
			} else if(Utilities.trim(entityType).equalsIgnoreCase("response")) {
				rDescriptor = operation.getResponseDescriptor();
			}
		}

		Metadata metadata = null;

		if(Utilities.trim(entityType).equalsIgnoreCase("request")){
			this.parseOperationParameters(operation);
		}

		if(Utilities.trim(rDescriptor).length() > 0) {

			try {
				if(Utilities.trim(entityType).equalsIgnoreCase("request")) {
					metadata = this.learn(rDescriptor, operation.getRequestRepresentation().getMediaType());
				}else if(Utilities.trim(entityType).equalsIgnoreCase("response")) {
					metadata = this.learn(rDescriptor, operation.getResponseRepresentation().getMediaType());
				}
			} catch (Exception e) {

				logger.error("Could not parse {} descriptor for this operation.", entityType, e);
				throw new BridgeException("Could not parse " + entityType + " descriptor for this operation.");
			}

			if(metadata != null) {
				metadata.setDescriptor(rDescriptor);
			}
		}else{
			logger.warn("{} Descriptor is empty. Nothing to learn", entityType);
		}

		if(metadata == null) {
			metadata = new Metadata();
			metadata.setPageAreas(this.pageAreas);
			metadata.setTestObjectMap(this.testObjectMap);
		}

		return metadata;
	}
	 */
	@Override
	public ArrayList<Metadata> learn(Api api, ApiOperation operation, String learnType, String entityType)
			throws BridgeException {
		ArrayList<Metadata> metaDataList = new ArrayList<Metadata>();


		ArrayList<Representation> representationList=null;
		if(Utilities.trim(learnType).equalsIgnoreCase(ApiLearnType.URL.toString())){
			throw new BridgeException("Unsupported Learning Type for REST APIs");
		}else if(Utilities.trim(learnType).equalsIgnoreCase(ApiLearnType.REQUEST_XML.toString())){
			if(Utilities.trim(entityType).equalsIgnoreCase("request")) {

				representationList=operation.getListRequestRepresentation();

			}  else if(Utilities.trim(entityType).equalsIgnoreCase("response")) {

				representationList=operation.getListResponseRepresentation();

				/*	Added By Paneendra for TENJINCG-1239 starts */
				///****  for header representation ****//
			} else  {

				representationList=operation.getListHeaderRepresentation();
			}

			/*	Added By Paneendra for TENJINCG-1239 ends */
		}

		Metadata metadata = null;

		if(Utilities.trim(entityType).equalsIgnoreCase("request")){
			this.parseOperationParameters(operation);
		}


		if(representationList!=null && representationList.size()>0) {

			for(Representation representation:representationList){
				if(representation.getRepresentationDescription()!=null) {
					this.pageSequence = -1;

					try {

						if(Utilities.trim(entityType).equalsIgnoreCase("request")) {
							metadata = this.learn(representation.getRepresentationDescription(), representation.getMediaType());


						}else if(Utilities.trim(entityType).equalsIgnoreCase("response")) {
							metadata = this.learn(representation.getRepresentationDescription(), representation.getMediaType());

						}
						///****  for header representation ****//

						/*	Added By Paneendra for TENJINCG-1239 starts */
						else  {
							metadata = this.learn(representation.getRepresentationDescription(), representation.getMediaType());


						}
						/*	Added By Paneendra for TENJINCG-1239 ends */

					} catch (Exception e) {

						logger.error("Could not parse {} descriptor for this operation.", entityType, e);
						throw new BridgeException("Could not parse " + entityType + " descriptor for this operation.");
					}

					if(metadata != null) {
						metadata.setDescriptor(representation.getRepresentationDescription());
					}
					/* if(metadata == null) {
				metadata = new Metadata();
				metadata.setPageAreas(this.pageAreas);
				metadata.setTestObjectMap(this.testObjectMap);
			}*/
					metaDataList.add(metadata);
				}else{
					logger.warn("{} Descriptor is empty. Nothing to learn", entityType);
				}
			}
		}else{
			logger.warn("{} Descriptor is empty. Nothing to learn", entityType);
		}
		if(metaDataList.isEmpty()) {
			metadata = new Metadata();
			metadata.setPageAreas(this.pageAreas);
			metadata.setTestObjectMap(this.testObjectMap);
			metaDataList.add(metadata);
		}
		return metaDataList;
	}

	@Override
	public Metadata learn(Api api, ApiOperation operation, String entityType) throws BridgeException {

		return null;
	}

	@Override
	public Metadata learn(Api api, String entityType) throws BridgeException {

		return null;
	}

	@Override
	public List<ApiOperation> getAllOperations(String url) throws BridgeException {

		return null;
	}

	@Override
	public void getAllOperations(Api api) throws BridgeException {


	}





	@Override
	public IterationStatus executeIteration(JSONObject dataSet, JSONObject headerdataset,int iteration, int expectedResponseCode /* Parameter added by Sriram for TENJINCG-913*/) throws BridgeException {



		logger.info("Executing iteration [{}]", iteration);

		this.tdUid = "";
		IterationStatus iStatus = new IterationStatus();
		iStatus.setRuntimeFieldValues(new ArrayList<RuntimeFieldValue>());
		this.iteration = iteration;

		String apiUrl = "";
		String autBaseUrl = ApiUtilities.getApplicationBaseUrl(step.getAut().getURL());
		if(step.getApi() != null) {
			apiUrl = step.getApi().getUrl();
		}else {
			logger.error("Step API is null");
			iStatus.setResult("E");
			iStatus.setMessage("An internal error occurred. Please contact Tenjin Support.");
			return iStatus;
		}

		if(this.step.getApiOperation() != null) {
		}else{
			iStatus.setResult("E");
			iStatus.setMessage("Operation is invalid. Please ensure API is set up property.");
			return iStatus;
		}

		logger.info("Beginning execution of Transaction [{}]", tdUid);

		try{
			JsonHandler jsonHandler = new JsonHandler(dataSet.toString());
			/*	Added By Paneendra for TENJINCG-1239 starts */
			JsonHandler jsonheaderHandler = new JsonHandler(headerdataset.toString());
			/*	Added By Paneendra for TENJINCG-1239 ends */
			String opMethodName = Utilities.trim(this.step.getApiOperation().getMethod());

			//TENJINCG-415
			this.authenticationType = this.step.getApiOperation().getAuthenticationType();
			//TENJINCG-415 ends

			logger.debug("Operation Method --> [{}]", opMethodName);
			String requestBody = "";
			String requestMediaType ="";
			/*	Added By Paneendra for TENJINCG-1239 starts */
			String headerBody = "";
			String headerMediaType ="";
			/*	Added By Paneendra for TENJINCG-1239 ends */
			String encrypt_Key= step.getApi().getEncryptionKey();
			if(opMethodName.equalsIgnoreCase("post") || opMethodName.equalsIgnoreCase("put")) {
				/*Representation requestRepresentation = this.step.getApiOperation().getRequestRepresentation();*/
				Representation requestRepresentation=this.step.getApiOperation().getListRequestRepresentation().get(0);

				if(requestRepresentation != null) {
					requestMediaType = requestRepresentation.getMediaType();
					if(Utilities.trim(requestMediaType).length() < 1) {
						logger.warn("No Request representation set. Assuming media type for request to be application/json by default");
						requestMediaType = "application/json";
					}

					logger.debug("Request Media Type is [{}]", requestMediaType);
					logger.info("Building request for media type {}", requestMediaType);

					if("application/json".equalsIgnoreCase(requestMediaType)) {
						String api_Url= step.getApi().getUrl();
						String encrypt_Type= step.getApi().getEncryptionType();
						if(encrypt_Type.equalsIgnoreCase("high")) {
							//if(api_Url.contains("API/Broker_FundTransfer")) {
							Representation headerRepresentation=this.step.getApiOperation().getListHeaderRepresentation().get(0);
							String representationDescription=headerRepresentation.getRepresentationDescription();
							headerMediaType = headerRepresentation.getMediaType();
							String access_token="";
							if("application/json".equalsIgnoreCase(headerMediaType) && representationDescription!=null) {
								headerBody = jsonheaderHandler.buildJsonRequest1();
								JSONObject json=new JSONObject(headerBody);
								access_token=(String) json.get("HDFC_OAUTH");
								logger.debug("Successfully built header for media type {}", headerMediaType);
							}
							requestBody = jsonHandler.buildJsonRequestForJWT(access_token);
						}/*else if(api_Url.contains("_route")){*/
						else if(encrypt_Type.equalsIgnoreCase("medium")) {
							requestBody = jsonHandler.buildJsonRequest1();
							requestBody = jsonHandler.encrypt(encrypt_Key,requestBody);
						}else {
							requestBody = jsonHandler.buildJsonRequest1();
						}
						logger.debug("Successfully built request for media type {}", requestMediaType);
					}

					/**
					 * Added by sharath for XML Execution starts
					 */
					else if("application/xml".equalsIgnoreCase(requestMediaType)) {
						// Handling SOAP format for XML

						/*this.tdUid = dataSet.getString("TDUID");

						logger.info("Beginning execution of Transaction [{}]", tdUid);
						logger.info("Building request...");

						GenericSOAPUtils util = new GenericSOAPUtils();
						JSONArray pageAreas = dataSet.getJSONArray("PAGEAREAS");

						requestBody = util.buildXMLRequest(tdUid, pageAreas, requestRepresentation.getRepresentationDescription());*/

						// Handling JSON format for XML

						//GenericSOAPUtils util = new GenericSOAPUtils();
						//requestBody = jsonHandler.buildJsonRequest();
						//requestBody = util.buildXMLRequest(dataSet, new File(requestRepresentation.getRepresentationDescription()));

						/*try {
							JSONArray pageAreas = dataSet.getJSONArray("PAGEAREAS");
							logger.info("This test has data across {} page areas", pageAreas.length());

							for(int i=0; i<pageAreas.length(); i++) {
								util.scanPageArea(pageAreas.getJSONObject(i));
							}

							this.tdUid = dataSet.getString("TDUID");
						} catch(JSONException e) {

						}

						requestBody = util.getStringFromDocument(this.requestDocument);*/
						String encrypt_Type= step.getApi().getEncryptionType();

						if(encrypt_Type.equalsIgnoreCase("medium")) {
							//requestBody = jsonHandler.buildJsonRequest();
							this.tdUid = dataSet.getString("TDUID");

							logger.info("Beginning execution of Transaction [{}]", tdUid);
							logger.info("Building request...");

							GenericRestUtils util = new GenericRestUtils();
							JSONArray pageAreas = dataSet.getJSONArray("PAGEAREAS");

							String requestXml = util.buildXMLRequest(tdUid, pageAreas, requestRepresentation.getRepresentationDescription());
							requestBody = jsonHandler.encrypt(encrypt_Key,requestXml);
						}else {
							requestBody = jsonHandler.buildJsonRequest1();
						}
						//requestBody = jsonHandler.buildJsonRequest();
						//JSONObject jsoObject = new JSONObject(requestBody);
						//requestBody = XML.toString(jsoObject);
						logger.debug("Successfully built request for media type {}", requestMediaType);
					}
					/**
					 * Added by sharath for XML Execution Ends
					 */

					logger.info("Request data would be: {}", requestBody);
				}else{
					logger.info("No Request Representation found.");
				}

			}

			/*	Added By Paneendra for TENJINCG-1239 starts */


			if("Apikey".equalsIgnoreCase(this.authenticationType)) {
				if(opMethodName.equalsIgnoreCase("post") || opMethodName.equalsIgnoreCase("put")||opMethodName.equalsIgnoreCase("get")) {
					Representation headerRepresentation=this.step.getApiOperation().getListHeaderRepresentation().get(0);

					if(headerRepresentation != null) {
						headerMediaType = headerRepresentation.getMediaType();
						if(Utilities.trim(headerMediaType).length() < 1) {
							logger.warn("No Header representation set. Assuming media type for request to be application/json by default");
							headerMediaType = "application/json";
						}

						logger.debug("Header Media Type is [{}]", headerMediaType);
						logger.info("Building header for media type {}", headerMediaType);

						if("application/json".equalsIgnoreCase(headerMediaType)) {
							headerBody = jsonheaderHandler.buildJsonRequest1();
							logger.debug("Successfully built header for media type {}", headerMediaType);
						}


						else if("application/xml".equalsIgnoreCase(headerMediaType)) {
							/*// Handling SOAP format for XML

						this.tdUid = dataSet.getString("TDUID");

						logger.info("Beginning execution of Transaction [{}]", tdUid);
						logger.info("Building header...");

						GenericSOAPUtils util = new GenericSOAPUtils();
						JSONArray pageAreas = dataSet.getJSONArray("PAGEAREAS");

						headerBody = util.buildXMLRequest(tdUid, pageAreas, headerRepresentation.getRepresentationDescription());

						// Handling JSON format for XML

						//GenericSOAPUtils util = new GenericSOAPUtils();
						//headerBody = jsonHandler.buildJsonRequest();
						//headerBody = util.buildXMLRequest(dataSet, new File(headerRepresentation.getRepresentationDescription()));

						try {
							JSONArray pageAreas = dataSet.getJSONArray("PAGEAREAS");
							logger.info("This test has data across {} page areas", pageAreas.length());

							for(int i=0; i<pageAreas.length(); i++) {
								util.scanPageArea(pageAreas.getJSONObject(i));
							}

							this.tdUid = dataSet.getString("TDUID");
						} catch(JSONException e) {

						}

						headerBody = util.getStringFromDocument(this.requestDocument);*/

							headerBody = jsonHandler.buildJsonRequest();
							JSONObject jsoObject = new JSONObject(headerBody);
							headerBody = XML.toString(jsoObject);
							logger.debug("Successfully built request for media type {}", headerMediaType);
						}


						logger.info("Header data would be: {}", headerBody);
					}else{
						logger.info("No Header Representation found.");
					}

					/*	Added By Paneendra for TENJINCG-1239 ends */
				}
			}else {
				logger.info("Authentication type is not apikey,skipping headerbody");
			}

			logger.info("Resolving other parameters");
			Map<String, String> resourceParamters = jsonHandler.getResourceParameters();

			logger.info("Creating authorization header");
			String authHeader = "";
			if("basic".equalsIgnoreCase(this.authenticationType)) {
				authHeader = this.getBasicAuthorizationHeader();
			}
			//Code changes for OAuth 2.0 requirement TENJINCG-1018- Avinash - Starts
			else if("oauth2".equalsIgnoreCase(this.authenticationType)){
				Aut aut = validateToken();
				step.setAut(aut);
				authHeader = aut.getAccessToken();
			}
			/*	Added By Paneendra for TENJINCG-1239 starts */
			else if("Apikey".equalsIgnoreCase(this.authenticationType)){


				authHeader="Api Key";
				headers=this.getXapikeyAuthorizationHeader(headerBody);
			}
			/*	Added By Paneendra for TENJINCG-1239 ends */
			if("oauth2".equalsIgnoreCase(this.authenticationType)){
				if("URL".equalsIgnoreCase(step.getAut().getAddAuthDataTo())){
					resourceParamters.put("access_token", authHeader);
				}else{
					authHeader = "Bearer "+authHeader;
				}
			}
			//Code changes for OAuth 2.0 requirement TENJINCG-1018- Avinash - Ends

			logger.info("Creating REST request");
			RestRequest request = new RestRequest();
			request.setBaseUrl(this.step.getAut().getApiBaseUrl());
			request.setPath(apiUrl);
			request.setMethod(opMethodName);
			request.setPathParameters(resourceParamters);
			request.setRequestData(requestBody);
			request.setRequestMediaType(requestMediaType);
			/*	Added By Paneendra for TENJINCG-1239 starts */
			request.setHeaders(headers);
			/*	Added By Paneendra for TENJINCG-1239 ends */
			/*Changes by Avinash for TENJINCG-1018 starts*/
			if(Utilities.trim(authHeader).length() > 0 && !"URL".equalsIgnoreCase(step.getAut().getAddAuthDataTo())) {
				request.getHeaders().put("Authorization", authHeader);
			}
			/*Changes by Avinash for TENJINCG-1018 ends*/
			iStatus.setApiRequestDescriptor(requestBody);



			logger.info("Sending Request");
			RestResponse response = request.sendRequest();
			iStatus.setMessage("HTTP Status [" + response.getStatus() + "]");
			Gson gson = new Gson();
			/*Added by Pushpa for TENJINCG-1228 starts*/


			RestRequest requestLog = new RestRequest();

			requestLog.setMethod(opMethodName);
			requestLog.setPathParameters(resourceParamters);
			requestLog.setRequestMediaType(requestMediaType);

			if(Utilities.trim(authHeader).length() > 0 && !"URL".equalsIgnoreCase(step.getAut().getAddAuthDataTo())) {
				requestLog.getHeaders().put("Authorization", authHeader);
			}
			requestLog.setRequestUrl(response.getFinalUrl());
			String reqLog = gson.toJson(requestLog);
			iStatus.setReqLog(reqLog);
			/*Added by Pushpa for TENJINCG-1228 ends*/



			//Changed by Sriram for TENJINCG-913
			/*if(response.getStatus() >=400 && response.getStatus() <600) {
				iStatus.setResult("F");
				iStatus.setMessage("HTTP Status [" + response.getStatus() + "]");
			}else{
				iStatus.setResult("S");
				iStatus.setMessage("HTTP Status [" + response.getStatus() + "]");
			}*/

			if(expectedResponseCode == 0) {
				//No expected response is defined. Use default logic to determine if step passed or failed
				if(response.getStatus() >=400 && response.getStatus() <600) {
					iStatus.setResult("F");
				}else{
					iStatus.setResult("S");
				}
				
			}else if(expectedResponseCode == response.getStatus()) {
				iStatus.setResult("S"); //Expected response code is same as actual
			}else {
				iStatus.setResult("F"); //Expected response code is not same as actual
			}
			//Changed by Sriram for TENJINCG-913 ends


			String responseJSON=(String) response.getResponseData();
			String decryptedResponse="";
			//String decryptedResponse=jsonHandler.getResponse(responseJSON);
			if(encrypt_Key.equalsIgnoreCase("NA")) {
				decryptedResponse = responseJSON;
				iStatus.setApiResponseDescriptor(responseJSON);
			}else {
			decryptedResponse=jsonHandler.decrypt(responseJSON,encrypt_Key);
			//decryptedResponse=decryptedResponse.substring(decryptedResponse.indexOf('{'),decryptedResponse.length());
			iStatus.setApiResponseDescriptor(decryptedResponse);
			//iStatus.setApiResponseDescriptor(responseJSON);
			}
			//Keerthi Starts
			if (response.getStatus() == 200 ) {//&& this.step.getType().equalsIgnoreCase("negative")
				if (requestMediaType.equalsIgnoreCase("application/xml")) {
					this.getPostExecutionResult(iStatus, decryptedResponse);
				} /*
					 * else { this.getJsonPostExecutionResult(iStatus, decryptedResponse); }
					 */
			}//Keerthi Ends
		} catch(RestClientException e) {
			logger.error(e.getMessage(), e);
			iStatus.setResult("E");
			iStatus.setMessage(e.getMessage());
		} catch(Exception e) {
			logger.error("An unhandled exception occurred while executing this transaction", e);
			iStatus.setResult("E");
			iStatus.setMessage("An internal error occurred while executing this transaction. Please contact Tenjin Support.");
		}



		return iStatus;
	}
	
	//Keerthi Starts
	protected void getJsonPostExecutionResult(IterationStatus iStatus, String responseJson) throws BridgeException {
		try {
			JSONObject jsonObj = new JSONObject(responseJson);
			String extract = recurseKeys(jsonObj, "Status");
			logger.debug("Getting The Response Status..");
			try {
				// JSONObject jsonObjloc = new JSONObject(jsonObj.getString("status"));
				if (jsonObj != null) {
					String msgStat;
					try {
						msgStat = jsonObj.getString("Status");
					} catch (Exception e) {
						msgStat = recurseKeys(jsonObj, "Status");
					}
					if (!msgStat.equalsIgnoreCase("1") && !msgStat.equalsIgnoreCase("Success")) {
						iStatus.setResult("F");
						String status;
						try {
							status = jsonObj.getString("status");
						} catch (Exception e) {
							status = jsonObj.getString("Status");
						}
						String error = jsonObj.getString("ResCode");
						RuntimeFieldValue rfv = this.getRuntimeFieldValueObject("status : ", status, 1, this.tdUid);
						RuntimeFieldValue rfv1 = this.getRuntimeFieldValueObject("code : ", error, 2, this.tdUid);
						iStatus.getRuntimeFieldValues().add(rfv);
						iStatus.getRuntimeFieldValues().add(rfv1);
						iStatus.setMessage(status + " - " + error);
					} else {
						iStatus.setResult("S");
						iStatus.setMessage("Test Executed Successfully: Code || " + msgStat);
					}
				}
			} catch (Exception e) {
				iStatus.setResult("F");
				String errorCode = jsonObj.getString("status");
				RuntimeFieldValue rfv = this.getRuntimeFieldValueObject("Status : ", errorCode, 1, this.tdUid);
				iStatus.getRuntimeFieldValues().add(rfv);
				iStatus.setMessage(errorCode);
			}
		} catch (Exception e) {
			logger.error("ERROR in getPostExecutionResult", e);
			//iStatus.setResult("E");
			//iStatus.setMessage(
					//"Could not determine the result of this transaction due to an internal error. Please contact Tenjin Support.");
		}
	}
	
	public static String recurseKeys(JSONObject jObj, String findKey) throws JSONException {
	    String finalValue = "";
	    if (jObj == null) {
	        return "";
	    }

	    Iterator<String> keyItr = jObj.keys();
	    Map<String, String> map = new HashMap<>();

	    while(keyItr.hasNext()) {
	        String key = keyItr.next();
	        map.put(key, jObj.getString(key));
	    }

	    for (Map.Entry<String, String> e : (map).entrySet()) {
	        String key = e.getKey();
	        if (key.equalsIgnoreCase(findKey)) {
	            return jObj.getString(key);
	        }

	        // read value
	        Object value = jObj.get(key);

	        if (value instanceof JSONObject) {
	            finalValue = recurseKeys((JSONObject)value, findKey);
	        }
	    }

	    // key is not found
	    return finalValue;
	}
	
	
		protected void getPostExecutionResult(IterationStatus iStatus, String responseXML) throws BridgeException {
		try {
		Document document = GenericSOAPUtils.loadDocument(responseXML, true);
		logger.debug("Getting The Response Status..");
		Node headerNode;
		
			headerNode = document.getDocumentElement().getElementsByTagNameNS("*", "responseCode").item(0);
			if(headerNode==null)
				headerNode = document.getDocumentElement().getElementsByTagNameNS("*", "error_code").item(0);
			if(headerNode==null)
				headerNode = document.getDocumentElement().getElementsByTagNameNS("*", "errorcode").item(0);
		String msgStat = headerNode.getTextContent();
		//Node msgStatNode = document.getDocumentElement().getElementsByTagNameNS("*", "txtstatus").item(0);
		//Utilities.trim(msgStatNode.getTextContent());
		if (!msgStat.equalsIgnoreCase("0")) {
		iStatus.setResult("F");
		//Element bodyElement = (Element) document.getDocumentElement().getElementsByTagNameNS("*", "codstatus").item(0);
		try {
			Element errorResponseElement=null;
			 errorResponseElement = (Element) document.getDocumentElement().getElementsByTagNameNS("*", "error_desc").item(0);
			if(errorResponseElement==null)
				 errorResponseElement = (Element) document.getDocumentElement().getElementsByTagNameNS("*", "errordesc").item(0);

			String errorMSG = Utilities.trim(errorResponseElement.getTextContent());
			RuntimeFieldValue rfv1 = this.getRuntimeFieldValueObject("errormsg : ", errorMSG, 2, this.tdUid);
			iStatus.getRuntimeFieldValues().add(rfv1);
		} catch (Exception e) {
			logger.info("Could not get the error message");
			}
		//String replayText = Utilities.trim(errorResponseElement.getTextContent());
		RuntimeFieldValue rfv = this.getRuntimeFieldValueObject("errorcode : ", msgStat, 1, this.tdUid);
		//RuntimeFieldValue rfv1 = this.getRuntimeFieldValueObject("txtstatus : ", replayText, 2, this.tdUid);
		iStatus.getRuntimeFieldValues().add(rfv);
		//iStatus.getRuntimeFieldValues().add(rfv1);
		iStatus.setMessage(msgStat);
			} else {
				iStatus.setResult("S");
				iStatus.setMessage("Test Executed Successfully: " + msgStat);
			}
		} catch (Exception e) {
			logger.error("ERROR in getPostExecutionResult", e);
			//iStatus.setResult("E");
			//iStatus.setMessage("Could not determine the result of this transaction due to an internal error. Please contact Tenjin Support.");
		}
		}
		
		protected RuntimeFieldValue getRuntimeFieldValueObject(String field, String value, int detailRecordNo,String tdUid) {
		RuntimeFieldValue r = new RuntimeFieldValue();
		r.setDetailRecordNo(1);
		r.setField(field);
		r.setIteration(this.iteration);
		r.setRunId(this.runId);
		r.setTdGid(this.step.getTdGid());
		r.setTdUid(tdUid);
		r.setTestStepRecordId(step.getRecordId());
		r.setValue(value);
		logger.info(r.toString());
		return r;
		}
	//Keerthi Ends

	/*	Added By Paneendra for TENJINCG-1239 starts */
	@SuppressWarnings("unchecked")
	private Map<String, String> getXapikeyAuthorizationHeader(String headerBody) {
		Map<String, String> headers = new HashMap<String, String>();


		JSONObject jObject = null;
		String value = null;
		try {
			jObject = new JSONObject(headerBody);
		} catch (JSONException e) {
			logger.error("Error ", e);
		}
		Iterator<String> keys = jObject.keys();

		while(keys.hasNext()){
			String key = keys.next();

			try {
				value = jObject.getString(key);
			} catch (JSONException e) {
				logger.error("Error ", e);
			} 
			headers.put(key, value);
			//headers.put("REQUESTUUID",uniqueglobalIdentifier());
			//headers.put("GLOBALUUID", uniqueglobalIdentifier());

		}


		return headers;
	}

	public String uniqueglobalIdentifier() {
		// Creating a random UUID (Universally unique identifier).
		UUID uuid = UUID.randomUUID();
		String randomUUIDString = uuid.toString();
		return randomUUIDString;
	}

	/*	Added By Paneendra for TENJINCG-1239 ends */

	/*Changes by Avinash for TENJINCG-1018 starts*/
	private Aut validateToken() throws RestClientException{
		Aut aut = null;
		try {
			aut = new OAuthHelper().getTokenDetails(step.getAut());
		} catch (DatabaseException e) {
			throw new RestClientException("Error while validating the access Token.");
		}

		Calendar toknGenTime = Calendar.getInstance();
		toknGenTime.setTime(aut.getTokenGenerationTime());
		toknGenTime.add(Calendar.SECOND, Integer.valueOf(aut.getTokenValidity()));

		//long secs = (new Date().getTime() - aut.getTokenGenerationTime().getTime())/1000;
		//For implicit we should not go for refresh token.
		//if((Integer.valueOf(aut.getTokenValidity()) > secs) || "implicit".equalsIgnoreCase(aut.getGrantType())){
		if(toknGenTime.compareTo(Calendar.getInstance()) > 0 || "implicit".equalsIgnoreCase(aut.getGrantType())){
			logger.debug("Token is valid. Generation Time {}, Expire Time {}", aut.getTokenGenerationTime(), new Date(aut.getTokenGenerationTime().getTime() + Integer.parseInt(aut.getTokenValidity())));
			return aut;
		}else{
			logger.debug("Token is invalid..so generating the new token for execution.");
			if(aut.getRefreshToken() == null && "authorization_code".equalsIgnoreCase(aut.getGrantType())){
				logger.debug("Token is invalid and no refresh token avail. so terminate iteration here.");
			}else{
				return getOAuth2Token(aut);
			}
		}
		return aut;
	}

	boolean passwordGrantFlag = false;
	private Aut getOAuth2Token(Aut aut) throws RestClientException {
		String url = "";
		if("password".equalsIgnoreCase(aut.getGrantType()) && Utilities.trim(aut.getRefreshToken()).isEmpty()){
			url= aut.getAccessTokenUrl()+"?grant_type="+aut.getGrantType()+"&scope="+aut.getLoginUserType()+"&username="+aut.getLoginName()+"&password="+aut.getPassword();
		}else if("client_credentials".equalsIgnoreCase(aut.getGrantType())){
			url= aut.getAccessTokenUrl()+"?grant_type="+aut.getGrantType()+"&scope="+aut.getLoginUserType();
		}else
			url = aut.getAccessTokenUrl()+"?grant_type=refresh_token&refresh_token="+aut.getRefreshToken();
		return getToken(aut, url);
	}

	private Aut getToken(Aut aut, String url) throws RestClientException{
		String clientCredentials = aut.getClientId()+":"+aut.getClientSecret();
		String encodedCredentials = new String(Base64.encodeBase64(clientCredentials.getBytes()));
		try {
			Client client = ClientBuilder.newClient();
			WebTarget target = client.target(url);
			Response response = target.request().accept(MediaType.APPLICATION_JSON).header("Authorization", "Basic "+encodedCredentials).post(null, Response.class);
			Date tokenGenerationTime = new Date();
			int status = response.getStatus();
			String responseString = response.readEntity(String.class);
			logger.debug("Response while refreshing the token : {}", responseString);
			JSONObject respJson = new JSONObject(responseString);

			if(status == 200){
				ApplicationUser refreshedTknDetails = new ApplicationUser();
				refreshedTknDetails.setAccessToken(respJson.getString("access_token"));
				refreshedTknDetails.setTokenValidity(respJson.getString("expires_in"));
				try {
					refreshedTknDetails.setRefreshToken(respJson.getString("refresh_token"));
				} catch (Exception e) {}
				refreshedTknDetails.setTokenGenTime(tokenGenerationTime);
				refreshedTknDetails.setUacId(aut.getUacId());
				refreshedTknDetails.setAppId(aut.getId());

				aut.setAccessToken(refreshedTknDetails.getAccessToken());
				aut.setRefreshToken(refreshedTknDetails.getRefreshToken());
				aut.setTokenGenerationTime(refreshedTknDetails.getTokenGenTime());
				aut.setTokenValidity(refreshedTknDetails.getTokenValidity());

				new OAuthHelper().persistAccessToken(refreshedTknDetails, runId);
			}else{
				//{"error":"invalid_token","error_description":"Invalid refresh token (expired): 396f80b7-5989-4533-84e3-b1d6889de779"}
				if("password".equalsIgnoreCase(aut.getGrantType()) && !passwordGrantFlag){
					passwordGrantFlag = true;
					aut.setRefreshToken(null);
					aut = getOAuth2Token(aut);
				}else{
					passwordGrantFlag = false;
					String message = "";
					String[] names = JSONObject.getNames(respJson);
					List<String> namesList = Arrays.asList(names);
					if(namesList.contains("error_description")){
						try {
							message = respJson.getString("error_description");
						} catch (JSONException e) {}
					}else if(namesList.contains("message")){
						try {
							message = respJson.getString("message");
						} catch (JSONException e) {}
					}else 
						message = "Error while refreshing the token.";

					throw new RestClientException(message);
				}
			}
		}catch (JSONException e) {
			logger.debug("Error while getting the access token. Message {}", e.getMessage());
			throw new RestClientException("Error while getting the access token.");
		}catch (DatabaseException e) {
			logger.debug("Error while getting the access token. Message {}", e.getMessage());
			throw new RestClientException("Error while getting the access token.");
		}
		return aut;
	}
	/*Changes by Avinash for TENJINCG-1018 ends*/
	private Metadata learn(String descriptor, String mediaType) throws BridgeException {
		if(mediaType.equalsIgnoreCase(MediaType.APPLICATION_JSON.toString())){
			return learnJson(descriptor);
		}else{
			return learnXml(descriptor); // JSON Method Calling
			//return learnXml1(descriptor); // Soap Method Calling
		}
	}

	private Metadata learnJson(String descriptor) throws BridgeException {
		Metadata metadata = new Metadata();

		logger.info("Parsing JSON...");

		try{
			/* TENJINCG-430 */
			/*Map<String, Object> map = new Gson().fromJson(descriptor, new TypeToken<LinkedHashMap<String, Object>>() {}.getType());*/
			JsonParser parser = new JsonParser();
			JsonElement descriptorElement= parser.parse(descriptor);
			JsonObject descriptorObject = null;
			boolean rootIsMrb = false;
			if(descriptorElement.isJsonArray()) {
				rootIsMrb = true;
				if(descriptorElement.getAsJsonArray().size() > 0) {
					descriptorObject = descriptorElement.getAsJsonArray().get(0).getAsJsonObject();
				}else{
					descriptorObject = new JsonObject();
				}
			}else{
				descriptorObject = descriptorElement.getAsJsonObject();
			}

			String fDescriptor = descriptorObject.toString();

			logger.debug("Setting root location to [{}]", ROOT_LOCATION_NAME);
			/*String uniqueLocationName = this.createPageAreaObject(ROOT_LOCATION_NAME, "N/A", false);*/
			String uniqueLocationName = this.createPageAreaObject(ROOT_LOCATION_NAME, "N/A", rootIsMrb);
			Map<String, Object> map = new Gson().fromJson(fDescriptor, new TypeToken<LinkedHashMap<String, Object>>() {}.getType());
			/* TENJINCG-430 ends*/
			this.parseMap(map, uniqueLocationName);

			//Metadata metadata = new Metadata();

			metadata.setPageAreas(this.pageAreas);
			this.pageAreas = ArrayListMultimap.create();
			metadata.setTestObjectMap(this.testObjectMap);
			this.testObjectMap = new ArrayList<TreeMap<String, TestObject>>();
		}catch(LearnerException e){
			logger.error(e.getMessage(),e);
			throw new BridgeException(e.getMessage());
		}catch(Exception e){
			logger.error("ERROR occurred while learning JSON", e);
			throw new BridgeException("Could not complete learning of this operation due to an internal error. Please contact Tenjin Support.");
		}

		return metadata;
	}





	private Metadata learnXml(String descriptor) throws BridgeException {
		Metadata metadata = new Metadata();

		/**
		 * Added by sharath for learning XML Starts
		 *//*
		logger.info("Parsing XML...");

		try{

			JsonParser parser = new JsonParser();
			this.requestDocument = GenericRestUtils.getXMLTemplateDocument(descriptor);
			this.baselineDocument = GenericRestUtils.loadDocument(descriptor, false);
			String xml=this.getStringFromDocument(this.requestDocument);

			//GenericRestUtils.buildXMLRequest(this.tdUid,this.pageAreas,descriptor);
			JsonParser parser = new JsonParser();
			JSONObject xmlJSONObj = XML.toJSONObject(descriptor);
			String jsonPrettyPrintString = xmlJSONObj.toString();
			System.out.println("---------------------------------------------------");
			System.out.println("Printing XML in JSON Format" + jsonPrettyPrintString);
			System.out.println("---------------------------------------------------");
			JsonElement descriptorElement= parser.parse(jsonPrettyPrintString);

			JsonObject descriptorObject = null;
			boolean rootIsMrb = false;
			if(descriptorElement.isJsonArray()) {
				rootIsMrb = true;
				if(descriptorElement.getAsJsonArray().size() > 0) {
					descriptorObject = descriptorElement.getAsJsonArray().get(0).getAsJsonObject();
				}else{
					descriptorObject = new JsonObject();
				}
			}else{
				descriptorObject = descriptorElement.getAsJsonObject();
			}

			String fDescriptor = descriptorObject.toString();

			logger.debug("Setting root location to [{}]", ROOT_LOCATION_NAME);
			String uniqueLocationName = this.createPageAreaObject(ROOT_LOCATION_NAME, "N/A", rootIsMrb);
			Map<String, Object> map = new Gson().fromJson(fDescriptor, new TypeToken<LinkedHashMap<String, Object>>() {}.getType());
			this.parseMap(map, uniqueLocationName);

			metadata.setPageAreas(this.pageAreas);
			this.pageAreas = ArrayListMultimap.create();
			metadata.setTestObjectMap(this.testObjectMap);
			this.testObjectMap = new ArrayList<TreeMap<String, TestObject>>();
		}catch(LearnerException e){
			logger.error(e.getMessage(),e);
			throw new BridgeException(e.getMessage());
		}catch(Exception e){
			logger.error("ERROR occurred while learning JSON", e);
			throw new BridgeException("Could not complete learning of this operation due to an internal error. Please contact Tenjin Support.");
		}
		  *//**
		  * Added by sharath for learning XML Ends
		  */


		//Changes for RBL - Deekshitha-Saranya	
		logger.info("Loading XML document");
		Document document = GenericRestUtils.loadDocument(descriptor, true);
		logger.debug("Getting root element");
		boolean iselementspresent = false;
		Element rootElement = document.getDocumentElement();
		NodeList list = rootElement.getChildNodes();
		//list.item(0).getParentNode().getChildNodes().getLength()
		boolean hasParent=list.item(0).getParentNode().isSameNode(rootElement);
		int eleSize=rootElement.getElementsByTagName("*").getLength();
		int listLength=list.getLength();
		
		if(listLength>1) {
		   Node node=list.item(0).getParentNode();
			
			logger.info("soap elements present");
			iselementspresent = true;
			this.scanNode(node, null);
			
		}
		/*for (int i = 0; i < listLength; i++) {
		//for (int i = 0; i < eleSize; i++) {
			Node node = list.item(i);

			try { 

				//if ((node.getNodeType() != Node.ELEMENT_NODE)&& !hasParent) { 
					if ((node.getNodeType() != Node.ELEMENT_NODE)) { 
					//if (node.getNodeType() != Node.ELEMENT_NODE) { 
					//if (node.getNodeType() != Node.ELEMENT_NODE) { 
					logger.info("ignoring text nodes");
					continue; 
				} 
			} catch (Exception e) { 
				continue;
			}
			if (node.getNodeType() != Node.ELEMENT_NODE) {
						logger.info("ignoring text nodes");
						continue;
					}
			logger.info("soap elements present");
			iselementspresent = true;
			this.scanNode(node, null);

		}*/

		if (!iselementspresent) {
			logger.error("ERROR -  Element Body and Header not found");
			throw new LearnerException("SOAP  Element Body and Header was not found. Please ensure the XML is valid.");
		}

		/*
		 * if(soapBodyNodeList == null || soapBodyNodeList.getLength() < 1) {
		 * logger.error("ERROR - SOAP Body Element not found"); throw new
		 * LearnerException
		 * ("SOAP Body Element was not found. Please ensure the XML is valid."); }
		 */

		// For TENJINCG-610
		/* this.scanNode(soapBodyNodeList.item(0)); */
		/* this.scanNode(soapBodyNodeList.item(0), null); */
		// For TENJINCG-610 ends


		metadata.setPageAreas(this.pageAreas);
		metadata.setTestObjectMap(this.testObjectMap);
		//parentFound=false;
		return metadata;
		//Changes end
		//return metadata;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void parseMap(Map<String, Object> map, String parentLocationName) {

		for(String key : map.keySet()) {

			Object obj = map.get(key);

			if(obj == null) {
				logger.warn("Null value recieved. Treating it as a string");
				obj ="";
			}

			if(isPrimitiveType(obj.getClass())){
				//Changes for learning for Swagger
				String mandatory="NO";
				String viewMode="N";
				String defVlaues=null;
				if(obj instanceof String &&(!((String) map.get(key)).equalsIgnoreCase(""))){

					System.out.println("Field " + key + "(" + obj.getClass().getSimpleName() + ")");
					/*this.createTestObject(key, parentLocationName);*/

					String [] valueArray=obj.toString().split(";");

					if(valueArray[0].equalsIgnoreCase("required")){
						mandatory="YES";
					}else if(valueArray[0].equalsIgnoreCase("read-Only")){
						viewMode="Y";
					}
					if(valueArray.length>1){
						defVlaues=valueArray[1];
					}
					this.createTestObjectWithValues(key, parentLocationName, mandatory, viewMode, defVlaues);
				}else{
					this.createTestObject(key, parentLocationName);
				}
				//Changes for learning for Swagger
			}else if(obj instanceof Map<?,?>) {
				System.err.println("Page " + key + " (Single-Record)");
				/*scanMap((Map<String, Object>) obj);*/
				String uLocName = this.createPageAreaObject(key, parentLocationName, false);
				this.parseMap((Map<String,Object>) obj, uLocName);
			}else if(obj instanceof Collection<?>) {
				System.err.println("Page " + key + " (Multi-Record)");
				Collection collection = (Collection) obj;
				if(collection.size() > 0) {

					// Added for converting Json Array and learn as XML.

					/*JSONObject json = new JSONObject(map);
					try {
						json = getJsonFromMap(map);
					} catch (JSONException e) {
					} 
					System.out.println("JSON Value is : " +json);

					String xmlString = null;
					try {
						xmlString = XML.toString(json);
					} catch (JSONException e) {

						logger.error("Error ", e);
					}
					this.learnXmlFromJson(xmlString);*/

					Iterator cIter = collection.iterator();
					while(cIter.hasNext()){
						String uLocName = this.createPageAreaObject(key, parentLocationName, true);
						/*Changed by Gangadhar Badagi for TENJINCG-478 starts
						scanMap((Map<String, Object>) cIter.next());*/

						/**
						 * Added by sharath for learning XML Starts
						 */

						//while(cIter.hasNext()){
						try {
							this.parseMap((Map<String,Object>) cIter.next(), uLocName);
						}catch(ClassCastException e){
							this.createTestObject("NativeType", uLocName);

						}	

					}
					/**
					 * Added by sharath for learning XML Ends
					 */

					/*try {
						this.parseMap((Map<String,Object>) cIter.next(), uLocName);
					}catch(ClassCastException e){
						this.createTestObject("NativeType", uLocName);

					}*/
					/*Changed by Gangadhar Badagi for TENJINCG-478 ends*/
				}
			}
		}

		TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();
		for(TestObject t:this.testObjects){
			pMap.put(t.getUniqueId(), t);
		}
		this.testObjects.clear();
		this.testObjectMap.add(pMap);
		this.tabOrder = -1;
	}

	private String createPageAreaObject(String locationName, String parentLocationName, boolean multiRecord) throws LearnerException{
		boolean landingPage = false;
		String locationType = "JSON Node";
		this.pageSequence++;
		if(this.pageSequence < 1) {
			landingPage = true;
			locationType = "MAIN";
		}

		try{
			Location location = new Location();
			location.setLabel(locationName);
			/*location.setLocationType("MAIN");*/
			location.setLocationType(locationType);
			location.setLandingPage(landingPage);
			location.setParent(parentLocationName);

			location.setScreenTitle(locationName);
			location.setSequence(this.pageSequence);
			location.setWayIn("N/A");
			location.setWayOut("N/A");
			if(locationName.equalsIgnoreCase("users")){
				System.err.println();
			}
			location.setPath(this.getPath(location));
			location.setMultiRecordBlock(multiRecord);
			this.addToMap(location);

			//this.uniqueLocationName = location.getLocationName();
			return location.getLocationName();
		} catch (Exception e) {
			logger.error("ERROR occurred while creating location", e);
			throw new LearnerException("Could not create location");
		}
	}


	private String getPath(Location location) {
		String path = "";

		List<String> paths = new ArrayList<String> ();

		paths.add(location.getLabel());
		Location cLoc = location;
		String parent = cLoc.getParent();
		while(!"N/A".equalsIgnoreCase(parent)) {
			Location parentLocation = this.getLocation(cLoc.getParent());
			if(parentLocation == null) {
				break;
			}else{
				paths.add(parentLocation.getLabel());
				parent = parentLocation.getParent();
				cLoc = parentLocation;
			}
		}

		for(int i=(paths.size()-1) ; i>=0; i--) {
			path += paths.get(i); 
			if(i > 0) {
				path+= "-->";
			}
		}

		return path;
	}

	private Location getLocation(String uniqueLocationName) {
		if(this.pageAreas != null) {
			for(String key : this.pageAreas.keySet()){
				Collection<Location> locs = this.pageAreas.get(key);
				Iterator<Location> locIter = locs.iterator();
				while(locIter.hasNext()) {
					Location loc = locIter.next();
					if(loc.getLocationName().equalsIgnoreCase(uniqueLocationName)) {
						return loc;
					}
				}
			}
		}

		return null;
	}

	private void addToMap(Location location) throws LearnerException{
		try{
			String locationName = location.getLabel();
			logger.debug("Scanning map for existing pages with name " + locationName);
			Collection<Location> locs = this.pageAreas.get(locationName);
			if(locs != null && locs.size() > 0){
				logger.debug("There is already " + locs.size() + " page(s) with name " + locationName);
				int nc = locs.size() + 1;
				String newLocName = "00" + Integer.toString(nc);
				newLocName = newLocName.substring(newLocName.length() -2);
				newLocName = locationName + " " + newLocName;
				logger.debug("New Location Name will be " + newLocName);
				location.setLocationName(newLocName);
			}else{
				logger.debug("There are no existing pages with name " + locationName);
				location.setLocationName(locationName);
			}

			this.pageAreas.put(locationName, location);
			logger.info("New Location --> {}", location.toString());
		}catch(Exception e){
			throw new LearnerException("Could not resolve location name for " + location.getLocationName() ,e);
		}
	}

	private void createTestObject(String fieldName, String locationName) {

		this.fieldSequence++;
		this.tabOrder++;

		TestObject t = new TestObject();
		t.setLabel(fieldName);
		t.setUniqueId(fieldName + this.fieldSequence);
		t.setIdentifiedBy("label");
		t.setObjectClass("JSON Element");
		t.setDefaultOptions(null);
		t.setMandatory("NO");
		t.setLovAvailable("N");
		t.setAutoLovAvailable("N");
		t.setViewmode("Y");
		t.setTabOrder(this.tabOrder);
		t.setSequence(this.fieldSequence);
		t.setLocation(locationName);
		t.setIsMultiRecord("N");
		t.setGroup("");
		this.testObjects.add(t);
	}
	//Changes for learning for Swagger
	private void createTestObjectWithValues(String fieldName, String locationName,String mandatory,String viewMode,String defVlaues) {

		this.fieldSequence++;
		this.tabOrder++;

		TestObject t = new TestObject();
		t.setLabel(fieldName);
		t.setUniqueId(fieldName + this.fieldSequence);
		t.setIdentifiedBy("label");
		t.setObjectClass("JSON Element");
		t.setDefaultOptions(defVlaues);
		t.setMandatory(mandatory);
		t.setLovAvailable("N");
		t.setAutoLovAvailable("N");
		t.setViewmode(viewMode);
		t.setTabOrder(this.tabOrder);
		t.setSequence(this.fieldSequence);
		t.setLocation(locationName);
		t.setIsMultiRecord("N");
		t.setGroup("");
		this.testObjects.add(t);
	}
	//Changes for learning for Swagger
	public static boolean isPrimitiveType(Class<?> clazz)
	{
		return WRAPPER_TYPES.contains(clazz);
	}

	private static Set<Class<?>> getWrapperTypes()
	{
		Set<Class<?>> ret = new HashSet<Class<?>>();
		ret.add(Boolean.class);
		ret.add(Character.class);
		ret.add(Byte.class);
		ret.add(Short.class);
		ret.add(Integer.class);
		ret.add(Long.class);
		ret.add(Float.class);
		ret.add(Double.class);
		ret.add(Void.class);
		ret.add(String.class);
		return ret;
	}

	private void parseOperationParameters(ApiOperation operation) {

		if(operation.getResourceParameters() != null && operation.getResourceParameters().size() > 0) {

			String resourceParamPageName = this.createPageAreaObject("Resource Parameters", "N/A", false);

			for(Parameter p : operation.getResourceParameters()) {
				this.createTestObject(p.getName(), resourceParamPageName);
			}

			TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();
			for(TestObject t:this.testObjects){
				pMap.put(t.getUniqueId(), t);
			}
			this.testObjects.clear();
			this.testObjectMap.add(pMap);
			this.tabOrder = -1;
		}

		/*if(operation.getRequestRepresentation() != null && operation.getRequestRepresentation().getParameters() != null 
	    			&& operation.getRequestRepresentation().getParameters().size() > 0) {
	    		String requestParamPageName = this.createPageAreaObject("(REQ)_Request Parameters", "N/A", false);
	    		for(Parameter p : operation.getRequestRepresentation().getParameters()) {
	    			this.createTestObject(p.getName(), requestParamPageName);
	    		}

	    		TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();
	    		for(TestObject t:this.testObjects){
	    			pMap.put(t.getUniqueId(), t);
	    		}
	    		this.testObjects.clear();
	    		this.testObjectMap.add(pMap);
	    		this.tabOrder = -1;
	    	}*/
		if(operation.getListRequestRepresentation().size()>0){
			for(Representation reqRepresentation:operation.getListRequestRepresentation()){
				if(reqRepresentation.getParameters()!=null && (reqRepresentation.getParameters().size()>0)){
					String requestParamPageName = this.createPageAreaObject("(REQ)_Request Parameters", "N/A", false);
					for(Parameter p : reqRepresentation.getParameters()) {
						this.createTestObject(p.getName(), requestParamPageName);
					}
					TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();
					for(TestObject t:this.testObjects){
						pMap.put(t.getUniqueId(), t);
					}
					this.testObjects.clear();
					this.testObjectMap.add(pMap);
					this.tabOrder = -1;
				}
			}
		}

	}

	private String getBasicAuthorizationHeader() {
		String userName = this.step.getAut().getLoginName();
		String password = this.step.getAut().getPassword();
		/*String userName = "sriram";
	    	String password = "Sriram@123";*/
		byte[] encodedBytes = Base64.encodeBase64((userName + ":" + password).getBytes());

		String encryptedToken = new String(encodedBytes);
		logger.info("Encoded user name: " + userName);
		logger.info("Encoded Password: " + password);
		logger.info("Encrypted auth token: " + encryptedToken);
		return "Basic " + encryptedToken;
	}

	/**
	 * Added by sharath for learning XML Starts
	 */




	public static boolean hasChildNodes(Node node) {
		boolean hasChildNodes = false;

		NodeList childNodes = node.getChildNodes();
		for(int i=0; i< childNodes.getLength(); ) {
			Node childNode = childNodes.item(i);
			if(childNode.getNodeType() == Node.ELEMENT_NODE) {
			hasChildNodes = true;
			break;
			}
		}

		return hasChildNodes;
	}

	/**
	 * Added by sharath for learning XML Ends
	 */

	public String getStringFromDocument(Document doc)
	{
		try {
			DOMSource domSource = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			return writer.toString();
		} catch (Exception e) {
			logger.error("ERROR in getStringFromDocument(doc)", e);
			return "";
		}
	}


	private void scanNode(Node node, String uniqueNameForThisNode) throws LearnerException{
		//For TENJINCG-610 ends

		//Changes for RBL - Saranya - Deekshitha
		String nodeName = "";
		boolean loactionExist=false;
		boolean testObjectExist=false;
         boolean parentNode=false;
		try {

			nodeName = node.getLocalName();
			if(nodeName==null && !parentFound) {
			//node.getParentNode().getNodeName();
			node=node.getParentNode();
			nodeName = node.getLocalName();
			parentFound=true;
			
			}
			//For TENJINCG-610
			if(uniqueNameForThisNode == null)
				uniqueNameForThisNode = nodeName;
			//For TENJINCG-610 ends

			logger.debug("Scanning node {}", nodeName);
			
			/*if(!node.getParentNode().isSameNode(node) && !parentNode) {
				node=node.getParentNode();
				parentNode=true;
			}*/
			NodeList childNodes = node.getChildNodes();
			logger.debug("This node has [{}] child nodes", childNodes.getLength());

			for(int i=0; i<childNodes.getLength(); i++) {
				Node childNode = childNodes.item(i);

				if(childNode.getNodeType() != Node.ELEMENT_NODE) {
					continue;
				}

				String childNodeLocalName = childNode.getLocalName();

				
				

				if(GenericRestUtils.hasChildNodes(childNode) ) {
					logger.debug("Creating page area for [{}]", childNodeLocalName);
					//For TENJINCG-610
					
					
					String uniqueName = this.createPageAreaObject(childNode);
					this.scanNode(childNode, uniqueName);
					//For TENJINCG-610 ends
				} else {
					//For TENJINCG-610
					/*this.createTestObject(childNode);*/
					//if(!this.pageAreas.containsKey(nodeName)){
					//if(!loactionExist && nodeName.equals(uniqueNameForThisNode)){
					
					//this.createTestObject(childNode, uniqueNameForThisNode);
					
						if(!loactionExist){
						String uniqueName = this.createPageAreaObject(node);
						loactionExist=true;
					}
						//if(!this.testObjectMap.contains(childNodeLocalName)) {
					//this.createTestObject(childNode, uniqueNameForThisNode);
						//}
					//}
					
						for(TestObject testobj :this.testObjects) {
							if(testobj.getLabel().equals(childNodeLocalName)) {
							 //if(testobj.getLabel().contains(childNodeLocalName)) {
									testObjectExist=true;
								}
							}
						if(!testObjectExist){
							this.createTestObject(childNode, uniqueNameForThisNode);
						}
					//this.createTestObject(childNode, uniqueNameForThisNode);
					//For TENJINCG-610 ends
				}

			}

		} catch(LearnerException e) {
			throw e;
		} catch(Exception e) {
			logger.error("ERROR scanning node", e);
			throw new LearnerException("Could not learn XML due to an internal error.");
		}

		TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();
		for(TestObject t:this.testObjects){
			pMap.put(t.getUniqueId(), t);
		}
		/*for(int i=0;i<this.testObjects.size();i++){
			TestObject t=this.testObjects.get(i);
			pMap.put(t.getUniqueId(), t);
		}*/
		this.testObjects.clear();
		this.testObjectMap.add(pMap);
		this.tabOrder = -1;

	}
	
	
	
	private void createTestObject(Node node, String parentLocationName) {
		//For TENJINCG-610 ends
		boolean testObjectExist=false;
		this.fieldSequence++;
		this.tabOrder++;

		TestObject t = new TestObject();
		t.setLabel(node.getLocalName());
		t.setUniqueId(node.getNodeName());
		t.setIdentifiedBy("label");
		t.setObjectClass(String.valueOf(node.getNodeType()));
		t.setDefaultOptions(null);
		t.setMandatory("NO");
		t.setLovAvailable("N");
		t.setAutoLovAvailable("N");
		t.setViewmode("Y");
		t.setTabOrder(this.tabOrder);
		t.setSequence(this.fieldSequence);
		//For TENJINCG-610
		/*t.setLocation(this.uniqueLocationName);*/
		t.setLocation(parentLocationName);
		//For TENJINCG-610 ends
		t.setIsMultiRecord("N");
		t.setGroup("");
		
		//this.testObjects.add(t);
		for(TestObject testobj :this.testObjects) {
			if(testobj.getLabel().equals(t.getLabel())) {
		 //if(testobj.getLabel().contains(t.getLabel())) {
				testObjectExist=true;
			}
		}
		if(!testObjectExist){
		this.testObjects.add(t);
		}
	}
	private String createPageAreaObject(Node node) throws LearnerException{
		//For TENJINCG-610 ends
		boolean landingPage = false;
		String locationType = "JSON Node";
		this.pageSequence++;
		if(this.pageSequence < 1) {
			landingPage = true;
			locationType = "XMLNODE";
		}

		try{
			
			Location location = new Location();
			location.setLabel(node.getLocalName());
			/*location.setLocationType("MAIN");*/
			location.setLocationType(locationType);
			location.setLandingPage(landingPage);
			if(node.getParentNode() != null) {
				location.setParent(node.getParentNode().getLocalName());
			}else{
				location.setParent("N/A");
			}

			location.setPath(getPath(node));
			location.setScreenTitle(node.getNodeName());
			location.setSequence(this.pageSequence);
			location.setWayIn("N/A");
			location.setWayOut("N/A");
			
			//this.addToMap(location);
             if(this.pageAreas.get(location.getLabel()).size()<0 /*&& this.pageAreas.containsKey(location.getParent())*/) {
				logger.debug("Location already exist");
			}else { 
			this.addToMap(location);
			}

			this.uniqueLocationName = location.getLocationName();
			//For TENJINCG-610
			return this.uniqueLocationName;
			//For TENJINCG-610 ends
		} catch (Exception e) {
			logger.error("ERROR occurred while creating location", e);
			throw new LearnerException("Could not create location");
		}
	}


	public  String getPath(Node node) {

		boolean reachedTop = false;
		String path = "";
		List<String> paths = new ArrayList<String>();
		try{
			while(!reachedTop) {
				Node parent = node.getParentNode();
				if(parent != null) {
					paths.add(node.getNodeName());
					node = parent;
				}else  {
					reachedTop = true;
				}
			}
		} catch(Exception e) {
			logger.error("ERROR", e);
		}

		for(int i= (paths.size()-1); i>=0; i--) {
			path = path + paths.get(i);
			if(i > 0) {
				path = path  + "-->";
			}
		}

		return path;
	}


	/*private void addToMap(Location location) throws LearnerException{
		try{
			String locationName = location.getLabel();
			logger.debug("Scanning map for existing pages with name " + locationName);
			if(location.getSequence()==1) {
				this.pageAreas=ArrayListMultimap.create();
			}

			//if(this.pageAreas!=null) {
			Collection<Location> locs = this.pageAreas.get(locationName);
			if(locs != null && locs.size() > 0){
				logger.debug("There is already " + locs.size() + " page(s) with name " + locationName);
				int nc = locs.size() + 1;
				String newLocName = "00" + Integer.toString(nc);
				newLocName = newLocName.substring(newLocName.length() -2);
				newLocName = locationName + " " + newLocName;
				logger.debug("New Location Name will be " + newLocName);
				location.setLocationName(newLocName);
			}
			//}
		else{
				logger.debug("There are no existing pages with name " + locationName);
				location.setLocationName(locationName);
			}

			this.pageAreas.put(locationName, location);
			logger.info("New Location --> {}", location.toString());
		}catch(Exception e){
			throw new LearnerException("Could not resolve location name for " + location.getLocationName() ,e);
		}
	}
	 */

	@Override
	public IterationStatus executeIteration(JSONObject dataSet, int iteration, int expectedResponseCode)
			throws BridgeException {

		return null;
	}

}
