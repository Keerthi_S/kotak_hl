package com.ycs.tenjin.adapter.api.rest.wadl;

public class WadlException extends RuntimeException {
	private static final long serialVersionUID = -5070853615239120073L;

	public WadlException(String message) {
		super(message);
	}
	
	public WadlException(String message, Throwable cause) {
		super(message, cause);
	}
}
