/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SOAPApiBridgeImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 03-Apr-2017           Sriram Sridharan          Newly Added For
* 17-Aug-2017			Sriram Sridharan		T25IT-192
* 22-Feb-2018			Sriram Sridharan		TENJINCG-610
* 04-June-2018          Leelaprasad             TENJINCG-662
* 12-12-2018			Sriram Sridharan		TENJINCG-913
* 17-01-2019			Preeti					TJN252-70
*/


package com.ycs.tenjin.adapter.message.SWIFT;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.oem.message.MessageValidateApplication;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.MessageValidate;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.bridge.utils.MessageValidatorUtils;
import com.ycs.tenjin.util.Utilities;


public class SWIFTMessageValidateBridgeImpl implements MessageValidateApplication {
	
	private static final Logger logger = LoggerFactory.getLogger(SWIFTMessageValidateBridgeImpl.class);
	Map<Location,ArrayList<String>> map1 = new LinkedHashMap<Location,ArrayList<String>>();

	@SuppressWarnings("unused")
	private ArrayList<TreeMap<String, TestObject>> testObjectMap;
	@SuppressWarnings("unused")
	private Multimap<String, Location> pageAreas;
	
	Map<String,ArrayList<TestObject>> outputFileObjectMap=null;
	private List<ValidationResult> results;
    private String msgFileOutput,msgFileOutputString="";
    MessageValidatorUtils msgUtils=new MessageValidatorUtils();
	public String tdUid;
	private int iteration;
	private int runId;
	private ExecutionStep step;
	private Map fieldList;

	
	public SWIFTMessageValidateBridgeImpl() {
		this.initialize();
	}
	
	public SWIFTMessageValidateBridgeImpl(int runId, ExecutionStep step) {
		this.runId = runId;
		this.step = step;
		this.msgFileOutput=step.getFileName();
	}

	@Override
	public void initialize() {
		this.testObjectMap = new ArrayList<TreeMap<String, TestObject>>();
		this.pageAreas = ArrayListMultimap.create();
	}

	@Override
	public String getAdapterName() {
		return null;
	}


	

	
	@Override
	public IterationStatus executeIteration(JSONObject dataSet, int iteration)
			throws BridgeException {
		results=new ArrayList<ValidationResult>();
		String extentsion = this.msgFileOutput.split("\\.")[1];
		if(!extentsion.equalsIgnoreCase("txt")) {
			throw new ExecutorException("Cannot process the "+extentsion+" file");
		}
		try {
			msgFileOutputString=msgUtils.fetchOutputMessageFile(this.step.getFilePath(),this.msgFileOutput);
		} catch (IOException e1) {
		}
		
		this.tdUid = "";
		IterationStatus iStatus = new IterationStatus();
		this.iteration = iteration;
		
		
		try {
			this.tdUid = dataSet.getString("TDUID");
			
			logger.info("Beginning execution of Transaction [{}]", tdUid);
			logger.info("Building request...");
			
			JSONArray pageAreas = dataSet.getJSONArray("PAGEAREAS");
			
			try {
				logger.info("This test has data across {} page areas", pageAreas.length());
				
				for(int i=0; i<pageAreas.length(); i++) {
					this.scanPageArea(pageAreas.getJSONObject(i));
				}
				iStatus.setValidationResults(this.results);
			} catch(JSONException e) {
				
			}
		} catch(JSONException e) {
			logger.error("JSONException caught", e);
			iStatus.setResult("E");
			iStatus.setMessage("An internal error occurred which aborted the execution of this transaction. Please contact Tenjin Support.");
		} catch(Exception e) {
			iStatus.setResult("E");
			iStatus.setMessage(e.getMessage());
		}
		
		return iStatus;
	}
	
private void scanPageArea(JSONObject pageArea) {
		
		try {
			JSONArray detailRecords = pageArea.getJSONArray("DETAILRECORDS");
			String pageName = pageArea.getString("PA_NAME");
			
		     Map<String,String> valuesOutput=fetchElementFromStringSWIFT(this.msgFileOutputString,pageName);

			if(detailRecords.length() > 0) {
				
				for(int d=0; d < detailRecords.length(); d++ ){
					
					JSONObject detailRecord = detailRecords.getJSONObject(d);
					JSONArray fields = detailRecord.getJSONArray("FIELDS");
					
					for(int f=0; f< fields.length(); f++) {
						JSONObject field = fields.getJSONObject(f);
						String label = field.getString("FLD_LABEL");
						String fieldName=label;
                        if (label.contains("(")) {
                            label = label.split("\\(")[1].split("\\)")[0];
                        }
						String data = field.getString("DATA");
						messageValidation(label,data,pageName,valuesOutput,fieldName);
					}
				}
				
			}else{
				logger.warn("No data found for page [{}]", pageName);
			}
		} catch (JSONException e) {
			
			logger.error("JSONException occurred", e);
			throw new ExecutorException(e.getMessage());
		}
		
	}
private Map<String,String> fetchElementFromStringSWIFT(
		String msgFileOutputString2, String pageName) {
	Map<String,String> labelValurPair = new HashMap<String, String>();

	try {
		msgFileOutputString2=msgFileOutputString2.trim();
		if(pageName.equalsIgnoreCase("header")){
			labelValurPair=headerSwift(msgFileOutputString2,pageName);
		}
		else if(pageName.equalsIgnoreCase("body")){
			labelValurPair=bodySwift(msgFileOutputString2,pageName);
		}
	} catch (Exception e) {
		throw new ExecutorException(e.getMessage());
	}
	return labelValurPair;
}


private Map<String,String> bodySwift(String msgFileOutputString2, String pageName) {
	Map<String,String> labelValurPair = new HashMap<String, String>();
	String dataBodyTagSplit = null;
	boolean isPresent = false;
	try {
		String dataBodyTag = null;
		try {
			dataBodyTag = msgFileOutputString2.split("\\{4:\r\n:")[1];
		} catch (Exception e1) {
			try {
				dataBodyTag = msgFileOutputString2.split("\\{4:   \n:")[1];
			} catch (Exception e) {
				dataBodyTag = msgFileOutputString2.split("\\{4:\n:")[1];
			}
		}
		if(dataBodyTag.contains("-}")) {
			 dataBodyTagSplit = dataBodyTag.split("-}")[0];
			try {
				if(!dataBodyTag.split("-}")[1].isEmpty()) {
						isPresent = true;
				}
			} catch (Exception e) {
				System.out.println("No Data");
			}
			
		} else {
			dataBodyTagSplit = dataBodyTag;
		}
		String[] dataBodyTag1 = dataBodyTagSplit.split(":");
		for(int j=0;j<dataBodyTag1.length;j = j+2) {
			if(dataBodyTag1[j].isEmpty()) {
				continue;
			} 
			labelValurPair.put(dataBodyTag1[j], dataBodyTag1[j+1]);
		}
		
		if(isPresent) {
			dataBodyTagSplit = dataBodyTag.split("-}")[1];
			String[] dataBodyTagSplit1 = dataBodyTagSplit.split("\\{");
			for(int j=0;j<dataBodyTagSplit1.length;j++) {
				if(dataBodyTagSplit1[j].isEmpty()) {
					continue;
				}
				try {
					if(dataBodyTagSplit1[j].split(":")[1].isEmpty()) {
						continue;
					}
				} catch (Exception e) {
					System.out.println("No Data");
					continue;
				}
				String[] datavalue1 = dataBodyTagSplit1[j].split(":");
				labelValurPair.put(datavalue1[0], datavalue1[1].replace("}", ""));
			}
			
		}
	} catch (Exception e) {
		throw new ExecutorException("Internal Error : contact Tenjin support");
		
	}
	
	return labelValurPair;
}



private Map<String,String> headerSwift(String msgFileOutputString2, String pageName) {
	Map<String,String> labelValurPair = new HashMap<String, String>();
	
	try {
		String dataKey = null;
		String dataValue = null;
		String dataHeadTag = null;
		try {
			dataHeadTag = msgFileOutputString2.split("\\{4:\r\n:")[0];
		} catch (Exception e1) {
			dataHeadTag = msgFileOutputString2.split("\\{4:   \n:")[0];
		}
		 dataHeadTag = dataHeadTag.substring(1, dataHeadTag.length());
			String [] dataHeadTag1 = dataHeadTag.split("\\}\\{");
			for(int j=0;j<dataHeadTag1.length;j++) {
				if(dataHeadTag1[j].isEmpty()) {
					continue;
				}
				if(dataHeadTag1[j].contains("{")) {
					dataHeadTag1[j] = dataHeadTag1[j].split("\\{")[1];
				}
				 dataKey=dataHeadTag1[j].split(":")[0];
				  try {
					dataValue=dataHeadTag1[j].split(":")[1];
				} catch (Exception e) {
					dataValue = "";
				}
				  dataValue = dataValue.replace("}", "");
				  labelValurPair.put(dataKey, dataValue);
			}
	} catch (Exception e) {
		throw new ExecutorException("Internal Error : contact Tenjin support");
	}
		

	return labelValurPair;
}

private void messageValidation(String label, String data,String pageName,
		Map<String, String> valuesOutput, String fieldName) {
	ValidationResult valRes=null;
	
	try {
		valRes = this.getValidationResultObject(pageName,label,data.trim(),valuesOutput.get(label).trim(),"1",fieldName);
		this.results.add(valRes);
	} catch(Exception ere){} 
	
}

	@Override
	public Map<Location, ArrayList<String>> learn(MessageValidate msg, String learnString,Map fieldList)
			throws BridgeException {
		this.fieldList=fieldList;
		learnElementFromStringSWIFT(learnString);	
		return map1;
	}

	
	private void learnElementFromStringSWIFT(String str) {
		try {
			str=str.trim();
			headerSwift(str);
			bodySwift(str);
		} catch (Exception e) {
			throw new ExecutorException(e.getMessage());
		}
	}

	private void bodySwift(String str) {

		ArrayList<String> dataFields=new ArrayList<String>();
		
		String dataBodyTagSplit = null;
		boolean isPresent = false;
		String dataValue = null;
		try {
			String dataBodyTag = null;
			try {
				dataBodyTag = str.split("\\{4:\r\n:")[1];
			} catch (Exception e1) {
				dataBodyTag = str.split("\\{4:   \n:")[1];
			}
			if(dataBodyTag.contains("-}")) {
				 dataBodyTagSplit = dataBodyTag.split("-}")[0];
				try {
					if(!dataBodyTag.split("-}")[1].isEmpty()) {
							isPresent = true;
					}
				} catch (Exception e) {
					System.out.println("No Data");
				}
				
			} else {
				dataBodyTagSplit = dataBodyTag;
			}
			String[] dataBodyTag1 = dataBodyTagSplit.split(":");
			for(int j=0;j<dataBodyTag1.length;j = j+2) {
				if(dataBodyTag1[j].isEmpty()) {
					continue;
				}
				dataValue=dataBodyTag1[j];
				Object field = fieldList.get(dataValue.trim());
                if (field != null) {
                    dataFields.add(field.toString());
                } else {
                    dataFields.add(dataValue.trim());
                }
			}
			
			if(isPresent) {
				dataBodyTagSplit = dataBodyTag.split("-}")[1];
				String[] dataBodyTagSplit1 = dataBodyTagSplit.split("\\{");
				for(int j=0;j<dataBodyTagSplit1.length;j++) {
					if(dataBodyTagSplit1[j].isEmpty()) {
						continue;
					}
					try {
						if(dataBodyTagSplit1[j].split(":")[1].isEmpty()) {
							continue;
						}
					} catch (Exception e) {
						System.out.println("No Data");
						continue;
					}
					dataValue=dataBodyTagSplit1[j].split(":")[0];
					dataFields.add(dataValue.trim());
				}
				
			}
			Location loc = new Location();
			loc.setLocationName("body");
			map1.put(loc,dataFields);
		}catch (Exception e) {
			throw new ExecutorException("Internal Error : contact Tenjin support");
		}
	
	}

	private void headerSwift(String str) {
		ArrayList<String> dataFields=new ArrayList<String>();
		try{
			String dataHeadTag = null;
			try {
				dataHeadTag = str.split("\\{4:\r\n:")[0];
			} catch (Exception e) {
				dataHeadTag = str.split("\\{4:   \n:")[0];
			}
			String [] dataHeadTag1 = dataHeadTag.split("\\{");
			for(int j=0;j<dataHeadTag1.length;j++) {
				if(dataHeadTag1[j].isEmpty()) {
					continue;
				}
				 String dataValue=dataHeadTag1[j].split(":")[0];
				 Object field = fieldList.get(dataValue.trim());
                 if (field != null) {
                     dataFields.add(field.toString());
                 } else {
                     dataFields.add(dataValue.trim());
                 }
         }
			Location loc = new Location();
			loc.setLocationName("header");
			map1.put(loc,dataFields);
		} catch (Exception er) {
			throw new ExecutorException("Internal Error : contact Tenjin support");
		}
		
		
	}

	
	final public ValidationResult getValidationResultObject(String page,
			String field, String expectedValue, String actualValue,
			String detailRecordNo, String fieldName) {
		ValidationResult r = new ValidationResult();
		actualValue = Utilities.trim(actualValue);
		r.setActualValue(actualValue);
		r.setExpectedValue(Utilities.trim(expectedValue));
		r.setDetailRecordNo(detailRecordNo);
		//r.setField(field);
		r.setField(fieldName);
		r.setPage(page);
		r.setIteration(this.iteration);
		r.setResValId(0);
		r.setRunId(this.runId);
		r.setTestStepRecordId(this.step.getRecordId());

		if (Utilities.isNumeric(actualValue)
				&& Utilities.isNumeric(expectedValue)) {
			if (Utilities.isNumericallyEqual(actualValue, expectedValue)) {
				r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
			} else {
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		} else if (actualValue.equalsIgnoreCase(expectedValue)) {

			r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
		} else {

			if (Utilities.isNumeric(expectedValue)
					&& Utilities.isNumeric(actualValue)) {
				if (Double.parseDouble(expectedValue) == Double
						.parseDouble(actualValue)) {
					r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
				} else {
					r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
				}
			} else {
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		}
		logger.info(r.toString());
		return r;
	}

	
	
	
	
	
}
