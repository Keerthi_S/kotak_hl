/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SOAPApiBridgeImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 03-Apr-2017           Sriram Sridharan          Newly Added For
* 17-Aug-2017			Sriram Sridharan		T25IT-192
* 22-Feb-2018			Sriram Sridharan		TENJINCG-610
* 04-June-2018          Leelaprasad             TENJINCG-662
* 12-12-2018			Sriram Sridharan		TENJINCG-913
* 17-01-2019			Preeti					TJN252-70
*/


package com.ycs.tenjin.adapter.message.ISO8583;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.oem.message.MessageValidateApplication;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.MessageValidate;
import com.ycs.tenjin.bridge.pojo.aut.Metadata;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.bridge.utils.MessageValidatorUtils;
import com.ycs.tenjin.util.Utilities;


/*Changed by leelaprasad for the defect TENJINCG-662 starts*/
/*public abstract class SOAPApiBridgeImpl implements ApiApplication {*/
@SuppressWarnings("unused")
public class ISO8583MessageValidateBridgeImpl implements MessageValidateApplication {
	/*Changed by leelaprasad for the defect TENJINCG-662 ends*/
	
	private static final Logger logger = LoggerFactory.getLogger(ISO8583MessageValidateBridgeImpl.class);
    MessageValidatorUtils msgUtils=new MessageValidatorUtils();

	
	private List<TestObject> testObjects = new ArrayList<TestObject>();
	private ArrayList<TreeMap<String, TestObject>> testObjectMap;
	private Multimap<String, Location> pageAreas;
	Map<String,ArrayList<TestObject>> outputFileObjectMap=null;
	private int pageSequence;
	private int fieldSequence;
	private int tabOrder;
	private String uniqueLocationName;
	
	public String tdUid;
	private int iteration;
	private int runId;
	private ExecutionStep step;
    private String msgFileOutput,msgFilePath,msgFileOutputString="";
    private Map fieldList;

	private List<ValidationResult> results;

	
	public ISO8583MessageValidateBridgeImpl() {
		this.initialize();
	}
	
	public ISO8583MessageValidateBridgeImpl(int runId, ExecutionStep step) {
		this.runId = runId;
		this.step = step;
		//this.msgFileOutput=step.getExpectedResult();
		this.msgFileOutput=step.getFileName();
		this.msgFilePath = step.getFilePath();
		
	}

	@Override
	public void initialize() {
		this.testObjectMap = new ArrayList<TreeMap<String, TestObject>>();
		this.pageAreas = ArrayListMultimap.create();
		this.pageSequence=-1;
		this.fieldSequence=-1;
		this.tabOrder = -1;
		this.uniqueLocationName = "";
	}

	@Override
	public String getAdapterName() {
		return null;
	}


	
	
	private Metadata learnXml(String sourceXml) throws LearnerException{
		Metadata metadata = new Metadata();

		return metadata;
	}

	

	@Override
	public IterationStatus executeIteration(JSONObject dataSet, int iteration)
			throws BridgeException {
		results=new ArrayList<ValidationResult>();
		String extentsion = this.msgFileOutput.split("\\.")[1];
		if(!extentsion.equalsIgnoreCase("txt")) {
			throw new ExecutorException("Cannot process the "+extentsion+" file");
		}
		try {
			msgFileOutputString=msgUtils.fetchOutputMessageFile(this.step.getFilePath(),this.msgFileOutput);
		} catch (IOException e1) {
		}
		
		this.tdUid = "";
		IterationStatus iStatus = new IterationStatus();
		//iStatus.setRuntimeFieldValues(new ArrayList<RuntimeFieldValue>());
		this.iteration = iteration;
		
		
		try {
			this.tdUid = dataSet.getString("TDUID");
			
			logger.info("Beginning execution of Transaction [{}]", tdUid);
			logger.info("Building request...");
			
			JSONArray pageAreas = dataSet.getJSONArray("PAGEAREAS");
			
			try {
				logger.info("This test has data across {} page areas", pageAreas.length());
				
				for(int i=0; i<pageAreas.length(); i++) {
					this.scanPageArea(pageAreas.getJSONObject(i));
				}
				iStatus.setValidationResults(this.results);
			} catch(JSONException e) {
				
			}
		} catch(JSONException e) {
			logger.error("JSONException caught", e);
			iStatus.setResult("E");
			iStatus.setMessage("An internal error occurred which aborted the execution of this transaction. Please contact Tenjin Support.");
		} catch(Exception e) {
			iStatus.setResult("E");
			iStatus.setMessage(e.getMessage());
		}
		
		return iStatus;
	}

	
	
	private ArrayList<String> readString(String filepath) throws IOException {
		ArrayList<String> list=new ArrayList<String>();
		StringBuilder sb = null;
		BufferedReader br = null;
		try {
			//sb = new StringBuilder();
			br = new BufferedReader(new StringReader(filepath));

			String line = br.readLine();

			while (line != null) {
				list.add(line);
				line = br.readLine();
			}
		} catch (Exception e) {
			e.getMessage();
		}finally{
			br.close();
		}
		return list;
	}
	private void scanPageArea(JSONObject pageArea) {
		
		try {
			JSONArray detailRecords = pageArea.getJSONArray("DETAILRECORDS");
			String pageName = pageArea.getString("PA_NAME");
			
		     ArrayList<TestObject> valuesOutput=dataReqRespISO8583(this.msgFileOutputString,pageName);

			if(detailRecords.length() > 0) {
				
				
				for(int d=0; d < detailRecords.length(); d++ ){
					
					JSONObject detailRecord = detailRecords.getJSONObject(d);
					JSONArray fields = detailRecord.getJSONArray("FIELDS");
					
					for(int f=0; f< fields.length(); f++) {
						JSONObject field = fields.getJSONObject(f);
						/*Modified by Preeti for TJN252-80 starts*/
						/*String label = field.getString("FLD_LABEL_2");*/
						String label = field.getString("FLD_LABEL");
						String fieldName=label;
                        if (label.contains("(")) {
                            label = label.split("\\(")[1].split("\\)")[0];
                        }
						/*Modified by Preeti for TJN252-80 ends*/
						String data = field.getString("DATA");
						messageValidation(label,data,pageName,valuesOutput,fieldName);
					}
				}
				
			}else{
				logger.warn("No data found for page [{}]", pageName);
			}
		} catch (JSONException e) {
			logger.error("JSONException occurred", e);
		}
		
	}

	private void messageValidation(String label, String data,String pageName,
			ArrayList<TestObject> valuesOutput, String fieldName) {
		ValidationResult valRes=null;
		for (TestObject testObject : valuesOutput) {
			if(testObject.getLabel().equalsIgnoreCase(label)){
				try {
					valRes = this.getValidationResultObject(pageName,label,data.trim(),testObject.getData().trim(),"1",fieldName);
					this.results.add(valRes);
					break;
				} catch(Exception ere){} 
			}
		}
	}



	@Override
	public Map<Location, ArrayList<String>> learn(MessageValidate msg, String learnString, Map fieldList)
			throws BridgeException {
		this.fieldList=fieldList;
		Map<Location,ArrayList<String>> learntMap=null;
		learntMap=getElementFromStringISO8583(learnString);	
		return learntMap;
	}
	
	
	private Map<Location,ArrayList<String>> getElementFromStringISO8583(String str
			) {
		Map<Location,ArrayList<String>> header=null;
		try {
			str=str.trim();
			header=bodyReqISO8583(str);
			header.putAll(bobyRespISO8583(str));
		} catch (Exception e) {
			logger.error("Error ",e);
		}
		return header;
	}

	private Map<Location, ArrayList<String>> bodyReqISO8583(String str) {
		Map<Location,ArrayList<String>> bodyReMap=new LinkedHashMap<Location,ArrayList<String>>();
		String data="";
		ArrayList dataFields=new ArrayList<String>();
		try{
				try{
				String	bodyRequest="1200:";
					if(str.contains(bodyRequest)){
						data=str.split(bodyRequest)[1].trim();
						String[] dataBody=data.split("] ");
						for(int j=0;j<dataBody.length;j++){
							try{
								if(dataBody[j].trim().contains(" [")){
								String labelColumn=dataBody[j].trim().split("\\[")[0];
								if(labelColumn.trim().length()==3){						 						
								//dataFields.add(labelColumn.trim());
									Object field = fieldList.get(labelColumn.trim());
                                    if (field != null) {
                                        dataFields.add(field.toString());
                                    } else {
                                        dataFields.add(labelColumn.trim());
                                    }
								}
								}
								else if(dataBody[j].trim().contains("<1210>")){
									break;
								}
							}catch(Exception reer){}
						}
					}
				}catch(Exception er){}
				Location loc = new Location();
				loc.setLocationName("1200");
				bodyReMap.put(loc,dataFields);
		}catch(Exception er){}
		return bodyReMap;
	}

	private ArrayList<TestObject> dataReqRespISO8583(String str,String pageName) {
		//Map<String,ArrayList<TestObject>> bodyReMap=new LinkedHashMap<String,ArrayList<TestObject>>();
		String data="";
		ArrayList<TestObject> dataFields=new ArrayList<TestObject>();
		try{
				try{
					System.out.println(pageName);
				String	bodyRequest=pageName+":";
					if(str.contains(bodyRequest)){
						data=str.split(bodyRequest)[1].trim();
						String dataBody;
						try {
							dataBody = data.trim().split("\\<1210>")[0];
						} catch (Exception e) {
							dataBody=data.trim();
						}
						List<String> listVales=readString(dataBody);
						for (String stringKey : listVales) {
							String key=stringKey.split("\\]")[1].split("\\[")[0].trim();
							String value=stringKey.split("\\[")[2].split("\\]")[0].trim();
							TestObject t=new TestObject();
							t.setLabel(key);
							t.setData(value);
							dataFields.add(t);
						}
						//bodyReMap.put(str, dataFields);
						
					}
				}catch(Exception er){}
				//bodyReMap.put("1200",dataFields);
		}catch(Exception er){}
		return dataFields;
	}
	private Map<Location, ArrayList<String>> bobyRespISO8583(String str) {
		Map<Location,ArrayList<String>> bodyRespMap=new LinkedHashMap<Location,ArrayList<String>>();
		String data="";
		ArrayList dataRespFields=new ArrayList<String>();
		try{
				try{
				String	bodyRequest="1210:";
					if(str.contains(bodyRequest)){
						data=str.split(bodyRequest)[1].trim();
						String[] dataBody=data.split("] ");
						for(int j=0;j<dataBody.length;j++){
							try{
								if(dataBody[j].trim().contains(" [")){
								String labelColumn=dataBody[j].trim().split("\\[")[0];
								if(labelColumn.trim().length()==3){						 						
									//dataRespFields.add(labelColumn.trim());
									Object field = fieldList.get(labelColumn.trim());
                                    if (field != null) {
                                    	dataRespFields.add(field.toString());
                                    } else {
                                    	dataRespFields.add(labelColumn.trim());
                                    }
								}
								}
								else if(dataBody[j].trim().contains("<1210>")){
									break;
								}
							}catch(Exception reer){}
						}
					}
				}catch(Exception er){}
				Location loc = new Location();
				loc.setLocationName("1210");
				bodyRespMap.put(loc, dataRespFields);
		}catch(Exception er){}
		return bodyRespMap;
	}

	protected void getPostExecutionResult(IterationStatus iStatus, String responseXML) throws BridgeException {
		iStatus.setResult("S");
		iStatus.setMessage("Execution Passed.");
	}
	
	protected RuntimeFieldValue getRuntimeFieldValueObject(String field, String value, int detailRecordNo, String tdUid){
		RuntimeFieldValue r = new RuntimeFieldValue();

		r.setDetailRecordNo(1);
		r.setField(field);
		r.setIteration(this.iteration);
		r.setRunId(this.runId);
		r.setTdGid(this.step.getTdGid());
		r.setTdUid(tdUid);
		r.setTestStepRecordId(step.getRecordId());
		r.setValue(value);

		logger.info(r.toString());
		return r;
	}
	final public ValidationResult getValidationResultObject(String page,
			String field, String expectedValue, String actualValue,
			String detailRecordNo, String fieldName) {
		ValidationResult r = new ValidationResult();
		actualValue = Utilities.trim(actualValue);
		r.setActualValue(actualValue);
		r.setExpectedValue(Utilities.trim(expectedValue));
		r.setDetailRecordNo(detailRecordNo);
		//r.setField(field);
		r.setField(fieldName);
		r.setPage(page);
		r.setIteration(this.iteration);
		r.setResValId(0);
		r.setRunId(this.runId);
		r.setTestStepRecordId(this.step.getRecordId());

		if (Utilities.isNumeric(actualValue)
				&& Utilities.isNumeric(expectedValue)) {
			if (Utilities.isNumericallyEqual(actualValue, expectedValue)) {
				r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
			} else {
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		} else if (actualValue.equalsIgnoreCase(expectedValue)) {
			/*************
			 * Fix by Sriram for comparision of numeric values Ends
			 */

			r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
		} else {

			if (Utilities.isNumeric(expectedValue)
					&& Utilities.isNumeric(actualValue)) {
				if (Double.parseDouble(expectedValue) == Double
						.parseDouble(actualValue)) {
					r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
				} else {
					r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
				}
			} else {
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		}
		logger.info(r.toString());
		return r;
	}
}
