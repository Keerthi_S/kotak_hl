package com.ycs.tenjin.adapter.message.ISO20022;

public class TagLocation {

	String msgType;
	int fieldSeq;
	
	public int getFieldSeq() {
		return fieldSeq;
	}
	public void setFieldSeq(int fieldSeq) {
		this.fieldSeq = fieldSeq;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	String fieldName;
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldTag() {
		return fieldTag;
	}
	public void setFieldTag(String fieldTag) {
		this.fieldTag = fieldTag;
	}
	public String getTagLevel() {
		return tagLevel;
	}
	public void setTagLevel(String tagLevel) {
		this.tagLevel = tagLevel;
	}
	public String getParentTag() {
		return parentTag;
	}
	public void setParentTag(String parentTag) {
		this.parentTag = parentTag;
	}
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	String fieldTag;
	String tagLevel;
	String parentTag;
	String parentName;
	
	
	@Override
	public String toString() {
		return "TagLocation [fieldName=" + fieldName + ", fieldTag=" + fieldTag
				+ ", tagLevel=" + tagLevel + ", path=" + path + "]";
	}
	String value;
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	String path;
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
}
