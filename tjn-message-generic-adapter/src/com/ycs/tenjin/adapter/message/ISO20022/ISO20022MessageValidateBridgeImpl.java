/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SOAPApiBridgeImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 03-Apr-2017           Sriram Sridharan          Newly Added For
* 17-Aug-2017			Sriram Sridharan		T25IT-192
* 22-Feb-2018			Sriram Sridharan		TENJINCG-610
* 04-June-2018          Leelaprasad             TENJINCG-662
* 12-12-2018			Sriram Sridharan		TENJINCG-913
* 17-01-2019			Preeti					TJN252-70
*/


package com.ycs.tenjin.adapter.message.ISO20022;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.vfs.FileSystemException;
import org.apache.commons.vfs.FileSystemOptions;
import org.apache.commons.vfs.provider.sftp.SftpFileSystemConfigBuilder;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.oem.message.MessageValidateApplication;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.MessageValidate;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.bridge.utils.MessageValidatorUtils;
import com.ycs.tenjin.util.Utilities;

@SuppressWarnings("unused")
public class ISO20022MessageValidateBridgeImpl implements MessageValidateApplication {
	
	private static final Logger logger = LoggerFactory.getLogger(ISO20022MessageValidateBridgeImpl.class);
	Map<String,ArrayList<String>> learnerMap1 = new LinkedHashMap<String,ArrayList<String>>();
	Map<String,ArrayList<TestObject>> executorMap1 = new LinkedHashMap<String,ArrayList<TestObject>>();
	Map<String,String> mapPath = new LinkedHashMap<String,String>();
	String currentTagParent = "";
	protected Multimap<String, String> pageAreas = ArrayListMultimap.create();
	protected Multimap<String, TagLocation> mapMulti = ArrayListMultimap.create();
	private List<TestObject> testObjects = new ArrayList<TestObject>();
	private ArrayList<TreeMap<String, TestObject>> testObjectMap;
	private String navigationPath="";
	private int pageSequence;
	private int fieldSequence;
	private int tabOrder;
	private String uniqueLocationName;
	MessageValidatorUtils msgUtils=new MessageValidatorUtils();
	public String tdUid;
	private int iteration;
	private int runId;
	private ExecutionStep step;
	private Map fieldList;

	Map<String,ArrayList<TestObject>> outputFileObjectMap=null;
	private List<ValidationResult> results;
    private String msgFileOutput,msgFilePath,msgFileOutputString="";
	public ISO20022MessageValidateBridgeImpl() {
		this.initialize();
	}
	
	public ISO20022MessageValidateBridgeImpl(int runId, ExecutionStep step) {
		this.runId = runId;
		this.step = step;
		msgFileOutput=step.getFileName();
		msgFilePath=step.getFilePath();
	}

	@Override
	public void initialize() {
		this.testObjectMap = new ArrayList<TreeMap<String, TestObject>>();
		this.pageAreas = ArrayListMultimap.create();
		this.pageSequence=-1;
		this.fieldSequence=-1;
		this.tabOrder = -1;
		this.uniqueLocationName = "";
	}

	@Override
	public String getAdapterName() {
		return null;
	}

	@Override
	public IterationStatus executeIteration(JSONObject dataSet, int iteration)
			throws BridgeException {
		results=new ArrayList<ValidationResult>();
		String extentsion = this.msgFileOutput.split("\\.")[1];
		if(!(extentsion.equalsIgnoreCase("xml") || extentsion.equalsIgnoreCase("txt"))) {
			throw new ExecutorException("Cannot process the "+extentsion+" file");
		}
		try {
			msgFileOutputString=this.fetchOutputMessageFile(this.step.getFilePath(),this.msgFileOutput);
		} catch (IOException e1) {
			throw new ExecutorException(e1.getMessage());
		}
		
		executeStringISO20022(this.msgFileOutputString);
		
		this.tdUid = "";
		IterationStatus iStatus = new IterationStatus();
		//iStatus.setRuntimeFieldValues(new ArrayList<RuntimeFieldValue>());
		this.iteration = iteration;
		
		
		try {
			this.tdUid = dataSet.getString("TDUID");
			
			logger.info("Beginning execution of Transaction [{}]", tdUid);
			logger.info("Building request...");
			
			JSONArray pageAreas = dataSet.getJSONArray("PAGEAREAS");
			
			try {
				logger.info("This test has data across {} page areas", pageAreas.length());
				
				for(int i=0; i<pageAreas.length(); i++) {
					this.scanPageArea(pageAreas.getJSONObject(i));
				}
				iStatus.setValidationResults(this.results);
			} catch(JSONException e) {
				
			}
		} catch(JSONException e) {
			logger.error("JSONException caught", e);
			iStatus.setResult("E");
			iStatus.setMessage("An internal error occurred which aborted the execution of this transaction. Please contact Tenjin Support.");
		} catch(Exception e) {
			iStatus.setResult("E");
			iStatus.setMessage(e.getMessage());
		}
		
		return iStatus;
	}

	@Override
	public Map<Location, ArrayList<String>> learn(MessageValidate msg, String learnTypeString,Map fieldList)
			throws BridgeException {
		this.fieldList=fieldList;
		Map<Location,ArrayList<String>> learntMap=null;
		learntMap = readTheXmlAndBuild(learnTypeString);
		return learntMap;
	}

	private Map<String, ArrayList<String>> learnElementFromStringISO20022(
			String learnTypeString) {
		Document document=null;
		Element root = null;
		try {
			learnTypeString=learnTypeString.trim();
			document =toXmlDocument(learnTypeString);
			root = document.getRootElement();
		} catch (Exception e) {
			logger.error("Error ",e);
		}
		return learnerMap1;
	}
	
	private static org.jdom2.Document toXmlDocument(String str) throws ParserConfigurationException, SAXException, IOException
	{
		 org.jdom2.Document document=null;
		 try{
			 SAXBuilder builder = new SAXBuilder();
			 document = builder.build(new StringReader(str));
		} catch (Exception e) {
		}
		return document;
	}
	
	private void learnListChildren(Element current, int depth, String pageAreaName) {
	    if(!learnerMap1.containsKey(currentTagParent) && !currentTagParent.equalsIgnoreCase("")){
	    	navigationPath = navigationPath+currentTagParent.trim()+">";
	    	learnerMap1.put(currentTagParent, new ArrayList<String>());
	    	mapPath.put(currentTagParent, navigationPath);
    	}
	     
	    
	    if(current.getText()!=null && !current.getText().trim().isEmpty()){
	    	ArrayList<String> fields = learnerMap1.get(currentTagParent);
	    	fields.add(current.getName());
	    }
	  
		// TagLocation loc2=new TagLocation();
	    String loc2="";
	    @SuppressWarnings("rawtypes")
		List children = current.getChildren();
	    if(children.size()>0){
    	    	currentTagParent=addLocationToMap(current.getName().trim());
	    	@SuppressWarnings("rawtypes")
	    	Iterator iterator = children.iterator();
	    	while (iterator.hasNext()) {
	    		Element child = (Element) iterator.next();
	    		//currentTagParent = current.getName();
	    		learnListChildren(child, depth+1,loc2);
	    	}
	    }
	    
	}
	
	private void executeStringISO20022(String learnTypeString) {
		Document document=null;
		Element root = null;
		try {
			learnTypeString=learnTypeString.trim();
			document =toXmlDocument(learnTypeString);
			root = document.getRootElement();
			executeListChildren(root, 0,"");
		} catch (Exception e) {
			logger.error("Error ",e);
		}
	}
	
	 public  Map<Location, ArrayList<String>>  readTheXmlAndBuild(String StringData){
		 Map<Location,ArrayList<String>> map = new LinkedHashMap<Location,ArrayList<String>>();
		  System.out.println("readTheXmlAndBuild");

		  Map<Integer, TagLocation> pathMapSeq=new TreeMap<Integer, TagLocation>();
		  Map<String, TagLocation> pathMap=new LinkedHashMap<String, TagLocation>();
		 
		  try {
				Document document =toXmlDocument(StringData);
				Element root=document.getRootElement();
				TagLocation loc=getFirstDocument(root);
				tempListChildren(root, 0,loc);
			} catch (Exception e) {
				
			}      
	    
		  Set<String> keys = mapMulti.keySet();
		  for (String keyprint : keys) {
	      Collection<TagLocation> locvalues = mapMulti.get(keyprint);
	        for(TagLocation locVal : locvalues){
	        	pathMapSeq.put(locVal.fieldSeq, locVal);
	        	//pathMap.put(locVal.getPath(), locVal);
	        	//System.out.println("INSERT INTO TJN_CBS_ISO20022_LABEL (MSG_TYPE,PARENTNAME,PARENTTAG,FIELDNAME,FIELDTAG,TAGLEVEL,FIELDVALUE,PATH) values ('"+locVal.getMsgType()+"','"+locVal.getParentName()+"','"+locVal.getParentTag()+"','"+locVal.getFieldName()+"','"+locVal.getFieldTag()+"','"+locVal.getTagLevel()+"','"+locVal.getValue()+"','"+locVal.getPath()+"');");
	        	//System.out.println(locVal.toString());
	        }
	    }
		 Location tempLoc = new Location();
	    for (Map.Entry<Integer,TagLocation> entry : pathMapSeq.entrySet()){
	    	//System.out.println(entry.getKey() + "  "+entry.getValue());
	    	//pathMap.put(entry.getValue().path, entry.getValue());
	    	TagLocation location  = entry.getValue();
	    	if(location.getValue().trim().isEmpty()){
	    		Location loc  = new Location();
	    		String newLocation = addLocationToMap(location.getFieldTag());
	    		loc.setLocationName(newLocation);
	    		loc.setPath(location.getPath());
	    		map.put(loc, new ArrayList<String>());
	    		tempLoc = loc;
	    	}else{
	    		ArrayList<String> fields = map.get(tempLoc);
	    		fields.add(location.getFieldTag());
	    	}
	    }
	    
	   return map;
	  }
	 
	 public  void tempListChildren(Element current, int depth,TagLocation loc) {
		    
		    boolean textVal=false;
		    String currentDescNameTag="";
		    String currentTagName="";
		    String currentValue="";
		    int fieldSeq=this.fieldSequence++;
		    if(current.getText()!=null && !current.getText().trim().isEmpty()){
		    	textVal=true;
		    	currentValue=current.getText();
		    }
		    if(textVal){
		    	String descTag="";
		    	currentDescNameTag=descTag;
		    	currentTagName=current.getName();
		    	if(current.getAttribute("Ccy")!=null && !current.getAttribute("Ccy").toString().isEmpty()){
		    		String dataName=current.getName()+current.getAttribute("Ccy").getValue()+descTag;
		    		int fixedlength=48;
		    		int dataLength=depth+dataName.length();
		    		//System.out.println("fixed:51 ccy dataLength :"+dataLength);
		    		int datSpace=fixedlength-(dataLength+7);
		    		String printSpace="";
		    		for (int i = 0; i <datSpace; i++) {
		    		      //System.out.print(' '); 
		    		      printSpace=printSpace+" ";
		    		    }
		        	//data=data+"("+current.getName()+" Ccy='"+current.getAttribute("Ccy").getValue()+"') "+descTag+" "+printSpace+": "+current.getText()+"\n";
		           // System.out.println(descTag+"("+current.getName()+") : "+current.getText());
		        		
		    	}
		    	else{
		    		
		    		String dataName=current.getName()+descTag;
		    		int fixedlength=48;
		    		int dataLength=depth+dataName.length();
		    		//System.out.println("fixed:51 dataLength :"+dataLength);
		    		int datSpace=fixedlength-(dataLength);    		
		    		String printSpace="";
		    		for (int i = 0; i <datSpace; i++) {
		    		      //System.out.print(' '); 
		    		      printSpace=printSpace+" ";
		    		    }
		    	//data=data+"("+current.getName()+") "+descTag+" "+printSpace+": "+current.getText()+"\n";
		       // System.out.println(descTag+"("+current.getName()+") : "+current.getText());
		    	}
		    }
		    else{
		    	currentTagName=current.getName();
		    	if(current.getAttribute("Ccy")!=null && !current.getAttribute("Ccy").toString().isEmpty()){
		        	String descTag="";
		        	currentDescNameTag=descTag;

		        	//data=data+"("+current.getName()+" Ccy='"+current.getAttribute("Ccy").getValue()+"') "+descTag+"\n";
		           // System.out.println(descTag+"("+current.getName()+")");
		        	}
		    	else{
		    	String descTag="";
		    	currentDescNameTag=descTag;
		    	//data=data+"("+current.getName()+") "+descTag+"\n";
		       // System.out.println(descTag+"("+current.getName()+")");
		    	}
		    }
		   
			 TagLocation loc2=new TagLocation();

		    @SuppressWarnings("rawtypes")
			List children = current.getChildren();
		    if(children.size()>0){
		    	loc2.setFieldName(currentDescNameTag);
		    	loc2.setFieldTag(currentTagName);
		    	loc2.setTagLevel(depth+"");
		    	loc2.setValue(currentValue);
		    	loc2.setMsgType(loc.msgType);
		    	if(loc.getPath().equalsIgnoreCase("Document") && currentTagName.equalsIgnoreCase("Document")){
		        	loc2.setPath(loc.getPath());
		        	loc2.setFieldSeq(fieldSeq);
		    	}
		    	else{
		    	loc2.setPath(loc.getPath()+">"+currentTagName);
		    	loc2.setFieldSeq(fieldSeq);

		    	}
		    	mapMulti.put(depth+"", loc2);
		    }
		    else{

		    	loc2.setFieldName(currentDescNameTag);
		    	loc2.setFieldTag(currentTagName);
		    	loc2.setTagLevel(depth+"");
		    	loc2.setParentName(loc.getFieldName());
		    	loc2.setParentTag(loc.getFieldTag());
		    	loc2.setValue(currentValue);
		    	loc2.setMsgType(loc.msgType);
		    	loc2.setPath(loc.getPath()+">"+currentTagName);
		    	loc2.setFieldSeq(fieldSeq);
		    	mapMulti.put(depth+"", loc2);
		    
		    }
		    
		    if(children.size()>0){
		    @SuppressWarnings("rawtypes")
			Iterator iterator = children.iterator();
		    while (iterator.hasNext()) {
		      Element child = (Element) iterator.next();
		  	loc2.setParentName(loc.getFieldName());
		  	loc2.setParentTag(loc.getFieldTag());
		  	loc2.setPath(loc2.getPath());
			loc2.setFieldSeq(loc2.getFieldSeq());
		      tempListChildren(child, depth+1,loc2);
		    }
		    }

		  }
	 
	 protected TagLocation getFirstDocument(
				Element root) {
			  
			  TagLocation loc=new TagLocation();
			  try {
				loc.setFieldName(root.getName());
					loc.setFieldTag(root.getName());
					loc.setParentName("MAIN");
					loc.setParentTag("MAIN");
					loc.setTagLevel("0");
					loc.setMsgType(root.getNamespaceURI().split("xsd:")[1]);
					loc.setPath("Document");
			} catch (Exception e) {
				try {
					loc.setFieldName(root.getName());
					loc.setFieldTag(root.getName());
					loc.setParentName("MAIN");
					loc.setParentTag("MAIN");
					loc.setTagLevel("0");
					if(root.getChildren().iterator().next().getName().contains(".00")){
						loc.setMsgType(root.getChildren().iterator().next().getName());
					}
					loc.setPath("Document");
				} catch (Exception e1) {
					
					e1.printStackTrace();
				}
			}
			
			  return loc;
			  }
	
	private void executeListChildren(Element current, int depth, String pageAreaName) {
	    if(!executorMap1.containsKey(currentTagParent) && !currentTagParent.equalsIgnoreCase("")){
	    	executorMap1.put(currentTagParent.trim(), new ArrayList<TestObject>());
    	}
	     
	    
	    if(current.getText()!=null && !current.getText().trim().isEmpty()){
	    	ArrayList<TestObject> fields = executorMap1.get(currentTagParent);
	    	TestObject t = new TestObject();
	    	t.setLabel(current.getName());
	    	t.setData(current.getText().trim());
	    	fields.add(t);
	    }
	  
		// TagLocation loc2=new TagLocation();
	    String loc2="";
	    @SuppressWarnings("rawtypes")
		List children = current.getChildren();
	    if(children.size()>0){
    	    	currentTagParent=addLocationToMap(current.getName().trim());
	    	@SuppressWarnings("rawtypes")
	    	Iterator iterator = children.iterator();
	    	while (iterator.hasNext()) {
	    		Element child = (Element) iterator.next();
	    		//currentTagParent = current.getName();
	    		executeListChildren(child, depth+1,loc2);
	    	}
	    }
	    
	}
	
	final public String addLocationToMap(String currentlocation) throws LearnerException {
		String locationName = currentlocation;

		try {
			int count = 0;
			for (String key : this.pageAreas.keys()) {
				String loc = (String) (this.pageAreas.get(key).toArray()[0]);

				if (loc.equalsIgnoreCase(locationName)) {
					count = count + 1;
				}
			}
			logger.debug("Scanning map for existing pages with name " + locationName);
			if (count > 0) {
				logger.debug("There is already " + count + " page(s) with name " + locationName);
				int nc = count + 1;

				String newLocName = "00" + Integer.toString(nc);
				newLocName = newLocName.substring(newLocName.length() - 2);
				locationName = locationName + " " + newLocName;
				logger.debug("New Location Name will be " + newLocName);

			} else {
				logger.debug("There are no existing pages with name " + locationName);
			}


			this.pageAreas.put(locationName, currentlocation);
			//logger.info("New Location --> {}", locationName);
		} catch (Exception e) {
			throw new LearnerException("Could not resolve location name for " + locationName, e);
		}
		return locationName;
	}
	
private void scanPageArea(JSONObject pageArea) {
		
		try {
			JSONArray detailRecords = pageArea.getJSONArray("DETAILRECORDS");
			String pageName = pageArea.getString("PA_NAME");
			ArrayList<TestObject> valuesOutput  = this.executorMap1.get(pageName);
		    

			if(detailRecords.length() > 0) {
				//TODO Create Page element
				
				for(int d=0; d < detailRecords.length(); d++ ){
					
					JSONObject detailRecord = detailRecords.getJSONObject(d);
					JSONArray fields = detailRecord.getJSONArray("FIELDS");
					
					for(int f=0; f< fields.length(); f++) {
						JSONObject field = fields.getJSONObject(f);
						/*Modified by Preeti for TJN252-80 starts*/
						/*String label = field.getString("FLD_LABEL_2");*/
						String label = field.getString("FLD_LABEL");
						/*Modified by Preeti for TJN252-80 ends*/
						String data = field.getString("DATA");
						messageValidation(label,data,pageName,valuesOutput);
					}
				}
				
			}else{
				logger.warn("No data found for page [{}]", pageName);
			}
		} catch (JSONException e) {
			
			logger.error("JSONException occurred", e);
		}
		
	}


private void messageValidation(String label, String data,String pageName,
		ArrayList<TestObject> valuesOutput) {
	ValidationResult valRes=null;
	for (TestObject testObject : valuesOutput) {
		if(testObject.getLabel().equalsIgnoreCase(label)){
			try {
				valRes = this.getValidationResultObject(pageName,label,data.trim(),testObject.getData().trim(),"1");
				this.results.add(valRes);
				break;
			} catch(Exception ere){} 
		}
		
	}
	
}

final public ValidationResult getValidationResultObject(String page,
		String field, String expectedValue, String actualValue,
		String detailRecordNo) {
	ValidationResult r = new ValidationResult();
	actualValue = Utilities.trim(actualValue);
	r.setActualValue(actualValue);
	r.setExpectedValue(Utilities.trim(expectedValue));
	r.setDetailRecordNo(detailRecordNo);
	r.setField(field);
	r.setPage(page);
	r.setIteration(this.iteration);
	r.setResValId(0);
	r.setRunId(this.runId);
	r.setTestStepRecordId(this.step.getRecordId());

	if (Utilities.isNumeric(actualValue)
			&& Utilities.isNumeric(expectedValue)) {
		if (Utilities.isNumericallyEqual(actualValue, expectedValue)) {
			r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
		} else {
			r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
		}
	} else if (actualValue.equalsIgnoreCase(expectedValue)) {
		/*************
		 * Fix by Sriram for comparision of numeric values Ends
		 */

		r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
	} else {

		if (Utilities.isNumeric(expectedValue)
				&& Utilities.isNumeric(actualValue)) {
			if (Double.parseDouble(expectedValue) == Double
					.parseDouble(actualValue)) {
				r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
			} else {
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		} else {
			r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
		}
	}
	logger.info(r.toString());
	return r;
}

private String fetchOutputMessageFile(String folderPath, String fileName) throws IOException {
	String result = "";
	//String RemotefileName = "";
	String extentsion = fileName.split("\\.")[1];
	if(folderPath.startsWith("sftp")) {
		folderPath = folderPath.split(":")[1];
		fileName = msgUtils.copyFileFromServer(fileName, extentsion, folderPath);
		try {
			folderPath = TenjinConfiguration.getProperty("MSG_OUTPUT_FILE");
			result = msgUtils.conversionOfFileToString(folderPath + File.separator + fileName);
		} catch (TenjinConfigurationException e) {
			logger.error("No file found with the specified name");
			throw new ExecutorException("No file found with the name " + fileName);
		}
	} else {
		try {
		if (fileName.contains("*")) {
			fileName = getFile(folderPath, fileName, extentsion);
		}
		if (folderPath != null) {
			result = msgUtils.conversionOfFileToString(folderPath + File.separator + fileName);
		}
	} catch (Exception e) {
		logger.error("No file found with the specified name");
		throw new ExecutorException("No file found with the name " + fileName);
	}
}
	return result;
}

private String getFile(String folderPath, String fileName, String extentsion) throws Exception {
	File dir = new File(folderPath);
	String fileNameSplit = fileName.split("\\.")[0];
	try {
		if(fileNameSplit.equals("*")) {
			File[] allFiles1 = dir.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File allFiles, String name) {
					return name.endsWith(extentsion);
				}
			});
			Arrays.sort(allFiles1, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
			fileName = allFiles1[0].getName();
		} else if (fileName.contains("*")) {
			String fileName1 = fileName.replace("*", "").split("\\.")[0];
			File[] allFiles = dir.listFiles(new FilenameFilter() {
				public boolean accept(File allFiles, String name) {
					return name.contains(fileName1);
				}
			});
			Arrays.sort(allFiles, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
			String extention1 = null;
			for (File file : allFiles) {
				extention1 = FilenameUtils.getExtension(file.getName());
				if (extention1.equalsIgnoreCase(extentsion)) {
					fileName = file.getName();
					break;
				}
			}
			
		}
	} catch(Exception e) {
		throw new Exception();
	}

	return fileName;
}



public String createConnectionString(String hostName, String username, String password, String remoteFilePath) {
    return "sftp://" + username + ":" + password + "@" + hostName + "/" + remoteFilePath;
}

public  FileSystemOptions createDefaultOptions() throws FileSystemException {
    FileSystemOptions opts = new FileSystemOptions();
    SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, "no");

    /*
     * Using the following line will cause VFS to choose File System's Root
     * as VFS's root. If I wanted to use User's home as VFS's root then set
     * 2nd method parameter to "true"
     */
    // Root directory set to user home
    SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, false);
    SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, 10000);
    return opts;
}

}
