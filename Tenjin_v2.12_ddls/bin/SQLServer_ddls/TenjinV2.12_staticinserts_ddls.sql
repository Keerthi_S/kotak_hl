--------------------------------------------------------
--  File created - Friday-January-04-2019  
--------------------------------------------------------
--------------------------------------------------------
--  Default user for Table MASUSER
--------------------------------------------------------
Insert into MASUSER (USER_ID,FIRST_NAME,LAST_NAME,EMAIL_ID,PHONE_1,PHONE_2,PASS_WORD,USER_ENABLED) values ('tenjin','Tenjin','User','leelaprasad.peddineni@yethi.in',null,null,'$2a$12$qqlhQoLgrLt1gBXlpb4THekBGeMQst.iC2BiaEovndobM..xUO5IC','Y');
Insert into USERROLES (ROLE_ID,USER_ID,GROUP_ID) values (183,'tenjin',1);
 --------------------------------------------------------
--  Default Domain for Table TJN_DOMAINS
--------------------------------------------------------
Insert into TJN_DOMAINS (DOMAIN_NAME,DOMAIN_CREATE_DATE,DOMAIN_CREATED_BY,DOMAIN_STATE) values ('DEFAULT',convert(DATETIME, '01-01-2019',105),'tenjin','A');
 --------------------------------------------------------
--  Default Data for Table MASUSERGROUPS
--------------------------------------------------------
Insert into MASUSERGROUPS (GROUP_ID,GROUP_DESC) values (1,'Site Administrator');
Insert into MASUSERGROUPS (GROUP_ID,GROUP_DESC) values (2,'Project Manager');
Insert into MASUSERGROUPS (GROUP_ID,GROUP_DESC) values (3,'Tenjin User');
 --------------------------------------------------------
--  Default Data for Table MASUIVALDEF
--------------------------------------------------------
Insert into MASUIVALDEF (UI_VAL_ID,UI_VAL_NAME,UI_VAL_DESC,UI_TARGET,UI_VAL_TYPE,UI_FORMULA,UI_DEF_EXPECTED_VALUES) values (1,'Field Exists Check','Verifies if the specified field exists and is visible on the specified screen.','Field','DIRECT','Exists','True,False');
Insert into MASUIVALDEF (UI_VAL_ID,UI_VAL_NAME,UI_VAL_DESC,UI_TARGET,UI_VAL_TYPE,UI_FORMULA,UI_DEF_EXPECTED_VALUES) values (2,'Field State Check','Verifies if a field is enabled or disabled on the specified screen','Field','DIRECT','Enabled','True,False');
Insert into MASUIVALDEF (UI_VAL_ID,UI_VAL_NAME,UI_VAL_DESC,UI_TARGET,UI_VAL_TYPE,UI_FORMULA,UI_DEF_EXPECTED_VALUES) values (3,'Field Value Check','Verifies if the field contains a set of values available at runtime. Applicable only for Lists and Radio buttons','Field','DIRECT','HasItems',null);
Insert into MASUIVALDEF (UI_VAL_ID,UI_VAL_NAME,UI_VAL_DESC,UI_TARGET,UI_VAL_TYPE,UI_FORMULA,UI_DEF_EXPECTED_VALUES) values (4,'Default Value Check','Verifies if a field is set to the specific value at runtime','Field','DIRECT','DefValue',null);
 --------------------------------------------------------
--  Default Data for  Table TJN_ADAPTER
--------------------------------------------------------
Insert into TJN_ADAPTER (NAME,OR_TOOL,APPLICATION_TYPE) values ('flexcube12',1,1);
Insert into TJN_ADAPTER (NAME,OR_TOOL,APPLICATION_TYPE) values ('t24',1,1);
Insert into TJN_ADAPTER (NAME,OR_TOOL,APPLICATION_TYPE) values ('Demo',1,1);
Insert into TJN_ADAPTER (NAME,OR_TOOL,APPLICATION_TYPE) values ('DemoNetBanking',1,1);
 --------------------------------------------------------
--  Default Data for  Table TJN_ADAPTER_OR_TOOL
--------------------------------------------------------
Insert into TJN_ADAPTER_OR_TOOL (ID,NAME) values (1,'selenium');
Insert into TJN_ADAPTER_OR_TOOL (ID,NAME) values (2,'uft');
Insert into TJN_ADAPTER_OR_TOOL (ID,NAME) values (3,'appium');
 --------------------------------------------------------
--  Default Data for  Table TJN_APPLICATION_TYPE
--------------------------------------------------------
Insert into TJN_APPLICATION_TYPE (ID,NAME) values (1,'Web');
Insert into TJN_APPLICATION_TYPE (ID,NAME) values (2,'Windows');
Insert into TJN_APPLICATION_TYPE (ID,NAME) values (3,'Android');
Insert into TJN_APPLICATION_TYPE (ID,NAME) values (4,'IOS');
 --------------------------------------------------------
--  Default Data for  Table TJN_MAIL_ESCALATION_RULES
--------------------------------------------------------
Insert into TJN_MAIL_ESCALATION_RULES (TJN_MAIL_ESCALATION_RULE_ID,TJN_MAIL_ESCALATION_RULE_NAME,TJN_MAIL_ESCALATION_RULE_DESC) values (101,'% of Error','If a run fails with more than configured percentage');
Insert into TJN_MAIL_ESCALATION_RULES (TJN_MAIL_ESCALATION_RULE_ID,TJN_MAIL_ESCALATION_RULE_NAME,TJN_MAIL_ESCALATION_RULE_DESC) values (102,'% of Rerun','If a run ends up in error with more than configured percentage');
Insert into TJN_MAIL_ESCALATION_RULES (TJN_MAIL_ESCALATION_RULE_ID,TJN_MAIL_ESCALATION_RULE_NAME,TJN_MAIL_ESCALATION_RULE_DESC) values (103,'% of Fail','If a run fails or ends up in error with more than configured percentage');
Insert into TJN_MAIL_ESCALATION_RULES (TJN_MAIL_ESCALATION_RULE_ID,TJN_MAIL_ESCALATION_RULE_NAME,TJN_MAIL_ESCALATION_RULE_DESC) values (104,'% of Fail  or Error','If a Test Set repeatedly fails or ends up in error with more than configured number of times');
Insert into TJN_MAIL_ESCALATION_RULES (TJN_MAIL_ESCALATION_RULE_ID,TJN_MAIL_ESCALATION_RULE_NAME,TJN_MAIL_ESCALATION_RULE_DESC) values (105,'Test set fail or Error','If this particular Test Set fails or ends up with error');
Insert into TJN_MAIL_ESCALATION_RULES (TJN_MAIL_ESCALATION_RULE_ID,TJN_MAIL_ESCALATION_RULE_NAME,TJN_MAIL_ESCALATION_RULE_DESC) values (106,'Application fail or error','If this particular Application fails or ends up error');

--------------------------------------------------------
--  Default Data for  Table TJN_USER_PREFERENCE
--------------------------------------------------------

Insert into TJN_USER_PREFERENCE (USER_ID,PRJ_PREFERENCE) values ('tenjin',0);

--------------------------------------------------------
--  Default Data for  Table USERSESSIONS
--------------------------------------------------------

Insert into USERSESSIONS (US_ID,US_USER_ID,US_LOGIN_TIMESTAMP,US_LOGOUT_TIMESTAMP,US_SESSION_STATE,TJN_SESSION_ID) values (1,'tenjin',convert(DATETIME, '01-01-2019',105),convert(DATETIME, '02-01-2019',105),'I',null);


------------------------------------------------------------------
---Default Data for  Table TJN_PYMNTMSG_MASTER
---------------------------------------------------------------
   
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','36','Exchange Rate (36)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','33B','Currency/Instructed Amount (33B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','20','Sender''s Reference (20)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','23B','Bank Operation Code (23B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','32A','Value Date/Currency/Interbank Settled Amount (32A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','50K','Ordering Customer (50K)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','57D','Account With Institution (57D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','59A','Beneficiary Customer (59A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','20','File Reference (20)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','71A','Details of Charges (71A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 192','20','Transaction Reference Number (20)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 192','21','Related Reference (21)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 192','11S','MT and Date of the Original Message (11S)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','23','Bank Operation Code (23)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','51A','Sending Instruction (51A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','50A','Ordering Customer (50A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','50F','Ordering Customer (50F)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','50K','Ordering Customer (50K)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','52A','Ordering Institution (52A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','52B','Ordering Institution (52B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','52C','Ordering Institution (52C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','26T','Transaction Type Code (26T)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','77B','Regulatory Reporting (77B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','71A','Details of Charges (71A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','36','Exchange Rate (36)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','21','Transaction Reference (21)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','32B','Transaction Amount (32B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','57A','Account With Institution (57A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','57C','Account With Institution (57C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','59A','Beneficiary Customer (59A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','59F','Beneficiary Customer (59F)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','70','Remittance Information (70)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','26T','Transaction Type Code (26T)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','33B','Currency/Instructed Amount (33B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','71F','Sender''s Charges (71F)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','71G','Receiver''s Charges (71G)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','32A','Value Date, Currency Code, Amount (32A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','19','Sum of Amounts (19)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','13C','Time Indication (13C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','53A','Sender''s Correspondent (53A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','53C','Sender''s Correspondent (53C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','54A','Receiver''s Correspondent (54A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 102','72','Sender to Receiver Information (72)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','20 ','Sender''s Reference (20)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','21R','Customer Specified Reference (21R)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','23E','Instruction Code (23E)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','21E','Registration Reference (21E)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','30','Requested Execution Date (30)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','51A','Sending Institution (51A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','50C','Instructing Party (50C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','50L','Instructing Party (50L)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','50A','Creditor (50A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','50K','Creditor (50K)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','52A','Creditor''s Bank (52A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','52C','Creditor''s Bank (52C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','52D','Creditor''s Bank (52D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','26T','Transaction Type Code (26T)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','77B','Regulatory Reporting (77B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','71A','Details of Charges (71A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','72','Sender to Receiver Information (72)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','21','Transaction Reference (21)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','21C','Mandate Reference (21C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','21D','Direct Debit Reference (21D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','32B','Currency and Transaction Amount (32B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','57A','Debtor''s Bank (57A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','57C','Debtor''s Bank (57C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','57D','Debtor''s Bank (57D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','70','Remittance Information (70)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','33B','Currency/Original Ordered Amount (33B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','71F','Sender''s Charges (71F)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','71G','Receiver''s Charges (71G)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','36','Exchange Rate (36)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','19','Sum of Amounts (19)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','53A','Sender''s Correspondent (53A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 104','53B','Sender''s Correspondent (53B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','20','Sender''s Reference (20)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','002','Primary Account Number (002)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','003','Processing Code (003)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','004','Transaction Amount (004)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','011','System trace audit number (011)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','012','Time, local transaction [YYYYMMDDhhmmss] (012)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','017','Date, local transaction [YYYYMMDD] (017)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','024','Application PAN sequence number (024)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','032','Acquiring institution identification code (032)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','037','Retrieval reference number (037)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','041','Card acceptor terminal identification (041)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','042','Card acceptor identification code (042)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','043','Card acceptor name/location (043)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','049','Currency code, transaction (049)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','102','Account identification 1 (102)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','103','Account identification 2 (103)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','038','Authorization identification response (038)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','039','Response code (039)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('ISO8583','103','048','Additional data � private (048)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 200','20','Transaction Reference Number (20)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 200','32A','Value Date, Currency Code, Amount (32A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 200','57D','Account With Institution (57D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','52A','Ordering Institution (52A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','52D','Ordering Institution (52D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','53A','Sender''s Correspondent (53A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','54A','Receiver''s Correspondent (54A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','56A','Intermediary (56A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','57A','Account With Institution (57A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','58A','Beneficiary Institution (58A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','72','Sender to Receiver Information (72)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 200','53B','Sender''s Correspondent(53B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 200','56A','Intermediary (56A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 200','72','Sender to Receiver Information (72)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','52A','52A: Ordering Institution');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','52D','Ordering Institution (52D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','53A','Sender''s Correspondent (53A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','54A','Receiver''s Correspondent (54A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','56A','Intermediary (56A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','57A','Account With Institution (57A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','58A','Beneficiary Institution (58A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','72','Sender to Receiver Information (72)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','13C','Time Indication (13C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','50F','Ordering Customer (50F)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','51A','Sending Institution (51A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','52D','Ordering Institution (52D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','53A','Sender''s Correspondent (53A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','53B','Sender''s Correspondent (53B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','53D','Sender''s Correspondent (53D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','54A','Receiver''s Correspondent (54A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','54B','Receiver''s Correspondent (54B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','54D','Receiver''s Correspondent (54D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','55A','Third Reimbursement Institution (55A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','55B','Third Reimbursement Institution (55B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','55D','Third Reimbursement Institution (55D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','56A','Intermediary Institution (56A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','56C','Intermediary Institution (56C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','56D','Intermediary Institution (56D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','57A','Account With Institution (57A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','57B','Account With Institution (57B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','57C','Account With Institution (57C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','59F','Beneficiary Customer (59F)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','71G','Receiver''s Charges (71G)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 200','56D','Intermediary (56D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 200','57A','Account With Institution (57A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 200','57B','Account With Institution (57B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','53B','Sender''s Correspondent (53B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','53D','Sender''s Correspondent (53D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','13C','Time Indication (13C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','54B','Receiver''s Correspondent (54B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','54D','Receiver''s Correspondent (54D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','56D','Intermediary (56D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','57B','Account With Institution (57B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','57D','Account With Institution (57D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','53B','Sender''s Correspondent (53B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','53D','Sender''s Correspondent (53D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','13C','Time Indication (13C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','54B','Receiver''s Correspondent (54B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','54D','Receiver''s Correspondent (54D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','57B','Account With Institution (57B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','57D','Account With Institution (57D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','56C','Intermediary (56C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','56D','Intermediary (56D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','57C','Account With Institution (57C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','50A','Ordering Customer (50A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','50B','Ordering Customer (50B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','50D','Ordering Customer (50D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','59A','Beneficiary Customer (59A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','59F','Beneficiary Customer (59F)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','70','Remittance Information (70)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','33B','Currency/Instructed Amount (33B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','20','Sender''s Reference (20)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','21R','Customer Specified Reference (21R)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','28D','Message Index/Total');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','50C','Instructing Party');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','50L','Instructing Party');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','50F','Ordering Customer (50F)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','50G','Ordering Customer (50G)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','50H','Ordering Customer (50H)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','52A','Account Servicing Institution (52A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','52C','Account Servicing Institution (52C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','51A','Sending Institution (51A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','30','Requested Execution Date (30)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','25','Authorisation (25)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','21','Transaction Reference (21)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','21F','F/X Deal  Reference (21F)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','23E','Instruction Code (23E)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','32B','Currency/Transaction Amount (32B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','56A','Intermediary (56A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','56C','Intermediary (56C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','56D','Intermediary (56D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','57A','Account With Institution (57A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','57C','Account With Institution (57C)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','57D','Account With Institution (57D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','59A','Beneficiary (59A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','59F','Beneficiary (59F)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','70','Remittance Information');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','33B','Currency/Original Ordered Amount');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','71A','Details of Charges');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','77B','Regulatory Reporting');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','25A','Charges Account');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 101','36','Exchange Rate');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','59','Beneficiary  (59)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 111','20','Sender''s Reference (20)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 111','21','Cheque Number (21)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','53A','Sender''s Correspondent (53A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','53B','Sender''s Correspondent (53B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','53D','Sender''s Correspondent (53D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','54A','Receiver''s Correspondent (54A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','54B','Receiver''s Correspondent (54B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','54D','Receiver''s Correspondent (54D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','72','Sender to Receiver Information (72)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','21','Cheque Number (21)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','30','Date of Issue (30)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','32A','Amount (32A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','32B','Amount (32B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','52A','Drawer''s Bank (52A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','52B','Drawer''s Bank (52B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','52D','Drawer''s Bank (52D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 110','59','Payee (59)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 111','30','Date Of Issue (30)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 111','32A','Amount (32A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 111','32B','Amount (32B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 111','52A','Drawer''s Bank (52A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 111','52B','Drawer''s Bank (52B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 111','52D','Drawer''s Bank (52D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 111','59','Payee (59)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 111','75','Queries (75)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 192','79','Narrative Description of the Original Message (79)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','20','Transaction Reference Number (20)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','21','Transaction Reference (21)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','32A','Value Date, Currency Code, Amount (32A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','58D','Beneficiary Institution (58D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 210','20','Transaction Reference Number (20)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 210','30','Value Date (30)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 210','21','Related Reference (21)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 210','32B','Currency Code, Amount (32B)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 210','52D','Ordering Institution (52D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 210','25','Account Identification (25)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202','15A','New Sequence (15A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','20','Transaction Reference Number (20)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','21','Related Reference (21)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','32A','Value Date, Currency Code, Amount (3A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 202_COVER','58D','Beneficiary Institution (58D)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 210','50A','Ordering Customer (50A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 210','52A','Ordering Institution (52A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 210','56A','Intermediary (56A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','23E','Instruction Code (23E)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','26T','Transaction Type Code (26T)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','50A','Ordering Customer (50A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','52A','Ordering Institution (52A)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','70','Remittance Information (70)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','71F','Sender''s Charges (71F)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','72','Sender to Receiver Information (72)');
Insert into TJN_PYMNTMSG_MASTER (FORMAT,MESSAGE_TYPE,TAG_NAME,FIELD_LABEL) values ('SWIFT','MT 103','77B','Regulatory Reporting (77B)');






