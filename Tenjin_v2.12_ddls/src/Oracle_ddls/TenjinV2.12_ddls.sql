--------------------------------------------------------
--  File created - Friday-March-29-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ACTIVERUNUSERS
--------------------------------------------------------

  CREATE TABLE "ACTIVERUNUSERS" 
   (	"RUN_ID" NUMBER, 
	"START_TIME" DATE, 
	"RUN_USER_NAME" VARCHAR2(20), 
	"MACHINE_IP" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table AUT_FUNC_FIELDS
--------------------------------------------------------

  CREATE TABLE "AUT_FUNC_FIELDS" 
   (	"FLD_APP" NUMBER, 
	"FLD_FUNC_CODE" VARCHAR2(50), 
	"FLD_SEQ_NO" NUMBER, 
	"FLD_UNAME" VARCHAR2(500), 
	"FLD_LABEL" VARCHAR2(500), 
	"FLD_TAB_ORDER" NUMBER, 
	"FLD_INSTANCE" NUMBER, 
	"FLD_INDEX" NUMBER, 
	"FLD_UID_TYPE" VARCHAR2(20), 
	"FLD_UID" VARCHAR2(300), 
	"FLD_TYPE" VARCHAR2(100), 
	"FLD_PAGE_AREA" VARCHAR2(100), 
	"TXNMODE" CHAR(1), 
	"VIEWMODE" VARCHAR2(10), 
	"MANDATORY" VARCHAR2(3), 
	"FLD_DEFAULT_OPTIONS" VARCHAR2(4000), 
	"FLD_IS_ENABLED" VARCHAR2(1), 
	"FLD_IS_FOOTER_ELEMENT" VARCHAR2(1), 
	"FLD_HAS_LOV" VARCHAR2(1), 
	"FLD_IS_MULTI_REC" VARCHAR2(20), 
	"FLD_GROUP" VARCHAR2(100), 
	"FLD_HAS_AUTOLOV" VARCHAR2(20), 
	"FLD_API_OPERATION" VARCHAR2(100), 
	"FLD_API_ENTITY_TYPE" VARCHAR2(10), 
	"FLD_API_RESP_TYPE" NUMBER
   ) ;

   COMMENT ON COLUMN "AUT_FUNC_FIELDS"."FLD_API_ENTITY_TYPE" IS 'Request / Response';
--------------------------------------------------------
--  DDL for Table AUT_FUNC_FIELDS_HPORDM
--------------------------------------------------------

  CREATE TABLE "AUT_FUNC_FIELDS_HPORDM" 
   (	"FLD_APP" NUMBER, 
	"FLD_FUNC_CODE" VARCHAR2(50), 
	"FLD_SEQ_NO" NUMBER, 
	"FLD_UNAME" VARCHAR2(400), 
	"FLD_LABEL" VARCHAR2(400), 
	"FLD_TAB_ORDER" NUMBER, 
	"FLD_INSTANCE" NUMBER, 
	"FLD_INDEX" NUMBER, 
	"FLD_UID_TYPE" VARCHAR2(20), 
	"FLD_UID" VARCHAR2(300), 
	"FLD_TYPE" VARCHAR2(100), 
	"FLD_PAGE_AREA" VARCHAR2(100), 
	"TXNMODE" CHAR(1), 
	"VIEWMODE" VARCHAR2(10), 
	"MANDATORY" VARCHAR2(3), 
	"FLD_DEFAULT_OPTIONS" VARCHAR2(4000), 
	"FLD_IS_ENABLED" VARCHAR2(1), 
	"FLD_IS_FOOTER_ELEMENT" VARCHAR2(1), 
	"FLD_HAS_LOV" VARCHAR2(1), 
	"FLD_IS_MULTI_REC" VARCHAR2(20), 
	"FLD_GROUP" VARCHAR2(100), 
	"FLD_HAS_AUTOLOV" VARCHAR2(20), 
	"FLD_API_OPERATION" VARCHAR2(100), 
	"FLD_API_ENTITY_TYPE" VARCHAR2(10), 
	"FLD_API_RESP_TYPE" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table AUT_FUNC_FIELD_INFO
--------------------------------------------------------

  CREATE TABLE "AUT_FUNC_FIELD_INFO" 
   (	"APP_ID" NUMBER(*,0), 
	"FUNC_CODE" VARCHAR2(255), 
	"PA_NAME" VARCHAR2(255), 
	"FLD_UNAME" VARCHAR2(255), 
	"FLD_LOGICAL_NAME" VARCHAR2(255), 
	"FLD_USER_REMARKS" VARCHAR2(2000)
   ) ;
--------------------------------------------------------
--  DDL for Table AUT_FUNC_PAGEAREAS
--------------------------------------------------------

  CREATE TABLE "AUT_FUNC_PAGEAREAS" 
   (	"PA_APP" NUMBER, 
	"PA_FUNC_CODE" VARCHAR2(50), 
	"PA_NAME" VARCHAR2(100), 
	"PA_PARENT" VARCHAR2(100), 
	"PA_SEQ_NO" NUMBER, 
	"PA_TYPE" VARCHAR2(25), 
	"PA_WAY_IN" VARCHAR2(300), 
	"PA_WAY_OUT" VARCHAR2(100), 
	"PA_SOURCE" CLOB, 
	"PA_PATH" VARCHAR2(400), 
	"TXNMODE" CHAR(1), 
	"PA_LABEL" VARCHAR2(100), 
	"PA_SCREEN_TITLE" VARCHAR2(200), 
	"PA_IS_MRB" VARCHAR2(1), 
	"PA_API_OPERATION" VARCHAR2(100), 
	"PA_API_ENTITY_TYPE" VARCHAR2(10), 
	"PA_API_RESP_TYPE" NUMBER
   ) ;

   COMMENT ON COLUMN "AUT_FUNC_PAGEAREAS"."PA_API_ENTITY_TYPE" IS 'Request / Response';
--------------------------------------------------------
--  DDL for Table AUT_FUNC_PAGEAREAS_HPORDM
--------------------------------------------------------

  CREATE TABLE "AUT_FUNC_PAGEAREAS_HPORDM" 
   (	"PA_APP" NUMBER, 
	"PA_FUNC_CODE" VARCHAR2(50), 
	"PA_NAME" VARCHAR2(100), 
	"PA_PARENT" VARCHAR2(100), 
	"PA_SEQ_NO" NUMBER, 
	"PA_TYPE" VARCHAR2(25), 
	"PA_WAY_IN" VARCHAR2(300), 
	"PA_WAY_OUT" VARCHAR2(100), 
	"PA_SOURCE" CLOB, 
	"PA_PATH" VARCHAR2(400), 
	"TXNMODE" CHAR(1), 
	"PA_LABEL" VARCHAR2(100), 
	"PA_SCREEN_TITLE" VARCHAR2(200), 
	"PA_IS_MRB" VARCHAR2(1), 
	"PA_API_OPERATION" VARCHAR2(100), 
	"PA_API_ENTITY_TYPE" VARCHAR2(10), 
	"PA_API_RESP_TYPE" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table AUT_MRB_LINKAGES
--------------------------------------------------------

  CREATE TABLE "AUT_MRB_LINKAGES" 
   (	"MRB_ID" NUMBER, 
	"APP_ID" NUMBER, 
	"FUNC_CODE" VARCHAR2(20), 
	"MRB_NAME" VARCHAR2(100), 
	"MRB_PARENT" VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table AUT_TEMPLATE_RULES
--------------------------------------------------------

  CREATE TABLE "AUT_TEMPLATE_RULES" 
   (	"APP_ID" NUMBER, 
	"FUNC_CODE" VARCHAR2(100), 
	"PA_NAME" VARCHAR2(100), 
	"TEMPLATE_RULE" VARCHAR2(1)
   ) ;
--------------------------------------------------------
--  DDL for Table BUILDRUN_DETAILS
--------------------------------------------------------

  CREATE TABLE "BUILDRUN_DETAILS" 
   (	"BUILD_NUMBER" NUMBER, 
	"RUNID" NUMBER, 
	"TC_NAME" VARCHAR2(200), 
	"PROJECT_NAME" VARCHAR2(200), 
	"RUN_RESULT" VARCHAR2(200)
   ) ;
--------------------------------------------------------
--  DDL for Table EXTR_AUDIT_TRAIL
--------------------------------------------------------

  CREATE TABLE "EXTR_AUDIT_TRAIL" 
   (	"EXTR_ID" NUMBER, 
	"EXTR_USER_ID" VARCHAR2(20), 
	"EXTR_IP_ADDRESS" VARCHAR2(50), 
	"EXTR_APP_ID" NUMBER, 
	"EXTR_FUNC_CODE" VARCHAR2(100), 
	"EXTR_START_TIME" TIMESTAMP (6), 
	"EXTR_END_TIME" TIMESTAMP (6), 
	"EXTR_INP_FILE_PATH" VARCHAR2(500), 
	"EXTR_STATUS" VARCHAR2(10), 
	"EXTR_MESSAGE" VARCHAR2(300), 
	"EXTR_RUN_ID" NUMBER, 
	"EXTR_AUT_USER_TYPE" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table EXTR_AUDIT_TRAIL_DETAIL
--------------------------------------------------------

  CREATE TABLE "EXTR_AUDIT_TRAIL_DETAIL" 
   (	"EXTR_ID" NUMBER, 
	"EXTR_RUN_ID" NUMBER, 
	"EXTR_APP_ID" NUMBER, 
	"EXTR_FUNC_CODE" VARCHAR2(100), 
	"EXTR_TDUID" VARCHAR2(50), 
	"EXTR_QUERY_FIELDS" VARCHAR2(300), 
	"EXTR_RESULT" VARCHAR2(20), 
	"EXTR_MESSAGE" VARCHAR2(500), 
	"EXTR_DATA" CLOB, 
	"EXTR_SS_ROW_NUM" NUMBER, 
	"EXTR_TDGID" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table FAILED_LOGIN_ATTEMPTS
--------------------------------------------------------

  CREATE TABLE "FAILED_LOGIN_ATTEMPTS" 
   (	"USER_ID" VARCHAR2(20), 
	"TERMINAL" VARCHAR2(100), 
	"TIME_STAMP" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table HIST_TESTDATA
--------------------------------------------------------

  CREATE TABLE "HIST_TESTDATA" 
   (	"SCRIPT_ID" VARCHAR2(20), 
	"ITERATION" NUMBER, 
	"FIELD_NAME" VARCHAR2(300), 
	"DATA_VAL" VARCHAR2(500), 
	"DATA_SEQ" NUMBER, 
	"RUN_ID" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table HIST_USR_PWD
--------------------------------------------------------

  CREATE TABLE "HIST_USR_PWD" 
   (	"HUP_REC_ID" NUMBER, 
	"HUP_TIMESTAMP" TIMESTAMP (6), 
	"HUP_USER_ID" VARCHAR2(20), 
	"HUP_EPASS" VARCHAR2(512),
	"PWD_CHANGED_BY" VARCHAR2(20),
	"PWD_CHANGE_REASON" VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table ITERATIONLOG
--------------------------------------------------------

  CREATE TABLE "ITERATIONLOG" 
   (	"ITERATION_NO" NUMBER(*,0), 
	"CONTEXT" VARCHAR2(250), 
	"RESULT" CHAR(1), 
	"ERRORMSG" VARCHAR2(4000), 
	"SCREENSHOT" BLOB, 
	"SCRIPT_ID" VARCHAR2(30), 
	"RUN_ID" NUMBER, 
	"SCR_TIMESTAMP" VARCHAR2(20), 
	"WS_REQ_MSG" CLOB, 
	"WS_RES_MSG" CLOB
   ) ;
--------------------------------------------------------
--  DDL for Table LRNR_AUDIT_TRAIL
--------------------------------------------------------

  CREATE TABLE "LRNR_AUDIT_TRAIL" 
   (	"LRNR_ID" NUMBER, 
	"LRNR_USER_ID" VARCHAR2(20), 
	"LRNR_IP_ADDRESS" VARCHAR2(50), 
	"LRNR_APP_ID" NUMBER, 
	"LRNR_FUNC_CODE" VARCHAR2(100), 
	"LRNR_START_TIME" TIMESTAMP (6), 
	"LRNR_END_TIME" TIMESTAMP (6), 
	"LRNR_STATUS" VARCHAR2(10), 
	"LRNR_MESSAGE" VARCHAR2(300), 
	"LRNR_RUN_ID" NUMBER, 
	"LRNR_AUT_USER_TYPE" VARCHAR2(20),
	"FIELDS_LEARNT_COUNT" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table LRNR_AUDIT_TRAIL_API
--------------------------------------------------------

  CREATE TABLE "LRNR_AUDIT_TRAIL_API" 
   (	"LRNR_ID" NUMBER, 
	"LRNR_USER_ID" VARCHAR2(20), 
	"LRNR_APP_ID" NUMBER, 
	"LRNR_API_CODE" VARCHAR2(50), 
	"LRNR_OPERATION" VARCHAR2(100), 
	"LRNR_START_TIME" TIMESTAMP (6), 
	"LRNR_END_TIME" TIMESTAMP (6), 
	"LRNR_STATUS" VARCHAR2(50), 
	"LRNR_MESSAGE" VARCHAR2(300), 
	"LRNR_RUN_ID" NUMBER, 
	"LRNR_REQ_DESC" CLOB, 
	"LRNR_RESP_DESC" CLOB,
	"FIELDS_LEARNT_COUNT" NUMBER
	 ) ;
--------------------------------------------------------
--  DDL for Table LRNR_FUNC_ACTIONS
--------------------------------------------------------

  CREATE TABLE "LRNR_FUNC_ACTIONS" 
   (	"APP_ID" NUMBER, 
	"FUNC_CODE" VARCHAR2(300), 
	"LRNR_ACTION" VARCHAR2(1000)
   ) ;
--------------------------------------------------------
--  DDL for Table MANUAL_FIELD_MAP
--------------------------------------------------------

  CREATE TABLE "MANUAL_FIELD_MAP" 
   (	"APP_ID" NUMBER, 
	"FUNCTION_NAME" VARCHAR2(20), 
	"TEST_STEP_ID" VARCHAR2(30), 
	"PAGE_AREA" VARCHAR2(200), 
	"MANUAL_FIELD_LABEL" VARCHAR2(100), 
	"MANUAL_STATUS" VARCHAR2(20), 
	"TEST_CASE_ID" NUMBER(9,0)
   ) ;
--------------------------------------------------------
--  DDL for Table MAPDETAIL
--------------------------------------------------------

  CREATE TABLE "MAPDETAIL" 
   (	"MAP_ID" NUMBER(*,0), 
	"MODE1_PAGEAREA" VARCHAR2(100), 
	"MODE1_FIELD" VARCHAR2(100), 
	"MODE2_PAGEAREA" VARCHAR2(100), 
	"MODE2_FIELD" VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table MAPMASTER
--------------------------------------------------------

  CREATE TABLE "MAPMASTER" 
   (	"MAP_ID" NUMBER(*,0), 
	"APP_ID" NUMBER(*,0), 
	"MODULE_CODE" VARCHAR2(100), 
	"MODE1" CHAR(1), 
	"MODE2" CHAR(1)
   ) ;
--------------------------------------------------------
--  DDL for Table MASAPPLICATION
--------------------------------------------------------

  CREATE TABLE "MASAPPLICATION" 
   (	"APP_ID" NUMBER, 
	"APP_NAME" VARCHAR2(100), 
	"APP_URL" VARCHAR2(200), 
	"APP_LOGIN_REQD" VARCHAR2(3), 
	"APP_LOGIN_NAME" VARCHAR2(50), 
	"APP_PASSWORD" VARCHAR2(200), 
	"APP_DEF_BROWSER" VARCHAR2(50), 
	"APP_ADAPTER_PACKAGE" VARCHAR2(20), 
	"APP_OPERATIONS" VARCHAR2(500), 
	"APP_USERTYPES" VARCHAR2(500) DEFAULT 'Maker,Checker', 
	"API_BASE_URL" VARCHAR2(200), 
	"APP_DATE_FORMAT" VARCHAR2(50), 
	"APP_PLATFORM" VARCHAR2(20), 
	"APP_PLATFORM_VERSION" VARCHAR2(20), 
	"APP_PACKAGE" VARCHAR2(255), 
	"APP_ACTIVITY" VARCHAR2(255), 
	"APP_TYPE" NUMBER(5,0), 
	"API_AUTH_TYPE" VARCHAR2(100), 
	"APP_PAUSE_TIME" NUMBER, 
	"APP_PAUSE_LOCATION" VARCHAR2(20), 
	"APP_FILE_NAME" VARCHAR2(200), 
	"OAUTH_GRANT_TYPE" VARCHAR2(50), 
	"OAUTH_CALLBACK_URL" VARCHAR2(500), 
	"OAUTH_AUTH_URL" VARCHAR2(500), 
	"OAUTH_ACCESS_TOKEN_URL" VARCHAR2(500), 
	"OAUTH_CLIENT_ID" VARCHAR2(200), 
	"OAUTH_CLIENT_SECRET" VARCHAR2(200), 
	"OAUTH_ADDAUTHDATA_TO" VARCHAR2(10),
	"TEMPLATE_PASSWORD" VARCHAR2(200)
   ) ;

   COMMENT ON COLUMN "MASAPPLICATION"."APP_PLATFORM" IS '''Android'',''iOS'',''Web''';
--------------------------------------------------------
--  DDL for Table MASDEVICES
--------------------------------------------------------

  CREATE TABLE "MASDEVICES" 
   (	"DEVICE_REC_ID" NUMBER, 
	"DEVICE_ID" VARCHAR2(255), 
	"DEVICE_PLATFORM" VARCHAR2(255), 
	"DEVICE_PLATFORM_VERSION" VARCHAR2(255), 
	"DEVICE_REGISTERED_ON" DATE, 
	"DEVICE_REGISTERED_BY" VARCHAR2(255), 
	"DEVICE_NAME" VARCHAR2(255), 
	"DEVICE_TYPE" VARCHAR2(50), 
	"DEVICE_DESC" VARCHAR2(3000), 
	"DEVICE_STATUS" VARCHAR2(30)
   ) ;
--------------------------------------------------------
--  DDL for Table MASMODULE
--------------------------------------------------------

  CREATE TABLE "MASMODULE" 
   (	"APP_ID" NUMBER, 
	"MODULE_CODE" VARCHAR2(100), 
	"MODULE_NAME" VARCHAR2(200), 
	"MODULE_MENU_CONTAINER" VARCHAR2(200), 
	"WS_URL" VARCHAR2(1000), 
	"WS_OPERATION" VARCHAR2(500), 
	"WS_XML" CLOB, 
	"END_TIMESTAMP" DATE, 
	"WS_END_TIMESTAMP" DATE, 
	"START_TIMESTAMP" DATE, 
	"GROUP_NAME" VARCHAR2(100) DEFAULT 'NONE', 
	"APP_DATE_FORMAT" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table MASMODULE_WS
--------------------------------------------------------

  CREATE TABLE "MASMODULE_WS" 
   (	"APP_ID" NUMBER, 
	"MODULE_CODE" VARCHAR2(100), 
	"MODULE_NAME" VARCHAR2(200), 
	"WS_URL" VARCHAR2(1000), 
	"WS_NAME" VARCHAR2(1000), 
	"WS_OPERATION" VARCHAR2(500), 
	"WS_XML" CLOB, 
	"WS_START_TIMESTAMP" DATE, 
	"WS_END_TIMESTAMP" DATE, 
	"WS_GROUP_NAME" VARCHAR2(100) DEFAULT 'NONE'
   ) ;
--------------------------------------------------------
--  DDL for Table MASPROJECTROLES
--------------------------------------------------------

  CREATE TABLE "MASPROJECTROLES" 
   (	"ROLE_ID" NUMBER, 
	"ROLE_DESC" VARCHAR2(30)
   ) ;
--------------------------------------------------------
--  DDL for Table MASREGCLIENTS
--------------------------------------------------------

  CREATE TABLE "MASREGCLIENTS" 
   (	"RC_NAME" VARCHAR2(15), 
	"RC_HOST" VARCHAR2(15), 
	"RC_PORT" VARCHAR2(5), 
	"RC_STATUS" VARCHAR2(8), 
	"RC_MOB_PORT" VARCHAR2(5), 
	"RC_REC_ID" NUMBER, 
	"RC_CREATED_ON" DATE, 
	"RC_CREATED_BY" VARCHAR2(20), 
	"RC_DEVICE_FARM_USERNAME" VARCHAR2(50), 
	"RC_DEVICE_FARM_PASSWORD" VARCHAR2(400), 
	"RC_DEVICE_FARM_FLAG" VARCHAR2(5), 
	"RC_DEVICE_FARM_KEY" VARCHAR2(400), 
	"RC_OS_TYPE" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table MASRESVALDEF
--------------------------------------------------------

  CREATE TABLE "MASRESVALDEF" 
   (	"RVD_ID" NUMBER, 
	"RVD_NAME" VARCHAR2(100), 
	"RVD_APP_ID" NUMBER(*,0), 
	"RVD_DESC" VARCHAR2(1000), 
	"RVD_TYPE" VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table MASTESTDATAPATH
--------------------------------------------------------

  CREATE TABLE "MASTESTDATAPATH" 
   (	"PRJ_ID" NUMBER, 
	"APP_ID" NUMBER, 
	"FUNC_CODE" VARCHAR2(100), 
	"TEST_DATA_PATH" VARCHAR2(500)
   ) ;
--------------------------------------------------------
--  DDL for Table MASTESTRUNS
--------------------------------------------------------

  CREATE TABLE "MASTESTRUNS" 
   (	"RUN_ID" NUMBER, 
	"RUN_PRJ_ID" NUMBER, 
	"RUN_TS_REC_ID" NUMBER, 
	"RUN_START_TIME" DATE, 
	"RUN_END_TIME" DATE, 
	"RUN_USER" VARCHAR2(20), 
	"RUN_STATUS" VARCHAR2(15), 
	"TOTAL_TCS" NUMBER, 
	"TOTAL_EXE_TCS" NUMBER, 
	"TOTAL_PASSED_TCS" NUMBER, 
	"TOTAL_FAILED_TCS" NUMBER, 
	"TOTAL_ERROR_TCS" NUMBER, 
	"RUN_MACHINE_IP" VARCHAR2(20), 
	"RUN_BROWSER_TYPE" VARCHAR2(30), 
	"RUN_PRJ_NAME" VARCHAR2(100), 
	"RUN_DOMAIN_NAME" VARCHAR2(100), 
	"RUN_EXEC_SPEED" NUMBER(1,0) DEFAULT 0, 
	"RUN_TASK_TYPE" VARCHAR2(15), 
	"RUN_CL_PORT" NUMBER, 
	"RUN_APP_ID" NUMBER, 
	"PARENT_RUN_ID" NUMBER, 
	"RUN_DEVICE_REC_ID" NUMBER, 
	"RUN_DEVICE_FARM" VARCHAR2(20),
	"RUN_JSON" CLOB
   ) ;
--------------------------------------------------------
--  DDL for Table MASUIVALDEF
--------------------------------------------------------

  CREATE TABLE "MASUIVALDEF" 
   (	"UI_VAL_ID" NUMBER, 
	"UI_VAL_NAME" VARCHAR2(100), 
	"UI_VAL_DESC" CLOB, 
	"UI_TARGET" VARCHAR2(20), 
	"UI_VAL_TYPE" VARCHAR2(20) DEFAULT 'DIRECT', 
	"UI_FORMULA" VARCHAR2(255), 
	"UI_DEF_EXPECTED_VALUES" VARCHAR2(255)
   ) ;
--------------------------------------------------------
--  DDL for Table MASUSER
--------------------------------------------------------

  CREATE TABLE "MASUSER" 
   (	"USER_ID" VARCHAR2(20), 
	"FIRST_NAME" VARCHAR2(50), 
	"LAST_NAME" VARCHAR2(50), 
	"EMAIL_ID" VARCHAR2(100), 
	"PHONE_1" VARCHAR2(15), 
	"PHONE_2" VARCHAR2(15), 
	"PASS_WORD" VARCHAR2(200), 
	"USER_ENABLED" VARCHAR2(1) DEFAULT 'Y'
   ) ;
--------------------------------------------------------
--  DDL for Table MASUSERGROUPS
--------------------------------------------------------

  CREATE TABLE "MASUSERGROUPS" 
   (	"GROUP_ID" NUMBER(*,0), 
	"GROUP_DESC" VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table MASUSER_BKP
--------------------------------------------------------

  CREATE TABLE "MASUSER_BKP" 
   (	"USER_ID" VARCHAR2(20), 
	"FIRST_NAME" VARCHAR2(50), 
	"LAST_NAME" VARCHAR2(50), 
	"EMAIL_ID" VARCHAR2(100), 
	"PHONE_1" VARCHAR2(15), 
	"PHONE_2" VARCHAR2(15), 
	"PASS_WORD" VARCHAR2(200)
   ) ;
--------------------------------------------------------
--  DDL for Table PCLOUDYDEVICES
--------------------------------------------------------

  CREATE TABLE "PCLOUDYDEVICES" 
   (	"DEVICE_ID" NUMBER, 
	"FULL_NAME" VARCHAR2(100), 
	"MODEL" VARCHAR2(20), 
	"CPU" VARCHAR2(20), 
	"DISPLAY_NAME" VARCHAR2(50), 
	"PLATFORM" VARCHAR2(20), 
	"VERSION" VARCHAR2(20), 
	"MANUFACTURER" VARCHAR2(20), 
	"DPI" VARCHAR2(20), 
	"RAM" NUMBER, 
	"RESOLUTION" VARCHAR2(20), 
	"DISPLAY_AREA" VARCHAR2(20), 
	"AVAILABLE" VARCHAR2(20), 
	"NETWORK" VARCHAR2(50), 
	"MOBILE_NUMBER" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table REGCLIENTS_DEVICES
--------------------------------------------------------

  CREATE TABLE "REGCLIENTS_DEVICES" 
   (	"REG_DEV_ID" NUMBER, 
	"REG_DEV_CLIENT" NUMBER, 
	"REG_DEV_DEVICE_REC_ID" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table RESVALCONFIG
--------------------------------------------------------

  CREATE TABLE "RESVALCONFIG" 
   (	"RES_VAL_ID" NUMBER, 
	"RES_VAL_SEQ" NUMBER, 
	"RES_VAL_TYPE" VARCHAR2(30), 
	"RES_VAL_FUNC" VARCHAR2(30), 
	"RES_VAL_FLD" VARCHAR2(100), 
	"RES_VAL_PA" VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table RESVALMAP
--------------------------------------------------------

  CREATE TABLE "RESVALMAP" 
   (	"TSTEP_REC_ID" NUMBER, 
	"RES_VAL_ID" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table RESVALRESULTS
--------------------------------------------------------

  CREATE TABLE "RESVALRESULTS" 
   (	"RUN_ID" NUMBER, 
	"RES_VAL_ID" NUMBER, 
	"TSTEP_REC_ID" NUMBER, 
	"RES_VAL_PAGE" VARCHAR2(100), 
	"RES_VAL_FIELD" VARCHAR2(100), 
	"RES_VAL_EXP_VAL" VARCHAR2(1000), 
	"RES_VAL_ACT_VAL" VARCHAR2(1000), 
	"RES_VAL_STATUS" VARCHAR2(30), 
	"ITERATION" NUMBER, 
	"RES_VAL_DETAIL_ID" VARCHAR2(10)
   ) ;
--------------------------------------------------------
--  DDL for Table RUNRESULTS_TC
--------------------------------------------------------

  CREATE TABLE "RUNRESULTS_TC" 
   (	"RUN_ID" NUMBER, 
	"TC_REC_ID" NUMBER, 
	"TC_TOTAL_STEPS" NUMBER, 
	"TC_TOTAL_EXEC" NUMBER, 
	"TC_TOTAL_PASS" NUMBER, 
	"TC_TOTAL_FAIL" NUMBER, 
	"TC_TOTAL_ERROR" NUMBER, 
	"TC_STATUS" VARCHAR2(30)
   ) ;
--------------------------------------------------------
--  DDL for Table RUNRESULTS_TS
--------------------------------------------------------

  CREATE TABLE "RUNRESULTS_TS" 
   (	"RUN_ID" NUMBER, 
	"TC_REC_ID" NUMBER, 
	"TSTEP_REC_ID" NUMBER, 
	"TSTEP_TOTAL_TXN" NUMBER, 
	"TSTEP_TOTAL_EXEC" NUMBER, 
	"TSTEP_TOTAL_PASS" NUMBER, 
	"TSTEP_TOTAL_FAIL" NUMBER, 
	"TSTEP_TOTAL_ERROR" NUMBER, 
	"TSTEP_STATUS" VARCHAR2(30)
   ) ;
--------------------------------------------------------
--  DDL for Table RUNRESULT_TS_TXN
--------------------------------------------------------

  CREATE TABLE "RUNRESULT_TS_TXN" 
   (	"RUN_ID" NUMBER(*,0), 
	"TXN_NO" NUMBER, 
	"TSTEP_REC_ID" NUMBER, 
	"TSTEP_RESULT" CHAR(1), 
	"TSTEP_MESSAGE" VARCHAR2(4000), 
	"TSTEP_SCRSHOT" BLOB, 
	"TSTEP_WS_REQ_MSG" CLOB, 
	"TSTEP_WS_RES_MSG" CLOB, 
	"TSTEP_TDUID" VARCHAR2(100), 
	"EXEC_START_TIME" DATE, 
	"EXEC_END_TIME" DATE, 
	"API_ACCESS_TOKEN" VARCHAR2(500), 
	"API_AUTH_TYPE" VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table TC_TSTEP_DEPENDENCY_RULES
--------------------------------------------------------

  CREATE TABLE "TC_TSTEP_DEPENDENCY_RULES" 
   (	"RULE_ID" NUMBER, 
	"TC_REC_ID" NUMBER, 
	"TSTEP_REC_ID" NUMBER, 
	"DEPENDENT_TSTEP_REC_ID" NUMBER, 
	"EXEC_STATUS" VARCHAR2(255), 
	"DEPENDENCY_TYPE" VARCHAR2(16)
   ) ;
--------------------------------------------------------
--  DDL for Table TESTCASES
--------------------------------------------------------

  CREATE TABLE "TESTCASES" 
   (	"TC_REC_ID" NUMBER, 
	"TC_REC_TYPE" VARCHAR2(3), 
	"TC_ID" VARCHAR2(100), 
	"TC_FOLDER_ID" NUMBER, 
	"TC_PRJ_ID" NUMBER, 
	"TC_NAME" VARCHAR2(200), 
	"TC_TYPE" VARCHAR2(60), 
	"TC_PRIORITY" VARCHAR2(10), 
	"TC_REVIEWED" VARCHAR2(3), 
	"TC_STATUS" VARCHAR2(60), 
	"TC_CREATED_ON" DATE, 
	"TC_MODIFIED_ON" DATE, 
	"TC_DESC" VARCHAR2(3000), 
	"TC_CREATED_BY" VARCHAR2(40), 
	"TC_MODIFIED_BY" VARCHAR2(40), 
	"TC_STORAGE" VARCHAR2(6) DEFAULT 'Local', 
	"TC_EXEC_MODE" VARCHAR2(20), 
	"TC_PRESET_RULES" VARCHAR2(10), 
	"TC_APP_ID" NUMBER,
	"TC_TAG"VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table TESTDATA
--------------------------------------------------------

  CREATE TABLE "TESTDATA" 
   (	"SCRIPT_ID" VARCHAR2(20), 
	"ITERATION" NUMBER, 
	"FIELD_NAME" VARCHAR2(300), 
	"DATA_VAL" VARCHAR2(500), 
	"DATA_SEQ" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table TESTSETMAP
--------------------------------------------------------

  CREATE TABLE "TESTSETMAP" 
   (	"TSM_PRJ_ID" NUMBER, 
	"TSM_TS_REC_ID" NUMBER, 
	"TSM_TC_REC_ID" NUMBER, 
	"TSM_TC_SEQ" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table TESTSETS
--------------------------------------------------------

  CREATE TABLE "TESTSETS" 
   (	"TS_NAME" VARCHAR2(100), 
	"TS_REC_ID" NUMBER, 
	"TS_REC_TYPE" VARCHAR2(10), 
	"TS_FOLDER_ID" NUMBER, 
	"TS_STATUS" VARCHAR2(60), 
	"TS_CREATED_ON" DATE, 
	"TS_MODIFIED_ON" DATE, 
	"TS_CREATED_BY" VARCHAR2(40), 
	"TS_MODIFIED_BY" VARCHAR2(40), 
	"TS_PRIORITY" VARCHAR2(10), 
	"TS_REVIEWED" VARCHAR2(3), 
	"TS_DESC" VARCHAR2(3000), 
	"TS_PRJ_ID" NUMBER, 
	"TS_TYPE" VARCHAR2(60), 
	"TS_EXEC_MODE" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table TESTSTEPS
--------------------------------------------------------

  CREATE TABLE "TESTSTEPS" 
   (	"TSTEP_REC_ID" NUMBER, 
	"TC_REC_ID" NUMBER, 
	"TSTEP_SHT_DESC" VARCHAR2(256), 
	"TSTEP_DATA_ID" VARCHAR2(30), 
	"TSTEP_TYPE" VARCHAR2(50), 
	"TSTEP_DESC" VARCHAR2(4000), 
	"TSTEP_EXP_RESULT" VARCHAR2(4000), 
	"TSTEP_ACT_RESULT" VARCHAR2(1000), 
	"APP_ID" VARCHAR2(40), 
	"FUNC_CODE" VARCHAR2(40), 
	"TXN_CONTEXT" VARCHAR2(1000), 
	"EXEC_CONTEXT" VARCHAR2(1000), 
	"TXNMODE" VARCHAR2(8), 
	"TSTEP_ID" VARCHAR2(30), 
	"TSTEP_OPERATION" VARCHAR2(100), 
	"TSTEP_AUT_LOGIN_TYPE" VARCHAR2(30), 
	"TSTEP_ROWS_TO_EXEC" NUMBER, 
	"TSTEP_STORAGE" VARCHAR2(6) DEFAULT 'Local', 
	"TSTEP_EXEC_SEQ" NUMBER, 
	"TSTEP_VAL_TYPE" VARCHAR2(255) DEFAULT 'Functional', 
	"TSTEP_DEVICE" NUMBER, 
	"TSTEP_API_RESP_TYPE" NUMBER, 
	"TSTEP_RE_RUN" VARCHAR2(20), 
	"TSTEP_CREATED_BY" VARCHAR2(20), 
	"TSTEP_CREATED_ON" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_ADAPTER
--------------------------------------------------------

  CREATE TABLE "TJN_ADAPTER" 
   (	"NAME" VARCHAR2(50), 
	"OR_TOOL" NUMBER, 
	"APPLICATION_TYPE" NUMBER(3,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_ADAPTER_OR_TOOL
--------------------------------------------------------

  CREATE TABLE "TJN_ADAPTER_OR_TOOL" 
   (	"ID" NUMBER, 
	"NAME" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_APPLICATION_TYPE
--------------------------------------------------------

  CREATE TABLE "TJN_APPLICATION_TYPE" 
   (	"ID" NUMBER, 
	"NAME" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_ASSISTED_LEARNING
--------------------------------------------------------

  CREATE TABLE "TJN_ASSISTED_LEARNING" 
   (	"ASST_LRN_ID" NUMBER, 
	"APP_ID" NUMBER, 
	"FUNC_CODE" VARCHAR2(100), 
	"PAGEAREA_NAME" VARCHAR2(100), 
	"FIELD_NAME" VARCHAR2(100), 
	"FIELD_IDENTIFIEDBY" VARCHAR2(50), 
	"FIELD_SEQUENCE" NUMBER, 
	"ASST_LRN_DATA" VARCHAR2(256)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_ASSISTED_LRNG_AUDIT_TRAIL
--------------------------------------------------------

  CREATE TABLE "TJN_ASSISTED_LRNG_AUDIT_TRAIL" 
   (	"APP_ID" NUMBER, 
	"FUNC_CODE" VARCHAR2(100), 
	"UPDATED_ON" TIMESTAMP (6), 
	"UPDATED_BY" VARCHAR2(100), 
	"OLD_VALUE" CLOB, 
	"NEW_VALUE" CLOB
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_AUDIT
--------------------------------------------------------

  CREATE TABLE "TJN_AUDIT" 
   (	"AUDIT_ID" NUMBER, 
	"ENTITY_RECORD_ID" NUMBER, 
	"ENTITY_TYPE" VARCHAR2(20), 
	"LAST_MODIFIED_BY" VARCHAR2(20), 
	"LAST_MODIFIED_ON" DATE, 
	"OLD_DATA" VARCHAR2(4000), 
	"NEW_DATA" VARCHAR2(4000), 
	"ID" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_AUDIT_METADATA
--------------------------------------------------------

  CREATE TABLE "TJN_AUDIT_METADATA" 
   (	"AUDIT_TIMESTAMP" DATE, 
	"AUDIT_USER_ID" VARCHAR2(15), 
	"AUDIT_ACTION" VARCHAR2(15), 
	"STATUS" VARCHAR2(15)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_AUT_APIS
--------------------------------------------------------

  CREATE TABLE "TJN_AUT_APIS" 
   (	"APP_ID" NUMBER(*,0), 
	"API_CODE" VARCHAR2(50), 
	"API_NAME" VARCHAR2(100), 
	"API_TYPE" VARCHAR2(20), 
	"API_URL" VARCHAR2(200), 
	"GROUP_NAME" VARCHAR2(200)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_AUT_API_OPERATIONS
--------------------------------------------------------

  CREATE TABLE "TJN_AUT_API_OPERATIONS" 
   (	"APP_ID" NUMBER, 
	"API_CODE" VARCHAR2(100), 
	"API_OPERATION" VARCHAR2(100), 
	"API_METHOD_NAME" VARCHAR2(20), 
	"API_AUTH_TYPE" VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_AUT_API_OP_PARAMETERS
--------------------------------------------------------

  CREATE TABLE "TJN_AUT_API_OP_PARAMETERS" 
   (	"APP_ID" NUMBER, 
	"API_CODE" VARCHAR2(50), 
	"API_OPERATION" VARCHAR2(100), 
	"PARAMETER_NAME" VARCHAR2(100), 
	"PARAMETER_TYPE" VARCHAR2(100), 
	"PARAMETER_STYLE" VARCHAR2(100), 
	"REP_TYPE" VARCHAR2(100)
   )  ENABLE ROW MOVEMENT ;
--------------------------------------------------------
--  DDL for Table TJN_AUT_API_OP_REPRESENTATIONS
--------------------------------------------------------

  CREATE TABLE "TJN_AUT_API_OP_REPRESENTATIONS" 
   (	"APP_ID" NUMBER, 
	"API_CODE" VARCHAR2(50), 
	"API_OPERATION" VARCHAR2(100), 
	"MEDIA_TYPE" VARCHAR2(100), 
	"REPRESENTATION_TYPE" VARCHAR2(20), 
	"API_REPRESENTATION_DESCRIPTION" CLOB, 
	"API_RESP_CODE" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_AUT_GROUPS
--------------------------------------------------------

  CREATE TABLE "TJN_AUT_GROUPS" 
   (	"APP_ID" NUMBER, 
	"APP_NAME" VARCHAR2(105), 
	"GROUP_NAME" VARCHAR2(105), 
	"GROUP_DESC" VARCHAR2(1000)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_CURRENT_USERS
--------------------------------------------------------

  CREATE TABLE "TJN_CURRENT_USERS" 
   (	"USER_ID" VARCHAR2(20), 
	"TERMINAL" VARCHAR2(100), 
	"LOGIN_TIME" DATE, 
	"TJN_SESSION_ID" VARCHAR2(1024)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_DEF_COMMENT
--------------------------------------------------------

  CREATE TABLE "TJN_DEF_COMMENT" 
   (	"COMM_ID" NUMBER, 
	"AUTHOR" VARCHAR2(30), 
	"TEXT" CLOB, 
	"CREATED_ON" DATE, 
	"MODIFIED_BY" VARCHAR2(30), 
	"MODIFED_ON" DATE, 
	"DEF_ID" VARCHAR2(20), 
	"COMM_POST" VARCHAR2(3), 
	"REC_COMM_ID" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_DEF_TOOL_MASTER
--------------------------------------------------------

  CREATE TABLE "TJN_DEF_TOOL_MASTER" 
   (	"INSTANCE_NAME" VARCHAR2(20), 
	"TOOL_NAME" VARCHAR2(50), 
	"URL" VARCHAR2(200), 
	"ADMIN_ID" VARCHAR2(75), 
	"ADMIN_PWD" VARCHAR2(200)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_DEF_USER_MAPING
--------------------------------------------------------

  CREATE TABLE "TJN_DEF_USER_MAPING" 
   (	"TJN_USERID" VARCHAR2(20), 
	"TJN_INSTANCE_NAME" VARCHAR2(50), 
	"DEF_TOOL_USERID" VARCHAR2(50), 
	"DEF_TOOL_PWD" VARCHAR2(256)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_DOMAINS
--------------------------------------------------------

  CREATE TABLE "TJN_DOMAINS" 
   (	"DOMAIN_NAME" VARCHAR2(100), 
	"DOMAIN_CREATE_DATE" DATE, 
	"DOMAIN_CREATED_BY" VARCHAR2(10), 
	"DOMAIN_STATE" VARCHAR2(1)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_DTT_PROJECTS
--------------------------------------------------------

  CREATE TABLE "TJN_DTT_PROJECTS" 
   (	"PRJ_ID" VARCHAR2(20), 
	"PROJECT_SET_NAME" VARCHAR2(20), 
	"INSTANCE" VARCHAR2(20), 
	"PROJECTS" VARCHAR2(500), 
	"INSTANCE_NAME" VARCHAR2(50), 
	"USER_ID" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_DTT_PROPERTIES
--------------------------------------------------------

  CREATE TABLE "TJN_DTT_PROPERTIES" 
   (	"INSTANCE_NAME" VARCHAR2(20), 
	"PROPERTY_NAME" VARCHAR2(20), 
	"PROPERTY_SCORE" NUMBER, 
	"PROPERTY_VALUE" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_EXEC_OUTPUT
--------------------------------------------------------

  CREATE TABLE "TJN_EXEC_OUTPUT" 
   (	"RUN_ID" NUMBER, 
	"TDGID" VARCHAR2(30), 
	"TDUID" VARCHAR2(30), 
	"TSTEP_REC_ID" NUMBER(*,0), 
	"ITERATION_NO" NUMBER(*,0), 
	"DETAIL_NO" NUMBER(*,0), 
	"FIELD_NAME" VARCHAR2(100), 
	"FIELD_VALUE" VARCHAR2(500)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_EXEC_SCHEME
--------------------------------------------------------

  CREATE TABLE "TJN_EXEC_SCHEME" 
   (	"SCH_ID" NUMBER, 
	"SCH_NAME" VARCHAR2(500), 
	"SCH_DESC" VARCHAR2(1000), 
	"APP_ID" NUMBER, 
	"FUNC_CODE" VARCHAR2(20), 
	"BASE_PAGE_AREA" VARCHAR2(200), 
	"SCH_ENABLED" VARCHAR2(1), 
	"CREATE_TIMESTAMP" DATE, 
	"CREATED_BY" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_EXEC_SCHEME_CONFIG
--------------------------------------------------------

  CREATE TABLE "TJN_EXEC_SCHEME_CONFIG" 
   (	"SCH_ID" NUMBER, 
	"ACT_BLOCK" VARCHAR2(50), 
	"PAGE_AREA" VARCHAR2(200), 
	"FIELD" VARCHAR2(200), 
	"ACTION" VARCHAR2(20), 
	"SEQ_NO" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_EXEC_SCRNSHOT
--------------------------------------------------------

  CREATE TABLE "TJN_EXEC_SCRNSHOT" 
   (	"RUN_ID" NUMBER, 
	"TDGID" VARCHAR2(30), 
	"TDUID" VARCHAR2(30), 
	"TSTEP_REC_ID" NUMBER(*,0), 
	"ITERATION_NO" NUMBER(*,0), 
	"SEQ_NO" NUMBER(*,0), 
	"TYPE_SCRNSHOT" VARCHAR2(30), 
	"SCRNSHOT" BLOB, 
	"SCR_TIMESTAMP" DATE, 
	"SCR_DESC" VARCHAR2(1000)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_EXEC_SETTINGS
--------------------------------------------------------

  CREATE TABLE "TJN_EXEC_SETTINGS" 
   (	"RUN_ID" NUMBER, 
	"CLIENT" VARCHAR2(20), 
	"BROWSER" VARCHAR2(30), 
	"EXEC_SPEED" VARCHAR2(20), 
	"SCREENSHOT_OPTION" VARCHAR2(256)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_LOV
--------------------------------------------------------

  CREATE TABLE "TJN_LOV" 
   (	"LOV_NAME" VARCHAR2(20), 
	"NAME" VARCHAR2(50), 
	"LOV_DESC" VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_MAIL_ESCALATION_RULES
--------------------------------------------------------

  CREATE TABLE "TJN_MAIL_ESCALATION_RULES" 
   (	"TJN_MAIL_ESCALATION_RULE_ID" NUMBER, 
	"TJN_MAIL_ESCALATION_RULE_NAME" VARCHAR2(100), 
	"TJN_MAIL_ESCALATION_RULE_DESC" VARCHAR2(256)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_MAIL_LOGS
--------------------------------------------------------

  CREATE TABLE "TJN_MAIL_LOGS" 
   (	"RUN_ID" NUMBER, 
	"MAIL_SENDER_ID" VARCHAR2(256), 
	"MAIL_RECEIPIENT_LIST" VARCHAR2(1024), 
	"MAIL_SENT_TIME" TIMESTAMP (6), 
	"MAIL_CC_LIST" VARCHAR2(1024), 
	"SENT_STATUS" VARCHAR2(32)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_OAUTH_AUDIT_TRAIL
--------------------------------------------------------

  CREATE TABLE "TJN_OAUTH_AUDIT_TRAIL" 
   (	"UTKN_ID" NUMBER, 
	"ACCESS_TOKEN" VARCHAR2(500), 
	"TKN_GENERATED_BY" VARCHAR2(10), 
	"REFRESH_TOKEN" VARCHAR2(500), 
	"APP_ID" NUMBER, 
	"GENERATE_TIME" TIMESTAMP (6), 
	"UPDATE_TIME" TIMESTAMP (6), 
	"UAC_ID" NUMBER, 
	"RUN_ID" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_PARAMS
--------------------------------------------------------

  CREATE TABLE "TJN_PARAMS" 
   (	"PARAM_NAME" VARCHAR2(255), 
	"PARAM_VALUE" VARCHAR2(255)
   ) ;

   COMMENT ON COLUMN "TJN_PARAMS"."PARAM_NAME" IS 'Name of the Parameter';
   COMMENT ON COLUMN "TJN_PARAMS"."PARAM_VALUE" IS 'Parameter Value';
--------------------------------------------------------
--  DDL for Table TJN_PRJ_AUTS
--------------------------------------------------------

  CREATE TABLE "TJN_PRJ_AUTS" 
   (	"PRJ_ID" NUMBER, 
	"APP_ID" NUMBER, 
	"DTT_PPROJECT_KEY" VARCHAR2(50), 
	"PRJ_DEF_LOGGING" VARCHAR2(1) DEFAULT 'N', 
	"PRJ_DEF_MANAGER" VARCHAR2(50), 
	"PRJ_DM_PROJECT" VARCHAR2(50), 
	"AUT_URL" VARCHAR2(200), 
	"TEST_DATA_PATH" VARCHAR2(500), 
	"PROJECT_SET_NAME" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_PRJ_MAIL_SETTINGS
--------------------------------------------------------

  CREATE TABLE "TJN_PRJ_MAIL_SETTINGS" 
   (	"PRJ_ID" NUMBER, 
	"PRJ_MAIL_NOTIFICATIONS" VARCHAR2(1) DEFAULT 'Y', 
	"PRJ_MAIL_SUBJECT" VARCHAR2(256), 
	"TJN_PRJ_MAIL_ESCALATION" VARCHAR2(1) DEFAULT 'Y', 
	"TJN_PRJ_CONSOLIDATE_MAIL" VARCHAR2(1) DEFAULT 'Y', 
	"TJN_PRJ_CONSOLIDATE_MAIL_TIME" DATE, 
	"TJN_MAIL_ESCALATION_RULE_ID" NUMBER, 
	"TJN_PRJ_ESCALATION_MAILS" VARCHAR2(2048), 
	"TJN_PRJ_MAIL_ESCRULE_DESC" VARCHAR2(256)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_PRJ_TM_ATTRIBUTES
--------------------------------------------------------

  CREATE TABLE "TJN_PRJ_TM_ATTRIBUTES" 
   (	"PRJ_ID" NUMBER(10,0), 
	"REC_TYPE" VARCHAR2(10), 
	"TJN_FIELD_NAME" VARCHAR2(50), 
	"TM_FIELD_NAME" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_PRJ_TM_LINKAGE
--------------------------------------------------------

  CREATE TABLE "TJN_PRJ_TM_LINKAGE" 
   (	"PRJ_ID" NUMBER, 
	"TM_INSTANCE_NAME" VARCHAR2(100), 
	"TM_PROJECT" VARCHAR2(100), 
	"TM_ENABLED" VARCHAR2(2) DEFAULT NULL, 
	"TC_FILTER" VARCHAR2(30), 
	"TC_FILTER_VALUE" VARCHAR2(100), 
	"TS_FILTER" VARCHAR2(30), 
	"TS_FILTER_VALUE" VARCHAR2(100), 
	"TC_DISPLAY_VALUE" VARCHAR2(100), 
	"TS_DISPLAY_VALUE" VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_PRJ_USERS
--------------------------------------------------------

  CREATE TABLE "TJN_PRJ_USERS" 
   (	"PRJ_ID" NUMBER, 
	"USER_ID" VARCHAR2(30), 
	"USER_ROLE" VARCHAR2(30)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_PRJ_USER_SESSIONS
--------------------------------------------------------

  CREATE TABLE "TJN_PRJ_USER_SESSIONS" 
   (	"PRJ_USER_SESSSION_ID" NUMBER, 
	"PRJ_ID" NUMBER, 
	"USER_ID" VARCHAR2(20), 
	"ENTRY_TIME" TIMESTAMP (6), 
	"EXIT_TIME" TIMESTAMP (6)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_PROJECTS
--------------------------------------------------------

  CREATE TABLE "TJN_PROJECTS" 
   (	"PRJ_ID" NUMBER, 
	"PRJ_NAME" VARCHAR2(60), 
	"PRJ_TYPE" VARCHAR2(7), 
	"PRJ_CREATE_DATE" DATE, 
	"PRJ_DOMAIN" VARCHAR2(300), 
	"PRJ_OWNER" VARCHAR2(60), 
	"PRJ_DB_SERVER" VARCHAR2(200), 
	"PRJ_DESC" VARCHAR2(1000), 
	"PRJ_SCREENSHOT_OPTIONS" VARCHAR2(1) DEFAULT 'A', 
	"PRJ_STATE" VARCHAR2(1) DEFAULT 'A', 
	"PRJ_DEF_MANAGER" VARCHAR2(100), 
	"PRJ_DM_PROJECT" VARCHAR2(100), 
	"PRJ_CREATED_BY" VARCHAR2(30), 
	"PRJ_START_DATE" DATE, 
	"PRJ_END_DATE" DATE, 
	"PRJ_OWNER_EMAIL" VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_RUN_DEFECTS
--------------------------------------------------------

  CREATE TABLE "TJN_RUN_DEFECTS" 
   (	"DEF_REC_ID" NUMBER, 
	"DEF_ID" VARCHAR2(20), 
	"DEF_SUMMARY" VARCHAR2(500), 
	"DEF_DESC" VARCHAR2(4000), 
	"DEF_CREATED_BY" VARCHAR2(50), 
	"DEF_CREATED_ON" TIMESTAMP (6), 
	"DEF_MODIFIED_BY" VARCHAR2(50), 
	"DEF_MODIFIED_ON" TIMESTAMP (6), 
	"DEF_SEVERITY" VARCHAR2(30), 
	"DEF_PRIORITY" VARCHAR2(15), 
	"DEF_STATUS" VARCHAR2(15), 
	"DEF_POSTED" VARCHAR2(3) DEFAULT 'NO', 
	"DEF_LAST_REFRESHED" TIMESTAMP (6), 
	"RUN_ID" NUMBER, 
	"DEFECT_TYPE" VARCHAR2(20) DEFAULT 'Application', 
	"DTT_REASON" VARCHAR2(10), 
	"POST_TYPE" VARCHAR2(1), 
	"REMARKS" VARCHAR2(100), 
	"TEST_SET" VARCHAR2(20), 
	"TJN_STATUS" VARCHAR2(10), 
	"DTT_INSTANCE" VARCHAR2(100), 
	"DTT_PROJECT" VARCHAR2(100), 
	"REC_STATUS" VARCHAR2(1) DEFAULT 'A'
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_RUN_DEFECT_LINKAGE
--------------------------------------------------------

  CREATE TABLE "TJN_RUN_DEFECT_LINKAGE" 
   (	"LINK_ID" NUMBER, 
	"DEF_REC_ID" NUMBER, 
	"DEF_PROJECT_ID" NUMBER, 
	"DEF_RUN_ID" NUMBER, 
	"DEF_TC_REC_ID" NUMBER, 
	"DEF_TSTEP_REC_ID" NUMBER, 
	"DEF_TDUID" VARCHAR2(50), 
	"DEF_APP_ID" NUMBER, 
	"DEF_FUNCTION_CODE" VARCHAR2(30), 
	"DEF_API_CODE" VARCHAR2(30), 
	"DEF_OPERATION" VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_RUN_DEF_ATTACHMENTS
--------------------------------------------------------

  CREATE TABLE "TJN_RUN_DEF_ATTACHMENTS" 
   (	"ATT_FILE_CONTENT" BLOB, 
	"ATT_FILE_NAME" VARCHAR2(100), 
	"DEF_REC_ID" NUMBER, 
	"ATTACHMENT_ID" NUMBER, 
	"REC_ATTACHMENT_ID" NUMBER, 
	"FILE_PATH" VARCHAR2(150), 
	"ATT_POSTED" VARCHAR2(3)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_SCHEDULER
--------------------------------------------------------

  CREATE TABLE "TJN_SCHEDULER" 
   (	"SCH_ID" NUMBER, 
	"SCH_DATE" TIMESTAMP (6), 
	"SCH_TIME" VARCHAR2(20), 
	"ACTION" VARCHAR2(20), 
	"REG_CLIENT" VARCHAR2(20), 
	"STATUS" VARCHAR2(20), 
	"CREATED_BY" VARCHAR2(50), 
	"CREATED_ON" TIMESTAMP (6), 
	"RUN_ID" NUMBER, 
	"BROWSERTYPE" VARCHAR2(50), 
	"AUTLOGINTYPE" VARCHAR2(20), 
	"PROJECT_ID" NUMBER, 
	"SCH_RECURRING" VARCHAR2(10), 
	"SCH_TASKNAME" VARCHAR2(40), 
	"SCH_SCREENSHOT_OPTION" NUMBER, 
	"SCH_API_LEARN_TYPE" VARCHAR2(20), 
	"MESSAGE" VARCHAR2(1000)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_SCH_MAPPING
--------------------------------------------------------

  CREATE TABLE "TJN_SCH_MAPPING" 
   (	"SCH_SEQ_NO" NUMBER, 
	"SCH_ID" VARCHAR2(20), 
	"PROJECT" VARCHAR2(20), 
	"APP_ID" VARCHAR2(20), 
	"FUNC_CODE" VARCHAR2(20), 
	"TSET_ID" VARCHAR2(20), 
	"EXTRACT_FILE_PATH" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_SCH_RECURRENCE
--------------------------------------------------------

  CREATE TABLE "TJN_SCH_RECURRENCE" 
   (	"SCH_ID" NUMBER, 
	"FREQUENCY" VARCHAR2(20), 
	"RECUR_CYCLES" NUMBER, 
	"RECUR_DAYS" VARCHAR2(200), 
	"END_DATE" TIMESTAMP (6)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_TM_TOOL_MASTER
--------------------------------------------------------

  CREATE TABLE "TJN_TM_TOOL_MASTER" 
   (	"TM_REC_ID" NUMBER, 
	"TM_INSTANCE_NAME" VARCHAR2(20), 
	"TM_TOOL" VARCHAR2(50), 
	"TM_URL" VARCHAR2(200)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_TM_USER_MAPPING
--------------------------------------------------------

  CREATE TABLE "TJN_TM_USER_MAPPING" 
   (	"TJN_USER_ID" VARCHAR2(30), 
	"TJN_INSTANCE_NAME" VARCHAR2(30), 
	"USERNAME" VARCHAR2(30), 
	"PASSWORD" VARCHAR2(200)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_USER_PREFERENCE
--------------------------------------------------------

  CREATE TABLE "TJN_USER_PREFERENCE" 
   (	"USER_ID" VARCHAR2(20), 
	"PRJ_PREFERENCE" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table TSTEP_UI_VALIDATIONS
--------------------------------------------------------

  CREATE TABLE "TSTEP_UI_VALIDATIONS" 
   (	"REC_ID" NUMBER, 
	"TSTEP_REC_ID" NUMBER, 
	"UI_VAL_ID" NUMBER, 
	"PAGE_AREA" VARCHAR2(200), 
	"PRE_STEPS" CLOB, 
	"VAL_STEPS" CLOB
   ) ;
--------------------------------------------------------
--  DDL for Table UIVALRESULTS
--------------------------------------------------------

  CREATE TABLE "UIVALRESULTS" 
   (	"RUN_ID" NUMBER, 
	"UI_VAL_REC_ID" NUMBER, 
	"TSTEP_REC_ID" NUMBER, 
	"UI_VAL_PAGE" VARCHAR2(100), 
	"UI_VAL_FIELD" VARCHAR2(256), 
	"UI_VAL_EXPECTED" CLOB, 
	"UI_VAL_ACTUAL" CLOB, 
	"UI_VAL_STATUS" VARCHAR2(30), 
	"ITERATION" NUMBER, 
	"UI_VAL_DETAIL_ID" VARCHAR2(10)
   ) ;
--------------------------------------------------------
--  DDL for Table USERROLES
--------------------------------------------------------

  CREATE TABLE "USERROLES" 
   (	"ROLE_ID" NUMBER(*,0), 
	"USER_ID" VARCHAR2(20), 
	"GROUP_ID" NUMBER(*,0)
   ) ;
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "USERS" 
   (	"USERNAME" VARCHAR2(45), 
	"PASSWORD" VARCHAR2(45), 
	"ENABLED" NUMBER(*,0)
   ) ;
--------------------------------------------------------
--  DDL for Table USERSESSIONS
--------------------------------------------------------

  CREATE TABLE "USERSESSIONS" 
   (	"US_ID" NUMBER, 
	"US_USER_ID" VARCHAR2(20), 
	"US_LOGIN_TIMESTAMP" DATE, 
	"US_LOGOUT_TIMESTAMP" DATE, 
	"US_SESSION_STATE" VARCHAR2(1), 
	"TJN_SESSION_ID" VARCHAR2(1024)
   ) ;
--------------------------------------------------------
--  DDL for Table USER_ROLES
--------------------------------------------------------

  CREATE TABLE "USER_ROLES" 
   (	"USER_ROLE_ID" NUMBER(*,0), 
	"USERNAME" VARCHAR2(45), 
	"ROLE" VARCHAR2(45)
   ) ;
--------------------------------------------------------
--  DDL for Table USRAUTCREDENTIALS
--------------------------------------------------------

  CREATE TABLE "USRAUTCREDENTIALS" 
   (	"UAC_ID" NUMBER, 
	"USER_ID" VARCHAR2(20), 
	"APP_ID" NUMBER, 
	"APP_LOGIN_NAME" VARCHAR2(50), 
	"APP_PASSWORD" VARCHAR2(200), 
	"APP_USER_TYPE" VARCHAR2(30), 
	"APP_TXN_PWD" VARCHAR2(200), 
	"OAUTH_ACCESS_TOKEN" VARCHAR2(500), 
	"OAUTH_REFRESH_TOKEN" VARCHAR2(500), 
	"OAUTH_TOKENGEN_TIMESTAMP" TIMESTAMP (6), 
	"OAUTH_TOKEN_VALIDITY" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table WAIT_TIME_FIELD_MAP
--------------------------------------------------------

  CREATE TABLE "WAIT_TIME_FIELD_MAP" 
   (	"APP_ID" NUMBER, 
	"FUNCTION_CODE" VARCHAR2(20), 
	"PAGE_AREA" VARCHAR2(200), 
	"FIELD_LABEL" VARCHAR2(100), 
	"WAIT_TIME_SEC" NUMBER(2,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TJN_USER_SECURITY
--------------------------------------------------------

  CREATE TABLE "TJN_USER_SECURITY" 
   (	"USER_ID" VARCHAR2(20), 
	"SECURITY_QUESTION" VARCHAR2(100), 
	"ANSWER" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table PRJ_MAIL_PREFERENCE
--------------------------------------------------------

  CREATE TABLE "PRJ_MAIL_PREFERENCE" 
   (	"PRJ_ID" NUMBER NOT NULL, 
	"ACTION" VARCHAR2(25), 
	"PRJ_USERS" VARCHAR2(500)
   ) ;
   
--------------------------------------------------------
--  DDL for Table WRK_AUT_FUNC_FIELDS
--------------------------------------------------------

  CREATE TABLE "WRK_AUT_FUNC_FIELDS" 
   (	"FLD_APP" NUMBER, 
	"FLD_FUNC_CODE" VARCHAR2(50), 
	"FLD_SEQ_NO" NUMBER, 
	"FLD_UNAME" VARCHAR2(500), 
	"FLD_LABEL" VARCHAR2(500), 
	"FLD_TAB_ORDER" NUMBER, 
	"FLD_INSTANCE" NUMBER, 
	"FLD_INDEX" NUMBER, 
	"FLD_UID_TYPE" VARCHAR2(20), 
	"FLD_UID" VARCHAR2(300), 
	"FLD_TYPE" VARCHAR2(100), 
	"FLD_PAGE_AREA" VARCHAR2(100), 
	"TXNMODE" CHAR(1), 
	"VIEWMODE" VARCHAR2(10), 
	"MANDATORY" VARCHAR2(3), 
	"FLD_DEFAULT_OPTIONS" VARCHAR2(1000), 
	"FLD_IS_ENABLED" VARCHAR2(1), 
	"FLD_IS_FOOTER_ELEMENT" VARCHAR2(1), 
	"FLD_HAS_LOV" VARCHAR2(1), 
	"FLD_IS_MULTI_REC" VARCHAR2(20), 
	"FLD_GROUP" VARCHAR2(100), 
	"FLD_HAS_AUTOLOV" VARCHAR2(20), 
	"FLD_API_OPERATION" VARCHAR2(100), 
	"FLD_API_ENTITY_TYPE" VARCHAR2(10), 
	"FLD_API_RESP_TYPE" VARCHAR2(10)
   ) ;

   COMMENT ON COLUMN "WRK_AUT_FUNC_FIELDS"."FLD_API_ENTITY_TYPE" IS 'Request / Response';
   
   --------------------------------------------------------
--  DDL for Sequence SEQ_UI_VALIDATION
--------------------------------------------------------

   CREATE SEQUENCE  "SEQ_UI_VALIDATION"  MINVALUE 1 MAXVALUE 999999 INCREMENT BY 1 START WITH 682 CACHE 20 ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_DEF_COMMENT
--------------------------------------------------------

   CREATE SEQUENCE  "SQ_DEF_COMMENT"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_GLOBAL
--------------------------------------------------------

   CREATE SEQUENCE  "SQ_GLOBAL"  MINVALUE 100 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 26556 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_RUN_DEFECTS
--------------------------------------------------------

   CREATE SEQUENCE  "SQ_RUN_DEFECTS"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 8787 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_RUN_DEFECTS_LINKAGE
--------------------------------------------------------

   CREATE SEQUENCE  "SQ_RUN_DEFECTS_LINKAGE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 9605 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SQ_RUN_DEF_ATTACHMENTS
--------------------------------------------------------

   CREATE SEQUENCE  "SQ_RUN_DEF_ATTACHMENTS"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 19220 CACHE 20 NOORDER  NOCYCLE ;

--------------------------------------------------------
--  DDL for View V_RUNRESULTS_TS
--------------------------------------------------------

  CREATE OR REPLACE VIEW "V_RUNRESULTS_TS" ("RUN_ID", "TSTEP_REC_ID", "TC_REC_ID", "TSTEP_TOTAL_TXN", "TSTEP_TOTAL_EXEC", "TSTEP_TOTAL_PASS", "TSTEP_TOTAL_FAIL", "TSTEP_TOTAL_ERROR", "TSTEP_TOTAL_EXECUTING", "TSTEP_TOTAL_NOT_STARTED", "TSTEP_STATUS") AS 
  SELECT temp."RUN_ID",temp."TSTEP_REC_ID",temp."TC_REC_ID",temp."TSTEP_TOTAL_TXN",temp."TSTEP_TOTAL_EXEC",temp."TSTEP_TOTAL_PASS",temp."TSTEP_TOTAL_FAIL",temp."TSTEP_TOTAL_ERROR",temp."TSTEP_TOTAL_EXECUTING",temp."TSTEP_TOTAL_NOT_STARTED", 
case when temp.tstep_total_executing > 0 Then 'Executing'
when temp.tstep_total_Not_started > 0 Then 'Not Started'
when temp.tstep_total_error > 0 Then 'Error'
when temp.tstep_total_fail > 0 Then 'Fail'
when temp.tstep_total_pass > 0 Then 'Pass' 
else 'Error' 
End as TSTEP_STATUS
FROM (SELECT A.RUN_ID, A.TSTEP_REC_ID, B.TC_REC_ID,
COUNT(A.TSTEP_RESULT) AS TSTEP_TOTAL_TXN,
COUNT(CASE WHEN A.TSTEP_RESULT IN ('S','F','E') THEN 1 ELSE NULL END) AS TSTEP_TOTAL_EXEC,
COUNT(CASE WHEN A.TSTEP_RESULT='S' THEN 1 ELSE NULL END) AS TSTEP_TOTAL_PASS,
COUNT(CASE WHEN A.TSTEP_RESULT='F' THEN 1 ELSE NULL END) AS TSTEP_TOTAL_FAIL,
COUNT(CASE WHEN A.TSTEP_RESULT='E' THEN 1 ELSE NULL END) AS TSTEP_TOTAL_ERROR,
COUNT(CASE WHEN A.TSTEP_RESULT='X' THEN 1 ELSE NULL END) AS TSTEP_TOTAL_EXECUTING,
COUNT(CASE WHEN A.TSTEP_RESULT='N' THEN 1 ELSE NULL END) AS TSTEP_TOTAL_NOT_STARTED
FROM RUNRESULT_TS_TXN A, TESTSTEPS B WHERE B.TSTEP_REC_ID = A.TSTEP_REC_ID  group by A.TSTEP_REC_ID, A.RUN_ID, B.TC_REC_ID) temp WITH READ ONLY;

--------------------------------------------------------
--  DDL for View V_RUNRESULTS_TC
--------------------------------------------------------

  CREATE OR REPLACE VIEW "V_RUNRESULTS_TC" ("RUN_ID", "TC_REC_ID", "TC_TOTAL_STEPS", "TC_TOTAL_EXEC", "TC_TOTAL_PASS", "TC_TOTAL_FAIL", "TC_TOTAL_ERROR", "TC_TOTAL_EXECUTING", "TC_TOTAL_NOT_STARTED", "TC_STATUS") AS 
  SELECT temp."RUN_ID",temp."TC_REC_ID",temp."TC_TOTAL_STEPS",temp."TC_TOTAL_EXEC",temp."TC_TOTAL_PASS",temp."TC_TOTAL_FAIL",temp."TC_TOTAL_ERROR",temp."TC_TOTAL_EXECUTING",temp."TC_TOTAL_NOT_STARTED", 
case when temp.TC_TOTAL_EXECUTING > 0 Then 'Executing'
when temp.tc_total_Not_started > 0 Then 'Not Started'
when temp.tc_total_error > 0 Then 'Error'
when temp.tc_total_fail > 0 Then 'Fail'
when temp.tc_total_pass > 0 Then 'Pass' 
else 'Error' 
End as tc_sTATUS
FROM (SELECT RUN_ID, TC_REC_ID,
COUNT(TSTEP_STATUS) AS TC_TOTAL_STEPS,
COUNT(CASE WHEN TSTEP_STATUS IN ('Pass','Fail','Error') THEN 1 ELSE NULL END) AS TC_TOTAL_EXEC,
COUNT(CASE WHEN TSTEP_STATUS='Pass' THEN 1 ELSE NULL END) AS TC_TOTAL_PASS,
COUNT(CASE WHEN TSTEP_STATUS='Fail' THEN 1 ELSE NULL END) AS TC_TOTAL_FAIL,
COUNT(CASE WHEN TSTEP_STATUS='Error' THEN 1 ELSE NULL END) AS TC_TOTAL_ERROR,
COUNT(CASE WHEN TSTEP_STATUS='Executing' THEN 1 ELSE NULL END) AS TC_TOTAL_EXECUTING,
COUNT(CASE WHEN TSTEP_STATUS='Not Started' THEN 1 ELSE NULL END) AS TC_TOTAL_NOT_STARTED
FROM V_RUNRESULTS_TS group by RUN_ID, TC_REC_ID) temp WITH READ ONLY;

--------------------------------------------------------
--  DDL for View V_RUNRESULTS
--------------------------------------------------------

  CREATE OR REPLACE VIEW "V_RUNRESULTS" ("RUN_ID", "TOTAL_TCS", "TOTAL_EXE_TCS", "TOTAL_PASSED_TCS", "TOTAL_FAILED_TCS", "TOTAL_ERROR_TCS", "TOTAL_EXECUTING_TCS", "TOTAL_NOT_STARTED_TCS", "RUN_STATUS") AS 
  SELECT TEMP."RUN_ID",TEMP."TOTAL_TCS",TEMP."TOTAL_EXE_TCS",TEMP."TOTAL_PASSED_TCS",TEMP."TOTAL_FAILED_TCS",TEMP."TOTAL_ERROR_TCS",TEMP."TOTAL_EXECUTING_TCS",TEMP."TOTAL_NOT_STARTED_TCS",
CASE when temp.TOTAL_EXECUTING_TCS > 0 Then 'Executing'
when temp.TOTAL_NOT_STARTED_TCS > 0 Then 'Not Started'
when temp.TOTAL_ERROR_TCS > 0 Then 'Error'
when temp.TOTAL_FAILED_TCS > 0 Then 'Fail'
when temp.TOTAL_PASSED_TCS > 0 Then 'Pass' 
else 'Error' END AS RUN_STATUS FROM
(SELECT RUN_ID, 
COUNT(TC_STATUS) AS TOTAL_TCS,
COUNT(CASE WHEN TC_STATUS IN ('Pass','Fail','Error') THEN 1 ELSE NULL END) AS TOTAL_EXE_TCS,
COUNT(CASE WHEN TC_STATUS='Pass' THEN 1 ELSE NULL END) AS TOTAL_PASSED_TCS,
COUNT(CASE WHEN TC_STATUS='Fail' THEN 1 ELSE NULL END) AS TOTAL_FAILED_TCS,
COUNT(CASE WHEN TC_STATUS='Error' THEN 1 ELSE NULL END) AS TOTAL_ERROR_TCS,
COUNT(CASE WHEN TC_STATUS='Executing' THEN 1 ELSE NULL END) AS TOTAL_EXECUTING_TCS,
COUNT(CASE WHEN TC_STATUS='Not Started' THEN 1 ELSE NULL END) AS TOTAL_NOT_STARTED_TCS
FROM V_RUNRESULTS_TC GROUP BY RUN_ID) TEMP WITH READ ONLY;
--------------------------------------------------------
--  DDL for View V_RUNRESULT_TS_TXN
--------------------------------------------------------

  CREATE OR REPLACE VIEW "V_RUNRESULT_TS_TXN" ("RUN_ID", "TXN_NO", "TSTEP_REC_ID", "TSTEP_TDUID", "TSTEP_TOTAL_SCRNSHOT", "TSTEP_TOTAL_OUTPUT", "TSTEP_TOTAL_VALS", "TSTEP_TOTAL_PASS", "TSTEP_TOTAL_FAIL", "TSTEP_TOTAL_ERROR") AS 
  SELECT B.RUN_ID, B.TXN_NO, B.TSTEP_REC_ID, B.TSTEP_TDUID,
(select count(C.TDUID) from TJN_EXEC_SCRNSHOT C where C.RUN_ID = B.RUN_ID AND B.TSTEP_REC_ID=C.TSTEP_REC_ID) as TSTEP_TOTAL_SCRNSHOT,
(select count(C.TDUID) from TJN_EXEC_OUTPUT C where C.RUN_ID = B.RUN_ID AND B.TSTEP_REC_ID=C.TSTEP_REC_ID) as TSTEP_TOTAL_OUTPUT,
COUNT(A.RES_VAL_STATUS) AS TSTEP_TOTAL_VALS,
COUNT(CASE WHEN A.RES_VAL_STATUS='Pass' THEN 1 ELSE NULL END) AS TSTEP_TOTAL_PASS,
COUNT(CASE WHEN A.RES_VAL_STATUS='Fail' THEN 1 ELSE NULL END) AS TSTEP_TOTAL_FAIL,
COUNT(CASE WHEN A.RES_VAL_STATUS='Error' THEN 1 ELSE NULL END) AS TSTEP_TOTAL_ERROR
FROM 
RESVALRESULTS A, 
RUNRESULT_TS_TXN B
WHERE B.RUN_ID=A.RUN_ID(+) AND B.TSTEP_REC_ID=A.TSTEP_REC_ID(+) AND B.TXN_NO = A.ITERATION(+) 
GROUP BY B.RUN_ID, B.TXN_NO, B.TSTEP_REC_ID, B.TSTEP_TDUID order by B.RUN_ID,B.TSTEP_REC_ID asc;
--------------------------------------------------------
--  DDL for View V_TSTEP_EXEC_HISTORY_2
--------------------------------------------------------

  CREATE OR REPLACE VIEW "V_TSTEP_EXEC_HISTORY_2" ("TSTEP_REC_ID", "TSTEP_ID", "APP_ID", "APP_NAME", "FUNC_CODE", "TC_REC_ID", "TC_ID", "RUN_ID", "PRJ_ID", "RUN_START_TIME", "RUN_END_TIME", "RUN_USER", "TSTEP_STATUS", "TOTAL_VALS", "PASS_VALS", "FAIL_VALS", "ERROR_VALS") AS 
  SELECT TESTSTEPS.TSTEP_REC_ID, TESTSTEPS.TSTEP_ID, TESTSTEPS.APP_ID, MASAPPLICATION.APP_NAME, TESTSTEPS.FUNC_CODE, TESTSTEPS.TC_REC_ID, TESTCASES.TC_ID, V_RUNRESULTS_TS.RUN_ID, MASTESTRUNS.RUN_PRJ_ID PRJ_ID, MASTESTRUNS.RUN_START_TIME, MASTESTRUNS.RUN_END_TIME, MASTESTRUNS.RUN_USER, V_RUNRESULTS_TS.TSTEP_STATUS ,
COUNT(RESVALRESULTS.RES_VAL_STATUS) AS TOTAL_VALS, COUNT(CASE WHEN RESVALRESULTS.RES_VAL_STATUS='Pass' THEN 1 ELSE NULL END) as PASS_VALS,COUNT(CASE WHEN RESVALRESULTS.RES_VAL_STATUS='Fail' THEN 1 ELSE NULL END) as FAIL_VALS,COUNT(CASE WHEN RESVALRESULTS.RES_VAL_STATUS='Error' THEN 1 ELSE NULL END) as ERROR_VALS
FROM TESTSTEPS, MASAPPLICATION,V_RUNRESULTS_TS, MASTESTRUNS, TESTCASES, RESVALRESULTS
WHERE
MASAPPLICATION.APP_ID = TESTSTEPS.APP_ID AND
MASTESTRUNS.RUN_ID = V_RUNRESULTS_TS.RUN_ID AND 
TESTCASES.TC_REC_ID = TESTSTEPS.TC_REC_ID AND
RESVALRESULTS.RUN_ID(+)=V_RUNRESULTS_TS.RUN_ID AND
RESVALRESULTS.TSTEP_REC_ID(+) = V_RUNRESULTS_TS.TSTEP_rEC_ID AND
TESTSTEPS.TSTEP_REC_ID = V_RUNRESULTS_TS.TSTEP_REC_ID AND 
V_RUNRESULTS_TS.TSTEP_REC_ID IN (SELECT TSTEP_REC_ID FROM TESTSTEPS) group by TESTSTEPS.TSTEP_REC_ID, TESTSTEPS.TSTEP_ID, TESTSTEPS.APP_ID, MASAPPLICATION.APP_NAME, TESTSTEPS.FUNC_CODE, 
TESTSTEPS.TC_REC_ID, TESTCASES.TC_ID, V_RUNRESULTS_TS.RUN_ID, MASTESTRUNS.RUN_PRJ_ID, MASTESTRUNS.RUN_START_TIME, 
MASTESTRUNS.RUN_END_TIME, MASTESTRUNS.RUN_USER, V_RUNRESULTS_TS.TSTEP_STATUS
ORDER BY TESTSTEPS.TSTEP_REC_ID WITH READ ONLY;
--------------------------------------------------------
--  DDL for Index TESTCASES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TESTCASES_PK" ON "TESTCASES" ("TC_REC_ID", "TC_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_APP_FUNC_PA_NAME
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_APP_FUNC_PA_NAME" ON "AUT_TEMPLATE_RULES" ("APP_ID", "FUNC_CODE", "PA_NAME") 
  ;
--------------------------------------------------------
--  DDL for Index PK_TSTEP_RES_VAL_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_TSTEP_RES_VAL_ID" ON "RESVALMAP" ("TSTEP_REC_ID", "RES_VAL_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_APP_MODULE_MODE_PAFIELDS
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_APP_MODULE_MODE_PAFIELDS" ON "MAPDETAIL" ("MAP_ID", "MODE1_PAGEAREA", "MODE1_FIELD") 
  ;
--------------------------------------------------------
--  DDL for Index PK_APP_FUNC_CODE
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_APP_FUNC_CODE" ON "LRNR_FUNC_ACTIONS" ("APP_ID", "FUNC_CODE") 
  ;
--------------------------------------------------------
--  DDL for Index PK_APP_RVD_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_APP_RVD_ID" ON "MASRESVALDEF" ("RVD_APP_ID", "RVD_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_PRJ_USER_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_PRJ_USER_ID" ON "TJN_PRJ_USERS" ("PRJ_ID", "USER_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_USER_SESSION_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_USER_SESSION_ID" ON "USERSESSIONS" ("US_USER_ID", "US_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_APP_MODULE_MAP_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_APP_MODULE_MAP_ID" ON "MAPMASTER" ("APP_ID", "MODULE_CODE", "MAP_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_UTKN_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_UTKN_ID" ON "TJN_OAUTH_AUDIT_TRAIL" ("UTKN_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_USER_GROUP_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_USER_GROUP_ID" ON "USERROLES" ("USER_ID", "GROUP_ID") 
  ;
--------------------------------------------------------
--  DDL for Index TJN_RUN_DEF_ATTACHMENTS_PK
--------------------------------------------------------

  CREATE INDEX "TJN_RUN_DEF_ATTACHMENTS_PK" ON "TJN_RUN_DEF_ATTACHMENTS" ("REC_ATTACHMENT_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_TJN_EXEC_SCRNSHOT
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_TJN_EXEC_SCRNSHOT" ON "TJN_EXEC_SCRNSHOT" ("RUN_ID", "TSTEP_REC_ID", "TDUID", "SEQ_NO") 
  ;
--------------------------------------------------------
--  DDL for Index PK_MAS_GROUP_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_MAS_GROUP_ID" ON "MASUSERGROUPS" ("GROUP_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_RUN_RESULTS_TC
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_RUN_RESULTS_TC" ON "RUNRESULTS_TC" ("RUN_ID", "TC_REC_ID") 
  ;
--------------------------------------------------------
--  DDL for Index TJN_AUDIT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TJN_AUDIT_PK" ON "TJN_AUDIT" ("AUDIT_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_TEST_PRJ_TS_TC_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_TEST_PRJ_TS_TC_ID" ON "TESTSETMAP" ("TSM_PRJ_ID", "TSM_TS_REC_ID", "TSM_TC_REC_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_PRJ_APP_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_PRJ_APP_ID" ON "TJN_PRJ_AUTS" ("PRJ_ID", "APP_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_RUN_RESULTS_TS
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_RUN_RESULTS_TS" ON "RUNRESULTS_TS" ("RUN_ID", "TC_REC_ID", "TSTEP_REC_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ROLE_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ROLE_ID" ON "MASPROJECTROLES" ("ROLE_ID") 
  ;
--------------------------------------------------------
--  DDL for Index TJN_PARAMS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TJN_PARAMS_PK" ON "TJN_PARAMS" ("PARAM_NAME") 
  ;
--------------------------------------------------------
--  DDL for Index PK_DEF_COMMENT_COMM_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_DEF_COMMENT_COMM_ID" ON "TJN_DEF_COMMENT" ("REC_COMM_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_APP_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_APP_ID" ON "MASAPPLICATION" ("APP_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_AUT_GROUP_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_AUT_GROUP_ID" ON "TJN_AUT_GROUPS" ("APP_ID", "GROUP_NAME") 
  ;
--------------------------------------------------------
--  DDL for Index PK_TJN_EXEC_OUTPUT
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_TJN_EXEC_OUTPUT" ON "TJN_EXEC_OUTPUT" ("RUN_ID", "TDUID", "DETAIL_NO", "FIELD_NAME") 
  ;
--------------------------------------------------------
--  DDL for Index PK_DOMAIN_NAME
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_DOMAIN_NAME" ON "TJN_DOMAINS" ("DOMAIN_NAME") 
  ;
--------------------------------------------------------
--  DDL for Index PK_MAS_USER_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_MAS_USER_ID" ON "MASUSER" ("USER_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_RES_VAL_ID_SEQ
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_RES_VAL_ID_SEQ" ON "RESVALCONFIG" ("RES_VAL_ID", "RES_VAL_SEQ") 
  ;
--------------------------------------------------------
--  DDL for Index PK_APP_MODULE_CODE
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_APP_MODULE_CODE" ON "MASMODULE" ("APP_ID", "MODULE_CODE") 
  ;

--------------------------------------------------------
--  DDL for Index TJN_TM_TOOL_MASTER_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TJN_TM_TOOL_MASTER_PK" ON "TJN_TM_TOOL_MASTER" ("TM_REC_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_RUN_DEFECTS_REC_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_RUN_DEFECTS_REC_ID" ON "TJN_RUN_DEFECTS" ("DEF_REC_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_RUN_SCRIPT_ITERATION
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_RUN_SCRIPT_ITERATION" ON "ITERATIONLOG" ("RUN_ID", "SCRIPT_ID", "ITERATION_NO") 
  ;
--------------------------------------------------------
--  DDL for Index PK_PRJ_APP_FUNC_DATA_PATH
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_PRJ_APP_FUNC_DATA_PATH" ON "MASTESTDATAPATH" ("PRJ_ID", "APP_ID", "FUNC_CODE") 
  ;
--------------------------------------------------------
--  DDL for Index PK_USER_APP_USER_TYPE
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_USER_APP_USER_TYPE" ON "USRAUTCREDENTIALS" ("USER_ID", "APP_ID", "APP_USER_TYPE") 
  ;
--------------------------------------------------------
--  DDL for Index PK_RUN_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_RUN_ID" ON "MASTESTRUNS" ("RUN_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_TJN_PRJ_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_TJN_PRJ_ID" ON "TJN_PROJECTS" ("PRJ_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_TCU_USER_ID
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_TCU_USER_ID" ON "TJN_CURRENT_USERS" ("USER_ID") 
  ;
--------------------------------------------------------
--  Constraints for Table TJN_PARAMS
--------------------------------------------------------

  ALTER TABLE "TJN_PARAMS" MODIFY ("PARAM_NAME" NOT NULL ENABLE);
  ALTER TABLE "TJN_PARAMS" ADD CONSTRAINT "TJN_PARAMS_PK" PRIMARY KEY ("PARAM_NAME") ENABLE;
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "USERS" MODIFY ("USERNAME" NOT NULL ENABLE);
  ALTER TABLE "USERS" MODIFY ("PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "USERS" MODIFY ("ENABLED" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_APPLICATION_TYPE
--------------------------------------------------------

  ALTER TABLE "TJN_APPLICATION_TYPE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_APPLICATION_TYPE" MODIFY ("NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TESTSTEPS
--------------------------------------------------------

  ALTER TABLE "TESTSTEPS" MODIFY ("TSTEP_STORAGE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_PRJ_USER_SESSIONS
--------------------------------------------------------

  ALTER TABLE "TJN_PRJ_USER_SESSIONS" MODIFY ("PRJ_USER_SESSSION_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_PRJ_USER_SESSIONS" MODIFY ("PRJ_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_PRJ_USER_SESSIONS" MODIFY ("USER_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MASMODULE
--------------------------------------------------------

  ALTER TABLE "MASMODULE" ADD CONSTRAINT "PK_APP_MODULE_CODE" PRIMARY KEY ("APP_ID", "MODULE_CODE") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_ADAPTER
--------------------------------------------------------

  ALTER TABLE "TJN_ADAPTER" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "TJN_ADAPTER" MODIFY ("OR_TOOL" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_EXEC_SCRNSHOT
--------------------------------------------------------

  ALTER TABLE "TJN_EXEC_SCRNSHOT" ADD CONSTRAINT "PK_TJN_EXEC_SCRNSHOT" PRIMARY KEY ("RUN_ID", "TSTEP_REC_ID", "TDUID", "SEQ_NO") ENABLE;
--------------------------------------------------------
--  Constraints for Table RUNRESULTS_TS
--------------------------------------------------------

  ALTER TABLE "RUNRESULTS_TS" ADD CONSTRAINT "PK_RUN_RESULTS_TS" PRIMARY KEY ("RUN_ID", "TC_REC_ID", "TSTEP_REC_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table ITERATIONLOG
--------------------------------------------------------

  ALTER TABLE "ITERATIONLOG" ADD CONSTRAINT "PK_RUN_SCRIPT_ITERATION" PRIMARY KEY ("RUN_ID", "SCRIPT_ID", "ITERATION_NO") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_PROJECTS
--------------------------------------------------------

  ALTER TABLE "TJN_PROJECTS" ADD CONSTRAINT "PK_TJN_PRJ_ID" PRIMARY KEY ("PRJ_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_OAUTH_AUDIT_TRAIL
--------------------------------------------------------

  ALTER TABLE "TJN_OAUTH_AUDIT_TRAIL" ADD CONSTRAINT "PK_UTKN_ID" PRIMARY KEY ("UTKN_ID") ENABLE;
  ALTER TABLE "TJN_OAUTH_AUDIT_TRAIL" MODIFY ("UAC_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_OAUTH_AUDIT_TRAIL" MODIFY ("UPDATE_TIME" NOT NULL ENABLE);
  ALTER TABLE "TJN_OAUTH_AUDIT_TRAIL" MODIFY ("GENERATE_TIME" NOT NULL ENABLE);
  ALTER TABLE "TJN_OAUTH_AUDIT_TRAIL" MODIFY ("APP_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_OAUTH_AUDIT_TRAIL" MODIFY ("TKN_GENERATED_BY" NOT NULL ENABLE);
  ALTER TABLE "TJN_OAUTH_AUDIT_TRAIL" MODIFY ("ACCESS_TOKEN" NOT NULL ENABLE);
  ALTER TABLE "TJN_OAUTH_AUDIT_TRAIL" MODIFY ("UTKN_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_AUDIT
--------------------------------------------------------

  ALTER TABLE "TJN_AUDIT" ADD CONSTRAINT "TJN_AUDIT_PK" PRIMARY KEY ("AUDIT_ID") ENABLE;
  ALTER TABLE "TJN_AUDIT" MODIFY ("ENTITY_TYPE" NOT NULL ENABLE);
  ALTER TABLE "TJN_AUDIT" MODIFY ("ENTITY_RECORD_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_AUDIT" MODIFY ("AUDIT_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MANUAL_FIELD_MAP
--------------------------------------------------------

  ALTER TABLE "MANUAL_FIELD_MAP" MODIFY ("APP_ID" NOT NULL ENABLE);
  ALTER TABLE "MANUAL_FIELD_MAP" MODIFY ("FUNCTION_NAME" NOT NULL ENABLE);
  ALTER TABLE "MANUAL_FIELD_MAP" MODIFY ("PAGE_AREA" NOT NULL ENABLE);
  ALTER TABLE "MANUAL_FIELD_MAP" MODIFY ("MANUAL_FIELD_LABEL" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TESTSETMAP
--------------------------------------------------------

  ALTER TABLE "TESTSETMAP" ADD CONSTRAINT "PK_TEST_PRJ_TS_TC_ID" PRIMARY KEY ("TSM_PRJ_ID", "TSM_TS_REC_ID", "TSM_TC_REC_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table REGCLIENTS_DEVICES
--------------------------------------------------------

  ALTER TABLE "REGCLIENTS_DEVICES" MODIFY ("REG_DEV_ID" NOT NULL ENABLE);
  ALTER TABLE "REGCLIENTS_DEVICES" MODIFY ("REG_DEV_CLIENT" NOT NULL ENABLE);
  ALTER TABLE "REGCLIENTS_DEVICES" MODIFY ("REG_DEV_DEVICE_REC_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_TM_TOOL_MASTER
--------------------------------------------------------

  ALTER TABLE "TJN_TM_TOOL_MASTER" MODIFY ("TM_REC_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_TM_TOOL_MASTER" ADD CONSTRAINT "TJN_TM_TOOL_MASTER_PK" PRIMARY KEY ("TM_REC_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_EXEC_OUTPUT
--------------------------------------------------------

  ALTER TABLE "TJN_EXEC_OUTPUT" MODIFY ("RUN_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_OUTPUT" MODIFY ("TDGID" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_OUTPUT" MODIFY ("TDUID" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_OUTPUT" MODIFY ("TSTEP_REC_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_OUTPUT" MODIFY ("ITERATION_NO" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_OUTPUT" ADD CONSTRAINT "PK_TJN_EXEC_OUTPUT" PRIMARY KEY ("RUN_ID", "TDUID", "DETAIL_NO", "FIELD_NAME") ENABLE;
--------------------------------------------------------
--  Constraints for Table WAIT_TIME_FIELD_MAP
--------------------------------------------------------

  ALTER TABLE "WAIT_TIME_FIELD_MAP" MODIFY ("APP_ID" NOT NULL ENABLE);
  ALTER TABLE "WAIT_TIME_FIELD_MAP" MODIFY ("FUNCTION_CODE" NOT NULL ENABLE);
  ALTER TABLE "WAIT_TIME_FIELD_MAP" MODIFY ("PAGE_AREA" NOT NULL ENABLE);
  ALTER TABLE "WAIT_TIME_FIELD_MAP" MODIFY ("FIELD_LABEL" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_MAIL_LOGS
--------------------------------------------------------

  ALTER TABLE "TJN_MAIL_LOGS" MODIFY ("RUN_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USER_ROLES
--------------------------------------------------------

  ALTER TABLE "USER_ROLES" MODIFY ("USER_ROLE_ID" NOT NULL ENABLE);
  ALTER TABLE "USER_ROLES" MODIFY ("USERNAME" NOT NULL ENABLE);
  ALTER TABLE "USER_ROLES" MODIFY ("ROLE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_PRJ_AUTS
--------------------------------------------------------

  ALTER TABLE "TJN_PRJ_AUTS" ADD CONSTRAINT "PK_PRJ_APP_ID" PRIMARY KEY ("PRJ_ID", "APP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table MASAPPLICATION
--------------------------------------------------------

  ALTER TABLE "MASAPPLICATION" ADD CONSTRAINT "PK_APP_ID" PRIMARY KEY ("APP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table USRAUTCREDENTIALS
--------------------------------------------------------

  ALTER TABLE "USRAUTCREDENTIALS" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "USRAUTCREDENTIALS" MODIFY ("APP_ID" NOT NULL ENABLE);
  ALTER TABLE "USRAUTCREDENTIALS" MODIFY ("APP_USER_TYPE" NOT NULL ENABLE);
  ALTER TABLE "USRAUTCREDENTIALS" MODIFY ("UAC_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table BUILDRUN_DETAILS
--------------------------------------------------------

  ALTER TABLE "BUILDRUN_DETAILS" MODIFY ("BUILD_NUMBER" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_EXEC_SETTINGS
--------------------------------------------------------

  ALTER TABLE "TJN_EXEC_SETTINGS" MODIFY ("RUN_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table LRNR_FUNC_ACTIONS
--------------------------------------------------------

  ALTER TABLE "LRNR_FUNC_ACTIONS" ADD CONSTRAINT "PK_APP_FUNC_CODE" PRIMARY KEY ("APP_ID", "FUNC_CODE") ENABLE;
--------------------------------------------------------
--  Constraints for Table RESVALMAP
--------------------------------------------------------

  ALTER TABLE "RESVALMAP" ADD CONSTRAINT "PK_TSTEP_RES_VAL_ID" PRIMARY KEY ("TSTEP_REC_ID", "RES_VAL_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table PCLOUDYDEVICES
--------------------------------------------------------

  ALTER TABLE "PCLOUDYDEVICES" MODIFY ("DEVICE_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_PRJ_USERS
--------------------------------------------------------

  ALTER TABLE "TJN_PRJ_USERS" ADD CONSTRAINT "PK_PRJ_USER_ID" PRIMARY KEY ("PRJ_ID", "USER_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_DTT_PROJECTS
--------------------------------------------------------

  ALTER TABLE "TJN_DTT_PROJECTS" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_DTT_PROJECTS" MODIFY ("INSTANCE_NAME" NOT NULL ENABLE);
  ALTER TABLE "TJN_DTT_PROJECTS" MODIFY ("PROJECTS" NOT NULL ENABLE);
  ALTER TABLE "TJN_DTT_PROJECTS" MODIFY ("INSTANCE" NOT NULL ENABLE);
  ALTER TABLE "TJN_DTT_PROJECTS" MODIFY ("PROJECT_SET_NAME" NOT NULL ENABLE);
  ALTER TABLE "TJN_DTT_PROJECTS" MODIFY ("PRJ_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_TM_USER_MAPPING
--------------------------------------------------------

  ALTER TABLE "TJN_TM_USER_MAPPING" MODIFY ("TJN_USER_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_TM_USER_MAPPING" MODIFY ("TJN_INSTANCE_NAME" NOT NULL ENABLE);
  ALTER TABLE "TJN_TM_USER_MAPPING" MODIFY ("USERNAME" NOT NULL ENABLE);
  ALTER TABLE "TJN_TM_USER_MAPPING" MODIFY ("PASSWORD" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MASUSER
--------------------------------------------------------

  ALTER TABLE "MASUSER" MODIFY ("USER_ENABLED" NOT NULL ENABLE);
  ALTER TABLE "MASUSER" ADD CONSTRAINT "PK_MAS_USER_ID" PRIMARY KEY ("USER_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_SCH_RECURRENCE
--------------------------------------------------------

  ALTER TABLE "TJN_SCH_RECURRENCE" MODIFY ("SCH_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MASMODULE_WS
--------------------------------------------------------

  ALTER TABLE "MASMODULE_WS" MODIFY ("APP_ID" NOT NULL ENABLE);
  ALTER TABLE "MASMODULE_WS" MODIFY ("WS_NAME" NOT NULL ENABLE);
  ALTER TABLE "MASMODULE_WS" MODIFY ("WS_OPERATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table FAILED_LOGIN_ATTEMPTS
--------------------------------------------------------

  ALTER TABLE "FAILED_LOGIN_ATTEMPTS" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "FAILED_LOGIN_ATTEMPTS" MODIFY ("TERMINAL" NOT NULL ENABLE);
  ALTER TABLE "FAILED_LOGIN_ATTEMPTS" MODIFY ("TIME_STAMP" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USERROLES
--------------------------------------------------------

  ALTER TABLE "USERROLES" ADD CONSTRAINT "PK_USER_GROUP_ID" PRIMARY KEY ("USER_ID", "GROUP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table AUT_FUNC_PAGEAREAS
--------------------------------------------------------

  ALTER TABLE "AUT_FUNC_PAGEAREAS" MODIFY ("PA_APP" NOT NULL ENABLE);
  ALTER TABLE "AUT_FUNC_PAGEAREAS" MODIFY ("PA_FUNC_CODE" NOT NULL ENABLE);
  ALTER TABLE "AUT_FUNC_PAGEAREAS" MODIFY ("PA_NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_EXEC_SCHEME
--------------------------------------------------------

  ALTER TABLE "TJN_EXEC_SCHEME" MODIFY ("SCH_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_SCHEME" MODIFY ("SCH_NAME" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_SCHEME" MODIFY ("APP_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_SCHEME" MODIFY ("FUNC_CODE" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_SCHEME" MODIFY ("BASE_PAGE_AREA" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_SCHEME" MODIFY ("SCH_ENABLED" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_SCHEME" MODIFY ("CREATE_TIMESTAMP" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_SCHEME" MODIFY ("CREATED_BY" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MASUSERGROUPS
--------------------------------------------------------

  ALTER TABLE "MASUSERGROUPS" ADD CONSTRAINT "PK_MAS_GROUP_ID" PRIMARY KEY ("GROUP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table MAPMASTER
--------------------------------------------------------

  ALTER TABLE "MAPMASTER" ADD CONSTRAINT "PK_APP_MODULE_MAP_ID" PRIMARY KEY ("APP_ID", "MODULE_CODE", "MAP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_DOMAINS
--------------------------------------------------------

  ALTER TABLE "TJN_DOMAINS" ADD CONSTRAINT "PK_DOMAIN_NAME" PRIMARY KEY ("DOMAIN_NAME") ENABLE;
--------------------------------------------------------
--  Constraints for Table MASTESTDATAPATH
--------------------------------------------------------

  ALTER TABLE "MASTESTDATAPATH" ADD CONSTRAINT "PK_PRJ_APP_FUNC_DATA_PATH" PRIMARY KEY ("PRJ_ID", "APP_ID", "FUNC_CODE") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_SCHEDULER
--------------------------------------------------------

  ALTER TABLE "TJN_SCHEDULER" MODIFY ("RUN_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_AUT_APIS
--------------------------------------------------------

  ALTER TABLE "TJN_AUT_APIS" MODIFY ("APP_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_AUT_APIS" MODIFY ("API_CODE" NOT NULL ENABLE);
  ALTER TABLE "TJN_AUT_APIS" MODIFY ("API_NAME" NOT NULL ENABLE);
  ALTER TABLE "TJN_AUT_APIS" MODIFY ("API_TYPE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MASDEVICES
--------------------------------------------------------

  ALTER TABLE "MASDEVICES" MODIFY ("DEVICE_REC_ID" NOT NULL ENABLE);
  ALTER TABLE "MASDEVICES" MODIFY ("DEVICE_ID" NOT NULL ENABLE);
  ALTER TABLE "MASDEVICES" MODIFY ("DEVICE_PLATFORM" NOT NULL ENABLE);
  ALTER TABLE "MASDEVICES" MODIFY ("DEVICE_PLATFORM_VERSION" NOT NULL ENABLE);
  ALTER TABLE "MASDEVICES" MODIFY ("DEVICE_NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table AUT_FUNC_PAGEAREAS_HPORDM
--------------------------------------------------------

  ALTER TABLE "AUT_FUNC_PAGEAREAS_HPORDM" MODIFY ("PA_APP" NOT NULL ENABLE);
  ALTER TABLE "AUT_FUNC_PAGEAREAS_HPORDM" MODIFY ("PA_FUNC_CODE" NOT NULL ENABLE);
  ALTER TABLE "AUT_FUNC_PAGEAREAS_HPORDM" MODIFY ("PA_NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USERSESSIONS
--------------------------------------------------------

  ALTER TABLE "USERSESSIONS" ADD CONSTRAINT "PK_USER_SESSION_ID" PRIMARY KEY ("US_USER_ID", "US_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_ASSISTED_LEARNING
--------------------------------------------------------

  ALTER TABLE "TJN_ASSISTED_LEARNING" MODIFY ("ASST_LRN_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_USER_PREFERENCE
--------------------------------------------------------

  ALTER TABLE "TJN_USER_PREFERENCE" MODIFY ("USER_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_AUT_API_OPERATIONS
--------------------------------------------------------

  ALTER TABLE "TJN_AUT_API_OPERATIONS" MODIFY ("APP_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table EXTR_AUDIT_TRAIL
--------------------------------------------------------

  ALTER TABLE "EXTR_AUDIT_TRAIL" MODIFY ("EXTR_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_ADAPTER_OR_TOOL
--------------------------------------------------------

  ALTER TABLE "TJN_ADAPTER_OR_TOOL" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_ADAPTER_OR_TOOL" MODIFY ("NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_RUN_DEF_ATTACHMENTS
--------------------------------------------------------

  ALTER TABLE "TJN_RUN_DEF_ATTACHMENTS" MODIFY ("DEF_REC_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_RUN_DEF_ATTACHMENTS" MODIFY ("REC_ATTACHMENT_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_RUN_DEF_ATTACHMENTS" MODIFY ("FILE_PATH" NOT NULL ENABLE);
  ALTER TABLE "TJN_RUN_DEF_ATTACHMENTS" MODIFY ("ATT_POSTED" NOT NULL ENABLE);
  ALTER TABLE "TJN_RUN_DEF_ATTACHMENTS" ADD CONSTRAINT "TJN_RUN_DEF_ATTACHMENTS_PK" PRIMARY KEY ("REC_ATTACHMENT_ID") DEFERRABLE ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_DEF_USER_MAPING
--------------------------------------------------------

  ALTER TABLE "TJN_DEF_USER_MAPING" MODIFY ("TJN_USERID" NOT NULL ENABLE);
  ALTER TABLE "TJN_DEF_USER_MAPING" MODIFY ("TJN_INSTANCE_NAME" NOT NULL ENABLE);
  ALTER TABLE "TJN_DEF_USER_MAPING" MODIFY ("DEF_TOOL_USERID" NOT NULL ENABLE);
  ALTER TABLE "TJN_DEF_USER_MAPING" MODIFY ("DEF_TOOL_PWD" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MASPROJECTROLES
--------------------------------------------------------

  ALTER TABLE "MASPROJECTROLES" ADD CONSTRAINT "PK_ROLE_ID" PRIMARY KEY ("ROLE_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_RUN_DEFECTS
--------------------------------------------------------

  ALTER TABLE "TJN_RUN_DEFECTS" MODIFY ("DEF_SUMMARY" NOT NULL ENABLE);
  ALTER TABLE "TJN_RUN_DEFECTS" MODIFY ("DEF_CREATED_BY" NOT NULL ENABLE);
  ALTER TABLE "TJN_RUN_DEFECTS" MODIFY ("DEF_CREATED_ON" NOT NULL ENABLE);
  ALTER TABLE "TJN_RUN_DEFECTS" MODIFY ("DEF_SEVERITY" NOT NULL ENABLE);
  ALTER TABLE "TJN_RUN_DEFECTS" MODIFY ("DEF_STATUS" NOT NULL ENABLE);
  ALTER TABLE "TJN_RUN_DEFECTS" MODIFY ("RUN_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_RUN_DEFECTS" MODIFY ("DEFECT_TYPE" NOT NULL ENABLE);
  ALTER TABLE "TJN_RUN_DEFECTS" MODIFY ("REC_STATUS" NOT NULL ENABLE);
  ALTER TABLE "TJN_RUN_DEFECTS" ADD CONSTRAINT "PK_RUN_DEFECTS_REC_ID" PRIMARY KEY ("DEF_REC_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table RESVALCONFIG
--------------------------------------------------------

  ALTER TABLE "RESVALCONFIG" ADD CONSTRAINT "PK_RES_VAL_ID_SEQ" PRIMARY KEY ("RES_VAL_ID", "RES_VAL_SEQ") ENABLE;
--------------------------------------------------------
--  Constraints for Table AUT_TEMPLATE_RULES
--------------------------------------------------------

  ALTER TABLE "AUT_TEMPLATE_RULES" MODIFY ("APP_ID" NOT NULL ENABLE);
  ALTER TABLE "AUT_TEMPLATE_RULES" MODIFY ("FUNC_CODE" NOT NULL ENABLE);
  ALTER TABLE "AUT_TEMPLATE_RULES" MODIFY ("PA_NAME" NOT NULL ENABLE);
  ALTER TABLE "AUT_TEMPLATE_RULES" MODIFY ("TEMPLATE_RULE" NOT NULL ENABLE);
  ALTER TABLE "AUT_TEMPLATE_RULES" ADD CONSTRAINT "PK_APP_FUNC_PA_NAME" PRIMARY KEY ("APP_ID", "FUNC_CODE", "PA_NAME") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_CURRENT_USERS
--------------------------------------------------------

  ALTER TABLE "TJN_CURRENT_USERS" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_CURRENT_USERS" MODIFY ("TERMINAL" NOT NULL ENABLE);
  ALTER TABLE "TJN_CURRENT_USERS" MODIFY ("LOGIN_TIME" NOT NULL ENABLE);
  ALTER TABLE "TJN_CURRENT_USERS" ADD CONSTRAINT "PK_TCU_USER_ID" PRIMARY KEY ("USER_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_DEF_COMMENT
--------------------------------------------------------

  ALTER TABLE "TJN_DEF_COMMENT" MODIFY ("TEXT" NOT NULL ENABLE);
  ALTER TABLE "TJN_DEF_COMMENT" MODIFY ("DEF_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_DEF_COMMENT" MODIFY ("COMM_POST" NOT NULL ENABLE);
  ALTER TABLE "TJN_DEF_COMMENT" ADD CONSTRAINT "PK_DEF_COMMENT_COMM_ID" PRIMARY KEY ("REC_COMM_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_AUT_GROUPS
--------------------------------------------------------

  ALTER TABLE "TJN_AUT_GROUPS" ADD CONSTRAINT "PK_AUT_GROUP_ID" PRIMARY KEY ("APP_ID", "GROUP_NAME") ENABLE;
--------------------------------------------------------
--  Constraints for Table MASTESTRUNS
--------------------------------------------------------

  ALTER TABLE "MASTESTRUNS" MODIFY ("RUN_ID" NOT NULL ENABLE);
  ALTER TABLE "MASTESTRUNS" MODIFY ("RUN_EXEC_SPEED" NOT NULL ENABLE);
  ALTER TABLE "MASTESTRUNS" ADD CONSTRAINT "PK_RUN_ID" PRIMARY KEY ("RUN_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table TESTCASES
--------------------------------------------------------

  ALTER TABLE "TESTCASES" ADD CONSTRAINT "TESTCASES_PK" PRIMARY KEY ("TC_REC_ID", "TC_ID") ENABLE;
  ALTER TABLE "TESTCASES" MODIFY ("TC_ID" NOT NULL ENABLE);
  ALTER TABLE "TESTCASES" MODIFY ("TC_REC_ID" NOT NULL ENABLE);
  ALTER TABLE "TESTCASES" MODIFY ("TC_STORAGE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TJN_MAIL_ESCALATION_RULES
--------------------------------------------------------

  ALTER TABLE "TJN_MAIL_ESCALATION_RULES" MODIFY ("TJN_MAIL_ESCALATION_RULE_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_MAIL_ESCALATION_RULES" MODIFY ("TJN_MAIL_ESCALATION_RULE_NAME" NOT NULL ENABLE);
  ALTER TABLE "TJN_MAIL_ESCALATION_RULES" MODIFY ("TJN_MAIL_ESCALATION_RULE_DESC" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MAPDETAIL
--------------------------------------------------------

  ALTER TABLE "MAPDETAIL" ADD CONSTRAINT "PK_APP_MODULE_MODE_PAFIELDS" PRIMARY KEY ("MAP_ID", "MODE1_PAGEAREA", "MODE1_FIELD") ENABLE;
--------------------------------------------------------
--  Constraints for Table TJN_EXEC_SCHEME_CONFIG
--------------------------------------------------------

  ALTER TABLE "TJN_EXEC_SCHEME_CONFIG" MODIFY ("SCH_ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_SCHEME_CONFIG" MODIFY ("ACT_BLOCK" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_SCHEME_CONFIG" MODIFY ("PAGE_AREA" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_SCHEME_CONFIG" MODIFY ("FIELD" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_SCHEME_CONFIG" MODIFY ("ACTION" NOT NULL ENABLE);
  ALTER TABLE "TJN_EXEC_SCHEME_CONFIG" MODIFY ("SEQ_NO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MASRESVALDEF
--------------------------------------------------------

  ALTER TABLE "MASRESVALDEF" ADD CONSTRAINT "PK_APP_RVD_ID" PRIMARY KEY ("RVD_APP_ID", "RVD_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table TSTEP_UI_VALIDATIONS
--------------------------------------------------------

  ALTER TABLE "TSTEP_UI_VALIDATIONS" MODIFY ("REC_ID" NOT NULL ENABLE);
  ALTER TABLE "TSTEP_UI_VALIDATIONS" MODIFY ("TSTEP_REC_ID" NOT NULL ENABLE);
  ALTER TABLE "TSTEP_UI_VALIDATIONS" MODIFY ("UI_VAL_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table RUNRESULTS_TC
--------------------------------------------------------

  ALTER TABLE "RUNRESULTS_TC" ADD CONSTRAINT "PK_RUN_RESULTS_TC" PRIMARY KEY ("RUN_ID", "TC_REC_ID") ENABLE;

--------------------------------------------------------
--  DDL for Sequence PRODUCTLICENSE
--------------------------------------------------------
 CREATE TABLE "PRODUCTLICENSE" 
   (    "LICENSE_KEY" CLOB, 
    "RENEWAL_LICENSE_KEY" CLOB
   ) ;
--------------------------------------------------------
--  Constraints for Table PRODUCTLICENSE
--------------------------------------------------------

  ALTER TABLE "PRODUCTLICENSE" MODIFY ("LICENSE_KEY" NOT NULL ENABLE);
  
  --------------------------------------------------------
--  DDL for Table TJN_AUDIT
--------------------------------------------------------

ALTER TABLE TJN_AUDIT ADD OLD_DATA CLOB;
ALTER TABLE TJN_AUDIT ADD NEW_DATA CLOB;

--------------------------------------------------------
--  DDL for Table TJN_PRJ_MAIL_SETTINGS
--------------------------------------------------------
ALTER TABLE TJN_PRJ_MAIL_SETTINGS ADD TJN_ADDITIONAL_MAILS VARCHAR2(2048 BYTE);
ALTER TABLE TJN_PRJ_MAIL_SETTINGS ADD PRJ_MAIL_TYPE VARCHAR2(256 BYTE);

--------------------------------------------------------
--  DDL for Table UNSTRUCTURED_DATA_VALIDATION
--------------------------------------------------------
CREATE TABLE "UNSTRUCTURED_DATA_VALIDATION"
   (    "RECORD_ID" NUMBER,
    "TESTSTEP_ID" NUMBER,
    "VALIDATION_DATA" VARCHAR2(1000)
   ) ;
   
   
   
 --------------------------------------------------------
--  DDL for Table TJN_PRJ_TESTDATA
--------------------------------------------------------
   
   CREATE TABLE "TJN_PRJ_TESTDATA"
   (    "PRJ_ID" NUMBER,
   "APP_ID" NUMBER,
    "FUNC_CODE" VARCHAR2(255),
    "FILE_NAME" VARCHAR2(255),
    "UPLOADED_BY" VARCHAR2(255),
  "UPLOADED_ON" Date
   ) ;
   
--------------------------------------------------------
--  Tenjin 212 alters
--------------------------------------------------------   
ALTER TABLE TJN_PROJECTS ADD TEST_DATA_REPO_TYPE VARCHAR(255);
ALTER TABLE TJN_PROJECTS ADD REPO_ROOT VARCHAR(255);
ALTER TABLE RUNRESULT_TS_TXN ADD API_REQ_LOG VARCHAR2(4000 BYTE);
ALTER TABLE TJN_USER_SECURITY ADD RESET_COUNT VARCHAR2(20 BYTE);
ALTER TABLE TJN_RUN_DEFECTS MODIFY DEF_SEVERITY VARCHAR2(30 BYTE);
ALTER TABLE TJN_RUN_DEFECTS MODIFY DEF_PRIORITY VARCHAR2(30 BYTE);


--------------------------------------------------------
    Tenjin 212  VAPT Password alters
--------------------------------------------------------  

ALTER TABLE TJN_USER_SECURITY ADD RESET_COUNT VARCHAR2(20 BYTE);
ALTER TABLE USRAUTCREDENTIALS  MODIFY  APP_PASSWORD  VARCHAR2(1024 BYTE);
ALTER TABLE USRAUTCREDENTIALS  MODIFY  APP_TXN_PWD   VARCHAR2(1024 BYTE);
ALTER TABLE MASAPPLICATION     MODIFY  TEMPLATE_PASSWORD VARCHAR2(1024  BYTE);
ALTER TABLE TJN_DEF_TOOL_MASTER MODIFY  ADMIN_PWD      VARCHAR2(1024  BYTE);
ALTER TABLE TJN_DEF_USER_MAPING MODIFY  DEF_TOOL_PWD    VARCHAR2(1024  BYTE);
ALTER TABLE MASREGCLIENTS     MODIFY  RC_DEVICE_FARM_PASSWORD VARCHAR2(1024  BYTE);
ALTER TABLE MASREGCLIENTS     MODIFY  RC_DEVICE_FARM_KEY   VARCHAR2(1024  BYTE);
ALTER TABLE TJN_PARAMS         MODIFY  PARAM_VALUE      VARCHAR2(1024  BYTE);
ALTER TABLE TJN_TM_USER_MAPPING MODIFY  PASSWORD     VARCHAR2(1024  BYTE);
   
 ----------------------------------------------------------------------
-- ISO message validation alters
-----------------------------------------------------------------------

   
   --------------------------------------------------------
--  DDL for Table TJN_AUT_MESSAGES
--------------------------------------------------------

  CREATE TABLE "TJN_AUT_MESSAGES" 
   (	"ID" NUMBER, 
	"TYPE" VARCHAR2(20 BYTE), 
	"MESSAGES" VARCHAR2(50 BYTE), 
	"CONTENTEN" CLOB, 
	"FILENAME" VARCHAR2(30 BYTE), 
	"APP_ID" NUMBER(38,0), 
	"SUBTYPE" VARCHAR2(20 BYTE)
   ) ;
--------------------------------------------------------
--  Constraints for Table TJN_AUT_MESSAGES
--------------------------------------------------------

  ALTER TABLE "TJN_AUT_MESSAGES" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "TJN_AUT_MESSAGES" MODIFY ("TYPE" NOT NULL ENABLE);
  ALTER TABLE "TJN_AUT_MESSAGES" MODIFY ("APP_ID" NOT NULL ENABLE);


--------------------------------------------------------
--  File created - Tuesday-May-18-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TJN_PYMNTMSG_MASTER
--------------------------------------------------------

  CREATE TABLE "TJN_PYMNTMSG_MASTER" 
   (	"FORMAT" VARCHAR2(20 BYTE), 
	"MESSAGE_TYPE" VARCHAR2(30 BYTE), 
	"TAG_NAME" VARCHAR2(30 BYTE), 
	"FIELD_LABEL" VARCHAR2(50 BYTE)
   ) ;
   
   --------------------------------------------------------
--  Constraints for Table TJN_PYMNTMSG_MASTER
--------------------------------------------------------

  ALTER TABLE "TJN_PYMNTMSG_MASTER" MODIFY ("FORMAT" NOT NULL ENABLE);
  ALTER TABLE "TJN_PYMNTMSG_MASTER" MODIFY ("MESSAGE_TYPE" NOT NULL ENABLE);
  ALTER TABLE "TJN_PYMNTMSG_MASTER" MODIFY ("TAG_NAME" NOT NULL ENABLE);
  ALTER TABLE "TJN_PYMNTMSG_MASTER" MODIFY ("FIELD_LABEL" NOT NULL ENABLE);


--------------------------------------------------------
ALTER TABLE TESTSTEPS ADD TSTEP_FILENAME VARCHAR(40);
ALTER TABLE TESTSTEPS ADD TSTEP_FILEPATH VARCHAR(60);
ALTER TABLE TJN_RUN_DEFECT_LINKAGE ADD REPO_OWNER VARCHAR(255);
     
CREATE TABLE "TJN_AWSS3_DETAIL"
( "ACCESS_KEY_ID" CLOB NOT NULL,
"ACCESS_SEC_KEY" CLOB NOT NULL 
) ;


ALTER TABLE TJN_AUT_APIS ADD ENCRYPTION_TYPE VARCHAR2(20 BYTE);