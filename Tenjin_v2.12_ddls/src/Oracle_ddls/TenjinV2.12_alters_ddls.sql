--------------------------------------------------------
--  DDL for Table TJN_EXEC_SETTINGS
--------------------------------------------------------

ALTER TABLE [TJN_EXEC_SETTINGS]
MODIFY [BROWSER] varchar(30);

--------------------------------------------------------
--  DDL for Table USRAUTCREDENTIALS
--------------------------------------------------------

ALTER TABLE [USRAUTCREDENTIALS]
MODIFY [APP_USER_TYPE] varchar(30);

--------------------------------------------------------
--  DDL for Table TJN_AUDIT
--------------------------------------------------------

ALTER TABLE TJN_AUDIT ADD OLD_DATA CLOB;
ALTER TABLE TJN_AUDIT ADD NEW_DATA CLOB;

--------------------------------------------------------
--  DDL for Table LRNR_AUDIT_TRAIL
--------------------------------------------------------

ALTER TABLE LRNR_AUDIT_TRAIL ADD FIELDS_LEARNT_COUNT NUMBER; 

--------------------------------------------------------
--  DDL for Table LRNR_AUDIT_TRAIL_API
--------------------------------------------------------

ALTER TABLE LRNR_AUDIT_TRAIL_API ADD FIELDS_LEARNT_COUNT NUMBER;

--------------------------------------------------------
--  DDL for Table TJN_PRJ_MAIL_SETTINGS
--------------------------------------------------------
ALTER TABLE TJN_PRJ_MAIL_SETTINGS ADD TJN_ADDITIONAL_MAILS VARCHAR2(2048 BYTE);
ALTER TABLE TJN_PRJ_MAIL_SETTINGS ADD PRJ_MAIL_TYPE VARCHAR2(256 BYTE);

--------------------------------------------------------
--  DDL for Table TESTSTEPS
--------------------------------------------------------
ALTER TABLE TESTSTEPS MODIFY FUNC_CODE VARCHAR2(50 BYTE);

--------------------------------------------------------
--  DDL for Table MASTESTRUNS
--------------------------------------------------------

ALTER TABLE MASTESTRUNS ADD RUN_JSON CLOB;

--------------------------------------------------------
--  DDL for Table TESTCASES
--------------------------------------------------------
ALTER TABLE TESTCASES ADD TC_TAG VARCHAR2(100);


--------------------------------------------------------
--  DDL for Table UNSTRUCTURED_DATA_VALIDATION
--------------------------------------------------------
CREATE TABLE "UNSTRUCTURED_DATA_VALIDATION"
   (    "RECORD_ID" NUMBER,
    "TESTSTEP_ID" NUMBER,
    "VALIDATION_DATA" VARCHAR2(1000)
   ) ;
   
   
--------------------------------------------------------
--  DDL for Table  TJN_PROJECTS
-------------------------------------------------------- 
   
ALTER TABLE TJN_PROJECTS ADD TEST_DATA_REPO_TYPE VARCHAR(255);
ALTER TABLE TJN_PROJECTS ADD REPO_ROOT VARCHAR(255);
   
   
--------------------------------------------------------
--  Tenjin 212 alters
--------------------------------------------------------   
ALTER TABLE TJN_PROJECTS ADD TEST_DATA_REPO_TYPE VARCHAR(255);
ALTER TABLE TJN_PROJECTS ADD REPO_ROOT VARCHAR(255);
ALTER TABLE RUNRESULT_TS_TXN ADD API_REQ_LOG VARCHAR2(4000 BYTE);
ALTER TABLE TJN_USER_SECURITY ADD RESET_COUNT VARCHAR2(20 BYTE);
ALTER TABLE TJN_RUN_DEFECTS MODIFY DEF_SEVERITY VARCHAR2(30 BYTE);
ALTER TABLE TJN_RUN_DEFECTS MODIFY DEF_PRIORITY VARCHAR2(30 BYTE);


CREATE TABLE "TJN_PRJ_TESTDATA"
   (    "PRJ_ID" NUMBER,
   "APP_ID" NUMBER,
    "FUNC_CODE" VARCHAR2(255),
    "FILE_NAME" VARCHAR2(255),
    "UPLOADED_BY" VARCHAR2(255),
  "UPLOADED_ON" Date
   ) ;


--------------------------------------------------------
    Tenjin 212  VAPT Password alters
--------------------------------------------------------  

ALTER TABLE TJN_USER_SECURITY ADD RESET_COUNT VARCHAR2(20 BYTE);
ALTER TABLE USRAUTCREDENTIALS  MODIFY  APP_PASSWORD  VARCHAR2(1024 BYTE);
ALTER TABLE USRAUTCREDENTIALS  MODIFY  APP_TXN_PWD   VARCHAR2(1024 BYTE);
ALTER TABLE MASAPPLICATION     MODIFY  TEMPLATE_PASSWORD VARCHAR2(1024  BYTE);
ALTER TABLE TJN_DEF_TOOL_MASTER MODIFY  ADMIN_PWD      VARCHAR2(1024  BYTE);
ALTER TABLE TJN_DEF_USER_MAPING MODIFY  DEF_TOOL_PWD    VARCHAR2(1024  BYTE);
ALTER TABLE MASREGCLIENTS     MODIFY  RC_DEVICE_FARM_PASSWORD VARCHAR2(1024  BYTE);
ALTER TABLE MASREGCLIENTS     MODIFY  RC_DEVICE_FARM_KEY   VARCHAR2(1024  BYTE);
ALTER TABLE TJN_PARAMS         MODIFY  PARAM_VALUE      VARCHAR2(1024  BYTE);
ALTER TABLE TJN_TM_USER_MAPPING MODIFY  PASSWORD     VARCHAR2(1024  BYTE);

  --------------------------------------------------------------------- 
ALTER TABLE TJN_RUN_DEFECT_LINKAGE ADD REPO_OWNER VARCHAR(255);
   
 CREATE TABLE "TJN_AWSS3_DETAIL"
( "ACCESS_KEY_ID" CLOB NOT NULL,
"ACCESS_SEC_KEY" CLOB NOT NULL 
) ;  

ALTER TABLE TJN_AUT_APIS ADD ENCRYPTION_TYPE VARCHAR2(20 BYTE);