--------------------------------------------------------
--  DDL for Table TJN_EXEC_SETTINGS
--------------------------------------------------------

ALTER TABLE [TJN_EXEC_SETTINGS]
ALTER COLUMN [BROWSER] varchar(30);

--------------------------------------------------------
--  DDL for Table USRAUTCREDENTIALS
--------------------------------------------------------

ALTER TABLE [USRAUTCREDENTIALS]
ALTER COLUMN [APP_USER_TYPE] varchar(30);

--------------------------------------------------------
--  DDL for Table TJN_AUDIT
--------------------------------------------------------

ALTER TABLE TJN_AUDIT ADD OLD_DATA VARCHAR(max);
ALTER TABLE TJN_AUDIT ADD NEW_DATA VARCHAR(max);

--------------------------------------------------------
--  DDL for Table LRNR_AUDIT_TRAIL
--------------------------------------------------------

ALTER TABLE LRNR_AUDIT_TRAIL ADD FIELDS_LEARNT_COUNT FLOAT; 

--------------------------------------------------------
--  DDL for Table LRNR_AUDIT_TRAIL_API
--------------------------------------------------------

ALTER TABLE LRNR_AUDIT_TRAIL_API ADD FIELDS_LEARNT_COUNT FLOAT;

--------------------------------------------------------
--  DDL for Table TJN_PRJ_MAIL_SETTINGS
--------------------------------------------------------
ALTER TABLE TJN_PRJ_MAIL_SETTINGS ADD TJN_ADDITIONAL_MAILS VARCHAR(2048);
ALTER TABLE TJN_PRJ_MAIL_SETTINGS ADD PRJ_MAIL_TYPE VARCHAR(256);

--------------------------------------------------------
--  DDL for Table TESTSTEPS
--------------------------------------------------------
ALTER TABLE TESTSTEPS ALTER COLUMN FUNC_CODE VARCHAR(50);


--------------------------------------------------------
--  DDL for Table MASTESTRUNS
--------------------------------------------------------

ALTER TABLE MASTESTRUNS ADD RUN_JSON VARCHAR(MAX);

--------------------------------------------------------
--  DDL for Table UNSTRUCTURED_DATA_VALIDATION
--------------------------------------------------------
CREATE
TABLE [dbo].[UNSTRUCTURED_DATA_VALIDATION](
	[RECORD_ID] [float] NULL,
	[TESTSTEP_ID] [float] NULL,
	[VALIDATION_DATA] [varchar](1000) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

--------------------------------------------------------
--  DDL for Table TJN_PROJECTS
--------------------------------------------------------
ALTER TABLE TJN_PROJECTS ADD TEST_DATA_REPO_TYPE VARCHAR(255);
ALTER TABLE TJN_PROJECTS ADD REPO_ROOT VARCHAR(255);


--------------------------------------------------------
--  Tenjin 212 alters
--------------------------------------------------------   

ALTER TABLE TJN_PROJECTS ADD TEST_DATA_REPO_TYPE VARCHAR(255);
ALTER TABLE TJN_PROJECTS ADD REPO_ROOT VARCHAR(255);
ALTER TABLE RUNRESULT_TS_TXN ADD API_REQ_LOG VARCHAR(4000 );
ALTER TABLE TJN_USER_SECURITY ADD RESET_COUNT VARCHAR(20 );
ALTER TABLE TJN_RUN_DEFECTS ALTER COLUMN  DEF_SEVERITY VARCHAR(30 );
ALTER TABLE TJN_RUN_DEFECTS ALTER COLUMN DEF_PRIORITY VARCHAR(30 );

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TJN_PRJ_TESTDATA](
	[PRJ_ID] [float] NOT NULL,
	[APP_ID] [float] NOT NULL,
	[FUNC_CODE] [varchar](255) NOT NULL,
	[FILE_NAME] [varchar](255) NOT NULL,
        [UPLOADED_BY] [varchar](255) NOT NULL,
        [UPLOADED_ON]  [datetime2](7) NOT NULL,
) ON [PRIMARY]
GO 





--------------------------------------------------------
    Tenjin 212  VAPT Password alters
--------------------------------------------------------  

ALTER TABLE TJN_USER_SECURITY ADD RESET_COUNT VARCHAR(20);
ALTER TABLE USRAUTCREDENTIALS  ALTER COLUMN  APP_PASSWORD  VARCHAR(1024);
ALTER TABLE USRAUTCREDENTIALS  ALTER COLUMN  APP_TXN_PWD   VARCHAR(1024);
ALTER TABLE MASAPPLICATION     ALTER COLUMN  TEMPLATE_PASSWORD VARCHAR(1024);
ALTER TABLE TJN_DEF_TOOL_MASTER ALTER COLUMN  ADMIN_PWD      VARCHAR(1024);
ALTER TABLE TJN_DEF_USER_MAPING ALTER COLUMN  DEF_TOOL_PWD    VARCHAR(1024);
ALTER TABLE MASREGCLIENTS     ALTER COLUMN  RC_DEVICE_FARM_PASSWORD VARCHAR(1024);
ALTER TABLE MASREGCLIENTS     ALTER COLUMN  RC_DEVICE_FARM_KEY   VARCHAR(1024);
ALTER TABLE TJN_PARAMS         ALTER COLUMN  PARAM_VALUE      VARCHAR(1024);
ALTER TABLE TJN_TM_USER_MAPPING ALTER COLUMN  PASSWORD     VARCHAR(1024);


--------------------------------------------------------
--  DDL for Table TESTCASES
--------------------------------------------------------
ALTER TABLE TESTCASES ADD TC_TAG VARCHAR(100);


CREATE TABLE "TJN_AWSS3_DETAIL"
( "ACCESS_KEY_ID" varchar(MAX) NOT NULL,
"ACCESS_SEC_KEY" varchar(MAX) NOT NULL 
) ;

ALTER TABLE TJN_AUT_APIS ADD ENCRYPTION_TYPE VARCHAR2(20);