/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApplicationUnresponsiveException.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 07-Jul-2017           Sriram Sridharan          Newly Added For TENJINCG-197
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
*/

package com.ycs.tenjin.bridge.exceptions;

public class ApplicationUnresponsiveException extends Exception{
	private static final long serialVersionUID = -5693557267195171276L;
	private static final String DEFAULT_PREFIX = "Application is unresponsive";
	public ApplicationUnresponsiveException() {
		super(DEFAULT_PREFIX);
	}
	
	public ApplicationUnresponsiveException(String message) {
		super(DEFAULT_PREFIX + " - " + message);
	}
	
	public ApplicationUnresponsiveException(String message, Throwable cause) {
		super(DEFAULT_PREFIX + " - " + message, cause);
	}
}
