/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MessageAdapterFactory.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 15-06-2021           Ashiki		          Newly Added For TENJINCG-1275
*/


package com.ycs.tenjin.bridge.oem.message;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.message.datahandler.MessageValidateTestDataHandler;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;

public class MessageValidateApplicationFactory {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageValidateApplicationFactory.class);
	
	public static List<String> getAvailableMessageAdapters() throws BridgeException {
		List<String> adapters = new ArrayList<String>();
		
		String expPackageName = "com.ycs.tenjin.adapter.message";
		Reflections reflections = new Reflections(expPackageName);
		
		Set<Class<? extends MessageValidateApplication>> subTypes = reflections.getSubTypesOf(MessageValidateApplication.class);
		for(Class<? extends MessageValidateApplication> type:subTypes){
			String packageName = type.getPackage().getName();
			packageName = packageName.replace(expPackageName + ".", "");
			adapters.add(packageName);
		}
		
		return adapters;
	}
	
	public static MessageValidateApplication create(String messageType) throws BridgeException {
		String expPackageName = "com.ycs.tenjin.adapter.message";
		expPackageName += "." + messageType;
		Reflections reflections = new Reflections(expPackageName);
		Set<Class<? extends MessageValidateApplication>> subTypes = reflections.getSubTypesOf(MessageValidateApplication.class);
		for(Class<? extends MessageValidateApplication> type:subTypes){
			try {
				return type.newInstance();
			} catch (InstantiationException e) {
				
				logger.error("ERROR initializing adapter for [{}]", messageType, e);
			} catch (IllegalAccessException e) {
				
				logger.error("ERROR initializing adapter for [{}]", messageType, e);
			}
		}
		
		logger.error("Could not initialize a proper adapter for [{}]", messageType);
		throw new BridgeException("Could not initialize " + messageType + " adapter. Please contact Tenjin Support.");
		
		
	}
	
	public static MessageValidateApplication getExecutor(String messageType, int runId, ExecutionStep step) throws BridgeException {
		String expPackageName = "com.ycs.tenjin.adapter.message";
		expPackageName += "." + messageType;
		
		Reflections reflections = new Reflections(expPackageName);
		Set<Class<? extends MessageValidateApplication>> subTypes = reflections.getSubTypesOf(MessageValidateApplication.class);
		
		for(Class<? extends MessageValidateApplication>  type : subTypes) {
			try {
				Constructor<?> constructor = type.getConstructor(int.class, ExecutionStep.class);
				return (MessageValidateApplication) constructor.newInstance(runId, step);
			} catch (Exception e) {
				
				logger.error("Could not initiate Executor for " +messageType, e);
			} 
		}
		
		logger.error("Could not initiate Executor for {}",messageType);
		return null;
	}
	
	public static MessageValidateApplication getLeaner(String messageType) throws BridgeException {
		String expPackageName = "com.ycs.tenjin.adapter.message";
		expPackageName += "." + messageType;
		
		Reflections reflections = new Reflections(expPackageName);
		Set<Class<? extends MessageValidateApplication>> subTypes = reflections.getSubTypesOf(MessageValidateApplication.class);
		
		for(Class<? extends MessageValidateApplication>  type : subTypes) {
			try {
				return type.newInstance();
			} catch (Exception e) {
				
				logger.error("Could not initiate Learner for " +messageType, e);
			} 
		}
		
		logger.error("Could not initiate Learner for {}",messageType);
		return null;
	}
	public static MessageValidateTestDataHandler getDataHandler() throws BridgeException {
		String expPackageName = "com.ycs.tenjin.bridge.oem.message.datahandler";		
		Reflections reflections = new Reflections(expPackageName);
		Set<Class<? extends MessageValidateTestDataHandler>> subTypes = reflections.getSubTypesOf(MessageValidateTestDataHandler.class);
		
		for (Class<? extends MessageValidateTestDataHandler> type : subTypes) {
			try {
				return type.newInstance();
			} catch (Exception e) {
				logger.warn(
						"WARNING - Could not initiate test data provider for {}"						, e);
				return null;
			}
		}
		
		return null;
	}
	
	
}
