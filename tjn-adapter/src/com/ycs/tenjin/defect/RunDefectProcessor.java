/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RunDefectProcessor.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 15-Nov-2016           Sriram Sridharan          Newly Added For 
* 18-Aug-2017			Sriram Sridharan		T25IT-206
*/


package com.ycs.tenjin.defect;

import java.util.List;
import java.util.Map;

import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.bridge.pojo.run.UIValidationResult;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.RunDefectHelper;

public class RunDefectProcessor {
	
	public void createTransactionFailureDefects(int runId, int projectId, String userId, Map<String, List<StepIterationResult>> failureMap) 
			throws DatabaseException{
		
		   RunDefectHelper rHelper=new RunDefectHelper();
		   rHelper.createTransactionFailureDefects(runId, projectId, userId, failureMap);
		}
	
	public void createValidationFailureDefects(int runId, int projectId, String userId, List<StepIterationResult> failures) throws DatabaseException{
		RunDefectHelper rHelper=new RunDefectHelper();
		rHelper.createValidationFailureDefects(runId, projectId, userId, failures);
	}
	
	public void createUIValidationFailureDefects(int runId, int projectId, int testStepRecordId, String userId, String validationType, String pageName, List<UIValidationResult> failures) throws DatabaseException {
		RunDefectHelper rHelper = new RunDefectHelper();
		rHelper.createUIValidationFailureDefects(runId, projectId, userId, testStepRecordId, failures, pageName, validationType);
	}
	
}