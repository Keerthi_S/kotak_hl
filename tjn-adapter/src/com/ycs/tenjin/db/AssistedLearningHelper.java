/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AssistedLearningHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 26-09-2018			Preeti					TENJINCG-742
* 04-10-2018			Preeti					TENJINCG-822
*/
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.AssistedLearningRecord;
import com.ycs.tenjin.util.Constants;

public class AssistedLearningHelper {
	private static final Logger logger = LoggerFactory.getLogger(AssistedLearningHelper.class);
	
	
	public List<AssistedLearningRecord> hydrateAssistedLearningRecords(int appId, String functionCode) throws DatabaseException {
		ArrayList<AssistedLearningRecord> results = new ArrayList<AssistedLearningRecord>();
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("SELECT * FROM TJN_ASSISTED_LEARNING WHERE APP_ID=? AND FUNC_CODE=? ORDER BY FIELD_SEQUENCE");
			){
			
			pst.setInt(1, appId);
			pst.setString(2, functionCode);
			
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					AssistedLearningRecord record = this.buildAssistedLearningObject(rs);
					results.add(record);
				}
			}
			
		} catch(SQLException e) {
			logger.error("Could not fetch assisted learning records");
			throw new DatabaseException("Could not fetch assisted learning records",e);
		}
		return results;
			
	}
	public List<AssistedLearningRecord> persistAssistedLearningRecords(List<AssistedLearningRecord> records) throws DatabaseException {
		
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("INSERT INTO TJN_ASSISTED_LEARNING (ASST_LRN_ID,APP_ID,FUNC_CODE,PAGEAREA_NAME,FIELD_NAME,FIELD_IDENTIFIEDBY,FIELD_SEQUENCE,ASST_LRN_DATA) VALUES(?,?,?,?,?,?,?,?)");
			
				){
			

			for(AssistedLearningRecord record : records){
				int pKey = DatabaseHelper.getGlobalSequenceNumber(conn);
				pst.setInt(1, pKey);
				pst.setInt(2, record.getAppId());
				pst.setString(3, record.getFunctionCode());
				pst.setString(4, record.getPageAreaName());
				pst.setString(5, record.getFieldName());
				pst.setString(6, record.getIdentifiedBy());
				pst.setInt(7, record.getSequence());
				pst.setString(8, record.getData());
				record.setRecordId(pKey);
				pst.addBatch();
		}	
		pst.executeBatch();
		}catch(SQLException e) {
			logger.error("Could not insert assisted learning records");
			throw new DatabaseException("Could not insert assisted learning records",e);
		}
		return records;
	}
	
	
	
	public void deleteAllAssistedLearningRecords(String functionCode, int appId) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("DELETE FROM TJN_ASSISTED_LEARNING WHERE FUNC_CODE =? AND APP_ID=?");
			){
			pst.setString(1, functionCode);
			pst.setInt(2, appId);
			pst.execute();
		}catch(SQLException e) {
			logger.error("Could not delete assisted learning records");
			throw new DatabaseException("Could not delete assisted learning records",e);
		}
	}
	
	public void persistAssistedLearningAuditTrail(int appId, String functionCode, String userId, String oldRecordJson, String newRecordJson)  throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("INSERT INTO TJN_ASSISTED_LRNG_AUDIT_TRAIL (APP_ID,FUNC_CODE,OLD_VALUE,NEW_VALUE,UPDATED_ON,UPDATED_BY) VALUES(?,?,?,?,?,?)");
			
				){
			
				pst.setInt(1, appId);
				pst.setString(2, functionCode);
				pst.setString(3, oldRecordJson);
				pst.setString(4, newRecordJson);
				pst.setTimestamp(5, new Timestamp(new Date().getTime()));
				pst.setString(6, userId);
				pst.execute();
				
		}catch(SQLException e) {
			logger.error("Could not insert assisted learning records audit trail");
			throw new DatabaseException("Could not insert assisted learning records audit trail",e);
		}
	}
	
	private AssistedLearningRecord buildAssistedLearningObject(ResultSet rs) throws SQLException {
		AssistedLearningRecord record = new AssistedLearningRecord();
		record.setRecordId(rs.getInt("ASST_LRN_ID"));
		record.setAppId(rs.getInt("APP_ID"));
		record.setFunctionCode(rs.getString("FUNC_CODE"));
		record.setSequence(rs.getInt("FIELD_SEQUENCE"));
		record.setPageAreaName(rs.getString("PAGEAREA_NAME"));
		record.setFieldName(rs.getString("FIELD_NAME"));
		record.setIdentifiedBy(rs.getString("FIELD_IDENTIFIEDBY"));
		record.setData(rs.getString("ASST_LRN_DATA"));
		return record;
		
	}
}
