/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LearningHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright  2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 02-12-2016            Parveen                REQ #TJN_243_04 Changing insert query
 * 19-12-2016            Leelaprasad            DEFECT #TEN-120
 * 17-02-2017            Leelaprasad            Defect #TENJINCG-114
 * 26-Jul-2017			Sriram					TENJINCG-310
 * 01-08-2016           Leelaprasad             TENJINCG-265
 * 10-08-2017           Padmavathi                for T25IT-63
 * 19-08-2017           Padmavathi                for T25IT-251
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 03-Nov-2017			Sriram Sridharan		Fixes for TENJINCG-414
   27-Nov-2017			Sriram Sridharan		Fixes for TENJINCG-514
   11-07-2018       	Padmavathi              for closing connections
   30-08-2018			Preeti					adding page label in iteration data
   11-10-2018           Leelaprasad              TENJINCG-854
   25-10-2018           Leelaprasad              TENJINCG-884
   29-10-2018           Padmavathi              for TENJINCG-889
   13-11-2018           Padmavathi              TENJINCG-899
   24-12-2018			Ashiki					TJN252-47
   21-02-2018           Leealprasad P           TJN252-90
   24-04-2019			Roshni					TENJINCG-1038
   27-08-2019			Ashiki					TENJINCG-1105
 * 06-02-2020			Roshni					TENJINCG-1168
 * 08-04-2020			Lokanath				TENJINCG-1193
 * 19-03-2020			Pushpalatha				TJN252-7
 * 22-09-2020			Ashiki					TENJINCG-1225
 */

package com.ycs.tenjin.db;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiLearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Metadata;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class LearningHelper {
	int respCode=0;
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(LearningHelper.class);
	
	public void beginLearningRun(int runId, boolean updateStartTime) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		PreparedStatement pst = null;
		
		try {
			String updateQuery = "";
			if(updateStartTime){
				updateQuery = "Update MasTestRuns Set run_status=?, run_start_time=? where run_id=?";
				pst = conn.prepareStatement(updateQuery);
				pst.setString(1, BridgeProcess.IN_PROGRESS);
				pst.setTimestamp(2, new Timestamp(new Date().getTime()));
				pst.setInt(3, runId);
			}else{
				updateQuery = "Update MasTestRuns Set run_status=? where run_id=?";
				pst = conn.prepareStatement(updateQuery);
				pst.setString(1, BridgeProcess.IN_PROGRESS);
				pst.setInt(2, runId);
			}
			
			pst.executeUpdate();
			
		} catch (SQLException e) {
			
			logger.error("ERROR updating Run Start", e);
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
	public Aut hydrateAllApplicationUsers(int appId,String userId) throws DatabaseException{
		
		Aut aut=null;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT * FROM USRAUTCREDENTIALS WHERE APP_ID =? AND APP_LOGIN_NAME = ? ");)
		{
			pst.setInt(1, appId);
			pst.setString(2, userId);
			try(ResultSet rs= pst.executeQuery();){
				
				while (rs.next()) {
					 aut=new Aut();
					aut.setPassword(rs.getString("APP_PASSWORD"));
					return aut;
					
				}
				
			}
		}
		catch(Exception e){
			e.getStackTrace();
			logger.error("Could not fetch selected application user",e);
			throw new DatabaseException("Could not fetch selected application user due to an internal error",e);
		}
		return aut;
}
	public void endLearningRun(int runId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		PreparedStatement pst = null;
		ResultSet rs=null;
		
		try {
			String updateQuery = "";
			try{
			updateQuery = "Update MasTestRuns Set run_status=?, run_end_time=? where run_id=?";
			pst = conn.prepareStatement(updateQuery);
			pst.setString(1, BridgeProcess.COMPLETE);
			pst.setTimestamp(2, new Timestamp(new Date().getTime()));
			pst.setInt(3, runId);
			
			pst.executeUpdate();
			}
			finally{
				DatabaseHelper.close(pst);
			}
			try{
			pst=conn.prepareStatement("SELECT SCH_ID FROM TJN_SCHEDULER WHERE RUN_ID=? AND STATUS='Executing'");
			pst.setInt(1, runId);
			rs=pst.executeQuery();
			int schId=0;
			if(rs.next()){
				schId=rs.getInt("SCH_ID");
			}
			if(schId!=0)
			{
				new SchedulerHelper().updateScheduleStatus("Completed",schId);
			}
			}
			finally{
				DatabaseHelper.close(pst);
			}
			
		} catch (SQLException e) {
			
			logger.error("ERROR updating Run End", e);
		} finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
	
	
	

	

	public TestObject hydrateTestObject(String moduleCode, int appId, String label) throws DatabaseException, SQLException{
		TestObject t= null;

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}
		PreparedStatement pst =null;
		ResultSet rs = null;
		try{
			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE = ? AND FLD_LABEL = ?");
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, label);

			 rs = pst.executeQuery();

			while(rs.next()){
				t = new TestObject();
				t.setLabel(rs.getString("FLD_LABEL"));
				t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
				t.setUniqueId(rs.getString("FLD_UID"));
				t.setObjectClass(rs.getString("FLD_TYPE"));
				t.setLocation(rs.getString("FLD_PAGE_AREA"));
				t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
				t.setName(rs.getString("FLD_UNAME"));
				

			
				t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
				
				
				if(t.getObjectClass().equalsIgnoreCase("table")){
					
				}
			}
			
		}catch(Exception e){
			throw new DatabaseException("Could not fetch field information for field " + label,e);
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return t;
	}
	public TestObject hydrateTestObjectOnPage(String pageName, int appId, String label) throws DatabaseException, SQLException{
		TestObject t= null;

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}
		PreparedStatement pst =null;
		ResultSet rs = null;
		try{
			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_PAGE_AREA = ? AND FLD_LABEL = ?");
			pst.setInt(1, appId);
			pst.setString(2, pageName);
			pst.setString(3, label);

			rs = pst.executeQuery();

			while(rs.next()){
				t = new TestObject();
				t.setLabel(rs.getString("FLD_LABEL"));
				t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
				t.setUniqueId(rs.getString("FLD_UID"));
				t.setObjectClass(rs.getString("FLD_TYPE"));
				t.setLocation(rs.getString("FLD_PAGE_AREA"));
				t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
				t.setName(rs.getString("FLD_UNAME"));
				
				t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
				
				
				if(t.getObjectClass().equalsIgnoreCase("table")){
					
				}
			}
			
		}catch(Exception e){
			throw new DatabaseException("Could not fetch field information for field " + label,e);
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return t;
	}

	public TestObject hydrateTestObject(String moduleCode, String page, int appId, String label) throws DatabaseException, SQLException{
		TestObject t= null;

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}
		PreparedStatement pst =null;
		ResultSet rs = null;
		try{
			
			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE = ? AND FLD_UNAME = ? AND FLD_PAGE_AREA = ?");
			
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, label);
			pst.setString(4, page);
			rs = pst.executeQuery();

			while(rs.next()){
				t = new TestObject();
				t.setLabel(rs.getString("FLD_LABEL"));
				t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
				t.setUniqueId(rs.getString("FLD_UID"));
				t.setObjectClass(rs.getString("FLD_TYPE"));
				t.setLocation(rs.getString("FLD_PAGE_AREA"));
				t.setViewmode(rs.getString("VIEWMODE"));
				t.setMandatory(rs.getString("MANDATORY"));
				t.setLovAvailable(rs.getString("FLD_HAS_LOV"));

				
				t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
				t.setGroup(rs.getString("FLD_GROUP"));
				t.setIsMultiRecord(rs.getString("FLD_IS_MULTI_REC"));
				
				t.setName(rs.getString("FLD_UNAME"));
				if(t.getObjectClass().equalsIgnoreCase("table")){
					
				}
			}
			
		}catch(Exception e){
			throw new DatabaseException("Could not fetch field information for field " + label,e);
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return t;
	}

	
	public ArrayList<TestObject> hydrateTestObjectsOnPage(String moduleCode, int appId, String pageAreaName) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}

		ArrayList<TestObject> testObjects = null;


		PreparedStatement pst = null;
		ResultSet rs = null;

		try{
			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE = ? AND FLD_PAGE_AREA = ? ORDER BY FLD_SEQ_NO");
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, pageAreaName);

			rs = pst.executeQuery();
			testObjects = new ArrayList<TestObject>();
			while(rs.next()){
				TestObject t = new TestObject();
				t.setName(rs.getString("FLD_UNAME"));
				t.setLabel(rs.getString("FLD_LABEL"));
				t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
				t.setUniqueId(rs.getString("FLD_UID"));
				t.setObjectClass(rs.getString("FLD_TYPE"));
				t.setLocation(rs.getString("FLD_PAGE_AREA"));
				t.setViewmode(rs.getString("VIEWMODE"));
				t.setMandatory(rs.getString("MANDATORY"));
				t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
				
				t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
				
				
				t.setDefaultOptions(rs.getString("FLD_DEFAULT_OPTIONS"));
				testObjects.add(t);
			}
		}catch(Exception e){
			logger.error("Exception occurred while fetching test objects under page {}", pageAreaName,e);
			throw new DatabaseException("Could not fetch fields due to an internal error");
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return testObjects;
	}
	
	public TestObject hydrateTestObject(Connection conn, String moduleCode, String page, int appId, String label) throws DatabaseException, SQLException{
		TestObject t= null;


		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}
		PreparedStatement pst =null;
		ResultSet rs = null;
		try{
			
			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE = ? AND FLD_UNAME = ? AND FLD_PAGE_AREA = ?");
			
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, label);
			pst.setString(4, page);
		    rs = pst.executeQuery();

			while(rs.next()){
				t = new TestObject();
				t.setLabel(rs.getString("FLD_LABEL"));
				t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
				t.setUniqueId(rs.getString("FLD_UID"));
				t.setObjectClass(rs.getString("FLD_TYPE"));
				t.setLocation(rs.getString("FLD_PAGE_AREA"));
				t.setViewmode(rs.getString("VIEWMODE"));
				t.setMandatory(rs.getString("MANDATORY"));
				t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
				
				t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
				t.setGroup(rs.getString("FLD_GROUP"));
				t.setIsMultiRecord(rs.getString("FLD_IS_MULTI_REC"));
				
				t.setName(rs.getString("FLD_UNAME"));
				
			}
			
		}catch(Exception e){
			throw new DatabaseException("Could not fetch field information for field " + label,e);
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return t;
	}

	

	
	
	
	/*Modified by Roshni TENJINCG-1168 starts */
	/*public Multimap<String, Location> persistLocations(Multimap<String, Location> locMap, String moduleCode, int appId) throws DatabaseException{*/
	public Multimap<String, Location> persistLocations(Multimap<String, Location> locMap, String moduleCode, int appId,Connection conn) throws DatabaseException{
	
		//Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		/*Modified by Roshni TENJINCG-1168 ends */	
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		Multimap<String, Location> failed = ArrayListMultimap.create();
		
		PreparedStatement pst = null;

		try{
			
			String query = "INSERT INTO AUT_FUNC_PAGEAREAS (PA_APP,PA_FUNC_CODE,PA_NAME,PA_PARENT,PA_SEQ_NO,PA_TYPE,PA_WAY_IN,PA_WAY_OUT,PA_SOURCE,PA_PATH,TXNMODE,PA_LABEL,PA_SCREEN_TITLE,PA_IS_MRB) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			
			pst = conn.prepareCall(query);
			//conn.setAutoCommit(false);
			for(String key:locMap.keySet()){
				Collection<Location> locs = locMap.get(key);

				for(Location location:locs){
					try{
						logger.debug("Persisting location " + location.getLocationName());
						
						String insQuery = "INSERT INTO AUT_FUNC_PAGEAREAS (PA_APP,PA_FUNC_CODE,PA_NAME,PA_PARENT,PA_SEQ_NO,PA_TYPE,PA_WAY_IN,PA_WAY_OUT,PA_SOURCE,PA_PATH,TXNMODE,PA_LABEL,PA_SCREEN_TITLE,PA_IS_MRB) VALUES(" + appId + ",'" + moduleCode + "','" + location.getLocationName() + 
								"','" + location.getParent() + "'," + location.getSequence() + ",'" + location.getLocationType() + "','" + 
								location.getWayIn() + "','" + location.getWayOut() + "',EMPTY_CLOB(),'" + location.getPath() + "','G','" + location.getLabel() + 
								"','" + location.getScreenTitle() + "','Y')";
						logger.debug(insQuery);
						pst.setInt(1,appId);
						pst.setString(2, moduleCode);
						pst.setString(3, location.getLocationName());
						pst.setString(4,location.getParent());
						pst.setInt(5,location.getSequence());
						pst.setString(6,location.getLocationType());
						pst.setString(7, location.getWayIn());
						pst.setString(8, location.getWayOut());
						pst.setClob(9, (Clob)null);
						pst.setString(10, location.getPath());
						
						pst.setString(11, "G");
						pst.setString(12, location.getLabel());
						
						pst.setString(13, location.getScreenTitle());
						

						if(location.getLocationType() != null && location.getLocationType().equalsIgnoreCase("table")){
							location.setMultiRecordBlock(true);
						}

						if(location.isMultiRecordBlock()){
							pst.setString(14, "Y");
						}else{
							pst.setString(14, "N");
						}
						pst.addBatch();

					}catch(Exception e){
						logger.error("Error persisting location " + location.getLocationName(),e);
						logger.error("Clearing learning data to avoid corruption");
						
						location.setPersistStatus(e.getMessage());
						failed.put(location.getLocationName(), location);
						
					}
				}
				
			}
			pst.executeBatch();
			//conn.commit();
		}catch(Exception e){
			try{
				if(conn!=null)
					conn.rollback();
			}catch(Exception e1){
			}
			logger.error("ERROR Executing Batch statement to persist locations");
			logger.error(e.getMessage(),e);
			throw new DatabaseException("Could not persist locations",e);
		}finally{
			
			DatabaseHelper.close(pst);
			/*commented by Roshni TENJINCG-1168 */	
			//DatabaseHelper.close(conn);
		}

		
		return failed;
	
	}
	
	
	
	public Multimap<String, Location> persistLocations(Multimap<String, Location> locMap, String moduleCode, int appId, String operation, String entityType) throws DatabaseException{
		int responseCode=this.respCode;
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		Multimap<String, Location> failed = ArrayListMultimap.create();
		
		PreparedStatement pst =null;

		try{
		
			String query = "INSERT INTO AUT_FUNC_PAGEAREAS (PA_APP,PA_FUNC_CODE,PA_NAME,PA_PARENT,PA_SEQ_NO,PA_TYPE,PA_WAY_IN,PA_WAY_OUT,PA_SOURCE,PA_PATH,TXNMODE,PA_LABEL,PA_SCREEN_TITLE,PA_IS_MRB,PA_API_OPERATION, PA_API_ENTITY_TYPE,PA_API_RESP_TYPE) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			
			pst = conn.prepareCall(query);
			//conn.setAutoCommit(false);
			for(String key:locMap.keySet()){
				Collection<Location> locs = locMap.get(key);

				for(Location location:locs){
					try{
						logger.debug("Persisting location " + location.getLocationName());
						
						pst.setInt(1,appId);
						pst.setString(2, moduleCode);
						pst.setString(3, location.getLocationName());
						pst.setString(4,location.getParent());
						pst.setInt(5,location.getSequence());
						pst.setString(6,location.getLocationType());
						pst.setString(7, location.getWayIn());
						pst.setString(8, location.getWayOut());
						pst.setClob(9, (Clob)null);
						pst.setString(10, location.getPath());
						
						pst.setString(11, "G");
						pst.setString(12, location.getLabel());
						
						pst.setString(13, location.getScreenTitle());
						

						if(location.getLocationType() != null && location.getLocationType().equalsIgnoreCase("table")){
							location.setMultiRecordBlock(true);
						}

						if(location.isMultiRecordBlock()){
							pst.setString(14, "Y");
						}else{
							pst.setString(14, "N");
						}
						
						pst.setString(15, operation);
						pst.setString(16, entityType.toUpperCase());
						pst.setInt(17, responseCode);
						pst.addBatch();

					}catch(Exception e){
						logger.error("Error persisting location " + location.getLocationName(),e);
						logger.error("Clearing learning data to avoid corruption");
						
						location.setPersistStatus(e.getMessage());
						failed.put(location.getLocationName(), location);
						
					}
				}
			
			}
			pst.executeBatch();
			//conn.commit();
		}catch(Exception e){
			try{
				if(conn!=null)
					conn.rollback();
			}catch(Exception e1){
			}
			logger.error("ERROR Executing Batch statement to persist locations");
			logger.error(e.getMessage(),e);
			throw new DatabaseException("Could not persist locations",e);
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		
		return failed;
		
	}

	
	
	/*Modified by Roshni TENJINCG-1168 starts */	
	/*public void clearLearningData(String modCode, int appId)throws DatabaseException, SQLException{*/
	public void clearLearningData(String modCode, int appId,Connection conn)throws DatabaseException, SQLException{
		/*Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);*/
		/*Modified by Roshni TENJINCG-1168 ends */	
		PreparedStatement pst =null;
		PreparedStatement pst2 =null;
		try{
			pst = conn.prepareStatement("DELETE FROM AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ? AND TXNMODE = 'G'");
			pst.setInt(1, appId);
			pst.setString(2, modCode);
			pst.execute();

			pst2 = conn.prepareStatement("DELETE FROM AUT_FUNC_PAGEAREAS WHERE PA_APP = ? AND PA_FUNC_CODE = ? AND TXNMODE = 'G'");

			pst2.setInt(1, appId);
			pst2.setString(2, modCode);
			pst2.execute();
		}catch(Exception e){
			throw new DatabaseException("Could not clear existing learning information",e);
		}
		finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(pst2);
		/*Commented by Roshni TENJINCG-1168 */	
			//DatabaseHelper.close(conn);
		}
	}
	
	public void clearLearningData(String modCode, int appId, String operation, String entityType)throws DatabaseException, SQLException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst =null;
		PreparedStatement pst2 =null;
		try{
			
			pst = conn.prepareStatement("DELETE FROM AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ? AND FLD_API_OPERATION=? AND FLD_API_ENTITY_TYPE=? AND FLD_API_RESP_TYPE=?");
			pst.setInt(1, appId);
			pst.setString(2, modCode);
			pst.setString(3, operation);
			pst.setString(4, entityType.toUpperCase());
			pst.setInt(5,this.respCode);
			pst.execute();

			pst2 = conn.prepareStatement("DELETE FROM AUT_FUNC_PAGEAREAS WHERE PA_APP = ? AND PA_FUNC_CODE = ? AND PA_API_OPERATION=? and PA_API_ENTITY_TYPE=? AND PA_API_RESP_TYPE=?");

			pst2.setInt(1, appId);
			pst2.setString(2, modCode);
			pst2.setString(3, operation);
			pst2.setString(4, entityType.toUpperCase());
			pst2.setInt(5,this.respCode);
			pst2.execute();
		}catch(Exception e){
			throw new DatabaseException("Could not clear existing learning information",e);
		}
		finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(conn);
		}
	}

	



	
	
	/*Modified by Roshni TENJINCG-1168 starts */	
	/*public void processPersistedFields(String modCode, int appId) throws DatabaseException{*/
	public void processPersistedFields(String modCode, int appId, Connection conn) throws DatabaseException{
		/*Modified by Roshni TENJINCG-1168 ends */	
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst =null;
		ResultSet rs =null;
		int totalFieldCount = 0;
		TreeMap<String, TestObject> finalMap = new TreeMap<String, TestObject>();
		Multimap<String, TestObject> map = ArrayListMultimap.create();
		logger.debug("Preparing map of existing duplicate fields");
		try {
			try {
				
				pst = conn.prepareStatement(
						"SELECT * FROM WRK_AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ? AND FLD_LABEL IN (SELECT FLD_LABEL FROM WRK_AUT_FUNC_FIELDS GROUP BY FLD_LABEL HAVING COUNT(*) > 1) ORDER BY FLD_SEQ_NO");
				pst.setInt(1, appId);
				pst.setString(2, modCode);
				rs = pst.executeQuery();
				while (rs.next()) {
					totalFieldCount++;
					TestObject t = new TestObject();
					t.setLabel(rs.getString("FLD_LABEL"));
					t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
					t.setUniqueId(rs.getString("FLD_UID"));
					t.setObjectClass(rs.getString("FLD_TYPE"));
					t.setLocation(rs.getString("FLD_PAGE_AREA"));
					t.setSequence(rs.getInt("FLD_SEQ_NO"));
					t.setTabOrder(rs.getInt("FLD_TAB_ORDER"));
					t.setName(rs.getString("FLD_UNAME"));
					t.setViewmode(rs.getString("VIEWMODE"));
					t.setMandatory(rs.getString("MANDATORY"));
					t.setDefaultOptions(rs.getString("FLD_DEFAULT_OPTIONS"));
					t.setEnabled(rs.getString("FLD_IS_ENABLED"));
					t.setIsFooterElement(rs.getString("FLD_IS_FOOTER_ELEMENT"));
					t.setLovAvailable(rs.getString("FLD_HAS_LOV"));

					
					t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
					t.setIsMultiRecord(rs.getString("FLD_IS_MULTI_REC"));
					t.setGroup(rs.getString("FLD_GROUP"));
					
					
					map.put(t.getLabel(), t);
				}
			} catch (Exception e) {
				throw new DatabaseException("Could not process Work Table FIELDS", e);
			} finally {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}
			logger.debug("Done");
			logger.debug("Processing map");
			for (String key : map.keySet()) {
				Collection<TestObject> fields = map.get(key);
				if (fields != null && fields.size() > 0) {
					int instance = 0;
					for (TestObject t : fields) {
						String uniqueName = "";
						
						if (instance < 1 || t.getLabel().equalsIgnoreCase("button")) {
							instance++;
							uniqueName = t.getLabel();
						} else {
							instance++;
							uniqueName = "00" + instance;
							uniqueName = uniqueName.substring(uniqueName.length() - 2);
							uniqueName = t.getLabel() + " " + uniqueName;
						}
						t.setName(uniqueName);
						t.setInstance(instance);
						finalMap.put(Integer.toString(t.getSequence()), t);
					}
				}
			}
			TreeMap<String, TestObject> clearMap = new TreeMap<String, TestObject>();
			logger.debug("Preparing map of existing unique fields");
			try {
				pst = conn.prepareStatement(
						"SELECT * FROM WRK_AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ? AND FLD_LABEL IN (SELECT FLD_LABEL FROM WRK_AUT_FUNC_FIELDS GROUP BY FLD_LABEL HAVING COUNT(*) < 2 ) ORDER BY FLD_SEQ_NO");
				pst.setInt(1, appId);
				pst.setString(2, modCode);
				rs = pst.executeQuery();
				while (rs.next()) {
					totalFieldCount++;
					TestObject t = new TestObject();
					t.setLabel(rs.getString("FLD_LABEL"));
					t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
					t.setUniqueId(rs.getString("FLD_UID"));
					t.setObjectClass(rs.getString("FLD_TYPE"));
					t.setLocation(rs.getString("FLD_PAGE_AREA"));
					t.setSequence(rs.getInt("FLD_SEQ_NO"));
					t.setTabOrder(rs.getInt("FLD_TAB_ORDER"));
					t.setName(rs.getString("FLD_UNAME"));
					t.setViewmode(rs.getString("VIEWMODE"));
					t.setMandatory(rs.getString("MANDATORY"));
					t.setDefaultOptions(rs.getString("FLD_DEFAULT_OPTIONS"));
					t.setEnabled(rs.getString("FLD_IS_ENABLED"));
					t.setIsFooterElement(rs.getString("FLD_IS_FOOTER_ELEMENT"));
					if (t.getName() == null || t.getName().equalsIgnoreCase("")) {
						t.setName(rs.getString("FLD_LABEL"));
					}
					t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
					
					t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
					t.setIsMultiRecord(rs.getString("FLD_IS_MULTI_REC"));
					t.setGroup(rs.getString("FLD_GROUP"));
					

					t.setInstance(1);
					
					clearMap.put(Integer.toString(t.getSequence()), t);
				}
			} catch (Exception e) {
				throw new DatabaseException("Could not process Work Table FIELDS", e);
			} finally {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}
			logger.debug("Done");
			try {
				ArrayList<TreeMap<String, TestObject>> fields = new ArrayList<TreeMap<String, TestObject>>();
				fields.add(finalMap);
				fields.add(clearMap);
				/*Modified by Roshni TENJINCG-1168 starts */	
				/*Multimap<String, TestObject> failed = this.persistFieldsInWorkTable(fields, modCode, appId);*/
				Multimap<String, TestObject> failed = this.persistFieldsInWorkTable(fields, modCode, appId,conn);
				/*Modified by Roshni TENJINCG-1168 ends */	
				int totalFailed = 0;
				for (String key : failed.keySet()) {
					
					totalFailed += failed.get(key).size();
				}
				logger.info("Finished adding processed Test Objects to the database.");
				logger.info("Successfully added " + (totalFieldCount - totalFailed) + " Test Objects out of "
						+ totalFieldCount);
				logger.debug("Clearing Field Wiat time for the Module");
				this.unMapAllWaitTimeField(appId, modCode);
				logger.debug("field wait time clered for module" + modCode);
				logger.info("Failed adding " + totalFailed + " Test Objects, reasons below");

				for (String key : failed.keySet()) {
					for (TestObject t : failed.get(key)) {
						logger.info("Page Area " + key + " --> " + t.getName() + " [Actual Label: " + t.getLabel()
								+ "] --> " + t.getIdentifiedBy() + "[" + t.getUniqueId() + "] --> Type["
								+ t.getObjectClass() + "]");
						logger.error("Fail Reason --> " + t.getPersistStatus());
					}
				}
			} catch (Exception e) {
				throw new DatabaseException("Could not move fields from work table to actual table", e);
			}
			logger.debug("Resolving field indices");
			PreparedStatement ps = null;
			PreparedStatement p = null;
			ResultSet r = null;
			PreparedStatement u = null;
			try {
				
				pst = conn.prepareStatement(
						"SELECT FLD_LABEL,FLD_PAGE_AREA FROM WRK_AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ? GROUP BY FLD_LABEL , FLD_PAGE_AREA  HAVING COUNT(*) > 1");
				pst.setInt(1, appId);
				pst.setString(2, modCode);
				rs = pst.executeQuery();

				String query = "UPDATE WRK_AUT_FUNC_FIELDS SET FLD_INDEX = ? WHERE FLD_APP = ? AND FLD_SEQ_NO = ?";
				ps = conn.prepareStatement(query);
				//conn.setAutoCommit(false);
				while (rs.next()) {
					try{
					String fieldLabel = rs.getString("FLD_LABEL");
					String pageArea = rs.getString("FLD_PAGE_AREA");
					p = conn.prepareStatement(
							"SELECT * FROM WRK_AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ? AND FLD_LABEL = ? AND FLD_PAGE_AREA = ? ORDER BY FLD_SEQ_NO");
					p.setInt(1, appId);
					p.setString(2, modCode);
					p.setString(3, fieldLabel);
					p.setString(4, pageArea);
					r = p.executeQuery();
					int index = 0;

					while (r.next()) {
						index++;
						ps.setInt(1, index);
						ps.setInt(2, appId);
						ps.setInt(3, r.getInt("FLD_SEQ_NO"));
						ps.addBatch();
					}
					
					}finally{
						DatabaseHelper.close(r);
						DatabaseHelper.close(p);
					}
				}
				ps.executeBatch();
				//conn.commit();

				//conn.setAutoCommit(false);
				u = conn.prepareStatement(
						"Update WRK_AUT_FUNC_FIELDS SET FLD_INDEX = ? WHERE FLD_APP = ? and fld_Func_code = ? and fld_index = ?");
				u.setInt(1, 1);
				u.setInt(2, appId);
				u.setString(3, modCode);
				u.setInt(4, 0);
				u.execute();
				//conn.commit();
			} catch (Exception e) {
				if (conn != null)
					try {
						conn.rollback();
					} catch (SQLException e1) {
						
						logger.error("Error ", e1);
					}
				throw new DatabaseException("Could not resolve field index");
			} finally {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(ps);
				DatabaseHelper.close(u);
			}
			logger.debug("Done");
			logger.debug("Preparing final data");
			PreparedStatement pstFinal = null;
			try {
				//conn.setAutoCommit(false);
				pstFinal = conn.prepareStatement(
						"INSERT INTO AUT_FUNC_FIELDS SELECT * FROM WRK_AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ?");
				pstFinal.setInt(1, appId);
				pstFinal.setString(2, modCode);
				pstFinal.execute();
				//conn.commit();
				pstFinal.close();
			} catch (Exception e) {
				if (conn != null)
					try {
						conn.rollback();
					} catch (SQLException e1) {
						
						logger.error("Error ", e1);
					}
				throw new DatabaseException("Could not persist screen elements to final Table", e);
			} finally {
				DatabaseHelper.close(pstFinal);
			} 
		} finally {
			// 
			DatabaseHelper.close(conn);
		}
		logger.debug("Done");
	}
	
	public void processPersistedFields(String modCode, int appId, String operation, String entityType,int responseCode) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		int totalFieldCount = 0;
		TreeMap<String, TestObject> finalMap = new TreeMap<String, TestObject>();
		Multimap<String, TestObject> map = ArrayListMultimap.create();
		logger.debug("Preparing map of existing duplicate fields");
		PreparedStatement pst =null;
		ResultSet rs =null;
		try{
		try{
			pst = conn.prepareStatement("SELECT * FROM WRK_AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ? AND FLD_LABEL IN (SELECT FLD_LABEL FROM WRK_AUT_FUNC_FIELDS GROUP BY FLD_LABEL HAVING COUNT(*) > 1) AND FLD_API_OPERATION=? AND FLD_API_ENTITY_TYPE=? ORDER BY FLD_SEQ_NO");
			pst.setInt(1, appId);
			pst.setString(2, modCode);
			pst.setString(3, operation);
			pst.setString(4, entityType.toUpperCase());
			rs = pst.executeQuery();
			while(rs.next()){
				totalFieldCount++;
				TestObject t = new TestObject();
				t.setLabel(rs.getString("FLD_LABEL"));
				t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
				t.setUniqueId(rs.getString("FLD_UID"));
				t.setObjectClass(rs.getString("FLD_TYPE"));
				t.setLocation(rs.getString("FLD_PAGE_AREA"));
				t.setSequence(rs.getInt("FLD_SEQ_NO"));
				t.setTabOrder(rs.getInt("FLD_TAB_ORDER"));
				t.setName(rs.getString("FLD_UNAME"));
				t.setViewmode(rs.getString("VIEWMODE"));
				t.setMandatory(rs.getString("MANDATORY"));
				t.setDefaultOptions(rs.getString("FLD_DEFAULT_OPTIONS"));
				t.setEnabled(rs.getString("FLD_IS_ENABLED"));
				t.setIsFooterElement(rs.getString("FLD_IS_FOOTER_ELEMENT"));
				t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
				
				t.setResponseType(rs.getInt("FLD_API_RESP_TYPE"));
				
				
				t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
				t.setIsMultiRecord(rs.getString("FLD_IS_MULTI_REC"));
				t.setGroup(rs.getString("FLD_GROUP"));
				
				map.put(t.getLabel(), t);
			}
		}catch(Exception e){
			throw new DatabaseException("Could not process Work Table FIELDS",e);
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		logger.debug("Done");

		logger.debug("Processing map");
		for(String key:map.keySet()){
			Collection<TestObject> fields = map.get(key);
			if(fields != null && fields.size() > 0){
				int instance = 0;
				for(TestObject t:fields){
					String uniqueName = "";
					if(instance < 1){
						instance++;
						uniqueName = t.getLabel();
					}else{
						instance++;
						uniqueName = "00" + instance;
						uniqueName = uniqueName.substring(uniqueName.length()-2);
						uniqueName = t.getLabel() + " " + uniqueName;
					}
					t.setName(uniqueName);
					t.setInstance(instance);
					finalMap.put(Integer.toString(t.getSequence()), t);
				}
			}
		}

		TreeMap<String, TestObject> clearMap = new TreeMap<String, TestObject>();
		logger.debug("Preparing map of existing unique fields");
		try{
			pst = conn.prepareStatement("SELECT * FROM WRK_AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ? AND FLD_LABEL IN (SELECT FLD_LABEL FROM WRK_AUT_FUNC_FIELDS GROUP BY FLD_LABEL HAVING COUNT(*) < 2 ) AND FLD_API_OPERATION=? and FLD_API_ENTITY_TYPE=? ORDER BY FLD_SEQ_NO");
			pst.setInt(1, appId);
			pst.setString(2, modCode);
			pst.setString(3, operation);
			pst.setString(4, entityType.toUpperCase());
			rs = pst.executeQuery();
			while(rs.next()){
				totalFieldCount++;
				TestObject t = new TestObject();
				t.setLabel(rs.getString("FLD_LABEL"));
				t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
				t.setUniqueId(rs.getString("FLD_UID"));
				t.setObjectClass(rs.getString("FLD_TYPE"));
				t.setLocation(rs.getString("FLD_PAGE_AREA"));
				t.setSequence(rs.getInt("FLD_SEQ_NO"));
				t.setTabOrder(rs.getInt("FLD_TAB_ORDER"));
				t.setName(rs.getString("FLD_UNAME"));
				t.setViewmode(rs.getString("VIEWMODE"));
				t.setMandatory(rs.getString("MANDATORY"));
				t.setDefaultOptions(rs.getString("FLD_DEFAULT_OPTIONS"));
				t.setEnabled(rs.getString("FLD_IS_ENABLED"));
				t.setIsFooterElement(rs.getString("FLD_IS_FOOTER_ELEMENT"));
				if(t.getName() == null || t.getName().equalsIgnoreCase("")){
					t.setName(rs.getString("FLD_LABEL"));
				}
				t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
				
				t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
				t.setIsMultiRecord(rs.getString("FLD_IS_MULTI_REC"));
				t.setGroup(rs.getString("FLD_GROUP"));
				
				
				
				t.setInstance(1);
				
				clearMap.put(Integer.toString(t.getSequence()), t);
			}
		}catch(Exception e){
			throw new DatabaseException("Could not process Work Table FIELDS",e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		logger.debug("Done");

		try{
			ArrayList<TreeMap<String, TestObject>> fields = new ArrayList<TreeMap<String,TestObject>>();
			fields.add(finalMap);
			fields.add(clearMap);
			Multimap<String, TestObject> failed = this.persistFieldsInWorkTable(fields, modCode, appId, operation, entityType);
			int totalFailed = 0;
			for(String key:failed.keySet()){
				
				totalFailed+=failed.get(key).size();
			}
			logger.info("Finished adding processed Test Objects to the database.");
			logger.info("Successfully added " + (totalFieldCount - totalFailed) + " Test Objects out of " + totalFieldCount);
			logger.info("Failed adding " + totalFailed + " Test Objects, reasons below");

			for(String key:failed.keySet()){
				for(TestObject t:failed.get(key)){
					logger.info("Page Area " + key + " --> " + t.getName() + " [Actual Label: " + t.getLabel() + "] --> " + t.getIdentifiedBy() + "[" + t.getUniqueId() + "] --> Type[" + t.getObjectClass() + "]");
					logger.error("Fail Reason --> " + t.getPersistStatus());
				}
			}
		}catch(Exception e){
			throw new DatabaseException("Could not move fields from work table to actual table",e);
		}
		logger.debug("Resolving field indices");
		PreparedStatement ps=null;
		PreparedStatement p =null;
		ResultSet r =null;
		PreparedStatement u =null;
		try{
			pst = conn.prepareStatement("SELECT FLD_LABEL,FLD_PAGE_AREA FROM WRK_AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ? AND FLD_API_OPERATION=? and FLD_API_ENTITY_TYPE=? GROUP BY FLD_LABEL , FLD_PAGE_AREA  HAVING COUNT(*) > 1");
			pst.setInt(1, appId);
			pst.setString(2, modCode);
			pst.setString(3, operation);
			pst.setString(4, entityType.toUpperCase());
			rs = pst.executeQuery();
			String query = "UPDATE WRK_AUT_FUNC_FIELDS SET FLD_INDEX = ? WHERE FLD_APP = ? AND FLD_SEQ_NO = ?";
			ps = conn.prepareStatement(query);
			//conn.setAutoCommit(false);
			while(rs.next()){
				try{
				String fieldLabel = rs.getString("FLD_LABEL");
				String pageArea = rs.getString("FLD_PAGE_AREA");
				p = conn.prepareStatement("SELECT * FROM WRK_AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ? AND FLD_LABEL = ? AND FLD_PAGE_AREA = ? AND FLD_API_OPERATION=? and FLD_API_ENTITY_TYPE=? ORDER BY FLD_SEQ_NO");
				p.setInt(1, appId);
				p.setString(2, modCode);
				p.setString(3, fieldLabel);
				p.setString(4, pageArea);
				p.setString(5, operation);
				p.setString(6, entityType.toUpperCase());
				r = p.executeQuery();
				int index = 0;

				while(r.next()){
					index++;
					ps.setInt(1, index);
					ps.setInt(2,appId);
					ps.setInt(3,r.getInt("FLD_SEQ_NO"));
					
					ps.addBatch();
				}
				
				}finally{
			DatabaseHelper.close(r);
			DatabaseHelper.close(p);
			}
			}
			ps.executeBatch();
			//conn.commit();

			//conn.setAutoCommit(false);
			u = conn.prepareStatement("Update WRK_AUT_FUNC_FIELDS SET FLD_INDEX = ? WHERE FLD_APP = ? and fld_Func_code = ? and fld_index = ? and fld_api_operation=? and FLD_API_ENTITY_TYPE=?");
			u.setInt(1, 1);
			u.setInt(2, appId);
			u.setString(3, modCode);
			u.setInt(4, 0);
			u.setString(5, operation);
			u.setString(6, entityType.toUpperCase());
			u.execute();
			//conn.commit();
		}catch(Exception e){
			
			logger.error("ERROR resolving field index", e);
			if(conn!=null)
				try {
					conn.rollback();
				} catch (SQLException e1) {
					
					logger.error("Error ", e1);
				}
			throw new DatabaseException("Could not resolve field index");
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(ps);
			DatabaseHelper.close(u);
		}

		logger.debug("Done");
		logger.debug("Preparing final data");
		PreparedStatement pstFinal = null;
		try{
			//conn.setAutoCommit(false);
			pstFinal = conn.prepareStatement("INSERT INTO AUT_FUNC_FIELDS SELECT * FROM WRK_AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ? AND FLD_API_OPERATION=?");
			pstFinal.setInt(1, appId);
			pstFinal.setString(2, modCode);
			pstFinal.setString(3, operation);
			pstFinal.execute();
			//conn.commit();
		}catch(Exception e){
			if(conn!=null)
				try {
					conn.rollback();
				} catch (SQLException e1) {
					
					logger.error("Error ", e1);
				}
			throw new DatabaseException("Could not persist screen elements to final Table",e);
		}finally{
			DatabaseHelper.close(pstFinal);
			
		}}finally{
			DatabaseHelper.close(conn);
		}
		logger.debug("Done");
	}

	/*Modified by Roshni TENJINCG-1168 starts */	
	/*public Multimap<String, TestObject> persistFieldsInWorkTable(ArrayList<TreeMap<String, TestObject>> fields, String modCode, int appId) throws DatabaseException{*/
	public Multimap<String, TestObject> persistFieldsInWorkTable(ArrayList<TreeMap<String, TestObject>> fields, String modCode, int appId,Connection conn) throws DatabaseException{
	/*Modified by Roshni TENJINCG-1168 ends */	
		Multimap<String, TestObject> failed = ArrayListMultimap.create();
		
		//Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst0 = null;
		try{
			logger.debug("Clearing Work table");
			pst0 = conn.prepareStatement("DELETE FROM WRK_AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ?");
			pst0.setInt(1, appId);
			pst0.setString(2, modCode);
			pst0.execute();
			logger.debug("Done");
		}catch(Exception e){
			throw new DatabaseException("Could not clear work table",e);
		}finally{
			DatabaseHelper.close(pst0);
		}
		PreparedStatement pst =null;
		try{
			//conn.setAutoCommit(false);
			String query = "Insert Into WRK_AUT_FUNC_FIELDS (FLD_APP,FLD_FUNC_CODE,FLD_SEQ_NO,FLD_UNAME,FLD_LABEL,FLD_TAB_ORDER,FLD_INSTANCE,FLD_INDEX,FLD_UID_TYPE,FLD_UID,FLD_TYPE,FLD_PAGE_AREA,TXNMODE,VIEWMODE,MANDATORY,FLD_DEFAULT_OPTIONS,FLD_IS_ENABLED,FLD_IS_FOOTER_ELEMENT,FLD_HAS_LOV,FLD_IS_MULTI_REC,FLD_GROUP,FLD_HAS_AUTOLOV) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			pst = conn.prepareCall(query);
			for(TreeMap<String,TestObject> fieldsMap:fields){
				for(String key:fieldsMap.keySet()){
					TestObject t = fieldsMap.get(key);

					try{
						
						pst.setInt(1, appId);
						pst.setString(2, modCode);
						pst.setInt(3, t.getSequence());
						pst.setString(4, t.getName());
						if(t.getLabel() != null && !t.getLabel().equalsIgnoreCase("")){
							pst.setString(5, t.getLabel());
						}else{
							pst.setString(5, t.getUniqueId());
						}
						pst.setInt(6, t.getTabOrder());
						pst.setInt(7, t.getInstance());
						pst.setInt(8, 0);
						pst.setString(9, t.getIdentifiedBy());
						pst.setString(10, t.getUniqueId());
						pst.setString(11, t.getObjectClass());
						pst.setString(12, t.getLocation());
						pst.setString(13, "G");
						pst.setString(14, t.getViewmode());
						pst.setString(15, t.getMandatory());
						if(t.getDefaultOptions() != null && t.getDefaultOptions().length() <= 1000){
							pst.setString(16, t.getDefaultOptions());
						}else{
							logger.warn("Default Options for Field [{}] on page [{}] has been purged as it is more than 1000 characters", t.getLabel(), t.getLocation());
							pst.setString(16, null);
						}
						pst.setString(17, t.getEnabled());
						if(t.getIsFooterElement() == null || t.getIsFooterElement().equalsIgnoreCase("")){
							t.setIsFooterElement("N");
						}
						pst.setString(18, t.getIsFooterElement());
						pst.setString(19, t.getLovAvailable());
						pst.setString(20, t.getIsMultiRecord());
						pst.setString(21, t.getGroup());
						
						pst.setString(22, t.getAutoLovAvailable());
						
						pst.addBatch();

					}catch(Exception e){
						
						t.setPersistStatus(e.getMessage());
						failed.put(t.getLocation(), t);

					}

				}
			}
			
			pst.executeBatch();
			//conn.commit();
		}catch(Exception e){
			logger.error("Exception while persisting fields in work table",e);
			if(conn != null){
				try {
						conn.rollback();
					} catch (SQLException e1) {
						
						logger.error("Error ", e1);
					}
			}
		}finally{
			
			DatabaseHelper.close(pst);
			/*Commented by Roshni TENJINCG-1168 ends */	
			//DatabaseHelper.close(conn);
		}
		
		return failed;
	
	}
	
	public Multimap<String, TestObject> persistFieldsInWorkTable(ArrayList<TreeMap<String, TestObject>> fields, String modCode, int appId, String operation, String entityType) throws DatabaseException{
      
		Multimap<String, TestObject> failed = ArrayListMultimap.create();
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst0 =null;
		try{
			try{
			logger.debug("Clearing Work table");
			pst0 = conn.prepareStatement("DELETE FROM WRK_AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ?");
			pst0.setInt(1, appId);
			pst0.setString(2, modCode);
			pst0.execute();
			logger.debug("Done");
		}catch(Exception e){
			throw new DatabaseException("Could not clear work table",e);
		}finally{
			DatabaseHelper.close(pst0);
		}
		PreparedStatement pst =null;
		try{
			//conn.setAutoCommit(false);
			
			String query = "Insert Into WRK_AUT_FUNC_FIELDS (FLD_APP,FLD_FUNC_CODE,FLD_SEQ_NO,FLD_UNAME,FLD_LABEL,FLD_TAB_ORDER,FLD_INSTANCE,FLD_INDEX,FLD_UID_TYPE,FLD_UID,FLD_TYPE,FLD_PAGE_AREA,TXNMODE,VIEWMODE,MANDATORY,FLD_DEFAULT_OPTIONS,FLD_IS_ENABLED,FLD_IS_FOOTER_ELEMENT,FLD_HAS_LOV,FLD_IS_MULTI_REC,FLD_GROUP,FLD_HAS_AUTOLOV,FLD_API_OPERATION, FLD_API_ENTITY_TYPE,FLD_API_RESP_TYPE) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			pst = conn.prepareCall(query);
			for(TreeMap<String,TestObject> fieldsMap:fields){
				for(String key:fieldsMap.keySet()){
					TestObject t = fieldsMap.get(key);

					try{
						
						pst.setInt(1, appId);
						pst.setString(2, modCode);
						pst.setInt(3, t.getSequence());
						pst.setString(4, t.getName());
						if(t.getLabel() != null && !t.getLabel().equalsIgnoreCase("")){
							pst.setString(5, t.getLabel());
						}else{
							pst.setString(5, t.getUniqueId());
						}
						pst.setInt(6, t.getTabOrder());
						pst.setInt(7, t.getInstance());
						pst.setInt(8, 0);
						pst.setString(9, t.getIdentifiedBy());
						pst.setString(10, t.getUniqueId());
						pst.setString(11, t.getObjectClass());
						pst.setString(12, t.getLocation());
						pst.setString(13, "G");
						pst.setString(14, t.getViewmode());
						pst.setString(15, t.getMandatory());
						if(t.getDefaultOptions() != null && t.getDefaultOptions().length() <= 1000){
							pst.setString(16, t.getDefaultOptions());
						}else{
							logger.warn("Default Options for Field [{}] on page [{}] has been purged as it is more than 1000 characters", t.getLabel(), t.getLocation());
							pst.setString(16, null);
						}
						pst.setString(17, t.getEnabled());
						if(t.getIsFooterElement() == null || t.getIsFooterElement().equalsIgnoreCase("")){
							t.setIsFooterElement("N");
						}
						pst.setString(18, t.getIsFooterElement());
						pst.setString(19, t.getLovAvailable());
						pst.setString(20, t.getIsMultiRecord());
						pst.setString(21, t.getGroup());
						
						
						pst.setString(22, t.getAutoLovAvailable());
						
						pst.setString(23, operation);
						pst.setString(24, entityType.toUpperCase());
						pst.setInt(25, this.respCode);
						
						
						pst.addBatch();

						
					}catch(Exception e){
						
						t.setPersistStatus(e.getMessage());
						failed.put(t.getLocation(), t);

					}

				}
			}
			
			pst.executeBatch();
			//conn.commit();
		}catch(Exception e){
			logger.error("Exception while persisting fields in work table",e);
			if(conn != null){
				try {
						conn.rollback();
					} catch (SQLException e1) {
						
						logger.error("Error ", e1);
					}
			}
		}finally{
			
			DatabaseHelper.close(pst);
		}
		}finally{
			DatabaseHelper.close(conn);
		}
		
		return failed;
	
	}
	
	
	/*modified by shruthi for VAPT helper fixes starts*/
	public ArrayList<TestObject> hydrateScreenElements(String modCode, String appName) throws DatabaseException, SQLException{

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		ArrayList<TestObject> testObjects = new ArrayList<TestObject>();
		if(conn != null){
			/*Statement st = null;*/
			PreparedStatement pst = null;
			ResultSet rs=null;
			try{
				/*st = conn.createStatement();
				rs = st.executeQuery("Select * from AUT_FUNC_FIELDS where FLD_FUNC_CODE = '" + modCode + "' AND FLD_APP = '" + appName + "' AND FLD_LABEL IS NOT NULL");*/
				pst = conn.prepareStatement("Select * from AUT_FUNC_FIELDS where FLD_FUNC_CODE =? AND FLD_APP =? AND FLD_LABEL IS NOT NULL");
				pst.setString(1, modCode);
				pst.setString(2, appName);
				rs = pst.executeQuery();

				while(rs.next()){
					TestObject t = new TestObject();
					t.setLabel(rs.getString("FLD_LABEL"));
					t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
					t.setUniqueId(rs.getString("FLD_UID"));
					t.setObjectClass(rs.getString("FLD_TYPE"));
					t.setLocation(rs.getString("FLD_PAGE_AREA"));
					testObjects.add(t);
				}
				
			}catch(Exception e){
				throw new DatabaseException("Could not fetch test object details",e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}else{
			throw new DatabaseException("A valid connection to the database could not be obtained");
		}

		return testObjects;

	}
	/*modified by shruthi for VAPT helper fixes ends*/

	/*modified by shruthi for VAPT helper fixes starts*/
	public ArrayList<TestObject> hydrateScreenElements(String modCode, int appId) throws DatabaseException, SQLException{

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		ArrayList<TestObject> testObjects = new ArrayList<TestObject>();
		if(conn != null){
			/*Statement st = null;*/
			PreparedStatement pst = null;
			ResultSet rs=null;
			try{
				 /*st = conn.createStatement();
				rs = st.executeQuery("Select * from AUT_FUNC_FIELDS where FLD_FUNC_CODE = '" + modCode + "' AND FLD_APP = " + appId + " AND FLD_LABEL IS NOT NULL ORDER BY FLD_SEQ_NO");*/
				pst = conn.prepareStatement("Select * from AUT_FUNC_FIELDS where FLD_FUNC_CODE=? AND FLD_APP =? AND FLD_LABEL IS NOT NULL ORDER BY FLD_SEQ_NO");
				pst.setString(1, modCode);
				pst.setInt(2, appId);
				rs = pst.executeQuery();
				
				while(rs.next()){
					TestObject t = new TestObject();
					
					
					t.setSequence(rs.getInt("FLD_SEQ_NO"));
					t.setName(rs.getString("FLD_UNAME"));
					t.setLabel(rs.getString("FLD_LABEL"));
					t.setTabOrder(rs.getInt("FLD_TAB_ORDER"));
					t.setInstance(rs.getInt("FLD_INSTANCE"));
					t.setIndex(rs.getInt("FLD_INDEX"));
					t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
					t.setUniqueId(rs.getString("FLD_UID"));
					t.setObjectClass(rs.getString("FLD_TYPE"));
					t.setLocation(rs.getString("FLD_PAGE_AREA"));
					t.setViewmode(rs.getString("VIEWMODE"));
					t.setMandatory(rs.getString("MANDATORY"));
					testObjects.add(t);
				}
				
			}catch(Exception e){
				throw new DatabaseException("Could not fetch test object details",e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}else{
			throw new DatabaseException("A valid connection to the database could not be obtained");
		}

		return testObjects;

	}
	/*modified by shruthi for VAPT helper fixes ends*/
	
	/*modified by shruthi for VAPT helper fixes starts*/
	public Location hydrateLocation(Connection conn,String pageName) throws DatabaseException{
		Location t = new Location();
		if(conn != null){
			/*Statement st = null;*/
			PreparedStatement pst = null;
			ResultSet rs=null;
			try{
				/*st = conn.createStatement();

				rs = st.executeQuery("Select * From AUT_FUNC_PAGEAREAS where PA_NAME = '" + pageName + "'");*/
				pst = conn.prepareStatement("Select * From AUT_FUNC_PAGEAREAS where PA_NAME =?");
				pst.setString(1, pageName);
				rs = pst.executeQuery();
				
				while(rs.next()){

					t.setLocationName(rs.getString("PA_NAME"));
					t.setParent(rs.getString("PA_PARENT"));
					t.setSequence(rs.getInt("PA_SEQ_NO"));
					t.setWayIn(rs.getString("PA_WAY_IN"));
					t.setWayOut(rs.getString("PA_WAY_OUT"));
					t.setLocationType(rs.getString("PA_TYPE"));
				}
				
			}catch(Exception e){
				throw new DatabaseException("Could not fetch location details",e);
			}finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}

		}else{
			throw new DatabaseException("A valid connection to the database could not be obtained");
		}

		return t;
	}
	/*modified by shruthi for VAPT helper fixes ends*/

	public Location hydrateLocation(String moduleCode, int app,String pageName) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);	
		Location t = new Location();
		if(conn != null){
			PreparedStatement pst=null;
			ResultSet rs=null;
			try{
               pst=conn.prepareStatement("Select * From AUT_FUNC_PAGEAREAS where PA_NAME = ? And PA_FUNC_CODE=? and PA_APP = ?");
               pst.setString(1, pageName);
               pst.setString(2, moduleCode);
               pst.setInt(3, app);
               rs=pst.executeQuery();
				while(rs.next()){

					t.setLocationName(rs.getString("PA_NAME"));
					t.setParent(rs.getString("PA_PARENT"));
					t.setSequence(rs.getInt("PA_SEQ_NO"));
					t.setWayIn(rs.getString("PA_WAY_IN"));
					t.setWayOut(rs.getString("PA_WAY_OUT"));
					t.setLocationType(rs.getString("PA_TYPE"));
					t.setLabel(rs.getString("PA_LABEL"));
					
					 String mrb = rs.getString("PA_IS_MRB");
                     if(mrb != null && mrb.equalsIgnoreCase("Y")){
                            t.setMultiRecordBlock(true);
                     }else{
                            t.setMultiRecordBlock(false);
                     }
                     
				}
				
			}catch(Exception e){
				throw new DatabaseException("Could not fetch location details",e);
			}finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}else{
			throw new DatabaseException("A valid connection to the database could not be obtained");
		}

		return t;
	}
	/*modified by shruthi for VAPT helper fixes starts*/
	public Location hydrateLocation(Connection conn, String moduleCode, int app,String pageName) throws DatabaseException{
		Location t = new Location();
		if(conn != null){
			/*Statement st = null;*/
			PreparedStatement pst = null;
			ResultSet rs=null;
			try{
				/*st = conn.createStatement();

				rs = st.executeQuery("Select * From AUT_FUNC_PAGEAREAS where PA_NAME = '" + pageName + "' And PA_FUNC_CODE='" + moduleCode + "' and PA_APP = " + app + " AND TXNMODE='G'");*/
				pst = conn.prepareStatement("Select * From AUT_FUNC_PAGEAREAS where PA_NAME =? And PA_FUNC_CODE=? and PA_APP =? AND TXNMODE='G'");
				pst.setString(1, pageName);
				pst.setString(2, moduleCode);
				pst.setInt(3, app);
				rs = pst.executeQuery();

				while(rs.next()){

					t.setLocationName(rs.getString("PA_NAME"));
					t.setParent(rs.getString("PA_PARENT"));
					t.setSequence(rs.getInt("PA_SEQ_NO"));
					t.setWayIn(rs.getString("PA_WAY_IN"));
					t.setWayOut(rs.getString("PA_WAY_OUT"));
					t.setLocationType(rs.getString("PA_TYPE"));
					String mrb = rs.getString("PA_IS_MRB");
					if(mrb != null && mrb.equalsIgnoreCase("Y")){
						t.setMultiRecordBlock(true);
					}else{
						t.setMultiRecordBlock(false);
					}
				}
			}catch(Exception e){
				throw new DatabaseException("Could not fetch location details",e);
			}finally{
				
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}

		}else{
			throw new DatabaseException("A valid connection to the database could not be obtained");
		}

		return t;
	}
	/*modified by shruthi for VAPT helper fixes ends*/

	public ArrayList<Location> hydrateSiblingMultiRecordBlocks(Connection conn, String moduleCode, int app, String pageName, String parentPageName) throws DatabaseException{
		ArrayList<Location> siblingMrbs = new ArrayList<Location>();
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst =null;
		ResultSet rs = null;
		try{
		    pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_PAGEAREAS WHERE PA_APP = ? AND PA_FUNC_CODE = ? AND PA_PARENT = ?  ORDER BY PA_SEQ_NO");
			pst.setInt(1, app);
			pst.setString(2, moduleCode);
			pst.setString(3, parentPageName);

			rs = pst.executeQuery();
			while(rs.next()){
				Location t = new Location();
				t.setLocationName(rs.getString("PA_NAME"));
				t.setParent(rs.getString("PA_PARENT"));
				t.setSequence(rs.getInt("PA_SEQ_NO"));
				t.setWayIn(rs.getString("PA_WAY_IN"));
				t.setWayOut(rs.getString("PA_WAY_OUT"));
				t.setLocationType(rs.getString("PA_TYPE"));
				String mrb = rs.getString("PA_IS_MRB");
				if(mrb != null && mrb.equalsIgnoreCase("Y")){
					t.setMultiRecordBlock(true);
				}else{
					t.setMultiRecordBlock(false);
				}

				if(t.getLocationName() != null && !t.getLocationName().equalsIgnoreCase(pageName)){

					siblingMrbs.add(t);
				}
			}
		}catch(Exception e){
			logger.error("ERROR fetching sibling multi record blocks for {}", pageName);
			logger.error(e.getMessage(),e);
			throw new DatabaseException("Could not fetch sibling multi record blocks for " + pageName);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		

		return siblingMrbs;
	}

	public ArrayList<Location> hydrateChildMultiRecordBlocks(Connection conn, String moduleCode, int app, String pageName, String parentPageName) throws DatabaseException{

		ArrayList<Location> siblingMrbs = new ArrayList<Location>();
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst =null;
		ResultSet rs = null;

		try{
			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_PAGEAREAS WHERE PA_APP = ? AND PA_FUNC_CODE = ? AND PA_PARENT = ? ORDER BY PA_SEQ_NO");
			pst.setInt(1, app);
			pst.setString(2, moduleCode);
			pst.setString(3, parentPageName);

			rs = pst.executeQuery();
			while(rs.next()){
				Location t = new Location();
				t.setLocationName(rs.getString("PA_NAME"));
				t.setParent(rs.getString("PA_PARENT"));
				t.setSequence(rs.getInt("PA_SEQ_NO"));
				t.setWayIn(rs.getString("PA_WAY_IN"));
				t.setWayOut(rs.getString("PA_WAY_OUT"));
				t.setLocationType(rs.getString("PA_TYPE"));
				String mrb = rs.getString("PA_IS_MRB");
				if(mrb != null && mrb.equalsIgnoreCase("Y")){
					t.setMultiRecordBlock(true);
				}else{
					t.setMultiRecordBlock(false);
				}

				siblingMrbs.add(t);
			}
		}catch(Exception e){
			logger.error("ERROR fetching sibling multi record blocks for {}", pageName);
			logger.error(e.getMessage(),e);
			throw new DatabaseException("Could not fetch sibling multi record blocks for " + pageName);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return siblingMrbs;

	}

	/*modified by shruthi for VAPT helper fixes starts*/
	public ArrayList<Location> hydrateLocations(String modCode, String appName) throws DatabaseException, SQLException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		ArrayList<Location> testObjects = new ArrayList<Location>();
		if(conn != null){
			/*Statement st =null;*/
			PreparedStatement pst = null;

			ResultSet rs=null;
			try{
				/*st = conn.createStatement();

				rs = st.executeQuery("Select * From AUT_FUNC_PAGEAREAS where PA_FUNC_CODE ='" + modCode + "' And PA_APP = '" + appName + "' Order By PA_SEQ_NO");*/
				pst = conn.prepareStatement("Select * From AUT_FUNC_PAGEAREAS where PA_FUNC_CODE =? And PA_APP =? Order By PA_SEQ_NO");
				pst.setString(1, modCode);
				pst.setString(2, appName);
				rs = pst.executeQuery();

				while(rs.next()){
					Location t = new Location();
					t.setLocationName(rs.getString("PA_NAME"));
					t.setParent(rs.getString("PA_PARENT"));
					t.setSequence(rs.getInt("PA_SEQ_NO"));
					t.setWayIn(rs.getString("PA_WAY_IN"));
					t.setWayOut(rs.getString("PA_WAY_OUT"));
					t.setLocationType(rs.getString("PA_TYPE"));
					testObjects.add(t);
				}
				
			}catch(Exception e){
				throw new DatabaseException("Could not fetch location details",e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}else{
			throw new DatabaseException("A valid connection to the database could not be obtained");
		}

		return testObjects;
	}
	/*modified by shruthi for VAPT helper fixes ends*/
	/*modified by shruthi for VAPT helper fixes starts*/
	public ArrayList<Location> hydrateLocations(String modCode, int appId) throws DatabaseException, SQLException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		ArrayList<Location> testObjects = new ArrayList<Location>();
		if(conn != null){
			/*Statement st =null;*/
			PreparedStatement pst = null;
			ResultSet rs=null;
			try{
				/*st = conn.createStatement();

				rs = st.executeQuery("Select A.*, B.APP_NAME From AUT_FUNC_PAGEAREAS A, MASAPPLICATION B where B.APP_ID = A.PA_APP AND A.PA_FUNC_CODE ='" + modCode + "' And A.PA_APP = " + appId + " And coalesce(A.txnmode, 'G') = 'G' Order By A.PA_SEQ_NO");*/
				pst = conn.prepareStatement("Select A.*, B.APP_NAME From AUT_FUNC_PAGEAREAS A, MASAPPLICATION B where B.APP_ID = A.PA_APP AND A.PA_FUNC_CODE =? And A.PA_APP =? And coalesce(A.txnmode, 'G') = 'G' Order By A.PA_SEQ_NO");
				pst.setString(1, modCode);
				pst.setInt(1, appId);
				rs = pst.executeQuery();
				
				while(rs.next()){
					Location t = new Location();
					t.setLocationName(rs.getString("PA_NAME"));
					t.setParent(rs.getString("PA_PARENT"));
					t.setSequence(rs.getInt("PA_SEQ_NO"));
					t.setWayIn(rs.getString("PA_WAY_IN"));
					t.setWayOut(rs.getString("PA_WAY_OUT"));
					t.setLocationType(rs.getString("PA_TYPE"));
					t.setScreenTitle(rs.getString("PA_SCREEN_TITLE"));
					t.setAppName(rs.getString("APP_NAME"));
					t.setModuleCode(rs.getString("PA_FUNC_CODE"));
					t.setLabel(rs.getString("PA_LABEL"));
					testObjects.add(t);
				}
				
			}catch(Exception e){
				throw new DatabaseException("Could not fetch location details",e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}else{
			throw new DatabaseException("A valid connection to the database could not be obtained");
		}

		return testObjects;
	}
	/*modified by shruthi for VAPT helper fixes ends*/


	/***************************
	 * Method added by Sriram for Tuning Test data template generation performance (Tenjin v2.2)
	 */
	/*modified by shruthi for VAPT helper fixes start*/
	public ArrayList<Location> hydrateLocations(Connection conn, String modCode, int appId) throws DatabaseException, SQLException{
		ArrayList<Location> testObjects = new ArrayList<Location>();
		if(conn != null){
			/*Statement st =null;*/
			PreparedStatement pst = null;
			PreparedStatement pst1= null;
			ResultSet rs=null;
			/*Statement s2 =null;*/
			ResultSet rs2 =null;
			try{
				 /*st = conn.createStatement();

				rs = st.executeQuery("SELECT A.*, B.TEMPLATE_RULE FROM AUT_FUNC_PAGEAREAS A LEFT OUTER JOIN AUT_TEMPLATE_RULES B ON(B.APP_ID = A.PA_APP ) AND (B.FUNC_CODE = A.PA_FUNC_CODE)AND (B.PA_NAME = A.PA_NAME )WHERE A.PA_APP =" + appId +  "AND A.PA_FUNC_CODE = '" + modCode + "' ORDER BY A.PA_SEQ_NO");*/
				pst = conn.prepareStatement("SELECT A.*, B.TEMPLATE_RULE FROM AUT_FUNC_PAGEAREAS A LEFT OUTER JOIN AUT_TEMPLATE_RULES B ON(B.APP_ID = A.PA_APP ) AND (B.FUNC_CODE = A.PA_FUNC_CODE)AND (B.PA_NAME = A.PA_NAME )WHERE A.PA_APP =? AND A.PA_FUNC_CODE =? ORDER BY A.PA_SEQ_NO");
				pst.setInt(1, appId);
				pst.setString(2, modCode);
				rs = pst.executeQuery();

				
				while(rs.next()){
					Location t = new Location();
					t.setLocationName(rs.getString("PA_NAME"));
					t.setParent(rs.getString("PA_PARENT"));
					t.setSequence(rs.getInt("PA_SEQ_NO"));
					t.setWayIn(rs.getString("PA_WAY_IN"));
					t.setWayOut(rs.getString("PA_WAY_OUT"));
					t.setLocationType(rs.getString("PA_TYPE"));
					String mrb = rs.getString("PA_IS_MRB");

					if(mrb != null && mrb.equalsIgnoreCase("Y")){
						t.setMultiRecordBlock(true);
					}else{
						t.setMultiRecordBlock(false);
					}

					String ts = rs.getString("TEMPLATE_RULE");
					if(ts != null && !ts.equalsIgnoreCase("")){
						t.setTemplateInclusionRule(rs.getString("TEMPLATE_RULE"));
					}else{
						try{
							/*s2 = conn.createStatement();
							rs2 = s2.executeQuery("SELECT TEMPLATE_RULE FROM AUT_TEMPLATE_RULES WHERE APP_ID = " + appId + " AND FUNC_CODE = '***' AND PA_NAME = '" + t.getLocationName() + "'");*/
							pst1 = conn.prepareStatement("SELECT TEMPLATE_RULE FROM AUT_TEMPLATE_RULES WHERE APP_ID =? AND FUNC_CODE = '***' AND PA_NAME =?");
							pst1.setInt(1, appId);
							pst1.setString(2, t.getLocationName());
							rs2 = pst1.executeQuery();

							while(rs2.next()){
								ts = rs2.getString("TEMPLATE_RULE");
							}
						}catch(Exception e){
							ts = null;
						}finally{
							t.setTemplateInclusionRule(rs.getString("TEMPLATE_RULE"));
							DatabaseHelper.close(rs2);
							DatabaseHelper.close(pst1);
						}
					}
					testObjects.add(t);
				}
				
			}catch(Exception e){
				throw new DatabaseException("Could not fetch location details",e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}
		}else{
			throw new DatabaseException("A valid connection to the database could not be obtained");
		}

		return testObjects;
	}
	/*modified by shruthi for VAPT helper fixes ends*/
	
	/*modified by shruthi for VAPT helper fixes starts*/
	public ArrayList<Location> hydrateLocations(Connection conn, String modCode, int appId, String pageArea) throws DatabaseException{
		ArrayList<Location> testObjects = new ArrayList<Location>();
		if(conn != null){
			/*Statement st =null;*/
			PreparedStatement pst = null;
			ResultSet rs=null;
			try{
				/*st = conn.createStatement();*/
				
				if(pageArea.equalsIgnoreCase("W")){   

					/*rs = st.executeQuery("Select * From AUT_FUNC_PAGEAREAS where PA_NAME in (select distinct a.mode1_pagearea from mapdetail a, mapmaster b where a.map_id = b.map_id and b.app_id = " + appId + " and b.module_code = '" + modCode + "') and PA_FUNC_CODE ='" + modCode + "' And PA_APP = " + appId + " And txnmode = 'G' Order By PA_SEQ_NO"); */
					pst = conn.prepareStatement("Select * From AUT_FUNC_PAGEAREAS where PA_NAME in (select distinct a.mode1_pagearea from mapdetail a, mapmaster b where a.map_id = b.map_id and b.app_id =? and b.module_code =?) and PA_FUNC_CODE =? And PA_APP =? And txnmode = 'G' Order By PA_SEQ_NO");
					pst.setInt(1, appId);
					pst.setString(2, modCode);
					pst.setString(3, modCode);
					pst.setInt(4, appId);

				}
				else{
					/*rs = st.executeQuery("Select * From AUT_FUNC_PAGEAREAS where PA_FUNC_CODE ='" + modCode + "' And PA_APP = " + appId + " AND PA_PARENT = '" + pageArea + "' Order By PA_SEQ_NO");*/
					pst = conn.prepareStatement("Select * From AUT_FUNC_PAGEAREAS where PA_FUNC_CODE =? And PA_APP =? AND PA_PARENT =? Order By PA_SEQ_NO");
					pst.setString(1, modCode);
					pst.setInt(2, appId);
					pst.setString(3, pageArea);
				}
				rs = pst.executeQuery();

				while(rs.next()){
					Location t = new Location();
					t.setLocationName(rs.getString("PA_NAME"));
					t.setParent(rs.getString("PA_PARENT"));
					t.setSequence(rs.getInt("PA_SEQ_NO"));
					t.setWayIn(rs.getString("PA_WAY_IN"));
					t.setWayOut(rs.getString("PA_WAY_OUT"));
					t.setLocationType(rs.getString("PA_TYPE"));
					t.setScreenTitle(rs.getString("PA_SCREEN_TITLE"));
					String mrb = rs.getString("PA_IS_MRB");

					if(mrb != null && mrb.equalsIgnoreCase("Y")){
						t.setMultiRecordBlock(true);
					}else{
						t.setMultiRecordBlock(false);
					}
					testObjects.add(t);
				}
				
			}catch(Exception e){
				throw new DatabaseException("Could not fetch location details",e);
			}finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}

		}else{
			throw new DatabaseException("A valid connection to the database could not be obtained");
		}

		return testObjects;
	}
	/*modified by shruthi for VAPT helper fixes ends*/

	public Map<String, Map<String, Object>> hydrateScreenMetadataAsMap(int appId, String functionCode) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>();

		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_PAGEAREAS WHERE PA_APP=? AND PA_FUNC_CODE=? ORDER BY PA_SEQ_NO");
			pst.setInt(1, appId);
			pst.setString(2, functionCode);
			rs = pst.executeQuery();
			Map<String, Object> locationMap = new TreeMap<String, Object>();
			while(rs.next()){
				Location t = new Location();
				t.setLocationName(rs.getString("PA_NAME"));
				t.setParent(rs.getString("PA_PARENT"));
				t.setSequence(rs.getInt("PA_SEQ_NO"));
				t.setWayIn(rs.getString("PA_WAY_IN"));
				t.setWayOut(rs.getString("PA_WAY_OUT"));
				t.setLocationType(rs.getString("PA_TYPE"));
				String mrb = rs.getString("PA_IS_MRB");
				if(mrb != null && mrb.equalsIgnoreCase("Y")){
					t.setMultiRecordBlock(true);
				}else{
					t.setMultiRecordBlock(false);
				}

				t.setScreenTitle(rs.getString("PA_SCREEN_TITLE"));
				locationMap.put(t.getLocationName(), t);
			}

			map.put("PAGEAREAS", locationMap);

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);

			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE=? ORDER BY FLD_SEQ_NO");
			pst.setInt(1, appId);
			pst.setString(2, functionCode);

			rs = pst.executeQuery();

			Map<String, Object> fieldsMap = new TreeMap<String, Object>();
			while(rs.next()){
				TestObject t = new TestObject();
				t.setLabel(rs.getString("FLD_LABEL"));
				t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
				t.setUniqueId(rs.getString("FLD_UID"));
				t.setObjectClass(rs.getString("FLD_TYPE"));
				t.setLocation(rs.getString("FLD_PAGE_AREA"));
				t.setViewmode(rs.getString("VIEWMODE"));
				t.setMandatory(rs.getString("MANDATORY"));
				t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
				t.setName(rs.getString("FLD_UNAME"));
				fieldsMap.put(t.getName(), t);
			}

			map.put("FIELDS", fieldsMap);
			
		} catch (SQLException e) {
			
			logger.error("ERROR hydrating screen metadata for app {}, function {}", appId, functionCode, e);
			throw new DatabaseException("Could not fetch screen metadata for the specified application function");
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}


		return map;
	}

	/*modified by shruthi for VAPT helper fixes starts*/
	public JSONObject hydrateLocationHierarchyAsJSON(String modCode, int appId) throws DatabaseException{
		Location main = null;
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		JSONObject json = null;
		if(conn != null){
			/*Statement st = null;*/
			PreparedStatement pst = null;

			ResultSet rs=null;
			try{
				/*st = conn.createStatement();

				rs = st.executeQuery("Select * From AUT_FUNC_PAGEAREAS where PA_FUNC_CODE ='" + modCode + "' And PA_APP = " + appId + " AND PA_TYPE='MAIN'");*/
				pst = conn.prepareStatement("Select * From AUT_FUNC_PAGEAREAS where PA_FUNC_CODE =? And PA_APP =? AND PA_TYPE='MAIN'");
				pst.setString(1, modCode);
				pst.setInt(2, appId);
				rs = pst.executeQuery();

				while(rs.next()){
					main = new Location();
					main.setLocationName(rs.getString("PA_NAME"));
					main.setParent(rs.getString("PA_PARENT"));
					main.setSequence(rs.getInt("PA_SEQ_NO"));
					main.setWayIn(rs.getString("PA_WAY_IN"));
					main.setWayOut(rs.getString("PA_WAY_OUT"));
					main.setLocationType(rs.getString("PA_TYPE"));
					main.setScreenTitle(rs.getString("PA_SCREEN_TITLE"));
					String mrb = rs.getString("PA_IS_MRB");

					if(mrb != null && mrb.equalsIgnoreCase("Y")){
						main.setMultiRecordBlock(true);
					}else{
						main.setMultiRecordBlock(false);
					}
				}

				if(main == null){
					throw new DatabaseException("Could not find Landing page for function " + modCode);
				}

				main = scanLocationForChildren(main, conn, modCode, appId);

			}catch(Exception e){
				throw new DatabaseException("Could not fetch location details",e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}

			json = this.pageToJson(main);
		}else{
			throw new DatabaseException("A valid connection to the database could not be obtained");
		}


		return json;
	}
	/*modified by shruthi for VAPT helper fixes ends*/


	private Location scanLocationForChildren(Location location, Connection conn, String modCode, int appId) throws DatabaseException{
		if(conn == null){
			throw new DatabaseException("Invalid Database connection");
		}
		ArrayList<Location> oChildren = null;
		try{
			ArrayList<Location> children = hydrateLocations(conn, modCode, appId, location.getLocationName());
			oChildren = new ArrayList<Location>();
			for(Location child:children){
				Location loc = scanLocationForChildren(child, conn, modCode, appId);
				oChildren.add(loc);
			}


		}catch(Exception e){
			System.out.println("Could not process child page areas under page " + location.getLocationName());
		}

		if(oChildren != null && oChildren.size() > 0){
			location.setChildLocations(oChildren);
		}

		return location;
	}

	public void insertlearnaudittrail(Connection conn, String user_id,String ip,int app_id,List<Module> modules, String status, int runId) throws DatabaseException{
		
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
		PreparedStatement pst =null;
		ResultSet rs = null;
		try{
			for(Module module:modules){
				try{
				int lrnr_id=0;
				String query = "SELECT COALESCE(MAX(LRNR_ID),0) FROM LRNR_AUDIT_TRAIL";
				pst = conn.prepareCall(query);
				rs=pst.executeQuery();
				if(rs.next())
				{
					lrnr_id=rs.getInt(1);
				}
				lrnr_id+=1;
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				
				pst=conn.prepareStatement("INSERT INTO LRNR_AUDIT_TRAIL(LRNR_ID,LRNR_USER_ID,LRNR_IP_ADDRESS,LRNR_APP_ID,LRNR_FUNC_CODE,LRNR_STATUS,LRNR_RUN_ID,LRNR_AUT_USER_TYPE) VALUES(?,?,?,?,?,?,?,?)");
				
				pst.setInt(1,lrnr_id);
				pst.setString(2,user_id);
				pst.setString(3,ip);
				pst.setInt(4,app_id);
				pst.setString(5,module.getCode());
				pst.setString(6, status);
				pst.setInt(7, runId);
				
				pst.setString(8, module.getAutUserType());
				
				pst.execute();
				DatabaseHelper.close(pst);

				LearnerResultBean bean = new LearnerResultBean();
				bean.setId(lrnr_id);
				module.setLastSuccessfulLearningResult(bean);
			}finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}
			}
			
			logger.debug("Clearing Temp Record");
			pst = conn.prepareStatement("DELETE FROM LRNR_AUDIT_TRAIL WHERE LRNR_RUN_ID=? AND LRNR_APP_ID=? AND LRNR_FUNC_CODE=?");
			pst.setInt(1, runId);
			pst.setInt(2, 0);
			pst.setString(3, "TEMP");
			pst.executeUpdate();
			
		}catch(Exception e){
			throw new DatabaseException("Could not insert Learning Information",e);
		}finally{
			DatabaseHelper.close(pst);
		}
		/*Added by Pushpalatha for TJN252-7 starts*/
		/*try {
			conn.commit();
		} catch (SQLException e) {
			
			logger.error("Error ", e);
		}*/
		/*Added by Pushpalatha for TJN252-7 ends*/

	}
	
	public void updateLearningAuditTrailStart(int auditId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		
		try {
			pst = conn.prepareStatement("UPDATE LRNR_AUDIT_TRAIL SET LRNR_START_TIME =?,LRNR_STATUS=? WHERE LRNR_ID =?");
			pst.setInt(3, auditId);
			pst.setString(2, "LEARNING");
			pst.setTimestamp(1, new Timestamp(new Date().getTime()));
			pst.execute();
		} catch (SQLException e) {
			
			logger.error("ERROR updating learning audit for ID [{}]", auditId, e);
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
	
	
	public void updateLearningAuditTrail(List<Module> functions, String status, String message){
		Connection conn = null;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch (DatabaseException e1) {
			
			logger.error("COULD not connect to Database", e1);
		}
		PreparedStatement pst = null;
		
		
		try {
			for(Module function:functions){
				try{
				int auditId = function.getLastSuccessfulLearningResult().getId();
				pst = conn.prepareStatement("UPDATE LRNR_AUDIT_TRAIL SET LRNR_STATUS =?,LRNR_END_TIME=?,LRNR_MESSAGE=? ,LRNR_START_TIME =? WHERE LRNR_ID =?");
				pst.setInt(5, auditId);
				pst.setTimestamp(4, new Timestamp(new Date().getTime()));
				pst.setString(1, status);
				pst.setString(3, message);
				pst.setTimestamp(2, new Timestamp(new Date().getTime()));
				pst.execute();
				}finally{
					DatabaseHelper.close(pst);
				}
			}
		} catch (Exception e) {
			
			logger.error("ERROR updating learning audit for all functions", e);
		} finally{
			
			DatabaseHelper.close(conn);
		}
		
	}
	public void updateLearningAuditTrail(int auditId, String status, String message, int filedCount){
		Connection conn = null;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch (DatabaseException e1) {
			
			logger.error("COULD not connect to Database", e1);
		}
		PreparedStatement pst = null;
		
		try {
			pst = conn.prepareStatement("UPDATE LRNR_AUDIT_TRAIL SET LRNR_STATUS =?,LRNR_END_TIME=?,LRNR_MESSAGE=?,FIELDS_LEARNT_COUNT=? WHERE LRNR_ID =?");
			pst.setInt(5, auditId);
			pst.setString(1, status);
			pst.setString(3, message);
			pst.setTimestamp(2, new Timestamp(new Date().getTime()));
			pst.setInt(4, filedCount);
			pst.execute();
		} catch (SQLException e) {
			
			logger.error("ERROR updating learning audit for ID [{}]", auditId, e);
		} finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
	
	public TestObject hydrateTestObjectForExecution(String moduleCode, String page, int app, String label){
		TestObject t = null;

		try{
			t = hydrateTestObject(moduleCode,page,app,label);
			if(t == null){
				t = hydrateTestObject(moduleCode,app,label);
			}

			if(t == null){
				t = hydrateTestObjectOnPage(page, app, label);
			}
		}catch(Exception e){
			t = null;
		}

		return t;
	}

	
	static String xml = "";
	static JSONObject pageAreas;
	


	public static void generateNode(Location location){
		xml = xml + "<Location name='" + location.getLocationName() + "' type='" + location.getLocationType() + "' wayin='" + location.getWayIn() + "'>";
		if(location.getChildLocations() != null && location.getChildLocations().size() > 0){
			for(Location loc:location.getChildLocations()){
				generateNode(loc);
			}
			xml = xml + "</Location>";
		}else{
			xml = xml + "</Location>";
		}
	}

	private JSONObject pageToJson(Location page){
		JSONObject j = new JSONObject();

		try{
			j.put("PA_NAME", page.getLocationName());
			j.put("PA_TYPE", page.getLocationType());
			j.put("PA_WAY_IN", page.getWayIn());
			j.put("PA_WAY_OUT", page.getWayOut());
			j.put("PA_PARENT", page.getParent());
			j.put("PA_SCREEN_TITLE", page.getScreenTitle());
			if(page.isMultiRecordBlock()){
				j.put("PA_IS_MRB", "YES");
			}else{
				j.put("PA_IS_MRB", "NO");
			}

			JSONArray children = getChildPageJSONArray(j, page);
			j.put("CHILDREN", children);
			return j;
		}

		catch(JSONException e){
			logger.error("Could not convert page to JSON",e);
			return null;
		}

	}

	public static JSONArray getChildPageJSONArray(JSONObject j, Location page){
		JSONArray cJson = new JSONArray();
		try{
			List<Location> children = page.getChildLocations();
			if(children != null){
				for(Location child:children){
					JSONObject c = new JSONObject();
					if(child.getLocationName().equalsIgnoreCase("Document List")){
						System.err.println();
					}
					c.put("PA_NAME", child.getLocationName());
					c.put("PA_TYPE", child.getLocationType());
					c.put("PA_WAY_IN", child.getWayIn());
					c.put("PA_WAY_OUT", child.getWayOut());
					c.put("PA_PARENT", child.getParent());
					c.put("PA_SCREEN_TITLE", child.getScreenTitle());
					if(child.isMultiRecordBlock()){
						c.put("PA_IS_MRB", "YES");
					}else{
						c.put("PA_IS_MRB", "NO");
					}

					JSONArray cChildren = getChildPageJSONArray(c, child);
					c.put("CHILDREN", cChildren);

					cJson.put(c);
				}
			}
		}catch(JSONException e){
			logger.error("ERROR generating JSON for child pages of {}", page.getLocationName());
		}

		return cJson;
	}

	public List<Location> hydrateMetadata(int appId, String functionCode, boolean mandatoryOnly, boolean editableOnly) throws DatabaseException{

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		List<Location> locs = new ArrayList<Location>();

		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement pst2 = null;
		ResultSet rs2 = null;
		
		
		String fieldQuery = "SELECT A.*, B.APP_NAME, C.FLD_USER_REMARKS FROM MASAPPLICATION B, AUT_FUNC_FIELDS A LEFT JOIN AUT_FUNC_FIELD_INFO C ON "
				+ "A.FLD_APP = C.APP_ID AND A.FLD_FUNC_CODE=C.FUNC_CODE AND A.FLD_PAGE_AREA = C.PA_NAME AND A.FLD_UNAME = C.FLD_UNAME " + 
				 " WHERE B.APP_ID=A.FLD_APP AND A.FLD_APP=? AND A.FLD_FUNC_CODE=? AND A.FLD_PAGE_AREA=?";
		if(mandatoryOnly){
			fieldQuery = fieldQuery + " and A.MANDATORY='YES'";
		}

		if(editableOnly){
			fieldQuery = fieldQuery + " and A.FLD_IS_ENABLED='Y'";
		}

		fieldQuery = fieldQuery + " ORDER BY A.FLD_SEQ_NO";
		try{
			pst = conn.prepareStatement("SELECT A.*, B.APP_NAME FROM AUT_FUNC_PAGEAREAS A, MASAPPLICATION B WHERE B.APP_ID=A.PA_APP AND A.PA_APP=? AND A.PA_FUNC_CODE=? ORDER BY A.PA_SEQ_NO");
			pst.setInt(1,appId);
			pst.setString(2, functionCode);

			rs = pst.executeQuery();

			while(rs.next()){
				Location location = new Location();
				location.setLocationName(rs.getString("PA_NAME"));
				location.setParent(rs.getString("PA_PARENT"));
				location.setSequence(rs.getInt("PA_SEQ_NO"));
				location.setWayIn(rs.getString("PA_WAY_IN"));
				location.setWayOut(rs.getString("PA_WAY_OUT"));
				location.setLocationType(rs.getString("PA_TYPE"));
				if(Utilities.trim(rs.getString("PA_IS_MRB")).equalsIgnoreCase("Y")){
					location.setMultiRecordBlock(true);
				}else{
					location.setMultiRecordBlock(false);
				}
				
				location.setLabel(rs.getString("PA_LABEL"));
				location.setScreenTitle(rs.getString("PA_SCREEN_TITLE"));
				location.setAppName(rs.getString("APP_NAME"));
				location.setModuleCode(rs.getString("PA_FUNC_CODE"));

				try{
					pst2 = conn.prepareStatement(fieldQuery);
					pst2.setInt(1, appId);
					pst2.setString(2, functionCode);
					pst2.setString(3, location.getLocationName());

					rs2 = pst2.executeQuery();

					List<TestObject> fields = new ArrayList<TestObject>();

					while(rs2.next()){
						TestObject t = new TestObject();
						t.setLabel(rs2.getString("FLD_LABEL"));
						t.setIdentifiedBy(rs2.getString("FLD_UID_TYPE"));
						t.setUniqueId(rs2.getString("FLD_UID"));
						t.setObjectClass(rs2.getString("FLD_TYPE"));
						t.setLocation(rs2.getString("FLD_PAGE_AREA"));
						t.setViewmode(rs2.getString("VIEWMODE"));
						t.setMandatory(rs2.getString("MANDATORY"));
						t.setLovAvailable(rs2.getString("FLD_HAS_LOV"));
						t.setName(rs2.getString("FLD_UNAME"));
						t.setGroup(rs2.getString("FLD_GROUP"));
						t.setAutoLovAvailable(rs2.getString("FLD_HAS_AUTOLOV"));
						t.setIsMultiRecord(rs2.getString("FLD_IS_MULTI_REC"));
						t.setDefaultOptions(rs2.getString("FLD_DEFAULT_OPTIONS"));
						
						t.setAppId(rs2.getInt("FLD_APP"));
						t.setAppName(rs2.getString("APP_NAME"));
						t.setModuleCode(rs2.getString("FLD_FUNC_CODE"));
						
						t.setUserRemarks(rs2.getString("FLD_USER_REMARKS"));
						fields.add(t);
					}


					location.setTestObjects(fields);
				}catch(Exception e){
					logger.error("ERROR occurred while fetching fields under page {}", location.getLocationName(), e);
				}finally{
					DatabaseHelper.close(rs2);
					DatabaseHelper.close(pst2);
				}


				locs.add(location);
			}
		}catch(SQLException e){
			logger.error("ERROR fetching screen metadata", e);
			throw new DatabaseException("Could not fetch metadata due to an internal error. Please contact Tenjin Support");
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return locs;

	}
	
	public List<Location> hydrateMetadata(Connection conn, int appId, String functionCode, boolean mandatoryOnly, boolean editableOnly) throws DatabaseException{


		List<Location> locs = new ArrayList<Location>();

		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement pst2 = null;
		ResultSet rs2 = null;
		
		
		String fieldQuery = "SELECT A.*, B.APP_NAME FROM AUT_FUNC_FIELDS A,MASAPPLICATION B WHERE B.APP_ID=A.FLD_APP AND A.FLD_APP=? AND A.FLD_FUNC_CODE=? AND A.FLD_PAGE_AREA=?";
		if(mandatoryOnly){
			fieldQuery = fieldQuery + " and A.MANDATORY='YES'";
		}

		if(editableOnly){
			fieldQuery = fieldQuery + " and A.FLD_IS_ENABLED='Y'";
		}

		fieldQuery = fieldQuery + " ORDER BY A.FLD_SEQ_NO";
		try{
			pst = conn.prepareStatement("SELECT A.*, B.APP_NAME FROM AUT_FUNC_PAGEAREAS A, MASAPPLICATION B WHERE B.APP_ID=A.PA_APP AND A.PA_APP=? AND A.PA_FUNC_CODE=? ORDER BY A.PA_SEQ_NO");
			pst.setInt(1,appId);
			pst.setString(2, functionCode);
			rs = pst.executeQuery();

			while(rs.next()){
				Location location = new Location();
				location.setLocationName(rs.getString("PA_NAME"));
				location.setParent(rs.getString("PA_PARENT"));
				location.setSequence(rs.getInt("PA_SEQ_NO"));
				location.setWayIn(rs.getString("PA_WAY_IN"));
				location.setWayOut(rs.getString("PA_WAY_OUT"));
				location.setLocationType(rs.getString("PA_TYPE"));
				if(Utilities.trim(rs.getString("PA_IS_MRB")).equalsIgnoreCase("Y")){
					location.setMultiRecordBlock(true);
				}else{
					location.setMultiRecordBlock(false);
				}
				
				location.setLabel(rs.getString("PA_LABEL"));
				location.setScreenTitle(rs.getString("PA_SCREEN_TITLE"));
				location.setAppName(rs.getString("APP_NAME"));
				location.setModuleCode(rs.getString("PA_FUNC_CODE"));

				try{
					pst2 = conn.prepareStatement(fieldQuery);
					pst2.setInt(1, appId);
					pst2.setString(2, functionCode);
					pst2.setString(3, location.getLocationName());

					rs2 = pst2.executeQuery();

					List<TestObject> fields = new ArrayList<TestObject>();

					while(rs2.next()){
						TestObject t = new TestObject();
						t.setLabel(rs2.getString("FLD_LABEL"));
						t.setIdentifiedBy(rs2.getString("FLD_UID_TYPE"));
						t.setUniqueId(rs2.getString("FLD_UID"));
						t.setObjectClass(rs2.getString("FLD_TYPE"));
						t.setLocation(rs2.getString("FLD_PAGE_AREA"));
						t.setViewmode(rs2.getString("VIEWMODE"));
						t.setMandatory(rs2.getString("MANDATORY"));
						t.setLovAvailable(rs2.getString("FLD_HAS_LOV"));
						t.setName(rs2.getString("FLD_UNAME"));
						t.setGroup(rs2.getString("FLD_GROUP"));
						t.setAutoLovAvailable(rs2.getString("FLD_HAS_AUTOLOV"));
						t.setIsMultiRecord(rs2.getString("FLD_IS_MULTI_REC"));
						t.setDefaultOptions(rs2.getString("FLD_DEFAULT_OPTIONS"));
						
						t.setAppId(rs2.getInt("FLD_APP"));
						t.setAppName(rs2.getString("APP_NAME"));
						t.setModuleCode(rs2.getString("FLD_FUNC_CODE"));
						fields.add(t);
					}


					location.setTestObjects(fields);
				}catch(Exception e){
					logger.error("ERROR occurred while fetching fields under page {}", location.getLocationName(), e);
				}finally{
					
					DatabaseHelper.close(rs2);
					DatabaseHelper.close(pst2);
				}


				locs.add(location);
			}
		}catch(SQLException e){
			logger.error("ERROR fetching screen metadata", e);
			throw new DatabaseException("Could not fetch metadata due to an internal error. Please contact Tenjin Support");
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return locs;

	}

	
	
	public String getRuntimeFieldOutput(Connection conn, String tdUid, String fieldIdentifier) throws DatabaseException{
		String output = null;
		boolean outputFound = false;
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
		PreparedStatement pst1 = null;
		ResultSet rs1 =null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		try{
			pst1 = conn.prepareStatement("SELECT coalesce(MAX(RUN_ID),0) AS RUN_ID FROM RUNRESULT_TS_TXN WHERE TSTEP_TDUID = ?");
			pst1.setString(1, tdUid);
			rs1 = pst1.executeQuery();
			int runId = -1;
			while(rs1.next()){
				runId = rs1.getInt("RUN_ID");
			}

			if(runId < 1){
				throw new DatabaseException("Transaction with master record " + tdUid + " has not been executed");
			}
			
			pst = conn.prepareStatement("SELECT TSTEP_RESULT FROM RUNRESULT_TS_TXN WHERE RUN_ID = ? AND TSTEP_TDUID=?");
			pst.setInt(1, runId);
			pst.setString(2, tdUid);
			rs = pst.executeQuery();
			String res = "";
			while(rs.next()){
				res = rs.getString("TSTEP_RESULT");
				break;
			}
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			//VAPT fix null check
			if(res == null || !res.equalsIgnoreCase("S")){
				logger.error("Test Step with TDUID {} has either not been executed or has not completed successfully",tdUid);
				throw new DatabaseException("The test step with TDUID " + tdUid + " has either not been executed or has not completed successfully");
			}

			pst = conn.prepareStatement("SELECT FIELD_VALUE FROM TJN_EXEC_OUTPUT WHERE TDUID = ? AND RUN_ID = ? AND FIELD_NAME=?");
			pst.setString(1, tdUid);
			pst.setInt(2, runId);
			pst.setString(3, fieldIdentifier);
			rs = pst.executeQuery();
			
			while(rs.next()){
				output = rs.getString("FIELD_VALUE");
				outputFound = true;
			}
			
		}catch(Exception e){
			throw new DatabaseException("Could not fetch Output for Master Record " + tdUid,e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
		}
		
		if(outputFound){
			return output;
		}else{
			throw new DatabaseException("Could not find runtime value for field " + fieldIdentifier + " in test step with TDUID " + tdUid + ".");
		}
	}
	
	
	
	public Map<String,String> getManualInput(int appId, String moduleCode, String locName,int testCaseId,String testStepId) throws DatabaseException {
		

		Map<String,String> manualMappedfields=new HashMap<String,String>();  
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		ResultSet rs =null;PreparedStatement pst=null;
		int count=0;
		try{				
			 pst = conn.prepareStatement("SELECT MANUAL_FIELD_LABEL FROM MANUAL_FIELD_MAP WHERE APP_ID=? AND FUNCTION_NAME=? AND TEST_STEP_ID=? AND PAGE_AREA=? AND TEST_CASE_ID=?", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, testStepId);
			pst.setString(4, locName);	
			pst.setInt(5, testCaseId);
			
			 rs = pst.executeQuery();	
			 if(rs.last())
				{
					count=rs.getRow();
					rs.beforeFirst();
				}
				if(count>0){
					while(rs.next()){
					String maual=rs.getString("MANUAL_FIELD_LABEL");
					manualMappedfields.put(maual,maual);
					}
				}
				
		}catch(Exception e){
			throw new DatabaseException(e.getMessage(),e);
		}
		finally{
		DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
				
			
		}
		
	return 	manualMappedfields;	
	
	}

	@SuppressWarnings("unused")
	public void persistMetadata(Metadata metadata, int applicationId, String functionCode, String operation, String entityType,int respCode) throws DatabaseException {
		this.respCode=respCode;
		logger.info("Processing metadata");
		int totalFieldCount = 0;

		for(TreeMap<String,TestObject> map:metadata.getTestObjectMap()){
			for(String key:map.keySet()){
				totalFieldCount++;
			}
			totalFieldCount+=map.keySet().size();
		}

		int totalPageAreas = 0;
		for(String key:metadata.getPageAreas().keySet()){
			for(Location l:metadata.getPageAreas().get(key)){
				totalPageAreas ++;

			}
			totalPageAreas+=metadata.getPageAreas().get(key).size();
		}
		logger.info("Total page areas to be processed --> {}", totalPageAreas);
		logger.info("Total fields to be processed --> {}", totalFieldCount);

		logger.info("Clearing existing learning information for function {}", functionCode);

		try {
			this.clearLearningData(functionCode, applicationId, operation, entityType);
		} catch (Exception e) {
			
			logger.error("ERROR occurred while clearing existing learning data", e);
			throw new DatabaseException("Could not clear existing metadata from the database for this function");
		}

		logger.info("Persisting locations");
		try{
			Multimap<String, Location> failed = this.persistLocations(metadata.getPageAreas(), functionCode, applicationId, operation, entityType);
			int totalFailed = 0;
			for(String key:failed.keySet()){
				totalFailed+=failed.get(key).size();
			}

			logger.info("Successfully updated " + (totalPageAreas - totalFailed) + " page areas to the database out of " + totalPageAreas);
		}catch(Exception e){
			logger.error("ERROR occurred while persisting page areas", e);
			throw new DatabaseException("Could not insert page areas to database");
		}

		logger.info("Persisting fields");

		try{
			Multimap<String, TestObject> failed = this.persistFieldsInWorkTable(metadata.getTestObjectMap(), functionCode, applicationId, operation, entityType);
			int totalFailed = 0;
			for(String key:failed.keySet()){
				totalFailed+=failed.get(key).size();
			}
			logger.info("Successfully added " + (totalFieldCount - totalFailed) + " Test Objects out of " + totalFieldCount);
		}catch(Exception e){
			logger.error("ERROR inserting fields to work table", e);
			throw new DatabaseException("Could not insert learnt fields to the database");
		}

		logger.info("Processing persisted fields");
		try{
			this.processPersistedFields(functionCode, applicationId, operation, entityType,0);
			logger.info("Successfully processed learnt fields");
		}catch(Exception e){
			logger.error("ERROR occurred while processing fields from work table", e);
			throw new DatabaseException("Could not insert learnt fields to the database",e);
		}
	}
	
	
	public void insertApiLearningAuditTrail(Connection conn, int appId, Api api, int runId, String userId, String status) throws DatabaseException{
		PreparedStatement pst = null;
		
		if(api != null && api.getOperations() != null && api.getOperations().size() > 0) {
			PreparedStatement sPst =null;
			ResultSet sRs =null;
			try{
				
				//conn.setAutoCommit(false);
			
				for(ApiOperation op:api.getOperations()) {
					try{
					sPst = conn.prepareStatement("SELECT COALESCE(MAX(LRNR_ID),0) as lrnr_id FROM LRNR_AUDIT_TRAIL_API");
					sRs = sPst.executeQuery();
					
					int seq=0;
					while(sRs.next()) {
						seq = sRs.getInt("LRNR_ID");
					}
					
					seq++;
					
					pst = conn.prepareStatement("INSERT INTO LRNR_AUDIT_TRAIL_API (LRNR_ID,LRNR_USER_ID,LRNR_APP_ID,LRNR_API_CODE,LRNR_OPERATION,LRNR_STATUS, LRNR_RUN_ID) "
							+ "values (?,?,?,?,?,?,?)");
					
					pst.setInt(1, seq);
					pst.setString(2, userId);
					pst.setInt(3, appId);
					pst.setString(4, api.getCode());
					pst.setString(5, op.getName());
					pst.setString(6, status);
					pst.setInt(7, runId);
					pst.execute();
					
					ApiLearnerResultBean bean = new ApiLearnerResultBean();
					bean.setId(seq);
					op.setLastSuccessfulLearningResult(bean);
					}finally{
						DatabaseHelper.close(sRs);
						DatabaseHelper.close(sPst);
						DatabaseHelper.close(pst);
					}
				}
				
				//conn.commit();
			} catch(SQLException e) {
				logger.error("ERROR inserting audit recods for API Learning run {}", runId, e);
				throw new DatabaseException("Could not insert audit records");
			} finally{
				DatabaseHelper.close(sRs);
				DatabaseHelper.close(sPst);
				DatabaseHelper.close(pst);
				
			}
			
		}
	}
	
	public void updateLearningAuditTrail(String status, String message, List<ApiOperation> operations) {
		Connection conn;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		} catch (DatabaseException e1) {
			
			logger.error("Unable to get DB Connection", e1);
			return;
		}
		
		PreparedStatement pst = null;
		
		try {
			//conn.setAutoCommit(false);
			
			pst = conn.prepareStatement("UPDATE LRNR_AUDIT_TRAIL_API SET LRNR_STATUS =?,LRNR_END_TIME=?,LRNR_MESSAGE=? ,LRNR_START_TIME =? WHERE LRNR_ID =?");
			
			for(ApiOperation op:operations) {
				int auditId = op.getLastSuccessfulLearningResult().getId();
				pst.setInt(5, auditId);
				pst.setTimestamp(4, new Timestamp(new Date().getTime()));
				pst.setString(1, status);
				pst.setString(3, message);
				pst.setTimestamp(2, new Timestamp(new Date().getTime()));
				pst.addBatch();
			}
			
			pst.executeBatch();
			
			//conn.commit();
			
		} catch (SQLException e) {
			
			logger.error("ERROR updating learning audit trail", e);
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	
	public void updateLearningAuditTrailStart(ApiOperation operation) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		
		try {
			pst = conn.prepareStatement("UPDATE LRNR_AUDIT_TRAIL_API SET LRNR_START_TIME =?,LRNR_STATUS=? WHERE LRNR_ID =?");
			pst.setInt(3, operation.getLastSuccessfulLearningResult().getId());
			pst.setString(2, "LEARNING");
			pst.setTimestamp(1, new Timestamp(new Date().getTime()));
			pst.execute();
		} catch (SQLException e) {
			
			logger.error("ERROR updating learning audit for ID [{}]", operation.getLastSuccessfulLearningResult().getId(), e);
		} finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
	
	public void updateLearningAuditTrailEnd(String status, String message, ApiOperation operation, int fieldCount) {
		Connection conn;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		} catch (DatabaseException e1) {
			
			logger.error("Unable to get DB Connection", e1);
			return;
		}
		
		PreparedStatement pst = null;
		
		
		
		try {
			//conn.setAutoCommit(false);
			pst = conn.prepareStatement("UPDATE LRNR_AUDIT_TRAIL_API SET LRNR_STATUS =?,LRNR_END_TIME=?,LRNR_MESSAGE=?,FIELDS_LEARNT_COUNT=?  WHERE LRNR_ID =?");
			int auditId = operation.getLastSuccessfulLearningResult().getId();
			pst.setInt(5, auditId);
			pst.setString(1, status);
			pst.setString(3, message);
			pst.setTimestamp(2, new Timestamp(new Date().getTime()));
			pst.setInt(4, fieldCount);
			pst.executeUpdate();

			//conn.commit();
		} catch (SQLException e) {
			
			logger.error("ERROR updating learning audit trail", e);
		} finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	
	public List<Location> hydrateMetadata(int appId, String functionCode, String operation, String entityType, boolean mandatoryOnly) throws DatabaseException{

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		List<Location> locs = new ArrayList<Location>();

		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement pst2 = null;
		ResultSet rs2 = null;
		
		String fieldQuery = "SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE=? AND FLD_API_OPERATION=? AND FLD_API_ENTITY_TYPE=? AND FLD_PAGE_AREA=? AND FLD_API_RESP_TYPE=?";
		if(mandatoryOnly){
			fieldQuery = fieldQuery + " and MANDATORY='YES'";
		}

		fieldQuery = fieldQuery + " ORDER BY FLD_SEQ_NO";
		try{
			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_PAGEAREAS WHERE PA_APP=? AND PA_FUNC_CODE=? AND PA_API_OPERATION=? AND PA_API_ENTITY_TYPE=? ORDER BY PA_SEQ_NO");
			pst.setInt(1,appId);
			pst.setString(2, functionCode);
			pst.setString(3, operation);
			pst.setString(4, entityType.toUpperCase());

			rs = pst.executeQuery();

			while(rs.next()){
				Location location = new Location();
				location.setLocationName(rs.getString("PA_NAME"));
				location.setParent(rs.getString("PA_PARENT"));
				location.setSequence(rs.getInt("PA_SEQ_NO"));
				location.setWayIn(rs.getString("PA_WAY_IN"));
				location.setWayOut(rs.getString("PA_WAY_OUT"));
				location.setLocationType(rs.getString("PA_TYPE"));
				if(Utilities.trim(rs.getString("PA_IS_MRB")).equalsIgnoreCase("Y")){
					location.setMultiRecordBlock(true);
				}else{
					location.setMultiRecordBlock(false);
				}
				location.setResponseType(rs.getInt("PA_API_RESP_TYPE"));
				try{
					pst2 = conn.prepareStatement(fieldQuery);
					pst2.setInt(1, appId);
					pst2.setString(2, functionCode);
					pst2.setString(3, operation);
					pst2.setString(4, entityType.toUpperCase());
					pst2.setString(5, location.getLocationName());

					pst2.setInt(6, location.getResponseType());
					
					
					rs2 = pst2.executeQuery();

					List<TestObject> fields = new ArrayList<TestObject>();

					while(rs2.next()){
						TestObject t = new TestObject();
						t.setLabel(rs2.getString("FLD_LABEL"));
						t.setIdentifiedBy(rs2.getString("FLD_UID_TYPE"));
						t.setUniqueId(rs2.getString("FLD_UID"));
						t.setObjectClass(rs2.getString("FLD_TYPE"));
						t.setLocation(rs2.getString("FLD_PAGE_AREA"));
						t.setViewmode(rs2.getString("VIEWMODE"));
						t.setMandatory(rs2.getString("MANDATORY"));
						t.setLovAvailable(rs2.getString("FLD_HAS_LOV"));
						t.setName(rs2.getString("FLD_UNAME"));
						t.setGroup(rs2.getString("FLD_GROUP"));
						t.setAutoLovAvailable(rs2.getString("FLD_HAS_AUTOLOV"));
						t.setIsMultiRecord(rs2.getString("FLD_IS_MULTI_REC"));
						t.setDefaultOptions(rs2.getString("FLD_DEFAULT_OPTIONS"));
						fields.add(t);
					}


					location.setTestObjects(fields);
				}catch(Exception e){
					logger.error("ERROR occurred while fetching fields under page {}", location.getLocationName(), e);
				}finally{
					DatabaseHelper.close(rs2);
					DatabaseHelper.close(pst2);
				}

				locs.add(location);
			}
		}catch(SQLException e){
			logger.error("ERROR fetching screen metadata", e);
			throw new DatabaseException("Could not fetch metadata due to an internal error. Please contact Tenjin Support");
		}finally{
		DatabaseHelper.close(rs);
		DatabaseHelper.close(pst);
		DatabaseHelper.close(conn);
	}

		return locs;

	}
	/*modified by shruthi for VAPT helper fixes starts*/
	public Location hydrateLocation(String moduleCode, int app,String pageName, String apiOperation, String entityType) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);	
		
		entityType = Utilities.trim(entityType).toUpperCase();
		
		Location t = new Location();
		if(conn != null){
			/*Statement st =null;*/
			PreparedStatement pst = null;
			ResultSet rs=null;
			try{
			 /* st = conn.createStatement();

				rs = st.executeQuery("Select * From AUT_FUNC_PAGEAREAS where PA_NAME = '" + pageName + "' And PA_FUNC_CODE='" + moduleCode + "' and PA_API_OPERATION='" + apiOperation + "' AND PA_API_ENTITY_TYPE='" + entityType + "' and PA_APP = " + app);*/
				pst = conn.prepareStatement("Select * From AUT_FUNC_PAGEAREAS where PA_NAME =? And PA_FUNC_CODE=? and PA_API_OPERATION=? AND PA_API_ENTITY_TYPE=? and PA_APP =?");
				pst.setString(1, pageName);
				pst.setString(2, moduleCode);
				pst.setString(3, apiOperation);
				pst.setString(4, entityType);
				pst.setInt(5, app);
				rs = pst.executeQuery();

				while(rs.next()){

					t.setLocationName(rs.getString("PA_NAME"));
					t.setParent(rs.getString("PA_PARENT"));
					t.setSequence(rs.getInt("PA_SEQ_NO"));
					t.setWayIn(rs.getString("PA_WAY_IN"));
					t.setWayOut(rs.getString("PA_WAY_OUT"));
					t.setLocationType(rs.getString("PA_TYPE"));
					t.setPath(rs.getString("PA_PATH"));
					 String mrb = rs.getString("PA_IS_MRB");
                     if(mrb != null && mrb.equalsIgnoreCase("Y")){
                            t.setMultiRecordBlock(true);
                     }else{
                            t.setMultiRecordBlock(false);
                     }
                    
				}
				
			}catch(Exception e){
				throw new DatabaseException("Could not fetch location details",e);
			}finally{
				
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}

		}else{
			throw new DatabaseException("A valid connection to the database could not be obtained");
		}

		return t;
	}
	/*modified by shruthi for VAPT helper fixes ends*/

	public TestObject hydrateTestObject(Connection conn, String moduleCode, String page, int appId, String label, String apiOperation, String entityType) throws DatabaseException, SQLException{
		TestObject t= null;

		entityType = Utilities.trim(entityType).toUpperCase();
		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}
		PreparedStatement pst =null;
		ResultSet rs = null;
		try{
		
			 pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE = ? AND FLD_UNAME = ? AND FLD_PAGE_AREA = ? AND FLD_API_OPERATION=? AND FLD_API_ENTITY_TYPE=?");
			
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, label);
			pst.setString(4, page);
			pst.setString(5, apiOperation);
			pst.setString(6, entityType);
			rs = pst.executeQuery();

			while(rs.next()){
				t = new TestObject();
				t.setLabel(rs.getString("FLD_LABEL"));
				t.setName(rs.getString("FLD_UNAME"));
				t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
				t.setUniqueId(rs.getString("FLD_UID"));
				t.setObjectClass(rs.getString("FLD_TYPE"));
				t.setLocation(rs.getString("FLD_PAGE_AREA"));
				t.setViewmode(rs.getString("VIEWMODE"));
				t.setMandatory(rs.getString("MANDATORY"));
				t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
				t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
				t.setGroup(rs.getString("FLD_GROUP"));
				t.setIsMultiRecord(rs.getString("FLD_IS_MULTI_REC"));
				
			}
			
		}catch(Exception e){
			throw new DatabaseException("Could not fetch field information for field " + label,e);
		}
		finally{
			DatabaseHelper.close(pst);
		}

		return t;
	}
	
	public ArrayList<Location> hydrateChildMultiRecordBlocks(Connection conn, String moduleCode, int app, String pageName, String parentPageName, String apiOperation, String entityType) throws DatabaseException{

		ArrayList<Location> siblingMrbs = new ArrayList<Location>();
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst =null;
		ResultSet rs = null;
		try{
			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_PAGEAREAS WHERE PA_APP = ? AND PA_FUNC_CODE = ? AND PA_PARENT = ? AND PA_API_OPERATION=? AND PA_API_ENTITY_TYPE=? ORDER BY PA_SEQ_NO");
			pst.setInt(1, app);
			pst.setString(2, moduleCode);
			pst.setString(3, parentPageName);
			pst.setString(4, apiOperation);
			pst.setString(5, Utilities.trim(entityType).toUpperCase());

			rs = pst.executeQuery();
			while(rs.next()){
				Location t = new Location();
				t.setLocationName(rs.getString("PA_NAME"));
				t.setParent(rs.getString("PA_PARENT"));
				t.setSequence(rs.getInt("PA_SEQ_NO"));
				t.setWayIn(rs.getString("PA_WAY_IN"));
				t.setWayOut(rs.getString("PA_WAY_OUT"));
				t.setLocationType(rs.getString("PA_TYPE"));
				String mrb = rs.getString("PA_IS_MRB");
				if(mrb != null && mrb.equalsIgnoreCase("Y")){
					t.setMultiRecordBlock(true);
				}else{
					t.setMultiRecordBlock(false);
				}

				siblingMrbs.add(t);
			}
		}catch(Exception e){
			logger.error("ERROR fetching sibling multi record blocks for {}", pageName);
			logger.error(e.getMessage(),e);
			throw new DatabaseException("Could not fetch sibling multi record blocks for " + pageName);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return siblingMrbs;
	}

	public String fetchDateFormat(int appId, String moduleCode)
			throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		String dateFormat = "";
		/*Modified by Ashiki for VAPT Fix starts*/ 
		//Statement st=null;
		ResultSet rs=null;
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		if (conn != null) {
		
			try {
				//st = conn.createStatement();


				pst = conn.prepareStatement("SELECT * FROM MASMODULE A LEFT JOIN LRNR_FUNC_ACTIONS B ON (A.MODULE_CODE = B.FUNC_CODE) WHERE A.MODULE_CODE =? AND A.APP_ID = ?");
				pst.setString(1, moduleCode);
				pst.setInt(2,appId);
				rs=pst.executeQuery();
				
				while (rs.next()) {

					dateFormat = rs.getString("APP_DATE_FORMAT");
				}
				DatabaseHelper.close(rs);
				
				if(dateFormat==null){
					pst1 = conn.prepareStatement("SELECT APP_DATE_FORMAT FROM MASAPPLICATION WHERE APP_ID =?");
					pst1.setInt(1,appId);
					rs = pst1.executeQuery();

					while (rs.next()) {
						dateFormat = rs.getString("APP_DATE_FORMAT");
					}
				}
				
			} catch (Exception e) {

				throw new DatabaseException("Could not hydrate module "
						+ moduleCode + " for application " + appId, e);
			} finally {
				DatabaseHelper.close(pst);
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst1);
				//DatabaseHelper.close(st);
				/*Modified by Ashiki for VAPT Fix ends*/ 
				DatabaseHelper.close(conn);
				
			}
		} else {

			throw new DatabaseException("Invalid database connection");
		}

		return dateFormat;

	}
	

	public void unMapAllWaitTimeField(int appId, String moduleCode) throws DatabaseException {
		
		 Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
			
			if(conn == null){
				throw new DatabaseException("Invalid or no database connection");
			}
			
			PreparedStatement pst=null;
			
			try{				
				
				pst = conn.prepareStatement("DELETE FROM WAIT_TIME_FIELD_MAP WHERE APP_ID=? AND FUNCTION_CODE=?");
				
				pst.setInt(1, appId);
				pst.setString(2, moduleCode);
				pst.execute();
			}catch(Exception e){
				
			}
			finally{
				
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
	}
	
	
	public void clearLocations(Connection conn, String modCode, int appId)throws DatabaseException, SQLException{
		PreparedStatement pst2 =null;
		try{

			pst2 = conn.prepareStatement("DELETE FROM AUT_FUNC_PAGEAREAS WHERE PA_APP = ? AND PA_FUNC_CODE = ? AND TXNMODE = 'G'");

			pst2.setInt(1, appId);
			pst2.setString(2, modCode);
			pst2.execute();
		}catch(Exception e){
			throw new DatabaseException("Could not clear existing learning information",e);
		}
		finally{
			DatabaseHelper.close(pst2);
		}
	}
	
	public void clearTestObjects(Connection conn, String modCode, int appId)throws DatabaseException, SQLException{
		PreparedStatement pst =null;
		try{
			pst = conn.prepareStatement("DELETE FROM AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ? AND TXNMODE = 'G'");
			pst.setInt(1, appId);
			pst.setString(2, modCode);
			pst.execute();

			
		}catch(Exception e){
			throw new DatabaseException("Could not clear existing learning information",e);
		}
		finally{
			DatabaseHelper.close(pst);
		}
	}
	
	public void persistMetadataWithoutProcessing(int appId, String functionCode, List<Location> locations, List<TestObject> testObjects) throws DatabaseException {
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst= null;
		
		try {
			//conn.setAutoCommit(false);
			
			if(locations != null && locations.size() > 0) {
				logger.info("Clearing existing locations");
				this.clearLocations(conn, functionCode, appId);
				
				String pageQuery = "INSERT INTO AUT_FUNC_PAGEAREAS (PA_APP,PA_FUNC_CODE,PA_NAME,PA_PARENT,PA_SEQ_NO,PA_TYPE,PA_WAY_IN,PA_WAY_OUT,PA_SOURCE,PA_PATH,TXNMODE,PA_LABEL,PA_SCREEN_TITLE,PA_IS_MRB) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				pst = conn.prepareStatement(pageQuery);
				
				for(Location location : locations) {
					pst.setInt(1,appId);
					pst.setString(2, functionCode);
					pst.setString(3, location.getLocationName());
					pst.setString(4,location.getParent());
					pst.setInt(5,location.getSequence());
					pst.setString(6,location.getLocationType());
					pst.setString(7, location.getWayIn());
					pst.setString(8, location.getWayOut());
					pst.setClob(9, (Clob)null);
					pst.setString(10, location.getPath());
					pst.setString(11, "G");
					pst.setString(12, location.getLabel());
					pst.setString(13, location.getScreenTitle());
					if(location.getLocationType() != null && location.getLocationType().equalsIgnoreCase("table")){
						location.setMultiRecordBlock(true);
					}

					if(location.isMultiRecordBlock()){
						pst.setString(14, "Y");
					}else{
						pst.setString(14, "N");
					}
					pst.addBatch();
				}
				
				pst.executeBatch();
				DatabaseHelper.close(pst);
			}
			
			if(testObjects != null && testObjects.size() > 0) {
				
				logger.info("Clearing existing fields");
				this.clearTestObjects(conn, functionCode, appId);
				
				logger.info("Persisting Test Objects");
				String fieldQuery = "Insert Into AUT_FUNC_FIELDS (FLD_APP,FLD_FUNC_CODE,FLD_SEQ_NO,FLD_UNAME,FLD_LABEL,FLD_TAB_ORDER,FLD_INSTANCE,FLD_INDEX,FLD_UID_TYPE,FLD_UID,FLD_TYPE,FLD_PAGE_AREA,TXNMODE,VIEWMODE,MANDATORY,FLD_DEFAULT_OPTIONS,FLD_IS_ENABLED,FLD_IS_FOOTER_ELEMENT,FLD_HAS_LOV,FLD_IS_MULTI_REC,FLD_GROUP,FLD_HAS_AUTOLOV) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				
				pst = conn.prepareStatement(fieldQuery);
				for(TestObject t : testObjects) {
					pst.setInt(1, appId);
					pst.setString(2, functionCode);
					pst.setInt(3, t.getSequence());
					pst.setString(4, t.getName());
					if(t.getLabel() != null && !t.getLabel().equalsIgnoreCase("")){
						pst.setString(5, t.getLabel());
					}else{
						pst.setString(5, t.getUniqueId());
					}
					pst.setInt(6, t.getTabOrder());
					pst.setInt(7, t.getInstance());
					pst.setInt(8, 0);
					pst.setString(9, t.getIdentifiedBy());
					pst.setString(10, t.getUniqueId());
					pst.setString(11, t.getObjectClass());
					pst.setString(12, t.getLocation());
					pst.setString(13, "G");
					pst.setString(14, t.getViewmode());
					pst.setString(15, t.getMandatory());
					if(t.getDefaultOptions() != null && t.getDefaultOptions().length() <= 1000){
						pst.setString(16, t.getDefaultOptions());
					}else{
						logger.warn("Default Options for Field [{}] on page [{}] has been purged as it is more than 1000 characters", t.getLabel(), t.getLocation());
						pst.setString(16, null);
					}
					pst.setString(17, t.getEnabled());
					if(t.getIsFooterElement() == null || t.getIsFooterElement().equalsIgnoreCase("")){
						t.setIsFooterElement("N");
					}
					pst.setString(18, t.getIsFooterElement());
					pst.setString(19, t.getLovAvailable());
					pst.setString(20, t.getIsMultiRecord());
					pst.setString(21, t.getGroup());
					pst.setString(22, t.getAutoLovAvailable());
					pst.addBatch();
				}
				
				pst.executeBatch();
				
				
			}
			
			//logger.info("Committing...");
			//conn.commit();
		} catch (SQLException e) {
			
			logger.error("ERROR persisting metadata", e);
			try {
				conn.rollback();
			}catch(Exception i) {
				logger.warn("ERROR occurred while rolling back after failure", e);
			}
			throw new DatabaseException("Could not persist metadata", e);
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
		
	}
	
	public void updateImportAuditTrail(int runId, String functionCode, String status, String message) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement("UPDATE LRNR_AUDIT_TRAIL SET LRNR_STATUS=?, LRNR_MESSAGE=?,LRNR_END_TIME=? WHERE LRNR_RUN_ID=? AND LRNR_FUNC_CODE=? ");
			pst.setString(1, status);
			pst.setString(2, message);
			pst.setTimestamp(3, new Timestamp(new Date().getTime()));
			pst.setInt(4, runId);
			pst.setString(5, functionCode);
			pst.executeUpdate();
		} catch (SQLException e) {
			
			logger.error("ERROR while updating import audit trail", e);
			throw new DatabaseException("Could not update audit trail");
		} finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
	}
	
	public boolean isFunctionLearningInProgress(int appId, String functionCode)  {

		boolean learningInProgress = false;
		Connection conn =null;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			PreparedStatement pst =null;
			ResultSet rs = null;
			try {
				pst = conn.prepareStatement("SELECT LRNR_STATUS FROM LRNR_AUDIT_TRAIL WHERE LRNR_APP_ID=? AND LRNR_FUNC_CODE=? AND LRNR_STATUS IN (?,?)");
				pst.setInt(1, appId);
				pst.setString(2, functionCode);
				
				rs = pst.executeQuery();
				while(rs.next()) {
					learningInProgress = true;
					break;
				}
				
				
			} catch (SQLException e) {
				logger.error("ERROR - Could not check if function is currently being learnt");
			} finally {
				
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				
			}
			
			
		} catch (DatabaseException e) {
			logger.error("ERROR - Could not check if function is currently being learnt");
		}finally{
			DatabaseHelper.close(conn);
		}
		
		return learningInProgress;
	}
	/*modified by shruthi for VAPT helper fixes starts*/
	public Location hydrateLocationHierarchy(int appId, String modCode, String operation, String entityType,int responseType) throws DatabaseException{
		Location main = null;
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if(conn != null){
			/*Statement st =null;*/
			PreparedStatement pst = null;
			ResultSet rs=null;
			try{
				/*st = conn.createStatement();
				

				rs = st.executeQuery("Select * From AUT_FUNC_PAGEAREAS where PA_FUNC_CODE ='" + modCode + "' And PA_APP = " + appId + " AND PA_TYPE='MAIN' AND PA_API_OPERATION='" + 
				operation + "' AND PA_API_ENTITY_TYPE='"+Utilities.trim(entityType).toUpperCase()+"' AND PA_API_RESP_TYPE='"+responseType+"'");*/
				/*query changed by paneendra starts*/
				pst = conn.prepareStatement("Select * From AUT_FUNC_PAGEAREAS where PA_FUNC_CODE =? And PA_APP =  ? AND PA_TYPE='MAIN' AND PA_API_OPERATION=? AND PA_API_ENTITY_TYPE=? AND PA_API_RESP_TYPE=?");
				/*query changed by paneendra ends*/
				pst.setString(1, modCode);
				pst.setInt(2, appId);
				pst.setString(3, operation);
				pst.setString(4, Utilities.trim(entityType).toUpperCase());
				pst.setInt(5, responseType);
				rs = pst.executeQuery();

				while(rs.next()){
					main = new Location();
					main.setLocationName(rs.getString("PA_NAME"));
					main.setParent(rs.getString("PA_PARENT"));
					main.setSequence(rs.getInt("PA_SEQ_NO"));
					main.setWayIn(rs.getString("PA_WAY_IN"));
					main.setWayOut(rs.getString("PA_WAY_OUT"));
					main.setLocationType(rs.getString("PA_TYPE"));
					main.setPath(rs.getString("PA_PATH"));
					String isMrb = rs.getString("PA_IS_MRB");
					if(Utilities.trim(isMrb).equalsIgnoreCase("Y"))
						main.setMultiRecordBlock(true);
				}

				if(main == null){
					logger.warn("Could not find Landing page for function {} {}", modCode, entityType);
					return main;
				}

				main = scanLocationForChildren(main, conn, modCode, appId, operation, entityType);
				
				main.setTestObjects(this.hydrateTestObjectsOnPageforResponseType(conn, modCode, main.getLocationName(), appId, operation, entityType,responseType));
				
			}catch(Exception e){
				throw new DatabaseException("Could not fetch location details",e);
			}
			finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		}else{
			throw new DatabaseException("A valid connection to the database could not be obtained");
		}


		return main;
	}
	/*modified by shruthi for VAPT helper fixes ends*/
	
	private Location scanLocationForChildren(Location location, Connection conn, String modCode, int appId, String operation, String entityType) throws DatabaseException{
		if(conn == null){
			throw new DatabaseException("Invalid Database connection");
		}


		ArrayList<Location> oChildren = null;
		try{
			ArrayList<Location> children = hydrateLocations(conn, modCode, appId, location.getLocationName(), operation, entityType);
			oChildren = new ArrayList<Location>();
			for(Location child:children){
				Location loc = scanLocationForChildren(child, conn, modCode, appId, operation, entityType);
				oChildren.add(loc);
			}


		}catch(Exception e){
			System.out.println("Could not process child page areas under page " + location.getLocationName());
		}

		if(oChildren != null && oChildren.size() > 0){
			location.setChildLocations(oChildren);
		}

		return location;
	}
	/*modified by shruthi for VAPT helper fixes starts*/
	@SuppressWarnings("null")
	public ArrayList<Location> hydrateLocations(Connection conn, String modCode, int appId, String pageArea, String operation, String entityType) throws DatabaseException{
		ArrayList<Location> testObjects = new ArrayList<Location>();
		if(conn != null){
			PreparedStatement pst = null;
			ResultSet rs=null;
			try{
				if(pageArea.equalsIgnoreCase("W")){   

					pst = conn.prepareStatement("Select * From AUT_FUNC_PAGEAREAS where PA_NAME in (select distinct a.mode1_pagearea from mapdetail a, mapmaster b where a.map_id = b.map_id and b.app_id =? and b.module_code =?) and PA_FUNC_CODE =? And PA_APP =? And txnmode = 'G' Order By PA_SEQ_NO");
					pst.setInt(1, appId);
					pst.setString(2, modCode);
					pst.setString(3, modCode);
					pst.setInt(4, appId);
					rs = pst.executeQuery();
				}
				else{
				    pst = conn.prepareStatement("Select * From AUT_FUNC_PAGEAREAS where PA_FUNC_CODE =? And PA_APP =? AND PA_PARENT =? AND PA_API_OPERATION=? AND PA_API_ENTITY_TYPE=? Order By PA_SEQ_NO");
				    pst.setString(1, modCode);
				    pst.setInt(2, appId);
				    pst.setString(3, pageArea);
				    pst.setString(4, operation);
				    pst.setString(5, Utilities.trim(entityType).toUpperCase());
				    rs = pst.executeQuery();
				    
				}
				while(rs.next()){
					Location t = new Location();
					t.setLocationName(rs.getString("PA_NAME"));
					t.setParent(rs.getString("PA_PARENT"));
					t.setSequence(rs.getInt("PA_SEQ_NO"));
					t.setWayIn(rs.getString("PA_WAY_IN"));
					t.setWayOut(rs.getString("PA_WAY_OUT"));
					t.setLocationType(rs.getString("PA_TYPE"));
					t.setScreenTitle(rs.getString("PA_SCREEN_TITLE"));
					t.setPath(rs.getString("PA_PATH"));
					String mrb = rs.getString("PA_IS_MRB");

					if(mrb != null && mrb.equalsIgnoreCase("Y")){
						t.setMultiRecordBlock(true);
					}else{
						t.setMultiRecordBlock(false);
					}
					
					t.setTestObjects(this.hydrateTestObjectsOnPage(conn, modCode, t.getLocationName(), appId, operation, entityType));
					
					testObjects.add(t);
				}
				
			}catch(Exception e){
				throw new DatabaseException("Could not fetch location details",e);
			}finally{
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
			}

		}else{
			throw new DatabaseException("A valid connection to the database could not be obtained");
		}

		return testObjects;
	}
	/*modified by shruthi for VAPT helper fixes ends*/

	
	public List<TestObject> hydrateTestObjectsOnPage(Connection conn, String moduleCode, String page, int appId, String apiOperation, String entityType) throws DatabaseException{
		List<TestObject> testObjects = new ArrayList<TestObject>();

		entityType = Utilities.trim(entityType).toUpperCase();
		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}
		PreparedStatement pst =null;
		ResultSet rs = null;

		try{
			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE = ? AND FLD_PAGE_AREA = ? AND FLD_API_OPERATION=? AND FLD_API_ENTITY_TYPE=? ORDER BY FLD_SEQ_NO");
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, page);
			pst.setString(4, apiOperation);
			pst.setString(5, entityType);
			rs = pst.executeQuery();

			while(rs.next()){
				TestObject t = new TestObject();
				t.setLabel(rs.getString("FLD_LABEL"));
				t.setName(rs.getString("FLD_UNAME"));
				t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
				t.setUniqueId(rs.getString("FLD_UID"));
				t.setObjectClass(rs.getString("FLD_TYPE"));
				t.setLocation(rs.getString("FLD_PAGE_AREA"));
				t.setViewmode(rs.getString("VIEWMODE"));
				t.setMandatory(rs.getString("MANDATORY"));
				t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
				t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
				t.setGroup(rs.getString("FLD_GROUP"));
				t.setIsMultiRecord(rs.getString("FLD_IS_MULTI_REC"));
				testObjects.add(t);
			}
			
		}catch(Exception e){
			throw new DatabaseException("Could not fetch field information for fields on page " + page,e);
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return testObjects;
	}
	
	
	public List<TestObject> hydrateTestObjectsOnPageforResponseType(Connection conn, String moduleCode, String page, int appId, String apiOperation, String entityType,int responseType) throws DatabaseException{
		List<TestObject> testObjects = new ArrayList<TestObject>();

		entityType = Utilities.trim(entityType).toUpperCase();
		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}
		PreparedStatement pst =null;
		ResultSet rs = null;
		try{
			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE = ? AND FLD_PAGE_AREA = ? AND FLD_API_OPERATION=? AND FLD_API_ENTITY_TYPE=? AND FLD_API_RESP_TYPE=? ORDER BY FLD_SEQ_NO");
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, page);
			pst.setString(4, apiOperation);
			pst.setString(5, entityType);
			pst.setInt(6, responseType);
			rs = pst.executeQuery();

			while(rs.next()){
				TestObject t = new TestObject();
				t.setLabel(rs.getString("FLD_LABEL"));
				t.setName(rs.getString("FLD_UNAME"));
				t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
				t.setUniqueId(rs.getString("FLD_UID"));
				t.setObjectClass(rs.getString("FLD_TYPE"));
				t.setLocation(rs.getString("FLD_PAGE_AREA"));
				t.setViewmode(rs.getString("VIEWMODE"));
				t.setMandatory(rs.getString("MANDATORY"));
				t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
				t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
				t.setGroup(rs.getString("FLD_GROUP"));
				t.setIsMultiRecord(rs.getString("FLD_IS_MULTI_REC"));
				testObjects.add(t);
			}
		
		}catch(Exception e){
			throw new DatabaseException("Could not fetch field information for fields on page " + page,e);
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return testObjects;
	}
	
	public void clearExtraExistingRecords(String apiCode,String OperationName,int appId){
					Connection conn=null;
					PreparedStatement pst =null;
					PreparedStatement pst2 =null;
		try{
			 conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

			if(conn == null){
				throw new DatabaseException("Invalid database connection");
			}
			/*modified by shruthi for VAPT helper fixes starts*/
			pst = conn.prepareStatement("DELETE FROM AUT_FUNC_FIELDS WHERE FLD_APP = ? AND FLD_FUNC_CODE = ? AND FLD_API_OPERATION=?"
					+ " AND FLD_API_ENTITY_TYPE=? AND FLD_API_RESP_TYPE NOT IN(SELECT DISTINCT API_RESP_CODE FROM TJN_AUT_API_OP_REPRESENTATIONS WHERE APP_ID =? AND API_CODE=? AND API_OPERATION=? and REPRESENTATION_TYPE='RESPONSE')");
			/*modified by shruthi for VAPT helper fixes ends*/
			pst.setInt(1, appId);
			pst.setString(2, apiCode);
			pst.setString(3, OperationName);
			pst.setString(4, "RESPONSE");
			/*added by shruthi for VAPT helper fixes starts*/
			pst.setInt(5, appId);
			pst.setString(6, apiCode);
			pst.setString(7, OperationName);
			/*added by shruthi for VAPT helper fixes ends*/
			pst.executeQuery();
			/*modified by shruthi for VAPT helper fixes starts*/
			 pst2 = conn.prepareStatement("DELETE FROM AUT_FUNC_PAGEAREAS WHERE PA_APP = ? AND PA_FUNC_CODE = ? AND PA_API_OPERATION=?"
					+ " AND PA_API_ENTITY_TYPE=? AND PA_API_RESP_TYPE NOT IN(SELECT DISTINCT API_RESP_CODE FROM TJN_AUT_API_OP_REPRESENTATIONS WHERE APP_ID =? AND API_CODE=? AND API_OPERATION=? and REPRESENTATION_TYPE='RESPONSE')");
			 /*modified by shruthi for VAPT helper fixes starts*/
			pst2.setInt(1, appId);
			pst2.setString(2, apiCode);
			pst2.setString(3, OperationName);
			pst2.setString(4, "RESPONSE");
			/*added by shruthi for VAPT helper fixes starts*/
			pst2.setInt(5, appId);
			pst2.setString(6, apiCode);
			pst2.setString(7, OperationName);
			/*added by shruthi for VAPT helper fixes ends*/
			pst2.executeQuery();
		}catch(Exception e){
		}
		finally{
			
			DatabaseHelper.close(pst2);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
	}
	}
	
	public String getRuntimeFieldOutputForMRB(Connection conn, String tdUid, String fieldIdentifier,int iterationNO) throws DatabaseException{
		String output = null;
		boolean outputFound = false;
		
		if(conn == null){
			throw new DatabaseException("Invalid Database Connection");
		}
		PreparedStatement pst1 = null;
		ResultSet rs1 =null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		try{
			
			pst1 = conn.prepareStatement("SELECT coalesce(MAX(RUN_ID),0) AS RUN_ID FROM RUNRESULT_TS_TXN WHERE TSTEP_TDUID = ?");
			
			pst1.setString(1, tdUid);
			rs1 = pst1.executeQuery();
			int runId = -1;
			while(rs1.next()){
				runId = rs1.getInt("RUN_ID");
			}

			if(runId < 1){
				throw new DatabaseException("Transaction with master record " + tdUid + " has not been executed");
			}
			
			pst = conn.prepareStatement("SELECT TSTEP_RESULT FROM RUNRESULT_TS_TXN WHERE RUN_ID = ? AND TSTEP_TDUID=?");
			pst.setInt(1, runId);
			pst.setString(2, tdUid);
			rs = pst.executeQuery();
			String res = "";
			while(rs.next()){
				res = rs.getString("TSTEP_RESULT");
				break;
			}
			
		
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			//VAPT null check fix
			if(res == null || !res.equalsIgnoreCase("S")){
				logger.error("Test Step with TDUID {} has either not been executed or has not completed successfully",tdUid);
				throw new DatabaseException("The test step with TDUID " + tdUid + " has either not been executed or has not completed successfully");
			}

			
			pst = conn.prepareStatement("SELECT FIELD_VALUE FROM TJN_EXEC_OUTPUT WHERE TDUID = ? AND RUN_ID = ? AND FIELD_NAME=? AND DETAIL_NO=?");
			pst.setString(1, tdUid);
			pst.setInt(2, runId);
			pst.setString(3, fieldIdentifier);
			pst.setInt(4, iterationNO);
			rs = pst.executeQuery();
			
			while(rs.next()){
				output = rs.getString("FIELD_VALUE");
				outputFound = true;
			}
			
			
		}catch(Exception e){
			throw new DatabaseException("Could not fetch Output for Master Record " + tdUid,e);
		}finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst1);
		}
		
		if(outputFound){
			return output;
		}else{
			throw new DatabaseException("Could not find runtime value for field " + fieldIdentifier + " in test step with TDUID " + tdUid + ".");
		}
	}
	public String hydrateTemplatePassword(int appid){
		PreparedStatement pst = null;
		ResultSet rs =null;
		Connection conn=null;
		try{
			 conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			 pst=conn.prepareStatement("SELECT TEMPLATE_PASSWORD FROM MASAPPLICATION where APP_ID=?");
			 pst.setInt(1, appid);
			 rs=pst.executeQuery();
			 if(rs.next()){
				 if(rs.getString("TEMPLATE_PASSWORD")==null){
						return "";
					}
				 else{
					 return rs.getString("TEMPLATE_PASSWORD");
				 }
			 }
		}catch(Exception e){
			
		}
		return null;
		
	}
	
	public int getFiledsLearnCount(String functionCode, int appid) throws DatabaseException {

		int fieldCount=0;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("Select count(*) as FLD_LABEL_COUNT from AUT_FUNC_FIELDS where FLD_FUNC_CODE= ? and FLD_APP= ?");)
		{
			 pst.setString(1, functionCode);
			 pst.setInt(2, appid);
			try(ResultSet rs= pst.executeQuery();){
				
				while (rs.next()) {
					fieldCount=	rs.getInt("FLD_LABEL_COUNT");
					
				}
				
			}
		}
		catch(Exception e){
			e.getStackTrace();
			logger.error("Could not fetch the label count ",e);
			throw new DatabaseException("Could not fetch the label count due to an internal error",e);
		}
		return fieldCount;

		
	}
	
	public int getFiledsLearnCount(String functionCode, int appid, String operation) throws DatabaseException {

		int fieldCount=0;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT COUNT(*) as FLD_LABEL_COUNT FROM AUT_FUNC_FIELDS WHERE FLD_FUNC_CODE= ? AND FLD_APP= ? AND FLD_API_OPERATION=?");)
		{
			 pst.setString(1, functionCode);
			 pst.setInt(2, appid);
			 pst.setString(3, operation);
			try(ResultSet rs= pst.executeQuery();){
				
				while (rs.next()) {
					fieldCount=	rs.getInt("FLD_LABEL_COUNT");
				}
			}
		}
		catch(Exception e){
			e.getStackTrace();
			logger.error("Could not fetch field label count",e);
			throw new DatabaseException("Could not fetch field label count due to an internal error",e);
		}
		return fieldCount;

	}
	/*Added by Lokanath for TENJINCG-1193 starts*/
	public String hydrateRunStatus(int runId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		String runStatus=null;
			String query = "";
			try{
			query = "Select run_status from MasTestRuns where run_id=?";
			pst = conn.prepareStatement(query);
			pst.setInt(1, runId);
           try(ResultSet rs= pst.executeQuery();){
				while (rs.next()) {
					runStatus=	rs.getString("RUN_STATUS");
				}
			}
			}catch(SQLException e){
				
			}
			finally{
				DatabaseHelper.close(pst);
			}
			
		return runStatus;
}
	/*Added by Lokanath for TENJINCG-1193 ends*/
	
	/* Added by Ashiki for TENJINCG-1225 starts*/
	public void functionJsonData(String jsonString, int runId) throws DatabaseException {

		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("update MASTESTRUNS set  RUN_JSON = ? WHERE RUN_ID = ?");)
		{
			pst.setString(1, jsonString);
			System.out.println(jsonString);
			pst.setInt(2, runId);
			pst.execute();
		}catch(DatabaseException e){
			logger.error("ERROR persisting Function json data", e);
			throw e;
		}catch(Exception e){
			logger.error("Could not insert Function json data",e);
			throw new DatabaseException("Could not insert Function json data due to an internal error",e);
		}
	
		
	}
	
	public ArrayList<TestObject> hydrateAutFuncFields(int appId, String moduleCode, String pageAreaName ) throws DatabaseException {
		ArrayList<TestObject> testObjects = null;
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE=? AND FLD_PAGE_AREA=?");)
		{
			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, pageAreaName);
			try(ResultSet rs= pst.executeQuery();){
				testObjects = new ArrayList<TestObject>();
				while(rs.next()){
					TestObject t = new TestObject();
					t.setName(rs.getString("FLD_UNAME"));
					t.setLabel(rs.getString("FLD_LABEL"));
					t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
					t.setUniqueId(rs.getString("FLD_UID"));
					t.setObjectClass(rs.getString("FLD_TYPE"));
					t.setLocation(rs.getString("FLD_PAGE_AREA"));
					t.setViewmode(rs.getString("VIEWMODE"));
					t.setMandatory(rs.getString("MANDATORY"));
					t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
					t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
					t.setDefaultOptions(rs.getString("FLD_DEFAULT_OPTIONS"));
					t.setViewmode(rs.getString("TXNMODE"));
					t.setEnabled(rs.getString("FLD_IS_ENABLED"));
					t.setIsFooterElement(rs.getString("FLD_IS_FOOTER_ELEMENT"));
					t.setSequence(rs.getInt("FLD_SEQ_NO"));
					t.setTabOrder(rs.getInt("FLD_TAB_ORDER"));
					t.setInstance(rs.getInt("FLD_INSTANCE"));
					t.setIndex(rs.getInt("FLD_INDEX"));
					t.setIsMultiRecord(rs.getString("FLD_IS_MULTI_REC"));
					t.setGroup(rs.getString("FLD_GROUP"));
					t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
					t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
					t.setAppId(appId);
					t.setModuleCode(moduleCode);
					t.setLocation(pageAreaName);
					testObjects.add(t);
				}
			}
		}catch(DatabaseException e){
			logger.error("ERROR fetching AUT func fields", e);
			throw e;
		}catch(Exception e){
			logger.error("ERROR fetching AUT func fields",e);
			throw new DatabaseException("Could not fetch AUT func fields details due to an internal error",e);
		}
		return testObjects;
	
		
	}
	
	public ArrayList<Location> hydrateLearntfunctionData(int appId, String moduleCode) throws DatabaseException {
		ArrayList<Location> location = new ArrayList<Location>();
		Location t = new Location();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT * FROM AUT_FUNC_PAGEAREAS WHERE PA_APP = ? AND PA_FUNC_CODE=?");)
		{
			pst.setInt(1, appId);
			 pst.setString(2, moduleCode);
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					t.setLocationName(rs.getString("PA_NAME"));
					t.setFuncFields(this.hydrateAutFuncFields(appId, moduleCode, t.getLocationName()));
					t.setParent(rs.getString("PA_PARENT"));
					t.setSequence(rs.getInt("PA_SEQ_NO"));
					t.setWayIn(rs.getString("PA_WAY_IN"));
					t.setWayOut(rs.getString("PA_WAY_OUT"));
					t.setLocationType(rs.getString("PA_TYPE"));
					t.setLabel(rs.getString("PA_LABEL"));
					
					 String mrb = rs.getString("PA_IS_MRB");
                     if(mrb != null && mrb.equalsIgnoreCase("Y")){
                            t.setMultiRecordBlock(true);
                     }else{
                            t.setMultiRecordBlock(false);
                     }
                     t.setPath(rs.getString("PA_PATH"));
                     t.setScreenTitle(rs.getString("PA_SCREEN_TITLE"));
                     t.setSource(rs.getString("PA_SOURCE"));
                     t.setTxnMode(rs.getString("TXNMODE"));
                     location.add(t);
				}
			}
		}catch(DatabaseException e){
			logger.error("ERROR fetching Pagearea details", e);
			throw e;
		}catch(Exception e){
			logger.error("Could not Pagearea details",e);
			throw new DatabaseException("Could not fetch Pagearea details details due to an internal error",e);
		}
		return location;
	}
	
	/* Added by Ashiki for TENJINCG-1225 ends*/
	
	public void insertApiLearningAuditTrail(Connection conn, int appId, List<Api> apis, int runId, String userId,String status) throws DatabaseException {
		PreparedStatement pst = null;
			PreparedStatement sPst =null;
			ResultSet sRs =null;
			try{
				//conn.setAutoCommit(false);
			for (Api api : apis) {
				
				for(ApiOperation op:api.getOperations()) {
					try{
					sPst = conn.prepareStatement("SELECT COALESCE(MAX(LRNR_ID),0) as lrnr_id FROM LRNR_AUDIT_TRAIL_API");
					sRs = sPst.executeQuery();
					
					int seq=0;
					while(sRs.next()) {
						seq = sRs.getInt("LRNR_ID");
					}
					
					seq++;
					pst = conn.prepareStatement("INSERT INTO LRNR_AUDIT_TRAIL_API (LRNR_ID,LRNR_USER_ID,LRNR_APP_ID,LRNR_API_CODE,LRNR_OPERATION,LRNR_STATUS, LRNR_RUN_ID) "
							+ "values (?,?,?,?,?,?,?)");
					
					pst.setInt(1, seq);
					pst.setString(2, userId);
					pst.setInt(3, appId);
					pst.setString(4, api.getCode());
					pst.setString(5, op.getName());
					pst.setString(6, status);
					pst.setInt(7, runId);
					pst.execute();
					
					ApiLearnerResultBean bean = new ApiLearnerResultBean();
					bean.setId(seq);
					op.setLastSuccessfulLearningResult(bean);
					}finally{
						DatabaseHelper.close(sRs);
						DatabaseHelper.close(sPst);
						DatabaseHelper.close(pst);
					}
				}
			}
				
				//conn.commit();
			} catch(SQLException e) {
				logger.error("ERROR inserting audit recods for API Learning run {}", runId, e);
				throw new DatabaseException("Could not insert audit records");
			} finally{
				DatabaseHelper.close(sRs);
				DatabaseHelper.close(sPst);
				DatabaseHelper.close(pst);
				
			}
			
		
	
	}
}
