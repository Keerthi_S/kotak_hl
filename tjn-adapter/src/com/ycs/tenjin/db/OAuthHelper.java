/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  OAuthHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
* DATE                 ADDED BY              DESCRIPTION
* 22-03-2019			Avinash				Newly added for OAuth 2.0 requirement TENJINCG-1018
*/

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.ApplicationUser;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.util.Constants;

public class OAuthHelper {
	private static Logger logger = LoggerFactory.getLogger(OAuthHelper.class);
	
	public void persistAccessToken(ApplicationUser record, int runId) throws DatabaseException {
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst1 = conn.prepareStatement("UPDATE USRAUTCREDENTIALS SET OAUTH_ACCESS_TOKEN=?, OAUTH_REFRESH_TOKEN=?, OAUTH_TOKENGEN_TIMESTAMP=?, OAUTH_TOKEN_VALIDITY=? WHERE UAC_ID=?");
			){
				persistoAuthAudit(runId, record.getUacId());
				pst1.setString(1, record.getAccessToken());
				pst1.setString(2, record.getRefreshToken());
				pst1.setTimestamp(3, new Timestamp(record.getTokenGenTime().getTime()));
				pst1.setString(4, record.getTokenValidity());
				pst1.setInt(5, record.getUacId());
				pst1.execute();
		}catch(SQLException e) {
			logger.error("Could not update AUT user record for access token");
			throw new DatabaseException("Could not update AUT user record for access token",e);
		}
	}
	
	public void persistoAuthAudit(int runId, int uacId) throws DatabaseException {
		try(
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("SELECT OAUTH_ACCESS_TOKEN, OAUTH_REFRESH_TOKEN, OAUTH_TOKENGEN_TIMESTAMP, APP_ID, USER_ID FROM USRAUTCREDENTIALS WHERE UAC_ID = ?");
				PreparedStatement pst2 = conn.prepareStatement("INSERT INTO TJN_OAUTH_AUDIT_TRAIL (UTKN_ID, ACCESS_TOKEN, TKN_GENERATED_BY, REFRESH_TOKEN, APP_ID, GENERATE_TIME, UPDATE_TIME, UAC_ID, RUN_ID) VALUES (?,?,?,?,?,?,?,?,?)");
				PreparedStatement pst3 = conn.prepareStatement("SELECT coalesce(MAX(UTKN_ID),0) AS UTKN_ID FROM TJN_OAUTH_AUDIT_TRAIL");
			){
						
			//GEtting the max UTKN_ID
			int tknId = 0;
			try (ResultSet rs3 = pst3.executeQuery()){
				while(rs3.next()){
					tknId = rs3.getInt("UTKN_ID");
				}
			}
			++tknId;
			
			pst.setInt(1, uacId);
			try (ResultSet rs = pst.executeQuery()){
			//Inserting the record
				while (rs.next()){
					pst2.setInt(1, tknId);
					pst2.setString(2, rs.getString("OAUTH_ACCESS_TOKEN"));
					pst2.setString(3, "TENJIN");
					pst2.setString(4, rs.getString("OAUTH_REFRESH_TOKEN"));
					pst2.setInt(5, rs.getInt("APP_ID"));
					pst2.setTimestamp(6, rs.getTimestamp("OAUTH_TOKENGEN_TIMESTAMP"));
					pst2.setTimestamp(7, new Timestamp(new Date().getTime()));
					pst2.setInt(8, uacId);
					pst2.setInt(9, runId);
					pst2.execute();
				}
			}
		}catch(SQLException e) {
			logger.error("Could not insert OAUTH_AUDIT user record for access token");
			throw new DatabaseException("Could not insert OAUTH_AUDIT user record for access token",e);
		}
	}

	public Aut getTokenDetails(Aut aut) throws DatabaseException {
		try(
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("SELECT OAUTH_ACCESS_TOKEN, OAUTH_REFRESH_TOKEN, OAUTH_TOKEN_VALIDITY, OAUTH_TOKENGEN_TIMESTAMP FROM USRAUTCREDENTIALS WHERE UAC_ID=?");
			){
			pst.setInt(1, aut.getUacId());
			try (ResultSet rs = pst.executeQuery()){
				while(rs.next()){
					aut.setAccessToken(rs.getString("OAUTH_ACCESS_TOKEN"));
					aut.setRefreshToken(rs.getString("OAUTH_REFRESH_TOKEN"));
					aut.setTokenValidity(rs.getString("OAUTH_TOKEN_VALIDITY"));
					aut.setTokenGenerationTime(rs.getTimestamp("OAUTH_TOKENGEN_TIMESTAMP"));
				}
			}
		}catch(Exception e) {
			logger.error("Could not insert OAUTH_AUDIT user record for access token" ,e.getMessage());
			throw new DatabaseException("Could not insert OAUTH_AUDIT user record for access token",e);
		}
		return aut;
	}
}
