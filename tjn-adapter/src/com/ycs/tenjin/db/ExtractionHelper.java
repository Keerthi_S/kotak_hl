/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExtractionHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 08-Nov-2016           Sriram Sridharan          Newly Added For Data Extraction in Tenjin 2.4.1
* 11-01-2017             Leelaprasad              REQ#TENJINCG-34
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 11-07-2018			Preeti					Closed connections
*/


package com.ycs.tenjin.db;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.pojo.aut.ExtractionRecord;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.util.Constants;

public class ExtractionHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(ExtractionHelper.class);
	
	public void insertExtractorAuditTrailMaster(Connection conn, int runId, int appId, List<Module> functions, String userId) throws DatabaseException{
		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement pst0 = null;
		
		
		try {
			for(Module function:functions){
				try{
					int eId = 0;
				
					try{
						pst0 = conn.prepareStatement("SELECT COALESCE(MAX(EXTR_ID),0) FROM EXTR_AUDIT_TRAIL");
						rs = pst0.executeQuery();
				
						while(rs.next()){
							eId = rs.getInt(1);
						}
					}finally{
						DatabaseHelper.close(pst0);
					}
				
				pst = conn.prepareStatement("INSERT INTO EXTR_AUDIT_TRAIL ("
						+ "EXTR_ID,"
						+ "EXTR_USER_ID,"
						+ "EXTR_APP_ID,"
						+ "EXTR_FUNC_CODE,"
						+ "EXTR_INP_FILE_PATH,"
						+ "EXTR_RUN_ID,"
						+ "EXTR_AUT_USER_TYPE, "
						+ "EXTR_STATUS)"
						+ " VALUES (?,?,?,?,?,?,?,?)");
				eId = eId+1;
				pst.setInt(1, eId);
				pst.setString(2, userId);
				pst.setInt(3, appId);
				pst.setString(4, function.getCode());
				pst.setString(5, function.getExtractorInputFile());
				pst.setInt(6, runId);
				pst.setString(7, function.getAutUserType());
				pst.setString(8, BridgeProcess.QUEUED);
				pst.execute();
				DatabaseHelper.close(pst);
				logger.debug("Writing detail records");
				if(function.getExtractionRecords() != null){
					for(ExtractionRecord record:function.getExtractionRecords()){
						try{
						pst = conn.prepareStatement("INSERT INTO EXTR_AUDIT_TRAIL_DETAIL ("
								+ "EXTR_ID,"
								+ "EXTR_RUN_ID,"
								+ "EXTR_APP_ID,"
								+ "EXTR_FUNC_CODE,"
								+ "EXTR_TDUID,"
								+ "EXTR_QUERY_FIELDS, "
								+ "EXTR_RESULT, "
								+ "EXTR_SS_ROW_NUM, "
								+ "EXTR_TDGID"
								+ ") VALUES (?,?,?,?,?,?,?,?,?)");
						
						pst.setInt(1, eId);
						pst.setInt(2, runId);
						pst.setInt(3, appId);
						pst.setString(4, function.getCode());
						pst.setString(5, record.getTdUid());
						String qFields = "";
						if(record.getQueryFields() != null){
							for(String key:record.getQueryFields().keySet()){
								qFields = qFields + key + "[" + record.getQueryFields().get(key) + "];;;";
							}
						}
						pst.setString(6, qFields);
						pst.setString(7, "N");
						pst.setInt(8, record.getSpreadsheetRowNum());
						pst.setString(9, record.getTdGid());
						pst.execute();
						}finally{
							DatabaseHelper.close(pst);
						}
					}
				}
				}
				finally{
					DatabaseHelper.close(rs);
					DatabaseHelper.close(pst);
				}
			}
		} catch (Exception e) {
			
			logger.error("ERROR occurred while persisting Extraction audit trail",e);
			throw new DatabaseException("An error occurred while persisting Extraction information");
		} finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
	}
	
	/****************************
	 * Added by Sriram for Req#TJN_23_18
	 */
	public void beginExtractionRun(int runId, boolean updateStartTime) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		PreparedStatement pst = null;
		
		try {
			String updateQuery = "";
			if(updateStartTime){
				updateQuery = "Update MasTestRuns Set run_status=?, run_start_time=? where run_id=?";
				pst = conn.prepareStatement(updateQuery);
				pst.setString(1, BridgeProcess.IN_PROGRESS);
				pst.setTimestamp(2, new Timestamp(new Date().getTime()));
				pst.setInt(3, runId);
			}else{
				updateQuery = "Update MasTestRuns Set run_status=? where run_id=?";
				pst = conn.prepareStatement(updateQuery);
				pst.setString(1, BridgeProcess.IN_PROGRESS);
				pst.setInt(2, runId);
			}
			
			pst.executeUpdate();
			
		} catch (SQLException e) {
			
			logger.error("ERROR updating Run Start", e);
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}/****************************
	 * Added by Sriram for Req#TJN_23_18 ends
	 */
	
	@SuppressWarnings("resource")
	public void endExtractionRun(int runId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			String updateQuery = "";
			
			updateQuery = "Update MasTestRuns Set run_status=?, run_end_time=? where run_id=?";
			pst = conn.prepareStatement(updateQuery);
			pst.setString(1, BridgeProcess.COMPLETE);
			pst.setTimestamp(2, new Timestamp(new Date().getTime()));
			pst.setInt(3, runId);
			pst.executeUpdate();
			
			pst=conn.prepareStatement("SELECT SCH_ID FROM TJN_SCHEDULER WHERE RUN_ID=? AND STATUS='Executing'");
			pst.setInt(1, runId);
			rs=pst.executeQuery();
			int schId=0;
			if(rs.next()){
				schId=rs.getInt("SCH_ID");
			}
			if(schId!=0)
			{
				new SchedulerHelper().updateScheduleStatus("Completed",schId);
			}
			
		} catch (SQLException e) {
			
			logger.error("ERROR updating Run End", e);
		} finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
	
	
	
	public void updateMasterAuditTrail(int runId, String functionCode, String status, String message) {
		Connection conn = null;
		
		try {
			conn =  DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch (DatabaseException e) {
			
			logger.error("ERROR updating Extractor Audit Trail --> No DB Connection", e);
			return;
		}
		
		PreparedStatement pst = null;
		
		try {
			pst = conn.prepareStatement("UPDATE EXTR_AUDIT_TRAIL SET EXTR_STATUS=?, EXTR_MESSAGE=? WHERE EXTR_RUN_ID=? AND EXTR_FUNC_CODE=?");
			pst.setString(1, status);
			pst.setString(2, message);
			pst.setInt(3, runId);
			pst.setString(4, functionCode);
			
			pst.executeUpdate();
		} catch (SQLException e) {
			
			logger.error("ERROR updating Extractor Audit Trail", e);
			return;
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
		
	}
	
	public void updateMasterAuditTrail(int runId, String functionCode, String status, String message, long startTimeStamp, long endTimeStamp) {
		Connection conn = null;
		
		try {
			conn =  DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch (DatabaseException e) {
			
			logger.error("ERROR updating Extractor Audit Trail --> No DB Connection", e);
			return;
		}
		
		PreparedStatement pst = null;
		
		try {
			pst = conn.prepareStatement("UPDATE EXTR_AUDIT_TRAIL SET EXTR_STATUS=?, EXTR_MESSAGE=?,EXTR_END_TIME=?,EXTR_START_TIME=? WHERE EXTR_RUN_ID=? AND EXTR_FUNC_CODE=?");
			pst.setString(1, status);
			pst.setString(2, message);
			pst.setTimestamp(3, new Timestamp(endTimeStamp));
			pst.setTimestamp(4, new Timestamp(startTimeStamp));
			pst.setInt(5, runId);
			pst.setString(6, functionCode);
			
			pst.executeUpdate();
		} catch (SQLException e) {
			
			logger.error("ERROR updating Extractor Audit Trail", e);
			return;
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
		
	}
	
	public void updateMasterAuditTrail(int runId, String functionCode, String status, String message, long endTimeStamp) {
		Connection conn = null;
		
		try {
			conn =  DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch (DatabaseException e) {
			
			logger.error("ERROR updating Extractor Audit Trail --> No DB Connection", e);
			return;
		}
		
		PreparedStatement pst = null;
		
		try {
			pst = conn.prepareStatement("UPDATE EXTR_AUDIT_TRAIL SET EXTR_STATUS=?, EXTR_MESSAGE=?,EXTR_END_TIME=? WHERE EXTR_RUN_ID=? AND EXTR_FUNC_CODE=?");
			pst.setString(1, status);
			pst.setString(2, message);
			pst.setTimestamp(3, new Timestamp(endTimeStamp));
			pst.setInt(4, runId);
			pst.setString(5, functionCode);
			
			pst.executeUpdate();
		} catch (SQLException e) {
			
			logger.error("ERROR updating Extractor Audit Trail", e);
			return;
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
		
	}
	
	public void updateDetailAuditTrail(int runId, String functionCode, String tdUid, String status, String message, String dataJson) {
		Connection conn = null;
		
		try {
			conn =  DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch (DatabaseException e) {
			
			logger.error("ERROR updating Extractor Audit Trail --> No DB Connection", e);
			return;
		}
		
		PreparedStatement pst = null;
		
		try {
			pst = conn.prepareStatement("UPDATE EXTR_AUDIT_TRAIL_DETAIL SET EXTR_RESULT=?, EXTR_MESSAGE=?, EXTR_DATA=? WHERE EXTR_RUN_ID=? AND EXTR_FUNC_CODE=? AND EXTR_TDUID=?");
			pst.setString(1, status);
			pst.setString(2, message);
			
			Clob cLob = conn.createClob();
			
			cLob.setString(1, dataJson);
			pst.setClob(3, cLob);
			pst.setInt(4, runId);
			pst.setString(5, functionCode);
			pst.setString(6, tdUid);
			
			pst.executeUpdate();
		} catch (SQLException e) {
			
			logger.error("ERROR Updating detail audit trail", e);
			return;
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
	
	public void updateDetailAuditTrail(int runId, String functionCode, String status, String message, String dataJson) {
		Connection conn = null;
		
		try {
			conn =  DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch (DatabaseException e) {
			
			logger.error("ERROR updating Extractor Audit Trail --> No DB Connection", e);
			return;
		}
		
		PreparedStatement pst = null;
		
		try {
			pst = conn.prepareStatement("UPDATE EXTR_AUDIT_TRAIL_DETAIL SET EXTR_RESULT=?, EXTR_MESSAGE=?, EXTR_DATA=? WHERE EXTR_RUN_ID=? AND EXTR_FUNC_CODE=?");
			pst.setString(1, status);
			pst.setString(2, message);
			
			Clob cLob = conn.createClob();
			
			cLob.setString(1, dataJson);
			pst.setClob(3, cLob);
			pst.setInt(4, runId);
			pst.setString(5, functionCode);
			
			pst.executeUpdate();
		} catch (SQLException e) {
			
			logger.error("ERROR Updating detail audit trail", e);
			return;
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
	}
	
	public void updateAuditTrailAbort(int runId, String status, String message) {
		Connection conn = null;
		
		try {
			conn =  DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch (DatabaseException e) {
			
			logger.error("ERROR updating Extractor Audit Trail --> No DB Connection", e);
			return;
		}
		
		PreparedStatement pst = null;
		
		try {
			//logger.debug("Setting autoCommit to false");
			//conn.setAutoCommit(false);
			
			logger.debug("done");
			
			logger.debug("Updating master");
			pst = conn.prepareStatement("UPDATE EXTR_AUDIT_TRAIL SET EXTR_STATUS=?, EXTR_MESSAGE=?, EXTR_END_TIME=?,EXTR_START_TIME=? WHERE EXTR_RUN_ID=?");
			pst.setString(1, status);
			pst.setString(2, message);
			pst.setTimestamp(3, new Timestamp(new Date().getTime()));
			pst.setTimestamp(4, new Timestamp(new Date().getTime()));
			pst.setInt(5, runId);

			pst.executeUpdate();
			
			logger.debug("Closing pst");
			/*pst.close();*/
			
			logger.debug("Done");
		} catch (SQLException e) {
			
			logger.error("ERROR updating audit trail abort", e);
			try{
				conn.rollback();
			}catch(Exception ignore){}
			
			finally{
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
			
			return;
		}
		
		logger.debug("Updating detail");
		try {
			pst = conn.prepareStatement("UPDATE EXTR_AUDIT_TRAIL_DETAIL SET EXTR_RESULT=?, EXTR_MESSAGE=? WHERE EXTR_RUN_ID=?");
			pst.setString(1, status);
			pst.setString(2, message);
			pst.setInt(3, runId);
			pst.executeUpdate();
			
			logger.debug("Closing pst");
			
			logger.debug("Done");
		}catch(SQLException e){
			
			logger.error("ERROR updating audit trail abort", e);
			try{
				conn.rollback();
			}catch(Exception ignore){}
			
			
			finally{
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
			return;
		}
		
		/*logger.debug("Committing");
		try{
			conn.commit();
		}catch(SQLException e){
			logger.error("Could not commit audit trail abort", e);
			try{
				conn.rollback();
			}catch(Exception ignore){}
			return;
		} finally{
			
			    DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
		}*/
	}
}
