/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MessageHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*	
* DATE                 CHANGED BY              	DESCRIPTION
* 15-06-2021			Ashiki				Newly Added for TENJINCG-1275

*/

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.MessageValidate;
import com.ycs.tenjin.util.Constants;


public class MessageHelper {
	private static Logger logger = LoggerFactory.getLogger(MessageHelper.class);
	
	
	public MessageValidate hydrateMsg(int appId, String functionCode) throws DatabaseException{


		MessageValidate msg=new MessageValidate();
		try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst=conn.prepareStatement("SELECT  A.*,B.APP_NAME AS APP_NAME FROM TJN_AUT_MESSAGES A, MASAPPLICATION B WHERE A.APP_ID = B.APP_ID AND  A.APP_ID=? and A.MESSAGES=?");)
		{
			/*modified by shruthi for VAPT helper fixes starts*/
			pst.setInt(1, appId);
			pst.setString(2, functionCode);
			/*modified by shruthi for VAPT helper fixes ends*/
			try(ResultSet rs= pst.executeQuery();){
				while(rs.next()){
					
					msg.setCode(rs.getString("MESSAGES"));
					msg.setName(rs.getString("MESSAGES"));
					msg.setType(rs.getString("TYPE"));
					msg.setUrl(rs.getString("FILENAME"));
					msg.setApplicationName(rs.getString("APP_NAME"));
					msg.setApplicationId(appId);
				}
			}
		}
		catch(Exception e){
			logger.error("Could not fetch Message Format user",e);
			throw new DatabaseException("Could not fetch Format due to an internal error",e);
		}
		return msg;
	}
	
}