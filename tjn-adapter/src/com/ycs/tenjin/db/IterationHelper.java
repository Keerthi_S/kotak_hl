/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  IterationHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 24-Sep-2016           Sriram Sridharan        Newly Added For 
* 03-12-2016            Parveen                 REQ #TJN_243_04 Changing insert query
* 16-06-2017			Sriram Sridharan		TENJINCG-187
* 04-07-2017			Roshni					TENJINCG-259
* 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
* 02-02-2018			Preeti					TENJINCG-101
* 15-05-2018            Padmavathi              TENJINCG-647
* 11-07-2018			Preeti					Closed connections
* 24-08-2018            Padmavathi              TENJINCG-728
* 10-01-2019			Preeti					TJN252-43
* 18-01-2019            Padmavathi               TJN252-74
* 19-02-2019            Padmavathi               TJN252-48
* 22-03-2019			Preeti					TENJINCG-1018
* 18-11-2020			Pushpa					TENJINCG-1228
 
*/

package com.ycs.tenjin.db;

import java.io.FileInputStream;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.RuntimeScreenshot;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.mail.TenjinReflection;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class IterationHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(IterationHelper.class);
	
	
	public void beginRun(int runId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		
		try{
			pst = conn.prepareStatement("UPDATE MASTESTRUNS SET RUN_STATUS=? WHERE RUN_ID=?");
			pst.setInt(2, runId);
			pst.setString(1, BridgeProcess.IN_PROGRESS);
			pst.execute();
		} catch(SQLException e){
			logger.error("ERROR updating Run start for run [{}]", runId, e);
		} finally{
			
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
		}
	}
	
	@SuppressWarnings("resource")
	public void endRun(int runId, String status) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst = conn.prepareStatement("UPDATE MASTESTRUNS SET RUN_STATUS=?,RUN_END_TIME=? WHERE RUN_ID=?");
			pst.setInt(3, runId);
			pst.setString(1, status);
			pst.setTimestamp(2, new Timestamp(new Date().getTime()));
			pst.execute();
		
			pst=conn.prepareStatement("SELECT SCH_ID FROM TJN_SCHEDULER WHERE RUN_ID=? AND STATUS='Executing'");
			pst.setInt(1, runId);
			rs=pst.executeQuery();
			int schId=0;
			if(rs.next()){
				schId=rs.getInt("SCH_ID");
			}
			if(schId!=0)
			{
				new SchedulerHelper().updateScheduleStatus("Completed",schId);
			}
		} catch(SQLException e){
			logger.error("ERROR updating Run End for run [{}]", runId, e);
		} finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	
	
	public void updateRunStatus(int runId, int stepRecordId, int iteration, String status, Date startTime) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		PreparedStatement pst = null;
		
		try{
			pst = conn.prepareStatement("UPDATE RUNRESULT_TS_TXN SET TSTEP_RESULT=?,EXEC_START_TIME=? WHERE RUN_ID=? AND TSTEP_REC_ID=? AND TXN_NO=?");
			pst.setString(1, status);
			pst.setTimestamp(2,new Timestamp(startTime.getTime()));
			pst.setInt(3, runId);
			pst.setInt(4, stepRecordId);
			pst.setInt(5, iteration);
			
			pst.execute();
			
		}catch(SQLException e){
			logger.error("ERROR updating status of Step [{}] iteration [{}] in run [{}]", stepRecordId, iteration, runId, e);
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
		
	public void updateRunStatus(int runId, int stepRecordId, int iteration, String status, String message, Date endTime) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		PreparedStatement pst = null;
		
		try{
			pst = conn.prepareStatement("UPDATE RUNRESULT_TS_TXN SET TSTEP_RESULT=?, TSTEP_MESSAGE=?, EXEC_END_TIME=? WHERE RUN_ID=? AND TSTEP_REC_ID=? AND TXN_NO=?");
			pst.setString(1, status);
			pst.setString(2, message);
			pst.setTimestamp(3, new Timestamp(endTime.getTime()));
			pst.setInt(4, runId);
			pst.setInt(5, stepRecordId);
			pst.setInt(6, iteration);
			pst.execute();
			
		}catch(SQLException e){
			logger.error("ERROR updating status of Step [{}] in run [{}]", stepRecordId, runId, e);
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
		
		
		public void updateRunStatus(int runId, int stepRecordId, int iteration, String status, String message, Date endTime, String accessToken, String authType) throws DatabaseException{
		  
			try(
		  		   Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				   PreparedStatement pst = conn.prepareStatement("UPDATE RUNRESULT_TS_TXN SET TSTEP_RESULT=?, TSTEP_MESSAGE=?, EXEC_END_TIME=?, API_AUTH_TYPE=?, API_ACCESS_TOKEN=? WHERE RUN_ID=? AND TSTEP_REC_ID=? AND TXN_NO=?");
			 ){
				
				pst.setString(1, status);
				pst.setString(2, message);
				pst.setTimestamp(3, new Timestamp(endTime.getTime()));
				pst.setString(4, authType);
				pst.setString(5, accessToken);
				pst.setInt(6, runId);
				pst.setInt(7, stepRecordId);
				pst.setInt(8, iteration);
				
				pst.execute();
				
			}catch(SQLException e){
				logger.error("ERROR updating status of Step [{}] in run [{}]", stepRecordId, runId, e);
			} 
		}
	public void updateRunStatusForAllTransactions(int runId, int stepRecordId, String status, String message) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		PreparedStatement pst = null;
		
		try{
			pst = conn.prepareStatement("UPDATE RUNRESULT_TS_TXN SET TSTEP_RESULT=?, TSTEP_MESSAGE=? WHERE RUN_ID=? AND TSTEP_REC_ID=?");
			pst.setString(1, status);
			pst.setString(2, message);
			pst.setInt(3, runId);
			pst.setInt(4, stepRecordId);
			pst.execute();
			
		}catch(SQLException e){
			logger.error("ERROR updating status of Step [{}] in run [{}]", stepRecordId, runId, e);
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	
	public void updateRunStatus(int runId, List<Integer> stepRecordIds, String status, String message) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		PreparedStatement pst = null;
		
		try{
			
			for(int stepRecordId:stepRecordIds){
				try{
				pst = conn.prepareStatement("UPDATE RUNRESULT_TS_TXN SET TSTEP_RESULT=?, TSTEP_MESSAGE=? WHERE RUN_ID=? AND TSTEP_REC_ID=?");
				pst.setString(1, status);
				pst.setString(2, message);
				pst.setInt(3, runId);
				pst.setInt(4, stepRecordId);
				pst.execute();
				}finally{
					DatabaseHelper.close(pst);
				}
			}
		}catch(SQLException e){
			logger.error("ERROR updating status of multiple steps [{}] in run [{}]", stepRecordIds.toString(), runId, e);
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	
	public void persistRuntimeScreenshots(List<RuntimeScreenshot> screenshots, int runId, int stepRecordId, int iteration,boolean isFromUft) throws DatabaseException{
		if(screenshots == null || screenshots.size() < 1){
			return;
		}
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		PreparedStatement pst = null;
		
		try {
			//conn.setAutoCommit(false);
			pst = conn.prepareStatement("INSERT INTO TJN_EXEC_SCRNSHOT (RUN_ID,TDGID,TDUID,TSTEP_REC_ID,ITERATION_NO,SEQ_NO,TYPE_SCRNSHOT,SCRNSHOT,SCR_TIMESTAMP,SCR_DESC) VALUES (?,?,?,?,?,?,?,?,?,?)");
			for(RuntimeScreenshot screenshot:screenshots){
				if(screenshot.getScreenshot() == null && !isFromUft){
					continue;
				}
				else if( screenshot.getScreenshotByteArray()==null && isFromUft ){
					continue;
				}
				pst.setInt(1, screenshot.getRunId());
				pst.setString(2, screenshot.getTdGid());
				pst.setString(3, screenshot.getTdUid());
				pst.setInt(4, screenshot.getTestStepRecordId());
				pst.setInt(5, screenshot.getIteration());
				pst.setInt(6, screenshot.getSequence());
				pst.setString(7, screenshot.getType());
				if(isFromUft){
					pst.setBytes(8, screenshot.getScreenshotByteArray());
				}else{
					if(screenshot.getScreenshot() != null){
						pst.setBinaryStream(8, new FileInputStream(screenshot.getScreenshot()));
					}else{
						pst.setNull(8, java.sql.Types.BLOB);
					}
				}
				pst.setTimestamp(9, screenshot.getTimestamp());
				pst.setString(10, screenshot.getDescription());
				pst.addBatch();
			}
			
			pst.executeBatch();
			//conn.commit();
		} catch (Exception e) {
			
			logger.error("ERRoR while persisting runtime screenshots for Step [{}], ITeration [{}]", stepRecordId, iteration);
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
	}
	
	public void persistRuntimeFieldValues(List<RuntimeFieldValue> values, int runId, int stepRecordId, int iteration) throws DatabaseException{
		
		if(values == null || values.size() < 1){
			return;
		}
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		PreparedStatement pst = null;
		
		try {
			//conn.setAutoCommit(false);
			
			
			for(RuntimeFieldValue rfv : values) {
				try {
					pst=conn.prepareStatement("DELETE FROM TJN_EXEC_OUTPUT WHERE RUN_ID=? AND TSTEP_REC_ID=? AND TDUID=? AND ITERATION_NO=? AND FIELD_NAME=?  AND DETAIL_NO=? ");
					pst.setInt(1, rfv.getRunId());
					pst.setInt(2, rfv.getTestStepRecordId());
					pst.setString(3, rfv.getTdUid());
					pst.setInt(4, rfv.getIteration());
					pst.setString(5, rfv.getField());
					pst.setInt(6, rfv.getDetailRecordNo());
					pst.execute();
				   }catch (Exception e) {
						logger.error("Unable to persist run time field value due to "+e.getMessage());
					}finally{
						DatabaseHelper.close(pst);
					}
					
					try{
					pst = conn.prepareStatement("INSERT INTO TJN_EXEC_OUTPUT (RUN_ID,TDGID,TDUID,TSTEP_REC_ID,ITERATION_NO,DETAIL_NO,FIELD_NAME,FIELD_VALUE)  VALUES (?,?,?,?,?,?,?,?)");
					
					pst.setInt(1, rfv.getRunId());
					pst.setString(2, rfv.getTdGid());
					pst.setString(3, rfv.getTdUid());
					pst.setInt(4, rfv.getTestStepRecordId());
					pst.setInt(5, rfv.getIteration());
					pst.setInt(6, rfv.getDetailRecordNo());
					pst.setString(7, rfv.getField());
					pst.setString(8, rfv.getValue());
					pst.execute();
					DatabaseHelper.close(pst);
					
				} catch (Exception e) {
					logger.error("Unable to persist run time field value due to "+e.getMessage());
				}finally{
					DatabaseHelper.close(pst);
				}
			}
			//conn.commit();
		} catch (Exception e) {
			
			logger.error("ERRoR while persisting runtime values for Step [{}], ITeration [{}]", stepRecordId, iteration);
		} finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
	}
	
	public void persistValidationResults(List<ValidationResult> results, int runId, int stepRecordId, int iteration) throws DatabaseException{
		
		
		if(results == null || results.size() < 1){
			return;
		}
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		PreparedStatement p = null;
		
		if(results != null){
			try {
				//conn.setAutoCommit(false);
				p = conn.prepareStatement("INSERT INTO RESVALRESULTS (RUN_ID,RES_VAL_ID,TSTEP_REC_ID,RES_VAL_PAGE,RES_VAL_FIELD,RES_VAL_EXP_VAL,RES_VAL_ACT_VAL,RES_VAL_STATUS,ITERATION,RES_VAL_DETAIL_ID) VALUES (?,?,?,?,?,?,?,?,?,?)");
				for(ValidationResult result:results){
					
					p.setInt(1, result.getRunId());
					p.setInt(2, result.getResValId());
					p.setInt(3, result.getTestStepRecordId());
					p.setString(4, result.getPage());
					p.setString(5, result.getField());
					p.setString(6, result.getExpectedValue());
					p.setString(7, result.getActualValue());
					p.setString(8, result.getStatus());
					p.setInt(9, result.getIteration());
					p.setString(10, result.getDetailRecordNo());
					p.addBatch();
				}
				
				p.executeBatch();
				//conn.commit();
				
			} catch (SQLException e) {
				
				logger.error("ERRoR while persisting validation results for Step [{}], ITeration [{}]", stepRecordId, iteration);
			}finally{
				
				DatabaseHelper.close(p);
				DatabaseHelper.close(conn);
			}
		}
	}
	
	public void updateWSRequestAndResponse(int runId, int testStepRecordId, int iteration, String request, String response) throws DatabaseException {
		Connection conn= DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement("UPDATE RUNRESULT_TS_TXN SET TSTEP_WS_REQ_MSG=?, TSTEP_WS_RES_MSG=? WHERE RUN_ID=? AND TSTEP_REC_ID=? AND TXN_NO=?");
			pst.setString(1, request);
			pst.setString(2, response);
			pst.setInt(3, runId);
			pst.setInt(4, testStepRecordId);
			pst.setInt(5, iteration);
			
			pst.executeUpdate();
			
		} catch (SQLException e) {
			
			logger.error("ERROR updating WS Request and Response", e);
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
	}
	
	
	public void updateRequestLogs(int runId, int testStepRecordId, int iteration, String requestLog) throws DatabaseException {
		Connection conn= DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement("UPDATE RUNRESULT_TS_TXN SET API_REQ_LOG=? WHERE RUN_ID=? AND TSTEP_REC_ID=? AND TXN_NO=?");
			pst.setString(1, requestLog);
			pst.setInt(2, runId);
			pst.setInt(3, testStepRecordId);
			pst.setInt(4, iteration);
			
			pst.executeUpdate();
			
		} catch (SQLException e) {
			
			logger.error("ERROR updating WS Request and Response", e);
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
	}
	
	
	public void abortRun(int runId, String userId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		
		
		try {
			logger.debug("Updating abort status for individual transactions");
			pst = conn.prepareStatement("UPDATE RUNRESULT_TS_TXN SET TSTEP_RESULT=?, TSTEP_MESSAGE=? WHERE RUN_ID=? AND TSTEP_RESULT not in (?,?,?)");
			pst.setString(1, "E");
			pst.setString(2, "Execution aborted by ["+ userId +"]");
			pst.setInt(3, runId);
			pst.setString(4, "S");
			pst.setString(5, "F");
			pst.setString(6, "E");
			
			pst.executeUpdate();
			
			logger.debug("Updating abort status for the run");
			
		} catch (SQLException e) {
			
			logger.error("ERROR Updating abort status for run transactions", e);
			throw new DatabaseException("Could not abort run due to an internal error.");
		} finally {
		
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
		logger.debug("Updating abort status for Run {}", runId);
		try {
			this.endRun(runId, BridgeProcess.ABORTED);
		} catch (DatabaseException e) {
			logger.error("Could not update Run Status to aborted", e);
			throw new DatabaseException("Could not abort run due to an internal error.");
		}
	}
	
	public void abortRun(int runId, String userId, String message) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		
		
		try {
			logger.debug("Updating abort status for individual transactions");
			pst = conn.prepareStatement("UPDATE RUNRESULT_TS_TXN SET TSTEP_RESULT=?, TSTEP_MESSAGE=? WHERE RUN_ID=? AND TSTEP_RESULT not in (?,?,?)");
			pst.setString(1, "E");
			pst.setString(2, message);
			pst.setInt(3, runId);
			pst.setString(4, "S");
			pst.setString(5, "F");
			pst.setString(6, "E");
			
			pst.executeUpdate();
			
			logger.debug("Updating abort status for the run");
			
		} catch (SQLException e) {
			
			logger.error("ERROR Updating abort status for run transactions", e);
			throw new DatabaseException("Could not abort run due to an internal error.");
		} finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
		logger.debug("Updating abort status for Run {}", runId);
		try {
			this.endRun(runId, BridgeProcess.ABORTED);
		} catch (DatabaseException e) {
			logger.error("Could not update Run Status to aborted", e);
			throw new DatabaseException("Could not abort run due to an internal error.");
		}
	}
	
  public void mergeFailedTransactions(int currentRunId, int parentRunId) throws DatabaseException {
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs=null;
		PreparedStatement pst1 = null;
		String tdUIDs="";
		String stepRecIds="";
		try {
		/*Modified by Ashiki for VAPT Fix starts*/ 
			/*String query1="SELECT TSTEP_REC_ID,TSTEP_TDUID FROM RUNRESULT_TS_TXN WHERE RUN_ID="+parentRunId+" AND TSTEP_TDUID NOT IN"
					+ "(SELECT TSTEP_TDUID  FROM RUNRESULT_TS_TXN WHERE RUN_ID="+currentRunId+")";  */
			
			pst1 = conn.prepareStatement("SELECT TSTEP_REC_ID,TSTEP_TDUID FROM RUNRESULT_TS_TXN WHERE RUN_ID=? AND TSTEP_TDUID NOT IN (SELECT TSTEP_TDUID  FROM RUNRESULT_TS_TXN WHERE RUN_ID=?)");
			pst1.setInt(1,parentRunId);
			pst1.setInt(2,currentRunId);
			/*Modified by Ashiki for VAPT Fix ends*/ 
			rs=pst1.executeQuery();
			
			while(rs.next()){
				tdUIDs +="'"+rs.getString("TSTEP_TDUID")+"',";
				stepRecIds+="'"+String.valueOf(rs.getInt("TSTEP_REC_ID"))+"',";
				
			}
			
			tdUIDs=tdUIDs.substring(0, tdUIDs.length()-1);
			stepRecIds=stepRecIds.substring(0, stepRecIds.length()-1);
			/*Modified by Ashiki for VAPT Fix starts*/ 
			
			pst = conn.prepareStatement("INSERT INTO RUNRESULT_TS_TXN(TXN_NO,TSTEP_REC_ID,TSTEP_RESULT,TSTEP_MESSAGE,TSTEP_SCRSHOT,RUN_ID,TSTEP_TDUID,EXEC_START_TIME,EXEC_END_TIME) SELECT TXN_NO,TSTEP_REC_ID,TSTEP_RESULT,TSTEP_MESSAGE,TSTEP_SCRSHOT,?,TSTEP_TDUID,EXEC_START_TIME,EXEC_END_TIME FROM RUNRESULT_TS_TXN WHERE RUN_ID=? AND TSTEP_TDUID NOT IN (SELECT TSTEP_TDUID  FROM RUNRESULT_TS_TXN WHERE RUN_ID=?)");
			pst.setInt(1,currentRunId);
			pst.setInt(2,parentRunId);
			pst.setInt(3,currentRunId);
			/*Modified by Ashiki for VAPT Fix ends*/ 
			pst.execute();
			
		}catch(SQLException e){
			logger.error("ERROR while merging runs", e);
			throw new DatabaseException("Could not merge failures. Please contact Tenjin support.");
		}finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		this.mergeScreenshots(currentRunId, parentRunId,tdUIDs);	

		this.mergeRuntimeValues(currentRunId, parentRunId,tdUIDs);
		this.mergeValidationResults(currentRunId, parentRunId,stepRecIds);
	
	}
  
  public void mergeRuntimeValues(int currentRunId, int parentRunId,String tdUIDs) throws DatabaseException {
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		try {
			//Changes Priyanka
			String query="";
			query ="INSERT INTO TJN_EXEC_OUTPUT(RUN_ID,TDGID,TDUID,TSTEP_REC_ID,ITERATION_NO,DETAIL_NO,FIELD_NAME,FIELD_VALUE) SELECT ?, TDGID, TDUID,TSTEP_REC_ID,ITERATION_NO,DETAIL_NO,FIELD_NAME,FIELD_VALUE FROM TJN_EXEC_OUTPUT WHERE RUN_ID=?AND TDUID NOT IN (SELECT TDUID FROM TJN_EXEC_OUTPUT WHERE RUN_ID=?)";
			pst = conn.prepareStatement(query);
			pst.setInt(1,currentRunId);
			pst.setInt(2,parentRunId);
			pst.setInt(3,currentRunId);
			pst.execute();
		/*Modified by Ashiki for VAPT Fix starts
			pst = conn.prepareStatement("INSERT INTO TJN_EXEC_OUTPUT(RUN_ID,TDGID,TDUID,TSTEP_REC_ID,ITERATION_NO,DETAIL_NO,FIELD_NAME,FIELD_VALUE) SELECT ?, TDGID, TDUID,TSTEP_REC_ID,ITERATION_NO,DETAIL_NO,FIELD_NAME,FIELD_VALUE FROM TJN_EXEC_OUTPUT WHERE RUN_ID=? AND TDUID IN (?)");
			pst.setInt(1,currentRunId);
			pst.setInt(2,parentRunId);
			pst.setString(3,tdUIDs);
			Modified by Ashiki for VAPT Fix ends
			pst.execute();*/ 
		}catch(SQLException e){
			logger.error("ERROR while merging runs", e);
			throw new DatabaseException("Could not merge failures. Please contact Tenjin support.");
		}finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

  public void mergeScreenshots(int currentRunId, int parentRunId,String tdUIDs) throws DatabaseException {
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		try {
			//Changes Priyanka
			String query="";
			query ="INSERT INTO TJN_EXEC_SCRNSHOT(RUN_ID,TDGID,TDUID,TSTEP_REC_ID,ITERATION_NO,SEQ_NO,TYPE_SCRNSHOT,SCRNSHOT,SCR_TIMESTAMP,SCR_DESC) SELECT ?,TDGID,TDUID,TSTEP_REC_ID,ITERATION_NO,SEQ_NO,TYPE_SCRNSHOT,SCRNSHOT,SCR_TIMESTAMP,SCR_DESC FROM TJN_EXEC_SCRNSHOT WHERE RUN_ID=? AND TDUID NOT IN (SELECT TDUID FROM TJN_EXEC_SCRNSHOT WHERE RUN_ID=?)";
			pst = conn.prepareStatement(query);
			pst.setInt(1,currentRunId);
			pst.setInt(2,parentRunId);
			pst.setInt(3,currentRunId);
			pst.execute();
		
			/*Modified by Ashiki for VAPT Fix starts
			pst = conn.prepareStatement("INSERT INTO TJN_EXEC_SCRNSHOT(RUN_ID,TDGID,TDUID,TSTEP_REC_ID,ITERATION_NO,SEQ_NO,TYPE_SCRNSHOT,SCRNSHOT,SCR_TIMESTAMP,SCR_DESC) SELECT ?,TDGID,TDUID,TSTEP_REC_ID,ITERATION_NO,SEQ_NO,TYPE_SCRNSHOT,SCRNSHOT,SCR_TIMESTAMP,SCR_DESC FROM TJN_EXEC_SCRNSHOT WHERE RUN_ID=? AND TDUID IN (?)");
			pst.setInt(1,currentRunId);
			pst.setInt(2,parentRunId);
			pst.setString(3,tdUIDs);
			Modified by Ashiki for VAPT Fix ends
			pst.execute();*/ 
		}catch(SQLException e){
			logger.error("ERROR while merging runs", e);
			throw new DatabaseException("Could not merge failures. Please contact Tenjin support.");
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	

  public void mergeValidationResults(int currentRunId, int parentRunId,String stepRecIds ) throws DatabaseException {
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		try {
		
			/*Modified by Ashiki for VAPT Fix starts*/ 
			
			pst = conn.prepareStatement("INSERT INTO RESVALRESULTS (RUN_ID,RES_VAL_ID,TSTEP_REC_ID,RES_VAL_PAGE,RES_VAL_FIELD,RES_VAL_EXP_VAL,RES_VAL_ACT_VAL,RES_VAL_STATUS,ITERATION,RES_VAL_DETAIL_ID) SELECT ?, RES_VAL_ID,TSTEP_REC_ID,RES_VAL_PAGE,RES_VAL_FIELD,RES_VAL_EXP_VAL,RES_VAL_ACT_VAL,RES_VAL_STATUS,ITERATION,RES_VAL_DETAIL_ID FROM RESVALRESULTS WHERE RUN_ID=? AND TSTEP_REC_ID IN(?)");
			pst.setInt(1,currentRunId);
			pst.setInt(2,parentRunId);
			pst.setString(3,stepRecIds);
			/*Modified by Ashiki for VAPT Fix ends*/ 
			pst.execute();
		}catch(SQLException e){
			logger.error("ERROR while merging runs", e);
			throw new DatabaseException("Could not merge failures. Please contact Tenjin support.");
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}
	
	
	
  public int getParent(int runId)throws DatabaseException
	{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		int parentId=0;
	
		PreparedStatement pst = null;
		ResultSet rs1 = null;
		try {
				/*Modified by Ashiki for VAPT Fix starts*/ 
				pst = conn.prepareStatement("SELECT PARENT_RUN_ID FROM MASTESTRUNS WHERE RUN_ID=?");
				pst.setInt(1, runId);
				/*Modified by Ashiki for VAPT Fix ends*/ 
				rs1=pst.executeQuery();
				if(rs1.next())
				{
					parentId=rs1.getInt("PARENT_RUN_ID");
						 
				}
			
				
				
			
		
		   }catch(SQLException e){
			
			throw new DatabaseException("Could not merge failures. Please contact Tenjin support.");
		}finally{
			
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return parentId;
	}
	
  public String processExecStepStatus(List<String> list) {
		String execStatus="Pass";
		
		if(list.contains("Error")){
			return execStatus="Error";
		}else if(list.contains("Fail")){
			return execStatus="Fail";
		}
		return execStatus;
	}

  public String getStepStatus(int runId,int stepRecId) throws DatabaseException{
	  String execStatus="";      
	  try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);         
			  PreparedStatement pst = conn.prepareStatement("SELECT TSTEP_STATUS FROM V_RUNRESULTS_TS WHERE RUN_ID=? AND TSTEP_REC_ID =?") ){         
		  pst.setInt(1,runId);          
		  pst.setInt(2, stepRecId);       
		  try(ResultSet rs=pst.executeQuery();){   
			  while(rs.next()){                 
				  execStatus=(rs.getString("TSTEP_STATUS"));          
				  }           }      
		  }  catch(SQLException e){       
			  logger.error("could not get step status");      
			  }
	  return execStatus;
	  
  }
 
  public String getDynamicUserPassword(int appId, String softApprover,String userId) throws DatabaseException{
		String password = "";
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		if(conn == null){
			throw new DatabaseException("Invalid or no database connection");
		}
		
		
		PreparedStatement p =null;ResultSet rs = null;
		String query = "SELECT APP_PASSWORD FROM USRAUTCREDENTIALS WHERE APP_ID = ? AND APP_LOGIN_NAME = ? AND USER_ID=?";
		try {
			 p = conn.prepareStatement(query);
			p.setInt(1, appId);
			p.setString(2, softApprover); 
			p.setString(3, userId);
			 rs = p.executeQuery();
			while(rs.next()){
				password = rs.getString("APP_PASSWORD");
			}
			
			if(password == null || password.equalsIgnoreCase("")){
				throw new DatabaseException("Could not find User "+softApprover+" for the Application");
			}
		
		} catch (SQLException e) {
			throw new DatabaseException("ERROR while fetching "+softApprover+" from the Application");
		}catch (Exception e) {
			throw new DatabaseException("ERROR while fetching "+softApprover+" from the Application");
		}
		finally{
			try {
				rs.close();
			} catch (SQLException e) {
			}
			try {
				p.close();
			} catch (SQLException e) {
			}
			try {
				conn.close();
			} catch (SQLException e) {
			}
		}
		return password;
	}
	
	public String getDynamicUser(Connection conn, String fieldName, int runId)
			throws DatabaseException {

		String output = null;
		boolean outputFound = false;
		
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}

		try {
			PreparedStatement pst1 = null;
			String res1 = "";
			ResultSet rs1 = null;
			String[] resVal = null;
			try {
				pst1 = conn
						.prepareStatement("SELECT REPLACE(your_path,'<-',',')as data FROM (SELECT substr(sys_connect_by_path(RUN_ID, '<-'), 3) your_path, connect_by_root(PARENT_RUN_ID) root_parent FROM MASTESTRUNS  WHERE RUN_ID =? CONNECT BY PRIOR RUN_ID = PARENT_RUN_ID ORDER BY RUN_ID) WHERE root_parent=0 or root_parent is NULL");
				pst1.setInt(1, runId);
				rs1 = pst1.executeQuery();

				while (rs1.next()) {
					res1 = rs1.getString("data");
					res1 = res1.replace("null", "");
					resVal=res1.split(",");
					break;
				}
				
			} catch (Exception e) {
				
			} finally {
				rs1.close();
				pst1.close();
			}
/*Modified by Ashiki for VAPT Fix starts*/ 
			PreparedStatement pst2 = null;
			PreparedStatement pst3 = null;
			ResultSet rs2 = null;
			ResultSet rs3 = null;
			try {
			for(int i=0;i<=resVal.length;i++) {
				pst2 = conn
						.prepareStatement("SELECT DISTINCT TSTEP_RESULT FROM RUNRESULT_TS_TXN WHERE RUN_ID =? AND TSTEP_RESULT=?");
				pst2.setString(1, resVal[i]);
				pst2.setString(2, "S");
				rs2 = pst2.executeQuery();
				pst2.close();
				String res = "";
				while (rs2.next()) {
					res = rs2.getString("TSTEP_RESULT");
					break;
				}
				rs2.close();
				
				

				if (res == null || !res.equalsIgnoreCase("S")) {
					throw new DatabaseException(
							"The test step with TDUID has either not been executed or has not completed successfully");
				}
				pst3 = conn
						.prepareStatement("SELECT FIELD_VALUE FROM TJN_EXEC_OUTPUT where RUN_ID =? AND FIELD_NAME LIKE ?  ORDER BY  ROWNUM DESC");

				pst3.setString(1, resVal[i]);
				pst3.setString(2, "%" + fieldName + "%");
				rs3 = pst3.executeQuery();
				pst3.close();
				while (rs3.next()) {
					output = rs3.getString("FIELD_VALUE");
					outputFound = true;
					break;
				}
					pst3.close();
					rs3.close();
			}
		}catch (Exception e) {
			
		}finally {
			try {
			pst2.close();
			if(pst3!=null) {
				pst3.close();
			}
			}finally {
				if(rs3!=null) {
					rs3.close();
				}
				rs2.close();
			}
			
		}
			/*Modified by Ashiki for VAPT Fix ends*/ 
		} catch (Exception e) {
			throw new DatabaseException(
					"Could not fetch Output for Master Record", e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
			}
		}

		if (outputFound) {
			if (output.contains(";")) {
				output = output.replace(";", "");
				output = Utilities.trim(output);
			}
			return output;
		} else {
			throw new DatabaseException(
					"Could not find runtime value for field " + fieldName
							+ " in test step.");
		}
	} 
	/*Added by Lokanath for TENJINCG-1193 starts*/
	public void learnAbortRun(int runId, String userId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		try {
			logger.debug("Updating abort status for individual transactions");
			pst = conn.prepareStatement("UPDATE LRNR_AUDIT_TRAIL SET LRNR_STATUS =?,LRNR_END_TIME=?,LRNR_MESSAGE=? ,LRNR_START_TIME =? WHERE LRNR_RUN_ID =? AND LRNR_STATUS not in(?,?,?)");
			pst.setString(1, BridgeProcess.ABORTED);
			pst.setTimestamp(2, new Timestamp(new Date().getTime()));
			pst.setString(3, "Aborted by ["+ userId +"]");
			pst.setTimestamp(4, new Timestamp(new Date().getTime()));
			pst.setInt(5, runId);
			pst.setString(6, "COMPLETE");
			pst.setString(7, "ERROR");
			pst.setString(8, "ABORTED");
			pst.executeUpdate();
			
			logger.debug("Updating abort status for the run");
			
		} catch (SQLException e) {
			logger.error("ERROR Updating abort status for run transactions", e);
			throw new DatabaseException("Could not abort run due to an internal error.");
		} finally {
		
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
		logger.debug("Updating abort status for Run {}", runId);
		try {
			this.endRun(runId, BridgeProcess.ABORTED);
		} catch (DatabaseException e) {
			logger.error("Could not update Run Status to aborted", e);
			throw new DatabaseException("Could not abort run due to an internal error.");
		}
	}
	/*Added by Pushpalatha for TENJINCG-1106 starts*/
	public void persisteExecAudit(int runId, int prjId) throws DatabaseException {
		
		TenjinReflection tenjinReflection=new TenjinReflection();
		String runData=tenjinReflection.hydrateRunForAudit(runId,prjId);
		
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		
		try {
			/*Modified by Prem for Tenj210-148 start*/
			DatabaseMetaData meta = conn.getMetaData();
			String query=null;
			if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle")){
			query = "UPDATE MASTESTRUNS SET RUN_JSON =? WHERE RUN_ID =? ";
			pst = conn.prepareStatement(query);
			Clob clobOld = pst.getConnection().createClob();
			clobOld.setString(1, runData);
			pst.setClob(1, clobOld);
			}
			else {
			query = "UPDATE MASTESTRUNS SET RUN_JSON =? WHERE RUN_ID =? ";
			pst = conn.prepareStatement(query);
			pst.setString(1, runData);
			}
			/*Modified by Prem for Tenj210-148 End*/
			// int recId = DatabaseHelper.getGlobalSequenceNumber(conn);
			pst.setInt(2, runId);
			pst.execute();
			
			}catch (Exception e) {
				logger.error("Error ", e);
				throw new DatabaseException("Could not insert run json", e);
			} finally {
				
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
		
		
		
		
	}
	
	
	

	public int getPrjId(int runId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		int prjId=0;
	
		PreparedStatement pst = null;
		ResultSet rs1 = null;
		try {
		/*Modified by Ashiki for VAPT Fix starts*/ 
				pst = conn.prepareStatement("SELECT RUN_PRJ_ID FROM MASTESTRUNS WHERE RUN_ID=?");
				pst.setInt(1, runId);
				/*Modified by Ashiki for VAPT Fix ends*/ 
				rs1=pst.executeQuery();
				if(rs1.next())
				{
					prjId=rs1.getInt("RUN_PRJ_ID");
						 
				}
			
		   }catch(SQLException e){
			
			throw new DatabaseException("Could not merge failures. Please contact Tenjin support.");
		}finally{
			
			DatabaseHelper.close(rs1);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return prjId;
	}
	/*Added by Pushpalatha for TENJINCG-1106 ends*/
	
	
}
