/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DeviceHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */
/*

 *//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                	 CHANGED BY                DESCRIPTION
 * 10-Aug-2017				sameer					Mobility	
 */
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.device.RegisteredDevice;
import com.ycs.tenjin.util.Constants;

public class DeviceHelper {
	private static final Logger logger = LoggerFactory
			.getLogger(DeviceHelper.class);

	public RegisteredDevice hydrateDevice(int devRecId)
			throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		ResultSet rs = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		RegisteredDevice rd = null;
		try {

			pst = conn
					.prepareStatement("SELECT * FROM MASDEVICES WHERE DEVICE_REC_ID =?");
			pst.setInt(1, devRecId);
			rs = pst.executeQuery();

			while (rs.next()) {
				rd = new RegisteredDevice();
				rd.setDeviceId(rs.getString("DEVICE_ID"));
				rd.setDeviceName(rs.getString("DEVICE_NAME"));
				rd.setDeviceType(rs.getString("DEVICE_TYPE"));
				rd.setPlatform(rs.getString("DEVICE_PLATFORM"));
				rd.setPlatformVersion(rs.getString("DEVICE_PLATFORM_VERSION"));
				rd.setDeviceRecId(rs.getString("DEVICE_REC_ID"));
				rd.setDeviceStatus(rs.getString("DEVICE_STATUS"));
				rd.setDeviceDescription(rs.getString("DEVICE_DESC"));
			}

			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new DatabaseException("Could not fetch record ", e);
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return rd;
	}

}
