/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AutAuditHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 25-04-2019         	Roshni				    Newly Added For TENJINCG-1038
 */


package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.aut.AutAuditRecord;
import com.ycs.tenjin.util.Constants;

public class AutAuditHelper {

	private static final Logger logger = LoggerFactory.getLogger(AutAuditHelper.class);
	public void persistAuditRecord(AutAuditRecord record) throws DatabaseException {
		
		try (
				Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement("INSERT INTO TJN_AUDIT (AUDIT_ID,ENTITY_RECORD_ID,ENTITY_TYPE,LAST_MODIFIED_BY,LAST_MODIFIED_ON,OLD_DATA,NEW_DATA,ID) VALUES(?,?,?,?,?,?,?,?)");
				){
				pst.setInt(1, DatabaseHelper.getGlobalSequenceNumber(conn));
				pst.setInt(2, record.getEntityRecordId());
				pst.setString(3, record.getEntityType());
				pst.setString(4, record.getLastUpdatedBy());
				pst.setTimestamp(5, new Timestamp(new Date().getTime()));
				pst.setString(6, record.getOldData());
				pst.setString(7,record.getNewData());
				pst.setString(8, record.getId());
				pst.execute();
		}catch(SQLException e) {
			logger.error("Could not insert audit record");
			throw new DatabaseException("Could not insert audit record",e);
		}
	}

}
