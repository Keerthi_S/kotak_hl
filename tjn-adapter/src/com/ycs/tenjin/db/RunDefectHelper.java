/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RunDefectHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              	DESCRIPTION
* 15-Nov-2016           Sriram Sridharan          	Newly Added For 
* 21-Nov-2016			Manish					  	Dtt values while creating defect
* 12-Jan-2017			Manish					  	TENJINCG-5
* 13-Feb-2017			Manish					  	Dtt values while creating defect(defect TENJINCG-118)
* 18-Aug-2017			Sriram Sridharan			T25IT-206
 * 08-09-2017			Sameer Gupta				Rewritten for New Adapter Spec
 * 08-01-2018           Leelaprasad             	TENJINCG-592			
 * 31-01-2018			Preeti						TENJINCG-503
 * 20-04-2018			Preeti						TENJINCG-616
 * 11-07-2018			Preeti						Closed connections
 * 24-08-2018			Preeti						Burgon bank(SQL server)
 * 29-08-2018           Padmavathi                  Closed connections
 * 03-07-2109			Ashiki						TJN27-21
*/


package com.ycs.tenjin.db;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.bridge.pojo.run.UIValidationResult;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.util.Constants;

public class RunDefectHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(RunDefectHelper.class);
	private String REC_STATUS = "A";
	public List<String> getDistinctTransactionFailureMessages(int runId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		List<String> disntMsgList = new ArrayList<String>();
		String tstepMsg = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}
		try{
			pst = conn.prepareStatement("SELECT distinct tstep_message FROM RUNRESULT_TS_TXN WHERE RUN_ID = ? and tstep_result='F'");
			pst.setInt(1, runId);

			rs = pst.executeQuery();
			
			while(rs.next()){
				tstepMsg = new String(rs.getString("TSTEP_MESSAGE"));
				disntMsgList.add(tstepMsg);
			}
			
		}
	
		catch(Exception e){
			throw new DatabaseException("Could not fetch field information for field " + runId,e);
		}
		finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
		return disntMsgList;
	}
	
	
	public List<StepIterationResult> getTransactionsFailedWithMessage(int runId, String message) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		List<StepIterationResult> txnList = new ArrayList<StepIterationResult>();
		StepIterationResult  runRes= null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}

		try{
			pst = conn.prepareStatement("SELECT * FROM RUNRESULT_TS_TXN WHERE RUN_ID = ? and tstep_result='F' and tstep_message =?");
			pst.setInt(1, runId);
			pst.setString(2, message);

			rs = pst.executeQuery();
			while(rs.next()){
				runRes = new StepIterationResult();
				runRes.setTsRecId(rs.getInt("TSTEP_REC_ID"));
				runRes.setTduid(rs.getString("TSTEP_TDUID"));
				runRes.setIterationNo(rs.getInt("TXN_NO"));
				System.out.println("---Txn Rec Id---"+runRes.getTsRecId());
				System.out.println("---Txn No---"+runRes.getIterationNo());
				System.out.println("---Txn TDUIF--"+runRes.getTduid());
				txnList.add(runRes);
			}
			
		}
		catch(Exception e){
			throw new DatabaseException("Could not fetch field information for field " + runId,e);
		}
		finally{
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return txnList;
		

	}
	
	
	public List<StepIterationResult> getTransactionsWithValidationFailures(int runId) throws DatabaseException{
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}

		ArrayList<StepIterationResult> results= new ArrayList<StepIterationResult>();

		PreparedStatement rPst = null;
		ResultSet oRs = null;
     try{
		rPst = conn.prepareStatement("SELECT DISTINCT TSTEP_REC_ID, ITERATION FROM RESVALRESULTS WHERE RUN_ID = ? AND RES_VAL_STATUS='Fail'");
		rPst.setInt(1, runId);
		oRs = rPst.executeQuery();
		while(oRs.next()){
			StepIterationResult r = new StepIterationResult();
			r.setIterationNo(oRs.getInt("ITERATION"));
			r.setTsRecId(oRs.getInt("TSTEP_REC_ID")); 
			
			List<ValidationResult> validationResults=new ArrayList<ValidationResult>();
			PreparedStatement pst2 = null;
			ResultSet rs2 = null;
		 try{
		   pst2=conn.prepareStatement("select rtt.*, rvr.RES_VAL_PAGE, rvr.RES_VAL_FIELD,rvr.RES_VAL_EXP_VAL,rvr.RES_VAL_ACT_VAL from RUNRESULT_TS_TXN rtt, RESVALRESULTS rvr " 
		  +" where rvr.run_id = rtt.run_id and rvr.tstep_Rec_id = rtt.tstep_Rec_id and rvr.iteration = rtt.txn_no and rvr.res_val_status = 'Fail' and rtt.run_id =? and rtt.tstep_rec_id = ? and rtt.txn_no=?");
							pst2.setInt(1, runId);
							pst2.setInt(2, r.getTsRecId());
							pst2.setInt(3, r.getIterationNo());
							rs2 = pst2.executeQuery();
							
							while(rs2.next()){
								r.setTduid(rs2.getString("TSTEP_TDUID"));
								ValidationResult	obj = new ValidationResult();
								obj.setPage(rs2.getString("RES_VAL_PAGE"));
								obj.setField(rs2.getString("RES_VAL_FIELD"));
								obj.setExpectedValue(rs2.getString("RES_VAL_EXP_VAL"));
								obj.setActualValue(rs2.getString("RES_VAL_ACT_VAL"));

								validationResults.add(obj);
								System.out.println("---Res val----"+obj.getPage()+","+obj.getField()+","+obj.getExpectedValue()+","+obj.getActualValue());
							}
							r.setValidationResults(validationResults);
							results.add(r);
							
		
		}catch(Exception e){
			throw new DatabaseException("Could not fetch field information for field " + runId,e);
		}
		 finally{
			 	DatabaseHelper.close(rs2);
				DatabaseHelper.close(pst2);
		 }
       }
     }
     catch(Exception e){
			throw new DatabaseException("Could not fetch field information for field " + runId,e);
		}
		finally{
			
			DatabaseHelper.close(oRs);
			DatabaseHelper.close(rPst);
			DatabaseHelper.close(conn);
		}
       return results;
       
     }
	
	public List<String> getSeverityDefinition(int testStepRecordId, int projectId) throws DatabaseException{
		List<String> severities = new ArrayList<String>();
		
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}
		PreparedStatement rPst = null;
		ResultSet oRs = null;
     try{
		rPst = conn.prepareStatement("SELECT PROPERTY_VALUE FROM TJN_DTT_PROPERTIES WHERE PROPERTY_NAME='Severity' "
				+ "AND INSTANCE_NAME= (SELECT PRJ_DEF_MANAGER FROM TJN_PRJ_AUTS WHERE PRJ_ID=? AND APP_ID IN (SELECT APP_ID FROM TESTSTEPS WHERE TSTEP_REC_ID=?)) ORDER BY PROPERTY_SCORE DESC");
		rPst.setInt(1, projectId); 
		rPst.setInt(2, testStepRecordId);
		oRs = rPst.executeQuery();
		while(oRs.next())
		{
			severities.add(oRs.getString("PROPERTY_VALUE"));
		}
     }
		catch(Exception e)
		{
			throw new DatabaseException("Could not fetch field information for field");
			} finally {
				
				DatabaseHelper.close(oRs);
				DatabaseHelper.close(rPst);
				DatabaseHelper.close(conn);
			}
		
		return severities;
	}
     

    
    public boolean getScreenShot(int runId, Connection conn,int seqVal) throws DatabaseException{//, Connection conn
 	   boolean flag = false;
 	  PreparedStatement pst1 = null;
 	 ResultSet steps = null;
 	   if(conn == null){
				throw new DatabaseException("Invalid database connection");
			}
 	   
 	   try {
				String screens="select * from tjn_exec_scrnshot where run_id= ?";//
				pst1=conn.prepareStatement(screens);
				pst1.setInt(1, runId);
				steps=pst1.executeQuery();
				Path path=null;
				while(steps.next())
					{
					String fileName =steps.getString("TDGID")+"_"+steps.getString("TDUID")+"_"+steps.getInt("SEQ_NO");
					Blob blob =	steps.getBlob("SCRNSHOT");
					if(blob != null){
						byte barr[] = blob.getBytes(1,(int)blob.length());
						path = Paths.get(TenjinConfiguration.getProperty("MEDIA_REPO_PATH")+"\\Defect\\"+runId);//TenjinConfiguration.getProperty("MEDIA_REPO_PATH")
						if (!Files.exists(path))
						{
							Files.createDirectories(path);
						}
							
						OutputStream out = new FileOutputStream(path +"\\"+ fileName+".png");
					    out.write(barr);
					    out.close();
					 }
						PreparedStatement pst = null;
					    try {
					    	int attachmentId = getMaxAttachmentIdDefect(conn);
							/*Modified by Ashiki for VAPT Fix starts*/ 
					    	/*String query = "INSERT INTO TJN_RUN_DEF_ATTACHMENTS (REC_ATTACHMENT_ID,DEF_REC_ID,FILE_PATH,ATT_FILE_NAME,ATT_POSTED) "
									+ " VALUES(?,?,?,?,?)";*/
							pst = conn.prepareCall("INSERT INTO TJN_RUN_DEF_ATTACHMENTS (REC_ATTACHMENT_ID,DEF_REC_ID,FILE_PATH,ATT_FILE_NAME,ATT_POSTED) "
									+ " VALUES(?,?,?,?,?)");
							pst.setInt(1, attachmentId);
                            pst.setInt(2, seqVal);
							pst.setString(3, path + "\\" + fileName+".png");
							pst.setString(4, fileName+".png");
							pst.setString(5, "No");
							/*Modified by Ashiki for VAPT Fix ends*/ 
							pst.execute();
							DatabaseHelper.close(pst);
							logger.info("Attachments added successfully in the path [{}]",path);
						} catch (Exception e) {
							logger.error("Error ", e);
						}
					    finally{
					    	DatabaseHelper.close(pst);
					    }
					 flag = true; 
					}
			   } catch (Exception e) {
				   try {
					conn.rollback();
				} catch (SQLException e1) {}
				   throw new DatabaseException("Could not persist details while get screeen for a defect ",e);
			   }finally{
				   DatabaseHelper.close(steps);
				   DatabaseHelper.close(pst1);
			   }
 	   return flag;
    }
    
    public boolean getScreenShot(int runId, Connection conn,int seqVal, String tdUid, int stepRecordId) throws DatabaseException{//, Connection conn
   	   boolean flag = false;
   	   if(conn == null){
  				throw new DatabaseException("Invalid database connection");
  			}
   	   PreparedStatement pst1 = null;
   	   ResultSet steps = null;
   	   try {
  				String screens="select * from tjn_exec_scrnshot where run_id= ? and tstep_rec_id=? and tduid=?";//
  				pst1=conn.prepareStatement(screens);
  				pst1.setInt(1, runId);
  				pst1.setString(3, tdUid);
  				pst1.setInt(2, stepRecordId);
  				steps=pst1.executeQuery();
  				Path path=null;
  				while(steps.next())
  					{
  					String fileName =steps.getString("TDGID")+"_"+steps.getString("TDUID")+"_"+steps.getInt("SEQ_NO");
  					Blob blob =	steps.getBlob("SCRNSHOT");
  					if(blob != null){
  						byte barr[] = blob.getBytes(1,(int)blob.length());
  						path = Paths.get(TenjinConfiguration.getProperty("MEDIA_REPO_PATH")+"\\Defect\\"+runId);//TenjinConfiguration.getProperty("MEDIA_REPO_PATH")
  						if (!Files.exists(path))
  						{
  							Files.createDirectories(path);
  						}
  							
  						OutputStream out = new FileOutputStream(path +"\\"+ fileName+".png");
  					    out.write(barr);
  					    out.close();
  					 }
  						PreparedStatement pst = null;
  					    try {
  					    	int attachmentId = getMaxAttachmentIdDefect(conn);
  					    	/*Modified by Ashiki for VAPT Fix starts*/ 
  							/*String query = "INSERT INTO TJN_RUN_DEF_ATTACHMENTS (REC_ATTACHMENT_ID,DEF_REC_ID,FILE_PATH,ATT_FILE_NAME,ATT_POSTED) "
  								+ " VALUES(?,?,?,?,?)";*/
  							pst = conn.prepareCall("INSERT INTO TJN_RUN_DEF_ATTACHMENTS (REC_ATTACHMENT_ID,DEF_REC_ID,FILE_PATH,ATT_FILE_NAME,ATT_POSTED) "
  	  								+ " VALUES(?,?,?,?,?)");
  							pst.setInt(1, attachmentId);
                              pst.setInt(2, seqVal);
  							pst.setString(3, path + "\\" + fileName+".png");
  							pst.setString(4, fileName+".png");
  							pst.setString(5, "No");
  							/*Modified by Ashiki for VAPT Fix ends*/ 
  							pst.execute();
  							DatabaseHelper.close(pst);
  							logger.info("Attachments added successfully in the path [{}]",path);
  						} catch (Exception e) {
  							logger.error("Error ", e);
  						}
  					    finally{
  					    	DatabaseHelper.close(pst);
  					    }
  					 flag = true; 
  					}
  			   } catch (Exception e) {
  				   try {
  					conn.rollback();
  				} catch (SQLException e1) {}
  				   throw new DatabaseException("Could not persist details while get screeen for a defect ",e);
  			   }finally{
  				 DatabaseHelper.close(steps);
  				 DatabaseHelper.close(pst1);
  			   }
   	   return flag;
      }
 
    public int getMaxRecIdDefect() throws DatabaseException{
    	Connection conn = null;
    	conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
    	String screens=null;
    	PreparedStatement pst1 = null;
    	ResultSet steps = null;
    	int defRecId;
    	try {
    		DatabaseMetaData meta = conn.getMetaData(); 
			if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				screens = "select SQ_RUN_DEFECTS.nextval maxVal from dual";
			else
				screens = "select next value for SQ_RUN_DEFECTS as maxVal";
    		pst1=conn.prepareStatement(screens);
    		steps=pst1.executeQuery();
    		defRecId = 0;
    		while(steps.next()){
    			defRecId=steps.getInt("maxVal");

    			System.out.println("--defRecId----"+defRecId);
    			return defRecId;

    		}
    	} catch (SQLException e) {
    		
    		logger.error("ERROR --> " + e.getMessage(), e);
    		throw new DatabaseException("Internal error occurred. Please contact Tenjin Administrator");
    	}
    	finally{
    		DatabaseHelper.close(steps);
    		DatabaseHelper.close(pst1);
    		DatabaseHelper.close(conn);
    	}
		return defRecId;

    }
    
    public int getMaxRecIdDefect(Connection conn) throws DatabaseException{
    	PreparedStatement pst1 = null;
    	ResultSet steps = null;
    	int defRecId;
    	String screens = null;
    	try {
    		DatabaseMetaData meta = conn.getMetaData(); 
			if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				screens = "select SQ_RUN_DEFECTS.nextval maxVal from dual";
			else
				screens = "select next value for SQ_RUN_DEFECTS as maxVal";
    		pst1=conn.prepareStatement(screens);
    		steps=pst1.executeQuery();
    		defRecId = 0;
    		while(steps.next()){
    			defRecId=steps.getInt("maxVal");

    			System.out.println("--defRecId----"+defRecId);
    			
    			
    			
    		}
    		
    	} catch (SQLException e) {
    		
    		logger.error("ERROR --> " + e.getMessage(), e);
    		 try{
				   conn.close();
			   }catch(Exception ignore) {}
    		throw new DatabaseException("Internal error occurred. Please contact Tenjin Administrator");
    	}
    	finally{
    		DatabaseHelper.close(steps);
    		DatabaseHelper.close(pst1);
    	}
		return defRecId;

    }
    
    public int getMaxLinkIdDefect(Connection conn) throws DatabaseException{
    	PreparedStatement pst1 = null;
    	ResultSet steps = null;
    	int defLinkId;
    	String screens = null;
    	try {
    		DatabaseMetaData meta = conn.getMetaData(); 
			if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				screens = "select SQ_RUN_DEFECTS_LINKAGE.nextval maxVal from dual";
			else
				screens = "select next value for SQ_RUN_DEFECTS_LINKAGE as maxVal";
    		pst1=conn.prepareStatement(screens);
    		steps=pst1.executeQuery();
    		defLinkId = 0;
    		while(steps.next()){
    			defLinkId=steps.getInt("maxVal");

    			System.out.println("--defLinkId----"+defLinkId);
    			
    		}
    	} catch (SQLException e) {
    		
    		logger.error("ERROR --> " + e.getMessage(), e);
    		 try{
				   conn.close();
			   }catch(Exception ignore) {}
    		throw new DatabaseException("Internal error occurred. Please contact Tenjin Administrator");
    	}
    	finally{
    		DatabaseHelper.close(steps);
    		DatabaseHelper.close(pst1);
    	}
		return defLinkId;

    }
	       
    public int getMaxAttachmentIdDefect(Connection conn) throws DatabaseException{
    	PreparedStatement pst1 = null;
    	ResultSet steps = null;
    	int defAttachmentId;
    	String screens = null;
    	try {
    		DatabaseMetaData meta = conn.getMetaData(); 
			if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				screens = "select SQ_RUN_DEF_ATTACHMENTS.nextval maxVal from dual";
			else
				screens = "select next value for SQ_RUN_DEF_ATTACHMENTS as maxVal";
    		pst1=conn.prepareStatement(screens);
    		steps=pst1.executeQuery();
    		defAttachmentId = 0;
    		while(steps.next()){
    			defAttachmentId=steps.getInt("maxVal");

    			System.out.println("--defAttachmentId----"+defAttachmentId);
    			
    		}
    	} catch (SQLException e) {
    		
    		logger.error("ERROR --> " + e.getMessage(), e);
    		 try{
				   conn.close();
			   }catch(Exception ignore) {}
    		throw new DatabaseException("Internal error occurred. Please contact Tenjin Administrator");
    	}
    	finally{
    		DatabaseHelper.close(steps);
    		DatabaseHelper.close(pst1);
    	}
		return defAttachmentId;

    }
    
		public List<String> getDefaultSeverity(){
			List<String> serList = new ArrayList<String>();
			serList.add("High");
			serList.add("Medium");
			serList.add("Low");
			return serList;
		}
	
	public void createTransactionFailureDefects(int runId, int projectId, String userId, Map<String, List<StepIterationResult>> failureMap) throws DatabaseException
	{
		Connection conn = null;
		conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		RunDefectHelper rHelper=new RunDefectHelper();
		String highSeverity= "";
		if(conn == null){
			try {
				throw new DatabaseException("Invalid database connection");
			} catch (DatabaseException e) {

				logger.error("Error ", e);
			}
		}

	/*	try {
			conn.setAutoCommit(false);
		} catch (SQLException e2) {
			
			logger.error("ERROR while setting auto commit to false", e2);
			DatabaseHelper.close(conn);
			throw new DatabaseException("An internal error occurred. Please contact Tenjin Support.");
		}*/
		PreparedStatement pst =null;
		
		try {
			for(Map.Entry<String, List<StepIterationResult>> entry : failureMap.entrySet()){
				int seqVal= rHelper.getMaxRecIdDefect(conn);
				String summary =entry.getKey();
				List<StepIterationResult> val = entry.getValue();
				/*Modified by Ashiki for VAPT Fix starts*/ 
				
				 pst = conn.prepareCall("INSERT INTO TJN_RUN_DEFECTS (DEF_REC_ID,RUN_ID,DEF_SUMMARY,DEF_SEVERITY,DEF_CREATED_BY,DEF_CREATED_ON,"
							+ " DEF_STATUS,DEF_DESC,DTT_INSTANCE,DTT_PROJECT,REC_STATUS)"
							+ " VALUES(?,?,?,?,?,?,?,?,?,?,?)");
				Map<Integer, String> failedTransactions = new HashMap<Integer, String>();
				for(StepIterationResult setDef : val){
					String sTdUid = setDef.getTduid();
					int stepRecordId = setDef.getTsRecId();
					PreparedStatement pstLink =null;
					try{
						List<String> severities=rHelper.getSeverityDefinition(setDef.getTsRecId(), projectId);
						if(severities.size()==0){
							severities = rHelper.getDefaultSeverity();
							highSeverity = severities.get(0);
						}else{
							highSeverity=severities.get(0);
						}
						pst.setInt(1, seqVal);
						pst.setInt(2, runId);
						pst.setString(3, summary);
						pst.setString(4, highSeverity);
						pst.setString(5, userId);
						pst.setTimestamp(6, new Timestamp(new Date().getTime()));
						pst.setString(7, "New");
						pst.setString(8, summary);//DEF_DESC

						
						int linkId= rHelper.getMaxLinkIdDefect(conn);
						pstLink = conn.prepareCall("INSERT INTO TJN_RUN_DEFECT_LINKAGE (LINK_ID, DEF_REC_ID, DEF_PROJECT_ID, DEF_RUN_ID,"
								+ " DEF_TC_REC_ID, DEF_TSTEP_REC_ID, DEF_TDUID, DEF_APP_ID, DEF_FUNCTION_CODE, DEF_API_CODE,DEF_OPERATION) "
								+ " VALUES(?,?,?,?,?,?,?,?,?,?,?)");
						pstLink.setInt(1, linkId);
						pstLink.setInt(2, seqVal);
						pstLink.setInt(4, runId);
						 
						PreparedStatement pst1 = null;
						PreparedStatement pstDtt = null;
						ResultSet rs = null;
						ResultSet steps = null;
						try {
							String query1="SELECT * FROM TJN_PRJ_AUTS WHERE PRJ_ID=? AND APP_ID=?";
							pstDtt = conn.prepareStatement(query1);
							
							
							
							
							String testSteps="SELECT a.app_name as APP_NAME,b.TC_REC_ID as TC_REC_ID,b.TSTEP_REC_ID as TSTEP_REC_ID,"
									+ " b.FUNC_CODE as FUNC_CODE,b.APP_ID as APP_ID,c.tc_prj_id as TC_PRJ_ID, b.TSTEP_OPERATION "
									+ " FROM masapplication a, teststeps b, testcases c"
									+ "  where a.app_id = b.app_id and c.tc_rec_id = b.tc_Rec_id and b.TSTEP_REC_ID= ?";
							pst1=conn.prepareStatement(testSteps);
							pst1.setInt(1, setDef.getTsRecId());
							steps=pst1.executeQuery();
							while(steps.next())
							{
								try {
								pstLink.setInt(3, steps.getInt("TC_PRJ_ID"));// DEF_PROJECT_ID or PRJ_ID ????
								pstLink.setInt(5, steps.getInt("TC_REC_ID"));//DEF_TC_REC_ID
								pstLink.setInt(6, steps.getInt("TSTEP_REC_ID"));//DEF_TSTEP_REC_ID
								pstLink.setString(7, setDef.getTduid());//DEF_TDUID
								pstLink.setInt(8, steps.getInt("APP_ID"));//DEF_APP_ID
								pstLink.setString(9, steps.getString("FUNC_CODE"));//DEF_FUNCTION_CODE
								pstLink.setString(10, steps.getString("FUNC_CODE"));
								pstLink.setString(11, steps.getString("TSTEP_OPERATION"));
								/*Modified by Ashiki for VAPT Fix ends*/
								pstDtt.setInt(1, steps.getInt("TC_PRJ_ID"));
								pstDtt.setInt(2, steps.getInt("APP_ID"));
								rs = pstDtt.executeQuery();
									while(rs.next()){
										pst.setString(9, rs.getString("PRJ_DEF_MANAGER"));
										pst.setString(10, rs.getString("PRJ_DM_PROJECT"));
									}
								} finally{
									DatabaseHelper.close(rs);
									DatabaseHelper.close(pstDtt);
								}
								

							}

						} catch (Exception e) {
							logger.error("ERROR --> ", e);
							throw new DatabaseException("Unable to fetch details for  "+runId+ " and "+setDef.getTsRecId(),e);
						}finally{
							DatabaseHelper.close(steps);
							DatabaseHelper.close(pst1);
						}

						pstLink.execute();
						
					}catch(Exception e){
						conn.rollback();
						throw new DatabaseException("Unable to fetch details for  "+runId+ " and "+setDef.getTsRecId(),e);
					}finally{
						DatabaseHelper.close(pstLink);
					}

					failedTransactions.put(stepRecordId, sTdUid);

				}

				pst.setString(11, REC_STATUS);
				
				boolean insert=pst.execute();
				
				if(insert==false)
				{
				
					for(int stepRecordId:failedTransactions.keySet()){
						boolean isScreenshotPresent = rHelper.getScreenShot(runId, conn, seqVal, failedTransactions.get(stepRecordId), stepRecordId);
						if(!isScreenshotPresent){
							logger.warn("No screenshots were available for Transaction [{}]", failedTransactions.get(stepRecordId));
						}
					}	
				}
				
				//conn.commit();

			}
		} catch (Exception e) {
			logger.error("ERROR while persisting defect details", e);
			throw new DatabaseException("Could not persist details for "+runId,e);
		}finally{
			
			DatabaseHelper.close(pst);
			DatabaseHelper.closeConnection(conn);
		}

	}
	
	
	public void createValidationFailureDefects(int runId, int projectId, String userId, List<StepIterationResult> failures) throws DatabaseException
	{
		String description = "";
		 RunDefectHelper rHelper=new RunDefectHelper();
		 Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		 String highSeverity= "";
		 
		for(ValidationResult result: failures.get(0).getValidationResults()){
			description = description + "Page ["+result.getPage()+"] Field ["+result.getField()+"] Expected ["+result.getExpectedValue()+"] Actul ["+result.getActualValue()+"] \n";
		}
		   if(conn == null){
				throw new DatabaseException("Invalid database connection");
			}
		   PreparedStatement pst =null;
		   PreparedStatement pstLink =null;
		   for(StepIterationResult setDef : failures){
			   try{
				   String tdUid = setDef.getTduid();
					int stepRecordId = setDef.getTsRecId();
				   List<String> severities=rHelper.getSeverityDefinition(setDef.getTsRecId(), projectId);
				   if(severities.size()==0){
					   severities =rHelper.getDefaultSeverity();
					   highSeverity = severities.get(1);
				   }else{
					   int len =severities.size()/2;
					   highSeverity=severities.get(len);
				   }
				   int seqVal=rHelper.getMaxRecIdDefect(conn);
				   /*Modified by Ashiki for VAPT Fix starts*/
				   pst = conn.prepareCall("INSERT INTO TJN_RUN_DEFECTS (DEF_REC_ID,RUN_ID,DEF_SUMMARY,DEF_SEVERITY,DEF_CREATED_BY,DEF_CREATED_ON,"
						   + " DEF_STATUS,DEF_DESC,DTT_INSTANCE,DTT_PROJECT,REC_STATUS) "
						   + " VALUES(?,?,?,?,?,?,?,?,?,?,?)");
				   pst.setInt(1, seqVal);
				   pst.setInt(2, runId);
				   pst.setString(3, "Validation Failures");
				   pst.setString(4, highSeverity);
				   pst.setString(5, userId);
				   pst.setTimestamp(6, new Timestamp(new Date().getTime()));
				   pst.setString(7, "New");

				   int linkId= rHelper.getMaxLinkIdDefect(conn);
				   
				   pstLink = conn.prepareCall("INSERT INTO TJN_RUN_DEFECT_LINKAGE (LINK_ID, DEF_REC_ID, DEF_PROJECT_ID, DEF_RUN_ID,"
						   + " DEF_TC_REC_ID, DEF_TSTEP_REC_ID, DEF_TDUID, DEF_APP_ID, DEF_FUNCTION_CODE, DEF_API_CODE,DEF_OPERATIOn) "
						   + " VALUES(?,?,?,?,?,?,?,?,?,?,?)");
				   pstLink.setInt(1, linkId);
				   pstLink.setInt(2, seqVal);//DEF_REC_ID
				   pstLink.setInt(4, runId);//DEF_RUN_ID
				   PreparedStatement pstDtt = null;
				   PreparedStatement pst1 = null;
				   ResultSet steps = null;
				   ResultSet rs = null;
				   try {
					   String query1="SELECT * FROM TJN_PRJ_AUTS WHERE PRJ_ID=? AND APP_ID=?";
					   pstDtt = conn.prepareStatement(query1);

					 
					   String testSteps="SELECT a.app_name as APP_NAME,b.TC_REC_ID as TC_REC_ID,b.TSTEP_REC_ID as TSTEP_REC_ID,"
							   + " b.FUNC_CODE as FUNC_CODE,b.APP_ID as APP_ID,c.tc_prj_id as TC_PRJ_ID , b.TSTEP_OPERATION"
							   + " FROM masapplication a, teststeps b, testcases c"
							   + "  where a.app_id = b.app_id and c.tc_rec_id = b.tc_Rec_id and b.TSTEP_REC_ID= ?";
					   pst1=conn.prepareStatement(testSteps);
					   pst1.setInt(1, setDef.getTsRecId());
					   steps=pst1.executeQuery();
					   while(steps.next())
					   {

						   try {
							pstLink.setInt(3, steps.getInt("TC_PRJ_ID"));// DEF_PROJECT_ID or PRJ_ID ????
							pstLink.setInt(5, steps.getInt("TC_REC_ID"));//DEF_TC_REC_ID
							pstLink.setInt(6, steps.getInt("TSTEP_REC_ID"));//DEF_TSTEP_REC_ID
							pstLink.setString(7, setDef.getTduid());//DEF_TDUID
							pstLink.setInt(8, steps.getInt("APP_ID"));//DEF_APP_ID
							pstLink.setString(9, steps.getString("FUNC_CODE"));//DEF_FUNCTION_CODE
							pstLink.setString(10, steps.getString("FUNC_CODE"));
							pstLink.setString(11, steps.getString("TSTEP_OPERATION"));
							pstDtt.setInt(1, steps.getInt("TC_PRJ_ID"));
							pstDtt.setInt(2, steps.getInt("APP_ID"));
							rs = pstDtt.executeQuery();
							/*Modified by Ashiki for VAPT Fix ends*/
							while (rs.next()) {
								pst.setString(9, rs.getString("PRJ_DEF_MANAGER"));
								pst.setString(10, rs.getString("PRJ_DM_PROJECT"));
							} 
						} finally {
							DatabaseHelper.close(rs);
							DatabaseHelper.close(pstDtt);
						}

					   }

				   } catch (Exception e) {
					  
					   DatabaseHelper.close(conn);
					   throw new DatabaseException("Unable to fetch details for  "+runId+ " and "+setDef.getTsRecId(),e);
					  
				   }finally{
					   DatabaseHelper.close(steps);
						DatabaseHelper.close(pst1);
				   }


				   pst.setString(8, description);//DEF_DESC	
				   pst.setString(11, REC_STATUS);
				   pstLink.execute();
				   
					boolean insert=pst.execute();
				   if(insert==false)
				   {
					   boolean isScreenShotPresent = rHelper.getScreenShot(runId,conn,seqVal,tdUid, stepRecordId);
					   if(!isScreenShotPresent){
						   conn.rollback();
						   logger.info("Attachments are not existed  for {}",runId);
					   }/*else{
						   conn.commit();
					   }*/
				   }

			   }catch(Exception e){
				
				   throw new DatabaseException("Could not persist details for "+runId,e);
			   } finally{
				   DatabaseHelper.close(pstLink);
				   DatabaseHelper.close(pst);
				   DatabaseHelper.close(conn);
			   }
		   }
		
	}
	
	public void createUIValidationFailureDefects(int runId, int projectId,String userId, int testStepRecordId, List<UIValidationResult> failures, String pageName, String validationType) throws DatabaseException
	{
		String description = "";
		 RunDefectHelper rHelper=new RunDefectHelper();
		 Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		 String highSeverity= "";
		 
		for(UIValidationResult result: failures){
			description += "Field: " + result.getField() + "\n";
			description += "Expected: " + result.getExpectedValue() + "\n";
			description += "Actual: " + result.getActualValue() + "\n\n";
		}
		   if(conn == null){
				throw new DatabaseException("Invalid database connection");
			}
		   PreparedStatement pst =null;
		   PreparedStatement pstLink =null;
		   try{
			   List<String> severities=rHelper.getSeverityDefinition(testStepRecordId, projectId);
			   if(severities.size()==0){
				   severities =rHelper.getDefaultSeverity();
				   highSeverity = severities.get(1);
			   }else{
				   int len =severities.size()/2;
				   highSeverity=severities.get(len);
			   }
			   int seqVal=rHelper.getMaxRecIdDefect(conn);
			   /*Modified by Ashiki for VAPT Fix starts*/
			   pst = conn.prepareCall("INSERT INTO TJN_RUN_DEFECTS (DEF_REC_ID,RUN_ID,DEF_SUMMARY,DEF_SEVERITY,DEF_CREATED_BY,DEF_CREATED_ON,"
					   + " DEF_STATUS,DEF_DESC,DTT_INSTANCE,DTT_PROJECT,REC_STATUS) "
					   + " VALUES(?,?,?,?,?,?,?,?,?,?,?)");
			   pst.setInt(1, seqVal);
			   pst.setInt(2, runId);
			   pst.setString(3, "UI Validation Failure: " + validationType + " failed on page " + pageName);
			   pst.setString(4, highSeverity);
			   pst.setString(5, userId);
			   pst.setTimestamp(6, new Timestamp(new Date().getTime()));
			   pst.setString(7, "New");

			   int linkId= rHelper.getMaxLinkIdDefect(conn);
			   
			   pstLink = conn.prepareCall("INSERT INTO TJN_RUN_DEFECT_LINKAGE (LINK_ID, DEF_REC_ID, DEF_PROJECT_ID, DEF_RUN_ID,"
					   + " DEF_TC_REC_ID, DEF_TSTEP_REC_ID, DEF_TDUID, DEF_APP_ID, DEF_FUNCTION_CODE, DEF_API_CODE,DEF_OPERATIOn) "
					   + " VALUES(?,?,?,?,?,?,?,?,?,?,?)");
			   pstLink.setInt(1, linkId);
			   pstLink.setInt(2, seqVal);//DEF_REC_ID
			   pstLink.setInt(4, runId);//DEF_RUN_ID
			   PreparedStatement pstDtt = null;
			   PreparedStatement pst1 = null;
			   ResultSet steps = null;
			   ResultSet rs = null;
			   try {
				   String query1="SELECT * FROM TJN_PRJ_AUTS WHERE PRJ_ID=? AND APP_ID=?";
				   pstDtt = conn.prepareStatement(query1);

				  
				   String testSteps="SELECT a.app_name as APP_NAME,b.TC_REC_ID as TC_REC_ID,b.TSTEP_REC_ID as TSTEP_REC_ID,"
						   + " b.FUNC_CODE as FUNC_CODE,b.APP_ID as APP_ID,c.tc_prj_id as TC_PRJ_ID , b.TSTEP_OPERATION"
						   + " FROM masapplication a, teststeps b, testcases c"
						   + "  where a.app_id = b.app_id and c.tc_rec_id = b.tc_Rec_id and b.TSTEP_REC_ID= ?";
				   pst1=conn.prepareStatement(testSteps);
				   pst1.setInt(1, testStepRecordId);
				   steps=pst1.executeQuery();
				   while(steps.next())
				   {

					   try {
						pstLink.setInt(3, steps.getInt("TC_PRJ_ID"));// DEF_PROJECT_ID or PRJ_ID ????
						pstLink.setInt(5, steps.getInt("TC_REC_ID"));//DEF_TC_REC_ID
						pstLink.setInt(6, steps.getInt("TSTEP_REC_ID"));//DEF_TSTEP_REC_ID
						pstLink.setString(7, "");//DEF_TDUID
						pstLink.setInt(8, steps.getInt("APP_ID"));//DEF_APP_ID
						pstLink.setString(9, steps.getString("FUNC_CODE"));//DEF_FUNCTION_CODE
						pstLink.setString(10, steps.getString("FUNC_CODE"));
						pstLink.setString(11, steps.getString("TSTEP_OPERATION"));
						/*Modified by Ashiki for VAPT Fix ends*/
						pstDtt.setInt(1, steps.getInt("TC_PRJ_ID"));
						pstDtt.setInt(2, steps.getInt("APP_ID"));
						rs = pstDtt.executeQuery();
						/*Modified by Ashiki for VAPT Fix ends*/
						while (rs.next()) {
							pst.setString(9, rs.getString("PRJ_DEF_MANAGER"));
							pst.setString(10, rs.getString("PRJ_DM_PROJECT"));
						} 
					} finally {
						// 
						DatabaseHelper.close(rs);
						DatabaseHelper.close(pstDtt);
					}

				   }




			   } catch (Exception e) {
				   
				   DatabaseHelper.close(conn);
				   throw new DatabaseException("Unable to fetch details for  "+runId+ " and "+testStepRecordId,e);
				  
			   }finally{
				   DatabaseHelper.close(steps);
					DatabaseHelper.close(pst1);
			   }


			   pst.setString(8, description);//DEF_DESC	
			   pst.setString(11, REC_STATUS);
			   pstLink.execute();
			   
				boolean insert=pst.execute();
			   if(insert==false)
			   {
				   boolean isScreenShotPresent = rHelper.getScreenShot(runId,conn,seqVal);
				   if(!isScreenShotPresent){
					   conn.rollback();
					   logger.info("Attachments are not existed  for {}",runId);
				   }/*else{
					   conn.commit();
				   }*/
			   }

		   }catch(Exception e){
			   
			   throw new DatabaseException("Could not persist details for "+runId,e);
		   } finally{
			   DatabaseHelper.close(pstLink);
			   DatabaseHelper.close(pst);
			   DatabaseHelper.close(conn);
		   }
	   
		
	}
		
}