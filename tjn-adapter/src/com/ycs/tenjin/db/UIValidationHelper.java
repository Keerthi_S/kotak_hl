/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UIValidationHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright &copy 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 24-Jul-2017          Sriram                	Newly added for Epic TENJINCG-283
 * 24-Jul-2017			Sriram				 	TENJINCG-285
 * 24-Jul-2017			Sriram				 	TENJINCG-309
 * 27-Jul-2017			Sriram Sridharan		TENJINCG-311
 * 27-Jul-2017			Sriram Sridharan		TENJINCG-312
 * 28-Jul-2017			Pushpalatha				TENJINCG-316
 * 23-Mar-2018          Padmavathi              TENJINCG-613
 * 20-04-2018			Preeti  				TENJINCG-616
 * 11-07-2018			Preeti					Closed connections
 * 12-10-2018           Padmavathi              TENJINCG-847
 * 23-10-2018           Padmavathi              TENJINCG-847
 * 24-10-2018			Preeti					TENJINCG-850
 * 25-10-2018			Preeti					TENJINCG-850
 */

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.pojo.project.UIValidation;
import com.ycs.tenjin.bridge.pojo.project.UIValidationType;
import com.ycs.tenjin.bridge.pojo.run.UIValidationResult;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class UIValidationHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(UIValidationHelper.class);
	public List<UIValidationType> hydrateAllUIValidationTypes() throws DatabaseException {
		List<UIValidationType> types= new ArrayList<UIValidationType>();
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		
		try {
			pst = conn.prepareStatement("SELECT * FROM MASUIVALDEF order by UI_VAL_ID");
			
			rs = pst.executeQuery();
			
			while(rs.next()) {
				UIValidationType type = this.mapFieldsForUIValidationType(rs);
				types.add(type);
			}
			
		} catch (SQLException e) {
			logger.error("ERROR while hydrating UI Validation Types",e);
			throw new DatabaseException("Could not fetch validation types information due to an internal error. Please contact Tenjin Support.");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return types;
	}
	
	public UIValidationType hydrateUIValidationType(Connection conn, int uiValidationTypeId) throws DatabaseException {
		
		UIValidationType type = null;
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			pst = conn.prepareStatement("SELECT * FROM MASUIVALDEF WHERE UI_VAL_ID=?");
			pst.setInt(1, uiValidationTypeId);
			rs = pst.executeQuery();
			while(rs.next()) {
				type = this.mapFieldsForUIValidationType(rs);
			}
			
		} catch (SQLException e) {
			logger.error("ERROR while hydrating UI Validation Type {}", uiValidationTypeId,e);
			throw new DatabaseException("Could not fetch validation type information due to an internal error. Please contact Tenjin Support.");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		
		return type;
		
	}
	
	public UIValidationType hydrateUIValidationType(int uiValidationTypeId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		UIValidationType type=null;
		try{
			type=this.hydrateUIValidationType(conn, uiValidationTypeId);
		}finally{
			DatabaseHelper.close(conn);
		}
		return type;
		
	}
	
	public void persistUIValidation(UIValidation validation) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			DatabaseMetaData meta = conn.getMetaData(); 
			if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				pst = conn.prepareStatement("SELECT SEQ_UI_VALIDATION.NEXTVAL AS SEQ_NO FROM DUAL");
			else
				pst = conn.prepareStatement("select next value for SEQ_UI_VALIDATION as SEQ_NO");
			rs = pst.executeQuery();
			while(rs.next()) {
				validation.setRecordId(rs.getInt("SEQ_NO"));
			}
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			
			pst = conn.prepareStatement("insert into tstep_ui_validations (rec_id,tstep_Rec_id,ui_val_id,page_area,pre_steps,val_steps) values (?,?,?,?,?,?)");
			pst.setInt(1, validation.getRecordId());
			pst.setInt(2, validation.getStepRecordId());
			pst.setInt(3, validation.getType().getId());
			pst.setString(4, validation.getPageArea());
			pst.setString(5, validation.getPreSteps());
			pst.setString(6, validation.getSteps());
			pst.execute();
			
		} catch (SQLException e) {
			
			logger.error("ERROR - Could not create UI validation record", e);
			throw new DatabaseException("Could not create record due to an internal error. Please contact Tenjin Support.");
		} finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
	}
	
	public UIValidation hydrateUIValidationStep (int uiValRecId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		UIValidation val = null;
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			pst = conn.prepareStatement("SELECT * FROM TSTEP_UI_VALIDATIONS WHERE REC_ID=?");
			pst.setInt(1, uiValRecId);
			
			rs = pst.executeQuery();
			
			while(rs.next()) {
				val =this.mapFieldsForUIValidation(rs);
				int valId = rs.getInt("UI_VAL_ID");
				UIValidationType type = this.hydrateUIValidationType(conn, valId);
				if(type != null) {
					val.setType(type);
				}
			}
		} catch (SQLException e) {
			
			logger.error("ERROR - could not hydrate ui validation record", e);
			throw new DatabaseException("Could not fetch record due to an internal error. Please contact Tenjin Support.");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return val;
	}
	
	public void updateUIValidationStep(int valStepRecordId, String preStepsJson, String valStepsJson) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		
		try {
			pst = conn.prepareStatement("UPDATE TSTEP_UI_VALIDATIONS SET PRE_STEPS=?, VAL_STEPS=? WHERE REC_ID=?");
			pst.setString(1, preStepsJson);
			pst.setString(2, valStepsJson);
			pst.setInt(3, valStepRecordId);
			pst.executeUpdate();
			pst.close();
		} catch (SQLException e) {
			
			logger.error("ERROR updating record", e);
			throw new DatabaseException("Could not update record. Please contact Tenjin Support.");
		} finally {
			
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		
	}

	
	public List<UIValidation> hydrateUIValidationsForStep(int stepRecordId) throws DatabaseException {
		List<UIValidation> validations = new ArrayList<UIValidation>();
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			pst = conn.prepareStatement("SELECT * FROM TSTEP_UI_VALIDATIONS WHERE TSTEP_REC_ID=? order by rec_id");
			pst.setInt(1, stepRecordId);
			
			rs = pst.executeQuery();
			
			while(rs.next()) {
				UIValidation val = this.mapFieldsForUIValidation(rs);
				int valId = rs.getInt("UI_VAL_ID");
				UIValidationType type = this.hydrateUIValidationType(conn, valId);
				if(type != null) {
					val.setType(type);
				}
				
				validations.add(val);
			}
		} catch (SQLException e) {
			
			logger.error("ERROR hydrating validations for step {}", stepRecordId, e);
			throw new DatabaseException("Could not fetch UI Validations due to an internal error. Please contact Tenjin Support.");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return validations;
	}
	
	
	
	public List<UIValidation> hydrateUIValidationsForStep(Connection conn, int stepRecordId) throws DatabaseException {
		List<UIValidation> validations = new ArrayList<UIValidation>();
		
		try (
				PreparedStatement pst = conn.prepareStatement("SELECT * FROM TSTEP_UI_VALIDATIONS WHERE TSTEP_REC_ID=? order by rec_id");
			){
			pst.setInt(1, stepRecordId);
			
			try(ResultSet rs = pst.executeQuery();){
			
			while(rs.next()) {
				UIValidation val = this.mapFieldsForUIValidation(rs);
				int valId = rs.getInt("UI_VAL_ID");
				UIValidationType type = this.hydrateUIValidationType(conn, valId);
				if(type != null) {
					val.setType(type);
				}
				
				validations.add(val);
			}
			} 
		}catch (SQLException e) {
			
			logger.error("ERROR hydrating validations for step {}", stepRecordId, e);
			throw new DatabaseException("Could not fetch UI Validations due to an internal error. Please contact Tenjin Support.");
		} 
		return validations;
	}
	
	public void persistUIValidationResults(int runId, int stepRecordId, List<UIValidationResult> results) throws DatabaseException {
		
		if(results == null || results.size() < 1) {
			logger.warn("No UI Validation results to persist.");
			return;
		}
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		
		/*try {
			//conn.setAutoCommit(false);
		} catch (SQLException e1) {
			
			logger.warn("ERROR setting autoCommit to false", e1);
		}
		*/
		PreparedStatement pst = null;
		
		try {
			logger.debug("Clearing existing UI Validation results for step {}", stepRecordId);
			pst = conn.prepareStatement("Delete from UIValResults Where Tstep_Rec_id=? and run_id=?");
			pst.setInt(1,stepRecordId);
			pst.setInt(2, runId);
			pst.execute();
			/*pst.close();*/
		} catch (SQLException e) {
			
			logger.warn("ERROR clearing existing UI Validation records. Data may be inconsistent", e);
		}
		finally{
				DatabaseHelper.close(pst);
		}
		
		boolean success = false;
		
		try {
			logger.debug("Inserting new results...");
			pst = conn.prepareStatement("Insert Into UIValResults (RUN_ID,UI_VAL_REC_ID,TSTEP_REC_ID,UI_VAL_PAGE,UI_VAL_FIELD,UI_VAL_EXPECTED,UI_VAL_ACTUAL,UI_VAL_STATUS,ITERATION,UI_VAL_DETAIL_ID) Values (?,?,?,?,?,?,?,?,?,?)");
			
			logger.debug("Preparting batch inserts...");
			for(UIValidationResult result : results) {
				pst.setInt(1, runId);
				pst.setInt(2, result.getUiValRecId());
				pst.setInt(3, stepRecordId);
				pst.setString(4, result.getPage());
				pst.setString(5, result.getField());
				pst.setString(6, result.getExpectedValue());
				pst.setString(7, result.getActualValue());
				pst.setString(8, result.getStatus());
				pst.setInt(9, result.getIteration());
				pst.setString(10, result.getDetailRecordNo());
				pst.addBatch();
			}
			
			logger.debug("Executing batch");
			pst.executeBatch();
			success = true;
		} catch (SQLException e) {
			
			logger.error("ERROR inserting UI Validation results", e);	
			throw new DatabaseException("Could not insert UI Validation results due to an internal error. Please contact Tenjin Support.");
		}  finally {
			if(!success) {
				try {
					logger.debug("Rolling back...");
					conn.rollback();
				}catch(Exception ignore) {} finally {
					
					DatabaseHelper.close(pst);
					DatabaseHelper.close(conn);
				}
			
			}
			
			
		}
	}
	
	
	
	public Map<String, Map<String, List<UIValidation>>> hydrateUIValidationsForStep(int stepRecordId, String groupBy) throws DatabaseException {
		List<UIValidation> validations = this.hydrateUIValidationsForStep(stepRecordId);
		
		Map<String, Map<String, List<UIValidation>>> valMap = new TreeMap<String, Map<String,List<UIValidation>>>();
		
		if(validations != null) {
			if(Utilities.trim(groupBy).equalsIgnoreCase("page")) {
				valMap = this.groupValidationsByPageArea(validations);
			}else {
				valMap = this.groupValidationsByType(validations);
			}
		}
		
		return valMap;
	}
	
	public Map<String, Map<String, List<UIValidation>>> hydrateUIValidationsForStepWithResults(int stepRecordId, int runId,  String groupBy) throws DatabaseException {
		List<UIValidation> validations = this.hydrateUIValidationsForStepWithResults(stepRecordId, runId);
		
		Map<String, Map<String, List<UIValidation>>> valMap = new TreeMap<String, Map<String,List<UIValidation>>>();
		
		if(validations != null) {
			if(Utilities.trim(groupBy).equalsIgnoreCase("page")) {
				valMap = this.groupValidationsByPageArea(validations);
			}else {
				valMap = this.groupValidationsByType(validations);
			}
		}
		
		return valMap;
	}
	
	private Map<String, Map<String, List<UIValidation>>> groupValidationsByPageArea(List<UIValidation> validations) {
		Map<String, Map<String, List<UIValidation>>> valMap = new TreeMap<String, Map<String,List<UIValidation>>>();
		
		for(UIValidation val : validations) {
			if(val.getType() != null) {
				Map<String, List<UIValidation>> pageMap = valMap.get(val.getPageArea());
				if(pageMap == null) {
					pageMap = new TreeMap<String, List<UIValidation>>();
					valMap.put(val.getPageArea(), pageMap);
				}
				
				if(val.getType() != null) {
					List<UIValidation> pageVals = valMap.get(val.getPageArea()).get(val.getType().getName());
					if(pageVals != null ) {
						pageVals.add(val);
					}else {
						pageVals = new ArrayList<UIValidation>();
						pageVals.add(val);
					}
					valMap.get(val.getPageArea()).put(val.getType().getName(), pageVals);
				}
				
			}
		}
		
		return valMap;
	}
	
	private Map<String, Map<String, List<UIValidation>>> groupValidationsByType(List<UIValidation> validations) {
		Map<String, Map<String, List<UIValidation>>> valMap = new TreeMap<String, Map<String,List<UIValidation>>>();
		
		for(UIValidation val : validations) {
			if(val.getType() != null) {
				Map<String, List<UIValidation>> typeMap = valMap.get(val.getType().getName());
				if(typeMap == null) {
					typeMap = new TreeMap<String, List<UIValidation>>();
					valMap.put(val.getType().getName(), typeMap);
				}
				
				List<UIValidation> pageVals = valMap.get(val.getType().getName()).get(val.getPageArea());
				if(pageVals != null) {
					pageVals.add(val);
				}else {
					pageVals = new ArrayList<UIValidation>();
					pageVals.add(val);
				}
				valMap.get(val.getType().getName()).put(val.getPageArea(), pageVals);				
				
			}
		}
		
		return valMap;
	}
	
	public List<UIValidation> hydrateUIValidationsForStepWithResults(int stepRecordId, int runId) throws DatabaseException {
		List<UIValidation> validations = new ArrayList<UIValidation>();
		
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			pst = conn.prepareStatement("SELECT * FROM TSTEP_UI_VALIDATIONS WHERE TSTEP_REC_ID=? order by rec_id");
			pst.setInt(1, stepRecordId);
			
			rs = pst.executeQuery();
			
			while(rs.next()) {
				UIValidation val = this.mapFieldsForUIValidation(rs);
				int valId = rs.getInt("UI_VAL_ID");
				
				UIValidationType type = this.hydrateUIValidationType(conn, valId);
				if(type != null) {
					val.setType(type);
				}
				
				List<UIValidationResult> results = this.hydrateResultsForUIValidation(conn, val.getRecordId(), runId);
				val.setResults(results);
				
				validations.add(val);
			}
		} catch (SQLException e) {
			
			logger.error("ERROR hydrating validations for step {}", stepRecordId, e);
			throw new DatabaseException("Could not fetch UI Validations due to an internal error. Please contact Tenjin Support.");
		} finally {
			
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		
		return validations;
	}
	
	public List<UIValidationResult> hydrateResultsForUIValidation(Connection conn, int uiValidationId, int runId)  throws DatabaseException {
		List<UIValidationResult> results = new ArrayList<UIValidationResult>();
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			pst = conn.prepareStatement("SELECT * FROM UIVALRESULTS WHERE RUN_ID=? AND UI_VAL_rEC_ID=?");
			pst.setInt(1, runId);
			pst.setInt(2, uiValidationId);
			rs  = pst.executeQuery();
			
			while(rs.next()) {
				UIValidationResult result = this.mapFieldsForUIValidationResult(rs);
				results.add(result);
			}
			
		} catch (SQLException e) {
			
			logger.error("ERROR getting results for UI validation {}", e);
			throw new DatabaseException("Could not get UI Validation results");
		}
		finally{
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
		return results;
	}
	
	public void deleteUIValidationStep(String[] uiRecId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
			try {
				
				logger.debug("Setting auto-commit to false");
				/*Modified by Ashiki for VAPT Fix starts*/
				/*String uiRecIds = "";*/
				for(int i=0; i <uiRecId.length; i++) {
					pst = conn.prepareStatement("DELETE FROM TSTEP_UI_VALIDATIONS WHERE REC_ID =?");
					pst.setString(1, uiRecId[i]);
					/*uiRecIds += "'" + uiRecId[i] + "'"; 
					if(i < uiRecId.length-1) {
						uiRecIds += ",";
					}*/
					/*Modified by Ashiki for VAPT Fix ends*/
					pst.execute();
					DatabaseHelper.close(pst);
				}
				
				DatabaseHelper.commit(conn);
			} catch (SQLException e) {
				logger.error("ERROR while deleting multiple ui validation steps", e);
				DatabaseHelper.rollback(conn);			
				throw new DatabaseException("Could not delete ui validation steps", e);
			} finally {
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			}
	}
	
	private  UIValidationType mapFieldsForUIValidationType(ResultSet rs) throws SQLException{
		UIValidationType type = new UIValidationType();
		type.setId(rs.getInt("UI_VAL_ID"));
		type.setName(rs.getString("UI_VAL_NAME"));
		type.setDescription(rs.getString("UI_VAL_DESC"));
		type.setTarget(rs.getString("UI_TARGET"));
		type.setType(rs.getString("UI_VAL_TYPE"));
		type.setFormula(rs.getString("UI_FORMULA"));
		type.setDefaultExpectedValues(rs.getString("UI_DEF_EXPECTED_VALUES"));
		
		return type;
		
	}
	private UIValidation mapFieldsForUIValidation(ResultSet rs) throws SQLException{
		UIValidation val = new UIValidation();
		val.setRecordId(rs.getInt("REC_ID"));
		val.setStepRecordId(rs.getInt("TSTEP_REC_ID"));
		val.setPageArea(rs.getString("PAGE_AREA"));
		val.setPreSteps(rs.getString("PRE_STEPS"));
		val.setSteps(rs.getString("VAL_STEPS"));
		
		return val;
		
	}
	
	private UIValidationResult mapFieldsForUIValidationResult(ResultSet rs) throws SQLException{
		UIValidationResult result = new UIValidationResult();
		result.setPage(rs.getString("UI_VAL_PAGE"));
		result.setField(rs.getString("UI_VAL_FIELD"));
		result.setExpectedValue(rs.getString("UI_VAL_EXPECTED"));
		result.setActualValue(rs.getString("UI_VAL_ACTUAL"));
		result.setStatus(rs.getString("UI_VAL_STATUS"));
		
		return result;
	}
	
	public void persistUIValidationSteps(List<UIValidation> validations,int stepRecId) throws DatabaseException{
			try(Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);){
				//conn.setAutoCommit(false);
				this.persistUIValidationSteps(conn, validations, stepRecId);
				//conn.commit();
				} catch (SQLException e) {
					logger.error("ERROR - Could not create UI validation record", e);
					throw new DatabaseException("Could not create record due to an internal error. Please contact Tenjin Support.");
				} 
	}
	
	public int getUIValidationRecordId(Connection conn) throws DatabaseException{
		int recId=0;
	try{
		DatabaseMetaData meta = conn.getMetaData();
		logger.info("getting record id from SEQ_UI_VALIDATION");
		if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle")){
			try(PreparedStatement pst=conn.prepareStatement("SELECT SEQ_UI_VALIDATION.NEXTVAL AS SEQ_NO FROM DUAL");
					ResultSet rs = pst.executeQuery();) {
				while(rs.next()) {
					recId=(rs.getInt("SEQ_NO"));
				}
			}catch(Exception e){
				logger.error("error while fetching record id");
				throw new DatabaseException("Could not create record due to an internal error. Please contact Tenjin Support."); 
			}
		} else{
			try(PreparedStatement pst=conn.prepareStatement("select next value for SEQ_UI_VALIDATION as SEQ_NO");
					ResultSet rs = pst.executeQuery();) {
					while(rs.next()) {
						recId=(rs.getInt("SEQ_NO"));
					}
			}catch(Exception e){
				logger.error("error while fetching record id");
				throw new DatabaseException("Could not create record due to an internal error. Please contact Tenjin Support."); 
			}
		}
		}catch (SQLException e) {
			logger.error("ERROR - Could not get database product name", e);
			throw new DatabaseException("Could not create record due to an internal error. Please contact Tenjin Support.");
		} 
		return recId;
		
	}
	
	public void persistUIValidationSteps(Connection conn, List<UIValidation> validations,int stepRecId) throws DatabaseException{
	       
		try(
			PreparedStatement pst=conn.prepareStatement("INSERT INTO TSTEP_UI_VALIDATIONS (REC_ID,TSTEP_REC_ID,UI_VAL_ID,PAGE_AREA,PRE_STEPS,VAL_STEPS) VALUES (?,?,?,?,?,?)");){
				logger.info("persisting ui validations steps ");
				for(UIValidation validation:validations){
					int recId=this.getUIValidationRecordId(conn);
					pst.setInt(1, recId);
					pst.setInt(2, stepRecId);
					pst.setInt(3, validation.getType().getId());
					pst.setString(4, validation.getPageArea());
					pst.setString(5, validation.getPreSteps());
					pst.setString(6, validation.getSteps());
					pst.addBatch();
				}
					pst.executeBatch();
			} catch (SQLException e) {
				logger.error("ERROR - Could not create UI validation record", e);
				throw new DatabaseException("Could not create record due to an internal error. Please contact Tenjin Support.");
			} 
	}
}
