/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SchedulerHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 14-11-2016			Nagababu / Sahana		Added for TJN_24_03 (Scheduling)
 * 01-12-2016			Sahana					Req#TJN_243_03
 * 19-12-2016			Nagababu				Req#TEN-124
 * 14-07-2017			Gangadhar Badagi		TENJINCG-263
 * 17-07-2017			Gangadhar Badagi		To fetch AUT Login Type,Browser Type and Screen Shot option
 * 18-07-2017		    Gangadhar Badagi		added for Learn API
 * 21-07-2017		    Gangadhar Badagi		TENJINCG-308
 * 17-08-2017          	Leelaprasad             T25IT-60
 * 18-08-2017        	Leelaprasad             T25IT-132
 * 21-09-2017           Gangadhar Badagi		TENJINCG-348
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 12-04-2018			Preeti					TENJINCG-104
 * 20-04-2018			Preeti					TENJINCG-616
 * 22-06-2018           Padmavathi              T251IT-112
 * 22-06-2018			Preeti					T251IT-120
 * 05-07-2018			Preeti					T251IT-181
 * 12-07-2018			Pushpalatha				TENJINCG-563(closing connection)
 * 08-10-2018           Padmavathi              TENJINCG-851 
 * 02-05-2019			Roshni					TENJINCG-1046
 * 09-07-2019			Preeti					TV2.8R2-41
 * 17-03-2020			Lokanath				TENJINCG-1184
 * 19-04-2021           Paneendra               TENJINCG-1268
 */


/*
 * Added file by sahana for Req#TJN_24_03 on 14/09/2016
 */
package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.scheduler.SchMapping;
import com.ycs.tenjin.scheduler.Scheduler;
import com.ycs.tenjin.util.Constants;

public class SchedulerHelper {
	private static final Logger logger = LoggerFactory.getLogger(SchedulerHelper.class);

	public ArrayList<Scheduler> hydrateAllSchedulers(String status,int projectId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pst=null;
		ResultSet rs =null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		ArrayList<Scheduler> sch = new ArrayList<Scheduler>();
		/*Added by paneendra for TENJINCG-1268 starts */
		DateFormat formatterUTC = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
		/*formatterUTC.setTimeZone(TimeZone.getTimeZone("UTC"));*/
		formatterUTC.setTimeZone(TimeZone.getDefault());

		DateFormat formatterIST = new SimpleDateFormat("dd-MMM-yyyy");
		DateFormat formatterToIST = new SimpleDateFormat("HH:mm");
		formatterToIST.setTimeZone(TimeZone.getTimeZone("IST"));
		formatterIST.setTimeZone(TimeZone.getTimeZone("IST"));
		/*Added by paneendra for TENJINCG-1268 ends */

		try {
			DatabaseMetaData meta = conn.getMetaData();
			String query=null;
			
			if(status.equalsIgnoreCase("Running")){
				status="Executing";
			}
			if(status.equalsIgnoreCase("All")){
				
				if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
					query="SELECT T.MESSAGE,T.SCH_ID,TO_CHAR(T.SCH_DATE,'DD Mon, yyyy') as SCH_DATE,T.SCH_TIME,T.ACTION,T.REG_CLIENT,T.STATUS,T.CREATED_BY,TO_CHAR(T.CREATED_ON,'DD-MON-YYYY') as CREATED_ON,T.RUN_ID,T.PROJECT_ID,T.SCH_RECURRING,T.SCH_TASKNAME,SCH_SCREENSHOT_OPTION FROM TJN_SCHEDULER T WHERE T.ACTION='Execute' AND T.PROJECT_ID=? ORDER BY SCH_ID DESC";
				else
					query="SELECT T.MESSAGE,T.SCH_ID,REPLACE(CONVERT(VARCHAR(11), T.SCH_DATE, 106), ' ', ' ') as SCH_DATE,T.SCH_TIME,T.ACTION,T.REG_CLIENT,T.STATUS,T.CREATED_BY,REPLACE(CONVERT(VARCHAR(11), T.CREATED_ON, 106), ' ', '-') as CREATED_ON,T.RUN_ID,T.PROJECT_ID,T.SCH_RECURRING,T.SCH_TASKNAME,SCH_SCREENSHOT_OPTION FROM TJN_SCHEDULER T WHERE T.ACTION='Execute' AND T.PROJECT_ID=? ORDER BY SCH_ID DESC";
				
				pst=conn.prepareStatement(query);
				pst.setInt(1, projectId);
			}
			else{
				
				if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
					query="SELECT T.MESSAGE,T.SCH_ID,TO_CHAR(T.SCH_DATE,'DD Mon, 	yyyy') as SCH_DATE,T.SCH_TIME,T.ACTION,T.REG_CLIENT,T.STATUS,T.CREATED_BY,TO_CHAR(T.CREATED_ON,'DD-MON-YYYY') as CREATED_ON,T.RUN_ID,T.PROJECT_ID,T.SCH_RECURRING,T.SCH_TASKNAME,SCH_SCREENSHOT_OPTION FROM TJN_SCHEDULER T WHERE T.ACTION='Execute' AND T.STATUS=? AND T.PROJECT_ID=? ORDER BY SCH_ID DESC";
				else
					query="SELECT T.MESSAGE,T.SCH_ID,REPLACE(CONVERT(VARCHAR(11), T.SCH_DATE, 106), ' ', ' ') as SCH_DATE,T.SCH_TIME,T.ACTION,T.REG_CLIENT,T.STATUS,T.CREATED_BY,REPLACE(CONVERT(VARCHAR(11), T.CREATED_ON, 106), ' ', '-') as CREATED_ON,T.RUN_ID,T.PROJECT_ID,T.SCH_RECURRING,T.SCH_TASKNAME,SCH_SCREENSHOT_OPTION FROM TJN_SCHEDULER T WHERE T.ACTION='Execute' AND T.STATUS=? AND T.PROJECT_ID=? ORDER BY SCH_ID DESC";
				
				pst=conn.prepareStatement(query);
				pst.setString(1, status);
				pst.setInt(2, projectId);
			}
			rs = pst.executeQuery();
			while (rs.next()) {
				Scheduler sch1 = new Scheduler();

				sch1.setSchedule_Id(rs.getInt("SCH_ID"));
				/*Added by paneendra for TENJINCG-1268 starts */
				String schdatetime=rs.getString("SCH_DATE")+"  "+rs.getString("SCH_TIME");
				/*Modified by Priyanka during DB Migration starts*/
				String conschdatetime=schdatetime.replace(',', '-').replaceFirst(" ","-").replaceFirst(" ","");
				/*Modified by Priyanka during DB Migration ends*/
				Date date = formatterUTC.parse(conschdatetime);

				String istdate=formatterIST.format(date);
				String isttime=formatterToIST.format(date);
				sch1.setSch_date(istdate.replace('-', ' '));
				sch1.setSch_time(isttime);
				
				//sch1.setSch_date(rs.getString("SCH_DATE"));
				//sch1.setSch_time(rs.getString("SCH_TIME"));
				/*Added by paneendra for TENJINCG-1268 ends */
				sch1.setAction(rs.getString("ACTION"));
				sch1.setReg_client(rs.getString("REG_CLIENT"));
				sch1.setStatus(rs.getString("STATUS"));
				sch1.setCreated_by(rs.getString("CREATED_BY"));
				sch1.setCreated_on(rs.getString("CREATED_ON"));
				sch1.setRun_id(rs.getInt("RUN_ID"));
				sch1.setMessage(rs.getString("MESSAGE"));
				sch1.setProjectId(rs.getInt("PROJECT_ID"));
				sch1.setSchRecur(rs.getString("SCH_RECURRING"));
				sch1.setTaskName(rs.getString("SCH_TASKNAME"));
				sch1.setScreenShotOption(rs.getInt("SCH_SCREENSHOT_OPTION"));
				if(sch1.getReg_client().equalsIgnoreCase("-1")){
					sch1.setReg_client("NA");
				}
				sch.add(sch1);
			}
			
		} catch (Exception e) {
			logger.error("Could not fetch records due to "+e.getMessage());
			throw new DatabaseException("Could not fetch records", e);
		} finally {
			try {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {

				logger.error("Error ", e);
			}
		}

		return sch;

	}

	public void deleteSchedule(String schid) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {

			pst = conn.prepareStatement("DELETE FROM TJN_SCHEDULER WHERE SCH_ID =?");
			pst.setString(1, schid);
			pst.executeUpdate();

		} catch (Exception e) {
			throw new DatabaseException("Could not delete record", e);
		} finally {
			try {
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Could not delete records due to "+e.getMessage());
				logger.error("Error ", e);
			}
		}
	}

	public void deleteAllSchedules(String[] schid)throws DatabaseException {

		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement prest=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try {
			for( int i = 0 ; i < schid.length; i++ )
			{
		/*Modified by Ashiki for VAPT Fix starts*/

				prest = conn.prepareStatement("DELETE FROM TJN_SCHEDULER WHERE SCH_ID=?");
				prest.setString(1, schid[i]);
				prest.executeUpdate();
				DatabaseHelper.close(prest);

			}
		
			/*Modified by Ashiki for VAPT Fix ends*/
		} catch (Exception e) {
			logger.error("Could not delete records due to "+e.getMessage());
			throw new DatabaseException("Could not delete all schedules", e);
		}

		finally {
			try {
				DatabaseHelper.close(prest);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Error ", e);
			}
		}
	}

	public int checkSch_Task(Scheduler sch,int tsRecId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement p2=null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		int counter=0;
		String status=null;
		try {
			if(sch.getAction().equalsIgnoreCase("Learn")){
				p2=conn.prepareStatement("SELECT STATUS FROM TJN_SCHEDULER WHERE sch_id =(SELECT MAX(sch_id) FROM TJN_SCHEDULER SCH,MASREGCLIENTS MAS,LRNR_AUDIT_TRAIL LR WHERE LR.LRNR_USER_ID=SCH.CREATED_BY AND MAS.RC_HOST=LR.LRNR_IP_ADDRESS AND SCH.REG_CLIENT=? AND SCH.SCH_DATE=? AND SCH.SCH_TIME=?)");
				p2.setString(1, sch.getReg_client());
				p2.setString(2, sch.getSch_date());
				p2.setString(3, sch.getSch_time());
				rs = p2.executeQuery();
				if(rs.next()) { 
					status=rs.getString("STATUS");
					counter++;
				}
				
			}
			else if(sch.getAction().equalsIgnoreCase("Execute")){
				
				if(sch.getType()!=null &&(sch.getType().equalsIgnoreCase("1") || sch.getType().equalsIgnoreCase("TestCase"))){
					pst=conn.prepareStatement("SELECT STATUS FROM TJN_SCHEDULER WHERE SCH_ID = (SELECT MAX(SCH_ID) FROM TJN_SCHEDULER SCH,MASTESTRUNS MAS WHERE SCH.PROJECT_ID = MAS.RUN_PRJ_ID AND SCH.PROJECT_ID = ? AND SCH.REG_CLIENT = ? AND SCH.SCH_DATE = ? AND SCH.SCH_TIME = ?)");
					pst.setInt(1, sch.getProjectId());
					pst.setString(2, sch.getReg_client());
					pst.setString(3, sch.getSch_date());
					pst.setString(4, sch.getSch_time());
				}else if(sch.getType()!=null &&(sch.getType().equalsIgnoreCase("2") || sch.getType().equalsIgnoreCase("TestSet"))){
				pst=conn.prepareStatement("SELECT STATUS FROM TJN_SCHEDULER WHERE SCH_ID = (SELECT MAX(SCH_ID) FROM TJN_SCHEDULER SCH, MASTESTRUNS MAS WHERE SCH.PROJECT_ID = MAS.RUN_PRJ_ID AND SCH.PROJECT_ID = ? AND SCH.REG_CLIENT = ? AND MAS.RUN_TS_REC_ID = ? AND SCH.SCH_DATE = ? AND SCH.SCH_TIME = ?)");
				pst.setInt(1, sch.getProjectId());
				pst.setString(2, sch.getReg_client());
				pst.setInt(3, tsRecId);
				pst.setString(4, sch.getSch_date());
				pst.setString(5, sch.getSch_time());
				}
				rs=pst.executeQuery();
				if(rs.next()){
					status=rs.getString("STATUS");
					counter++;
				}
		
			}
			else{
				p2=conn.prepareStatement("SELECT STATUS FROM TJN_SCHEDULER WHERE sch_id =(SELECT MAX(sch_id) FROM TJN_SCHEDULER SCH,MASREGCLIENTS MAS WHERE SCH.REG_CLIENT=? AND SCH.SCH_DATE=? AND SCH.SCH_TIME=?)");
				p2.setString(1, sch.getReg_client());
				p2.setString(2, sch.getSch_date());
				p2.setString(3, sch.getSch_time());
				rs = p2.executeQuery();
				if(rs.next()) { 
					status=rs.getString("STATUS");
					counter++;
				}
		
			}
			if(counter>0){
				if(status.equalsIgnoreCase("Cancelled")){
					counter=0;
				}
			}
		}catch (Exception e) {
			logger.error("Could not  create schedule due to "+e.getMessage());
			logger.error("Error ", e);

			throw new DatabaseException("Could not create schedule", e);
		}finally {
			try {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(p2);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Could not  close connection due to "+e.getMessage());
				logger.error("Error ", e);
			}
		}
		return counter;
	}

	public Scheduler persistSchedule(Scheduler sch) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst1=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		try{
			pst1=conn.prepareStatement("SELECT coalesce(MAX(SCH_ID),0) AS SCH_ID FROM TJN_SCHEDULER");
			rs=pst1.executeQuery();
			if(rs.next()){
				sch.setSchedule_Id(rs.getInt("SCH_ID")+1);
			}
		
		}catch(Exception e){
			logger.error("Error ", e);
			throw new DatabaseException("Could not fetch Scheduler Id", e);
		}finally{
			try {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst1);
			} catch (Exception e) {
				logger.error("Could not  close connection due to "+e.getMessage());
				logger.error("Error ", e);
			}
		}
		PreparedStatement pst=null;
		PreparedStatement ps=null;
		try {
			
		    pst = conn.prepareStatement("INSERT INTO TJN_SCHEDULER VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			pst.setInt(1, sch.getSchedule_Id());
			pst.setString(2, sch.getSch_date());
			pst.setString(3, sch.getSch_time());
			pst.setString(4, sch.getAction());
			pst.setString(5, sch.getReg_client());
			pst.setString(6, sch.getStatus());
			pst.setString(7, sch.getCreated_by());
			pst.setString(8, sch.getCreated_on());
			pst.setInt(9, sch.getRun_id());
			pst.setString(10, sch.getBrowserType());
			if(!sch.getAction().equalsIgnoreCase("Execute")){
				pst.setString(11, sch.getAutLoginTYpe());
			}
			else{
				pst.setString(11, null);
			}
			pst.setInt(12,sch.getProjectId());
			pst.setString(13, sch.getSchRecur());
			pst.setString(14, sch.getTaskName());
			if(sch.getAction().equalsIgnoreCase("Execute")){
				pst.setInt(15, sch.getScreenShotOption());
			}
			else{
				pst.setInt(15, 0);
			}
			
			if(sch.getAction().equalsIgnoreCase("learnapi")){
				pst.setString(16, sch.getApiLearnType());
			}else{
				pst.setString(16, "N-A");
			}
			pst.setString(17, null);
			pst.execute();
			if(sch.getSchRecur().equalsIgnoreCase("Y")){
				ps=conn.prepareStatement("INSERT INTO TJN_SCH_RECURRENCE (SCH_ID,FREQUENCY,RECUR_CYCLES,RECUR_DAYS,END_DATE) VALUES (?,?,?,?,?)");
				ps.setInt(1, sch.getSchedule_Id());
				ps.setString(2, sch.getFrequency());
				ps.setInt(3, sch.getRecurCycles());	
				ps.setString(4, sch.getRecurDays());
				ps.setString(5, sch.getEndDate());
				ps.execute();
			}	
		}catch (Exception e) {
			logger.error("Could not create Schedule Recurrence due to "+e.getMessage());
			throw new DatabaseException("Could not create a Schedule recurrence ", e);

		} finally {
			try {
				DatabaseHelper.close(ps);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Could not  close connection due to "+e.getMessage());
				logger.error("Error ", e);
			}
		}

		return sch;
	}
	public void updateSchedule(Scheduler sch) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			pst = conn.prepareStatement("UPDATE TJN_SCHEDULER SET SCH_DATE =?, SCH_TIME = ?, "
					+ "ACTION=?,REG_CLIENT=?, STATUS = ?, CREATED_BY=?,CREATED_ON=?,RUN_ID=?,BROWSERTYPE=?,AUTLOGINTYPE=?,PROJECT_ID=?,SCH_TASKNAME=?,SCH_SCREENSHOT_OPTION=?,SCH_API_LEARN_TYPE=? WHERE SCH_ID=?");
			pst.setString(1,sch.getSch_date());
			pst.setString(2, sch.getSch_time());
			pst.setString(3, sch.getAction());
			pst.setString(4, sch.getReg_client());
			pst.setString(5, sch.getStatus());
			pst.setString(6, sch.getCreated_by());
			pst.setString(7,sch.getCreated_on());
			pst.setInt(8,sch.getRun_id());
			pst.setString(9, sch.getBrowserType());
			pst.setString(10, sch.getAutLoginTYpe());
			pst.setInt(11, sch.getProjectId());
			pst.setString(12, sch.getTaskName());
			pst.setInt(13, sch.getScreenShotOption());
			pst.setString(14, sch.getApiLearnType());
			pst.setInt(15, sch.getSchedule_Id());

			pst.execute();
		} catch (Exception e) {
			logger.error("Could not update schedule due to  "+e.getMessage());
			logger.error("Error ", e);
			throw new DatabaseException(
					"Could not update schedule because of an internal error ");
		}finally{
			try {
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Could not  close connection due to "+e.getMessage());
				logger.error("Error ", e);
			}
		}

	}
	public Scheduler hydrateSchedule(String schCode) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		Scheduler sch = null;
		ResultSet rs=null;
		/*Added by paneendra for TENJINCG-1268 starts */
		DateFormat formatterUTC = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
		/*formatterUTC.setTimeZone(TimeZone.getTimeZone("UTC"));*/
		formatterUTC.setTimeZone(TimeZone.getDefault());

		DateFormat formatterIST = new SimpleDateFormat("dd-MMM-yyyy");
		DateFormat formatterToIST = new SimpleDateFormat("HH:mm");
		formatterToIST.setTimeZone(TimeZone.getTimeZone("IST"));
		formatterIST.setTimeZone(TimeZone.getTimeZone("IST"));
		/*Added by paneendra for TENJINCG-1268 ends */
		try {
			
			DatabaseMetaData meta = conn.getMetaData();
			/*modified by paneendra for TENJINCG-1268 starts */
			if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				pst = conn.prepareStatement("SELECT SCH_ID,TO_CHAR(SCH_DATE,'DD-MON-YYYY') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,STATUS,CREATED_BY,TO_CHAR(CREATED_ON, 'DD-MON-YYYY') as CREATED_ON,AUTLOGINTYPE,BROWSERTYPE,RUN_ID,SCH_TASKNAME,SCH_SCREENSHOT_OPTION,SCH_API_LEARN_TYPE,PROJECT_ID,SCH_RECURRING FROM TJN_SCHEDULER WHERE SCH_ID=?");
			else
				pst = conn.prepareStatement("SELECT SCH_ID,REPLACE(CONVERT(VARCHAR(11), SCH_DATE, 106), ' ', '-') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,STATUS,CREATED_BY,REPLACE(CONVERT(VARCHAR(11), CREATED_ON, 106), ' ', '-') as CREATED_ON,AUTLOGINTYPE,BROWSERTYPE,RUN_ID,SCH_TASKNAME,SCH_SCREENSHOT_OPTION,SCH_API_LEARN_TYPE,PROJECT_ID,SCH_RECURRING FROM TJN_SCHEDULER WHERE SCH_ID=?");
			/*modified by paneendra for TENJINCG-1268 ends */
			pst.setString(1,schCode);
			rs = pst.executeQuery();


			while (rs.next()) {
				sch = new Scheduler();
				sch.setSchedule_Id(rs.getInt("SCH_ID"));
				/*Added by paneendra for TENJINCG-1268 starts */
				String schdatetime=rs.getString("SCH_DATE")+"  "+rs.getString("SCH_TIME");

				Date date = formatterUTC.parse(schdatetime);

				String istdate=formatterIST.format(date);
				String isttime=formatterToIST.format(date);

				sch.setSch_date(istdate);
				sch.setSch_time(isttime);

				//sch.setSch_date(rs.getString("SCH_DATE"));
				//sch.setSch_time(rs.getString("SCH_TIME"));
				/*Added by paneendra for TENJINCG-1268 ends */
				sch.setAction(rs.getString("ACTION"));
				sch.setReg_client(rs.getString("REG_CLIENT"));
				sch.setStatus(rs.getString("STATUS"));
				sch.setCreated_by(rs.getString("CREATED_BY"));
				sch.setCreated_on(rs.getString("CREATED_ON"));
				sch.setAutLoginTYpe(rs.getString("AUTLOGINTYPE"));
				sch.setBrowserType(rs.getString("BROWSERTYPE"));
				sch.setRun_id(rs.getInt("RUN_ID"));
				sch.setTaskName(rs.getString("SCH_TASKNAME"));
				sch.setScreenShotOption(rs.getInt("SCH_SCREENSHOT_OPTION"));
				sch.setApiLearnType(rs.getString("SCH_API_LEARN_TYPE"));
				sch.setProjectId(rs.getInt("PROJECT_ID"));
				/*added by shruthi for TENJINCG-1232 starts*/
				sch.setSchRecur(rs.getString("SCH_RECURRING"));
				/*added by shruthi for TENJINCG-1232 ends*/
			}

		} catch (Exception e) {
			logger.error("Could not fetch record due to "+e.getMessage());
			logger.error("Error ", e);
			throw new DatabaseException("Could not fetch record ", e);
		} finally {
			try {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Error ", e);
			}
		}

		return sch;
	}

	public ArrayList<SchMapping> hydrateScheduleMap1(Scheduler schedule) throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pst1=null;
		PreparedStatement pst=null;
		PreparedStatement pst2=null;
		ResultSet rs=null;
		ResultSet rs1=null;
		ResultSet rs2=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<SchMapping> sch = new ArrayList<SchMapping>();

		try {
			if(schedule.getAction().equalsIgnoreCase("Learn")){
				int runId=0;

				pst1 = conn.prepareStatement("SELECT RUN_ID FROM TJN_SCHEDULER WHERE SCH_ID=?");
				pst1.setInt(1,schedule.getSchedule_Id());
				rs = pst1.executeQuery();
				while(rs.next()){
					runId=rs.getInt("RUN_ID");
				}
				
				pst = conn
						.prepareStatement("SELECT * FROM LRNR_AUDIT_TRAIL WHERE LRNR_RUN_ID=?");
				pst.setInt(1,runId);
				rs1 = pst.executeQuery();

				while (rs1.next()) {
					SchMapping sch1=new SchMapping();
					sch1.setApp_id(String.valueOf(rs1.getInt("LRNR_APP_ID")));
					sch1.setFunc_Code(rs1.getString("LRNR_FUNC_CODE"));
					sch.add(sch1);
				}
				
			}
			
			else if(schedule.getAction().equalsIgnoreCase("LearnAPI")){
				int runId=0;

				pst1 = conn.prepareStatement("SELECT RUN_ID FROM TJN_SCHEDULER WHERE SCH_ID=?");
				pst1.setInt(1,schedule.getSchedule_Id());
				rs = pst1.executeQuery();
				while(rs.next()){
					runId=rs.getInt("RUN_ID");
				}
				
				pst = conn
						.prepareStatement("SELECT * FROM LRNR_AUDIT_TRAIL_API WHERE LRNR_RUN_ID=?");
				pst.setInt(1,runId);
				rs1 = pst.executeQuery();

				while (rs1.next()) {
					SchMapping sch1=new SchMapping();
					sch1.setApp_id(rs1.getString("LRNR_APP_ID"));
					sch1.setFunc_Code(rs1.getString("LRNR_API_CODE"));
					sch1.setApiOperation(rs1.getString("LRNR_OPERATION"));
					sch.add(sch1);
				}
				
			}
			
			else if(schedule.getAction().equalsIgnoreCase("Extract")){
				int runId=0;
				pst1 = conn.prepareStatement("SELECT RUN_ID FROM TJN_SCHEDULER WHERE SCH_ID=?");
				pst1.setInt(1,schedule.getSchedule_Id());
				rs = pst1.executeQuery();
				while(rs.next()){
					runId=rs.getInt("RUN_ID");
				}
			
				pst = conn
						.prepareStatement("SELECT * FROM EXTR_AUDIT_TRAIL WHERE EXTR_RUN_ID=?");
				pst.setInt(1,runId);
				rs1 = pst.executeQuery();

				while (rs1.next()) {
					SchMapping sch1=new SchMapping();
					sch1.setApp_id(String.valueOf(rs1.getInt("EXTR_APP_ID")));
					sch1.setFunc_Code(rs1.getString("EXTR_FUNC_CODE"));
					sch1.setExt_file_path(rs1.getString("EXTR_INP_FILE_PATH"));
					sch.add(sch1);
				}
				
			}
			else if(schedule.getAction().equalsIgnoreCase("Execute")){
				pst2 = conn
						.prepareStatement("SELECT RUN_TS_REC_ID FROM MASTESTRUNS WHERE RUN_ID=?");
				pst2.setInt(1,schedule.getRun_id());
				rs2 = pst2.executeQuery();
				while(rs2.next()){
					SchMapping sch1=new SchMapping();
					sch1.setTestStepId(rs2.getInt("RUN_TS_REC_ID"));
					sch.add(sch1);
				}
				
			}


		} catch (Exception e) {
			logger.error("Could not fetch record due to "+e.getMessage());
			throw new DatabaseException("Could not fetch records", e);
		} finally {
			try {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(rs1);
				DatabaseHelper.close(rs2);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(pst1);
				DatabaseHelper.close(pst2);
				DatabaseHelper.close(conn);
			} catch (Exception e) {

				logger.error("Error ", e);
			}
		}

		return sch;

	}
	
	public ArrayList<Scheduler> hydrateAllAdminSchedulers(String status) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pst=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		
		ArrayList<Scheduler> sch = new ArrayList<Scheduler>();

		try {
			DatabaseMetaData meta = conn.getMetaData();
			
			String query=null;
			if(status.equalsIgnoreCase("Running")){
				status="Executing";
			}
			if(status.equalsIgnoreCase("All")){
				
				if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
					query="SELECT MESSAGE,SCH_ID,TO_CHAR(SCH_DATE,'DD Mon, yyyy') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,"
						+ "STATUS,CREATED_BY,TO_CHAR(CREATED_ON,'DD-MON-YYYY') as CREATED_ON,PROJECT_ID,RUN_ID,SCH_RECURRING,SCH_TASKNAME FROM TJN_SCHEDULER WHERE (ACTION='Learn' OR ACTION='Extract' OR ACTION='LearnAPI') ORDER BY SCH_ID DESC";
				
				else
					query="SELECT MESSAGE,SCH_ID,REPLACE(CONVERT(VARCHAR(11), SCH_DATE, 106), ' ', ' ') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,"
						+ "STATUS,CREATED_BY,REPLACE(CONVERT(VARCHAR(11), CREATED_ON, 106), ' ', '-') as CREATED_ON,PROJECT_ID,RUN_ID,SCH_RECURRING,SCH_TASKNAME FROM TJN_SCHEDULER WHERE (ACTION='Learn' OR ACTION='Extract' OR ACTION='LearnAPI') ORDER BY SCH_ID DESC";
				
				pst=conn.prepareStatement(query);	
			}
			else{
				
				
				if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
					query="SELECT MESSAGE,SCH_ID,TO_CHAR(SCH_DATE,'DD Mon, yyyy') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,"
						+ "STATUS,CREATED_BY,TO_CHAR(CREATED_ON,'DD-MON-YYYY') as CREATED_ON,PROJECT_ID,RUN_ID,SCH_RECURRING,SCH_TASKNAME FROM TJN_SCHEDULER WHERE (ACTION='Learn' OR ACTION='Extract' OR ACTION='LearnAPI') AND STATUS=? ORDER BY SCH_ID DESC";
				else
					query="SELECT MESSAGE,SCH_ID,REPLACE(CONVERT(VARCHAR(11), SCH_DATE, 106), ' ', ' ') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,"
						+ "STATUS,CREATED_BY,REPLACE(CONVERT(VARCHAR(11), CREATED_ON, 106), ' ', '-') as CREATED_ON,PROJECT_ID,RUN_ID,SCH_RECURRING,SCH_TASKNAME FROM TJN_SCHEDULER WHERE (ACTION='Learn' OR ACTION='Extract' OR ACTION='LearnAPI') AND STATUS=? ORDER BY SCH_ID DESC";
				
				
				pst=conn.prepareStatement(query);
				pst.setString(1, status);
			}

			rs = pst.executeQuery();
			while (rs.next()) {
				Scheduler sch1 = new Scheduler();

				sch1.setSchedule_Id(rs.getInt("SCH_ID"));
				sch1.setSch_date(rs.getString("SCH_DATE"));
				sch1.setSch_time(rs.getString("SCH_TIME"));
				sch1.setAction(rs.getString("ACTION"));
				sch1.setReg_client(rs.getString("REG_CLIENT"));
				sch1.setStatus(rs.getString("STATUS"));
				sch1.setCreated_by(rs.getString("CREATED_BY"));
				sch1.setCreated_on(rs.getString("CREATED_ON"));
				sch1.setProjectId(rs.getInt("PROJECT_ID"));
				sch1.setMessage(rs.getString("MESSAGE"));
				sch1.setRun_id(rs.getInt("RUN_ID"));
				sch1.setSchRecur(rs.getString("SCH_RECURRING"));
				sch1.setTaskName(rs.getString("SCH_TASKNAME"));
				
				sch.add(sch1);	
			}
			
		} catch (Exception e) {
			logger.error("Could not fetch records due to  "+e.getMessage());
			throw new DatabaseException("Could not fetch records", e);
		} finally {
			try {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {

				logger.error("Error ", e);
			}
		}

		return sch;

	}
	
	public ArrayList<Scheduler> hydrateAllAdminSchedulers(String status, String userRole) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<Scheduler> sch = new ArrayList<Scheduler>();
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {
			String query=null;
			if(status.equalsIgnoreCase("Running")){
				status="Executing";
			}
			if(status.equalsIgnoreCase("All")){
				
				query="SELECT MESSAGE,SCH_ID,TO_CHAR(SCH_DATE,'DD Mon, yyyy') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,"
						+ "STATUS,CREATED_BY,TO_CHAR(CREATED_ON,'DD-MON-YYYY') as CREATED_ON,PROJECT_ID,RUN_ID,SCH_RECURRING,SCH_TASKNAME FROM TJN_SCHEDULER WHERE ACTION='Extract' ORDER BY SCH_ID DESC";
				pst=conn.prepareStatement(query);	
				}
			else{
				query="SELECT MESSAGE,SCH_ID,TO_CHAR(SCH_DATE,'DD Mon, yyyy') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,"
						+ "STATUS,CREATED_BY,TO_CHAR(CREATED_ON,'DD-MON-YYYY') as CREATED_ON,PROJECT_ID,RUN_ID,SCH_RECURRING,SCH_TASKNAME FROM TJN_SCHEDULER WHERE ACTION='Extract' AND STATUS=? ORDER BY SCH_ID DESC";
			pst=conn.prepareStatement(query);
				pst.setString(1, status);
			}

			rs = pst.executeQuery();
			while (rs.next()) {
				Scheduler sch1 = new Scheduler();

				sch1.setSchedule_Id(rs.getInt("SCH_ID"));
				sch1.setSch_date(rs.getString("SCH_DATE"));
				sch1.setSch_time(rs.getString("SCH_TIME"));
				sch1.setAction(rs.getString("ACTION"));
				sch1.setReg_client(rs.getString("REG_CLIENT"));
				sch1.setStatus(rs.getString("STATUS"));
				sch1.setCreated_by(rs.getString("CREATED_BY"));
				sch1.setCreated_on(rs.getString("CREATED_ON"));
				sch1.setProjectId(rs.getInt("PROJECT_ID"));
				sch1.setMessage(rs.getString("MESSAGE"));
				sch1.setRun_id(rs.getInt("RUN_ID"));
				sch1.setSchRecur(rs.getString("SCH_RECURRING"));
				sch1.setTaskName(rs.getString("SCH_TASKNAME"));
				sch.add(sch1);	
			}
		} catch (Exception e) {
			logger.error("Could not fetch records due to  "+e.getMessage());
			throw new DatabaseException("Could not fetch records", e);
		} finally {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
		}

		return sch;

	}
	

	public void deleteScheduledMap(String fc,String sId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst2=null;
		PreparedStatement pst=null;
		ResultSet rs1=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {

			int runId=0;
			pst2=conn.prepareStatement("SELECT RUN_ID FROM TJN_SCHEDULER WHERE SCH_ID=?");
			pst2.setString(1,sId);
			rs1= pst2.executeQuery();
			while(rs1.next()){
				runId=rs1.getInt("RUN_ID");
			}

			pst = conn.prepareStatement("DELETE FROM LRNR_AUDIT_TRAIL WHERE LRNR_RUN_ID=? AND LRNR_FUNC_CODE=? ");
			pst.setInt(1, runId);
			pst.setString(2, fc);
			pst.executeUpdate();

		} catch (Exception e) {
			logger.error("Could not delete record due to  "+e.getMessage());
			throw new DatabaseException("Could not delete record", e);
		} finally {
			try {
				DatabaseHelper.close(rs1);
				DatabaseHelper.close(pst2);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Error ", e);
			}
		}


	}
	
	
	public int getTestRunId() throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
		PreparedStatement pst = null;
		ResultSet rs = null;
		int runId = 0;
		try{
			pst = conn.prepareStatement("SELECT coalesce(MAX(RUN_ID),0) AS RUN_ID FROM MASTESTRUNS");
			rs = pst.executeQuery();
			while(rs.next()){
				runId = rs.getInt("RUN_ID");
			}
			
			runId++;
		}catch(Exception e){
			logger.error("ERROR generating new Run ID",e);
			throw new DatabaseException("Could not create new run due to an internal error");
		}
		finally{
			try {
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} 
			 catch (Exception e){
				
				logger.error("Error ", e);
			}
		}

		return runId;
	}
	
	public Map<Integer,Scheduler> fetchSchedulerRunDetails() throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		Map<Integer,Scheduler> schedule = new TreeMap<Integer,Scheduler>();
		PreparedStatement pst = null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		
		try {
			
			DatabaseMetaData meta = conn.getMetaData();
			if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
			
				pst=conn.prepareStatement("SELECT SCH_ID,SCH_TASKNAME,TO_CHAR(SCH_DATE,'YYYY-MM-DD')||' '||SCH_TIME AS SCH_DATE ,SCH_TIME,ACTION,REG_CLIENT,STATUS,CREATED_BY,CREATED_ON,RUN_ID,BROWSERTYPE,AUTLOGINTYPE,PROJECT_ID,SCH_RECURRING,SCH_SCREENSHOT_OPTION ,SCH_API_LEARN_TYPE FROM TJN_SCHEDULER WHERE TO_CHAR(SCH_DATE,'YYYY-MM-DD')||' '||SCH_TIME <= (SELECT TO_CHAR(SYSTIMESTAMP,'YYYY-MM-DD HH24:MI') FROM DUAL) AND STATUS='Scheduled' ORDER BY 2 DESC");
			else
				pst=conn.prepareStatement("SELECT SCH_ID,SCH_TASKNAME,CONCAT(CONVERT(VARCHAR(10), SCH_DATE, 120), ' ',SCH_TIME) AS SCH_DATE ,SCH_TIME,ACTION,REG_CLIENT,STATUS,CREATED_BY,CREATED_ON,RUN_ID,BROWSERTYPE,AUTLOGINTYPE,PROJECT_ID,SCH_RECURRING,SCH_SCREENSHOT_OPTION ,SCH_API_LEARN_TYPE FROM TJN_SCHEDULER WHERE CONCAT(CONVERT(VARCHAR(10), SCH_DATE, 120),' ',SCH_TIME) <= (SELECT CONVERT(VARCHAR(16), GETDATE(), 120)) AND STATUS='Scheduled' ORDER BY 2 DESC");
			
			
			rs=pst.executeQuery();
			while(rs.next()){
				logger.info("Schedule found"+rs.getInt("SCH_ID"));
				Scheduler s=new Scheduler();
				s.setSchedule_Id(rs.getInt("SCH_ID"));
				s.setSch_date(rs.getString("SCH_DATE"));
				s.setSch_time(rs.getString("SCH_TIME"));
				s.setAction(rs.getString("ACTION"));
				s.setReg_client(rs.getString("REG_CLIENT"));
				s.setStatus(rs.getString("STATUS"));
				s.setCreated_by(rs.getString("CREATED_BY"));
				s.setCreated_on(rs.getString("CREATED_ON"));
				s.setRun_id(rs.getInt("RUN_ID"));
				s.setBrowserType(rs.getString("BROWSERTYPE"));
				s.setAutLoginTYpe(rs.getString("AUTLOGINTYPE"));
				s.setProjectId(rs.getInt("PROJECT_ID"));
				s.setSchRecur(rs.getString("SCH_RECURRING"));
				s.setScreenShotOption(rs.getInt("SCH_SCREENSHOT_OPTION"));
				s.setApiLearnType(rs.getString("SCH_API_LEARN_TYPE"));
				s.setTaskName(rs.getString("SCH_TASKNAME"));
				schedule.put(s.getSchedule_Id(), s);
			}
			
		} catch (Exception e) {
			logger.error("Could not fetch records due to "+e.getMessage());
			throw new DatabaseException("Could not fetch records", e);
		} finally {
			try {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Could connect to DB due to "+e.getMessage());
			}
		}
		return schedule;
	}



	public void updateScheduleStatus(String status,int schId) throws DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			//conn.setAutoCommit(false);
			pst=conn.prepareStatement("UPDATE TJN_SCHEDULER SET STATUS=? WHERE SCH_ID=?");
			pst.setString(1, status);
			pst.setInt(2, schId);
			pst.execute();
			
			//conn.commit();
		}catch(Exception e){
			logger.error("Could not update Status due to "+e.getMessage());
			throw new DatabaseException("Could not update Status ",e);
		}finally {
			try {
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Error ", e);
			}
		}
	}
	public void recurUpdateStatus(int schId) throws DatabaseException
	{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			//conn.setAutoCommit(false);
			String query="UPDATE TJN_SCHEDULER SET SCH_RECURRING='N' WHERE SCH_ID=?";
			pst=conn.prepareStatement(query);
			pst.setInt(1, schId);
			pst.addBatch();
			pst.executeBatch();
			//conn.commit();

		}catch(Exception e){
			logger.error("Could not update Status due to "+e.getMessage());
			throw new DatabaseException("Could not update Status ",e);
		}finally {
			try {
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Error ", e);
			}
		}
	}
	public void updateMultipleScheduleStatus(String status,List<Integer> schIds) throws DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			//conn.setAutoCommit(false);
			String query="UPDATE TJN_SCHEDULER SET STATUS=? WHERE SCH_ID=?";
			pst=conn.prepareStatement(query);
			for(int i=0;i<schIds.size();i++){
				pst.setString(1, status);
				pst.setInt(2, schIds.get(i));
				pst.addBatch();
			}
			pst.executeBatch();
			//conn.commit();

		}catch(Exception e){
			logger.error("Could not update Status due to "+e.getMessage());
			throw new DatabaseException("Could not update Status ",e);
		}finally {
			try {
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Error ", e);
			}
		}
	}
	public void updateScheduleTime(String status,List<Integer> schIds) throws DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			//conn.setAutoCommit(false);
			String query="UPDATE TJN_SCHEDULER SET STATUS=? ,SCH_DATE=?,SCH_TIME=? WHERE SCH_ID=?";
			pst=conn.prepareStatement(query);
			for(int i=0;i<schIds.size();i++){
				pst.setString(1, status);
				pst.setTimestamp(2, new Timestamp(new Date().getTime()));
				pst.setString(3, new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime()));
				pst.setInt(4, schIds.get(i));
				pst.addBatch();
			}
			pst.executeBatch();
			//conn.commit();

		}catch(Exception e){
			logger.error("Could not update Status due to "+e.getMessage());
			throw new DatabaseException("Could not update Status ",e);
		}finally {
			try {
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Error ", e);
			}
		}
	}
	public boolean checkExecutingClientStatus(String client) throws DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		boolean status=false;
		try {
			String query="SELECT COUNT(*) AS CNT FROM TJN_SCHEDULER WHERE REG_CLIENT=? AND STATUS='Executing'";
			pst=conn.prepareStatement(query);
			pst.setString(1, client);
			rs=pst.executeQuery();
			while(rs.next()){
				if(rs.getInt("CNT")!=0){
					status=true;
				}
			}
			
		}catch(Exception e){
			logger.error("Could not get execution status of schedule due to "+e.getMessage());
			throw new DatabaseException("Could not get execution status of schedule",e);
		}finally {
			try {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Error ", e);
			}
		}
		return status;	
	}	
	@SuppressWarnings("resource")
	public Scheduler copyOfSchedulerTask(int schId) throws DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		ResultSet rs=null;
		Scheduler sch=null;

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		
		try {
			
			DatabaseMetaData meta = conn.getMetaData();
			if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				pst=conn.prepareStatement("SELECT SCH_ID,TO_CHAR(SCH_DATE,'DD-MON-YYYY') AS SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,STATUS,CREATED_BY,CREATED_ON,PROJECT_ID,SCH_RECURRING,SCH_TASKNAME,SCH_SCREENSHOT_OPTION FROM TJN_SCHEDULER WHERE SCH_ID=? AND SCH_RECURRING='Y'");
			else
				pst=conn.prepareStatement("SELECT SCH_ID,REPLACE(CONVERT(VARCHAR(11), SCH_DATE, 106), ' ', '-') AS SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,STATUS,CREATED_BY,CREATED_ON,PROJECT_ID,SCH_RECURRING,SCH_TASKNAME,SCH_SCREENSHOT_OPTION FROM TJN_SCHEDULER WHERE SCH_ID=? AND SCH_RECURRING='Y'");
			pst.setInt(1, schId);
			rs=pst.executeQuery();
			if(rs.next()){
				sch=new Scheduler();	
				sch.setSchedule_Id(rs.getInt("SCH_ID"));
				sch.setSch_date(rs.getString("SCH_DATE"));
				sch.setSch_time(rs.getString("SCH_TIME"));
				sch.setAction(rs.getString("ACTION"));
				sch.setReg_client(rs.getString("REG_CLIENT"));
				sch.setStatus(rs.getString("STATUS"));
				sch.setCreated_by(rs.getString("CREATED_BY"));
				sch.setCreated_on(rs.getString("CREATED_ON"));
				sch.setProjectId(rs.getInt("PROJECT_ID"));
				sch.setSchRecur(rs.getString("SCH_RECURRING"));
				sch.setTaskName(rs.getString("SCH_TASKNAME"));
				sch.setScreenShotOption(rs.getInt("SCH_SCREENSHOT_OPTION"));
			}
		
			if(sch!=null)
			{
				if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
					pst=conn.prepareStatement("SELECT FREQUENCY,RECUR_CYCLES,RECUR_DAYS,TO_CHAR(END_DATE,'DD-MON-YYYY') AS END_DATE FROM TJN_SCH_RECURRENCE WHERE SCH_ID=?");
				else
					pst=conn.prepareStatement("SELECT FREQUENCY,RECUR_CYCLES,RECUR_DAYS,REPLACE(CONVERT(VARCHAR(11), END_DATE, 106), ' ', '-') AS END_DATE FROM TJN_SCH_RECURRENCE WHERE SCH_ID=?");
				pst.setInt(1, schId);
				rs=pst.executeQuery();
				if(rs.next()){
					sch.setFrequency(rs.getString("FREQUENCY"));
					sch.setRecurCycles(rs.getInt("RECUR_CYCLES"));
					sch.setRecurDays(rs.getString("RECUR_DAYS"));
					sch.setEndDate(rs.getString("END_DATE"));
				}
				
			}
		}catch(Exception e){
			logger.error("Could not get the Scheduled Task which has Recurrence due to "+e.getMessage());
			throw new DatabaseException("Could not get the Scheduled Task which has Recurrence ",e);
		}finally {
			try {
				DatabaseHelper.close(pst);
				DatabaseHelper.close(rs);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Error ", e);
			}
		}
		return sch;
	}


	public ArrayList<Scheduler> hydrateAllSchedules(String action,int projectId) throws DatabaseException {
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_CORE);
		PreparedStatement pst=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		
		ArrayList<Scheduler> sch = new ArrayList<Scheduler>();

		try {
			
			DatabaseMetaData meta = conn.getMetaData();
			
			String query=null;
			if(action.trim().equals("Extract")){
				if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				{
					query="SELECT SCH_ID,TO_CHAR(SCH_DATE,'DD Mon, yyyy') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,"
					+ "STATUS,CREATED_BY,TO_CHAR(CREATED_ON,'DD-MON-YYYY') as CREATED_ON,PROJECT_ID,RUN_ID,SCH_RECURRING,SCH_TASKNAME,AUTLOGINTYPE,BROWSERTYPE,SCH_SCREENSHOT_OPTION FROM TJN_SCHEDULER WHERE ACTION='"+action+"' ORDER BY SCH_ID DESC";
				}
				else
				{
					query="SELECT SCH_ID,REPLACE(CONVERT(VARCHAR(11), SCH_DATE, 106), ' ', ' ') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,"
						+ "STATUS,CREATED_BY,REPLACE(CONVERT(VARCHAR(11), CREATED_ON, 106), ' ', '-')  as CREATED_ON,PROJECT_ID,RUN_ID,SCH_RECURRING,SCH_TASKNAME,AUTLOGINTYPE,BROWSERTYPE,SCH_SCREENSHOT_OPTION FROM TJN_SCHEDULER WHERE ACTION='"+action+"' ORDER BY SCH_ID DESC";
				}
			
			}
			else if(action.trim().equals("Execute"))
			{
				if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				{
					query="SELECT SCH_ID,TO_CHAR(SCH_DATE,'DD Mon, yyyy') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,"
					+ "STATUS,CREATED_BY,TO_CHAR(CREATED_ON,'DD-MON-YYYY') as CREATED_ON,PROJECT_ID,RUN_ID,SCH_RECURRING,SCH_TASKNAME,AUTLOGINTYPE,BROWSERTYPE,SCH_SCREENSHOT_OPTION FROM TJN_SCHEDULER WHERE ACTION='"+action+"' AND PROJECT_ID='"+projectId+"' ORDER BY SCH_ID DESC";
				}
				else
				{
					query="SELECT SCH_ID,REPLACE(CONVERT(VARCHAR(11), SCH_DATE, 106), ' ', ' ') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,"
						+ "STATUS,CREATED_BY,REPLACE(CONVERT(VARCHAR(11), CREATED_ON, 106), ' ', '-') as CREATED_ON,PROJECT_ID,RUN_ID,SCH_RECURRING,SCH_TASKNAME,AUTLOGINTYPE,BROWSERTYPE,SCH_SCREENSHOT_OPTION FROM TJN_SCHEDULER WHERE ACTION='"+action+"' AND PROJECT_ID='"+projectId+"' ORDER BY SCH_ID DESC";
				}
			}
			else{
				if(meta.getDatabaseProductName().equalsIgnoreCase("Oracle"))
				{
					query="SELECT SCH_ID,TO_CHAR(SCH_DATE,'DD Mon, yyyy') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,"
						+ "STATUS,CREATED_BY,TO_CHAR(CREATED_ON,'DD-MON-YYYY') as CREATED_ON,PROJECT_ID,RUN_ID,SCH_RECURRING,SCH_TASKNAME,AUTLOGINTYPE,BROWSERTYPE,SCH_SCREENSHOT_OPTION FROM TJN_SCHEDULER WHERE (ACTION='"+action+"' OR ACTION='LearnAPI')ORDER BY SCH_ID DESC";
				}
				else
				{
					query="SELECT SCH_ID,REPLACE(CONVERT(VARCHAR(11), SCH_DATE, 106), ' ', ' ') as SCH_DATE,SCH_TIME,ACTION,REG_CLIENT,"
							+ "STATUS,CREATED_BY,REPLACE(CONVERT(VARCHAR(11), CREATED_ON, 106), ' ', ' ') as CREATED_ON,PROJECT_ID,RUN_ID,SCH_RECURRING,SCH_TASKNAME,AUTLOGINTYPE,BROWSERTYPE,SCH_SCREENSHOT_OPTION FROM TJN_SCHEDULER WHERE (ACTION='"+action+"' OR ACTION='LearnAPI')ORDER BY SCH_ID DESC";
				}
			}
			
			pst=conn.prepareStatement(query);	
			rs = pst.executeQuery();
			while (rs.next()) {
				Scheduler sch1 = new Scheduler();
				
				sch1.setSchedule_Id(rs.getInt("SCH_ID"));
				sch1.setSch_date(rs.getString("SCH_DATE"));
				sch1.setSch_time(rs.getString("SCH_TIME"));
				sch1.setAction(rs.getString("ACTION"));
				sch1.setReg_client(rs.getString("REG_CLIENT"));
				sch1.setStatus(rs.getString("STATUS"));
				sch1.setCreated_by(rs.getString("CREATED_BY"));
				sch1.setCreated_on(rs.getString("CREATED_ON"));
				sch1.setProjectId(rs.getInt("PROJECT_ID"));
				sch1.setRun_id(rs.getInt("RUN_ID"));
				sch1.setSchRecur(rs.getString("SCH_RECURRING"));
				sch1.setTaskName(rs.getString("SCH_TASKNAME"));
				sch1.setAutLoginTYpe(rs.getString("AUTLOGINTYPE"));
				sch1.setBrowserType(rs.getString("BROWSERTYPE"));
				sch1.setScreenShotOption(rs.getInt("SCH_SCREENSHOT_OPTION"));
				sch.add(sch1);	
			}
			

		} catch (Exception e) {
			logger.error("Could not fetch records due to  "+e.getMessage());
			throw new DatabaseException("Could not fetch records", e);
		} finally {
			try {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {

				logger.error("Error ", e);
			}
		}

		return sch;

	}
	public void insertAbortedReason(String message,int schId) throws DatabaseException{
		Connection conn = DatabaseHelper
				.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {
			//conn.setAutoCommit(false);
			
		
			pst=conn.prepareStatement("UPDATE TJN_SCHEDULER SET MESSAGE=? WHERE SCH_ID=?");
			pst.setString(1, message);
			pst.setInt(2, schId);
			
			pst.execute();
			//conn.commit();
		}catch(Exception e){
			logger.error("Could not insert message due to "+e.getMessage());
			throw new DatabaseException("Could not insert message ",e);
		}finally {
			try {
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Error ", e);
			}
		}
	}
	
	/*Added by lokanath for TENJINCG-1184 starts*/
	public boolean checkRunIdType(int runId) throws DatabaseException{
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst=null;
		ResultSet rs=null;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		boolean status=false;
		try {
			String query="SELECT COUNT(*) AS CNT FROM TJN_SCHEDULER WHERE RUN_ID=?";
			pst=conn.prepareStatement(query);
			pst.setInt(1, runId);
			rs=pst.executeQuery();
			while(rs.next()){
				if(rs.getInt("CNT")!=0){
					status=true;
				}
			}
			
		}catch(Exception e){
			logger.error("Could not get scheduler run for this run"+ runId +"due to "+e.getMessage());
			throw new DatabaseException("Could not get scheduler runtype for this run",e);
		}finally {
			try {
				DatabaseHelper.close(rs);
				DatabaseHelper.close(pst);
				DatabaseHelper.close(conn);
			} catch (Exception e) {
				logger.error("Error ", e);
			}
		}
		return status;	
	}
	/*Added by lokanath for TENJINCG-1184 ends*/
	
}