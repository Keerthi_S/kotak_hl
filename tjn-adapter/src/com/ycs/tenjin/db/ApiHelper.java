/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApiHelper.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*	
* DATE                 CHANGED BY              	DESCRIPTION
* 25-Oct-2016			Sriram					Added authenticateRest() method to authenticate REST API requests (Req#TJN_24_18)
* 09-11-2016           	Parveen                 Defect #779
* 02-12-2016            Parveen               	REQ #TJN_243_04  Changing insert query
* 23-Dec-2016           Gangadhar Badagi     	Added to get current users and #TEN-157
* 15-03-2017            Leelaprasad            	Requirement TENJINCG-141 
* 31-07-2017            Gangadhar Badagi        TENJINCG-320
* 16-08-2017			Roshni					T25IT-191
* 17-08-2017			Sriram Sridharan		T25IT-187
* 17-Aug-2017			Sriram Sridharan		T25IT-188
* 17-Aug-2017			Sriram Sridharan		T25IT-181
* 19-08-2017			Manish					T25IT-68
* 26-08-2017			Pushpalatha				T25IT-310
* 28-08-2017			Pushpalatha				T25IT-331
* 29-08-2017			Pushpalatha				T25IT-288
* 22-Sep-2017			sameer gupta			For new adapter spec
* 13-Oct-2017			Preeti					TENJINCG-366 
* 16-Nov-2017           Gangadhar Badagi     	TENJINCG-451
* 17-Nov-2017           Gangadhar Badagi     	Changed forTENJINCG-451
* 23-Nov-2017			Sriram Sridharan		TENJINCG-515
* 23-Nov-2017			Sriram Sridharan		TENJINCG-513
* 29-12-2017            Leelaprasad             TENJINCG-560
* 09-01-2018           	Leelaprasad             TENJINCG-512
* 02-Feb-2018			Sriram					TENJINCG-415
* 02-02-2018			Pushpalatha				TENJINCG-568
* 16-02-2018			Preeti					TENJINCG-600
* 19-02-2018            Padmavathi              for TENJINCG-545
* 20-02-2018			Preeti					fixed for back button
* 20-02-2018			Pushpalatha				TENJINCG-603
* 20-04-2018			Preeti					TENJINCG-616
* 11-07-2018			Preeti					Closed connections
* 26-10-2018			Pushpa					TENJINCG-873
* 22-03-2019			Preeti					TENJINCG-1018
* 06-02-2020			Roshni					TENJINCG-1168
* 29-05-2020            Leelaprasad             TJN210-100
* 18-12-2020            Paneendra               TENJINCG-1239

*/

package com.ycs.tenjin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiLearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Parameter;
import com.ycs.tenjin.bridge.pojo.aut.Representation;
import com.ycs.tenjin.bridge.utils.ApiUtilities;
import com.ycs.tenjin.util.Constants;
//import com.ycs.tenjin.user.ConnPool;
import com.ycs.tenjin.util.Utilities;


public class ApiHelper {
	private static Logger logger = LoggerFactory.getLogger(ApiHelper.class);

	public Api hydrateApi(int appId, Api api) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		/*Modified by Priyanka for VAPT Helper fix starts*/
		Statement st = null;
		PreparedStatement pst=null;
		/*Modified by Priyanka for VAPT Helper fix ends*/
		ResultSet rs = null;
		Api Api1 = null;
		try {
			/*Modified by Priyanka for VAPT Helper fix starts*/   
			pst = conn.prepareStatement("SELECT * FROM TJN_AUT_APIS WHERE API_CODE =?  AND APP_ID=?");
			pst.setString(1, api.getCode());
			pst.setLong(2, api.getApplicationId());
			rs =pst.executeQuery();
			/*Modified by Priyanka for VAPT Helper fix starts*/		
			
			while (rs.next()) {
				Api1 = new Api();
				Api1.setApplicationId(rs.getInt("APP_ID"));
				Api1.setCode(rs.getString("API_CODE"));
				Api1.setName(rs.getString("API_NAME"));
				Api1.setType(rs.getString("API_TYPE"));
				Api1.setUrl(rs.getString("API_URL"));
				Api1.setUrlValid(ApiUtilities.isUrlValid(api.getUrl()));
			}
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(st);
			DatabaseHelper.close(conn);
		}

		return Api1;
	}

	public void persistApi(Api Api) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		try {
			/* Modified by Pushpalatha for TENJINCG-568 starts */
			pst = conn.prepareStatement(
					"INSERT INTO TJN_AUT_APIS (APP_ID,API_CODE,API_NAME,API_TYPE,API_URL,GROUP_NAME,ENCRYPTION_TYPE,ENCRYPTION_KEY) VALUES(?,?,?,?,?,?,?,?)");
			/* Modified by Pushpalatha for TENJINCG-568 ends */
			pst.setInt(1, Api.getApplicationId());
			pst.setString(2, Api.getCode());
			pst.setString(3, Api.getName());
			pst.setString(4, Api.getType());
			pst.setString(5, Api.getUrl());
			/* Added by Pushpalatha for TENJINCG-568 starts */
			pst.setString(6, Api.getGroup());
			/* Added by Pushpalatha for TENJINCG-568 ends */
			pst.setString(7, Api.getEncryptionType());
			/* Modified by Preeti for TENJINCG-616 starts */
			
			pst.setString(8, Api.getEncryptionKey());
			pst.execute();
			/* Modified by Preeti for TENJINCG-616 ends */
			

		} catch (Exception e) {
			throw new DatabaseException("Could not create record", e);
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	// changes done by parveen, Leelaprasad for #405 starts
	public String fetchAPIResponseTypes(int appId, String apiCode, String OperationCode) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		/*Modified by Priyanka for VAPT Helper fix starts*/
		PreparedStatement pst=null;
		/*Modified by Priyanka for VAPT Helper fix ends*/
		ResultSet rs = null;
		int i = 0;
		String responseTypes = "";
		try {

			/*Modified by Priyanka for VAPT Helper fix starts*/
			pst = conn.prepareStatement("SELECT distinct API_RESP_CODE FROM TJN_AUT_API_OP_REPRESENTATIONS WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=? and REPRESENTATION_TYPE='RESPONSE'");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);
			pst.setString(3, OperationCode);
			rs = pst.executeQuery();
			/*Modified by Priyanka for VAPT Helper fix ends*/
			while (rs.next()) {
				if (i == 0) {
					responseTypes = Integer.toString(rs.getInt("API_RESP_CODE"));
					i++;
				} else {
					responseTypes = responseTypes + "," + rs.getString("API_RESP_CODE");
				}

			}
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		if (responseTypes != "") {
			return responseTypes;
		} else {
			return null;
		}

	}
	// changes done by parveen, Leelaprasad for #405 ends

	public void clearApi(Connection conn, int appId, String apiCode) throws DatabaseException {
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement("DELETE FROM TJN_AUT_APIS WHERE APP_ID=? AND API_CODE=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);

			pst.execute();

			pst.close();
		} catch (SQLException e) {
			logger.error("Could not clear API [{}] in app [{}]", apiCode, appId, e);
			throw new DatabaseException("Could not remove API " + apiCode);
		} finally {
			DatabaseHelper.close(pst);
		}
	}

	public void persistApi(Connection conn, Api api) throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(
					"INSERT INTO TJN_AUT_APIS (APP_ID,API_CODE,API_NAME,API_TYPE,API_URL) VALUES(?,?,?,?,?)");
			pst.setInt(1, api.getApplicationId());
			pst.setString(2, api.getCode());
			pst.setString(3, api.getName());
			pst.setString(4, api.getType());
			pst.setString(5, api.getUrl());
			/* Modified by Preeti for TENJINCG-616 starts */
			pst.execute();
			/* Modified by Preeti for TENJINCG-616 ends */
			

		} catch (Exception e) {
			throw new DatabaseException("Could not create record", e);
		} finally {
			DatabaseHelper.close(pst);
		}
	}

	/* modified by Roshni for TENJINCG-1168 starts */
	public ArrayList<Api> hydrateAllApi(Connection conn, String appId) throws DatabaseException {

		/* modified by Roshni for TENJINCG-1168 ends */
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<Api> apiList = null;

		try {
			/*modified by paneendra for sqlinjection starts */
			pst = conn.prepareStatement("SELECT * FROM TJN_AUT_APIS where APP_ID=? ORDER BY API_URL ASC");
			pst.setString(1, appId);
			/*modified by paneendra for sqlinjection ends */
			/* Changed by Leelaprasad for the requirement TENJINCG-512 ends */
			rs = pst.executeQuery();
			apiList = new ArrayList<Api>();
			while (rs.next()) {
				Api api = new Api();
				api.setApplicationId(rs.getInt("APP_ID"));
				api.setCode(rs.getString("API_CODE"));
				api.setName(rs.getString("API_NAME"));
				api.setType(rs.getString("API_TYPE"));
				api.setUrl(rs.getString("API_URL"));
				api.setGroup(rs.getString("GROUP_NAME"));
				api.setUrlValid(ApiUtilities.isUrlValid(api.getUrl()));
				apiList.add(api);
			}


		} catch (Exception e) {
			throw new DatabaseException("Could not fetch API List", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return apiList;
	}

	/* Added by Pushpalatha for defect T25IT-288 starts */
	public String getApiType(int appId, String appCode) throws DatabaseException {
		String type = "";
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			/*Modified by Priyanka for VAPT Helper fix starts*/
			pst = conn.prepareStatement(
					"SELECT API_TYPE FROM TJN_AUT_APIS where APP_ID=? AND API_CODE=?");
			pst.setInt(1, appId);
			pst.setString(2, appCode);
			/*Modified by Priyanka for VAPT Helper fix ends*/
			rs = pst.executeQuery();

			while (rs.next()) {
				type = rs.getNString(1);

			}


		} catch (Exception e) {
			throw new DatabaseException("Could not fetch API List", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return type;
	}

	/* Added by Pushpalatha for defect T25IT-288 ends */
	/* For WebService Calls */
	public ArrayList<Api> hydrateAllApiWithDetails(int appId) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<Api> apiList = null;

		try {
			/*modified by paneendra for sqlinjection starts */
			pst = conn.prepareStatement("SELECT * FROM TJN_AUT_APIS where APP_ID=?");
			pst.setInt(1, appId);
			/*modified by paneendra for sqlinjection ends */
			rs = pst.executeQuery();
			apiList = new ArrayList<Api>();
			while (rs.next()) {
				Api api = new Api();
				api.setApplicationId(rs.getInt("APP_ID"));
				api.setCode(rs.getString("API_CODE"));
				api.setName(rs.getString("API_NAME"));
				api.setType(rs.getString("API_TYPE"));
				api.setUrl(rs.getString("API_URL"));
				api.setUrlValid(ApiUtilities.isUrlValid(api.getUrl()));

				if (api != null) {

					PreparedStatement oPst = conn.prepareStatement(
							"SELECT * FROM TJN_AUT_API_OPERATIONS WHERE API_CODE=? AND APP_ID=? ORDER BY API_OPERATION");
					oPst.setString(1, api.getCode());
					oPst.setInt(2, appId);

					ResultSet oRs = oPst.executeQuery();
					while (oRs.next()) {
						ApiOperation op = new ApiOperation();
						op.setName(oRs.getString("API_OPERATION"));
						// Change for TENJINCG-1118 (SRIRAM)
						op.setMethod(oRs.getString("API_METHOD_NAME"));

						PreparedStatement pstHistory = conn.prepareStatement(
								"SELECT * FROM LRNR_AUDIT_TRAIL_API WHERE LRNR_APP_ID=? AND LRNR_API_CODE=? AND LRNR_OPERATION=? AND LRNR_STATUS=? ORDER BY LRNR_RUN_ID DESC");
						pstHistory.setInt(1, appId);
						pstHistory.setString(2, api.getCode());
						pstHistory.setString(3, op.getName());
						pstHistory.setString(4, BridgeProcess.COMPLETE);

						ResultSet rsHistory = pstHistory.executeQuery();

						while (rsHistory.next()) {
							ApiLearnerResultBean bean = new ApiLearnerResultBean();
							bean.setId(rsHistory.getInt("LRNR_ID"));
							bean.setRunId(rsHistory.getInt("LRNR_RUN_ID"));
							bean.setStartTimestamp(rsHistory.getTimestamp("LRNR_START_TIME"));
							bean.setEndTimestamp(rsHistory.getTimestamp("LRNR_END_TIME"));
							bean.setUser(rsHistory.getString("LRNR_USER_ID"));
							bean.setRequestDescriptor(rsHistory.getString("LRNR_REQ_DESC"));
							bean.setResponseDescriptor(rsHistory.getString("LRNR_RESP_DESC"));

							if (bean.getStartTimestamp() != null && bean.getEndTimestamp() != null) {
								long startMillis = bean.getStartTimestamp().getTime();
								long endMillis = bean.getEndTimestamp().getTime();

								long millis = endMillis - startMillis;

								String eTime = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
										TimeUnit.MILLISECONDS.toMinutes(millis)
												- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
										TimeUnit.MILLISECONDS.toSeconds(millis)
												- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

								bean.setElapsedTime(eTime);
							}

							op.setLastSuccessfulLearningResult(bean);

							break;
						}

						DatabaseHelper.close(rsHistory);
						DatabaseHelper.close(pstHistory);
						op.setResourceParameters(
								this.hydrateParameters(conn, appId, api.getCode(), op.getName(), "RESOURCE"));

						api.getOperations().add(op);
					}

					DatabaseHelper.close(oRs);
					DatabaseHelper.close(oPst);
				}

				apiList.add(api);
			}


		} catch (Exception e) {
			throw new DatabaseException("Could not fetch API List", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return apiList;
	}

	public Api hydrateApi(int appId, String apiCode) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement pstHistory = null;
		ResultSet rsHistory = null;
		Api api = null;
		try {
			/*Modified by Priyanka for VAPT Helper fix starts*/
			PreparedStatement pst1 = null;
			pst1 =conn.prepareStatement("SELECT  A.*,B.APP_NAME AS APP_NAME FROM TJN_AUT_APIS A, MASAPPLICATION B WHERE A.APP_ID = B.APP_ID AND  A.APP_ID=? and A.api_code=?");
			pst1.setInt(1, appId);
			pst1.setString(2, apiCode);
			rs =pst1.executeQuery();
			/*Modified by Priyanka for VAPT Helper fix ends*/
			
			while (rs.next()) {
				api = new Api();
				api.setApplicationId(appId);
				api.setCode(rs.getString("API_CODE"));
				api.setName(rs.getString("API_NAME"));
				api.setType(rs.getString("API_TYPE"));
				api.setUrl(rs.getString("API_URL"));
				api.setApplicationName(rs.getString("APP_NAME"));
				api.setUrlValid(ApiUtilities.isUrlValid(api.getUrl()));
				/* Added by Pushpalatha for TENJINCG-568 starts */
				api.setGroup(rs.getString("GROUP_NAME"));
				/* Added by Pushpalatha for TENJINCG-568 ends */
				api.setEncryptionType(rs.getString("ENCRYPTION_TYPE"));
				api.setEncryptionKey(rs.getString("ENCRYPTION_KEY"));
			}
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			if (api != null) {
				pst = conn.prepareStatement(
						"SELECT * FROM TJN_AUT_API_OPERATIONS WHERE API_CODE=? AND APP_ID=? ORDER BY API_OPERATION");
				pst.setString(1, apiCode);
				pst.setInt(2, appId);

				rs = pst.executeQuery();
				while (rs.next()) {
					ApiOperation op = new ApiOperation();
					op.setName(rs.getString("API_OPERATION"));
					op.setMethod(rs.getString("API_METHOD_NAME")); // TENJINCG-513

					pstHistory = conn.prepareStatement(
							"SELECT * FROM LRNR_AUDIT_TRAIL_API WHERE LRNR_APP_ID=? AND LRNR_API_CODE=? AND LRNR_OPERATION=? AND LRNR_STATUS=? ORDER BY LRNR_RUN_ID DESC");
					pstHistory.setInt(1, appId);
					pstHistory.setString(2, apiCode);
					pstHistory.setString(3, op.getName());
					pstHistory.setString(4, BridgeProcess.COMPLETE);

					rsHistory = pstHistory.executeQuery();

					while (rsHistory.next()) {
						ApiLearnerResultBean bean = new ApiLearnerResultBean();
						bean.setId(rsHistory.getInt("LRNR_ID"));
						bean.setRunId(rsHistory.getInt("LRNR_RUN_ID"));
						bean.setStartTimestamp(rsHistory.getTimestamp("LRNR_START_TIME"));
						bean.setEndTimestamp(rsHistory.getTimestamp("LRNR_END_TIME"));
						bean.setUser(rsHistory.getString("LRNR_USER_ID"));
						bean.setRequestDescriptor(rsHistory.getString("LRNR_REQ_DESC"));
						bean.setResponseDescriptor(rsHistory.getString("LRNR_RESP_DESC"));
						/* Added by Ashiki for TENJINCG-1105 starts */
						bean.setLastLearntFieldCount(rsHistory.getInt("FIELDS_LEARNT_COUNT"));
						/* Added by Ashiki for TENJINCG-1105 ends */
						bean.setAppId(appId);

						if (bean.getStartTimestamp() != null && bean.getEndTimestamp() != null) {
							long startMillis = bean.getStartTimestamp().getTime();
							long endMillis = bean.getEndTimestamp().getTime();

							long millis = endMillis - startMillis;

							String eTime = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
									TimeUnit.MILLISECONDS.toMinutes(millis)
											- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
									TimeUnit.MILLISECONDS.toSeconds(millis)
											- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

							bean.setElapsedTime(eTime);
						}

						op.setLastSuccessfulLearningResult(bean);

						break;
					}

					DatabaseHelper.close(rsHistory);
					DatabaseHelper.close(pstHistory);

					// changes done by parveen, Leelaprasad for #405 starts
					
					op.setListHeaderRepresentation(
							this.hydrateRepresentation(conn, appId, api.getCode(), op.getName(), "HEADER"));
					
					op.setListRequestRepresentation(
							this.hydrateRepresentation(conn, appId, api.getCode(), op.getName(), "REQUEST"));
					op.setListResponseRepresentation(
							this.hydrateRepresentation(conn, appId, api.getCode(), op.getName(), "RESPONSE"));
					// changes done by parveen, Leelaprasad for #405 ends

					op.setResourceParameters(
							this.hydrateParameters(conn, appId, api.getCode(), op.getName(), "RESOURCE"));

					api.getOperations().add(op);
				}
				/* } */

				pstHistory = conn.prepareStatement(
						"SELECT * FROM LRNR_AUDIT_TRAIL_API WHERE LRNR_APP_ID=? AND LRNR_API_CODE=? AND LRNR_STATUS=? ORDER BY LRNR_RUN_ID DESC");
				pstHistory.setInt(1, appId);
				pstHistory.setString(2, apiCode);
				pstHistory.setString(3, BridgeProcess.COMPLETE);

				rsHistory = pstHistory.executeQuery();

				while (rsHistory.next()) {
					ApiLearnerResultBean bean = new ApiLearnerResultBean();
					bean.setId(rsHistory.getInt("LRNR_ID"));
					bean.setRunId(rsHistory.getInt("LRNR_RUN_ID"));
					bean.setStartTimestamp(rsHistory.getTimestamp("LRNR_START_TIME"));
					bean.setEndTimestamp(rsHistory.getTimestamp("LRNR_END_TIME"));
					bean.setUser(rsHistory.getString("LRNR_USER_ID"));
					bean.setRequestDescriptor(rsHistory.getString("LRNR_REQ_DESC"));
					bean.setResponseDescriptor(rsHistory.getString("LRNR_RESP_DESC"));
					bean.setOperationName(rsHistory.getString("LRNR_OPERATION"));
					/* Added by Ashiki for TENJINCG-1105 starts */
					bean.setLastLearntFieldCount(rsHistory.getInt("FIELDS_LEARNT_COUNT"));
					/* Added by Ashiki for TENJINCG-1105 starts */
					bean.setAppId(appId);
					api.setLastSuccessfulLearning(bean);
				}
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			DatabaseHelper.close(rsHistory);
			DatabaseHelper.close(pstHistory);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return api;
	}

	public Api hydrateApi(int appId, String apiCode, Connection conn) throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement pstHistory = null;
		ResultSet rsHistory = null;
		Api api = null;
		PreparedStatement pst1 = null;
		try {
			
			pst1 = conn.prepareStatement("SELECT  A.*,B.APP_NAME AS APP_NAME FROM TJN_AUT_APIS A, MASAPPLICATION B WHERE A.APP_ID = B.APP_ID AND  A.APP_ID=? and A.api_code=?");
			pst1.setInt(1, appId);
			pst1.setString(2, apiCode);
			rs=pst1.executeQuery();
			while (rs.next()) {
				api = new Api();
				api.setApplicationId(appId);
				api.setCode(rs.getString("API_CODE"));
				api.setName(rs.getString("API_NAME"));
				api.setType(rs.getString("API_TYPE"));
				api.setUrl(rs.getString("API_URL"));
				api.setApplicationName(rs.getString("APP_NAME"));
				api.setUrlValid(ApiUtilities.isUrlValid(api.getUrl()));
				/* Added by Pushpalatha for TENJINCG-568 starts */
				api.setGroup(rs.getString("GROUP_NAME"));
				/* Added by Pushpalatha for TENJINCG-568 ends */
			}
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst1);
			if (api != null) {
				pst = conn.prepareStatement(
						"SELECT * FROM TJN_AUT_API_OPERATIONS WHERE API_CODE=? AND APP_ID=? ORDER BY API_OPERATION");
				pst.setString(1, apiCode);
				pst.setInt(2, appId);

				rs = pst.executeQuery();
				while (rs.next()) {
					ApiOperation op = new ApiOperation();
					op.setName(rs.getString("API_OPERATION"));
					op.setMethod(rs.getString("API_METHOD_NAME")); // TENJINCG-513

					pstHistory = conn.prepareStatement(
							"SELECT * FROM LRNR_AUDIT_TRAIL_API WHERE LRNR_APP_ID=? AND LRNR_API_CODE=? AND LRNR_OPERATION=? AND LRNR_STATUS=? ORDER BY LRNR_RUN_ID DESC");
					pstHistory.setInt(1, appId);
					pstHistory.setString(2, apiCode);
					pstHistory.setString(3, op.getName());
					pstHistory.setString(4, BridgeProcess.COMPLETE);

					rsHistory = pstHistory.executeQuery();

					while (rsHistory.next()) {
						ApiLearnerResultBean bean = new ApiLearnerResultBean();
						bean.setId(rsHistory.getInt("LRNR_ID"));
						bean.setRunId(rsHistory.getInt("LRNR_RUN_ID"));
						bean.setStartTimestamp(rsHistory.getTimestamp("LRNR_START_TIME"));
						bean.setEndTimestamp(rsHistory.getTimestamp("LRNR_END_TIME"));
						bean.setUser(rsHistory.getString("LRNR_USER_ID"));
						bean.setRequestDescriptor(rsHistory.getString("LRNR_REQ_DESC"));
						bean.setResponseDescriptor(rsHistory.getString("LRNR_RESP_DESC"));
						/* Added by Ashiki for TENJINCG-1105 starts */
						bean.setLastLearntFieldCount(rsHistory.getInt("FIELDS_LEARNT_COUNT"));
						/* Added by Ashiki for TENJINCG-1105 ends */
						bean.setAppId(appId);

						if (bean.getStartTimestamp() != null && bean.getEndTimestamp() != null) {
							long startMillis = bean.getStartTimestamp().getTime();
							long endMillis = bean.getEndTimestamp().getTime();

							long millis = endMillis - startMillis;

							String eTime = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
									TimeUnit.MILLISECONDS.toMinutes(millis)
											- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
									TimeUnit.MILLISECONDS.toSeconds(millis)
											- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

							bean.setElapsedTime(eTime);
						}

						op.setLastSuccessfulLearningResult(bean);

						break;
					}

					DatabaseHelper.close(rsHistory);
					DatabaseHelper.close(pstHistory);

					// changes done by parveen, Leelaprasad for #405 starts
					
					
					op.setListHeaderRepresentation(
							this.hydrateRepresentation(conn, appId, api.getCode(), op.getName(), "HEADER"));
					
					op.setListRequestRepresentation(
							this.hydrateRepresentation(conn, appId, api.getCode(), op.getName(), "REQUEST"));
					op.setListResponseRepresentation(
							this.hydrateRepresentation(conn, appId, api.getCode(), op.getName(), "RESPONSE"));
					// changes done by parveen, Leelaprasad for #405 ends

					op.setResourceParameters(
							this.hydrateParameters(conn, appId, api.getCode(), op.getName(), "RESOURCE"));

					api.getOperations().add(op);
				}
				/* } */

				pstHistory = conn.prepareStatement(
						"SELECT * FROM LRNR_AUDIT_TRAIL_API WHERE LRNR_APP_ID=? AND LRNR_API_CODE=? AND LRNR_STATUS=? ORDER BY LRNR_RUN_ID DESC");
				pstHistory.setInt(1, appId);
				pstHistory.setString(2, apiCode);
				pstHistory.setString(3, BridgeProcess.COMPLETE);

				rsHistory = pstHistory.executeQuery();

				while (rsHistory.next()) {
					ApiLearnerResultBean bean = new ApiLearnerResultBean();
					bean.setId(rsHistory.getInt("LRNR_ID"));
					bean.setRunId(rsHistory.getInt("LRNR_RUN_ID"));
					bean.setStartTimestamp(rsHistory.getTimestamp("LRNR_START_TIME"));
					bean.setEndTimestamp(rsHistory.getTimestamp("LRNR_END_TIME"));
					bean.setUser(rsHistory.getString("LRNR_USER_ID"));
					bean.setRequestDescriptor(rsHistory.getString("LRNR_REQ_DESC"));
					bean.setResponseDescriptor(rsHistory.getString("LRNR_RESP_DESC"));
					bean.setOperationName(rsHistory.getString("LRNR_OPERATION"));
					/* Added by Ashiki for TENJINCG-1105 starts */
					bean.setLastLearntFieldCount(rsHistory.getInt("FIELDS_LEARNT_COUNT"));
					/* Added by Ashiki for TENJINCG-1105 starts */
					bean.setAppId(appId);
					api.setLastSuccessfulLearning(bean);
				}
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			DatabaseHelper.close(rsHistory);
			DatabaseHelper.close(pstHistory);
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
//			DatabaseHelper.close(conn);
		}
		return api;
	}

	public Api getLearningHistory(int appId, String apiCode) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);

		PreparedStatement pst = null;
		ResultSet rs = null;

		Api api = null;

		try {
			api = this.hydrateApi(conn, appId, apiCode);

			pst = conn.prepareStatement(
					"SELECT * FROM LRNR_AUDIT_TRAIL_API WHERE LRNR_APP_ID=? AND LRNR_API_CODE=? ORDER BY LRNR_START_TIME DESC;");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);

			rs = pst.executeQuery();

			while (rs.next()) {
				ApiLearnerResultBean bean = new ApiLearnerResultBean();
				bean.setId(rs.getInt("LRNR_ID"));
				bean.setRunId(rs.getInt("LRNR_RUN_ID"));
				bean.setStartTimestamp(rs.getTimestamp("LRNR_START_TIME"));
				bean.setEndTimestamp(rs.getTimestamp("LRNR_END_TIME"));
				bean.setUser(rs.getString("LRNR_USER_ID"));
				bean.setRequestDescriptor(rs.getString("LRNR_REQ_DESC"));
				bean.setResponseDescriptor(rs.getString("LRNR_RESP_DESC"));
				bean.setOperationName(rs.getString("LRNR_OPERATION"));

				if (bean.getStartTimestamp() != null && bean.getEndTimestamp() != null) {
					long startMillis = bean.getStartTimestamp().getTime();
					long endMillis = bean.getEndTimestamp().getTime();

					long millis = endMillis - startMillis;

					String eTime = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
							TimeUnit.MILLISECONDS.toMinutes(millis)
									- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
							TimeUnit.MILLISECONDS.toSeconds(millis)
									- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

					bean.setElapsedTime(eTime);
				}

				api.getLearningHistory().add(bean);
			}

		} catch (SQLException e) {
			logger.error("ERROR while fetching learning history for API [{}] under Application [{}]", apiCode, appId,
					e);
			throw new DatabaseException(
					"Could not fetch learning history due to an internal error. Please contact Tenjin Support.");
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return api;
	}

	public void updateApi(Api api) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		//Statement st = null;
				PreparedStatement pst = null;
		try {

			pst = conn.prepareStatement("update TJN_AUT_APIS set API_NAME=?,API_URL=?,ENCRYPTION_KEY=? where API_CODE=? AND APP_ID=?");
			pst.setString(1, api.getName());
			pst.setString(2, api.getUrl());
			pst.setString(3, api.getEncryptionKey());
			pst.setString(4, api.getCode());
			pst.setInt(5, api.getApplicationId());
			pst.execute();

		} catch (Exception e) {
			throw new DatabaseException("Could not update record", e);
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}


	}

	public void clearApi(String[] apiCodes, int appId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try {
			/*Modified by Priyanka for VAPT Helper fix starts*/
			 PreparedStatement pst=null;
			 /*Modified by Priyanka for VAPT Helper fix ends*/
			for (int i = 0; i < apiCodes.length; i++) {
				try {
					/* changed by manish for bug T25IT-68 on 19-Aug-2017 starts */
					this.clearAllApiOperations(apiCodes[i], appId);
					/* changed by manish for bug T25IT-68 on 19-Aug-2017 ends */
					/*Modified by Priyanka for VAPT Helper fix starts*/
					pst = conn.prepareStatement("DELETE FROM TJN_AUT_APIS WHERE API_CODE =?");
					pst.setString(1, apiCodes[i]);
					pst.executeUpdate();
					/*Modified by Priyanka for VAPT Helper fix ends*/
					
				} finally {
					DatabaseHelper.close(pst);
				}
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not delete record", e);
		} finally {
			DatabaseHelper.close(conn);
		}
	}

	/* Added by Pushpalatha for defect T25IT-310 starts */

	public void clearApiOperations(String[] apiCodes, int appId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		try {
			/* Changed by Pushpalatha for defect TT25IT-331 starts */
			ArrayList<ApiLearnerResultBean> lrnr_runs = null;
			int count = 0;
			for (int i = 0; i < apiCodes.length; i++) {
				lrnr_runs = new ApiHelper().hydrateLearningHistory1(apiCodes[i], appId);
				if (lrnr_runs.size() > 0) {
					count = count + 1;
				}
			}
			if (count == 0) {

				for (int i = 0; i < apiCodes.length; i++) {
					/* changed by manish for bug T25IT-68 on 19-Aug-2017 starts */
					 /*Modified by Priyanka for VAPT Helper fix starts*/
					 PreparedStatement pst=null;
					 /*Modified by Priyanka for VAPT Helper fix ends*/
					try {

						this.clearAllApiOperations(apiCodes[i], appId);
						/* changed by manish for bug T25IT-68 on 19-Aug-2017 ends */
						 /*Modified by Priyanka for VAPT Helper fix starts*/
						 pst = conn.prepareStatement("DELETE FROM TJN_AUT_APIS WHERE API_CODE =? AND APP_ID=?");
						 pst.setString(1, apiCodes[i]);
						 pst.setInt(2, appId);
						 pst.executeUpdate();
						 /*Modified by Priyanka for VAPT Helper fix ends*/
					} finally {
						DatabaseHelper.close(pst);

					}

				}

			}
			/* Changed by Pushpalatha for defect TT25IT-331 ends */

		} catch (Exception e) {
			throw new DatabaseException("Could not delete record", e);
		} finally {
			DatabaseHelper.close(conn);
		}
	}

	/* Added by Pushpalatha for defect T25IT-310 ends */

	public Api hydrateApi(Connection conn, int appId, String apicode) throws DatabaseException {

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		/*Modified by Priyanka for VAPT Helper fix starts*/
		PreparedStatement pst = null;
		/*Modified by Priyanka for VAPT Helper fix ends*/
		ResultSet rs = null;
		Api Api = null;
		try {
			/*Modified by Priyanka for VAPT Helper fix starts*/
			pst = conn.prepareStatement("SELECT * FROM TJN_AUT_APIS WHERE API_CODE =? AND APP_ID=?");
			pst.setString(1, apicode);
			pst.setInt(2, appId);
			rs = pst.executeQuery();
			/*Modified by Priyanka for VAPT Helper fix ends*/
			while (rs.next()) {
				Api = new Api();
				Api.setApplicationId(rs.getInt("APP_ID"));
				Api.setCode(rs.getString("API_CODE"));
				Api.setName(rs.getString("API_NAME"));
				Api.setType(rs.getString("API_TYPE"));
				Api.setUrl(rs.getString("API_URL"));
				Api.setUrlValid(ApiUtilities.isUrlValid(Api.getUrl()));
			}
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch record", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return Api;
	}

	public ApiOperation hydrateApiOperation(Connection conn, int appId, String apiCode, String operationName)
			throws DatabaseException {
		ApiOperation operation = null;

		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			pst = conn.prepareStatement(
					"SELECT * FROM TJN_AUT_API_OPERATIONS WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);
			pst.setString(3, operationName);

			rs = pst.executeQuery();

			while (rs.next()) {
				operation = new ApiOperation();
				operation.setName(rs.getString("API_OPERATION"));
				operation.setMethod(rs.getString("API_METHOD_NAME"));
			}


			if (operation != null) {
				logger.debug("Hydrating resource parameters for operation [{}]", operationName);
				operation.getResourceParameters()
						.addAll(this.hydrateParameters(conn, appId, apiCode, operationName, "RESOURCE"));

				// changes done by parveen, Leelaprasad for #405 starts
				logger.debug("Hydrating Request Representation");
				operation.setListRequestRepresentation(
						this.hydrateRepresentation(conn, appId, apiCode, operationName, "REQUEST"));

				/*	Added By Paneendra for TENJINCG-1239 starts */
				logger.debug("Hydrating Header Representation");
				operation.setListHeaderRepresentation(
						this.hydrateRepresentation(conn, appId, apiCode, operationName, "HEADER"));
				
				/*	Added By Paneendra for TENJINCG-1239 ends */
				
				logger.debug("Hydrating Response Representation");
				operation.setListResponseRepresentation(
						this.hydrateRepresentation(conn, appId, apiCode, operationName, "RESPONSE"));
				// changes done by parveen, Leelaprasad for #405 ends
			}

		} catch (SQLException e) {
			
			logger.error("ERROR while fetching details of operation [{}] for API [{}] in application [{}]",
					operationName, apiCode, appId, e);
			throw new DatabaseException("Could not fetch details of operation " + operationName);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return operation;
	}
	// changes done by parveen, Leelaprasad for #405 starts

	private ArrayList<Representation> hydrateRepresentation(Connection conn, int appId, String apiCode,
			String operationName, String entityType) throws DatabaseException {
		Representation rep = null;
		ArrayList<Representation> repList = new ArrayList<Representation>();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {

			pst = conn.prepareStatement(
					"SELECT * FROM TJN_AUT_API_OP_REPRESENTATIONS WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=? AND REPRESENTATION_TYPE=?  ");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);
			pst.setString(3, operationName);
			pst.setString(4, entityType.toUpperCase());

			rs = pst.executeQuery();

			while (rs.next()) {
				rep = new Representation();
				rep.setMediaType(rs.getString("MEDIA_TYPE"));
				rep.setRepresentationDescription(rs.getString("API_REPRESENTATION_DESCRIPTION"));
				rep.setResponseCode(rs.getInt("API_RESP_CODE"));
				repList.add(rep);
			}


			if (rep != null) {
				logger.debug("Hydrating parameters for {} representation", entityType);
				rep.setParameters(
						this.hydrateParameters(conn, appId, apiCode, operationName, entityType.toUpperCase()));
			}

			return repList;
		} catch (Exception e) {
			logger.error("ERROR hydrating {} representation for operation {}, API {}", entityType, operationName,
					apiCode, e);
			throw new DatabaseException("Could not fetch operation representation. Please contact Tenjin support.");
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
	}

	// changes done by parveen, Leelaprasad for #405 ends
	private List<Parameter> hydrateParameters(Connection conn, int appId, String apiCode, String operationName,
			String repType) throws DatabaseException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {

			List<Parameter> parameters = new ArrayList<Parameter>();

			pst = conn.prepareStatement(
					"SELECT * FROM TJN_AUT_API_OP_PARAMETERS WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=? AND REP_TYPE=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);
			pst.setString(3, operationName);
			pst.setString(4, repType.toUpperCase());

			rs = pst.executeQuery();

			while (rs.next()) {
				Parameter p = new Parameter();
				p.setName(rs.getString("PARAMETER_NAME"));
				p.setType(rs.getString("PARAMETER_TYPE"));
				p.setStyle(rs.getString("PARAMETER_STYLE"));
				parameters.add(p);
			}


			return parameters;
		} catch (Exception e) {
			logger.error("ERROR hydrating {} parameters for operation {}, API {}", repType, operationName, apiCode, e);
			throw new DatabaseException("Could not fetch operation parameters. Please contact Tenjin support.");
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
	}

	/* Added by Gangadhar Badagi for TENJINCG-451 starts */
	private Parameter hydrateParameter(Connection conn, int appId, String apiCode, String operationName,
			String parameterName) throws DatabaseException {
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {

			Parameter parameter = new Parameter();

			pst = conn.prepareStatement(
					"SELECT * FROM TJN_AUT_API_OP_PARAMETERS WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=? AND PARAMETER_NAME=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);
			pst.setString(3, operationName);
			pst.setString(4, parameterName);

			rs = pst.executeQuery();

			while (rs.next()) {

				parameter.setName(rs.getString("PARAMETER_NAME"));
				parameter.setType(rs.getString("PARAMETER_TYPE"));
				parameter.setStyle(rs.getString("PARAMETER_STYLE"));
			}

			return parameter;
		} catch (Exception e) {
			logger.error("ERROR hydrating {} parameters for operation {}, API {}", operationName, apiCode, e);
			throw new DatabaseException("Could not fetch operation parameters. Please contact Tenjin support.");
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}
	}
	/* Added by Gangadhar Badagi for TENJINCG-451 ends */

	public Representation hydrateRepresentationforResponseType(int appId, String apiCode, String operationName,
			String entityType, int responseType) {
		Representation rep = null;
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
			pst = conn.prepareStatement(
					"SELECT * FROM TJN_AUT_API_OP_REPRESENTATIONS WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=? AND REPRESENTATION_TYPE=? AND API_RESP_CODE=? ");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);
			pst.setString(3, operationName);
			pst.setString(4, entityType.toUpperCase());
			pst.setInt(5, responseType);

			rs = pst.executeQuery();

			while (rs.next()) {
				rep = new Representation();
				rep.setMediaType(rs.getString("MEDIA_TYPE"));
				rep.setRepresentationDescription(rs.getString("API_REPRESENTATION_DESCRIPTION"));

			}


			if (rep != null) {
				logger.debug("Hydrating parameters for {} representation", entityType);
				rep.setParameters(
						this.hydrateParameters(conn, appId, apiCode, operationName, entityType.toUpperCase()));
			}

			return rep;
		} catch (Exception e) {
			logger.error("ERROR hydrating {} representation for operation {}, API {}", entityType, operationName,
					apiCode, e);

		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return rep;

	}

	public ApiOperation hydrateApiOperation(int appId, String apiCode, String operationName) throws DatabaseException {
		ApiOperation operation = null;

		PreparedStatement pst = null;
		ResultSet rs = null;

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		try {
			// TENJINCG-415
			pst = conn.prepareStatement(
					"SELECT A.*, B.API_AUTH_TYPE AS PARENT_API_AUTH_TYPE FROM TJN_AUT_API_OPERATIONS A, MASAPPLICATION B WHERE B.APP_ID=A.APP_ID AND A.APP_ID=? AND A.API_CODE=? AND A.API_OPERATION=?");
			// TENJINCG-415 ends
			pst.setInt(1, appId);
			pst.setString(2, apiCode);
			pst.setString(3, operationName);

			rs = pst.executeQuery();

			while (rs.next()) {
				operation = new ApiOperation();
				operation.setName(rs.getString("API_OPERATION"));
				operation.setMethod(rs.getString("API_METHOD_NAME"));

				// TENJINCG-415
				operation.setAuthenticationType(rs.getString("API_AUTH_TYPE"));
				operation.setParentAuthenticationType(rs.getString("PARENT_API_AUTH_TYPE"));

				// If auth type is empty at the operation level, then set the value of auth type
				// to that of the Parent auth type
				if (Utilities.trim(operation.getAuthenticationType()).length() < 1) {
					operation.setAuthenticationType(operation.getParentAuthenticationType());
				}

				// TENJINCG-415 ends
			}

			if (operation != null) {
				logger.debug("Hydrating resource parameters for operation [{}]", operationName);
				operation.getResourceParameters()
						.addAll(this.hydrateParameters(conn, appId, apiCode, operationName, "RESOURCE"));
				// changes done by parveen, Leelaprasad for #405 starts

				/*	Added By Paneendra for TENJINCG-1239 starts */
				logger.debug("Hydrating Header Representation");
				operation.setListHeaderRepresentation(
						this.hydrateRepresentation(conn, appId, apiCode, operationName, "HEADER"));

				/*	Added By Paneendra for TENJINCG-1239 ends */
				logger.debug("Hydrating Request Representation");
				operation.setListRequestRepresentation(
						this.hydrateRepresentation(conn, appId, apiCode, operationName, "REQUEST"));

				logger.debug("Hydrating Response Representation");
				operation.setListResponseRepresentation(
						this.hydrateRepresentation(conn, appId, apiCode, operationName, "RESPONSE"));
				// changes done by parveen, Leelaprasad for #405 ends
			}

		} catch (SQLException e) {
			
			logger.error("ERROR while fetching details of operation [{}] for API [{}] in application [{}]",
					operationName, apiCode, appId, e);
			throw new DatabaseException("Could not fetch details of operation " + operationName);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return operation;
	}

	public void clearAllApiOperations(String apiCode, int appId) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);

		PreparedStatement pst = null;

		try {
			pst = conn.prepareStatement("DELETE FROM TJN_AUT_API_OPERATIONS WHERE APP_ID=? AND API_CODE=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);

			pst.executeUpdate();

			
			DatabaseHelper.close(pst);

			/* Fix for T25IT-181 */
			pst = conn.prepareStatement("DELETE FROM TJN_AUT_API_OP_PARAMETERS WHERE APP_ID=? AND API_CODE=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);

			pst.executeUpdate();

			
			DatabaseHelper.close(pst);

			pst = conn.prepareStatement("DELETE FROM TJN_AUT_API_OP_REPRESENTATIONS WHERE APP_ID=? AND API_CODE=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);

			pst.executeUpdate();

			
			/* Fix for T25IT-181 ends */
		} catch (SQLException e) {
			
			logger.error("An ERROR occurred while deleting Operation", e);
			throw new DatabaseException(
					"Could not delete operation due to an internal error. Please contact Tenjin Support.");
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
	}

	public void persistApiOperation(ApiOperation operation, String apiCode, int appId) throws DatabaseException {
		if (operation == null) {
			logger.warn("Operation object is null. Cannot save!");
			return;
		}

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);

		PreparedStatement pst = null;

		try {
			pst = conn.prepareStatement(
					"INSERT INTO TJN_aut_API_OPERATIONS(APP_ID,API_CODE,API_OPERATION,API_METHOD_NAME,API_AUTH_TYPE) VALUES (?,?,?,?,?)");
			// TENJINCG-415 ends
			// changes done by parveen, Leelaprasad for #405 ends
			pst.setInt(1, appId);
			pst.setString(2, apiCode);
			pst.setString(3, operation.getName());
			// changes done by parveen, Leelaprasad for #405 starts
			pst.setString(4, operation.getMethod());
			// changes done by parveen, Leelaprasad for #405 ends

			// TENJINCG-415
			pst.setString(5, operation.getAuthenticationType());
			// TENJINCG-415 ends

			pst.execute();
			
		} catch (SQLException e) {
			
			logger.error("ERROR persisting API Operation", e);
			throw new DatabaseException(
					"Could not save operation due to an internal error. Please contact Tenjin Support.");
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}

	public void persistApiOperations(List<ApiOperation> operationsList, String apiCode, int appId)
			throws DatabaseException {

		if (operationsList == null || operationsList.size() < 1) {
			logger.warn("No Operations to save!");
			return;
		}

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);

		PreparedStatement pst = null;

		try {
			//conn.setAutoCommit(false);
			// changes done by parveen, Leelaprasad for #405 starts
			pst = conn.prepareStatement(
					"INSERT INTO TJN_aut_API_OPERATIONS(APP_ID,API_CODE,API_OPERATION) VALUES (?,?,?)");
			// changes done by parveen, Leelaprasad for #405 ends
			for (ApiOperation operation : operationsList) {
				pst.setInt(1, appId);
				pst.setString(2, apiCode);
				pst.setString(3, operation.getName());


				pst.addBatch();
			}

			pst.executeBatch();
			//conn.commit();

			
		} catch (SQLException e) {
			
			logger.error("ERROR persisting API Operations", e);
			throw new DatabaseException(
					"Could not save operations due to an internal error. Please contact Tenjin Support.");
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}

	/* Changed by Leelaprasad for requirement TENJINCG-560 starts */
	public void updateOperationDescriptors(int appId, String apiCode, ApiOperation operation) throws DatabaseException {
		/* Changed by Leelaprasad for requirement TENJINCG-560 ends */

		Connection conn;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		} catch (DatabaseException e1) {
			
			logger.error("ERROR getting database connection", e1);
			return;
		}

		PreparedStatement pst = null;

		try {
			// changes done by parveen, Leelaprasad for #405 starts
			/* Changed by Leelaprasad for requirement TENJINCG-560 starts */
			int reqcount = this.validateRepresentation(appId, apiCode, operation.getName(), "REQUEST");
			int rescount = this.validateRepresentation(appId, apiCode, operation.getName(), "RESPONSE");
			if (reqcount == 0 || rescount == 0) {
				this.persistReqRepresentation(conn, appId, apiCode, operation.getName(), "REQUEST",
						operation.getListRequestRepresentation());
				this.persistReqRepresentation(conn, appId, apiCode, operation.getName(), "RESPONSE",
						operation.getListResponseRepresentation());

			} else {
				/* Changed by Leelaprasad for requirement TENJINCG-560 ends */
				pst = conn.prepareStatement(
						"UPDATE TJN_AUT_API_OP_REPRESENTATIONS SET API_REPRESENTATION_DESCRIPTION=? WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=? AND REPRESENTATION_TYPE=? AND API_RESP_CODE=?");

				for (Representation reqRepresentation : operation.getListRequestRepresentation()) {
					pst.setString(1, reqRepresentation.getRepresentationDescription());
					pst.setInt(2, appId);
					pst.setString(3, apiCode);
					pst.setString(4, operation.getName());
					pst.setString(5, "REQUEST");
					pst.setInt(6, reqRepresentation.getResponseCode());
					pst.addBatch();
				}
				for (Representation respRepresentation : operation.getListResponseRepresentation()) {
					pst.setString(1, respRepresentation.getRepresentationDescription());
					pst.setInt(2, appId);
					pst.setString(3, apiCode);
					pst.setString(4, operation.getName());
					pst.setString(5, "RESPONSE");
					pst.setInt(6, respRepresentation.getResponseCode());
					pst.addBatch();
				}
				pst.executeBatch();
				// changes done by parveen, Leelaprasad for #405 ends
				
				/* Changed by Leelaprasad for requirement TENJINCG-560 starts */
			}
			/* Changed by Leelaprasad for requirement TENJINCG-560 starts */
		} catch (SQLException e) {
			
			logger.error("ERROR updating descriptors for operation", e);
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}

	public List<ApiOperation> fetchApiOperations(String app, String code) {

		logger.info("Fetching the operations...");

		Connection conn;

		List<ApiOperation> operations = new ArrayList<ApiOperation>();

		try {

			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);

		} catch (DatabaseException e1) {

			

			logger.error("ERROR getting database connection", e1);

			return null;

		}

		PreparedStatement pst = null;
		ResultSet rs = null;

		try {

			pst = conn.prepareStatement("SELECT * FROM TJN_AUT_API_OPERATIONS WHERE APP_ID=? AND API_CODE=?");

			pst.setString(1, app);

			pst.setString(2, code);

			rs = pst.executeQuery();

			while (rs.next()) {

				ApiOperation operation = new ApiOperation();

				operation.setName(rs.getString("API_OPERATION"));

				operations.add(operation);

			}

			

		} catch (SQLException e) {

			

			logger.error("ERROR fetching operation", e);

		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);

		}

		logger.info("Done");

		return operations;

	}

	public void persistApi(int appId, Api api) throws DatabaseException {

		logger.debug("entered persistApi(int appId, Api api)");
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {

			logger.debug("Setting auto-commit to false");
			//conn.setAutoCommit(false);

			logger.debug("Inserting API Information");
			PreparedStatement pst = conn.prepareStatement(
					"INSERT INTO TJN_AUT_APIS (APP_ID,API_CODE,API_NAME,API_TYPE,API_URL) VALUES(?,?,?,?,?)");
			pst.setInt(1, appId);
			pst.setString(2, api.getCode());
			pst.setString(3, api.getName());
			pst.setString(4, api.getType());
			pst.setString(5, api.getUrl());
			/* Modified by Preeti for TENJINCG-616 starts */
			pst.execute();
			/* Modified by Preeti for TENJINCG-616 ends */
			
			DatabaseHelper.close(pst);

			if (api.getOperations() != null && api.getOperations().size() > 0) {
				logger.debug("Inserting Operations information");
				// changes done by parveen, Leelaprasad for #405 starts
				pst = conn.prepareStatement(
						"INSERT INTO TJN_AUT_API_OPERATIONS (APP_ID,API_CODE,API_OPERATION,API_METHOD_NAME) VALUES(?,?,?,?,?,?)");
				// changes done by parveen, Leelaprasad for #405 ends
				for (ApiOperation op : api.getOperations()) {
					pst.setInt(1, appId);
					pst.setString(2, api.getCode());
					pst.setString(3, op.getName());
					pst.setString(4, op.getMethod());
					pst.addBatch();
				}

				pst.executeBatch();
				
				DatabaseHelper.close(pst);

				for (ApiOperation op : api.getOperations()) {
					try {
						if (op.getResourceParameters() != null && op.getResourceParameters().size() > 0) {
							try {
								logger.debug("Inserting resource parameter information");
								pst = conn.prepareStatement(
										"INSERT INTO TJN_AUT_API_OP_PARAMETERS (APP_ID, API_CODE, API_OPERATION, PARAMETER_NAME, PARAMETER_TYPE, PARAMETER_STYLE, REP_TYPE) Values (?,?,?,?,?,?,?)");
								for (Parameter p : op.getResourceParameters()) {
									pst.setInt(1, appId);
									pst.setString(2, api.getCode());
									pst.setString(3, op.getName());
									pst.setString(4, p.getName());
									pst.setString(5, p.getType());
									pst.setString(6, p.getStyle());
									pst.setString(7, "RESOURCE");
									pst.addBatch();
								}
								pst.executeBatch();
								
								DatabaseHelper.close(pst);
							} finally {
								DatabaseHelper.close(pst);
							}
						}
						// changes done by parveen, Leelaprasad for #405 starts

						if (op.getListRequestRepresentation().size() > 0) {
							try {
								logger.debug("Inserting request representation information");
								pst = conn.prepareStatement(
										"INSERT INTO TJN_AUT_API_OP_REPRESENTATIONS (APP_ID, API_CODE, API_OPERATION, MEDIA_TYPE, REPRESENTATION_TYPE,API_REPRESENTATION_DESCRIPTION,API_RESP_CODE VALUES (?,?,?,?,?)");
								for (Representation reqRepresentation : op.getListRequestRepresentation()) {
									pst.setInt(1, appId);
									pst.setString(2, api.getCode());
									pst.setString(3, op.getName());
									pst.setString(4, reqRepresentation.getMediaType());
									pst.setString(5, "REQUEST");
									pst.setString(5, reqRepresentation.getRepresentationDescription());
									pst.setInt(6, reqRepresentation.getResponseCode());
									pst.addBatch();
								}

								pst.executeBatch();
								
								DatabaseHelper.close(pst);
							} finally {
								DatabaseHelper.close(pst);
							}
							for (Representation reqRepresentation : op.getListRequestRepresentation()) {
								try {
									if (reqRepresentation.getParameters().size() > 0) {
										pst = conn.prepareStatement(
												"INSERT INTO TJN_AUT_API_OP_PARAMETERS (APP_ID, API_CODE, API_OPERATION, PARAMETER_NAME, PARAMETER_TYPE, PARAMETER_STYLE, REP_TYPE) Values (?,?,?,?,?,?,?)");
										for (Parameter p : reqRepresentation.getParameters()) {
											pst.setInt(1, appId);
											pst.setString(2, api.getCode());
											pst.setString(3, op.getName());
											pst.setString(4, p.getName());
											pst.setString(5, p.getType());
											pst.setString(6, p.getStyle());
											pst.setString(7, "REQUEST");
											pst.addBatch();

										}
										pst.executeBatch();
										
										DatabaseHelper.close(pst);
									}
								} finally {
									DatabaseHelper.close(pst);
								}
							}

						}

						if (op.getListResponseRepresentation().size() > 0) {
							try {
								logger.debug("Inserting response representation information");
								pst = conn.prepareStatement(
										"INSERT INTO TJN_AUT_API_OP_REPRESENTATIONS (APP_ID, API_CODE, API_OPERATION, MEDIA_TYPE, REPRESENTATION_TYPE,API_REPRESENTATION_DESCRIPTION,API_RESP_CODE VALUES (?,?,?,?,?)");
								for (Representation respRepresentation : op.getListResponseRepresentation()) {
									pst.setInt(1, appId);
									pst.setString(2, api.getCode());
									pst.setString(3, op.getName());
									pst.setString(4, respRepresentation.getMediaType());
									pst.setString(5, "RESPONSE");
									pst.setString(5, respRepresentation.getRepresentationDescription());
									pst.setInt(6, respRepresentation.getResponseCode());
									pst.addBatch();
								}

								pst.executeBatch();
								
								DatabaseHelper.close(pst);
							} finally {
								DatabaseHelper.close(pst);
							}
							for (Representation respRepresentation : op.getListResponseRepresentation()) {
								try {
									if (respRepresentation.getParameters().size() > 0) {
										pst = conn.prepareStatement(
												"INSERT INTO TJN_AUT_API_OP_PARAMETERS (APP_ID, API_CODE, API_OPERATION, PARAMETER_NAME, PARAMETER_TYPE, PARAMETER_STYLE, REP_TYPE) Values (?,?,?,?,?,?,?)");
										for (Parameter p : respRepresentation.getParameters()) {
											pst.setInt(1, appId);
											pst.setString(2, api.getCode());
											pst.setString(3, op.getName());
											pst.setString(4, p.getName());
											pst.setString(5, p.getType());
											pst.setString(6, p.getStyle());
											pst.setString(7, "RESPONSE");
											pst.addBatch();

										}
										pst.executeBatch();
										
										DatabaseHelper.close(pst);
									}
								} finally {
									DatabaseHelper.close(pst);
								}
							}

						}
					} finally {
						DatabaseHelper.close(pst);
					}
					// changes done by parveen, Leelaprasad for #405 ends
				}
			}

			logger.debug("Insertion successful. Committing the transaction");
			//conn.commit();

		} catch (Exception e) {
			logger.error("ERROR creating API", e);
			try {
				conn.rollback();
			} catch (Exception e1) {
				logger.error("ERROR --> Exception occurred while rolling back transaction", e1);
			}
			throw new DatabaseException("Could not create record", e);
		} finally {
			DatabaseHelper.close(conn);
		}

	}

	/* Changed by leelaprasad for TJN210-100 starts */
	public void persistApi(Connection conn, Aut aut, Api api) throws DatabaseException {
		int appId = aut.getId();
		String authType = aut.getApiAuthenticationType();
		/* Changed by leelaprasad for TJN210-100 ends */
		logger.debug("entered persistApi(int appId, Api api)");
		// Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {

			logger.debug("Setting auto-commit to false");
			//conn.setAutoCommit(false);

			logger.debug("Inserting API Information");
			/* modified by Padmavathi for TENJINCG-545 starts */
			PreparedStatement pst = conn.prepareStatement(
					"INSERT INTO TJN_AUT_APIS (APP_ID,API_CODE,API_NAME,API_TYPE,API_URL,GROUP_NAME) VALUES(?,?,?,?,?,?)");
			/* modified by Padmavathi for TENJINCG-545 ends */
			pst.setInt(1, appId);
			pst.setString(2, api.getCode());
			pst.setString(3, api.getName());
			pst.setString(4, api.getType());
			pst.setString(5, api.getUrl());
			/* added by Padmavathi for TENJINCG-545 starts */
			pst.setString(6, api.getGroup());
			/* added by Padmavathi for TENJINCG-545 ends */
			/* Modified by Preeti for TENJINCG-616 starts */
			pst.execute();
			/* Modified by Preeti for TENJINCG-616 ends */
			
			DatabaseHelper.close(pst);

			if (api.getOperations() != null && api.getOperations().size() > 0) {
				try {
					logger.debug("Inserting Operations information");
					// changes done by parveen, Leelaprasad for #405 starts
					/* Changed by leelaprasad for TJN210-100 starts */
					pst = conn.prepareStatement(
							"INSERT INTO TJN_AUT_API_OPERATIONS (APP_ID,API_CODE,API_OPERATION,API_METHOD_NAME,API_AUTH_TYPE) VALUES(?,?,?,?,?)");
					/* Changed by leelaprasad for TJN210-100 ends */
					// changes done by parveen, Leelaprasad for #405 ends
					for (ApiOperation op : api.getOperations()) {
						pst.setInt(1, appId);
						pst.setString(2, api.getCode());
						pst.setString(3, op.getName());
						// changes done by parveen, Leelaprasad for #405 starts
						pst.setString(4, op.getMethod());
						// changes done by parveen, Leelaprasad for #405 ends
						/* Changed by leelaprasad for TJN210-100 starts */
						pst.setString(5, authType);
						/* Changed by leelaprasad for TJN210-100 ends */
						pst.addBatch();
					}

					pst.executeBatch();
					
					DatabaseHelper.close(pst);
				} finally {
					DatabaseHelper.close(pst);
				}
				for (ApiOperation op : api.getOperations()) {
					if (op.getResourceParameters() != null && op.getResourceParameters().size() > 0) {
						try {
							logger.debug("Inserting resource parameter information");
							pst = conn.prepareStatement(
									"INSERT INTO TJN_AUT_API_OP_PARAMETERS (APP_ID, API_CODE, API_OPERATION, PARAMETER_NAME, PARAMETER_TYPE, PARAMETER_STYLE, REP_TYPE) Values (?,?,?,?,?,?,?)");
							for (Parameter p : op.getResourceParameters()) {
								pst.setInt(1, appId);
								pst.setString(2, api.getCode());
								pst.setString(3, op.getName());
								pst.setString(4, p.getName());
								pst.setString(5, p.getType());
								pst.setString(6, p.getStyle());
								pst.setString(7, "RESOURCE");
								pst.addBatch();
							}
							pst.executeBatch();
							
							DatabaseHelper.close(pst);
						} finally {
							DatabaseHelper.close(pst);
						}
					}
					// changes done by parveen, Leelaprasad for #405 starts
					

					if (op.getListRequestRepresentation() != null && op.getListRequestRepresentation().size() > 0) {
						try {
							logger.debug("Inserting request representation information");
							pst = conn.prepareStatement(
									"INSERT INTO TJN_AUT_API_OP_REPRESENTATIONS (APP_ID, API_CODE, API_OPERATION, MEDIA_TYPE, REPRESENTATION_TYPE,API_REPRESENTATION_DESCRIPTION,API_RESP_CODE) VALUES (?,?,?,?,?,?,?)");
							for (Representation reqRepresentation : op.getListRequestRepresentation()) {
								pst.setInt(1, appId);
								pst.setString(2, api.getCode());
								pst.setString(3, op.getName());
								pst.setString(4, reqRepresentation.getMediaType());
								pst.setString(5, "REQUEST");
								pst.setString(6, reqRepresentation.getRepresentationDescription());
								pst.setInt(7, reqRepresentation.getResponseCode());
								pst.addBatch();
							}

							pst.executeBatch();
							
							DatabaseHelper.close(pst);
						} finally {
							DatabaseHelper.close(pst);
						}
						for (Representation reqRepresentation : op.getListRequestRepresentation()) {
							if (reqRepresentation.getParameters().size() > 0) {
								try {
									pst = conn.prepareStatement(
											"INSERT INTO TJN_AUT_API_OP_PARAMETERS (APP_ID, API_CODE, API_OPERATION, PARAMETER_NAME, PARAMETER_TYPE, PARAMETER_STYLE, REP_TYPE) Values (?,?,?,?,?,?,?)");
									for (Parameter p : reqRepresentation.getParameters()) {
										pst.setInt(1, appId);
										pst.setString(2, api.getCode());
										pst.setString(3, op.getName());
										pst.setString(4, p.getName());
										pst.setString(5, p.getType());
										pst.setString(6, p.getStyle());
										pst.setString(7, "REQUEST");
										pst.addBatch();

									}
									pst.executeBatch();
									
									DatabaseHelper.close(pst);
								} finally {
									DatabaseHelper.close(pst);
								}
							}
						}

					}
					if (op.getListResponseRepresentation() != null && op.getListResponseRepresentation().size() > 0) {
						try {
							logger.debug("Inserting response representation information");
							pst = conn.prepareStatement(
									"INSERT INTO TJN_AUT_API_OP_REPRESENTATIONS (APP_ID, API_CODE, API_OPERATION, MEDIA_TYPE, REPRESENTATION_TYPE,API_REPRESENTATION_DESCRIPTION,API_RESP_CODE) VALUES (?,?,?,?,?,?,?)");
							for (Representation respRepresentation : op.getListResponseRepresentation()) {
								pst.setInt(1, appId);
								pst.setString(2, api.getCode());
								pst.setString(3, op.getName());
								pst.setString(4, respRepresentation.getMediaType());
								pst.setString(5, "RESPONSE");
								pst.setString(6, respRepresentation.getRepresentationDescription());
								pst.setInt(7, respRepresentation.getResponseCode());
								pst.addBatch();
							}

							pst.executeBatch();
							
							DatabaseHelper.close(pst);
						} finally {
							DatabaseHelper.close(pst);
						}
						for (Representation respRepresentation : op.getListResponseRepresentation()) {
							if (respRepresentation.getParameters().size() > 0) {
								try {
									pst = conn.prepareStatement(
											"INSERT INTO TJN_AUT_API_OP_PARAMETERS (APP_ID, API_CODE, API_OPERATION, PARAMETER_NAME, PARAMETER_TYPE, PARAMETER_STYLE, REP_TYPE) Values (?,?,?,?,?,?,?)");
									for (Parameter p : respRepresentation.getParameters()) {
										pst.setInt(1, appId);
										pst.setString(2, api.getCode());
										pst.setString(3, op.getName());
										pst.setString(4, p.getName());
										pst.setString(5, p.getType());
										pst.setString(6, p.getStyle());
										pst.setString(7, "RESPONSE");
										pst.addBatch();

									}
									pst.executeBatch();
									
									DatabaseHelper.close(pst);
								} finally {
									DatabaseHelper.close(pst);
								}
							}
						}

					}
					// changes done by parveen, Leelaprasad for #405 ends

				}
			}

			logger.debug("Insertion successful. Committing the transaction");
			//conn.commit();

		} catch (Exception e) {
			logger.error("ERROR creating API", e);
			try {
				conn.rollback();
			} catch (Exception e1) {
				logger.error("ERROR --> Exception occurred while rolling back transaction", e1);
			}
			throw new DatabaseException("Could not create record", e);
		} 
	}

	/* Changed by Gangadhar Badagi for TENJINCG-451 starts */
	

	/* Changed by leelaprasad for TJN210-100 starts */
	public void updateApi(Connection conn, Aut aut, Api newApi, Api existedApi) throws DatabaseException {
		int appId = aut.getId();
		String authType = aut.getApiAuthenticationType();
		/* Changed by leelaprasad for TJN210-100 ends */
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		PreparedStatement pst = null;
		try {

			//logger.debug("Setting auto-commit to false");
			//conn.setAutoCommit(false);

			logger.debug("Updating API Information");
			/* modified by Padmavathi for TENJINCG-545 starts */
			pst = conn.prepareStatement(
					"UPDATE TJN_AUT_APIS SET API_NAME=?, API_TYPE=?, API_URL=?,GROUP_NAME=? WHERE APP_ID=? AND API_CODE=?");
			/* modified by Padmavathi for TENJINCG-545 ends */
			pst.setString(1, newApi.getName());
			pst.setString(2, newApi.getType());
			pst.setString(3, newApi.getUrl());
			/* added by Padmavathi for TENJINCG-545 starts */
			pst.setString(4, newApi.getGroup());
			/* added by Padmavathi for TENJINCG-545 ends */
			pst.setInt(5, appId);
			pst.setString(6, existedApi.getCode());
			pst.executeUpdate();
			
			DatabaseHelper.close(pst);
			if (newApi.getOperations() != null && newApi.getOperations().size() > 0) {
				logger.debug("Updating Operations information");
				/* Changed by leelaprasad for TJN210-100 starts */
				pst = conn.prepareStatement(
						"UPDATE TJN_AUT_API_OPERATIONS SET API_OPERATION=?, API_METHOD_NAME=?,API_AUTH_TYPE=? WHERE APP_ID=? AND API_CODE=?");
				/* Changed by leelaprasad for TJN210-100 ends */
				for (ApiOperation op : newApi.getOperations()) {
					pst.setString(1, op.getName());
					pst.setString(2, op.getMethod());
					/* Changed by leelaprasad for TJN210-100 starts */
					pst.setString(3, authType);
					pst.setInt(4, appId);
					pst.setString(5, existedApi.getCode());
					/* Changed by leelaprasad for TJN210-100 ends */
					pst.addBatch();
				}
				pst.executeBatch();
				
				DatabaseHelper.close(pst);
				for (ApiOperation op : newApi.getOperations()) {

					if (op.getResourceParameters() != null && op.getResourceParameters().size() > 0) {

						for (Parameter p : op.getResourceParameters()) {
							try {
								Parameter doesParamaterExist = this.hydrateParameter(conn, appId, existedApi.getCode(),
										op.getName(), p.getName());
								if (doesParamaterExist.getName() == null) {
									logger.debug("Inserting resource parameter information");
									pst = conn.prepareStatement(
											"INSERT INTO TJN_AUT_API_OP_PARAMETERS (APP_ID, API_CODE, API_OPERATION, PARAMETER_NAME, PARAMETER_TYPE, PARAMETER_STYLE, REP_TYPE) Values (?,?,?,?,?,?,?)");
									pst.setInt(1, appId);
									pst.setString(2, newApi.getCode());
									pst.setString(3, op.getName());
									pst.setString(4, p.getName());
									pst.setString(5, p.getType());
									pst.setString(6, p.getStyle());
									pst.setString(7, "RESOURCE");
									pst.execute();
								}

								else {
									logger.debug("Updating resource parameter information");
									pst = conn.prepareStatement(
											"UPDATE TJN_AUT_API_OP_PARAMETERS SET PARAMETER_TYPE=?, PARAMETER_STYLE=?, REP_TYPE=? WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=? AND PARAMETER_NAME=?");

									pst.setString(1, p.getType());
									pst.setString(2, p.getStyle());
									pst.setString(3, "RESOURCE");
									pst.setInt(4, appId);
									pst.setString(5, existedApi.getCode());
									pst.setString(6, op.getName());
									pst.setString(7, p.getName());
									pst.execute();
								}
							} finally {
								DatabaseHelper.close(pst);
							}
						}
					}

				}
			}

			//logger.debug("Updation successful. Committing the transaction");
			//conn.commit();

		} catch (Exception e) {
			logger.error("ERROR updating API", e);
			try {
				conn.rollback();
			} catch (Exception e1) {
				logger.error("ERROR --> Exception occurred while rolling back transaction", e1);
			}
			throw new DatabaseException("Could not update record", e);
		}
	}

	/* Changed by Gangadhar Badagi for TENJINCG-451 ends */
	public void persistOperationRepresentations(int appId, String apiCode, ApiOperation operation) {

		Connection conn;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		} catch (DatabaseException e1) {
			
			logger.error("ERROR getting database connection", e1);
			return;
		}

		PreparedStatement pst = null;

		try {
			// changes done by parveen, Leelaprasad for #405 starts
			pst = conn.prepareStatement(
					"INSERT INTO TJN_AUT_API_OP_REPRESENTATIONS (APP_ID,API_CODE,API_OPERATION,MEDIA_TYPE,REPRESENTATION_TYPE,API_REPRESENTATION_DESCRIPTION,API_RESP_CODE) VALUES (?,?,?,?,?,?,?) ");

			for (Representation requestRepresentation : operation.getListRequestRepresentation()) {
				pst.setInt(1, appId);
				pst.setString(2, apiCode);
				pst.setString(3, operation.getName());
				pst.setString(4, requestRepresentation.getMediaType());
				pst.setString(5, "REQUEST");
				pst.setString(6, requestRepresentation.getRepresentationDescription());
				pst.setInt(7, requestRepresentation.getResponseCode());
				pst.addBatch();
			}
			for (Representation responseRepresentation : operation.getListResponseRepresentation()) {
				pst.setInt(1, appId);
				pst.setString(2, apiCode);
				pst.setString(3, operation.getName());
				pst.setString(4, responseRepresentation.getMediaType());
				pst.setString(5, "RESPONSE");
				pst.setString(6, responseRepresentation.getRepresentationDescription());
				pst.setInt(7, responseRepresentation.getResponseCode());
				pst.addBatch();
			}

			pst.executeBatch();
			// changes done by parveen, Leelaprasad for #405 ends
		} catch (SQLException e) {
			
			logger.error("ERROR updating descriptors for operation", e);
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}

	public void persistApiOperationInDetail(ApiOperation operation, String apiCode, int appId)
			throws DatabaseException {
		if (operation == null) {
			logger.warn("Operation object is null. Cannot save!");
			return;
		}
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		try {
			logger.debug("Persisting operation [{}]", operation.getName());
			this.persistApiOperation(operation, apiCode, appId);

			logger.debug("Persisting resource parameters for operation [{}]", operation.getName());
			this.persistParameters(conn, appId, apiCode, operation.getName(), "RESOURCE",
					operation.getResourceParameters());
			/*	Added By Paneendra for TENJINCG-1239 starts */
			logger.debug("Persisting Header Representation");
			this.persistHeaderRepresentation(conn, appId, apiCode, operation.getName(), "HEADER",
					operation.getListHeaderRepresentation());
			/*	Added By Paneendra for TENJINCG-1239 ends */
			logger.debug("Persisting Request Representation");
			this.persistReqRepresentation(conn, appId, apiCode, operation.getName(), "REQUEST",
					operation.getListRequestRepresentation());

			logger.debug("Persisting Response Representation");
			this.persistRespRepresentation(conn, appId, apiCode, operation.getName(), "RESPONSE",
					operation.getListResponseRepresentation());
			// changes done by parveen, Leelaprasad for #405 ends
		} finally {
			/*
			 * try{ if(conn != null) conn.close(); }catch(Exception ignore) {}
			 */
			DatabaseHelper.close(conn);
		}

	}



	// changes done by parveen, Leelaprasad for #405 starts
	
	/*	Added By Paneendra for TENJINCG-1239 starts */
	private void persistHeaderRepresentation(Connection conn, int appId, String apiCode, String operationName,
			String repType, List<Representation> headerRepresentations) throws DatabaseException {
		/* Fix for T25IT-187 */
		if (/* Added by sriram for TENJINCG-1118 */headerRepresentations == null
				|| /* Added by sriram for TENJINCG-1118 ends */ headerRepresentations.size() < 1) {
			return;
		}
		/* Fix for T25IT-187 ends */
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(
					"INSERT INTO TJN_AUT_API_OP_REPRESENTATIONS (APP_ID,API_CODE,API_OPERATION,MEDIA_TYPE,REPRESENTATION_TYPE,API_REPRESENTATION_DESCRIPTION,API_RESP_CODE) VALUES (?,?,?,?,?,?,?) ");
			for (Representation representation : headerRepresentations) {
				pst.setInt(1, appId);
				pst.setString(2, apiCode);
				pst.setString(3, operationName);
				pst.setString(4, representation.getMediaType());
				pst.setString(5, repType.toUpperCase());
				pst.setString(6, representation.getRepresentationDescription());
				pst.setInt(7, representation.getResponseCode());
				pst.addBatch();
			}

			pst.executeBatch();
			//conn.commit();
			
		} catch (Exception e) {
			logger.error("ERROR persisting {} representation for operation {}, API {}", repType, operationName, apiCode,
					e);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				throw new DatabaseException("Could not persist operation representation. Please contact Tenjin support.");
			}
			
		} finally {
			DatabaseHelper.close(pst);
		}
	}

	/*	Added By Paneendra for TENJINCG-1239 ends */
	
	private void persistReqRepresentation(Connection conn, int appId, String apiCode, String operationName,
			String repType, List<Representation> requestRepresentations) throws DatabaseException {
		/* Fix for T25IT-187 */
		if (/* Added by sriram for TENJINCG-1118 */requestRepresentations == null
				|| /* Added by sriram for TENJINCG-1118 ends */ requestRepresentations.size() < 1) {
			return;
		}
		/* Fix for T25IT-187 ends */
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(
					"INSERT INTO TJN_AUT_API_OP_REPRESENTATIONS (APP_ID,API_CODE,API_OPERATION,MEDIA_TYPE,REPRESENTATION_TYPE,API_REPRESENTATION_DESCRIPTION,API_RESP_CODE) VALUES (?,?,?,?,?,?,?) ");
			for (Representation representation : requestRepresentations) {
				pst.setInt(1, appId);
				pst.setString(2, apiCode);
				pst.setString(3, operationName);
				pst.setString(4, representation.getMediaType());
				pst.setString(5, repType.toUpperCase());
				pst.setString(6, representation.getRepresentationDescription());
				pst.setInt(7, representation.getResponseCode());
				pst.addBatch();
			}

			pst.executeBatch();
			//conn.commit();
			
		} catch (Exception e) {
			logger.error("ERROR persisting {} representation for operation {}, API {}", repType, operationName, apiCode,
					e);
			
			try {
				conn.rollback();
			} catch (SQLException e1) {
				throw new DatabaseException("Could not persist operation representation. Please contact Tenjin support.");
			}
		} finally {
			DatabaseHelper.close(pst);
		}
	}

	private void persistRespRepresentation(Connection conn, int appId, String apiCode, String operationName,
			String repType, List<Representation> responseRepresentations) throws DatabaseException {
		/* Fix for T25IT-187 */
		if (responseRepresentations == null || responseRepresentations.size() < 1) {
			return;
		}
		/* Fix for T25IT-187 ends */

		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(
					"INSERT INTO TJN_AUT_API_OP_REPRESENTATIONS (APP_ID,API_CODE,API_OPERATION,MEDIA_TYPE,REPRESENTATION_TYPE,API_REPRESENTATION_DESCRIPTION,API_RESP_CODE) VALUES (?,?,?,?,?,?,?) ");

			for (Representation representation : responseRepresentations) {
				pst.setInt(1, appId);
				pst.setString(2, apiCode);
				pst.setString(3, operationName);
				pst.setString(4, representation.getMediaType());
				pst.setString(5, repType.toUpperCase());
				pst.setString(6, representation.getRepresentationDescription());
				pst.setInt(7, representation.getResponseCode());
				pst.addBatch();
			}

			pst.executeBatch();
			//conn.commit();
		} catch (Exception e) {
			logger.error("ERROR persisting {} representation for operation {}, API {}", repType, operationName, apiCode,
					e);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				throw new DatabaseException("Could not persist operation representation. Please contact Tenjin support.");
			}
		} finally {
			DatabaseHelper.close(pst);
		}
	}

	// changes done by parveen, Leelaprasad for #405 ends
	private void persistParameters(Connection conn, int appId, String apiCode, String operationName, String repType,
			List<Parameter> parameters) throws DatabaseException {
		PreparedStatement pst = null;
		try {
			for (Parameter parameter : parameters) {
				pst = conn.prepareStatement(
						"INSERT INTO TJN_AUT_API_OP_PARAMETERS (APP_ID,API_CODE,API_OPERATION,PARAMETER_NAME,PARAMETER_TYPE,PARAMETER_STYLE,REP_TYPE) VALUES (?,?,?,?,?,?,?) ");
				pst.setInt(1, appId);
				pst.setString(2, apiCode);
				pst.setString(3, operationName);
				pst.setString(4, parameter.getName());
				pst.setString(5, parameter.getType());
				pst.setString(6, parameter.getStyle());
				pst.setString(7, repType.toUpperCase());
				pst.execute();
				DatabaseHelper.close(pst);
			}
		} catch (Exception e) {
			logger.error("ERROR persisting {} parameters for operation {}, API {}", repType, operationName, apiCode, e);
			throw new DatabaseException("Could not persist operation parameters. Please contact Tenjin support.");
		} finally {
			DatabaseHelper.close(pst);
		}
	}

	public void clearApiOperationInDetail(int appId, String apiCode, String optName) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		try {
			logger.debug("Clearing operation [{}]", optName);
			this.clearApiOperation(conn, appId, apiCode, optName);

			logger.debug("Clearing resource parameters for operation [{}]", optName);
			this.clearApiOperationParameters(conn, appId, apiCode, optName);

			logger.debug("Clearing Request Representation for operation [{}]", optName);
			this.clearApiOperationRepresentation(conn, appId, apiCode, optName);

		} finally {
			DatabaseHelper.close(conn);
		}

	}

	private void clearApiOperationRepresentation(Connection conn, int appId, String apiCode, String optName)
			throws DatabaseException {
		
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(
					"DELETE FROM TJN_AUT_API_OP_REPRESENTATIONS WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);
			pst.setString(3, optName);
			pst.execute();
		} catch (SQLException e) {
			logger.error("Could not clear Representation in API Operation [{}] in API code [{}] in APP [{}]", optName,
					apiCode, appId);
			throw new DatabaseException("Could not clear Representation in API Operation [" + optName
					+ "] in API code [" + apiCode + "] in APP [" + appId + "]");
		} finally {
			DatabaseHelper.close(pst);
		}
	}

	private void clearApiOperationParameters(Connection conn, int appId, String apiCode, String optName)
			throws DatabaseException {
		
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(
					"DELETE FROM TJN_AUT_API_OP_PARAMETERS WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);
			pst.setString(3, optName);
			pst.execute();
		} catch (SQLException e) {
			logger.error("Could not clear Parameters in API Operation [{}] in API code [{}] in APP [{}]", optName,
					apiCode, appId);
			throw new DatabaseException("Could not clear Parameters in API Operation [" + optName + "] in API code ["
					+ apiCode + "] in APP [" + appId + "]");
		} finally {
			DatabaseHelper.close(pst);
		}
	}

	private void clearApiOperation(Connection conn, int appId, String apiCode, String optName)
			throws DatabaseException {
		
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(
					"DELETE FROM TJN_AUT_API_OPERATIONS WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);
			pst.setString(3, optName);
			pst.execute();
		} catch (SQLException e) {
			logger.error("Could not clear API Operation [{}] in API code [{}] in APP [{}]", optName, apiCode, appId);
			throw new DatabaseException("Could not clear API Operation [" + optName + "] in API code [" + apiCode
					+ "] in APP [" + appId + "]");
		} finally {
			DatabaseHelper.close(pst);
		}
	}

	/* Added by Gangadhar Badagi for TENJINCG-320 starts */
	public ArrayList<ApiLearnerResultBean> hydrateLearningHistory(String apiCode, int appID) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		ArrayList<ApiLearnerResultBean> lrnrRuns = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement(
					"Select * from lrnr_audit_trail_api where lrnr_app_id=? and lrnr_api_code=? order by LRNR_RUN_ID desc");
			pst.setInt(1, appID);
			pst.setString(2, apiCode);

			rs = pst.executeQuery();
			lrnrRuns = new ArrayList<ApiLearnerResultBean>();
			while (rs.next()) {
				ApiLearnerResultBean lrnrBean = new ApiLearnerResultBean();
				lrnrBean.setId(rs.getInt("LRNR_RUN_ID"));
				lrnrBean.setStartTimestamp(rs.getTimestamp("LRNR_START_TIME"));
				lrnrBean.setEndTimestamp(rs.getTimestamp("LRNR_END_TIME"));
				lrnrBean.setStatus(rs.getString("LRNR_STATUS"));
				lrnrBean.setMessage(rs.getString("LRNR_MESSAGE"));
				lrnrBean.setUser(rs.getString("LRNR_USER_ID"));
				/* Added by Roshni for T25IT-191 starts */
				lrnrBean.setOperationName(rs.getString("LRNR_OPERATION"));
				/* Added by Roshni for T25IT-191 ends */
				/* Added by Ashiki for TENJINCG-1105 starts */
				lrnrBean.setLastLearntFieldCount(rs.getInt("FIELDS_LEARNT_COUNT"));
				/* Added by Ashiki for TENJINCG-1105 ends */
				if (lrnrBean.getStartTimestamp() != null && lrnrBean.getEndTimestamp() != null) {
					long startMillis = lrnrBean.getStartTimestamp().getTime();
					long endMillis = lrnrBean.getEndTimestamp().getTime();

					long millis = endMillis - startMillis;

					String eTime = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
							TimeUnit.MILLISECONDS.toMinutes(millis)
									- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
							TimeUnit.MILLISECONDS.toSeconds(millis)
									- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
					lrnrBean.setElapsedTime(eTime);
				}
				if (lrnrBean.getStatus().equalsIgnoreCase("QUEUED")/* ||lrnrBean.getStartTimestamp()==null */) {
					continue;
				} else {
					lrnrRuns.add(lrnrBean);
				}
			}

		} catch (SQLException e) {

			throw new DatabaseException("Could not fetch learning history for test function " + apiCode, e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return lrnrRuns;
	}
	/* Added by Gangadhar Badagi for TENJINCG-320 ends */

	public ArrayList<ApiLearnerResultBean> hydrateLearningHistory1(String apiCode, int appID) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);

		if (conn == null) {
			throw new DatabaseException("Invalid Database Connection");
		}
		ArrayList<ApiLearnerResultBean> lrnrRuns = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement(
					"Select * from lrnr_audit_trail_api where lrnr_app_id=? and lrnr_api_code=? and LRNR_STATUS=? order by LRNR_RUN_ID desc");
			pst.setInt(1, appID);
			pst.setString(2, apiCode);
			pst.setString(3, "Complete");
			rs = pst.executeQuery();
			lrnrRuns = new ArrayList<ApiLearnerResultBean>();
			while (rs.next()) {
				ApiLearnerResultBean lrnrBean = new ApiLearnerResultBean();
				lrnrBean.setId(rs.getInt("LRNR_RUN_ID"));
				lrnrBean.setStartTimestamp(rs.getTimestamp("LRNR_START_TIME"));
				lrnrBean.setEndTimestamp(rs.getTimestamp("LRNR_END_TIME"));
				lrnrBean.setStatus(rs.getString("LRNR_STATUS"));
				lrnrBean.setMessage(rs.getString("LRNR_MESSAGE"));
				lrnrBean.setUser(rs.getString("LRNR_USER_ID"));
				/* Added by Roshni for T25IT-191 starts */
				lrnrBean.setOperationName(rs.getString("LRNR_OPERATION"));
				/* Added by Roshni for T25IT-191 ends */

				if (lrnrBean.getStartTimestamp() != null && lrnrBean.getEndTimestamp() != null) {
					long startMillis = lrnrBean.getStartTimestamp().getTime();
					long endMillis = lrnrBean.getEndTimestamp().getTime();

					long millis = endMillis - startMillis;

					String eTime = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
							TimeUnit.MILLISECONDS.toMinutes(millis)
									- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
							TimeUnit.MILLISECONDS.toSeconds(millis)
									- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
					lrnrBean.setElapsedTime(eTime);
				}
				if (lrnrBean.getStatus().equalsIgnoreCase("QUEUED")/* ||lrnrBean.getStartTimestamp()==null */) {
					continue;
				} else {
					lrnrRuns.add(lrnrBean);
				}
			}

		} catch (SQLException e) {

			throw new DatabaseException("Could not fetch learning history for test function " + apiCode, e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return lrnrRuns;
	}

	/* Fix for T25IT-188 */
	public void updateApiOperation(ApiOperation operation, int appId, String apiCode, String oldOperationName)
			throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(
					"UPDATE TJN_AUT_API_OPERATIONS SET API_OPERATION=?,  API_METHOD_NAME=? WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=?");
			pst.setString(1, operation.getName());
			// changes done by parveen, Leelaprasad for #405 starts
			pst.setString(2, operation.getMethod());
			pst.setInt(3, appId);
			pst.setString(4, apiCode);
			pst.setString(5, oldOperationName);

			pst.executeUpdate();

			DatabaseHelper.close(pst);

			if (!oldOperationName.equalsIgnoreCase(operation.getName())) {

				pst = conn.prepareStatement(
						"UPDATE tjn_aut_api_op_representations SET API_OPERATION=?,API_REPRESENTATION_DESCRIPTION=? WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=?");
				for (Representation requestRepresentation : operation.getListRequestRepresentation()) {
					pst.setString(1, operation.getName());
					pst.setString(2, requestRepresentation.getRepresentationDescription());
					pst.setInt(3, appId);
					pst.setString(4, apiCode);
					pst.setString(5, oldOperationName);
					pst.addBatch();
				}
				pst.executeBatch();
				DatabaseHelper.close(pst);
				pst = conn.prepareStatement(
						"UPDATE tjn_aut_api_op_representations SET API_OPERATION=?,API_REPRESENTATION_DESCRIPTION=?,API_RESP_CODE=? WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=?");
				for (Representation reponseRepresentation : operation.getListRequestRepresentation()) {
					pst.setString(1, operation.getName());
					pst.setString(2, reponseRepresentation.getRepresentationDescription());
					pst.setInt(3, reponseRepresentation.getResponseCode());
					pst.setInt(4, appId);
					pst.setString(5, apiCode);
					pst.setString(6, oldOperationName);
					pst.addBatch();
				}
				pst.executeBatch();
				DatabaseHelper.close(pst);

				pst = conn.prepareStatement(
						"UPDATE tjn_aut_api_op_parameters SET API_OPERATION=? WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=?");
				pst.setString(1, operation.getName());
				pst.setInt(2, appId);
				pst.setString(3, apiCode);
				pst.setString(4, oldOperationName);
				pst.execute();
				DatabaseHelper.close(pst);
			}

		} catch (SQLException e) {
			logger.error("ERROR updating API Operation", e);
			throw new DatabaseException(
					"Could not update operation due to an internal error. Please contact Tenjin Support.");
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}
	/* Fix for T25IT-188 ends */

	/* Fix for T25IT-181 */
	public void clearAllApiOperations(Connection conn, String apiCode, int appId) throws DatabaseException {
		// Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);

		PreparedStatement pst = null;

		try {
			pst = conn.prepareStatement("DELETE FROM TJN_AUT_API_OPERATIONS WHERE APP_ID=? AND API_CODE=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);

			pst.executeUpdate();

			
			DatabaseHelper.close(pst);

			/* Fix for T25IT-181 */
			pst = conn.prepareStatement("DELETE FROM TJN_AUT_API_OP_PARAMETERS WHERE APP_ID=? AND API_CODE=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);

			pst.executeUpdate();

			
			DatabaseHelper.close(pst);

			pst = conn.prepareStatement("DELETE FROM TJN_AUT_API_OP_REPRESENTATIONS WHERE APP_ID=? AND API_CODE=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);

			pst.executeUpdate();

			
			DatabaseHelper.close(pst);
			/* Fix for T25IT-181 ends */
		} catch (SQLException e) {
			
			logger.error("An ERROR occurred while deleting Operation", e);
			throw new DatabaseException(
					"Could not delete operation due to an internal error. Please contact Tenjin Support.");
		} finally {
			DatabaseHelper.close(pst);
		}
	}

	public void clearApi(int appId, String apiCode) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		try {
			//conn.setAutoCommit(false);
			this.clearAllApiOperations(conn, apiCode, appId);
			this.clearApi(conn, appId, apiCode);
		} catch (Exception e) {
			logger.error("ERROR while clearing API", e);
			try {
				conn.rollback();
			} catch (Exception ignore) {
			}

			throw new DatabaseException(
					"Could not clear API Information due to an internal error. Please contact Tenjin Support.");
		}  finally {
				DatabaseHelper.close(conn);
			}

	}
	/* Fix for T25IT-181 ends */

	/* Modified by Preeti for TENJINCG-600 starts */
	/* added by Preeti for TENJINCG-366 starts */
	public ArrayList<Api> hydrateAllApi(String appId, String apiType, String groupName) throws DatabaseException {

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<Api> apiList = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			String preparedQuery = "SELECT * FROM TJN_AUT_APIS WHERE APP_ID=?";
			if(Utilities.trim(groupName).length() > 0 && !Utilities.trim(groupName).equalsIgnoreCase("all") && !Utilities.trim(groupName).equalsIgnoreCase("null")) {
				preparedQuery += " AND GROUP_NAME=?"; 
			}
			if(Utilities.trim(apiType).length() > 0 && !Utilities.trim(apiType).equalsIgnoreCase("all") && !Utilities.trim(apiType).equalsIgnoreCase("null")){
				preparedQuery += " AND API_TYPE=?";
			}
			
			preparedQuery += " ORDER BY API_URL ASC";
			
			pst = conn.prepareStatement(preparedQuery);
			
			pst.setString(1, appId);
			if(pst.getParameterMetaData().getParameterCount() > 1) {
				if(Utilities.trim(groupName).length() > 0 && !Utilities.trim(groupName).equalsIgnoreCase("all") && !Utilities.trim(groupName).equalsIgnoreCase("null")) {
					pst.setString(2, groupName);
				}else {
					pst.setString(2, apiType);
				}
			}
			
			if(pst.getParameterMetaData().getParameterCount() > 2) {
				pst.setString(3, apiType);
			}
			
			rs = pst.executeQuery();
			apiList = new ArrayList<Api>();
			while (rs.next()) {
				Api api = new Api();
				api.setApplicationId(rs.getInt("APP_ID"));
				api.setCode(rs.getString("API_CODE"));
				api.setName(rs.getString("API_NAME"));
				api.setType(rs.getString("API_TYPE"));
				api.setGroup(rs.getString("GROUP_NAME"));
				api.setUrl(rs.getString("API_URL"));
				api.setUrlValid(ApiUtilities.isUrlValid(api.getUrl()));
				apiList.add(api);
			}


		} catch (Exception e) {
			throw new DatabaseException("Could not fetch API List", e);
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

		return apiList;
	}
	/* added by Preeti for TENJINCG-366 ends */

	/* added by Pushpalatha for TENJINCG-873 starts */
	/* modified by Roshni for TENJINCG-1168 starts */
	public ArrayList<Api> hydrateAllApi(Connection conn, String appId, String groupName) throws DatabaseException {
		/* modified by Roshni for TENJINCG-1168 ends */
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		ArrayList<Api> apiList = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {

			if(Utilities.trim(groupName).length() > 0 && !Utilities.trim(groupName).equalsIgnoreCase("all") && !Utilities.trim(groupName).equalsIgnoreCase("null")) {
				pst = conn.prepareStatement(" SELECT * FROM TJN_AUT_APIS WHERE APP_ID=? AND GROUP_NAME=? ORDER BY API_URL ASC"); 
				pst.setString(1, appId);
				pst.setString(2, Utilities.trim(groupName) );
			}else {
				pst = conn.prepareStatement(" SELECT * FROM TJN_AUT_APIS WHERE APP_ID=? ORDER BY API_URL ASC");
				pst.setString(1, appId);
			}
			/*Modified by Ashiki for VAPT Fix ends*/ 
			rs = pst.executeQuery();
			apiList = new ArrayList<Api>();
			while (rs.next()) {
				Api api = new Api();
				api.setApplicationId(rs.getInt("APP_ID"));
				api.setCode(rs.getString("API_CODE"));
				api.setName(rs.getString("API_NAME"));
				api.setType(rs.getString("API_TYPE"));
				api.setGroup(rs.getString("GROUP_NAME"));
				api.setUrl(rs.getString("API_URL"));
				api.setUrlValid(ApiUtilities.isUrlValid(api.getUrl()));
				apiList.add(api);
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not fetch API List", e);
		} finally {

			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return apiList;
	}

	// changes done by parveen, Leelaprasad for #405 starts
	public void persistAPIs(List<Api> apiList) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(
					"INSERT INTO TJN_AUT_APIS (APP_ID,API_CODE,API_NAME,API_TYPE,API_URL,GROUP_NAME) VALUES(?,?,?,?,?,?)");

			for (Api api : apiList) {
				Api exmApi = this.hydrateApi(api.getApplicationId(), api);
				if (exmApi == null) {

					pst.setInt(1, api.getApplicationId());
					pst.setString(2, api.getCode());
					pst.setString(3, api.getName());
					pst.setString(4, api.getType());
					pst.setString(5, api.getUrl());
					pst.setString(6, api.getGroup());
					pst.execute();

					this.persistListOfApiOperationsInDetail(api.getOperations(), api.getApplicationId());

				}
			}

		} catch (Exception e) {
			throw new DatabaseException("Could not create record", e);
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}

	}

	public void persistListOfApiOperationsInDetail(List<ApiOperation> operations, int appId) throws DatabaseException {
		if (operations == null) {
			logger.warn("Operation object is null. Cannot save!");
			return;
		}

		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_CORE);

		try {
			for (ApiOperation operation : operations) {
				logger.debug("Persisting operation [{}]", operation.getName());
				this.persistApiOperation(operation, operation.getApiCode(), appId);

				logger.debug("Persisting resource parameters for operation [{}]", operation.getName());
				this.persistParameters(conn, appId, operation.getApiCode(), operation.getName(), "RESOURCE",
						operation.getResourceParameters());

				logger.debug("Persisting Request Representation");

				this.persistReqRepresentation(conn, appId, operation.getApiCode(), operation.getName(), "REQUEST",
						operation.getListRequestRepresentation());

				logger.debug("Persisting Response Representation");
				this.persistRespRepresentation(conn, appId, operation.getApiCode(), operation.getName(), "RESPONSE",
						operation.getListResponseRepresentation());
			}
		} finally {
			DatabaseHelper.close(conn);
		}

	}
	// changes done by parveen, Leelaprasad for #405 ends

	// TENJINCG-515
	public void updateApiOperationResourceParameters(int appId, String apiCode, String operationCode,
			List<Parameter> resourceParameters) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		try {
			logger.debug("Clearing existing paramters");
			this.clearApiOperationParameters(conn, appId, apiCode, operationCode);

			logger.debug("Persisting new set of resource parameters");
			this.persistParameters(conn, appId, apiCode, operationCode, "RESOURCE", resourceParameters);
		} finally {
			DatabaseHelper.closeConnection(conn);
		}
	}

	public void updateApiRepresentations(int appId, String apiCode, String operationCode,
			List<Representation> requestRepresentations, List<Representation> responseRepresentations)
			throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);

		try {
			logger.debug("Clearing existing representations");
			this.clearApiOperationRepresentation(conn, appId, apiCode, operationCode);

			logger.debug("Persisting new set of resource parameters");
			this.persistReqRepresentation(conn, appId, apiCode, operationCode, "REQUEST", requestRepresentations);
			this.persistRespRepresentation(conn, appId, apiCode, operationCode, "RESPONSE", responseRepresentations);
		} finally {
			DatabaseHelper.closeConnection(conn);
		}
	}

	// TENJINCG-515 ends
	/* Changed by Leelaprasad for requirement TENJINCG-560 starts */
	private int validateRepresentation(int appId, String apiCode, String operationName, String entityType)
			throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement preparedStatement = null;
		int count = 0;
		ResultSet rs = null;
		try {
			preparedStatement = conn.prepareStatement(
					"Select * from TJN_AUT_API_OP_REPRESENTATIONS WHERE APP_ID=? AND API_CODE=? AND API_OPERATION=? AND REPRESENTATION_TYPE=?");
			preparedStatement.setInt(1, appId);
			preparedStatement.setString(2, apiCode);
			preparedStatement.setString(3, operationName);
			preparedStatement.setString(4, entityType);
			rs = preparedStatement.executeQuery();
			count = 0;
			if (rs.next()) {
				count++;

			}
		} catch (Exception de) {
			throw new DatabaseException("");
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(preparedStatement);
			DatabaseHelper.close(conn);

		}
		return count;

	}
	/* Changed by Leelaprasad for requirement TENJINCG-560 ends */

	/* Added by Pushpalatha for TENJINCG-603 starts */
	public boolean mapSingleApi(int appId, String groupName, String apiCode) throws DatabaseException {
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		PreparedStatement pst = null;
		boolean mapped = true;
		if (conn == null) {
			throw new DatabaseException("Invalid or no database connection");
		}

		try {

			pst = conn.prepareStatement("UPDATE TJN_AUT_APIs SET group_name = ? where app_id = ? and api_code = ?");
			pst.setString(1, groupName);
			pst.setInt(2, appId);
			pst.setString(3, apiCode);
			pst.executeUpdate();

		} catch (Exception e) {
			mapped = false;
			throw new DatabaseException("Could not update group due to an internal error", e);
		} finally {
			DatabaseHelper.close(pst);
			DatabaseHelper.close(conn);
		}
		return mapped;
	}
	/* Added by Pushpalatha for TENJINCG-603 ends */

	// Added by Avinash for OAuth 2.0 requirement -TENJINCG-1018 Starts
	public boolean checkForoAuthOperation(int testSetId, String authType) {
		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement(
						"SELECT 1 FROM TESTSETMAP TMAP, TESTCASES TC, TESTSTEPS TSTEP, TJN_AUT_API_OPERATIONS OP WHERE TMAP.TSM_TS_REC_ID = ? AND TMAP.TSM_TC_REC_ID = TC.TC_REC_ID AND TSTEP.TC_REC_ID = TC.TC_REC_ID AND TSTEP.APP_ID = OP.APP_ID AND OP.API_CODE = TSTEP.FUNC_CODE AND TSTEP.TSTEP_OPERATION = OP.API_OPERATION AND OP.API_AUTH_TYPE = ?");) {
			pst.setInt(1, testSetId);
			pst.setString(2, authType);
			try (ResultSet rs = pst.executeQuery();) {
				while (rs.next()) {
					return true;
				}
			}
		} catch (Exception e) {
			logger.debug("Error while checking for oauth type operations.");
		}
		return false;
	}

	/* Changed by leelaprasad for TJN210-100 starts */
	public void updateApiAutOperationsAuthTypes(int appId, String authType) throws DatabaseException {
		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement(
						"update tjn_aut_api_operations set API_AUTH_TYPE=? where app_id=?and API_AUTH_TYPE is not null");) {
			pst.setInt(1, appId);
			pst.setString(2, authType);
			try (ResultSet rs = pst.executeQuery();) {

			}
		} catch (Exception e) {
			logger.debug("Error while updating  for oauth type for operations.");
		}

	}
	/* Changed by leelaprasad for TJN210-100 ends */

	/* Change done by Shruthi for TENJINCG-1206 starts */
	public void deleteApiOperations(String[] selectedOperations, String appId, String apiCode)
			throws DatabaseException {
		for (String opName : selectedOperations) {
			
			try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP)){
				
				PreparedStatement pst=null;
					pst = conn.prepareStatement("DELETE FROM TJN_AUT_API_OPERATIONS WHERE APP_ID=? AND API_CODE=? AND API_OPERATION =?");
					pst.setString(1, appId);
					pst.setString(2, apiCode);
					pst.setString(3, opName);
					
				pst.executeUpdate();
				pst.close();
			}catch (SQLException e) {
				logger.error("ERROR removing Application Operation", e);
				throw new DatabaseException("Could not delete API operation due to an internal error.", e);
			}

			
			try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP)){
				
				PreparedStatement pst=null;
					pst = conn.prepareStatement("DELETE FROM DELETE FROM TJN_AUT_API_OP_PARAMETERS WHERE APP_ID=? AND API_CODE=? AND API_OPERATION IN=?");
					pst.setString(1, appId);
					pst.setString(2, apiCode);
					pst.setString(3, opName);
					
				pst.executeUpdate();
				pst.close();
			}catch (SQLException e) {
				logger.error("ERROR removing Application Operation", e);
				throw new DatabaseException("Could not delete API operation due to an internal error.", e);
			}
			
         try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP)){
				
				PreparedStatement pst=null;
					pst = conn.prepareStatement("DELETE FROM DELETE FROM TJN_AUT_API_OP_REPRESENTATIONS WHERE APP_ID=? AND API_CODE=? AND API_OPERATION IN=?");
					pst.setString(1, appId);
					pst.setString(2, apiCode);
					pst.setString(3, opName);
					pst.close();
				pst.executeUpdate();
			}catch (SQLException e) {
				logger.error("ERROR removing Application Operation", e);
				throw new DatabaseException("Could not delete API operation due to an internal error.", e);
			}
		}

	}

	/* Change done by Shruthi for TENJINCG-1206 ends */
	/* Added by Ashiki for TENJINCG-1213 starts */
	public ArrayList<ApiOperation> hydrateApis(String apicode, String appId) throws DatabaseException {
		ArrayList<ApiOperation> opr = new ArrayList<ApiOperation>();
		try (Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
				PreparedStatement pst = conn.prepareStatement(
						"Select A.*,B.APP_NAME,C.API_TYPE FROM TJN_AUT_API_OPERATIONS A,MASAPPLICATION B,TJN_AUT_APIS C WHERE A.APP_ID=B.APP_ID AND A.API_CODE=C.API_CODE AND A.API_CODE =? AND A.APP_ID=? ORDER BY API_OPERATION");) {
			pst.setString(1, apicode);
			pst.setString(2, appId);
			try (ResultSet rs = pst.executeQuery();) {
				while (rs.next()) {
					ApiOperation operation = new ApiOperation();
					operation.setApiType(rs.getString("API_TYPE"));
					operation.setApiCode(rs.getString("API_CODE"));
					operation.setAuthenticationType(rs.getString("API_AUTH_TYPE"));
					operation.setName(rs.getString("API_OPERATION"));
					operation.setMethod(rs.getString("API_METHOD_NAME"));
					operation.setAppName(rs.getString("APP_NAME"));
					opr.add(operation);
				}

			}
		} catch (SQLException e) {
			logger.error("ERROR removing Application User", e);
			throw new DatabaseException("Could not delete Application user due to an internal error.", e);
		}
		return opr;
	}

	public List<ApiOperation> hydrateApiOperation(Connection conn, int appId, String apiCode) throws DatabaseException {
		List<ApiOperation> operations = new ArrayList<ApiOperation>();
		ApiOperation operation = null;

		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			pst = conn.prepareStatement("SELECT * FROM TJN_AUT_API_OPERATIONS WHERE APP_ID=? AND API_CODE=?");
			pst.setInt(1, appId);
			pst.setString(2, apiCode);

			rs = pst.executeQuery();

			while (rs.next()) {
				operation = new ApiOperation();
				operation.setName(rs.getString("API_OPERATION"));
				operation.setMethod(rs.getString("API_METHOD_NAME"));
				if (operation != null) {
					logger.debug("Hydrating resource parameters for operation [{}]", operation.getName());
					operation.getResourceParameters()
							.addAll(this.hydrateParameters(conn, appId, apiCode, operation.getName(), "RESOURCE"));

					logger.debug("Hydrating Request Representation");
					operation.setListRequestRepresentation(
							this.hydrateRepresentation(conn, appId, apiCode, operation.getName(), "REQUEST"));

					logger.debug("Hydrating Response Representation");
					operation.setListResponseRepresentation(
							this.hydrateRepresentation(conn, appId, apiCode, operation.getName(), "RESPONSE"));
				}
				operations.add(operation);
			}

		} catch (SQLException e) {
			logger.error("ERROR while fetching details of operation [{}] for API [{}] in application [{}]",
					operation.getName(), apiCode, appId, e);
			throw new DatabaseException("Could not fetch details of operation " + operation.getName());
		} finally {
			DatabaseHelper.close(rs);
			DatabaseHelper.close(pst);
		}

		return operations;

	}
	/* Added by Ashiki for TENJINCG-1213 end */

}