/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutionStep.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 24-Sep-2016           Sriram Sridharan          Newly Added For
* 18-Nov-2016			Sriram Sridharan		Defect#789 
* 27-Jul-2017			Sriram Sridharan		TENJINCG-311
* 15-05-2018            Padmavathi              TENJINCG-647
* 15-06-2021			Ashiki					TENJINCG-1275
*/

package com.ycs.tenjin.bridge.pojo.run;

import java.util.ArrayList;

import com.ycs.tenjin.ValidationDetails;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.MessageValidate;

public class ExecutionStep {
	
	private int recordId;
	private String tdGid;
	private String operation;
	private int appId;
	private String appName;
	private String functionCode;
	private int transactionLimit;
	private String id;
	private String autLoginType;
	private String testCaseId;
	private String functionMenu;
	private int testCaseRecordId;
	/*Added by Ashiki for TENJINCG-1275 starts*/
	private String fileName;
	private String filePath;
	private String msgVCode;
	/*Added by Ashiki for TENJINCG-1275 ends*/
	/*Added by Prem for TENJINCG-1181 start*/
	private ArrayList<ValidationDetails> validationdetails;
	
	public ArrayList<ValidationDetails> getValidationdetails() {
		return validationdetails;
	}
	public void setValidationdetails(ArrayList<ValidationDetails> validationdetails) {
		this.validationdetails = validationdetails;
	}
	/*Added by Prem for TENJINCG-1181 ends*/
	/***
	 * Changed for TENJINCG-1175 (TENJINCG-1178) Avinash
	 */
	private String txnMode;
	
	public String getTxnMode() {
		return txnMode;
	}
	public void setTxnMode(String txnMode) {
		this.txnMode = txnMode;
	}
	/***
	 * Changed for TENJINCG-1175 (TENJINCG-1178) Avinash - Ends
	 */
	//changes done by parveen, Leelaprasad for #405 starts
	private int responseType;
	public int getResponseType() {
		return responseType;
	}
	public void setResponseType(int responseType) {
		this.responseType = responseType;
	}
	//changes done by parveen, Leelaprasad for #405 ends
	
	/*Added by Padmavathi for TENJINCG-647 starts*/
	private String dependencyRule;
	private int dependentStepId;
	
	
	public int getDependentStepId() {
		return dependentStepId;
	}
	public void setDependentStepId(int dependentStepId) {
		this.dependentStepId = dependentStepId;
	}
	public String getDependencyRule() {
		return dependencyRule;
	}
	public void setDependencyRule(String dependencyRule) {
		this.dependencyRule = dependencyRule;
	}
	/*Added by Padmavathi for TENJINCG-647 ends*/
	public int getTestCaseRecordId() {
		return testCaseRecordId;
	}
	public void setTestCaseRecordId(int testCaseRecordId) {
		this.testCaseRecordId = testCaseRecordId;
	}
	public String getFunctionMenu() {
		return functionMenu;
	}
	public void setFunctionMenu(String functionMenu) {
		this.functionMenu = functionMenu;
	}
	public String getTestCaseId() {
		return testCaseId;
	}
	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}
	public String getAutLoginType() {
		return autLoginType;
	}
	public void setAutLoginType(String autLoginType) {
		this.autLoginType = autLoginType;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getTransactionLimit() {
		return transactionLimit;
	}
	public void setTransactionLimit(int transactionLimit) {
		this.transactionLimit = transactionLimit;
	}
	public int getRecordId() {
		return recordId;
	}
	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}
	public String getTdGid() {
		return tdGid;
	}
	public void setTdGid(String tdGid) {
		this.tdGid = tdGid;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getFunctionCode() {
		return functionCode;
	}
	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}
	
	/******************************
	 * Added by Sriram for Def#789
	 */
	private String type;
	private String expectedResult;
	
	public String getExpectedResult() {
		return expectedResult;
	}
	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	/******************************
	 * Added by Sriram for Def#789 ends
	 */
	
	/********************************
	 * Added by Sriram for TENJINCG-168 (API Execution)
	 */
	private String executionMode;
	
	private String apiCode;
	
	private ApiOperation apiOperation;
	
	private Api api;
	
	private Aut aut;
	
	
	
	public Aut getAut() {
		return aut;
	}
	public void setAut(Aut aut) {
		this.aut = aut;
	}
	public Api getApi() {
		return api;
	}
	public void setApi(Api api) {
		this.api = api;
	}
	public ApiOperation getApiOperation() {
		return apiOperation;
	}
	public void setApiOperation(ApiOperation apiOperation) {
		this.apiOperation = apiOperation;
	}
	public String getApiCode() {
		return apiCode;
	}
	public void setApiCode(String apiCode) {
		this.apiCode = apiCode;
	}
	public String getExecutionMode() {
		return executionMode;
	}
	public void setExecutionMode(String executionMode) {
		this.executionMode = executionMode;
	}
	
	/********************************
	 * Added by Sriram for TENJINCG-168 (API Execution) ends
	 */
	/* For TENJINCG-311 */
	private String validationType;
	private String uiValidationString;
	
	public String getUiValidationString() {
		return uiValidationString;
	}
	public void setUiValidationString(String uiValidationString) {
		this.uiValidationString = uiValidationString;
	}
	public String getValidationType() {
		return validationType;
	}
	public void setValidationType(String validationType) {
		this.validationType = validationType;
	}
	/* For TENJINCG-311 ends*/
	/*Added by Ashiki for TENJINCG-1275 starts*/
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getMsgVCode() {
		return msgVCode;
	}
	public void setMsgVCode(String msgVCode) {
		this.msgVCode = msgVCode;
	}
	public MessageValidate getMsgV() {
		return msgV;
	}
	public void setMsgV(MessageValidate msgV) {
		this.msgV = msgV;
	}
	private MessageValidate msgV;
	/*Added by Ashiki for TENJINCG-1275 ends*/
}
