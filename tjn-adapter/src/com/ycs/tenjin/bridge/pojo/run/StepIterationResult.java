/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  StepIterationResult.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 02-02-2018			Preeti					TENJINCG-101
* 22-10-2018			Sriram Sridharan		TENJINCG-870
* 22-03-2019			Preeti					TENJINCG-1018
* 18-11-2020			Pushpa					TENJINCG-1228
*/

package com.ycs.tenjin.bridge.pojo.run;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ycs.tenjin.bridge.constants.TenjinConstants;

public class StepIterationResult {
	private String dataId;
	private String stepName;
	private int tsRecId;
	private int runId;
	private String result;
	private String message;
	private int iterationNo;
	private Blob screenshot;
	private String snapshotPath;
	private byte screenshotByteArray[];
	private ArrayList<ResultValidation> validations;
	private String wsReqMessage;
	private String wsResMessage;
	/*Added by Pushpalatha for TENJINCG-1228 starts*/
	private String apiRequestLog;
	
	public String getApiRequestLog() {
		return apiRequestLog;
	}
	public void setApiRequestLog(String apiRequestLog) {
		this.apiRequestLog = apiRequestLog;
	}
	/*Added by Pushpalatha for TENJINCG-1228 ends*/
	
	/*Added by Preeti for TENJINCG-101 starts*/
	private java.sql.Timestamp startTimeStamp;
	private java.sql.Timestamp endTimeStamp;
	private String elapsedTime;
	//Added for OAuth 2.0 requirement TENJINCG-1018- Avinash - Starts
	private String apiAuthType;
	public String getApiAuthType() {
		return apiAuthType;
	}
	public void setApiAuthType(String apiAuthType) {
		this.apiAuthType = apiAuthType;
	}
	public String getApiAccessToken() {
		return apiAccessToken;
	}
	public void setApiAccessToken(String apiAccessToken) {
		this.apiAccessToken = apiAccessToken;
	}
	private String apiAccessToken;
	//Added for OAuth 2.0 requirement TENJINCG-1018- Avinash - Ends
	
	public String getElapsedTime() {
		return elapsedTime;
	}
	public void setElapsedTime(String elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
	public java.sql.Timestamp getStartTimeStamp() {
		return startTimeStamp;
	}
	public void setStartTimeStamp(java.sql.Timestamp startTimeStamp) {
		this.startTimeStamp = startTimeStamp;
	}
	
	public java.sql.Timestamp getEndTimeStamp() {
		return endTimeStamp;
	}
	public void setEndTimeStamp(java.sql.Timestamp endTimeStamp) {
		this.endTimeStamp = endTimeStamp;
	}
	/*Added by Preeti for TENJINCG-101 ends*/
	/*****************************************************
	 * Added by Sriram for TJN_24_05
	 */
	private List<ValidationResult> validationResults;
	private boolean videoAvailable;
	
	public boolean isVideoAvailable() {
		return videoAvailable;
	}
	public void setVideoAvailable(boolean videoAvailable) {
		this.videoAvailable = videoAvailable;
	}
	public List<ValidationResult> getValidationResults() {
		return validationResults;
	}
	public void setValidationResults(List<ValidationResult> validationResults) {
		this.validationResults = validationResults;
		
		if(this.validationResults != null && this.validationResults.size() > 0) {
			int f=0;
			int e=0;
			
			for(ValidationResult result : this.validationResults) {
				if("fail".equalsIgnoreCase(result.getStatus())){
					f++;
				}else if("error".equalsIgnoreCase(result.getStatus())){
					e++;
				}
			}
			
			if(e > 0) {
				this.validationStatus = TenjinConstants.ACTION_RESULT_ERROR;
			}else if(f > 0) {
				this.validationStatus = TenjinConstants.ACTION_RESULT_FAILURE;
			}else{
				this.validationStatus = TenjinConstants.ACTION_RESULT_SUCCESS;
			}
		}else{
			this.validationStatus = "NA";
		}
		
	}
	private ArrayList<RuntimeScreenshot> runtimeSnapshots;
	private ArrayList<RuntimeFieldValue> runtimeFieldValues;
	
	
	
	public ArrayList<RuntimeScreenshot> getRuntimeSnapshots() {
		return runtimeSnapshots;
	}
	public void setRuntimeSnapshots(
			ArrayList<RuntimeScreenshot> runtimeSnapshots) {
		this.runtimeSnapshots = runtimeSnapshots;
	}
	
	public ArrayList<RuntimeFieldValue> getRuntimeFieldValues() {
		return runtimeFieldValues;
	}
	public void setRuntimeFieldValues(
			ArrayList<RuntimeFieldValue> runtimeFieldValues) {
		this.runtimeFieldValues = runtimeFieldValues;
	}
	private String tduid;
	
	
	public String getTduid() {
		return tduid;
	}
	/**
	 * @param tduid the tduid to set
	 */
	public void setTduid(String tduid) {
		this.tduid = tduid;
	}
	
	/**************************************************************
	 * Added by Sriram for Execution Screen overhaul (Tenjin v2.2) - 22-09-2015 ends
	 */
	public String getWsReqMessage() {
		return wsReqMessage;
	}
	public void setWsReqMessage(String wsReqMessage) {
		this.wsReqMessage = wsReqMessage;
	}
	public String getWsResMessage() {
		return wsResMessage;
	}
	public void setWsResMessage(String wsResMessage) {
		this.wsResMessage = wsResMessage;
	}
	
	@Deprecated //Depreceated by Sriram for TJN_24_05
	public ArrayList<ResultValidation> getValidations() {
		return validations;
	}
	@Deprecated //Depreceated by Sriram for TJN_24_05
	public void setValidations(ArrayList<ResultValidation> validations) {
		this.validations = validations;
	}
	public byte[] getScreenshotByteArray() {
		return screenshotByteArray;
	}
	public void setScreenshotByteArray(byte[] screenshotByteArray) {
		this.screenshotByteArray = screenshotByteArray;
	}
	public String getSnapshotPath() {
		return snapshotPath;
	}
	public void setSnapshotPath(String snapshotPath) {
		this.snapshotPath = snapshotPath;
	}
	public Blob getScreenshot() {
		return screenshot;
	}
	public void setScreenshot(Blob screenshot) {
		this.screenshot = screenshot;
	}
	public int getTsRecId() {
		return tsRecId;
	}
	public void setTsRecId(int tsRecId) {
		this.tsRecId = tsRecId;
	}
	public int getIterationNo() {
		return iterationNo;
	}
	public void setIterationNo(int iterationNo) {
		this.iterationNo = iterationNo;
	}
	
	public String getDataId() {
		return dataId;
	}
	public void setDataId(String dataId) {
		this.dataId = dataId;
	}
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public int getRunId() {
		return runId;
	}
	public void setRunId(int runId) {
		this.runId = runId;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	
	/*************
	 * Added by Sriram for TENJINCG-168 (API Execution)
	 */
	private String validationStatus;

	public String getValidationStatus() {
		return validationStatus;
	}
	public void setValidationStatus(String validationStatus) {
		this.validationStatus = validationStatus;
	}
	
	private int totalResultValidations;
	private int totalResultValidationsPassed;
	private int totalResultValidationsFailed;
	private int totalResultValidationsErrored;
	private int totalRuntimeValues;
	private int totalScreenshots;
	private Date executionStartTime;
	private Date executionEndTime;

	
	public Date getExecutionStartTime() {
		return executionStartTime;
	}
	public void setExecutionStartTime(Date executionStartTime) {
		this.executionStartTime = executionStartTime;
	}
	public Date getExecutionEndTime() {
		return executionEndTime;
	}
	public void setExecutionEndTime(Date executionEndTime) {
		this.executionEndTime = executionEndTime;
	}
	public int getTotalResultValidations() {
		return totalResultValidations;
	}
	public void setTotalResultValidations(int totalResultValidations) {
		this.totalResultValidations = totalResultValidations;
	}
	public int getTotalResultValidationsPassed() {
		return totalResultValidationsPassed;
	}
	public void setTotalResultValidationsPassed(int totalResultValidationsPassed) {
		this.totalResultValidationsPassed = totalResultValidationsPassed;
	}
	public int getTotalResultValidationsFailed() {
		return totalResultValidationsFailed;
	}
	public void setTotalResultValidationsFailed(int totalResultValidationsFailed) {
		this.totalResultValidationsFailed = totalResultValidationsFailed;
	}
	public int getTotalResultValidationsErrored() {
		return totalResultValidationsErrored;
	}
	public void setTotalResultValidationsErrored(int totalResultValidationsErrored) {
		this.totalResultValidationsErrored = totalResultValidationsErrored;
	}
	public int getTotalRuntimeValues() {
		return totalRuntimeValues;
	}
	public void setTotalRuntimeValues(int totalRuntimeValues) {
		this.totalRuntimeValues = totalRuntimeValues;
	}
	public int getTotalScreenshots() {
		return totalScreenshots;
	}
	public void setTotalScreenshots(int totalScreenshots) {
		this.totalScreenshots = totalScreenshots;
	}
	/**********************
	 * Added for TENJINCG-870 - Sriram ends
	 */
	
	
}
