/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  RuntimeFieldValue.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 
*/

package com.ycs.tenjin.bridge.pojo.run;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RuntimeFieldValue {
	private int runId;
	private String tdGid;
	private String tdUid;
	private int iteration;
	private int detailRecordNo;
	private String field;
	private String value;
	private int testStepRecordId;
	// added by shivam for extraction starts
	String locationName;
	// added by shivam for extraction ends

	public int getTestStepRecordId() {
		return testStepRecordId;
	}
	public void setTestStepRecordId(int testStepRecordId) {
		this.testStepRecordId = testStepRecordId;
	}
	public int getRunId() {
		return runId;
	}
	public void setRunId(int runId) {
		this.runId = runId;
	}
	public String getTdGid() {
		return tdGid;
	}
	public void setTdGid(String tdGid) {
		this.tdGid = tdGid;
	}
	public String getTdUid() {
		return tdUid;
	}
	public void setTdUid(String tdUid) {
		this.tdUid = tdUid;
	}
	public int getIteration() {
		return iteration;
	}
	public void setIteration(int iteration) {
		this.iteration = iteration;
	}
	@XmlElement
	public int getDetailRecordNo() {
		return detailRecordNo;
	}
	public void setDetailRecordNo(int detailRecordNo) {
		this.detailRecordNo = detailRecordNo;
	}
	@XmlElement
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	@XmlElement
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	// added by shivam for extraction starts
	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	// added by shivam for extraction ends

}
