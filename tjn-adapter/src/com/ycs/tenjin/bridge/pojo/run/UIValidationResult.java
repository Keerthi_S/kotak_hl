/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UIValidationResult.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 26-Jul-2017		Sriram Sridahran		Newly added for TENJINCG-312
*/

package com.ycs.tenjin.bridge.pojo.run;

public class UIValidationResult {
	private int runId;
	private int testStepRecordId;
	private int uiValRecId;
	private int iteration;
	private String page;
	private String field;
	private String expectedValue;
	private String actualValue;
	private String status;
	private String detailRecordNo;
	public int getRunId() {
		return runId;
	}
	public void setRunId(int runId) {
		this.runId = runId;
	}
	public int getTestStepRecordId() {
		return testStepRecordId;
	}
	public void setTestStepRecordId(int testStepRecordId) {
		this.testStepRecordId = testStepRecordId;
	}
	public int getUiValRecId() {
		return uiValRecId;
	}
	public void setUiValRecId(int uiValRecId) {
		this.uiValRecId = uiValRecId;
	}
	public int getIteration() {
		return iteration;
	}
	public void setIteration(int iteration) {
		this.iteration = iteration;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getExpectedValue() {
		return expectedValue;
	}
	public void setExpectedValue(String expectedValue) {
		this.expectedValue = expectedValue;
	}
	public String getActualValue() {
		return actualValue;
	}
	public void setActualValue(String actualValue) {
		this.actualValue = actualValue;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDetailRecordNo() {
		return detailRecordNo;
	}
	public void setDetailRecordNo(String detailRecordNo) {
		this.detailRecordNo = detailRecordNo;
	}
	@Override
	public String toString() {
		return "UIValidationResult [uiValRecId=" + uiValRecId + ", page=" + page + ", field=" + field
				+ ", expectedValue=" + expectedValue + ", actualValue=" + actualValue + ", status=" + status
				+ ", detailRecordNo=" + detailRecordNo + "]";
	}
	
	
}
