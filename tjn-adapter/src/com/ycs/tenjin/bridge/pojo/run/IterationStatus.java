/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  IterationStatus.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* Sep 1, 2016           Sriram Sridharan          Newly Added For 
* 27-Jul-2017			Sriram Sridharan		TENJINCG-311
* 17-Nov-2020			Pushpalatha				TENJINCG-1228
*/

package com.ycs.tenjin.bridge.pojo.run;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class IterationStatus {
	private List<RuntimeScreenshot> runtimeScreenshots;
	private List<RuntimeFieldValue> runtimeFieldValues = new ArrayList<RuntimeFieldValue>();
	private List<ValidationResult> validationResults;
	private String result;
	private String message;
	/*Added by Pushpa for TENJINCG-1228 starts*/
	private String reqLog;
	public String getReqLog() {
		return reqLog;
	}
	public void setReqLog(String reqLog) {
		this.reqLog = reqLog;
	}
	/*Added by Pushpa for TENJINCG-1228 ends*/
	@XmlElement
	public List<RuntimeScreenshot> getRuntimeScreenshots() {
		return runtimeScreenshots;
	}
	public void setRuntimeScreenshots(List<RuntimeScreenshot> runtimeScreenshots) {
		this.runtimeScreenshots = runtimeScreenshots;
	}
	
	@XmlElement
	public List<RuntimeFieldValue> getRuntimeFieldValues() {
		return runtimeFieldValues;
	}
	public void setRuntimeFieldValues(List<RuntimeFieldValue> runtimeFieldValues) {
		this.runtimeFieldValues = runtimeFieldValues;
	}
	
	@XmlElement
	public List<ValidationResult> getValidationResults() {
		return validationResults;
	}
	public void setValidationResults(List<ValidationResult> validationResults) {
		this.validationResults = validationResults;
	}
	@XmlElement
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	@XmlElement
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	/*********************
	 * Added by Sriram for TJN_243_02
	 */
	private int runId;
	private String tdGid;
	private String tdUid;
	private int testStepRecordId;
	
	@XmlElement
	public int getTestStepRecordId() {
		return testStepRecordId;
	}
	public void setTestStepRecordId(int testStepRecordId) {
		this.testStepRecordId = testStepRecordId;
	}
	@XmlElement
	public int getRunId() {
		return runId;
	}
	public void setRunId(int runId) {
		this.runId = runId;
	}
	@XmlElement
	public String getTdGid() {
		return tdGid;
	}
	public void setTdGid(String tdGid) {
		this.tdGid = tdGid;
	}
	@XmlElement
	public String getTdUid() {
		return tdUid;
	}
	public void setTdUid(String tdUid) {
		this.tdUid = tdUid;
	}
	private String apiRequestDescriptor;
	private String apiResponseDescriptor;
	public String getApiRequestDescriptor() {
		return apiRequestDescriptor;
	}
	public void setApiRequestDescriptor(String apiRequestDescriptor) {
		this.apiRequestDescriptor = apiRequestDescriptor;
	}
	public String getApiResponseDescriptor() {
		return apiResponseDescriptor;
	}
	public void setApiResponseDescriptor(String apiResponseDescriptor) {
		this.apiResponseDescriptor = apiResponseDescriptor;
	}
	
	
	/********************************
	 * Added by Sriram for TENJINCG-168 (API Execution) ends
	 */
	
	/* Added by Sriram for TENJINCG-311 */
	private List<UIValidationResult> uiValidationResults;
	public List<UIValidationResult> getUiValidationResults() {
		return uiValidationResults;
	}
	public void setUiValidationResults(List<UIValidationResult> uiValidationResults) {
		this.uiValidationResults = uiValidationResults;
	}
	/* Added by Sriram for TENJINCG-311 ends*/
	
	/********************************
	 * Added by Sameer for Message validation
	 */

	private boolean expectedMessageFound = false;
	private boolean validateAllMessagesWithExpectedMessage = false;
	
	public boolean getExpectedMessageFound() {
		return expectedMessageFound;
	}
	public void setExpectedMessageFound(boolean expectedMessageFound) {
		this.expectedMessageFound = expectedMessageFound;
	}
	public boolean getValidateAllMessagesWithExpectedMessage() {
		return validateAllMessagesWithExpectedMessage;
	}
	public void setValidateAllMessagesWithExpectedMessage(
			boolean validateAllMessagesWithExpectedMessage) {
		this.validateAllMessagesWithExpectedMessage = validateAllMessagesWithExpectedMessage;
	}
	
	

	/********************************
	 * Added by Sameer for Message validation
	 */
	
}
