package com.ycs.tenjin.bridge.pojo;

public class TransactionStatus {
	private String tdUid;
	private String status;
	private String message;
	private String resultValidationStatus;
	private boolean runtimeValuesPresent;
	
	
	
	
	public boolean isRuntimeValuesPresent() {
		return runtimeValuesPresent;
	}
	public void setRuntimeValuesPresent(boolean runtimeValuesPresent) {
		this.runtimeValuesPresent = runtimeValuesPresent;
	}
	public String getTdUid() {
		return tdUid;
	}
	public void setTdUid(String tdUid) {
		this.tdUid = tdUid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getResultValidationStatus() {
		return resultValidationStatus;
	}
	public void setResultValidationStatus(String resultValidationStatus) {
		this.resultValidationStatus = resultValidationStatus;
	}
	
	
	
}
