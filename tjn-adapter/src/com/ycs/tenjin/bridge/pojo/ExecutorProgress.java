/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExecutorProgress.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 27-03-2017			Sriram Sridharan	Newly added for TENJINCG-152
* 16-06-2017			Sriram Sridharan		TENJINCG-187
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 26-06-2018           Padmavathi              T251IT-53
*/

package com.ycs.tenjin.bridge.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.ycs.tenjin.util.Utilities;

public class ExecutorProgress {
	
	private int runId;
	
	private Map<String, Map<String, String>> transactionsMap = new HashMap<String, Map<String, String>>();
	
	private List<TransactionStatus> transactions;
	
	private int totalTransactions;
	
	private int completedTransactions;
	
	private int percentage;
	
	private String elapsedTime;
	/*Added by Padmavathi for T251IT-53 starts*/
	private String runAborteduserId;
	
	public String getRunAborteduserId() {
		return runAborteduserId;
	}

	public void setRunAborteduserId(String runAborteduserId) {
		this.runAborteduserId = runAborteduserId;
	}
	/*Added by Padmavathi for T251IT-53 ends*/
	private String status;
	/* Added by Sriram for TENJINCG-187 */
	private boolean runAborted;
	
	

	public boolean isRunAborted() {
		return runAborted;
	}

	public void setRunAborted(boolean runAborted) {
		this.runAborted = runAborted;
	}
	/* Added by Sriram for TENJINCG-187 ends*/

	public int getRunId() {
		return runId;
	}

	public void setRunId(int runId) {
		this.runId = runId;
	}

	public int getTotalTransactions() {
		return totalTransactions;
	}

	public void setTotalTransactions(int totalTransactions) {
		this.totalTransactions = totalTransactions;
	}

	public int getCompletedTransactions() {
		return completedTransactions;
	}

	public void setCompletedTransactions(int completedTransactions) {
		this.completedTransactions = completedTransactions;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public Map<String, Map<String, String>> getTransactionsMap() {
		return transactionsMap;
	}

	public void setTransactionsMap(Map<String, Map<String, String>> transactionsMap) {
		this.transactionsMap = transactionsMap;
	}

	public String getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(String elapsedTime) {
		this.elapsedTime = elapsedTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<TransactionStatus> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionStatus> transactions) {
		this.transactions = transactions;
	}
	
	
	public String toJson() {
		this.transactions = new ArrayList<TransactionStatus>();
		if(this.transactionsMap != null) {
			for(String key: transactionsMap.keySet()) {
				TransactionStatus status = new TransactionStatus();
				status.setTdUid(key);
				Map<String, String> statusMap = transactionsMap.get(key);
				status.setStatus(statusMap.get("status"));
				status.setMessage(statusMap.get("message"));
				if(Utilities.trim(statusMap.get("resultValidationStatus")).length() > 0) {
					status.setResultValidationStatus(statusMap.get("resultValidationStatus"));
				}else{
					status.setResultValidationStatus("N/A");
				}
				
				if(Utilities.trim(statusMap.get("runtimevalues")).equalsIgnoreCase("yes")){
					status.setRuntimeValuesPresent(true);
				}else{
					status.setRuntimeValuesPresent(false);
				}
				
				this.transactions.add(status);
			}
		}
		
		return new Gson().toJson(this);
	}

	public static void main(String[] args) {
		Map<String, String> map1 = new HashMap<String, String>();
		map1.put("status", "S");
		map1.put("message", "");
		
		Map<String, String> map2 = new HashMap<String, String>();
		map2.put("status", "F");
		map2.put("message", "Internal Error");
		
		ExecutorProgress ep = new ExecutorProgress();
		ep.setRunId(1);
		ep.setElapsedTime("00:10:01");
		ep.setCompletedTransactions(2);
		ep.setTotalTransactions(2);
		ep.setPercentage(100);
		ep.setStatus("Complete");
		
		ep.getTransactionsMap().put("txn1", map1);
		ep.getTransactionsMap().put("txn2", map2);
		System.err.println(ep.toJson());
	}
	
	
}
