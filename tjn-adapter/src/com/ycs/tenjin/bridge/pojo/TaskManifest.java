/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TaskManifest.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 22-Sep-2016          Sriram Sridharan        Newly Added For 
 * 08-09-2017		   Sameer Gupta			   Rewritten for New Adapter Spec
 * 18-05-2018          Padmavathi              TENJINCG-647
 * 19-11-2018		   Prem					   TENJINCG-843
 * 11-04-2019		   Ashiki				   TENJINCG-1032
 * 24-06-2019          Padmavathi              for license
 * 18-11-2020		   Ashiki				   TENJINCG-1213
 * 15-06-2021			Ashiki				   TENJINCG-1275



*/


package com.ycs.tenjin.bridge.pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.MessageValidate;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;

public class TaskManifest {
	/*Added by Ashiki for TENJINCG-1275 starts*/
	private String msgValidateLearnType;
	private MessageValidate msgValidate;
	/*Added by Ashiki for TENJINCG-1275 ends*/
	private String taskType;
	private int runId;
	private int projectId;
	private List<Module> functions;
	private Aut aut;
	private String browser;
	private String clientIp;
	private int clientPort;
	private String userId;
//	private String automationEngine;
	private long tastStartTimeMillis;
	private Map<String, Map<String, Aut>> autMap;
	private List<ExecutionStep> steps;
	private int screenshotOption;
	private int fieldTimeout;
	private Map<Integer, List<String>> tdUidMap;
	private Map<String, String> testDataPaths;
	/*Added by Ashiki for TENJINCG-1213 start*/
	private String apiType;
	/*Added by Ashiki for TENJINCG-1213 end*/
	//Parveen for TJN_24_11
	private String videoCaptureFlag;
	
	
	public String getVideoCaptureFlag() {
		return videoCaptureFlag;
	}
	public void setVideoCaptureFlag(String videoCaptureFlag) {
		this.videoCaptureFlag = videoCaptureFlag;
	}
	//Parveen for TJN_24_11 ends
	/*Added by Padmavathi for TENJINCG-647 starts*/
	private Map<Integer,String> passedStepsStatusMap;
	
	
	public Map<Integer, String> getPassedStepsStatusMap() {
	return passedStepsStatusMap;
	}
	public void setPassedStepsStatusMap(Map<Integer, String> passedStepsStatusMap) {
	this.passedStepsStatusMap = passedStepsStatusMap;
	}
	/*Added by Padmavathi for TENJINCG-647 ends*/
	public Map<String, Map<String, Aut>> getAutMap() {
		return autMap;
	}
	public void setAutMap(Map<String, Map<String, Aut>> autMap) {
		this.autMap = autMap;
	}
	public List<ExecutionStep> getSteps() {
		return steps;
	}
	public void setSteps(List<ExecutionStep> steps) {
		this.steps = steps;
	}
	public int getScreenshotOption() {
		return screenshotOption;
	}
	public void setScreenshotOption(int screenshotOption) {
		this.screenshotOption = screenshotOption;
	}
	public int getFieldTimeout() {
		return fieldTimeout;
	}
	public void setFieldTimeout(int fieldTimeout) {
		this.fieldTimeout = fieldTimeout;
	}
	public Map<Integer, List<String>> getTdUidMap() {
		return tdUidMap;
	}
	public void setTdUidMap(Map<Integer, List<String>> tdUidMap) {
		this.tdUidMap = tdUidMap;
	}
	public Map<String, String> getTestDataPaths() {
		return testDataPaths;
	}
	public void setTestDataPaths(Map<String, String> testDataPaths) {
		this.testDataPaths = testDataPaths;
	}
	public long getTastStartTimeMillis() {
		return tastStartTimeMillis;
	}
	public void setTastStartTimeMillis(long tastStartTimeMillis) {
		this.tastStartTimeMillis = tastStartTimeMillis;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public int getRunId() {
		return runId;
	}
	public void setRunId(int runId) {
		this.runId = runId;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public List<Module> getFunctions() {
		return functions;
	}
	public void setFunctions(List<Module> functions) {
		this.functions = functions;
	}
	public Aut getAut() {
		return aut;
	}
	public void setAut(Aut aut) {
		this.aut = aut;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	public int getClientPort() {
		return clientPort;
	}
	public void setClientPort(int clientPort) {
		this.clientPort = clientPort;
	}
	
	
	/*********************************
	 * Added by Sriram for TENJINCG-152 (API Testing Enablement)
	 */
	private List<Api> apis = new ArrayList<Api>();
	private String apiLearnType;
	private Api api;

/*Added by Ashiki for TENJINCG-1213 start*/
	public List<Api> getApis() {
		return apis;
	}
	public void setApis(List<Api> apis) {
		this.apis = apis;
	}
	/*Added by Ashiki for TENJINCG-1213 end*/
	
	
	public String getApiLearnType() {
		return apiLearnType;
	}
	public Api getApi() {
		return api;
	}
	public void setApi(Api api) {
		this.api = api;
	}
	public void setApiLearnType(String apiLearnType) {
		this.apiLearnType = apiLearnType;
	}
	/*********************************
	 * Added by Sriram for TENJINCG-152 (API Testing Enablement) ends
	 */
	 
	/*********************************
	 * TENJINCG-731 - By Sameer - Mobility - Start
	 */
	private String deviceId;
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	/*Added by Ashiki for TENJINCG-1032 starts*/
	private String regClientName;
	private String autLoginType;
	
	public String getAutLoginType() {
		return autLoginType;
	}
	public void setAutLoginType(String autLoginType) {
		this.autLoginType = autLoginType;
	}
	public String getRegClientName() {
		return regClientName;
	}
	public void setRegClientName(String regClientName) {
		this.regClientName = regClientName;
	}
	/*********************************
	 * TENJINCG-731 - By Sameer - Mobility - End
	 */
	/*Added by Ashiki for TENJINCG-1032 ends*/

/*Uncommented by Padmavathi for license starts*/
	public String getAdapterName() {
		return adapterName;
	}
	public void setAdapterName(String adapterName) {
		this.adapterName = adapterName;
	}
	
	/*Added by Ashiki for TENJINCG-1213 start*/
	public String getApiType() {
		return apiType;
	}
	public void setApiType(String apiType) {
		this.apiType = apiType;
	}
	/*Added by Ashiki for TENJINCG-1213 end*/
	
	/*Added by Ashiki for TENJINCG-1275 starts*/
	public String getMsgValidateLearnType() {
		return msgValidateLearnType;
	}
	public void setMsgValidateLearnType(String msgValidateLearnType) {
		this.msgValidateLearnType = msgValidateLearnType;
	}
	public MessageValidate getMsgValidate() {
		return msgValidate;
	}
	public void setMsgValidate(MessageValidate msgValidate) {
		this.msgValidate = msgValidate;
	}
	/*Added by Ashiki for TENJINCG-1275 ends*/
	/*Added By Prem for TENJINCG-843*/
	private String adapterName;
	/*Uncommented by Padmavathi for license ends*/
}
