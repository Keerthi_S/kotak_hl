/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  UIValidation.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 17-Jul-2017		Sriram Sridahran		Newly added for Epic TENJINCG-283 (TENJINCG-285, TENJINCG-309)
* 26-Jul-2017		Sriram Sridharan		For TENJINCG-311
*/

package com.ycs.tenjin.bridge.pojo.project;

import java.util.List;

import com.ycs.tenjin.bridge.pojo.run.UIValidationResult;


public class UIValidation {
	private int recordId;
	private int stepRecordId;
	private UIValidationType type;
	private String pageArea;
	private String preSteps;
	private String steps;
	private String validity;
	/* For TENJINCG-311 */
	private List<UIValidationResult> results;
	
	
	public List<UIValidationResult> getResults() {
		return results;
	}
	public void setResults(List<UIValidationResult> results) {
		this.results = results;
	}
	/* For TENJINCG-311 ends*/
	public String getValidity() {
		return validity;
	}
	public void setValidity(String validity) {
		this.validity = validity;
	}
	public int getRecordId() {
		return recordId;
	}
	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}
	public int getStepRecordId() {
		return stepRecordId;
	}
	public void setStepRecordId(int stepRecordId) {
		this.stepRecordId = stepRecordId;
	}
	public UIValidationType getType() {
		return type;
	}
	public void setType(UIValidationType type) {
		this.type = type;
	}
	public String getPageArea() {
		return pageArea;
	}
	public void setPageArea(String pageArea) {
		this.pageArea = pageArea;
	}
	public String getPreSteps() {
		return preSteps;
	}
	public void setPreSteps(String preSteps) {
		this.preSteps = preSteps;
	}
	public String getSteps() {
		return steps;
	}
	public void setSteps(String steps) {
		this.steps = steps;
	}
	
	
	
}
