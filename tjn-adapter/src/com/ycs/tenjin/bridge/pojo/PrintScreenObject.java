/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  PrintScreenObject.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.R

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 26-09-2018           Leelaprasad             newly added for TENJINCG-739
 */

package com.ycs.tenjin.bridge.pojo;

import java.io.File;

public class PrintScreenObject {
	private String clientIP;
	private int runId;
	private File screenShot;
	private boolean screenshotInProgress = false; 
	private int numberOfFailedTries;

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public int getRunId() {
		return runId;
	}

	public void setRunId(int runId) {
		this.runId = runId;
	}

	public File getScreenshot() {
		return screenShot;
	}

	public void setScreenshot(File screenShot) {
		this.screenShot = screenShot;
	}

	public boolean isScreenshotInProgress() {
		return screenshotInProgress;
	}

	public void setScreenshotInProgress(boolean screenshotInProgress) {
		this.screenshotInProgress = screenshotInProgress;
	}

	public int getNumberOfFailedTries() {
		return numberOfFailedTries;
	}

	public void setNumberOfFailedTries(int numberOfFailedTries) {
		this.numberOfFailedTries = numberOfFailedTries;
	}

	

}
