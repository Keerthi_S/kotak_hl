/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LearnerProgress.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 07-Apr-2017           Sriram Sridharan          Newly Added For
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
*/


package com.ycs.tenjin.bridge.pojo;

import java.util.ArrayList;
import java.util.List;

import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;

public class LearnerProgress {
	
	private int runId;
	
	private String currentOperation;
	
	private String currentFunction;
	
	private int percentage;
	
	private String status;
	
	private String elapsedTime;
	
	private int totalOperations;
	
	private int completedOperations;
	
	private List<ApiOperation> operations = new ArrayList<ApiOperation>();

	public List<ApiOperation> getOperations() {
		return operations;
	}

	public void setOperations(List<ApiOperation> operations) {
		this.operations = operations;
	}

	public int getTotalOperations() {
		return totalOperations;
	}

	public void setTotalOperations(int totalOperations) {
		this.totalOperations = totalOperations;
	}

	public int getCompletedOperations() {
		return completedOperations;
	}

	public void setCompletedOperations(int completedOperations) {
		this.completedOperations = completedOperations;
	}

	public int getRunId() {
		return runId;
	}

	public void setRunId(int runId) {
		this.runId = runId;
	}

	public String getCurrentOperation() {
		return currentOperation;
	}

	public void setCurrentOperation(String currentOperation) {
		this.currentOperation = currentOperation;
	}

	public String getCurrentFunction() {
		return currentFunction;
	}

	public void setCurrentFunction(String currentFunction) {
		this.currentFunction = currentFunction;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(String elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
	
	
	
}
