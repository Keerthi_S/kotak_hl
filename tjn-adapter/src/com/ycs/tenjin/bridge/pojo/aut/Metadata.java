/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Metadata.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* Sep 1, 2016           Sriram Sridharan          Newly Added For 
*/

package com.ycs.tenjin.bridge.pojo.aut;

import java.util.ArrayList;
import java.util.TreeMap;

import com.google.common.collect.Multimap;

public class Metadata {
	private ArrayList<TreeMap<String, TestObject>> testObjectMap;
	private Multimap<String, Location> pageAreas;
	public ArrayList<TreeMap<String, TestObject>> getTestObjectMap() {
		return testObjectMap;
	}
	public void setTestObjectMap(
			ArrayList<TreeMap<String, TestObject>> testObjectMap) {
		this.testObjectMap = testObjectMap;
	}
	public Multimap<String, Location> getPageAreas() {
		return pageAreas;
	}
	public void setPageAreas(Multimap<String, Location> pageAreas) {
		this.pageAreas = pageAreas;
	}
	
	
	/* Added by Sriram for TENJINCG-152 - API Testing Enablement
	 */
	
	private String descriptor;
	public String getDescriptor() {
		return descriptor;
	}
	public void setDescriptor(String descriptor) {
		this.descriptor = descriptor;
	}
	
	/* Added by Sriram for TENJINCG-152 - API Testing Enablement ends
	 */
	
	//changes done by parveen, Leelaprasad for #405 starts
	private Representation representation;
	private int responseCode;
	public Representation getRepresentation() {
		return representation;
	}
	public void setRepresentation(Representation representation) {
		this.representation = representation;
	}
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	
	//changes done by parveen, Leelaprasad for #405 ends
	
	
}
