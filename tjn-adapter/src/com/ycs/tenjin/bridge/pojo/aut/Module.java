/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Module.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 8-Nov-2016			Sriram					Added extractorInputFile property for Data Extraction in Tenjin v2.4.1
*  20-06-2017           Padmavathi              TENJINCG-195
*  24-Oct-2017			Sriram					TENJINCG-399
*  09-11-2017			Preeti					TENJINCG-413
*  18-06-2018           Padmavathi              T251IT-61
*/

package com.ycs.tenjin.bridge.pojo.aut;

import java.util.List;

import com.ycs.tenjin.bridge.annotations.TenjinMetadata;
import com.ycs.tenjin.spreadsheet.annotation.Downloadable;
import com.ycs.tenjin.spreadsheet.annotation.Uploadable;
import com.ycs.tenjin.util.Utilities;

public class Module {
	@TenjinMetadata(column_name="MODULE_CODE", node_name="code")
	@Downloadable(columnName="Function Code")
	@Uploadable(columnName="Function Code")
	private String code;
	@TenjinMetadata(column_name="MODULE_NAME", node_name="name")
	@Downloadable(columnName="Function Name")
	@Uploadable(columnName="Function Name")
	private String name;
	private int aut;
	@TenjinMetadata(column_name="MODULE_MENU_CONTAINER",  node_name="menu")
	@Downloadable(columnName="Menu")
	@Uploadable(columnName="Menu")
	private String menuContainer;
	private String learningResult;
	private String learnerMessage;
	private String wsurl;
	private String wsop;
	/* Added by Padmavathi for Req# Tenjincg-195 20-06-2017 starts*/
	private String dateFormat;
	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	/* Added by Padmavathi for Req# Tenjincg-195 20-06-2017 ends*/
	/***************
	 *  Newly Added For Data Extraction in Tenjin 2.4.1 - Sriram
	 */
	private String extractorInputFile;
	private List<ExtractionRecord> extractionRecords;
	private ExtractorResultBean lastSuccessfulExtractionStatus;
	
	
	public List<ExtractionRecord> getExtractionRecords() {
		return extractionRecords;
	}

	public void setExtractionRecords(List<ExtractionRecord> extractionRecords) {
		this.extractionRecords = extractionRecords;
	}

	public ExtractorResultBean getLastSuccessfulExtractionStatus() {
		return lastSuccessfulExtractionStatus;
	}

	public void setLastSuccessfulExtractionStatus(
			ExtractorResultBean lastSuccessfulExtractionStatus) {
		this.lastSuccessfulExtractionStatus = lastSuccessfulExtractionStatus;
	}

	public String getExtractorInputFile() {
		return extractorInputFile;
	}

	public void setExtractorInputFile(String extractorInputFile) {
		this.extractorInputFile = extractorInputFile;
	}
	/*********************
	 * Newly Added For Data Extraction in Tenjin 2.4.1 - Sriram ends
	 */
	
	/***************************************
	 * Added by Sriram for Req#TJN_23_18
	 */
	private String autUserType;
	
	public String getAutUserType() {
		return autUserType;
	}
	

	public void setAutUserType(String autUserType) {
		this.autUserType = autUserType;
	}
	/***************************************
	 * Added by Sriram for Req#TJN_23_18 ends
	 */
	/****************
	 * Added by Sriram to include pre learn information (Tenjin v2.2)
	 */
	private String preLearnString;
	
	public String getPreLearnString() {
		return preLearnString;
	}

	public void setPreLearnString(String preLearnString) {
		this.preLearnString = preLearnString;
	}
	/****************
	 * Added by Sriram to include pre learn information (Tenjin v2.2) ends
	 */
	public String getWsurl() {
		return wsurl;
	}

	public void setWsurl(String wsurl) {
		this.wsurl = wsurl;
	}

	public String getWsop() {
		return wsop;
	}

	public void setWsop(String wsop) {
		this.wsop = wsop;
	}

	/**
	 * @return the learningResult
	 */
	@Deprecated
	public String getLearningResult() {
		return learningResult;
	}

	/**
	 * @param learningResult
	 *            the learningResult to set
	 */
	@Deprecated
	public void setLearningResult(String learningResult) {
		this.learningResult = learningResult;
	}

	/**
	 * @return the learnerMessage
	 */
	@Deprecated
	public String getLearnerMessage() {
		return learnerMessage;
	}

	/**
	 * @param learnerMessage
	 *            the learnerMessage to set
	 */
	@Deprecated
	public void setLearnerMessage(String learnerMessage) {
		this.learnerMessage = learnerMessage;
	}

	/**
	 * @return the menuContainer
	 */
	public String getMenuContainer() {
		return menuContainer;
	}

	/**
	 * @param menuContainer
	 *            the menuContainer to set
	 */
	public void setMenuContainer(String menuContainer) {
		this.menuContainer = menuContainer;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the aut
	 */
	public int getAut() {
		return aut;
	}

	/**
	 * @param aut
	 *            the aut to set
	 */
	public void setAut(int aut) {
		this.aut = aut;
	}
	/*************************************************
	 * Added by Sriram for Req#TJN_24_05
	 */
	@TenjinMetadata(column_name="END_TIMESTAMP", node_name="last-learnt")
	private LearnerResultBean lastSuccessfulLearningResult;
	private List<LearnerResultBean> learnHistory;
	@TenjinMetadata(column_name="GROUP_NAME", node_name="func-group")
	@Downloadable(columnName="Group")
	@Uploadable(columnName="Group")
	private String group;
	
	
	
	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public LearnerResultBean getLastSuccessfulLearningResult() {
		return lastSuccessfulLearningResult;
	}

	public void setLastSuccessfulLearningResult(
			LearnerResultBean lastSuccessfulLearningResult) {
		this.lastSuccessfulLearningResult = lastSuccessfulLearningResult;
	}

	public List<LearnerResultBean> getLearnHistory() {
		return learnHistory;
	}

	public void setLearnHistory(List<LearnerResultBean> learnHistory) {
		this.learnHistory = learnHistory;
	}

	public ModuleBean convertToBean(){
		ModuleBean bean = new ModuleBean();
		bean.setModuleCode(this.code);
		bean.setModuleName(this.name);
		bean.setMenuContainer(this.menuContainer);
		return bean;
	}
	
	@Downloadable(columnName="Application")
	@Uploadable(columnName="Application")
	private String appName;
	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	@Override
	public String toString() {
		return "Module [code=" + code + ", name=" + name + ", aut=" + aut + ", dateFormat=" + dateFormat + ", group="
				+ group + ", appName=" + appName + "]";
	}
	
	int uploadRow;
	public int getUploadRow() {
		return uploadRow;
	}

	public void setUploadRow(int uploadRow) {
		this.uploadRow = uploadRow;
	}
	
	
	
	/*************************************************
	 * Added by Sriram for Req#TJN_24_05
	 */
	//TENJINCG-399
	/*Modified by Preeti for TENJINCG-413 starts*/
	public void merge(Module module) {
		this.name = Utilities.trim(module.getName()).length() >= 0 ? Utilities.trim(module.getName()) : this.name;
		this.menuContainer = Utilities.trim(module.getMenuContainer()).length() >= 0 ? Utilities.trim(module.getMenuContainer()) : this.menuContainer;
		this.group = Utilities.trim(module.getGroup()).length() >= 0 ? Utilities.trim(module.getGroup()) : this.group;
		this.dateFormat = module.getDateFormat()!=null ? Utilities.trim(module.getDateFormat()) : this.dateFormat;
		/*Modified by padmavathi for T251IT-61 ends*/
	}//TENJINCG-399 ends
	/*Modified by Preeti for TENJINCG-413 ends*/
}
