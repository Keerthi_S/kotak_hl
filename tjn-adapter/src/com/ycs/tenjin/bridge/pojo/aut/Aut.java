/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Aut.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 15-11-2016           Parveen                 Defect #665
 * 07-12-2016		   Manish				   TJN_24_06(DTT mapping to project and auts under that project)
 * 13-Jan-2016		   Sahana				   TENJINCG-24(Define and Default Test Data Path at Application Level)
 * 20-06-2017          Padmavathi              TENJINCG-195
 * 19-08-2017          Manish              		T25IT-169
 * 24-Oct-2017		   Sahana					new design change for TENJINCG-375
 * 03-11-2017          Padmavathi               for TENJINCG-400
 * 24-Nov-2017			Sahana					Updating appType to integer
 * 02-Feb-2018			Sriram					TENJINCG-415
 * 16-03-2018			Preeti					TENJINCG-73
 * 30-05-2018           Padmavathi              for updating apibase url
 * 14-06-2018			Preeti					T251IT-37
 * 26-Nov 2018			Shivam	Sharma			 TENJINCG-904
 * 15-03-2019			Ashiki				    TENJINCG-986
 * 22-03-2019			Preeti					TENJINCG-1018
 * 24-04-2019			Roshni					TENJINCG-1038
 */


package com.ycs.tenjin.bridge.pojo.aut;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

import com.ycs.tenjin.bridge.annotations.TenjinMetadata;
import com.ycs.tenjin.rest.SkipSerialization;
import com.ycs.tenjin.spreadsheet.annotation.Downloadable;
import com.ycs.tenjin.util.Utilities;

public class Aut{
	@TenjinMetadata(column_name="APP_ID", node_name="appid")
	@Downloadable(columnName="ID")
	private int id;
	@TenjinMetadata(column_name="APP_NAME", node_name="name")
	@Downloadable(columnName="Name")
	private String name;
	@TenjinMetadata(column_name="APP_URL", node_name="url")
	@Downloadable(columnName="URL")
	private String URL;
	@SkipSerialization
	private String loginReqd;
	/*removed skip serialization annotation for bug T25IT-169 on 19-Aug-2017*/
	private String loginName;
	@SkipSerialization
	private String password;
	private ArrayList<Module> modules;
	private ArrayList<ModuleBean> functions;
	private String userId;
	/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 starts*/
	@Downloadable(columnName="Default Date Format")
	private String dateFormat;
	public String getDateFormat() {
		return dateFormat;
	}
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	/* Changed by Padmavathi for Req# Tenjincg-195 20-06-2017 ends*/
	/*changed by sahana for improvement #TENJINCG-24:Starts*/
	private String testDataPath;
	public String getTestDataPath() {
		return testDataPath;
	}
	public void setTestDataPath(String testDataPath) {
		this.testDataPath = testDataPath;
	}
	/*changed by sahana for improvement #TENJINCG-24:ends*/


	@Downloadable(columnName="User Types")
	private String loginUserType;


	/*added by manish for mapping DTT instance and DTT project to project and aut on 06-Dec-2016 starts*/
	private String instance;
	private String dttProject;
	private String dttProjectKey;
	private String tool;
	private String dttEnableStatus;
	
	/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Starts*/
	private String grantType;
	private String callBackUrl;
	private String authUrl;
	private String accessTokenUrl;
	private String clientId;
	private String clientSecret;
	private String addAuthDataTo;
	//Code changes for OAuth 2.0 requirement - Starts
	private String accessToken;
	private Timestamp tokenGenerationTime;
	private String tokenValidity;
	private String refreshToken;
	private int uacId;
	
	public int getUacId() {
		return uacId;
	}
	public void setUacId(int uacId) {
		this.uacId = uacId;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public Timestamp getTokenGenerationTime() {
		return tokenGenerationTime;
	}
	public void setTokenGenerationTime(Date tokenGenerationTime) {
		if(tokenGenerationTime != null)
			this.tokenGenerationTime = new Timestamp(tokenGenerationTime.getTime());
	}
	public String getTokenValidity() {
		return tokenValidity;
	}
	public void setTokenValidity(String tokenValidity) {
		this.tokenValidity = tokenValidity;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	//Code changes for OAuth 2.0 requirement TENJINCG-1018- Ends
	
	public String getGrantType() {
		return grantType;
	}
	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}
	public String getCallBackUrl() {
		return callBackUrl;
	}
	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}
	public String getAuthUrl() {
		return authUrl;
	}
	public void setAuthUrl(String authUrl) {
		this.authUrl = authUrl;
	}
	public String getAccessTokenUrl() {
		return accessTokenUrl;
	}
	public void setAccessTokenUrl(String accessTokenUrl) {
		this.accessTokenUrl = accessTokenUrl;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getClientSecret() {
		return clientSecret;
	}
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	public String getAddAuthDataTo() {
		return addAuthDataTo;
	}
	public void setAddAuthDataTo(String addAuthDataTo) {
		this.addAuthDataTo = addAuthDataTo;
	}
	
	/* Added by Sunitha for OAuth 2.0 requirement Ends*/

	/*Added by Preeti for TENJINCG-73 starts*/
	private Integer pauseTime;
	private String pauseLocation;

	public Integer getPauseTime() {
		return pauseTime;
	}
	public void setPauseTime(Integer pauseTime) {
		this.pauseTime = pauseTime;
	}

	public String getPauseLocation() {
		return pauseLocation;
	}
	public void setPauseLocation(String pauseLocation) {
		this.pauseLocation = pauseLocation;
	}
	/*Added by Preeti for TENJINCG-73 ends*/

	public String getDttEnableStatus() {
		return dttEnableStatus;
	}
	public void setDttEnableStatus(String dttEnableStatus) {
		this.dttEnableStatus = dttEnableStatus;
	}
	public String getTool() {
		return tool;
	}
	public void setTool(String tool) {
		this.tool = tool;
	}
	public String getDttProjectKey() {
		return dttProjectKey;
	}
	public void setDttProjectKey(String dttProjectKey) {
		this.dttProjectKey = dttProjectKey;
	}
	public String getInstance() {
		return instance;
	}
	public void setInstance(String instance) {
		this.instance = instance;
	}
	public String getDttProject() {
		return dttProject;
	}
	public void setDttProject(String dttProject) {
		this.dttProject = dttProject;
	}

	/*added by manish for mapping DTT instance and DTT project to project and aut on 06-Dec-2016 ends*/

	/*Added by  Parveen for defect #665 starts*/
	@Downloadable(columnName="Operations")
	private String operation;
	/*Added by  Parveen for defect #665 ends*/


	/*Added by  Parveen for defect #665 starts*/
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	/*Added by  Parveen for defect #665 ends*/
	/**********************************
	 * Changed by Sriram for Req#TJN_24_05
	 */
	@SkipSerialization
	private int objectIdentificationTimeout;




	public int getObjectIdentificationTimeout() {
		return objectIdentificationTimeout;
	}
	public void setObjectIdentificationTimeout(int objectIdentificationTimeout) {
		this.objectIdentificationTimeout = objectIdentificationTimeout;
	}

	/**********************************
	 * Changed by Sriram for Req#TJN_24_05 ends
	 */

	/**********************************
	 * Added by Sriram for Req#TJN_23_17 18-Aug-2016
	 */
	@Downloadable(columnName="Adapter")
	private String adapterPackage;



	public String getAdapterPackage() {
		return adapterPackage;
	}
	public void setAdapterPackage(String adapterPackage) {
		this.adapterPackage = adapterPackage;
	}

	/***********************************
	 * Added by Sriram for Req#TJN_23_17 18-Aug-2016 ends
	 */


	/**********************************
	 * Added by sameer for new adpater spec
	 */
	@Downloadable(columnName="Adapter")
	private Integer adapterPackageOrTool;

	public Integer getAdapterPackageOrTool() {
		return adapterPackageOrTool;
	}
	public void setAdapterPackageOrTool(Integer adapterPackageOrTool) {
		this.adapterPackageOrTool = adapterPackageOrTool;
	} 

	/***********************************
	 * Added by sameer for new adpater spec
	 */

	// changed by sahana for TJN_23_13 requirement
	@SkipSerialization
	private String txnPassword;


	//added browser to aut on 22-7-2016

	public String getTxnPassword() {
		return txnPassword;
	}
	public void setTxnPassword(String txnPassword) {
		this.txnPassword = txnPassword;
	}

	// changed by sahana for TJN_23_09 requirement
	@TenjinMetadata(column_name = "APP_DEF_BROWSER", node_name = "browser")
	@Downloadable(columnName="Default Browser")
	private String browser;



	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}

	/*************************************************
	 * Change by Sriram to demonstrate SVN
	 */
	//System.out.println("Hello SVN!");
	/*************************************************
	 * Change by Sriram to demonstrate SVN ends
	 */

	/*Change By Babu: for Bug id: #1363,Sorting in AUT screen:starts*/
	public static Comparator<Aut> AutComp = new Comparator<Aut>() {
		public int compare(Aut o1,Aut o2) {
			Integer s1 = o1.getId();
			Integer s2 = o2.getId();
			//ascending order
			return s1.compareTo(s2);
			//return 0;
		}
	};
	/*Change By Babu: for Bug id: #1363,Sorting in AUT screen:Ends*/	
	public String getLoginUserType() {
		return loginUserType;
	}
	public void setLoginUserType(String loginUserType) {
		this.loginUserType = loginUserType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public ArrayList<Module> getModules() {
		return modules;
	}
	public void setModules(ArrayList<Module> modules) {
		this.modules = modules;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the uRL
	 */
	public String getURL() {
		return URL;
	}
	/**
	 * @param uRL the uRL to set
	 */
	public void setURL(String uRL) {
		URL = uRL;
	}
	/**
	 * @return the loginReqd
	 */
	public String getLoginReqd() {
		return loginReqd;
	}
	/**
	 * @param loginReqd the loginReqd to set
	 */
	public void setLoginReqd(String loginReqd) {
		this.loginReqd = loginReqd;
	}
	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}
	/**
	 * @param loginName the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}




	public ArrayList<ModuleBean> getFunctions() {
		return functions;
	}
	public void setFunctions(ArrayList<ModuleBean> functions) {
		this.functions = functions;
	}
	public boolean isSameAs(Aut aut){

		boolean res = true;
		Aut oAut = aut;

		if(oAut.getName() == null){
			oAut.setName("-1");
		}

		if(oAut.getURL() == null){
			oAut.setURL("-1");
		}

		if(oAut.getLoginName() == null){
			oAut.setLoginName("-1");
		}

		if(oAut.getPassword() == null){
			oAut.setPassword("-1");
		}

		if(oAut.getLoginReqd() == null){
			oAut.setLoginName("-1");
		}

		if(this.id != aut.getId()){
			return false;
		}
		if(this.name == null){
			if(!oAut.getName().equalsIgnoreCase("-1")){
				return false;
			}
		}

		else{
			if(!oAut.getName().equalsIgnoreCase(this.name)){
				return false;
			}
		}

		if(this.URL == null){
			if(!oAut.getURL().equalsIgnoreCase("-1")){
				return false;
			}
		}

		else{
			if(!oAut.getURL().equalsIgnoreCase(this.URL)){
				return false;
			}
		}

		if(this.loginName == null){
			if(!oAut.getLoginName().equalsIgnoreCase("-1")){
				return false;
			}
		}

		else{
			if(!oAut.getLoginName().equalsIgnoreCase(this.loginName)){
				return false;
			}
		}

		if(this.password == null){
			if(!oAut.getPassword().equalsIgnoreCase("-1")){
				return false;
			}
		}

		else{
			if(!oAut.getPassword().equalsIgnoreCase(this.password)){
				return false;
			}
		}

		if(this.loginReqd == null){
			if(!oAut.getLoginReqd().equalsIgnoreCase("-1")){
				return false;
			}
		}

		else{
			if(!oAut.getLoginReqd().equalsIgnoreCase(this.loginReqd)){
				return false;
			}
		}

		return res;

	}


	//Added API Base URL property
	@Downloadable(columnName="API Base URL")
	private String apiBaseUrl;
	public String getApiBaseUrl() {
		return apiBaseUrl;
	}
	public void setApiBaseUrl(String apiBaseUrl) {
		this.apiBaseUrl = apiBaseUrl;
	}

	/* Added by sahana for Req#TENJINCG-375 starts */
	private Integer applicationType;
	private String platform;
	private String platformVersion;
	private String applicationPackage;
	private String applicationActivity;

	public Integer getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(Integer type) {
		this.applicationType = type;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getPlatformVersion() {
		return platformVersion;
	}
	public void setPlatformVersion(String platformVersion) {
		this.platformVersion = platformVersion;
	}
	public String getApplicationPackage() {
		return applicationPackage;
	}
	public void setApplicationPackage(String applicationPackage) {
		this.applicationPackage = applicationPackage;
	}
	public String getApplicationActivity() {
		return applicationActivity;
	}
	public void setApplicationActivity(String applicationActivity) {
		this.applicationActivity = applicationActivity;
	}
	/* Added by sahana for Req#TENJINCG-375 ends */

	/*	Added by Padmavathi for TENJINCG-400 starts*/
	public void merge(Aut aut) {
		this.name = aut.getName()!=null ? Utilities.trim(aut.getName()) : this.name;
		this.URL = aut.getURL()!=null  ?Utilities.trim( aut.getURL()) : this.URL;
		this.adapterPackage =aut.getAdapterPackage()!=null ?Utilities.trim(aut.getAdapterPackage()) : this.adapterPackage;
		this.applicationPackage = aut.getApplicationPackage()!=null ? Utilities.trim(aut.getApplicationPackage()) : this.applicationPackage;
		this.applicationActivity = aut.getApplicationActivity()!=null ? Utilities.trim(aut.getApplicationActivity()) : this.applicationActivity;
		this.browser = aut.getBrowser()!=null ? Utilities.trim(aut.getBrowser()): this.browser;
		this.dateFormat = aut.getDateFormat()!=null ?Utilities.trim( aut.getDateFormat()) : this.dateFormat;
		this.platformVersion =aut.getPlatformVersion()!=null? Utilities.trim(aut.getPlatformVersion()) : this.platformVersion;
		this.loginUserType= aut.getLoginUserType()!=null ? Utilities.trim(aut.getLoginUserType()): this.loginUserType;
		this.operation= aut.getOperation()!=null ? Utilities.trim(aut.getOperation()) : this.operation;
		/*Added by Preeti for TENJINCG-73 starts*/
		this.pauseTime = aut.getPauseTime()!=null ? aut.getPauseTime() : this.pauseTime;
		this.pauseLocation = aut.getPauseLocation()!=null ? Utilities.trim(aut.getPauseLocation()) : this.pauseLocation;
		/*Added by Preeti for TENJINCG-73 ends*/

		/*Added by Padmavathi for updating apibaseurl starts*/
		this.apiBaseUrl=aut.getURL()!=null ?Utilities.trim( aut.getURL()) : this.URL;
		/*Added by Padmavathi for updating apibaseurl ends*/
		
		/*Added by Roshni for TENJINCG-1038 starts*/
		this.templatePwd=aut.getTemplatePwd()!=null ?Utilities.trim(aut.getTemplatePwd()):this.templatePwd;
		/*Added by Roshni for TENJINCG-1038 ends*/
		
	}
	/*	Added by Padmavathi for TENJINCG-400 ends*/

	/*Added by sahana for changing appType to integer:starts*/
	public String getApplicationTypeValue()
	{
		/*Modified by Preeti for T251IT-37 starts*/
		String appValue = "";
		if (this.applicationType == 1) {
			appValue = "Web Application";
		} else if (this.applicationType == 2) {
			appValue = "Windows";
		} else if (this.applicationType == 3) {
			appValue = "Mobile-Android";
		} else if (this.applicationType == 4) {
			appValue = "Mobile-iOS";
		}
		return appValue;
		/*Modified by Preeti for T251IT-37 ends*/
	}
	/*Added by sahana for changing appType to integer:ends*/

	//TENJINCG-415
	private String apiAuthenticationType;
	public String getApiAuthenticationType() {
		return apiAuthenticationType;
	}
	public void setApiAuthenticationType(String apiAuthenticationType) {
		this.apiAuthenticationType = apiAuthenticationType;
	}
	//added by shivam sharma for  TENJINCG-904 starts
	private String appFileName;
	public String getAppFileName() {
		return appFileName;
	}
	public void setAppFileName(String appFileName) {
		this.appFileName = appFileName;
	}
	//added by shivam sharma for  TENJINCG-904 ends
	
	
	/* Added By Ashiki TENJINCG-986 starts */
	private String subSet;
	public String getSubSet() {
		return subSet;
	}
	public void setSubSet(String subSet) {
		this.subSet = subSet;
	}

	/* Added By Ashiki TENJINCG-986 starts */
	//TENJINCG-415 ends
	
	/*Added by Roshni for TENJINCG-1038 starts*/
	private String templatePwd;
	public String getTemplatePwd() {
		return templatePwd;
	}
	public void setTemplatePwd(String templatePwd) {
		this.templatePwd = templatePwd;
	}
	private AutAuditRecord auditRecord;
	public AutAuditRecord getAutAuditRecord() {
		return auditRecord;
	}
	public void setAutAuditRecord(AutAuditRecord auditRecord) {
		this.auditRecord = auditRecord;
	}
	/*Added by Roshni for TENJINCG-1038 ends*/

}
