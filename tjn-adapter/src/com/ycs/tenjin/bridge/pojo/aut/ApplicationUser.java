/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApplicationUser.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 27-10-2018			Ashiki			Newly added for TENJINCG-846
* 22-03-2019			Preeti					TENJINCG-1018
* */

package com.ycs.tenjin.bridge.pojo.aut;
import java.sql.Timestamp;
import java.util.Date;

import com.ycs.tenjin.rest.SkipSerialization;
import com.ycs.tenjin.util.Utilities;

public class ApplicationUser {
	private int appId;
	private String appName;
	private String loginName;
	@SkipSerialization
	private String password;
	private String userId;
	private String loginUserType;
	@SkipSerialization
	private String txnPassword;
	private int uacId;
	
	/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Starts*/
	private String accessToken;
	private String refreshToken;
	private Timestamp tokenGenTime;
	private String tokenValidity;
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public Timestamp getTokenGenTime() {
		return tokenGenTime;
	}
	public void setTokenGenTime(Date tokenGenTime) {
		if(tokenGenTime != null)
		this.tokenGenTime = new Timestamp(tokenGenTime.getTime());
	}
	
	public String getTokenValidity() {
		return tokenValidity;
	}
	public void setTokenValidity(String tokenValidity) {
		this.tokenValidity = tokenValidity;
	}
	
	/* Added by Sunitha for OAuth 2.0 requirement TENJINCG-1018 Ends*/
	
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getLoginUserType() {
		return loginUserType;
	}
	public void setLoginUserType(String loginUserType) {
		this.loginUserType = loginUserType;
	}
	public String getTxnPassword() {
		return txnPassword;
	}
	public void setTxnPassword(String txnPassword) {
		this.txnPassword = txnPassword;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	public int getUacId() {
		return uacId;
	}
	public void setUacId(int uacId) {
		this.uacId = uacId;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}

public void merge(ApplicationUser applicationUser) {
		
	this.userId = applicationUser.getUserId()!= null ? Utilities.trim(applicationUser.getUserId()) : this.userId;
	this.appName = applicationUser.getAppName()!= null ? Utilities.trim(applicationUser.getAppName()) : this.appName;
	this.loginName = applicationUser.getLoginName() != null ? Utilities.trim(applicationUser.getLoginName()) : this.loginName;
	this.password = applicationUser.getPassword() !=null ? Utilities.trim(applicationUser.getPassword()) : this.password;
	this.txnPassword = applicationUser.getTxnPassword()!=null ? Utilities.trim(applicationUser.getTxnPassword()) : this.txnPassword;
	this.loginUserType = applicationUser.getLoginUserType() != null ? Utilities.trim(applicationUser.getLoginUserType()) : this.loginUserType; 
		
	}
}
