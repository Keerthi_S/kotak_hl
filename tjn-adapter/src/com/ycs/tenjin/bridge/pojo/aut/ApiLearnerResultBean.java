/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApiLearnerResultBean.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              	DESCRIPTION
* 05-Apr-2017           Sriram Sridharan          	Newly Added For
* 27-08-2019			Ashiki						TENJINCG-1105
*/


package com.ycs.tenjin.bridge.pojo.aut;

import java.sql.Timestamp;

public class ApiLearnerResultBean {
	private int id;
	private String user;
	private int appId;
	private String apiCode;
	private String operationName;
	private String requestDescriptor;
	private String responseDescriptor;
	private Timestamp startTimestamp;
	private Timestamp endTimestamp;
	private String elapsedTime;
	private String status;
	private String message;
	private int runId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	public String getApiCode() {
		return apiCode;
	}
	public void setApiCode(String apiCode) {
		this.apiCode = apiCode;
	}
	public String getOperationName() {
		return operationName;
	}
	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}
	public String getRequestDescriptor() {
		return requestDescriptor;
	}
	public void setRequestDescriptor(String requestDescriptor) {
		this.requestDescriptor = requestDescriptor;
	}
	public String getResponseDescriptor() {
		return responseDescriptor;
	}
	public void setResponseDescriptor(String responseDescriptor) {
		this.responseDescriptor = responseDescriptor;
	}
	public Timestamp getStartTimestamp() {
		return startTimestamp;
	}
	public void setStartTimestamp(Timestamp startTimestamp) {
		this.startTimestamp = startTimestamp;
	}
	public Timestamp getEndTimestamp() {
		return endTimestamp;
	}
	public void setEndTimestamp(Timestamp endTimestamp) {
		this.endTimestamp = endTimestamp;
	}
	public String getElapsedTime() {
		return elapsedTime;
	}
	public void setElapsedTime(String elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getRunId() {
		return runId;
	}
	public void setRunId(int runId) {
		this.runId = runId;
	}
	
	/*Added by Ashiki for TENJINCG-1105 starts*/
	private int lastLearntFieldCount;
	
	public int getLastLearntFieldCount() {
		return lastLearntFieldCount;
	}

	public void setLastLearntFieldCount(int lastLearntFieldCount) {
		this.lastLearntFieldCount = lastLearntFieldCount;
	}
	/*Added by Ashiki for TENJINCG-1105 ends*/
}
