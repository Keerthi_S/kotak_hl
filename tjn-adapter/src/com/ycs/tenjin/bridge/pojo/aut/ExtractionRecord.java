/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExtractionRecord.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 08-Nov-2016           Sriram Sridharan          Newly Added For Data Extraction in Tenjin 2.4.1
*/


package com.ycs.tenjin.bridge.pojo.aut;

import java.util.Map;

public class ExtractionRecord {
	private String tdUid;
	private String tdGid;
	private String result;
	private String message;
	private Map<String, String> queryFields;
	private String data;
	private int spreadsheetRowNum;
	
	
	
	public String getTdGid() {
		return tdGid;
	}
	public void setTdGid(String tdGid) {
		this.tdGid = tdGid;
	}
	public int getSpreadsheetRowNum() {
		return spreadsheetRowNum;
	}
	public void setSpreadsheetRowNum(int spreadsheetRowNum) {
		this.spreadsheetRowNum = spreadsheetRowNum;
	}
	public String getTdUid() {
		return tdUid;
	}
	public void setTdUid(String tdUid) {
		this.tdUid = tdUid;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Map<String, String> getQueryFields() {
		return queryFields;
	}
	public void setQueryFields(Map<String, String> queryFields) {
		this.queryFields = queryFields;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	
	
}
