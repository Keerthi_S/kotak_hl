/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LearnerResultBean.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 10-Sep-2016           Sriram Sridharan          Newly Added For 
* 05-Dec-2016           Leelaprasad               Req##TJN_243_09
* 27-03-2019			Sahana					  pCloudy
* 27-08-2019			Ashiki					  TENJINCG-1105
*/

package com.ycs.tenjin.bridge.pojo.aut;

import java.sql.Timestamp;
import java.util.List;

import com.ycs.tenjin.util.Utilities;

public class LearnerResultBean {
	
	private int id;
	private Timestamp startTimestamp;
	private Timestamp endTimestamp;
	private String elapsedTime;
	private String functionCode;
	private String functionName;
	private String learnStatus;
	private String message;
	private List<LearnerResultBean> functionResults;
	private String appName;
	private int appId;
	
	/*Changed by Leelaprasad for the Requirement TJN_243_09 on 05-12-2016 starts*/
	private String userID;
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	/*Changed by Leelaprasad for the Requirement TJN_243_09 on 05-12-2016 ends*/
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	public List<LearnerResultBean> getFunctionResults() {
		return functionResults;
	}
	public void setFunctionResults(List<LearnerResultBean> functionResults) {
		this.functionResults = functionResults;
	}
	public Timestamp getStartTimestamp() {
		return startTimestamp;
	}
	public void setStartTimestamp(Timestamp startTimestamp) {
		this.startTimestamp = startTimestamp;
	}
	public Timestamp getEndTimestamp() {
		return endTimestamp;
	}
	public void setEndTimestamp(Timestamp endTimestamp) {
		this.endTimestamp = endTimestamp;
	}
	public String getElapsedTime() {
		return elapsedTime;
	}
	public void setElapsedTime(String elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
	public String getFunctionCode() {
		return functionCode;
	}
	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getLearnStatus() {
		return learnStatus;
	}
	public void setLearnStatus(String learnStatus) {
		this.learnStatus = Utilities.trim(learnStatus);
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	/*Added by Sahana for pCloudy: Starts*/
	private String deviceFarmFlag;
	private String learnerIpAddr;

	public String getLearnerIpAddr() {
		return learnerIpAddr;
	}
	public void setLearnerIpAddr(String learnerIpAddr) {
		this.learnerIpAddr = learnerIpAddr;
	}
	public String getDeviceFarmFlag() {
		return deviceFarmFlag;
	}
	public void setDeviceFarmFlag(String deviceFarmFlag) {
		this.deviceFarmFlag = deviceFarmFlag;
	}
	/*Added by Sahana for pCloudy: Ends*/
	
	/*Added by Ashiki for TENJINCG-1105 starts*/
	private int lastLearntFieldCount;
	
	public int getLastLearntFieldCount() {
		return lastLearntFieldCount;
	}

	public void setLastLearntFieldCount(int lastLearntFieldCount) {
		this.lastLearntFieldCount = lastLearntFieldCount;
	}
	/*Added by Ashiki for TENJINCG-1105 ends*/
	
	@Override
	public String toString() {
		return "LearnerResultBean [startTimestamp=" + startTimestamp
				+ ", endTimestamp=" + endTimestamp + ", elapsedTime="
				+ elapsedTime + ", learnStatus=" + learnStatus + ", message="
				+ message + "]";
	}
	
	
	
	
}
