/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestObject.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 
 * 01-08-2016           Leelaprasad             TENJINCG-265
 * 29-10-2018           Padmavathi              for TENJINCG-889
 * 02-11-2018			Sriram					TENJINCG-894
*/
package com.ycs.tenjin.bridge.pojo.aut;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.annotations.TenjinMetadata;
import com.ycs.tenjin.spreadsheet.annotation.Downloadable;
import com.ycs.tenjin.spreadsheet.annotation.Uploadable;


public class TestObject {
	
	private static final Logger logger = LoggerFactory.getLogger(TestObject.class);
	@TenjinMetadata(column_name="FLD_UID", node_name="FLD_UID")	
	@Downloadable(columnName="Unique ID")
	@Uploadable(columnName="Unique ID")
	String uniqueId;
	@TenjinMetadata(column_name="FLD_UID_TYPE", node_name="FLD_UID_TYPE")
	@Downloadable(columnName="Identified By")
	@Uploadable(columnName="Identified By")
	String identifiedBy;
	@TenjinMetadata(column_name="FLD_LABEL", node_name="FLD_LABEL")
	@Downloadable(columnName="Label")
	@Uploadable(columnName="Label")
	String label;
	@TenjinMetadata(column_name="FLD_TYPE", node_name="FLD_TYPE")
	@Downloadable(columnName="Type")
	@Uploadable(columnName="Type")
	String objectClass;
	@TenjinMetadata(column_name="FLD_PAGE_AREA", node_name="FLD_PAGE_AREA")
	@Downloadable(columnName="Page Area")
	@Uploadable(columnName="Page Area")
	String location;
	String subLocation;
	String tableName;
	String attributesXML;
	@TenjinMetadata(column_name="VIEWMODE", node_name="VIEWMODE")
	@Downloadable(columnName="View Mode")
	@Uploadable(columnName="View Mode")
	String viewmode;
	@TenjinMetadata(column_name="MANDATORY", node_name="MANDATORY")
	@Downloadable(columnName="Mandatory")
	@Uploadable(columnName="Mandatory")
	String mandatory;
	@TenjinMetadata(column_name="FLD_DEFAULT_OPTIONS", node_name="FLD_DEFAULT_OPTIONS")
	@Downloadable(columnName="Default Options")
	@Uploadable(columnName="Default Options")
	String defaultOptions;
	@TenjinMetadata(column_name="FLD_IS_ENABLED", node_name="FLD_IS_ENABLED")
	@Downloadable(columnName="Enabled")
	@Uploadable(columnName="Enabled")
	String enabled;
	@TenjinMetadata(column_name="FLD_IS_FOOTER_ELEMENT", node_name="FLD_IS_FOOTER_ELEMENT")
	@Downloadable(columnName="Footer Element")
	@Uploadable(columnName="Footer Element")
	String isFooterElement;
	@TenjinMetadata(column_name="FLD_HAS_LOV", node_name="FLD_HAS_LOV")
	@Downloadable(columnName="Has LOV")
	@Uploadable(columnName="Has LOV")
	String lovAvailable;
	String data;
	@TenjinMetadata(column_name="FLD_IS_MULTI_REC", node_name="FLD_IS_MULTI_REC")
	@Downloadable(columnName="Multi Record")
	@Uploadable(columnName="Multi Record")
	String isMultiRecord;
	@TenjinMetadata(column_name="FLD_GROUP", node_name="FLD_GROUP")
	@Downloadable(columnName="Group")
	@Uploadable(columnName="Group")
	String group;
	
	//changes done by parveen, Leelaprasad for #405 starts
	@TenjinMetadata(column_name="FLD_API_RESP_TYPE", node_name="FLD_API_RESP_TYPE")
	@Downloadable(columnName="Response Type")
	@Uploadable(columnName="Response Type")
	int responseType;
	
	public int getResponseType() {
		return responseType;
	}

	public void setResponseType(int responseType) {
		this.responseType = responseType;
	}
	//changes done by parveen, Leelaprasad for #405 ends
	/*Changed by Leelaprasad for the requirement TENJINCG-265 starts*/
    int  fieldWaitTime=0;

	public int getFieldWaitTime() {
		return fieldWaitTime;
	}

	public void setFieldWaitTime(int fieldWaitTime) {
		this.fieldWaitTime = fieldWaitTime;
	}
	/*Changed by Leelaprasad for the requirement TENJINCG-265 ends*/
	/***************************************************************************************************************
	 * Added by Sharath for Auto Lov Starts
	 */
	@TenjinMetadata(column_name="FLD_HAS_AUTOLOV", node_name="FLD_HAS_AUTOLOV")
	@Downloadable(columnName="Has Auto LOV")
	@Uploadable(columnName="Has Auto LOV")
	String autoLovAvailable;
	
	public String getAutoLovAvailable() {
		return autoLovAvailable;
	}

	public void setAutoLovAvailable(String autoLovAvailable) {
		this.autoLovAvailable = autoLovAvailable;
	}

	/***************************************************************************************************************
	 * Added by Sharath for Auto Lov Ends
	 */
	
	
	public String getIsMultiRecord() {
		return isMultiRecord;
	}

	public void setIsMultiRecord(String isMultiRecord) {
		this.isMultiRecord = isMultiRecord;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	/***********************************************
	 * Added by Sriram for Requirement TJN_22_04 (Lesser Handling Through app specific package) Tenjin v2.2 (14-10-2015)
	 */
	String action;
	
	
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	/***********************************************
	 * Added by Sriram for Requirement TJN_22_04 (Lesser Handling Through app specific package) Tenjin v2.2 (14-10-2015) ends
	 */

	public String getLovAvailable() {
		return lovAvailable;
	}

	public void setLovAvailable(String lovAvailable) {
		this.lovAvailable = lovAvailable;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getIsFooterElement() {
		return isFooterElement;
	}

	public void setIsFooterElement(String isFooterElement) {
		this.isFooterElement = isFooterElement;
	}

	public String getDefaultOptions() {
		return defaultOptions;
	}

	public void setDefaultOptions(String defaultOptions) {
		this.defaultOptions = defaultOptions;
	}

	public String getMandatory() {
		return mandatory;
	}

	public void setMandatory(String mandatory) {
		this.mandatory = mandatory;
	}

	public String getViewmode() {
		return viewmode;
	}

	public void setViewmode(String viewmode) {
		this.viewmode = viewmode;
	}
	int id;
	
	/*****************************************
	 * Fix by Sriram to add Sequence Attribute (28-Nov-2014)
	 */
	@TenjinMetadata(column_name="FLD_SEQ_NO", node_name="FLD_SEQ_NO")
	private int sequence;
	
	
	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	/*****************************************
	 * Fix by Sriram to add Sequence Attribute (28-Nov-2014) ends
	 */
	/*****************************************
	 * Fix by Sriram to handle duplicate field names (27-Nov-2014)
	 */
	@TenjinMetadata(column_name="FLD_UNAME", node_name="FLD_UNAME")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	/*****************************************
	 * Fix by Sriram to handle duplicate field names (27-Nov-2014) ends
	 */
	
	/*****************************************
	 * Fix by Sriram to introduce persist status attribute (27-Nov-2014)
	 */
	private String persistStatus;
	
	public String getPersistStatus() {
		return persistStatus;
	}

	public void setPersistStatus(String persistStatus) {
		this.persistStatus = persistStatus;
	}

	/*****************************************
	 * Fix by Sriram to introduce persist status attribute (27-Nov-2014) ends
	 */
	
	/*************************************************
	 * Fix by Sriram to introduce taborder, instance and index attributes (3-Dec-2014)
	 */
	@TenjinMetadata(column_name="FLD_TAB_ORDER", node_name="FLD_TAB_ORDER")
	private int tabOrder;
	@TenjinMetadata(column_name="FLD_INDEX", node_name="FLD_INDEX")
	private int index;
	@TenjinMetadata(column_name="FLD_INSTANCE", node_name="FLD_INSTANCE")
	private int instance;
	

	public int getTabOrder() {
		return tabOrder;
	}

	public void setTabOrder(int tabOrder) {
		this.tabOrder = tabOrder;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getInstance() {
		return instance;
	}

	public void setInstance(int instance) {
		this.instance = instance;
	}
	/*************************************************
	 * Fix by Sriram to introduce taborder, instance and index attributes (3-Dec-2014) ends
	 */
	public String getLocation() {
		return location;
	}

	

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getObjectClass() {
		return objectClass;
	}

	public void setObjectClass(String objectClass) {
		this.objectClass = objectClass;
	}

	/**
	 * @return the attributesXML
	 */
	public String getAttributesXML() {
		return attributesXML;
	}

	/**
	 * @param attributesXML
	 *            the attributesXML to set
	 */
	public void setAttributesXML(String attributesXML) {
		this.attributesXML = attributesXML;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	public String getSubLocation() {
		return subLocation;
	}

	public void setSubLocation(String subLocation) {
		this.subLocation = subLocation;
	}

	/**
	 * @return the uniqueId
	 */
	public String getUniqueId() {
		return uniqueId;
	}

	/**
	 * @param uniqueId
	 *            the uniqueId to set
	 */
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	/**
	 * @return the identifiedBy
	 */
	public String getIdentifiedBy() {
		return identifiedBy;
	}

	/**
	 * @param identifiedBy
	 *            the identifiedBy to set
	 */
	public void setIdentifiedBy(String identifiedBy) {
		this.identifiedBy = identifiedBy;
	}
		
	public JSONObject toJson() {
		JSONObject field = new JSONObject();
		
		try{
			field.put("DATA",data);
			field.put("FLD_LABEL",label);
			field.put("FLD_TYPE",objectClass);
			field.put("FLD_UID",uniqueId);
			field.put("FLD_UID_TYPE",identifiedBy);
			field.put("ACTION",action);
			field.put("FLD_HAS_LOV",lovAvailable);
			field.put("FLD_IS_MULTI_REC",isMultiRecord);
			field.put("FLD_GROUP",group);
			field.put("FLD_HAS_AUTOLOV",autoLovAvailable);
			field.put("MANDATORY", mandatory);
			/*Changed by Leelaprasad for the requirement TENJINCG-265 starts*/
			field.put("FLD_WAIT_TIME", fieldWaitTime);
			/*Changed by Leelaprasad for the requirement TENJINCG-265 ends*/
			/*Added by Padmavathi for TENJINCG-889 starts*/
			field.put("FLD_NAME", name);
			/*Added by Padmavathi for TENJINCG-889 ends*/
		}catch(Exception e){
			logger.error("ERROR converting TestObject to JSONObject", e);
		}
		return field;
	}
	
	public static TestObject loadFromJson(JSONObject field){
		
		try {
			String dataVal = field.getString("DATA");
			String oLabel = field.getString("FLD_LABEL");
			String type = field.getString("FLD_TYPE");
			String uid = field.getString("FLD_UID");
			String uidType = field.getString("FLD_UID_TYPE");
			String oAction = field.getString("ACTION");
			String name = "";
			String hasLov = "",hasAutoLov="";
			/*Changed by Leelaprasad for the requirement TENJINCG-265 starts*/
			int fieldWaitTime=0;
			/*Changed by Leelaprasad for the requirement TENJINCG-265 ends*/
			try{
				hasLov = field.getString("FLD_HAS_LOV");
			}catch(Exception e){
				hasLov = "N";
			}
			
			/***************************************************************************************************************
			 * Added by Sharath for Auto Lov Starts
			 */
			
			try{
				hasAutoLov = field.getString("FLD_HAS_AUTOLOV");
			}catch(Exception e){
				hasAutoLov = "N";
			}

			/***************************************************************************************************************
			 * Added by Sharath for Auto Lov Ends
			 */
             /*Changed by Leelaprasad for the requirement TENJINCG-265 starts*/
			try{
				fieldWaitTime = Integer.parseInt(field.getString("FLD_WAIT_TIME"));
				
			}catch(Exception e){
				
			}
			/*Changed by Leelaprasad for the requirement TENJINCG-265 ends*/

			String isMultiRec = "";
			
			try{
				isMultiRec =  field.getString("FLD_IS_MULTI_REC");
			}catch(Exception ignore){}
			
			try{
				name = field.getString("FLD_UNAME");
			} catch(Exception ignore) {}
			
			String group = "";
			
			try{
				group = field.getString("FLD_GROUP");
			}catch(Exception ignore){}
			/*Changed by Leelaprasad for the requirement TENJINCG-265 starts*/
			int fWaittime=0;
			try{
				fWaittime=Integer.parseInt(field.getString("FLD_WAIT_TIME"));
			}catch(Exception igo){}
			/*Changed by Leelaprasad for the requirement TENJINCG-265 ends*/
			TestObject t = new TestObject();
			t.setLabel(oLabel);
			t.setObjectClass(type);
			t.setUniqueId(uid);
			t.setIdentifiedBy(uidType);
			t.setLovAvailable(hasLov);
			/***************************************************************************************************************
			 * Added by Sharath for Auto Lov Starts
			 */
			t.setAutoLovAvailable(hasAutoLov);
	
			/***************************************************************************************************************
			 * Added by Sharath for Auto Lov Ends
			 */
			 /*Changed by Leelaprasad for the requirement TENJINCG-265 starts*/
			t.setFieldWaitTime(fieldWaitTime);
			/*Changed by Leelaprasad for the requirement TENJINCG-265 ends*/
			
			t.setAction(oAction);
			t.setData(dataVal);
			t.setIsMultiRecord(isMultiRec);
			t.setGroup(group);
			t.setName(name);
			/*Changed by Leelaprasad for the requirement TENJINCG-265 starts*/
			t.setFieldWaitTime(fWaittime);
			/*Changed by Leelaprasad for the requirement TENJINCG-265 ends*/
			return t;
		} catch (JSONException e) {
			
			logger.error("Could not load TestObject from JSON", e);
			return null;
		}
	}
	/* Added for TENJINCG-314 */
	@Downloadable(columnName="Application")
	private String appName;
	@Downloadable(columnName="Function Code")
	private String moduleCode;
	private int appId;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}
	
	private int uploadRow;

	public int getUploadRow() {
		return uploadRow;
	}
	public void setUploadRow(int uploadRow) {
		this.uploadRow = uploadRow;
	}
	/* Added for TENJINCG-314 ends*/
	
	//TENJINCG-894 (Sriram)
	private String userRemarks;

	public String getUserRemarks() {
		return userRemarks;
	}

	public void setUserRemarks(String userRemarks) {
		this.userRemarks = userRemarks;
	}
	//TENJINCG-894 (Sriram) ends
	
	/*Added by paneendra for Tenj212-37 starts*/
	private String parentPath;

	public String getParentPath() {
		return parentPath;
	}

	public void setParentPath(String parentPath) {
		this.parentPath = parentPath;
	}
	/*Added by paneendra for Tenj212-37 ends*/
}
