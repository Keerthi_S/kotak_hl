/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Location.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright  2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 30-08-2018			Preeti					adding page label in iteration data
 * 22-09-2020			Ashiki					TENJINCG-1225
 */

package com.ycs.tenjin.bridge.pojo.aut;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.annotations.TenjinMetadata;
import com.ycs.tenjin.spreadsheet.annotation.Downloadable;
import com.ycs.tenjin.spreadsheet.annotation.Uploadable;


public class Location {
	@TenjinMetadata(column_name="PA_NAME", node_name="PA_NAME")
	@Downloadable(columnName="Page Name")
	@Uploadable(columnName="Page Name")
	private String locationName;
	@TenjinMetadata(column_name="PA_PARENT", node_name="PA_PARENT")
	@Downloadable(columnName="Parent")
	@Uploadable(columnName="Parent")
	private String parent;
	@TenjinMetadata(column_name="PA_WAY_IN", node_name="PA_WAY_IN")
	@Downloadable(columnName="Way In")
	@Uploadable(columnName="Way In")
	private String wayIn;
	@TenjinMetadata(column_name="PA_WAY_OUT", node_name="PA_WAY_OUT")
	@Downloadable(columnName="Way Out")
	@Uploadable(columnName="Way Out")
	private String wayOut;
	@TenjinMetadata(column_name="PA_TYPE", node_name="PA_TYPE")
	@Downloadable(columnName="Type")
	@Uploadable(columnName="Type")
	private String locationType;
	@TenjinMetadata(column_name="PA_SEQ_NO", node_name="PA_SEQ_NO")
	@Downloadable(columnName="Sequence")
	@Uploadable(columnName="Sequence")
	private int sequence;
	
	private boolean landingPage;
	private String path;
	private List<Location> childLocations;
	private List<TestObject> testObjects;
	private String templateInclusionRule;
	@TenjinMetadata(column_name="PA_IS_MRB", node_name="PA_IS_MRB")
	@Downloadable(columnName="Is Multi Record?")
	@Uploadable(columnName="Is Multi Record?")
	private boolean multiRecordBlock;
	private boolean pageNavOnly;
	
	//changes done by parveen, Leelaprasad for #405 starts
	@TenjinMetadata(column_name="PA_API_RESP_TYPE", node_name="PA_API_RESP_TYPE")
	@Downloadable(columnName="Response Type")
	@Uploadable(columnName="Response Type")
	int responseType;
	
	public int getResponseType() {
		return responseType;
	}
	public void setResponseType(int responseType) {
		this.responseType = responseType;
	}
	//changes done by parveen, Leelaprasad for #405 ends
	
	private static final Logger logger = LoggerFactory.getLogger(Location.class);
	
	public boolean isPageNavOnly() {
		return pageNavOnly;
	}
	public void setPageNavOnly(boolean pageNavOnly) {
		this.pageNavOnly = pageNavOnly;
	}
	public boolean isMultiRecordBlock() {
		return multiRecordBlock;
	}
	
	public boolean getMultiRecordBlock() {
		return multiRecordBlock;
	}
	public void setMultiRecordBlock(boolean multiRecordBlock) {
		this.multiRecordBlock = multiRecordBlock;
	}
	public String getTemplateInclusionRule() {
		return templateInclusionRule;
	}
	public void setTemplateInclusionRule(String templateInclusionRule) {
		this.templateInclusionRule = templateInclusionRule;
	}
	/**************************************************
	 * Change by Sriram to introduce screen title attribute (1-Dec-2014)
	 */
	@TenjinMetadata(column_name="PA_SCREEN_TITLE", node_name="PA_SCREEN_TITLE")
	@Downloadable(columnName="Screen Title")
	@Uploadable(columnName="Screen Title")
	private String screenTitle;
	
	
	public String getScreenTitle() {
		return screenTitle;
	}
	public void setScreenTitle(String screenTitle) {
		this.screenTitle = screenTitle;
	}
	/**************************************************
	 * Change by Sriram to introduce screen title attribute (1-Dec-2014) ends
	 */
	/****************************
	 * Change by Sriram to add persist status attribute
	 */
	private String persistStatus;
	
	
	public String getPersistStatus() {
		return persistStatus;
	}
	public void setPersistStatus(String persistStatus) {
		this.persistStatus = persistStatus;
	}
	/****************************
	 * Change by Sriram to add persist status attribute ends
	 */
	/****************************
	 * Change by Sriram to add Label field for Location (18-Nov-2014)
	 */
	@TenjinMetadata(column_name="PA_LABEL", node_name="PA_LABEL")
	@Downloadable(columnName="Label")
	@Uploadable(columnName="Label")
	private String label;
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	
	/****************************
	 * Change by Sriram to add Label field for Location (18-Nov-2014) Ends
	 */
	public List<Location> getChildLocations() {
		return childLocations;
	}
	public void setChildLocations(List<Location> childLocations) {
		this.childLocations = childLocations;
	}
	public List<TestObject> getTestObjects() {
		return testObjects;
	}
	public void setTestObjects(List<TestObject> testObjects) {
		this.testObjects = testObjects;
	}
	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * @return the locationType
	 */
	public String getLocationType() {
		return locationType;
	}
	/**
	 * @param locationType the locationType to set
	 */
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	/**
	 * @return the sequence
	 */
	public int getSequence() {
		return sequence;
	}
	/**
	 * @param sequence the sequence to set
	 */
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	/**
	 * @return the locationName
	 */
	public String getLocationName() {
		return locationName;
	}
	/**
	 * @param locationName the locationName to set
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	/**
	 * @return the parent
	 */
	public String getParent() {
		return parent;
	}
	/**
	 * @param parent the parent to set
	 */
	public void setParent(String parent) {
		this.parent = parent;
	}
	/**
	 * @return the wayIn
	 */
	public String getWayIn() {
		return wayIn;
	}
	/**
	 * @param wayIn the wayIn to set
	 */
	public void setWayIn(String wayIn) {
		this.wayIn = wayIn;
	}
	/**
	 * @return the wayOut
	 */
	public String getWayOut() {
		return wayOut;
	}
	/**
	 * @param wayOut the wayOut to set
	 */
	public void setWayOut(String wayOut) {
		this.wayOut = wayOut;
	}
	/**
	 * @return the landingPage
	 */
	public boolean isLandingPage() {
		return landingPage;
	}
	/**
	 * @param landingPage the landingPage to set
	 */
	public void setLandingPage(boolean landingPage) {
		this.landingPage = landingPage;
	}
	@Override
	public String toString() {
		return "Location [locationName=" + locationName + ", parent=" + parent
				+ ", wayIn=" + wayIn + ", locationType=" + locationType
				+ ", screenTitle=" + screenTitle + ",label="+ label+ "]";
	}

	public static Location loadFromJson(JSONObject json){
		String pageName = "";
		String pageType = "";
		String wayIn = "";
		String wayOut = "";
		String pageParent = "";
		String path ="";
		/*changed for adding page label in iteration data starts*/
		String label="";
		/*changed for adding page label in iteration data ends*/
		boolean pageNavOnly = false;
		boolean isMrb = false;
		try{
			pageName = json.getString("PA_NAME");
			pageType = json.getString("PA_TYPE");
			pageParent = json.getString("PA_PARENT");
			wayIn = json.getString("PA_WAY_IN");
			wayOut = json.getString("PA_WAY_OUT");
			String mrb = json.getString("PA_IS_MRB");
			/*changed for adding page label in iteration data starts*/
			label=json.getString("PA_LABEL");
			/*changed for adding page label in iteration data ends*/
			
			if(mrb != null && mrb.equalsIgnoreCase("Y")){
				isMrb = true;
			}else{
				isMrb = false;
			}
			try{
				String nav = json.getString("PA_NAV_ONLY");
				if(nav != null && nav.equalsIgnoreCase("YES")){
					pageNavOnly = true;
				}else{
					pageNavOnly = false;
				}
			}catch(Exception e){
				pageNavOnly = false;
			}
			
			try{
				path = json.getString("PA_PATH");
			}catch(Exception e) {}
			
			Location location = new Location();
			location.setLocationName(pageName);
			location.setLocationType(pageType);
			location.setParent(pageParent);
			location.setWayIn(wayIn);
			location.setWayOut(wayOut);
			location.setMultiRecordBlock(isMrb);
			location.setPageNavOnly(pageNavOnly);
			/*changed for adding page label in iteration data starts*/
			location.setLabel(label);
			/*changed for adding page label in iteration data ends*/
			location.setPath(path);
			return location;
		}catch(JSONException e){
			logger.error("Could not convert json to location");
			logger.error("JSON --> {}", json.toString());
			logger.error(e.getMessage(),e);
			return null;
		}
	}
	
	public JSONObject toJson(){
		
		JSONObject page = new JSONObject();
		
		//JSONObject page = new JSONObject();
		try {
			page.put("PA_NAME", locationName);
			page.put("PA_TYPE", locationType);
			page.put("PA_WAY_IN", wayIn);
			page.put("PA_WAY_OUT", wayOut);
			page.put("PA_PARENT", parent);
			page.put("PA_PATH", path);
			/*changed for adding page label in iteration data starts*/
			page.put("PA_LABEL", label);
			/*changed for adding page label in iteration data starts*/
			if(multiRecordBlock){
				page.put("PA_IS_MRB", "Y");
			}else{
				page.put("PA_IS_MRB", "N");
			}
			//added by shivam to fix JSONObject["PA_SCREEN_TITLE"] not found. - starts
			page.put("PA_SCREEN_TITLE", screenTitle);

			//added by shivam to fix JSONObject["PA_SCREEN_TITLE"] not found. - ends
		} catch (JSONException e) {
			
			logger.error("ERROR converting Location to JSON",e);
		}
		
		return page;
	}
	
	@Downloadable(columnName="Function Code")
	private String moduleCode;
	@Downloadable(columnName="Application")
	private String appName;

	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	private int uploadRow;

	public int getUploadRow() {
		return uploadRow;
	}
	public void setUploadRow(int uploadRow) {
		this.uploadRow = uploadRow;
	}

	//added by shivam for new adapter spec -starts
	private WebElement wrapper; 
	public WebElement getWrapper() {
		return wrapper;
	}
	public void setWrapper(WebElement wrapper) {
		this.wrapper = wrapper;
	}
	//added by shivam for new adapter spec -starts
	
	/* Added by Ashiki for TENJINCG-1225 ends*/
	private List<TestObject> funcFields;
	private String source;
	private String txnMode;
	
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getTxnMode() {
		return txnMode;
	}
	public void setTxnMode(String txnMode) {
		this.txnMode = txnMode;
	}
	public List<TestObject> getFuncFields() {
		return funcFields;
	}
	public void setFuncFields(List<TestObject> funcFields) {
		this.funcFields = funcFields;
	}
	/* Added by Ashiki for TENJINCG-1225 ends*/
	

	
}
