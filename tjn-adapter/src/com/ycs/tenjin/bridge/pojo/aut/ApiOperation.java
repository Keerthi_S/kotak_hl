/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApiOperation.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 03-Apr-2017           Sriram Sridharan          Newly Added For
* 17-Aug-2017			Sriram Sridharan		Fix for T25IT-188
* 23-Nov-2017			Sriram Sridharan		TENJINCG-515
* 02-Feb-2018			Sriram					TENJINCG-415
* 18-11-2020			Ashiki					TENJINCG-1213
*/


package com.ycs.tenjin.bridge.pojo.aut;

import java.util.ArrayList;
import java.util.List;

import com.ycs.tenjin.util.Utilities;

public class ApiOperation {
	
	private String name;
	private ApiLearnerResultBean lastSuccessfulLearningResult;
	
	//changes done by parveen, Leelaprasad for #405 starts
	private String apiCode;
	private ArrayList<Representation> listResponseRepresentation;
	private ArrayList<Representation> listRequestRepresentation;
	
	/*	Added By Paneendra for TENJINCG-1239 starts */
	private ArrayList<Representation> listHeaderRepresentation;
	
	/*	Added By Paneendra for TENJINCG-1239 ends */
	public String getApiCode() {
		return apiCode;
	}
	public void setApiCode(String apiCode) {
		this.apiCode = apiCode;
	}
	
	
	/*	Added By Paneendra for TENJINCG-1239 starts */
	
	public ArrayList<Representation> getListHeaderRepresentation() {
		return listHeaderRepresentation;
	}
	public void setListHeaderRepresentation(ArrayList<Representation> listHeaderRepresentation) {
		this.listHeaderRepresentation = listHeaderRepresentation;
	}
	
	/*	Added By Paneendra for TENJINCG-1239 ends */
	public ArrayList<Representation> getListResponseRepresentation() {
		return listResponseRepresentation;
	}
	public void setListResponseRepresentation(
			ArrayList<Representation> listResponseRepresentation) {
		this.listResponseRepresentation = listResponseRepresentation;
	}
	public ArrayList<Representation> getListRequestRepresentation() {
		return listRequestRepresentation;
	}
	public void setListRequestRepresentation(
			ArrayList<Representation> listRequestRepresentation) {
		this.listRequestRepresentation = listRequestRepresentation;
	}
	//changes done by parveen, Leelaprasad for #405 ends
	
	public ApiLearnerResultBean getLastSuccessfulLearningResult() {
		return lastSuccessfulLearningResult;
	}
	public void setLastSuccessfulLearningResult(ApiLearnerResultBean lastSuccessfulLearningResult) {
		this.lastSuccessfulLearningResult = lastSuccessfulLearningResult;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/* Added for REST API Support */
	private List<Parameter> resourceParameters = new ArrayList<Parameter>();
	private String method;
	


	public List<Parameter> getResourceParameters() {
		return resourceParameters;
	}
	public void setResourceParameters(List<Parameter> resourceParameters) {
		this.resourceParameters = resourceParameters;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public void merge(ApiOperation newOperation ) {
		if(newOperation == null) {
			return;
		}
		
		this.name = Utilities.trim(newOperation.getName()).length() > 0 ? newOperation.getName() : this.name;
		this.method = Utilities.trim(newOperation.getMethod()).length() > 0 ? newOperation.getMethod() : this.method;
		if(newOperation.getListRequestRepresentation() != null) {
			this.listRequestRepresentation = newOperation.getListRequestRepresentation();
		}
		/*	Added By Paneendra for TENJINCG-1239 starts */
		if(newOperation.getListHeaderRepresentation() != null) {
			this.listHeaderRepresentation = newOperation.getListHeaderRepresentation();
		}
		
		/*	Added By Paneendra for TENJINCG-1239 ends */
		if(newOperation.getListResponseRepresentation() != null) {
			this.listResponseRepresentation = newOperation.getListResponseRepresentation();
		}
		
		if(newOperation.getResourceParameters() != null) {
			this.resourceParameters = newOperation.getResourceParameters();
		}
		
		this.authenticationType = Utilities.trim(newOperation.getAuthenticationType()).length() > 0 ? newOperation.getAuthenticationType() : this.authenticationType;
	}
	private String authenticationType;
	private String parentAuthenticationType;

	public String getAuthenticationType() {
		return authenticationType;
	}
	public void setAuthenticationType(String authenticationType) {
		this.authenticationType = authenticationType;
	}
	public String getParentAuthenticationType() {
		return parentAuthenticationType;
	}
	public void setParentAuthenticationType(String parentAuthenticationType) {
		this.parentAuthenticationType = parentAuthenticationType;
	}
	//TENJINCG-415 ends
	/*Added by Ashiki for TENJINCG-1213 starts*/
	private String appName;
	private String apiType;
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getApiType() {
		return apiType;
	}
	public void setApiType(String apiType) {
		this.apiType = apiType;
	}
	/*Added by Ashiki for TENJINCG-1213 ends*/
	
	
}
