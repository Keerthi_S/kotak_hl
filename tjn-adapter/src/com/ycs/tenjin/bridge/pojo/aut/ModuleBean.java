/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ModuleBean.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 20-06-2017           Padmavathi              TENJINCG-195
*/

package com.ycs.tenjin.bridge.pojo.aut;

import com.ycs.tenjin.spreadsheet.annotation.Downloadable;
public class ModuleBean {
	@Downloadable(columnName="Code")
	String moduleCode;
	@Downloadable(columnName="Name")
	String moduleName;
	@Downloadable(columnName="Application")
	String applicationName;
	@Downloadable(columnName="Menu")
	String menuContainer;
	private int aut;
	private String learningResult;
	private String learnerMessage;
	/*********************************************8
	 * Change by Nagareddy for Test data path
	 */
	
	@Downloadable(columnName="Group")
	private String groupName;
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/* Added by Padmavathi for Req# Tenjincg-195 20-06-2017 starts*/
	private String dateFormat;
	public String getDateFormat() {
		return dateFormat;
	}
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	/* Added by Padmavathi for Req# Tenjincg-195 20-06-2017 ends*/
	/*********************************************8
	 * Change by Nagareddy for Test data path ends
	 */
	/****************************
	 * Change by: Badal
	 * Issue: Include last learnt timestamp
	 */
	private String lastLearntStartTimestamp;
	private String lastLearntTimestamp;
	private String wsLastLearntTimestamp;	//03-Nov-2014 Adding Last Learnt Timestamp
	
	public String getLastLearntTimestamp() {
		return lastLearntTimestamp;
	}
	public void setLastLearntTimestamp(String lastLearntTimestamp) {
		this.lastLearntTimestamp = lastLearntTimestamp;
	}
	//03-Nov-2014 Adding Last Learnt Timestamp: Starts
	public String getWSLastLearntTimestamp() {
		return wsLastLearntTimestamp;
	}
	public void setWSLastLearntTimestamp(String t) {
		this.wsLastLearntTimestamp = t;
	}
	
	public String getLastLearntStartTimestamp() {
		return lastLearntStartTimestamp;
	}
	public void setLastLearntStartTimestamp(String lastLearntStartTimestamp) {
		this.lastLearntStartTimestamp = lastLearntStartTimestamp;
	}

	//03-Nov-2014 Adding Last Learnt Timestamp: Ends
	/*****************
	 * Change by badal ends
	 */
	/*20-Oct-2014 WS: Starts*/
	String wsurl;
	String wsop;
	
	public String getWSURL() {
		return wsurl;
	}
	public void setWSURL(String u) {
		this.wsurl = u;
	}
	
	public String getWSOp() {
		return wsop;
	}
	public void setWSOp(String o) {
		this.wsop = o;
	}
	
	public String getLearningResult() {
		return learningResult;
	}
	public void setLearningResult(String learningResult) {
		this.learningResult = learningResult;
	}
	public String getLearnerMessage() {
		return learnerMessage;
	}
	public void setLearnerMessage(String learnerMessage) {
		this.learnerMessage = learnerMessage;
	}
	public int getAut() {
		return aut;
	}
	public void setAut(int aut) {
		this.aut = aut;
	}
	/**
	 * @return the menuContainer
	 */
	public String getMenuContainer() {
		return menuContainer;
	}
	/**
	 * @param menuContainer the menuContainer to set
	 */
	public void setMenuContainer(String menuContainer) {
		this.menuContainer = menuContainer;
	}
	/**
	 * @return the moduleCode
	 */
	public String getModuleCode() {
		return moduleCode;
	}
	/**
	 * @param moduleCode the moduleCode to set
	 */
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	/**
	 * @return the moduleName
	 */
	public String getModuleName() {
		return moduleName;
	}
	/**
	 * @param moduleName the moduleName to set
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	/**
	 * @return the applicationName
	 */
	public String getApplicationName() {
		return applicationName;
	}
	/**
	 * @param applicationName the applicationName to set
	 */
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	
	
}
