package com.ycs.tenjin.bridge.pojo.aut;

public class Parameter {
	
	private String name;
	
	private String type;
	
	private String style;
	
	//TENJINCG-1118 (Sriram)
	private boolean required;
	//TENJINCG-1118 (Sriram) ends
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}
	
	
	
	
}
