/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExtractorResultBean.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 08-Nov-2016           Sriram Sridharan          Newly Added For Data Extraction in Tenjin 2.4.1
* 09-01-2017           Leelaprasad              Req#TENJINCG-6
*/


package com.ycs.tenjin.bridge.pojo.aut;

import java.sql.Timestamp;
import java.util.List;

public class ExtractorResultBean {
	
	int id;
	int runId;
	String userId;
	String hostName;
	int appId;
	String functionCode;
	Timestamp startTime;
	Timestamp endTime;
	String inputFilePath;
	String status;
	String message;
	String autUserType;
	List<ExtractionRecord> records;
	/* Changed by Leelaprasad for the Requirement TENJINCG-6 ON 09-01-2017 starts*/
	private String elapsedTime;
	public String getElapsedTime() {
		return elapsedTime;
	}
	public void setElapsedTime(String eTime) {
		this.elapsedTime = eTime;
	}
	/* Changed by Leelaprasad for the Requirement TENJINCG-6 ON 09-01-2017 ends*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRunId() {
		return runId;
	}
	public void setRunId(int runId) {
		this.runId = runId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	public String getFunctionCode() {
		return functionCode;
	}
	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public String getInputFilePath() {
		return inputFilePath;
	}
	public void setInputFilePath(String inputFilePath) {
		this.inputFilePath = inputFilePath;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getAutUserType() {
		return autUserType;
	}
	public void setAutUserType(String autUserType) {
		this.autUserType = autUserType;
	}
	public List<ExtractionRecord> getRecords() {
		return records;
	}
	public void setRecords(List<ExtractionRecord> records) {
		this.records = records;
	}
	
	
	
}
