/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Api.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 03-Apr-2017           Sriram Sridharan          Newly Added For TENJINCG-152 (API Testing Enablement)
* 21-Nov-2017           Gangadhar Badagi          To update url instead of name
* 02-Feb-2018			Pushpalatha				TENJINCG-568
*/


package com.ycs.tenjin.bridge.pojo.aut;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ycs.tenjin.util.Utilities;

public class Api {

	private String code;
	private String name;
	private String type;
	private String url;
	private int applicationId;
	private String applicationName;
	
	private ApiLearnerResultBean lastSuccessfulLearning;
	private List<ApiLearnerResultBean> learningHistory = new ArrayList<ApiLearnerResultBean>();
	
	private boolean wsdlAvailable;
	private boolean urlValid;
	/*Added by Pushpalatha for TENJINCG-568 starts*/
	private String group;
	
	//Changes for TENJINCG-1118(Sriram)
	@JsonIgnore
	private boolean nonExistent;
	
	public boolean isNonExistent() {
		return nonExistent;
	}
	public void setNonExistent(boolean nonExistent) {
		this.nonExistent = nonExistent;
	}
	//Changes for TENJINCG-1118(Sriram) ends
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	/*Added by Pushpalatha for TENJINCG-568 ends*/
	public boolean isUrlValid() {
		return urlValid;
	}
	public void setUrlValid(boolean urlValid) {
		this.urlValid = urlValid;
	}
	public boolean isWsdlAvailable() {
		return wsdlAvailable;
	}
	public void setWsdlAvailable(boolean wsdlAvailable) {
		this.wsdlAvailable = wsdlAvailable;
	}
	public List<ApiLearnerResultBean> getLearningHistory() {
		return learningHistory;
	}
	public void setLearningHistory(List<ApiLearnerResultBean> learningHistory) {
		this.learningHistory = learningHistory;
	}
	public ApiLearnerResultBean getLastSuccessfulLearning() {
		return lastSuccessfulLearning;
	}
	public void setLastSuccessfulLearning(ApiLearnerResultBean lastSuccessfulLearning) {
		this.lastSuccessfulLearning = lastSuccessfulLearning;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	
	private List<ApiOperation> operations = new ArrayList<ApiOperation>();
	public List<ApiOperation> getOperations() {
		return operations;
	}
	public void setOperations(List<ApiOperation> operations) {
		this.operations = operations;
	}
	
	
	public void merge(Api newApi) {
		this.name = Utilities.trim(newApi.getName()).length() > 0 ? newApi.getName() : this.name;
		this.url = Utilities.trim(newApi.getUrl()).length() > 0 ? newApi.getUrl() : this.url;
	}
	
	
	private String encryptionType;

	public String getEncryptionType() {
		return encryptionType;
	}
	public void setEncryptionType(String encryptionType) {
		this.encryptionType = encryptionType;
	}
	
	private String encryptionKey;

	public String getEncryptionKey() {
		return encryptionKey;
	}
	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}
	
	
	
}
