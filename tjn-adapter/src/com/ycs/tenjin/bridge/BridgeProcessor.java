/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  BridgeProcessor.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 22-Sep-2016          Sriram Sridharan        Newly Added For 
 * 16-06-2017			Sriram Sridharan		TENJINCG-187
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 08-05-2018			Preeti					TENJINCG-625
 * 26-09-2018           Leelaprasad             TENJINCG-739
 * 19-11-2018			Prem					TENJINCG-843 
 * 30-11-2018           Padmavathi              TENJINCG-853 
 * 24-06-2019           Padmavathi              for license
 */

package com.ycs.tenjin.bridge;


import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.oem.CommonOem;
import com.ycs.tenjin.bridge.oem.Oem;
import com.ycs.tenjin.bridge.pojo.ExecutorProgress;
import com.ycs.tenjin.bridge.pojo.PrintScreenObject;
import com.ycs.tenjin.bridge.pojo.TaskManifest;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.util.Utilities;

public class BridgeProcessor {
	public static final Logger logger = LoggerFactory
			.getLogger(BridgeProcessor.class);

	private String taskName = "";
	private Thread runnable;
	private TaskManifest manifest;
	private BridgeProcess task;

	public BridgeProcessor(TaskManifest manifest) {
		this.manifest = manifest;
	}

	public BridgeProcessor() {

	}


	public interface NamedRunnable extends Runnable{
	    String getTaskName();
	}
	
	public class NamedThreadFactory implements ThreadFactory{
	    public Thread newThread(Runnable r) {
	        Thread t = new Thread(r);
	        if (r instanceof NamedRunnable) {
	            NamedRunnable namedRunnable = (NamedRunnable)r;
	            t.setName(namedRunnable.getTaskName());
	        }
	        return t;
	    }
	}
	
	

	public void createTask() {
		
		/***
		 * Changed for TENJINCG-1175 (TENJINCG-1178) Sriram
		 */
//		String adapterType = "";
//		if (BridgeProcess.APIEXECUTE.equalsIgnoreCase(this.manifest
//				.getTaskType())
//				|| BridgeProcess.APILEARN.equalsIgnoreCase(this.manifest
//						.getTaskType())
//				|| BridgeProcess.APIEXTRACT.equalsIgnoreCase(this.manifest
//						.getTaskType())) {
//			adapterType = "api";
//		} else {
//			logger.info("Initializing Adapter for Automation Engine --> [{}]",
//					"gui");
//			
//			adapterType = "gui";
//		}
		/***
		 * Changed for TENJINCG-1175 (TENJINCG-1178) Sriram ends
		 */

		try {
			//Changed for TENJINCG-1175 (TENJINCG-1178) Sriram
//			Oem oemAdapter = OemFactory.create(adapterType);
//			logger.info("Adapter for automation engine [{}] initialized",
//					adapterType);
			
			Oem oemAdapter = new CommonOem();
			// Changed for TENJINCG-1175 (TENJINCG-1178) Sriram ends
			oemAdapter.setManifest(this.manifest);
			
			
			this.task = new BridgeProcess("Tenjin Run " + this.taskName,
					oemAdapter, this.manifest.getRunId(),this.manifest.getTaskType(),this.manifest.getProjectId(),this.manifest.getAdapterName());
			

			logger.info("Initializing Run Cache Store");
			try {
				CacheUtils.initializeRunCache();
			} catch (TenjinConfigurationException e) {

				logger.warn(
						"Could not initialize Cache Store. Please ensure RUN_CACHE_NAME property is set in Tenjin Configuration.",
						e);
			}
			ExecutorProgress progress = new ExecutorProgress();
			progress.setRunAborted(false);
			
			try {
				String printScreenFlag = TenjinConfiguration.getProperty("ENABLE_PRINTSCREEN");
				if(printScreenFlag != null && printScreenFlag.equalsIgnoreCase("Y")){
					PrintScreenObject prntScreen = new PrintScreenObject();
					String ipAddress = this.manifest.getClientIp();
					if(Utilities.isLocalHost(this.manifest.getClientIp())){
						try {
							ipAddress = Inet4Address.getLocalHost().getHostAddress();
						} catch (UnknownHostException e) {
							logger.debug("ERROr while getting the ipAddress for localhost");
						}
					}else{
						ipAddress = this.manifest.getClientIp();
					}
					
					prntScreen.setRunId(this.manifest.getRunId());
					prntScreen.setClientIP(ipAddress);
					CacheUtils.putObjectInRunCache("screenshot" + ipAddress, prntScreen);
				}
			} catch (TenjinConfigurationException e1) {
				logger.debug("Error while initializing the print screen configuration.");
			}

			CacheUtils.putObjectInRunCache(this.manifest.getRunId(), progress);
		} catch (Exception e) {

			logger.error(e.getMessage());
		}

		this.taskName = Integer.toString(this.manifest.getRunId());
		this.runnable = new Thread(this.task, "Tenjin Run "
				+ this.taskName);
		/*ThreadPoolExecutor executor = 
				  (ThreadPoolExecutor) Executors.newCachedThreadPool();*/
		//executor.submit(this.task);
		//this.task = new BridgeProcess("Tenjin Run " + this.taskName, this.manifest.getRunId(),this.manifest.getProjectId(),this.manifest.getAdapterName());
		logger.info("Task Created --> {}", this.taskName);
	}
	
	public void createTask1() {
	
		this.task = new BridgeProcess("Tenjin Run " + this.taskName, this.manifest.getRunId(),this.manifest.getProjectId(),this.manifest.getTaskType());
		logger.info("Initializing Run Cache Store");
		try {
				CacheUtils.initializeRunCache();
		} catch (TenjinConfigurationException e) {
			logger.warn("Could not initialize Cache Store. Please ensure RUN_CACHE_NAME property is set in Tenjin Configuration.",e);
		}
		ExecutorProgress progress = new ExecutorProgress();
		CacheUtils.putObjectInRunCache(this.manifest.getRunId(), progress);
		this.taskName = Integer.toString(this.manifest.getRunId());
		this.runnable = new Thread(this.task, "Tenjin Run "
				+ this.taskName);
		logger.info("Task Created --> {}", this.taskName);
	}
	
	public void runTask() {
		if (this.runnable != null) {
			logger.info("Starting task {}", this.taskName);
			//this.runnable.start();
			ThreadPoolExecutor executor = 
					  (ThreadPoolExecutor) Executors.newCachedThreadPool();
			executor.execute(this.task);
			executor.shutdown();
		}
	}

}
