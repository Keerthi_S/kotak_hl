/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApiUtilities.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 04-Apr-2017           Sriram Sridharan        Newly Added For
* 06-Aug-2017			Roshni Das			    T25IT-9
* 10-Aug-2017			Sriram					T25IT-54
* 22-Sep-2017			sameer gupta			For new adapter spec
* 28-06-2018			Preeti					T251IT-133
*/


package com.ycs.tenjin.bridge.utils;


import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.cli2.validation.InvalidArgumentException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.api.ApiApplication;
import com.ycs.tenjin.bridge.oem.api.ApiApplicationFactory;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.DatabaseException;

public class ApiUtilities {
	
	private static final Logger logger = LoggerFactory.getLogger(ApiUtilities.class);
	
	
	public static void main(String[] args) throws BridgeException, DatabaseException, InvalidArgumentException, IOException {

		
		System.out.println(getApplicationBaseUrl("http://localhost:8080/TenjinWeb"));
		System.out.println(getApplicationBaseUrl("api.openweathermap.org"));
	}
		
	
	public static boolean isUrlValid(String url) {
		boolean urlValid = false;
		
		
		urlValid = true;
		return urlValid;
	}
	
	
	public static String generateSOAPApiUrl(String autUrl, String apiCode) {
		try {
			URL oUrl = new URL(autUrl);
			String protocol = oUrl.getProtocol();
			String host = oUrl.getHost();
			int port = oUrl.getPort();
			String finalUrl = protocol + "://" + host + ":" + port + "/" + apiCode + "/" + apiCode + "?wsdl";
			return finalUrl;
		} catch (MalformedURLException e) {
			
			logger.error("ERROR generating API URL", e);
			return "";
		}
	}
	
	public static String getApplicationBaseUrl(String url) {
		
		
		boolean protocolMentioned = true;
		URL oUrl = null;
		try {
			oUrl = new URL(url);
		}catch(MalformedURLException e) {
			if(e.getMessage().toLowerCase().contains("no protocol")) {
				protocolMentioned = false;
				url = "http://" + url;
				try {
					oUrl = new URL(url);
				}catch(MalformedURLException e1) {
					logger.error("ERROR getting application base url. Invalid URL [{}]", url, e1);
					return "";
				}
			}else {
				logger.error("ERROR getting application base url. Invalid URL [{}]", url, e);
				return "";
			}
		}
		
		String finalUrl = "";
		try {
			String protocol = oUrl.getProtocol();
			String host = oUrl.getHost();
			int port = oUrl.getPort();
			
			if(protocolMentioned)
				finalUrl = protocol + "://" + host;
			else
				finalUrl = host;
			if(port > 0) {
				finalUrl += ":" + port;
			}
		} catch (Exception e) {
			logger.error("ERROR getting application base url.", e);
		}
		
		return finalUrl;
	}
	
	
	
	public static Api getAllApiOperations(int appId, String apiCode, boolean persist) throws BridgeException {
		Api api = null;
		
		logger.debug("Fetching API [{}] in application [{}]", apiCode, appId);
		
		try {
			api = new ApiHelper().hydrateApi(appId, apiCode);
		} catch (DatabaseException e) {
			
			logger.error("An error occurred while fetching API [{}] under application [{}]", apiCode, appId, e);
		}
		
		if(api == null) {
			throw new BridgeException("API ["+ apiCode +"] was not found. Please contact your Tenjin Administrator.");
		}
		
		ApiApplication bridge = null;
		
		logger.info("Initializing API bridge for [{}]", api.getType());
		bridge = ApiApplicationFactory.create(api.getType());
		
		logger.info("Getting all operations for API [{}] from URL [{}]", api.getCode(), api.getUrl());
		bridge.getAllOperations(api);
		
		if(persist) {
			logger.info("Updating the operations in the database");
			ApiHelper helper = new ApiHelper();
			
			try {
				logger.info("Clearing existing operations");
				helper.clearAllApiOperations(api.getCode(), appId);
				
				logger.info("Saving the new operations");
				helper.persistApiOperations(api.getOperations(), api.getCode(), appId);
			} catch (DatabaseException e) {
				
				throw new BridgeException("Could not save Operations due to an internal error. Please contact Tenjin Support.");
			}
		}
		
		return api;
		
	}
	
	
	public static void checkPath(String path) {
		
		try {
			File file = new File(path);
			
			if(!file.exists()) {
				file.mkdirs();
			}
		} catch (Exception e) {
			
			logger.warn("Could not load path [{}]", path);
		}
		
	}
	
	public static JSONArray getWSDLsForAut(int appId) throws AutException, TenjinConfigurationException {
		String mediaRepoPath = TenjinConfiguration.getProperty("MEDIA_REPO_PATH");
		String dateFormat = TenjinConfiguration.getProperty("DATE_FORMAT");
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		
		String wsdlsPath = mediaRepoPath + "/applications/" + appId + "/api/wsdl";
		File wsdlsLocation = new File(wsdlsPath) ;
		wsdlsLocation.mkdirs();
		
		JSONArray jArray = new JSONArray();
		
		if(wsdlsLocation.listFiles() == null) {
			logger.warn("No WSDLs are uploaded for AUT [{}]", appId);
			return jArray;
		}
		
		for(File wsdl : wsdlsLocation.listFiles()) {
			JSONObject json = new JSONObject();
			try {
				json.put("name", wsdl.getName());
				json.put("path", wsdl.getPath());
				Date lastModified = new Date(wsdl.lastModified());
				json.put("lastModified", sdf.format(lastModified));
				
				jArray.put(json);
			} catch (JSONException e) {
				
				logger.error("JSONException occurred", e);
			}
		}
		
		return jArray;
	}
	
	
	public static JSONArray getWADLsForAut(int appId) throws AutException, TenjinConfigurationException {
		String mediaRepoPath = TenjinConfiguration.getProperty("MEDIA_REPO_PATH");
		String dateFormat = TenjinConfiguration.getProperty("DATE_FORMAT");
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		
		String wadlsPath = mediaRepoPath + "/applications/" + appId + "/api/wadl";
		File wadlsLocation = new File(wadlsPath) ;
		wadlsLocation.mkdirs();
		
		JSONArray jArray = new JSONArray();
		
		if(wadlsLocation.listFiles() == null) {
			logger.warn("No WADLs are uploaded for AUT [{}]", appId);
			return jArray;
		}
		
		for(File wadl : wadlsLocation.listFiles()) {
			JSONObject json = new JSONObject();
			try {
				json.put("name", wadl.getName());
				json.put("path", wadl.getPath());
				Date lastModified = new Date(wadl.lastModified());
				json.put("lastModified", sdf.format(lastModified));
				
				jArray.put(json);
			} catch (JSONException e) {
				
				logger.error("JSONException occurred", e);
			}
		}
		
		return jArray;
	}
	
	
	public static void deleteWADLsForAut(int appId) throws AutException, TenjinConfigurationException {

		String mediaRepoPath = TenjinConfiguration.getProperty("MEDIA_REPO_PATH");
		
		String wadlsPath = mediaRepoPath + "/applications/" + appId + "/api/wadl";
		File wadlsLocation = new File(wadlsPath) ;
		if(wadlsLocation.listFiles() == null) {
			logger.warn("No WADLs are uploaded for AUT [{}]", appId);
		}
		
		for(File wadl : wadlsLocation.listFiles()) {
			wadl.delete();
		}
	
	}
	public static void deleteWSDLsForAut(int appId) throws AutException, TenjinConfigurationException {
		String mediaRepoPath = TenjinConfiguration.getProperty("MEDIA_REPO_PATH");
		String wsdlsPath = mediaRepoPath + "/applications/" + appId + "/api/wsdl";
		File wsdlsLocation = new File(wsdlsPath) ;
		
		if(wsdlsLocation.listFiles() == null) {
			logger.warn("No WSDLs are uploaded for AUT [{}]", appId);
		}
		
		for(File wsdl : wsdlsLocation.listFiles()) {
			wsdl.delete();
		}
		
	}
	
	public static JSONArray getXSDsForAut(int appId) throws AutException, TenjinConfigurationException {
		String mediaRepoPath = TenjinConfiguration.getProperty("MEDIA_REPO_PATH");
		String dateFormat = TenjinConfiguration.getProperty("DATE_FORMAT");
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		
		String xsdsPath = mediaRepoPath + "/applications/" + appId + "/api/xsd";
		File xsdsLocation = new File(xsdsPath) ;
		xsdsLocation.mkdirs();
		
		JSONArray jArray = new JSONArray();
		
		if(xsdsLocation.listFiles() == null) {
			logger.warn("No XSDs are uploaded for AUT [{}]", appId);
			return jArray;
		}
		
		
		for(File xsd : xsdsLocation.listFiles()) {
			JSONObject json = new JSONObject();
			try {
				json.put("name", xsd.getName());
				json.put("path", xsd.getPath());
				Date lastModified = new Date(xsd.lastModified());
				json.put("lastModified", sdf.format(lastModified));
				
				jArray.put(json);
			} catch (JSONException e) {
				
				logger.error("JSONException occurred", e);
			}
		}
		
		return jArray;
	}
	
	
	public static boolean apiHasWSDL(int appId, String apiCode) {
		boolean wsdlAvailable = false;
		
		try {
			String mediaRepoPath = TenjinConfiguration.getProperty("MEDIA_REPO_PATH");
			String wsdlsPath = mediaRepoPath + "/applications/" + appId + "/api/wsdl";
			
			File wsdl = new File(wsdlsPath + "/" + apiCode + ".wsdl");
			if(wsdl.exists()) {
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			
			logger.error("ERROR loading WSDL for API [{}]", apiCode, e);
		}
		
		return wsdlAvailable;
	}
	//Fix for T25IT-89
	public static boolean isValidXML(String xml)  {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		try {
			docFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			docFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
		} catch (ParserConfigurationException e1) {
			logger.error(" Error in building the document  "+e1);
		}
		try {
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));
			docFactory.newDocumentBuilder().parse(is);
			return true;
		} catch (Exception e) {
			return false;
		} 
	}
	
	public static boolean isValidJSON(String json) {
		try {
			new JSONObject(json);
			return true;
		}catch(JSONException e) {
			try {
				new JSONArray(json);
				return true;
			} catch (JSONException e1) {
				
				return false;
			}
		}
	}
}
