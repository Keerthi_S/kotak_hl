/***
Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MessageValidatorUtils.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 15-06-2021			Ashiki					TENJINCG-1275
 */

package com.ycs.tenjin.bridge.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;

public class MessageValidatorUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageValidatorUtils.class);

	
	
	public String conversionOfFileToString(String filepath) throws IOException {
		StringBuilder sb = null;
		BufferedReader br = null;
		try {
			sb = new StringBuilder();
			br = new BufferedReader(new FileReader(filepath));
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
		} catch (Exception e) {
			e.getMessage();
		}finally{
			br.close();
		}
		return sb.toString();
	}
	
	public String fetchOutputMessageFile(String folderPath,String fileName) throws IOException {
		
		String result = "";
		String fileNameSplit = fileName.split("\\.")[0];
		String extentsion = fileName.split("\\.")[1];
		
		if (folderPath.startsWith("sftp")) {

			folderPath = folderPath.split(":")[1];
			fileName = this.copyFileFromServer(fileName, extentsion, folderPath);
			try {
				folderPath = TenjinConfiguration.getProperty("MSG_OUTPUT_FILE");
				result = this.conversionOfFileToString(folderPath + File.separator + fileName);
			} catch (TenjinConfigurationException e) {
				logger.error("No file found with the specified name");
				throw new ExecutorException("No file found with the name " + fileName);
			}
		} else {
			File dir = new File(folderPath);
			try {
				if (fileNameSplit.equals("*")) {
					File[] allFiles1 = dir.listFiles(new FilenameFilter() {
						@Override
						public boolean accept(File allFiles, String name) {
							return name.endsWith(extentsion);
						}
					});
					Arrays.sort(allFiles1, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
					fileName = allFiles1[0].getName();
				} else if (fileName.contains("*")) {
					String fileName1 = fileName.replace("*", "").split("\\.")[0];
					File[] allFiles = dir.listFiles(new FilenameFilter() {
						@Override
						public boolean accept(File allFiles, String name) {
							return name.contains(fileName1);
						}
					});
					Arrays.sort(allFiles, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
					String extention1 = null;
					for (File file : allFiles) {
						extention1 = FilenameUtils.getExtension(file.getName());
						if (extention1.equalsIgnoreCase("txt")) {
							fileName = file.getName();
							break;
						}
					}
				}
				if (folderPath != null) {
					result = this.conversionOfFileToString(folderPath + File.separator + fileName);
				}
			} catch (Exception e) {
				logger.error("No file found with the specified name");
				throw new ExecutorException("No file found with the name " + fileName);
			}
		}
		return result;
	}
	public String copyFileFromServer(String fileName, String extentsion, String folderPath) {
		String hostName = null,userName = null,password = null;
		Session session = null;
	    Channel channel = null;
	    ChannelSftp channelSftp = null;
			try {
				hostName = TenjinConfiguration.getProperty("SFTP_HOST");
				userName = TenjinConfiguration.getProperty("SFTP_USERNAME");
				password = TenjinConfiguration.getProperty("SFTP_PASSWORD");
			} catch (TenjinConfigurationException e1) {
				logger.error("Couldnot get the information");
			}

			 try {
			        JSch jsch = new JSch();
			        session = jsch.getSession(userName, hostName, 22);
			        session.setPassword(password);
			        java.util.Properties config = new java.util.Properties();
			        config.put("StrictHostKeyChecking", "no");
			        session.setConfig(config);
			        session.connect();
			        channel = session.openChannel("sftp");
			        channel.connect();
			        System.out.println("sftp channel opened and connected.");
			        channelSftp = (ChannelSftp) channel;
			        if (fileName.contains("*")) {
						fileName = getMatchingFile(folderPath, fileName, extentsion,channelSftp);
					}
			        String localDir = TenjinConfiguration.getProperty("MSG_OUTPUT_FILE")+"\\"+fileName;
			        String remoteDirectoryFile = folderPath+"/"+fileName;
			        channelSftp.get(remoteDirectoryFile, localDir);
			}catch (Exception ex) {
		        System.out.println("Exception found while tranfer the response.");
		    } finally {
		        channelSftp.exit();
		        channel.disconnect();
		        session.disconnect();
		    } 
	     return fileName;
	}

	@SuppressWarnings("unchecked")
	private String getMatchingFile(String folderPath, String fileName, String extention, ChannelSftp channelSftp)
			throws SftpException {
		Vector<ChannelSftp.LsEntry> list = null;
		String fileNameSplit = fileName.split("\\.")[0];
		channelSftp.cd(folderPath);
		if (fileNameSplit.equals("*")) {
			list = channelSftp.ls("*." + extention + "");
		} else if (fileName.contains("*")) {
			String fileName1 = fileName.replace("*", "").split("\\.")[0];
			list = channelSftp.ls("*" + fileName1 + "*." + extention + "");
		}

		Collections.sort(list, new sortFiles());
		fileName = list.get(0).getFilename();
		return fileName;
	}

	
}
