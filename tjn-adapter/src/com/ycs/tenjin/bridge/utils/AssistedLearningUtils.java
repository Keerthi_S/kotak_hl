/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AssistedLearningUtils.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 15-11-2016           Leelaprasad             Defect #656
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 19-01-2018			Pushpalatha				TenjinCG-587
 * 26-09-2018			Preeti					TENJINCG-742
 * 04-10-2018			Preeti					TENJINCG-823
 * 16-10-2018			Preeti					TENJINCG-880
 * 29-10-2018			Preeti					Burgon bank project changes 
 * 07-11-2018            Leelaprasad             TENJINCG-900
*/


package com.ycs.tenjin.bridge.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.bridge.exceptions.AssistedLearningException;
import com.ycs.tenjin.bridge.pojo.aut.AssistedLearningRecord;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.handler.AssistedLearningHandler;
import com.ycs.tenjin.util.Utilities;

public class AssistedLearningUtils {
	private static final Logger logger = LoggerFactory.getLogger(AssistedLearningUtils.class);
	
		
	public static CellStyle getHeaderRowStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}
	
	public static void enterRowData(String[] rowData, int rowNumber, Sheet sheet, boolean createRow, CellStyle cellStyle){
		
		int colIndex = 0;
		Row row;
		if(createRow){
			row = sheet.createRow(rowNumber);
		}else{
			row = sheet.getRow(rowNumber);
		}
		for(String data:rowData){
			Cell cell = row.createCell(colIndex);
			cell.setCellType(Cell.CELL_TYPE_STRING);
			cell.setCellValue(data);
			
			if(cellStyle != null){
				cell.setCellStyle(cellStyle);
			}
			colIndex++;
		}
	}
	
	
	public static void importFromSpreadsheetToXml(Aut aut, String functionCode, String spreadSheetPath, String xmlPath) throws AssistedLearningException{
		
		try{
			logger.info("Accessing spreadsheet from path {}", spreadSheetPath);
			Workbook workbook = WorkbookFactory.create(new File(spreadSheetPath));

			
			Sheet sheet = workbook.getSheetAt(0);
			
			logger.info("Preparing to read spreadsheet");
			Multimap<String , TestObject> map = LinkedListMultimap.create();
			
			int PAGE_COL = 1;
			int FIELD_COL = 2;
			int ID_TYPE_COL=3;
			int DATA_COL = 4;
			
			Iterator<Row> rowIter = sheet.iterator();
			
			while(rowIter.hasNext()){
				
				Row row = rowIter.next();
				
				if(row.getRowNum() < 1){
					continue;
				}
				TestObject t = new TestObject();
				
				Cell fLabelCell = row.getCell(FIELD_COL, Row.CREATE_NULL_AS_BLANK);
				fLabelCell.setCellType(Cell.CELL_TYPE_STRING);
				t.setLabel(fLabelCell.getStringCellValue());
				
				fLabelCell = row.getCell(ID_TYPE_COL, Row.CREATE_NULL_AS_BLANK);
				fLabelCell.setCellType(Cell.CELL_TYPE_STRING);
				if(!Utilities.trim(fLabelCell.getStringCellValue()).equalsIgnoreCase("")){
					t.setIdentifiedBy(fLabelCell.getStringCellValue());
				}
				
				fLabelCell = row.getCell(DATA_COL, Row.CREATE_NULL_AS_BLANK);
				fLabelCell.setCellType(Cell.CELL_TYPE_STRING);
				t.setData(fLabelCell.getStringCellValue());
				
				
				fLabelCell = row.getCell(PAGE_COL, Row.CREATE_NULL_AS_BLANK);
				fLabelCell.setCellType(Cell.CELL_TYPE_STRING);
				logger.info("Found a record to write on page {}", fLabelCell.getStringCellValue());
				map.put(fLabelCell.getStringCellValue(), t);
			}
			
			logger.info("Creating XML");
			createXml(aut, functionCode, xmlPath, map);
			
			logger.info("Done");
			
		}catch(Exception e){
			logger.error("An internal error occurred", e);
			throw new AssistedLearningException("Could not import data from spreadsheet due to an internal error", e);
		}
	}
	
	public static void exportFromXmlToSpreadsheet(int appId, String functionCode, String destinationPath) throws AssistedLearningException{
		Multimap<String, TestObject> map = getAssistedLearningData(functionCode, appId);
		Workbook workbook = null;
		
		try{
			workbook = new XSSFWorkbook();
			
			Sheet sheet = workbook.createSheet("Data");
			
			sheet.createRow(0);
			CellStyle headerCellStyle = getHeaderRowStyle(workbook);
			
			String[] headerCellValues = {"Sequence", "Page", "Field", "Identified By", "Data"};
			
			enterRowData(headerCellValues, 0, sheet, false, headerCellStyle);
			
			int row = 1;
			int sequence = 0;
			for(String key:map.keySet()){
				for(TestObject t:map.get(key)){
					sequence++;
					String[] rowData = {Integer.toString(sequence), key, t.getLabel(), t.getIdentifiedBy(), t.getData()};
					enterRowData(rowData, row, sheet, true, null);
					row++;
				}
			}
			OutputStream os = new FileOutputStream(destinationPath + File.separator + functionCode + "_assisted_learning.xlsx");
			workbook.write(os);
			os.close();
			
		}catch(Exception e){
			logger.error("Could not write to file", e);
		}
		
	}
	
	
	
	
	private static void createXml(Aut aut, String functionCode, String filePath, Multimap<String, TestObject> dataMap) throws AssistedLearningException, ParserConfigurationException{
		
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		docFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		docFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
		try {
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			Element rootElement = doc.createElement("TJNLEARNING");
			doc.appendChild(rootElement);
			
			Element app = doc.createElement("APP");
			app.appendChild(doc.createTextNode(aut.getName()));
			rootElement.appendChild(app);
			
			Element function = doc.createElement("FUNCTION");
			function.appendChild(doc.createTextNode(functionCode));
			rootElement.appendChild(function);
			
			for(String key:dataMap.keySet()){
				
				if(key.equalsIgnoreCase("")){
					continue;
				}
				Element screen = doc.createElement("SCREEN");
				screen.setAttribute("name", key);
				
				for(TestObject t:dataMap.get(key)){
					Element field = doc.createElement("FIELD");
					
					String label = replaceXMLSpecialCharacters(t.getLabel());
					field.setAttribute("label", label);
					
					field.setAttribute("value", t.getData());
					if(t.getIdentifiedBy() != null && !t.getIdentifiedBy().equalsIgnoreCase("")){
						field.setAttribute("idtype",t.getIdentifiedBy());
					}
					
					screen.appendChild(field);
				}
				rootElement.appendChild(screen);
			}
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filePath + "\\" + functionCode +"_" + aut.getId() + ".xml"));
			transformer.transform(source, result);
			
		} catch (ParserConfigurationException e) {
			
			logger.error("Unable to create Learning XML for {}", functionCode, e);
			throw new AssistedLearningException("An internal error occurred while loading data");
		} catch (TransformerConfigurationException e) {
			
			logger.error("Unable to create Learning XML for {}", functionCode, e);
			throw new AssistedLearningException("An internal error occurred while loading data");
		} catch (TransformerException e) {
			
			logger.error("Unable to create Learning XML for {}", functionCode, e);
			throw new AssistedLearningException("An internal error occurred while loading data");
		}
		
	}
	
	public static ArrayList<AssistedLearningRecord> getAssistedLearningDataFromExcel(String filePath,String functionCode, int appId) throws AssistedLearningException{
		ArrayList<AssistedLearningRecord> results = new ArrayList<AssistedLearningRecord>();
		try{
			logger.info("Accessing spreadsheet from path {}", filePath);
			Workbook workbook = WorkbookFactory.create(new File(filePath));
			Sheet sheet = workbook.getSheetAt(0);
			logger.info("Preparing to read spreadsheet");
			Multimap<String , TestObject> map = LinkedListMultimap.create();
			int PAGE_COL = 1;
			int FIELD_COL = 2;
			int ID_TYPE_COL=3;
			int DATA_COL = 4;
			Iterator<Row> rowIter = sheet.iterator();
			while(rowIter.hasNext()){
				Row row = rowIter.next();
				if(row.getRowNum() < 1){
					continue;
				}
				TestObject t = new TestObject();
				Cell fLabelCell = row.getCell(FIELD_COL, Row.CREATE_NULL_AS_BLANK);
				fLabelCell.setCellType(Cell.CELL_TYPE_STRING);
				t.setLabel(fLabelCell.getStringCellValue());
				fLabelCell = row.getCell(ID_TYPE_COL, Row.CREATE_NULL_AS_BLANK);
				fLabelCell.setCellType(Cell.CELL_TYPE_STRING);
				if(!Utilities.trim(fLabelCell.getStringCellValue()).equalsIgnoreCase("")){
					t.setIdentifiedBy(fLabelCell.getStringCellValue());
				}
				
				fLabelCell = row.getCell(DATA_COL, Row.CREATE_NULL_AS_BLANK);
				fLabelCell.setCellType(Cell.CELL_TYPE_STRING);
				t.setData(fLabelCell.getStringCellValue());
				
				fLabelCell = row.getCell(PAGE_COL, Row.CREATE_NULL_AS_BLANK);
				fLabelCell.setCellType(Cell.CELL_TYPE_STRING);
				logger.info("Found a record to write on page {}", fLabelCell.getStringCellValue());
				map.put(fLabelCell.getStringCellValue(), t);
			}
			
			logger.info("Getting all screens");
			int sequence=1;
			for(String key:map.keySet()){
				if(key.equalsIgnoreCase("")){
					continue;
				}
				for(TestObject t:map.get(key)){
					AssistedLearningRecord record = new AssistedLearningRecord();
					record.setAppId(appId);
					record.setFunctionCode(functionCode);
					record.setSequence(sequence);
					record.setPageAreaName(key.trim());
					record.setFieldName(t.getLabel().trim());
					if(t.getIdentifiedBy() != null && !t.getIdentifiedBy().trim().equalsIgnoreCase("")){
						record.setIdentifiedBy(t.getIdentifiedBy().trim());
					}
					record.setData(t.getData().trim());
					results.add(record);
					sequence++;
				}
			}	
		}catch(Exception e){
			logger.error("An internal error occurred", e);
			throw new AssistedLearningException("Could not import data from spreadsheet due to an internal error", e);
		}
		return results;
	}
	
	
	
	public static Multimap<String, TestObject> getAssistedLearningData(String moduleCode, int appId) throws AssistedLearningException{
		Multimap<String, TestObject> map = LinkedListMultimap.create();
		AssistedLearningHandler handler = new AssistedLearningHandler();
		try {
			 String screenName = "";
			 List<AssistedLearningRecord> records = handler.getAssistedLearningRecords(appId, moduleCode);
			 for(AssistedLearningRecord record : records){
				 screenName = record.getPageAreaName();
				
				 TestObject t = new TestObject();
				 t.setLabel(record.getFieldName());
				 t.setData(record.getData());
				 t.setIdentifiedBy(record.getIdentifiedBy());
				 map.put(screenName, t);
			 }
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			throw new AssistedLearningException("Could not get assisted learning data for " + moduleCode);
		}
		return map;
	}
	
	public static String getAssistedLearningDataXMLFormat(Aut aut, String functionCode) throws AssistedLearningException, ParserConfigurationException{
		Multimap<String, TestObject> map = getAssistedLearningData(functionCode, aut.getId());
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		docFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		docFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
		String finalXML = "";
		try {
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			Element rootElement = doc.createElement("TJNLEARNING");
			doc.appendChild(rootElement);
			
			Element app = doc.createElement("APP");
			app.appendChild(doc.createTextNode(aut.getName()));
			rootElement.appendChild(app);
			
			Element function = doc.createElement("FUNCTION");
			function.appendChild(doc.createTextNode(functionCode));
			rootElement.appendChild(function);
			
			for(String key:map.keySet()){
				if(key.equalsIgnoreCase("")){
					continue;
				}
				Element screen = doc.createElement("SCREEN");
				screen.setAttribute("name", key);
				
				for(TestObject t:map.get(key)){
					Element field = doc.createElement("FIELD");
					String label = replaceXMLSpecialCharacters(t.getLabel());
					field.setAttribute("label", label);
					field.setAttribute("value", t.getData());
					if(t.getIdentifiedBy() != null && !t.getIdentifiedBy().equalsIgnoreCase("")){
						field.setAttribute("idtype",t.getIdentifiedBy());
					}
					
					screen.appendChild(field);
				}
				rootElement.appendChild(screen);
			}
			StringWriter writer = new StringWriter();
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(writer);
			transformer.transform(source, result);
			finalXML = writer.toString();
		} catch (ParserConfigurationException e) {
			
			logger.error("Unable to create Learning XML for {}", functionCode, e);
			throw new AssistedLearningException("Could not get Assisted Learning data for " + functionCode);
		} catch (TransformerException e) {
			
			logger.error("Unable to create Learning XML for {}", functionCode, e);
			throw new AssistedLearningException("Could not get Assisted Learning data for " + functionCode);
		}
		return finalXML; 
		
	}

	
	
	
	public static String replaceXMLSpecialCharacters(String value){
		String toReplace;
		toReplace = value;
		toReplace = value.replaceAll("&","&amp;");
		toReplace = value.replaceAll("<","&lt;");
		toReplace = value.replaceAll(">","&gt;");
		toReplace = value.replaceAll("\"","&quot;");
		toReplace = value.replaceAll("'","&apos;");

		return toReplace;
	}

}
