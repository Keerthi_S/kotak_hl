package com.ycs.tenjin.bridge.utils;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.annotations.TenjinMetadata;


public class AnnotationUtils {
	private static Logger logger = LoggerFactory
			.getLogger(AnnotationUtils.class);
	
	public static ArrayList<String> getXMLNodes(Class<?> class1) throws IllegalArgumentException, SecurityException, InvocationTargetException, NoSuchMethodException{
		ArrayList<String> map = new ArrayList<String>();
		Field[] fields = FieldUtils.getFieldsWithAnnotation(class1, TenjinMetadata.class);
		for (Field field : fields) {
			Annotation annotation = field.getAnnotation(TenjinMetadata.class);
				try {
					map.add((String)annotation.getClass().getMethod("node_name", null).invoke(annotation, null));
				} catch (IllegalAccessException e) {
					
					logger.error("Error ", e);
			} 
		}
		return map;
	}
	
	public static ArrayList<String> getDBColumns(Class<?> class1) throws IllegalArgumentException, SecurityException, InvocationTargetException, NoSuchMethodException{
		ArrayList<String> map = new ArrayList<String>();
		Field[] fields = FieldUtils.getFieldsWithAnnotation(class1, TenjinMetadata.class);
		for (Field field : fields) {
			Annotation annotation = field.getAnnotation(TenjinMetadata.class);
				try {
					map.add((String)annotation.getClass().getMethod("column_name", null).invoke(annotation, null));
				} catch (IllegalAccessException e) {
					
					logger.error("Error ", e);
			}
		}
		return map;
	}
	
	public static Map<String, String> getXMLNodesAndDBCols(Class<?> class1) throws IllegalArgumentException, SecurityException, InvocationTargetException, NoSuchMethodException{
		Map<String, String> map = new HashMap<String, String>();
		Field[] fields = FieldUtils.getFieldsWithAnnotation(class1, TenjinMetadata.class);
		for (Field field : fields) {
			Annotation annotation = field.getAnnotation(TenjinMetadata.class);
				try {
					map.put((String)annotation.getClass().getMethod("node_name", null).invoke(annotation, null), ((String)annotation.getClass().getMethod("column_name", null).invoke(annotation, null)).trim()+"|"+annotation.getClass().getMethod("db_type", null).invoke(annotation, null).toString());
				} catch (IllegalAccessException e) {
					
					logger.error("Error ", e);
			} 
		}
		System.out.println(map);
		return map;
	}
	
	public static Map<Method, String> getGettersWithDBColumn(Class<?> class1) throws IntrospectionException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Map<Method, String> getterMethods = new HashMap<Method, String>();
		Field[] fields = FieldUtils.getFieldsWithAnnotation(class1, TenjinMetadata.class);
		for (Field field : fields) {
			Annotation annotation = field.getAnnotation(TenjinMetadata.class);
			String columnName = ((String)annotation.getClass().getMethod("column_name", null).invoke(annotation, null));
			getterMethods.put(new PropertyDescriptor(field.getName(), class1).getReadMethod(), columnName);
		}
		return getterMethods;
	}
	
	public static Map<Method, String> getSettersWithDBColumn(Class<?> class1) throws IntrospectionException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Map<Method, String> setterMethods = new HashMap<Method, String>();
		Field[] fields = FieldUtils.getFieldsWithAnnotation(class1, TenjinMetadata.class);
		for (Field field : fields) {
			Annotation annotation = field.getAnnotation(TenjinMetadata.class);
			String columnName = ((String)annotation.getClass().getMethod("column_name", null).invoke(annotation, null));
			setterMethods.put(new PropertyDescriptor(field.getName(), class1).getWriteMethod(), columnName);
		}
		return setterMethods;
	}
	
	public static Map<Method, String> getGettersWithXMLNode(Class<?> class1) throws IntrospectionException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Map<Method, String> getterMethods = new HashMap<Method, String>();
		Field[] fields = FieldUtils.getFieldsWithAnnotation(class1, TenjinMetadata.class);
		for (Field field : fields) {
			Annotation annotation = field.getAnnotation(TenjinMetadata.class);
			String nodeName = ((String)annotation.getClass().getMethod("node_name", null).invoke(annotation, null));
			getterMethods.put(new PropertyDescriptor(field.getName(), class1).getReadMethod(), nodeName);
		}
		return getterMethods;
	}
	
	public static Map<Method, String> getSettersWithXMLNode(Class<?> class1) throws IntrospectionException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Map<Method, String> setterMethods = new HashMap<Method, String>();
		Field[] fields = FieldUtils.getFieldsWithAnnotation(class1, TenjinMetadata.class);
		for (Field field : fields) {
			Annotation annotation = field.getAnnotation(TenjinMetadata.class);
			String nodeName = ((String)annotation.getClass().getMethod("node_name", null).invoke(annotation, null));
			setterMethods.put(new PropertyDescriptor(field.getName(), class1).getWriteMethod(), nodeName);
		}
		return setterMethods;
	}
	
	public static Map<Method, String> getSettersWithXMLNodeAndDBColumn(Class<?> class1) throws IntrospectionException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Map<Method, String> setterMethods = new HashMap<Method, String>();
		Field[] fields = FieldUtils.getFieldsWithAnnotation(class1, TenjinMetadata.class);
		for (Field field : fields) {
			Annotation annotation = field.getAnnotation(TenjinMetadata.class);
			String nodeName = ((String)annotation.getClass().getMethod("node_name", null).invoke(annotation, null))+";"+((String)annotation.getClass().getMethod("column_name", null).invoke(annotation, null));
			setterMethods.put(new PropertyDescriptor(field.getName(), class1).getWriteMethod(), nodeName);
		}
		return setterMethods;
	}
	
	public static String getFunctionNodeName(Class<?> clazz) throws IllegalArgumentException, SecurityException, IntrospectionException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Map<Method, String> setterMethods = AnnotationUtils.getSettersWithXMLNodeAndDBColumn(clazz);
		String functionNodeName = "";
		for (Entry<Method, String> entrySet : setterMethods.entrySet()) {
			if(entrySet.getValue().contains("MODULE_CODE")){
				functionNodeName = entrySet.getValue().substring(0, entrySet.getValue().indexOf(";"));
			}
		}
		return functionNodeName;
	}
	
	public static String getApplicationNodeName(Class<?> clazz) throws IllegalArgumentException, SecurityException, IntrospectionException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Map<Method, String> setterMethods = AnnotationUtils.getSettersWithXMLNodeAndDBColumn(clazz);
		String applicationNodeName = "";
		for (Entry<Method, String> entrySet : setterMethods.entrySet()) {
			if(entrySet.getValue().contains("APP_NAME")){
				applicationNodeName = entrySet.getValue().substring(0, entrySet.getValue().indexOf(";"));
			}
		}
		return applicationNodeName;
	}
	
	public static String getFunctionNameNodeName(Class<?> clazz) throws IllegalArgumentException, SecurityException, IntrospectionException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Map<Method, String> setterMethods = AnnotationUtils.getSettersWithXMLNodeAndDBColumn(clazz);
		String applicationNodeName = "";
		for (Entry<Method, String> entrySet : setterMethods.entrySet()) {
			if(entrySet.getValue().contains("MODULE_NAME")){
				applicationNodeName = entrySet.getValue().substring(0, entrySet.getValue().indexOf(";"));
			}
		}
		return applicationNodeName;
	}
	
	}