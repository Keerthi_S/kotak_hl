/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  CacheUtils.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 07-Apr-2017           Sriram Sridharan          Newly Added For
* 16-06-2017			Sriram Sridharan		TENJINCG-187
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 13-12-2018			Sriram Sridharan		Changed logger message for run abort check
*/


package com.ycs.tenjin.bridge.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.pojo.ExecutorProgress;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.ObjectExistsException;

public class CacheUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(CacheUtils.class);
	
	public static void initializeRunCache() throws TenjinConfigurationException {
		
		String cacheName = TenjinConfiguration.getProperty("RUN_CACHE_NAME");
		
		try {
			CacheManager cacheManager = CacheManager.create();
			Cache cache = cacheManager.getCache(cacheName);
			
			if(cache == null) {
				logger.warn("Cache [{}] does not exist. Creating a new cache store");
								cacheManager.addCache(new Cache(cacheName,10000,true,true,7200,7200));
						}
			
			logger.info("Initialized Cache Store [{}]", cacheName);
		} catch (ObjectExistsException e) {
			
			logger.error("ERROR initializng cache store ", e);
		} catch (CacheException e) {
			
			logger.error("ERROR initializng cache store ", e);
		} catch (IllegalStateException e) {
			
			logger.error("ERROR initializng cache store ", e);
		} catch (ClassCastException e) {
			
			logger.error("ERROR initializng cache store ", e);
		}
	}
	
	public static void putObjectInRunCache(Object key, Object value) {
		Cache cache = null;
		try {
			cache = getCache();
		} catch (TenjinConfigurationException e) {
			logger.error("ERROR - Could not get cache store", e);
		}
		
		if(cache != null) {
			cache.put(new Element(key, value));
		}else{
			logger.warn("Cache [{}] does not exist.");
		}
	}
	
	public static void removeObjectInRunCache(Object key) {
		Cache cache = null;
		try {
			cache = getCache();
		} catch (TenjinConfigurationException e) {
			
			logger.error("ERROR - Could not get cache store", e);
		}
		
		if(cache != null) {
			cache.remove(key);
		}else{
			logger.warn("Cache [{}] does not exist.");
		}
	}
	
	public static Object getObjectFromRunCache(Object key) throws TenjinConfigurationException{
		Cache cache = getCache();
		
		if(cache != null) {
			Element element = cache.get(key);
			if(element != null) {
				return element.getObjectValue();
			}else {
				
				return null;
			}
		}else{ 
			
			return null;
		}
	}
	
	private static Cache getCache() throws TenjinConfigurationException{
		String cacheName = TenjinConfiguration.getProperty("RUN_CACHE_NAME");
		Cache cache = null;
		try {
			CacheManager cacheManager = CacheManager.create();
			cache = cacheManager.getCache(cacheName);			
		} catch (ObjectExistsException e) {
			
			logger.error("ERROR initializng cache store ", e);
		} catch (CacheException e) {
			
			logger.error("ERROR initializng cache store ", e);
		} catch (IllegalStateException e) {
			
			logger.error("ERROR initializng cache store ", e);
		} catch (ClassCastException e) {
			
			logger.error("ERROR initializng cache store ", e);
		}
		
		return cache;
	}
	
	public static boolean isRunAborted(int runId) {
	//	MDC.put("logFileName", "run_activity_check.log");
		logger.info("Checking if Run {} is aborted", runId);
		boolean active = false;
		try {
			ExecutorProgress p = (ExecutorProgress) getObjectFromRunCache(runId);
			if(p != null) {
				logger.info("Run [{}] is aborted? --> [{}]", runId, p.isRunAborted());
				return p.isRunAborted();
			}else{
				logger.warn("Run {} does not exist in cache store");
			}
		} catch (TenjinConfigurationException e) {
			
			logger.error("ERROR checking if run is active", e);
		}
		
		//MDC.remove("logFileName");
		return active;
	}
}
