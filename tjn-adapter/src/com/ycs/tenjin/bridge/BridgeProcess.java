/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  BridgeProcess.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 22-Sep-2016          Sriram Sridharan       	Newly Added For 
 * 16-06-2017			Sriram Sridharan		TENJINCG-187
 * 19-06-2017			Sriram Sridharan		TENJINCG-204
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 08-05-2018			Preeti					TENJINCG-625
 * 19-11-2018			Prem					TENJINCG-843
 * 30-11-2018           Padmavathi              TENJINCG-853
 * 24-06-2019           Padmavathi              for license
 * 18-11-2020			Ashiki					TENJINCG-1213
 */

package com.ycs.tenjin.bridge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.Oem;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.IterationHelper;
import com.ycs.tenjin.mail.TenjinJavaxMail;

public class BridgeProcess implements Runnable {

	public static final String LEARN = "Learn";
	public static final String EXECUTE = "Execute";
	public static final String EXTRACT = "Extract";
	public static final String COMPLETE = "Complete";
	public static final String NOT_STARTED = "Not Started";
	public static final String IN_PROGRESS = "Executing";
	public static final String ABORTED = "Aborted";
	public static final String ABORTING = "Aborting";
	private static final Logger logger = LoggerFactory
			.getLogger(BridgeProcess.class);
	
	public static final String QUEUED = "QUEUED";
	
	private Thread t;
	private String threadName;
	private Oem oemAdapter;
	private int runId;
	
	private int projectId;
	private String taskType;
	private  String adapterName;
	public BridgeProcess(String taskName, Oem oemAdapter) {
		this.threadName = taskName;
		this.oemAdapter = oemAdapter;
	}

	
	public BridgeProcess(String taskName, Oem oemAdapter, int runId,String taskType,int projectId ,String adapterName) {
		this.threadName = taskName;
		this.oemAdapter = oemAdapter;
		this.runId = runId;
		this.adapterName=adapterName;
		this.taskType=taskType;
		this.projectId=projectId;
	}
	
	public BridgeProcess(String taskName, int runId, int projectId, String taskType) {
		this.threadName = taskName;
		this.runId = runId;
		this.projectId = projectId;
		this.taskType = taskType;
	}

	@Override
	public void run() {

		MDC.put("logFileName", "Run_" + this.runId);
		try {
			if(this.taskType=="Mail")
				new TenjinJavaxMail().buildMailInformation(this.runId,this.projectId);
			else
				this.oemAdapter.execute();
		} catch (BridgeException e) {
			logger.error("ERROR in AdapterTask --> oemAdapter.execute() failed");
			logger.error(e.getMessage(), e);
			try {
				new IterationHelper().abortRun(this.runId, "","Execution terminated abnormally. Please contact Tenjin Support.");
			} catch (DatabaseException e1) {
				logger.error("ERROR aborting run from runnable", e1);
			}
		} finally {
			logger.info("Waiting to clear run cache...");
			if(this.taskType.equalsIgnoreCase("Execute") || this.taskType.equalsIgnoreCase("Execute (API)"))
			{
				
				try {
					IterationHelper helper = new IterationHelper();
					int pRunId = helper.getParent(runId);
					if (pRunId != 0) {
						helper.mergeFailedTransactions(runId, pRunId);
					}
					/*Added by Pushpalatha for TENJINCG-1106 starts*/
					int prjId=helper.getPrjId(runId);
					helper.persisteExecAudit(runId,prjId);
					/*Added by Pushpalatha for TENJINCG-1106 ends*/
				} catch (Exception e) {
					logger.error("failed to merge the run results with parent run results");
				}
				
				/*write a method in iteration helper persist exec audit---- hydrate run
				 * 
				 *  
				 *  */
				new TenjinJavaxMail().buildMailInformation(this.runId,this.projectId);
			}
			try {
				Thread.sleep(10000);
			} catch (Exception ignore) {
			}
			logger.info("Removing Run from Cache");
			CacheUtils.removeObjectInRunCache(this.runId);
			 
				logger.info("Done");
				
		 try {
			String replace = this.adapterName.replace("[","").replace("]","").replace(" ","");
			 int count;
			 List<String> myList = new ArrayList<String>(Arrays.asList(replace.split(",")));
			try {
				for (String string : myList) {
					 count=(int) CacheUtils.getObjectFromRunCache(string);
					 if(count<0) {
						 CacheUtils.putObjectInRunCache(string, count);
					 }
					 logger.info("Waiting to remove consumed adapter from Cache");
					 CacheUtils.putObjectInRunCache(string, count-1);
				}
				 logger.info("Suceessfully removed consumed adapter from Cache");
			} catch (TenjinConfigurationException e) {
				
				logger.error("Error ", e);
			}
		} catch (Exception e) {
			
			logger.error("Error ", e);
		}
		 
			MDC.remove("logFileName");
		}
	}

	public void start() {
		logger.info("Starting thread {}", this.threadName);
		if (t == null) {
			t = new Thread(this, this.threadName);
			t.start();
		}
	}

	public static final String APILEARN = "Learn (API)";
	public static final String APIEXECUTE = "Execute (API)";
	public static final String APIEXTRACT = "Extract (API)";
	/*Added by Ashiki for TENJINCG-1213 starts*/
	public static final String APICODELEARN = "Learn (APICODE)";
	/*Added by Ashiki for TENJINCG-1213 end*/

/*Added by Ashiki for TENJINCG-1275 starts*/
	public static final String MSGLEARN = "Learn (MSG)";
	public static final String MSGEXECUTE = "Execute (MSG)";
	public static final String MSGEXTRACT = "Extract (MSG)";

/*Added by Ashiki for TENJINCG-1275 end*/
}
