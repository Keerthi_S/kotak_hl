package com.ycs.tenjin.bridge.exceptions;

public class AssistedLearningException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AssistedLearningException(String message){
		super(message);
	}
	
	public AssistedLearningException(String message, Throwable cause){
		super(message, cause);
	}
}
