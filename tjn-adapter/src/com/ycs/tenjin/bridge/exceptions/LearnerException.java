package com.ycs.tenjin.bridge.exceptions;

public class LearnerException extends RuntimeException{
	private static final long serialVersionUID = 1L;

	public LearnerException(String message,Throwable cause){
		super(message,cause);
	}
	
	public LearnerException(String message){
		super(message);
	}
}
