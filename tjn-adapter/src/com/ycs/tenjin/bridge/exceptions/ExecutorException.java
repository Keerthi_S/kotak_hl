package com.ycs.tenjin.bridge.exceptions;

public class ExecutorException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ExecutorException(String message){
		super(message);
	}
	
	public ExecutorException(String message, Throwable cause){
		super(message, cause);
	}
	
}
