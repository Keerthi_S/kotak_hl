/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  CacheUtils.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION 
 * 09-13-2017			Sameer Gupta			Rewritten for New Adapter Spec
 */

package com.ycs.tenjin.bridge.remoteapiserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.utils.CacheUtils;

public class RemoteApiServerCache {

	private static final Logger logger = LoggerFactory
			.getLogger(RemoteApiServerCache.class);

	public static final String remote_api_server_request ="_remote_api_server_request";
	public static final String remote_api_server_response ="_remote_api_server_response";

	public static void putRequest(String clientIp, String value) {

	
		String key = clientIp + remote_api_server_request;
		CacheUtils.putObjectInRunCache(key, value);

	}

	public static void putResponse(String clientIp, String value) {

		logger.info("Add RemoteApiServer response for clientIp [{}]", clientIp);
		String key = clientIp + remote_api_server_response;
		CacheUtils.putObjectInRunCache(key, value);

	}

	public static String getRequest(String clientIp) {

		String key = clientIp + remote_api_server_request;
		String result = null;
		try {
			result = (String) CacheUtils.getObjectFromRunCache(key);
		} catch (TenjinConfigurationException e) {  
			logger.error("ERROR initializng cache store ", e);
		}
		return result;

	}

	public static String getResponse(String clientIp) {

		String key = clientIp + remote_api_server_response;
		String result = null;
		try {
			result = (String) CacheUtils.getObjectFromRunCache(key);
		} catch (TenjinConfigurationException e) { 
			logger.error("ERROR initializng cache store ", e);
		}
		return result;

	}

	public static void removeRequest(String clientIp) {

		logger.info("Remove RemoteApiServer request for clientIp [{}]", clientIp);
		String key = clientIp + remote_api_server_request;
		CacheUtils.removeObjectInRunCache(key); 
		
	}

	public static void removeResponse(String clientIp) {

		logger.info("Add RemoteApiServer response for clientIp [{}]", clientIp);
		String key = clientIp + remote_api_server_response;
		CacheUtils.removeObjectInRunCache(key); 
		
	}

}
