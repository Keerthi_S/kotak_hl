package com.ycs.tenjin.bridge.remoteapiserver;

import java.util.LinkedList;

public class RemoteApiServerRequest {

	private String functionName;
	private LinkedList<String> parameters = new LinkedList<String>();
	private Boolean requestAccepted = false;

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public LinkedList<String> getParameters() {
		return parameters;
	}

	public void setParameters(LinkedList<String> parameters) {
		this.parameters = parameters;
	}

	public void addParameters(String parameter) {
		this.parameters.add(parameter);
	}

	public Boolean getRequestAccepted() {
		return requestAccepted;
	}

	public void setRequestAccepted(Boolean requestAccepted) {
		this.requestAccepted = requestAccepted;
	}

}
