package com.ycs.tenjin.bridge.remoteapiserver;

import java.rmi.RemoteException;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.rmi.AutException;
import com.ycs.tenjin.rmi.RemoteTenjinInstance;

public class RemoteApiServerInstance implements RemoteTenjinInstance {

	private static final Logger logger = LoggerFactory
			.getLogger(RemoteApiServerInstance.class);

	protected String clientIp;
	RemoteApiServer objRemoteApiServer = new RemoteApiServer();

	public RemoteApiServerInstance(String clientIp) throws RemoteException {
		this.clientIp = clientIp;
	}

	public void launch(String adapter,String taskHandlerType, String url) throws RemoteException,
			AutException {

		logger.info("Processing launch request for clientIp [{}]",
				this.clientIp);
		LinkedList<String> parameters = new LinkedList<String>();
		parameters.add(adapter);
		parameters.add(taskHandlerType);
		parameters.add(url);

		objRemoteApiServer.processRequest(this.clientIp, "launch", parameters); 
		
	}

	public void login(String adapter,String taskHandlerType, String LoginName, String Password)
			throws RemoteException, AutException {

		logger.info("Processing launch request for clientIp [{}]",
				this.clientIp);
		LinkedList<String> parameters = new LinkedList<String>();
		parameters.add(adapter);
		parameters.add(taskHandlerType);
		parameters.add(LoginName);
		parameters.add(Password);

		objRemoteApiServer.processRequest(this.clientIp, "login", parameters); 
		
	}

	public void navigateToFunction(String adapter,String taskHandlerType, String FunctionCode,
			String FunctionName, String FunctionPath) throws RemoteException,
			AutException {

		logger.info("Processing launch request for clientIp [{}]",
				this.clientIp);
		LinkedList<String> parameters = new LinkedList<String>();
		parameters.add(adapter);
		parameters.add(taskHandlerType);
		parameters.add(FunctionCode);
		parameters.add(FunctionName);
		parameters.add(FunctionPath);

		objRemoteApiServer.processRequest(this.clientIp, "navigateToFunction", parameters); 
		
	}

	public void logout(String adapter,String taskHandlerType) throws RemoteException, AutException {

		logger.info("Processing launch request for clientIp [{}]",
				this.clientIp);
		LinkedList<String> parameters = new LinkedList<String>();
		parameters.add(adapter); 
		parameters.add(taskHandlerType);

		objRemoteApiServer.processRequest(this.clientIp, "logout", parameters); 
		
	

	}

	public void recover(String adapter,String taskHandlerType) throws RemoteException, AutException {

		logger.info("Processing launch request for clientIp [{}]",
				this.clientIp);
		LinkedList<String> parameters = new LinkedList<String>();
		parameters.add(adapter); 
		parameters.add(taskHandlerType);

		objRemoteApiServer.processRequest(this.clientIp, "recover", parameters); 
		
	

	}

	public void learn(String adapter, String functionCode, int runId,
			String appName, String functionPath) throws RemoteException,
			AutException {

		logger.info("Processing launch request for clientIp [{}]",
				this.clientIp);
		LinkedList<String> parameters = new LinkedList<String>();
		parameters.add(adapter);
		parameters.add(functionCode);
		parameters.add(Integer.toString(runId));
		parameters.add(appName);
		parameters.add(functionPath);

		objRemoteApiServer.processRequest(this.clientIp, "learn", parameters); 
		
	

	}

	public void execute(String adapter, String iterationJsonData, int runId,
			int iteration, String TdGid, int RecordId, String operation,
			String testStepType, String functionMenu, int screenshotOption,String expectedResult)
			throws RemoteException, AutException {

		logger.info("Processing launch request for clientIp [{}]",
				this.clientIp);
		LinkedList<String> parameters = new LinkedList<String>();
		parameters.add(adapter);
		parameters.add(iterationJsonData);
		parameters.add(Integer.toString(runId));
		parameters.add(Integer.toString(iteration));
		parameters.add(TdGid);
		parameters.add(Integer.toString(RecordId));
		parameters.add(operation);
		parameters.add(testStepType);
		parameters.add(functionMenu);
		parameters.add(Integer.toString(screenshotOption)); 
		parameters.add(expectedResult);

		objRemoteApiServer.processRequest(this.clientIp, "execute", parameters); 
		
	

	}

	public void extract(String adapter, String jsonData, int runId)
			throws RemoteException, AutException {

		logger.info("Processing launch request for clientIp [{}]",
				this.clientIp);
		LinkedList<String> parameters = new LinkedList<String>();
		parameters.add(adapter);
		parameters.add(jsonData);
		parameters.add(Integer.toString(runId));

		objRemoteApiServer.processRequest(this.clientIp, "extract", parameters); 
		
	

	}

}
