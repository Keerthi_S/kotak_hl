package com.ycs.tenjin.bridge.remoteapiserver;

import java.rmi.RemoteException;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ycs.tenjin.rmi.AutException;

public class RemoteApiServer {

	private static final Logger logger = LoggerFactory
			.getLogger(RemoteApiServer.class);

	private int threadSleepTime = 1000;
	private int maxTriesForRequestAcceptance = 70;
	
	private int maxTriesForResponseAcceptance = 3600;
	
	public RemoteApiServer() {  
	}
	
	public RemoteApiServer(int threadSleepTime, int maxTriesForRequestAcceptance,
			int maxTriesForResponseAcceptance) {
		this.threadSleepTime = threadSleepTime;
		this.maxTriesForRequestAcceptance = maxTriesForRequestAcceptance;
		this.maxTriesForResponseAcceptance = maxTriesForResponseAcceptance;

	}

	public String processRequest(String clientIp, String functionName,
			LinkedList<String> parameters) throws RemoteException, AutException {

		String returnValue = null;
		// if request is already there throw exception
		if (RemoteApiServerCache.getRequest(clientIp) != null) {
			throw new RemoteException(
					"Tenjin Client is buzzy.");
		} else {

			try {
				sendRequest(clientIp, functionName, parameters);

				waitForMessageAcceptanceByClient(clientIp);

				waitForResponse(clientIp);

				returnValue = processResponse(clientIp);
			} finally {
				RemoteApiServerCache.removeRequest(clientIp);
				RemoteApiServerCache.removeResponse(clientIp);
			}

		}
		return returnValue;
	}

	private static void sendRequest(String clientIp, String functionName,
			LinkedList<String> parameters) {
		RemoteApiServerRequest objRemoteApiServerRequest = new RemoteApiServerRequest();
		objRemoteApiServerRequest.setFunctionName(functionName);
		objRemoteApiServerRequest.setParameters(parameters);

		Gson gson = new GsonBuilder().create();
		String value = gson.toJson(objRemoteApiServerRequest,
				objRemoteApiServerRequest.getClass());

		RemoteApiServerCache.putRequest(clientIp, value);
		RemoteApiServerCache.removeResponse(clientIp);
	}

	private void waitForMessageAcceptanceByClient(String clientIp)
			throws RemoteException {
		Gson gson;
		String myRequest;
		boolean requestAccepted = false;
		for (int i = 0; i < this.maxTriesForRequestAcceptance; i++) {
			myRequest = RemoteApiServerCache.getRequest(clientIp);

			gson = new Gson();
			RemoteApiServerRequest obj1RemoteApiServerRequest = gson.fromJson(
					myRequest, RemoteApiServerRequest.class);

			if (obj1RemoteApiServerRequest.getRequestAccepted()) {
				requestAccepted = true;
				break;
			}
			try {
				Thread.sleep(threadSleepTime);
			} catch (InterruptedException e) {
				logger.info("Thread sleep issue while processing request [{}]",
						clientIp);
			}
		}
		if (!requestAccepted) {
			RemoteApiServerCache.removeRequest(clientIp);
			throw new RemoteException("Tenjin Client is not active.");
		}
	}

	private void waitForResponse(String clientIp) throws RemoteException {
		String myResponse;
		boolean responseReceived = false;
		for (int i = 0; i < maxTriesForResponseAcceptance; i++) {
			myResponse = RemoteApiServerCache.getResponse(clientIp);

			if (myResponse != null) {
				responseReceived = true;
				break;
			}
			try {
				Thread.sleep(threadSleepTime);
			} catch (InterruptedException e) {
				logger.info("Thread sleep issue while processing request [{}]",
						clientIp);
			}

		}
		if (!responseReceived) {
			RemoteApiServerCache.removeRequest(clientIp);
			throw new RemoteException(
					"Tenjin Client is taking too long to process the request.");
		}
	}

	private String processResponse(String clientIp) throws RemoteException,
			AutException {
		String jsonResponse = RemoteApiServerCache.getResponse(clientIp);

		JsonElement jsonElementMessage = new JsonParser().parse(jsonResponse);
		JsonObject jsonObjectMessage = jsonElementMessage.getAsJsonObject();

		String messageType = jsonObjectMessage.get("messageType").getAsString();
		String message = jsonObjectMessage.get("message").getAsString();

		String returnValue = "";
		if (messageType.equalsIgnoreCase("RemoteException")) {
			throw new RemoteException(message);
		} else if (messageType.equalsIgnoreCase("AutException")) {
			throw new AutException(message);
		} else if (messageType.equalsIgnoreCase("ReturnValue")) {
			returnValue = message;
		}

		return returnValue;
	}

}
