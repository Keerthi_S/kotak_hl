package com.ycs.tenjin.bridge.oem.gui.taskhandler;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.videoRecord.Recorderer;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.ValidationDetails;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.exceptions.ApplicationUnresponsiveException;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.DriverException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.exceptions.RunAbortedException;
import com.ycs.tenjin.bridge.exceptions.TestDataException;
import com.ycs.tenjin.bridge.oem.TaskHandler;
import com.ycs.tenjin.bridge.oem.api.ApiApplication;
import com.ycs.tenjin.bridge.oem.api.ApiApplicationFactory;
import com.ycs.tenjin.bridge.oem.api.datahandler.ApiResponseTestDataHandlerImpl;
import com.ycs.tenjin.bridge.oem.api.datahandler.ApiTestDataHandlerImpl;
import com.ycs.tenjin.bridge.oem.api.generic.ApiResponseValidator;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationAbstract;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationFactory;
import com.ycs.tenjin.bridge.oem.gui.datahandler.GuiDataHandler;
import com.ycs.tenjin.bridge.oem.gui.datahandler.GuiDataHandlerImpl;
import com.ycs.tenjin.bridge.pojo.ExecutorProgress;
import com.ycs.tenjin.bridge.pojo.TaskManifest;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Representation;
import com.ycs.tenjin.bridge.pojo.project.UIValidation;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.bridge.pojo.run.UIValidationResult;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.bridge.utils.MessageUtil;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.IterationHelper;
import com.ycs.tenjin.db.RunDefectHelper;
import com.ycs.tenjin.db.UIValidationHelper;
import com.ycs.tenjin.defect.RunDefectProcessor;
import com.ycs.tenjin.unstructure.abstraction.UnstructureValidationAbstract;
import com.ycs.tenjin.unstructure.util.ValidationFactory;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.PatternMatch;
import com.ycs.tenjin.util.Utilities;

public class CommonExecutionTaskHandlerImpl implements TaskHandler{
	protected static final Logger logger = LoggerFactory
			.getLogger(CommonExecutionTaskHandlerImpl.class);
	private TaskManifest manifest;
	protected int runId;
	protected Map<String, Map<String, Aut>> autMap;
	protected List<ExecutionStep> steps;
	protected String targetIp;
	protected int targetPort;
	protected int screenshotOption;
	protected int fieldTimeout;
	protected String browser;
	protected Map<String, String> testDataPaths;
	protected Map<Integer, List<String>> tdUidMap;
	protected String videoCaptureFlag;
	protected String videoOutputFolder;
	protected boolean videoCaptureInProgress;
	protected GuiApplicationAbstract oApplication = null;
	private ExecutorProgress progress = new ExecutorProgress();
	private int completedTransactions = 0;
	private int totalTransactions = 0;
	private Map<Integer,String> execStatusMap=new HashMap<Integer,String>();
	private List<String> execStatusList=new ArrayList<String>();
	private int projectId;
	private String userId;
	private long startTimeinMillis;
	private Recorderer recorder;
	private int AUT_RETRY_LIMIT;
	private int AUT_RETRY_INTERVAL;
	protected String deviceId;
	private Map<Integer,String> passedStepsStatusMap;
	
	public CommonExecutionTaskHandlerImpl(TaskManifest manifest) {
		this.manifest = manifest;
	}
	
	void initialize(){
		this.runId = this.manifest.getRunId();
		this.autMap = this.manifest.getAutMap();
		this.steps = this.manifest.getSteps();
		this.targetIp = this.manifest.getClientIp();
		this.targetPort = this.manifest.getClientPort();
		this.screenshotOption = this.manifest.getScreenshotOption();
		this.fieldTimeout = this.manifest.getFieldTimeout();
		this.browser = this.manifest.getBrowser();
		this.testDataPaths = this.manifest.getTestDataPaths();
		this.tdUidMap = this.manifest.getTdUidMap();
		this.videoCaptureFlag = this.manifest.getVideoCaptureFlag();
		this.projectId = this.manifest.getProjectId();
		this.userId = this.manifest.getUserId();
		this.deviceId = this.manifest.getDeviceId();
		
		try {
			AUT_RETRY_INTERVAL = Integer.parseInt(TenjinConfiguration.getProperty("AUT_RETRY_INTERVAL"));
		} catch (NumberFormatException e) {
			logger.error("Invalid AUT_RETRY_INVERVAL specified in config file. Defaulting to 10 seconds");
			AUT_RETRY_INTERVAL = 10;
		} catch (TenjinConfigurationException e) {
			logger.error(e.getMessage());
			AUT_RETRY_INTERVAL = 10;
		}

		try {
			AUT_RETRY_LIMIT = Integer.parseInt(TenjinConfiguration.getProperty("AUT_RETRY_LIMIT"));
		} catch (NumberFormatException e) {
			logger.error("Invalid AUT_RETRY_LIMIT specified in config file. Defaulting to 5");
			AUT_RETRY_LIMIT = 5;
		} catch (TenjinConfigurationException e) {
			logger.error(e.getMessage());
			AUT_RETRY_LIMIT = 5;
		}
	}
	
	@Override
	public void execute() {
		initialize();
		
		logger.info("Beginning Execution of Tenjin Run [{}]", this.runId);
		this.startTimeinMillis = System.currentTimeMillis();
		if(this.tdUidMap != null) {
			for(Integer stepRecordId : this.tdUidMap.keySet()) {
				List<String> tdUids = this.tdUidMap.get(stepRecordId);
				if(tdUids != null ) {
					for (@SuppressWarnings("unused") String tdUid:tdUids) {
						this.totalTransactions++;
					}
				}
			}
		}
		
		logger.info("Total Transactions in this run --> [{}]", this.totalTransactions);
		this.progress.setTotalTransactions(this.totalTransactions);
		
		logger.debug("Checking if thread for run {} is alive", this.runId);
		if (CacheUtils.isRunAborted(this.runId)) {
			logger.debug("Thread for Run {} is interrupted.", this.runId);
			this.abort();
			return;
		}
		
		logger.debug("Thread for Run {} is still alive", this.runId);
		logger.info("Beginning Execution of Tenjin Run [{}]", this.runId);
		String currentLoginType = "NA";
		boolean loggedOnToAut = false;
		IterationHelper helper = new IterationHelper();
		String currentAut = "";
		if (this.steps == null) {
			logger.error("ERROR --> Steps is null. Nothing to execute. Execution is terminated");
			return;
		}
		
		logger.info("Checking if video capture is required");
		if (this.videoCaptureFlag.equalsIgnoreCase("Y")) {
			logger.info("Creating media directories...");
			try {
				String mediaPath = TenjinConfiguration.getProperty("MEDIA_REPO_PATH");
				File file = new File(mediaPath);
				if (!file.exists()) {
					logger.debug("Media Repository does not exist. Creating path [{}]",mediaPath);
					file.mkdir();
				}

				logger.debug("Creating directory for Run [{}]", this.runId);
				file = new File(mediaPath + "\\" + this.runId);
				file.mkdir();
				this.videoOutputFolder = mediaPath + "\\" + this.runId;
			} catch (Exception e) {
				logger.error("ERROR creating Media directory. Video will not be captured",e);
				this.videoCaptureFlag = "N";
			}
		}
		
		logger.debug("Checking if thread for run {} is alive", this.runId);
		if (CacheUtils.isRunAborted(this.runId)) {
			logger.debug("Thread for Run {} is interrupted.", this.runId);
			this.abort();
			return;
		}
		
		logger.debug("Thread for Run {} is still alive", this.runId);
		logger.info("Updating Run Start");
		try {
			helper.beginRun(this.runId);
		} catch (Exception ignore) {}
		
		int stepNum=0;
		for (ExecutionStep step : this.steps) {
			stepNum++;
			boolean isGUIStep = step.getTxnMode().equalsIgnoreCase("gui");
			logger.debug("Checking if thread for run {} is alive", this.runId);
			if (CacheUtils.isRunAborted(this.runId)) {
				logger.debug("Thread for Run {} is interrupted.", this.runId);
				this.abort();
				return;
			}
			
			logger.debug("Thread for Run {} is still alive", this.runId);
			if(step.getDependencyRule()!=null && !step.getDependencyRule().equalsIgnoreCase("-1")&& step.getDependentStepId()!=0 ){
				if(steps.size()>1 && stepNum!=1) {
					String previousStepStatus=execStatusMap.get(step.getDependentStepId());
				if(previousStepStatus==null && this.passedStepsStatusMap!=null){
					 previousStepStatus=this.passedStepsStatusMap.get(step.getDependentStepId());
				 }
				if(previousStepStatus!=null &&!previousStepStatus.equalsIgnoreCase(step.getDependencyRule())){
					try {
						helper.updateRunStatusForAllTransactions(this.runId,step.getRecordId(), "E", "Error due to not fulfilled dependency rules");
						execStatusMap.put(step.getRecordId(), "Error");
						previousStepStatus="Error";
					}catch (DatabaseException ignore) {	}
						continue;
					}
				}
			}
			
			logger.info("Begin execution of step --> [{}], Record ID [{}]", step.getId(), step.getRecordId());
			boolean autLoginRequired = false;

			logger.info("Loading application information for step [{}]", step.getId());
			Map<String, Aut> autMap = this.autMap.get(step.getAppName());
			Aut aut = null;
			Api api = null;
			ApiApplication bridge = null;
			ApiOperation operation = null;
			if(isGUIStep){
				if (loggedOnToAut) {
					boolean logOffRequired = false;
					if (!currentLoginType.equalsIgnoreCase(step.getAutLoginType())) {
						logger.info(
								"Tenjin needs to logoff from [{}] as [{}] and login again as [{}]",
								currentAut, currentLoginType,
								step.getAutLoginType());
						logOffRequired = true;
					} else if (!currentAut.equalsIgnoreCase(step.getAppName())) {
						logger.info(
								"Tenjin needs to logout from [{}] and login to [{}] as [{}]",
								currentAut, step.getAppName(),
								step.getAutLoginType());
						logOffRequired = true;
					}
	
					if (logOffRequired) {
						logger.info("Logging out of [{}]", currentAut);
						if (oApplication != null) {
							try {
								oApplication.logout();
							} catch (AutException | DriverException e) {
								logger.error("Could not logoff the AUT");
							}
	
							try {
								oApplication.destroy();
							} catch (Exception ignore) {}
						}
						autLoginRequired = true;
					}
				} else {
					autLoginRequired = true;
				}
				aut = autMap.get(step.getAutLoginType());
			}else{
				aut = autMap.get(step.getAutLoginType());
				step.setAut(aut);
				try {
					ApiHelper apiHelper = new ApiHelper();
					api = apiHelper.hydrateApi(aut.getId(), step.getApiCode());
					step.setApi(api);
					operation = apiHelper.hydrateApiOperation(aut.getId(), step.getApiCode(), step.getOperation());
					step.setApiOperation(operation);
				} catch (DatabaseException e) {
					logger.error("ERROR loading API Information", e);
					try {
						logger.info("Setting results for all transactions in Step [{}] - Record ID[{}] to E", step.getId(), step.getRecordId());
						helper.updateRunStatusForAllTransactions(this.runId, step.getRecordId(), "E", "Could not load information about API " + step.getApiCode() + ".");
						execStatusList.add("Error");
					} catch (DatabaseException e1) {
						logger.error(e1.getMessage());
					}
				}
				
				if(api == null) {
					logger.error("Invalid API Code [{}] in Application [{}]", step.getApiCode(), aut.getName());
					try {
						logger.info("Setting results for all transactions in Step [{}] - Record ID[{}] to E", step.getId(), step.getRecordId());
						helper.updateRunStatusForAllTransactions(this.runId, step.getRecordId(), "E", "Invalid API [" + step.getApiCode() + "].");
						execStatusList.add("Error");
					} catch (DatabaseException e1) {
						logger.error(e1.getMessage());
					}
				}
				
				logger.info("Loading executor for [{}]", api.getType());
				logger.info("Initializing API Adapter for API [{}]", api.getCode());
				try {
					bridge = ApiApplicationFactory.getExecutor(api.getType(), this.runId, step);
				} catch (BridgeException e) {
					logger.error("Executor invocation failed for step [{}] - Record ID [{}]", step.getId(), step.getRecordId());
					try {
						logger.info("Setting results for all transactions in Step [{}] - Record ID[{}] to E", step.getId(), step.getRecordId());
						helper.updateRunStatusForAllTransactions(this.runId, step.getRecordId(), "E", e.getMessage());
						execStatusList.add("Error");
					} catch (DatabaseException e1) {
						logger.error(e1.getMessage());
					}
					continue;
				}
			}
			
			logger.debug("Checking if thread for run {} is alive", this.runId);
			if (CacheUtils.isRunAborted(this.runId)) {
				logger.debug("Thread for Run {} is interrupted.", this.runId);
				this.abort();
				return;
			}
			
			if (step.getAutLoginType() != null && step.getAutLoginType().trim().toLowerCase().startsWith("dynamic")) {
				try {
					String dynamicUserVariable = "";
					try {
						if (step.getAutLoginType().contains("_")) {
							dynamicUserVariable = step.getAutLoginType().split("_")[1];
						} else {
							dynamicUserVariable = step.getAutLoginType().replace("dynamic", "");
						}
					} catch (Exception e1) {
						logger.debug("ERROR while fetching the User details before");
					}
					String dynamicUsername = "";
					String dynamicUserPassword = "";
					/*Added by paneendra for VAPT fix starts*/
					CryptoUtilities cryptoUtilities=new CryptoUtilities();
					/*Added by paneendra for VAPT fix ends*/
					try {
						Connection projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
						dynamicUsername = new IterationHelper().getDynamicUser(projConn, dynamicUserVariable, runId);
					} catch (Exception e) {
						logger.debug("ERROR while fetching the User details for step gid for dynamic user {}",step.getTdGid());
					}
					try {
						dynamicUserPassword = new IterationHelper().getDynamicUserPassword(step.getAppId(),	dynamicUsername, this.userId);
						try {
							/*Modified by paneendra for VAPT fix starts*/
							/*dynamicUserPassword = new Crypto().decrypt(dynamicUserPassword
									.split("!#!")[1], decodeHex(dynamicUserPassword
									.split("!#!")[0].toCharArray()));*/
							
						 dynamicUserPassword = cryptoUtilities.decrypt(dynamicUserPassword);
						 /*Modified by paneendra for VAPT fix ends*/
						} catch (Exception e) {
							logger.error("Error ", e);
						}
					} catch (DatabaseException e) {
						logger.debug("ERROR while fetching the User details");
					} catch (Exception er) {
						logger.info("ERROR while fetching the Dynamic User pwd after" + er.getMessage());
					}
					aut.setLoginName(dynamicUsername);
					aut.setPassword(dynamicUserPassword);
				} catch (Exception e) {
					logger.debug("ERROR while fetching the User details for dynamic");
				}
			}

			logger.debug("Thread for Run {} is still alive", this.runId);
			if(isGUIStep){
				if (autLoginRequired) {
					loggedOnToAut = false;
					currentLoginType = "";
					currentAut = "";

					String aBrowser = "";
					if (Utilities.trim(browser).equalsIgnoreCase("")) {
						browser = "APPDEFAULT";
					}
					if ("APPDEFAULT".equalsIgnoreCase(Utilities.trim(browser))) {
						aBrowser = autMap.get(step.getAutLoginType()).getBrowser();
					} else {
						aBrowser = this.browser;
					}

					try {
						logger.info("Creating Application instance for [{}]", step.getAppName());
						oApplication = GuiApplicationFactory.getApplication(aut.getAdapterPackage(), aut, this.targetIp, this.targetPort, aBrowser,this.deviceId);
						this.launchAndLogin(oApplication, aut, aBrowser, step);
						loggedOnToAut = true;
						currentLoginType = step.getAutLoginType();
						currentAut = step.getAppName();
					} catch (DriverException e) {
						logger.error("Driver invocation failed for step [{}] - Record ID [{}]",	step.getId(), step.getRecordId());
						try {
							logger.info("Setting results for all transactions in Step [{}] - Record ID[{}] to E", step.getId(), step.getRecordId());
							helper.updateRunStatusForAllTransactions(this.runId, step.getRecordId(), "E", e.getMessage());
							execStatusList.add("Error");
						} catch (DatabaseException e1) {
							logger.error(e1.getMessage());
						}

						try {
							logger.info("Destroying Driver");
							oApplication.destroy();
						} catch (Exception ignore) {}
						continue;
					} catch (BridgeException e) {
						logger.error("Adapter invocation failed for step [{}] - Record ID [{}]",step.getId(), step.getRecordId());
						try {
							logger.info("Setting results for all transactions in Step [{}] - Record ID[{}] to E",step.getId(), step.getRecordId());
							helper.updateRunStatusForAllTransactions(this.runId, step.getRecordId(), "E", e.getMessage());
							execStatusList.add("Error");
						} catch (DatabaseException e1) {
							logger.error(e1.getMessage());
						}
						try {
							logger.info("Destroying Driver");
							oApplication.destroy();
						} catch (Exception ignore) {}
						continue;
					} catch (AutException e) {
						logger.error("Login to [{}] as [{}] failed for step [{}] - Record ID [{}]", step.getAppName(), step.getAutLoginType(),
								step.getId(), step.getRecordId());
						try {
							logger.info("Setting results for all transactions in Step [{}] - Record ID[{}] to E", step.getId(), step.getRecordId());
							helper.updateRunStatusForAllTransactions(this.runId, step.getRecordId(), "F", e.getMessage());
							execStatusList.add("Fail");
						} catch (DatabaseException e1) {
							logger.error(e1.getMessage());
						}
						try {
							logger.info("Destroying Driver");
							oApplication.destroy();
						} catch (Exception ignore) {}
						continue;
					}
				}
			}

			logger.debug("Checking if thread for run {} is alive", this.runId);
			if (CacheUtils.isRunAborted(this.runId)) {
				logger.debug("Thread for Run {} is interrupted.", this.runId);
				this.abort();
				return;
			}

			logger.debug("Thread for Run {} is still alive", this.runId);
			
			if(isGUIStep)
				this.processGUIStepIteration(step, aut);
			else
				this.processAPIStepIteration(step, api, aut, operation, bridge);
			
			String execStatus="";
			 try {
				 if((execStatusList.get(0))==null){
					 execStatus=helper.getStepStatus(this.runId, step.getRecordId());
				 }else{
					 execStatus=helper.processExecStepStatus(execStatusList);   
				 }
			 } catch (DatabaseException e) {
                logger.error("Error ", e);
            }catch (NullPointerException e) {
                logger.error("Error ", e);
            }
           execStatusMap.put(step.getRecordId(), execStatus);
           execStatusList.clear();
		}
		
		logger.info("All steps executed. Logging off from application");
		logger.debug("Checking if thread for run {} is alive", this.runId);
		if (CacheUtils.isRunAborted(this.runId)) {
			logger.debug("Thread for Run {} is interrupted.", this.runId);
			this.abort();
			return;
		}

		logger.debug("Thread for Run {} is still alive", this.runId);
		if (oApplication != null) {
			try {
				oApplication.logout();
			} catch (AutException | DriverException e) {
				logger.error("Could not logoff the AUT");
			}

			try {
				oApplication.destroy();
			} catch (Exception ignore) {}
		}
		logger.info("Execution of Run ID [{}] Completed", this.runId);
		logger.info("checking for unstructured data validations");
		try {
			//*Added by Prem form TENJINCG-1181 starts*/
			ArrayList<ValidationResult> validationresult = new ArrayList<ValidationResult>();
			IterationHelper Ithelper = new IterationHelper();
			for (ExecutionStep step : this.steps) {
				if(step.getValidationdetails()!=null && step.getValidationdetails().size()!=0) {
					String filepath=this.getTransanctionOutputfile();
					int ExstepNum=0;
					for(ValidationDetails validation:step.getValidationdetails()) {
						ValidationResult vl = new ValidationResult();
						ExstepNum++;
						validation.setBaseFilePath(filepath);
						UnstructureValidationAbstract unstructureValidation = (UnstructureValidationAbstract) ValidationFactory.getValidationObject(validation);
						ValidationDetails finalValidationResult =unstructureValidation.validate();
												
						vl.setRunId(this.runId);
						vl.setTestStepRecordId(finalValidationResult.getTestStepId());
						vl.setResValId(0);
						vl.setIteration(1);
						if(finalValidationResult.getFileType().equals("pdf")){
							if((finalValidationResult.getPageNumber())!=0){
								vl.setPage(String.valueOf(finalValidationResult.getPageNumber()));
							}
							else{
								vl.setPage("N/A");
							}
						}
						else if(finalValidationResult.getFileType().equals("excel")){
							vl.setPage(finalValidationResult.getSheetName());
						}
						
						vl.setField(finalValidationResult.getValidationName());
						vl.setExpectedValue(finalValidationResult.getSearchText());
						vl.setActualValue(finalValidationResult.getActualValue());
						if((boolean) finalValidationResult.getResult()){
							vl.setStatus("pass");
						}
						else{vl.setStatus("Fail");}
						vl.setDetailRecordNo(String.valueOf(ExstepNum));
						validationresult.add(vl);
					}
					
					Ithelper.persistValidationResults(validationresult, this.runId, step.getRecordId(), 1);
				}
				//*Added by Prem form TENJINCG-1181 Ends*/
			}
			}
			catch(Exception e) {
				
			}
		logger.info("Generating Defects");
		RunDefectHelper rHelper = new RunDefectHelper();
		RunDefectProcessor rProcessor = new RunDefectProcessor();
		try {
			List<String> uniqueMessages = rHelper.getDistinctTransactionFailureMessages(this.runId);
			Map<String, List<StepIterationResult>> transactionFailures = new HashMap<String, List<StepIterationResult>>();
			
			for(String message:uniqueMessages){				
				if(!message.equalsIgnoreCase("Validation Failed")){
					List<StepIterationResult> txnsFailedWithMessage = rHelper.getTransactionsFailedWithMessage(this.runId, message);
					transactionFailures.put(message, txnsFailedWithMessage);
				}
			}
			rProcessor.createTransactionFailureDefects(this.runId,this.projectId, this.userId, transactionFailures);
			logger.info("Getting validation failures");
			List<StepIterationResult> validationFailures = rHelper.getTransactionsWithValidationFailures(this.runId);
			if (validationFailures != null && validationFailures.size() > 0) {
				rProcessor.createValidationFailureDefects(this.runId,this.projectId, this.userId, validationFailures);
			}

			logger.info("Getting UI Validation Failure defects");
			for (ExecutionStep step : this.steps) {
				if (step.getValidationType() == null || !step.getValidationType().equalsIgnoreCase("ui")) {
					continue;
				}
				Map<String, Map<String, List<UIValidation>>> uiValResultsMap = this.getUIValidationResultsForStep(this.runId,step.getRecordId());

				if (uiValResultsMap == null) {
					continue;
				}
				logger.info("Generating UI Validation Failure defects for step {}", step.getId());
				for (String validationType : uiValResultsMap.keySet()) {
					for (String pageName : uiValResultsMap.get(validationType).keySet()) {
						List<UIValidation> validations = uiValResultsMap.get(validationType).get(pageName);
						List<UIValidationResult> failures = new ArrayList<UIValidationResult>();
						for (UIValidation val : validations) {
							for (UIValidationResult result : val.getResults()) {
								if (!result.getStatus().equalsIgnoreCase(TenjinConstants.ACTION_RESULT_SUCCESS)) {
									failures.add(result);
								}
							}
						}
						logger.info("Generating UI Validation Failure defects for step {}, Validation type {}, Page {}", step.getId(), validationType, pageName);
						rProcessor.createUIValidationFailureDefects(this.runId, this.projectId, step.getRecordId(),	this.userId, validationType, pageName,failures);
					}
				}
			}
		} catch (DatabaseException e) {
			logger.error("ERROR while generating defects for run [{}]",	this.runId, e);
		}

		logger.info("Updating Run End");
		try {
			helper.endRun(this.runId, BridgeProcess.COMPLETE);
		} catch (Exception ignore) {}
		
		logger.info("Execution Task Completed");
	}
	
	private String getTransanctionOutputfile() {
	     String source = "C:\\Users\\" + System.getProperty("user.name") + ".YETHI\\Downloads\\Daily_Status_Report_Template.xlsx";
//	     String source = "C:\\Users\\" + System.getProperty("user.name") + ".YETHI\\Downloads\\Tenjin_Execution_Report_Run_2573.pdf";
	     String target ="D:\\PDFValidator\\temp\\";
	  
	     File sourceFile = new File(source);
	     String name = sourceFile.getName();
	  
	     File targetFile = new File(target+name);
	  
	     try {
	        FileUtils.copyFile(sourceFile, targetFile);
	    } catch (IOException e) {
	        logger.error("Error ", e);
	    }
	  
		return target+name;
	}

	@SuppressWarnings("unchecked")
	void processAPIStepIteration(ExecutionStep step, Api api, Aut aut, ApiOperation operation, ApiApplication bridge){
		ApiTestDataHandlerImpl handler = new ApiTestDataHandlerImpl();
		List<String> tdUids = this.tdUidMap.get(step.getRecordId());
		IterationHelper helper = new IterationHelper();
		int txnLimit = step.getTransactionLimit();
		for(int i=0; i<txnLimit; i++){
			int iteration = i+1;
			Date startTime = new Date();
			logger.info("Beginning Execution of Test Case [{}], Test Step [{}], Iteration [{}]", step.getTestCaseId(), step.getId(), iteration);
			String tdUid = tdUids.get(i);
			logger.info("TDUID for this transaction --> [{}]", tdUid);

			logger.debug("Setting status of Transaction [{}] to X", tdUid);
			try {
				helper.updateRunStatus(this.runId, step.getRecordId(), iteration, "X",startTime);
			} catch (DatabaseException e) {}
			
			this.updateProgress(tdUid, "X", "", "N/A","start");

			String folderPath = this.testDataPaths.get(step.getFunctionCode()+"_"+step.getOperation());
			JSONObject iterationData;
			JSONObject iterationHeaderData;
			
			String requestMediaType="";
			Representation requestRepresentation=step.getApiOperation().getListRequestRepresentation().get(0);
			if(requestRepresentation != null) {
				requestMediaType = requestRepresentation.getMediaType();
			}



			logger.info("Loading test data for step [REQUEST]");
			try {
				if(Utilities.trim(api.getType()).toLowerCase().startsWith("soap")) {
					iterationData = handler.loadTestDataFromTemplate(folderPath, aut.getId(), step.getApiCode(), step.getTdGid(), tdUid,step.getTestCaseRecordId(),step.getId(), step.getOperation(), "request");
				}else{
					iterationData = handler.loadTestDataFromTemplateForXML(folderPath, aut.getId(), step.getApiCode(), step.getTdGid(), tdUid, step.getApiOperation().getName(), "request", 0,requestMediaType,step.getTestCaseRecordId(),step.getId());
				}
			} catch (TestDataException e) {
				logger.error("Could not load test data",e );
				try {
					Date endTime = new Date();
					helper.updateRunStatus(this.runId, step.getRecordId(), iteration, "E", e.getMessage(),endTime, aut.getAccessToken(), operation.getAuthenticationType());
				execStatusList.add("Error");
				} catch (DatabaseException ignore) {}
				this.updateProgress(tdUid, "E", e.getMessage(), "N/A", "end");
				execStatusList.add("Error");
				continue;
			}
			
			/*	Added By Paneendra for TENJINCG-1239 starts */
			logger.info("Loading test data for step [HEADER]");
			try {
				if(Utilities.trim(api.getType()).toLowerCase().startsWith("soap")) {
					iterationHeaderData = handler.loadTestDataFromTemplate(folderPath, aut.getId(), step.getApiCode(), step.getTdGid(), tdUid,step.getTestCaseRecordId(),step.getId(), step.getOperation(), "header");
				}else{
					iterationHeaderData = handler.loadTestDataFromTemplate(folderPath, aut.getId(), step.getApiCode(), step.getTdGid(), tdUid, step.getApiOperation().getName(), "header", -1);
				}
			} catch (TestDataException e) {
				logger.error("Could not load test data",e );
				try {
					Date endTime = new Date();
					helper.updateRunStatus(this.runId, step.getRecordId(), iteration, "E", e.getMessage(),endTime, aut.getAccessToken(), operation.getAuthenticationType());
				execStatusList.add("Error");
				} catch (DatabaseException ignore) {}
				this.updateProgress(tdUid, "E", e.getMessage(), "N/A", "end");
				execStatusList.add("Error");
				continue;
			}
			
			/*	Added By Paneendra for TENJINCG-1239 ends */
		
			
			IterationStatus iStatus = null;

			try {
				logger.info("Executing Iteration...");
				
				//commented for TENJINCG-1239
				//iStatus = bridge.executeIteration(iterationData,iteration, step.getResponseType());
				//commented for TENJINCG-1239
				
				/*	Added By Paneendra for TENJINCG-1239 starts */
				iStatus = bridge.executeIteration(iterationData, iterationHeaderData,iteration, step.getResponseType());
				/*	Added By Paneendra for TENJINCG-1239 ends */
			}  catch (Exception e) {
				logger.error("ERROR - Uncaught exception in executeIteration", e);
				try {
					Date endTime = new Date();
					helper.updateRunStatus(this.runId, step.getRecordId(), iteration, "E", "Execution aborted abnormally due to an internal error. Please contact Tenjin Support.",endTime, aut.getAccessToken(), operation.getAuthenticationType());
					execStatusList.add("Error");
				} catch (DatabaseException ignore) {}
				this.updateProgress(tdUid, "E", "Execution aborted abnormally due to an internal error. Please contact Tenjin Support.", "N/A", "end");
				continue;
			}

			if(iStatus != null){
				if(iStatus.getMessage()==null){
					iStatus.setMessage(" ");
				}
				
				if("Negative".equalsIgnoreCase(step.getType()) && iStatus.getResult().equalsIgnoreCase("S")){
					logger.info("Marking this step as a failure as the transaction is Passed but the Step is a negative step");
					iStatus.setResult("F");
				}
				
				if("Negative".equalsIgnoreCase(step.getType()) && (iStatus.getResult().equalsIgnoreCase("F") || iStatus.getResult().equalsIgnoreCase("E"))){
					String[] stepMessages = null;
					try {
						stepMessages = step.getExpectedResult().split(";");
					} catch (Exception e) {
						logger.info("Expected Result is empty");
					}
					
					List<String> errorMessages = new ArrayList<String>();
					for (RuntimeFieldValue runTimeField : iStatus.getRuntimeFieldValues()) {
						if (runTimeField.getField().toLowerCase().contains("code")) {
						//if (runTimeField.getField().contains("WSERROR")) {
							errorMessages.add(runTimeField.getValue());
						}
					}
					boolean allMessageValidationsPassed = true;
					if(stepMessages != null && stepMessages.length > 0) {
						PatternMatch pm = new PatternMatch();
						for(String stepMessage : stepMessages) {
							ValidationResult r = new ValidationResult();
							r.setExpectedValue(stepMessage);
							r.setDetailRecordNo("1");
							r.setField("Transaction Message");
							r.setIteration(iteration);
							r.setPage("Negative Test Step Validation");
							r.setResValId(0);
							r.setRunId(this.runId);
							r.setTestStepRecordId(step.getRecordId());
							boolean matchFound = false;
							if(errorMessages != null && errorMessages.size() > 0) {
								for(String errorMessage : errorMessages) {
									if(pm.matchString(errorMessage, stepMessage)) {
										r.setActualValue(errorMessage);
										r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
										matchFound = true;
										break;
									}
								}
							}
							
							if(!matchFound) {
								allMessageValidationsPassed = false;
								r.setActualValue("The pattern did not match any message from the Response body");
								r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
							}
							
							if(iStatus.getValidationResults() != null){
								iStatus.getValidationResults().add(0, r);
							}else{
								List<ValidationResult> rs = new ArrayList<ValidationResult>();
								rs.add(r);
								iStatus.setValidationResults(rs);
							}
						}
					}
					if(allMessageValidationsPassed) {
						iStatus.setResult("S");
					}
				}
				String resultValidationStatus = "N/A";
				if(iStatus.getResult().equalsIgnoreCase("S")) {
					logger.info("Loading test data for step [RESPONSE]");
					JSONObject expectedResponseData = null;
					ApiResponseValidator validator = null;
					boolean okToValidate = true;
					try {
						if(Utilities.trim(step.getApi().getType()).toLowerCase().startsWith("rest")) {
							int respType=step.getResponseType();
							if(respType!=0){
							//expectedResponseData = new ApiResponseTestDataHandlerImpl().loadResponseVerificationDataFromTemplate(folderPath, aut.getId(), step.getApiCode(), step.getTdGid(),tdUid, step.getOperation(), "response",step.getResponseType());
							if(requestRepresentation.getMediaType().equalsIgnoreCase("application/xml")){
							expectedResponseData = handler.loadTestDataFromTemplate(folderPath, aut.getId(),
							step.getApiCode(), step.getTdGid(), tdUid, step.getTestCaseRecordId(), step.getId(),
							step.getOperation(), "response");
							}else {
							expectedResponseData = new ApiResponseTestDataHandlerImpl().loadResponseVerificationDataFromTemplate(folderPath, aut.getId(), step.getApiCode(), step.getTdGid(),tdUid, step.getOperation(), "response",step.getResponseType());
							}
							okToValidate = true;
							}else{
							okToValidate=false;
							}
							}else{
							expectedResponseData = handler.loadTestDataFromTemplate(folderPath, aut.getId(), step.getApiCode(), step.getTdGid(), tdUid,step.getTestCaseRecordId(),step.getId(), step.getOperation(), "response");
							}
						/*
						 * if(Utilities.trim(step.getApi().getType()).toLowerCase().startsWith("rest"))
						 * { int respType=step.getResponseType(); if(respType!=0){ expectedResponseData
						 * = handler.loadTestDataFromTemplate(folderPath, aut.getId(),
						 * step.getApiCode(), step.getTdGid(),
						 * tdUid,step.getTestCaseRecordId(),step.getId(), step.getOperation(),
						 * "response"); //Keerthi Changes for get vakidation values form template
						 * //expectedResponseData = new
						 * ApiResponseTestDataHandlerImpl().loadResponseVerificationDataFromTemplate(
						 * folderPath, aut.getId(), step.getApiCode(), step.getTdGid(),tdUid,
						 * step.getOperation(), "response",step.getResponseType()); okToValidate = true;
						 * }else{ okToValidate=false; } }else{ expectedResponseData =
						 * handler.loadTestDataFromTemplate(folderPath, aut.getId(), step.getApiCode(),
						 * step.getTdGid(), tdUid,step.getTestCaseRecordId(),step.getId(),
						 * step.getOperation(), "response"); }
						 */
						validator = ApiApplicationFactory.getResponseValidator(api.getType(), this.runId, step, iteration);
					} catch (TestDataException e) {
						logger.error("ERROR loading response data for validation", e);
						iStatus.setResult("E");
						iStatus.setMessage("Execution passed, but Tenjin could not load verification data. Please contact Tenjin Support");
						okToValidate = false;
					} catch (BridgeException e) {
						logger.error(e.getMessage());
						iStatus.setResult("E");
						iStatus.setMessage("Execution passed, but Tenjin could not initialize Validator for Response validation. Please contact Tenjin Support.");
						okToValidate = false;
					}
					
					if(okToValidate){
						Map<String, Object> map =  validator.validateResponse(expectedResponseData, iStatus.getApiResponseDescriptor());
						
						List<ValidationResult> results = null;
						List<RuntimeFieldValue> rfvs = null;
						if(map != null) {
							results = (List<ValidationResult>) map.get("VALIDATIONS");
							rfvs = (List<RuntimeFieldValue>) map.get("RUNTIME_VALUES");
						}
						
						if(rfvs != null && !rfvs.isEmpty()) {
							iStatus.getRuntimeFieldValues().addAll(rfvs);
						}
						
						if(results != null && !results.isEmpty()) {
							iStatus.setValidationResults(results);
						}
						
						if(results != null && results.size() > 0) {
							int f=0;
							int e=0;
							for(ValidationResult result : results) {
								if("fail".equalsIgnoreCase(result.getStatus())) {
									f++;
								}else if("pass".equalsIgnoreCase(result.getStatus())) {
								}else{
									e++;
								}
							}
							
							if(e > 0) {
								resultValidationStatus = TenjinConstants.ACTION_RESULT_ERROR;
								iStatus.setResult("F"); 
							}else if(f > 0) {
								resultValidationStatus = TenjinConstants.ACTION_RESULT_FAILURE;
								iStatus.setResult("F"); 
							}else {
								resultValidationStatus = TenjinConstants.ACTION_RESULT_SUCCESS;
							}
						}
					}
				}
				
				try {
					logger.info("Updating Results...");
					String result = iStatus.getResult();
					if(Utilities.trim(result).equalsIgnoreCase("")){
						logger.warn("iStatus.getResult() was either null or blank. Hence, setting to E");
						iStatus.setResult("E");
						
						if(Utilities.trim(iStatus.getMessage()).equalsIgnoreCase("")){
							logger.warn("Application did not return any message for this transaction. hence, defaulting the message");
							iStatus.setMessage("No message received from Application");
						}
					}
					
					Date endTime = new Date();
					helper.updateRunStatus(this.runId, step.getRecordId(), iteration, iStatus.getResult(), iStatus.getMessage(),endTime, aut.getAccessToken(), operation.getAuthenticationType());
					 result = iStatus.getResult();
					if(result.equalsIgnoreCase("E")){
						execStatusList.add("Error");
	                }else if(result.equalsIgnoreCase("F")){
	                	execStatusList.add("Fail");
	                }else{
	                	execStatusList.add("Pass");                 
	                }
					logger.info("Persisting validation resulsts...");
					helper.persistValidationResults(iStatus.getValidationResults(), this.runId, step.getRecordId(), iteration);

					logger.info("Persisting Run-Time Values...");
					helper.persistRuntimeFieldValues(iStatus.getRuntimeFieldValues(), this.runId, step.getRecordId(), iteration);

					logger.info("Persisting Run-Time Screenshots...");
					helper.persistRuntimeScreenshots(iStatus.getRuntimeScreenshots(), this.runId, step.getRecordId(), iteration,false);
					
					logger.info("Updating Request and Response...");
					helper.updateWSRequestAndResponse(this.runId, step.getRecordId(), iteration, iStatus.getApiRequestDescriptor(), iStatus.getApiResponseDescriptor());
					/*Added by Pushpalatha for TENJINCG-1228 starts*/
					logger.info("Updating Request and Response...");
					helper.updateRequestLogs(this.runId, step.getRecordId(), iteration, iStatus.getReqLog());
					/*Added by Pushpalatha for TENJINCG-1228 ends*/
				} catch (DatabaseException e) {}

				boolean hasRuntimeValues = false;
				if(iStatus.getRuntimeFieldValues() != null && iStatus.getRuntimeFieldValues().size() > 0) {
					hasRuntimeValues = true;
				}
				this.updateProgress(tdUid, iStatus.getResult(), iStatus.getMessage(), resultValidationStatus, hasRuntimeValues,"end");
			} else {
				this.updateProgress(tdUid, "E", "Could not get result.", "N/A", "end");
			}
		}
	}
	
	void processGUIStepIteration(ExecutionStep step, Aut aut){
		GuiDataHandler handler = null;
		try {
			handler = GuiApplicationFactory.getTestDataProvider(aut.getAdapterPackage());
		} catch (BridgeException e2) {
			logger.warn(e2.getMessage());
		}

		if (handler == null) {
			logger.warn("No Application specific Test Data provider was found. Will use default provider");
			handler = new GuiDataHandlerImpl();
		}
		
		IterationHelper helper = new IterationHelper();
		List<String> tdUids = this.manifest.getTdUidMap().get(step.getRecordId());
		int txnLimit = step.getTransactionLimit();
		
		for (int i = 0; i < txnLimit; i++) {
			int iteration = i + 1;
			Date startTime = new Date();
			logger.info(
					"Beginning Execution of Test Case [{}], Test Step [{}], Iteration [{}]",
					step.getTestCaseId(), step.getId(), iteration);
			String tdUid = tdUids.get(i);
			logger.info("TDUID for this transaction --> [{}]", tdUid);

			logger.debug("Setting status of Transaction [{}] to X", tdUid);
			try {
				helper.updateRunStatus(this.runId, step.getRecordId(),iteration, "X",startTime);
			} catch (DatabaseException e) {}
			
			this.updateProgress(tdUid, "X", "", "N/A","start");
			
			String folderPath = this.testDataPaths.get(step.getFunctionCode()+step.getAppId());
			JSONObject iterationData;
			logger.info("Loading test data for step");
			logger.debug("Checking if thread for run {} is alive", this.runId);
			if (CacheUtils.isRunAborted(this.runId)) {
				logger.debug("Thread for Run {} is interrupted.", this.runId);
				this.abort();
				return;
			}

			logger.debug("Thread for Run {} is still alive", this.runId);
			try {
				iterationData = handler.loadTestDataFromTemplate(
						folderPath, aut.getId(), step.getFunctionCode(),
						step.getTdGid(), tdUid, step.getTestCaseRecordId(),
						step.getId());
			} catch (TestDataException e) {
				logger.error("Could not load test data", e);
				try {
					Date endTime = new Date();
					helper.updateRunStatus(this.runId, step.getRecordId(),iteration, "E", e.getMessage(),endTime);
					execStatusList.add("Error");
				} catch (DatabaseException ignore) {}
				this.updateProgress(tdUid, "E", e.getMessage(), "N/A", "end");
				continue;
			}

			if (this.videoCaptureFlag.equalsIgnoreCase("Y") && !Utilities.trim(this.videoOutputFolder).equalsIgnoreCase("")) {
				logger.info("Beginning video capture");
				recorder = new Recorderer();
				recorder.startRecording(this.videoOutputFolder, tdUid);
				this.videoCaptureInProgress = true;
			} else {
				this.videoCaptureInProgress = false;
			}

			try {
				logger.info("Navigating to function [{}] using menu [{}]", step.getFunctionCode(), step.getFunctionMenu());
				logger.debug("Checking if thread for run {} is alive", this.runId);
				if (CacheUtils.isRunAborted(this.runId)) {
					logger.debug("Thread for Run {} is interrupted.", this.runId);
					this.abort();
					return;
				}
				logger.debug("Thread for Run {} is still alive", this.runId);
				
				this.navigateToFunction(oApplication, step.getFunctionCode(), null, step.getFunctionMenu());
				logger.info("Executing Iteration...");
				logger.debug("Checking if thread for run {} is alive", this.runId);
				if (CacheUtils.isRunAborted(this.runId)) {
					logger.debug("Thread for Run {} is interrupted.", this.runId);
					this.abort();
					return;
				}
				logger.debug("Thread for Run {} is still alive", this.runId);

				ExecutionTaskHandler objExecutionTaskHandler = TaskHandlerFactory.getExecutionTaskHandler(this.oApplication);
				IterationStatus iStatus = objExecutionTaskHandler
						.executeFunctionImpl(step, iteration,
								iterationData, this.screenshotOption,
								this.runId, this.fieldTimeout);
                String execStatus=this.persistExecutionResults(step, iteration, runId,iStatus);
                
                this.updateProgress(tdUid, iStatus.getResult(), iStatus.getMessage(), execStatus ,"end");
                
                execStatusList.add(execStatus);
			}catch (DriverException | ExecutorException e) {
				logger.error("Navigate to function failed");
				try {
					Date endTime = new Date();
					helper.updateRunStatus(this.runId, step.getRecordId(),iteration, "E", e.getMessage(),endTime);
					 execStatusList.add("Error");
				} catch (DatabaseException ignore) {}
				if (this.videoCaptureInProgress && recorder != null) {
					logger.info("Stopping Video capture");
					recorder.stopRecording();
					this.videoCaptureInProgress = false;
				}
				this.updateProgress(tdUid, "E", e.getMessage(), "N/A", "end");
				continue;
			}catch (AutException e) {
				logger.error("Navigate to function failed");
				try {
					Date endTime = new Date();
					helper.updateRunStatus(this.runId, step.getRecordId(),iteration, "F", e.getMessage(),endTime);
					 execStatusList.add("Fail");
				} catch (DatabaseException ignore) {}

				if (this.videoCaptureInProgress && recorder != null) {
					logger.info("Stopping Video capture");
					recorder.stopRecording();
					this.videoCaptureInProgress = false;
				}
				this.updateProgress(tdUid, "F", e.getMessage(), "N/A", "end");
				continue;
			}catch (BridgeException e) {
				logger.error("Navigate to function failed");
				try {
					Date endTime = new Date();
					helper.updateRunStatus(this.runId, step.getRecordId(),iteration, "E", e.getMessage(),endTime);
					 execStatusList.add("Error");
				} catch (DatabaseException ignore) {}
				if (this.videoCaptureInProgress && recorder != null) {
					logger.info("Stopping Video capture");
					recorder.stopRecording();
					this.videoCaptureInProgress = false;
				}
				this.updateProgress(tdUid, "E", e.getMessage(), "N/A", "end");
				continue;
			} catch (RunAbortedException e) {
				logger.error(e.getMessage());
				this.abort();
				return;
			}

			logger.info("Recovering...");
			logger.debug("Checking if thread for run {} is alive", this.runId);
			if (CacheUtils.isRunAborted(this.runId)) {
				logger.debug("Thread for Run {} is interrupted.", this.runId);
				this.abort();
				return;
			}

			logger.debug("Thread for Run {} is still alive", this.runId);
			try {
				oApplication.recover();
			} catch (AutException | DriverException e1) {
				logger.error("ERROR while recovering after executing iteration",e1);
			}

			logger.debug("Checking if thread for run {} is alive", this.runId);
			if (CacheUtils.isRunAborted(this.runId)) {
				logger.debug("Thread for Run {} is interrupted.", this.runId);
				this.abort();
				return;
			}

			if (this.videoCaptureInProgress && recorder != null) {
				logger.info("Stopping Video capture");
				recorder.stopRecording();
				this.videoCaptureInProgress = false;
			}
		}
	}
	
	private void launchAndLogin(GuiApplicationAbstract oApplication, Aut aut,
			String aBrowser, ExecutionStep step) throws DriverException,
			AutException {
		boolean launchAndLoginComplete = false;
		int retries = 0;
		while (!launchAndLoginComplete) {
			try {
				if (CacheUtils.isRunAborted(this.runId)) {
					logger.debug("Thread for Run {} is interrupted.", this.runId);
					this.abort();
					return;
				}
				logger.info("Launching Application [{}] on [{}]", step.getAppName(), aBrowser);
				oApplication.launch(aut.getURL());

				if (CacheUtils.isRunAborted(this.runId)) {
					logger.debug("Thread for Run {} is interrupted.", this.runId);
					this.abort();
					return;
				}

				logger.info("Logging in to [{}] as [{}]", step.getAppName(), step.getAutLoginType());
				if(aut.getPauseLocation()!=null && aut.getPauseLocation().equalsIgnoreCase("before login")){
					oApplication.explicitWait(aut.getPauseTime());
					oApplication.login(aut.getLoginName(), aut.getPassword());
				}else if(aut.getPauseLocation()!=null && aut.getPauseLocation().equalsIgnoreCase("after login")){
					oApplication.login(aut.getLoginName(), aut.getPassword());
					oApplication.explicitWait(aut.getPauseTime());
				}else
					oApplication.login(aut.getLoginName(), aut.getPassword());
				launchAndLoginComplete = true;
			} catch (ApplicationUnresponsiveException e) {
				logger.error(e.getMessage());
				if (retries >= AUT_RETRY_LIMIT) {
					logger.error("Could not launch and login to application {} after {} retries", aut.getName(), AUT_RETRY_LIMIT);
					throw new AutException(e.getMessage());
				} else if (CacheUtils.isRunAborted(this.runId)) {
					logger.debug("Thread for Run {} is interrupted.", this.runId);
					this.abort();
					return;
				}else {
					logger.info("Will try again after {} seconds", AUT_RETRY_INTERVAL);
					logger.info("Cleaning up open browser in the mean time");
					oApplication.destroy();
					oApplication.explicitWait(AUT_RETRY_INTERVAL);
				}
			} finally {
				retries++;
			}
		}
	}
	
	private void navigateToFunction(GuiApplicationAbstract oApplication,
			String functionCode, String functionName, String menuHierarchy)
			throws AutException, BridgeException {
		boolean navigationComplete = false;
		int retries = 0;

		while (!navigationComplete) {
			try {
				oApplication.navigateToFunction(functionCode, functionName, menuHierarchy);
				navigationComplete = true;
			} catch (ApplicationUnresponsiveException | DriverException e) {
				logger.error(e.getMessage());
				if (retries >= AUT_RETRY_LIMIT) {
					logger.error("Could not navigate to function {} after {} attempts", functionCode, AUT_RETRY_LIMIT);
					throw new AutException(e.getMessage());
				} else {
					logger.info("Will try again after {} seconds", AUT_RETRY_INTERVAL);
					oApplication.explicitWait(AUT_RETRY_INTERVAL);
					retries = retries + 1;
				}
			}
		}
	}
	
	private String persistExecutionResults(ExecutionStep step, int iteration,
			int runId, IterationStatus iStatus) {
		IterationHelper helper = new IterationHelper();
		String execStatus=null;
		if (iStatus != null) {
			try {
				logger.info("Updating Results...");
				String result = iStatus.getResult();
				if (Utilities.trim(result).equalsIgnoreCase("")) {
					logger.warn("iStatus.getResult() was either null or blank. Hence, setting to E");
					iStatus.setResult("E");
					if (Utilities.trim(iStatus.getMessage()).equalsIgnoreCase("")) {
						logger.warn("Application did not return any message for this transaction. hence, defaulting the message");
						iStatus.setMessage("No message received from Application");
					}
				}
				
				if (iStatus.getValidateAllMessagesWithExpectedMessage()) {
					if(!iStatus.getResult().equals("E")){
						this.testStepTypeValidation(step, iStatus);
					}
				} else {
					if (iStatus.getMessage() == null) {
						iStatus.setMessage(" ");
					}
					
					if ((iStatus.getResult().equalsIgnoreCase("F") || iStatus
							.getResult().equalsIgnoreCase("E"))
							&& Utilities
									.trim(iStatus.getMessage())
									.equalsIgnoreCase(
											Utilities.trim(step.getExpectedResult()))) {
						
						iStatus.setResult("S");
						ValidationResult r = new ValidationResult();
						r.setActualValue(iStatus.getMessage());
						r.setExpectedValue(step.getExpectedResult());
						r.setDetailRecordNo("1");
						r.setField("Transaction Message");
						r.setIteration(iteration);
						r.setPage("Negative Test Step Validation");
						r.setResValId(0);
						r.setRunId(runId);
						r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
						r.setTestStepRecordId(step.getRecordId());

						if (iStatus.getValidationResults() != null) {
							iStatus.getValidationResults().add(0, r);
						} else {
							List<ValidationResult> rs = new ArrayList<ValidationResult>();
							rs.add(r);
							iStatus.setValidationResults(rs);
						}
					}
				}

				logger.info("Persisting validation resulsts...");
				helper.persistValidationResults(iStatus.getValidationResults(),	runId, step.getRecordId(), iteration);

				List<StepIterationResult> validationFailures = new RunDefectHelper().getTransactionsWithValidationFailures(runId);
				if (validationFailures != null && validationFailures.size() > 0) {
					for (StepIterationResult r : validationFailures) {
						
						if (step.getRecordId() == r.getTsRecId()
								&& iStatus.getResult().equalsIgnoreCase("S")
								&& r.getIterationNo() == iteration) {
							
							iStatus.setResult("F");
							iStatus.setMessage("Validation Failed");
							break;
						}
					}
				}
				Date endTime = new Date();
				helper.updateRunStatus(runId, step.getRecordId(), iteration,iStatus.getResult(), iStatus.getMessage(), endTime);
				
				logger.info("Persisting Run-Time Values...");
				helper.persistRuntimeFieldValues(iStatus.getRuntimeFieldValues(), runId, step.getRecordId(), iteration);

				logger.info("Persisting Run-Time Screenshots...");
				helper.persistRuntimeScreenshots(iStatus.getRuntimeScreenshots(), runId,step.getRecordId(), iteration,false);
				
				logger.info("Persisting UI Validation results");
				new UIValidationHelper().persistUIValidationResults(runId, step.getRecordId(), iStatus.getUiValidationResults());
			} catch (DatabaseException e) {}
			
			String result = iStatus.getResult();
			if(result.equalsIgnoreCase("E")){
				execStatus="Error";
	        }else if(result.equalsIgnoreCase("F")){
	        	execStatus="Fail";
	        }else{
	        	execStatus="Pass";                 
	        }
		}
		return execStatus;
	}
	
	private void testStepTypeValidation(ExecutionStep step, IterationStatus iStatus) {
		boolean testTypeIsPositive = true;
		boolean testResultIsPositive = true;
		String teststepType = step.getType();
		if (!teststepType.equalsIgnoreCase("positive"))
			testTypeIsPositive = false;
		if (!iStatus.getResult().equalsIgnoreCase("S"))
			testResultIsPositive = false;

		String message = iStatus.getMessage().trim();
		if (step.getExpectedResult().equals("")) {
			iStatus.setExpectedMessageFound(true);
		}

		if (testTypeIsPositive == testResultIsPositive && iStatus.getExpectedMessageFound()) {
			iStatus.setResult("S");
			if (message.equals(""))
				iStatus.setMessage("Transaction completed successfully.");
		} else {
			iStatus.setResult("F");
			if (message.equals("")) {
				message = "Transaction Failed";
				iStatus.setMessage("Transaction Failed");
			}
			if (!iStatus.getExpectedMessageFound()) {
				iStatus.setMessage(message + " - Expected Result not found");
			}
		}
	}
	
	private Map<String, Map<String, List<UIValidation>>> getUIValidationResultsForStep(
			int runId, int stepRecordId) {
		try {
			Map<String, Map<String, List<UIValidation>>> map = new UIValidationHelper()
					.hydrateUIValidationsForStepWithResults(stepRecordId,
							runId, "type");

			for (String type : map.keySet()) {
				String messageKeyPrefix = type.replace(" ", "_");
				for (String page : map.get(type).keySet()) {
					for (UIValidation val : map.get(type).get(page)) {
						if (val.getResults() != null) {
							for (UIValidationResult result : val.getResults()) {
								String expMessageKey = messageKeyPrefix;
								String actMessageKey = messageKeyPrefix;
								if (Utilities.trim(result.getExpectedValue())
										.equalsIgnoreCase("true")
										|| Utilities.trim(
												result.getExpectedValue())
												.equalsIgnoreCase("false")) {
									
									expMessageKey += "."
											+ Utilities.trim(
													result.getExpectedValue())
													.toLowerCase()
											+ ".expected";
									actMessageKey += "."
											+ Utilities.trim(
													result.getActualValue())
													.toLowerCase() + ".actual";
								} else {
									expMessageKey += ".expected";
									actMessageKey += ".actual";
								}

								try {
									String expMsg = MessageUtil.getMessage(
											expMessageKey,
											result.getExpectedValue());
									String actMsg = MessageUtil.getMessage(
											actMessageKey,
											result.getActualValue());
									result.setExpectedValue(Utilities.trim(
											expMsg).length() > 0 ? Utilities
											.trim(expMsg) : result
											.getExpectedValue());
									result.setActualValue(Utilities
											.trim(actMsg).length() > 0 ? Utilities
											.trim(actMsg) : result
											.getActualValue());
								} catch (TenjinConfigurationException e) {
									logger.warn("Could not get message from configuration",	e);
								}
							}
						}
					}
				}
			}
			return map;
		} catch (DatabaseException e) {
			logger.error("ERROR getting uI Validation results for step", e);
			return null;
		}
	}
	
	private void updateProgress(String tdUid, String status, String message, String resultValidationStatus, String stage) {
		if("end".equalsIgnoreCase(stage)) {
			this.completedTransactions++;
			this.progress.setCompletedTransactions(this.completedTransactions);
		}
		
		Map<String, String> txnMap = new HashMap<String, String>();
		txnMap.put("status", status);
		txnMap.put("message", message);
		txnMap.put("resultValidationStatus", resultValidationStatus);
		
		this.progress.getTransactionsMap().put(tdUid, txnMap);
		
		double percentage = ((double) this.completedTransactions / (double) this.totalTransactions) * 100;
		double roundOff = Math.round(percentage * 100.0) / 100.0;
		int rWithoutDecimals = (int) roundOff;
		this.progress.setPercentage(rWithoutDecimals);
		
		String elapsedTime= Utilities.calculateElapsedTime(this.startTimeinMillis, System.currentTimeMillis());
		this.progress.setElapsedTime(elapsedTime);
		
		if(rWithoutDecimals >= 100){
			this.progress.setStatus(BridgeProcess.COMPLETE);
		}else{
			this.progress.setStatus(BridgeProcess.IN_PROGRESS);
		}
		CacheUtils.putObjectInRunCache(this.runId, this.progress);
	}
	
	private void updateProgress(String tdUid, String status, String message, String resultValidationStatus, boolean hasRuntimeValues, String stage) {
		if("end".equalsIgnoreCase(stage)) {
			this.completedTransactions++;
			this.progress.setCompletedTransactions(this.completedTransactions);
		}
		
		Map<String, String> txnMap = new HashMap<String, String>();
		txnMap.put("status", status);
		txnMap.put("message", message);
		txnMap.put("resultValidationStatus", resultValidationStatus);
		if(hasRuntimeValues) {
			txnMap.put("runtimevalues", "yes");
		}else{
			txnMap.put("runtimevalues", "no");
		}
		
		this.progress.getTransactionsMap().put(tdUid, txnMap);
		
		double percentage = ((double) this.completedTransactions / (double) this.totalTransactions) * 100;
		double roundOff = Math.round(percentage * 100.0) / 100.0;
		int rWithoutDecimals = (int) roundOff;
		this.progress.setPercentage(rWithoutDecimals);
		
		String elapsedTime= Utilities.calculateElapsedTime(this.startTimeinMillis, System.currentTimeMillis());
		this.progress.setElapsedTime(elapsedTime);
		
		if(rWithoutDecimals >= 100){
			this.progress.setStatus(BridgeProcess.COMPLETE);
		}else{
			this.progress.setStatus(BridgeProcess.IN_PROGRESS);
		}
		
		CacheUtils.putObjectInRunCache(this.runId, this.progress);
	}
	
	private void abort() {
		logger.info("Recieved call to ABORT this run.");
		logger.info("Initiating tear-down procedure");
		this.tearDown();
		ExecutorProgress eProgress = null;
		try {
			 eProgress = (ExecutorProgress)CacheUtils.getObjectFromRunCache((this.runId));
		} catch (TenjinConfigurationException e1) {
			logger.error("Error ", e1);
		}
		String runAbortedUserId=this.userId;
		if(eProgress!=null&&eProgress.getRunAborteduserId()!=null){
			runAbortedUserId=eProgress.getRunAborteduserId();
		}
		logger.info("Aborting run");
		try {
			new IterationHelper().abortRun(this.runId, runAbortedUserId);
		} catch (DatabaseException e) {
			logger.error("ERROR aborting run. Could not update abort status to the run",e);
		}
		logger.info("ABORT procedure complete! Run [{}] is now ABORTED by [{}]", this.runId, this.userId);
	}
	
	private void tearDown() {
		logger.info("Closing application(s)");
		if (this.oApplication != null) {
			try {
				logger.info("Performing Recovery Procedure");
				this.oApplication.recover();
			} catch (AutException | DriverException e1) {
				logger.error("ERROR recovering from Application", e1);
			}

			try {
				logger.info("Logging out of AUT");
				this.oApplication.logout();
			} catch (AutException | DriverException e) {
				logger.error("ERROR logging off from AUT", e);
			}

			logger.info("Destroying application");
			this.oApplication.destroy();
		} else {
			logger.info("No application was initialized. Nothing to tear-down");
		}
		logger.info("Tear Down completed");
	}
}