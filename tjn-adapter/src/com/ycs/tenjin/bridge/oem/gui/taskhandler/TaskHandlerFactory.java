/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SeleniumTaskHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION 
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 */

package com.ycs.tenjin.bridge.oem.gui.taskhandler;

import java.lang.reflect.Constructor;
import java.util.Set;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationAbstract;

public class TaskHandlerFactory {

	private static final Logger logger = LoggerFactory
			.getLogger(TaskHandlerFactory.class);

	public static LearningTaskHandler getLearningTaskHandler(
			GuiApplicationAbstract application) throws BridgeException {

		String expPackageName = "com.ycs.tenjin.bridge."
				+ application.getAdapterPackageOrTool() + ".taskhandler";
		Reflections reflections = new Reflections(expPackageName);

		Set<Class<? extends LearningTaskHandler>> subTypes = reflections
				.getSubTypesOf(LearningTaskHandler.class);
		for (Class<? extends LearningTaskHandler> type : subTypes) {
			try {
				Constructor<?> constructor = type
						.getConstructor(GuiApplicationAbstract.class);

				return (LearningTaskHandler) constructor
						.newInstance(application);

			} catch (Exception e) {
				logger.error("Could not initiate adapter for {}",
						application.getAppName(), e);
				throw new BridgeException("Could not initiate the Adapter for "
						+ application.getAppName());
			}
		}

		logger.error("ERROR initializing adapter for {}. Application implementation was not found");
		throw new BridgeException("Could not initialize adapter for "
				+ application.getAppName() + ". Please contact Tenjin Support");

	}

	public static ExecutionTaskHandler getExecutionTaskHandler(
			GuiApplicationAbstract application) throws BridgeException {

		String expPackageName = "com.ycs.tenjin.bridge."
				+ application.getAdapterPackageOrTool() + ".taskhandler";
		Reflections reflections = new Reflections(expPackageName);

		Set<Class<? extends ExecutionTaskHandler>> subTypes = reflections
				.getSubTypesOf(ExecutionTaskHandler.class);
		for (Class<? extends ExecutionTaskHandler> type : subTypes) {
			try {
				Constructor<?> constructor = type
						.getConstructor(GuiApplicationAbstract.class);

				return (ExecutionTaskHandler) constructor
						.newInstance(application);
			} catch (Exception e) {
				logger.error("Could not initiate adapter for {}",
						application.getAppName(), e);
				throw new BridgeException("Could not initiate the Adapter for "
						+ application.getAppName());
			}
		}

		logger.error("ERROR initializing adapter for {}. Application implementation was not found");
		throw new BridgeException("Could not initialize adapter for "
				+ application.getAppName() + ". Please contact Tenjin Support");

	}

	public static ExtractionTaskHandler getExtractionTaskHandler(
			GuiApplicationAbstract application) throws BridgeException {

		String expPackageName = "com.ycs.tenjin.bridge."
				+ application.getAdapterPackageOrTool() + ".taskhandler";
		Reflections reflections = new Reflections(expPackageName);

		Set<Class<? extends ExtractionTaskHandler>> subTypes = reflections
				.getSubTypesOf(ExtractionTaskHandler.class);
		for (Class<? extends ExtractionTaskHandler> type : subTypes) {
			try {
				Constructor<?> constructor = type
						.getConstructor(GuiApplicationAbstract.class);
				return (ExtractionTaskHandler) constructor
						.newInstance(application);
			} catch (Exception e) {
				logger.error("Could not initiate adapter for {}",
						application.getAppName(), e);
				throw new BridgeException("Could not initiate the Adapter for "
						+ application.getAppName());
			}
		}

		logger.error("ERROR initializing adapter for {}. Application implementation was not found");
		throw new BridgeException("Could not initialize adapter for "
				+ application.getAppName() + ". Please contact Tenjin Support");

	}

}
