/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LocalAdapter.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION 
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 18-05-2018           Padmavathi              for TENJINCG-647
 */

package com.ycs.tenjin.bridge.oem.gui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.Oem;
import com.ycs.tenjin.bridge.oem.TaskHandler;
import com.ycs.tenjin.bridge.oem.gui.taskhandler.GuiExecutionTaskHandlerImpl;
import com.ycs.tenjin.bridge.oem.gui.taskhandler.GuiExtractionTaskHandlerImpl;
import com.ycs.tenjin.bridge.oem.gui.taskhandler.GuiLearningTaskHandlerImpl;
import com.ycs.tenjin.bridge.pojo.TaskManifest;
import com.ycs.tenjin.util.Utilities;

public class GuiOemImpl implements Oem {

	private TaskManifest manifest;

	public GuiOemImpl(TaskManifest manifest) {
		this.manifest = manifest;
	}

	public GuiOemImpl() {

	}

	private static final Logger logger = LoggerFactory
			.getLogger(GuiOemImpl.class);

	@Override
	public void execute() throws BridgeException {

		try {
			String taskType = Utilities.trim(manifest.getTaskType());
			TaskHandler taskHandler = null;
			if (taskType.equalsIgnoreCase(BridgeProcess.LEARN)) {

				logger.info("Initiating Learning Task on target [{}]",
						manifest.getClientIp());
				
				taskHandler = new GuiLearningTaskHandlerImpl(
						manifest.getAut(), manifest.getFunctions(),
						manifest.getClientIp(), manifest.getClientPort(),
						manifest.getBrowser(), manifest.getRunId(),manifest.getDeviceId());
				

			} else if (taskType.equalsIgnoreCase(BridgeProcess.EXECUTE)) {

				logger.info("Initiating Execution Task on target [{}]",
						manifest.getClientIp());
				
				taskHandler = new GuiExecutionTaskHandlerImpl(
						manifest.getRunId(), manifest.getAutMap(),
						manifest.getSteps(), manifest.getTdUidMap(),
						manifest.getTestDataPaths(), manifest.getClientIp(),
						manifest.getClientPort(),
						manifest.getScreenshotOption(),
						manifest.getFieldTimeout(), manifest.getBrowser(),
						manifest.getVideoCaptureFlag(),
						/*********************************
	 					* TENJINCG-731 - By Sameer - Mobility - Start
	 					*/
						manifest.getProjectId(), manifest.getUserId(),manifest.getPassedStepsStatusMap(),manifest.getDeviceId());

			} else if (taskType.equalsIgnoreCase(BridgeProcess.EXTRACT)) {

				logger.info("Initiating Extraction Task on target [{}]",
						manifest.getClientIp());
				taskHandler = new GuiExtractionTaskHandlerImpl(
						manifest.getAut(), manifest.getFunctions(),
						manifest.getClientIp(), manifest.getClientPort(),
						manifest.getBrowser(), manifest.getUserId(),
						manifest.getRunId(),manifest.getDeviceId());
						/*********************************
	 					* TENJINCG-731 - By Sameer - Mobility - End
	 					*/

			} else {
				throw new BridgeException("Unsupported Adapter Task");
			}

			if (taskHandler != null) {
				logger.info("Executing TaskHandler");
				taskHandler.execute();
			}
		} catch (Exception e) {
			logger.error("Exception while starting local adapter task", e);
			throw new BridgeException(
					"An internal error occurred while starting the task. Please contact Tenjin support");
		}

	}

	@Override
	public void setManifest(TaskManifest manifest) {
		this.manifest = manifest;

	}

}
