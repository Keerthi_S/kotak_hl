/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GuiDataHandlerImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright  2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 10-Sep-2016           Sriram Sridharan          Newly Added For 
 * 30-Jan-2017           Jyoti Ranjan             TENJINCG_84
 * 21-Jul-2017           Padmavathi               TENJINCG_290
 * 21-Jul-2017           Sriram Sridharan         TENJINCG_290
 * 24-Jul-2017           Padmavathi               TENJINCG_290
 * 08-08-2017           Padmavathi                for T25IT-13
 * 09-08-2017           Padmavathi                for T25IT-43
 * 10-08-2017           Padmavathi                for T25IT-63
 * 17-08-2017           Sriram                for T25IT-214
 * 21-08-2017           Gangadhar Badagi          T25IT-229
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 17-10-2017           Padmavathi              for Tenjincg-349
 * 19-01-2018			Pushpalatha				TenjinCG-581
 * 19-09-2018			Ashiki					TenjinCG-745
 * 27-09-2018			Ashiki					TenjinCG-744
 * 01-10-2018           Leelaprasad             To add of User defined data to run time values
 * 11-10-2018           Leelaprasad              TENJINCG-854
 * 30-10-2018			Ashiki					TENJINCG-891
 * 13-11-2018           Padmavathi              TENJINCG-899
 * 14-11-2018           Padmavathi              TENJINCG-899 
 * 15-11-2018           Padmavathi              TENJINCG-899 
 * 28-12-2018           Leelaprasad             TJN252-17
 * 31-12-2018           Leelaprasad             TJN252-58
 * 31-12-2018			Ashiki					TJN252-47
 * 31-12-2018			Ashiki					To fetch field name for JSON
 * 09-01-2018			Ashiki					TJN252-68
 * 16-01-2019			Ashiki					TJN252-76
 * 22-01-2019			Ashiki					Label fix
 * 25-01-2019			Preeti					TJN252-80
 * 20-02-2019			Ashiki					TJN252-87
 * 24-04-2019			Roshni					TENJINCG-1038
 * 18-12-2020           Paneendra               TENJINCG-1239
 *//*

package com.ycs.tenjin.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.aut.FieldType;
import com.ycs.tenjin.aut.Location;
import com.ycs.tenjin.aut.TestObject;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.LearningHelper;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CurrencyServiceClient;
import com.ycs.tenjin.util.Utilities;

public class SpreadsheetDataHandler implements TestDataHandler {

	public static final Logger logger = LoggerFactory.getLogger(SpreadsheetDataHandler.class);

	private Workbook workbook;
	private static final int TDGIDCOL=0;
	private static final int TDUIDCOL=1;
	private static final int TDDRIDCOL=2;
	private static final int DATA_START_COL=3;

	private static final int HEADER_ROW_NUM=4;
	private static final int DATA_START_ROW_NUM=5;
	private static final int LEGEND_ROW_NUM=2;
	private static final int SHEET_NAME_ROW_NUM=0;
	private static final int SHEET_NAME_CELL_NUM=1;

	private static final short MRB_PAGE_SHEET_COLOR=IndexedColors.LIGHT_YELLOW.getIndex();
	private static final short QUERY_FIELD_COLOR = IndexedColors.LIGHT_ORANGE.getIndex();
	private static final short VERIFY_FIELD_COLOR = IndexedColors.YELLOW.getIndex();
	private static final short ADD_FIELD_COLOR = IndexedColors.LAVENDER.getIndex();
	private static final short DELETE_FIELD_COLOR = IndexedColors.GREY_50_PERCENT.getIndex();

	@Override
	public void generateTemplate(int applicationId, String functionCode, String destinationFolderPath, String templateFileName, boolean mandatoryOnly, boolean editableOnly) throws TestDataException {
		

		LearningHelper helper = new LearningHelper();
		try {
			List<Location> locations = helper.hydrateMetadata(applicationId, functionCode, mandatoryOnly, editableOnly);
			for(Location location:locations){
				JSONObject json = location.toJson();
				JSONArray fields = new JSONArray();
				for(TestObject t:location.getTestObjects()){
					JSONObject field = t.toJson();
					fields.put(field);
					json.put("FIELDS", fields);
				}

				locs.put(json);
			}
			
			this.generateTemplate(applicationId, functionCode, destinationFolderPath, templateFileName, locations);

		} catch (DatabaseException e) {
			
			throw new TestDataException(e.getMessage());
		} 

		

	}

	@Override
	public void generateTemplate(int applicationId, String functionCode, String destinationFolderPath, String templateFileName,	JSONArray manifest) {
		
		List<Location> listLocation=new ArrayList<Location>();
		Location loc;
		try{
			for(int i=0;i<manifest.length();i++)
			{		JSONObject jsonObj1=manifest.getJSONObject(i);
					 loc=new Location();
					loc.setLocationName((String)(jsonObj1.get("name")));					
					loc.setLocationType((String)(jsonObj1.get("type")));
					if(jsonObj1.get("ismrb").equals("T"))
						loc.setMultiRecordBlock(true);
					else 
						loc.setMultiRecordBlock(false);
					List<TestObject> listTestObject=new ArrayList<TestObject>();					
					JSONArray jsonArr=(JSONArray)jsonObj1.get("fields");					
					for(int j=0;j<jsonArr.length();j++)
					{		JSONObject jsonObj2=jsonArr.getJSONObject(j);
							TestObject testObj=new TestObject();
							testObj.setDefaultOptions((String)jsonObj2.get("default_option"));
							testObj.setMandatory((String)jsonObj2.get("mandatory"));
							testObj.setName((String)jsonObj2.get("name"));
							testObj.setIdentifiedBy((String)jsonObj2.get("type"));
							testObj.setObjectClass(jsonObj2.getString("object_class"));
							listTestObject.add(testObj);
					}
					loc.setTestObjects(listTestObject);
					listLocation.add(loc);
			}
			
		}
		catch(Exception e)
		{	logger.error("Error ", e);
			logger.error("json Exception occured during extracting fields");
		}
	
		try {
			this.generateTemplate(applicationId, functionCode, destinationFolderPath, templateFileName, listLocation);
		} catch (TestDataException e) {
			
			logger.error("Error ", e);
		}

	}


	private void generateTemplate(int applicationId, String functionCode, String destinationFolderPath, String templateFileName, List<Location> locations) throws TestDataException{

		logger.debug("Creating new blank workbook");
		this.workbook = new XSSFWorkbook();
		
		logger.debug("Loading styles");
		CellStyle hStyle = getHeaderRowStyle(workbook);
		CellStyle mStyle = getMandatoryFieldStyle(workbook);
		CellStyle queryCellStyle = getQueryCellStyle(workbook);
		CellStyle addCellStyle = getAddCellStyle(workbook);
		CellStyle deleteCellStyle = getDeleteCellStyle(workbook);
		CellStyle verifyCellStyle = getVerifyCellStyle(workbook);

		boolean workbookHasSheets = false;
		
		for(Location location:locations){

			logger.debug("Processing location --> [{}]", location.getLocationName());
			if(location.getTestObjects() != null && location.getTestObjects().size() > 0){
				String sheetName= "";
				if(Utilities.trim(location.getLocationName()).length() > 30){
					logger.debug("Location [{}] has name of length more than 30 characters");
					sheetName = "Page_Area_" + location.getSequence();
				}else{
					sheetName = location.getLocationName();
				}

				XSSFSheet sheet = (XSSFSheet) this.workbook.createSheet(sheetName);
				workbookHasSheets = true;
				if(location.isMultiRecordBlock()){
					sheet.setTabColor(MRB_PAGE_SHEET_COLOR);
				}
				
				Row sheetNameRow = null;
				sheetNameRow = sheet.createRow(SHEET_NAME_ROW_NUM);
				sheetNameRow.createCell(0).setCellValue("Page Name:");
				sheetNameRow.getCell(0).setCellStyle(getPageNameCellStyle(workbook));
				sheetNameRow.createCell(1).setCellValue(location.getLocationName());
				sheetNameRow.getCell(1).setCellStyle(getPageNameMergedCellStyle(workbook));
				CellRangeAddress region = new CellRangeAddress(0,0,1,6);
				sheet.addMergedRegion(region);
				RegionUtil.setBorderBottom(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderTop(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderLeft(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderRight(CellStyle.BORDER_THICK, region, sheet, workbook);
				
				
				sheetNameRow.createCell(7).setCellValue("Do not change this value. It may result in errors during execution");
				sheetNameRow.getCell(7).setCellStyle(getPageNameSubscriptCellStyle(workbook));
				region = new CellRangeAddress(0,0,7,11);
				sheet.addMergedRegion(region);
				
				Row legendRow = null;
				Row headerRow = null;
				logger.debug("Writing legend");
				legendRow = sheet.createRow(LEGEND_ROW_NUM);
				legendRow.createCell(0).setCellValue("Legend");
				legendRow.createCell(1).setCellValue("INPUT");
				legendRow.createCell(2).setCellValue("QUERY");
				legendRow.createCell(3).setCellValue("ADD");
				legendRow.createCell(4).setCellValue("DELETE");
				legendRow.createCell(5).setCellValue("VERIFY");
				
				logger.debug("Applying styles for legend");
				legendRow.getCell(2).setCellStyle(queryCellStyle);
				legendRow.getCell(3).setCellStyle(addCellStyle);
				legendRow.getCell(4).setCellStyle(deleteCellStyle);
				legendRow.getCell(5).setCellStyle(verifyCellStyle);
				
				logger.debug("Writing header row");
				headerRow = sheet.createRow(HEADER_ROW_NUM);
				headerRow.createCell(0).setCellValue("TDGID");
				headerRow.getCell(0).setCellStyle(hStyle);
				headerRow.createCell(1).setCellValue("TDUID");
				headerRow.getCell(1).setCellStyle(hStyle);
				headerRow.createCell(2).setCellValue("TDDRID");
				headerRow.getCell(2).setCellStyle(hStyle);
				
				
				int currentCol = DATA_START_COL;
				
				logger.debug("Writing fields");
				List<String> buttons = new ArrayList<String>();
				for(TestObject field:location.getTestObjects()){
					if(field.getObjectClass().equalsIgnoreCase(FieldType.BUTTON.toString())){
						buttons.add(field.getName());
						continue;
					}
					
					if(field.getObjectClass().equalsIgnoreCase(FieldType.LINK.toString()) || 
							field.getObjectClass().equalsIgnoreCase(FieldType.TABLE.toString()) ||
							field.getObjectClass().equalsIgnoreCase(FieldType.TOOLBARITEM.toString()) ||
							field.getObjectClass().equalsIgnoreCase(FieldType.IMAGE.toString())){
						logger.debug("Field [{}] -- Element Type [{}] will be skipped from template", field.getName(), field.getObjectClass());
						continue;
					}
					
					logger.debug("Adding field [{}]", field.getName());
					headerRow.createCell(currentCol).setCellValue(field.getName());
					if(Utilities.trim(field.getMandatory()).equalsIgnoreCase("yes")){
						logger.debug("Applying mandatory style for field [{}]", field.getName());
						headerRow.getCell(currentCol).setCellStyle(mStyle);
					}else{
						logger.debug("Applying normal style for field [{}]", field.getName());
						headerRow.getCell(currentCol).setCellStyle(hStyle);
					}
					
					
					if(!Utilities.trim(field.getDefaultOptions()).equalsIgnoreCase("")){
						logger.debug("Generating List Cell for field [{}]", field.getName());
						this.generateListCell(field.getDefaultOptions(), headerRow.getRowNum()+1, currentCol, sheet);
					}
					currentCol++;
				}
				
				//Paint all buttons
				String allButtons = "";
				if(buttons.size() > 0){
					headerRow.createCell(currentCol).setCellValue("Button");
					headerRow.getCell(currentCol).setCellStyle(hStyle);
					int counter=1;
					for(String button:buttons){
						allButtons = allButtons + button;
						if(counter < buttons.size()){
							allButtons = allButtons + ",";
						}
					}
					this.generateListCell(allButtons, headerRow.getRowNum()+1, currentCol, sheet);
				}
				
			}else{
				logger.debug("No fields found in location. This will be skipped");
			}
		}
		
		if(workbookHasSheets){
			String endFilePath = "";
			if(!Utilities.trim(templateFileName).endsWith(".xlsx")){
				templateFileName = Utilities.trim(templateFileName) + ".xlsx";
			}
			
			endFilePath = destinationFolderPath + "\\" + templateFileName;
			logger.debug("Writing file to disk");
			try{
				OutputStream out = new FileOutputStream(endFilePath);
				this.workbook.write(out);
				logger.debug("File saved in path {}",endFilePath);
			}catch(Exception e){
				logger.error("ERROR while writing template to disk", e);
				throw new TestDataException("Could not generate Test data template due to an internal error. Please contact Tenjin support");
			}
		}

	}

	@Override
	public JSONObject loadTestDataFromTemplate(String absoluteFilePath, int appId, String functionCode, String tdGid, String tdUid) throws TestDataException {
		
		JSONObject data = null;
		
		try{
			logger.info("Loading workbook from path [{}]", absoluteFilePath);
			this.workbook = WorkbookFactory.create(new File(absoluteFilePath));
		}catch(Exception e){
			
			logger.error(e.getMessage());
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		}
		
		LearningHelper helper = new LearningHelper();
		int numberOfSheets = workbook.getNumberOfSheets();
		logger.info("Loading Test Data");
		logger.info("Number of sheets --> [{}]", numberOfSheets);
		
		String sheetName = "";
		Location location = null;
		JSONArray detailRecords = null;
		Row sheetNameRow = null;
		String locationName = null;
		Cell sheetNameCell = null;
		JSONArray pageAreas = new JSONArray();
		try {
			for(int i=0; i<numberOfSheets; i++){
				Sheet sheet = workbook.getSheetAt(i);
				if(Utilities.trim(sheet.getSheetName()).equalsIgnoreCase("PAGE_AREA_INDEX")){
					continue;
				}
				sheetName = sheet.getSheetName();
				logger.info("Processing sheet {}", sheetName);
				
				sheetNameRow = null;
				sheetNameCell = null;
				sheetNameRow = sheet.getRow(SHEET_NAME_ROW_NUM);
				if(sheetNameRow == null){
					logger.error("SHEET NAME ROW not found. Invalid template");
					throw new TestDataException("Invalid Test Data Template - Page Name row was not found. Please use the latest test data template and try again");
				}
				
				sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);
				
				locationName = getCellValue(sheetNameCell);
				if(locationName.equalsIgnoreCase("")){
					logger.error("LOCATION NAME TAMPERED WITH - Location Name is blank");
					throw new TestDataException("Invalid Test Data Template - Could not find Page Name in sheet [" + sheetName + "]. Please use the latest test data template and try again." );
				}
				
				location = helper.hydrateLocation(functionCode, appId, locationName);
				
				if(location == null){
					logger.error("Invalid Location Name --> [{}]. No details in DB", locationName);
					throw new TestDataException("Could not find information about Page " + locationName + ". Please ensure your test data template is valid.");
				}
				
				logger.debug("Getting detail records on page {}", locationName);
				detailRecords = this.scanSheetForData(sheet, tdGid, functionCode, appId, tdUid, helper, location);
				
				if(detailRecords != null && detailRecords.length() > 0){
					JSONObject page = location.toJson();
					
					page.put("DETAILRECORDS", detailRecords);
					pageAreas.put(page);
				}else{
					logger.info("Page {} has not fields for input/output. Skipping it", sheetName);
				}
			}
			
			logger.info("Finished processing all page areas");
			data = new JSONObject();
			data.put("TDUID", tdUid);
			data.put("PAGEAREAS", pageAreas);
		} catch(TestDataException e){
			throw e;
		}catch (DatabaseException e) {
			
			throw new TestDataException("Could not load test data - " + e.getMessage());
		} catch (JSONException e) {
			
			logger.error("JSONException caught", e);
			throw new TestDataException("Could not load test data due to an internal error. Please contact Tenjin Support.");
		} catch(Exception e){
			
			logger.error("Unknown Exception caught", e);
			throw new TestDataException("Could not load test data due to an internal error. Please contact Tenjin Support.");
		}
		
		
		return data;
	}
	
	private JSONArray scanSheetForData(Sheet sheet, String tdGid, String moduleCode, int appId, String tdUid, LearningHelper helper, Location location) throws TestDataException{
		JSONArray detailRecords = new JSONArray();
		
		
		if(sheet == null){
			logger.error("ERROR in scanSheetForData -- sheet is null");
			return null;
		}
		
		logger.debug("Processing sheet [{}] for Location [{}]", sheet.getSheetName(), location.getLocationName());
		
		try {
			Row masterRow = null;
			Map<String, Row> tcRowMap = new TreeMap<String, Row>(); //Map that contains the Row Number as the key, and the detail row for the TDUID as the value
			
			logger.debug("Getting all rows with TDGID [{}] and TDUID [{}] on sheet [{}]", tdGid, tdUid, sheet.getSheetName());
			Iterator<Row> rowIter = sheet.iterator();
			String aTdDrId = "";
			while(rowIter.hasNext()){
				Row row = rowIter.next();
				
				if(getCellValue(row, TDGIDCOL).equalsIgnoreCase(tdGid) && getCellValue(row, TDUIDCOL).equalsIgnoreCase(tdUid)){
					aTdDrId = getCellValue(row, TDDRIDCOL);
					if(aTdDrId.equalsIgnoreCase("1") || aTdDrId.equalsIgnoreCase("")){
						masterRow = row;
					}
					
					tcRowMap.put(aTdDrId, row);
				}
			}
			
			if(masterRow == null){
				logger.error("Could not identify Master Record row for TDGID {}, TDUID {}", tdGid, tdUid);
				return new JSONArray();
			}
			
			logger.info("Processing master row");
			JSONArray fields = this.getTestDataFromRow(sheet, masterRow, appId, moduleCode, helper, location);
			
			String masterDrId = getCellValue(masterRow, TDDRIDCOL);
			JSONObject detailRecord = new JSONObject();
			detailRecord.put("DR_ID", masterDrId);
			detailRecord.put("FIELDS", fields);
			
			detailRecords.put(detailRecord);
			
			if(tcRowMap.keySet().size() > 1){
				logger.debug("Detail Records Found for this transaction");
				
				for(String drId:tcRowMap.keySet()){
					if(masterDrId.equalsIgnoreCase(drId)){
						continue;
					}
					
					logger.info("Processing detail record {} for Transaction {}", drId, tdUid);
					JSONArray drFields = this.getTestDataFromRow(sheet, tcRowMap.get(drId), appId, moduleCode, helper, location);
					
					JSONObject d = new JSONObject();
					d.put("DR_ID", drId);
					d.put("FIELDS", drFields);
					detailRecords.put(d);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR in scanSheetForData for page {}", sheet.getSheetName(), e );
			throw new TestDataException("An error occurred while scanning page " + sheet.getSheetName() + " for test data. Please contact Tenjin Support.");
		}
		
		
		return detailRecords;
	}
	
	private JSONArray getTestDataFromRow(Sheet sheet, Row masterRow, int appId, String moduleCode, LearningHelper lHelper, Location location) throws TestDataException{
		
		if(masterRow == null){
			logger.error("ERROR in getTestDataFromRow --> row is null");
			return null;
		}
		
		if(sheet == null){
			logger.error("ERROR in getTestDataFromRow --> sheet is null");
			return null;
		}
		
		try {
			logger.debug("Getting test data from row number [{}] on sheet [{}]", masterRow.getRowNum(), sheet.getSheetName());
			Row headerRow = sheet.getRow(HEADER_ROW_NUM);
			Iterator<Cell> cellIter = headerRow.cellIterator();
			
			Cell cell = null;
			
			String fieldName = "";
			String fieldValue = "";
			TestObject t = null;
			JSONArray fields = new JSONArray();
			while(cellIter.hasNext()){
				cell = cellIter.next();
				
				if(cell.getColumnIndex() >= DATA_START_COL){
					
					fieldName = cell.getStringCellValue();
					fieldValue = masterRow.getCell(cell.getColumnIndex(), Row.CREATE_NULL_AS_BLANK).getStringCellValue();
					fieldValue = getCellValue(masterRow, cell.getColumnIndex());
					logger.debug("Processing field {}, value {}", fieldName, fieldValue);
					
					fieldValue = this.resolve(fieldValue);
					
					if(fieldValue.length() <= 0){
						logger.debug("Value for field {} on page {} is empty. Skipping it", fieldName, sheet.getSheetName());
						continue;
					}
					
					String oLabel = "";
					if(fieldName.equalsIgnoreCase("button")){
						oLabel = fieldValue;
					}else{
						oLabel = fieldName;
					}
					
					logger.debug("Hydrating test object for field {}", fieldName);
					t = lHelper.hydrateTestObject(moduleCode, location.getLocationName(), appId, oLabel);
					
					
				
					if(t == null){
						logger.error("TestObject for field {} on page {} is null", fieldName, sheet.getSheetName());
						throw new TestDataException("Could not find information about field " + fieldName + " on page " + sheet.getSheetName() + ". Re-learning of function may be required");
					}
					
					if(fieldName.equalsIgnoreCase("button")){
						t.setLabel("Button");
					}
					
					t.setData(fieldValue);
					
					logger.debug("Ascertaining action");
					
					short bgColor = getCellFillForegroundColor(masterRow, cell.getColumnIndex());
					
					if(!location.isMultiRecordBlock()){
						if(bgColor == QUERY_FIELD_COLOR){
							t.setAction("QUERY");
						}else if(bgColor == VERIFY_FIELD_COLOR){
							t.setAction("VERIFY");
						}else if(bgColor == ADD_FIELD_COLOR){
							t.setAction("ADD");
						}else if(bgColor == DELETE_FIELD_COLOR){
							t.setAction("DELETE");
						}else{
							t.setAction("INPUT");
						}
					}else{
						short headerBgColor = getCellFillForegroundColor(headerRow, cell.getColumnIndex());
						if(headerBgColor == QUERY_FIELD_COLOR){
							t.setAction("QUERY");
						}else if(bgColor == VERIFY_FIELD_COLOR){
							t.setAction("VERIFY");
						}else if(bgColor == ADD_FIELD_COLOR){
							t.setAction("ADD");
						}else if(bgColor == DELETE_FIELD_COLOR){
							t.setAction("DELETE");
						}else{
							t.setAction("INPUT");
						}
					}
					
					
					
					logger.debug("Preparing JSON for field {}", fieldName);
					JSONObject field = t.toJson();
					fields.put(field);
					
				}
			}
			
			return fields;
		} catch (DatabaseException e) {
			
			throw new TestDataException(e.getMessage(), e);
		} catch (SQLException e) {
			
			throw new TestDataException(e.getMessage(), e);
		} catch (Exception e) {
			
			throw new TestDataException("Could not process test case row due to an internal error. Please contact Tenjin Support.");
		}
	
	}
	
	private static short getCellFillForegroundColor(Row row, int colIndex){
		if(row == null){
			logger.error("ERROR in getting cell foreground color - row is null");
			return 0;
		}
		
		try{
			Cell cell = row.getCell(colIndex, Row.CREATE_NULL_AS_BLANK);
			return  cell.getCellStyle().getFillForegroundColor();
		}catch(Exception e){
			logger.error("ERROR in getCellFillForegroundColor for cell with index {} on rowo {}", colIndex, row.getRowNum(), e);
			return 0;
		}
		
		
	}
	
	private void generateListCell(String options, int startRow, int startCol, XSSFSheet sheet){

		if(Utilities.trim(options).length() > 250){
			logger.warn("List cell not generated due to options exceeing maximum length");
			return;
		}

		DataValidation dataValidation = null;
		DataValidationConstraint constraint = null;
		DataValidationHelper validationHelper = null;
		validationHelper=new XSSFDataValidationHelper(sheet);
		CellRangeAddressList addressList = new  CellRangeAddressList(startRow,500,startCol,startCol);
		constraint =validationHelper.createExplicitListConstraint(options.split(";"));
		dataValidation = validationHelper.createValidation(constraint, addressList);
		dataValidation.setSuppressDropDownArrow(true);    
		sheet.addValidationData(dataValidation);
	}

	private static CellStyle getVerifyCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getPageNameCellStyle(Workbook wb){
		CellStyle style = null;
		style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);
		return style;
	}
	
	private static CellStyle getPageNameMergedCellStyle(Workbook wb){
		CellStyle style = null;
		style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);
		return style;
	}
	
	private static CellStyle getPageNameSubscriptCellStyle(Workbook wb){
		CellStyle style = null;
		style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		font.setFontHeightInPoints((short)8);
		style.setFont(font);
		return style;
	}
	
	private static CellStyle getQueryCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);

		//headerFont.setColor(IndexedColors.BLACK.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getAddCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);

		//headerFont.setColor(IndexedColors.BLACK.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.LAVENDER.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getDeleteCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.WHITE.getIndex());
		//headerFont.setColor(IndexedColors.BLACK.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getHeaderRowStyle(Workbook wb){

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getMandatoryFieldStyle(Workbook wb){

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.DARK_RED.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	public static void main(String[] args) {
		TestDataHandler handler = new SpreadsheetDataHandler();
		
		try {
			handler.generateTemplate(1, "NEWCUST", "D:\\", "NEWCUST_TTD_new.xlsx", false, false);
		} catch (TestDataException e) {
			
			logger.error("Error ", e);
		}
	}

	@Override
	public List<String> getAllTdUidsInRange( String absoluteFilePath, String tdGid, int dataLimit) throws TestDataException {
		
		logger.info("Loading workbook from path [{}]", absoluteFilePath);
		
		try {
			this.workbook = new XSSFWorkbook(new FileInputStream(new File(absoluteFilePath)));
		} catch (FileNotFoundException e) {
			
			logger.error(e.getMessage());
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		} catch (IOException e) {
			
			logger.error("IOException caught", e);
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		}
		
		
		//Map<String, List<String>> map = new TreeMap<String, List<String>>();
		List<String> tdUids = new ArrayList<String>();
		
		try{
			
			logger.info("Getting all TDUIDs for TDGID [{}]", tdGid);
			logger.debug("Loading first sheet");
			
			Sheet sheet = this.workbook.getSheetAt(0);
			int currentRow = DATA_START_ROW_NUM-1;
			boolean loopOk = true;
			while(loopOk && tdUids.size() < dataLimit){
				currentRow++;
				logger.debug("Scanning row [{}]", currentRow);
				Row row = sheet.getRow(currentRow);
				if(row == null){
					loopOk = false;//No more rows. End the loop
					logger.debug("No more rows..");
					continue;
				}
				
				Cell gidCell = row.getCell(TDGIDCOL);
				if(gidCell == null){
					loopOk = false;//No more rows. End the loop
					logger.debug("No more rows..");
					continue;
				}
				
				gidCell.setCellType(Cell.CELL_TYPE_STRING);
				
				String gid = gidCell.getStringCellValue();
				if(gid == null || gid.equalsIgnoreCase("")){
					loopOk = false;//No more rows. End the loop
					logger.debug("No more rows..");
					continue;
				}
				String uid = "";
				if(gid.equalsIgnoreCase(tdGid)){
					Cell uidCell = row.getCell(TDUIDCOL);
					if(uidCell != null){
						uidCell.setCellType(Cell.CELL_TYPE_STRING);
						uid = uidCell.getStringCellValue();
						if(uid == null || Utilities.trim(uid).equalsIgnoreCase("")){
							logger.error("TDUID not specified in row {} in Test data file for TDGID {}",currentRow, tdGid);
							continue;
						}
					}else{
						logger.error("ERROR in row {} in Test data file for TDGID {} --> TDUID seems to be invalid",currentRow,  tdGid);
						continue;
					}
					if(!tdUids.contains(uid)){
						logger.debug("Found TDUID [{}]", uid);
						tdUids.add(uid);
					}
					
				}
			}
		}catch(Exception e){
			logger.error("ERROR getting all TDUIDs for TDGID [{}]\n File Path --> [{}]", tdGid, absoluteFilePath, e);
			throw new TestDataException("Could not validate test data for this step due to an internal error. Contact Tenjin Support.");
		}
		
		
		return tdUids;
	}
	
	private static String getCellValue(Cell cell){
		if(cell != null){
			cell.setCellType(Cell.CELL_TYPE_STRING);
			return Utilities.trim(cell.getStringCellValue());
		}else{
			return "";
		}
	}
	
	private static String getCellValue(Row row, int colIndex){
		
		if(row == null){
			logger.error("ERROR in getting cell value - row is null");
			return "";
		}
		try{
			Cell cell = row.getCell(colIndex, Row.CREATE_NULL_AS_BLANK);
			cell.setCellType(Cell.CELL_TYPE_STRING);
			return Utilities.trim(cell.getStringCellValue());
		}catch(Exception e){
			logger.error("ERROR getting value of cell with index {} on row {}", colIndex, row.getRowNum(), e);
			return "";
		}
	}
	
	
	
	private String resolve(String dataval) throws TestDataException{

		
		String output = dataval;
		String data = dataval;
		
		ArrayList<String> params = new ArrayList<String>();
		if(dataval != null && dataval.startsWith("<<") && dataval.endsWith(">>")){
			dataval = dataval.replace("<<","");
			dataval = dataval.replace(">>","");
			String[] split = dataval.split(";;");
			if(split.length == 1){
				params.add(split[0]);
			}else{
				params.add(split[0]);
				for(int i=1;i<split.length;i++){
					String s = split[i];
					params.add(s);
				}
			}
		}else if (data.startsWith("@@ONLINE_EXCHANGE_RATE")){
			String param = data.substring(data.indexOf("(")+1, data.indexOf(")"));
			String[] eachParam = param.split(",");
			String brn = null, ccy1 = null, ccy2 = null, rateCode = null, t;
			for (int i=0; i<eachParam.length; i++) {
				t = eachParam[i];
				String[] ti = t.split("-");
				if (ti[0].equalsIgnoreCase("BRN"))
					brn = ti[1];
				else if (ti[0].equalsIgnoreCase("CCY1"))
					ccy1 = ti[1];
				else if (ti[0].equalsIgnoreCase("CCY2"))
					ccy2 = ti[1];
				else if (ti[0].equalsIgnoreCase("RATECODE"))
					rateCode = ti[1];
			}
			CurrencyServiceClient cs = new CurrencyServiceClient();
			Float rate = cs.getRate(brn, ccy1, ccy2, rateCode);
			output = String.valueOf(rate);
		}else{
			//Means no need to resolve anything. This is a direct input
			return dataval;
		}
		
		if(params != null && params.size() == 1){
			//Invalid usage. It means we have a TDUID, but no idea about which fiedl value we should take. So log error and throw exception
			logger.error("ERROR invalid usage. Data specified is {} but expecting a field name to fetch",dataval);
			throw new TestDataException("Invalid Syntax to get runtime output. Right syntax is <<TDUID(FIELD_IDENTIFIER)>>");
		}
		
		if(params != null && params.size() > 1){
			//Value needs to be resolved
			String tdUid = params.get(0);
			String fieldIdentifier = params.get(1);
			
			
			Connection projConn = null;
			try{
				projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				output = new LearningHelper().getRuntimeFieldOutput(projConn, tdUid, fieldIdentifier);
				logger.info("Output Avaliable is : " + output);
			}catch(DatabaseException e){
				throw new TestDataException(e.getMessage(),e);
			}catch(Exception e){
				logger.error("ERROR resolving field value {}", dataval,e);
				throw new TestDataException("Could not resolve value for " + dataval + " due to an internal error");
			}finally{
				try{
					projConn.close();
				}catch(Exception ignore){}
			}
						
			return output;
		}else{
			return dataval;
		}
		
		
	
	}

}
*/


/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SpreadsheetDataHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 10-Sep-2016           Sriram Sridharan          Newly Added For 
 * 20-Sep-2016			Nagababu				Entire class changed to fix Multiple levels of Multi-record blocks
 * 18-Nov-2016         Nagareddy			 Manual Input field
 * 16-June-2017         Padmavathi			 TENJINCG-201
 * 19-Aug-2017          Gangadhar Badagi        T25IT-229
 */

package com.ycs.tenjin.bridge.oem.gui.datahandler;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.poifs.crypt.CipherAlgorithm;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.crypt.EncryptionMode;
import org.apache.poi.poifs.crypt.Encryptor;
import org.apache.poi.poifs.crypt.HashAlgorithm;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.exceptions.TestDataException;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ExtractionRecord;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.LearningHelper;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;
public class GuiDataHandlerImpl implements GuiDataHandler {
	public static final Logger logger = LoggerFactory.getLogger(GuiDataHandlerImpl.class);

	private Workbook workbook;
	private static final int TDGIDCOL=0;
	private static final int TDUIDCOL=1;
	private static final int TDDRIDCOL=2;
	private static final int DATA_START_COL=3;

	private static final int HEADER_ROW_NUM=4;
	private static final int DATA_START_ROW_NUM=5;
	private static final int LEGEND_ROW_NUM=2;
	private static final int SHEET_NAME_ROW_NUM=0;
	private static final int SHEET_NAME_CELL_NUM=1;

	private static final short MRB_PAGE_SHEET_COLOR=IndexedColors.LIGHT_YELLOW.getIndex();
	private static final short QUERY_FIELD_COLOR = IndexedColors.LIGHT_ORANGE.getIndex();
	
	
	@SuppressWarnings("unused")
	private Map<String, Object> locationsMap;
	private Map<String, Object> fieldsMap;
	
	private Boolean forExtraction = false; 
	public void enableExtraction(Boolean forExtraction) {
		this.forExtraction = forExtraction;
	}
	
	public Workbook getWorkbook() {
		return workbook;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}
	private Map<String, Boolean> locMap;

	@Override
	public void generateTemplate(int applicationId, String functionCode, String destinationFolderPath, String templateFileName, boolean mandatoryOnly, boolean editableOnly) throws TestDataException {
		

		LearningHelper helper = new LearningHelper();
		try {
			List<Location> locations = helper.hydrateMetadata(applicationId, functionCode, mandatoryOnly, editableOnly);
			
			
			this.generateTemplate(applicationId, functionCode, destinationFolderPath, templateFileName, locations);

		} catch (DatabaseException e) {
			
			throw new TestDataException(e.getMessage());
		} 

		

	}

	@Override
	public void generateTemplate(int applicationId, String functionCode, String destinationFolderPath, String templateFileName,	JSONArray manifest) {
		
		List<Location> listLocation=new ArrayList<Location>();
		Location loc;
		try{
			for(int i=0;i<manifest.length();i++)
			{		JSONObject jsonObj1=manifest.getJSONObject(i);
					 loc=new Location();
					loc.setLocationName((String)(jsonObj1.get("name")));					
					loc.setLocationType((String)(jsonObj1.get("type")));
					if(jsonObj1.get("ismrb").equals("T"))
						loc.setMultiRecordBlock(true);
					else 
						loc.setMultiRecordBlock(false);
					List<TestObject> listTestObject=new ArrayList<TestObject>();					
					JSONArray jsonArr=(JSONArray)jsonObj1.get("fields");					
					for(int j=0;j<jsonArr.length();j++)
					{		JSONObject jsonObj2=jsonArr.getJSONObject(j);
							TestObject testObj=new TestObject();
							testObj.setDefaultOptions(jsonObj2.get("default_option") != null && !((String)jsonObj2.get("default_option")).equalsIgnoreCase("null")? (String)jsonObj2.get("default_option") : null);
							testObj.setMandatory((String)jsonObj2.get("mandatory"));
							testObj.setName((String)jsonObj2.get("name"));
							testObj.setIdentifiedBy((String)jsonObj2.get("type"));
							testObj.setObjectClass(jsonObj2.getString("object_class"));
							testObj.setUserRemarks(jsonObj2.getString("userRemarks"));
							listTestObject.add(testObj);
					}
					loc.setTestObjects(listTestObject);
					listLocation.add(loc);
			}
			
		}
		catch(Exception e)
		{	logger.error("Error ", e);
			logger.error("json Exception occured during extracting fields");
		}
	
		try {
			this.generateTemplate(applicationId, functionCode, destinationFolderPath, templateFileName, listLocation);
		} catch (TestDataException e) {
			
			logger.error("Error ", e);
		}

	}


	private void generateTemplate(int applicationId, String functionCode, String destinationFolderPath, String templateFileName, List<Location> locations) throws TestDataException{
		logger.debug("Creating new workbook");
		
		XSSFWorkbook workbook1;
		try {
		   	workbook1	=new XSSFWorkbook(this.getClass().getClassLoader().getResourceAsStream("TTD_Template.xlsx"));
			logger.info("workbook created");
			} catch (IOException e1) {
			logger.error(" Help WorkSheet does not exist");
			workbook1 = new XSSFWorkbook();
		}
		catch (NullPointerException e1) {
			logger.error(" Help WorkSheet does not exist");
	    	workbook1 = new XSSFWorkbook();
		}
			this.workbook = workbook1;
		boolean workbookHasSheets=this.generateTemplate(this.workbook, applicationId, functionCode, destinationFolderPath, templateFileName, locations);
		
		if(workbookHasSheets){
			String endFilePath = "";
			if(!Utilities.trim(templateFileName).endsWith(".xlsx")){
				templateFileName = Utilities.trim(templateFileName) + ".xlsx";
			}
			endFilePath = destinationFolderPath +File.separator+ templateFileName;
			logger.debug("Writing file to disk");
			try{
			
				OutputStream out = new FileOutputStream(endFilePath);
				this.workbook.write(out);
				String pwd=new LearningHelper().hydrateTemplatePassword(applicationId);
				if(pwd!=null && !pwd.equalsIgnoreCase("")){
					POIFSFileSystem fs = new POIFSFileSystem();
					EncryptionInfo info = new EncryptionInfo(fs, EncryptionMode.agile, CipherAlgorithm.aes192, HashAlgorithm.sha256, -1, -1, null);
					Encryptor enc = info.getEncryptor();
		        
					/*Modified by paneendra for VAPT FIX starts*/
					/*pwd=new Crypto().decrypt(pwd.split("!#!")[1],decodeHex(pwd.split("!#!")[0].toCharArray()));*/
					pwd=new CryptoUtilities().decrypt(pwd);
					/*Modified by paneendra for VAPT FIX ends*/
					enc.confirmPassword(pwd);
					OPCPackage opc = OPCPackage.open(new File(endFilePath), PackageAccess.READ_WRITE);
					OutputStream os = enc.getDataStream(fs);
		        	opc.save(os);
		        	opc.close();

		        	out = new FileOutputStream(endFilePath);
		        	fs.writeFilesystem(out);
				}
				logger.debug("File saved in path {}",endFilePath);
			}catch(Exception e){
				logger.error("ERROR while writing template to disk", e);
				throw new TestDataException("Could not generate Test data template due to an internal error. Please contact Tenjin support");
			}
		}
		
	}
	
	private boolean generateTemplate(Workbook workbook, int applicationId, String functionCode, String destinationFolderPath, String templateFileName, List<Location> locations) throws TestDataException{

		logger.debug("Loading styles");
		CellStyle hStyle = getHeaderRowStyle(workbook);
		CellStyle mStyle = getMandatoryFieldStyle(workbook);
		CellStyle queryCellStyle = getQueryCellStyle(workbook);
		CellStyle addCellStyle = getAddCellStyle(workbook);
		CellStyle deleteCellStyle = getDeleteCellStyle(workbook);
		CellStyle verifyCellStyle = getVerifyCellStyle(workbook);
	
		boolean workbookHasSheets = false;
		for(Location location:locations){

			logger.debug("Processing location --> [{}]", location.getLocationName());
			if(location.getTestObjects() != null && location.getTestObjects().size() > 0){
				String sheetName= "";
				
				String updateLocation = "";
				updateLocation = location.getLocationName().replace("/","");
				updateLocation = updateLocation.replace("*","");
				updateLocation = updateLocation.replace("[","");
				updateLocation = updateLocation.replace("\\","");
				updateLocation = updateLocation.replace("]","");
				updateLocation = updateLocation.replace(":","");
				updateLocation = updateLocation.replace("?","");
				
				if(Utilities.trim(updateLocation).length() > 30){
					logger.debug("Location [{}] has name of length more than 30 characters");
				
					sheetName=Utilities.trim(updateLocation.substring(0, 26))+String.format("%03d", location.getSequence()) ;
					if(sheetName.contains("/")){
						sheetName=sheetName.replace("/","");
					}
					
				}else{
					sheetName = updateLocation;
				}

				XSSFSheet sheet = (XSSFSheet) workbook.createSheet(sheetName);
				workbookHasSheets = true;
				if(location.isMultiRecordBlock()){
					sheet.setTabColor(MRB_PAGE_SHEET_COLOR);
				}
				
				Row sheetNameRow = null;
				sheetNameRow = sheet.createRow(SHEET_NAME_ROW_NUM);
				sheetNameRow.createCell(0).setCellValue("Page Name:");
				sheetNameRow.getCell(0).setCellStyle(getPageNameCellStyle(workbook));
				sheetNameRow.createCell(1).setCellValue(location.getLocationName());
				sheetNameRow.getCell(1).setCellStyle(getPageNameMergedCellStyle(workbook));
				CellRangeAddress region = new CellRangeAddress(0,0,1,6);
				sheet.addMergedRegion(region);
				RegionUtil.setBorderBottom(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderTop(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderLeft(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderRight(CellStyle.BORDER_THICK, region, sheet, workbook);
				
				
				sheetNameRow.createCell(7).setCellValue("Do not change this value. It may result in errors during execution");
				sheetNameRow.getCell(7).setCellStyle(getPageNameSubscriptCellStyle(workbook));
				region = new CellRangeAddress(0,0,7,11);
				sheet.addMergedRegion(region);
				
				Row legendRow = null;
				Row headerRow = null;
				logger.debug("Writing legend");
				legendRow = sheet.createRow(LEGEND_ROW_NUM);
				legendRow.createCell(0).setCellValue("Legend");
				legendRow.createCell(1).setCellValue("INPUT");
				legendRow.createCell(2).setCellValue("QUERY");
				legendRow.createCell(3).setCellValue("ADD");
				legendRow.createCell(4).setCellValue("DELETE");
				legendRow.createCell(5).setCellValue("VERIFY");
				
				logger.debug("Applying styles for legend");
				legendRow.getCell(2).setCellStyle(queryCellStyle);
				legendRow.getCell(3).setCellStyle(addCellStyle);
				legendRow.getCell(4).setCellStyle(deleteCellStyle);
				legendRow.getCell(5).setCellStyle(verifyCellStyle);
				
				logger.debug("Writing header row");
				headerRow = sheet.createRow(HEADER_ROW_NUM);
				headerRow.createCell(0).setCellValue("TDGID");
				headerRow.getCell(0).setCellStyle(hStyle);
				headerRow.createCell(1).setCellValue("TDUID");
				headerRow.getCell(1).setCellStyle(hStyle);
				headerRow.createCell(2).setCellValue("TDDRID");
				headerRow.getCell(2).setCellStyle(hStyle);
				
				int currentCol = DATA_START_COL;
				
				logger.debug("Writing fields");
				List<String> buttons = new ArrayList<String>();
					Font font = workbook.createFont();
					font.setFontName("Arial");
		            font.setFontHeightInPoints((short) 8);
		            
				for(TestObject field:location.getTestObjects()){
					if(field.getObjectClass().equalsIgnoreCase(FieldType.BUTTON.toString())){
						buttons.add(field.getName());
						continue;
					}
					
					if(field.getObjectClass().equalsIgnoreCase(FieldType.LINK.toString()) || 
							field.getObjectClass().equalsIgnoreCase(FieldType.TABLE.toString()) ||
							field.getObjectClass().equalsIgnoreCase(FieldType.TOOLBARITEM.toString())){
						logger.debug("Field [{}] -- Element Type [{}] will be skipped from template", field.getName(), field.getObjectClass());
						continue;
					}
					
					
					Cell cell=headerRow.createCell(currentCol);
					if(field.getUserRemarks()!=null && !field.getUserRemarks().equalsIgnoreCase("null")){
					Drawing drawing = cell.getSheet().createDrawingPatriarch();
					CreationHelper factory = cell.getSheet().getWorkbook().getCreationHelper();
					ClientAnchor anchor = factory.createClientAnchor();
					 anchor.setCol1(cell.getColumnIndex());
					 anchor.setCol2(cell.getColumnIndex()+3);
					 anchor.setRow1(cell.getRowIndex());
					 anchor.setRow2(cell.getRowIndex() +6);
				    Comment comment = drawing.createCellComment(anchor);
					 RichTextString string =factory.createRichTextString(field.getUserRemarks());
			         string.applyFont(font);
			         comment.setString(string);
			         cell.setCellComment(comment);
					}
					
					if(Utilities.trim(field.getMandatory()).equalsIgnoreCase("yes")){
						field.setName(field.getName().replace("<font color='red'><b>*</b></font>", ""));
					logger.debug("Adding field [{}]", field.getName());
					cell.setCellValue(field.getName());
					}else{
						logger.debug("Adding field [{}]", field.getName());
						
						cell.setCellValue(field.getName());
					}
					if(Utilities.trim(field.getMandatory()).equalsIgnoreCase("yes")){
						 logger.debug("Applying mandatory style for field [{}]", field.getName());
						headerRow.getCell(currentCol).setCellStyle(mStyle);
					}else if (field.getObjectClass().equalsIgnoreCase(FieldType.IMAGE.toString())) {
						headerRow.getCell(currentCol).setCellStyle(hStyle);
					}else{
						logger.debug("Applying normal style for field [{}]", field.getName());
						headerRow.getCell(currentCol).setCellStyle(hStyle);
					}
					
					
					if(!Utilities.trim(field.getDefaultOptions()).equalsIgnoreCase("")){
						logger.debug("Generating List Cell for field [{}]", field.getName());
						this.generateListCell(field.getDefaultOptions(), headerRow.getRowNum()+1, currentCol, sheet);
					}
				
					DataFormat fmt = workbook.createDataFormat();
					CellStyle textStyle = workbook.createCellStyle();
					textStyle.setDataFormat(fmt.getFormat("@"));
					sheet.setDefaultColumnStyle(currentCol, textStyle);
					int value;
					try {
						value = TenjinConfiguration.getProperty("MIN_EXCEL_VERSION").compareTo("2007");
						/*modified by paneendra for VAPT fix starts*/
						if(value>= 0){
						/*modified by paneendra for VAPT fix ends*/
							sheet.autoSizeColumn(currentCol);
						}
					} catch (TenjinConfigurationException e) {
						
						logger.error("Error ", e);
					}
					currentCol++;
				}
				
				String allButtons = "";
				if(buttons.size() > 0){
					headerRow.createCell(currentCol).setCellValue("Button");
					headerRow.getCell(currentCol).setCellStyle(hStyle);
					int counter=1;
					for(String button:buttons){
						allButtons = allButtons + button;
						if(counter < buttons.size()){
							allButtons = allButtons + ",";
						}
					}
					this.generateListCell(allButtons, headerRow.getRowNum()+1, currentCol, sheet);
				}
				
			}else{
				logger.debug("No fields found in location. This will be skipped");
			}
		}
		
		
		return workbookHasSheets;
	}
	
	public boolean generateTemplate(Workbook workbook, int applicationId, String functionCode, String destinationFolderPath, String templateFileName, List<Location> locations, String entityType) throws TestDataException{

		
		logger.debug("Loading styles");
		CellStyle hStyle = getHeaderRowStyle(workbook);
		CellStyle mStyle = getMandatoryFieldStyle(workbook);
		CellStyle verifyCellStyle = getVerifyCellStyle(workbook);

		boolean workbookHasSheets = false;
		
		
		String locationPrefix=null;
		
		for(Location location:locations){
			if(location.getResponseType()!=0 && location.getResponseType()!=-1){
				locationPrefix = "(" + entityType + "_"+location.getResponseType()+")_";
			}
			/*	Added By Paneendra for TENJINCG-1239 starts */
			else if(location.getResponseType()==-1) {
				locationPrefix = "(" + entityType +"_"+ ")_Header";
			}/*	Added By Paneendra for TENJINCG-1239 ends */
			else{
				locationPrefix = "(" + entityType + ")_";
			}
			logger.debug("Processing location --> [{}]", location.getLocationName());
			if(location.getTestObjects() != null && location.getTestObjects().size() > 0){
				String sheetName= "";
				if(Utilities.trim(locationPrefix + location.getLocationName()).length() > 30){
					logger.debug("Location [{}] has name of length more than 30 characters");
					sheetName = locationPrefix + "Page_Area_" + location.getSequence();
				}else{
					sheetName = locationPrefix + location.getLocationName();
				}

				XSSFSheet sheet =null;
				try {
					sheet = (XSSFSheet) workbook.createSheet(sheetName);
				} catch (Exception e) {
					
					logger.error("Error ", e);
				}
				
				if(sheet == null) {
					continue;
				}
				workbookHasSheets = true;
				if(location.isMultiRecordBlock()){
					sheet.setTabColor(MRB_PAGE_SHEET_COLOR);
				}
				
				Row sheetNameRow = null;
				sheetNameRow = sheet.createRow(SHEET_NAME_ROW_NUM);
				sheetNameRow.createCell(0).setCellValue("Page Name:");
				sheetNameRow.getCell(0).setCellStyle(getPageNameCellStyle(workbook));
				/*Modified by Pushpa for Tenj210-120 starts */
				/*sheetNameRow.createCell(1).setCellValue(sheetName);*/
				sheetNameRow.createCell(1).setCellValue(location.getLocationName());
				/*Modified by Pushpa for Tenj210-120 ends */
				sheetNameRow.getCell(1).setCellStyle(getPageNameMergedCellStyle(workbook));
				CellRangeAddress region = new CellRangeAddress(0,0,1,6);
				sheet.addMergedRegion(region);
				RegionUtil.setBorderBottom(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderTop(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderLeft(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderRight(CellStyle.BORDER_THICK, region, sheet, workbook);
				
				
				sheetNameRow.createCell(7).setCellValue("Do not change this value. It may result in errors during execution");
				sheetNameRow.getCell(7).setCellStyle(getPageNameSubscriptCellStyle(workbook));
				region = new CellRangeAddress(0,0,7,11);
				sheet.addMergedRegion(region);
				
				Row legendRow = null;
				Row headerRow = null;
				logger.debug("Writing legend");
				legendRow = sheet.createRow(LEGEND_ROW_NUM);
				legendRow.createCell(0).setCellValue("Legend");
				legendRow.createCell(1).setCellValue("INPUT");
				
				legendRow.createCell(2).setCellValue("VERIFY");
				
				logger.debug("Applying styles for legend");
				
				legendRow.getCell(2).setCellStyle(verifyCellStyle);
				
				logger.debug("Writing header row");
				headerRow = sheet.createRow(HEADER_ROW_NUM);
				headerRow.createCell(0).setCellValue("TDGID");
				headerRow.getCell(0).setCellStyle(hStyle);
				headerRow.createCell(1).setCellValue("TDUID");
				headerRow.getCell(1).setCellStyle(hStyle);
				headerRow.createCell(2).setCellValue("TDDRID");
				headerRow.getCell(2).setCellStyle(hStyle);
				
				
				int currentCol = DATA_START_COL;
				
				logger.debug("Writing fields");
				List<String> buttons = new ArrayList<String>();
				for(TestObject field:location.getTestObjects()){
					if(field.getObjectClass().equalsIgnoreCase(FieldType.BUTTON.toString())){
						buttons.add(field.getName());
						continue;
					}
					
					if(field.getObjectClass().equalsIgnoreCase(FieldType.LINK.toString()) || 
							field.getObjectClass().equalsIgnoreCase(FieldType.TABLE.toString()) ||
							field.getObjectClass().equalsIgnoreCase(FieldType.TOOLBARITEM.toString())){
						logger.debug("Field [{}] -- Element Type [{}] will be skipped from template", field.getName(), field.getObjectClass());
						continue;
					}
					
					
					if(Utilities.trim(field.getMandatory()).equalsIgnoreCase("yes")){
						field.setName(field.getName().replace("<font color='red'><b>*</b></font>", ""));
					logger.debug("Adding field [{}]", field.getName());
					headerRow.createCell(currentCol).setCellValue(field.getName());
					}else{
						logger.debug("Adding field [{}]", field.getName());
						headerRow.createCell(currentCol).setCellValue(field.getName());
					}
					if(Utilities.trim(field.getMandatory()).equalsIgnoreCase("yes")){
						 logger.debug("Applying mandatory style for field [{}]", field.getName());
						headerRow.getCell(currentCol).setCellStyle(mStyle);
					}else if (field.getObjectClass().equalsIgnoreCase(FieldType.IMAGE.toString())) {
						headerRow.getCell(currentCol).setCellStyle(hStyle);
					}else{
						logger.debug("Applying normal style for field [{}]", field.getName());
						headerRow.getCell(currentCol).setCellStyle(hStyle);
					}
					
					
					if(!Utilities.trim(field.getDefaultOptions()).equalsIgnoreCase("")){
						logger.debug("Generating List Cell for field [{}]", field.getName());
						this.generateListCell(field.getDefaultOptions(), headerRow.getRowNum()+1, currentCol, sheet);
					}
					int value;
					try {
						value = TenjinConfiguration.getProperty("MIN_EXCEL_VERSION").compareTo("2007");
						/*modified by paneendra for VAPT fix starts*/
						if(value>= 0){
						/*modified by paneendra for VAPT fix ends*/
							sheet.autoSizeColumn(currentCol);
						}
					} catch (TenjinConfigurationException e) {
						
						logger.error("Error ", e);
					}
					currentCol++;
				}
				
				String allButtons = "";
				if(buttons.size() > 0){
					headerRow.createCell(currentCol).setCellValue("Button");
					headerRow.getCell(currentCol).setCellStyle(hStyle);
					int counter=1;
					for(String button:buttons){
						allButtons = allButtons + button;
						if(counter < buttons.size()){
							allButtons = allButtons + ",";
						}
					}
					this.generateListCell(allButtons, headerRow.getRowNum()+1, currentCol, sheet);
				}
				
			}else{
				logger.debug("No fields found in location. This will be skipped");
			}
		}
		
		
		return workbookHasSheets;
	}

	
	
	private void generateListCell(String options, int startRow, int startCol, XSSFSheet sheet){

		if(Utilities.trim(options).length() > 250){
			logger.warn("List cell not generated due to options exceeing maximum length");
			return;
		}

		DataValidation dataValidation = null;
		DataValidationConstraint constraint = null;
		DataValidationHelper validationHelper = null;
		validationHelper=new XSSFDataValidationHelper(sheet);
		CellRangeAddressList addressList = new  CellRangeAddressList(startRow,500,startCol,startCol);
		constraint =validationHelper.createExplicitListConstraint(options.split(";"));
		dataValidation = validationHelper.createValidation(constraint, addressList);
		dataValidation.setSuppressDropDownArrow(true);    
		sheet.addValidationData(dataValidation);
	}

	private static CellStyle getVerifyCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getPageNameCellStyle(Workbook wb){
		CellStyle style = null;
		style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);
		return style;
	}
	
	private static CellStyle getPageNameMergedCellStyle(Workbook wb){
		CellStyle style = null;
		style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);
		return style;
	}
	
	private static CellStyle getPageNameSubscriptCellStyle(Workbook wb){
		CellStyle style = null;
		style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		font.setFontHeightInPoints((short)8);
		style.setFont(font);
		return style;
	}
	
	private static CellStyle getQueryCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);


		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getAddCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);


		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.LAVENDER.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getDeleteCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.WHITE.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getHeaderRowStyle(Workbook wb){

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getMandatoryFieldStyle(Workbook wb){

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.DARK_RED.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	@Override
	public List<String> getAllTdUidsInRange( String absoluteFilePath, String tdGid, int dataLimit) throws TestDataException {
		
		logger.info("Loading workbook from path [{}]", absoluteFilePath);
		
		try {
			this.workbook = new XSSFWorkbook(new FileInputStream(new File(absoluteFilePath)));
		} catch (FileNotFoundException e) {
			
			logger.error(e.getMessage());
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		} catch (IOException e) {
			
			logger.error("IOException caught", e);
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		}
		
		
		List<String> tdUids = new ArrayList<String>();
		
		try{
			
			logger.info("Getting all TDUIDs for TDGID [{}]", tdGid);
			logger.debug("Loading first sheet");
			
			Sheet sheet=null;
			if(this.workbook.getSheetAt(0).getSheetName().equalsIgnoreCase("TTD_HELP")){
				sheet = this.workbook.getSheetAt(1);
				}
			else{
		      sheet = this.workbook.getSheetAt(0);
			}
			
			int currentRow = DATA_START_ROW_NUM-1;
			boolean loopOk = true;
			while(loopOk && tdUids.size() < dataLimit){
				currentRow++;
				logger.debug("Scanning row [{}]", currentRow);
				Row row = sheet.getRow(currentRow);
//				String sheetName=sheet.getSheetName();
				if(row == null){
					loopOk = false;
					logger.debug("No more rows..");
					continue;
				}
				
				Cell gidCell = row.getCell(TDGIDCOL);
				if(gidCell == null){
					loopOk = false;
					logger.debug("No more rows..");
					continue;
				}
				
				gidCell.setCellType(Cell.CELL_TYPE_STRING);
				
				String gid = gidCell.getStringCellValue();
				if(gid == null || gid.equalsIgnoreCase("")){
					loopOk = false;//No more rows. End the loop
					logger.debug("No more rows..");
					continue;
				}
				String uid = "";
				
				if(gid.trim().equalsIgnoreCase(tdGid)){
					Cell uidCell = row.getCell(TDUIDCOL);
					if(uidCell != null){
						uidCell.setCellType(Cell.CELL_TYPE_STRING);
						uid = uidCell.getStringCellValue();
						if(uid == null || Utilities.trim(uid).equalsIgnoreCase("")){
							logger.error("TDUID not specified in row {} in Test data file for TDGID {}",currentRow, tdGid);
							continue;
						}
					}else{
						logger.error("ERROR in row {} in Test data file for TDGID {} --> TDUID seems to be invalid",currentRow,  tdGid);
						continue;
					}
					if(!tdUids.contains(uid)){
						logger.debug("Found TDUID [{}]", uid);
						tdUids.add(uid);
					}
					
				}
			}
		}catch(Exception e){
			logger.error("ERROR getting all TDUIDs for TDGID [{}]\n File Path --> [{}]", tdGid, absoluteFilePath, e);
			throw new TestDataException("Could not validate test data for this step due to an internal error. Contact Tenjin Support.");
		}
		
		
		return tdUids;
	}
	
	private static String getCellValue(Cell cell){
		if(cell != null){
			cell.setCellType(Cell.CELL_TYPE_STRING);
			return Utilities.trim(cell.getStringCellValue());
		}else{
			return "";
		}
	}
	
	private static String getCellValue(Row row, int colIndex){
		
		if(row == null){
			logger.error("ERROR in getting cell value - row is null");
			return "";
		}
		try{
			Cell cell = row.getCell(colIndex, Row.CREATE_NULL_AS_BLANK);
			cell.setCellType(Cell.CELL_TYPE_STRING);
			return Utilities.trim(cell.getStringCellValue());
		}catch(Exception e){
			logger.error("ERROR getting value of cell with index {} on row {}", colIndex, row.getRowNum(), e);
			return "";
		}
	}
	
	
	
	private String resolve(String dataval,String fieldName,int appId, String moduleCode) throws TestDataException, DatabaseException, ParseException{

		String dateFormat="";
		LearningHelper helper=new LearningHelper();
		String output = dataval;
		String startText="";
		String endText="";
		int startIndex=0;
		int endIndex=0;
		boolean runtimeFieldText=false;
		int iterationNum=0;
		ArrayList<String> params = new ArrayList<String>();
		
		 
		if(dataval != null && dataval.startsWith("<<") && dataval.endsWith(">>") ){
			int runtimeValCount=this.getCountForRuntimeValues(dataval);
			if(runtimeValCount==1) {
			dataval = dataval.replace("<<","");
			dataval = dataval.replace(">>","");
			String[] split = dataval.split(";;");
			if(split.length == 1){
				params.add(split[0]);
			}else{
				String processId=split[0];
				if(split[0].contains("(")){
				int index=processId.indexOf("(");
			
				iterationNum=Integer.parseInt(processId.substring(index+1, processId.length()-1));
				processId=processId.substring(0, index);
				}
				params.add(processId);
				for(int i=1;i<split.length;i++){
					String s = split[i];
					params.add(s);
				}
			}
			}else {
		    	 dataval=this.processMultipleRuntimeValues(dataval);
		     }
		}
		else if(dataval.contains("<<") && dataval.contains(">>")){
			int runtimeValCount=this.getCountForRuntimeValues(dataval);
			if(runtimeValCount==1) {
			runtimeFieldText=true;
			startIndex=dataval.indexOf("<");
			endIndex=dataval.indexOf(">");
			
			startText=dataval.substring(0,startIndex);
			endText=dataval.substring(endIndex+2,dataval.length());
			
			dataval=dataval.substring(startIndex, endIndex+2);
			
			dataval = dataval.replace("<<","");
			dataval = dataval.replace(">>","");
			String[] split = dataval.split(";;");
			if(split.length == 1){
				params.add(split[0]);
			}else{
				
				String processId=split[0];
				if(split[0].contains("(")){
				int index=processId.indexOf("(");
			
				iterationNum=Integer.parseInt(processId.substring(index+1, processId.length()-1));
				processId=processId.substring(0, index);
				}
				params.add(processId);
				for(int i=1;i<split.length;i++){
					String s = split[i];
					params.add(s);
				}
			}
		}else {
	    	 dataval=this.processMultipleRuntimeValues(dataval);
	     }
		}
		 
		
		if(params != null && params.size() == 1){
			logger.error("ERROR invalid usage. Data specified is {} but expecting a field name to fetch",dataval);
			throw new TestDataException("Invalid Syntax to get runtime output. Right syntax is <<TDUID(FIELD_IDENTIFIER)>>");
		}
		
		if(!(dataval.equalsIgnoreCase(""))&&(dataval.toLowerCase().contains("@@sendkeyspassword"))) {
			dataval=dataval.substring(19,dataval.length()-1);
		try {
			Aut applicationUser = new LearningHelper().hydrateAllApplicationUsers(appId, Utilities.trim(dataval));
			if(applicationUser==null) {
				throw new TestDataException("The Application User Name specified for run time password does not exists in Tenjin");
			}
			/*Modified by paneendra for VAPT FIX starts*/
			/*String password = new Crypto().decrypt(applicationUser.getPassword().split("!#!")[1],
                    decodeHex(applicationUser.getPassword().split("!#!")[0].toCharArray()));*/
			String password = new CryptoUtilities().decrypt(applicationUser.getPassword());
			/*Modified by paneendra for VAPT FIX ends*/
			dataval=Utilities.trim(password);
			dataval="@@sendkeys,"+dataval;
			logger.info(dataval);
		}catch (Exception e) {
			
			logger.error("Error ", e);
		}
			return dataval;
			
		}
		
		if(!(dataval.equalsIgnoreCase(""))&&(dataval.toLowerCase().contains("@@password"))) {
			dataval=dataval.substring(11,dataval.length()-1);
		try {
			Aut applicationUser = new LearningHelper().hydrateAllApplicationUsers(appId, Utilities.trim(dataval));
			if(applicationUser==null) {
				throw new TestDataException("The Application User Name specified for run time password does not exists in Tenjin");
			}
			/*Modified by paneendra for VAPT FIX starts*/
			/*String password = new Crypto().decrypt(applicationUser.getPassword().split("!#!")[1],
                    decodeHex(applicationUser.getPassword().split("!#!")[0].toCharArray()));*/
			String password = new CryptoUtilities().decrypt(applicationUser.getPassword());
			/*Modified by paneendra for VAPT FIX ends*/
			dataval=Utilities.trim(password);
			logger.info(dataval);
		}
		 catch (Exception e) {
			
			logger.error("Error ", e);
		}
			
			return dataval;
		}
		if(params != null && params.size() > 1){
			String tdUid = params.get(0);
			String fieldIdentifier = params.get(1);
			
			
			Connection projConn = null;
			try{
				projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				if(iterationNum!=0){
					output = new LearningHelper().getRuntimeFieldOutputForMRB(projConn, tdUid, fieldIdentifier, iterationNum);
				}else{
					output = new LearningHelper().getRuntimeFieldOutput(projConn, tdUid, fieldIdentifier);
				}
				logger.info("Output Avaliable is : " + output);
			}catch(DatabaseException e){
				throw new TestDataException(e.getMessage(),e);
			}catch(Exception e){
				logger.error("ERROR resolving field value {}", dataval,e);
				throw new TestDataException("Could not resolve value for " + dataval + " due to an internal error");
			}finally{
				try{
					projConn.close();
				}catch(Exception ignore){}
			}
			
			if(runtimeFieldText){
				
				output=startText+output+endText;
				
			}
			
			
			if (!(output.equalsIgnoreCase(""))&&(fieldName.contains("Date") || fieldName.contains("date"))&&(!fieldName.toLowerCase().contains("dateformat"))) {
				try{
				dateFormat = helper.fetchDateFormat(appId, moduleCode);
				 if(dateFormat!=null && !dateFormat.equalsIgnoreCase("") ){
				String defaultDateFormat = "dd/MM/yyyy";
				SimpleDateFormat defaultSdf = new SimpleDateFormat(
						defaultDateFormat);

				Date userEnteredDate = defaultSdf.parse(output);

				SimpleDateFormat targetSdf = new SimpleDateFormat(dateFormat);
				String targetDateString = targetSdf.format(userEnteredDate);
				output = Utilities.trim(targetDateString);
				 }else{
					 output = Utilities.trim(output);
				 }
			}
				catch(Exception e){
					throw new TestDataException("Tenjin is Unable to read the date value specified in TTD template for the Field"+fieldName);
					}
				}
			return output;		
			
		}else{
			if (!(output.equalsIgnoreCase(""))&&(fieldName.contains("Date") || fieldName.contains("date"))&&(!fieldName.toLowerCase().contains("dateformat"))) {
				 try{
				 dateFormat=	helper.fetchDateFormat(appId, moduleCode);
				 if(dateFormat!=null && !dateFormat.equalsIgnoreCase("") ){
				 String defaultDateFormat = "dd/MM/yyyy";
				 SimpleDateFormat defaultSdf = new SimpleDateFormat(defaultDateFormat);
				 
				 Date userEnteredDate = defaultSdf.parse(dataval);
				 
				 SimpleDateFormat targetSdf = new SimpleDateFormat(dateFormat);
				 String targetDateString = targetSdf.format(userEnteredDate);
				 dataval = Utilities.trim(targetDateString);
				 }
				 else{
					 dataval = Utilities.trim(dataval);
				 }
				 }
				 catch(Exception e){
					 throw new TestDataException("Tenjin is Unable to read the date value specified in TTD template for the Field"+fieldName);
				 }
			}
			return dataval;
		}
		
		
	
	}
	public String processMultipleRuntimeValues(String dataVal) throws TestDataException {
		String runtimeValue="";
		int starts=0;
		int ends=0;
		Boolean started=false;
		List<String> runtimeValues=new ArrayList<String>();
		int num=0;
		for (int i = 0; i < dataVal.length(); i++) {
			if(num!=0) {
				num=0;
				continue;
			}
			ArrayList<String> params = new ArrayList<String>();
			String s=String.valueOf(dataVal.charAt(i)); 
			if(s.equalsIgnoreCase("<")) {
				if(starts==0 && !started) {
				starts=i;
				}
				started=true;
			}else if(s.equalsIgnoreCase(">")) {
				if(started) {
					num++;
					started=false;
					
					ends=i+2;
					String runTimeValueToProcess=dataVal.substring(starts, ends);
					runTimeValueToProcess = runTimeValueToProcess.replace("<<","");
					runTimeValueToProcess = runTimeValueToProcess.replace(">>","");
					String[] split = runTimeValueToProcess.split(";;");
					if(split.length == 1){
						params.add(split[0]);
					}else{
					    params.add(split[0]);
						for(int j=1;j<split.length;j++){
							String st = split[j];
							params.add(st);
						}
					}
				     
					if(params != null && params.size() > 1){
						String tdUid = params.get(0);
						String fieldIdentifier = params.get(1);
						
						
						Connection projConn = null;
						try{
							projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
							runTimeValueToProcess = new LearningHelper().getRuntimeFieldOutput(projConn, tdUid, fieldIdentifier);
							
							logger.info("Output Avaliable is : " + runTimeValueToProcess);
						}catch(DatabaseException e){
							throw new TestDataException(e.getMessage(),e);
						}catch(Exception e){
							logger.error("ERROR resolving field value {}", runTimeValueToProcess,e);
							throw new TestDataException("Could not resolve value for " + runTimeValueToProcess + " due to an internal error");
						}finally{
							try{
								projConn.close();
							}catch(Exception ignore){}
						}
					}
					
					runtimeValues.add(starts+"#"+ends+"#"+runTimeValueToProcess);
				}
				starts=0;
			}
		}
		runtimeValue=this.concatRunTimeValues(dataVal,runtimeValues);
		return runtimeValue;
	}

	private String concatRunTimeValues(String dataVal, List<String> runtimeValues) {
		 String finalDataVal=dataVal;
		 Collections.reverse(runtimeValues);
		for(int i=0;i<runtimeValues.size();i++) {
			String runTimeValueToProcess=runtimeValues.get(i);
			
			String[] processArray=runTimeValueToProcess.split("#");
			String startindex=processArray[0];
			String endIndex=processArray[1];
			String processedValue=processArray[2];
			finalDataVal=finalDataVal.substring(0, Integer.parseInt(startindex))+processedValue+finalDataVal.substring(Integer.parseInt(endIndex));
			}
		return finalDataVal;
		
		
	}

	public int getCountForRuntimeValues(String dataVal) throws TestDataException {
	    int runtimeOpen=0;
	    int runtimeEnd=0;
	    int runtimeCount=0;
		for (int i = 0; i < dataVal.length(); i++) {
			String s=String.valueOf(dataVal.charAt(i)); 
			if(s.equalsIgnoreCase("<")) {
				runtimeOpen++;
			}else if(s.equalsIgnoreCase(">")) {
				runtimeEnd++;
			}
			
		}
		if(runtimeOpen!=0 && runtimeOpen==runtimeEnd) {
			runtimeCount=runtimeOpen/2;
		}else {
			throw new TestDataException("Syntax for Runtime value is not proper");
		}
	return runtimeCount;
	  
  }
	private ArrayList<Row> getSecondLevelDetailRowsForMaster(XSSFSheet sheet,String tdgid, String tduid, String detailRecordId, int dataStartRow) throws Exception{
		ArrayList<Row> detailRows = new ArrayList<Row>();
		
		try{
			int cRow = dataStartRow;
			while(sheet.getRow(cRow) != null && sheet.getRow(cRow).getCell(0) != null && sheet.getRow(cRow).getCell(0).getStringCellValue() != null && !sheet.getRow(cRow).getCell(0).getStringCellValue().equalsIgnoreCase("")){
				if(sheet.getRow(cRow).getCell(0).getStringCellValue().equalsIgnoreCase(tdgid) && sheet.getRow(cRow).getCell(1) != null && sheet.getRow(cRow).getCell(1).getStringCellValue().equalsIgnoreCase(tduid) && 
						sheet.getRow(cRow).getCell(2) != null && sheet.getRow(cRow).getCell(2).getStringCellValue() != null && !sheet.getRow(cRow).getCell(2).getStringCellValue().equalsIgnoreCase("")){
					String dId = sheet.getRow(cRow).getCell(2).getStringCellValue();
					if(dId.contains(".")){
						String[] dsplit = dId.split("\\.");
						if(dId.toLowerCase().startsWith(detailRecordId) && dsplit.length > 1 && dsplit.length < 4){
							detailRows.add(sheet.getRow(cRow));
						}
					}
				}
				
				cRow++;
			}
		}catch(Exception e){
			logger.error("Could not fetch second level detail rows for TDGID {}, TDUID {}, Detail Record ID {}", tdgid, tduid, detailRecordId);
			logger.error(e.getMessage(),e);
			throw new TestDataException(e.getMessage(),e);
		}
		
		return detailRows;
	}
	private int getTotalFieldsOnSheet(Sheet lSheet, int headerRow, int startColumnIndex){
		try{
			int totalFields = 0;
			int curCol = startColumnIndex;
			while(lSheet.getRow(headerRow) != null && lSheet.getRow(headerRow).getCell(curCol) != null && lSheet.getRow(headerRow).getCell(curCol).getStringCellValue() != null && !lSheet.getRow(headerRow).getCell(curCol).getStringCellValue().equalsIgnoreCase("")){
				totalFields++;
				curCol++;
			}
			
			return totalFields;
		}catch(Exception e){
			logger.error("ERROR getting number of fields on sheet");
			logger.error(e.getMessage(),e);
			return 0;
		}
	}
	private ArrayList<Row> getDetailRowsForMaster(Sheet sheet,String tdgid, String tduid, int masterStart) throws Exception{
		ArrayList<Row> detailRows = new ArrayList<Row>();
		int dRow = masterStart;
		try{
		while(sheet.getRow(dRow) != null && sheet.getRow(dRow).getCell(0) != null && sheet.getRow(dRow).getCell(0).getStringCellValue() != null && !sheet.getRow(dRow).getCell(0).getStringCellValue().equalsIgnoreCase("")){
			if(sheet.getRow(dRow).getCell(0).getStringCellValue().equalsIgnoreCase(tdgid) && sheet.getRow(dRow).getCell(1) != null && sheet.getRow(dRow).getCell(1).getStringCellValue().equalsIgnoreCase(tduid)){
				detailRows.add(sheet.getRow(dRow));
			}
			dRow++;
		} 
		
		}catch(Exception e){
			logger.error("ERROR fetching detail rows for TDGID {}, TDUID {}", tdgid, tduid);
			logger.error(e.getMessage(),e);
			throw new TestDataException(e.getMessage(),e);
		}
		return detailRows;
	}

	
	
	
	public JSONObject loadTestDataFromTemplate(String absoluteFilePath, int appId, String functionCode, String tdGid, String tdUid,int testCaseId,String testStepId) throws TestDataException {
	
		JSONObject data = null;
		
		try{
			logger.info("Loading workbook from path [{}]", absoluteFilePath);
			this.workbook = WorkbookFactory.create(new File(absoluteFilePath));
		}catch(Exception e){
			
			logger.error(e.getMessage());
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		}
		
		LearningHelper helper = new LearningHelper();
		int numberOfSheets = workbook.getNumberOfSheets();
		logger.info("Loading Test Data");
		logger.info("Number of sheets --> [{}]", numberOfSheets);
		
		Location location = null;
		
		try {
		
			data=this.scanTestData(workbook,tdGid, functionCode, appId, tdUid, helper, location,testCaseId,testStepId);
			
		} catch(TestDataException e){
			throw e;
		}catch (JSONException e) {
			
			logger.error("JSONException caught", e);
			throw new TestDataException("Could not load test data due to an internal error. Please contact Tenjin Support.");
		} catch(Exception e){
			
			logger.error("Unknown Exception caught", e);
			throw new TestDataException("Could not load test data due to an internal error. Please contact Tenjin Support.");
		}
		return data;
	}

	
	private JSONObject scanTestData(Workbook workbook, String tdGid, String moduleCode, int appId, String tdUid, LearningHelper helper, Location location,int testCaseId,String testStepId) throws TestDataException, JSONException{
		JSONArray detailRecords = new JSONArray();
		
		this.locMap =  new HashMap<String, Boolean>();
		int headerRow = 4;
		int dataStartCol = 3;
		int numberOfSheets=workbook.getNumberOfSheets();
		for(int i=0;i<numberOfSheets;i++){
			JSONObject page = new JSONObject();
			Sheet sheet=null;
			String sheetName=null;
			try {
				sheet = workbook.getSheetAt(i);
				sheetName=sheet.getSheetName();
				
				if(Utilities.trim(sheet.getSheetName()).equalsIgnoreCase("PAGE_AREA_INDEX") || Utilities.trim(sheet.getSheetName()).equalsIgnoreCase("TTD_HELP")){
					continue;
				}
				sheetName = sheet.getSheetName();
				logger.info("Processing sheet {}", sheetName);

				Row sheetNameRow = null;
				Cell sheetNameCell = null;
				sheetNameRow = sheet.getRow(SHEET_NAME_ROW_NUM);
				if(sheetNameRow == null){
					logger.error("SHEET NAME ROW not found. Invalid template");
					throw new TestDataException("Invalid Test Data Template - Page Name row was not found. Please use the latest test data template and try again");
				}

				sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);

				String locationName = getCellValue(sheetNameCell);
				if(locationName.equalsIgnoreCase("")){
					logger.error("LOCATION NAME TAMPERED WITH - Location Name is blank");
					throw new TestDataException("Invalid Test Data Template - Could not find Page Name in sheet [" + sheetName + "]. Please use the latest test data template and try again." );
				}
				location = helper.hydrateLocation(moduleCode, appId, locationName);

				if(location == null){
					logger.error("Invalid Location Name --> [{}]. No details in DB", locationName);
					throw new TestDataException("Could not find information about Page " + locationName + ". Please ensure your test data template is valid.");
				}

				logger.debug("Getting detail records on page {}", locationName);
				
				Map<String, String> manualField=null;
				try {
					manualField = helper.getManualInput(appId,moduleCode,locationName,testCaseId,testStepId);
				} catch (DatabaseException e1) {
					
					
				}
				

				Row masterRow = null;
				Map<String, Row> tcRowMap = new TreeMap<String, Row>(); //Map that contains the Row Number as the key, and the detail row for the TDUID as the value

				logger.debug("Getting all rows with TDGID [{}] and TDUID [{}] on sheet [{}]", tdGid, tdUid, sheet.getSheetName());
				Iterator<Row> rowIter = sheet.iterator();
				String aTdDrId = "";
				while(rowIter.hasNext()){
					Row row = rowIter.next();

					if(getCellValue(row, TDGIDCOL).equalsIgnoreCase(tdGid) && getCellValue(row, TDUIDCOL).equalsIgnoreCase(tdUid)){
						aTdDrId = getCellValue(row, TDDRIDCOL);
						if(aTdDrId.equalsIgnoreCase("1") || aTdDrId.equalsIgnoreCase("")){
							masterRow = row;
						}

						tcRowMap.put(aTdDrId, row);
					}
				}
				Location loc = null;
				loc=location;
				int tFields = this.getTotalFieldsOnSheet(sheet, headerRow, dataStartCol);
				int masterStart=0;
				int dRow=0;
				if(masterRow!=null)
				{
					masterStart = masterRow.getRowNum();
					dRow = masterStart;
				}				
				if(masterRow == null){
					logger.error("Could not identify Master Record row for TDGID {}, TDUID {}", tdGid, tdUid);
					continue;
				}
				else{
				
					JSONArray detailedRecords=this.getTestDataMultiLevelRecordBlock(sheet, tdGid, tdUid, dRow, appId, moduleCode, loc, tFields,manualField);
					if(detailedRecords!=null && detailedRecords.length()>0){
						page=location.toJson();
						JSONObject rec = detailedRecords.getJSONObject(0);
						page.put("DR_ID", rec.getString("DR_ID"));
						
						page.put("DETAILRECORDS", detailedRecords);
					}
					
				}
				if(page!=null && page.length()>0){
					detailRecords.put(page);
				}
			} catch (Exception e) {
				logger.error("ERROR in scanSheetForData for page {}", sheet.getSheetName(), e );
				
				throw new TestDataException(e.getMessage());
			}
		}
		
		JSONObject data = new JSONObject();
		data.put("TDUID", tdUid);
		data.put("PAGEAREAS", detailRecords);
		return data;
	}

	public JSONArray getTestDataMultiLevelRecordBlock(Sheet sheet,String tdGid,String tdUid,int dRow,int appId,String moduleCode,Location loc,int tFields,Map<String,String> manualField) throws Exception{
		this.locMap =  new HashMap<String, Boolean>();
		JSONArray detailRecords=new JSONArray();
		
		ArrayList<Row> detailRows = this.getDetailRowsForMaster(sheet, tdGid, tdUid, dRow);

		for(Row detailRow:detailRows){
			JSONObject detail = new JSONObject();
			String dRecordId = detailRow.getCell(2).getStringCellValue();
			detail.put("DR_ID", dRecordId);
			JSONArray fs = new JSONArray();
			fs=this.MultiRecordSingleLevel(loc, sheet, tdUid, tdGid, dRow, detailRow, tFields, moduleCode, appId);
			detail.put("FIELDS", fs);
			
			JSONArray detailRecord=this.MultiRecordMultiLevel(moduleCode, appId, loc, tdGid, tdUid, dRecordId);
			if(detailRecord!=null && detailRecord.length()>0){
				detail.put("DETAILRECORDS", detailRecord);	
			}
			if(detail!=null && detail.length()>0)
				detailRecords.put(detail);
		}
	
		return detailRecords;
	}
	public JSONArray MultiRecordSingleLevel(Location loc,Sheet sheet,String tdUid,String tdGid,int dRow,Row detailRow,int tFields,String moduleCode,int appId) throws Exception{
		int headerRow = 4;
		int dataStartCol = 3;
		String pName=loc.getLocationName();
		JSONArray fs=new JSONArray();	
		Connection conn=null;
		try{
			conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		for(int i=dataStartCol;i<dataStartCol+tFields;i++){
			Cell cell = detailRow.getCell(i);
			if(true){
				
				String fieldValue = "";
				if(this.forExtraction){
						if (cell != null && cell.getStringCellValue() != null
								&& !cell.getStringCellValue().equalsIgnoreCase("")) {
							fieldValue = cell.getStringCellValue();
						} else {
							fieldValue = "@@OUTPUT";
						}
				}else{
					if(cell != null && cell.getStringCellValue() != null && !cell.getStringCellValue().equalsIgnoreCase("")){
						fieldValue = cell.getStringCellValue();
					}else{
						continue;
					}
				}
				
				String fieldName = sheet.getRow(headerRow).getCell(i).getStringCellValue();
				
				String val = "";
				try{
					val = this.resolve(fieldValue,fieldName,appId,moduleCode);
				}catch(Exception e){
					throw new TestDataException(e.getMessage(),e);
				}
				TestObject t=null;
				if(fieldName.equalsIgnoreCase("Button"))
				{
					t=new TestObject();
					t.setLabel("Button");
					t.setObjectClass("Button");
					t.setUniqueId(val);
					t.setIdentifiedBy("Button");
					t.setLovAvailable(null);
				}
				else{
					t = new LearningHelper().hydrateTestObject(conn, moduleCode, pName, appId, fieldName);
				}
				if(t==null || t.getLabel() == null){
					throw new TestDataException("Information for field " + fieldName + " was not found in " + pName + " page in " + moduleCode + ". Re-learn this function or use the latest Test Data template");
				}
				JSONObject field = new JSONObject();
				field.put("DATA", val);
				field.put("FLD_LABEL", t.getLabel());
				field.put("FLD_TYPE",t.getObjectClass());
				field.put("FLD_UID", t.getUniqueId());
				field.put("FLD_UID_TYPE", t.getIdentifiedBy());
				field.put("FLD_NAME", fieldName);
				String hasLov = "";
				if(t.getLovAvailable() == null){
					hasLov = "N";
				}else{
					hasLov = t.getLovAvailable();
				}
				field.put("FLD_HAS_LOV", hasLov);
				
				if (cell == null) {
					field.put("ACTION", "INPUT");
				}else{
					if(!loc.isMultiRecordBlock()){
						short bgColor = cell.getCellStyle().getFillForegroundColor();
						if(bgColor == IndexedColors.GREEN.getIndex()){
							field.put("ACTION", "QUERY");
						}else if(bgColor == IndexedColors.YELLOW.getIndex()){
							field.put("ACTION", "VERIFY");
						}else{
							field.put("ACTION", "INPUT");
						}
					}else{
						Cell hCell = sheet.getRow(headerRow).getCell(i);
						Short headerBgColor = hCell.getCellStyle().getFillForegroundColor();
						Short bgColor = cell.getCellStyle().getFillForegroundColor();
						if(bgColor == IndexedColors.LAVENDER.getIndex()){
							field.put("ACTION","ADDROW");
						}else if(bgColor == IndexedColors.GREY_40_PERCENT.getIndex()){
							field.put("ACTION", "DELETEROW");
						}else if(bgColor == IndexedColors.YELLOW.getIndex()){
							field.put("ACTION", "VERIFY");
						}else{
							if(headerBgColor == IndexedColors.LIGHT_ORANGE.getIndex()){
								field.put("ACTION", "QUERY");
							}else{
								field.put("ACTION", "INPUT");
							}
						}
					}
				}
				
				fs.put(field);
			}
		}
		}finally{
			DatabaseHelper.close(conn);
		}
		return fs;
	}
	public JSONArray MultiRecordMultiLevel(String moduleCode,int appId,Location loc,String tdGid,String tdUid,String dRecordId) throws Exception{
		ArrayList<Location> vicinityMrbs = null;
		ArrayList<Location> childMrbs = null;
		
		JSONArray detailRecords=new JSONArray();
		Connection conn=null;
		Multimap<String, ArrayList<Location>> childChildMrbLocations = ArrayListMultimap.create();
		try{
			conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		try{
			childMrbs = new LearningHelper().hydrateChildMultiRecordBlocks(conn, moduleCode,appId,"",loc.getLocationName());
		}catch(Exception e){
			logger.error("ERROR hydrating Child record blocks of {}", loc.getLocationName());
			logger.error(e.getMessage(),e);
		}
		  for(int i=0;i<childMrbs.size();i++){
			  ArrayList<Location> childChildMrbs = new LearningHelper().hydrateChildMultiRecordBlocks(conn, moduleCode,appId,"",childMrbs.get(i).getLocationName());
			  childChildMrbLocations.put(childMrbs.get(i).getLocationName(), childChildMrbs);
		  }
		}finally{
			DatabaseHelper.close(conn);
		}
		if(childMrbs!=null && childMrbs.size()>0){

			int numberOfSheets = 0;
			try{
				numberOfSheets = workbook.getNumberOfSheets();
			}catch(Exception e){
				logger.error("ERROR getting number of sheets");
				logger.error(e.getMessage(),e);
				throw new TestDataException(e.getMessage());
			}
			vicinityMrbs = new ArrayList<Location>();
			if(childMrbs != null && childMrbs.size() > 0){
				for(Location vMrb:childMrbs){
					vicinityMrbs.add(vMrb);
				}
			}
			JSONObject page=new JSONObject(); 

			if(vicinityMrbs != null && vicinityMrbs.size() > 0){
				for(Location mrb:vicinityMrbs){
					JSONArray eachrec=this.MultiLevelLevelMultiRecordBlocks(mrb, numberOfSheets, tdGid, tdUid, dRecordId, loc, moduleCode, appId);
					if(eachrec!=null && eachrec.length()>0){
						page=mrb.toJson();
						JSONObject rec = eachrec.getJSONObject(0);
						page.put("DR_ID", rec.getString("DR_ID"));
						page.put("FIELDS", rec.getJSONArray("FIELDS"));
						
					}	
					for(Entry<String, ArrayList<Location>> e : childChildMrbLocations.entries()) {
						String keyVal=e.getKey();
						ArrayList<Location> locations=e.getValue();
						if(mrb.getLocationName().equalsIgnoreCase(keyVal)){
							for(Location location:locations){
								JSONArray detailed=this.MultiLevelLevelMultiRecordBlocks(location,numberOfSheets,tdGid,tdUid,dRecordId,loc,moduleCode,appId);
								if(detailed!=null && detailed.length()>0){
									if(eachrec!=null && eachrec.length()>0){
										
										page.put("DETAILRECORDS", detailed);
									}
								}
							}
							break;
						}
					}
					if(page!=null && page.length()>0){
						detailRecords.put(page);
				}
			}
		  }	
		}
		return detailRecords;
	}
	
	public JSONArray MultiLevelLevelMultiRecordBlocks(Location vicinityMrbs,int numberOfSheets,String tdGid,String tdUid,String dRecordId,Location loc,String moduleCode,int appId) throws Exception{
		int headerRow = 4;
		int dataStartRow = 5;
		int dataStartCol = 3;
		ArrayList<Row> sDetailRows=null;	
		JSONArray sDetails = new JSONArray();
		Map<String, Boolean> mrbLocMap = new HashMap<String, Boolean>();
		Connection conn=null;
		try{
			conn=DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if(vicinityMrbs!=null){
			Location mrb=vicinityMrbs;
				logger.debug("Scanning Multi Record Block {} for more detail records for TDGID {}, TDUID {}, detail ID {}", mrb.getLocationName(), tdGid, tdUid, dRecordId);
				if(mrb.getLocationName().equalsIgnoreCase("Clearing Details")){
					System.err.println("Breakpoint");
				}
				XSSFSheet mrbSheet = null;
				String sName = "";
				for(int i=0;i<numberOfSheets;i++){
					try{
						XSSFSheet oSheet = (XSSFSheet) workbook.getSheetAt(i);
						Row sheetNameRow = oSheet.getRow(SHEET_NAME_ROW_NUM);
						if(sheetNameRow == null){
							logger.error("SHEET NAME ROW not found. Invalid template");
							throw new TestDataException("Invalid Test Data Template - Page Name row was not found. Please use the latest test data template and try again");
						}

						Cell sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);

						sName = getCellValue(sheetNameCell);
						if(Utilities.trim(sName).equalsIgnoreCase(mrb.getLocationName())){
							boolean sheetVisited = false;
							try{
								sheetVisited = mrbLocMap.get(sName);
							}catch(Exception e){

							}
							if(sheetVisited){
								logger.debug("This sheet has already been visited. Skipping");
								continue;
							}else{
								mrbSheet = oSheet;
								logger.info("Found sheet for {}", mrb.getLocationName());
								break;
							}

						}
					}catch(Exception e){
						logger.error("ERROR in finding sheet for {}", mrb.getLocationName());
						logger.error(e.getMessage(),e);
					}
				}

				if(mrbSheet != null){
					sDetailRows = this.getSecondLevelDetailRowsForMaster(mrbSheet, tdGid, tdUid, dRecordId , dataStartRow);

					if(sDetailRows != null && sDetailRows.size() > 0){
						int totalFields = this.getTotalFieldsOnSheet(mrbSheet, headerRow, dataStartCol);

						for(Row sDetailRow:sDetailRows){
							JSONObject sDetail = new JSONObject();
							String drid = sDetailRow.getCell(2).getStringCellValue();
							sDetail.put("DR_ID", drid);
							JSONArray dfs = new JSONArray();
							for(int f=dataStartCol;f<=dataStartCol+totalFields;f++){
								Cell cell = sDetailRow.getCell(f);
								if(cell != null && cell.getStringCellValue() != null && !cell.getStringCellValue().equalsIgnoreCase("")){
									String fieldName = mrbSheet.getRow(headerRow).getCell(f).getStringCellValue();
									String fieldValue = cell.getStringCellValue();
									String val = "";
									try{
										val = this.resolve(fieldValue,fieldName,appId,moduleCode);
									}catch(Exception e){
										throw new TestDataException(e.getMessage(),e);
									}
									TestObject t=null;
									if(fieldName.equalsIgnoreCase("Button"))
									{
										t=new TestObject();
										t.setLabel("Button");
										t.setObjectClass("Button");
										t.setUniqueId(val);
										t.setIdentifiedBy("Button");
										t.setLocation(loc.getLocationName());
										t.setViewmode("N");
										t.setMandatory("NO");
										t.setLovAvailable("N");

									}
									else{
										t = new LearningHelper().hydrateTestObject(conn, moduleCode, sName, appId, fieldName);
									}
									if(t.getLabel() == null){
										throw new TestDataException("Information for field " + fieldName + " was not found in " + sName + " page in " + moduleCode + ". Re-learn this function or use the latest Test Data template");
									}
									JSONObject field = new JSONObject();
									field.put("DATA", val);
									field.put("FLD_LABEL", t.getLabel());
									
									field.put("FLD_TYPE",t.getObjectClass());
									field.put("FLD_UID", t.getUniqueId());
									field.put("FLD_UID_TYPE", t.getIdentifiedBy());
									field.put("FLD_NAME", fieldName);
									String hasLov = "";
									if(t.getLovAvailable() == null){
										hasLov = "N";
									}else{
										hasLov = t.getLovAvailable();
									}
									field.put("FLD_HAS_LOV", hasLov);
									if(!loc.isMultiRecordBlock()){
										short bgColor = cell.getCellStyle().getFillForegroundColor();
										if(bgColor == IndexedColors.GREEN.getIndex()){
											field.put("ACTION", "QUERY");
										}else if(bgColor == IndexedColors.YELLOW.getIndex()){
											field.put("ACTION", "VERIFY");
										}else{
											field.put("ACTION", "INPUT");
										}
									}else{
										Cell hCell = mrbSheet.getRow(headerRow).getCell(f);
										Short headerBgColor = hCell.getCellStyle().getFillForegroundColor();
										Short bgColor = cell.getCellStyle().getFillForegroundColor();
										if(bgColor == IndexedColors.LAVENDER.getIndex()){
											field.put("ACTION","ADDROW");
										}else if(bgColor == IndexedColors.GREY_40_PERCENT.getIndex()){
											field.put("ACTION", "DELETEROW");
										}else if(bgColor == IndexedColors.YELLOW.getIndex()){
											field.put("ACTION", "VERIFY");
										}else{
											if(headerBgColor == IndexedColors.LIGHT_ORANGE.getIndex()){
												field.put("ACTION", "QUERY");
											}else{
												field.put("ACTION", "INPUT");
											}
										}
									}
									
									dfs.put(field);
								}
							}


							sDetail.put("FIELDS", dfs);
							if(dfs != null && dfs.length() > 0){
								sDetails.put(sDetail);	
								this.locMap.put(sName, true);
								mrbLocMap.put(sName, true);
							}

						}
						if(sDetails!=null && sDetails.length()>0){
							
						}
					}

				}
		}
		}finally{
			DatabaseHelper.close(conn);
		}
		return sDetails;
	}

	@Override
	public List<String> getDataExtractionInputRecordIDs(String fullFilePath, int appId, String functionCode) throws TestDataException {
		
		Workbook workbook = null;
		try {
			workbook = WorkbookFactory.create(new FileInputStream(fullFilePath));
		} catch (FileNotFoundException e) {
			
			logger.error(e.getMessage(), e);
			throw new TestDataException("Could not read input file due to an internal error. Please contact Tenjin Support.");
		} catch (IOException e) {
			
			logger.error(e.getMessage(), e);
			throw new TestDataException("Could not read input file due to an internal error. Please contact Tenjin Support.");
		} catch (InvalidFormatException e) {
			
			logger.error(e.getMessage(), e);
			throw new TestDataException("Could not read input file due to an internal error. Please contact Tenjin Support.");
		}
		
		String sheetName = "";
		Sheet sheet = workbook.getSheetAt(0);
		sheetName = sheet.getSheetName();
		
		if(Utilities.trim(sheetName).equalsIgnoreCase("TTD_HELP")) {
			sheet = workbook.getSheetAt(1);
			sheetName = sheet.getSheetName();
		}
		
		
		logger.info("Sheet at position 0 is --> {}", sheetName);
		
		int tdGidCol = 0;
		int tdUidCol = TDUIDCOL;
		int dataStartCol = DATA_START_COL;
		int dataStartRow = DATA_START_ROW_NUM;
		int headerRowNum = HEADER_ROW_NUM;
				
		int[] rowIDsToScan = this.getPhysicalRowIDs(sheet, dataStartRow);
		List<String> extractionData = new ArrayList<String>();
		Map<Integer, String> headerMap = this.getRowData(sheet, headerRowNum, dataStartCol);
		List<Integer> queryHeaders = new ArrayList<Integer>();
		for(int i=0;i<rowIDsToScan.length;i++){
			logger.debug("Scanning row {}", rowIDsToScan[i]);
			Row row = sheet.getRow(rowIDsToScan[i]);
			Cell tdGidCell = row.getCell(tdGidCol);
			Cell tdUidCell = row.getCell(tdUidCol);
			String tdGid = "";
			String tdUid = "";
			if(tdGidCell != null){
				tdGidCell.setCellType(Cell.CELL_TYPE_STRING);
				tdGid = tdGidCell.getStringCellValue();
			}
			
			if(tdUidCell != null){
				tdUidCell.setCellType(Cell.CELL_TYPE_STRING);
				tdUid = tdUidCell.getStringCellValue();
			}
			
			if(tdUid == null || Utilities.trim(tdUid).equalsIgnoreCase("")){
				logger.warn("TDUID not input for row {}. This row will be skipped", row.getRowNum());
				continue;
			}
			
			Map<Integer, Cell> cellMap = this.getPhysicalCellsInRow(sheet, row.getRowNum(), dataStartCol);
			String queryFields = "";
			for(Integer colIndex:cellMap.keySet()){
				Cell cell = cellMap.get(colIndex);
				
				short bgColor = cell.getCellStyle().getFillForegroundColor();
				if(bgColor == QUERY_FIELD_COLOR){
					queryFields = queryFields + cell.getStringCellValue() + ";;";
					if(!queryHeaders.contains(colIndex)){
						queryHeaders.add(colIndex);
					}
				}
			}
			
			extractionData.add(row.getRowNum() + ";;" + tdGid + ";;" + tdUid + ";;" + queryFields);
			
		}
		
		String extractionHeader = "";
		extractionHeader = headerRowNum + ";;TDGID;;TDUID;;";
		for(Integer colIndex:queryHeaders){
			extractionHeader += headerMap.get(colIndex) + ";;";
		}
		
		extractionData.add(0, extractionHeader);
		return extractionData;
	}
	
	private Map<Integer, Cell> getPhysicalCellsInRow(Sheet sheet, int rowNumber, int scanStartColumn) throws TestDataException{
		Map<Integer, Cell> map = new TreeMap<Integer, Cell>();
		try{
			Row row = sheet.getRow(rowNumber);
			if(row == null){
				return map;
			}
			Iterator<Cell> cellIter = row.cellIterator();
			while(cellIter.hasNext()){
				Cell cell = cellIter.next();
				if(cell.getColumnIndex() >= scanStartColumn){
					cell.setCellType(Cell.CELL_TYPE_STRING);
					map.put(cell.getColumnIndex(), cell);
				}
				
				
			}
			
			return map;
		}catch(Exception e){
			logger.error("ERROR getting cell data in row {}", rowNumber,e);
			throw new TestDataException("Could not get cell data for row " + rowNumber);
		}
	}
	
	private int[] getPhysicalRowIDs(Sheet sheet, int startRow) throws TestDataException{
		try{
			@SuppressWarnings("rawtypes")
			Iterator rowIter = sheet.rowIterator();
			
			
			int totalPhysicalRows = 0;
			while(rowIter.hasNext()){
				if(((Row)rowIter.next()).getRowNum() >= startRow){
					totalPhysicalRows++;
				}
			}
			
			int[] physicalRowIDs = new int[totalPhysicalRows];
			
			rowIter = sheet.rowIterator();
			int i = -1;
			while(rowIter.hasNext()){
				Row row = (Row)rowIter.next();
				if(row.getRowNum() >= startRow){
					i++;
					physicalRowIDs[i] = row.getRowNum();
				}
			}
			
			return physicalRowIDs;
		}catch(Exception e){
			logger.error("Could not find physical row count in sheet",e);
			throw new TestDataException("Could not get number of rows in sheet");
		}
	}
	
	private Map<Integer, String> getRowData(Sheet sheet, int rowNumber, int scanStartColumn) throws TestDataException{
		Map<Integer, String> map = new TreeMap<Integer, String>();
		try{
			Row row = sheet.getRow(rowNumber);
			Iterator<Cell> cellIter = row.cellIterator();
			while(cellIter.hasNext()){
				Cell cell = cellIter.next();
				if(cell.getColumnIndex() >= scanStartColumn){
					cell.setCellType(Cell.CELL_TYPE_STRING);
					map.put(cell.getColumnIndex(), cell.getStringCellValue());
				}
				
				
			}
			
			return map;
		}catch(Exception e){
			logger.error("ERROR getting cell data in row {}", rowNumber,e);
			throw new TestDataException("Could not get cell data for row " + rowNumber);
		}
	}

	@Override
	public JSONObject getDataExtractionFields(String fullFilePath, int appId, String functionCode, String tdUid, int rowNumber) throws TestDataException {
		
		JSONObject json = null;
		
		Workbook workbook = null;
		int tdUidCol = TDUIDCOL;
		int dataStartCol = DATA_START_COL;
		int headerRowNum = HEADER_ROW_NUM;
		
		try {
			workbook = WorkbookFactory.create(new FileInputStream(fullFilePath));
			this.setWorkbook(workbook);
		} catch (InvalidFormatException e) {
			logger.error(e.getMessage(),e);
			throw new TestDataException("Could not get fields to extract due to an internal error");
		} catch (FileNotFoundException e) {
			
			logger.error(e.getMessage(),e);
			throw new TestDataException("Could not load input excel file. Please try again");
		} catch (IOException e) {
			
			logger.error(e.getMessage(),e);
			throw new TestDataException("Could not get fields to extract due to an internal error");
		}
		
		LearningHelper lHelper = new LearningHelper();
		
		JSONObject locationsJson = null;
		try {
			logger.info("Loading metadata for screens in {} in app {}", functionCode, appId);
			Map<String, Map<String, Object>> metadata = lHelper.hydrateScreenMetadataAsMap(appId, functionCode);
			this.locationsMap = metadata.get("PAGEAREAS");
			this.fieldsMap = metadata.get("FIELDS");
			locationsJson = lHelper.hydrateLocationHierarchyAsJSON(functionCode, appId);
			System.err.println(locationsJson);
			logger.info("Metadata loading --> Done");
		} catch (DatabaseException e1) {
			
			logger.error("Unable to load screen metadata for {} in app {} due to an exception", functionCode, appId, e1);
			throw new TestDataException("Could not load metadata for function " + functionCode + " due to an internal error");
		}
		
		try{
			json = new JSONObject();
			json.put("PA_NAME", locationsJson.getString("PA_NAME"));
			json.put("PA_TYPE", locationsJson.getString("PA_TYPE"));
			json.put("PA_WAY_IN", locationsJson.getString("PA_WAY_IN"));
			json.put("PA_WAY_OUT", locationsJson.getString("PA_WAY_OUT"));
			json.put("PA_PARENT", locationsJson.getString("PA_PARENT"));
			if(locationsJson.getString("PA_SCREEN_TITLE") != null){
				json.put("PA_SCREEN_TITLE", locationsJson.getString("PA_SCREEN_TITLE"));
			}
			
			json.put("PA_IS_MRB", locationsJson.getString("PA_IS_MRB"));
			json.put("CHILDREN", locationsJson.getJSONArray("CHILDREN"));
			Sheet sheet = this.getSheetByName(workbook, json.getString("PA_NAME"));
			
			if(sheet == null){
				logger.error("ERROR while getting extraction data --> Sheet for MAIN page {} was not found in Excel Workbook", json.getString("PA_NAME"));
				throw new TestDataException("The test data sheet you uploaded is invalid. Sheet for MAIN page " + json.getString("PA_NAME") + " was not found");
			}
			

			Map<Integer, String> headerMap = this.getRowData(sheet, headerRowNum, dataStartCol);
			Map<Integer, Cell> dataRow = this.getPhysicalCellsInRow(sheet, rowNumber, dataStartCol);
			
			Row row = sheet.getRow(rowNumber);
			Cell tdUidCell = row.getCell(tdUidCol);
			String aTdUid = "";
			if(tdUidCell != null){
				aTdUid = tdUidCell.getStringCellValue();
			}
			if(aTdUid == null){
				aTdUid = "";
			}
			
			if(!aTdUid.equalsIgnoreCase(tdUid)){
				logger.error("Invalid TDUID found in row {} of input sheet. Expected {}, found {}", rowNumber, tdUid, aTdUid);
				throw new TestDataException("Invalid TDUID found in row " + rowNumber + " of input sheet. Expected " + tdUid + ", found " + aTdUid);
			}
			
			JSONArray fields = new JSONArray();
			
			for(Integer colIndex:headerMap.keySet()){
				String fieldLabel = headerMap.get(colIndex);
				String fieldAction = "";
				String dataVal = "";
				TestObject t = (TestObject) this.fieldsMap.get(fieldLabel);
				Cell cell = dataRow.get(colIndex);
				if(cell != null){
					dataVal = cell.getStringCellValue();
					short bgColor = cell.getCellStyle().getFillForegroundColor();
					if(bgColor == QUERY_FIELD_COLOR){
						fieldAction = "QUERY";
					}
				}else{
					fieldAction = "EXTRACT";
				}
				JSONObject field = new JSONObject();
				if(fieldLabel.equalsIgnoreCase("Button")){
					field.put("FLD_TYPE","Button");
					field.put("FLD_UID", "Button");
					field.put("FLD_UID_TYPE", "Button");
				}
				else{
					field.put("FLD_TYPE",t.getObjectClass());
					field.put("FLD_UID", t.getUniqueId());
					field.put("FLD_UID_TYPE", t.getIdentifiedBy());
				}
				field.put("DATA", dataVal);
				field.put("FLD_LABEL", fieldLabel);
				
				String hasLov = "";
				if(fieldLabel.equalsIgnoreCase("Button")){
					hasLov = "N";
				}
				else{
					if(t.getLovAvailable() == null){
						hasLov = "N";
					}else{
						hasLov = t.getLovAvailable();
					}
				}
				field.put("FLD_HAS_LOV", hasLov);
				field.put("ACTION",fieldAction);
				
				fields.put(field);
			}
			
			if(!json.getString("PA_IS_MRB").equalsIgnoreCase("YES")){
				json.put("FIELDS", fields);
			}else{
				JSONObject detailRecords = new JSONObject();
				detailRecords.put("FIELDS", fields);
				json.put("DETAILRECORDS", detailRecords);
			}
			
			
			JSONArray children = this.getChildPageJSONArray(json, tdUid);
			json.put("CHILDREN", children);
			
		}catch(JSONException e){
			logger.error("JSON error while scanning workbook for fields",e);
			throw new TestDataException("Could not process data sheet due to an internal error");
		}
		
		return json;
	}
	
	private JSONArray getChildPageJSONArray(JSONObject page, String tdUid){
		JSONArray cJson = new JSONArray();
		
		int tdUidCol = TDUIDCOL;
		int dataStartCol = DATA_START_COL;
		int dataStartRow = DATA_START_ROW_NUM;
		int headerRowNum = HEADER_ROW_NUM;
		
		try{
			JSONArray children= page.getJSONArray("CHILDREN");
			if(children != null){
				for(int i=0;i<children.length();i++){
					JSONObject child = children.getJSONObject(i);
					JSONObject json = new JSONObject();
					json.put("PA_NAME", child.getString("PA_NAME"));
					json.put("PA_TYPE", child.getString("PA_TYPE"));
					json.put("PA_WAY_IN", child.getString("PA_WAY_IN"));
					json.put("PA_WAY_OUT", child.getString("PA_WAY_OUT"));
					json.put("PA_PARENT", child.getString("PA_PARENT"));
					try{
						json.put("PA_SCREEN_TITLE",child.getString("PA_SCREEN_TITLE"));
					}catch(Exception e){}
					
					json.put("PA_IS_MRB", child.getString("PA_IS_MRB"));
					json.put("CHILDREN", child.getJSONArray("CHILDREN"));
					
					Sheet sheet = this.getSheetByName(this.getWorkbook(), json.getString("PA_NAME"));
					
					if(sheet == null){
						logger.warn("Sheet with name {} was not found in the excel workbook. This sheet will be skipped");
						continue;
					}
					
					int[] rowIDsToScan = this.getPhysicalRowIDs(sheet, dataStartRow);
					boolean includeSheet = false;
					int rowNumber= 0;
					for(int j=0; j<rowIDsToScan.length; j++){
						Row row = sheet.getRow(rowIDsToScan[j]);
						Cell tdUidCell = row.getCell(tdUidCol);
						String aTdUid = "";
						
						if(tdUidCell != null){
							tdUidCell.setCellType(Cell.CELL_TYPE_STRING);
							aTdUid = tdUidCell.getStringCellValue();
						}
						
						if(tdUid != null && tdUid.equalsIgnoreCase(aTdUid)){
							includeSheet = true;
							rowNumber = rowIDsToScan[j];
							break;
						}
					}
					
					if(includeSheet){
						Map<Integer, String> headerMap = this.getRowData(sheet, headerRowNum, dataStartCol);
						Map<Integer, Cell> dataRow = this.getPhysicalCellsInRow(sheet, rowNumber, dataStartCol);
						
						JSONArray fields = new JSONArray();
						
						for(Integer colIndex:headerMap.keySet()){
							String fieldLabel = headerMap.get(colIndex);
							String fieldAction = "";
							String dataVal = "";
							TestObject t = (TestObject) this.fieldsMap.get(fieldLabel);
							Cell cell = dataRow.get(colIndex);
							if(cell != null){
								dataVal = cell.getStringCellValue();
								short bgColor = cell.getCellStyle().getFillForegroundColor();
								if(bgColor == QUERY_FIELD_COLOR){
									fieldAction = "QUERY";
								}
							}else{
								fieldAction = "EXTRACT";
							}
							
							JSONObject field = new JSONObject();
							field.put("DATA", dataVal);
							field.put("FLD_LABEL", fieldLabel);
							
							if(fieldLabel.equalsIgnoreCase("BUTTON")) {
								field.put("FLD_TYPE","Button");
								field.put("FLD_UID", "Button");
								field.put("FLD_UID_TYPE", "Button");
							}else{
								field.put("FLD_TYPE",t.getObjectClass());
								field.put("FLD_UID", t.getUniqueId());
								field.put("FLD_UID_TYPE", t.getIdentifiedBy());
							}
							String hasLov = "";
							if(fieldLabel.equalsIgnoreCase("button") || (t != null && t.getLovAvailable() == null )){
								hasLov = "N";
							}else{
								hasLov = t.getLovAvailable();
							}
							field.put("FLD_HAS_LOV", hasLov);
							field.put("ACTION",fieldAction);
							
							fields.put(field);
						}
						
						if(!json.getString("PA_IS_MRB").equalsIgnoreCase("YES")){
							json.put("FIELDS", fields);
						}else{
							JSONObject detailRecords = new JSONObject();
							detailRecords.put("FIELDS", fields);
							json.put("DETAILRECORDS", detailRecords);
						}
					}
					
					JSONArray cChildren = getChildPageJSONArray(child, tdUid);
					json.put("CHILDREN", cChildren);
					
					if(includeSheet){
						cJson.put(json);
					}else if(cChildren != null && cChildren.length()> 0){
						cJson.put(json);
					}
					
					

					
				}
			}
		}catch(JSONException e){
			logger.error("ERROR generating JSON for child pages");
		} catch (TestDataException e) {
			
			logger.error("Error ", e);
		}
		
		return cJson;
	}
	
	private Sheet getSheetByName(Workbook workbook, String sheetName) throws TestDataException{
		Sheet sheet = null;
		Row sheetNameRow = null;
		Cell sheetNameCell = null;
		String locationName = "";
		for(int i=0;i<workbook.getNumberOfSheets();i++){
			sheet = workbook.getSheetAt(i);
			sheetNameRow = sheet.getRow(SHEET_NAME_ROW_NUM);
			if(sheetNameRow == null){
				logger.error("SHEET NAME ROW not found. Invalid template");
				throw new TestDataException("Invalid Test Data Template - Page Name row was not found. Please use the latest test data template and try again");
			}
			
			sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);
			
			locationName = getCellValue(sheetNameCell);
			
			if(Utilities.trim(locationName).equalsIgnoreCase(sheetName)){
				return sheet;
			}
		}
		
		return sheet;
	}
	
	
	public String generateExtractionOutput(int runId, int appId, Module function, String[] tdUids, boolean useFreshTemplate) throws TestDataException, TenjinConfigurationException{
		String outputFilePath = "";
		String EXTR_OUTPUT_PATH = "EXTR_OUTPUT";
		String workPath = TenjinConfiguration.getProperty("TJN_WORK_PATH");
		File dir = new File(workPath + File.separator + EXTR_OUTPUT_PATH);
		if(!dir.exists()){
			dir.mkdir();
		}
		workPath = workPath + File.separator+ EXTR_OUTPUT_PATH;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String outputFileName = sdf.format(new Date());
		outputFilePath = workPath + File.separator+ outputFileName;
		if(tdUids == null || tdUids.length < 1){
			logger.error("No TDUIDs found to generate output for");
			throw new TestDataException("Could not generate output. No TDUID specified.");
		}
		
		if(!useFreshTemplate){
			if(!Utilities.trim(function.getExtractorInputFile()).equalsIgnoreCase("")){
				logger.info("Loading Template From [{}]", function.getExtractorInputFile());
				File sFile = new File( function.getExtractorInputFile());
				try {
					logger.info("Copying uploaded file to output directory");
					FileUtils.copyFile(sFile, new File(outputFilePath));
				} catch (IOException e) {
					
					logger.error("Unable to copy to output dir", e);
					throw new TestDataException("An internal error occurred while writing to output file. Please contact Tenjin Support");
				}
				
			}else{
				throw new TestDataException("The Uploaded spreadsheet could not be found. It seems the file was either moved or deleted. Please run the extractor again.");
			}
		}else{
			logger.info("Generating a fresh template and using it");
			this.generateTemplate(appId, function.getCode(), outputFilePath, outputFileName, false, false);
		}
		
		try {
			this.workbook = WorkbookFactory.create(new File(outputFilePath));
		} catch (InvalidFormatException e) {
			logger.error(e.getMessage(),e);
			throw new TestDataException("An internal error occurred while generating extraction output");
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
			throw new TestDataException("An internal error occurred while generating extraction output");
		}
		
		for(String tdUid:tdUids){
			JSONObject data = null;
			if(function.getLastSuccessfulExtractionStatus() != null && function.getLastSuccessfulExtractionStatus().getRecords() != null){
				for(ExtractionRecord record:function.getLastSuccessfulExtractionStatus().getRecords()){
					if(record.getTdUid().equalsIgnoreCase(tdUid)){
						try {
							data = new JSONObject(record.getData());
						} catch (JSONException e) {
							
							logger.error("ERROR --> Invalid JSON format for the extracted data for TDUID [{}]", tdUid);
							logger.error("DATA found --> {}", record.getData());
							logger.error("This TDUID will be skipped");
						}
						
						break;
					}
				}
			}
			
			if(data == null){
				logger.error("Could not get Extracted data for record [{}]. This record will be skipped", tdUid);
				continue;
			}
			
			try {
				logger.info("Entering extractor data for [{}]", tdUid);
				this.enterExtractionDataInSameWorkbook(tdUid, data);
			} catch (Exception e) {
				
				logger.error("ERROR entering extraction data for TDUID [{}]", tdUid, e);
			}
		}
		
		OutputStream os;
		String oldFileName = outputFileName;
		outputFileName = function.getCode() + "_Extracted_" + runId + ".xlsx";
		
		outputFilePath = workPath + File.separator + outputFileName;
		try {
			os = new FileOutputStream(outputFilePath);
			workbook.write(os);
		} catch (FileNotFoundException e) {
			
			logger.error("ERROR writing excel to file",e);
			throw new TestDataException("An internal error occurred while writing to output file. Please contact Tenjin Support");
		} catch (IOException e) {
			
			logger.error("ERROR writing excel to file",e);
			throw new TestDataException("An internal error occurred while writing to output file. Please contact Tenjin Support");
		}
		
		logger.info("Cleaning up");
		File file = new File(workPath +File.separator + oldFileName);
		if(file.exists()){
			System.out.println(file.delete());
		}
		
		return outputFileName;
	}
	
	
	private void enterExtractionDataInSameWorkbook(String tdUid, JSONObject data) throws TestDataException{
		int tdUidCol = TDUIDCOL;
		int dataStartCol = DATA_START_COL;
		int dataStartRow = DATA_START_ROW_NUM;
		int headerRowNum = HEADER_ROW_NUM;
		try {
			JSONArray pages = new JSONArray(data.getString("EXTRACTION"));
			for(int i = 0 ; i<pages.length();i++){
				JSONObject page = new JSONObject(pages.get(i).toString());
				String pageName = page.get("PA_NAME").toString();
				Sheet sheet = this.getSheetByName(this.workbook, pageName);
				if(sheet == null){
					throw new TestDataException("Could not find sheet named [" + pageName + "]. The uploaded sheet may have been tampered with. Please run the extractor again.");
				}
				String DRID = page.get("DR_ID").toString();
 
				int rowNumber =  this.checkIfValueExists(sheet, dataStartRow, tdUidCol, tdUid,DRID);
				if(rowNumber >= dataStartRow){
					Map<Integer, String> headerMap = this.getRowData(sheet, headerRowNum, dataStartCol); // All headers
					Map<String, Integer> transposedMap = this.transposeMap(headerMap);

					JSONArray fields = page.getJSONArray("FIELDS");
					for(int j=0; j<fields.length(); j++){
						JSONObject field = fields.getJSONObject(j);

						String label= field.getString("FLD_LABEL");
						String dataVal = field.getString("DATA");

						int targetColIndex = transposedMap.get(label);
						Cell targetCell = sheet.getRow(rowNumber).getCell(targetColIndex, Row.CREATE_NULL_AS_BLANK);
						targetCell.setCellValue(dataVal);

					}
				}else{
					//TDUID is not present in this sheet. so skip it.
				} 
			}
		} catch (JSONException e) {
			
			logger.error("JSONException caught --> " + e.getMessage());
			throw new TestDataException("Could not generate output due to an internal error. Please contact Tenjin Support.");
		}

	}
	
	
	private int checkIfValueExists(Sheet sheet, int searchStartRow, int colId, String valueToSearchTDUID,
			String valueToSearchDRID) throws TestDataException {
		int[] rowIDsToScan = this.getPhysicalRowIDs(sheet, searchStartRow);

		int rowNumber = -1;
		for (int j = 0; j < rowIDsToScan.length; j++) {
			Row row = sheet.getRow(rowIDsToScan[j]);
			 
			Cell targetCellTDUID = row.getCell(colId);
			String valueTDUID = ""; 
			if (targetCellTDUID != null) {
				targetCellTDUID.setCellType(Cell.CELL_TYPE_STRING);
				valueTDUID = targetCellTDUID.getStringCellValue();
			}

			Cell targetCellDRID = row.getCell(colId+1);
			String valueDRID = ""; 
			if (targetCellDRID != null) {
				targetCellDRID.setCellType(Cell.CELL_TYPE_STRING);
				valueDRID = targetCellDRID.getStringCellValue();
			}
			
			if (valueToSearchTDUID != null && valueToSearchTDUID.equalsIgnoreCase(valueTDUID)
					&& valueToSearchDRID != null && valueToSearchDRID.equalsIgnoreCase(valueDRID)) {
				rowNumber = rowIDsToScan[j];
				break;
			}
		}

		return rowNumber;
	}
	
	private Map<String, Integer> transposeMap(Map<Integer, String> mapToTranspose){
		Map<String, Integer> map = new TreeMap<String, Integer>();
		
		for(Integer i:mapToTranspose.keySet()){
			map.put(mapToTranspose.get(i), i);
		}
		
		return map;
	}

	


} 