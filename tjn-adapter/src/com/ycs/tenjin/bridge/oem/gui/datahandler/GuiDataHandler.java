/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  TestDataHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 10-Sep-2016           Sriram Sridharan          Newly Added For 
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
*/

package com.ycs.tenjin.bridge.oem.gui.datahandler;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ycs.tenjin.bridge.exceptions.TestDataException;

public interface GuiDataHandler {
	
	public void enableExtraction(Boolean forExtraction); 
	
	public void generateTemplate(int applicationId, String functionCode, String destinationFolderPath, String templateFileName, boolean mandatoryOnly, boolean editableOnly) throws TestDataException;
	
	public void generateTemplate(int applicationId, String functionCode, String destinationFolderPath, String templateFileName, JSONArray manifest)  throws TestDataException;
	
	public JSONObject loadTestDataFromTemplate(String absoluteFilePath, int appId, String functionCode, String tdGid, String tdUid, int testCaseId,String testStepId)  throws TestDataException;
	
	public List<String> getAllTdUidsInRange(String absoluteFilePath, String tdGid, int dataLimit) throws TestDataException;
	
	
	public List<String> getDataExtractionInputRecordIDs(String fullFilePath, int appId, String functionCode) throws TestDataException;
	
	
	public JSONObject getDataExtractionFields(String fullFilePath, int appId, String functionCode, String tdUid, int rowNumber) throws TestDataException;
	
}
