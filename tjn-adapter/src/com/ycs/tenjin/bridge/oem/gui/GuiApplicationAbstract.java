/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Application.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Sep 8, 2017          Sameer Gupta          		Newly Added For  
 * 26-Nov 2018			Shivam	Sharma			 TENJINCG-904
 */

package com.ycs.tenjin.bridge.oem.gui;

import com.ycs.tenjin.bridge.exceptions.ApplicationUnresponsiveException;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.DriverException;
import com.ycs.tenjin.bridge.pojo.aut.Aut;

public abstract class GuiApplicationAbstract implements GuiApplication {

	protected String appName;
	protected int appId;
	protected String adapterPrefix;
	protected String appUrl;
	protected String loginName;
	protected String loginUserType;

	protected String clientIp;
	protected int port;
	protected String browser;
	/*********************************
	 * TENJINCG-731 - By Sameer - Mobility - Start
	 */
	protected String deviceId;
	//added by shivam sharma for  TENJINCG-904 starts
	protected String appFileName;
	//added by shivam sharma for  TENJINCG-904 ends
	/*********************************
	 * TENJINCG-731 - By Sameer - Mobility - End
	 */
	
	/* Added by sameer for mobile starts */
	protected String adapterPackageOrTool;
	private Integer applicationType; 
	private String applicationPackage;
	private String applicationActivity;
	/* Added by sameer for mobile ends */
	/*Added by Prem for  v210Reg-30 start*/
	protected String autBrowser;
	/*Added by Prem for  v210Reg-30 End*/
	public GuiApplicationAbstract() {

	}

	/*********************************
	 * TENJINCG-731 - By Sameer - Mobility - Start
	 */
	public GuiApplicationAbstract(Aut aut, String clientIp, int port,
			String browser,String deviceId) {
		this.appName = aut.getName();
		this.appId = aut.getId();
		this.adapterPrefix = aut.getAdapterPackage();
		this.appUrl = aut.getURL();
		this.loginName = aut.getLoginName();
		this.loginUserType = aut.getLoginUserType();
		/*Added by Prem for  v210Reg-30 start*/
		this.autBrowser=aut.getBrowser();
		/*Added by Prem for  v210Reg-30 End*/

		if (aut.getAdapterPackageOrTool().equals(1)) {
			this.adapterPackageOrTool = "selenium";
		} else if (aut.getAdapterPackageOrTool().equals(2)) {
			this.adapterPackageOrTool = "uft";
		} else if (aut.getAdapterPackageOrTool().equals(3)) {
			this.adapterPackageOrTool = "appium";
		} 
		this.applicationType =  aut.getApplicationType();
		this.applicationPackage = aut.getApplicationPackage();
		this.applicationActivity = aut.getApplicationActivity();

		this.clientIp = clientIp;
		this.port = port;
		this.browser = browser;
		/*********************************
	 	* TENJINCG-731 - By Sameer - Mobility - Start
	 	*/
		this.deviceId= deviceId;
		//added by shivam sharma for  TENJINCG-904 starts
		this.appFileName = aut.getAppFileName();
		//added by shivam sharma for  TENJINCG-904 ends
		/*********************************
	 * TENJINCG-731 - By Sameer - Mobility - End
	 */
	}

	// /////////////////////////////////////////////////
	// Getters
	// /////////////////////////////////////////////////

	public String getAdapterPrefix() {
		return adapterPrefix;
	}

	public String getAppName() {
		return this.appName;
	}

	public int getAppId() {
		return this.appId;
	}

	public String getAdapterPackageOrTool() {
		return adapterPackageOrTool;
	}

	public Integer getApplicationType() {
		return this.applicationType;
	}

	public String getApplicationPackage() {
		return this.applicationPackage;
	}

	public String getApplicationActivity() {
		return this.applicationActivity;
	}

	// /////////////////////////////////////////////////
	// Methods
	// /////////////////////////////////////////////////
	public abstract void launch(String URL) throws AutException,
			DriverException;

	public abstract void login(String LoginName, String Password)
			throws AutException, ApplicationUnresponsiveException,
			DriverException;

	public abstract void navigateToFunction(String Code, String Name,
			String MenuContainer) throws AutException, BridgeException,
			ApplicationUnresponsiveException, DriverException;

	public abstract void logout() throws AutException, DriverException;

	public abstract void recover() throws AutException, DriverException;

	public abstract void destroy();

	public void explicitWait(long seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (Exception ignore) {
		}
	}

}
