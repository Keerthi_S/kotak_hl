/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AdapterFactory.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Sep 2, 2016           Sriram Sridharan          Newly Added For 
 * Oct 26, 2016			Sriram Sridharan			to allow App Specific Test Data Handling
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 */

package com.ycs.tenjin.bridge.oem.gui;

import java.lang.reflect.Constructor;
import java.util.Set;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.gui.datahandler.GuiDataHandler;
import com.ycs.tenjin.bridge.pojo.aut.Aut;

public class GuiApplicationFactory {

	private static final Logger logger = LoggerFactory
			.getLogger(GuiApplicationFactory.class);

	
	public static GuiApplicationAbstract getApplication(String adapterPrefix,
			Aut aut, String clientIp, int port, String browser,String deviceId)
			throws BridgeException {

		boolean deviceRequired = false;
		
		String expPackageName = "";
		if (aut.getAdapterPackageOrTool().equals(1)) {
			expPackageName = "com.ycs.tenjin.adapter." + "selenium" + "."
					+ adapterPrefix;
		} else if (aut.getAdapterPackageOrTool().equals(2)) {
			expPackageName = "com.ycs.tenjin.bridge." + "uft";
		} else if (aut.getAdapterPackageOrTool().equals(3) || aut.getAdapterPackageOrTool().equals(4)) {
			expPackageName = "com.ycs.tenjin.adapter." + "appium" + "."
					+ adapterPrefix;
			
			deviceRequired = true;
			
		}
		

		Reflections reflections = new Reflections(expPackageName);

		Set<Class<? extends GuiApplication>> subTypes = reflections
				.getSubTypesOf(GuiApplication.class);
		for (Class<? extends GuiApplication> type : subTypes) {
			try {
				
				if(deviceRequired){ 
					Constructor<?> constructor = type.getConstructor(Aut.class,
							String.class, int.class, String.class, String.class);
					return (GuiApplicationAbstract) constructor.newInstance(aut,
							clientIp, port, browser,deviceId);
				}else{ 
					Constructor<?> constructor = type.getConstructor(Aut.class,
							String.class, int.class, String.class);
					return (GuiApplicationAbstract) constructor.newInstance(aut,
							clientIp, port, browser);
				}
				
			} catch (Exception e) {

				logger.error("Could not initiate adapter for {}",
						aut.getName(), e);
				throw new BridgeException("Could not initiate the Adapter for "
						+ aut.getName());
			}
		}

		logger.error("ERROR initializing adapter for {}. Application implementation was not found");
		throw new BridgeException("Could not initialize adapter for "
				+ aut.getName() + ". Please contact Tenjin Support");

	}

	
	public static GuiDataHandler getTestDataProvider(String adapterPackage)
			throws BridgeException {
		String expPackageName = "com.ycs.tenjin.adapter.selenium."
				+ adapterPackage;
		logger.debug("Loading Executor from adapter package {}", expPackageName);

		Reflections reflections = new Reflections(expPackageName);

		Set<Class<? extends GuiDataHandler>> subTypes = reflections
				.getSubTypesOf(GuiDataHandler.class);

		for (Class<? extends GuiDataHandler> type : subTypes) {
			try {
				return type.newInstance();
			} catch (Exception e) {
				logger.warn(
						"WARNING - Could not initiate test data provider for {}",
						adapterPackage, e);
				return null;
			}
		}

		logger.warn("WARNING - No Test Data provider found specific to {}",
				adapterPackage);
		return null;
	}
	
	

}
