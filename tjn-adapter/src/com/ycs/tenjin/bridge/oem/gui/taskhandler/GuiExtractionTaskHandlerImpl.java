/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExtractionTaskHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 09-Nov-2016          Sriram Sridharan        Newly Added For Data Extraction in Tenjin 2.4.1
 * 19-12-2016			Sriram					Fix for Defect#TEN-102
 * 07-Jul-2017          Sriram Sridharan        TENJINCG-197
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 16-03-2018			Preeti 					TENJINCG-73
 */

package com.ycs.tenjin.bridge.oem.gui.taskhandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.exceptions.ApplicationUnresponsiveException;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.DriverException;
import com.ycs.tenjin.bridge.exceptions.ExtractorException;
import com.ycs.tenjin.bridge.oem.TaskHandler;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationAbstract;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationFactory;
import com.ycs.tenjin.bridge.oem.gui.datahandler.GuiDataHandler;
import com.ycs.tenjin.bridge.oem.gui.datahandler.GuiDataHandlerImpl;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.ExtractionRecord;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.ExtractionHelper;
import com.ycs.tenjin.util.Utilities;

public class GuiExtractionTaskHandlerImpl implements TaskHandler {

	protected GuiApplicationAbstract oApplication;
	protected Module function;
	protected ExtractionHelper helper = new ExtractionHelper();
	protected String clientIp;
	protected int port;
	protected String browserType;
	protected Aut aut;
	protected List<Module> functions;
	protected int runId;
	
	protected String deviceId;


	public GuiExtractionTaskHandlerImpl(Aut aut, List<Module> functions, String clientIp, int port, String browserType,
			String user, int runId, String deviceId) {
		this.aut = aut;
		this.functions = functions;
		this.clientIp = clientIp;
		this.port = port;
		this.browserType = browserType;
		this.runId = runId;
	
		this.deviceId = deviceId;
		
	}

	protected static final Logger logger = LoggerFactory.getLogger(GuiExtractionTaskHandlerImpl.class);

	@Override
	public void execute() {

		logger.info("Beginning Learning Run");
		try {
			this.helper.beginExtractionRun(this.runId, false);
		} catch (Exception e) {
			logger.error("ERROR --> Could not udpate Start for Run ID [{}]", this.runId);
		}

		try {
			logger.info("Initializing Selenium adapter for application [{}]", aut.getName());

			this.oApplication = GuiApplicationFactory.getApplication(this.aut.getAdapterPackage(), aut, this.clientIp,
					this.port, this.browserType, this.deviceId);
			
			if (Utilities.trim(this.browserType).equalsIgnoreCase("appdefault")) {
				this.browserType = this.aut.getBrowser();
			}
			
			this.launchAndLogin(oApplication, aut, this.browserType);

			for (Module function : this.functions) {
				logger.info("Extracting from function [{}]", function.getCode());
				this.function = function;
				this.extractFromFunction();
				logger.info("Initiating Recovery Procedure...");
				this.oApplication.recover();
			}

		} catch (BridgeException e) {
			this.helper.updateAuditTrailAbort(this.runId, "ABORTED", e.getMessage());
		} catch (DriverException e) {
			this.helper.updateAuditTrailAbort(this.runId, "ABORTED", e.getMessage());
		} catch (AutException e) {
			this.helper.updateAuditTrailAbort(this.runId, "ABORTED", e.getMessage());
		} catch (Exception e) {
			this.helper.updateAuditTrailAbort(this.runId, "ABORTED", "Internal Error. Please contact Tenjin Support.");
		} finally {
			try {
				logger.info("Initiating recovery procedure...");
				this.oApplication.recover();
			} catch (AutException e) {
				logger.error("Recovery Failed", e);
			} catch (DriverException e) {
				logger.error("Recovery Failed", e);
			}

			try {
				logger.info("Logging out of [{}]", this.aut.getName());
				this.oApplication.logout();
			} catch (AutException e) {
				logger.error("ERROR while logging out", e);
			} catch (DriverException e) {
				logger.error("ERROR while logging out", e);
			}

			logger.info("Destroying driver");
			this.oApplication.destroy();

			logger.info("Updating Run END");
			try {
				this.helper.endExtractionRun(this.runId);
			} catch (DatabaseException e) {
				logger.error("ERROR --> Could not update END of Run [{}]", this.runId);
			}
			
		}
	}

	private void extractFromFunction() throws ExtractorException {

		logger.info("Extracting from function [{}]", this.function.getCode());
		logger.debug("Updating Audit --> Start [{}]", this.function.getCode());
		this.helper.updateMasterAuditTrail(this.runId, this.function.getCode(), BridgeProcess.IN_PROGRESS, "",
				System.currentTimeMillis(), 0);

		GuiDataHandler handler = new GuiDataHandlerImpl();

		if (this.function.getExtractionRecords() != null) {

			for (ExtractionRecord eRecord : this.function.getExtractionRecords()) {

				logger.info("Beginning extraction of record [{}]", eRecord.getTdUid());
				logger.debug("Updating Audit --> Start TDUID [{}] - Function [{}]", eRecord.getTdUid(),
						this.function.getCode());
				this.helper.updateDetailAuditTrail(this.runId, this.function.getCode(), eRecord.getTdUid(), "X", "",
						"");

				try {
					logger.info("Navigating to [{}]", this.function.getCode());
					this.navigateToFunction(oApplication, this.function.getCode(), this.function.getName(),
							this.function.getMenuContainer());
				} catch (AutException e1) {
					this.helper.updateDetailAuditTrail(this.runId, this.function.getCode(), eRecord.getTdUid(), "E",
							e1.getMessage(), "");
					continue;
				} catch (BridgeException e1) {
					this.helper.updateDetailAuditTrail(this.runId, this.function.getCode(), eRecord.getTdUid(), "E",
							e1.getMessage(), "");
					continue;
				}

				String sJson = "";
				String status = "";
				String message = "";
				try {

					logger.info("Loading Extraction Template");
					handler.enableExtraction(true);
					JSONObject jsonForExtraction = handler.loadTestDataFromTemplate(
							this.function.getExtractorInputFile(), aut.getId(), this.function.getCode(),
							eRecord.getTdGid(), eRecord.getTdUid(), 0, null);

					// Prepare the step
					ExecutionStep step = new ExecutionStep();
					step.setTdGid(eRecord.getTdGid());
					step.setRecordId(0);
					step.setOperation("extraction");
					step.setFunctionMenu(this.function.getMenuContainer());
					step.setExpectedResult("");
					step.setType("");

					// Call execution handler
					ExecutionTaskHandler objExecutionTaskHandler = TaskHandlerFactory
							.getExecutionTaskHandler(this.oApplication);
					IterationStatus iStatus = objExecutionTaskHandler.executeFunctionImpl(step, 0, jsonForExtraction, 0,
							this.runId, 0); 
					
					if(iStatus==null){
						String iterationKey = runId + "IterationStatus_FOR_EXTRACTION";  
						iStatus = (IterationStatus)CacheUtils.getObjectFromRunCache(iterationKey);
					}
 
					List<RuntimeFieldValue>	runtimeFieldValues = iStatus.getRuntimeFieldValues();

					// Prepare Extraction Result Json
					JSONArray jsonArrayExtractionResult = getExtractionResult(jsonForExtraction, runtimeFieldValues);
					JSONObject json = new JSONObject();
					json.put("EXTRACTION", jsonArrayExtractionResult);
					sJson = json.toString();

					status = "S";
					message = "Extraction Complete";
				} catch (Exception e) {
					status = "E";
					message = e.getMessage();
				}
				try {
					this.oApplication.recover();
				} catch (AutException e1) {
					this.helper.updateDetailAuditTrail(this.runId, this.function.getCode(), eRecord.getTdUid(), "E",
							e1.getMessage(), "");
					continue;
				} catch (DriverException e1) {
					this.helper.updateDetailAuditTrail(this.runId, this.function.getCode(), eRecord.getTdUid(), "E",
							e1.getMessage(), "");
					continue;
				}

				logger.debug("Updating END audit trail for record [{}]", eRecord.getTdUid());
				this.helper.updateDetailAuditTrail(this.runId, this.function.getCode(), eRecord.getTdUid(), status,
						message, sJson);

			}

			logger.debug("Updating Audit Trail end for function [{}]", this.function.getCode());
			this.helper.updateMasterAuditTrail(this.runId, this.function.getCode(), BridgeProcess.COMPLETE, "",
					System.currentTimeMillis());
		}
	}

	private JSONArray getExtractionResult(JSONObject jsonForExtraction, List<RuntimeFieldValue> runtimeFieldValues)
			throws JSONException {
		System.out.println(jsonForExtraction);
		JSONArray pages = jsonForExtraction.getJSONArray("PAGEAREAS");
		JSONArray updatedJson = new JSONArray();
		Map<String, String> runtimeFieldValueMap = new HashMap<String, String>();
		for (RuntimeFieldValue runtimeFieldValue : runtimeFieldValues) {
			String location = runtimeFieldValue.getLocationName();
			String label = runtimeFieldValue.getField();
			int detailRecordNo = runtimeFieldValue.getDetailRecordNo();
			String data = runtimeFieldValue.getValue();
			runtimeFieldValueMap.put(location + "$#$#" + label + "$#$#" + detailRecordNo, data);
		}
		try {
			for (int i = 0; i < pages.length(); i++) {
				JSONObject existingPage = pages.getJSONObject(i);
				String locationName = existingPage.get("PA_NAME").toString();

				JSONArray detailRecords = pages.getJSONObject(i).getJSONArray("DETAILRECORDS");

				for (int j = 0; j < detailRecords.length(); j++) {

					JSONArray existingFields = detailRecords.getJSONObject(j).getJSONArray("FIELDS");
					String DR_ID = detailRecords.getJSONObject(j).get("DR_ID").toString();

					JSONArray updatedFields = new JSONArray();
					for (int k = 0; k < existingFields.length(); k++) {
						JSONObject existingField = existingFields.getJSONObject(k);
						JSONObject updatedField = new JSONObject();
						updatedField.put("FLD_UID_TYPE", existingField.get("FLD_UID_TYPE"));
						updatedField.put("FLD_UID", existingField.get("FLD_UID"));
						updatedField.put("FLD_HAS_LOV", existingField.get("FLD_HAS_LOV"));
						updatedField.put("FLD_LABEL", existingField.get("FLD_NAME"));
						updatedField.put("FLD_TYPE", existingField.get("FLD_TYPE"));
						updatedField.put("ACTION", existingField.get("ACTION"));
						String fieldName = existingField.get("FLD_NAME").toString();
						String data = null;
						String existingData = existingField.get("DATA").toString();
						if (existingData != null && !existingData.equals("") && !existingData.equalsIgnoreCase("@@output")) {
							data = existingData;
						} else {
							if (runtimeFieldValueMap.containsKey(locationName + "$#$#" + fieldName + "$#$#" + DR_ID)) {
								data = runtimeFieldValueMap.get(locationName + "$#$#" + fieldName + "$#$#" + DR_ID);
							}
						}
						if (data == null)
							data = "";
						updatedField.put("DATA", data);
						logger.info(updatedField.toString());
						updatedFields.put(updatedField);
					}

					JSONObject updatedPage = new JSONObject();
					updatedPage.put("PA_NAME", locationName);
					updatedPage.put("PA_IS_MRB", existingPage.get("PA_IS_MRB"));
					updatedPage.put("PA_WAY_IN", existingPage.get("PA_WAY_IN"));
					updatedPage.put("PA_PARENT", existingPage.get("PA_PARENT"));
					updatedPage.put("PA_TYPE", existingPage.get("PA_TYPE"));
					updatedPage.put("PA_WAY_OUT", existingPage.get("PA_WAY_OUT"));
					updatedPage.put("DR_ID", DR_ID);
					updatedPage.put("FIELDS", updatedFields);
					updatedJson.put(updatedPage);
				}

			}
		} catch (Exception e) {
			logger.error("Error ", e);
		}
		return updatedJson;
		
	}

	private int AUT_RETRY_LIMIT;
	private int AUT_RETRY_INTERVAL;

	private void launchAndLogin(GuiApplicationAbstract oApplication, Aut aut, String aBrowser)
			throws DriverException, AutException {

		boolean launchAndLoginComplete = false;

		int retries = 0;

		while (!launchAndLoginComplete) {
			try {
				logger.info("Launching Application [{}] on [{}]", aut.getName(), aBrowser);
				oApplication.launch(aut.getURL());

				logger.info("Logging in to [{}]", aut.getName());
				if (aut.getPauseLocation() != null && aut.getPauseLocation().equalsIgnoreCase("before login")) {
					oApplication.explicitWait(aut.getPauseTime());
					oApplication.login(aut.getLoginName(), aut.getPassword());
				} else if (aut.getPauseLocation() != null && aut.getPauseLocation().equalsIgnoreCase("after login")) {
					oApplication.login(aut.getLoginName(), aut.getPassword());
					oApplication.explicitWait(aut.getPauseTime());
				} else
					oApplication.login(aut.getLoginName(), aut.getPassword());
				launchAndLoginComplete = true;

			} catch (ApplicationUnresponsiveException e) {
				logger.error(e.getMessage());
				if (retries >= AUT_RETRY_LIMIT) {
					logger.error("Could not launch and login to application {} after {} retries", aut.getName(),
							AUT_RETRY_LIMIT);
					throw new AutException(e.getMessage());
				} else {
					logger.info("Will try again after {} seconds", AUT_RETRY_INTERVAL);
					logger.info("Cleaning up open browser in the mean time");
					oApplication.destroy();
				}
			}
		}

	}

	private void navigateToFunction(GuiApplicationAbstract oApplication, String functionCode, String functionName,
			String menuHierarchy) throws AutException, BridgeException {
		boolean navigationComplete = false;
		int retries = 0;

		while (!navigationComplete) {
			try {
				oApplication.navigateToFunction(functionCode, functionName, menuHierarchy);
				navigationComplete = true;
			} catch (ApplicationUnresponsiveException | DriverException e) {
				logger.error(e.getMessage());
				if (retries >= AUT_RETRY_LIMIT) {
					logger.error("Could not navigate to function {} after {} attempts", functionCode, AUT_RETRY_LIMIT);
					throw new AutException(e.getMessage());
				} else {
					logger.info("Will try again after {} seconds", AUT_RETRY_INTERVAL);
					retries = retries + 1;
				}
			}
		}

	}

}
