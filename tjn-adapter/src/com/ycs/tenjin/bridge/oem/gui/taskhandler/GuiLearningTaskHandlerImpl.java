/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LearningTaskHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              	DESCRIPTION
 * 10-Sep-2016          Sriram Sridharan        	Newly Added For
 * 26-Oct-2016			Sriram Sriharan			 	Changed by Sriram for Req#TJN_23_18 
 * 17-Feb-2017			Sahana					  	defect #TENJINCG-110(Task goes to running state when the client IP is not reachable)
 * 07-Jul-2017          Sriram Sridharan          	TENJINCG-197
 * 08-09-2017			Sameer Gupta				Rewritten for New Adapter Spec
 * 16-03-2018			Preeti 						TENJINCG-73
 * 18-06-2019			Ashiki						V2.8-129
 * 27-08-2019			Ashiki						TENJINCG-1105
 * 06-02-2020			Roshni						TENJINCG-1168
 */

package com.ycs.tenjin.bridge.oem.gui.taskhandler;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Multimap;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.exceptions.ApplicationUnresponsiveException;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.DriverException;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.oem.TaskHandler;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationAbstract;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationFactory;
import com.ycs.tenjin.bridge.pojo.ExecutorProgress;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.LearnerResultBean;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Metadata;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.IterationHelper;
import com.ycs.tenjin.db.LearningHelper;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class GuiLearningTaskHandlerImpl implements TaskHandler {

	protected GuiApplicationAbstract oApplication;
	protected Module function;
	protected LearningHelper helper = new LearningHelper();
	protected String clientIp;
	protected int port;
	protected String browserType;
	protected Aut aut;
	protected List<Module> functions;
	protected int runId;
	
	protected String deviceId; 
	

	protected static final Logger logger = LoggerFactory
			.getLogger(GuiLearningTaskHandlerImpl.class);

	

	
	public GuiLearningTaskHandlerImpl(Aut aut, List<Module> functions,
			String clientIp, int port, String browserType, int runId,String deviceId) {
		this.aut = aut;
		this.functions = functions;
		this.clientIp = clientIp;
		this.port = port;
		this.browserType = browserType;
		// this.userId = user;
		this.runId = runId;
		
		this.deviceId = deviceId;
		
	}

	@Override
	public void execute() {
		logger.info("Beginning Learning Run");
		try {
			logger.debug("Checking if thread for run {} is alive", this.runId);
			if (CacheUtils.isRunAborted(this.runId)) {
				logger.debug("Thread for Run {} is interrupted.", this.runId);
				this.learnAbort();
				return;
			}
			this.helper.beginLearningRun(this.runId, false);
		} catch (Exception e) {
			logger.error("ERROR --> Could not udpate Start for Run ID [{}]",
					this.runId);
		}
		
		try {
			logger.info("Initializing Selenium adapter for application [{}]",
					aut.getName());
			
			this.oApplication = GuiApplicationFactory.getApplication(
					this.aut.getAdapterPackage(), aut, this.clientIp,
					this.port, this.browserType,this.deviceId);
			
			
			this.launchAndLogin(oApplication, aut, this.browserType);

			for (Module function : this.functions) {
				logger.info("Learning function [{}]", function.getCode());
				
				logger.debug("Checking if thread for run {} is alive", this.runId);
				if (CacheUtils.isRunAborted(this.runId)) {
					logger.debug("Thread for Run {} is interrupted.", this.runId);
					this.learnAbort();
					return;
				}
				this.function = function;
				this.learnFunction();
				logger.debug("Checking if thread for run {} is alive", this.runId);
				if (CacheUtils.isRunAborted(this.runId)) {
					logger.debug("Thread for Run {} is interrupted.", this.runId);
					this.learnAbort();
					return;
				}
				logger.info("Initiating Recovery Procedure...");
				this.oApplication.recover();
			}
		} catch (BridgeException e) {
			
			this.helper.updateLearningAuditTrail(this.functions, "ABORTED",
					e.getMessage());
		} catch (DriverException e) {
			
			this.helper.updateLearningAuditTrail(this.functions, "ABORTED",
					e.getMessage());
		} catch (AutException e) {
			
			this.helper.updateLearningAuditTrail(this.functions, "ABORTED",
					e.getMessage());
		} catch (Exception e) {
			
			this.helper.updateLearningAuditTrail(this.functions, "ABORTED",
					"Internal Error. Contact Tenjin Support");
		} finally {
			try {
				logger.info("Initiating recovery procedure...");
				this.oApplication.recover();
			} catch (AutException e) {
				logger.error("Recovery Failed", e);
			}
			catch (Exception e1) {
				logger.error("Recovery Failed", e1);
			}
			try {
				logger.info("Logging out of [{}]", this.aut.getName());
				this.oApplication.logout();
			} catch (AutException | DriverException e) {
				logger.error("ERROR while logging out", e);
			}

			logger.info("Destroying driver");
			this.oApplication.destroy();

			logger.info("Updating Run END");
			try {
				//Commented by Lokanath
				//this.helper.endLearningRun(this.runId);
				/*Added by Lokanath for TENJINCG-1193 starts*/
				String learningStatus=null;
				learningStatus=this.helper.hydrateRunStatus(this.runId);
				System.out.println("In GUI runstatus::::"+learningStatus);
				if(learningStatus.equalsIgnoreCase("Learning")||learningStatus.equalsIgnoreCase("Executing")){
					this.helper.endLearningRun(this.runId);
				}
				/*Added by Lokanath for TENJINCG-1193 ends*/
			} catch (DatabaseException e) {
				logger.error("ERROR --> Could not update END of Run [{}]",
						this.runId);
			}
			
		}
	}

	private LearnerResultBean learnFunction() {
		LearnerResultBean resultBean = new LearnerResultBean();
		resultBean.setAppId(this.function.getAut());
		resultBean.setFunctionCode(this.function.getCode());
		resultBean.setFunctionName(this.function.getName());
		long startMillis = System.currentTimeMillis();
		resultBean.setStartTimestamp(new Timestamp(new Date().getTime()));
		logger.info("Learning of function {} initiated --> {}",
				this.function.getCode(), resultBean.getStartTimestamp());
		int fieldCount=0;
		try {
			

			logger.info("Inserting Audit record for {}",
					this.function.getCode());
			

			this.helper.updateLearningAuditTrailStart(this.function
					.getLastSuccessfulLearningResult().getId());
			

			logger.info("Navigating to function {} using menu {}",
					this.function.getCode(), this.function.getMenuContainer());
			
			this.navigateToFunction(oApplication, function.getCode(),
					function.getName(), function.getMenuContainer());

			startMillis = System.currentTimeMillis();

			LearningTaskHandler objLearningTaskHandler = TaskHandlerFactory
					.getLearningTaskHandler(this.oApplication);
			Metadata metadata = objLearningTaskHandler.learnFunctionImpl(
					this.function, this.runId);
	           if(!(oApplication.getAdapterPackageOrTool().equalsIgnoreCase("uft")))
			this.persistMetadata(function, helper, metadata);

			resultBean.setEndTimestamp(new Timestamp(new Date().getTime()));
			resultBean.setElapsedTime(Utilities.calculateElapsedTime(
					startMillis, System.currentTimeMillis()));
			resultBean.setLearnStatus(TenjinConstants.ACTION_RESULT_SUCCESS);

			logger.info("Inserting End Audit Record for {}",
					this.function.getCode());
			
			fieldCount=this.helper.getFiledsLearnCount(function.getCode(),function.getAut());
			
			
			this.helper.updateLearningAuditTrail(function
					.getLastSuccessfulLearningResult().getId(), "COMPLETE", "Function learnt successfully.",fieldCount);
		} catch (AutException e) {
			logger.error(e.getMessage(), e);
			resultBean.setLearnStatus(TenjinConstants.ACTION_RESULT_ERROR);
			resultBean.setMessage(e.getMessage());
			resultBean.setEndTimestamp(new Timestamp(new Date().getTime()));
			resultBean.setElapsedTime(Utilities.calculateElapsedTime(
					startMillis, System.currentTimeMillis()));
			
			this.helper.updateLearningAuditTrail(function
					.getLastSuccessfulLearningResult().getId(), "ERROR", e
					.getMessage(),fieldCount);
			return resultBean;
		} catch (BridgeException e) {
			logger.error(e.getMessage(), e);
			resultBean.setLearnStatus(TenjinConstants.ACTION_RESULT_ERROR);
			resultBean.setMessage(e.getMessage());
			resultBean.setEndTimestamp(new Timestamp(new Date().getTime()));
			resultBean.setElapsedTime(Utilities.calculateElapsedTime(
					startMillis, System.currentTimeMillis()));
			this.helper.updateLearningAuditTrail(function
					.getLastSuccessfulLearningResult().getId(), "ERROR", e
					.getMessage(),fieldCount);
			return resultBean;
		} catch (LearnerException e) {
			resultBean.setLearnStatus(TenjinConstants.ACTION_RESULT_ERROR);
			resultBean.setMessage(e.getMessage());
			resultBean.setEndTimestamp(new Timestamp(new Date().getTime()));
			resultBean.setElapsedTime(Utilities.calculateElapsedTime(
					startMillis, System.currentTimeMillis()));
			this.helper.updateLearningAuditTrail(function
					.getLastSuccessfulLearningResult().getId(), "ERROR", e
					.getMessage(),fieldCount);
			return resultBean;
		} catch (Exception e) {
			logger.error("ERROR - An Internal error occurred", e);
			resultBean.setLearnStatus(TenjinConstants.ACTION_RESULT_ERROR);
			resultBean.setMessage("Learning failed due to an internal error");
			resultBean.setEndTimestamp(new Timestamp(new Date().getTime()));
			resultBean.setElapsedTime(Utilities.calculateElapsedTime(
					startMillis, System.currentTimeMillis()));
			this.helper.updateLearningAuditTrail(function
					.getLastSuccessfulLearningResult().getId(), "ERROR",
					"Internal Error. Contact Tenjin Support",fieldCount);
			return resultBean;
		}

		return resultBean;

	}

	
	private int AUT_RETRY_LIMIT;
	private int AUT_RETRY_INTERVAL;

	
	private void launchAndLogin(GuiApplicationAbstract oApplication, Aut aut,
			String aBrowser) throws DriverException, AutException, InterruptedException {

		boolean launchAndLoginComplete = false;

		int retries = 0;

		while (!launchAndLoginComplete) {
			try {
				

				logger.info("Launching Application [{}] on [{}]",
						aut.getName(), aBrowser);
				oApplication.launch(aut.getURL());

				logger.info("Logging in to [{}]", aut.getName());
				if(aut.getPauseLocation()!=null && aut.getPauseLocation().equalsIgnoreCase("before login"))
				{
					oApplication.explicitWait(aut.getPauseTime());
					oApplication.login(aut.getLoginName(), aut.getPassword());
				}
				else if(aut.getPauseLocation()!=null && aut.getPauseLocation().equalsIgnoreCase("after login"))
				{
					oApplication.login(aut.getLoginName(), aut.getPassword());
					oApplication.explicitWait(aut.getPauseTime());
				}
				else
					oApplication.login(aut.getLoginName(), aut.getPassword());
				launchAndLoginComplete = true;

			} catch (ApplicationUnresponsiveException e) {
				
				logger.error(e.getMessage());
				if (retries >= AUT_RETRY_LIMIT) {
					logger.error(
							"Could not launch and login to application {} after {} retries",
							aut.getName(), AUT_RETRY_LIMIT);
					throw new AutException(e.getMessage());
				} else {
					logger.info("Will try again after {} seconds",
							AUT_RETRY_INTERVAL);
					logger.info("Cleaning up open browser in the mean time");
					oApplication.destroy();
					
				}
			}
		}

	}

	private void navigateToFunction(GuiApplicationAbstract oApplication,
			String functionCode, String functionName, String menuHierarchy)
			throws AutException, BridgeException {
		boolean navigationComplete = false;
		int retries = 0;

		while (!navigationComplete) {
			try {
				oApplication.navigateToFunction(functionCode, functionName,
						menuHierarchy);
				navigationComplete = true;
			} catch (ApplicationUnresponsiveException | DriverException e) {
				logger.error(e.getMessage());
				if (retries >= AUT_RETRY_LIMIT) {
					logger.error(
							"Could not navigate to function {} after {} attempts",
							functionCode, AUT_RETRY_LIMIT);
					throw new AutException(e.getMessage());
				} else {
					logger.info("Will try again after {} seconds",
							AUT_RETRY_INTERVAL);
					retries = retries + 1;
				}
			}
		}

	}

 
	private void persistMetadata(Module function, LearningHelper helper,
			Metadata metadata) {
		logger.info("Processing metadata");
		int totalFieldCount = 0;

		for (TreeMap<String, TestObject> map : metadata.getTestObjectMap()) {
			for (@SuppressWarnings("unused")
			String key : map.keySet()) {
				totalFieldCount++;
			}
			totalFieldCount += map.keySet().size();
		}

		int totalPageAreas = 0;
		for (String key : metadata.getPageAreas().keySet()) {
			for (@SuppressWarnings("unused")
			Location l : metadata.getPageAreas().get(key)) {
				totalPageAreas++;

			}
			totalPageAreas += metadata.getPageAreas().get(key).size();
		}
		logger.info("Total page areas to be processed --> {}", totalPageAreas);
		logger.info("Total fields to be processed --> {}", totalFieldCount);

		logger.info("Clearing existing learning information for function {}",
			
				function.getCode());
		/*added by Roshni TENJINCG-1168 starts */
		Connection conn=null;
		try {
			conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch (DatabaseException e1) {
			
			logger.error("Error ", e1);
		}
		/*added by Roshni TENJINCG-1168 ends */
		try {
		/*Modified by Roshni TENJINCG-1168 starts */
		/*helper.clearLearningData(function.getCode(),
					this.oApplication.getAppId()); */
			helper.clearLearningData(function.getCode(),
					this.oApplication.getAppId(),conn);
		/*Modified by Roshni TENJINCG-1168 ends */
		} catch (Exception e) {
			logger.error(
					"ERROR occurred while clearing existing learning data", e);
			throw new LearnerException(
					"Could not clear existing metadata from the database for this function");
		}

		logger.info("Persisting locations");
		try {
			Multimap<String, Location> failed = helper.persistLocations(
					metadata.getPageAreas(), function.getCode(),
					/*Modified by Roshni TENJINCG-1168 starts */
					/*this.oApplication.getAppId()); */
					this.oApplication.getAppId(),conn);
					/*Modified by Roshni TENJINCG-1168 ends */
			int totalFailed = 0;
			for (String key : failed.keySet()) {
				totalFailed += failed.get(key).size();
			}

			logger.info("Successfully updated "
					+ (totalPageAreas - totalFailed)
					+ " page areas to the database out of " + totalPageAreas);
		} catch (Exception e) {
			logger.error("ERROR occurred while persisting page areas", e);
			throw new LearnerException(
					"Could not insert page areas to database");
		}

		logger.info("Persisting fields");

		try {
		/*Modified by Roshni TENJINCG-1168 starts */
			/*Multimap<String, TestObject> failed = helper
					.persistFieldsInWorkTable(metadata.getTestObjectMap(),
							function.getCode(), this.oApplication.getAppId());*/
			Multimap<String, TestObject> failed = helper
					.persistFieldsInWorkTable(metadata.getTestObjectMap(),
							function.getCode(), this.oApplication.getAppId(),conn);
			/*Modified by Roshni TENJINCG-1168 ends */
			int totalFailed = 0;
			for (String key : failed.keySet()) {
				
				totalFailed += failed.get(key).size();
			}
			logger.info("Successfully added " + (totalFieldCount - totalFailed)
					+ " Test Objects out of " + totalFieldCount);
		} catch (Exception e) {
			logger.error("ERROR inserting fields to work table", e);
			throw new LearnerException(
					"Could not insert learnt fields to the database");
		}

		logger.info("Processing persisted fields");
		try {
		/*Modified by Roshni TENJINCG-1168 starts */
			/*helper.processPersistedFields(function.getCode(),
					this.oApplication.getAppId());*/
			helper.processPersistedFields(function.getCode(),
					this.oApplication.getAppId(),conn);
					/*Modified by Roshni TENJINCG-1168 ends */
			logger.info("Successfully processed learnt fields");
		} catch (Exception e) {
			logger.error(
					"ERROR occurred while processing fields from work table", e);
			throw new LearnerException(
					"Could not insert learnt fields to the database", e);
		}
	}
	/*Added by Lokanath for TENJINCG-1193 starts*/
	private void learnAbort() {
		logger.info("Recieved call to ABORT this run.");
		logger.info("Initiating tear-down procedure");
		ExecutorProgress eProgress = null;
		try {
			 eProgress = (ExecutorProgress)CacheUtils.getObjectFromRunCache((this.runId));
		} catch (TenjinConfigurationException e1) {
			logger.error("Error ", e1);
		}
		String runAbortedUserId=this.clientIp;
		if(eProgress!=null&&eProgress.getRunAborteduserId()!=null){
			runAbortedUserId=eProgress.getRunAborteduserId();
		}
		logger.info("Aborting run");
		try {
			new IterationHelper().learnAbortRun(this.runId, runAbortedUserId);
		} catch (DatabaseException e) {
			logger.error(
					"ERROR aborting run. Could not update abort status to the run",
					e);
		}

		logger.info(
				"ABORT procedure complete! Run [{}] is now ABORTED by [{}]",
				this.runId, this.clientIp);
	}
	/*Added by Lokanath for TENJINCG-1193 ends*/
}
