/***


Yethi Consulting Private Ltd. CONFIDENTIAL

Name of this file:  CommonOem.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 25-02-2020		Sriram Sridharan		Newly added for TENJINCG-1175 (TENJINCG-1178)
 * 18-11-2020		    Ashiki				   TENJINCG-1213
 * 15-06-2021			Ashiki				   TENJINCG-1275
 */

package com.ycs.tenjin.bridge.oem;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.gui.taskhandler.CommonExecutionTaskHandlerImpl;
import com.ycs.tenjin.bridge.pojo.TaskManifest;

public class CommonOem implements Oem{
	private static final Logger logger = LoggerFactory.getLogger(CommonOem.class);
	
	private TaskManifest manifest;
	
	public void execute() throws BridgeException{
		String adapterType="";
		if(StringUtils.trim(manifest.getTaskType()).equals(BridgeProcess.EXECUTE)) {
			TaskHandler executionTaskHandler = new CommonExecutionTaskHandlerImpl(manifest);
			executionTaskHandler.execute();
		}
		/*Added by Ashiki for TENJINCG-1275 starts*/
		else if(StringUtils.trim(manifest.getTaskType()).equals(BridgeProcess.MSGEXECUTE)) {
			adapterType = "message";
			Oem oemAdapter = OemFactory.create(adapterType);
			oemAdapter.setManifest(manifest);
			
			oemAdapter.execute();
		}

		/*Added by Ashiki for TENJINCG-1275 ends*/
		
		
		else {
			 adapterType = "gui";
			logger.info("Initiating OEM Specific {} Task", manifest.getTaskType());
			/*Modified by Ashiki for TENJINCG-1213 start*/
			if (BridgeProcess.APILEARN.equalsIgnoreCase(this.manifest
					.getTaskType())
					|| BridgeProcess.APIEXTRACT.equalsIgnoreCase(this.manifest
							.getTaskType())||BridgeProcess.APICODELEARN.equalsIgnoreCase(this.manifest
									.getTaskType())) {
			/*Modified by Ashiki for TENJINCG-1213 start*/
				adapterType = "api";
			} 
			
			Oem oemAdapter = OemFactory.create(adapterType);
			oemAdapter.setManifest(manifest);
			
			oemAdapter.execute();
		}
	}
	
	public void setManifest(TaskManifest manifest) {
		this.manifest = manifest;
	}
}
