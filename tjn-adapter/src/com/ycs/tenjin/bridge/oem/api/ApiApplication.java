/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApiBridge.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 03-Apr-2017           Sriram Sridharan          Newly Added For
22-Sep-2017			sameer gupta			For new adapter spec
12-12-2018			Sriram Sridharan			TENJINCG-913
18-12-2020          Paneendra                   TENJINCG-1239
*/


package com.ycs.tenjin.bridge.oem.api;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Metadata;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;

public interface ApiApplication {
	
	public void initialize();
	
	public String getAdapterName() ;
	
	
	public ArrayList<Metadata> learn(Api api, ApiOperation operation, String learnType, String entityType) throws BridgeException;
	
	public Metadata learn(Api api, ApiOperation operation, String entityType) throws BridgeException;
	
	public Metadata learn(Api api, String entityType) throws BridgeException;
	
	public List<ApiOperation> getAllOperations(String url) throws BridgeException;
	
	public void getAllOperations(Api api) throws BridgeException;

	public IterationStatus executeIteration(JSONObject dataSet, int iteration, int expectedResponseCode /* Parameter added by sriram for TENJINCG-913 */) throws BridgeException;
	/*	Added By Paneendra for TENJINCG-1239 starts */
	public IterationStatus executeIteration(JSONObject dataSet, JSONObject headerdataset, int iteration,
			int expectedResponseCode) throws BridgeException;
	/*	Added By Paneendra for TENJINCG-1239 Ends */
	
}
