/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApiResponseTestDataHandlerImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 03-Nov-2017		    Sriram		   			Newly added for TENJINCG-414 
* 16-01-2018			Sriarm					TENJINCG-416
* 25-01-2019			Preeti					TJN252-80
*/


package com.ycs.tenjin.bridge.oem.api.datahandler;

import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.TestDataException;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.LearningHelper;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class ApiResponseTestDataHandlerImpl {
	private Workbook workbook;
	private static final int TDGIDCOL=0;
	private static final int TDUIDCOL=1;
	private static final int TDDRIDCOL=2;

	private static final int HEADER_ROW_NUM=4;
	private static final int SHEET_NAME_ROW_NUM=0;
	private static final int SHEET_NAME_CELL_NUM=1;

	private static final short QUERY_FIELD_COLOR = IndexedColors.LIGHT_ORANGE.getIndex();

	private Map<String, String> sheetNames;
	
	private static final Logger logger = LoggerFactory.getLogger(ApiResponseTestDataHandlerImpl.class);

	public static void main(String[] args) throws TestDataException {
		
	}
	
	public JSONObject loadResponseVerificationDataFromTemplate(String absoluteFilePath, int appId, String functionCode, String tdGid, String tdUid, String apiOperation, String entityType,int responseType) throws TestDataException {	
		JSONObject data = null;
		
		try{
			logger.info("Loading workbook from path [{}]", absoluteFilePath);
			this.workbook = WorkbookFactory.create(new File(absoluteFilePath));
		}catch(Exception e){
			
			logger.error(e.getMessage());
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		}
		
		
		LearningHelper helper = new LearningHelper();
		int numberOfSheets = workbook.getNumberOfSheets();
		logger.info("Loading Test Data");
		logger.info("Number of sheets --> [{}]", numberOfSheets);
		
		
		JSONArray pageAreas = this.scanResponseTestData(workbook, tdGid, functionCode, appId, tdUid, helper, apiOperation, entityType,responseType);
		
		try {
			data = new JSONObject();
			data.put("PAGEAREAS", pageAreas);
			data.put("TDUID", tdUid);
		} catch (JSONException e) {
			logger.error("ERROR while generating final JSONObject", e);
		}
		
		return data;
	}

	public JSONArray scanResponseTestData(Workbook workbook, String tdGid, String moduleCode, int appId, String tdUid, LearningHelper helper, String apiOperation, String entityType,int responseType) throws TestDataException{	
		Location mainLocation;
		try {
			mainLocation = helper.hydrateLocationHierarchy(appId, moduleCode, apiOperation, entityType,responseType);
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			throw new TestDataException("An internal error occurred while scanning Test data for this step. Please contact Tenjin Support.", e);
		} 
		
		this.loadSheetNames(workbook, entityType,responseType);
		JSONArray mainJson = this.recursivelyScanPageAreaForResponseData(workbook, mainLocation, tdGid, tdUid, "");
		return mainJson;
	}
	
	private Sheet getSheetByLocationName(Workbook workbook, String locationName) {
		if(this.sheetNames == null) {
			logger.error("ERROR - sheetNames map is not loaded.");
			return null;
		}
		logger.debug("Locating sheet for Page Area {}", locationName);
		String targetSheetName = this.sheetNames.get(locationName);
		if(Utilities.trim(targetSheetName).length() < 1) {
			logger.error("ERROR - No matching sheet found for page area {}", locationName);
			return null;
		}else{
			Sheet sheet = workbook.getSheet(targetSheetName);
			if(sheet == null) {
				logger.error("ERROR - No matching sheet found for page area {}", locationName);
				return null;
			}
			
			logger.debug("Target Sheet [{}] found for Page Area {}", sheet.getSheetName(), locationName);
			return sheet;
		}
	}
	
	private Map<String, Row> getRowsForTdUid(Sheet sheet, String tdGid, String tdUid, String pageAreaName, boolean isMrb, String parentDetailId) throws TestDataException {
		logger.debug("Getting all rows with TDGID [{}] and TDUID [{}] on sheet [{}]", tdGid, tdUid, sheet.getSheetName());
		if(pageAreaName.equalsIgnoreCase("balances") || pageAreaName.equalsIgnoreCase("data")) {
			System.err.println();
		}
		String sParentDetailId = Utilities.trim(parentDetailId).length()< 1 ? "1" : Utilities.trim(parentDetailId); //Temp variable to treat blank TDDRID ("") as "1" in case of Single Record Blocks
		try {
			Map<String, Row> tcRowMap = new TreeMap<String, Row>(); //Map that contains the Row Number as the key, and the detail row for the TDUID as the value
			Iterator<Row> rowIter = sheet.iterator();
			String aTdDrId = "";
			while(rowIter.hasNext()){
				Row row = rowIter.next();

				
				if(getCellValue(row, TDGIDCOL).equalsIgnoreCase(tdGid) && getCellValue(row, TDUIDCOL).equalsIgnoreCase(tdUid)){
					String detailIdForThisRow = getCellValue(row, TDDRIDCOL);
					detailIdForThisRow = Utilities.trim(detailIdForThisRow).length() < 1 ? "1" : Utilities.trim(detailIdForThisRow);
					if(!isMrb && detailIdForThisRow.equalsIgnoreCase(sParentDetailId)) {
						aTdDrId = getCellValue(row, TDDRIDCOL);
						tcRowMap.put(aTdDrId, row);
					}else if(isMrb) {
						if(Utilities.trim(parentDetailId).length() < 1 || (Utilities.trim(parentDetailId).length()>0 && detailIdForThisRow.startsWith(parentDetailId + "."))){
							aTdDrId = getCellValue(row, TDDRIDCOL);
							tcRowMap.put(aTdDrId, row);
						}
					}
				}
			}
			
			return tcRowMap;
		} catch (Exception e) {
			logger.error("ERROR occurred while scanning for Master Records for page area [{}]", pageAreaName, e);
			throw new TestDataException("An unexpected error occurred while scanning Test Data sheet for Page [" + pageAreaName + "]. Please contact Tenjin Support.");
		}
	}
	
	
	private JSONArray recursivelyScanPageAreaForResponseData(Workbook workbook, Location location, String tdGid, String tdUid, String parentDetailId) throws TestDataException {
		try {
			JSONArray pageJsonArray = new JSONArray();
			JSONObject pageJson = new JSONObject();
			int dataStartCol = 3;
			if(location!=null){
			pageJson = location.toJson();
			
			Sheet sheet = this.getSheetByLocationName(workbook, location.getLocationName());
			
			if(sheet == null) {
				logger.warn("Sheet for Page Area [{}] was not found. Scanning if children of this page have any data", location.getLocationName());
				if(location.getChildLocations() != null) {
					for(Location childLocation : location.getChildLocations()) {
						JSONArray pArray = this.recursivelyScanPageAreaForResponseData(workbook, childLocation, tdGid, tdUid, parentDetailId);
						if(pArray != null && pArray.length() > 0) {
							for(int i=0; i<pArray.length(); i++) {
								pageJsonArray.put(pArray.getJSONObject(i));
							}
						}
					}
				}
				
				return pageJsonArray;
			}
			
			
			Map<String, TestObject> testObjectMap = this.loadTestObjectMap(location.getTestObjects());
			if(sheet != null) {
				Map<String, Row> detailRecordRows = this.getRowsForTdUid(sheet, tdGid, tdUid, location.getLocationName(), location.isMultiRecordBlock(), parentDetailId);
				
				if(detailRecordRows.size() < 1) {
					logger.debug("No detail records found on page [{}]: Sheet [{}]", location.getLocationName(), sheet.getSheetName());
					return new JSONArray();
				}
				
				Row headerRow = sheet.getRow(HEADER_ROW_NUM);
				if(headerRow == null) {
					logger.error("ERROR - No header row found on sheet {}", sheet.getSheetName());
					throw new TestDataException("No Header Row was found on sheet " + sheet.getSheetName() + ". Please verify if you are using the correct Test Data Template.");
				}
				
				
				JSONArray detailRecordsArray = new JSONArray();
				for(String tdDrId : detailRecordRows.keySet()) {
					Row detailRecordRow = detailRecordRows.get(tdDrId);
					JSONObject detailRecordJson = new JSONObject();
					JSONArray fieldsJsonArray = new JSONArray();
					Cell detailRecordIdCell = detailRecordRow.getCell(TDDRIDCOL, Row.CREATE_NULL_AS_BLANK);
					String aTdDrId = getCellValue(detailRecordIdCell);
					if(Utilities.trim(aTdDrId).length() < 1 ) {
						aTdDrId = "1";
					}
					Iterator<Cell> headerRowIter = headerRow.cellIterator();
					while(headerRowIter.hasNext()) {
						Cell headerCell = headerRowIter.next();
						if(headerCell.getColumnIndex() < dataStartCol) {
							continue;
						}
					
						String fieldName = getCellValue(headerCell);
						Cell valueCell = detailRecordRow.getCell(headerCell.getColumnIndex(), Row.CREATE_NULL_AS_BLANK);
						String fieldValue = getCellValue(valueCell);
						
						if(Utilities.trim(fieldValue).length() < 1){
							continue;
						}
						
						fieldValue = this.resolve(fieldValue);
						
						
						short bgColor = valueCell.getCellStyle().getFillForegroundColor();
						String action = "VERIFY";
						if(bgColor == QUERY_FIELD_COLOR) {
							action = "QUERY";
						}
						
						TestObject t = testObjectMap.get(fieldName);
						if(t == null) {
							logger.error("ERROR - No matching test object found for page [{}], field [{}]", location.getLocationName(), fieldName);
							throw new TestDataException("Metadata does not exist for Field [" + fieldName + "] on page [" + location.getLocationName() + "]. You might need to re-learn this API / Function and use a more recent Test Data Template.");
						}
						
						JSONObject fieldJson = t.toJson();
						
						fieldJson.put("FLD_LABEL", t.getLabel());
						fieldJson.put("DATA", fieldValue);
						fieldJson.put("ACTION", action);
						fieldsJsonArray.put(fieldJson);
						
						logger.info("Page: [{}], Field: [{}], Data: [{}], Action: [{}]", location.getLocationName(), fieldName, fieldValue, action);
					}
					
					detailRecordJson.put("DR_ID", aTdDrId);
					detailRecordJson.put("FIELDS", fieldsJsonArray);
					JSONArray childLocationJsonArray = new JSONArray();
					if(location.getChildLocations() != null) {
						for(Location childLocation : location.getChildLocations()) {
							JSONArray cArray = this.recursivelyScanPageAreaForResponseData(workbook, childLocation, tdGid, tdUid, aTdDrId);
							if(cArray != null && cArray.length() > 0) {
								for(int i=0; i<cArray.length(); i++) {
									childLocationJsonArray.put(cArray.getJSONObject(i));
								}
							}
						}
					}
					
					detailRecordJson.put("CHILDREN", childLocationJsonArray);
					
					detailRecordsArray.put(detailRecordJson);
				}
				
				pageJson.put("DETAILRECORDS", detailRecordsArray);
				pageJsonArray.put(pageJson);
			}
			}
			
			
			return pageJsonArray;
		} catch (JSONException e) {
			logger.error(e.getMessage(),e);
			throw new TestDataException("An internal error occurred while scanning Test Data for this step. Please contact Tenjin Support.");
		}
	}
	
	private Map<String, TestObject> loadTestObjectMap(List<TestObject> testObjects) {
		Map<String, TestObject> map = new TreeMap<String, TestObject>();
		if(testObjects != null && testObjects.size() > 0) {
			for(TestObject t : testObjects) {
				map.put(t.getName(), t);
			}
		}
		
		return map;
	}
	
	private void loadSheetNames(Workbook workbook, String entityType,int responseType) {
		this.sheetNames = new HashMap<String, String> ();
		String pagePrefix = "";
		if("request".equalsIgnoreCase(entityType)) {
			pagePrefix = "(REQ)_";
		} else if("response".equalsIgnoreCase(entityType)){
			
			pagePrefix = "(RESP_"+responseType+")_";
		}
		for(int i=0; i<workbook.getNumberOfSheets(); i++) {
			Sheet sheet = workbook.getSheetAt(i);
			String sheetName=sheet.getSheetName();
			if(Utilities.trim(sheet.getSheetName()).equalsIgnoreCase("TTD_HELP")){
				continue;
			}else if(!Utilities.trim(sheet.getSheetName()).toLowerCase().startsWith(pagePrefix.toLowerCase())){
				logger.info("Skipping page [{}] as entity type is [{}]", sheetName, entityType);
				continue;
			}
			Row sheetNameRow = sheet.getRow(SHEET_NAME_ROW_NUM);
			if(sheetNameRow == null){
				logger.error("SHEET NAME ROW not found on sheet {}. Invalid template", sheet.getSheetName());
			}
			
			Cell sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);
			
			String locationName = getCellValue(sheetNameCell);
			logger.debug("Loading sheet Name {} to memory for page area {}", sheet.getSheetName(), locationName);
			this.sheetNames.put(locationName, sheet.getSheetName());
		}
	}
	
	private static String getCellValue(Cell cell){
		if(cell != null){
			cell.setCellType(Cell.CELL_TYPE_STRING);
			return Utilities.trim(cell.getStringCellValue());
		}else{
			return "";
		}
	}
	
	private static String getCellValue(Row row, int colIndex){
		
		if(row == null){
			logger.error("ERROR in getting cell value - row is null");
			return "";
		}
		try{
			Cell cell = row.getCell(colIndex, Row.CREATE_NULL_AS_BLANK);
			cell.setCellType(Cell.CELL_TYPE_STRING);
			return Utilities.trim(cell.getStringCellValue());
		}catch(Exception e){
			logger.error("ERROR getting value of cell with index {} on row {}", colIndex, row.getRowNum(), e);
			return "";
		}
	}
	
	
	private String resolve(String dataval) throws TestDataException{

		
		String output = dataval;
		
		ArrayList<String> params = new ArrayList<String>();
		if(dataval != null && dataval.startsWith("<<") && dataval.endsWith(">>")){
			dataval = dataval.replace("<<","");
			dataval = dataval.replace(">>","");
			String[] split = dataval.split(";;");
			if(split.length == 1){
				params.add(split[0]);
			}else{
				params.add(split[0]);
				for(int i=1;i<split.length;i++){
					String s = split[i];
					params.add(s);
				}
			}
		} else{
			return dataval;
		}
		
		if(params != null && params.size() == 1){
			logger.error("ERROR invalid usage. Data specified is {} but expecting a field name to fetch",dataval);
			throw new TestDataException("Invalid Syntax to get runtime output. Right syntax is <<TDUID(FIELD_IDENTIFIER)>>");
		}
		
		if(params != null && params.size() > 1){
			String tdUid = params.get(0);
			String fieldIdentifier = params.get(1);
			
			
			Connection projConn = null;
			try{
				projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				output = new LearningHelper().getRuntimeFieldOutput(projConn, tdUid, fieldIdentifier);
				logger.info("Output Avaliable is : " + output);
			}catch(DatabaseException e){
				throw new TestDataException(e.getMessage(),e);
			}catch(Exception e){
				logger.error("ERROR resolving field value {}", dataval,e);
				throw new TestDataException("Could not resolve value for " + dataval + " due to an internal error");
			}finally{
				try{
					projConn.close();
				}catch(Exception ignore){}
			}
						
			return output;
		}else{
			return dataval;
		}
		
		
	
	}
}
