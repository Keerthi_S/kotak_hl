/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LearningTaskHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY               DESCRIPTION
* 03-Apr-2017           Sriram Sridharan        Newly Added For
* 30-08-2017			Sriram Sridharan		T25IT-347
* 22-Sep-2017			sameer gupta			For new adapter spec
* 29-12-2017            Leelaprasad             TENJINCG-560
* 27-08-2019			Ashiki					TENJING-1105
* 22-09-2020			Ashiki					TENJINCG-1225
* 18-12-2020            Paneendra               TENJINCG-1239 &1243
* 05-02-2020			Ashiki					Tenj211-22  

*/


package com.ycs.tenjin.bridge.oem.api.taskhandler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.TaskHandler;
import com.ycs.tenjin.bridge.oem.api.ApiApplication;
import com.ycs.tenjin.bridge.oem.api.ApiApplicationFactory;
import com.ycs.tenjin.bridge.oem.api.generic.ApiLearnType;
import com.ycs.tenjin.bridge.pojo.LearnerProgress;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Metadata;
import com.ycs.tenjin.bridge.pojo.aut.Representation;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.LearningHelper;
import com.ycs.tenjin.util.Utilities;

public class ApiLearningTaskHandlerImpl  implements TaskHandler {
	private LearningHelper helper = new LearningHelper();
	private Aut aut;
	private int runId;
	private Api api;
	private String learnType;
	private ApiHelper apiHelper = new ApiHelper();
	private LearnerProgress progress;
	private long startTimeinMillis;
	private int completedOperations;
	/*Added by Ashiki for TENJINCG-1213 starts*/
	private List<Api> apis;
	private String apiType;
	/*Added by Ashiki for TENJINCG-1213 end*/
	private static final Logger logger = LoggerFactory.getLogger(ApiLearningTaskHandlerImpl.class);
	
	public ApiLearningTaskHandlerImpl(Api api){
		this.api = api;
	}

	public ApiLearningTaskHandlerImpl(Aut aut, Api api, String user, int runId, String learnType){
		this.aut = aut;
		this.api = api;
		this.runId = runId;
		this.learnType = learnType;
		this.progress = new LearnerProgress();
		this.completedOperations = 0;
	}
	
	
	/*Added by Ashiki for TENJINCG-1213 starts*/
	public ApiLearningTaskHandlerImpl(Aut aut, List<Api> apis, String user, int runId, String learnType, String apiType) {
		this.aut = aut;
		this.apis = apis;
		this.runId = runId;
		this.learnType = learnType;
		this.progress = new LearnerProgress();
		this.completedOperations = 0;
		this.apiType=apiType;
	}
	/*Added by Ashiki for TENJINCG-1213 end*/
	private void updateProgress(ApiOperation operation, String stage) {
		//this.progress.setTotalOperations(this.api.getOperations().size());
		
		if("end".equalsIgnoreCase(stage)){
			this.completedOperations++;
			this.progress.setCompletedOperations(this.completedOperations);
			this.progress.getOperations().add(operation);
		}else if(operation != null){
			this.progress.setCurrentOperation(operation.getName());
		}else{
			this.progress.setCurrentOperation("");
		}
		
		this.progress.setRunId(this.runId);
		/*Modified by Ashiki for TENJINCG-1213 starts*/
		double percentage;
		if(this.api!=null) {
			 percentage = ((double) this.completedOperations / (double) this.api.getOperations().size()) * 100;
		}
		else {
			 percentage = ((double) this.completedOperations / (double) this.apis.size()) * 100;	
		}
		/*Modified by Ashiki for TENJINCG-1213 starts*/
		double roundOff = Math.round(percentage * 100.0) / 100.0;
		int rWithoutDecimals = (int) roundOff;
		this.progress.setPercentage(rWithoutDecimals);
		
		
		String elapsedTime= Utilities.calculateElapsedTime(this.startTimeinMillis, System.currentTimeMillis());
		this.progress.setElapsedTime(elapsedTime);
		
		if(rWithoutDecimals >= 100){
			this.progress.setStatus(BridgeProcess.COMPLETE);
		}else{
			this.progress.setStatus(BridgeProcess.IN_PROGRESS);
		}
		
		CacheUtils.putObjectInRunCache(this.runId, this.progress);
		
		
	}
	
	public void execute() {
		logger.info("Beginning Learning Run");
		
		this.startTimeinMillis = System.currentTimeMillis();
		
		try{
			this.helper.beginLearningRun(this.runId, false);
			this.updateProgress(null, "start");
		}catch(Exception e){
			logger.error("ERROR --> Could not udpate Start for Run ID [{}]", this.runId, e);
		}
		

		logger.info("Initializing API Adapter for API ");
		ApiApplication bridge = null;
		
		try {
		/*Modified by Ashiki for TENJINCG-1213 starts*/
			if(api!=null) {
				bridge = ApiApplicationFactory.create(api.getType());
			}else if(apis!=null) {
				bridge=ApiApplicationFactory.create(apiType);	
			}
			 
			else
				bridge=ApiApplicationFactory.create(apiType);
			
				/*Modified by Ashiki for TENJINCG-1213 end*/
		} catch (BridgeException e1) {
			
			this.helper.updateLearningAuditTrail("ABORTED", e1.getMessage(), api.getOperations());
			for(ApiOperation operation: this.api.getOperations()) {
				operation.getLastSuccessfulLearningResult().setStatus("ABORTED");
				operation.getLastSuccessfulLearningResult().setMessage(e1.getMessage());
				this.updateProgress(operation, "end");
			}
		}
		
		if(bridge != null) {
		/*Added by Ashiki for TENJINCG-1213 starts*/
			if(this.apis==null) {
			/*Added by Ashiki for TENJINCG-1213 starts*/
				for(ApiOperation operation:api.getOperations()) {
					int fieldCount=0;
					try{
						logger.info("Updating Learning Audit for operation [{}]", operation.getName());
						this.helper.updateLearningAuditTrailStart(operation);
						
						logger.debug("Updating Progress");
						operation.getLastSuccessfulLearningResult().setStatus(BridgeProcess.IN_PROGRESS);
						this.updateProgress(operation, "start");
						
						if(this.learnType.equalsIgnoreCase(ApiLearnType.REQUEST_XML.toString()) && operation.getListRequestRepresentation().size() < 1 && operation.getListHeaderRepresentation().size() < 1 && operation.getListResponseRepresentation().size() < 1)
						{	
						logger.error("ERROR - Cannot learn this operation as header request and response descriptors are empty");
							throw new BridgeException("No Request or Response or Header content found to learn.");
						}
						
						
						// Added by paneendra for TENJINCG-1239 & 1243 starts //
						logger.info("Initializing API Bridge for learning header");
						bridge.initialize();
						
						logger.info("Learning header for Operation [{}]", operation.getName());
						ArrayList<Metadata> headMetaDataList = new ArrayList<Metadata>();
						headMetaDataList= bridge.learn(api, operation, this.learnType, "header");
				        int k=0;  
					
						this.helper.clearLearningData(api.getCode(),  this.aut.getId(), operation.getName(), "header");
					
						for (Metadata headMeta : headMetaDataList) {
							
							String headerDescriptor = headMeta.getDescriptor();
							for(String key : headMeta.getPageAreas().keySet()) {
								System.err.println("Key [" + key + "]");
								Collection<Location> locs = headMeta.getPageAreas().get(key);
								for(Location location:locs) {
									System.err.println("\tLocation --> [" + location.getLocationName() + "]");
								}
								
							}
							int respCode=0;
							logger.info("Persisting header metadata");
							
							if(!operation.getListHeaderRepresentation().isEmpty()){
								respCode=operation.getListHeaderRepresentation().get(k).getResponseCode();
								operation.getListHeaderRepresentation().get(k).setRepresentationDescription(headerDescriptor);
							}
	                       else {
								if (this.api.getType().contains("soap")) {

									respCode = 0;
									Representation representation = new Representation();
									representation.setMediaType("application/xml");
									representation
											.setRepresentationDescription(headerDescriptor);
									representation.setResponseCode(1);
									ArrayList<Representation> reqList = new ArrayList<Representation>();
									reqList.add(representation);
									operation.setListHeaderRepresentation(reqList);

								} else {
									respCode = 0;
								}

							}
							this.helper.persistMetadata(headMeta, this.aut.getId(), api.getCode(), operation.getName(), "header",respCode);
							
							k++;
							
						}
						// Added by paneendra for TENJINCG-1239 & 1243 ends //
						
					
						
						logger.info("Initializing API Bridge for learning request");
						bridge.initialize();
						
						logger.info("Learning request for Operation [{}]", operation.getName());
						ArrayList<Metadata> reqMetaDataList = new ArrayList<Metadata>();
						reqMetaDataList= bridge.learn(api, operation, this.learnType, "request");
				        int i=0;  
					
						this.helper.clearLearningData(api.getCode(),  this.aut.getId(), operation.getName(), "request");
					
						for (Metadata requestMeta : reqMetaDataList) {
							
							String requestDescriptor = requestMeta.getDescriptor();
							for(String key : requestMeta.getPageAreas().keySet()) {
								System.err.println("Key [" + key + "]");
								Collection<Location> locs = requestMeta.getPageAreas().get(key);
								for(Location location:locs) {
									System.err.println("\tLocation --> [" + location.getLocationName() + "]");
								}
								
							}
							int respCode=0;
							logger.info("Persisting request metadata");
							
							if(!operation.getListRequestRepresentation().isEmpty()){
								respCode=operation.getListRequestRepresentation().get(i).getResponseCode();
								operation.getListRequestRepresentation().get(i).setRepresentationDescription(requestDescriptor);
							}
	                       else {
								if (this.api.getType().contains("soap")) {

									respCode = 0;
									Representation representation = new Representation();
									representation.setMediaType("application/xml");
									representation
											.setRepresentationDescription(requestDescriptor);
									representation.setResponseCode(0);
									ArrayList<Representation> reqList = new ArrayList<Representation>();
									reqList.add(representation);
									operation.setListRequestRepresentation(reqList);

								} else {
									respCode = 0;
								}

							}
							this.helper.persistMetadata(requestMeta, this.aut.getId(), api.getCode(), operation.getName(), "request",respCode);
							
							i++;
							
						}
				     	
						
						logger.info("Initializing API Bridge for learning response");
						bridge.initialize();
						
						logger.info("Learning response for Operation [{}]", operation.getName());
						ArrayList<Metadata> respMetaDataList = new ArrayList<Metadata>();
						respMetaDataList = bridge.learn(api, operation, this.learnType, "response");
						int j=0;
						for (Metadata respmetadata : respMetaDataList) {
							String responseDescriptor = respmetadata.getDescriptor();
							for(String key : respmetadata.getPageAreas().keySet()) {
								System.err.println("Key [" + key + "]");
								Collection<Location> locs = respmetadata.getPageAreas().get(key);
								for(Location location:locs) {
									System.err.println("\tLocation --> [" + location.getLocationName() + "]");
								}
							}
							logger.info("Persisting response metadata");
							
							int respCode=0;
							if(this.api.getType().contains("soap") &&operation.getListResponseRepresentation().isEmpty()){
								respCode=200;
							}else{
							respCode=operation.getListResponseRepresentation().get(j).getResponseCode();
							}
							this.helper.persistMetadata(respmetadata, this.aut.getId(), api.getCode(), operation.getName(), "response",respCode);
							if(this.api.getType().contains("soap") &&operation.getListResponseRepresentation().isEmpty()){
							
								Representation representation = new Representation();
								representation.setMediaType("application/xml");
								representation.setRepresentationDescription(responseDescriptor);
								representation.setResponseCode(respCode);
								ArrayList<Representation> resList = new ArrayList<Representation>();
								resList.add(representation);
								operation.setListResponseRepresentation(resList);
							}else{
							operation.getListResponseRepresentation().get(j).setRepresentationDescription(responseDescriptor);
							}
							j++;
							
						}
						if(!this.api.getType().contains("soap")){
							this.helper.clearExtraExistingRecords(api.getCode(), operation.getName(),this.aut.getId());
						}
						
						logger.info("Updating API Descriptors");
						this.apiHelper.updateOperationDescriptors(this.aut.getId(), this.api.getCode(), operation);
						
						fieldCount=this.helper.getFiledsLearnCount(this.api.getCode(),this.aut.getId(),operation.getName());
						logger.info("Updating end audit for operation [{}]", operation.getName());
						this.helper.updateLearningAuditTrailEnd(BridgeProcess.COMPLETE, "", operation,fieldCount);
						
						operation.getLastSuccessfulLearningResult().setStatus(BridgeProcess.COMPLETE);
						this.updateProgress(operation, "end");
						
						 /* Added by Ashiki for TENJINCG-1225 starts*/
					      ArrayList<Location> location = new ArrayList<Location>();
					      location= this.helper.hydrateLearntfunctionData(this.aut.getId(), this.api.getCode());
					      ObjectMapper mapper = new ObjectMapper();
					      String jsonString = mapper.writeValueAsString(location);
					      this.helper.functionJsonData(jsonString,this.runId);
					      /* Added by Ashiki for TENJINCG-1225 ends*/
						
					} catch(BridgeException e){
						this.helper.updateLearningAuditTrailEnd("ERROR", e.getMessage(), operation,fieldCount);
						operation.getLastSuccessfulLearningResult().setStatus("ERROR");
						operation.getLastSuccessfulLearningResult().setMessage(e.getMessage());
						this.updateProgress(operation, "end");
					} catch(DatabaseException e) {
						this.helper.updateLearningAuditTrailEnd("ERROR", "A database error occurred. Please contact Tenjin Support.", operation,fieldCount);
						operation.getLastSuccessfulLearningResult().setStatus("ERROR");
						operation.getLastSuccessfulLearningResult().setMessage("A database error occurred. Please contact Tenjin Support.");
						this.updateProgress(operation, "end");
					} catch(Exception e) {
						logger.error("ERROR occurred while learning operation [{}]", operation, e);
						this.helper.updateLearningAuditTrailEnd("ERROR", "A internal error occurred. Please contact Tenjin Support.", operation,fieldCount);
						operation.getLastSuccessfulLearningResult().setStatus("ERROR");
						operation.getLastSuccessfulLearningResult().setMessage("A internal error occurred. Please contact Tenjin Support.");
						this.updateProgress(operation, "end");
					} 
				}
			}else {
				for (Api api : apis) {
					for(ApiOperation operation:api.getOperations()) {

						int fieldCount=0;
						try{
							logger.info("Updating Learning Audit for operation [{}]", operation.getName());
							this.helper.updateLearningAuditTrailStart(operation);
							
							logger.debug("Updating Progress");
							operation.getLastSuccessfulLearningResult().setStatus(BridgeProcess.IN_PROGRESS);
							this.updateProgress(operation, "start");
							
							if(this.learnType.equalsIgnoreCase(ApiLearnType.REQUEST_XML.toString()) && operation.getListRequestRepresentation().size() < 1 && operation.getListResponseRepresentation().size() < 1)
							{	
							logger.error("ERROR - Cannot learn this operation as both request and response descriptors are empty");
								throw new BridgeException("No Request or Response content found to learn.");
							}
							
							// Added by paneendra for TENJINCG-1239 & 1243 starts //
							logger.info("Initializing API Bridge for learning header");
							bridge.initialize();
							
							logger.info("Learning header for Operation [{}]", operation.getName());
							ArrayList<Metadata> headMetaDataList = new ArrayList<Metadata>();
							headMetaDataList= bridge.learn(api, operation, this.learnType, "header");
					        int k=0;  
						
							this.helper.clearLearningData(api.getCode(),  this.aut.getId(), operation.getName(), "header");
						
							for (Metadata headMeta : headMetaDataList) {
								
								String headerDescriptor = headMeta.getDescriptor();
								for(String key : headMeta.getPageAreas().keySet()) {
									System.err.println("Key [" + key + "]");
									Collection<Location> locs = headMeta.getPageAreas().get(key);
									for(Location location:locs) {
										System.err.println("\tLocation --> [" + location.getLocationName() + "]");
									}
									
								}
								int respCode=0;
								logger.info("Persisting header metadata");
								 /*Modified by Ashiki for Tenj211-22 starts*/  
								if(!(operation.getListHeaderRepresentation()==null)){
								 /*Modified by Ashiki for Tenj211-22 ends*/  
									respCode=operation.getListHeaderRepresentation().get(k).getResponseCode();
									operation.getListHeaderRepresentation().get(k).setRepresentationDescription(headerDescriptor);
								}
		                       else {
		                      /*Modified by Ashiki for Tenj211-22 starts*/  
									if (apiType.contains("soap")) {
 							 /*Modified by Ashiki for Tenj211-22 ends*/  
										respCode = 0;
										Representation representation = new Representation();
										representation.setMediaType("application/xml");
										representation
												.setRepresentationDescription(headerDescriptor);
										representation.setResponseCode(0);
										ArrayList<Representation> reqList = new ArrayList<Representation>();
										reqList.add(representation);
										operation.setListHeaderRepresentation(reqList);

									} else {
										respCode = 0;
									}

								}
								this.helper.persistMetadata(headMeta, this.aut.getId(), api.getCode(), operation.getName(), "header",respCode);
								
								k++;
								
							}
					     	
							// Added by paneendra for TENJINCG-1239 & 1243 ends //
							
							
							logger.info("Initializing API Bridge for learning request");
							bridge.initialize();
							
							logger.info("Learning request for Operation [{}]", operation.getName());
							ArrayList<Metadata> reqMetaDataList = new ArrayList<Metadata>();
							reqMetaDataList= bridge.learn(api, operation, this.learnType, "request");
					        int i=0;  
						
							this.helper.clearLearningData(api.getCode(),  this.aut.getId(), operation.getName(), "request");
						
							for (Metadata requestMeta : reqMetaDataList) {
								
								String requestDescriptor = requestMeta.getDescriptor();
								for(String key : requestMeta.getPageAreas().keySet()) {
									System.err.println("Key [" + key + "]");
									Collection<Location> locs = requestMeta.getPageAreas().get(key);
									for(Location location:locs) {
										System.err.println("\tLocation --> [" + location.getLocationName() + "]");
									}
									
								}
								int respCode=0;
								logger.info("Persisting request metadata");
								
								if(!operation.getListRequestRepresentation().isEmpty()){
									respCode=operation.getListRequestRepresentation().get(i).getResponseCode();
									operation.getListRequestRepresentation().get(i).setRepresentationDescription(requestDescriptor);
								}
		                       else {
									if (this.api.getType().contains("soap")) {

										respCode = 0;
										Representation representation = new Representation();
										representation.setMediaType("application/xml");
										representation
												.setRepresentationDescription(requestDescriptor);
										representation.setResponseCode(0);
										ArrayList<Representation> reqList = new ArrayList<Representation>();
										reqList.add(representation);
										operation.setListRequestRepresentation(reqList);

									} else {
										respCode = 0;
									}

								}
								this.helper.persistMetadata(requestMeta, this.aut.getId(), api.getCode(), operation.getName(), "request",respCode);
								
								i++;
								
							}
					     	
							
							logger.info("Initializing API Bridge for learning response");
							bridge.initialize();
							
							logger.info("Learning response for Operation [{}]", operation.getName());
							ArrayList<Metadata> respMetaDataList = new ArrayList<Metadata>();
							respMetaDataList = bridge.learn(api, operation, this.learnType, "response");
							int j=0;
							for (Metadata respmetadata : respMetaDataList) {
								String responseDescriptor = respmetadata.getDescriptor();
								for(String key : respmetadata.getPageAreas().keySet()) {
									System.err.println("Key [" + key + "]");
									Collection<Location> locs = respmetadata.getPageAreas().get(key);
									for(Location location:locs) {
										System.err.println("\tLocation --> [" + location.getLocationName() + "]");
									}
								}
								logger.info("Persisting response metadata");
								
								int respCode=0;
								if(apiType.contains("soap") &&operation.getListResponseRepresentation().isEmpty()){
									respCode=200;
								}else{
								respCode=operation.getListResponseRepresentation().get(j).getResponseCode();
								}
								this.helper.persistMetadata(respmetadata, this.aut.getId(), api.getCode(), operation.getName(), "response",respCode);
								if(apiType.contains("soap") &&operation.getListResponseRepresentation().isEmpty()){
								
									Representation representation = new Representation();
									representation.setMediaType("application/xml");
									representation.setRepresentationDescription(responseDescriptor);
									representation.setResponseCode(respCode);
									ArrayList<Representation> resList = new ArrayList<Representation>();
									resList.add(representation);
									operation.setListResponseRepresentation(resList);
								}else{
								operation.getListResponseRepresentation().get(j).setRepresentationDescription(responseDescriptor);
								}
								j++;
								
							}
							if(!apiType.contains("soap")){
								this.helper.clearExtraExistingRecords(api.getCode(), operation.getName(),this.aut.getId());
							}
							
							logger.info("Updating API Descriptors");
							this.apiHelper.updateOperationDescriptors(this.aut.getId(), apiType, operation);
							
							fieldCount=this.helper.getFiledsLearnCount(api.getCode(),this.aut.getId(),operation.getName());
							logger.info("Updating end audit for operation [{}]", operation.getName());
							this.helper.updateLearningAuditTrailEnd(BridgeProcess.COMPLETE, "", operation,fieldCount);
							
							operation.getLastSuccessfulLearningResult().setStatus(BridgeProcess.COMPLETE);
							this.updateProgress(operation, "end");
							
							 /* Added by Ashiki for TENJINCG-1225 starts*/
						      ArrayList<Location> location = new ArrayList<Location>();
						      location= this.helper.hydrateLearntfunctionData(this.aut.getId(), api.getCode());
						      ObjectMapper mapper = new ObjectMapper();
						      String jsonString = mapper.writeValueAsString(location);
						      this.helper.functionJsonData(jsonString,this.runId);
							
						} catch(BridgeException e){
							this.helper.updateLearningAuditTrailEnd("ERROR", e.getMessage(), operation,fieldCount);
							operation.getLastSuccessfulLearningResult().setStatus("ERROR");
							operation.getLastSuccessfulLearningResult().setMessage(e.getMessage());
							this.updateProgress(operation, "end");
						} catch(DatabaseException e) {
							this.helper.updateLearningAuditTrailEnd("ERROR", "A database error occurred. Please contact Tenjin Support.", operation,fieldCount);
							operation.getLastSuccessfulLearningResult().setStatus("ERROR");
							operation.getLastSuccessfulLearningResult().setMessage("A database error occurred. Please contact Tenjin Support.");
							this.updateProgress(operation, "end");
						} catch(Exception e) {
							logger.error("ERROR occurred while learning operation [{}]", operation, e);
							this.helper.updateLearningAuditTrailEnd("ERROR", "A internal error occurred. Please contact Tenjin Support.", operation,fieldCount);
							operation.getLastSuccessfulLearningResult().setStatus("ERROR");
							operation.getLastSuccessfulLearningResult().setMessage("A internal error occurred. Please contact Tenjin Support.");
							this.updateProgress(operation, "end");
						} 
					
					}
				}	
			}
			
			
			
		}
		
		logger.info("Updating run end");
		try{
			this.helper.endLearningRun(this.runId);
		}catch(DatabaseException e){
			logger.error("ERROR --> Could not update END of Run [{}]", this.runId);
		} 
		
		logger.info("API Learning Run [{}] Completed.", this.runId);
		
		
		
	
	}

/*Added by Ashiki for TENJINCG-1213 starts*/
	public List<Api> getApis() {
		return apis;
	}

	public void setApis(List<Api> apis) {
		this.apis = apis;
	}

	public String getApiType() {
		return apiType;
	}

	public void setApiType(String apiType) {
		this.apiType = apiType;
	}
	/*Added by Ashiki for TENJINCG-1213 ends*/
}
