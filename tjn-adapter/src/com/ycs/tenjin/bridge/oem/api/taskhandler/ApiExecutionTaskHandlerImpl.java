/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApiExecutionTaskHandlerImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright  � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                  CHANGED BY              DESCRIPTION
* 03-Apr-2017           Sriram Sridharan        Newly Added For
* 22-Sep-2017			sameer gupta			For new adapter spec
* 03-Nov-2017			Sriram Sridharan		Fixes for TENJINCG-414
* 27-Nov-2017			Sriram Sridharan		Fixes for TENJINCG-514
* 19-12-2017       		Leelaprasad             TENJINCG-554
* 02-02-2018       		Padmavathi              TENJINCG-577
* 02-02-2018			Preeti					TENJINCG-101
* 08-05-2018			Preeti					TENJINCG-625
* 15-05-2018           Padmavathi              TENJINCG-647
* 16-05-2018           Padmavathi              TENJINCG-647
* 18-05-2018           Padmavathi              TENJINCG-647
* 12-12-2018			Sriram Sridharan		TENJINCG-913
* 18-01-2019           Padmavathi              TJN252-74
* 11-03-2019           Padmavathi              TENJINCG-997
* 22-03-2019			Preeti					TENJINCG-1018
* 18-11-2019			Pushpa					TENJINCG-1228
*/

package com.ycs.tenjin.bridge.oem.api.taskhandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.TestDataException;
import com.ycs.tenjin.bridge.oem.TaskHandler;
import com.ycs.tenjin.bridge.oem.api.ApiApplication;
import com.ycs.tenjin.bridge.oem.api.ApiApplicationFactory;
import com.ycs.tenjin.bridge.oem.api.datahandler.ApiResponseTestDataHandlerImpl;
import com.ycs.tenjin.bridge.oem.api.datahandler.ApiTestDataHandlerImpl;
import com.ycs.tenjin.bridge.oem.api.generic.ApiResponseValidator;
import com.ycs.tenjin.bridge.pojo.ExecutorProgress;
import com.ycs.tenjin.bridge.pojo.aut.Api;
import com.ycs.tenjin.bridge.pojo.aut.ApiOperation;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Representation;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.ApiHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.IterationHelper;
import com.ycs.tenjin.db.RunDefectHelper;
import com.ycs.tenjin.defect.RunDefectProcessor;
import com.ycs.tenjin.util.PatternMatch;
import com.ycs.tenjin.util.Utilities;

public class ApiExecutionTaskHandlerImpl implements TaskHandler {
	private int runId;
	private Map<String, Map<String, Aut>> autMap;
	private List<ExecutionStep> steps;
	private Map<String, String> testDataPaths;
	private Map<Integer, List<String>> tdUidMap;
	private ExecutorProgress progress;
	private int completedTransactions;
	private int totalTransactions;
	
	private static final Logger logger = LoggerFactory.getLogger(ApiExecutionTaskHandlerImpl.class);
	
	private int projectId;
	private String userId;
	private long startTimeinMillis;
	private Map<Integer,String> passedStepsStatusMap;
	
	public ApiExecutionTaskHandlerImpl(int runId, Map<String, Map<String, Aut>> autMap, List<ExecutionStep> steps, Map<Integer, List<String>> tdUidMap,
			Map<String, String> testDataPaths, int projectId, String userId,Map<Integer,String> passedStepsStatusMap) {
		super();
		this.runId = runId;
		this.autMap = autMap;
		this.steps = steps;
		this.tdUidMap = tdUidMap;
		this.testDataPaths = testDataPaths;
		this.projectId = projectId;
		this.userId = userId;
		this.progress = new ExecutorProgress();
		this.completedTransactions = 0;
		this.passedStepsStatusMap=passedStepsStatusMap;
	}
	
	private void updateProgress(String tdUid, String status, String message, String resultValidationStatus, String stage) {
		
		
		
		if("end".equalsIgnoreCase(stage)) {
			this.completedTransactions++;
			this.progress.setCompletedTransactions(this.completedTransactions);
		}
		
		Map<String, String> txnMap = new HashMap<String, String>();
		txnMap.put("status", status);
		txnMap.put("message", message);
		txnMap.put("resultValidationStatus", resultValidationStatus);
		
		this.progress.getTransactionsMap().put(tdUid, txnMap);
		
		double percentage = ((double) this.completedTransactions / (double) this.totalTransactions) * 100;
		double roundOff = Math.round(percentage * 100.0) / 100.0;
		int rWithoutDecimals = (int) roundOff;
		this.progress.setPercentage(rWithoutDecimals);
		
		String elapsedTime= Utilities.calculateElapsedTime(this.startTimeinMillis, System.currentTimeMillis());
		this.progress.setElapsedTime(elapsedTime);
		
		if(rWithoutDecimals >= 100){
			this.progress.setStatus(BridgeProcess.COMPLETE);
		}else{
			this.progress.setStatus(BridgeProcess.IN_PROGRESS);
		}
		
		CacheUtils.putObjectInRunCache(this.runId, this.progress);
	}
	
	private void updateProgress(String tdUid, String status, String message, String resultValidationStatus, boolean hasRuntimeValues, String stage) {
				
		if("end".equalsIgnoreCase(stage)) {
			this.completedTransactions++;
			this.progress.setCompletedTransactions(this.completedTransactions);
		}
		
		Map<String, String> txnMap = new HashMap<String, String>();
		txnMap.put("status", status);
		txnMap.put("message", message);
		txnMap.put("resultValidationStatus", resultValidationStatus);
		if(hasRuntimeValues) {
			txnMap.put("runtimevalues", "yes");
		}else{
			txnMap.put("runtimevalues", "no");
		}
		
		this.progress.getTransactionsMap().put(tdUid, txnMap);
		
		double percentage = ((double) this.completedTransactions / (double) this.totalTransactions) * 100;
		double roundOff = Math.round(percentage * 100.0) / 100.0;
		int rWithoutDecimals = (int) roundOff;
		this.progress.setPercentage(rWithoutDecimals);
		
		String elapsedTime= Utilities.calculateElapsedTime(this.startTimeinMillis, System.currentTimeMillis());
		this.progress.setElapsedTime(elapsedTime);
		
		if(rWithoutDecimals >= 100){
			this.progress.setStatus(BridgeProcess.COMPLETE);
		}else{
			this.progress.setStatus(BridgeProcess.IN_PROGRESS);
		}
		
		CacheUtils.putObjectInRunCache(this.runId, this.progress);
	}
	
	@SuppressWarnings("unchecked")
	public void execute() {
		Map<Integer,String> execStatusMap=new HashMap<Integer,String>();
		List<String> execStatusList=new ArrayList<String>();
		logger.info("Beginning Execution of Tenjin Run [{}]", this.runId);
		this.startTimeinMillis = System.currentTimeMillis();
		if(this.tdUidMap != null) {
			for(Integer stepRecordId : this.tdUidMap.keySet()) {
				List<String> tdUids = this.tdUidMap.get(stepRecordId);
				if(tdUids != null ) {
					for (@SuppressWarnings("unused") String tdUid:tdUids) {
						this.totalTransactions++;
					}
				}
			}
		}
		
		logger.info("Total Transactions in this run --> [{}]", this.totalTransactions);
		this.progress.setTotalTransactions(this.totalTransactions);
		
		IterationHelper helper = new IterationHelper();
		ApiHelper apiHelper = new ApiHelper();
		
		if(this.steps == null){
			logger.error("ERROR --> Steps is null. Nothing to execute. Execution is terminated");
			return;
		}
		
		logger.info("Updating Run Start");
		try{
			helper.beginRun(this.runId);
		}catch(Exception ignore){}
		
		for(ExecutionStep step:this.steps){
			logger.info("Begin execution of step --> [{}], Record ID [{}]", step.getId(), step.getRecordId());
			logger.info("Loading application information for step [{}]", step.getId());
			Map<String, Aut> autMap = this.autMap.get(step.getAppName());
			
			Aut aut = autMap.get(step.getAutLoginType());
			
			step.setAut(aut);
			
			logger.info("Loading Information about API [{}], in application [{}]", step.getApiCode(), aut.getName());
			Api api = null;
			ApiOperation operation = null;
			try {
				api = apiHelper.hydrateApi(aut.getId(), step.getApiCode());
				step.setApi(api);
				operation = apiHelper.hydrateApiOperation(aut.getId(), step.getApiCode(), step.getOperation());
				step.setApiOperation(operation);
			} catch (DatabaseException e) {
				
				logger.error("ERROR loading API Information", e);
				try {
					logger.info("Setting results for all transactions in Step [{}] - Record ID[{}] to E", step.getId(), step.getRecordId());
					helper.updateRunStatusForAllTransactions(this.runId, step.getRecordId(), "E", "Could not load information about API " + step.getApiCode() + ".");
					execStatusList.add("Error");
				} catch (DatabaseException e1) {
					
					logger.error(e1.getMessage());
				}
			}
			
			if(api == null) {
				logger.error("Invalid API Code [{}] in Application [{}]", step.getApiCode(), aut.getName());
				try {
					logger.info("Setting results for all transactions in Step [{}] - Record ID[{}] to E", step.getId(), step.getRecordId());
					helper.updateRunStatusForAllTransactions(this.runId, step.getRecordId(), "E", "Invalid API [" + step.getApiCode() + "].");
					execStatusList.add("Error");
				} catch (DatabaseException e1) {
					
					logger.error(e1.getMessage());
				}
			}
			
			logger.info("Loading executor for [{}]", api.getType());
			
			
			logger.info("Initializing API Adapter for API [{}]", api.getCode());
			ApiApplication bridge = null;
			
			try {
				bridge = ApiApplicationFactory.getExecutor(api.getType(), this.runId, step);
			} catch (BridgeException e) {
				
				logger.error("Executor invocation failed for step [{}] - Record ID [{}]", step.getId(), step.getRecordId());
				try {
					logger.info("Setting results for all transactions in Step [{}] - Record ID[{}] to E", step.getId(), step.getRecordId());
					
					helper.updateRunStatusForAllTransactions(this.runId, step.getRecordId(), "E", e.getMessage());
					
					execStatusList.add("Error");
				} catch (DatabaseException e1) {
					
					logger.error(e1.getMessage());
				}
				continue;
			}
			
			List<String> tdUids = this.tdUidMap.get(step.getRecordId());
			int txnLimit = step.getTransactionLimit();
			
			ApiTestDataHandlerImpl handler = new ApiTestDataHandlerImpl();
			
			boolean dependencyCheck=false;
			String previousStepStatus=null;
			
			if(step.getDependencyRule()!=null && !step.getDependencyRule().equalsIgnoreCase("-1")&& step.getDependentStepId()!=0){
				 previousStepStatus=execStatusMap.get(step.getDependentStepId());
				 if(previousStepStatus==null && this.passedStepsStatusMap!=null){
					 previousStepStatus=this.passedStepsStatusMap.get(step.getDependentStepId());
				 }
				if(previousStepStatus!=null && !previousStepStatus.equalsIgnoreCase(step.getDependencyRule())){
					dependencyCheck=true;
				}
			}
			for(int i=0; i<txnLimit; i++){
				int iteration = i+1;
				Date startTime = new Date();
				logger.info("Beginning Execution of Test Case [{}], Test Step [{}], Iteration [{}]", step.getTestCaseId(), step.getId(), iteration);
				String tdUid = tdUids.get(i);
				logger.info("TDUID for this transaction --> [{}]", tdUid);

				logger.debug("Setting status of Transaction [{}] to X", tdUid);
				try {
					helper.updateRunStatus(this.runId, step.getRecordId(), iteration, "X",startTime);
				} catch (DatabaseException e) {}
				
				this.updateProgress(tdUid, "X", "", "N/A","start");

				
						if(dependencyCheck){
								try {
									Date endTime = new Date();
									helper.updateRunStatus(this.runId, step.getRecordId(), iteration, "E", "Error due to not fulfilled dependency rules",endTime, aut.getAccessToken(), operation.getAuthenticationType());
									this.updateProgress(tdUid, "E", "Error due to not fulfilled dependency rules", "N/A","end");
									execStatusList.add("Error");
								  }catch (DatabaseException ignore) {}

								continue;
							}
						
				
				String folderPath = this.testDataPaths.get(step.getFunctionCode()+"_"+step.getOperation());
				JSONObject iterationData;

				logger.info("Loading test data for step [REQUEST]");

				try {
					
					if(Utilities.trim(api.getType()).toLowerCase().startsWith("soap")) {
						iterationData = handler.loadTestDataFromTemplate(folderPath, aut.getId(), step.getApiCode(), step.getTdGid(), tdUid,step.getTestCaseRecordId(),step.getId(), step.getOperation(), "request");
					}else{
						/*Modified by Pushpa for TJN27-169 starts*/
						Representation requestRepresentation=step.getApiOperation().getListRequestRepresentation().get(0);
						if(requestRepresentation.getMediaType().equalsIgnoreCase("application/xml")){
							iterationData = handler.loadTestDataFromTemplate(folderPath, aut.getId(), step.getApiCode(), step.getTdGid(), tdUid,step.getTestCaseRecordId(),step.getId(), step.getOperation(), "request");
						}else{
							iterationData = handler.loadTestDataFromTemplate(folderPath, aut.getId(), step.getApiCode(), step.getTdGid(), tdUid, step.getApiOperation().getName(), "request", 0);
						}
					/*Modified by Pushpa for TJN27-169 ends*/
					}
				} catch (TestDataException e) {
					
					logger.error("Could not load test data",e );
					try {
						Date endTime = new Date();
						
						helper.updateRunStatus(this.runId, step.getRecordId(), iteration, "E", e.getMessage(),endTime, aut.getAccessToken(), operation.getAuthenticationType());
						
						
					execStatusList.add("Error");
					} catch (DatabaseException ignore) {}
					this.updateProgress(tdUid, "E", e.getMessage(), "N/A", "end");
					execStatusList.add("Error");
					continue;
				}
				IterationStatus iStatus = null;

				try {
					logger.info("Executing Iteration...");
					
					iStatus = bridge.executeIteration(iterationData, iteration, step.getResponseType());
				}  catch (Exception e) {
					logger.error("ERROR - Uncaught exception in executeIteration", e);
					try {
						Date endTime = new Date();
						helper.updateRunStatus(this.runId, step.getRecordId(), iteration, "E", "Execution aborted abnormally due to an internal error. Please contact Tenjin Support.",endTime, aut.getAccessToken(), operation.getAuthenticationType());
					execStatusList.add("Error");
						
					} catch (DatabaseException ignore) {}
					this.updateProgress(tdUid, "E", "Execution aborted abnormally due to an internal error. Please contact Tenjin Support.", "N/A", "end");
					continue;
				}

				if(iStatus != null){
					
					if(iStatus.getMessage()==null)
					{
						iStatus.setMessage(" ");
					}
					
					if("Negative".equalsIgnoreCase(step.getType()) && iStatus.getResult().equalsIgnoreCase("S")){
						logger.info("Marking this step as a failure as the transaction is Passed but the Step is a negative step");
						iStatus.setResult("F");
					}
					
					
					if("Negative".equalsIgnoreCase(step.getType()) && (iStatus.getResult().equalsIgnoreCase("F") || iStatus.getResult().equalsIgnoreCase("E"))){
						String[] stepMessages=step.getExpectedResult().split(";");
						
						List<String> errorMessages = new ArrayList<String>();
						for (RuntimeFieldValue runTimeField : iStatus.getRuntimeFieldValues()) {
							if (runTimeField.getField().contains("WSERROR")) {
								errorMessages.add(runTimeField.getValue());
							}
						}
						boolean allMessageValidationsPassed = true;
						if(stepMessages != null && stepMessages.length > 0) {
							PatternMatch pm = new PatternMatch();
							for(String stepMessage : stepMessages) {
								ValidationResult r = new ValidationResult();
								r.setExpectedValue(stepMessage);
								r.setDetailRecordNo("1");
								r.setField("Transaction Message");
								r.setIteration(iteration);
								r.setPage("Negative Test Step Validation");
								r.setResValId(0);
								r.setRunId(this.runId);
								r.setTestStepRecordId(step.getRecordId());
								boolean matchFound = false;
								if(errorMessages != null && errorMessages.size() > 0) {
									
									for(String errorMessage : errorMessages) {
										if(pm.matchString(errorMessage, stepMessage)) {
											r.setActualValue(errorMessage);
											r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
											matchFound = true;
											break;
										}
 									}
								}
								
								if(!matchFound) {
									allMessageValidationsPassed = false;
									r.setActualValue("The pattern did not match any message from the Response body");
									r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
								}
								
								if(iStatus.getValidationResults() != null){
									iStatus.getValidationResults().add(0, r);
								}else{
									List<ValidationResult> rs = new ArrayList<ValidationResult>();
									rs.add(r);
									iStatus.setValidationResults(rs);
								}
							}
						}
						
						if(allMessageValidationsPassed) {
							iStatus.setResult("S");
						}
						
					 
				}
					
					
					String resultValidationStatus = "N/A";
					if(iStatus.getResult().equalsIgnoreCase("S")) {
						logger.info("Loading test data for step [RESPONSE]");
						
						JSONObject expectedResponseData = null;
						ApiResponseValidator validator = null;
						
						boolean okToValidate = true;
						try {
							
							if(Utilities.trim(step.getApi().getType()).toLowerCase().startsWith("rest")) {
								
								int respType=step.getResponseType();
								if(respType!=0){
									expectedResponseData = new ApiResponseTestDataHandlerImpl().loadResponseVerificationDataFromTemplate(folderPath, aut.getId(), step.getApiCode(), step.getTdGid(),tdUid, step.getOperation(), "response",step.getResponseType());	
									okToValidate = true;
								}else{
									okToValidate=false;
								}
								
							}else{
								expectedResponseData = handler.loadTestDataFromTemplate(folderPath, aut.getId(), step.getApiCode(), step.getTdGid(), tdUid,step.getTestCaseRecordId(),step.getId(), step.getOperation(), "response");
							}
							validator = ApiApplicationFactory.getResponseValidator(api.getType(), this.runId, step, iteration);
						} catch (TestDataException e) {
							
							logger.error("ERROR loading response data for validation", e);
							iStatus.setResult("E");
							iStatus.setMessage("Execution passed, but Tenjin could not load verification data. Please contact Tenjin Support");
							okToValidate = false;
						} catch (BridgeException e) {
							
							logger.error(e.getMessage());
							iStatus.setResult("E");
							iStatus.setMessage("Execution passed, but Tenjin could not initialize Validator for Response validation. Please contact Tenjin Support.");
							okToValidate = false;
						}
						
						if(okToValidate){
							Map<String, Object> map =  validator.validateResponse(expectedResponseData, iStatus.getApiResponseDescriptor());
							
							List<ValidationResult> results = null;
							List<RuntimeFieldValue> rfvs = null;
							if(map != null) {
								results = (List<ValidationResult>) map.get("VALIDATIONS");
								rfvs = (List<RuntimeFieldValue>) map.get("RUNTIME_VALUES");
							}
							
							
							if(rfvs != null) {
								iStatus.getRuntimeFieldValues().addAll(rfvs);
							}
							
							if(results != null) {
								iStatus.setValidationResults(results);
							}
							
							if(results != null && results.size() > 0) {
								
								int f=0;
								int e=0;
								
								for(ValidationResult result : results) {
									if("fail".equalsIgnoreCase(result.getStatus())) {
										f++;
									}else if("pass".equalsIgnoreCase(result.getStatus())) {
									}else{
										e++;
									}
								}
								
								if(e > 0) {
									resultValidationStatus = TenjinConstants.ACTION_RESULT_ERROR;
									iStatus.setResult("F"); 
								}else if(f > 0) {
									resultValidationStatus = TenjinConstants.ACTION_RESULT_FAILURE;
									iStatus.setResult("F"); 
								}else {
									resultValidationStatus = TenjinConstants.ACTION_RESULT_SUCCESS;
								}
							}
						}
					}
					
					
					try {
						logger.info("Updating Results...");
						String result = iStatus.getResult();
						if(Utilities.trim(result).equalsIgnoreCase("")){
							logger.warn("iStatus.getResult() was either null or blank. Hence, setting to E");
							iStatus.setResult("E");
							
							if(Utilities.trim(iStatus.getMessage()).equalsIgnoreCase("")){
								logger.warn("Application did not return any message for this transaction. hence, defaulting the message");
								iStatus.setMessage("No message received from Application");
							}

						}
						
						Date endTime = new Date();
						helper.updateRunStatus(this.runId, step.getRecordId(), iteration, iStatus.getResult(), iStatus.getMessage(),endTime, aut.getAccessToken(), operation.getAuthenticationType());
						 result = iStatus.getResult();
						if(result.equalsIgnoreCase("E")){
							execStatusList.add("Error");
		                }else if(result.equalsIgnoreCase("F")){
		                	execStatusList.add("Fail");
		                }else{
		                	execStatusList.add("Pass");                 
		                }
						logger.info("Persisting validation resulsts...");
						helper.persistValidationResults(iStatus.getValidationResults(), this.runId, step.getRecordId(), iteration);

						logger.info("Persisting Run-Time Values...");
						helper.persistRuntimeFieldValues(iStatus.getRuntimeFieldValues(), this.runId, step.getRecordId(), iteration);

						logger.info("Persisting Run-Time Screenshots...");
						helper.persistRuntimeScreenshots(iStatus.getRuntimeScreenshots(), this.runId, step.getRecordId(), iteration,false);
						
						logger.info("Updating Request and Response...");
						helper.updateWSRequestAndResponse(this.runId, step.getRecordId(), iteration, iStatus.getApiRequestDescriptor(), iStatus.getApiResponseDescriptor());
						/*Added by Pushpa for TENJINCG-1228 starts*/
						logger.info("Updating Request Logs...");
						/*Added by Pushpa for TENJINCG-1228 ends*/
						helper.updateRequestLogs(this.runId, step.getRecordId(), iteration, iStatus.getReqLog());
					} catch (DatabaseException e) {}

					boolean hasRuntimeValues = false;
					if(iStatus.getRuntimeFieldValues() != null && iStatus.getRuntimeFieldValues().size() > 0) {
						hasRuntimeValues = true;
					}
					this.updateProgress(tdUid, iStatus.getResult(), iStatus.getMessage(), resultValidationStatus, hasRuntimeValues,"end");

				} else {
					this.updateProgress(tdUid, "E", "Could not get result.", "N/A", "end");
				}
			}
			
			
			String execStatus=helper.processExecStepStatus(execStatusList);
            execStatusMap.put(step.getRecordId(), execStatus);
            
            execStatusList.clear();
            
		}
		logger.info("All steps executed.");
		logger.info("Execution of Run ID [{}] Completed", this.runId);
		
		logger.info("Generating Defects");
		RunDefectHelper rHelper = new RunDefectHelper();
		RunDefectProcessor rProcessor = new RunDefectProcessor();
		try {
			List<String> uniqueMessages = rHelper.getDistinctTransactionFailureMessages(this.runId);
			
			Map<String, List<StepIterationResult>> transactionFailures = new HashMap<String, List<StepIterationResult>>();
			
			for(String message:uniqueMessages){
				List<StepIterationResult> txnsFailedWithMessage = rHelper.getTransactionsFailedWithMessage(this.runId, message);
				transactionFailures.put(message, txnsFailedWithMessage);
			}
			
			logger.info("Creating defects for Transaction Failures");
			rProcessor.createTransactionFailureDefects(this.runId,  this.projectId, this.userId, transactionFailures);
			
			logger.info("Getting validation failures");
			List<StepIterationResult> validationFailures = rHelper.getTransactionsWithValidationFailures(this.runId);
			if(validationFailures != null && validationFailures.size() > 0){
				rProcessor.createValidationFailureDefects(this.runId, this.projectId, this.userId, validationFailures);
			}
			
		} catch (DatabaseException e) {
			logger.error("ERROR while generating defects for run [{}]", this.runId,e);
		}
		
		logger.info("Updating Run End");
		try{
			helper.endRun(this.runId, BridgeProcess.COMPLETE);
		}catch(Exception ignore){}
		
		logger.info("Execution Task Completed");
		
	}
	
	
}
