/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApiAdapter.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 03-Apr-2017           Sriram Sridharan          Newly Added For TENJINCG-152 (API Testing Enablement)
22-Sep-2017			sameer gupta			For new adapter spec
18-05-2018          Padmavathi              for TENJINCG-647
18-11-2020			Ashiki					TENJINCG-1213
*/


package com.ycs.tenjin.bridge.oem.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.Oem;
import com.ycs.tenjin.bridge.oem.api.taskhandler.ApiExecutionTaskHandlerImpl;
import com.ycs.tenjin.bridge.oem.api.taskhandler.ApiLearningTaskHandlerImpl;
import com.ycs.tenjin.bridge.pojo.TaskManifest;
import com.ycs.tenjin.util.Utilities;

public class ApiOemImpl implements Oem {
	private TaskManifest manifest;

	public ApiOemImpl(TaskManifest manifest){
		this.manifest = manifest;
	}

	public ApiOemImpl(){

	}
	
	private static final Logger logger = LoggerFactory.getLogger(ApiOemImpl.class);

	@Override
	public void execute() throws BridgeException {
		
		try {
			String taskType = Utilities.trim(manifest.getTaskType());
			if(taskType.equalsIgnoreCase(BridgeProcess.APILEARN)){
				logger.info("Initiating Learning Task on target [{}]", manifest.getClientIp());
				ApiLearningTaskHandlerImpl taskHandler = new ApiLearningTaskHandlerImpl(this.manifest.getAut(), this.manifest.getApi(), this.manifest.getUserId(), this.manifest.getRunId(), this.manifest.getApiLearnType());
				taskHandler.execute();
				/*Added by Ashiki for TENJINCG-1213 starts*/
			}else if(taskType.equalsIgnoreCase(BridgeProcess.APICODELEARN)){

				logger.info("Initiating Learning Task on target [{}]", manifest.getClientIp());
				ApiLearningTaskHandlerImpl taskHandler = new ApiLearningTaskHandlerImpl(this.manifest.getAut(), this.manifest.getApis(), this.manifest.getUserId(), this.manifest.getRunId(), this.manifest.getApiLearnType(), this.manifest.getApiType());
				taskHandler.execute();
			/*Added by Ashiki for TENJINCG-1213 end*/
			}else if(taskType.equalsIgnoreCase(BridgeProcess.APIEXECUTE)){
				logger.info("Initiating Execution Task");
				
				ApiExecutionTaskHandlerImpl taskHandler = new  ApiExecutionTaskHandlerImpl(manifest.getRunId(), manifest.getAutMap(), manifest.getSteps(), manifest.getTdUidMap(), manifest.getTestDataPaths(), manifest.getProjectId(), manifest.getUserId(),manifest.getPassedStepsStatusMap());
				taskHandler.execute();
			}else if(taskType.equalsIgnoreCase(BridgeProcess.APIEXTRACT)){
				
			}else{
				throw new BridgeException("Unsupported Adapter Task");
			}
			
			
		} catch (Exception e) {
			
			logger.error("Exception while starting local adapter task", e);
			throw new BridgeException("An internal error occurred while starting the task. Please contact Tenjin support");
		}
	}

	@Override
	public void setManifest(TaskManifest manifest) {
		
		this.manifest = manifest;
	}
}
