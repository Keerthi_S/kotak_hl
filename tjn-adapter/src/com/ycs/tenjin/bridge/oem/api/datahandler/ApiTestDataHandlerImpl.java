/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SpreadsheetDataHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

  *//******************************************
  * CHANGE HISTORY
  * ==============
  *
  * DATE                 CHANGED BY              DESCRIPTION
  * 10-Sep-2016           Sriram Sridharan          Newly Added For 
  * 30-Jan-2017           Jyoti Ranjan             TENJINCG_84
   22-Sep-2017			sameer gupta			For new adapter spec
   17-10-2017           Padmavathi              for Tenjincg-349
   27-11-2017			Sriram Sridharan		For TENJINCG-514
   25-01-2019			Preeti					TJN252-80
   24-04-2019			Roshni					TENJINCG-1038
   18-12-2020           Paneendra				TENJINCG-1239

   */

package com.ycs.tenjin.bridge.oem.api.datahandler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.poifs.crypt.CipherAlgorithm;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.crypt.EncryptionMode;
import org.apache.poi.poifs.crypt.Encryptor;
import org.apache.poi.poifs.crypt.HashAlgorithm;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.exceptions.TestDataException;
import com.ycs.tenjin.bridge.oem.gui.datahandler.GuiDataHandlerImpl;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.LearningHelper;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.CryptoUtilities;
import com.ycs.tenjin.util.Utilities;

public class ApiTestDataHandlerImpl implements ApiTestDataHandler{

	public static final Logger logger = LoggerFactory.getLogger(ApiTestDataHandlerImpl.class);

	private Workbook workbook;
	private static final int TDGIDCOL=0;
	private static final int TDUIDCOL=1;
	private static final int TDDRIDCOL=2;
	private static final int DATA_START_COL=3;

	private static final int HEADER_ROW_NUM=4;
	private static final int LEGEND_ROW_NUM=2;
	private static final int SHEET_NAME_ROW_NUM=0;
	private static final int SHEET_NAME_CELL_NUM=1;

	private static final short MRB_PAGE_SHEET_COLOR=IndexedColors.LIGHT_YELLOW.getIndex();
	private static final short QUERY_FIELD_COLOR = IndexedColors.LIGHT_ORANGE.getIndex();

	private Connection conn;


	public Workbook getWorkbook() {
		return workbook;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}
	private Map<String, Boolean> locMap;

	private HashMap<String, String> sheetNames;


	public void generateTemplate(int applicationId, String functionCode, String destinationFolderPath, String templateFileName, boolean mandatoryOnly, boolean editableOnly) throws TestDataException {


		LearningHelper helper = new LearningHelper();
		try {
			List<Location> locations = helper.hydrateMetadata(applicationId, functionCode, mandatoryOnly, editableOnly);

			this.generateTemplate(applicationId, functionCode, destinationFolderPath, templateFileName, locations);

		} catch (DatabaseException e) {

			throw new TestDataException(e.getMessage());
		} 



	}


	private void generateTemplate(int applicationId, String functionCode, String destinationFolderPath, String templateFileName, List<Location> locations) throws TestDataException{

		logger.debug("Creating new blank workbook");
		this.workbook = new XSSFWorkbook();

		logger.debug("Loading styles");
		CellStyle hStyle = getHeaderRowStyle(workbook);
		CellStyle mStyle = getMandatoryFieldStyle(workbook);
		CellStyle queryCellStyle = getQueryCellStyle(workbook);
		CellStyle addCellStyle = getAddCellStyle(workbook);
		CellStyle deleteCellStyle = getDeleteCellStyle(workbook);

		CellStyle verifyCellStyle = getVerifyCellStyle(workbook);
		boolean workbookHasSheets = false;

		for(Location location:locations){

			logger.debug("Processing location --> [{}]", location.getLocationName());
			if(location.getTestObjects() != null && location.getTestObjects().size() > 0){
				String sheetName= "";
				if(Utilities.trim(location.getLocationName()).length() > 30){
					logger.debug("Location [{}] has name of length more than 30 characters");
					sheetName = "Page_Area_" + location.getSequence();
				}else{
					sheetName = location.getLocationName();
				}

				XSSFSheet sheet = (XSSFSheet) this.workbook.createSheet(sheetName);
				workbookHasSheets = true;
				if(location.isMultiRecordBlock()){
					sheet.setTabColor(MRB_PAGE_SHEET_COLOR);
				}

				Row sheetNameRow = null;
				sheetNameRow = sheet.createRow(SHEET_NAME_ROW_NUM);
				sheetNameRow.createCell(0).setCellValue("Page Name:");
				sheetNameRow.getCell(0).setCellStyle(getPageNameCellStyle(workbook));
				sheetNameRow.createCell(1).setCellValue(location.getLocationName());
				sheetNameRow.getCell(1).setCellStyle(getPageNameMergedCellStyle(workbook));
				CellRangeAddress region = new CellRangeAddress(0,0,1,6);
				sheet.addMergedRegion(region);
				RegionUtil.setBorderBottom(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderTop(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderLeft(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderRight(CellStyle.BORDER_THICK, region, sheet, workbook);


				sheetNameRow.createCell(7).setCellValue("Do not change this value. It may result in errors during execution");
				sheetNameRow.getCell(7).setCellStyle(getPageNameSubscriptCellStyle(workbook));
				region = new CellRangeAddress(0,0,7,11);
				sheet.addMergedRegion(region);

				Row legendRow = null;
				Row headerRow = null;
				logger.debug("Writing legend");
				legendRow = sheet.createRow(LEGEND_ROW_NUM);
				legendRow.createCell(0).setCellValue("Legend");
				legendRow.createCell(1).setCellValue("INPUT");
				legendRow.createCell(2).setCellValue("QUERY");
				legendRow.createCell(3).setCellValue("ADD");
				legendRow.createCell(4).setCellValue("DELETE");
				legendRow.createCell(5).setCellValue("VERIFY");

				logger.debug("Applying styles for legend");
				legendRow.getCell(2).setCellStyle(queryCellStyle);
				legendRow.getCell(3).setCellStyle(addCellStyle);
				legendRow.getCell(4).setCellStyle(deleteCellStyle);
				legendRow.getCell(5).setCellStyle(verifyCellStyle);

				logger.debug("Writing header row");
				headerRow = sheet.createRow(HEADER_ROW_NUM);
				headerRow.createCell(0).setCellValue("TDGID");
				headerRow.getCell(0).setCellStyle(hStyle);
				headerRow.createCell(1).setCellValue("TDUID");
				headerRow.getCell(1).setCellStyle(hStyle);
				headerRow.createCell(2).setCellValue("TDDRID");
				headerRow.getCell(2).setCellStyle(hStyle);


				int currentCol = DATA_START_COL;

				logger.debug("Writing fields");
				List<String> buttons = new ArrayList<String>();
				for(TestObject field:location.getTestObjects()){
					if(field.getObjectClass().equalsIgnoreCase(FieldType.BUTTON.toString())){
						buttons.add(field.getName());
						continue;
					}

					if(field.getObjectClass().equalsIgnoreCase(FieldType.LINK.toString()) || 
							field.getObjectClass().equalsIgnoreCase(FieldType.TABLE.toString()) ||
							field.getObjectClass().equalsIgnoreCase(FieldType.TOOLBARITEM.toString())){
						logger.debug("Field [{}] -- Element Type [{}] will be skipped from template", field.getName(), field.getObjectClass());
						continue;
					}

					if(Utilities.trim(field.getMandatory()).equalsIgnoreCase("yes")){
						field.setName(field.getName().replace("<font color='red'><b>*</b></font>", ""));
						logger.debug("Adding field [{}]", field.getName());
						headerRow.createCell(currentCol).setCellValue(field.getName());
					}else{
						logger.debug("Adding field [{}]", field.getName());
						headerRow.createCell(currentCol).setCellValue(field.getName());
					}
					//Changed by Jyoti Ranjan TENJINCG-84 End /
					if(Utilities.trim(field.getMandatory()).equalsIgnoreCase("yes")){
						logger.debug("Applying mandatory style for field [{}]", field.getName());
						headerRow.getCell(currentCol).setCellStyle(mStyle);
					}else if (field.getObjectClass().equalsIgnoreCase(FieldType.IMAGE.toString())) {
						headerRow.getCell(currentCol).setCellStyle(hStyle);
					}else{
						logger.debug("Applying normal style for field [{}]", field.getName());
						headerRow.getCell(currentCol).setCellStyle(hStyle);
					}


					if(!Utilities.trim(field.getDefaultOptions()).equalsIgnoreCase("")){
						logger.debug("Generating List Cell for field [{}]", field.getName());
						this.generateListCell(field.getDefaultOptions(), headerRow.getRowNum()+1, currentCol, sheet);
					}
					currentCol++;
				}

				//Paint all buttons
				String allButtons = "";
				if(buttons.size() > 0){
					headerRow.createCell(currentCol).setCellValue("Button");
					headerRow.getCell(currentCol).setCellStyle(hStyle);
					int counter=1;
					for(String button:buttons){
						allButtons = allButtons + button;
						if(counter < buttons.size()){
							allButtons = allButtons + ",";
						}
					}
					this.generateListCell(allButtons, headerRow.getRowNum()+1, currentCol, sheet);
				}

			}else{
				logger.debug("No fields found in location. This will be skipped");
			}
		}

		if(workbookHasSheets){
			String endFilePath = "";
			if(!Utilities.trim(templateFileName).endsWith(".xlsx")){
				templateFileName = Utilities.trim(templateFileName) + ".xlsx";
			}

			endFilePath = destinationFolderPath + "\\" + templateFileName;
			logger.debug("Writing file to disk");
			try{
				OutputStream out = new FileOutputStream(endFilePath);
				this.workbook.write(out);
				logger.debug("File saved in path {}",endFilePath);
			}catch(Exception e){
				logger.error("ERROR while writing template to disk", e);
				throw new TestDataException("Could not generate Test data template due to an internal error. Please contact Tenjin support");
			}
		}

	}


	private void generateListCell(String options, int startRow, int startCol, XSSFSheet sheet){

		if(Utilities.trim(options).length() > 250){
			logger.warn("List cell not generated due to options exceeing maximum length");
			return;
		}

		DataValidation dataValidation = null;
		DataValidationConstraint constraint = null;
		DataValidationHelper validationHelper = null;
		validationHelper=new XSSFDataValidationHelper(sheet);
		CellRangeAddressList addressList = new  CellRangeAddressList(startRow,500,startCol,startCol);
		constraint =validationHelper.createExplicitListConstraint(options.split(";"));
		dataValidation = validationHelper.createValidation(constraint, addressList);
		dataValidation.setSuppressDropDownArrow(true);    
		sheet.addValidationData(dataValidation);
	}

	private static CellStyle getVerifyCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getPageNameCellStyle(Workbook wb){
		CellStyle style = null;
		style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);
		return style;
	}

	private static CellStyle getPageNameMergedCellStyle(Workbook wb){
		CellStyle style = null;
		style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);
		return style;
	}

	private static CellStyle getPageNameSubscriptCellStyle(Workbook wb){
		CellStyle style = null;
		style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		font.setFontHeightInPoints((short)8);
		style.setFont(font);
		return style;
	}

	private static CellStyle getQueryCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);

		//headerFont.setColor(IndexedColors.BLACK.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getAddCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.LAVENDER.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getDeleteCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.WHITE.getIndex());
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getHeaderRowStyle(Workbook wb){

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getMandatoryFieldStyle(Workbook wb){

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.DARK_RED.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}



	private static String getCellValue(Cell cell){
		if(cell != null){
			cell.setCellType(Cell.CELL_TYPE_STRING);
			return Utilities.trim(cell.getStringCellValue());
		}else{
			return "";
		}
	}

	private static String getCellValue(Row row, int colIndex){

		if(row == null){
			logger.error("ERROR in getting cell value - row is null");
			return "";
		}
		try{
			Cell cell = row.getCell(colIndex, Row.CREATE_NULL_AS_BLANK);
			cell.setCellType(Cell.CELL_TYPE_STRING);
			return Utilities.trim(cell.getStringCellValue());
		}catch(Exception e){
			logger.error("ERROR getting value of cell with index {} on row {}", colIndex, row.getRowNum(), e);
			return "";
		}
	}



	private String resolve(String dataval) throws TestDataException{


		String output = dataval;

		ArrayList<String> params = new ArrayList<String>();
		if(dataval != null && dataval.startsWith("<<") && dataval.endsWith(">>")){
			dataval = dataval.replace("<<","");
			dataval = dataval.replace(">>","");
			String[] split = dataval.split(";;");
			if(split.length == 1){
				params.add(split[0]);
			}else{
				params.add(split[0]);
				for(int i=1;i<split.length;i++){
					String s = split[i];
					params.add(s);
				}
			}
		} else{
			//Means no need to resolve anything. This is a direct input
			return dataval;
		}

		if(params != null && params.size() == 1){
			//Invalid usage. It means we have a TDUID, but no idea about which fiedl value we should take. So log error and throw exception
			logger.error("ERROR invalid usage. Data specified is {} but expecting a field name to fetch",dataval);
			throw new TestDataException("Invalid Syntax to get runtime output. Right syntax is <<TDUID(FIELD_IDENTIFIER)>>");
		}

		if(params != null && params.size() > 1){
			//Value needs to be resolved
			String tdUid = params.get(0);
			String fieldIdentifier = params.get(1);


			Connection projConn = null;
			try{
				projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				output = new LearningHelper().getRuntimeFieldOutput(projConn, tdUid, fieldIdentifier);
				logger.info("Output Avaliable is : " + output);
			}catch(DatabaseException e){
				throw new TestDataException(e.getMessage(),e);
			}catch(Exception e){
				logger.error("ERROR resolving field value {}", dataval,e);
				throw new TestDataException("Could not resolve value for " + dataval + " due to an internal error");
			}finally{
				try{
					projConn.close();
				}catch(Exception ignore){}
			}

			return output;
		}else{
			return dataval;
		}



	}
	private ArrayList<Row> getSecondLevelDetailRowsForMaster(XSSFSheet sheet,String tdgid, String tduid, String detailRecordId, int dataStartRow) throws Exception{
		ArrayList<Row> detailRows = new ArrayList<Row>();

		try{
			int cRow = dataStartRow;
			while(sheet.getRow(cRow) != null && sheet.getRow(cRow).getCell(0) != null && sheet.getRow(cRow).getCell(0).getStringCellValue() != null && !sheet.getRow(cRow).getCell(0).getStringCellValue().equalsIgnoreCase("")){
				if(sheet.getRow(cRow).getCell(0).getStringCellValue().equalsIgnoreCase(tdgid) && sheet.getRow(cRow).getCell(1) != null && sheet.getRow(cRow).getCell(1).getStringCellValue().equalsIgnoreCase(tduid) && 
						sheet.getRow(cRow).getCell(2) != null && sheet.getRow(cRow).getCell(2).getStringCellValue() != null && !sheet.getRow(cRow).getCell(2).getStringCellValue().equalsIgnoreCase("")){
					String dId = sheet.getRow(cRow).getCell(2).getStringCellValue();
					if(dId.contains(".")){
						String[] dsplit = dId.split("\\.");
						if(dId.toLowerCase().startsWith(detailRecordId) && dsplit.length > 1 && dsplit.length < 4){
							detailRows.add(sheet.getRow(cRow));
						}
					}
				}

				cRow++;
			}
		}catch(Exception e){
			logger.error("Could not fetch second level detail rows for TDGID {}, TDUID {}, Detail Record ID {}", tdgid, tduid, detailRecordId);
			logger.error(e.getMessage(),e);
			throw new TestDataException(e.getMessage(),e);
		}

		return detailRows;
	}
	private int getTotalFieldsOnSheet(Sheet lSheet, int headerRow, int startColumnIndex){
		try{
			int totalFields = 0;
			int curCol = startColumnIndex;
			while(lSheet.getRow(headerRow) != null && lSheet.getRow(headerRow).getCell(curCol) != null && lSheet.getRow(headerRow).getCell(curCol).getStringCellValue() != null && !lSheet.getRow(headerRow).getCell(curCol).getStringCellValue().equalsIgnoreCase("")){
				totalFields++;
				curCol++;
			}

			return totalFields;
		}catch(Exception e){
			logger.error("ERROR getting number of fields on sheet");
			logger.error(e.getMessage(),e);
			return 0;
		}
	}
	private ArrayList<Row> getDetailRowsForMaster(Sheet sheet,String tdgid, String tduid, int masterStart) throws Exception{
		ArrayList<Row> detailRows = new ArrayList<Row>();
		int dRow = masterStart;
		try{
			while(sheet.getRow(dRow) != null && sheet.getRow(dRow).getCell(0) != null && sheet.getRow(dRow).getCell(0).getStringCellValue() != null && !sheet.getRow(dRow).getCell(0).getStringCellValue().equalsIgnoreCase("")){
				if(sheet.getRow(dRow).getCell(0).getStringCellValue().equalsIgnoreCase(tdgid) && sheet.getRow(dRow).getCell(1) != null && sheet.getRow(dRow).getCell(1).getStringCellValue().equalsIgnoreCase(tduid)){
					detailRows.add(sheet.getRow(dRow));
				}
				dRow++;
			} 

		}catch(Exception e){
			logger.error("ERROR fetching detail rows for TDGID {}, TDUID {}", tdgid, tduid);
			logger.error(e.getMessage(),e);
			/*throw new TestDataException(e.getMessage(),e);*/
			throw new TestDataException(e.getMessage(),e);
		}
		return detailRows;
	}

	public JSONObject loadTestDataFromTemplate(String absoluteFilePath, int appId, String functionCode, String tdGid, String tdUid,int testCaseId,String testStepId, String apiOperation, String entityType) throws TestDataException {

		JSONObject data = null;

		try{
			logger.info("Loading workbook from path [{}]", absoluteFilePath);
			this.workbook = WorkbookFactory.create(new File(absoluteFilePath));
		}catch(Exception e){

			logger.error(e.getMessage());
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		}

		try{
			logger.info("Obtaining Database Connection");
			this.conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch(DatabaseException e) {
			logger.error(e.getMessage());
			throw new TestDataException("Could not obtain Database connection to load Test Data. Please contact Tenjin Support.");
		}

		LearningHelper helper = new LearningHelper();
		int numberOfSheets = workbook.getNumberOfSheets();
		logger.info("Loading Test Data");
		logger.info("Number of sheets --> [{}]", numberOfSheets);

		Location location = null;


		try {
			data=this.scanTestData(workbook,tdGid, functionCode, appId, tdUid, helper, location,testCaseId,testStepId, apiOperation, entityType);
		}catch(TestDataException e){
			throw e;
		}catch (JSONException e) {

			logger.error("JSONException caught", e);
			throw new TestDataException("Could not load test data due to an internal error. Please contact Tenjin Support.");
		} catch(Exception e){

			logger.error("Unknown Exception caught", e);
			throw new TestDataException("Could not load test data due to an internal error. Please contact Tenjin Support.");
		} finally {
			logger.info("Closing DB Connection after loading test data");
			try{
				this.conn.close();
			} catch(Exception ignore) {}
		}
		return data;
	}
	public JSONObject loadTestDataFromTemplateForXML(String absoluteFilePath, int appId, String functionCode, String tdGid, String tdUid, String apiOperation, String entityType,int responseType,String requestMediaType,int testCaseRecordId ,String testStepId) throws TestDataException {

		JSONObject data = null;
		JSONArray pageAreas=null;
		try{
			logger.info("Loading workbook from path [{}]", absoluteFilePath);
			this.workbook = WorkbookFactory.create(new File(absoluteFilePath));
		}catch(Exception e){

			logger.error(e.getMessage());
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		}

		try{
			logger.info("Obtaining Database Connection");
			this.conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch(DatabaseException e) {
			logger.error(e.getMessage());
			throw new TestDataException("Could not obtain Database connection to load Test Data. Please contact Tenjin Support.");
		}

		LearningHelper helper = new LearningHelper();
		int numberOfSheets = workbook.getNumberOfSheets();
		logger.info("Loading Test Data");
		logger.info("Number of sheets --> [{}]", numberOfSheets);

		Location location = null;


		try {
			if(requestMediaType.equalsIgnoreCase("application/json")) {
				data=this.scanTestDataForJSON(workbook, tdGid, functionCode, appId, tdUid, helper, apiOperation,entityType,responseType);
			} else {
				data=this.scanTestDataForXML(workbook,tdGid, functionCode, appId, tdUid, helper, location,testCaseRecordId,testStepId, apiOperation, entityType);
			}
		}catch(TestDataException e){
			throw e;
		}catch (JSONException e) {

			logger.error("JSONException caught", e);
			throw new TestDataException("Could not load test data due to an internal error. Please contact Tenjin Support.");
		} catch(Exception e){

			logger.error("Unknown Exception caught", e);
			throw new TestDataException("Could not load test data due to an internal error. Please contact Tenjin Support.");
		} finally {
			logger.info("Closing DB Connection after loading test data");
			try{
				this.conn.close();
			} catch(Exception ignore) {}
		}
		return data;
	}

	public JSONObject scanTestDataForJSON(Workbook workbook, String tdGid, String moduleCode, int appId, String tdUid, LearningHelper helper, String apiOperation, String entityType,int responseType) throws TestDataException{	
		//changes done by parveen, Leelaprasad for #405 ends
		Location mainLocation;
		try {
			//changes done by parveen, Leelaprasad for #405 starts
			mainLocation = helper.hydrateLocationHierarchy(appId, moduleCode, apiOperation, entityType,responseType);
			//changes done by parveen, Leelaprasad for #405 ends
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			throw new TestDataException("An internal error occurred while scanning Test data for this step. Please contact Tenjin Support.", e);
		} 

		if(mainLocation == null) {
			logger.warn("No Locations to scan for {}", entityType);
			//return new JSONArray();
		}

		//changes done by parveen, Leelaprasad for #405 starts
		this.loadSheetNames(workbook, entityType,responseType);
		//changes done by parveen, Leelaprasad for #405 ends
		JSONArray mainJson = this.recursivelyScanPageAreaForResponseData(workbook, mainLocation, tdGid, tdUid, "");
		JSONObject data = new JSONObject();
		try {
			data.put("TDUID", tdUid);
			data.put("PAGEAREAS", mainJson);
		} catch (JSONException e) {
		}

		return data;

	}


	private JSONObject scanTestData(Workbook workbook, String tdGid, String moduleCode, int appId, String tdUid, LearningHelper helper, Location location,int testCaseId,String testStepId, String apiOperation, String entityType) throws TestDataException, JSONException{

		String pagePrefix = "";
		if("request".equalsIgnoreCase(entityType)) {
			pagePrefix = "(REQ)_";
		} else if("response".equalsIgnoreCase(entityType)){
			pagePrefix = "(RESP_200)_";
		}

		JSONArray detailRecords = new JSONArray();
		JSONObject page = new JSONObject();
		this.locMap =  new HashMap<String, Boolean>();
		int headerRow = 4;
		int dataStartCol = 3;
		int numberOfSheets=workbook.getNumberOfSheets();
		for(int i=0;i<numberOfSheets;i++){
			Sheet sheet=null;
			String sheetName=null;
			try {
				sheet = workbook.getSheetAt(i);
				sheetName=sheet.getSheetName();
				if(Utilities.trim(sheet.getSheetName()).equalsIgnoreCase("PAGE_AREA_INDEX")){
					continue;
				}else if(!Utilities.trim(sheet.getSheetName()).toLowerCase().startsWith(pagePrefix.toLowerCase())){
					logger.info("Skipping page [{}] as entity type is [{}]", sheetName, entityType);
					continue;
				}


				sheetName = sheet.getSheetName();
				logger.info("Processing sheet {}", sheetName);

				Row sheetNameRow = null;
				Cell sheetNameCell = null;
				sheetNameRow = sheet.getRow(SHEET_NAME_ROW_NUM);
				if(sheetNameRow == null){
					logger.error("SHEET NAME ROW not found. Invalid template");
					throw new TestDataException("Invalid Test Data Template - Page Name row was not found. Please use the latest test data template and try again");
				}

				sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);

				String locationName = getCellValue(sheetNameCell);
				if(locationName.equalsIgnoreCase("")){
					logger.error("LOCATION NAME TAMPERED WITH - Location Name is blank");
					throw new TestDataException("Invalid Test Data Template - Could not find Page Name in sheet [" + sheetName + "]. Please use the latest test data template and try again." );
				}
				location = helper.hydrateLocation(moduleCode, appId, locationName, apiOperation, entityType);

				if(location == null){
					throw new TestDataException("Could not find information about Page " + locationName + ". Please ensure your test data template is valid.");
				}

				logger.debug("Getting detail records on page {}", locationName);

				/***********************************************
				 * Changes by Nagareddy for Manual Input field 
				 */
				Map<String, String> manualField=null;
				try {
					manualField = helper.getManualInput(appId,moduleCode,locationName,testCaseId,testStepId);
				} catch (DatabaseException e1) {


				}
				/***********************************************
				 *  Changes by Nagareddy for Manual Input field 
				 */

				Row masterRow = null;
				Map<String, Row> tcRowMap = new TreeMap<String, Row>(); //Map that contains the Row Number as the key, and the detail row for the TDUID as the value

				logger.debug("Getting all rows with TDGID [{}] and TDUID [{}] on sheet [{}]", tdGid, tdUid, sheet.getSheetName());
				Iterator<Row> rowIter = sheet.iterator();
				String aTdDrId = "";
				while(rowIter.hasNext()){
					Row row = rowIter.next();

					if(getCellValue(row, TDGIDCOL).equalsIgnoreCase(tdGid) && getCellValue(row, TDUIDCOL).equalsIgnoreCase(tdUid)){
						aTdDrId = getCellValue(row, TDDRIDCOL);
						if(aTdDrId.equalsIgnoreCase("1") || aTdDrId.equalsIgnoreCase("")){
							masterRow = row;
						}

						tcRowMap.put(aTdDrId, row);
					}
				}
				Location loc = null;
				loc=location;
				int tFields = this.getTotalFieldsOnSheet(sheet, headerRow, dataStartCol);
				int masterStart=0;
				int dRow=0;
				JSONArray detailedRecords=null;
				if(masterRow!=null)
				{
					masterStart = masterRow.getRowNum();
					dRow = masterStart;
				}				
				if(masterRow == null){
					logger.error("Could not identify Master Record row for TDGID {}, TDUID {}", tdGid, tdUid);
					continue;
				}
				else{
					detailedRecords=this.getTestDataMultiLevelRecordBlock(sheet, tdGid, tdUid, dRow, appId, moduleCode, loc, tFields,manualField, apiOperation, entityType);
					if(detailedRecords!=null && detailedRecords.length()>0){
						//page=location.toJson();
						JSONObject rec = detailedRecords.getJSONObject(0);
						page.put("DR_ID", rec.getString("DR_ID"));
						page.put("DETAILRECORDS", detailedRecords);
					}
					//}
				}
				if(detailedRecords!=null && detailedRecords.length()==0){
					//page=location.toJson();
					page.put("DETAILRECORDS", new JSONArray());
					detailRecords.put(page);
				}
				else{
					if(page!=null && page.length()>0){
						detailRecords.put(page);
					}
				}
			} catch (Exception e) {
				logger.error("ERROR in scanSheetForData for page {}", sheet.getSheetName(), e );
				throw new TestDataException("An error occurred while scanning page " + sheet.getSheetName() + " for test data. Please contact Tenjin Support.");
			}
		}

		JSONObject data = new JSONObject();
		data.put("TDUID", tdUid);
		data.put("PAGEAREAS", detailRecords);
		return data;
	}

	public JSONArray getTestDataMultiLevelRecordBlock(Sheet sheet,String tdGid,String tdUid,int dRow,int appId,String moduleCode,Location loc,int tFields,Map<String,String> manualField, String apiOperation, String entityType) throws Exception{
		this.locMap =  new HashMap<String, Boolean>();
		JSONArray detailRecords=new JSONArray();		
		ArrayList<Row> detailRows = this.getDetailRowsForMaster(sheet, tdGid, tdUid, dRow);

		for(Row detailRow:detailRows){
			JSONObject detail = new JSONObject();
			String dRecordId = detailRow.getCell(2).getStringCellValue();
			detail.put("DR_ID", dRecordId);
			JSONArray fs = new JSONArray();
			fs=this.MultiRecordSingleLevel(loc, sheet, tdUid, tdGid, dRow, detailRow, tFields, moduleCode, appId, apiOperation, entityType);
			detail.put("FIELDS", fs);
			/*detail.put("PAGEAREA",loc.toJson());*/
			//This is for Checking Level N records exists or not
			JSONArray detailRecord=this.MultiRecordMultiLevel(moduleCode, appId, loc, tdGid, tdUid, dRecordId, apiOperation, entityType);
			/*if(detailRecord!=null && detailRecord.length()>0){
				detail.put("DETAILRECORDS", detailRecord);	
			}*/
			if(detail!=null && detail.length()>0)
				detailRecords.put(detail);
		}

		return detailRecords;
	}
	public JSONArray MultiRecordSingleLevel(Location loc,Sheet sheet,String tdUid,String tdGid,int dRow,Row detailRow,int tFields,String moduleCode,int appId, String apiOperation, String entityType) throws Exception{
		int headerRow = 4;
		int dataStartCol = 3;
		String pName=loc.getLocationName();
		JSONArray fs=new JSONArray();		
		for(int i=dataStartCol;i<=dataStartCol+tFields;i++){
			Cell cell = detailRow.getCell(i);
			if(cell != null){

				cell.setCellType(Cell.CELL_TYPE_STRING);
				//if(cell.getStringCellValue() != null && !cell.getStringCellValue().equalsIgnoreCase("")){
				String fieldName="";
				try {
					fieldName = sheet.getRow(headerRow).getCell(i).getStringCellValue();
				} catch (Exception e1) {
					continue;
				}
				String fieldValue = cell.getStringCellValue();
				String val = "";
				try{
					val = this.resolve(fieldValue);
				}catch(Exception e){
					throw new TestDataException(e.getMessage(),e);
				}
				TestObject t=null;
				if(fieldName.equalsIgnoreCase("Button"))
				{
					t=new TestObject();
					t.setLabel("Button");
					t.setObjectClass("Button");
					t.setUniqueId(val);
					t.setIdentifiedBy("Button");
					t.setLovAvailable(null);
				}
				else{
					t = this.getTestObject(this.conn, moduleCode, pName, appId, fieldName, apiOperation, entityType);
					//t = new LearningHelper().hydrateTestObject(this.conn, moduleCode, pName, appId, fieldName, apiOperation, entityType);
					//t = this.getTestData(workbook, moduleCode,val, val, appId,apiOperation, entityType);
				}
				if(t.getLabel() == null){
					throw new TestDataException("Information for field " + fieldName + " was not found in " + pName + " page in " + moduleCode + ". Re-learn this function or use the latest Test Data template");
				}
				JSONObject field = new JSONObject();
				field.put("DATA", val);
				/*Modified by Preeti for TJN252-80 starts*/
				/*field.put("FLD_LABEL", fieldName);
					field.put("FLD_LABEL_2", t.getLabel());*/
				field.put("FLD_LABEL", t.getLabel());
				/*Modified by Preeti for TJN252-80 ends*/
				field.put("FLD_TYPE",t.getObjectClass());
				field.put("FLD_UID", t.getUniqueId());
				field.put("FLD_UID_TYPE", t.getIdentifiedBy());
				field.put("FLD_SEQ_NO", t.getSequence());
				field.put("FLD_PAGE_AREA", t.getLocation());
				field.put("FLD_PARENT_PATH", t.getParentPath());

				String hasLov = "";
				if(t.getLovAvailable() == null){
					hasLov = "N";
				}else{
					hasLov = t.getLovAvailable();
				}
				field.put("FLD_HAS_LOV", hasLov);
				field.put("FLD_UNAME",t.getName());
				field.put("FLD_TAB_ORDER",t.getTabOrder());

				if(!loc.isMultiRecordBlock()){
					short bgColor = cell.getCellStyle().getFillForegroundColor();
					if(bgColor == IndexedColors.GREEN.getIndex()){
						field.put("ACTION", "QUERY");
					}else if(bgColor == IndexedColors.YELLOW.getIndex()){
						field.put("ACTION", "VERIFY");
					}else{
						field.put("ACTION", "INPUT");
					}
				}else{
					Cell hCell = sheet.getRow(headerRow).getCell(i);
					Short headerBgColor = hCell.getCellStyle().getFillForegroundColor();
					Short bgColor = cell.getCellStyle().getFillForegroundColor();
					if(bgColor == IndexedColors.LAVENDER.getIndex()){
						field.put("ACTION","ADDROW");
					}else if(bgColor == IndexedColors.GREY_40_PERCENT.getIndex()){
						field.put("ACTION", "DELETEROW");
					}else if(bgColor == IndexedColors.YELLOW.getIndex()){
						field.put("ACTION", "VERIFY");
					}else{
						if(headerBgColor == IndexedColors.LIGHT_ORANGE.getIndex()){
							field.put("ACTION", "QUERY");
						}else{
							field.put("ACTION", "INPUT");
						}
					}
				}
				fs.put(field);
				//}
			}
		}
		return fs;
	}
	public JSONArray MultiRecordMultiLevel(String moduleCode,int appId,Location loc,String tdGid,String tdUid,String dRecordId, String apiOperation, String entityType) throws Exception{
		ArrayList<Location> vicinityMrbs = null;
		ArrayList<Location> childMrbs = null;

		JSONArray detailRecords=new JSONArray();

		try{
			childMrbs = new LearningHelper().hydrateChildMultiRecordBlocks(this.conn, moduleCode,appId,"",loc.getLocationName() , apiOperation, entityType);
		}catch(Exception e){
			logger.error("ERROR hydrating Child record blocks of {}", loc.getLocationName());
			logger.error(e.getMessage(),e);
		}
		//Finding the Children of ChildMrbs
		Multimap<String, ArrayList<Location>> childChildMrbLocations = ArrayListMultimap.create();
		for(int i=0;i<childMrbs.size();i++){
			ArrayList<Location> childChildMrbs = new LearningHelper().hydrateChildMultiRecordBlocks(this.conn, moduleCode,appId,"",childMrbs.get(i).getLocationName(), apiOperation, entityType);
			childChildMrbLocations.put(childMrbs.get(i).getLocationName(), childChildMrbs);
		}

		if(childMrbs!=null && childMrbs.size()>0){

			int numberOfSheets = 0;
			try{
				numberOfSheets = workbook.getNumberOfSheets();
			}catch(Exception e){
				logger.error("ERROR getting number of sheets");
				logger.error(e.getMessage(),e);
				throw new TestDataException(e.getMessage());
			}
			vicinityMrbs = new ArrayList<Location>();
			if(childMrbs != null && childMrbs.size() > 0){
				for(Location vMrb:childMrbs){
					vicinityMrbs.add(vMrb);
				}
			}
			JSONObject page=new JSONObject(); 

			if(vicinityMrbs != null && vicinityMrbs.size() > 0){
				for(Location mrb:vicinityMrbs){
					JSONArray eachrec=this.MultiLevelLevelMultiRecordBlocks(mrb, numberOfSheets, tdGid, tdUid, dRecordId, loc, moduleCode, appId, apiOperation, entityType);
					if(eachrec!=null && eachrec.length()>0){
						page=mrb.toJson();
						JSONObject rec = eachrec.getJSONObject(0);
						page.put("DR_ID", rec.getString("DR_ID"));
						page.put("FIELDS", rec.getJSONArray("FIELDS"));
					}	
					// Here we need to implement Multi Level Level Records Fetch 
					for(Entry<String, ArrayList<Location>> e : childChildMrbLocations.entries()) {
						String keyVal=e.getKey();
						ArrayList<Location> locations=e.getValue();
						if(mrb.getLocationName().equalsIgnoreCase(keyVal)){
							for(Location location:locations){
								JSONArray detailed=this.MultiLevelLevelMultiRecordBlocks(location,numberOfSheets,tdGid,tdUid,dRecordId,loc,moduleCode,appId, apiOperation, entityType);
								if(detailed!=null && detailed.length()>0){
									/*page=location.toJson();*/
									if(eachrec!=null && eachrec.length()>0){
										page.put("DETAILRECORDS", detailed);
									}
								}
							}
							break;
						}
					}
					if(page!=null && page.length()>0){
						detailRecords.put(page);
					}
				}
			}	
		}

		return detailRecords;
	}

	public JSONArray MultiLevelLevelMultiRecordBlocks(Location vicinityMrbs,int numberOfSheets,String tdGid,String tdUid,String dRecordId,Location loc,String moduleCode,int appId, String apiOperation, String entityType) throws Exception{

		String pagePrefix = "";
		if("request".equalsIgnoreCase(entityType)) {
			pagePrefix = "(REQ)_";
		} else if("response".equalsIgnoreCase(entityType)) {
			pagePrefix = "(RESP)_";
		}

		int headerRow = 4;
		int dataStartRow = 5;
		int dataStartCol = 3;
		ArrayList<Row> sDetailRows=null;	
		JSONArray sDetails = new JSONArray();
		Map<String, Boolean> mrbLocMap = new HashMap<String, Boolean>();
		if(vicinityMrbs!=null){
			Location mrb=vicinityMrbs;
			logger.debug("Scanning Multi Record Block {} for more detail records for TDGID {}, TDUID {}, detail ID {}", mrb.getLocationName(), tdGid, tdUid, dRecordId);
			if(mrb.getLocationName().equalsIgnoreCase("Clearing Details")){
				System.err.println("Breakpoint");
			}
			XSSFSheet mrbSheet = null;
			String sName = "";
			for(int i=0;i<numberOfSheets;i++){
				try{
					XSSFSheet oSheet = (XSSFSheet) workbook.getSheetAt(i);
					Row sheetNameRow = oSheet.getRow(SHEET_NAME_ROW_NUM);
					if(sheetNameRow == null){
						logger.error("SHEET NAME ROW not found. Invalid template");
						throw new TestDataException("Invalid Test Data Template - Page Name row was not found. Please use the latest test data template and try again");
					}

					Cell sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);

					sName = getCellValue(sheetNameCell);
					//sName = oSheet.getSheetName();
					if(Utilities.trim(sName).equalsIgnoreCase(mrb.getLocationName())){
						boolean sheetVisited = false;
						try{
							sheetVisited = mrbLocMap.get(sName);
						}catch(Exception e){

						}
						if(sheetVisited){
							logger.debug("This sheet has already been visited. Skipping");
							continue;
						}else{
							mrbSheet = oSheet;
							logger.info("Found sheet for {}", mrb.getLocationName());
							break;
						}

					}
				}catch(Exception e){
					logger.error("ERROR in finding sheet for {}", mrb.getLocationName());
					logger.error(e.getMessage(),e);
				}
			}

			if(!sName.toLowerCase().startsWith(pagePrefix.toLowerCase())) {
				logger.info("Skipping sheet [{}] as entity type is [{}]", sName, entityType);
				mrbSheet = null;
			}

			if(mrbSheet != null){
				sDetailRows = this.getSecondLevelDetailRowsForMaster(mrbSheet, tdGid, tdUid, dRecordId , dataStartRow);

				if(sDetailRows != null && sDetailRows.size() > 0){
					int totalFields = this.getTotalFieldsOnSheet(mrbSheet, headerRow, dataStartCol);

					for(Row sDetailRow:sDetailRows){
						JSONObject sDetail = new JSONObject();
						String drid = sDetailRow.getCell(2).getStringCellValue();
						sDetail.put("DR_ID", drid);
						JSONArray dfs = new JSONArray();
						for(int f=dataStartCol;f<=dataStartCol+totalFields;f++){
							Cell cell = sDetailRow.getCell(f);
							if(cell != null && cell.getStringCellValue() != null && !cell.getStringCellValue().equalsIgnoreCase("")){
								String fieldName = mrbSheet.getRow(headerRow).getCell(f).getStringCellValue();
								String fieldValue = cell.getStringCellValue();
								String val = "";
								try{
									val = this.resolve(fieldValue);
								}catch(Exception e){
									throw new TestDataException(e.getMessage(),e);
								}
								TestObject t=null;
								if(fieldName.equalsIgnoreCase("Button"))
								{
									t=new TestObject();
									t.setLabel("Button");
									t.setObjectClass("Button");
									t.setUniqueId(val);
									t.setIdentifiedBy("Button");
									t.setLocation(loc.getLocationName());
									t.setViewmode("N");
									t.setMandatory("NO");
									t.setLovAvailable("N");
									t.setSequence(loc.getSequence());

								}
								else{
									t = new LearningHelper().hydrateTestObject(this.conn, moduleCode, sName, appId, fieldName, apiOperation, entityType);
								}
								if(t.getLabel() == null){
									throw new TestDataException("Information for field " + fieldName + " was not found in " + sName + " page in " + moduleCode + ". Re-learn this function or use the latest Test Data template");
								}
								JSONObject field = new JSONObject();
								field.put("DATA", val);
								field.put("FLD_LABEL", t.getLabel());
								field.put("FLD_TYPE",t.getObjectClass());
								field.put("FLD_UID", t.getUniqueId());
								field.put("FLD_UID_TYPE", t.getIdentifiedBy());
								field.put("FLD_SEQ_NO", t.getSequence());
								String hasLov = "";
								if(t.getLovAvailable() == null){
									hasLov = "N";
								}else{
									hasLov = t.getLovAvailable();
								}
								field.put("FLD_HAS_LOV", hasLov);
								if(!loc.isMultiRecordBlock()){
									short bgColor = cell.getCellStyle().getFillForegroundColor();
									if(bgColor == IndexedColors.GREEN.getIndex()){
										field.put("ACTION", "QUERY");
									}else if(bgColor == IndexedColors.YELLOW.getIndex()){
										field.put("ACTION", "VERIFY");
									}else{
										field.put("ACTION", "INPUT");
									}
								}else{
									Cell hCell = mrbSheet.getRow(headerRow).getCell(f);
									Short headerBgColor = hCell.getCellStyle().getFillForegroundColor();
									Short bgColor = cell.getCellStyle().getFillForegroundColor();
									if(bgColor == IndexedColors.LAVENDER.getIndex()){
										field.put("ACTION","ADDROW");
									}else if(bgColor == IndexedColors.GREY_40_PERCENT.getIndex()){
										field.put("ACTION", "DELETEROW");
									}else if(bgColor == IndexedColors.YELLOW.getIndex()){
										field.put("ACTION", "VERIFY");
									}else{
										if(headerBgColor == IndexedColors.LIGHT_ORANGE.getIndex()){
											field.put("ACTION", "QUERY");
										}else{
											field.put("ACTION", "INPUT");
										}
									}
								}
								/*JSONObject jObj=new JSONObject(field.toString());
								JSONParser parser=new JSONParser();
								Object j=parser.parse(jObj.toString());
								dfs.put(j);*/
								dfs.put(field);
							}
						}


						sDetail.put("FIELDS", dfs);
						if(dfs != null && dfs.length() > 0){
							sDetails.put(sDetail);	
							this.locMap.put(sName, true);
							mrbLocMap.put(sName, true);
						}

					}
					if(sDetails!=null && sDetails.length()>0){
					}
				}

			}
		}
		return sDetails;
	}




	/* Added by Sriram for TENJINCG-152 (API Testing Enablement)*/
	@Override
	public void generateTemplate(int applicationId, String apiCode, String destinationFolderPath, String templateFileName, boolean mandatoryOnly) throws TestDataException {


	}

	@Override
	public void generateTemplate(int applicationId, String apiCode, String operation, String destinationFolderPath, String templateFileName, boolean mandatoryOnly) throws TestDataException {

		LearningHelper helper = new LearningHelper();
		try {


			logger.info("Hydrating Request metadata for operation [{}]", operation);
			List<Location> reqLocations = helper.hydrateMetadata(applicationId, apiCode, operation, "request", mandatoryOnly);

			logger.info("Creating a new workbook");
			XSSFWorkbook workbook1;
			try {
				workbook1	=new XSSFWorkbook(this.getClass().getClassLoader().getResourceAsStream("TTD_Template.xlsx"));
				logger.info("workbook created");
			} catch (IOException e1) {
				logger.error(" Help WorkSheet does not exist");
				workbook1 = new XSSFWorkbook();
			}
			catch (NullPointerException e1) {
				logger.error(" Help WorkSheet does not exist");
				workbook1 = new XSSFWorkbook();
			}
			this.workbook = workbook1;

			GuiDataHandlerImpl objSpreadsheetDataHandler = new GuiDataHandlerImpl();
			boolean requestHasSheets = objSpreadsheetDataHandler.generateTemplate(this.workbook, applicationId, apiCode, destinationFolderPath, templateFileName, reqLocations, "REQ");


			/*	Added By Paneendra for TENJINCG-1243 starts */
			logger.info("Hydrating Header metadata for operation [{}]", operation);
			List<Location> headerLocations = helper.hydrateMetadata(applicationId, apiCode, operation, "header", mandatoryOnly);
			boolean headerHasSheets = objSpreadsheetDataHandler.generateTemplate(this.workbook, applicationId, apiCode, destinationFolderPath, templateFileName, headerLocations, "REQ");
			/*	Added By Paneendra for TENJINCG-1243 ends */


			logger.info("Hydrating Response metadata for operation [{}]", operation);
			List<Location> resLocations = helper.hydrateMetadata(applicationId, apiCode, operation, "response", mandatoryOnly);
			boolean responseHasSheets = objSpreadsheetDataHandler.generateTemplate(this.workbook, applicationId, apiCode, destinationFolderPath, templateFileName, resLocations, "RESP");



			if(requestHasSheets || responseHasSheets || headerHasSheets) {
				String endFilePath = "";
				if(!Utilities.trim(templateFileName).endsWith(".xlsx")){
					templateFileName = Utilities.trim(templateFileName) + ".xlsx";
				}

				endFilePath = destinationFolderPath + "/" + templateFileName;
				logger.debug("Writing file to disk");
				try{
					OutputStream out = new FileOutputStream(endFilePath);
					this.workbook.write(out);
					/*Added by Roshni for TENJINCG-1038 starts*/
					String pwd=new LearningHelper().hydrateTemplatePassword(applicationId);
					if(pwd!=null && !pwd.equalsIgnoreCase("")){
						POIFSFileSystem fs = new POIFSFileSystem();
						EncryptionInfo info = new EncryptionInfo(fs, EncryptionMode.agile, CipherAlgorithm.aes192, HashAlgorithm.sha256, -1, -1, null);
						Encryptor enc = info.getEncryptor();

						/*Modified by paneendra for VAPT FIX starts*/
						pwd=new CryptoUtilities().decrypt(pwd);
						/*Modified by paneendra for VAPT FIX ends*/
						enc.confirmPassword(pwd);
						OPCPackage opc = OPCPackage.open(new File(endFilePath), PackageAccess.READ_WRITE);
						OutputStream os = enc.getDataStream(fs);
						opc.save(os);
						opc.close();

						out = new FileOutputStream(endFilePath);
						fs.writeFilesystem(out);
					}
					/*Added by Roshni for TENJINCG-1038 ends*/

					logger.debug("File saved in path {}",endFilePath);
				}catch(Exception e){
					logger.error("ERROR while writing template to disk", e);
					throw new TestDataException("Could not generate Test data template due to an internal error. Please contact Tenjin support");
				}
			}

		} catch (DatabaseException e) {

			throw new TestDataException(e.getMessage());
		} 
	}
	/* Added by Sriram for TENJINCG-152 (API Testing Enablement) ends*/

	//TENJINCG-514
	public JSONObject loadTestDataFromTemplate(String absoluteFilePath, int appId, String functionCode, String tdGid, String tdUid, String apiOperation, String entityType,int responseType) throws TestDataException {	
		//changes done by parveen, Leelaprasad for #405 ends
		JSONObject data = null;

		try{
			logger.info("Loading workbook from path [{}]", absoluteFilePath);
			this.workbook = WorkbookFactory.create(new File(absoluteFilePath));
		}catch(Exception e){

			logger.error(e.getMessage());
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		}


		LearningHelper helper = new LearningHelper();
		int numberOfSheets = workbook.getNumberOfSheets();
		logger.info("Loading Test Data");
		logger.info("Number of sheets --> [{}]", numberOfSheets);

		//changes done by parveen, Leelaprasad for #405 starts
		JSONArray pageAreas = this.scanTestData(workbook, tdGid, functionCode, appId, tdUid, helper, apiOperation, entityType,responseType);
		//changes done by parveen, Leelaprasad for #405 ends

		try {
			data = new JSONObject();
			data.put("PAGEAREAS", pageAreas);
			data.put("TDUID", tdUid);
			data.put("RESOURCE_PARAMS", this.loadResourceParametersForRequest(workbook, tdGid, tdUid));
		} catch (JSONException e) {
			logger.error("ERROR while generating final JSONObject", e);
		}

		return data;
	}

	public JSONArray scanTestData(Workbook workbook, String tdGid, String moduleCode, int appId, String tdUid, LearningHelper helper, String apiOperation, String entityType,int responseType) throws TestDataException{	
		//changes done by parveen, Leelaprasad for #405 ends
		Location mainLocation;
		try {
			//changes done by parveen, Leelaprasad for #405 starts
			mainLocation = helper.hydrateLocationHierarchy(appId, moduleCode, apiOperation, entityType,responseType);
			//changes done by parveen, Leelaprasad for #405 ends
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			throw new TestDataException("An internal error occurred while scanning Test data for this step. Please contact Tenjin Support.", e);
		} 

		if(mainLocation == null) {
			logger.warn("No Locations to scan for {}", entityType);
			return new JSONArray();
		}

		//changes done by parveen, Leelaprasad for #405 starts
		this.loadSheetNames(workbook, entityType,responseType);
		//changes done by parveen, Leelaprasad for #405 ends
		JSONArray mainJson = this.recursivelyScanPageAreaForResponseData(workbook, mainLocation, tdGid, tdUid, "");
		return mainJson;
	}

	private void loadSheetNames(Workbook workbook, String entityType,int responseType) {
		//changes done by parveen, Leelaprasad for #405 ends
		this.sheetNames = new HashMap<String, String> ();
		String pagePrefix = "";
		if("request".equalsIgnoreCase(entityType)){
			pagePrefix = "(REQ)_";
		} else if("response".equalsIgnoreCase(entityType)){
			//changes done by parveen, Leelaprasad for #405 starts
			pagePrefix = "(RESP_"+responseType+")_";
			//changes done by parveen, Leelaprasad for #405 ends
		}/*	Added By Paneendra for TENJINCG-1239 starts */
		else {
			pagePrefix = "(REQ_)_HeaderRoot";
		}/*	Added By Paneendra for TENJINCG-1239 ends */
		for(int i=0; i<workbook.getNumberOfSheets(); i++) {
			Sheet sheet = workbook.getSheetAt(i);
			String sheetName=sheet.getSheetName();
			if(Utilities.trim(sheet.getSheetName()).equalsIgnoreCase("TTD_HELP")){
				continue;
			}else if(!Utilities.trim(sheet.getSheetName()).toLowerCase().startsWith(pagePrefix.toLowerCase())){
				logger.info("Skipping page [{}] as entity type is [{}]", sheetName, entityType);
				continue;
			}
			Row sheetNameRow = sheet.getRow(SHEET_NAME_ROW_NUM);
			if(sheetNameRow == null){
				logger.error("SHEET NAME ROW not found on sheet {}. Invalid template", sheet.getSheetName());
			}

			Cell sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);

			String locationName = getCellValue(sheetNameCell);
			logger.debug("Loading sheet Name {} to memory for page area {}", sheet.getSheetName(), locationName);
			this.sheetNames.put(locationName, sheet.getSheetName());
		}
	}

	private JSONArray loadResourceParametersForRequest(Workbook workbook, String tdGid, String tdUid) throws TestDataException {

		try {
			Sheet rParamSheet = workbook.getSheet("(REQ)_Resource Parameters");
			if(rParamSheet == null) {
				logger.info("No resource params found for this TDUID");
				return new JSONArray();
			}

			Map<String, Row> detailRecordRows = this.getRowsForTdUid(rParamSheet, tdGid, tdUid, "Resource Parameters", false, "");


			if(detailRecordRows.size() < 1) {
				logger.debug("No detail records found on page [{}]: Sheet [{}]", "Resource Paramters", rParamSheet);
				return new JSONArray();
			}

			Row headerRow = rParamSheet.getRow(HEADER_ROW_NUM);
			if(headerRow == null) {
				logger.error("ERROR - No header row found on sheet {}", rParamSheet.getSheetName());
				throw new TestDataException("No Header Row was found on sheet " + rParamSheet.getSheetName() + ". Please verify if you are using the correct Test Data Template.");
			}

			int dataStartCol = 3;
			JSONArray detailRecordsArray = new JSONArray();
			for(String tdDrId : detailRecordRows.keySet()) {
				Row detailRecordRow = detailRecordRows.get(tdDrId);
				JSONObject detailRecordJson = new JSONObject();
				JSONArray fieldsJsonArray = new JSONArray();
				Cell detailRecordIdCell = detailRecordRow.getCell(TDDRIDCOL, Row.CREATE_NULL_AS_BLANK);
				String aTdDrId = getCellValue(detailRecordIdCell);
				if(Utilities.trim(aTdDrId).length() < 1 ) {
					aTdDrId = "1";
				}
				Iterator<Cell> headerRowIter = headerRow.cellIterator();
				while(headerRowIter.hasNext()) {
					Cell headerCell = headerRowIter.next();
					if(headerCell.getColumnIndex() < dataStartCol) {
						continue;
					}

					String fieldName = getCellValue(headerCell);
					Cell valueCell = detailRecordRow.getCell(headerCell.getColumnIndex(), Row.CREATE_NULL_AS_BLANK);
					String fieldValue = getCellValue(valueCell);

					if(Utilities.trim(fieldValue).length() < 1){
						continue;
					}

					fieldValue = this.resolve(fieldValue);

					String action = "INPUT";



					JSONObject fieldJson = new JSONObject();
					/*Modified by Preeti for TJN252-80 starts*/
					fieldJson.put("FLD_LABEL", fieldName);
					/*Modified by Preeti for TJN252-80 ends*/
					fieldJson.put("DATA", fieldValue);
					fieldJson.put("ACTION", action);
					fieldsJsonArray.put(fieldJson);

					logger.info("Page: [{}], Field: [{}], Data: [{}], Action: [{}]","Resource Parameters", fieldName, fieldValue, action);
				}

				detailRecordJson.put("DR_ID", aTdDrId);
				detailRecordJson.put("FIELDS", fieldsJsonArray);


				detailRecordsArray.put(detailRecordJson);


			}
			return detailRecordsArray;

		}catch(Exception e) {
			logger.error("ERROR occurred while scanning for resource parameters", e);
			throw new TestDataException("Could not process Resource parameters from test data due to an internal error. Please contact Tenjin Support.");
		}
	}

	private Sheet getSheetByLocationName(Workbook workbook, String locationName) {
		if(this.sheetNames == null) {
			logger.error("ERROR - sheetNames map is not loaded.");
			return null;
		}
		logger.debug("Locating sheet for Page Area {}", locationName);
		String targetSheetName = this.sheetNames.get(locationName);
		if(Utilities.trim(targetSheetName).length() < 1) {
			logger.error("ERROR - No matching sheet found for page area {}", locationName);
			return null;
		}else{
			Sheet sheet = workbook.getSheet(targetSheetName);
			if(sheet == null) {
				logger.error("ERROR - No matching sheet found for page area {}", locationName);
				return null;
			}

			logger.debug("Target Sheet [{}] found for Page Area {}", sheet.getSheetName(), locationName);
			return sheet;
		}
	}

	private Map<String, TestObject> loadTestObjectMap(List<TestObject> testObjects) {
		Map<String, TestObject> map = new TreeMap<String, TestObject>();
		if(testObjects != null && testObjects.size() > 0) {
			for(TestObject t : testObjects) {
				map.put(t.getName(), t);
			}
		}

		return map;
	}

	private Map<String, Row> getRowsForTdUid(Sheet sheet, String tdGid, String tdUid, String pageAreaName, boolean isMrb, String parentDetailId) throws TestDataException {
		logger.debug("Getting all rows with TDGID [{}] and TDUID [{}] on sheet [{}]", tdGid, tdUid, sheet.getSheetName());
		if(pageAreaName.equalsIgnoreCase("balances") || pageAreaName.equalsIgnoreCase("data")) {
			System.err.println();
		}
		String sParentDetailId = Utilities.trim(parentDetailId).length()< 1 ? "1" : Utilities.trim(parentDetailId); //Temp variable to treat blank TDDRID ("") as "1" in case of Single Record Blocks
		try {
			Map<String, Row> tcRowMap = new TreeMap<String, Row>(); //Map that contains the Row Number as the key, and the detail row for the TDUID as the value
			Iterator<Row> rowIter = sheet.iterator();
			String aTdDrId = "";
			while(rowIter.hasNext()){
				Row row = rowIter.next();

				if(getCellValue(row, TDGIDCOL).equalsIgnoreCase(tdGid) && getCellValue(row, TDUIDCOL).equalsIgnoreCase(tdUid)){
					String detailIdForThisRow = getCellValue(row, TDDRIDCOL);
					detailIdForThisRow = Utilities.trim(detailIdForThisRow).length() < 1 ? "1" : Utilities.trim(detailIdForThisRow);
					if(!isMrb && detailIdForThisRow.equalsIgnoreCase(sParentDetailId)) {
						aTdDrId = getCellValue(row, TDDRIDCOL);
						tcRowMap.put(aTdDrId, row);
					}else if(isMrb) {
						if(Utilities.trim(parentDetailId).length() < 1 || (Utilities.trim(parentDetailId).length()>0 && detailIdForThisRow.startsWith(parentDetailId + "."))){
							aTdDrId = getCellValue(row, TDDRIDCOL);
							tcRowMap.put(aTdDrId, row);
						}
					}
				}
			}

			return tcRowMap;
		} catch (Exception e) {
			logger.error("ERROR occurred while scanning for Master Records for page area [{}]", pageAreaName, e);
			throw new TestDataException("An unexpected error occurred while scanning Test Data sheet for Page [" + pageAreaName + "]. Please contact Tenjin Support.");
		}
	}

	private JSONArray recursivelyScanPageAreaForResponseData(Workbook workbook, Location location, String tdGid, String tdUid, String parentDetailId) throws TestDataException {
		try {
			JSONArray pageJsonArray = new JSONArray();
			JSONObject pageJson = new JSONObject();
			int dataStartCol = 3;

			pageJson = location.toJson();

			Sheet sheet = this.getSheetByLocationName(workbook, location.getLocationName());

			if(sheet == null) {
				logger.warn("Sheet for Page Area [{}] was not found. Scanning if children of this page have any data", location.getLocationName());
				if(location.getChildLocations() != null) {
					for(Location childLocation : location.getChildLocations()) {
						JSONArray pArray = this.recursivelyScanPageAreaForResponseData(workbook, childLocation, tdGid, tdUid, parentDetailId);
						if(pArray != null && pArray.length() > 0) {
							for(int i=0; i<pArray.length(); i++) {
								pageJsonArray.put(pArray.getJSONObject(i));
							}
						}
					}
				}

				return pageJsonArray;
			}


			Map<String, TestObject> testObjectMap = this.loadTestObjectMap(location.getTestObjects());
			if(sheet != null) {
				Map<String, Row> detailRecordRows = this.getRowsForTdUid(sheet, tdGid, tdUid, location.getLocationName(), location.isMultiRecordBlock(), parentDetailId);

				if(detailRecordRows.size() < 1) {
					logger.debug("No detail records found on page [{}]: Sheet [{}]", location.getLocationName(), sheet.getSheetName());
					return new JSONArray();
				}

				Row headerRow = sheet.getRow(HEADER_ROW_NUM);
				if(headerRow == null) {
					logger.error("ERROR - No header row found on sheet {}", sheet.getSheetName());
					throw new TestDataException("No Header Row was found on sheet " + sheet.getSheetName() + ". Please verify if you are using the correct Test Data Template.");
				}

				JSONArray detailRecordsArray = new JSONArray();
				for(String tdDrId : detailRecordRows.keySet()) {
					Row detailRecordRow = detailRecordRows.get(tdDrId);
					JSONObject detailRecordJson = new JSONObject();
					JSONArray fieldsJsonArray = new JSONArray();
					Cell detailRecordIdCell = detailRecordRow.getCell(TDDRIDCOL, Row.CREATE_NULL_AS_BLANK);
					String aTdDrId = getCellValue(detailRecordIdCell);
					if(Utilities.trim(aTdDrId).length() < 1 ) {
						aTdDrId = "1";
					}
					Iterator<Cell> headerRowIter = headerRow.cellIterator();
					while(headerRowIter.hasNext()) {
						Cell headerCell = headerRowIter.next();
						if(headerCell.getColumnIndex() < dataStartCol) {
							continue;
						}

						String fieldName = getCellValue(headerCell);
						Cell valueCell = detailRecordRow.getCell(headerCell.getColumnIndex(), Row.CREATE_NULL_AS_BLANK);
						String fieldValue = getCellValue(valueCell);

						if(Utilities.trim(fieldValue).length() < 1){
							continue;
						}

						fieldValue = this.resolve(fieldValue);

						short bgColor = valueCell.getCellStyle().getFillBackgroundColor();
						String action = "VERIFY";
						if(bgColor == QUERY_FIELD_COLOR) {
							action = "QUERY";
						}

						TestObject t = testObjectMap.get(fieldName);
						if(t == null) {
							logger.error("ERROR - No matching test object found for page [{}], field [{}]", location.getLocationName(), fieldName);
							throw new TestDataException("Metadata does not exist for Field [" + fieldName + "] on page [" + location.getLocationName() + "]. You might need to re-learn this API / Function and use a more recent Test Data Template.");
						}

						JSONObject fieldJson = t.toJson();

						fieldJson.put("FLD_LABEL", t.getLabel());
						fieldJson.put("DATA", fieldValue);
						fieldJson.put("ACTION", action);
						fieldsJsonArray.put(fieldJson);

						logger.info("Page: [{}], Field: [{}], Data: [{}], Action: [{}]", location.getLocationName(), fieldName, fieldValue, action);
					}

					detailRecordJson.put("DR_ID", aTdDrId);
					detailRecordJson.put("FIELDS", fieldsJsonArray);
					JSONArray childLocationJsonArray = new JSONArray();
					if(location.getChildLocations() != null) {
						for(Location childLocation : location.getChildLocations()) {
							JSONArray cArray = this.recursivelyScanPageAreaForResponseData(workbook, childLocation, tdGid, tdUid, aTdDrId);
							if(cArray != null && cArray.length() > 0) {
								for(int i=0; i<cArray.length(); i++) {
									childLocationJsonArray.put(cArray.getJSONObject(i));
								}
							}
						}
					}

					detailRecordJson.put("CHILDREN", childLocationJsonArray);

					detailRecordsArray.put(detailRecordJson);
				}

				pageJson.put("DETAILRECORDS", detailRecordsArray);
				pageJsonArray.put(pageJson);
			}


			return pageJsonArray;
		} catch (JSONException e) {
			logger.error(e.getMessage(),e);
			throw new TestDataException("An internal error occurred while scanning Test Data for this step. Please contact Tenjin Support.");
		}

	}
	private JSONObject scanTestDataForXML(Workbook workbook, String tdGid, String moduleCode, int appId, String tdUid, LearningHelper helper, Location location,int testCaseRecordId,String testStepId, String apiOperation, String entityType) throws TestDataException, JSONException, DatabaseException, SQLException{

		
		/*Modified by paneendra for Tenj212-37 starts*/
		
		/*String pagePrefix = "";
		if("request".equalsIgnoreCase(entityType)) {
			pagePrefix = "(REQ)_";
		} else if("response".equalsIgnoreCase(entityType)){
			pagePrefix = "(RESP)_";
			pagePrefix = "(RESP_200)_";
		}*/


		JSONObject xmldetailRecords = new JSONObject();
		JSONObject page1 = new JSONObject();
		JSONArray detailxmlrecords= new JSONArray();
		xmldetailRecords=this.getTestData(workbook,tdGid,tdUid,moduleCode,appId,apiOperation, entityType);

		/*	JSONArray detailRecords = new JSONArray();
		JSONObject page = new JSONObject();
		this.locMap =  new HashMap<String, Boolean>();
		int headerRow = 4;
		int dataStartCol = 3;
		int numberOfSheets=workbook.getNumberOfSheets();
		for(int i=0;i<numberOfSheets;i++){
			Sheet sheet=null;
			String sheetName=null;
			try {
				sheet = workbook.getSheetAt(i);
				sheetName=sheet.getSheetName();
				if(Utilities.trim(sheet.getSheetName()).equalsIgnoreCase("PAGE_AREA_INDEX")){
					continue;
				}else if(!Utilities.trim(sheet.getSheetName()).toLowerCase().startsWith(pagePrefix.toLowerCase())){
					logger.info("Skipping page [{}] as entity type is [{}]", sheetName, entityType);
					continue;
				}


				sheetName = sheet.getSheetName();
				logger.info("Processing sheet {}", sheetName);

				Row sheetNameRow = null;
				Cell sheetNameCell = null;
				sheetNameRow = sheet.getRow(SHEET_NAME_ROW_NUM);
				if(sheetNameRow == null){
					logger.error("SHEET NAME ROW not found. Invalid template");
					throw new TestDataException("Invalid Test Data Template - Page Name row was not found. Please use the latest test data template and try again");
				}

				sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);

				String locationName = getCellValue(sheetNameCell);
				if(locationName.equalsIgnoreCase("")){
					logger.error("LOCATION NAME TAMPERED WITH - Location Name is blank");
					throw new TestDataException("Invalid Test Data Template - Could not find Page Name in sheet [" + sheetName + "]. Please use the latest test data template and try again." );
				}
				location = helper.hydrateLocation(moduleCode, appId, locationName, apiOperation, entityType);

				logger.error("Invalid Location Name --> [{}]. No details in DB", locationName);
				if(location == null){
					throw new TestDataException("Could not find information about Page " + locationName + ". Please ensure your test data template is valid.");
				}

				logger.debug("Getting detail records on page {}", locationName);

		 *//***********************************************
		 * Changes by Nagareddy for Manual Input field 
		 *//*
				Map<String, String> manualField=null;
				try {
					//String testCaseId=this.getTestCaseId(testCaseRecordId);
					manualField = helper.getManualInput(appId,moduleCode,locationName,testCaseRecordId,testStepId);
				} catch (DatabaseException e1) {
					// TODO Auto-generated catch block

				}
		  *//***********************************************
		  *  Changes by Nagareddy for Manual Input field 
		  *//*

				Row masterRow = null;
				Map<String, Row> tcRowMap = new TreeMap<String, Row>(); //Map that contains the Row Number as the key, and the detail row for the TDUID as the value

				logger.debug("Getting all rows with TDGID [{}] and TDUID [{}] on sheet [{}]", tdGid, tdUid, sheet.getSheetName());
				Iterator<Row> rowIter = sheet.iterator();
				String aTdDrId = "";
				while(rowIter.hasNext()){
					Row row = rowIter.next();

					if(getCellValue(row, TDGIDCOL).equalsIgnoreCase(tdGid) && getCellValue(row, TDUIDCOL).equalsIgnoreCase(tdUid)){
						aTdDrId = getCellValue(row, TDDRIDCOL);
						if(aTdDrId.equalsIgnoreCase("1") || aTdDrId.equalsIgnoreCase("")){
							masterRow = row;
						}

						tcRowMap.put(aTdDrId, row);
					}
				}
				Location loc = null;
				loc=location;
				int tFields = this.getTotalFieldsOnSheet(sheet, headerRow, dataStartCol);
				int masterStart=0;
				int dRow=0;
				JSONArray detailedRecords=null;
				if(masterRow!=null)
				{
					masterStart = masterRow.getRowNum();
					dRow = masterStart;
				}				
				if(masterRow == null){
					logger.error("Could not identify Master Record row for TDGID {}, TDUID {}", tdGid, tdUid);
					continue;
				}
				else{
					if(!location.isMultiRecordBlock()){
						detailedRecords=this.getTestDataForSingleRecordBlock(sheet, masterRow, appId, moduleCode, helper, location,manualField);
						if(detailedRecords!=null && detailedRecords.length()>0){
							page=location.toJson();
							page.put("DETAILRECORDS", detailedRecords);
						}
					}
					else{
					detailedRecords=this.getTestDataMultiLevelRecordBlock(sheet, tdGid, tdUid, dRow, appId, moduleCode, loc, tFields,manualField, apiOperation, entityType);
					if(detailedRecords!=null && detailedRecords.length()>0){
						page=location.toJson();
						JSONObject rec = detailedRecords.getJSONObject(0);
						page.put("DR_ID", rec.getString("DR_ID"));
						//page.put("FIELDS", rec.getJSONArray("FIELDS"));
						page.put("DETAILRECORDS", detailedRecords);
					}
					//}
				}
				if(detailedRecords!=null && detailedRecords.length()==0){
					page=location.toJson();
					page.put("DETAILRECORDS", new JSONArray());
					detailRecords.put(page);
				}
				else{
					if(page!=null && page.length()>0){
						detailRecords.put(page);
					}
				}
			} catch (Exception e) {
				logger.error("ERROR in scanSheetForData for page {}", sheet.getSheetName(), e );
				throw new TestDataException("An error occurred while scanning page " + sheet.getSheetName() + " for test data. Please contact Tenjin Support.");
			}
		}*/
		//page1.put("FIELDS", xmldetailRecords);
		page1.put("DETAILRECORDS", xmldetailRecords);

		detailxmlrecords.put(page1);
		/*modified by paneendra for Tenj212-37 ends*/
		JSONObject data = new JSONObject();
		data.put("TDUID", tdUid);
		//data.put("PAGEAREAS", detailRecords);
		data.put("PAGEAREAS", detailxmlrecords);
		return data;
	}

	
	/*modified by paneendra for Tenj212-37 starts*/
	public JSONObject getTestData(Workbook workbook, String tdGid, String tdUid,String moduleCode,int appId,String apiOperation, String entityType) throws DatabaseException, SQLException, JSONException{
		TestObject t= null;

		entityType = Utilities.trim(entityType).toUpperCase();
		Connection conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		if(conn == null){
			throw new DatabaseException("Invalid database connection");
		}
		PreparedStatement pst =null;
		PreparedStatement pst1 =null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		JSONArray fs=new JSONArray();
		JSONObject detail = new JSONObject();
		try{

			// pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE = ? AND FLD_UNAME = ? AND FLD_PAGE_AREA = ? AND FLD_API_OPERATION=? AND FLD_API_ENTITY_TYPE=?");
			pst = conn.prepareStatement("SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE = ?  AND FLD_API_OPERATION=? AND FLD_API_ENTITY_TYPE=? ORDER BY FLD_SEQ_NO");

			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, apiOperation);
			pst.setString(4, entityType);
			rs = pst.executeQuery();
			String parent="";

			while(rs.next()){

				t = new TestObject();
				t.setLabel(rs.getString("FLD_LABEL"));
				t.setName(rs.getString("FLD_UNAME"));
				t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
				t.setUniqueId(rs.getString("FLD_UID"));
				t.setObjectClass(rs.getString("FLD_TYPE"));
				parent=rs.getString("FLD_PAGE_AREA");
				t.setLocation(parent);
				t.setViewmode(rs.getString("VIEWMODE"));
				t.setMandatory(rs.getString("MANDATORY"));
				t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
				t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
				t.setGroup(rs.getString("FLD_GROUP"));
				t.setIsMultiRecord(rs.getString("FLD_IS_MULTI_REC"));
				t.setSequence(rs.getInt("FLD_SEQ_NO"));



				pst1 = conn.prepareStatement("SELECT PA_PATH FROM AUT_FUNC_PAGEAREAS WHERE PA_APP = ? AND PA_FUNC_CODE = ? AND PA_NAME=? AND PA_API_OPERATION=? AND PA_API_ENTITY_TYPE=? ");

				pst1.setInt(1, appId);
				pst1.setString(2, moduleCode);
				pst1.setString(3, parent);
				pst1.setString(4, apiOperation);
				pst1.setString(5, entityType);
				rs1 = pst1.executeQuery();

				while (rs1.next()) {

					t.setParentPath(rs1.getString("PA_PATH"));

				}
				JSONObject field = new JSONObject();
				//	field.put("DATA", val);
				/*Modified by Preeti for TJN252-80 starts*/
				/*field.put("FLD_LABEL", fieldName);
			field.put("FLD_LABEL_2", t.getLabel());*/
				field.put("FLD_LABEL", t.getLabel());
				/*Modified by Preeti for TJN252-80 ends*/
				field.put("FLD_TYPE",t.getObjectClass());
				field.put("FLD_UID", t.getUniqueId());
				field.put("FLD_UID_TYPE", t.getIdentifiedBy());
				field.put("FLD_PAGE_AREA", t.getLocation());
				field.put("FLD_PARENT_PATH", t.getParentPath());

				//detail.put("FIELDS", field);
				String data=this.scanPageAreaData(workbook,tdGid,tdUid,parent,t.getLabel(),entityType);
				field.put("DATA",data);
				fs.put(field);


			}

		}catch(Exception e){
			throw new DatabaseException("Could not fetch field information for field " +e);
		}
		finally{
			DatabaseHelper.close(pst);
			DatabaseHelper.close(pst1);
			DatabaseHelper.close(conn);
		}

		detail.put("FIELDS", fs);
		return detail;
	}

	private String scanPageAreaData(Workbook workbook2, String tdGid, String tdUid, String parent, String label,String entityType) throws TestDataException {

		String pagePrefix = "";
		if("request".equalsIgnoreCase(entityType)) {
			pagePrefix = "(REQ)_";
		} else if("response".equalsIgnoreCase(entityType)){
			/*pagePrefix = "(RESP)_";*/
			pagePrefix = "(RESP_200)_";
		}

		String data="";
		this.locMap =  new HashMap<String, Boolean>();
		int headerRow = 4;
		int dataStartCol = 3;
		int numberOfSheets=workbook.getNumberOfSheets();
		for(int i=0;i<numberOfSheets;i++){
			Sheet sheet=null;
			String sheetName=null;
			try {
				sheet = workbook.getSheetAt(i);
				sheetName=sheet.getSheetName();
				if(Utilities.trim(sheet.getSheetName()).equalsIgnoreCase("PAGE_AREA_INDEX")){
					continue;
				}else if(!Utilities.trim(sheet.getSheetName()).toLowerCase().startsWith(pagePrefix.toLowerCase())){
					logger.info("Skipping page [{}] as entity type is [{}]", sheetName, entityType);
					continue;
				}


				sheetName = sheet.getSheetName();
				logger.info("Processing sheet {}", sheetName);

				Row sheetNameRow = null;
				Cell sheetNameCell = null;
				sheetNameRow = sheet.getRow(SHEET_NAME_ROW_NUM);
				if(sheetNameRow == null){
					logger.error("SHEET NAME ROW not found. Invalid template");
					throw new TestDataException("Invalid Test Data Template - Page Name row was not found. Please use the latest test data template and try again");
				}

				sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);

				String locationName = getCellValue(sheetNameCell);
				if(locationName.equalsIgnoreCase("")){
					logger.error("LOCATION NAME TAMPERED WITH - Location Name is blank");
					throw new TestDataException("Invalid Test Data Template - Could not find Page Name in sheet [" + sheetName + "]. Please use the latest test data template and try again." );
				}

				logger.debug("Getting detail records on page {}", locationName);

				Row masterRow = null;
				Map<String, Row> tcRowMap = new TreeMap<String, Row>(); //Map that contains the Row Number as the key, and the detail row for the TDUID as the value

				logger.debug("Getting all rows with TDGID [{}] and TDUID [{}] on sheet [{}]", tdGid, tdUid, sheet.getSheetName());
				Iterator<Row> rowIter = sheet.iterator();
				String aTdDrId = "";
				while(rowIter.hasNext()){
					Row row = rowIter.next();

					if(getCellValue(row, TDGIDCOL).equalsIgnoreCase(tdGid) && getCellValue(row, TDUIDCOL).equalsIgnoreCase(tdUid)){
						aTdDrId = getCellValue(row, TDDRIDCOL);
						if(aTdDrId.equalsIgnoreCase("1") || aTdDrId.equalsIgnoreCase("")){
							masterRow = row;
						}

						tcRowMap.put(aTdDrId, row);
					}
				}
				int tFields = this.getTotalFieldsOnSheet(sheet, headerRow, dataStartCol);
				int masterStart=0;
				int dRow=0;
				/*JSONArray detailedRecords=null;*/
				if(masterRow!=null)
				{
					masterStart = masterRow.getRowNum();
					dRow = masterStart;
				}				
				if(masterRow == null){
					logger.error("Could not identify Master Record row for TDGID {}, TDUID {}", tdGid, tdUid);
					continue;
				}
				else{

					if(locationName.equals(parent)) {
						data=this.getDataFieldFromSheet(sheet, dRow,tFields,label,tdGid,tdUid);
					
					}

				}
			} catch (Exception e) {
				logger.error("ERROR in scanSheetForData for page {}", sheet.getSheetName(), e );
				throw new TestDataException("An error occurred while scanning page " + sheet.getSheetName() + " for test data. Please contact Tenjin Support.");
			}
		}

		return data;
	}

	private String getDataFieldFromSheet(Sheet sheet, int dRow, int tFields, String label, String tdGid, String tdUid) throws Exception {
		
		ArrayList<Row> detailRows = this.getDetailRowsForMaster(sheet, tdGid, tdUid, dRow);
       String data="";
		for(Row detailRow:detailRows){
			data=this.getLabelValue(label, sheet, tdUid, tdGid, dRow, detailRow, tFields);
		
	}
		return data;

	
}

	private String getLabelValue(String label, Sheet sheet, String tdUid, String tdGid, int dRow, Row detailRow,
			int tFields) throws TestDataException {
		int headerRow = 4;
		int dataStartCol = 3;
		String val = "";
		for(int i=dataStartCol;i<=dataStartCol+tFields;i++){
			Cell cell = detailRow.getCell(i);
			if(cell != null){

				cell.setCellType(Cell.CELL_TYPE_STRING);
				if(cell.getStringCellValue() != null && !cell.getStringCellValue().equalsIgnoreCase("")){
				String fieldName = sheet.getRow(headerRow).getCell(i).getStringCellValue();
				String fieldValue = cell.getStringCellValue();
				//if(fieldName.equals(label)) {
				//Keerthi
				if(fieldName.contains(label)) {
				try{
					val = this.resolve(fieldValue);
					return val;
				}catch(Exception e){
					throw new TestDataException(e.getMessage(),e);
				}
				}
				}
				
			}
		}
		return " ";
	}
	
	/*modified by paneendra for Tenj212-37 ends*/
	public TestObject getTestObject(Connection conn, String moduleCode, String page, int appId, String label,
			String apiOperation, String entityType) throws DatabaseException, SQLException {
		TestObject t = null;

		entityType = Utilities.trim(entityType).toUpperCase();
		if (conn == null) {
			throw new DatabaseException("Invalid database connection");
		}
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		try {

			pst = conn.prepareStatement(
					"SELECT * FROM AUT_FUNC_FIELDS WHERE FLD_APP=? AND FLD_FUNC_CODE = ? AND FLD_UNAME = ? AND FLD_PAGE_AREA = ? AND FLD_API_OPERATION=? AND FLD_API_ENTITY_TYPE=?");

			pst.setInt(1, appId);
			pst.setString(2, moduleCode);
			pst.setString(3, label);
			pst.setString(4, page);
			pst.setString(5, apiOperation);
			pst.setString(6, entityType);
			rs = pst.executeQuery();
			String parent = "";
			while (rs.next()) {
				t = new TestObject();
				t.setLabel(rs.getString("FLD_LABEL"));
				t.setName(rs.getString("FLD_UNAME"));
				t.setIdentifiedBy(rs.getString("FLD_UID_TYPE"));
				t.setUniqueId(rs.getString("FLD_UID"));
				t.setObjectClass(rs.getString("FLD_TYPE"));
				t.setLocation(rs.getString("FLD_PAGE_AREA"));
				parent = rs.getString("FLD_PAGE_AREA");
				t.setViewmode(rs.getString("VIEWMODE"));
				t.setMandatory(rs.getString("MANDATORY"));
				t.setLovAvailable(rs.getString("FLD_HAS_LOV"));
				t.setAutoLovAvailable(rs.getString("FLD_HAS_AUTOLOV"));
				t.setGroup(rs.getString("FLD_GROUP"));
				t.setIsMultiRecord(rs.getString("FLD_IS_MULTI_REC"));

			}
			pst1 = conn.prepareStatement(
					"SELECT PA_PATH FROM AUT_FUNC_PAGEAREAS WHERE PA_APP = ? AND PA_FUNC_CODE = ? AND PA_NAME=? AND PA_API_OPERATION=? AND PA_API_ENTITY_TYPE=? ");

			pst1.setInt(1, appId);
			pst1.setString(2, moduleCode);
			pst1.setString(3, parent);
			pst1.setString(4, apiOperation);
			pst1.setString(5, entityType);
			rs1 = pst1.executeQuery();

			while (rs1.next()) {
				t.setParentPath(rs1.getString("PA_PATH"));
			}
		} catch (Exception e) {
			throw new DatabaseException("Could not fetch field information for field " + label, e);
		} finally {
			DatabaseHelper.close(pst);
		}
		return t;
	}
}