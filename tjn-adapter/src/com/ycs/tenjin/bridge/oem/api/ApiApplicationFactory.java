/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ApiAdapterFactory.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 03-Apr-2017           Sriram Sridharan          Newly Added For
22-Sep-2017			sameer gupta			For new adapter spec
*/


package com.ycs.tenjin.bridge.oem.api;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.oem.api.generic.ApiResponseValidator;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;

public class ApiApplicationFactory {
	
	private static final Logger logger = LoggerFactory.getLogger(ApiApplicationFactory.class);
	
	public static List<String> getAvailableAPIAdapters() throws BridgeException {
		List<String> adapters = new ArrayList<String>();
		
		String expPackageName = "com.ycs.tenjin.adapter.api";
		Reflections reflections = new Reflections(expPackageName);
		
		Set<Class<? extends ApiApplication>> subTypes = reflections.getSubTypesOf(ApiApplication.class);
		for(Class<? extends ApiApplication> type:subTypes){
			String packageName = type.getPackage().getName();
			packageName = packageName.replace(expPackageName + ".", "");
			adapters.add(packageName);
		}
		
		return adapters;
	}
	
	public static ApiApplication create(String apiType) throws BridgeException {
		String expPackageName = "com.ycs.tenjin.adapter.api";
		expPackageName += "." + apiType;
		Reflections reflections = new Reflections(expPackageName);
		Set<Class<? extends ApiApplication>> subTypes = reflections.getSubTypesOf(ApiApplication.class);
		for(Class<? extends ApiApplication> type:subTypes){
			try {
				return type.newInstance();
			} catch (InstantiationException e) {
				
				logger.error("ERROR initializing adapter for [{}]", apiType, e);
			} catch (IllegalAccessException e) {
				
				logger.error("ERROR initializing adapter for [{}]", apiType, e);
			}
		}
		
		logger.error("Could not initialize a proper adapter for [{}]", apiType);
		throw new BridgeException("Could not initialize " + apiType + " adapter. Please contact Tenjin Support.");
		
		
	}
	
	public static ApiApplication getExecutor(String apiType, int runId, ExecutionStep step) throws BridgeException {
		String expPackageName = "com.ycs.tenjin.adapter.api";
		expPackageName += "." + apiType;
		
		Reflections reflections = new Reflections(expPackageName);
		Set<Class<? extends ApiApplication>> subTypes = reflections.getSubTypesOf(ApiApplication.class);
		
		for(Class<? extends ApiApplication>  type : subTypes) {
			try {
				Constructor<?> constructor = type.getConstructor(int.class, ExecutionStep.class);
				return (ApiApplication) constructor.newInstance(runId, step);
			} catch (Exception e) {
				
				logger.error("Could not initiate Executor for " +apiType, e);
			} 
		}
		
		logger.error("Could not initiate Executor for {}",apiType);
		return null;
	}
	
	public static ApiResponseValidator getResponseValidator(String apiType, int runId, ExecutionStep step, int iteration) throws BridgeException {
		String expPackageName = "com.ycs.tenjin.adapter.api";
		expPackageName += "." + apiType;
		
		Reflections reflections = new Reflections(expPackageName);
		Set<Class<? extends ApiResponseValidator>> subTypes = reflections.getSubTypesOf(ApiResponseValidator.class);
		
		for(Class<? extends ApiResponseValidator>  type : subTypes) {
			try {
				Constructor<?> constructor = type.getConstructor(int.class, ExecutionStep.class, int.class);
				return (ApiResponseValidator) constructor.newInstance(runId, step, iteration);
			} catch (Exception e) {
				
				logger.error("Could not initiate ApiResponseValidator for " +apiType, e);
			} 
		}
		
		logger.error("Could not initiate ApiResponseValidator for {}",apiType);
		throw new BridgeException("Could not initiate Validator for " + apiType + ". Please contact Tenjin Support.");
	}
	
	public static void main(String[] args) throws BridgeException {
		ApiApplication bridge = create("soap.fcubs");
		System.out.println(bridge.getAdapterName());
	}
}
