/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MessageValidateTestDataHandlerImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright © 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 *//*

*//******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 15-06-2021          Ashiki	        Newly Added For TENJINCG-1275

 */

package com.ycs.tenjin.bridge.oem.message.datahandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.exceptions.TestDataException;
import com.ycs.tenjin.bridge.pojo.aut.ExtractionRecord;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.DatabaseHelper;
import com.ycs.tenjin.db.LearningHelper;
import com.ycs.tenjin.util.Constants;
import com.ycs.tenjin.util.Utilities;

public class MessageValidateTestDataHandlerImpl implements MessageValidateTestDataHandler{

	public static final Logger logger = LoggerFactory.getLogger(MessageValidateTestDataHandlerImpl.class);

	private Workbook workbook;
	private static final int TDGIDCOL=0;
	private static final int TDUIDCOL=1;
	private static final int TDDRIDCOL=2;
	private static final int DATA_START_COL=3;

	private static final int HEADER_ROW_NUM=4;
	private static final int DATA_START_ROW_NUM=5;
	private static final int LEGEND_ROW_NUM=2;
	private static final int SHEET_NAME_ROW_NUM=0;
	private static final int SHEET_NAME_CELL_NUM=1;

	private static final short MRB_PAGE_SHEET_COLOR=IndexedColors.LIGHT_YELLOW.getIndex();
	private static final short QUERY_FIELD_COLOR = IndexedColors.LIGHT_ORANGE.getIndex();
	private static final short VERIFY_FIELD_COLOR = IndexedColors.YELLOW.getIndex();
	private static final short ADD_FIELD_COLOR = IndexedColors.LAVENDER.getIndex();
	private static final short DELETE_FIELD_COLOR = IndexedColors.GREY_50_PERCENT.getIndex();
	
	private Connection conn;
	@SuppressWarnings("unused")
	private Map<String, Object> locationsMap;
	private Map<String, Object> fieldsMap;
	
	
	public Workbook getWorkbook() {
		return workbook;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}
	private Map<String, Boolean> locMap;

	private HashMap<String, String> sheetNames;

	
	public void generateTemplate(int applicationId, String functionCode, String destinationFolderPath, String templateFileName, boolean mandatoryOnly, boolean editableOnly) throws TestDataException {
		

		LearningHelper helper = new LearningHelper();
		try {
			List<Location> locations = helper.hydrateMetadata(applicationId, functionCode, mandatoryOnly, editableOnly);
			
			this.generateTemplate(applicationId, functionCode, destinationFolderPath, templateFileName, locations);

		} catch (DatabaseException e) {
			
			throw new TestDataException(e.getMessage());
		} 

		

	}

	
	public void generateTemplate(int applicationId, String functionCode, String destinationFolderPath, String templateFileName,	JSONArray manifest) {
		
		List<Location> listLocation=new ArrayList<Location>();
		Location loc;
		try{
			for(int i=0;i<manifest.length();i++)
			{		JSONObject jsonObj1=manifest.getJSONObject(i);
					 loc=new Location();
					loc.setLocationName((String)(jsonObj1.get("name")));					
					loc.setLocationType((String)(jsonObj1.get("type")));
					if(jsonObj1.get("ismrb").equals("T"))
						loc.setMultiRecordBlock(true);
					else 
						loc.setMultiRecordBlock(false);
					List<TestObject> listTestObject=new ArrayList<TestObject>();					
					JSONArray jsonArr=(JSONArray)jsonObj1.get("fields");					
					for(int j=0;j<jsonArr.length();j++)
					{		JSONObject jsonObj2=jsonArr.getJSONObject(j);
							TestObject testObj=new TestObject();
							testObj.setDefaultOptions((String)jsonObj2.get("default_option"));
							testObj.setMandatory((String)jsonObj2.get("mandatory"));
							testObj.setName((String)jsonObj2.get("name"));
							testObj.setIdentifiedBy((String)jsonObj2.get("type"));
							testObj.setObjectClass(jsonObj2.getString("object_class"));
							listTestObject.add(testObj);
					}
					loc.setTestObjects(listTestObject);
					listLocation.add(loc);
			}
		}
		catch(Exception e)
		{	logger.error("Error ", e);
			logger.error("json Exception occured during extracting fields");
		}
	
		try {
			this.generateTemplate(applicationId, functionCode, destinationFolderPath, templateFileName, listLocation);
		} catch (TestDataException e) {
			
			logger.error("Error ", e);
		}

	}


	private void generateTemplate(int applicationId, String functionCode, String destinationFolderPath, String templateFileName, List<Location> locations) throws TestDataException{

		logger.debug("Creating new blank workbook");
		this.workbook = new XSSFWorkbook();
		
		logger.debug("Loading styles");
		CellStyle hStyle = getHeaderRowStyle(workbook);
		CellStyle mStyle = getMandatoryFieldStyle(workbook);
		CellStyle queryCellStyle = getQueryCellStyle(workbook);
		CellStyle addCellStyle = getAddCellStyle(workbook);
		CellStyle deleteCellStyle = getDeleteCellStyle(workbook);

		CellStyle verifyCellStyle = getVerifyCellStyle(workbook);
		boolean workbookHasSheets = false;
		
		for(Location location:locations){

			logger.debug("Processing location --> [{}]", location.getLocationName());
			if(location.getTestObjects() != null && location.getTestObjects().size() > 0){
				String sheetName= "";
				if(Utilities.trim(location.getLocationName()).length() > 30){
					logger.debug("Location [{}] has name of length more than 30 characters");
					sheetName = "Page_Area_" + location.getSequence();
				}else{
					sheetName = location.getLocationName();
				}

				XSSFSheet sheet = (XSSFSheet) this.workbook.createSheet(sheetName);
				workbookHasSheets = true;
				if(location.isMultiRecordBlock()){
					sheet.setTabColor(MRB_PAGE_SHEET_COLOR);
				}
				
				Row sheetNameRow = null;
				sheetNameRow = sheet.createRow(SHEET_NAME_ROW_NUM);
				sheetNameRow.createCell(0).setCellValue("Page Name:");
				sheetNameRow.getCell(0).setCellStyle(getPageNameCellStyle(workbook));
				sheetNameRow.createCell(1).setCellValue(location.getLocationName());
				sheetNameRow.getCell(1).setCellStyle(getPageNameMergedCellStyle(workbook));
				CellRangeAddress region = new CellRangeAddress(0,0,1,6);
				sheet.addMergedRegion(region);
				RegionUtil.setBorderBottom(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderTop(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderLeft(CellStyle.BORDER_THICK, region, sheet, workbook);
				RegionUtil.setBorderRight(CellStyle.BORDER_THICK, region, sheet, workbook);
				
				
				sheetNameRow.createCell(7).setCellValue("Do not change this value. It may result in errors during execution");
				sheetNameRow.getCell(7).setCellStyle(getPageNameSubscriptCellStyle(workbook));
				region = new CellRangeAddress(0,0,7,11);
				sheet.addMergedRegion(region);
				
				Row legendRow = null;
				Row headerRow = null;
				logger.debug("Writing legend");
				legendRow = sheet.createRow(LEGEND_ROW_NUM);
				legendRow.createCell(0).setCellValue("Legend");
				legendRow.createCell(1).setCellValue("INPUT");
				legendRow.createCell(2).setCellValue("QUERY");
				legendRow.createCell(3).setCellValue("ADD");
				legendRow.createCell(4).setCellValue("DELETE");
				legendRow.createCell(5).setCellValue("VERIFY");
				
				logger.debug("Applying styles for legend");
				legendRow.getCell(2).setCellStyle(queryCellStyle);
				legendRow.getCell(3).setCellStyle(addCellStyle);
				legendRow.getCell(4).setCellStyle(deleteCellStyle);
				legendRow.getCell(5).setCellStyle(verifyCellStyle);
								
				logger.debug("Writing header row");
				headerRow = sheet.createRow(HEADER_ROW_NUM);
				headerRow.createCell(0).setCellValue("TDGID");
				headerRow.getCell(0).setCellStyle(hStyle);
				headerRow.createCell(1).setCellValue("TDUID");
				headerRow.getCell(1).setCellStyle(hStyle);
				headerRow.createCell(2).setCellValue("TDDRID");
				headerRow.getCell(2).setCellStyle(hStyle);
				
				
				int currentCol = DATA_START_COL;
				
				logger.debug("Writing fields");
				List<String> buttons = new ArrayList<String>();
				for(TestObject field:location.getTestObjects()){
					if(field.getObjectClass().equalsIgnoreCase(FieldType.BUTTON.toString())){
						buttons.add(field.getName());
						continue;
					}
					
					if(field.getObjectClass().equalsIgnoreCase(FieldType.LINK.toString()) || 
							field.getObjectClass().equalsIgnoreCase(FieldType.TABLE.toString()) ||
							field.getObjectClass().equalsIgnoreCase(FieldType.TOOLBARITEM.toString())){
						logger.debug("Field [{}] -- Element Type [{}] will be skipped from template", field.getName(), field.getObjectClass());
						continue;
					}
					
					if(Utilities.trim(field.getMandatory()).equalsIgnoreCase("yes")){
						field.setName(field.getName().replace("<font color='red'><b>*</b></font>", ""));
					logger.debug("Adding field [{}]", field.getName());
					headerRow.createCell(currentCol).setCellValue(field.getName());
					}else{
						logger.debug("Adding field [{}]", field.getName());
						headerRow.createCell(currentCol).setCellValue(field.getName());
					}
					if(Utilities.trim(field.getMandatory()).equalsIgnoreCase("yes")){
						 logger.debug("Applying mandatory style for field [{}]", field.getName());
						headerRow.getCell(currentCol).setCellStyle(mStyle);
					}else if (field.getObjectClass().equalsIgnoreCase(FieldType.IMAGE.toString())) {
						headerRow.getCell(currentCol).setCellStyle(hStyle);
					}else{
						logger.debug("Applying normal style for field [{}]", field.getName());
						headerRow.getCell(currentCol).setCellStyle(hStyle);
					}
					
					
					if(!Utilities.trim(field.getDefaultOptions()).equalsIgnoreCase("")){
						logger.debug("Generating List Cell for field [{}]", field.getName());
						this.generateListCell(field.getDefaultOptions(), headerRow.getRowNum()+1, currentCol, sheet);
					}
					currentCol++;
				}
				
				String allButtons = "";
				if(buttons.size() > 0){
					headerRow.createCell(currentCol).setCellValue("Button");
					headerRow.getCell(currentCol).setCellStyle(hStyle);
					int counter=1;
					for(String button:buttons){
						allButtons = allButtons + button;
						if(counter < buttons.size()){
							allButtons = allButtons + ",";
						}
					}
					this.generateListCell(allButtons, headerRow.getRowNum()+1, currentCol, sheet);
				}
				
			}else{
				logger.debug("No fields found in location. This will be skipped");
			}
		}
		
		if(workbookHasSheets){
			String endFilePath = "";
			if(!Utilities.trim(templateFileName).endsWith(".xlsx")){
				templateFileName = Utilities.trim(templateFileName) + ".xlsx";
			}
			
			endFilePath = destinationFolderPath + "\\" + templateFileName;
			logger.debug("Writing file to disk");
			try{
				OutputStream out = new FileOutputStream(endFilePath);
				this.workbook.write(out);
				logger.debug("File saved in path {}",endFilePath);
			}catch(Exception e){
				logger.error("ERROR while writing template to disk", e);
				throw new TestDataException("Could not generate Test data template due to an internal error. Please contact Tenjin support");
			}
		}

	}

	@SuppressWarnings("unused")
	private JSONArray scanSheetForData(Sheet sheet, String tdGid, String moduleCode, int appId, String tdUid, LearningHelper helper, Location location,Map<String,String> manualField) throws TestDataException{
		JSONArray detailRecords = new JSONArray();
		String pName = "";
		JSONObject page = new JSONObject();
		this.locMap =  new HashMap<String, Boolean>();
		int testCaseCol = 0;
		int headerRow = 4;
		int dataStartRow = 5;
		int dataStartCol = 3;
		try{
			pName = location.getLocationName();
			if(pName.equalsIgnoreCase("tax details")){
				System.err.println();
			}
						
		}catch(Exception e){
			logger.error("ERROR Getting sheet name");
			logger.error(e.getMessage(),e);
			try {
				throw new TestDataException("An internal error occurred while reading test data for this step");
			} catch (Exception e1) {
				
				logger.error("Error ", e1);
			}
		}
		if(sheet == null){
			logger.error("ERROR in scanSheetForData -- sheet is null");
			return null;
		}
		
		logger.debug("Processing sheet [{}] for Location [{}]", sheet.getSheetName(), location.getLocationName());
		
		try {
			Row masterRow = null;
			Map<String, Row> tcRowMap = new TreeMap<String, Row>(); //Map that contains the Row Number as the key, and the detail row for the TDUID as the value
			
			logger.debug("Getting all rows with TDGID [{}] and TDUID [{}] on sheet [{}]", tdGid, tdUid, sheet.getSheetName());
			Iterator<Row> rowIter = sheet.iterator();
			String aTdDrId = "";
			while(rowIter.hasNext()){
				Row row = rowIter.next();
				
				if(getCellValue(row, TDGIDCOL).equalsIgnoreCase(tdGid) && getCellValue(row, TDUIDCOL).equalsIgnoreCase(tdUid)){
					aTdDrId = getCellValue(row, TDDRIDCOL);
					if(aTdDrId.equalsIgnoreCase("1") || aTdDrId.equalsIgnoreCase("")){
						masterRow = row;
					}
					
					tcRowMap.put(aTdDrId, row);
				}
			}
			
			if(masterRow == null){
				logger.error("Could not identify Master Record row for TDGID {}, TDUID {}", tdGid, tdUid);
				return new JSONArray();
			}
			Location loc = null;
			LearningHelper lHelper = new LearningHelper();
			
			loc=location;
			int tFields = this.getTotalFieldsOnSheet(sheet, headerRow, dataStartCol);
			int masterStart = masterRow.getRowNum();
			int dRow = masterStart;
			if(!location.isMultiRecordBlock()){
				logger.info("Processing master row");
				Map<String, Boolean> mrbLocMap = new HashMap<String, Boolean>();
				/*int numberOfSheets = 0;
				try{
					numberOfSheets = workbook.getNumberOfSheets();
				}catch(Exception e){
					logger.error("ERROR getting number of sheets");
					logger.error(e.getMessage(),e);
					throw new TestDataException(e.getMessage());
				}
				this.getSingleRecordTestData(numberOfSheets);*/
				JSONArray fields = this.getTestDataFromRow(sheet, masterRow, appId, moduleCode, helper, location, manualField);
				if(fields!=null && fields.length()>0){
					String masterDrId = getCellValue(masterRow, TDDRIDCOL);
					JSONObject detailRecord = new JSONObject();
					detailRecord.put("DR_ID", masterDrId);
					detailRecord.put("FIELDS", fields);
					detailRecords.put(detailRecord);
				}
			}
			else{
				
				ArrayList<Row> detailRows = this.getDetailRowsForMaster(sheet, tdGid, tdUid, dRow);

				if(sheet.getSheetName().equalsIgnoreCase("Document Collection")){
					System.err.println();
				}

				//JSONArray details = new JSONArray();
				for(Row detailRow:detailRows){
					JSONObject detail = new JSONObject();
					String dRecordId = detailRow.getCell(2).getStringCellValue();
					detail.put("DR_ID", dRecordId);
					JSONArray fs = new JSONArray();
					for(int i=dataStartCol;i<=dataStartCol+tFields;i++){
						Cell cell = detailRow.getCell(i);
						if(cell != null && cell.getStringCellValue() != null && !cell.getStringCellValue().equalsIgnoreCase("")){
							String fieldName = sheet.getRow(headerRow).getCell(i).getStringCellValue();
							String fieldValue = cell.getStringCellValue();
							//logger.debug("Field: " + fieldName + ", Value: " + fieldValue);
							String val = "";
							try{
								val = this.resolve(fieldValue);
							}catch(Exception e){
								throw new TestDataException(e.getMessage(),e);
							}
							TestObject t=null;
							if(fieldName.equalsIgnoreCase("Button"))
							{
								t=new TestObject();
								t.setLabel("Button");
								t.setObjectClass("Button");
								t.setUniqueId(val);
								t.setIdentifiedBy("Button");
								t.setLocation(loc.getLocationName());
								t.setViewmode("N");
								t.setMandatory("NO");
								t.setLovAvailable("N");
							}
							else{
							 t = helper.hydrateTestObject(this.conn, moduleCode, pName, appId, fieldName);
							}
							if(t.getLabel() == null){
								//	logger.error("Field " + fieldName + " was not found in " + pName + " page in " + moduleCode + ". Re-learning may be required.");
								throw new TestDataException("Information for field " + fieldName + " was not found in " + pName + " page in " + moduleCode + ". Re-learn this function or use the latest Test Data template");
							}
							JSONObject field = new JSONObject();
							field.put("DATA", val);
							/*Modified by Preeti for TJN252-80 starts*/
							/*field.put("FLD_LABEL", fieldName);*/
							/******************************************************************************
							 * Fix for CR#4401 in Tenjin_SBM by Sriram (23-06-2016)
							 */
							/*field.put("FLD_LABEL_2", t.getLabel());*/
							field.put("FLD_LABEL", t.getLabel());
							/*Modified by Preeti for TJN252-80 ends*/
							/******************************************************************************
							 * Fix for CR#4401 in Tenjin_SBM by Sriram (23-06-2016) ends
							 */
							field.put("FLD_TYPE",t.getObjectClass());
							field.put("FLD_UID", t.getUniqueId());
							field.put("FLD_UID_TYPE", t.getIdentifiedBy());
							String hasLov = "";
							if(t.getLovAvailable() == null){
								hasLov = "N";
							}else{
								hasLov = t.getLovAvailable();
							}
							field.put("FLD_HAS_LOV", hasLov);
							/*****************************************************
							 * Change by Sriram to enable color-coding in Tenjin Test Data (17-Nov-2014)
							 */
							if(!loc.isMultiRecordBlock()){
								short bgColor = cell.getCellStyle().getFillForegroundColor();
								if(bgColor == IndexedColors.GREEN.getIndex()){
									field.put("ACTION", "QUERY");
								}else if(bgColor == IndexedColors.YELLOW.getIndex()){
									field.put("ACTION", "VERIFY");
								}else{
									field.put("ACTION", "INPUT");
								}
							}else{
								Cell hCell = sheet.getRow(headerRow).getCell(i);
								Short headerBgColor = hCell.getCellStyle().getFillForegroundColor();
								Short bgColor = cell.getCellStyle().getFillForegroundColor();
								if(bgColor == IndexedColors.LAVENDER.getIndex()){
									field.put("ACTION","ADDROW");
								}else if(bgColor == IndexedColors.GREY_40_PERCENT.getIndex()){
									field.put("ACTION", "DELETEROW");
								}else if(bgColor == IndexedColors.YELLOW.getIndex()){
									field.put("ACTION", "VERIFY");
								}else{
									if(headerBgColor == IndexedColors.LIGHT_ORANGE.getIndex()){
										field.put("ACTION", "QUERY");
									}else{
										field.put("ACTION", "INPUT");
									}
								}
							}

							fs.put(field);
						}
					}

					detail.put("FIELDS", fs);

					ArrayList<Location> vicinityMrbs = null;
					ArrayList<Location> vMrbs = null;
					ArrayList<Location> siblingMrbs = null;
					ArrayList<Location> childMrbs = null;

					try{
						childMrbs = helper.hydrateChildMultiRecordBlocks(this.conn, moduleCode,appId,"",pName);
					}catch(Exception e){
						logger.error("ERROR hydrating Child record blocks of {}", pName);
						logger.error(e.getMessage(),e);
					}

					if(childMrbs!=null && childMrbs.size()>0){

						int numberOfSheets = 0;
						try{
							numberOfSheets = workbook.getNumberOfSheets();
						}catch(Exception e){
							logger.error("ERROR getting number of sheets");
							logger.error(e.getMessage(),e);
							throw 
							new TestDataException(e.getMessage());
						}
						vicinityMrbs = new ArrayList<Location>();
						if(vMrbs != null && vMrbs.size() > 0){
							for(Location vMrb:vMrbs){
								vicinityMrbs.add(vMrb);
							}
						}

						if(siblingMrbs != null && siblingMrbs.size() > 0){
							for(Location vMrb:siblingMrbs){
								vicinityMrbs.add(vMrb);
							}
						}

						if(childMrbs != null && childMrbs.size() > 0){
							for(Location vMrb:childMrbs){
								vicinityMrbs.add(vMrb);
							}
						}
						ArrayList<Row> sDetailRows=null;
						JSONArray sDetails = new JSONArray();
						Map<String, Boolean> mrbLocMap = new HashMap<String, Boolean>();
						if(vicinityMrbs != null && vicinityMrbs.size() > 0){
							JSONArray level2Details = new JSONArray();
							for(Location mrb:vicinityMrbs){
								logger.debug("Scanning Multi Record Block {} for more detail records for TDGID {}, TDUID {}, detail ID {}", mrb.getLocationName(), tdGid, tdUid, dRecordId);
								if(mrb.getLocationName().equalsIgnoreCase("Clearing Details")){
									System.err.println("Breakpoint");
								}
								XSSFSheet mrbSheet = null;
								String sName = "";
								for(int i=0;i<numberOfSheets;i++){
									try{
										XSSFSheet oSheet = (XSSFSheet) workbook.getSheetAt(i);
										sName = oSheet.getSheetName();
										if(Utilities.trim(sName).equalsIgnoreCase(mrb.getLocationName())){
											boolean sheetVisited = false;
											try{
												sheetVisited = mrbLocMap.get(sName);
											}catch(Exception e){

											}
											if(sheetVisited){
												logger.debug("This sheet has already been visited. Skipping");
												continue;
											}else{
												mrbSheet = oSheet;
												logger.info("Found sheet for {}", mrb.getLocationName());
												break;
											}

										}
									}catch(Exception e){
										logger.error("ERROR in finding sheet for {}", mrb.getLocationName());
										logger.error(e.getMessage(),e);
									}
								}

								if(mrbSheet != null){
								sDetailRows = this.getSecondLevelDetailRowsForMaster(mrbSheet, tdGid, tdUid, dRecordId , dataStartRow);

									if(sDetailRows != null && sDetailRows.size() > 0){
										int totalFields = this.getTotalFieldsOnSheet(mrbSheet, headerRow, dataStartCol);

										for(Row sDetailRow:sDetailRows){
											JSONObject sDetail = new JSONObject();
											String drid = sDetailRow.getCell(2).getStringCellValue();
											sDetail.put("DR_ID", drid);
											JSONArray dfs = new JSONArray();
											for(int f=dataStartCol;f<=dataStartCol+totalFields;f++){
												Cell cell = sDetailRow.getCell(f);
												if(cell != null && cell.getStringCellValue() != null && !cell.getStringCellValue().equalsIgnoreCase("")){
													String fieldName = mrbSheet.getRow(headerRow).getCell(f).getStringCellValue();
													String fieldValue = cell.getStringCellValue();
													String val = "";
													try{
														val = this.resolve(fieldValue);
													}catch(Exception e){
														throw new TestDataException(e.getMessage(),e);
													}
													TestObject t=null;
													if(fieldName.equalsIgnoreCase("Button"))
													{
														t=new TestObject();
														t.setLabel("Button");
														t.setObjectClass("Button");
														t.setUniqueId(val);
														t.setIdentifiedBy("Button");
														t.setLocation(loc.getLocationName());
														t.setViewmode("N");
														t.setMandatory("NO");
														t.setLovAvailable("N");
														
													}
													else{
														t = helper.hydrateTestObject(this.conn, moduleCode, sName, appId, fieldName);
													}
													if(t.getLabel() == null){
														throw new TestDataException("Information for field " + fieldName + " was not found in " + sName + " page in " + moduleCode + ". Re-learn this function or use the latest Test Data template");
													}
													JSONObject field = new JSONObject();
													field.put("DATA", val);
													field.put("FLD_LABEL", t.getLabel());
													field.put("FLD_TYPE",t.getObjectClass());
													field.put("FLD_UID", t.getUniqueId());
													field.put("FLD_UID_TYPE", t.getIdentifiedBy());
													String hasLov = "";
													if(t.getLovAvailable() == null){
														hasLov = "N";
													}else{
														hasLov = t.getLovAvailable();
													}
													field.put("FLD_HAS_LOV", hasLov);
													if(!loc.isMultiRecordBlock()){
														short bgColor = cell.getCellStyle().getFillForegroundColor();
														if(bgColor == IndexedColors.GREEN.getIndex()){
															field.put("ACTION", "QUERY");
														}else if(bgColor == IndexedColors.YELLOW.getIndex()){
															field.put("ACTION", "VERIFY");
														}else{
															field.put("ACTION", "INPUT");
														}
													}else{
														Cell hCell = mrbSheet.getRow(headerRow).getCell(f);
														Short headerBgColor = hCell.getCellStyle().getFillForegroundColor();
														Short bgColor = cell.getCellStyle().getFillForegroundColor();
														if(bgColor == IndexedColors.LAVENDER.getIndex()){
															field.put("ACTION","ADDROW");
														}else if(bgColor == IndexedColors.GREY_40_PERCENT.getIndex()){
															field.put("ACTION", "DELETEROW");
														}else if(bgColor == IndexedColors.YELLOW.getIndex()){
															field.put("ACTION", "VERIFY");
														}else{
															if(headerBgColor == IndexedColors.LIGHT_ORANGE.getIndex()){
																field.put("ACTION", "QUERY");
															}else{
																field.put("ACTION", "INPUT");
															}
														}
													}

													dfs.put(field);
												}
											}

											sDetail.put("FIELDS", dfs);
											if(dfs != null && dfs.length() > 0){
												sDetails.put(sDetail);
												
												this.locMap.put(sName, true);
												mrbLocMap.put(sName, true);
											}
											
										}
									}
									

								}
							
							}
							if(sDetailRows!=null && sDetailRows.size()>0)
								detail.put("LEVEL2DETAILS", sDetails);
						}
					}
					if(detail!=null && detail.length()>0)
						detailRecords.put(detail);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR in scanSheetForData for page {}", sheet.getSheetName(), e );
			throw new TestDataException("An error occurred while scanning page " + sheet.getSheetName() + " for test data. Please contact Tenjin Support.");
		}
		
		
		return detailRecords;
	}
	private JSONArray getTestDataFromRow(Sheet sheet, Row masterRow, int appId, String moduleCode, LearningHelper lHelper, Location location,Map<String,String> manualField) throws TestDataException{
		
		if(masterRow == null){
			logger.error("ERROR in getTestDataFromRow --> row is null");
			return null;
		}
		
		if(sheet == null){
			logger.error("ERROR in getTestDataFromRow --> sheet is null");
			return null;
		}
		
		try {
			logger.debug("Getting test data from row number [{}] on sheet [{}]", masterRow.getRowNum(), sheet.getSheetName());
			Row headerRow = sheet.getRow(HEADER_ROW_NUM);
			Iterator<Cell> cellIter = headerRow.cellIterator();
			
			Cell cell = null;
			
			String fieldName = "";
			String fieldValue = "";
			TestObject t = null;
			JSONArray fields = new JSONArray();
			while(cellIter.hasNext()){
				cell = cellIter.next();
				
				if(cell.getColumnIndex() >= DATA_START_COL){
					
					fieldName = cell.getStringCellValue();
					/*fieldValue = masterRow.getCell(cell.getColumnIndex(), Row.CREATE_NULL_AS_BLANK).getStringCellValue();*/
					fieldValue = getCellValue(masterRow, cell.getColumnIndex());
					logger.debug("Processing field {}, value {}", fieldName, fieldValue);
					
					boolean manualInputStatus=false;
					if(manualField!=null && manualField.size()>0 &&  manualField.get(fieldName)!=null ){
						if( manualField.get(fieldName).equals(fieldName)){
						manualInputStatus=true;
						}								
					}
					
					fieldValue = this.resolve(fieldValue);
					
					if(fieldValue.length() <= 0 && !manualInputStatus){
						logger.debug("Value for field {} on page {} is empty. Skipping it", fieldName, sheet.getSheetName());
						continue;
					}
					
					String oLabel = "";
					if(fieldName.equalsIgnoreCase("Button"))
					{
						t=new TestObject();
						t.setObjectClass("Button");
						t.setUniqueId(fieldValue);
						t.setIdentifiedBy("Button");
						t.setLocation(location.getLocationName());
						t.setViewmode("N");
						t.setMandatory("NO");
						t.setLovAvailable("N");
					}
					if(fieldName.equalsIgnoreCase("button")){
						oLabel = fieldValue;
					}else{
						oLabel = fieldName;
					}
					if (fieldName.equalsIgnoreCase("button")) {
						t.setLabel("Button");
					}else{
						logger.debug("Hydrating test object for field {}", fieldName);
						t = lHelper.hydrateTestObject(moduleCode, location.getLocationName(), appId, oLabel);
					}
					
				
					if(t == null){
						logger.error("TestObject for field {} on page {} is null", fieldName, sheet.getSheetName());
						throw new TestDataException("Could not find information about field " + fieldName + " on page " + sheet.getSheetName() + ". Re-learning of function may be required");
					}
					
					if(fieldName.equalsIgnoreCase("button")){
						t.setLabel("Button");
					}
					
					t.setData(fieldValue);
					
					logger.debug("Ascertaining action");
					
					short bgColor = getCellFillForegroundColor(masterRow, cell.getColumnIndex());
					
					if(!location.isMultiRecordBlock()){
						if(bgColor == QUERY_FIELD_COLOR){
							t.setAction("QUERY");
						}else if(bgColor == VERIFY_FIELD_COLOR){
							t.setAction("VERIFY");
						}else if(bgColor == ADD_FIELD_COLOR){
							t.setAction("ADD");
						}else if(bgColor == DELETE_FIELD_COLOR){
							t.setAction("DELETE");
						}else{
							
							if(manualInputStatus){
								t.setAction("MANUAL");
							}
							else{
								t.setAction("INPUT");
							}							
						}
					}else{
						short headerBgColor = getCellFillForegroundColor(headerRow, cell.getColumnIndex());
						if(headerBgColor == QUERY_FIELD_COLOR){
							t.setAction("QUERY");
						}else if(bgColor == VERIFY_FIELD_COLOR){
							t.setAction("VERIFY");
						}else if(bgColor == ADD_FIELD_COLOR){
							t.setAction("ADD");
						}else if(bgColor == DELETE_FIELD_COLOR){
							t.setAction("DELETE");
						}else{
							if(manualInputStatus){
								t.setAction("MANUAL");
							}
							else{
								t.setAction("INPUT");
							}
						}
					}
					
					
					
					logger.debug("Preparing JSON for field {}", fieldName);
					JSONObject field = t.toJson();
					fields.put(field);
					
				}
			}
			
			return fields;
		} catch (DatabaseException e) {
			
			throw new TestDataException(e.getMessage(), e);
		} catch (SQLException e) {
			
			throw new TestDataException(e.getMessage(), e);
		} catch (Exception e) {
			
			throw new TestDataException("Could not process test case row due to an internal error. Please contact Tenjin Support.");
		}
	
	}
	
	private static short getCellFillForegroundColor(Row row, int colIndex){
		if(row == null){
			logger.error("ERROR in getting cell foreground color - row is null");
			return 0;
		}
		
		try{
			Cell cell = row.getCell(colIndex, Row.CREATE_NULL_AS_BLANK);
			return  cell.getCellStyle().getFillForegroundColor();
		}catch(Exception e){
			logger.error("ERROR in getCellFillForegroundColor for cell with index {} on rowo {}", colIndex, row.getRowNum(), e);
			return 0;
		}
		
		
	}
	
	private void generateListCell(String options, int startRow, int startCol, XSSFSheet sheet){

		if(Utilities.trim(options).length() > 250){
			logger.warn("List cell not generated due to options exceeing maximum length");
			return;
		}

		DataValidation dataValidation = null;
		DataValidationConstraint constraint = null;
		DataValidationHelper validationHelper = null;
		validationHelper=new XSSFDataValidationHelper(sheet);
		CellRangeAddressList addressList = new  CellRangeAddressList(startRow,500,startCol,startCol);
		constraint =validationHelper.createExplicitListConstraint(options.split(";"));
		dataValidation = validationHelper.createValidation(constraint, addressList);
		dataValidation.setSuppressDropDownArrow(true);    
		sheet.addValidationData(dataValidation);
	}

	private static CellStyle getVerifyCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getPageNameCellStyle(Workbook wb){
		CellStyle style = null;
		style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);
		return style;
	}
	
	private static CellStyle getPageNameMergedCellStyle(Workbook wb){
		CellStyle style = null;
		style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);
		return style;
	}
	
	private static CellStyle getPageNameSubscriptCellStyle(Workbook wb){
		CellStyle style = null;
		style = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		font.setFontHeightInPoints((short)8);
		style.setFont(font);
		return style;
	}
	
	private static CellStyle getQueryCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);


		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getAddCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);


		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.LAVENDER.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getDeleteCellStyle(Workbook wb){
		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.WHITE.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getHeaderRowStyle(Workbook wb){

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	private static CellStyle getMandatoryFieldStyle(Workbook wb){

		CellStyle style = null;

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerFont.setColor(IndexedColors.GREY_25_PERCENT.getIndex());

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(headerFont);
		style.setFillForegroundColor(IndexedColors.DARK_RED.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		return style;
	}

	
	public List<String> getAllTdUidsInRange( String absoluteFilePath, String tdGid, int dataLimit) throws TestDataException {
		
		logger.info("Loading workbook from path [{}]", absoluteFilePath);
		
		try {
			this.workbook = new XSSFWorkbook(new FileInputStream(new File(absoluteFilePath)));
		} catch (FileNotFoundException e) {
			
			logger.error(e.getMessage());
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		} catch (IOException e) {
			
			logger.error("IOException caught", e);
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		}
		
		
		List<String> tdUids = new ArrayList<String>();
		
		try{
			
			logger.info("Getting all TDUIDs for TDGID [{}]", tdGid);
			logger.debug("Loading first sheet");
			
			Sheet sheet = this.workbook.getSheetAt(0);
			int currentRow = DATA_START_ROW_NUM-1;
			boolean loopOk = true;
			while(loopOk && tdUids.size() < dataLimit){
				currentRow++;
				logger.debug("Scanning row [{}]", currentRow);
				Row row = sheet.getRow(currentRow);
				if(row == null){
					loopOk = false;//No more rows. End the loop
					logger.debug("No more rows..");
					continue;
				}
				
				Cell gidCell = row.getCell(TDGIDCOL);
				if(gidCell == null){
					loopOk = false;//No more rows. End the loop
					logger.debug("No more rows..");
					continue;
				}
				
				gidCell.setCellType(Cell.CELL_TYPE_STRING);
				
				String gid = gidCell.getStringCellValue();
				if(gid == null || gid.equalsIgnoreCase("")){
					loopOk = false;//No more rows. End the loop
					logger.debug("No more rows..");
					continue;
				}
				String uid = "";
				if(gid.equalsIgnoreCase(tdGid)){
					Cell uidCell = row.getCell(TDUIDCOL);
					if(uidCell != null){
						uidCell.setCellType(Cell.CELL_TYPE_STRING);
						uid = uidCell.getStringCellValue();
						if(uid == null || Utilities.trim(uid).equalsIgnoreCase("")){
							logger.error("TDUID not specified in row {} in Test data file for TDGID {}",currentRow, tdGid);
							continue;
						}
					}else{
						logger.error("ERROR in row {} in Test data file for TDGID {} --> TDUID seems to be invalid",currentRow,  tdGid);
						continue;
					}
					if(!tdUids.contains(uid)){
						logger.debug("Found TDUID [{}]", uid);
						tdUids.add(uid);
					}
					
				}
			}
		}catch(Exception e){
			logger.error("ERROR getting all TDUIDs for TDGID [{}]\n File Path --> [{}]", tdGid, absoluteFilePath, e);
			throw new TestDataException("Could not validate test data for this step due to an internal error. Contact Tenjin Support.");
		}
		
		
		return tdUids;
	}
	
	private static String getCellValue(Cell cell){
		if(cell != null){
			cell.setCellType(Cell.CELL_TYPE_STRING);
			return Utilities.trim(cell.getStringCellValue());
		}else{
			return "";
		}
	}
	
	private static String getCellValue(Row row, int colIndex){
		
		if(row == null){
			logger.error("ERROR in getting cell value - row is null");
			return "";
		}
		try{
			Cell cell = row.getCell(colIndex, Row.CREATE_NULL_AS_BLANK);
			cell.setCellType(Cell.CELL_TYPE_STRING);
			return Utilities.trim(cell.getStringCellValue());
		}catch(Exception e){
			logger.error("ERROR getting value of cell with index {} on row {}", colIndex, row.getRowNum(), e);
			return "";
		}
	}
	
	
	
	@SuppressWarnings("unused")
	private String resolve(String dataval) throws TestDataException{

		
		String output = dataval;
		String data = dataval;
		
		ArrayList<String> params = new ArrayList<String>();
		if(dataval != null && dataval.startsWith("<<") && dataval.endsWith(">>")){
			dataval = dataval.replace("<<","");
			dataval = dataval.replace(">>","");
			String[] split = dataval.split(";;");
			if(split.length == 1){
				params.add(split[0]);
			}else{
				params.add(split[0]);
				for(int i=1;i<split.length;i++){
					String s = split[i];
					params.add(s);
				}
			}
		} else{
			return dataval;
		}
		
		if(params != null && params.size() == 1){
			logger.error("ERROR invalid usage. Data specified is {} but expecting a field name to fetch",dataval);
			throw new TestDataException("Invalid Syntax to get runtime output. Right syntax is <<TDUID(FIELD_IDENTIFIER)>>");
		}
		
		if(params != null && params.size() > 1){
			String tdUid = params.get(0);
			String fieldIdentifier = params.get(1);
			
			
			Connection projConn = null;
			try{
				projConn = DatabaseHelper.getConnection(Constants.DB_TENJIN_PROJ);
				output = new LearningHelper().getRuntimeFieldOutput(projConn, tdUid, fieldIdentifier);
				logger.info("Output Avaliable is : " + output);
			}catch(DatabaseException e){
				throw new TestDataException(e.getMessage(),e);
			}catch(Exception e){
				logger.error("ERROR resolving field value {}", dataval,e);
				throw new TestDataException("Could not resolve value for " + dataval + " due to an internal error");
			}finally{
				try{
					projConn.close();
				}catch(Exception ignore){}
			}
						
			return output;
		}else{
			return dataval;
		}
		
		
	
	}
	private ArrayList<Row> getSecondLevelDetailRowsForMaster(XSSFSheet sheet,String tdgid, String tduid, String detailRecordId, int dataStartRow) throws Exception{
		ArrayList<Row> detailRows = new ArrayList<Row>();
		
		try{
			int cRow = dataStartRow;
			while(sheet.getRow(cRow) != null && sheet.getRow(cRow).getCell(0) != null && sheet.getRow(cRow).getCell(0).getStringCellValue() != null && !sheet.getRow(cRow).getCell(0).getStringCellValue().equalsIgnoreCase("")){
				if(sheet.getRow(cRow).getCell(0).getStringCellValue().equalsIgnoreCase(tdgid) && sheet.getRow(cRow).getCell(1) != null && sheet.getRow(cRow).getCell(1).getStringCellValue().equalsIgnoreCase(tduid) && 
						sheet.getRow(cRow).getCell(2) != null && sheet.getRow(cRow).getCell(2).getStringCellValue() != null && !sheet.getRow(cRow).getCell(2).getStringCellValue().equalsIgnoreCase("")){
					String dId = sheet.getRow(cRow).getCell(2).getStringCellValue();
					if(dId.contains(".")){
						String[] dsplit = dId.split("\\.");
						if(dId.toLowerCase().startsWith(detailRecordId) && dsplit.length > 1 && dsplit.length < 4){
							detailRows.add(sheet.getRow(cRow));
						}
					}
				}
				
				cRow++;
			}
		}catch(Exception e){
			logger.error("Could not fetch second level detail rows for TDGID {}, TDUID {}, Detail Record ID {}", tdgid, tduid, detailRecordId);
			logger.error(e.getMessage(),e);
			throw new TestDataException(e.getMessage(),e);
		}
		
		return detailRows;
	}
	private int getTotalFieldsOnSheet(Sheet lSheet, int headerRow, int startColumnIndex){
		try{
			int totalFields = 0;
			int curCol = startColumnIndex;
			while(lSheet.getRow(headerRow) != null && lSheet.getRow(headerRow).getCell(curCol) != null && lSheet.getRow(headerRow).getCell(curCol).getStringCellValue() != null && !lSheet.getRow(headerRow).getCell(curCol).getStringCellValue().equalsIgnoreCase("")){
				totalFields++;
				curCol++;
			}
			
			return totalFields;
		}catch(Exception e){
			logger.error("ERROR getting number of fields on sheet");
			logger.error(e.getMessage(),e);
			return 0;
		}
	}
	private ArrayList<Row> getDetailRowsForMaster(Sheet sheet,String tdgid, String tduid, int masterStart) throws Exception{
		ArrayList<Row> detailRows = new ArrayList<Row>();
		int dRow = masterStart;
		try{
		while(sheet.getRow(dRow) != null && sheet.getRow(dRow).getCell(0) != null && sheet.getRow(dRow).getCell(0).getStringCellValue() != null && !sheet.getRow(dRow).getCell(0).getStringCellValue().equalsIgnoreCase("")){
			if(sheet.getRow(dRow).getCell(0).getStringCellValue().equalsIgnoreCase(tdgid) && sheet.getRow(dRow).getCell(1) != null && sheet.getRow(dRow).getCell(1).getStringCellValue().equalsIgnoreCase(tduid)){
				detailRows.add(sheet.getRow(dRow));
			}
			dRow++;
		} 
		
		}catch(Exception e){
			logger.error("ERROR fetching detail rows for TDGID {}, TDUID {}", tdgid, tduid);
			logger.error(e.getMessage(),e);
			/*throw new TestDataException(e.getMessage(),e);*/
			throw new TestDataException(e.getMessage(),e);
		}
		return detailRows;
	}
	
	@SuppressWarnings("unused")
	public JSONObject loadTestDataFromTemplate(String absoluteFilePath, int appId, String functionCode, String tdGid, String tdUid,int testCaseId,String testStepId, String apiOperation, String entityType) throws TestDataException {
	//public JSONObject loadTestDataFromTemplate(String absoluteFilePath, int appId, String functionCode, String tdGid, String tdUid) throws TestDataException {
		
		JSONObject data = null;
		
		try{
			logger.info("Loading workbook from path [{}]", absoluteFilePath);
			this.workbook = WorkbookFactory.create(new File(absoluteFilePath));
		}catch(Exception e){
			
			logger.error(e.getMessage());
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		}
		
		try{
			logger.info("Obtaining Database Connection");
			this.conn = DatabaseHelper.getConnection(Constants.DB_TENJIN_APP);
		} catch(DatabaseException e) {
			logger.error(e.getMessage());
			throw new TestDataException("Could not obtain Database connection to load Test Data. Please contact Tenjin Support.");
		}
		
		LearningHelper helper = new LearningHelper();
		int numberOfSheets = workbook.getNumberOfSheets();
		logger.info("Loading Test Data");
		logger.info("Number of sheets --> [{}]", numberOfSheets);
		
		String sheetName = "";
		Location location = null;
		JSONArray detailRecords = null;
		Row sheetNameRow = null;
		String locationName = null;
		Cell sheetNameCell = null;
		JSONArray pageAreas =new JSONArray();
		
		try {
			data=this.scanTestData(workbook,tdGid, functionCode, appId, tdUid, helper, location,testCaseId,testStepId, apiOperation, entityType);
		} catch(TestDataException e){
			throw e;
		}
		catch (JSONException e) {
			
			logger.error("JSONException caught", e);
			throw new TestDataException("Could not load test data due to an internal error. Please contact Tenjin Support.");
		} catch(Exception e){
			
			logger.error("Unknown Exception caught", e);
			throw new TestDataException("Could not load test data due to an internal error. Please contact Tenjin Support.");
		} finally {
			logger.info("Closing DB Connection after loading test data");
			try{
				this.conn.close();
			} catch(Exception ignore) {}
		}
		return data;
	}

	private JSONObject scanTestData(Workbook workbook, String tdGid, String moduleCode, int appId, String tdUid, LearningHelper helper, Location location,int testCaseId,String testStepId, String apiOperation, String entityType) throws TestDataException, JSONException{
		
		
		JSONArray detailRecords = new JSONArray();
		JSONObject page = new JSONObject();
		this.locMap =  new HashMap<String, Boolean>();
		int headerRow = 4;
		int dataStartCol = 3;
		int numberOfSheets=workbook.getNumberOfSheets();
		for(int i=0;i<numberOfSheets;i++){
			Sheet sheet=null;
			String sheetName=null;
			try {
				sheet = workbook.getSheetAt(i);
				sheetName=sheet.getSheetName();
				if(Utilities.trim(sheet.getSheetName()).equalsIgnoreCase("TTD_HELP")){
					continue;
				}
				sheetName = sheet.getSheetName();
				logger.info("Processing sheet {}", sheetName);

				Row sheetNameRow = null;
				Cell sheetNameCell = null;
				sheetNameRow = sheet.getRow(SHEET_NAME_ROW_NUM);
				if(sheetNameRow == null){
					logger.error("SHEET NAME ROW not found. Invalid template");
					throw new TestDataException("Invalid Test Data Template - Page Name row was not found. Please use the latest test data template and try again");
				}

				sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);

				String locationName = getCellValue(sheetNameCell);
				if(locationName.equalsIgnoreCase("")){
					logger.error("LOCATION NAME TAMPERED WITH - Location Name is blank");
					throw new TestDataException("Invalid Test Data Template - Could not find Page Name in sheet [" + sheetName + "]. Please use the latest test data template and try again." );
				}
				location=getLocation(locationName);
				if(location == null){
					throw new TestDataException("Could not find information about Page " + locationName + ". Please ensure your test data template is valid.");
				}

				logger.debug("Getting detail records on page {}", locationName);

				Map<String, String> manualField=null;
				try {
					manualField = helper.getManualInput(appId,moduleCode,locationName,testCaseId,testStepId);
				} catch (DatabaseException e1) {
					

				}

				Row masterRow = null;
				Map<String, Row> tcRowMap = new TreeMap<String, Row>(); //Map that contains the Row Number as the key, and the detail row for the TDUID as the value

				logger.debug("Getting all rows with TDGID [{}] and TDUID [{}] on sheet [{}]", tdGid, tdUid, sheet.getSheetName());
				Iterator<Row> rowIter = sheet.iterator();
				String aTdDrId = "";
				while(rowIter.hasNext()){
					Row row = rowIter.next();

					if(getCellValue(row, TDGIDCOL).equalsIgnoreCase(tdGid) && getCellValue(row, TDUIDCOL).equalsIgnoreCase(tdUid)){
						aTdDrId = getCellValue(row, TDDRIDCOL);
						if(aTdDrId.equalsIgnoreCase("1") || aTdDrId.equalsIgnoreCase("")){
							masterRow = row;
						}

						tcRowMap.put(aTdDrId, row);
					}
				}
				Location loc = null;
				loc=location;
				int tFields = this.getTotalFieldsOnSheet(sheet, headerRow, dataStartCol);
				int masterStart=0;
				int dRow=0;
				JSONArray detailedRecords=null;
				if(masterRow!=null)
				{
					masterStart = masterRow.getRowNum();
					dRow = masterStart;
				}				
				if(masterRow == null){
					logger.error("Could not identify Master Record row for TDGID {}, TDUID {}", tdGid, tdUid);
					continue;
				}
				else{
					detailedRecords=this.getTestDataMultiLevelRecordBlock(sheet, tdGid, tdUid, dRow, appId, moduleCode, loc, tFields,manualField, apiOperation, entityType);
					if(detailedRecords!=null && detailedRecords.length()>0){
						page=location.toJson();
						JSONObject rec = detailedRecords.getJSONObject(0);
						page.put("DR_ID", rec.getString("DR_ID"));
						page.put("DETAILRECORDS", detailedRecords);
					}
				}
				if(detailedRecords!=null && detailedRecords.length()==0){
					page=location.toJson();
					page.put("DETAILRECORDS", new JSONArray());
					detailRecords.put(page);
				}
				else{
					if(page!=null && page.length()>0){
						detailRecords.put(page);
					}
				}
			} catch (Exception e) {
				logger.error("ERROR in scanSheetForData for page {}", sheet.getSheetName(), e );
				throw new TestDataException("An error occurred while scanning page " + sheet.getSheetName() + " for test data. Please contact Tenjin Support.");
			}
		}
		
		JSONObject data = new JSONObject();
		data.put("TDUID", tdUid);
		data.put("PAGEAREAS", detailRecords);
		return data;
	}

	private Location getLocation(String locationName) {
		Location t = new Location();
					t.setLocationName(locationName);
					t.setParent(locationName);
					t.setSequence(1);
					t.setWayIn(locationName);
					t.setWayOut(locationName);
				return t;
	}

	@SuppressWarnings("unused")
	private JSONObject scanTestData1(Workbook workbook,Sheet sheet1, String tdGid, String moduleCode, int appId, String tdUid, LearningHelper helper, Location location,Map<String,String> manualField, String apiOperation, String entityType) throws TestDataException, JSONException{
		JSONArray detailRecords = new JSONArray();
		JSONObject page = new JSONObject();
		this.locMap =  new HashMap<String, Boolean>();
		int headerRow = 4;
		int dataStartCol = 3;
		int numberOfSheets=workbook.getNumberOfSheets();
		for(int i=0;i<numberOfSheets;i++){
			Sheet sheet=null;
			String sheetName=null;
			try {
				sheet = workbook.getSheetAt(i);
				sheetName=sheet.getSheetName();
				if(Utilities.trim(sheet.getSheetName()).equalsIgnoreCase("PAGE_AREA_INDEX")){
					continue;
				}
				sheetName = sheet.getSheetName();
				logger.info("Processing sheet {}", sheetName);

				Row sheetNameRow = null;
				Cell sheetNameCell = null;
				sheetNameRow = sheet.getRow(SHEET_NAME_ROW_NUM);
				if(sheetNameRow == null){
					logger.error("SHEET NAME ROW not found. Invalid template");
					throw new TestDataException("Invalid Test Data Template - Page Name row was not found. Please use the latest test data template and try again");
				}

				sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);

				String locationName = getCellValue(sheetNameCell);
				if(locationName.equalsIgnoreCase("")){
					logger.error("LOCATION NAME TAMPERED WITH - Location Name is blank");
					throw new TestDataException("Invalid Test Data Template - Could not find Page Name in sheet [" + sheetName + "]. Please use the latest test data template and try again." );
				}
				location = helper.hydrateLocation(moduleCode, appId, locationName);

				if(location == null){
					logger.error("Invalid Location Name --> [{}]. No details in DB", locationName);
					throw new TestDataException("Could not find information about Page " + locationName + ". Please ensure your test data template is valid.");
				}

				logger.debug("Getting detail records on page {}", locationName);

				Row masterRow = null;
				Map<String, Row> tcRowMap = new TreeMap<String, Row>(); //Map that contains the Row Number as the key, and the detail row for the TDUID as the value

				logger.debug("Getting all rows with TDGID [{}] and TDUID [{}] on sheet [{}]", tdGid, tdUid, sheet.getSheetName());
				Iterator<Row> rowIter = sheet.iterator();
				String aTdDrId = "";
				if(!location.isMultiRecordBlock()){
					while(rowIter.hasNext()){
						Row row = rowIter.next();

						if(getCellValue(row, TDGIDCOL).equalsIgnoreCase(tdGid) && getCellValue(row, TDUIDCOL).equalsIgnoreCase(tdUid)){
							aTdDrId = getCellValue(row, TDDRIDCOL);
							if(aTdDrId.equalsIgnoreCase("1") || aTdDrId.equalsIgnoreCase("")){
								masterRow = row;
							}

							tcRowMap.put(aTdDrId, row);
						}
					}
				}
				else if(location.isMultiRecordBlock()){
					while(rowIter.hasNext()){
						Row row = rowIter.next();

						if(getCellValue(row, TDGIDCOL).equalsIgnoreCase(tdGid) && getCellValue(row, TDUIDCOL).equalsIgnoreCase(tdUid)){
							aTdDrId = getCellValue(row, TDDRIDCOL);
							if(aTdDrId.equalsIgnoreCase("1") || aTdDrId.equalsIgnoreCase("")){
								masterRow = row;
							}
							else if(aTdDrId.split("\\.").length>1){
								masterRow = row;
								System.out.println(location.getLocationName());
							}
							tcRowMap.put(aTdDrId, row);
						}
					}
				}
				Location loc = null;
				loc=location;
				int tFields = this.getTotalFieldsOnSheet(sheet, headerRow, dataStartCol);
				int masterStart=0;
				int dRow=0;
				if(masterRow!=null)
				{
					masterStart = masterRow.getRowNum();
					dRow = masterStart;
				}				
				if(masterRow == null){
					logger.error("Could not identify Master Record row for TDGID {}, TDUID {}", tdGid, tdUid);
					continue;
				}
				else{
					if(!location.isMultiRecordBlock()){
						JSONArray detailedRecords=this.getTestDataForSingleRecordBlock(sheet, masterRow, appId, moduleCode, helper, location, manualField);
						if(detailedRecords!=null && detailedRecords.length()>0){
							page=location.toJson();
							page.put("DETAILRECORDS", detailedRecords);
						}
					}
					else{
						JSONArray detailedRecords=this.getTestDataMultiLevelRecordBlock(sheet, tdGid, tdUid, dRow, appId, moduleCode, loc, tFields,manualField, apiOperation, entityType);
						if(detailedRecords!=null && detailedRecords.length()>0){
							page=location.toJson();
							page.put("DETAILRECORDS", detailedRecords);
						}
					}
				}
				if(page!=null && page.length()>0){
					detailRecords.put(page);
				}
			} catch (Exception e) {
				logger.error("ERROR in scanSheetForData for page {}", sheet.getSheetName(), e );
				throw new TestDataException("An error occurred while scanning page " + sheet.getSheetName() + " for test data. Please contact Tenjin Support.");
			}
		}
		
		JSONObject data = new JSONObject();
		data.put("TDUID", tdUid);
		data.put("PAGEAREAS", detailRecords);
		return data;
	}
	public JSONArray getTestDataForSingleRecordBlock(Sheet sheet,Row masterRow,int appId,String moduleCode,LearningHelper helper,Location location,Map<String,String> manualField) throws TestDataException, JSONException{
		logger.info("Processing Single SingleRecord Block");
		JSONArray fields = this.getTestDataFromRow(sheet, masterRow, appId, moduleCode, helper, location,manualField);
		JSONArray detailRecords=new JSONArray();
		if(fields!=null && fields.length()>0){
			String masterDrId = getCellValue(masterRow, TDDRIDCOL);
			JSONObject detailRecord = new JSONObject();
			detailRecord.put("DR_ID", masterDrId);
			detailRecord.put("FIELDS", fields);
			detailRecords.put(detailRecord);
		}
		return detailRecords;
	}
	@SuppressWarnings("unused")
	public JSONArray getTestDataMultiLevelRecordBlock(Sheet sheet,String tdGid,String tdUid,int dRow,int appId,String moduleCode,Location loc,int tFields,Map<String,String> manualField, String apiOperation, String entityType) throws Exception{
		this.locMap =  new HashMap<String, Boolean>();
		String pName=loc.getLocationName();
		JSONArray detailRecords=new JSONArray();		
		ArrayList<Row> detailRows = this.getDetailRowsForMaster(sheet, tdGid, tdUid, dRow);

		for(Row detailRow:detailRows){
			JSONObject detail = new JSONObject();
			String dRecordId = detailRow.getCell(2).getStringCellValue();
			detail.put("DR_ID", dRecordId);
			JSONArray fs = new JSONArray();
			fs=this.MultiRecordSingleLevel(loc, sheet, tdUid, tdGid, dRow, detailRow, tFields, moduleCode, appId, apiOperation, entityType);
			detail.put("FIELDS", fs);
			JSONArray detailRecord=this.MultiRecordMultiLevel(moduleCode, appId, loc, tdGid, tdUid, dRecordId, apiOperation, entityType);
			if(detailRecord!=null && detailRecord.length()>0){
				detail.put("DETAILRECORDS", detailRecord);	
			}
			if(detail!=null && detail.length()>0)
				detailRecords.put(detail);
		}
	
		return detailRecords;
	}
	public JSONArray MultiRecordSingleLevel(Location loc,Sheet sheet,String tdUid,String tdGid,int dRow,Row detailRow,int tFields,String moduleCode,int appId, String apiOperation, String entityType) throws Exception{
		int headerRow = 4;
		int dataStartCol = 3;
		String pName=loc.getLocationName();
		JSONArray fs=new JSONArray();		
		for(int i=dataStartCol;i<=dataStartCol+tFields;i++){
			Cell cell = detailRow.getCell(i);
			if(cell != null){

				cell.setCellType(Cell.CELL_TYPE_STRING);
				if(cell.getStringCellValue() != null && !cell.getStringCellValue().equalsIgnoreCase("")){
					String fieldName = sheet.getRow(headerRow).getCell(i).getStringCellValue();
					String fieldValue = cell.getStringCellValue();
					String val = "";
					try{
						val = this.resolve(fieldValue);
					}catch(Exception e){
						throw new TestDataException(e.getMessage(),e);
					}
					TestObject t=null;
					if(fieldName.equalsIgnoreCase("Button"))
					{
						t=new TestObject();
						t.setLabel("Button");
						t.setObjectClass("Button");
						t.setUniqueId(val);
						t.setIdentifiedBy("Button");
						t.setLovAvailable(null);
					}
					else{
						t=new TestObject();
						t.setLabel(fieldName);
						t.setObjectClass("VERIFY");
						t.setUniqueId(val);
						t.setIdentifiedBy("VERIFY");
						t.setLovAvailable(null);
					}
					if(t.getLabel() == null){
						throw new TestDataException("Information for field " + fieldName + " was not found in " + pName + " page in " + moduleCode + ". Re-learn this function or use the latest Test Data template");
					}
					JSONObject field = new JSONObject();
					field.put("DATA", val);
					field.put("FLD_LABEL", t.getLabel());
					field.put("FLD_TYPE",t.getObjectClass());
					field.put("FLD_UID", t.getUniqueId());
					field.put("FLD_UID_TYPE", t.getIdentifiedBy());
					String hasLov = "";
					if(t.getLovAvailable() == null){
						hasLov = "N";
					}else{
						hasLov = t.getLovAvailable();
					}
					field.put("FLD_HAS_LOV", hasLov);
					field.put("FLD_UNAME",t.getName());
					field.put("FLD_TAB_ORDER",t.getTabOrder());

					if(!loc.isMultiRecordBlock()){
						short bgColor = cell.getCellStyle().getFillForegroundColor();
						if(bgColor == IndexedColors.GREEN.getIndex()){
							field.put("ACTION", "QUERY");
						}else if(bgColor == IndexedColors.YELLOW.getIndex()){
							field.put("ACTION", "VERIFY");
						}else{
							field.put("ACTION", "INPUT");
						}
					}else{
						Cell hCell = sheet.getRow(headerRow).getCell(i);
						Short headerBgColor = hCell.getCellStyle().getFillForegroundColor();
						Short bgColor = cell.getCellStyle().getFillForegroundColor();
						if(bgColor == IndexedColors.LAVENDER.getIndex()){
							field.put("ACTION","ADDROW");
						}else if(bgColor == IndexedColors.GREY_40_PERCENT.getIndex()){
							field.put("ACTION", "DELETEROW");
						}else if(bgColor == IndexedColors.YELLOW.getIndex()){
							field.put("ACTION", "VERIFY");
						}else{
							if(headerBgColor == IndexedColors.LIGHT_ORANGE.getIndex()){
								field.put("ACTION", "QUERY");
							}else{
								field.put("ACTION", "INPUT");
							}
						}
					}
					fs.put(field);
				}
			}
		}
		return fs;
	}
	@SuppressWarnings("unused")
	public JSONArray MultiRecordMultiLevel(String moduleCode,int appId,Location loc,String tdGid,String tdUid,String dRecordId, String apiOperation, String entityType) throws Exception{
		ArrayList<Location> vicinityMrbs = null;
		ArrayList<Location> childMrbs = null;
		
		int headerRow = 4;
		int dataStartRow = 5;
		int dataStartCol = 3;
		JSONObject detail=new JSONObject();
		JSONArray detailRecords=new JSONArray();
		
		try{
			childMrbs = new LearningHelper().hydrateChildMultiRecordBlocks(this.conn, moduleCode,appId,"",loc.getLocationName() , apiOperation, entityType);
		}catch(Exception e){
			logger.error("ERROR hydrating Child record blocks of {}", loc.getLocationName());
			logger.error(e.getMessage(),e);
		}
			Multimap<String, ArrayList<Location>> childChildMrbLocations = ArrayListMultimap.create();
		  for(int i=0;i<childMrbs.size();i++){
			  ArrayList<Location> childChildMrbs = new LearningHelper().hydrateChildMultiRecordBlocks(this.conn, moduleCode,appId,"",childMrbs.get(i).getLocationName(), apiOperation, entityType);
			  childChildMrbLocations.put(childMrbs.get(i).getLocationName(), childChildMrbs);
		  }
		
		if(childMrbs!=null && childMrbs.size()>0){

			int numberOfSheets = 0;
			try{
				numberOfSheets = workbook.getNumberOfSheets();
			}catch(Exception e){
				logger.error("ERROR getting number of sheets");
				logger.error(e.getMessage(),e);
				throw new TestDataException(e.getMessage());
			}
			vicinityMrbs = new ArrayList<Location>();
			if(childMrbs != null && childMrbs.size() > 0){
				for(Location vMrb:childMrbs){
					vicinityMrbs.add(vMrb);
				}
			}
			ArrayList<Row> sDetailRows=null;
			JSONArray sDetails = new JSONArray();
			Map<String, Boolean> mrbLocMap = new HashMap<String, Boolean>();
			JSONObject page=new JSONObject(); 

			if(vicinityMrbs != null && vicinityMrbs.size() > 0){
				for(Location mrb:vicinityMrbs){
					JSONArray jsArr=new JSONArray();
					JSONArray eachrec=this.MultiLevelLevelMultiRecordBlocks(mrb, numberOfSheets, tdGid, tdUid, dRecordId, loc, moduleCode, appId, apiOperation, entityType);
					if(eachrec!=null && eachrec.length()>0){
						page=mrb.toJson();
						JSONObject rec = eachrec.getJSONObject(0);
						page.put("DR_ID", rec.getString("DR_ID"));
						page.put("FIELDS", rec.getJSONArray("FIELDS"));
					}	
					for(Entry<String, ArrayList<Location>> e : childChildMrbLocations.entries()) {
						String keyVal=e.getKey();
						ArrayList<Location> locations=e.getValue();
						if(mrb.getLocationName().equalsIgnoreCase(keyVal)){
							for(Location location:locations){
								JSONArray detailed=this.MultiLevelLevelMultiRecordBlocks(location,numberOfSheets,tdGid,tdUid,dRecordId,loc,moduleCode,appId, apiOperation, entityType);
								if(detailed!=null && detailed.length()>0){
									if(eachrec!=null && eachrec.length()>0){
										page.put("DETAILRECORDS", detailed);
									}
								}
							}
							break;
						}
					}
					if(page!=null && page.length()>0){
						detailRecords.put(page);
				}
			}
		  }	
		}

		return detailRecords;
	}
	
	@SuppressWarnings("unused")
	public JSONArray MultiLevelLevelMultiRecordBlocks(Location vicinityMrbs,int numberOfSheets,String tdGid,String tdUid,String dRecordId,Location loc,String moduleCode,int appId, String apiOperation, String entityType) throws Exception{
		
		String pagePrefix = "";
		if("request".equalsIgnoreCase(entityType)) {
			pagePrefix = "(REQ)_";
		} else if("response".equalsIgnoreCase(entityType)) {
			pagePrefix = "(RESP)_";
		}
		
		int headerRow = 4;
		int dataStartRow = 5;
		int dataStartCol = 3;
		ArrayList<Row> sDetailRows=null;	
		JSONArray sDetails = new JSONArray();
		JSONObject detail=new JSONObject();
		Map<String, Boolean> mrbLocMap = new HashMap<String, Boolean>();
		if(vicinityMrbs!=null){
			Location mrb=vicinityMrbs;
				logger.debug("Scanning Multi Record Block {} for more detail records for TDGID {}, TDUID {}, detail ID {}", mrb.getLocationName(), tdGid, tdUid, dRecordId);
				if(mrb.getLocationName().equalsIgnoreCase("Clearing Details")){
					System.err.println("Breakpoint");
				}
				XSSFSheet mrbSheet = null;
				String sName = "";
				for(int i=0;i<numberOfSheets;i++){
					try{
						XSSFSheet oSheet = (XSSFSheet) workbook.getSheetAt(i);
						Row sheetNameRow = oSheet.getRow(SHEET_NAME_ROW_NUM);
						if(sheetNameRow == null){
							logger.error("SHEET NAME ROW not found. Invalid template");
							throw new TestDataException("Invalid Test Data Template - Page Name row was not found. Please use the latest test data template and try again");
						}

						Cell sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);

						sName = getCellValue(sheetNameCell);
						if(Utilities.trim(sName).equalsIgnoreCase(mrb.getLocationName())){
							boolean sheetVisited = false;
							try{
								sheetVisited = mrbLocMap.get(sName);
							}catch(Exception e){

							}
							if(sheetVisited){
								logger.debug("This sheet has already been visited. Skipping");
								continue;
							}else{
								mrbSheet = oSheet;
								logger.info("Found sheet for {}", mrb.getLocationName());
								break;
							}

						}
					}catch(Exception e){
						logger.error("ERROR in finding sheet for {}", mrb.getLocationName());
						logger.error(e.getMessage(),e);
					}
				}
				
				if(!sName.toLowerCase().startsWith(pagePrefix.toLowerCase())) {
					logger.info("Skipping sheet [{}] as entity type is [{}]", sName, entityType);
					mrbSheet = null;
				}

				if(mrbSheet != null){
					sDetailRows = this.getSecondLevelDetailRowsForMaster(mrbSheet, tdGid, tdUid, dRecordId , dataStartRow);

					if(sDetailRows != null && sDetailRows.size() > 0){
						int totalFields = this.getTotalFieldsOnSheet(mrbSheet, headerRow, dataStartCol);

						for(Row sDetailRow:sDetailRows){
							JSONObject sDetail = new JSONObject();
							String drid = sDetailRow.getCell(2).getStringCellValue();
							sDetail.put("DR_ID", drid);
							JSONArray dfs = new JSONArray();
							for(int f=dataStartCol;f<=dataStartCol+totalFields;f++){
								Cell cell = sDetailRow.getCell(f);
								if(cell != null && cell.getStringCellValue() != null && !cell.getStringCellValue().equalsIgnoreCase("")){
									String fieldName = mrbSheet.getRow(headerRow).getCell(f).getStringCellValue();
									String fieldValue = cell.getStringCellValue();
									String val = "";
									try{
										val = this.resolve(fieldValue);
									}catch(Exception e){
										throw new TestDataException(e.getMessage(),e);
									}
									TestObject t=null;
									if(fieldName.equalsIgnoreCase("Button"))
									{
										t=new TestObject();
										t.setLabel("Button");
										t.setObjectClass("Button");
										t.setUniqueId(val);
										t.setIdentifiedBy("Button");
										t.setLocation(loc.getLocationName());
										t.setViewmode("N");
										t.setMandatory("NO");
										t.setLovAvailable("N");

									}
									else{
										t = new LearningHelper().hydrateTestObject(this.conn, moduleCode, sName, appId, fieldName, apiOperation, entityType);
									}
									if(t.getLabel() == null){
										throw new TestDataException("Information for field " + fieldName + " was not found in " + sName + " page in " + moduleCode + ". Re-learn this function or use the latest Test Data template");
									}
									JSONObject field = new JSONObject();
									field.put("DATA", val);
									field.put("FLD_LABEL", t.getLabel());
									field.put("FLD_TYPE",t.getObjectClass());
									field.put("FLD_UID", t.getUniqueId());
									field.put("FLD_UID_TYPE", t.getIdentifiedBy());
									String hasLov = "";
									if(t.getLovAvailable() == null){
										hasLov = "N";
									}else{
										hasLov = t.getLovAvailable();
									}
									field.put("FLD_HAS_LOV", hasLov);
									if(!loc.isMultiRecordBlock()){
										short bgColor = cell.getCellStyle().getFillForegroundColor();
										if(bgColor == IndexedColors.GREEN.getIndex()){
											field.put("ACTION", "QUERY");
										}else if(bgColor == IndexedColors.YELLOW.getIndex()){
											field.put("ACTION", "VERIFY");
										}else{
											field.put("ACTION", "INPUT");
										}
									}else{
										Cell hCell = mrbSheet.getRow(headerRow).getCell(f);
										Short headerBgColor = hCell.getCellStyle().getFillForegroundColor();
										Short bgColor = cell.getCellStyle().getFillForegroundColor();
										if(bgColor == IndexedColors.LAVENDER.getIndex()){
											field.put("ACTION","ADDROW");
										}else if(bgColor == IndexedColors.GREY_40_PERCENT.getIndex()){
											field.put("ACTION", "DELETEROW");
										}else if(bgColor == IndexedColors.YELLOW.getIndex()){
											field.put("ACTION", "VERIFY");
										}else{
											if(headerBgColor == IndexedColors.LIGHT_ORANGE.getIndex()){
												field.put("ACTION", "QUERY");
											}else{
												field.put("ACTION", "INPUT");
											}
										}
									}
									dfs.put(field);
								}
							}


							sDetail.put("FIELDS", dfs);
							if(dfs != null && dfs.length() > 0){
								sDetails.put(sDetail);	
								this.locMap.put(sName, true);
								mrbLocMap.put(sName, true);
							}

						}
						if(sDetails!=null && sDetails.length()>0){
						}
					}

				}
		}
		return sDetails;
	}

	
	public List<String> getDataExtractionInputRecordIDs(String fullFilePath, int appId, String functionCode) throws TestDataException {
		
		Workbook workbook = null;
		try {
			workbook = WorkbookFactory.create(new FileInputStream(fullFilePath));
		} catch (FileNotFoundException e) {
			
			logger.error(e.getMessage(), e);
			throw new TestDataException("Could not read input file due to an internal error. Please contact Tenjin Support.");
		} catch (IOException e) {
			
			logger.error(e.getMessage(), e);
			throw new TestDataException("Could not read input file due to an internal error. Please contact Tenjin Support.");
		} catch (InvalidFormatException e) {
			
			logger.error(e.getMessage(), e);
			throw new TestDataException("Could not read input file due to an internal error. Please contact Tenjin Support.");
		}
		
		String sheetName = "";
		Sheet sheet = workbook.getSheetAt(0);
		sheetName = sheet.getSheetName();
		
		logger.info("Sheet at position 0 is --> {}", sheetName);
		
		int tdGidCol = 0;
		int tdUidCol = TDUIDCOL;
		int dataStartCol = DATA_START_COL;
		int dataStartRow = DATA_START_ROW_NUM;
		int headerRowNum = HEADER_ROW_NUM;
				
		int[] rowIDsToScan = this.getPhysicalRowIDs(sheet, dataStartRow);
		List<String> extractionData = new ArrayList<String>();
		Map<Integer, String> headerMap = this.getRowData(sheet, headerRowNum, dataStartCol);
		List<Integer> queryHeaders = new ArrayList<Integer>();
		for(int i=0;i<rowIDsToScan.length;i++){
			logger.debug("Scanning row {}", rowIDsToScan[i]);
			Row row = sheet.getRow(rowIDsToScan[i]);
			Cell tdGidCell = row.getCell(tdGidCol);
			Cell tdUidCell = row.getCell(tdUidCol);
			String tdGid = "";
			String tdUid = "";
			if(tdGidCell != null){
				tdGidCell.setCellType(Cell.CELL_TYPE_STRING);
				tdGid = tdGidCell.getStringCellValue();
			}
			
			if(tdUidCell != null){
				tdUidCell.setCellType(Cell.CELL_TYPE_STRING);
				tdUid = tdUidCell.getStringCellValue();
			}
			
			if(tdUid == null || Utilities.trim(tdUid).equalsIgnoreCase("")){
				logger.warn("TDUID not input for row {}. This row will be skipped", row.getRowNum());
				continue;
			}
			
			Map<Integer, Cell> cellMap = this.getPhysicalCellsInRow(sheet, row.getRowNum(), dataStartCol);
			String queryFields = "";
			for(Integer colIndex:cellMap.keySet()){
				Cell cell = cellMap.get(colIndex);
				
				short bgColor = cell.getCellStyle().getFillForegroundColor();
				if(bgColor == QUERY_FIELD_COLOR){
					queryFields = queryFields + cell.getStringCellValue() + ";;";
					if(!queryHeaders.contains(colIndex)){
						queryHeaders.add(colIndex);
					}
				}
			}
			
			extractionData.add(row.getRowNum() + ";;" + tdGid + ";;" + tdUid + ";;" + queryFields);
			
		}
		
		String extractionHeader = "";
		extractionHeader = headerRowNum + ";;TDGID;;TDUID;;";
		for(Integer colIndex:queryHeaders){
			extractionHeader += headerMap.get(colIndex) + ";;";
		}
		
		extractionData.add(0, extractionHeader);
		return extractionData;
	}
	
	private Map<Integer, Cell> getPhysicalCellsInRow(Sheet sheet, int rowNumber, int scanStartColumn) throws TestDataException{
		Map<Integer, Cell> map = new TreeMap<Integer, Cell>();
		try{
			Row row = sheet.getRow(rowNumber);
			if(row == null){
				return map;
			}
			Iterator<Cell> cellIter = row.cellIterator();
			while(cellIter.hasNext()){
				Cell cell = cellIter.next();
				if(cell.getColumnIndex() >= scanStartColumn){
					cell.setCellType(Cell.CELL_TYPE_STRING);
					map.put(cell.getColumnIndex(), cell);
				}
				
				
			}
			
			return map;
		}catch(Exception e){
			logger.error("ERROR getting cell data in row {}", rowNumber,e);
			throw new TestDataException("Could not get cell data for row " + rowNumber);
		}
	}
	
	@SuppressWarnings("rawtypes")
	private int[] getPhysicalRowIDs(Sheet sheet, int startRow) throws TestDataException{
		try{
			Iterator rowIter = sheet.rowIterator();
			
			int totalPhysicalRows = 0;
			while(rowIter.hasNext()){
				if(((Row)rowIter.next()).getRowNum() >= startRow){
					totalPhysicalRows++;
				}
			}
			
			int[] physicalRowIDs = new int[totalPhysicalRows];
			
			rowIter = sheet.rowIterator();
			int i = -1;
			while(rowIter.hasNext()){
				Row row = (Row)rowIter.next();
				if(row.getRowNum() >= startRow){
					i++;
					physicalRowIDs[i] = row.getRowNum();
				}
			}
			
			return physicalRowIDs;
		}catch(Exception e){
			logger.error("Could not find physical row count in sheet",e);
			throw new TestDataException("Could not get number of rows in sheet");
		}
	}
	
	private Map<Integer, String> getRowData(Sheet sheet, int rowNumber, int scanStartColumn) throws TestDataException{
		Map<Integer, String> map = new TreeMap<Integer, String>();
		try{
			Row row = sheet.getRow(rowNumber);
			Iterator<Cell> cellIter = row.cellIterator();
			while(cellIter.hasNext()){
				Cell cell = cellIter.next();
				if(cell.getColumnIndex() >= scanStartColumn){
					cell.setCellType(Cell.CELL_TYPE_STRING);
					map.put(cell.getColumnIndex(), cell.getStringCellValue());
				}
				
				
			}
			
			return map;
		}catch(Exception e){
			logger.error("ERROR getting cell data in row {}", rowNumber,e);
			throw new TestDataException("Could not get cell data for row " + rowNumber);
		}
	}

	
	@SuppressWarnings("unused")
	public JSONObject getDataExtractionFields(String fullFilePath, int appId, String functionCode, String tdUid, int rowNumber) throws TestDataException {
		
		JSONObject json = null;
		
		Workbook workbook = null;
		int tdGidCol = TDGIDCOL;
		int tdUidCol = TDUIDCOL;
		int dataStartCol = DATA_START_COL;
		int dataStartRow = DATA_START_ROW_NUM;
		int headerRowNum = HEADER_ROW_NUM;
		
		try {
			workbook = WorkbookFactory.create(new FileInputStream(fullFilePath));
			this.setWorkbook(workbook);
		} catch (InvalidFormatException e) {
			logger.error(e.getMessage(),e);
			throw new TestDataException("Could not get fields to extract due to an internal error");
		} catch (FileNotFoundException e) {
			
			logger.error(e.getMessage(),e);
			throw new TestDataException("Could not load input excel file. Please try again");
		} catch (IOException e) {
			
			logger.error(e.getMessage(),e);
			throw new TestDataException("Could not get fields to extract due to an internal error");
		}
		
		LearningHelper lHelper = new LearningHelper();
		
		JSONObject locationsJson = null;
		try {
			logger.info("Loading metadata for screens in {} in app {}", functionCode, appId);
			Map<String, Map<String, Object>> metadata = lHelper.hydrateScreenMetadataAsMap(appId, functionCode);
			this.locationsMap = metadata.get("PAGEAREAS");
			this.fieldsMap = metadata.get("FIELDS");
			locationsJson = lHelper.hydrateLocationHierarchyAsJSON(functionCode, appId);
			System.err.println(locationsJson);
			logger.info("Metadata loading --> Done");
		} catch (DatabaseException e1) {
			
			logger.error("Unable to load screen metadata for {} in app {} due to an exception", functionCode, appId, e1);
			throw new TestDataException("Could not load metadata for function " + functionCode + " due to an internal error");
		}
		
		try{
			json = new JSONObject();
			json.put("PA_NAME", locationsJson.getString("PA_NAME"));
			json.put("PA_TYPE", locationsJson.getString("PA_TYPE"));
			json.put("PA_WAY_IN", locationsJson.getString("PA_WAY_IN"));
			json.put("PA_WAY_OUT", locationsJson.getString("PA_WAY_OUT"));
			json.put("PA_PARENT", locationsJson.getString("PA_PARENT"));
			if(locationsJson.getString("PA_SCREEN_TITLE") != null){
				json.put("PA_SCREEN_TITLE", locationsJson.getString("PA_SCREEN_TITLE"));
			}
			
			json.put("PA_IS_MRB", locationsJson.getString("PA_IS_MRB"));
			json.put("CHILDREN", locationsJson.getJSONArray("CHILDREN"));
			Sheet sheet = this.getSheetByName(workbook, json.getString("PA_NAME"));
			
			if(sheet == null){
				logger.error("ERROR while getting extraction data --> Sheet for MAIN page {} was not found in Excel Workbook", json.getString("PA_NAME"));
				throw new TestDataException("The test data sheet you uploaded is invalid. Sheet for MAIN page " + json.getString("PA_NAME") + " was not found");
			}
			

			Map<Integer, String> headerMap = this.getRowData(sheet, headerRowNum, dataStartCol);
			Map<Integer, Cell> dataRow = this.getPhysicalCellsInRow(sheet, rowNumber, dataStartCol);
			
			Row row = sheet.getRow(rowNumber);
			Cell tdUidCell = row.getCell(tdUidCol);
			String aTdUid = "";
			if(tdUidCell != null){
				aTdUid = tdUidCell.getStringCellValue();
			}
			if(aTdUid == null){
				aTdUid = "";
			}
			
			if(!aTdUid.equalsIgnoreCase(tdUid)){
				logger.error("Invalid TDUID found in row {} of input sheet. Expected {}, found {}", rowNumber, tdUid, aTdUid);
				throw new TestDataException("Invalid TDUID found in row " + rowNumber + " of input sheet. Expected " + tdUid + ", found " + aTdUid);
			}
			
			JSONArray fields = new JSONArray();
			
			for(Integer colIndex:headerMap.keySet()){
				String fieldLabel = headerMap.get(colIndex);
				String fieldAction = "";
				String dataVal = "";
				TestObject t = (TestObject) this.fieldsMap.get(fieldLabel);
				Cell cell = dataRow.get(colIndex);
				if(cell != null){
					dataVal = cell.getStringCellValue();
					short bgColor = cell.getCellStyle().getFillForegroundColor();
					if(bgColor == QUERY_FIELD_COLOR){
						fieldAction = "QUERY";
					}
				}else{
					fieldAction = "EXTRACT";
				}
				JSONObject field = new JSONObject();
				if(fieldLabel.equalsIgnoreCase("Button")){
					field.put("FLD_TYPE","Button");
					field.put("FLD_UID", "Button");
					field.put("FLD_UID_TYPE", "Button");
				}
				else{
					field.put("FLD_TYPE",t.getObjectClass());
					field.put("FLD_UID", t.getUniqueId());
					field.put("FLD_UID_TYPE", t.getIdentifiedBy());
				}
				field.put("DATA", dataVal);
				field.put("FLD_LABEL", fieldLabel);
				
				String hasLov = "";
				if(fieldLabel.equalsIgnoreCase("Button")){
					hasLov = "N";
				}
				else{
					if(t.getLovAvailable() == null){
						hasLov = "N";
					}else{
						hasLov = t.getLovAvailable();
					}
				}
				field.put("FLD_HAS_LOV", hasLov);
				field.put("ACTION",fieldAction);
				
				fields.put(field);
			}
			
			if(!json.getString("PA_IS_MRB").equalsIgnoreCase("YES")){
				json.put("FIELDS", fields);
			}else{
				JSONObject detailRecords = new JSONObject();
				detailRecords.put("FIELDS", fields);
				json.put("DETAILRECORDS", detailRecords);
			}
			
			
			JSONArray children = this.getChildPageJSONArray(json, tdUid);
			json.put("CHILDREN", children);
			
		}catch(JSONException e){
			logger.error("JSON error while scanning workbook for fields",e);
			throw new TestDataException("Could not process data sheet due to an internal error");
		}
		
		return json;
	}
	
	@SuppressWarnings("unused")
	private JSONArray getChildPageJSONArray(JSONObject page, String tdUid){
		JSONArray cJson = new JSONArray();
		
		int tdGidCol = TDGIDCOL;
		int tdUidCol = TDUIDCOL;
		int dataStartCol = DATA_START_COL;
		int dataStartRow = DATA_START_ROW_NUM;
		int headerRowNum = HEADER_ROW_NUM;
		
		try{
			JSONArray children= page.getJSONArray("CHILDREN");
			if(children != null){
				for(int i=0;i<children.length();i++){
					JSONObject child = children.getJSONObject(i);
					JSONObject json = new JSONObject();
					json.put("PA_NAME", child.getString("PA_NAME"));
					json.put("PA_TYPE", child.getString("PA_TYPE"));
					json.put("PA_WAY_IN", child.getString("PA_WAY_IN"));
					json.put("PA_WAY_OUT", child.getString("PA_WAY_OUT"));
					json.put("PA_PARENT", child.getString("PA_PARENT"));
					try{
						json.put("PA_SCREEN_TITLE",child.getString("PA_SCREEN_TITLE"));
					}catch(Exception e){}
					
					json.put("PA_IS_MRB", child.getString("PA_IS_MRB"));
					json.put("CHILDREN", child.getJSONArray("CHILDREN"));
					
					Sheet sheet = this.getSheetByName(this.getWorkbook(), json.getString("PA_NAME"));
					
					if(sheet == null){
						logger.warn("Sheet with name {} was not found in the excel workbook. This sheet will be skipped");
						continue;
					}
					
					int[] rowIDsToScan = this.getPhysicalRowIDs(sheet, dataStartRow);
					boolean includeSheet = false;
					int rowNumber= 0;
					for(int j=0; j<rowIDsToScan.length; j++){
						Row row = sheet.getRow(rowIDsToScan[j]);
						Cell tdGidCell = row.getCell(tdGidCol);
						Cell tdUidCell = row.getCell(tdUidCol);
						String tdGid = "";
						String aTdUid = "";
						
						if(tdUidCell != null){
							tdUidCell.setCellType(Cell.CELL_TYPE_STRING);
							aTdUid = tdUidCell.getStringCellValue();
						}
						
						if(tdUid != null && tdUid.equalsIgnoreCase(aTdUid)){
							includeSheet = true;
							rowNumber = rowIDsToScan[j];
							break;
						}
					}
					
					if(includeSheet){
						Map<Integer, String> headerMap = this.getRowData(sheet, headerRowNum, dataStartCol);
						Map<Integer, Cell> dataRow = this.getPhysicalCellsInRow(sheet, rowNumber, dataStartCol);
						
						JSONArray fields = new JSONArray();
						
						for(Integer colIndex:headerMap.keySet()){
							String fieldLabel = headerMap.get(colIndex);
							String fieldAction = "";
							String dataVal = "";
							TestObject t = (TestObject) this.fieldsMap.get(fieldLabel);
							Cell cell = dataRow.get(colIndex);
							if(cell != null){
								dataVal = cell.getStringCellValue();
								short bgColor = cell.getCellStyle().getFillForegroundColor();
								if(bgColor == QUERY_FIELD_COLOR){
									fieldAction = "QUERY";
								}
							}else{
								fieldAction = "EXTRACT";
							}
							
							JSONObject field = new JSONObject();
							field.put("DATA", dataVal);
							field.put("FLD_LABEL", fieldLabel);
							field.put("FLD_TYPE",t.getObjectClass());
							field.put("FLD_UID", t.getUniqueId());
							field.put("FLD_UID_TYPE", t.getIdentifiedBy());
							String hasLov = "";
							if(t.getLovAvailable() == null){
								hasLov = "N";
							}else{
								hasLov = t.getLovAvailable();
							}
							field.put("FLD_HAS_LOV", hasLov);
							field.put("ACTION",fieldAction);
							
							fields.put(field);
						}
						
						if(!json.getString("PA_IS_MRB").equalsIgnoreCase("YES")){
							json.put("FIELDS", fields);
						}else{
							JSONObject detailRecords = new JSONObject();
							detailRecords.put("FIELDS", fields);
							json.put("DETAILRECORDS", detailRecords);
						}
					}
					
					JSONArray cChildren = getChildPageJSONArray(child, tdUid);
					json.put("CHILDREN", cChildren);
					
					if(includeSheet){
						cJson.put(json);
					}else if(cChildren != null && cChildren.length()> 0){
						cJson.put(json);
					}
					
					

					
				}
			}
		}catch(JSONException e){
			logger.error("ERROR generating JSON for child pages");
		} catch (TestDataException e) {
			
			logger.error("Error ", e);
		}
		
		return cJson;
	}
	
	private Sheet getSheetByName(Workbook workbook, String sheetName) throws TestDataException{
		Sheet sheet = null;
		Row sheetNameRow = null;
		Cell sheetNameCell = null;
		String locationName = "";
		for(int i=0;i<workbook.getNumberOfSheets();i++){
			sheet = workbook.getSheetAt(i);
			sheetNameRow = sheet.getRow(SHEET_NAME_ROW_NUM);
			if(sheetNameRow == null){
				logger.error("SHEET NAME ROW not found. Invalid template");
				throw new TestDataException("Invalid Test Data Template - Page Name row was not found. Please use the latest test data template and try again");
			}
			
			sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);
			
			locationName = getCellValue(sheetNameCell);
			
			if(Utilities.trim(locationName).equalsIgnoreCase(sheetName)){
				return sheet;
			}
		}
		
		return sheet;
	}
	
	
	public String generateExtractionOutput(int runId, int appId, Module function, String[] tdUids, boolean useFreshTemplate) throws TestDataException, TenjinConfigurationException{
		String outputFilePath = "";
		String EXTR_OUTPUT_PATH = "EXTR_OUTPUT";
		String workPath = TenjinConfiguration.getProperty("TJN_WORK_PATH");
		File dir = new File(workPath + "\\" + EXTR_OUTPUT_PATH);
		if(!dir.exists()){
			dir.mkdir();
		}
		workPath = workPath + "\\" + EXTR_OUTPUT_PATH;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String outputFileName = sdf.format(new Date());
		outputFilePath = workPath + "\\" + outputFileName;
		if(tdUids == null || tdUids.length < 1){
			logger.error("No TDUIDs found to generate output for");
			throw new TestDataException("Could not generate output. No TDUID specified.");
		}
		
		if(!useFreshTemplate){
			if(!Utilities.trim(function.getExtractorInputFile()).equalsIgnoreCase("")){
				logger.info("Loading Template From [{}]", function.getExtractorInputFile());
				File sFile = new File( function.getExtractorInputFile());
				try {
					logger.info("Copying uploaded file to output directory");
					FileUtils.copyFile(sFile, new File(outputFilePath));
				} catch (IOException e) {
					logger.error("Unable to copy to output dir", e);
					throw new TestDataException("An internal error occurred while writing to output file. Please contact Tenjin Support");
				}
				
			}else{
				throw new TestDataException("The Uploaded spreadsheet could not be found. It seems the file was either moved or deleted. Please run the extractor again.");
			}
		}else{
			logger.info("Generating a fresh template and using it");
			this.generateTemplate(appId, function.getCode(), outputFilePath, outputFileName, false, false);
		}
		
		try {
			this.workbook = WorkbookFactory.create(new File(outputFilePath));
		} catch (InvalidFormatException e) {
			logger.error(e.getMessage(),e);
			throw new TestDataException("An internal error occurred while generating extraction output");
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
			throw new TestDataException("An internal error occurred while generating extraction output");
		}
		
		for(String tdUid:tdUids){
			JSONObject data = null;
			if(function.getLastSuccessfulExtractionStatus() != null && function.getLastSuccessfulExtractionStatus().getRecords() != null){
				for(ExtractionRecord record:function.getLastSuccessfulExtractionStatus().getRecords()){
					if(record.getTdUid().equalsIgnoreCase(tdUid)){
						try {
							data = new JSONObject(record.getData());
						} catch (JSONException e) {
							
							logger.error("ERROR --> Invalid JSON format for the extracted data for TDUID [{}]", tdUid);
							logger.error("DATA found --> {}", record.getData());
							logger.error("This TDUID will be skipped");
						}
						
						break;
					}
				}
			}
			
			if(data == null){
				logger.error("Could not get Extracted data for record [{}]. This record will be skipped", tdUid);
				continue;
			}
			
			try {
				logger.info("Entering extractor data for [{}]", tdUid);
				this.enterExtractionDataInSameWorkbook(tdUid, data);
			} catch (Exception e) {
				
				logger.error("ERROR entering extraction data for TDUID [{}]", tdUid, e);
			}
		}
		
		OutputStream os;
		String oldFileName = outputFileName;
		outputFileName = function.getCode() + "_Extracted_" + runId + ".xlsx";
		outputFilePath = workPath + "\\" + outputFileName;
		try {
			os = new FileOutputStream(outputFilePath);
			workbook.write(os);
		} catch (FileNotFoundException e) {
			
			logger.error("ERROR writing excel to file",e);
			throw new TestDataException("An internal error occurred while writing to output file. Please contact Tenjin Support");
		} catch (IOException e) {
			
			logger.error("ERROR writing excel to file",e);
			throw new TestDataException("An internal error occurred while writing to output file. Please contact Tenjin Support");
		}
		
		logger.info("Cleaning up");
		File file = new File(workPath + "\\" + oldFileName);
		if(file.exists()){
			System.out.println(file.delete());
		}
		
		return outputFileName;
	}
	
	
	@SuppressWarnings("unused")
	private void enterExtractionDataInSameWorkbook(String tdUid, JSONObject data) throws TestDataException{
		int tdGidCol = TDGIDCOL;
		int tdUidCol = TDUIDCOL;
		int dataStartCol = DATA_START_COL;
		int dataStartRow = DATA_START_ROW_NUM;
		int headerRowNum = HEADER_ROW_NUM;
		int tdDrIdCol = TDDRIDCOL;
		
		try {
			String pageName = data.getString("PA_NAME");
			Sheet sheet = this.getSheetByName(this.workbook, pageName);
			
			if(sheet == null){
				throw new TestDataException("Could not find sheet named [" + pageName + "]. The uploaded sheet may have been tampered with. Please run the extractor again.");
			}
			
			String isMrb = data.getString("PA_IS_MRB");
			if(isMrb != null && isMrb.equalsIgnoreCase("NO")){
				int rowNumber =  this.checkIfValueExists(sheet, dataStartRow, tdUidCol, tdUid);
				if(rowNumber >= dataStartRow){
					Map<Integer, String> headerMap = this.getRowData(sheet, headerRowNum, dataStartCol); // All headers
					Map<String, Integer> transposedMap = this.transposeMap(headerMap);
					
					JSONArray fields = data.getJSONArray("FIELDS");
					for(int i=0; i<fields.length(); i++){
						JSONObject field = fields.getJSONObject(i);
						
						String label= field.getString("FLD_LABEL");
						String dataVal = field.getString("DATA");
						
						int targetColIndex = transposedMap.get(label);
						Cell targetCell = sheet.getRow(rowNumber).getCell(targetColIndex, Row.CREATE_NULL_AS_BLANK);
						targetCell.setCellValue(dataVal);
					}
				}else{
					//TDUID is not present in this sheet. so skip it.
				}
			}else{
				int rowNumber = this.checkIfValueExists(sheet, dataStartRow, tdUidCol, tdUid);
				if(rowNumber >= dataStartRow){
					Map<Integer, String> headerMap = this.getRowData(sheet, headerRowNum, dataStartCol); // All headers
					Map<String, Integer> transposedMap = this.transposeMap(headerMap);
					JSONArray detailRecords = data.getJSONArray("DETAILRECORDSEXT");
					for(int i=0; i<detailRecords.length(); i++){
						JSONObject detailRecord = detailRecords.getJSONObject(i);
						String drId = detailRecord.getString("DR_ID");
						Cell tdDrIdCell = sheet.getRow(rowNumber).getCell(tdDrIdCol, Row.CREATE_NULL_AS_BLANK);
						tdDrIdCell.setCellValue(drId);
						JSONArray fields = detailRecord.getJSONArray("FIELDS");
						for(int f=0; f<fields.length(); f++){
							JSONObject field = fields.getJSONObject(f);
							
							String label= field.getString("FLD_LABEL");
							String dataVal = field.getString("DATA");
							
							int targetColIndex = transposedMap.get(label);
							Cell targetCell = sheet.getRow(rowNumber).getCell(targetColIndex, Row.CREATE_NULL_AS_BLANK);
							targetCell.setCellValue(dataVal);
						}
						
						sheet.shiftRows(rowNumber+1, sheet.getLastRowNum(),1);
						rowNumber++;
					}
				}else{
				}
			}
			
			JSONArray children = data.getJSONArray("CHILDREN");
			if(children != null){
				for(int i=0; i<children.length(); i++){
					enterExtractionDataInSameWorkbook(tdUid, children.getJSONObject(i));
				}
			}
			
			
		} catch (JSONException e) {
			
			logger.error("JSONException caught --> " + e.getMessage());
			throw new TestDataException("Could not generate output due to an internal error. Please contact Tenjin Support.");
		}
		
	}
	
	@SuppressWarnings("unused")
	private int checkIfValueExists(Sheet sheet, int searchStartRow, int colId, String valueToSearch) throws TestDataException{
		int[] rowIDsToScan = this.getPhysicalRowIDs(sheet, searchStartRow);
		boolean valueExists = false;
		int rowNumber= -1;
		for(int j=0; j<rowIDsToScan.length; j++){
			Row row = sheet.getRow(rowIDsToScan[j]);
			//Cell tdGidCell = row.getCell(tdGidCol);
			Cell targetCell = row.getCell(colId);
			String aValue = "";
			
			if(targetCell != null){
				targetCell.setCellType(Cell.CELL_TYPE_STRING);
				aValue = targetCell.getStringCellValue();
			}
			
			if(valueToSearch != null && valueToSearch.equalsIgnoreCase(aValue)){
				valueExists = true;
				rowNumber = rowIDsToScan[j];
				break;
			}
		}
		
		return rowNumber;
	}
	
	private Map<String, Integer> transposeMap(Map<Integer, String> mapToTranspose){
		Map<String, Integer> map = new TreeMap<String, Integer>();
		
		for(Integer i:mapToTranspose.keySet()){
			map.put(mapToTranspose.get(i), i);
		}
		
		return map;
	}

	

	public JSONObject loadTestDataFromTemplate(String absoluteFilePath, int appId, String functionCode, String tdGid, String tdUid, String apiOperation, String entityType,int responseType) throws TestDataException {	
		JSONObject data = null;

		try{
			logger.info("Loading workbook from path [{}]", absoluteFilePath);
			this.workbook = WorkbookFactory.create(new File(absoluteFilePath));
		}catch(Exception e){
			
			logger.error(e.getMessage());
			throw new TestDataException("Could not load test data file. Please contact Tenjin Support.");
		}

		LearningHelper helper = new LearningHelper();
		int numberOfSheets = workbook.getNumberOfSheets();
		logger.info("Loading Test Data");
		logger.info("Number of sheets --> [{}]", numberOfSheets);

		JSONArray pageAreas = this.scanTestData(workbook, tdGid, functionCode, appId, tdUid, helper, apiOperation, entityType,responseType);

		try {
			data = new JSONObject();
			data.put("PAGEAREAS", pageAreas);
			data.put("TDUID", tdUid);
			data.put("RESOURCE_PARAMS", this.loadResourceParametersForRequest(workbook, tdGid, tdUid));
		} catch (JSONException e) {
			logger.error("ERROR while generating final JSONObject", e);
		}

		return data;
	}

	public JSONArray scanTestData(Workbook workbook, String tdGid, String moduleCode, int appId, String tdUid, LearningHelper helper, String apiOperation, String entityType,int responseType) throws TestDataException{	
		Location mainLocation;
		try {
			mainLocation = helper.hydrateLocationHierarchy(appId, moduleCode, apiOperation, entityType,responseType);
		} catch (DatabaseException e) {
			logger.error(e.getMessage(),e);
			throw new TestDataException("An internal error occurred while scanning Test data for this step. Please contact Tenjin Support.", e);
		} 
		
		if(mainLocation == null) {
			logger.warn("No Locations to scan for {}", entityType);
			return new JSONArray();
		}
		
		this.loadSheetNames(workbook, entityType,responseType);
		JSONArray mainJson = this.recursivelyScanPageAreaForResponseData(workbook, mainLocation, tdGid, tdUid, "");
		return mainJson;
	}

	private void loadSheetNames(Workbook workbook, String entityType,int responseType) {
		this.sheetNames = new HashMap<String, String> ();
		String pagePrefix = "";
		if("request".equalsIgnoreCase(entityType)) {
			pagePrefix = "(REQ)_";
		} else if("response".equalsIgnoreCase(entityType)){
			pagePrefix = "(RESP_"+responseType+")_";
		}
		for(int i=0; i<workbook.getNumberOfSheets(); i++) {
			Sheet sheet = workbook.getSheetAt(i);
			String sheetName=sheet.getSheetName();
			if(Utilities.trim(sheet.getSheetName()).equalsIgnoreCase("TTD_HELP")){
				continue;
			}else if(!Utilities.trim(sheet.getSheetName()).toLowerCase().startsWith(pagePrefix.toLowerCase())){
				logger.info("Skipping page [{}] as entity type is [{}]", sheetName, entityType);
				continue;
			}
			Row sheetNameRow = sheet.getRow(SHEET_NAME_ROW_NUM);
			if(sheetNameRow == null){
				logger.error("SHEET NAME ROW not found on sheet {}. Invalid template", sheet.getSheetName());
			}

			Cell sheetNameCell = sheetNameRow.getCell(SHEET_NAME_CELL_NUM, Row.CREATE_NULL_AS_BLANK);

			String locationName = getCellValue(sheetNameCell);
			logger.debug("Loading sheet Name {} to memory for page area {}", sheet.getSheetName(), locationName);
			this.sheetNames.put(locationName, sheet.getSheetName());
		}
	}

	private JSONArray loadResourceParametersForRequest(Workbook workbook, String tdGid, String tdUid) throws TestDataException {

		try {
			Sheet rParamSheet = workbook.getSheet("(REQ)_Resource Parameters");
			if(rParamSheet == null) {
				logger.info("No resource params found for this TDUID");
				return new JSONArray();
			}

			Map<String, Row> detailRecordRows = this.getRowsForTdUid(rParamSheet, tdGid, tdUid, "Resource Parameters", false, "");


			if(detailRecordRows.size() < 1) {
				logger.debug("No detail records found on page [{}]: Sheet [{}]", "Resource Paramters", rParamSheet);
				return new JSONArray();
			}

			Row headerRow = rParamSheet.getRow(HEADER_ROW_NUM);
			if(headerRow == null) {
				logger.error("ERROR - No header row found on sheet {}", rParamSheet.getSheetName());
				throw new TestDataException("No Header Row was found on sheet " + rParamSheet.getSheetName() + ". Please verify if you are using the correct Test Data Template.");
			}

			int dataStartCol = 3;
			JSONArray detailRecordsArray = new JSONArray();
			for(String tdDrId : detailRecordRows.keySet()) {
				Row detailRecordRow = detailRecordRows.get(tdDrId);
				JSONObject detailRecordJson = new JSONObject();
				JSONArray fieldsJsonArray = new JSONArray();
				Cell detailRecordIdCell = detailRecordRow.getCell(TDDRIDCOL, Row.CREATE_NULL_AS_BLANK);
				String aTdDrId = getCellValue(detailRecordIdCell);
				if(Utilities.trim(aTdDrId).length() < 1 ) {
					aTdDrId = "1";
				}
				Iterator<Cell> headerRowIter = headerRow.cellIterator();
				while(headerRowIter.hasNext()) {
					Cell headerCell = headerRowIter.next();
					if(headerCell.getColumnIndex() < dataStartCol) {
						continue;
					}

					String fieldName = getCellValue(headerCell);
					Cell valueCell = detailRecordRow.getCell(headerCell.getColumnIndex(), Row.CREATE_NULL_AS_BLANK);
					String fieldValue = getCellValue(valueCell);

					if(Utilities.trim(fieldValue).length() < 1){
						continue;
					}

					fieldValue = this.resolve(fieldValue);

					String action = "INPUT";



					JSONObject fieldJson = new JSONObject();
					fieldJson.put("FLD_LABEL", fieldName);
					fieldJson.put("DATA", fieldValue);
					fieldJson.put("ACTION", action);
					fieldsJsonArray.put(fieldJson);

					logger.info("Page: [{}], Field: [{}], Data: [{}], Action: [{}]","Resource Parameters", fieldName, fieldValue, action);
				}

				detailRecordJson.put("DR_ID", aTdDrId);
				detailRecordJson.put("FIELDS", fieldsJsonArray);


				detailRecordsArray.put(detailRecordJson);


			}
			return detailRecordsArray;

		}catch(Exception e) {
			logger.error("ERROR occurred while scanning for resource parameters", e);
			throw new TestDataException("Could not process Resource parameters from test data due to an internal error. Please contact Tenjin Support.");
		}
	}
	
	private Sheet getSheetByLocationName(Workbook workbook, String locationName) {
		if(this.sheetNames == null) {
			logger.error("ERROR - sheetNames map is not loaded.");
			return null;
		}
		logger.debug("Locating sheet for Page Area {}", locationName);
		String targetSheetName = this.sheetNames.get(locationName);
		if(Utilities.trim(targetSheetName).length() < 1) {
			logger.error("ERROR - No matching sheet found for page area {}", locationName);
			return null;
		}else{
			Sheet sheet = workbook.getSheet(targetSheetName);
			if(sheet == null) {
				logger.error("ERROR - No matching sheet found for page area {}", locationName);
				return null;
			}
			
			logger.debug("Target Sheet [{}] found for Page Area {}", sheet.getSheetName(), locationName);
			return sheet;
		}
	}
	
	private Map<String, TestObject> loadTestObjectMap(List<TestObject> testObjects) {
		Map<String, TestObject> map = new TreeMap<String, TestObject>();
		if(testObjects != null && testObjects.size() > 0) {
			for(TestObject t : testObjects) {
				map.put(t.getName(), t);
			}
		}
		
		return map;
	}
	
	private Map<String, Row> getRowsForTdUid(Sheet sheet, String tdGid, String tdUid, String pageAreaName, boolean isMrb, String parentDetailId) throws TestDataException {
		logger.debug("Getting all rows with TDGID [{}] and TDUID [{}] on sheet [{}]", tdGid, tdUid, sheet.getSheetName());
		if(pageAreaName.equalsIgnoreCase("balances") || pageAreaName.equalsIgnoreCase("data")) {
			System.err.println();
		}
		String sParentDetailId = Utilities.trim(parentDetailId).length()< 1 ? "1" : Utilities.trim(parentDetailId); //Temp variable to treat blank TDDRID ("") as "1" in case of Single Record Blocks
		try {
			Map<String, Row> tcRowMap = new TreeMap<String, Row>(); //Map that contains the Row Number as the key, and the detail row for the TDUID as the value
			Iterator<Row> rowIter = sheet.iterator();
			String aTdDrId = "";
			while(rowIter.hasNext()){
				Row row = rowIter.next();

				
				if(getCellValue(row, TDGIDCOL).equalsIgnoreCase(tdGid) && getCellValue(row, TDUIDCOL).equalsIgnoreCase(tdUid)){
					String detailIdForThisRow = getCellValue(row, TDDRIDCOL);
					detailIdForThisRow = Utilities.trim(detailIdForThisRow).length() < 1 ? "1" : Utilities.trim(detailIdForThisRow);
					if(!isMrb && detailIdForThisRow.equalsIgnoreCase(sParentDetailId)) {
						aTdDrId = getCellValue(row, TDDRIDCOL);
						tcRowMap.put(aTdDrId, row);
					}else if(isMrb) {
						if(Utilities.trim(parentDetailId).length() < 1 || (Utilities.trim(parentDetailId).length()>0 && detailIdForThisRow.startsWith(parentDetailId + "."))){
							aTdDrId = getCellValue(row, TDDRIDCOL);
							tcRowMap.put(aTdDrId, row);
						}
					}
				}
			}
			
			return tcRowMap;
		} catch (Exception e) {
			logger.error("ERROR occurred while scanning for Master Records for page area [{}]", pageAreaName, e);
			throw new TestDataException("An unexpected error occurred while scanning Test Data sheet for Page [" + pageAreaName + "]. Please contact Tenjin Support.");
		}
	}

	private JSONArray recursivelyScanPageAreaForResponseData(Workbook workbook, Location location, String tdGid, String tdUid, String parentDetailId) throws TestDataException {
		try {
			JSONArray pageJsonArray = new JSONArray();
			JSONObject pageJson = new JSONObject();
			int dataStartCol = 3;

			pageJson = location.toJson();

			Sheet sheet = this.getSheetByLocationName(workbook, location.getLocationName());

			if(sheet == null) {
				logger.warn("Sheet for Page Area [{}] was not found. Scanning if children of this page have any data", location.getLocationName());
				if(location.getChildLocations() != null) {
					for(Location childLocation : location.getChildLocations()) {
						JSONArray pArray = this.recursivelyScanPageAreaForResponseData(workbook, childLocation, tdGid, tdUid, parentDetailId);
						if(pArray != null && pArray.length() > 0) {
							/*pageJsonArray.put(pArray);*/
							for(int i=0; i<pArray.length(); i++) {
								pageJsonArray.put(pArray.getJSONObject(i));
							}
						}
					}
				}

				return pageJsonArray;
			}


			Map<String, TestObject> testObjectMap = this.loadTestObjectMap(location.getTestObjects());
			if(sheet != null) {
				Map<String, Row> detailRecordRows = this.getRowsForTdUid(sheet, tdGid, tdUid, location.getLocationName(), location.isMultiRecordBlock(), parentDetailId);


				if(detailRecordRows.size() < 1) {
					logger.debug("No detail records found on page [{}]: Sheet [{}]", location.getLocationName(), sheet.getSheetName());
					return new JSONArray();
				}

				Row headerRow = sheet.getRow(HEADER_ROW_NUM);
				if(headerRow == null) {
					logger.error("ERROR - No header row found on sheet {}", sheet.getSheetName());
					throw new TestDataException("No Header Row was found on sheet " + sheet.getSheetName() + ". Please verify if you are using the correct Test Data Template.");
				}


				JSONArray detailRecordsArray = new JSONArray();
				for(String tdDrId : detailRecordRows.keySet()) {
					Row detailRecordRow = detailRecordRows.get(tdDrId);
					JSONObject detailRecordJson = new JSONObject();
					JSONArray fieldsJsonArray = new JSONArray();
					Cell detailRecordIdCell = detailRecordRow.getCell(TDDRIDCOL, Row.CREATE_NULL_AS_BLANK);
					String aTdDrId = getCellValue(detailRecordIdCell);
					if(Utilities.trim(aTdDrId).length() < 1 ) {
						aTdDrId = "1";
					}
					Iterator<Cell> headerRowIter = headerRow.cellIterator();
					while(headerRowIter.hasNext()) {
						Cell headerCell = headerRowIter.next();
						if(headerCell.getColumnIndex() < dataStartCol) {
							continue;
						}

						String fieldName = getCellValue(headerCell);
						Cell valueCell = detailRecordRow.getCell(headerCell.getColumnIndex(), Row.CREATE_NULL_AS_BLANK);
						String fieldValue = getCellValue(valueCell);

						if(Utilities.trim(fieldValue).length() < 1){
							continue;
						}

						fieldValue = this.resolve(fieldValue);

						short bgColor = valueCell.getCellStyle().getFillBackgroundColor();
						String action = "VERIFY";
						if(bgColor == QUERY_FIELD_COLOR) {
							action = "QUERY";
						}

						TestObject t = testObjectMap.get(fieldName);
						if(t == null) {
							logger.error("ERROR - No matching test object found for page [{}], field [{}]", location.getLocationName(), fieldName);
							throw new TestDataException("Metadata does not exist for Field [" + fieldName + "] on page [" + location.getLocationName() + "]. You might need to re-learn this API / Function and use a more recent Test Data Template.");
						}

						JSONObject fieldJson = t.toJson();
						fieldJson.put("FLD_LABEL", t.getLabel());
						fieldJson.put("DATA", fieldValue);
						fieldJson.put("ACTION", action);
						fieldsJsonArray.put(fieldJson);

						logger.info("Page: [{}], Field: [{}], Data: [{}], Action: [{}]", location.getLocationName(), fieldName, fieldValue, action);
					}

					detailRecordJson.put("DR_ID", aTdDrId);
					detailRecordJson.put("FIELDS", fieldsJsonArray);
					JSONArray childLocationJsonArray = new JSONArray();
					if(location.getChildLocations() != null) {
						for(Location childLocation : location.getChildLocations()) {
							JSONArray cArray = this.recursivelyScanPageAreaForResponseData(workbook, childLocation, tdGid, tdUid, aTdDrId);
							if(cArray != null && cArray.length() > 0) {
								for(int i=0; i<cArray.length(); i++) {
									childLocationJsonArray.put(cArray.getJSONObject(i));
								}
							}
						}
					}

					detailRecordJson.put("CHILDREN", childLocationJsonArray);

					detailRecordsArray.put(detailRecordJson);
				}

				pageJson.put("DETAILRECORDS", detailRecordsArray);
				pageJsonArray.put(pageJson);
			}


			return pageJsonArray;
		} catch (JSONException e) {
			logger.error(e.getMessage(),e);
			throw new TestDataException("An internal error occurred while scanning Test Data for this step. Please contact Tenjin Support.");
		}
	}

	@Override
	public String generateMessageTemplate(String folderPath, String fileName,
			Map<Location, ArrayList<String>> learntMap) throws TestDataException {
				try {
					XSSFWorkbook workbook;
					workbook = new XSSFWorkbook(this.getClass().getClassLoader().getResourceAsStream("TTD_Template.xlsx"));

					CellStyle hStyle = getHeaderRowStyle(workbook);
					CellStyle verifyCellStyle = getVerifyCellStyle(workbook);

					Set<Location> pageArea = learntMap.keySet();

					for (Location location : pageArea) {
						ArrayList<String> fieldTags = learntMap.get(location);
						if (fieldTags.size() > 0) {
							XSSFSheet sheet = (XSSFSheet) workbook.createSheet(location.getLocationName());
							int count = 3;
							try {
								Row sheetNameRow = sheet.createRow(0);
								sheetNameRow.createCell(0).setCellValue("Page Name:");
								sheetNameRow.getCell(0).setCellStyle(getPageNameCellStyle(workbook));
								sheetNameRow.createCell(1).setCellValue(location.getLocationName());
								sheetNameRow.getCell(1).setCellStyle(getPageNameMergedCellStyle(workbook));
								CellRangeAddress region = new CellRangeAddress(0,0,1,6);
								sheet.addMergedRegion(region);
								RegionUtil.setBorderBottom(CellStyle.BORDER_THICK, region, sheet, workbook);
								RegionUtil.setBorderTop(CellStyle.BORDER_THICK, region, sheet, workbook);
								RegionUtil.setBorderLeft(CellStyle.BORDER_THICK, region, sheet, workbook);
								RegionUtil.setBorderRight(CellStyle.BORDER_THICK, region, sheet, workbook);
								
								
								sheetNameRow.createCell(7).setCellValue("Do not change this value. It may result in errors during execution");
								sheetNameRow.getCell(7).setCellStyle(getPageNameSubscriptCellStyle(workbook));
								region = new CellRangeAddress(0,0,7,11);
								sheet.addMergedRegion(region);
								
								
								if(location.getPath()!=null && !(location.getPath().isEmpty())){
									Row pathRow = sheet.createRow(1);
									pathRow.createCell(0).setCellValue("Path");
									pathRow.createCell(1).setCellValue(location.getPath());
								}
								Row legendRow = null;
								Row headerRow = null;
								legendRow = sheet.createRow(2);
								legendRow.createCell(0).setCellValue("Legend");
								legendRow.createCell(1).setCellValue("INPUT");
								
								legendRow.createCell(2).setCellValue("VERIFY");
								legendRow.getCell(2).setCellStyle(verifyCellStyle);
								
								headerRow = sheet.createRow(4);
								headerRow.createCell(0).setCellValue("TDGID");
								headerRow.getCell(0).setCellStyle(hStyle);
								headerRow.createCell(1).setCellValue("TDUID");
								headerRow.getCell(1).setCellStyle(hStyle);
								headerRow.createCell(2).setCellValue("TDDRID");
								headerRow.getCell(2).setCellStyle(hStyle);
								
								for (String field : fieldTags) {
									headerRow.createCell(count).setCellValue(field);
									headerRow.getCell(count).setCellStyle(hStyle);
									count++;
								}
							} catch (Exception e) {
								e.getMessage();
							}
						}
					}
					String endFilePath = folderPath+ File.separator+ fileName+"_TTD.xlsx";
					FileOutputStream file = new FileOutputStream(new File(endFilePath));
					workbook.write(file);
				}catch(Exception e){
					
				}
				return fileName+"_TTD.xlsx";
			}

	

	

}