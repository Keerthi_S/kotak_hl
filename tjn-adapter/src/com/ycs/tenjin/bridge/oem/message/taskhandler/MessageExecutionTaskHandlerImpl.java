/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  MessageExecutionTaskHandlerImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright  � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                  CHANGED BY              DESCRIPTION
* 15-06-2021			Ashiki					Newly added for TENJINCG-1275
*/

package com.ycs.tenjin.bridge.oem.message.taskhandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.BridgeProcess;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.TestDataException;
import com.ycs.tenjin.bridge.oem.TaskHandler;
import com.ycs.tenjin.bridge.oem.message.MessageValidateApplication;
import com.ycs.tenjin.bridge.oem.message.MessageValidateApplicationFactory;
import com.ycs.tenjin.bridge.oem.message.datahandler.MessageValidateTestDataHandlerImpl;
import com.ycs.tenjin.bridge.pojo.ExecutorProgress;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.MessageValidate;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.StepIterationResult;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.db.IterationHelper;
import com.ycs.tenjin.db.MessageHelper;
import com.ycs.tenjin.db.RunDefectHelper;
import com.ycs.tenjin.defect.RunDefectProcessor;
import com.ycs.tenjin.util.Utilities;

public class MessageExecutionTaskHandlerImpl implements TaskHandler {
	private int runId;
	private Map<String, Map<String, Aut>> autMap;
	private List<ExecutionStep> steps;
	private Map<String, String> testDataPaths;
	private Map<Integer, List<String>> tdUidMap;
	private ExecutorProgress progress;
	private int completedTransactions;
	private int totalTransactions;
	
	private static final Logger logger = LoggerFactory.getLogger(MessageExecutionTaskHandlerImpl.class);
	
	private int projectId;
	private String userId;
	private long startTimeinMillis;
	private Map<Integer,String> passedStepsStatusMap;
	
	public MessageExecutionTaskHandlerImpl(int runId, Map<String, Map<String, Aut>> autMap, List<ExecutionStep> steps, Map<Integer, List<String>> tdUidMap,
			Map<String, String> testDataPaths, int projectId, String userId,Map<Integer,String> passedStepsStatusMap) {
		super();
		this.runId = runId;
		this.autMap = autMap;
		this.steps = steps;
		this.tdUidMap = tdUidMap;
		this.testDataPaths = testDataPaths;
		this.projectId = projectId;
		this.userId = userId;
		this.progress = new ExecutorProgress();
		this.completedTransactions = 0;
		this.passedStepsStatusMap=passedStepsStatusMap;
	}
	
	private void updateProgress(String tdUid, String status, String message, String resultValidationStatus, String stage) {
		
		
		if("end".equalsIgnoreCase(stage)) {
			this.completedTransactions++;
			this.progress.setCompletedTransactions(this.completedTransactions);
		}
		
		Map<String, String> txnMap = new HashMap<String, String>();
		txnMap.put("status", status);
		txnMap.put("message", message);
		txnMap.put("resultValidationStatus", resultValidationStatus);
		
		this.progress.getTransactionsMap().put(tdUid, txnMap);
		
		double percentage = ((double) this.completedTransactions / (double) this.totalTransactions) * 100;
		double roundOff = Math.round(percentage * 100.0) / 100.0;
		int rWithoutDecimals = (int) roundOff;
		this.progress.setPercentage(rWithoutDecimals);
		
		String elapsedTime= Utilities.calculateElapsedTime(this.startTimeinMillis, System.currentTimeMillis());
		this.progress.setElapsedTime(elapsedTime);
		
		if(rWithoutDecimals >= 100){
			this.progress.setStatus(BridgeProcess.COMPLETE);
		}else{
			this.progress.setStatus(BridgeProcess.IN_PROGRESS);
		}
		
		CacheUtils.putObjectInRunCache(this.runId, this.progress);
	}
	
	private void updateProgress(String tdUid, String status, String message, String resultValidationStatus, boolean hasRuntimeValues, String stage) {
				
		if("end".equalsIgnoreCase(stage)) {
			this.completedTransactions++;
			this.progress.setCompletedTransactions(this.completedTransactions);
		}
		
		Map<String, String> txnMap = new HashMap<String, String>();
		txnMap.put("status", status);
		txnMap.put("message", message);
		txnMap.put("resultValidationStatus", resultValidationStatus);
		if(hasRuntimeValues) {
			txnMap.put("runtimevalues", "yes");
		}else{
			txnMap.put("runtimevalues", "no");
		}
		
		this.progress.getTransactionsMap().put(tdUid, txnMap);
		
		double percentage = ((double) this.completedTransactions / (double) this.totalTransactions) * 100;
		double roundOff = Math.round(percentage * 100.0) / 100.0;
		int rWithoutDecimals = (int) roundOff;
		this.progress.setPercentage(rWithoutDecimals);
		
		String elapsedTime= Utilities.calculateElapsedTime(this.startTimeinMillis, System.currentTimeMillis());
		this.progress.setElapsedTime(elapsedTime);
		
		if(rWithoutDecimals >= 100){
			this.progress.setStatus(BridgeProcess.COMPLETE);
		}else{
			this.progress.setStatus(BridgeProcess.IN_PROGRESS);
		}
		
		CacheUtils.putObjectInRunCache(this.runId, this.progress);
	}
	
	@SuppressWarnings({  "unused" })
	public void execute() {
		Map<Integer,String> execStatusMap=new HashMap<Integer,String>();
		List<String> execStatusList=new ArrayList<String>();
		logger.info("Beginning Execution of Tenjin Run [{}]", this.runId);
		this.startTimeinMillis = System.currentTimeMillis();
		if(this.tdUidMap != null) {
			for(Integer stepRecordId : this.tdUidMap.keySet()) {
				List<String> tdUids = this.tdUidMap.get(stepRecordId);
				if(tdUids != null ) {
					for (String tdUid:tdUids) {
						this.totalTransactions++;
					}
				}
			}
		}
		
		logger.info("Total Transactions in this run --> [{}]", this.totalTransactions);
		this.progress.setTotalTransactions(this.totalTransactions);
		
		IterationHelper helper = new IterationHelper();
		MessageHelper msgHelper = new MessageHelper();
		
		if(this.steps == null){
			logger.error("ERROR --> Steps is null. Nothing to execute. Execution is terminated");
			return;
		}
		
		logger.info("Updating Run Start");
		try{
			helper.beginRun(this.runId);
		}catch(Exception ignore){}
		
		for(ExecutionStep step:this.steps){
			logger.info("Begin execution of step --> [{}], Record ID [{}]", step.getId(), step.getRecordId());
			logger.info("Loading application information for step [{}]", step.getId());
			Map<String, Aut> autMap = this.autMap.get(step.getAppName());
			
			Aut aut = autMap.get(step.getAutLoginType());
			
			step.setAut(aut);
			
			logger.info("Loading Information about API [{}], in application [{}]", step.getApiCode(), aut.getName());
			MessageValidate msgV = null;
			String operation = "";
			try {
				msgV = msgHelper.hydrateMsg(aut.getId(), step.getFunctionCode());
				step.setMsgV(msgV);
				operation = "Verify";
				step.setOperation(operation);
			} catch (DatabaseException e) {
				logger.error("ERROR loading MSG Information", e);
				try {
					logger.info("Setting results for all transactions in Step [{}] - Record ID[{}] to E", step.getId(), step.getRecordId());
					helper.updateRunStatusForAllTransactions(this.runId, step.getRecordId(), "E", "Could not load information about MSG " + step.getMsgVCode() + ".");
					execStatusList.add("Error");
				} catch (DatabaseException e1) {
					
					logger.error(e1.getMessage());
				}
			}
			
			if(msgV == null) {
				}
			
			logger.info("Loading executor for [{}]", msgV.getType());
			
			
			logger.info("Initializing API Adapter for API [{}]", msgV.getCode());
			MessageValidateApplication bridge = null;
			
			try {
				bridge = MessageValidateApplicationFactory.getExecutor(msgV.getType(), this.runId, step);
			} catch (BridgeException e) {
				logger.error("Executor invocation failed for step [{}] - Record ID [{}]", step.getId(), step.getRecordId());
				try {
					logger.info("Setting results for all transactions in Step [{}] - Record ID[{}] to E", step.getId(), step.getRecordId());
					helper.updateRunStatusForAllTransactions(this.runId, step.getRecordId(), "E", e.getMessage());
					execStatusList.add("Error");
				} catch (DatabaseException e1) {
					logger.error(e1.getMessage());
				}
				continue;
			}
			
			List<String> tdUids = this.tdUidMap.get(step.getRecordId());
			int txnLimit = step.getTransactionLimit();
			
			MessageValidateTestDataHandlerImpl handler = new MessageValidateTestDataHandlerImpl();
			boolean dependencyCheck=false;
			String previousStepStatus=null;
			if(step.getDependencyRule()!=null && !step.getDependencyRule().equalsIgnoreCase("-1")&& step.getDependentStepId()!=0 && !Utilities.trim(step.getDependencyRule()).equalsIgnoreCase("")){
				 previousStepStatus=execStatusMap.get(step.getDependentStepId());
				 if(previousStepStatus==null && this.passedStepsStatusMap!=null){
					 previousStepStatus=this.passedStepsStatusMap.get(step.getDependentStepId());
				 }
				if(previousStepStatus!=null && !previousStepStatus.equalsIgnoreCase(step.getDependencyRule())){
					dependencyCheck=true;
				}
			}
			for(int i=0; i<txnLimit; i++){
				int iteration = i+1;
				Date startTime = new Date();
				logger.info("Beginning Execution of Test Case [{}], Test Step [{}], Iteration [{}]", step.getTestCaseId(), step.getId(), iteration);
				String tdUid = tdUids.get(i);
				logger.info("TDUID for this transaction --> [{}]", tdUid);

				logger.debug("Setting status of Transaction [{}] to X", tdUid);
				try {
					helper.updateRunStatus(this.runId, step.getRecordId(), iteration, "X",startTime);
				} catch (DatabaseException e) {}
				
				this.updateProgress(tdUid, "X", "", "N/A","start");

				String folderPath = this.testDataPaths.get(step.getFunctionCode()+step.getAppId());
				JSONObject iterationData;

				logger.info("Loading test data for step [REQUEST]");

				try {
					iterationData = handler.loadTestDataFromTemplate(folderPath, aut.getId(), step.getApiCode(), step.getTdGid(), tdUid,step.getTestCaseRecordId(),step.getId(), step.getOperation(), "request");
				} catch (TestDataException e) {
					
					logger.error("Could not load test data",e );
					try {
						Date endTime = new Date();
						helper.updateRunStatus(this.runId, step.getRecordId(), iteration, "E", e.getMessage(),endTime);
					execStatusList.add("Error");
					} catch (DatabaseException ignore) {}
					this.updateProgress(tdUid, "E", e.getMessage(), "N/A", "end");
					execStatusList.add("Error");
					continue;
				}
				IterationStatus iStatus = null;

				try {
					logger.info("Executing Iteration...");
					iStatus = bridge.executeIteration(iterationData, iteration);
				}  catch (Exception e) {
					logger.error("ERROR - Uncaught exception in executeIteration", e);
					try {
						Date endTime = new Date();
						helper.updateRunStatus(this.runId, step.getRecordId(), iteration, "E", e.getMessage(),endTime);
					execStatusList.add("Error");
						
					} catch (DatabaseException ignore) {}
					this.updateProgress(tdUid, "E", "Execution aborted abnormally due to an internal error. Please contact Tenjin Support.", "N/A", "end");
					continue;
				}

				if(iStatus != null){
					if(iStatus.getValidationResults()!=null && iStatus.getValidationResults().size()>0)
					{
						boolean isVerifyFailed=false;
						for(ValidationResult validationResult:iStatus.getValidationResults())
						{
							if(validationResult.getRunId()==this.runId)
							{
								if(!validationResult.getStatus().equals("Pass"))
								{
									isVerifyFailed=true;
									break;
								}
							}
						}
						if(isVerifyFailed)
						{ 
							iStatus.setResult("F");
							iStatus.setMessage("Execution Completed");
						}
						else {

							iStatus.setResult("S");
							iStatus.setMessage("Execution Completed");
						}
					}
				}
					try {
						logger.info("Updating Results...");
						String result = iStatus.getResult();
						if(Utilities.trim(result).equalsIgnoreCase("")){

							this.updateProgress(tdUid, "E", "Could not get result.", "N/A", "end");
							
						}
						else{
						
						Date endTime = new Date();
						helper.updateRunStatus(this.runId, step.getRecordId(), iteration, iStatus.getResult(), iStatus.getMessage(),endTime);
						 result = iStatus.getResult();
						if(result.equalsIgnoreCase("E")){
							execStatusList.add("Error");
		                }else if(result.equalsIgnoreCase("F")){
		                	execStatusList.add("Fail");
		                }else{
		                	execStatusList.add("Pass");                 
		                }
						logger.info("Persisting validation resulsts...");
						helper.persistValidationResults(iStatus.getValidationResults(), this.runId, step.getRecordId(), iteration);
						logger.info("Persisting Run-Time Values...");
						logger.info("Persisting Run-Time Screenshots...");
						}
						} catch (DatabaseException e) {}
					boolean hasRuntimeValues = false;
					if(iStatus.getRuntimeFieldValues() != null && iStatus.getRuntimeFieldValues().size() > 0) {
						hasRuntimeValues = true;
					}
				String	resultValidationStatus="";
					this.updateProgress(tdUid, iStatus.getResult(), iStatus.getMessage(), resultValidationStatus, hasRuntimeValues,"end");

				} 
			}
			RunDefectHelper rHelper = new RunDefectHelper();
		RunDefectProcessor rProcessor = new RunDefectProcessor();
		try {
			List<String> uniqueMessages = rHelper.getDistinctTransactionFailureMessages(this.runId);
			
			Map<String, List<StepIterationResult>> transactionFailures = new HashMap<String, List<StepIterationResult>>();
			
			for(String message:uniqueMessages){
				List<StepIterationResult> txnsFailedWithMessage = rHelper.getTransactionsFailedWithMessage(this.runId, message);
				transactionFailures.put(message, txnsFailedWithMessage);
			}
			
			logger.info("Creating defects for Transaction Failures");
			rProcessor.createTransactionFailureDefects(this.runId,  this.projectId, this.userId, transactionFailures);
			
			logger.info("Getting validation failures");
			List<StepIterationResult> validationFailures = rHelper.getTransactionsWithValidationFailures(this.runId);
			if(validationFailures != null && validationFailures.size() > 0){
				rProcessor.createValidationFailureDefects(this.runId, this.projectId, this.userId, validationFailures);
			}
			
		} catch (DatabaseException e) {
			
			logger.error("ERROR while generating defects for run [{}]", this.runId,e);
		}
		
		logger.info("Updating Run End");
		try{
			helper.endRun(this.runId, BridgeProcess.COMPLETE);
		}catch(Exception ignore){}
		
		logger.info("Execution Task Completed");
	}
	
	
}
