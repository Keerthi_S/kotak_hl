/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  AssistedLearningHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 	CHANGED BY              DESCRIPTION
* 26-09-2018			Preeti					TENJINCG-742
* 04-10-2018			Preeti					TENJINCG-822
* 01-03-2019			Preeti					TENJINCG-998
*/
package com.ycs.tenjin.handler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ycs.tenjin.bridge.pojo.aut.AssistedLearningRecord;
import com.ycs.tenjin.db.AssistedLearningHelper;
import com.ycs.tenjin.db.DatabaseException;
import com.ycs.tenjin.util.Utilities;

public class AssistedLearningHandler {
	private static final Logger logger = LoggerFactory.getLogger(AssistedLearningHandler.class);
	private AssistedLearningHelper helper= new AssistedLearningHelper();
	
	public List<AssistedLearningRecord> getAssistedLearningRecords(int appId, String functionCode) throws DatabaseException {
		return this.helper.hydrateAssistedLearningRecords(appId, functionCode);
	}
	
	public void persistAssistedLearningRecords(String functionCode,int appId, String userId, List<AssistedLearningRecord> record) throws Exception {
		List<AssistedLearningRecord> oldRecords = this.helper.hydrateAssistedLearningRecords(appId, functionCode);
		this.validateRecords(record,oldRecords);
		Gson gson = new Gson();
	    String oldRecordJson = gson.toJson(oldRecords);	   
		this.deleteAllRecords(functionCode,appId);
		List<AssistedLearningRecord> newRecords = this.helper.persistAssistedLearningRecords(record);
		String newRecordJson = gson.toJson(newRecords);
		this.helper.persistAssistedLearningAuditTrail(appId,functionCode,userId,oldRecordJson,newRecordJson);
	}
	
	public void deleteAllRecords(String functionCode, int appId) throws DatabaseException {
		this.helper.deleteAllAssistedLearningRecords(functionCode,appId);
	}
	
	public void validateRecords(List<AssistedLearningRecord> records,List<AssistedLearningRecord> oldRecords) throws Exception {
		if(records.size()==0 && oldRecords.size()==0){
			logger.error("ERROR - No record");
			throw new Exception("No assisted learning data available to save.");
		}
		for(AssistedLearningRecord record : records){
			if(Utilities.trim(record.getPageAreaName()).length() < 1) {
				
				logger.error("ERROR - Page area value is blank");
				throw new Exception("Please enter Page value for sequence number "+record.getSequence());
			}
			
			if(Utilities.trim(record.getPageAreaName()).length() > 100) {
				logger.error("ERROR - Max length");
				throw new Exception("Page value should not exceed its max length 100 for sequence number "+record.getSequence());
			}
			if(Utilities.trim(record.getFieldName()).length() >100) {
				logger.error("ERROR - Max length");
				throw new Exception("Field value should not exceed its max length 100 for sequence number "+record.getSequence());
			}
			if(Utilities.trim(record.getIdentifiedBy()).length() >50) {
				logger.error("ERROR - Max length");
				throw new Exception("Identified By value should not exceed its max length 50 for sequence number "+record.getSequence());
			}
			if(Utilities.trim(record.getData()).length() >256) {
				logger.error("ERROR - Max length");
				throw new Exception("Data value should not exceed its max length 256 for sequence number "+record.getSequence());
			}
		}
	}
}
