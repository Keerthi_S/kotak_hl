package com.ycs.tenjin.adapter.selenium.kotakdiy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.ApplicationUnresponsiveException;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.DriverException;
import com.ycs.tenjin.bridge.oem.gui.GuiApplication;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.selenium.asv2.SeleniumApplicationImpl;

public class ApplicationImpl  extends SeleniumApplicationImpl implements GuiApplication {

	public ApplicationImpl(Aut aut, String clientIp, int port, String browser) {
		super(aut, clientIp, port, browser);
		// TODO Auto-generated constructor stub
	}
	private final Logger logger = LoggerFactory.getLogger(ApplicationImpl.class);
	
	@Override
	public void login(String LoginName, String Password)
			throws AutException, ApplicationUnresponsiveException, DriverException {
		if (this.loginName.equalsIgnoreCase("Login")) {
			return;
		}
			logger.info("Login skipped");
		
	}

	@Override
	public void navigateToFunction(String Code, String Name, String MenuContainer)
			throws AutException, BridgeException, ApplicationUnresponsiveException, DriverException {
		try {
			this.driverContext.getDriver().findElement(By.xpath(".//*[text()='"+MenuContainer+"']//following::button[1]")).click();
		} catch (Exception e) {
			throw new AutException("Could not Navigate to the menu->"+ MenuContainer);
		}
		
	}

	@Override
	public void logout() throws AutException, DriverException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void recover() throws AutException, DriverException {
		// TODO Auto-generated method stub
		WebDriverWait wait = new WebDriverWait(this.driverContext.getDriver(),3);
		boolean flag= true;
		while(flag) {
			try {
				WebElement b=wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//a[text()='BACK']")));
				b.click();
				this.explicitWait(2);
			} catch (Exception e) {
				flag=false;
			}
		}

		
		
	}
	
	public String getCompositeXpath() {
		return ".//input[@type='text' or @type='Tel' or @type='tel' or @type='Text' or @type='TEXT' or @type='number']|.//textarea|.//input[@type='password' or @type='Password' or @type='PASSWORD' or @type='passWord' or @type='PassWord']|.//select |.//input[@type='radio' or @type='Radio' or @type='RADIO']|.//input[@type='Checkbox' or @type='CHECKBOX' or @type='checkbox' or @type='checkBox'] | .//input[@type='file'] | .//input[starts-with(@id,'otp-')] ";
	}

}