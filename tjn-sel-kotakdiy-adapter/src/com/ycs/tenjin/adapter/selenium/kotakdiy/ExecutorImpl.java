package com.ycs.tenjin.adapter.selenium.kotakdiy;

import org.json.JSONArray;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.CaptureScreenshot;

import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.RuntimeScreenshot;
import com.ycs.tenjin.bridge.selenium.asv2.taskhandler.task.ExecutorTaskAbstract;

public class ExecutorImpl extends ExecutorTaskAbstract {

	public ExecutorImpl(int runId, ExecutionStep step, int screenshotOption, int fieldTimeout) {
		super(runId, step, screenshotOption, fieldTimeout);
		// TODO Auto-generated constructor stub
	}
	GenericAdapterMethods genericAdapterMethods;

	@Override
	public void initialize() throws ExecutorException {
	this.genericAdapterMethods = new GenericAdapterMethods(this.driverContext, this.webGenericMethods);
	this.commonMethods = new CommonMethodImpl(this.driverContext, this.webGenericMethods);
	this.executionMethods = new ExecutionMethodImpl(this.driverContext,	this.webGenericMethods);}

	@Override
	public void preExecution() throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postExecution() throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preProcessAllPageAreas() throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postProcessAllPageAreas() throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prePageExecution(Location location) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postPageExecution(Location location) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preSingleRecordPageAreaExecution(Location location) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postSingleRecordPageAreaExecution(Location location) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preMultiRecordPageAreaExecution(Location location) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postMultiRecordPageAreaExecution(Location location) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public WebElement queryRowMRB(Location location, JSONArray drQueryFields) throws ExecutorException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebElement addRowMRB(Location location, WebElement targetRow) throws ExecutorException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebElement deleteRowMRB(Location location, WebElement targetRow) throws ExecutorException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void preFieldExecution(Location location, TestObject t) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postFieldExecution(Location location, TestObject t) throws ExecutorException {
		try {
			WebElement tick = this.driverContext.getDriver().findElement(By.xpath(".//img[contains(@src,'Done.svg')]"));
			this.capturePageScreenshot(false);
		} catch (Exception e) {
		}
		
	}

	@Override
	public void preMRBFieldExecution(Location location, TestObject t) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postMRBFieldExecution(Location location, TestObject t) throws ExecutorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processIterationStatus() throws ExecutorException {
		// TODO Auto-generated method stub
		
	}
	
	public void CaptureSnap(boolean lastPage) {
		boolean takeScreenshot = false;
		if (this.screenshotOption == 2 && lastPage) {
			takeScreenshot = true;
		} else if (this.screenshotOption == 3) {
			takeScreenshot = true;
		}
		if (takeScreenshot) {
			RuntimeScreenshot ras = this.getRuntimeScreenshotObject("End Of Page");
			this.runtimeScreenshots.add(ras);
		}
	}

}
