package com.ycs.tenjin.adapter.selenium.kotakdiy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.selenium.asv2.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv2.generic.WebMethodsImpl;

public class GenericAdapterMethods {
	protected WebMethodsImpl webGenericMethods;
	private DriverContext driverContext;
	private static final Logger logger = LoggerFactory.getLogger(GenericAdapterMethods.class);
	
	public GenericAdapterMethods(DriverContext driverContext,WebMethodsImpl webGenericMethods) {
		this.driverContext = driverContext;
		this.webGenericMethods = webGenericMethods;
	}
}
