package com.ycs.tenjin.adapter.selenium.kotakdiy;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class test {
	 public static void main(String[] args) {  
	        
		    System.setProperty("webdriver.chrome.driver", "D:\\Tenjin\\client\\browser_driver\\chromedriver.exe");  
		    WebDriver driver=new ChromeDriver();  
		    WebDriverWait wait = new WebDriverWait(driver, 10);
		    
		    driver.navigate().to("https://hltest-agencies.ind16s.sfdc-y37hzm.force.com/DIY/");  
		    driver.manage().window().maximize();
		    
		    driver.findElement(By.xpath(".//*[text()='Balance Transfer']//following::button[1]")).click();
		    
		    WebElement mob=wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='mobileNumberField']")));
		    mob.sendKeys("7564777566");
		    WebElement dob=wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='dateField']")));
		    dob.sendKeys("12/12/1996");
		    
		    //driver.findElement(By.xpath(".//input[@name='options']")).click();
		    driver.findElement(By.xpath(".//button[text()='CONTINUE']")).click();
		    test t=new test();
		    t.Otpin(driver,"@@OTP,543210");
		    driver.close();
		    
		    } 

	public void Otpin(WebDriver driver, String data) {
		data = data.replace("@@OTP,", "");
		char[] da = data.toCharArray();
		List<WebElement> eles = driver.findElements(By.xpath(".//*[contains(@id,'OTPInput1')]/input"));
		for (int i = 0; i < da.length; i++) {
			String a = Character.toString(da[i]);
			eles.get(i).sendKeys(a);
		}
	}
	 
}
