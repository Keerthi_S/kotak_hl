package com.ycs.tenjin.adapter.selenium.kotakdiy;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.selenium.asv2.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv2.generic.WebMethodsImpl;
import com.ycs.tenjin.bridge.selenium.asv2.web.ExecutionMethodsAbstract;

public class ExecutionMethodImpl extends ExecutionMethodsAbstract  {
	
	GenericAdapterMethods genericAdapterMethods;
	WebDriverWait wait = new WebDriverWait(this.driverContext.getDriver(), 20);

	public ExecutionMethodImpl(DriverContext driverContext, WebMethodsImpl webGenericMethods) {
		super(driverContext, webGenericMethods);
		this.genericAdapterMethods = new GenericAdapterMethods(this.driverContext,this.webGenericMethods); 
	}

	@Override
	public WebElement locateMRBElement(TestObject t, WebElement targetRowForMRB, int objectIdentificationTimeout)
			throws AutException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void performMRBInput(WebElement element, TestObject t, String data) throws AutException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String performMRBOutput(WebElement element, TestObject t) throws AutException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebElement locateElementExtend(TestObject t) throws AutException {
		WebElement ele = null;
		try {
			if (t.getObjectClass().equalsIgnoreCase("button")) 
				ele = this.driverContext.getDriver().findElement(By.xpath(".//button[text()='" + t.getData() + "'] | .//img[contains(@src,'"+t.getData()+".svg')]"));
		} catch (Exception e) {
			throw new AutException("Could not locate button -->"+t.getData());
		}
		return ele;
	}

	@Override
	public WebElement processButtonHandlersExtended(WebElement wrapper, TestObject t) throws AutException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean performInputExtend(WebElement element, TestObject t, String data) throws AutException {
		if (data.equalsIgnoreCase("@@ROBOT")) {
			//WebElement iframe = this.driverContext.getDriver().findElement(By.xpath(".//iframe[@title='reCAPTCHA']"));
			//this.driverContext.getDriver().switchTo().frame(iframe);
			//WebElement reCaph = this.driverContext.getDriver().findElement(By.xpath(".//span[@id='recaptcha-anchor']"));
			//wait.until(ExpectedConditions.elementToBeSelected(By.xpath(".//span[@id='recaptcha-anchor' and @aria-checked='true']")));
			
			
			new WebDriverWait(this.driverContext.getDriver(), 10).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[starts-with(@name, 'a-') and starts-with(@src, 'https://www.google.com/recaptcha')]")));
			WebElement eleme = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.recaptcha-checkbox-checkmark")));
			//eleme.click();
			
			((JavascriptExecutor) this.driverContext.getDriver()).executeScript("var elem=arguments[0]; setTimeout(function() {elem.click();}, 100)", eleme);
			this.driverContext.getDriver().switchTo().defaultContent();
			return true;
		} else if (data.equalsIgnoreCase("@@CHECK")) {
			this.driverContext.getDriver().findElement(By.xpath(".//input[@type='checkbox']/ancestor::div[1]//span")).click();
			return true;
		} else if(data.startsWith("@@OTP,")) {
			this.Otpin(driverContext.getDriver(), data);
			return true;
		}
		return false;
	}
	
	public void Otpin(WebDriver driver, String data) {
		data = data.replace("@@OTP,", "");
		char[] da = data.toCharArray();
		List<WebElement> eles = driver.findElements(By.xpath(".//*[contains(@id,'OTPInput1')]/input"));
		for (int i = 0; i < da.length; i++) {
			String a = Character.toString(da[i]);
			eles.get(i).sendKeys(a);
		}
	}
	
	@Override
	public void prePerformInput(WebElement element, TestObject t, String data) throws AutException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postPerformInput(WebElement element, TestObject t, String data) throws AutException {
		if (FieldType.BUTTON.toString().equalsIgnoreCase(t.getObjectClass())) {
			waitTillLoading();
			WebDriverWait w = new WebDriverWait(this.driverContext.getDriver(),5);
		      // presenceOfElementLocated condition
		      w.until(ExpectedConditions.presenceOfElementLocated (By.xpath(".//webruntime-router-container")));
		}
		
	}

	private void waitTillLoading() {
		WebElement spin;
		int count=0;
		boolean flag = true;
		while(flag && count < 20) {
		try {
			spin = this.driverContext.getDriver().findElement(By.xpath(".//span[text()='Loading']"));
			this.explicitWait(1);
			count++;
		} catch (Exception e) {
			flag=false;
			logger.debug("Skipped from spinner");
		}
		}
	}

	@Override
	public String performOutputExtend(WebElement element, TestObject t) throws AutException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void prePerformOutput(WebElement element, TestObject t) throws AutException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postPerformOutput(WebElement element, TestObject t) throws AutException {
		// TODO Auto-generated method stub
		
	}
	
	public void explicitWait(long seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (Exception e) {
		}
		 
	}

}
