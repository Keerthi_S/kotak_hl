package com.ycs.tenjin.adapter.selenium.kotakdiy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.selenium.asv2.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv2.generic.WebMethodsImpl;
import com.ycs.tenjin.bridge.selenium.asv2.web.LearningMethodsAbstract;

public class LearningMethodImpl extends LearningMethodsAbstract {

	public LearningMethodImpl(DriverContext driverContext, WebMethodsImpl webGenericMethods) {
		super(driverContext, webGenericMethods);
		
		// TODO Auto-generated constructor stub
	}

	@Override
	public FieldType getTypeOfElementExtend(Map<String, String> attributesMap) {
		// TODO Auto-generated method stub
		FieldType elementtype = null;
		String type="";
			type = attributesMap.get("type");
		 if (type==null)
			type = attributesMap.get("id");
		 if ((type != null && type.equalsIgnoreCase("number") || type.equalsIgnoreCase("tel")) || (attributesMap.get("tag").equalsIgnoreCase("input")&&type.toLowerCase().startsWith("otp-field"))) {
			 elementtype = FieldType.TEXTBOX;
			}
		try {
			if (type.equalsIgnoreCase("text") && attributesMap.get("aria-haspopup").equalsIgnoreCase("listbox"))
				elementtype = FieldType.LIST;
		} catch (Exception e) {
		}
		return elementtype;
	}

	@Override
	public Map<String, String> getUniqueIdOfElementExtend(Map<String, String> attributesMap, WebElement element)
			throws LearnerException {
		Map<String, String> uidMap = new HashMap<String, String>();
		String uid=null;
		uid = attributesMap.get("name");
		try {//uid ==null && 
			if(attributesMap.get("type").equalsIgnoreCase("checkbox"))
			 uid = attributesMap.get("value");
		} catch (Exception e) {
			logger.info("Could not get the uid-->"+uid);
		}
		if(uid==null)
			return null;
		uidMap.put("uid", uid);
		uidMap.put("uidType", "name");
		logger.debug("id found->"+uid);
		return uidMap;
	}

	@Override
	public String getLabelOfElementExtend(FieldType type, WebElement element, Map<String, String> attributesMap) {
		String label=null;
		try {
			if (label == null && attributesMap.get("type").equalsIgnoreCase("checkbox"))
				label = element.findElement(By.xpath(".//following::label[2]")).getText();
			else
				label = element.findElement(By.xpath(".//preceding::label[2]")).getText();
		} catch (Exception e) {
			logger.info("Could not get the label for with id-->"+label);
		}
		try {
			if (attributesMap.get("type").equalsIgnoreCase("checkbox") && !attributesMap.get("value").isEmpty())
				label = element.findElement(By.xpath(".//following-sibling::label[1]")).getText();
		} catch (Exception e) {
			logger.info("Could not get the label for with id-->"+label);
		}
		try {
			if (attributesMap.get("id").toLowerCase().startsWith("otp-field")||attributesMap.get("id").toLowerCase().startsWith("default1")||attributesMap.get("id").equalsIgnoreCase("institutionId")||attributesMap.get("id").equalsIgnoreCase("userId")||attributesMap.get("name").toLowerCase().startsWith("land"))
				label = element.findElement(By.xpath(".//preceding::label[1]")).getText();
		} catch (Exception e) {
			logger.info("Could not get the label for with id-->"+attributesMap.get("id"));
		}
		try {
			if (attributesMap.get("name").equalsIgnoreCase("searchText"))
				label = "Specify if other city";
		} catch (Exception e) {
			logger.info("Could not get the label for with id-->"+attributesMap.get("searchtext"));
		}
		try {
			if(label==null)
				label = element.findElement(By.xpath(".//preceding::div[1]")).getText();
		} catch (Exception e) {
			logger.info("Could not get the label for with id-->"+attributesMap.get("searchtext"));
		}
		/*
		 * try { if (attributesMap.get("role").equalsIgnoreCase("combobox")) label =
		 * element.findElement(By.xpath(".//ancestor::lightning-combobox/label")).
		 * getText(); } catch (Exception e) {
		 * logger.info("Could not get the label for with id-->"+attributesMap.get("role"
		 * )); }
		 */
		if(label.isEmpty())
			 label = element.findElement(By.xpath(".//preceding::label[3]")).getText();
		logger.debug("Label found->"+label);
				return label;
	}

	@Override
	public String getDropdownValuesOfElementExtend(FieldType type, WebElement element,
			Map<String, String> attributesMap) {
		String dropValues = "";
		if (type.toString().equalsIgnoreCase(FieldType.LIST.toString())) {
		try {
			element.click();
			List<WebElement> options = this.driverContext.getDriver().findElements(By.xpath(".//div[contains(@id,'dropdown-element')]//span |.//option"));
			for (WebElement ele : options) {
				String value = ele.getText();
				if (value.isEmpty())
					continue;
				dropValues = dropValues + value + ";";
			}
			if (!dropValues.equals(""))
				dropValues = dropValues.substring(0, dropValues.length() - 1);
			element.click();
		} catch (Exception e) {
			logger.error("Error to get options for : " + element.getAttribute("innerHTML"));
		}
		}
		return dropValues;
	}

	@Override
	public boolean isMandatory(Map<String, String> attributesMap, WebElement element) throws LearnerException {
		// TODO Auto-generated method stub
		return false;
	}

}
