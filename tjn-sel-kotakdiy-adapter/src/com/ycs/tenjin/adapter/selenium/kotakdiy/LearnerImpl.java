package com.ycs.tenjin.adapter.selenium.kotakdiy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ycs.tenjin.bridge.enums.PageType;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.selenium.asv2.SeleniumApplicationImpl;
import com.ycs.tenjin.bridge.selenium.asv2.taskhandler.task.LearnerTaskAbstract;

public class LearnerImpl extends LearnerTaskAbstract {
	
	GenericAdapterMethods genericAdapterMethods;
	WebDriverWait wait = new WebDriverWait(this.driverContext.getDriver(), 60);
	ArrayList<String> tabstl = new ArrayList<String>();
	
	public LearnerImpl(SeleniumApplicationImpl oApplication, Module function) {
		super(oApplication, function);
		this.learningMethods = new LearningMethodImpl(this.driverContext,webGenericMethods);
		this.commonMethods = new CommonMethodImpl(this.driverContext, this.webGenericMethods);
		this.genericAdapterMethods = new GenericAdapterMethods(this.driverContext,this.webGenericMethods); 
		this.executionMethods = new ExecutionMethodImpl(this.driverContext,this.webGenericMethods);
	}

	@Override
	public WebElement getWrapper(Location objLocation) throws LearnerException {
		WebElement wraper = null;
		try {
			wraper = this.driverContext.getDriver().findElement(By.xpath(".//article"));
		} catch (Exception e) {
			wraper = this.driverContext.getDriver().findElement(By.xpath(".//lightning-layout-item | .//div[@class='well ps-no-well']"));
		}
		if (wraper == null || !wraper.isDisplayed())
			throw new LearnerException("Could not set wrapper this->" + objLocation.getLocationName());
		return wraper;
	}

	@Override
	public String getScreenTitle(Location objLocation) throws LearnerException {
		String title = null;
		try {
			title = this.driverContext.getDriver().findElement(By.xpath(".//*[@aria-live='polite']")).getText();
		} catch (Exception e) {
			title = this.driverContext.getDriver().findElement(By.xpath(".//h5")).getText();
		}
		/*title = this.driverContext.getDriver().findElement(By.xpath(".//*[contains(@class,'title')]")).getText();
		if (title.startsWith("Hello"))
			title = this.driverContext.getDriver().findElement(By.xpath(".//*[contains(@class,'title')]/following-sibling::div[2]")).getText();
		else if (title.startsWith("Okay"))
			title = this.driverContext.getDriver().findElements(By.xpath(".//*[contains(@class,'title')]")).get(1).getText();
		if (title.contains("�")) {
			title = title.replace("�", "");
		}*/
		return title;
	}
	
	private boolean isAssisted(Location currentLocation) {
        logger.info("Learning isAssisted---->" + currentLocation.getLocationName());
		Collection<TestObject> fields = this.learningData.get(currentLocation.getLocationName());
		if(fields.size()>0){
			return true;
		}
		return false;
	}

	@Override
	public ArrayList<Location> getAllPageAreas(Location currentLocation) throws LearnerException {
		boolean check = isAssisted(currentLocation);
		String presentScrnTitle = this.getScreenTitle(currentLocation);
		ArrayList<Location> listLocations = new ArrayList<Location>();
		
		//Get Page Areas
			if(!this.tabstl.contains(presentScrnTitle) && check) {
			Location location = new Location();
			location.setLocationType(PageType.PAGE.toString());
			location.setWayIn("NA");
			location.setLocationName(this.getScreenTitle(currentLocation));
			location.setScreenTitle(presentScrnTitle);
			location.setParent(currentLocation.getScreenTitle());
			location.setWayOut("NA");
			listLocations.add(location);
			this.tabstl.add(presentScrnTitle);
		}
		return listLocations;
	}

	@Override
	public void validateLocation(Location location) throws LearnerException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getWayout(Location objLocation) throws LearnerException {
		// TODO Auto-generated method stub
		return "NA";
	}

	@Override
	public ArrayList<TestObject> getAllVisibleElementForMRB(Location currentLocation) throws LearnerException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void preLearning() throws LearnerException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postLearning() throws LearnerException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prePageLearning(Location currentLocation) throws LearnerException {
		try {
			List<WebElement> Addpls = this.driverContext.getDriver().findElements(By.xpath(".//img[contains(@src,'Add')]"));
			for (WebElement adpl : Addpls) {
				adpl.click();
			}
		} catch (Exception e) {
			logger.info("No Add Plus Button before learning to click");
		}
	}

	@Override
	public void postPageLearning(Location currentLocation) throws LearnerException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map<String, Object> performAssistedLearningExtended(Location currentLocation, String stage,
			TestObject field) {
		if (field.getIdentifiedBy().equalsIgnoreCase("name") && !field.getData().startsWith("@@")) {
			this.driverContext.getDriver().findElement(By.xpath(".//input[@"+field.getIdentifiedBy()+"='"+field.getLabel()+"']")).sendKeys(field.getData());;
		}else if(field.getData().equalsIgnoreCase("@@ROBOT")) {
			this.driverContext.getDriver().findElement(By.xpath(".//span[@id='recaptcha-anchor']")).click();
			wait.until(ExpectedConditions.elementToBeSelected(By.xpath(".//span[@id='recaptcha-anchor' and @aria-checked='true']")));
		}else if(field.getData().equalsIgnoreCase("@@CHECK")) {
			this.driverContext.getDriver().findElement(By.xpath(".//input[@type='checkbox']")).click();
		}return null;
	}

	@Override
	public void preGetAllVisibleElements(Location currentLocation, List<WebElement> elements,
			ArrayList<TestObject> testObjects) throws LearnerException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postGetAllVisibleElements(Location currentLocation, List<WebElement> elements,
			ArrayList<TestObject> testObjects) throws LearnerException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<Location> getAllPageAreasMRBs(Location currentLocation) throws LearnerException {
		// TODO Auto-generated method stub
		return null;
	}

}
