package com.ycs.tenjin.defect.impl.dms;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.dms.services.DmsService;
import com.ycs.tenjin.defect.Attachment;
import com.ycs.tenjin.defect.Comment;
import com.ycs.tenjin.defect.Defect;
import com.ycs.tenjin.defect.DefectManager;
import com.ycs.tenjin.defect.EDMException;
import com.ycs.tenjin.defect.UnreachableEDMException;
import com.ycs.tenjin.defect.impl.GenericEDMImpl;



public class DmsImpl extends GenericEDMImpl implements DefectManager{
	
	private static final Logger logger=LoggerFactory.getLogger(DmsImpl.class);
	private String authHeader=null;
	
	DmsService dmsService =new DmsService();
	SetterRequest setterrequest =new SetterRequest();

	public DmsImpl(String baseUrl, String userId, String password) {
		super(baseUrl, userId, password);
		
	}

	@Override
	public String login() throws UnreachableEDMException, EDMException {
		this.authHeader=dmsService.getBasicAuthHeader(this.userId,this.password);
		logger.debug(authHeader);
	    return authHeader;
	}

	@Override
	public boolean isUrlValid() throws UnreachableEDMException {
		Integer UrlvaidateStatus=dmsService.urlvalidation(this.baseUrl);
		if(UrlvaidateStatus==200)
		    return true;
		else{
	      return false;
		}
	}

	@Override
	public List<Defect> postDefects(List<Defect> defects) throws UnreachableEDMException, EDMException {
		
		return null;
	}

	@Override
	public Defect getDefect(String defectIdentifier) throws UnreachableEDMException, EDMException {
		Defect defect=new Defect();
		try {
			defect = dmsService.getIssuedata(this.baseUrl,authHeader,defectIdentifier);
		} catch (JSONException e) {
			
			logger.error("Error ", e);
		}
		return defect;
	}

	@Override
	public List<Defect> getDefects(String[] defectIdentifiers) throws UnreachableEDMException, EDMException {
		
		return null;
	}

	@Override
	public List<Defect> getDefects(List<String> defectIdentifiers) throws UnreachableEDMException, EDMException {
		
		return null;
	}

	@Override
	public void postDefectAttachment(Defect defect, Attachment attachment)
			throws UnreachableEDMException, EDMException {
		try {
			List<com.ycs.tenjin.dms.bean.Attachment>  attachmentdata= dmsService.addattachment(this.baseUrl,defect.getIdentifier(),attachment,authHeader);
		        attachment.setAttachmentIdentifier(attachmentdata.get(0).getId());
				attachment.setFileName(attachmentdata.get(0).getFilename());
		  } catch (EDMException e) {
			  throw new EDMException("An internal error occurred while fetching posting attachment to DMS. Please contact support");
		 }catch (Exception e) {
			 logger.error("Error ", e);
		 }
	}

	@Override
	public void postDefectAttachment(String defectIdentifier, String projectIdentifer, Attachment attachment)
			throws UnreachableEDMException, EDMException {
		
		
	}

	@Override
	public void postComment(Defect defect, Comment comment) throws UnreachableEDMException, EDMException {
		
		
	}

	@Override
	public List<Attachment> getDefectAttachments(String defectIdentifier, String projectIdentifier)
			throws UnreachableEDMException, EDMException {
		
		 String listAttachment=dmsService.getlistattachment(this.baseUrl,this.authHeader,defectIdentifier);
		 List<Attachment> attachmentList=new ArrayList<Attachment>();
		 
		 JSONArray attachmentsJsonArray=null;
		 try {
			 attachmentsJsonArray= new JSONArray(listAttachment);
		} catch (JSONException e) {
			logger.error("Error ", e);
		}
		 
		 try {
			 for(int i=0; i<attachmentsJsonArray.length(); i++)
			 {
				 JSONObject json=attachmentsJsonArray.getJSONObject(i);
				 Attachment attachment=new Attachment();
				 attachment.setAttachmentIdentifier(json.getString("defectAttachmentId"));
				 attachment.setFileName(json.getString("name"));
				 attachmentList.add(attachment);
			 }
			
		} catch (Exception e) {
			logger.error("Error ", e);
		}
		return attachmentList ;
	}

	@Override
	public Attachment getDefectAttachment(String defectIdentifier, String projectIdentifier,
			String attachmentIdentifier) throws UnreachableEDMException, EDMException {
		
		return null;
	}

	@Override
	public List<Attachment> getDefectAttachments(String defectIdentifier, String projectIdentifier,
			List<String> attachmentIdentifiers) throws UnreachableEDMException, EDMException {
		
		return null;
	}

	/**
	 *  DMS Get all projects method Implementation.
	 **/
	@Override
	public JSONArray getAllProjects() throws UnreachableEDMException,
			EDMException {
		JSONArray allProjects = dmsService.getAllProjects(this.baseUrl, this.authHeader);
		return allProjects;
	}

	@Override
	public void logout() throws UnreachableEDMException, EDMException {
		
		
	}

	@Override
	public List<Comment> getComments(String defectIdentifier) throws UnreachableEDMException, EDMException {
		
		return null;
	}

	@Override
	public void setProjectDesc(String projectDescription) {
		
		
	}

	@Override
	public void postDefectDetails(Defect defect) throws UnreachableEDMException, EDMException {	
		String input=setterrequest.getObjCreateIssue(defect,this.userId);
		Map<String,String> map = dmsService.createIssuse(input,this.baseUrl,authHeader);
		defect.setIdentifier(map.get("defectIdentifier"));
		}

	@Override
	public Defect getDefect(Defect defect) {
		
		return null;
	}

}
