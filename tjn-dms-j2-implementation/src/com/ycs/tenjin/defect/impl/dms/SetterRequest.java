package com.ycs.tenjin.defect.impl.dms;

import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.ycs.tenjin.defect.Defect;

public class SetterRequest {
	
	private static final org.slf4j.Logger logger=LoggerFactory.getLogger(SetterRequest.class);
/**
 * Conversion for create issue.
 */
 public String getObjCreateIssue(Defect defect,String userId) {
	 
	        JsonObject fields = new JsonObject();
	       
	        JsonObject testCycle = new JsonObject();
	        testCycle.addProperty("key", defect.getEdmProjectName());
	       
	        JsonObject priority = new JsonObject();
	        /*Modified by Ashiki for Tenj210-86 starts*/
	        priority.addProperty("name", defect.getPriority());
	        /*Modified by Ashiki for Tenj210-86 ends*/
	        JsonObject issueType = new JsonObject();
	        issueType.addProperty("name", "Bug");
	       
	        JsonObject defectStatus = new JsonObject();
	        defectStatus.addProperty("name", "New");
	       
	        JsonObject defectSeverity = new JsonObject();
	        /*Modified by Ashiki for Tenj210-86 starts*/
	        defectSeverity.addProperty("name", defect.getSeverity());
	        /*Modified by Ashiki for Tenj210-86 ends*/
	        JsonObject assignedTo = new JsonObject();
	        assignedTo.addProperty("username", userId);
	       
	        String description=defect.getDescription();
	        fields.add("testCycle", testCycle);
	        fields.addProperty("summary", defect.getSummary());
	        fields.add("defectPriority", priority);
	        fields.add("defectType", issueType);
	        fields.add("defectStatus", defectStatus);
	        fields.add("defectSeverity",defectSeverity);
	        fields.add("assignedTo",assignedTo);   
	        fields.addProperty("description", description);
	        logger.info("--------Request JSON----------{}",fields.toString());
	       return fields.toString();
	}
	
}
