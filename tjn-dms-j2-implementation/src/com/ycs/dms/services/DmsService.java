package com.ycs.dms.services;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.ycs.jira.util.Properties;
import com.ycs.tenjin.defect.Attachment;
import com.ycs.tenjin.defect.Defect;
import com.ycs.tenjin.defect.EDMException;
import com.ycs.tenjin.defect.UnreachableEDMException;



public class DmsService {

	private static final Logger logger = LoggerFactory
			.getLogger(DmsService.class);

	private static ClientConfig config = new ClientConfig();
	
	/**
	 * Urlvalidation dms Rest api service.
	 **/
	public Integer urlvalidation(String baseUrl) throws UnreachableEDMException {

		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(baseUrl);

		Response response = target.request().get();
		logger.info("--------urlvalidation status----------{}",response.getStatus());
		return response.getStatus();
	}
	
	public  String getBasicAuthHeader(String username, String password) {
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(
           auth.getBytes(Charset.forName("US-ASCII")) );
        String authHeader = "Basic " + new String( encodedAuth );
        return authHeader;
        
    }
	
	/**
	 * Get all Test Cyle dms Rest api service.
	 **/
	public JSONArray getAllProjects(String baseUrl, String authHeader)
			throws UnreachableEDMException, EDMException {
		
		String targetUrl = baseUrl + "/rest/def/testCycles/user";
		JSONArray finalArray = new JSONArray();
		String jsonString = "";
		try {
			jsonString = GenericDmsServices.get(targetUrl, authHeader);

			JSONArray array = new JSONArray(jsonString);
			for (int i = 0; i < array.length(); i++) {
				JSONObject TestCycleFromDms = array.getJSONObject(i);
				JSONObject testCycle = new JSONObject();
				testCycle.put("pid", TestCycleFromDms.get("key"));
				testCycle.put("pname", TestCycleFromDms.get("name"));
				finalArray.put(testCycle);
			}
		} catch (JSONException e) {
			throw new UnreachableEDMException(
					"Unable to fetch projects due to json parse error.");
		}
		return finalArray;
	}
	
	
	/**
	 * Issue creation DMS Rest api service.
	 * 
	 * @throws EDMException
	 **/
	@SuppressWarnings("unchecked")
	public Map<String,String> createIssuse(String inputRequest,
			String BaseUrl, String authHeader) throws UnreachableEDMException,
			EDMException {

		String targetUrl = BaseUrl + "/rest/def/defects/";
		Gson gson = new Gson();
		String jsonString = GenericDmsServices.post(targetUrl, authHeader, inputRequest);
		Map<String,String> json = gson.fromJson(jsonString, Map.class);
		return json;
	}

	public List<com.ycs.tenjin.dms.bean.Attachment> addattachment(String baseUrl, String identifier,
			Attachment fullfilename, String authHeader) throws EDMException {

		List<com.ycs.tenjin.dms.bean.Attachment> resobj = null;
		String path = fullfilename.getFilePath();

		File f = new File(path);
		if (!f.exists() && !f.isDirectory()) {

			logger.error("File not exisetd");
		}
		File fileUpload = new File(path);

		CloseableHttpClient httpClient = HttpClientBuilder.create().build();

		HttpPost postRequest = new HttpPost(baseUrl + "/rest/def/defects/"+ identifier +"/defectAttachments");
		postRequest.setHeader(HttpHeaders.AUTHORIZATION,  authHeader);
		postRequest.setHeader("X-Atlassian-Token", "nocheck");

		MultipartEntityBuilder entity = MultipartEntityBuilder.create();
		entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

		FileBody fb = new FileBody(fileUpload);
		entity.addPart("file", fb);
		postRequest.setEntity(entity.build());
		HttpResponse response = null;
		String jsonString = null;
		try {
			response = httpClient.execute(postRequest);
			jsonString = EntityUtils.toString(response.getEntity());
			// logger.info("----------------------{}",jsonString);
		} catch (ClientProtocolException e1) {
			logger.error("Error ", e1);
		} catch (ParseException e1) {
			logger.error("Error ", e1);
		} catch (IOException e1) {
			logger.error("Error ", e1);
		}
		int status = response.getStatusLine().getStatusCode();
		Gson gson = new Gson();
		resobj = new ArrayList<com.ycs.tenjin.dms.bean.Attachment>();

		if (response.getStatusLine().getStatusCode() == 200) {
			logger.info("Post attachment for defect:" + identifier + " {}",
					Properties.DMS_SUCCESS);
			resobj = gson
					.fromJson(
							jsonString,
							new TypeToken<ArrayList<com.ycs.tenjin.dms.bean.Attachment>>() {

								private static final long serialVersionUID = -7815779803494858753L;
							}.getType());
		} else {
			String message = jsonString; // response.getEntity().toString();
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			throw new EDMException(status + " - " + message);
		}
		return resobj;
	}

	public Defect getIssuedata(String BaseUrl, String authHeader,
			String getIssueId) throws UnreachableEDMException, EDMException, JSONException {

		String targetUrl = BaseUrl + "/rest/def/defects/" + getIssueId;

		String jsonString = "";
			jsonString = GenericDmsServices.get(targetUrl, authHeader);
			JSONObject jsonObj=null;
		try {
			jsonObj = new JSONObject(jsonString);
		} catch (JSONException e) {
			
			logger.error("Error ", e);
		}
		Defect resobj = new Defect();
		
		resobj.setIdentifier(jsonObj.get("defectIdentifier").toString());
		resobj.setSummary(jsonObj.getString("summary"));
		try{
		resobj.setDescription(jsonObj.getString("description"));
		}catch(Exception e){
			resobj.setDescription("");
		}
		JSONObject Severity = jsonObj.getJSONObject("defectSeverity");
		resobj.setSeverity(Severity.getString("name"));
		
		JSONObject priority = jsonObj.getJSONObject("defectPriority");
		resobj.setPriority(priority.getString("name"));
		
		JSONObject status = jsonObj.getJSONObject("defectStatus");
		resobj.setStatus(status.getString("name"));
		
		return resobj;

	}
	
	public String getlistattachment(String baseUrl, String authHeader, String defectIdentifier) throws EDMException {
		 String targetUrl =baseUrl+"/rest/def/"+defectIdentifier+"/defectAttachments";
		 String output = GenericDmsServices.get(targetUrl,authHeader);
	    return output;
	    
	}

	
}
