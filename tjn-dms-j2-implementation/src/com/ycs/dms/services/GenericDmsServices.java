package com.ycs.dms.services;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.defect.EDMException;



public class GenericDmsServices {
	private static final Logger logger=LoggerFactory.getLogger(GenericDmsServices.class);
	private static ClientConfig config = new ClientConfig();
	
	/**
	 *  Generic get service.
	 **/
	protected static String get(String url, String authHeader) throws EDMException {

		logger.info("Calling --> GET {}", url);

		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(url);

		Response response = target.request()
				.header(HttpHeaders.AUTHORIZATION, authHeader).get();

		logger.info("Response from server recieved");
		int status = response.getStatus();
		String message = response.readEntity(String.class);

		if (status == 200 || status == 201) {
			logger.info("Status - [{}]", status);
			logger.info("Message - [{}]", message);
			return message;
		}
		
		else if(status == 401){
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			throw new EDMException("Authentication failed. Please check user credentials and try again");
		}else {
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			throw new EDMException("Defect does not exist, Either someone has deleted the defect or Defect Management project.");
			
		}
	}
	
	
	/**
	 *  Generic post service.
	 **/
	 protected  static String post(String url, String token, String input)
			throws EDMException {

		logger.info("Calling --> POST {}", url);

		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(url);

		Response response = target
				.request(MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, token)
				.post(Entity.entity(input, MediaType.APPLICATION_JSON),
						Response.class);

		logger.info("Response from server recieved");
		int status = response.getStatus();
		String message = response.readEntity(String.class);

		if (status == 200 || status == 201) {
			logger.info("Status - [{}]", status);
			logger.info("Message - [{}]", message);
			return message;
		}
		else if(status == 401){
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			throw new EDMException("Authentication failed. Please check user credentials and try again");
		}
		else if(status == 400){
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			if(message.contains("priority")){
				throw new EDMException("Priority value is not valid");
			}
			else if(message.contains("customfield_10131")){
				throw new EDMException("Severity value is not valid");
			}
			else
				throw new EDMException("Could not post defect due to an Internal error, Please contact Tenjin support");
		} 
		else {
			logger.error("Status - [{}]", status);
			logger.error("Message - [{}]", message);
			throw new EDMException("Could not post defect due to an Internal error, Please contact Tenjin support");
		}
	}

	
}
