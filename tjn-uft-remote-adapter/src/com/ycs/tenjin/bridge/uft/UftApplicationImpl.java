/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Application.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */


/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 24-Sep-2016          Sameer Gupta	        Newly Added For  
 */

package com.ycs.tenjin.bridge.uft;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.exceptions.ApplicationUnresponsiveException;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.DriverException;
import com.ycs.tenjin.bridge.oem.gui.GuiApplication;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationAbstract;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.remoteapiserver.RemoteApiServerInstance;
import com.ycs.tenjin.rmi.RemoteTenjinConstants;
import com.ycs.tenjin.rmi.RemoteTenjinInstance;

public class UftApplicationImpl extends GuiApplicationAbstract implements GuiApplication {
	private static final Logger logger = LoggerFactory
			.getLogger(UftApplicationImpl.class);
	/*********************************
	 * TENJINCG-731 - By Sameer - Mobility - 
	 */
	public UftApplicationImpl(Aut aut, String clientIp, int port, String browser) {
		super(aut, clientIp, port, browser,null );
	}

	String taskHandlerType = "";
	public void launch(String URL) throws AutException, DriverException   {

		try {
			getTaskHandlerType(); 
			RemoteTenjinInstance rti = getRMI();
			rti.launch(this.adapterPrefix,this.taskHandlerType,URL);
		} catch (RemoteException e) {
			logger.debug(e.getMessage());
			throw new DriverException(e.getMessage());
		} catch (com.ycs.tenjin.rmi.AutException e) { 
			logger.debug(e.getMessage());
			throw new AutException(e.getMessage());
		} 
	} 
	public void login(String LoginName, String Password) throws AutException, ApplicationUnresponsiveException, DriverException {

		try {
			getTaskHandlerType(); 
			RemoteTenjinInstance rti = getRMI();
			rti.login(this.adapterPrefix,this.taskHandlerType,LoginName, Password);
		} catch (RemoteException e) {
			logger.debug(e.getMessage());
			throw new DriverException(e.getMessage());
		} catch (com.ycs.tenjin.rmi.AutException e) { 
			logger.debug(e.getMessage());
			throw new AutException(e.getMessage());
		} 
	}

	public void navigateToFunction(String Code, String Name,
			String MenuContainer) throws AutException, BridgeException,
			ApplicationUnresponsiveException,DriverException{

		try {
			getTaskHandlerType(); 
			RemoteTenjinInstance rti = getRMI();
			rti.navigateToFunction(this.adapterPrefix,this.taskHandlerType,Code, Name, MenuContainer);
		} catch (RemoteException e) {
			logger.debug(e.getMessage());
			throw new DriverException(e.getMessage());
		} catch (com.ycs.tenjin.rmi.AutException e) { 
			logger.debug(e.getMessage());
			throw new AutException(e.getMessage());
		} 
	}

	public void logout()throws AutException,DriverException {

		try {
			getTaskHandlerType(); 
			RemoteTenjinInstance rti = getRMI();
			rti.logout(this.adapterPrefix,this.taskHandlerType);
		} catch (RemoteException e) {
			logger.debug(e.getMessage());
			throw new DriverException(e.getMessage());
		} catch (com.ycs.tenjin.rmi.AutException e) { 
			logger.debug(e.getMessage());
			throw new AutException(e.getMessage());
		} 
	}

	public void recover()throws AutException,DriverException {

		try {
			getTaskHandlerType(); 
			RemoteTenjinInstance rti = getRMI();
			rti.recover(this.adapterPrefix,this.taskHandlerType);
		} catch (RemoteException e) {
			logger.debug(e.getMessage());
			throw new DriverException(e.getMessage());
		} catch (com.ycs.tenjin.rmi.AutException e) { 
			logger.debug(e.getMessage());
			throw new AutException(e.getMessage());
		} 
	}

	public RemoteTenjinInstance getRMI() throws RemoteException {

		RemoteTenjinInstance rti = null;
		String remoteServerType = null;
		
		try {
			remoteServerType = TenjinConfiguration
					.getProperty("REMOTE_SERVER_TYPE");
		} catch (TenjinConfigurationException e) { 
			logger.error("ERROR initializng cache store ", e);
		}

		if (remoteServerType.equalsIgnoreCase("rmi")) {

			Registry registry;
			try {
				registry = LocateRegistry.getRegistry(this.clientIp, this.port);
				rti = (RemoteTenjinInstance) registry
						.lookup(RemoteTenjinConstants.RMI_ID);
			} catch (RemoteException | NotBoundException e) {
				 throw new RemoteException(e.getMessage());
			}
			
		} else if (remoteServerType.equalsIgnoreCase("api")) {
			rti = new RemoteApiServerInstance(this.clientIp);
		}

		return rti;
	}

	private void getTaskHandlerType() {
		if(Thread.currentThread().getStackTrace()[3].getClassName().toString().toLowerCase().contains("learning")){
			taskHandlerType = "learning"; 
		}else if (Thread.currentThread().getStackTrace()[3].getClassName().toString().toLowerCase().contains("execution")){
			taskHandlerType = "execution";
		}else{
			taskHandlerType = "extraction";
		}
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
