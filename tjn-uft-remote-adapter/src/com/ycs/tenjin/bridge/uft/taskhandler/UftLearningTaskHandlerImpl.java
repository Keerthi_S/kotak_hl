/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  LearningTaskHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 24-Sep-2016          Sameer Gupta	        Newly Added For  
 * 11-12-2018			Sharath					making function code Unique
 */

package com.ycs.tenjin.bridge.uft.taskhandler;

import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.DriverException;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationAbstract;
import com.ycs.tenjin.bridge.oem.gui.taskhandler.LearningTaskHandler;
import com.ycs.tenjin.bridge.pojo.aut.Metadata;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.uft.UftApplicationImpl;
import com.ycs.tenjin.rmi.AutException;
import com.ycs.tenjin.rmi.RemoteTenjinInstance;

public class UftLearningTaskHandlerImpl implements LearningTaskHandler {

	protected static final Logger logger = LoggerFactory
			.getLogger(UftLearningTaskHandlerImpl.class);

	protected UftApplicationImpl oApplication;

	public UftLearningTaskHandlerImpl(GuiApplicationAbstract oApplication) {
		this.oApplication = (UftApplicationImpl) oApplication;
	}

	public Metadata learnFunctionImpl(Module function, int runId)
			throws LearnerException, DriverException {

		try {

			RemoteTenjinInstance rti = this.oApplication.getRMI();
			/*
			* Added by sharath for making function code Unique Starts
			*/
			rti.learn(this.oApplication.getAdapterPrefix(), function.getCode(),
					runId, this.oApplication.getAppName(),
					function.getMenuContainer());
			/*
			* Added by sharath for making function code Unique Ends
			*/

		} catch (RemoteException e) {
			logger.debug(e.getMessage());
			throw new DriverException(e.getMessage());
		} catch (AutException e) {
			logger.debug(e.getMessage());
			throw new LearnerException(e.getMessage());
		}
		return null;

	}

}
