/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  ExtractionTaskHandler.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 24-Sep-2016          Sameer Gupta	        Newly Added For  
 */

package com.ycs.tenjin.bridge.uft.taskhandler;

import java.rmi.RemoteException;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.DriverException;
import com.ycs.tenjin.bridge.exceptions.ExtractorException;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationAbstract;
import com.ycs.tenjin.bridge.oem.gui.taskhandler.ExtractionTaskHandler;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.uft.UftApplicationImpl;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.rmi.AutException;
import com.ycs.tenjin.rmi.RemoteTenjinInstance;

public class UftExtractionTaskHandlerImpl implements ExtractionTaskHandler {

	protected static final Logger logger = LoggerFactory
			.getLogger(UftExtractionTaskHandlerImpl.class);

	protected UftApplicationImpl oApplication;

	public UftExtractionTaskHandlerImpl(GuiApplicationAbstract oApplication) {
		this.oApplication = (UftApplicationImpl) oApplication;
	}

	public void extractFunctionImpl(String TdUid, JSONObject jsonData,
			Module function, int runId) throws DriverException,
			ExtractorException {

		try {

			// By Sameer : Fix to remove Temp File Creation
			String iterationJsonKey = runId + "JSON_DATA_FOR_EXTRACTION";
			CacheUtils.putObjectInRunCache(iterationJsonKey,
					jsonData.toString());

			RemoteTenjinInstance rti = this.oApplication.getRMI();
			rti.extract(this.oApplication.getAdapterPrefix(), iterationJsonKey,
					runId);

		} catch (RemoteException e) {
			logger.debug(e.getMessage());
			throw new DriverException(e.getMessage());
		} catch (AutException e) {
			logger.debug(e.getMessage());
			throw new ExtractorException(e.getMessage());
		}

	}

}
