package com.ycs.tenjin.defect;

import java.sql.Timestamp;

public class Comment {
	
	private int id;
	private String author;
	private String text;
	private Timestamp createdOn;
	private String modifieBy;
	private Timestamp modifedOn;
	private String identifier;
	private int recordCommentId;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	
	/**
	 * @return the modifieBy
	 */
	public String getModifieBy() {
		return modifieBy;
	}
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return the createdOn
	 */
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @param modifieBy the modifieBy to set
	 */
	public void setModifieBy(String modifieBy) {
		this.modifieBy = modifieBy;
	}
	/**
	 * @return the modifedOn
	 */
	public Timestamp getModifedOn() {
		return modifedOn;
	}
	/**
	 * @param modifedOn the modifedOn to set
	 */
	public void setModifedOn(Timestamp modifedOn) {
		this.modifedOn = modifedOn;
	}
	/**
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}
	/**
	 * @param identifier the identifier to set
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	/**
	 * @return the recordCommentId
	 */
	public int getRecordCommentId() {
		return recordCommentId;
	}
	/**
	 * @param recordCommentId the recordCommentId to set
	 */
	public void setRecordCommentId(int recordCommentId) {
		this.recordCommentId = recordCommentId;
	}

	

}
