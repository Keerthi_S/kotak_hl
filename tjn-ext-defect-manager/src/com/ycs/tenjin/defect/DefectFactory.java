/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DefectFactory.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 25-Oct-2016          Jyoti Ranjan          Newly Added For 
* 10-Aug-2017		    Manish					T25IT-113
* 01-09-2017		    Padmavathi				T25IT-434
* 08-12-2020			Pushpalatha				TENJINCG-1220
 */


package com.ycs.tenjin.defect;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/****  This class generate instance of the subclass interface dynamically. ****/
public class DefectFactory {
	private static final Logger logger = LoggerFactory.getLogger(DefectFactory.class);

	public static DefectManager getDefectTools(String toolName,String baseUrl, String userId, String password) throws EDMException{
		String expPackageName = "com.ycs.tenjin.defect.impl." + toolName;
		Reflections reflections = new Reflections(expPackageName);
		Set<Class<? extends DefectManager>> subTypes = reflections.getSubTypesOf(DefectManager.class);
		/*changed by manish for bug T25IT-113 on 10 Aug 2017 starts*/
		DefectManager defMngr = null;
		for(Class<? extends DefectManager> type:subTypes){
			
			try {
				Constructor<?> constructor = type.getConstructor(String.class,String.class,String.class);
				defMngr =  (DefectManager) constructor.newInstance(baseUrl,userId,password);
			} catch (SecurityException e) {
				logger.error(e.getMessage());
				throw new EDMException("An Internal error occured, Please contact Tenjin support");
			} catch (IllegalArgumentException e) {
				logger.error(e.getMessage());
				throw new EDMException("An Internal error occured, Please contact Tenjin support");
			} catch (NoSuchMethodException e) {
				logger.error(e.getMessage());
				throw new EDMException("An Internal error occured, Please contact Tenjin support");
			} catch (InstantiationException e) {
				logger.error(e.getMessage());
				throw new EDMException("An Internal error occured, Please contact Tenjin support");
			} catch (IllegalAccessException e) {
				logger.error(e.getMessage());
				throw new EDMException("An Internal error occured, Please contact Tenjin support");
			} catch (InvocationTargetException e) {
				logger.error(e.getMessage());
				throw new EDMException("An Internal error occured, Please contact Tenjin support");
			}
			
		}

		return defMngr;

	}
	
	public static List<String> getAvailableTools() throws EDMException{
		List<String> adapters = new ArrayList<String>();
		
		String expPackageName = "com.ycs.tenjin.defect.impl";
		Reflections reflections = new Reflections(expPackageName);
		
		Set<Class<? extends DefectManager>> subTypes = reflections.getSubTypesOf(DefectManager.class);
		for(Class<? extends DefectManager> type:subTypes){
			String packageName = type.getPackage().getName();
			/*Changed by Padmavathi for T25IT-434 starts*/
			if(type.toString().toLowerCase().contains("genericedmimpl")){
				continue;
			}
			/*Changed by Padmavathi for T25IT-434 ends*/
			packageName = packageName.replace(expPackageName + ".", "");
			adapters.add(packageName);
		}
		
		return adapters;
	}
	/*Added by Pushpalatha for TENJINCG-1220 starts*/
	public static DefectManager getDefectTools(String tool, String url, String username, String password,
			String gitOrg) throws EDMException {
		String expPackageName = "com.ycs.tenjin.defect.impl." + tool;
		Reflections reflections = new Reflections(expPackageName);
		Set<Class<? extends DefectManager>> subTypes = reflections.getSubTypesOf(DefectManager.class);
		DefectManager defMngr = null;
		for(Class<? extends DefectManager> type:subTypes){
			
			try {
				Constructor<?> constructor = type.getConstructor(String.class,String.class,String.class,String.class);
				defMngr =  (DefectManager) constructor.newInstance(url,username,password,gitOrg);
			} catch (SecurityException e) {
				logger.error(e.getMessage());
				throw new EDMException("An Internal error occured, Please contact Tenjin support");
			} catch (IllegalArgumentException e) {
				logger.error(e.getMessage());
				throw new EDMException("An Internal error occured, Please contact Tenjin support");
			} catch (NoSuchMethodException e) {
				logger.error(e.getMessage());
				throw new EDMException("An Internal error occured, Please contact Tenjin support");
			} catch (InstantiationException e) {
				logger.error(e.getMessage());
				throw new EDMException("An Internal error occured, Please contact Tenjin support");
			} catch (IllegalAccessException e) {
				logger.error(e.getMessage());
				throw new EDMException("An Internal error occured, Please contact Tenjin support");
			} catch (InvocationTargetException e) {
				logger.error(e.getMessage());
				throw new EDMException("An Internal error occured, Please contact Tenjin support");
			}
			
			
		}

		return defMngr;
		
	}
	/*Added by Pushpalatha for TENJINCG-1220 ends*/
}
