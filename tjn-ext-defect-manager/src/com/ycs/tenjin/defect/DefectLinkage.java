/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DefectLinkage.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 08-Dec-2016			Sriram						Added by Sriram to show defect linkages
*/

package com.ycs.tenjin.defect;

import java.util.List;

public class DefectLinkage {
	private String applicationName;
	private String functionCode;
	private String testCaseId;
	private String testStepId;
	private List<String> tdUids;
	private int appId;
	private int testCaseRecordId;
	private int testStepRecordId;
	private int runId;
	private int projectId;
	private int linkId;
	private int tdUidCount;
	
	public List<String> getTdUids() {
		return tdUids;
	}
	public void setTdUids(List<String> tdUids) {
		this.tdUids = tdUids;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getFunctionCode() {
		return functionCode;
	}
	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}
	public String getTestCaseId() {
		return testCaseId;
	}
	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}
	public String getTestStepId() {
		return testStepId;
	}
	public void setTestStepId(String testStepId) {
		this.testStepId = testStepId;
	}
	public int getAppId() {
		return appId;
	}
	public void setAppId(int appId) {
		this.appId = appId;
	}
	public int getTestCaseRecordId() {
		return testCaseRecordId;
	}
	public void setTestCaseRecordId(int testCaseRecordId) {
		this.testCaseRecordId = testCaseRecordId;
	}
	public int getTestStepRecordId() {
		return testStepRecordId;
	}
	public void setTestStepRecordId(int testStepRecordId) {
		this.testStepRecordId = testStepRecordId;
	}
	public int getRunId() {
		return runId;
	}
	public void setRunId(int runId) {
		this.runId = runId;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public int getLinkId() {
		return linkId;
	}
	public void setLinkId(int linkId) {
		this.linkId = linkId;
	}
	public int getTdUidCount() {
		return tdUidCount;
	}
	public void setTdUidCount(int tdUidCount) {
		this.tdUidCount = tdUidCount;
	}
	
	
	
	
}
