/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DefectManager.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 19-Sep-2016           Sriram Sridharan          Newly Added For
* 07-Oct-2016           Jyoti Ranjan             Added setProjectDesc method.
*/

package com.ycs.tenjin.defect;

import java.util.List;

import org.json.JSONArray;

public interface DefectManager {
	
	public String login() throws UnreachableEDMException, EDMException;
	
	public boolean isUrlValid() throws UnreachableEDMException;
	
	public void postDefect(Defect defect) throws UnreachableEDMException, EDMException;
	
	public List<Defect> postDefects(List<Defect> defects) throws UnreachableEDMException, EDMException;
	
	public Defect getDefect(String defectIdentifier) throws UnreachableEDMException, EDMException;
	
	public List<Defect> getDefects(String[] defectIdentifiers) throws UnreachableEDMException, EDMException;
	
	public List<Defect> getDefects(List<String> defectIdentifiers) throws UnreachableEDMException, EDMException;
	
	public void postDefectAttachment(Defect defect, Attachment attachment) throws UnreachableEDMException, EDMException;
	
	public void postDefectAttachment(String defectIdentifier, String projectIdentifer, Attachment attachment) throws UnreachableEDMException, EDMException;
	
	public void postComment(Defect defect, Comment comment) throws UnreachableEDMException, EDMException;
	
	public List<Attachment> getDefectAttachments(String defectIdentifier, String projectIdentifier) throws UnreachableEDMException, EDMException;
	
	public Attachment getDefectAttachment(String defectIdentifier, String projectIdentifier, String attachmentIdentifier) throws UnreachableEDMException, EDMException;
	
	public List<Attachment> getDefectAttachments(String defectIdentifier, String projectIdentifier, List<String> attachmentIdentifiers) throws UnreachableEDMException, EDMException;
	
	public JSONArray getAllProjects() throws UnreachableEDMException, EDMException;
	
	public void logout() throws UnreachableEDMException, EDMException;
	
	public List<Comment> getComments(String defectIdentifier) throws UnreachableEDMException,EDMException;
	
	public void setProjectDesc(String projectDescription);

	public Defect getDefect(Defect defect) throws EDMException;
	
}
