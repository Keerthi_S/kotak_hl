/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Attachment.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 08-Sep-2016           Sriram Sridharan          Newly Added For 
* 03-Nov-2016           Jyoti Ranjan             Added few parameters
* 16-12-2020			Prem					 TENJINCG-1244	
*/

package com.ycs.tenjin.defect;

public class Attachment {
    private int attachmentRecId;
	private String fileName;
	/*Modified by Prem TENJINCG-1244*/
	private String fileContent;
	/*Modified by Prem TENJINCG-1244*/
	private String attachmentIdentifier;
	private String filePath;
	private String attachmentUrl;
	
	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

	/**
	 * @return the attachmentRecId
	 */
	public int getAttachmentRecId() {
		return attachmentRecId;
	}

	/**
	 * @param attachmentRecId the attachmentRecId to set
	 */
	public void setAttachmentRecId(int attachmentRecId) {
		this.attachmentRecId = attachmentRecId;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the fileContent
	 */
	/*public byte[] getFileContent() {
		return fileContent;
	}

	*//**
	 * @param fileContent the fileContent to set
	 *//*
	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}*/

	/**
	 * @return the attachmentIdentifier
	 */
	public String getAttachmentIdentifier() {
		return attachmentIdentifier;
	}

	/**
	 * @param attachmentIdentifier the attachmentIdentifier to set
	 */
	public void setAttachmentIdentifier(String attachmentIdentifier) {
		this.attachmentIdentifier = attachmentIdentifier;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getAttachmentUrl() {
		return attachmentUrl;
	}

	public void setAttachmentUrl(String attachmentUrl) {
		this.attachmentUrl = attachmentUrl;
	}
	
	
	
	
}
