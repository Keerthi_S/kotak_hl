/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GerericEDMImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 19-Sep-2016           Sriram Sridharan          Newly Added For 
*/

package com.ycs.tenjin.defect.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.defect.Attachment;
import com.ycs.tenjin.defect.Defect;
import com.ycs.tenjin.defect.DefectManager;
import com.ycs.tenjin.defect.EDMException;
import com.ycs.tenjin.defect.UnreachableEDMException;

public abstract class GenericEDMImpl implements DefectManager{
	
	protected String baseUrl;
	protected String userId;
	protected String password;
	/*Added by Pushpalatha for TENJINCG-1220 starts*/
	protected String organization;
	/*Added by Pushpalatha for TENJINCG-1220 ends*/
	protected boolean loginActive;
	private static final Logger logger = LoggerFactory.getLogger(GenericEDMImpl.class);
	
	public GenericEDMImpl(String baseUrl, String userId, String password){
		this.baseUrl = baseUrl;
		this.userId = userId;
		this.password = password;
		this.loginActive = false;
	}
	/*Added by Pushpalatha for TENJINCG-1220 starts*/
	public GenericEDMImpl(String baseUrl, String userId, String password,String organization){
		this.baseUrl = baseUrl;
		this.userId = userId;
		this.password = password;
		this.loginActive = false;
		this.organization=organization;
	}
	/*Added by Pushpalatha for TENJINCG-1220 ends*/
	public abstract void postDefectDetails(Defect defect) throws UnreachableEDMException, EDMException;
	
	
	@Override
	public void postDefect(Defect defect) throws UnreachableEDMException, EDMException {
		
		if(!this.loginActive){
			logger.info("Logging in to EDM as [{}]", this.userId);
			this.login();
		}
		
		logger.info("Posting defect");
		this.postDefectDetails(defect);
		
		if(defect.getAttachments() != null){
			logger.info("Checking for attachments");
			for(Attachment att:defect.getAttachments()){
				logger.info("Posting attachment");
				this.postDefectAttachment(defect, att);
			}
		}
		
		
	}
	
}
