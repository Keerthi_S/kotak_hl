/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Defect.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 08-Sep-2016           Sriram Sridharan          Newly Added For 
* 06-Oct-2016           Jyoti Ranjan              Added few parameters
* 08-Dec-2016			Sriram						Changed by Sriram to show defect linkages
* 10-Jan-2017			Manish					TENJINCG-5
* 11-12-2020			Pushpalatha				TENJINCG-1221
*/

package com.ycs.tenjin.defect;

import java.sql.Timestamp;
import java.util.List;

public class Defect {
	
	private int recordId;
	private String identifier;
	private String summary;
	private String description;
	private Timestamp createdOn;
	private String createdBy;
	private Timestamp modifiedOn;
	private String modifiedBy;
	private String severity;
	private String priority;
	private String status;
	private List<Attachment> attachments;
	private int appId; 
	private String appName; 
	private String functionCode;
	private int runId;
	private String transactionId;
	private String testCycle;
	private String testRound;
	private int testCaseId;
	private int testStepId;
	private String tenjinProjectName; 
	private String tenjinDomainName; 
	private String edmProjectName;
	private String assignedTo;
	private String applicationVersion;
	private List<Comment> comments;
	private String defectType;
	
	private String tjnStatus;
	private String remarks;
	private String defectReason;
	private String postType;
	private String testSet;
	private String tdUId;
	private String posted;
	private String dttInstanceName;
	private Timestamp lastRefreshed;
	
	private int linkCount;
	private String runId_status;
	/*Added by Pushpalatha for TENJINCG-1221 starts*/
	private String repoOwner;
	
	private String organization;
	
	
	
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getRepoOwner() {
		return repoOwner;
	}
	public void setRepoOwner(String repoOwner) {
		this.repoOwner = repoOwner;
	}
	/*Added by Pushpalatha for TENJINCG-1221 ends*/
	private String rec_status;
	
	public String getRec_status() {
		return rec_status;
	}
	public void setRec_status(String rec_status) {
		this.rec_status = rec_status;
	}
	
	/*changed by manish for req TENJINCG-5 on 10-Jan-2017 ends*/

	
	
	/***********
	 * Added by Sriram for TJN_24_06_2
	 */
	private List<DefectLinkage> linkages;
	
	
	public List<DefectLinkage> getLinkages() {
		return linkages;
	}
	public void setLinkages(List<DefectLinkage> linkages) {
		this.linkages = linkages;
	}
	/***********
	 * Added by Sriram for TJN_24_06_2
	 */
	public int getLinkCount() {
		return linkCount;
	}
	public void setLinkCount(int linkCount) {
		this.linkCount = linkCount;
	}
	public Timestamp getLastRefreshed() {
		return lastRefreshed;
	}
	public void setLastRefreshed(Timestamp lastRefreshed) {
		this.lastRefreshed = lastRefreshed;
	}
	public String getPosted() {
		return posted;
	}
	public void setPosted(String posted) {
		this.posted = posted;
	}
	public String getDttInstanceName() {
		return dttInstanceName;
	}
	public void setDttInstanceName(String dttInstanceName) {
		this.dttInstanceName = dttInstanceName;
	}
	public String getDefectType() {
		return defectType;
	}
	public void setDefectType(String defectType) {
		this.defectType = defectType;
	}
	/**
	 * @return the assignedTo
	 */
	public String getAssignedTo() {
		return assignedTo;
	}
	/**
	 * @param assignedTo the assignedTo to set
	 */
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	/**
	 * @return the applicationVersion
	 */
	public String getApplicationVersion() {
		return applicationVersion;
	}
	/**
	 * @param applicationVersion the applicationVersion to set
	 */
	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}
	/**
	 * @return the testCaseId
	 */
	public int getTestCaseId() {
		return testCaseId;
	}
	/**
	 * @param testCaseId the testCaseId to set
	 */
	public void setTestCaseId(int testCaseId) {
		this.testCaseId = testCaseId;
	}
	/**
	 * @return the testStepId
	 */
	public int getTestStepId() {
		return testStepId;
	}
	/**
	 * @param testStepId the testStepId to set
	 */
	public void setTestStepId(int testStepId) {
		this.testStepId = testStepId;
	}
	/**
	 * @return the tenjinProjectName
	 */
	public String getTenjinProjectName() {
		return tenjinProjectName;
	}
	/**
	 * @param tenjinProjectName the tenjinProjectName to set
	 */
	public void setTenjinProjectName(String tenjinProjectName) {
		this.tenjinProjectName = tenjinProjectName;
	}
	/**
	 * @return the tenjinDomainName
	 */
	public String getTenjinDomainName() {
		return tenjinDomainName;
	}
	/**
	 * @param tenjinDomainName the tenjinDomainName to set
	 */
	public void setTenjinDomainName(String tenjinDomainName) {
		this.tenjinDomainName = tenjinDomainName;
	}
	/**
	 * @return the edmProjectName
	 */
	public String getEdmProjectName() {
		return edmProjectName;
	}
	/**
	 * @param edmProjectName the edmProjectName to set
	 */
	public void setEdmProjectName(String edmProjectName) {
		this.edmProjectName = edmProjectName;
	}
	/**
	 * @return the recordId
	 */
	public int getRecordId() {
		return recordId;
	}
	/**
	 * @param recordId the recordId to set
	 */
	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}
	/**
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}
	/**
	 * @param object the identifier to set
	 */
	public void setIdentifier(String object) {
		this.identifier = object;
	}
	/**
	 * @return the summary
	 */
	public String getSummary() {
		return summary;
	}
	/**
	 * @param summary the summary to set
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the createdOn
	 */
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the modifiedOn
	 */
	public Timestamp getModifiedOn() {
		return modifiedOn;
	}
	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the severity
	 */
	public String getSeverity() {
		return severity;
	}
	/**
	 * @param severity the severity to set
	 */
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	/**
	 * @return the priority
	 */
	public String getPriority() {
		return priority;
	}
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the attachments
	 */
	public List<Attachment> getAttachments() {
		return attachments;
	}
	/**
	 * @param attachments the attachments to set
	 */
	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}
	/**
	 * @return the appId
	 */
	public int getAppId() {
		return appId;
	}
	/**
	 * @param appId the appId to set
	 */
	public void setAppId(int appId) {
		this.appId = appId;
	}
	/**
	 * @return the appName
	 */
	public String getAppName() {
		return appName;
	}
	/**
	 * @param appName the appName to set
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}
	/**
	 * @return the functionCode
	 */
	public String getFunctionCode() {
		return functionCode;
	}
	/**
	 * @param functionCode the functionCode to set
	 */
	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}
	/**
	 * @return the runId
	 */
	public int getRunId() {
		return runId;
	}
	/**
	 * @param runId the runId to set
	 */
	public void setRunId(int runId) {
		this.runId = runId;
	}
	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the testCycle
	 */
	public String getTestCycle() {
		return testCycle;
	}
	/**
	 * @param testCycle the testCycle to set
	 */
	public void setTestCycle(String testCycle) {
		this.testCycle = testCycle;
	}
	/**
	 * @return the testRound
	 */
	public String getTestRound() {
		return testRound;
	}
	/**
	 * @param testRound the testRound to set
	 */
	public void setTestRound(String testRound) {
		this.testRound = testRound;
	}
	
	/**
	 * @return the comments
	 */
	 public List<Comment> getComments() {
			return comments;
		}
	 
	 /**
		 * @param comments the comments to set
		 */
		public void setComments(List<Comment> comments) {
			this.comments = comments;
		}
		
	/**
	 * @return the tjnStatus
	 */
	public String getTjnStatus() {
		return tjnStatus;
	}
	/**
	 * @param tjnStatus the tjnStatus to set
	 */
	public void setTjnStatus(String tjnStatus) {
		this.tjnStatus = tjnStatus;
	}
	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}
	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	/**
	 * @return the defectReason
	 */
	public String getDefectReason() {
		return defectReason;
	}
	/**
	 * @param defectReason the defectReason to set
	 */
	public void setDefectReason(String defectReason) {
		this.defectReason = defectReason;
	}
	/**
	 * @return the postType
	 */
	public String getPostType() {
		return postType;
	}
	/**
	 * @param postType the postType to set
	 */
	public void setPostType(String postType) {
		this.postType = postType;
	}
	/**
	 * @return the testSet
	 */
	public String getTestSet() {
		return testSet;
	}
	/**
	 * @param testSet the testSet to set
	 */
	public void setTestSet(String testSet) {
		this.testSet = testSet;
	}
	/**
	 * @return the tdUId
	 */
	public String getTdUId() {
		return tdUId;
	}
	/**
	 * @param tdUId the tdUId to set
	 */
	public void setTdUId(String tdUId) {
		this.tdUId = tdUId;
	}
	
	
	/* Added by Sriram for API testing */
	private String apiCode;
	private String operation;

	public String getApiCode() {
		return apiCode;
	}
	public void setApiCode(String apiCode) {
		this.apiCode = apiCode;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	/* Added by Sriram for API testing ends*/
	/*added by shruthi for TENJINCG-1224 starts*/
	public String getRunId_status() {
		return runId_status;
	}
	public void setRunId_status(String runId_status) {
		this.runId_status = runId_status;
	}
	/*added by shruthi for TENJINCG-1224 ends*/
	
	
	/*added by paneedra for TENJINCG-1262 starts*/
	private String projObjId;
	private String defObjId;

	public String getProjObjId() {
		return projObjId;
	}
	public void setProjObjId(String projObjId) {
		this.projObjId = projObjId;
	}
	public String getDefObjId() {
		return defObjId;
	}
	public void setDefObjId(String defObjId) {
		this.defObjId = defObjId;
	}
	/*added by paneedra for TENJINCG-1262 ends*/
	
	
}
