/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GenericExecutorImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * 24-Sep-2016           Sriram Sridharan          Newly Added For 
 * 18-Nov-2016			 nagareddy				Manual input field
 * 20-Dec-2016			 Sriram					Changed visibility of screenshotIndex to protected for FCUBS
 * 17-02-2016            Leelaprasad             TENJINCG-116
 * 16-06-2017			 Sriram Sridharan		TENJINCG-187
 * 30-06-2017			 Padmavathi	            TENJINCG-261
 * 27-Jul-2017			Sriram Sridharan		TENJINCG-311
 * 18-Aug-2017			Sriram Sridahran		T25IT-240
 * 30-08-2017			 Sriram Sridharan	    T25IT-339,341,344
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 11-10-2018           Leelaprasad              TENJINCG-854
 * 26-10-2018           Padmavathi               TENJINCG-783
 * 09-01-2019			Pushpa					 TJN252-64
 */

package com.ycs.tenjin.bridge.selenium.asv1.taskhandler.task;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.bridge.constants.TenjinConstants;
import com.ycs.tenjin.bridge.enums.PageType;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.exceptions.RunAbortedException;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.pojo.run.ExecutionStep;
import com.ycs.tenjin.bridge.pojo.run.Formula;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.pojo.run.RuntimeFieldValue;
import com.ycs.tenjin.bridge.pojo.run.RuntimeScreenshot;
import com.ycs.tenjin.bridge.pojo.run.UIValidationResult;
import com.ycs.tenjin.bridge.pojo.run.ValidationResult;
import com.ycs.tenjin.bridge.selenium.asv1.SeleniumApplicationImpl;
import com.ycs.tenjin.bridge.selenium.asv1.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv1.generic.ElementIdentificationException;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.util.PatternMatch;
import com.ycs.tenjin.util.Utilities;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public abstract class ExecutorTaskAbstract implements ExecutorTask {

	private static final Logger logger = LoggerFactory.getLogger(ExecutorTaskAbstract.class);

	protected IterationStatus iStatus = null;
	protected List<ValidationResult> results = new ArrayList<ValidationResult>();
	protected List<RuntimeScreenshot> runtimeScreenshots = null;
	protected List<RuntimeFieldValue> runtimeValues = null;
	protected ExecutionStep step;
	protected int runId;
	protected int iteration;
	protected int fieldTimeout;
	protected int screenshotOption;
	protected String tdUid;

	/* For TENJINCG-311 */
	protected List<UIValidationResult> uiValidationResults = new ArrayList<UIValidationResult>();
	protected JSONObject uiValidationJson;
	/* For TENJINCG-311 ends */
	
	/***************
	 * Changed visibility of screenshotIndex to protected for FCUBS
	 */
	/*private int screenshotIndex;*/
	protected int screenshotIndex;
	/***************
	 * Changed visibility of screenshotIndex to protected for FCUBS ends
	 */

	public ExecutorTaskAbstract(int runId, ExecutionStep step,  int screenshotOption, int fieldTimeout){
		this.runId = runId;
		this.step = step;
		this.screenshotOption = screenshotOption;
		this.fieldTimeout = fieldTimeout;
		/*Changed by Pushpa for  TJN252-64 starts*/
		this.screenshotIndex = 1;
		/*Changed by Pushpa for  TJN252-64 ends*/
		
		/* For TENJINCG-311 */
		if(Utilities.trim(this.step.getUiValidationString()).length() > 0) {
			try {
				this.uiValidationJson = new JSONObject(this.step.getUiValidationString());
			}catch(JSONException e) {
				logger.warn("Could not load UI Validation descriptor. UI Validations may not happen");
			}
		}
		/* For TENJINCG-311 ends*/
	}
	
	public ExecutorTaskAbstract() {
		this.screenshotOption = 0;
		this.fieldTimeout = 0;
		/*Changed by Pushpa for  TJN252-64 starts*/
		this.screenshotIndex = 1;
		/*Changed by Pushpa for  TJN252-64 ends*/
	}


	@Override
	/* Changed by Sriram for TENJINCG-187 */
	/*public IterationStatus executeIteration(JSONObject dataSet, Application oApplication, int iteration){*/
	public IterationStatus executeIteration(JSONObject dataSet, SeleniumApplicationImpl oApplication, int iteration) throws RunAbortedException{
	/* Changed by Sriram for TENJINCG-187 ends*/
		this.iteration = iteration;
		
		this.iStatus = new IterationStatus();
		this.results = new ArrayList<ValidationResult>();
		this.runtimeScreenshots = new ArrayList<RuntimeScreenshot>();
		this.runtimeValues = new ArrayList<RuntimeFieldValue>();
		logger.debug("entered executeIteration()");
		DriverContext driverContext = oApplication.getDriverContext();
		logger.debug("Dataset for Iteration {}, Step {}, Record ID {} is as follows", iteration, step.getId(), step.getRecordId());
		logger.debug(dataSet.toString());


		try {
			tdUid = dataSet.getString("TDUID");

			JSONArray pageAreas = dataSet.getJSONArray("PAGEAREAS");
			int pageAreaCount = pageAreas.length();

			logger.info("Retrieved Pageareas to traverse. This step needs to traverse through {} pageareas", pageAreaCount);

			try{
				/* Added by Sriram for TENJINCG-187 */
				logger.debug("Checking if thread for run {} is alive", this.runId);
				if(CacheUtils.isRunAborted(this.runId)) {
					logger.debug("Thread for Run {} is interrupted.", this.runId);
					throw new RunAbortedException();
				}
				
				logger.debug("Thread for Run {} is still alive", this.runId);
				/* Added by Sriram for TENJINCG-187 ends*/
				this.processAllPageAreas(tdUid, pageAreas, driverContext, oApplication);
			}catch(ExecutorException e){
				this.iStatus.setResult("E");
				this.iStatus.setMessage(e.getMessage());
				this.captureFailureScreenshot(tdUid, oApplication);
				return this.iStatus;
			}

			//Once all pages are completed, commit the operation
			/* Added by Sriram for TENJINCG-187 */
			logger.debug("Checking if thread for run {} is alive", this.runId);
			if(CacheUtils.isRunAborted(this.runId)) {
				logger.debug("Thread for Run {} is interrupted.", this.runId);
				throw new RunAbortedException();
			}
			
			logger.debug("Thread for Run {} is still alive", this.runId);
			/* Added by Sriram for TENJINCG-187 ends*/
			this.postAllPageAreaProcess(driverContext, oApplication);

			/* Added by Sriram for TENJINCG-187 */
			logger.debug("Checking if thread for run {} is alive", this.runId);
			if(CacheUtils.isRunAborted(this.runId)) {
				logger.debug("Thread for Run {} is interrupted.", this.runId);
				throw new RunAbortedException();
			}
			
			logger.debug("Thread for Run {} is still alive", this.runId);
			/* Added by Sriram for TENJINCG-187 ends*/
			this.captureEndOfExecutionScreenshot(tdUid, oApplication);

		} catch (JSONException e) {
			
			logger.error("ERROR in Test Data JSON --> {}", e.getMessage(), e);
			this.iStatus.setResult("E");
			this.iStatus.setMessage("Could not process Test data for this step due to an internal error");
		}finally{
			this.iStatus.setRuntimeFieldValues(this.runtimeValues);
			this.iStatus.setRuntimeScreenshots(this.runtimeScreenshots);
			this.iStatus.setValidationResults(this.results);
			/* Added by Sriram for TENJINCG-311 */
			this.iStatus.setUiValidationResults(this.uiValidationResults);
			/* Added by Sriram for TENJINCG-311 ends*/
		}



		return this.iStatus;
	}

	/* Changed by Sriram for TENJINCG-187 */
	/*protected void processAllPageAreas(String tdUid, JSONArray pageAreas, DriverContext driverContext, Application oApplication) throws ExecutorException{*/
	protected void processAllPageAreas(String tdUid, JSONArray pageAreas, DriverContext driverContext, SeleniumApplicationImpl oApplication) throws ExecutorException, RunAbortedException{
	/* Changed by Sriram for TENJINCG-187 ends*/
		try {
			int pageAreaCount = pageAreas.length();
			for(int i=0; i< pageAreaCount; i++){
				JSONObject pageArea = pageAreas.getJSONObject(i);

				Location location = Location.loadFromJson(pageArea);

				logger.info("Navigating to page {}", location.getLocationName());
				/* Added by Sriram for TENJINCG-187 */
				logger.debug("Checking if thread for run {} is alive", this.runId);
				if(CacheUtils.isRunAborted(this.runId)) {
					logger.debug("Thread for Run {} is interrupted.", this.runId);
					throw new RunAbortedException();
				}
				
				logger.debug("Thread for Run {} is still alive", this.runId);
				/* Added by Sriram for TENJINCG-187 ends*/
				this.navigateToPage(location, driverContext, oApplication);

				/* Commented by sameer for New Adapter Spec */
//				JSONArray fields = null;
//				int fieldsCount = 0;	

				JSONArray detailRecords = null;

				detailRecords = pageArea.getJSONArray("DETAILRECORDS");

				/* Added by Sriram for TENJINCG-187 */
				logger.debug("Checking if thread for run {} is alive", this.runId);
				if(CacheUtils.isRunAborted(this.runId)) {
					logger.debug("Thread for Run {} is interrupted.", this.runId);
					throw new RunAbortedException();
				}
				
				logger.debug("Thread for Run {} is still alive", this.runId);
				/* Added by Sriram for TENJINCG-187 ends*/
				
				/* For TENJINCG-311 */
				if(this.uiValidationJson != null) {
					try {
						JSONArray uiValidationArray = this.uiValidationJson.getJSONArray(location.getLocationName());
						if(uiValidationArray != null && uiValidationArray.length() > 0) {
							this.performUIValidationsOnPageArea(location.getLocationName(), uiValidationArray, driverContext, oApplication, tdUid, "1");
						}
					}catch(Exception e) {
						logger.debug("No UI Validations for this page");
					}
				}
				/* For TENJINCG-311 ends*/
				
				if(!location.isMultiRecordBlock()){
					this.processSingleRecordPageArea(detailRecords, location, driverContext, oApplication, tdUid);
				}else{
					//This is MRB
					this.processMultiRecordPageArea(detailRecords, location, driverContext, oApplication, tdUid);
				}

				/* Added by Sriram for TENJINCG-187 */
				logger.debug("Checking if thread for run {} is alive", this.runId);
				if(CacheUtils.isRunAborted(this.runId)) {
					logger.debug("Thread for Run {} is interrupted.", this.runId);
					throw new RunAbortedException();
				}
				
				logger.debug("Thread for Run {} is still alive", this.runId);
				/* Added by Sriram for TENJINCG-187 ends*/
				if(location.getLocationName().equalsIgnoreCase("List Of Entries")){
					logger.info("Ignoring for {}", location.getLocationName());
				}else{
					this.capturePageSnapshot(tdUid, oApplication);
				}


			}
		} catch(ExecutorException e){
			throw e;
		}catch(Exception e){
			logger.error("ERROR processing page areas", e);
			throw new ExecutorException("Execution aborted abnormally due to an unknown error", e);
		}
	}
	
	protected void navigateToPage(Location location, DriverContext driverContext, SeleniumApplicationImpl oApplication) throws ExecutorException{

		try {
			if(PageType.LINK.toString().equalsIgnoreCase(location.getLocationType())){
				this.navigateToLink(location, driverContext, oApplication);
			}else if(PageType.TAB.toString().equalsIgnoreCase(location.getLocationType())){
				/********************************************************************************************
				 * Changed by Sharath with -- >> location.getLocationName() -- >> from location.getWayIn() Starts
				 */
				this.navigateToTab(location.getLocationName(), driverContext, oApplication);
				/********************************************************************************************
				 * Changed by Sharath with -- >> location.getLocationName() -- >> from location.getWayIn() Ends
				 */
			}else if(PageType.MAIN.toString().equalsIgnoreCase(location.getLocationType())){
				logger.info("Context is on main page already");
			}else if(PageType.ACCORDION.toString().equalsIgnoreCase(location.getLocationType())){
				this.navigateToAccordion(location, driverContext, oApplication);
			}else if(PageType.PAGE.toString().equalsIgnoreCase(location.getLocationType())){
				this.navigateToPagination(location, driverContext, oApplication);
			}else{
				logger.error("Don't know what to do yet");
				this.navigateToLink(location, driverContext, oApplication);
			}
		} catch (Exception e) {
			logger.error("Could not navigate to {}", location.getLocationName(), e);
			throw new ExecutorException("Could not navigate to page " + location.getLocationName());
		}

	}
	
	protected void navigateToTab(String tabName, DriverContext driverContext, SeleniumApplicationImpl oApplication) throws ExecutorException{

		try{
			if(tabName.contains("-")){
				tabName = tabName.replace("-", "/");
			}
			
			logger.debug("Navigating to tab {}", tabName);
			driverContext = oApplication.navigateToTab(tabName);
		}catch(Exception e){
			throw new ExecutorException("Could not navigate to tab " + tabName, e);
		}

	}

	protected void navigateToLink(Location location, DriverContext driverContext, SeleniumApplicationImpl oApplication) throws ExecutorException{

		String wayIn = location.getWayIn();
		logger.info("Way In Value is : "+ wayIn);

		try {
			String[] arr = null;

			if(Utilities.trim(wayIn).contains(";")){
				arr = wayIn.split(";");
				if(arr[0].toLowerCase().startsWith("toolbar")){
					logger.debug("Navigating to toolbar item {}", arr[1]);
					driverContext = oApplication.clickToolbarItem(arr[1]);
				}/*************************************************************************
				 * Added by Sharath for Handling Frames and links Starts
				 */
				else if(arr[0].toLowerCase().startsWith("navigationlink")){
					logger.debug("Navigating to link item {}", arr[1]);
					driverContext = oApplication.clickNavigationItem(arr[1]);
				}
				else{
					driverContext = oApplication.navigateToLink(arr[1]);
				}
				/*************************************************************************
				 * Added by Sharath for Handling Frames and links Ends
				 */
			}
			
			else{
				logger.debug("Clicking link {}", wayIn);
				driverContext = oApplication.navigateToLink(wayIn);
			}
		} catch (AutException e) {
			throw new ExecutorException("Could not navigate to link " + wayIn, e);
		}

	}
	
	protected void navigateToAccordion(Location location, DriverContext driverContext, SeleniumApplicationImpl oApplication) throws ExecutorException{

		String wayIn = location.getWayIn();
		logger.info("Way In Value is : "+ wayIn);
		if(Utilities.trim(wayIn).equalsIgnoreCase("")){
			throw new ExecutorException("Way-In is not defined for Page " + location.getLocationName() + ". Please re-learn this function");
		}

		try {
				
			driverContext = oApplication.navigateToAccordion(wayIn);
		} catch (AutException e) {
			throw new ExecutorException("Could not navigate to link " + wayIn, e);
		} catch (ElementIdentificationException e) {
			
			throw new ExecutorException("Could not navigate to link " + wayIn, e);
		}

	}
	
	protected void navigateToPagination(Location location, DriverContext driverContext, SeleniumApplicationImpl oApplication) throws ExecutorException{

		String wayIn = location.getWayIn();
		logger.info("Way In Value is : "+ wayIn);
		if(Utilities.trim(wayIn).equalsIgnoreCase("")){
			throw new ExecutorException("Way-In is not defined for Page " + location.getLocationName() + ". Please re-learn this function");
		}

		try {
				
			driverContext = oApplication.navigateToPagination(wayIn);
		} catch (AutException e) {
			throw new ExecutorException("Could not navigate to link " + wayIn, e);
		} catch (ElementIdentificationException e) {
			
			throw new ExecutorException("Could not navigate to link " + wayIn, e);
		}

	}



	public void processMultiRecordPageArea(JSONArray detailRecords, Location location, DriverContext driverContext, SeleniumApplicationImpl oApplication, String tdUid2) {
		

	}


	protected void processSingleRecordPageArea(JSONArray detailRecords, Location location, DriverContext driverContext, SeleniumApplicationImpl oApplication, String tdUid) throws ExecutorException{

		try {
			if(detailRecords == null){
				logger.error("DETAILRECORDS is null for page area {}", location.getLocationName());
				return;
			}

			logger.info("{} detail records are present for page {}", detailRecords.length(), location.getLocationName());



			logger.debug("Getting master record at index {}",0);
			JSONObject detail = detailRecords.getJSONObject(0);
			String dId = detail.getString("DR_ID");
			logger.info("Executing --> Page [{}], Detail Record [{}]", location.getLocationName(), dId);

			logger.debug("Getting all fields JSONArray for detail record {}", dId);
			JSONArray fields = detail.getJSONArray("FIELDS");

			/* Commented by sameer for New Adapter Spec */
//			boolean groupActive = false;
//			String activeGroup = "";

			for(int i=0; i<fields.length(); i++){

				logger.debug("Getting field information at index {}", i);
				JSONObject field = fields.getJSONObject(i);

				logger.debug("Loading test object");
				TestObject t = TestObject.loadFromJson(field);
				if(t == null){
					logger.error("Could not load TestObject from JSON");
					continue;
				}

				logger.debug("Processing I/O --> Field [{}], Page [{}]", t.getLabel(), location.getLocationName());
				this.processField(oApplication, driverContext, location, t, tdUid);


			}
		}  catch (JSONException e) {
			
			throw new ExecutorException(e.getMessage(),e );
		}



	}

	protected void processField(SeleniumApplicationImpl oApplication, DriverContext driverContext, Location location, TestObject t, String tdUid2) {		
		try {
			if(t.getLabel().equalsIgnoreCase("Permenant Address")){
				System.err.println();
			}
			logger.debug("Locating element {} on page {}",t.getLabel(), location.getLocationName());
			/* Changed by Padmavathi for Req# Tenjincg-261 30-06-2017 starts*/
			/*WebElement element = oApplication.locateElement(t.getUniqueId(), t.getIdentifiedBy());*/
		        t.setLocation(location.getLocationName());
			WebElement element = oApplication.locateElement(t.getUniqueId(), t.getIdentifiedBy(),t);
			/* Changed by Padmavathi for Req# Tenjincg-261 30-06-2017 ends*/
			
			if(element == null){
				System.err.println();
			}
			/*************
			 * added by Nagareddy for Manual input field
			 */
			 if(t.getAction().equalsIgnoreCase("MANUAL")||t.getData().equalsIgnoreCase("@@MANUAL")){				
						logger.info("performing Manual on element");
				
					//1. Create a cache manager
						CacheManager cm = CacheManager.getInstance();
						
						//2. Create a cache called "cacheStore"
						try{
						cm.addCache("cacheStore");
						}catch(Exception e){}

						//3. Get a cache called "cacheStore"
						Cache cache = cm.getCache("cacheStore"); 
				
					//4. Put few elements in cache
						t.setData("false");
						t.setLocation(location.getLocationName());
						cache.put(new Element(this.runId,t));
						//5. Get element from cache
						Element ele = cache.get(this.runId);
						 /*Changed by Leelaprasad for the requirement Defect fix TENJINCG-116 starts*/
						try{
							String execConfigPath = TenjinConfiguration.getProperty("MANUAL_TIME").trim();
							if(execConfigPath!=null && !execConfigPath.equalsIgnoreCase(""))
							{
								int idletimetolive=Integer.parseInt(execConfigPath);
								int seconds=60;
								ele.setTimeToLive(idletimetolive*seconds);
								}					
							}catch(Exception e){}
						 /*Changed by Leelaprasad for the requirement Defect fix TENJINCG-116 ends*/

						//6. Print out the element
						TestObject output = (TestObject) (ele == null ? null : ele.getObjectValue());
						System.out.println("In Manual "+t.getLabel()+" "+output.getData());		
						
						try{
							this.performPause(element,t,location.getLocationName(),oApplication,output.getData());
							if(output.getData()==null){
								logger.error("ERROR while performing Manual I/O on page {}", location.getLocationName());
								throw new ExecutorException("Could not perform Manual Input/Output on page " + location.getLocationName() + " due to an unknown error");
							}
									}catch(Exception er){
										logger.error("ERROR while performing I/O on page {}", location.getLocationName(), er);
										throw new ExecutorException("Could not perform Manual Input/Output on page " + location.getLocationName() + " due to an unknown error");
									}
			
			}			
			else if(t.getAction().equalsIgnoreCase("input")){
				logger.debug("Performing Input/Output");

				if(Utilities.trim(t.getData()).equalsIgnoreCase("@@OUTPUT")){
					String output = oApplication.getFieldValue(element, t);
					RuntimeFieldValue r = this.getRuntimeFieldValueObject(t.getLabel(), output, 1, tdUid);
					this.runtimeValues.add(r);
				}else{
					logger.debug("Performing input");
					oApplication.performInput(element, t, t.getData());
					logger.info("Performed Input -- Field {}, Page {}, Data {}", t.getLabel(), location.getLocationName(), t.getData());
				}
			}else if(t.getAction().equalsIgnoreCase("verify")){
				String output = oApplication.getFieldValue(element, t);
				ValidationResult valRes = this.getValidationResultObject(location.getLocationName(), t.getLabel(), t.getData(), output,"1");
				this.results.add(valRes);
			}
		} catch (ElementIdentificationException e) {
			
			throw new ExecutorException(e.getMessage(),e);
		} catch (AutException e) {
			
			throw new ExecutorException(e.getMessage(),e);
		} catch(Exception e){
			logger.error("UNKNOWN ERROR occurred while processing field [{}] on page [{}]", t.getLabel(), location.getLocationName(), e);
			throw new ExecutorException("Could not process field [" + t.getLabel() + "] on page [" + location.getLocationName() + "]. Please contact Tenjin Support.");
		}}

	protected RuntimeFieldValue getRuntimeFieldValueObject(String field, String value, int detailRecordNo, String tdUid){
		RuntimeFieldValue r = new RuntimeFieldValue();
		/*Changed by Leelaprasad for the TENJINCG-854 starts*/
		/*r.setDetailRecordNo(1);*/
		r.setDetailRecordNo(detailRecordNo);
		/*Changed by Leelaprasad for the TENJINCG-854 ends*/
		r.setField(field);
		r.setIteration(this.iteration);
		r.setRunId(this.runId);
		r.setTdGid(this.step.getTdGid());
		r.setTdUid(tdUid);
		r.setTestStepRecordId(step.getRecordId());
		r.setValue(value);

		logger.info(r.toString());
		return r;
	}

	protected ValidationResult getValidationResultObject(String page, String field, String expectedValue, String actualValue, String detailRecordNo) {
		ValidationResult r = new ValidationResult();
		actualValue = Utilities.trim(actualValue);
		r.setActualValue(actualValue);
		r.setExpectedValue(expectedValue);
		r.setDetailRecordNo(detailRecordNo);
		r.setField(field);
		r.setPage(page);
		r.setIteration(this.iteration);
		r.setResValId(0);
		r.setRunId(this.runId);
		r.setTestStepRecordId(this.step.getRecordId());

		if(Utilities.isNumeric(actualValue) && Utilities.isNumeric(expectedValue)){
			if(Utilities.isNumericallyEqual(actualValue, expectedValue)){
				r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
			}else{
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		}else if(actualValue.equalsIgnoreCase(expectedValue)){
			/*************
			 * Fix by Sriram for comparision of numeric values Ends
			 */

			r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
		}else{

			if(Utilities.isNumeric(expectedValue) && Utilities.isNumeric(actualValue)){
				if(Double.parseDouble(expectedValue) == Double.parseDouble(actualValue)){
					r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
				}else{
					r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
				}
			}else{
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		}

		logger.info(r.toString());
		return r;

	}

	protected void capturePageSnapshot(String tdUid, SeleniumApplicationImpl oApplication) throws ExecutorException{
		if(this.screenshotOption > 2){
			RuntimeScreenshot ras = this.getRuntimeScreenshotObject(tdUid, "End Of Page", oApplication);
			this.runtimeScreenshots.add(ras);
		}
	}

	protected void captureMessageScreenshot(String tdUid, SeleniumApplicationImpl oApplication) throws ExecutorException{

		if(this.screenshotOption > 1){
			RuntimeScreenshot ras = this.getRuntimeScreenshotObject(tdUid, "Message", oApplication);
			this.runtimeScreenshots.add(ras);
		}
	}

	protected void captureEndOfExecutionScreenshot(String tdUid, SeleniumApplicationImpl oApplication) throws ExecutorException{
		if(this.screenshotOption > 2){
			RuntimeScreenshot ras = this.getRuntimeScreenshotObject(tdUid, "End of Execution", oApplication);
			this.runtimeScreenshots.add(ras);
		}
	}

	protected void captureFailureScreenshot(String tdUid, SeleniumApplicationImpl oApplication) throws ExecutorException{
		

		if(this.screenshotOption > 0){
			RuntimeScreenshot ras = this.getRuntimeScreenshotObject(tdUid, "Failure/Error", oApplication);
			this.runtimeScreenshots.add(ras);
		}
	}

	protected RuntimeScreenshot getRuntimeScreenshotObject(String tdUid, String screenshotType, SeleniumApplicationImpl oApplication) {

		RuntimeScreenshot ras = new RuntimeScreenshot();

		ras.setIteration(this.iteration);
		ras.setRunId(this.runId);
		ras.setSequence(this.screenshotIndex++);
		ras.setTdUid(tdUid);
		ras.setTdGid(this.step.getTdGid());
		ras.setTestStepRecordId(this.step.getRecordId());
		ras.setTimestamp(new Timestamp(new Date().getTime()));
		ras.setType(screenshotType);
		try{
			ras.setScreenshot(oApplication.captureScreenshotAsFile());
		}catch(Exception e){
			logger.error("ERROR capturing {} screenshot", screenshotType, e);
		}

		return ras;
	}
	
	
	protected abstract void sayHello();
	
	protected abstract void postAllPageAreaProcess(DriverContext driverContext, SeleniumApplicationImpl oApplication) throws ExecutorException;

	protected abstract void checkPostTransactionResult(DriverContext driverContext, SeleniumApplicationImpl oApplication, String functionCode) throws ExecutorException;
	
	protected abstract void performPreQueryActions(DriverContext driverContext, SeleniumApplicationImpl oApplication) throws ExecutorException;
	
	protected abstract void performPostQueryActions(DriverContext driverContext, SeleniumApplicationImpl oApplication) throws ExecutorException;
	public void performPause(WebElement element,TestObject t, String locationName,SeleniumApplicationImpl oApplication,String output) {
		
		
		WebElement dataElement=element.findElement(By.id(t.getUniqueId()));
		
		if(dataElement.getAttribute("value")!=null  && output!=null && output.equalsIgnoreCase("false") && (dataElement.getAttribute("value").equalsIgnoreCase("") ||dataElement.getAttribute("value").length()>0))
		{
			System.out.println("Pausing ..."+t.getLabel()+" "+output);
			dataElement.click();	
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				
				logger.error("Error ", e1);
			}
			CacheManager cm = CacheManager.getInstance();		
			//3. Get a cache called "cacheStore"
			Cache cache = cm.getCache("cacheStore");					
			//5. Get element from cache
			Element ele = cache.get(this.runId);							
			TestObject continueTrue = (TestObject) (ele == null ? null : ele.getObjectValue());
			if(continueTrue!=null && continueTrue.getData().equalsIgnoreCase("true")){
				//remove the object								
				System.out.println("removing the element from cahce");
				cache.remove(this.runId);	
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					
					logger.error("Error ", e1);
				}
				cm.shutdown();
			}
			else{
				try{
				this.performPause(element, t, locationName, oApplication,continueTrue.getData());
				
				}catch(NullPointerException e){
					return ;
				}
				
			}	
		}					
		else if(dataElement.getAttribute("value")!=null  && output!=null && output.equalsIgnoreCase("true"))
		{
			System.out.println("In Output Is true");
			CacheManager cm = CacheManager.getInstance();							
			//3. Get a cache called "cacheStore"
			Cache cache = cm.getCache("cacheStore");							
			//5. Get element from cache
			/* Commented by sameer for New Adapter Spec */
//			Element ele = cache.get(this.runId);							
			//remove the object
			cache.remove(this.runId);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				
				logger.error("Error ", e1);
			}					
		}
		
	}
	
	/* For TENJINCG-311 */
	protected UIValidationResult getUIValidationResultObject(int valRecId, String page, String field, String expectedValue, String actualValue, String detailRecordNo) {
		UIValidationResult r = new UIValidationResult();
		r.setUiValRecId(valRecId);
		r.setPage(page);
		r.setField(field);
		r.setExpectedValue(expectedValue);
		r.setActualValue(actualValue);
		r.setDetailRecordNo(detailRecordNo);
		r.setIteration(this.iteration);
		r.setRunId(this.runId);
		r.setTestStepRecordId(this.step.getRecordId());
		
		if(Utilities.isNumeric(actualValue) && Utilities.isNumeric(expectedValue)){
			if(Utilities.isNumericallyEqual(actualValue, expectedValue)){
				r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
			}else{
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		}else if(actualValue.equalsIgnoreCase(expectedValue)){
			/*************
			 * Fix by Sriram for comparision of numeric values Ends
			 */

			r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
		}else{

			if(Utilities.isNumeric(expectedValue) && Utilities.isNumeric(actualValue)){
				if(Double.parseDouble(expectedValue) == Double.parseDouble(actualValue)){
					r.setStatus(TenjinConstants.ACTION_RESULT_SUCCESS);
				}else{
					r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
				}
			}else{
				r.setStatus(TenjinConstants.ACTION_RESULT_FAILURE);
			}
		}

		logger.info(r.toString());
		return r;
	}
	
	protected UIValidationResult getUIValidationResultObject(int valRecId, String page, String field, String expectedValue, String actualValue, String status, String detailRecordNo) {
		UIValidationResult r = new UIValidationResult();
		r.setUiValRecId(valRecId);
		r.setPage(page);
		r.setField(field);
		r.setExpectedValue(expectedValue);
		r.setActualValue(actualValue);
		r.setDetailRecordNo(detailRecordNo);
		r.setIteration(this.iteration);
		r.setRunId(this.runId);
		r.setTestStepRecordId(this.step.getRecordId());
		r.setStatus(status);
		/*if(Utilities.isNumeric(actualValue) && Utilities.isNumeric(expectedValue)){
			if(Utilities.isNumericallyEqual(actualValue, expectedValue)){
				r.setStatus(HatsConstants.ACTION_RESULT_SUCCESS);
			}else{
				r.setStatus(HatsConstants.ACTION_RESULT_FAILURE);
			}
		}else if(actualValue.equalsIgnoreCase(expectedValue)){
			*//*************
			 * Fix by Sriram for comparision of numeric values Ends
			 *//*

			r.setStatus(HatsConstants.ACTION_RESULT_SUCCESS);
		}else{

			if(Utilities.isNumeric(expectedValue) && Utilities.isNumeric(actualValue)){
				if(Double.parseDouble(expectedValue) == Double.parseDouble(actualValue)){
					r.setStatus(HatsConstants.ACTION_RESULT_SUCCESS);
				}else{
					r.setStatus(HatsConstants.ACTION_RESULT_FAILURE);
				}
			}else{
				r.setStatus(HatsConstants.ACTION_RESULT_FAILURE);
			}
		}*/

		logger.info(r.toString());
		return r;
	}
	
	private void performPreUIValidationSteps(String pageAreaName, JSONArray preStepsJsonArray, DriverContext driverContext, SeleniumApplicationImpl oApplication, String tdUid) {
		
		if(preStepsJsonArray == null || preStepsJsonArray.length() < 1) {
			logger.debug("No pre-validation input to be performed");
			return;
		}
		
		try {
			for(int i=0; i<preStepsJsonArray.length(); i++) {
				JSONObject field = preStepsJsonArray.getJSONObject(i);
				
				WebElement element = null;
				
				if(field.getString("FLD_LABEL").equalsIgnoreCase("button")) {
					driverContext = oApplication.navigateToButton("", field.getString("DATA"));
				}else {
					TestObject t = TestObject.loadFromJson(field);
					element = oApplication.locateElement(t.getUniqueId(), t.getIdentifiedBy(), t);
					oApplication.performInput(element, t, t.getData());
				}
			}
		} catch (JSONException e) {
			
			logger.error("JSONException occurred while performing Pre Input for UI Validations", e);
		} catch (ElementIdentificationException e) {
			
			logger.error("Could not perform Pre-validation input", e);
		} catch (AutException e) {
			
			logger.error("Could not perform Pre-validation input", e);
		} catch(Exception e) {
			logger.error("ERROR - Could not perform Pre-Validation Input", e);
		}
	}
	
	public void performUIValidationsOnPageArea(String pageAreaName, JSONArray uiValidationsArray, DriverContext driverContext, SeleniumApplicationImpl oApplication, String tdUid, String detailRecordNo) {
		logger.info("Performing UI-UX validations on page [{}]", pageAreaName);
		
		try {
			for(int i=0; i<uiValidationsArray.length(); i++) {
				JSONObject uiValidation = uiValidationsArray.getJSONObject(i);
				
				int validationType = uiValidation.getInt("VAL_ID");
				int valRecId = uiValidation.getInt("REC_ID");
				
				logger.info("Performing Type[{}] UI validation - #{}", validationType, valRecId);
				
				
				JSONArray preSteps = uiValidation.getJSONArray("PRE_STEPS");
				this.performPreUIValidationSteps(pageAreaName, preSteps, driverContext, oApplication, tdUid);
				
				JSONArray valSteps = uiValidation.getJSONArray("VAL_STEPS");
				for(int j=0; j<valSteps.length(); j++) {
					JSONObject field = valSteps.getJSONObject(j);
					TestObject t = TestObject.loadFromJson(field);
					
					Formula formula = this.parseFormula(Utilities.trim(t.getData()));
					WebElement element = null;
					try {
						element = oApplication.locateElement(t.getUniqueId(), t.getIdentifiedBy(), t);
						
						//Fix for T25IT-339,341,344
						/*if(formula != null && formula.getName().equalsIgnoreCase("exist")) {*/
						if(formula != null && (formula.getName().equalsIgnoreCase("exist") || formula.getName().equalsIgnoreCase("exists"))) {
						//Fix for T25IT-339,341,344 ends
							UIValidationResult result = this.getUIValidationResultObject(valRecId, pageAreaName, t.getLabel(), formula.getArguments()[0], "true", detailRecordNo);
							this.uiValidationResults.add(result);
							continue;
						}
					} catch (ElementIdentificationException e) {
						
						logger.warn(e.getMessage());
						//Fix for T25IT-339,341,344
						/*if(formula != null && formula.getName().equalsIgnoreCase("exist")) {*/
						if(formula != null && (formula.getName().equalsIgnoreCase("exist") || formula.getName().equalsIgnoreCase("exists"))) {
						//Fix for T25IT-339,341,344
							UIValidationResult result = this.getUIValidationResultObject(valRecId, pageAreaName, t.getLabel(), formula.getArguments()[0], "false", detailRecordNo);
							this.uiValidationResults.add(result);
						}else {
							logger.error("ERROR performing UI Validation as element was not identified");
							UIValidationResult result = this.getUIValidationResultObject(valRecId, pageAreaName, t.getLabel(), formula.getArguments()[0], "ERROR - Could not identify target element", detailRecordNo);
							this.uiValidationResults.add(result);
						}
						continue;
					}
					
					
					this.handleFormula(formula, driverContext, oApplication, element, t, valRecId, pageAreaName, detailRecordNo);
					
				}
				
			}
		} catch (JSONException e) {
			
			logger.error("ERROR - Could not perform UI Validations on page [{}] due to an internal error", pageAreaName, e);
		}
	}
	
	protected void handleFormula(Formula formula, DriverContext driverContext, SeleniumApplicationImpl oApplication, WebElement element, TestObject t, int valRecId, String pageArea, String detailRecordNo) {
		logger.info("Resolving formula [{}]", t.getData());
		//Formula formula = this.parseFormula(Utilities.trim(t.getData()));
		
		if(formula.getName().equalsIgnoreCase("output")) {
			try {
				String output = oApplication.getFieldValue(element, t);
				RuntimeFieldValue r = this.getRuntimeFieldValueObject(t.getLabel(), output, 1, tdUid);
				this.runtimeValues.add(r);
			} catch (AutException e) {
				
				logger.error("ERROR - Could not get runtime value of element [{}] on page [{}]", t.getLabel(), pageArea, e);
			}
		}else if(formula.getName().equalsIgnoreCase("Enabled")) {
			String actValue = "false";
			try {
				//logger.info("ReadOnly attribute value --> {}", element.getAttribute("readonly"));
				if(element.isEnabled() && !Utilities.trim(element.getAttribute("readonly")).equalsIgnoreCase("true")) {
					actValue = "true";
				}
			}catch(Exception e) {
				logger.error("ERROR - Could not check if field is enabled", e);
				actValue = "ERROR - Could not check the field's status due to an unexpected error.";
			}
			
			UIValidationResult result = this.getUIValidationResultObject(valRecId, pageArea , t.getLabel(), formula.getArguments()[0], actValue, detailRecordNo);
			this.uiValidationResults.add(result);
		}else if(formula.getName().equalsIgnoreCase("hasitems")) {
			String[] availableItems = oApplication.getElementValues(element, t);
			String expectedResult = "";
			for(String a : formula.getArguments()) {
				expectedResult = expectedResult + a +", "; 
			}
			if(availableItems != null && availableItems.length < 1) {
				UIValidationResult result = this.getUIValidationResultObject(valRecId, pageArea, t.getLabel(), expectedResult, "This element has no default values", TenjinConstants.ACTION_RESULT_FAILURE, detailRecordNo);
				this.uiValidationResults.add(result);
			}else {
				String missingValues = "";
				for(String eValue : formula.getArguments()) {
					boolean found = false;
					for(String avlItem : availableItems) {
						if(eValue.equalsIgnoreCase(avlItem)) {
							found = true;
							break;
						}
					}
					
					if(!found) {
						missingValues += eValue + ",";
					}
				}
				
				if(missingValues.endsWith(",")) {
					missingValues = missingValues.substring(0, missingValues.length()-1);
				}
				UIValidationResult result = null;
				if(Utilities.trim(missingValues).length() > 0) {
					 result = this.getUIValidationResultObject(valRecId, pageArea, t.getLabel(), expectedResult, "The following values are missing: " + missingValues, TenjinConstants.ACTION_RESULT_FAILURE, detailRecordNo);
				}else {
					result = this.getUIValidationResultObject(valRecId, pageArea, t.getLabel(), expectedResult, "All expected values are available in the element.", TenjinConstants.ACTION_RESULT_SUCCESS, detailRecordNo);
				}
				
				this.uiValidationResults.add(result);
			}
		}
		
		//Fix for T25IT-240
		else if(formula.getName().equalsIgnoreCase("defvalue")) {
			UIValidationResult result = null;
			try {
				String output = oApplication.getFieldValue(element, t);
				ValidationResult valResult = this.getValidationResultObject(pageArea, t.getLabel(), formula.getArguments()[0], output, "");
				result = this.getUIValidationResultObject(valRecId, pageArea, t.getLabel(), formula.getArguments()[0], output, valResult.getStatus());
				
			} catch (AutException e) {
				logger.error("ERROR getting field value");
				result = this.getUIValidationResultObject(valRecId, pageArea, t.getLabel(), formula.getArguments()[0], "ERROR - Could not get element value.", TenjinConstants.ACTION_RESULT_FAILURE);
			}
			this.uiValidationResults.add(result);
		}
		//Fix for T25IT-240 ends
		
	}
	
	protected Formula parseFormula(String formulaString) {
		try {
			formulaString = formulaString.replace("@@", "");
			if(!formulaString.contains("(") && !formulaString.endsWith(")")) {
				return new Formula(formulaString, new String[] {});
			}else if(formulaString.contains("(") && formulaString.endsWith(")")){
				String[] fSplit = formulaString.split("\\(");
				String fName = fSplit[0];
				String args = fSplit[1].substring(0, fSplit[1].length()-1);
				
				return new Formula(fName, args.split(","));
				
			}else {
				logger.error("ERROR Invalid Formula");
				return null;
			}
		} catch(Exception e) {
			return null;
		}
	}
	
	/* For TENJINCG-311 ends*/
	
	/*Added by Padmavathi for TENJINCG-783 Starts*/
	public static boolean isPartialResultMatch(String expectedValue,String actualValue){
		
		boolean flag=false;
		PatternMatch pm = new PatternMatch();
		boolean isMatch = pm.matchString(actualValue, expectedValue, true);
		if (isMatch) {
			flag = true;
		}

		return flag;
				
	} 
	/*Added by Padmavathi for TENJINCG-783 Ends*/

}
