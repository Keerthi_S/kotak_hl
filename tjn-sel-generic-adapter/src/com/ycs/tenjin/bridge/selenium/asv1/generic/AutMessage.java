package com.ycs.tenjin.bridge.selenium.asv1.generic;

import java.io.File;
import java.util.List;

public class AutMessage {
	private List<String> messages;
	private File screenshot;
	private String type;
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<String> getMessages() {
		return messages;
	}
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	public File getScreenshot() {
		return screenshot;
	}
	public void setScreenshot(File screenshot) {
		this.screenshot = screenshot;
	}
	
	
}
