/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GenericExecutorImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India

 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Feb-2016           Sriram Sridharan          Newly Added For 
 * 16-06-2017			Sriram Sridharan		TENJINCG-187
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 */

package com.ycs.tenjin.bridge.selenium.asv1.taskhandler.task;

import org.json.JSONObject;

import com.ycs.tenjin.bridge.exceptions.RunAbortedException;
import com.ycs.tenjin.bridge.pojo.run.IterationStatus;
import com.ycs.tenjin.bridge.selenium.asv1.SeleniumApplicationImpl;





public interface ExecutorTask {
	
	/**
	 * Executes one iteration (or a transaction) of a Test Step
	 * @param dataSet The JSON Data set to be executed
	 * @param oApplication The Adapter instance of the Application Under Test
	 * @return IterationStatus object describing the result the execution
	 */
	/* Changed by Sriram for TENJINCG-187 */
	/*public IterationStatus executeIteration(JSONObject dataSet, Application oApplication, int iteration);*/
	public IterationStatus executeIteration(JSONObject dataSet, SeleniumApplicationImpl oApplication, int iteration) throws RunAbortedException;
	/* Changed by Sriram for TENJINCG-187 */
	
}
