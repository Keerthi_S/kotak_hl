/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  SeleniumApplicationImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.R

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
 *	 
 */

/******************************************
 * CHANGE HISTORY
 * ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * Sep 1, 2016           Sriram Sridharan          Newly Added For 
 * 30-06-2017			 Padmavathi	            TENJINCG-261
 * 07-Jul-2017           Sriram Sridharan          TENJINCG-197
 * 27-Jul-2017			Sriram Sridharan		TENJINCG-311
 * 30-08-2017			 Sriram Sridharan	    T25IT-339,341,344
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 26-09-2018           Leelaprasad             TENJINCG-739
 */

package com.ycs.tenjin.bridge.selenium.asv1;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.exceptions.ApplicationUnresponsiveException;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.DriverException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.oem.gui.GuiApplicationAbstract;
import com.ycs.tenjin.bridge.pojo.PrintScreenObject;
import com.ycs.tenjin.bridge.pojo.aut.Aut;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.selenium.asv1.generic.AutAlert;
import com.ycs.tenjin.bridge.selenium.asv1.generic.AutMessage;
import com.ycs.tenjin.bridge.selenium.asv1.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv1.generic.ElementIdentificationException;
import com.ycs.tenjin.bridge.selenium.asv1.generic.Launcher;
import com.ycs.tenjin.bridge.utils.CacheUtils;
import com.ycs.tenjin.bridge.utils.HatsStack;
import com.ycs.tenjin.util.Utilities;

public abstract class SeleniumApplicationImpl extends GuiApplicationAbstract {

	private static final Logger logger = LoggerFactory
			.getLogger(SeleniumApplicationImpl.class);

	// /////////////////////////////////////////////////
	// Variables
	// /////////////////////////////////////////////////
	protected DriverContext driverContext;
	@Override
	public String toString() {
		return "Old Spec";
	}

	protected int OBJECT_IDENTIFICATION_TIMEOUT = 0;
	protected String compositeXpath = ".//input[@type='text' or @type='Text' or @type='TEXT']|.//textarea|.//input[@type='password' or @type='Password' or @type='PASSWORD' or @type='passWord' or @type='PassWord']|.//button|.//input[@type='button' or @type='submit' or @type='Button' or @type='Submit' or @type='BUTTON' or @type='SUBMIT']|.//select|.//input[@type='radio' or @type='Radio' or @type='RADIO']|.//input[@type='Checkbox' or @type='CHECKBOX' or @type='checkbox' or @type='checkBox']";
	protected HatsStack windowStack = new HatsStack();

	
	/*********************************
	 * TENJINCG-731 - By Sameer - Mobility - 
	 */
	public SeleniumApplicationImpl(Aut aut, String clientIp, int port,
			String browser) {
		super(aut, clientIp, port, browser, null);
		/*added by Prem for  v210Reg-30 start*/
//		this.browser = aut.getBrowser();
		/*added by Prem for  v210Reg-30 End*/
		this.adapterPackageOrTool=this.adapterPackageOrTool+".asv1";
		OBJECT_IDENTIFICATION_TIMEOUT = 15;
	}

	// /////////////////////////////////////////////////
	// Getters
	// /////////////////////////////////////////////////

	public String getCompositeXpath() {
		return compositeXpath;
	}
 
	public DriverContext getDriverContext() {
		return driverContext;
	}

	// /////////////////////////////////////////////////
	// Overriding Methods
	// /////////////////////////////////////////////////

	public void launch(String url) throws AutException, DriverException {

		try {
			WebDriver driver = Launcher.getDriver(this.clientIp, this.port,
					this.browser);

			driver.get(url);
			this.driverContext = new DriverContext();
			this.driverContext.setDriver(driver);
		} catch (Exception e) {
			logger.error("Could not launch application", e);
			throw new AutException("Could not launch AUT", e);
		}
	}

	public abstract void login(String userName, String password)
			throws AutException, ApplicationUnresponsiveException;

	public abstract void navigateToFunction(String functionCode,
			String functionName, String menuHierarchy) throws AutException,
			BridgeException, ApplicationUnresponsiveException;
	
	
	public abstract void logout() throws AutException;

	public abstract void recover() throws AutException;

	// /////////////////////////////////////////////////
	// Extra Methods
	// /////////////////////////////////////////////////

	

	public WebElement locateElement(By by)
			throws ElementIdentificationException {
		WebElement element = null;
		int secCounter = 0;

		while (element == null && secCounter < OBJECT_IDENTIFICATION_TIMEOUT) {
			try {
				element = this.driverContext.getDriver().findElement(by);
			} catch (Exception e) {
				explicitWait(1);
				secCounter++;
			}
		}

		if (element == null) {
			throw new ElementIdentificationException(
					"Could not identify element after "
							+ OBJECT_IDENTIFICATION_TIMEOUT + " seconds");
		} else {
			return element;
		}
	}

	public WebElement locateElement(WebElement wrapper, By by,
			boolean waitTillTimeout) throws ElementIdentificationException {
		WebElement element = null;
		// int secCounter=0;

		if (waitTillTimeout) {
			return this.locateElement(wrapper, by);
		}

		try {
			element = wrapper.findElement(by);
		} catch (Exception e) {
			logger.error(
					"Could not locate element with selector {}. Please consider passing waitTillTimeout = true",
					by.toString(), e);
			throw new ElementIdentificationException(
					"Could not identify element with selector " + by.toString());
		}

		if (element == null) {
			throw new ElementIdentificationException(
					"Could not identify element after "
							+ OBJECT_IDENTIFICATION_TIMEOUT + " seconds");
		} else {
			return element;
		}
	}

	public WebElement locateElement(By by, boolean waitTillTimeout)
			throws ElementIdentificationException {
		WebElement element = null;
		// int secCounter=0;

		if (waitTillTimeout) {
			return this.locateElement(by);
		}

		try {
			element = this.driverContext.getDriver().findElement(by);
		} catch (Exception e) {
			logger.error(
					"Could not locate element with selector {}. Please consider passing waitTillTimeout = true",
					by.toString(), e);
			throw new ElementIdentificationException(
					"Could not identify element with selector " + by.toString());
		}

		if (element == null) {
			throw new ElementIdentificationException(
					"Could not identify element after "
							+ OBJECT_IDENTIFICATION_TIMEOUT + " seconds");
		} else {
			return element;
		}
	}

	public WebElement locateElement(WebElement wrapper, By by)
			throws ElementIdentificationException {
		WebElement element = null;
		int secCounter = 0;

		while (element == null && secCounter < OBJECT_IDENTIFICATION_TIMEOUT) {
			try {
				element = wrapper.findElement(by);
			} catch (Exception e) {
				explicitWait(1);
				secCounter++;
			}
		}

		if (element == null) {
			throw new ElementIdentificationException(
					"Could not identify element after "
							+ OBJECT_IDENTIFICATION_TIMEOUT + " seconds");
		} else {
			return element;
		}
	}

	
	public WebElement locateElement(String uniqueId, String idType,
			TestObject testObj) throws ElementIdentificationException {
		/* Changed by Padmavathi for Req# Tenjincg-261 30-06-2017 ends */

		List<WebElement> element = null;
		WebElement actualElement = null;
		int secCounter = 0;

		By by;
		if (!"xpath".equalsIgnoreCase(idType)
				&& !"tagName".equalsIgnoreCase(idType)) {
			by = By.xpath(".//*[@" + idType + "='" + uniqueId + "']");
		} else if (!"tagName".equalsIgnoreCase(idType)) {
			by = By.xpath(uniqueId);
		} else {
			by = By.tagName(uniqueId);
		}

		while (element == null && secCounter < OBJECT_IDENTIFICATION_TIMEOUT) {
			try {
				element = this.driverContext.getDriver().findElements(by);
				for (WebElement ele : element) {
					// Fix for T25IT-339,341,344
					/* if(ele.isDisplayed() && ele.isEnabled()){ */
					if (ele.isDisplayed()) {
						// Fix for T25IT-339,341,344 ends
						actualElement = ele;
					}
				}
			} catch (Exception e) {
				explicitWait(1);
				secCounter++;
			}
		}

		if (actualElement == null) {
			
			if (testObj.getLocation() == null) {
				throw new ElementIdentificationException(
						"Could not identify element with " + idType + " "
								+ uniqueId + " after waiting "
								+ OBJECT_IDENTIFICATION_TIMEOUT + " seconds");
			} else {
				throw new ElementIdentificationException(
						"Could not identify element  "
								+ testObj.getLabel()
								+ " in  page area  "
								+ testObj.getLocation()
								+ ".field may be removed from application or tenjin unable to locate the field");
			}
			/* Changed by Padmavathi for Req# Tenjincg-261 30-06-2017 ends */
		} else {
			return actualElement;
		}
	}

	public WebElement locateElement(String uniqueId, String idType)
			throws ElementIdentificationException {
		/* Changed by Padmavathi for Req# Tenjincg-261 30-06-2017 ends */

		List<WebElement> element = null;
		WebElement actualElement = null;
		int secCounter = 0;

		By by;
		if (!"xpath".equalsIgnoreCase(idType)
				&& !"tagName".equalsIgnoreCase(idType)) {
			by = By.xpath(".//*[@" + idType + "='" + uniqueId + "']");
		} else if (!"tagName".equalsIgnoreCase(idType)) {
			by = By.xpath(uniqueId);
		} else {
			by = By.tagName(uniqueId);
		}

		while (element == null && secCounter < OBJECT_IDENTIFICATION_TIMEOUT) {
			try {
				element = this.driverContext.getDriver().findElements(by);
				for (WebElement ele : element) {
					if (ele.isDisplayed() && ele.isEnabled()) {
						actualElement = ele;
					}
				}
			} catch (Exception e) {
				explicitWait(1);
				secCounter++;
			}
		}

		if (actualElement == null) {
			throw new ElementIdentificationException(
					"Could not identify element with " + idType + " "
							+ uniqueId + " after waiting "
							+ OBJECT_IDENTIFICATION_TIMEOUT + " seconds");

		} else {
			return actualElement;
		}
	}

	public File captureScreenshotAsFile() {
		
			
			File screenshot = null;
			try {
				String printScreenFlag = TenjinConfiguration.getProperty("ENABLE_PRINTSCREEN");
				if(printScreenFlag != null && printScreenFlag.equalsIgnoreCase("Y")){
					//String maxAttempt = TenjinConfiguration.getProperty("MAXIMUM_ATTEMPT");
					String ipAddress = this.clientIp;
					if(Utilities.isLocalHost(this.clientIp)){
						ipAddress = Inet4Address.getLocalHost().getHostAddress();
					}
					//int maxAttemptInt = Integer.parseInt(maxAttempt);
					//Set boolean value as true as it is needs to take screenshot
					PrintScreenObject prntScreenObject = (PrintScreenObject)CacheUtils.getObjectFromRunCache("screenshot" +ipAddress);
					prntScreenObject.setScreenshotInProgress(true);
					prntScreenObject.setScreenshot(null);
					CacheUtils.putObjectInRunCache("screenshot" +ipAddress, prntScreenObject);
					
					//poll object from cache to check the screenshot is captured
					boolean isScreenshotReceived = false;
					
					while(!isScreenshotReceived){
						prntScreenObject = (PrintScreenObject)CacheUtils.getObjectFromRunCache("screenshot" +ipAddress);
						if(!prntScreenObject.isScreenshotInProgress()){
							isScreenshotReceived = true;
							prntScreenObject.setScreenshotInProgress(false);
							screenshot = prntScreenObject.getScreenshot();
							CacheUtils.putObjectInRunCache("screenshot" +ipAddress, prntScreenObject);
						}
					}
				}else{
					screenshot = ((TakesScreenshot) this.driverContext.getDriver())
							.getScreenshotAs(OutputType.FILE);
				}
			}catch(Exception e){
				logger.debug("Could not capture screenshot of application", e);
				//if any failure capture the selenium screenshot
				screenshot = ((TakesScreenshot) this.driverContext.getDriver())
						.getScreenshotAs(OutputType.FILE);
			}
			return screenshot;
			
	}

	public List<WebElement> locateElements(By by)
			throws ElementIdentificationException {
		try {
			return this.driverContext.getDriver().findElements(by);
		} catch (Exception e) {
			logger.error("ERROR in locateElemenets(By by)", e);
			throw new ElementIdentificationException(
					"Could not identify elements with the specified criteria",
					e);
		}
	}

	public List<WebElement> locateElements(String uniqueId, String idType)
			throws ElementIdentificationException {
		By by;
		if (!"xpath".equalsIgnoreCase(idType)) {
			by = By.xpath(".//*[@" + idType + "='" + uniqueId + "']");
		} else {
			by = By.xpath(uniqueId);
		}

		try {
			return this.driverContext.getDriver().findElements(by);
		} catch (Exception e) {
			logger.error("ERROR in locateElemenets({}, {})", uniqueId, idType,
					e);
			throw new ElementIdentificationException(
					"Could not identify elements with " + idType + " - "
							+ uniqueId, e);
		}
	}

	public List<WebElement> locateElements(WebElement wrapper, By by)
			throws ElementIdentificationException {
		try {
			return wrapper.findElements(by);
		} catch (Exception e) {
			logger.error("ERROR in locateElemenets(By by)", e);
			throw new ElementIdentificationException(
					"Could not identify elements with the specified criteria",
					e);
		}
	}

	public List<WebElement> locateElements(WebElement wrapper, String uniqueId,
			String idType) throws ElementIdentificationException {
		By by;
		if (!"xpath".equalsIgnoreCase(idType)) {
			by = By.xpath(".//*[@" + idType + "='" + uniqueId + "']");
		} else {
			by = By.xpath(uniqueId);
		}

		try {
			return wrapper.findElements(by);
		} catch (Exception e) {
			logger.error("ERROR in locateElemenets({}, {})", uniqueId, idType,
					e);
			throw new ElementIdentificationException(
					"Could not identify elements with " + idType + " - "
							+ uniqueId, e);
		}
	}

	public void performInput(WebElement element, TestObject t, String data)
			throws AutException {

		String elemType = t.getObjectClass();

		try {
			if (FieldType.TEXTAREA.toString().equalsIgnoreCase(elemType)
					|| FieldType.TEXTBOX.toString().equalsIgnoreCase(elemType)
					|| FieldType.PASSWORD.toString().equalsIgnoreCase(elemType)) {
				this.textBoxInput(element, data);

				
				/************************************************************************************************************
				 * Added by Sharath for Auto Lov Starts
				 */
				try {
					if (t.getAutoLovAvailable().equalsIgnoreCase("Y")) {
						explicitWait(3);
						logger.info("Entering AutoLOV -- Field {}, Data {}",
								t.getLabel(), t.getData());
						this.switchDriverToLatestWindow();
						this.driverContext.getDriver().close();
						this.refreshDriverContext();
						logger.info("Out of the AutoLOV ");
					}
				} catch (Exception e) {
					logger.info("Contine with next field");
				}
				/************************************************************************************************************
				 * Added by Sharath for Auto Lov Ends
				 */

			} else if (FieldType.CHECKBOX.toString().equalsIgnoreCase(elemType)) {
				this.checkBoxInput(element, data);
			} else if (FieldType.RADIO.toString().equalsIgnoreCase(elemType)) {
				this.radioButtonInput(t, element, data);
			} else if (FieldType.LIST.toString().equalsIgnoreCase(elemType)) {
				this.listInput(element, data);
			} else if (FieldType.BUTTON.toString().equalsIgnoreCase(elemType)) {
				logger.debug("Navigating to button [{}]", t.getData());
				try {
					this.navigateToButton(element);
				} catch (Exception e) {
					throw new AutException("Could not click button "
							+ t.getData() + " on page " + t.getLocation()
							+ ". Please contact Tenjin Support.", e);
				}
			}
		} catch (StaleElementReferenceException e) {
			logger.error("Could not input {} to field {} on page {}", data,
					t.getLabel(), this.driverContext.getCurrentLocation());
			throw new AutException("Could not input " + data + " to field "
					+ t.getLabel() + " on page "
					+ this.driverContext.getCurrentLocation(), e);
		} catch (ExecutorException e) {

			if (e.getCause() != null
					&& e.getCause() instanceof StaleElementReferenceException) {
				logger.error("STALE ELEMENT >> PAGE DOM HAS REFRESHED");
				throw new ExecutorException(
						"Could not perform I/O as page DOM has refreshed",
						e.getCause());
			}

			logger.error("Could not input {} to field {} on page {}", data,
					t.getLabel(), this.driverContext.getCurrentLocation());
			throw new AutException("Could not input " + data + " to field "
					+ t.getLabel() + " on page "
					+ this.driverContext.getCurrentLocation());
		} catch (Exception e) {
			logger.error("Could not input {} to field {} on page {}", data,
					t.getLabel(), this.driverContext.getCurrentLocation());
			throw new AutException("Could not input " + data + " to field "
					+ t.getLabel() + " on page "
					+ this.driverContext.getCurrentLocation());
		}
	}

	public String getFieldValue(WebElement element, TestObject t)
			throws AutException {

		String elemType = t.getObjectClass();

		String retVal = "";

		try {
			if (FieldType.TEXTBOX.toString().equalsIgnoreCase(elemType)
					|| FieldType.PASSWORD.toString().equalsIgnoreCase(elemType)) {
				retVal = this.textBoxOutput(element);
			} else if (FieldType.CHECKBOX.toString().equalsIgnoreCase(elemType)) {
				retVal = this.checkBoxOutput(element);
			} else if (FieldType.RADIO.toString().equalsIgnoreCase(elemType)) {
				retVal = this.radioButtonOutput(element, t);
			} else if (FieldType.LIST.toString().equalsIgnoreCase(elemType)) {
				retVal = this.listOutput(element);
			} else if (FieldType.TEXTAREA.toString().equalsIgnoreCase(elemType)) {
				retVal = this.textAreaOutput(element);
			}
		} catch (ExecutorException e) {
			logger.error("Could not get value of field {} on page {}",
					t.getLabel(), this.driverContext.getCurrentLocation());
			throw new AutException("Could not get value of field "
					+ t.getLabel() + " on page "
					+ this.driverContext.getCurrentLocation());
		} catch (Exception e) {
			logger.error("Could not get value of field {} on page {}",
					t.getLabel(), this.driverContext.getCurrentLocation());
			throw new AutException("Could not get value of field "
					+ t.getLabel() + " on page "
					+ this.driverContext.getCurrentLocation());
		}

		return retVal;
	}

	protected void textBoxInput(WebElement element, String data)
			throws ExecutorException, StaleElementReferenceException {
		try {
			element.clear();
			if (data != null && !data.equalsIgnoreCase("@@CLEAR")) {
				element.sendKeys(data);
				// element.sendKeys(Keys.TAB);
				// Need to check for SECTOR Field
			}
		} catch (Exception e) {
			logger.error(
					"An exception occurred while setting value to text field",
					e);
			throw new ExecutorException("Could not set value " + data
					+ " in text field", e);
		}
	}

	protected String textBoxOutput(WebElement element) throws ExecutorException {
		try {
			return element.getAttribute("value");
		} catch (Exception e) {
			logger.error("ERROR getting value from text box", e);
			throw new ExecutorException("Could not get value of text box");
		}
	}

	protected String textAreaOutput(WebElement element)
			throws ExecutorException {
		try {
			return element.getText();
		} catch (Exception e) {
			logger.error("ERROR getting value from textarea", e);
			throw new ExecutorException("Could not get value of text area");
		}
	}

	protected void checkBoxInput(WebElement element, String data)
			throws ExecutorException {
		try {
			if (data.equalsIgnoreCase("yes")) {
				if (!element.isSelected()) {
					element.click();
				}
			} else if (data.equalsIgnoreCase("no")) {
				if (element.isSelected()) {
					element.click();
				}
			} else {
				throw new ExecutorException("Could not input " + data
						+ " in checkbox");
			}
		} catch (Exception e) {
			logger.error(
					"An exception occurred while setting value to checkbox/radio button",
					e);
			throw new ExecutorException("Could not input " + data
					+ " in checkbox");
		}
	}

	protected String checkBoxOutput(WebElement element)
			throws ExecutorException {
		try {
			if (element.isSelected()) {
				return "YES";
			} else {
				return "NO";
			}
		} catch (Exception e) {
			throw new ExecutorException("Could not get value of checkbox");
		}
	}

	protected void radioButtonInput(TestObject t, WebElement element,
			String data) throws ExecutorException {
		try {
			/*
			 * if(data.equalsIgnoreCase("yes")){ element.click(); }
			 */
			boolean targetRadioFound = false;
			List<WebElement> radios = this.driverContext.getDriver()
					.findElements(By.name(t.getUniqueId()));
			if (radios != null && radios.size() > 0) {
				for (WebElement radio : radios) {
					String rLabel = radio.getAttribute("value");
					if (rLabel != null && rLabel.equalsIgnoreCase(data)) {
						targetRadioFound = true;
						radio.click();
						break;
					}
				}
			}

			if (!targetRadioFound) {
				throw new ExecutorException("Could not find Option " + data
						+ " in " + t.getLabel());
			}

		} catch (ExecutorException e) {
			throw new ExecutorException(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(
					"An exception occurred while setting value to checkbox/radio button",
					e);
			throw new ExecutorException("Could not input " + data
					+ " to field " + t.getLabel(), e);
		}
	}

	protected String radioButtonOutput(WebElement element, TestObject t)
			throws ExecutorException {
		String retVal = "";
		try {
			List<WebElement> radios = this.driverContext.getDriver()
					.findElements(By.name(t.getUniqueId()));
			if (radios != null && radios.size() > 0) {
				for (WebElement radio : radios) {
					String rLabel = radio.getAttribute("value");
					if (element.isSelected()) {
						retVal = rLabel;
						break;
					}
				}
			}
		} catch (Exception e) {
			logger.error("Could not get value of radio button {}",
					t.getLabel(), e);
			throw new ExecutorException("Could not get value of radio button "
					+ t.getLabel());
		}

		return retVal;

	}

	protected void listInput(WebElement element, String data)
			throws ExecutorException {
		try {
			Select select = new Select(element);
			select.selectByVisibleText(data);
		} catch (Exception e) {
			logger.error(
					"An exception occurred while selecting value from list box",
					e);
			throw new ExecutorException("Could not select " + data
					+ " from list ");
		}
	}

	protected String listOutput(WebElement element) throws ExecutorException {

		try {
			Select select = new Select(element);
			WebElement option = select.getFirstSelectedOption();

			return option.getText();
		} catch (Exception e) {
			throw new ExecutorException("Could not get value of list ", e);
		}

	}

	// Access modifier changed to Public by sriram to make this method visible
	// to Learner, Executor and Extractor Implementations
	/* protected void switchDriverToLatestWindow() { */
	public void switchDriverToLatestWindow() {
		try {

			/******************************************************************************************************************
			 * Added by Sharath for Overdire Check Starts
			 */
			this.driverContext.setContextSame(false);

			/******************************************************************************************************************
			 * Added by Sharath for Overdire Check Ends
			 */

			/******************************************************************************
			 * 8 1. Currently, the driver points to the handle on the top of the
			 * stack (this.windowStack.peek()) 2. Get all the availble window
			 * handles from the driver 3. Check if the handle count, and the
			 * stack size are same. 3.1 If the handle count and stack size are
			 * same, then do nothing 3.2 If handle count is greater than stack
			 * size, then one new window has popped up 3.2.1 Find out which one
			 * is the new handle, push it to the stack 3.3 If handle count is
			 * lesser than stack size, then one window has closed 3.3.1 Pop the
			 * top most handle from the stack (ASSUMPTION here is that open
			 * windows can be closed only in the LIFO order. Which means, the
			 * handle of the window that is closed is always at the top of our
			 * stack)
			 * 
			 * 4. Switch driver to the top most handle from the stack
			 * (this.windowStack.peek())
			 */

			Set<String> windowHandles = this.driverContext.getDriver()
					.getWindowHandles();

			if (windowHandles.size() == this.windowStack.size()) {
				/******************************************************************************************************************
				 * Added by Sharath for Overdire Check Starts
				 */
				this.driverContext.setContextSame(true);

				/******************************************************************************************************************
				 * Added by Sharath for Overdire Check Ends
				 */
				return;
			} else if (windowHandles.size() > this.windowStack.size()) {
				// New window has popped up (3.2)
				// 3.2.1 - Identify what is the new handle
				for (String handle : windowHandles) {
					if (!this.windowStack.contains(handle)) {
						// This is the new window handle. Push it to the stack
						// and break here
						logger.debug("New window found {}", handle);
						this.windowStack.push(handle);
						break;
					}
				}
			} else if (windowHandles.size() < this.windowStack.size()) {
				// Existing window has closed.
				// 3.3.1 Pop the latest window from our stack
				logger.debug("Window {} closed", this.windowStack.peek());
				this.windowStack.pop();
			}

			logger.debug("Switching to window {}", this.windowStack.peek());
			this.driverContext.getDriver().switchTo()
					.window(this.windowStack.peek());
			this.driverContext.getDriver().manage().window().maximize();

		} catch (Exception e) {
			logger.error("ERROR switching to focus window", e);
		}
	}

	public void destroy() {
		try {
			this.driverContext.getDriver().close();
			this.driverContext.getDriver().quit();
		} catch (Exception ignore) {
		}
	}

	public DriverContext refreshDriverContext() throws AutException {

		// Check for any alerts
		this.checkForAlerts();

		// Switch to the latest window
		this.switchDriverToLatestWindow();

		// Set the wrapper
		this.driverContext.setWrapper(this.getWrapper());

		// Get screen title
		this.driverContext.setCurrentLocation(this.getScreenTitle());

		

		// Check for AutErrors
		AutMessage autError = this.checkForAutMessages();
		this.driverContext.setAutMessage(autError);

		logger.info("Refreshed Driver Context --> {}",
				this.driverContext.toString());

		return this.driverContext;
	}

	/* Changed by Sriram for TENJINCG-197 ends */

	public abstract DriverContext checkForAlerts() throws AutException;

	public abstract AutAlert checkForAlerts(WebDriver driver)
			throws AutException;

	public abstract DriverContext checkForTabs() throws AutException;

	public abstract DriverContext checkForNavigationLinks() throws AutException;

	public abstract DriverContext checkForNavigationButtons()
			throws AutException;

	public abstract WebElement getWrapper() throws AutException;

	public abstract String getScreenTitle() throws AutException;

	public abstract DriverContext navigateToTab(String tabText)
			throws AutException;

	public abstract DriverContext navigateToTab(WebElement tab)
			throws AutException;

	public abstract DriverContext navigateToLink(String linkText)
			throws AutException;

	public abstract DriverContext navigateToLink(WebElement link)
			throws AutException;

	public abstract DriverContext navigateToButton(String buttonUid,
			String identifiedBy) throws AutException;

	public abstract DriverContext navigateToButton(WebElement button)
			throws AutException;

	public abstract WebElement getTabContent() throws AutException;

	public abstract DriverContext closeCurrentFrame() throws AutException;

	public abstract DriverContext clickToolbarItem(String itemDescription)
			throws AutException;

	protected abstract DriverContext checkForToolbarItems() throws AutException;

	protected abstract AutMessage checkForAutMessages() throws AutException;

	public abstract DriverContext addMultiRecordFieldGroup(String groupName)
			throws AutException;

	
	public abstract WebElement queryRowInTableGrid(WebElement targetTable,
			JSONArray drQueryFields, String pageName) throws AutException;

	// Access modifier changed to Public by sriram to make this method visible
	// to Learner, Executor and Extractor Implementations
	public abstract WebElement locateTableGrid(String wayIn)
			throws AutException;

	public abstract DriverContext performTableOperation(String operationName,
			WebElement targetRow, Location location) throws AutException;

	/******************************************************************************************
	 * Added by Sharath for Checking of frames Starts
	 */

	public abstract DriverContext clickNavigationItem(String itemDescription)
			throws AutException;

	protected abstract DriverContext checkForFrames() throws AutException;

	/******************************************************************************************
	 * Added by Sharath for Checking of frames Starts
	 */

	/******************************************************************************************
	 * Added by Sharath for Checking of frames Starts
	 */

	public abstract DriverContext clickNavigationLink(String itemDescription)
			throws AutException;

	/******************************************************************************************
	 * Added by Sharath for Checking of frames Starts
	 */

	/******************************************************************************************
	 * Added by Sriram for TCSBancs
	 */
	public abstract List<String> getAllAccordions()
			throws ElementIdentificationException, AutException;

	public abstract DriverContext navigateToAccordion(String accordionIdentifier)
			throws ElementIdentificationException, AutException;

	public abstract List<String> getAllMenuItems()
			throws ElementIdentificationException, AutException;

	public abstract DriverContext navigateToMenuItem(String menuItem)
			throws ElementIdentificationException, AutException;

	public abstract List<String> getAllPaginations()
			throws ElementIdentificationException, AutException;

	public abstract DriverContext navigateToPagination(
			String paginationIdentifier) throws ElementIdentificationException,
			AutException;

	/******************************************************************************************
	 * Added by Sriram for TCSBancs ends
	 */

	/**************************************************************************************************************************
	 * ABSTRACT METHOD DECLARATIONS END
	 * 
	 * @throws ApplicationUnresponsiveException
	 */
	/* Added by Sriram for TENJINCG-197 */
	@SuppressWarnings("unused")
	private boolean isUrlValid(String url)
			throws ApplicationUnresponsiveException {

		InputStream i = null;
		try {
			logger.info("Validating URL {}", url);
			URL oUrl = new URL(url);

			try {
				i = oUrl.openStream();
			} catch (IOException e) {
				logger.error("UnknownHostException caught:", e);
				throw new ApplicationUnresponsiveException(
						"Could not resolve host. Please make sure the Application is up and running.");
			}

			if (i != null) {
				logger.info("URL validation successful");
			} else {
				logger.error("URL Validation failed - InputStream is null");
				throw new ApplicationUnresponsiveException(
						"Could not resolve host. Please make sure the Application is up and running.");
			}
			
		} catch (MalformedURLException e1) {
			logger.error("ERROR validating URL", e1);
			throw new ApplicationUnresponsiveException(
					"Could not verify application running status. Please make sure that the Application is up and running. Contact Tenjin Support if problem persists.");
		}
		finally {
			try {
				i.close();
			} catch (IOException e) {
				logger.error("Error"+e);
			}
		}

		return true;

	}

	/* Added by Sriram for TENJINCG-197 */

	/* For TENJINCG-311 */
	public String[] getElementValues(WebElement element, TestObject t) {
		if (t != null) {
			String values = "";
			if (t.getObjectClass().toLowerCase().contains("radio")) {
				try {
					List<WebElement> radios = this.locateElements(By.name(t
							.getUniqueId()));
					for (WebElement radio : radios) {
						values = values + "," + radio.getAttribute("value");
					}
				} catch (Exception e) {
					logger.warn(
							"Could not get available values for Radio button",
							e);
				}
			} else if (t.getObjectClass().equalsIgnoreCase("list")) {
				try {
					List<WebElement> options = this.locateElements(element,
							By.tagName("option"));
					for (WebElement option : options) {
						values = values + Utilities.trim(option.getText())
								+ ",";
					}
				} catch (Exception e) {
					logger.warn("Could not get available values for List", e);
				}
			}

			return values.split(",");
		} else {
			return new String[] {};
		}
	}
	/* For TENJINCG-311 ends */
}
