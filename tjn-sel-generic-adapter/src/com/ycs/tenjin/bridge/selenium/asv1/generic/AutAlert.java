package com.ycs.tenjin.bridge.selenium.asv1.generic;

import java.io.File;

public class AutAlert {
	
	private String type;
	private String message;
	private File screenshot;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public File getScreenshot() {
		return screenshot;
	}
	public void setScreenshot(File screenshot) {
		this.screenshot = screenshot;
	}
	@Override
	public String toString() {
		return "AutAlert [type=" + type + ", message=" + message + "]";
	}
	
	
	
}
