package com.ycs.tenjin.bridge.selenium.asv1.taskhandler.task;

import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.pojo.aut.Metadata;

public interface LearnerTask {
	
	
	public Metadata learn() throws LearnerException;
	
}	
