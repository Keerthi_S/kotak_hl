/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GenericExtractorImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* 09-Nov-2016           Sriram Sridharan          Newly Added For Data Extraction in Tenjin 2.4.1
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
*/


package com.ycs.tenjin.bridge.selenium.asv1.taskhandler.task;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.BridgeException;
import com.ycs.tenjin.bridge.exceptions.ExecutorException;
import com.ycs.tenjin.bridge.exceptions.ExtractorException;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.selenium.asv1.SeleniumApplicationImpl;
import com.ycs.tenjin.bridge.selenium.asv1.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv1.generic.ElementIdentificationException;
import com.ycs.tenjin.util.Utilities;

public abstract class ExtractorTaskAbstract implements ExtractorTask {
	
	private static final Logger logger = LoggerFactory.getLogger(ExtractorTaskAbstract.class);
	protected SeleniumApplicationImpl oApplication;
	protected Module function;
	private ExecutorTaskAbstract impl;
	@SuppressWarnings("unused")
	private String tdUid;
	protected DriverContext driverContext;
	
	public ExtractorTaskAbstract(SeleniumApplicationImpl oApplication, Module function, String tdUid){
		this.oApplication = oApplication;
		this.function = function;
		this.tdUid = tdUid;
	}

	@Override
	public void extract(JSONObject pageArea) throws ExtractorException {
		
		
		logger.info("Loading executor implementation for [" + this.oApplication.getAppName() +"]");
		try {
			this.impl = (ExecutorTaskAbstract) TaskFactory.getExecutor(this.oApplication);
		} catch (BridgeException e) {
			
			logger.error("Could not get Executor Implementation for [{}]", this.oApplication.getAppName(), e);
			throw new ExtractorException("The extractor aborted because of an unexpected error. Please contact Tenjin Support.");
		}
		
		this.driverContext = this.oApplication.getDriverContext();
		/*******************
		 * 0. Navigate to the page
		 * 1. Get all values of all fields in the page
		 * 2. Scan for child locations of this page, repeat steps 1,2 and 3 for each page.
		 * 3. Close the page
		 */
		
		String pageName = "";
		
		try{
			pageName = pageArea.getString("PA_NAME");
		}catch(Exception e){
			logger.error("JSONError --> ",e);
			throw new ExtractorException("The extractor aborted because of an unexpected error. Please contact Tenjin Support.");
		}
		
		//Navigate to page
		logger.info("Navigating to {}", pageName);
		Location location = Location.loadFromJson(pageArea);
		this.impl.navigateToPage(location, this.driverContext, this.oApplication);
		
		//Get all values for all fields in the page
		JSONArray fields = null;
		
		try{
			if(!pageArea.getString("PA_IS_MRB").equalsIgnoreCase("YES")){
				try{
					fields = pageArea.getJSONArray("FIELDS");
				}catch(JSONException e){
					logger.warn("No fields to extract on {}", pageName);
					fields = null;
				}
				
				JSONArray queryFields = new JSONArray();
				
				if(fields != null && fields.length() > 0){
					for(int i=0; i<fields.length(); i++){
						JSONObject j = fields.getJSONObject(i);
						if(j.getString("ACTION").equalsIgnoreCase("QUERY")){
							queryFields.put(j);
						}
					}
					
					if(queryFields != null && queryFields.length() > 0){
						logger.debug("Entered query mode on page {}", pageName);
						this.impl.performPreQueryActions(this.driverContext, this.oApplication);
						
						for(int i=0; i<queryFields.length(); i++){
							JSONObject field = queryFields.getJSONObject(i);
							String dataVal = "";
							String label = "";
							String type = "";
							String uid = "";
							String uidType = "";
							/* Commented by sameer for New Adapter Spec */
//							String action = "";
							dataVal = field.getString("DATA");
							label = field.getString("FLD_LABEL");
							type = field.getString("FLD_TYPE");
							uid = field.getString("FLD_UID");
							uidType = field.getString("FLD_UID_TYPE");
//							action = field.getString("ACTION");
							
							TestObject t = new TestObject();
							t.setLabel(label);
							t.setObjectClass(type);
							t.setUniqueId(uid);
							t.setIdentifiedBy(uidType);
							logger.debug("Field: " + label + " --> Value: " + dataVal);
							WebElement element = null;
							List<WebElement> elements = null;
							//Locate the field
							logger.debug("Locating field with " + uidType + ": " + uid);
							
							elements = this.oApplication.locateElements(t.getUniqueId(), t.getIdentifiedBy());
							for (WebElement element1 : elements) {
								//VAPT Null Check Fix
								if(element1 !=null && element1.isDisplayed()){
									element = element1;
								}
							}
							
							field.put("DATA", dataVal);
							this.oApplication.performInput(element, t, dataVal);
							logger.info("Performed Query Field Input: Field --> {}, Data --> {}", label, dataVal);
						}
						
						this.impl.performPostQueryActions(this.driverContext, this.oApplication);
					}
					
					//Now loop through all fields to get their value
					for(int i=0; i<fields.length(); i++){
						JSONObject field = fields.getJSONObject(i);
						String dataVal = "";
						String label = "";
						String type = "";
						String uid = "";
						String uidType = "";
						String action = "";
						dataVal = field.getString("DATA");
						label = field.getString("FLD_LABEL");
						type = field.getString("FLD_TYPE");
						uid = field.getString("FLD_UID");
						uidType = field.getString("FLD_UID_TYPE");
						action = field.getString("ACTION");
						
						if(Utilities.trim(action).equalsIgnoreCase("query")){
							continue;
						}
						
						TestObject t = new TestObject();
						t.setLabel(label);
						t.setObjectClass(type);
						t.setUniqueId(uid);
						t.setIdentifiedBy(uidType);
						logger.debug("Field: " + label + " --> Value: " + dataVal);
						List<WebElement> elements = null;
						WebElement element = null;
						//Locate the field
						logger.debug("Locating field with " + uidType + ": " + uid);
						
						/* Commented by sameer for New Adapter Spec */
//						WebElement wrappers;
//						wrappers = this.driverContext.getWrapper();
						
						elements = this.oApplication.locateElements(t.getUniqueId(), t.getIdentifiedBy());
						//elements = this.oApplication.locateElements(wrappers, t.getUniqueId(), t.getIdentifiedBy());
						for (WebElement element1 : elements) {
							if(element1.isDisplayed() && element1 !=null){
								element = element1;
							}else if (element1.isEnabled() && element == null){
								element = element1;
							}
						}
						
						logger.debug("Getting value of field {}", label);
						
						dataVal = this.oApplication.getFieldValue(element, t);
						
						field.put("DATA", dataVal);
					}
				}else{
					logger.info("No fields to extract on page {}", pageName);
				}
			}//IF page is not MRB
			else{
				logger.info("Entering multi-record mode for page {}", pageName);
				this.extractFromMRB(pageArea, "");
			}
		}catch(ExecutorException e){
			throw new ExtractorException(e.getMessage());
		}catch(ElementIdentificationException e){
			throw new ExtractorException(e.getMessage());
		}catch(AutException e){
			throw new ExtractorException(e.getMessage());
		}catch(Exception e){
			logger.error("ERROR --> Unexpected error while extracting",e );
			throw new ExtractorException("Internal error. Please contact Tenjin Support");
		}
		
		//Scan for children
		JSONArray children = null;
		try {
			children = pageArea.getJSONArray("CHILDREN");
		} catch (JSONException e) {
			
			logger.error("JSONError while getting children of page {}", pageName, e);
		}
		
		
		if(children != null && children.length() > 0){
			for(int i=0;i<children.length();i++){
				try{
					extract(children.getJSONObject(i));
				}catch(JSONException e){
					logger.error("JSON error while attempting to scan child number {} for page {}", i, pageName, e);
				}
			}
		}
		
		//Close the page
		logger.info("Closing {}", pageName);
		try {
			this.oApplication.closeCurrentFrame();
		} catch (AutException e) {
			
			logger.error("Could not close current frame",e );
		}
	}

	
	protected abstract void extractFromMRB(JSONObject page, String parentRecordNo);
}
